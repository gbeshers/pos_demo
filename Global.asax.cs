using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Web;
using System.Web.SessionState;
using CASL_engine;
using System.Web.Configuration;
using System.Collections.Specialized;
using System.Reflection;
using System.Diagnostics;

namespace corebt
{
  /// <summary>
  /// Summary description for Global.
  /// </summary>
  public class Global : System.Web.HttpApplication
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    public Global()
    {
      InitializeComponent();
    }

    protected void Application_Start(Object sender, EventArgs e)
    {
      //
      // This prevents GenericObject.get() -> CASL_error.create()
      // infinite recursion causing stack overflow.  GMB.
      //
      c_CASL.Critical_Initialization();

      Application["message"] = "";
      // Bug #8029 Mike O - Got rid of the deprecated way to access web.config, added auto-store of appSettings pairs to GEO
      NameValueCollection appSettings = WebConfigurationManager.AppSettings;
      // Bug 11018: FT: Remove (actually make optional) codebase from Web.config
      string codebase = appSettings.Get("codebase");
      if (String.IsNullOrEmpty(codebase))
      {
        // Translate root virtual path to real path and use that
        String rootPath = Server.MapPath("~");
        rootPath = rootPath.Replace("\\", "/");
        c_CASL.set_code_base(rootPath);
      }
      else
      {
        // If codebase key is set in Web.config, use that instead
        c_CASL.set_code_base(appSettings.Get("codebase"));
      }
      // End Bug 11018

      c_CASL.set_show_verbose_error(appSettings.Get("show_verbose_error").Equals("true") ? true : false);

      c_CASL.set_Config_Load_Version(appSettings.Get("Config_Load_Version"));

      c_CASL.set_app_settings(appSettings);
      // End Bug #8029
      c_CASL.set_debug(appSettings.Get("debug").Equals("true") ? true : false);//BUG#11361

      // Bug 19256 DJD: Changed the obsolete "ConfigurationSettings" to "ConfigurationManager"

      // GH TESTING ANU
      c_CASL.set_Add_Log_Entry(ConfigurationManager.AppSettings["Add_Log_Entry"].Equals("true") ? true : false);
      c_CASL.set_Add_cs_Log_Entry(ConfigurationManager.AppSettings["Add_cs_Log_Entry"].Equals("true") ? true : false);  // Bug 19856 UMN
      // GH TESTING ANU
      c_CASL.set_disable_echo(ConfigurationManager.AppSettings["disable_echo"].Equals("true") ? true : false);

      c_CASL.SetConnectionMinPoolSize(ConfigurationManager.AppSettings["connection_pool_min"]);// BUG# 8056 DH
      c_CASL.SetConnectionMaxPoolSize(ConfigurationManager.AppSettings["connection_pool_max"]);// BUG# 8056 DH
      c_CASL.SetConnectionPacketSize(ConfigurationManager.AppSettings["connection_packet_size"]);// BUG# 8077 DH
      c_CASL.SetQueryTimeout(ConfigurationManager.AppSettings["sql_query_timeout"]);// BUG# 8135 DH

       pci.pci.setbdk(ConfigurationManager.AppSettings["ebdk3"]);//BUG16062

      // Bug 21105 - Add Secure TLS Web.Config Entries [TLS Remediation]
      System.Net.ServicePointManager.SecurityProtocol = GetSecurityProtocolFromAppSettings();
      //	  /* 
      //	   * Application["LOADING_STATUS"] 
      //	   *  site-wide variable, to be used as SYSTEM LOADING indicator, includes the following values: 
      //	   *    "PRE" : BEFORE SYSTEM/CASL LOADING PROCESS
      //	   *    "LOADING" : DURING SYSTEM/CASL LOADING PROCESS,NO APPLICATION CAN BE USED EXCEPT STATIC PAGE, SERVER LOCAL URI ACCESS, CASL IDE 
      //	   *    "FINISHED" : AFTER SYSTEM/CASL LOADING PROCESS  
      //	   *    "ERROR" : ERROR HAPPEN DURING INITIAL CASL LOADING PROCESS, NO APPLICATION CAN BE USED EXCEPT STATIC PAGE, SERVER LOCAL URI ACCESS, CASL IDE, RELOAD
      //	   * /
      Application["LOADING_STATUS"] = "PRE";

      // If requested, pause here to allow debugger to attach to the process.

      if (ConfigurationManager.AppSettings[
             "Pause_for_Visual_Studio"].Equals("true")) {
        string pause_string = ConfigurationManager.AppSettings[
               "Pause_for_Visual_Studio_seconds"];
        int pause_time = 0;
        if (Int32.TryParse(pause_string, out pause_time))
          System.Threading.Thread.Sleep(pause_time * 1000);
        else
          Logger.cs_log("Pause_for_Visual_Studio_seconds " +
                        " must be an integer '" + pause_string + "'");
        Debugger.Break();
      }

      //
      // Modify flight recorder behavior without recompiling via Web.config.
      //
      // We initialize the flight_recorder early in setup so that
      // tracing can happen even before initialize_casl_engine().
      // NOTE: control flags from Web.config are processed in this call.
      //
      Flt_Rec.initialize(
           ConfigurationManager.AppSettings["Flight_Recorder_When"],
           ConfigurationManager.AppSettings["Flight_Recorder_Where"],
           ConfigurationManager.AppSettings["Flight_Recorder_Options"]
      );

      
    }

    /// <summary>
    /// Bug 21105 - Add Secure TLS Web.Config Entries [TLS Remediation]
    /// Retrieves the "security_protocol" App Setting from the Web.Config file.  Converts pipe 
    /// delimited string values to the corresponding enum values (System.Net.SecurityProtocolType).
    /// </summary>
    /// <returns> System.Net.SecurityProtocolType </returns>
    private static System.Net.SecurityProtocolType GetSecurityProtocolFromAppSettings()
    {
      // Default to "Tls | Tls11 | Tls12" if "security_protocol" is not provided in the Web.Config (or value provided does NOT parse).
      System.Net.SecurityProtocolType sp_default = System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;
      System.Net.SecurityProtocolType sp_appSettings = System.Net.SecurityProtocolType.Tls12;
      bool useAppSetting = false;
      string s = ConfigurationManager.AppSettings["security_protocol"];
      if (!String.IsNullOrEmpty(s))
      {
        string[] protocols = s.Split('|');
        for (int i = 0; i < protocols.Length; i++)
        {
          string protocol = protocols[i].Trim();
          System.Net.SecurityProtocolType sp;
          useAppSetting = (Enum.TryParse(protocol, out sp));
          if (useAppSetting)
          {
            sp_appSettings = i == 0 ? sp : (sp_appSettings | sp);
          }
          else
          {
            break;
          }
        }
      }
      return (useAppSetting ? sp_appSettings : sp_default);
    }

    protected void Session_Start(Object sender, EventArgs e)
    {
      // Bug 23135 UMN
      Session["init"] = 0;
    }

    protected void Application_BeginRequest(Object sender, EventArgs e)
    {
      // Bug 21231 UMN PCI-DSS Remove Server header
      try
      {
        var application = sender as HttpApplication;
        if (application != null && application.Context != null)
        {
          application.Context.Response.Headers.Remove("Server");
        }
      }
      catch (Exception ex)
      {
        //Logger.cs_log(String.Format("TTS 21231: Error in Application_BeginRequest: {0}", ex.Message));
        // HX: Bug 17849
        Logger.cs_log(String.Format("TTS 21231: Error in Application_BeginRequest: {0}", ex.ToMessageAndCompleteStacktrace()));
      }

      //Bug#7994 ANU To redirect the wsdl request
      string requestPath = Request.RawUrl.Trim().ToLower();

      if (requestPath.IndexOf("?wsdl") > 0 || requestPath.IndexOf("?disco") > 0)
      {
        // BUG 16461 MS says pass false to redirect, and call completerequest to avoid thread aborts
        Response.Redirect("NotAuthorized.aspx", false);
        HttpContext.Current.Response.Clear();
        Context.ApplicationInstance.CompleteRequest();
        return;
      }
    }

    protected void Application_EndRequest(Object sender, EventArgs e)
    {

    }

    protected void Application_AuthenticateRequest(Object sender, EventArgs e)
    {

    }

    protected void Application_Error(Object sender, EventArgs e)
    {
      /*
        * + need to catch error 
        * + save error and display
        */


      //				Exception ex = Server.GetLastError().GetBaseException();
      //				Response.Write("Test Web"+
      //					"MESSAGE: " + ex.Message + 
      //					"\nSOURCE: " + ex.Source +
      //					"\nFORM: " + Request.Form.ToString() + 
      //					"\nQUERYSTRING: " + Request.QueryString.ToString() +
      //					"\nTARGETSITE: " + ex.TargetSite +
      //					"\nSTACKTRACE: " + ex.StackTrace);
      
      // Bug#8455 Added by Anu for handling Application Error for PCI
      Exception ex = Server.GetLastError();
      var httpCode = ((HttpException)ex).GetHttpCode();
      Server.ClearError();
      
      Logger.cs_log(String.Format("Application_Error exception: {0}", ex.Message));
      Logger.cs_log(String.Format("Application_Error base exception: {0}", ex.GetBaseException().Message));

      if (ex is HttpRequestValidationException)
      {
        try
        {
          Response.Clear();
          Response.StatusCode = 200;
          // Bug 16644 UMN XSS fix
          Response.Write(@"<html><head><center><title>HTML Not Allowed</title></head>
          <body style='font-family: Arial, Sans-serif;'><h1>Invalid Input</h1><p>HTML entry is not allowed on that page.</p><p>Please make sure that your entries do not contain any special characters like &lt; or &gt;.</p>
           </center></body></html>");
        }
        catch
        {
        }
      }
      // Bug 10457 UMN handle interaction with customErrors
      else if (ex is HttpException && !Response.IsRequestBeingRedirected)
      {
        try
        {
          HttpContext.Current.Response.Clear();
          if (httpCode >= 500)
            Response.Redirect("50x.htm", false);
          else
            Response.Redirect("40x.htm", false);
        }
        catch
        {
        }
      }
      Context.ApplicationInstance.CompleteRequest();
    }

    protected void Session_End(Object sender, EventArgs e)
    {
      // Bug 17617 MJO - Catch and log exceptions here in case the site is in the process of going down
      try
      {
        // Bug #13681 Mike O - If this session is responsible for locking Config, release the lock
        if (c_CASL.GEO.has("Config")) {
          GenericObject conf = (GenericObject)c_CASL.c_object("Config");
          if (conf != null &&
              conf.get("_in_use", "").Equals(Session.SessionID)) {

            CASL_Stack.Initialization();  // required for CASL_call().  IPAY-323.  GMB.
            
            // Bug # 21057 - EOM - Close browser in config locks config
            // permanently
            // - Original call: c_CASL.c_call("release_app", "_subject",
            //   c_CASL.c_object("Config")); Did not execute. 
            // This event gets fired at tail end of Authentication logon
            // (for unknown reasons) and destroys the session that obtains
            // the lock in Config
            misc.CASL_call("a_method", "release_app",
                "_subject", ((GenericObject)c_CASL.c_object("Config")));
          }
        }
      }
      catch (Exception ex)
      {
        try
        {
          //Logger.cs_log(String.Format("Exception caught in Global.Session_End: {0}{1}{2}", ex.Message, Environment.NewLine, ex.StackTrace));
          // HX: Bug 17849
          Logger.cs_log(String.Format("Exception caught in Global.Session_End: {0}", ex.ToMessageAndCompleteStacktrace()));
        }
        catch
        {
        }
      }
    }

    protected void Application_End(Object sender, EventArgs e)
    {
      // Bug 17617 UMN put back old code commented out by Bug 11558
      try
      {
        // Bug#8054 ANU  To log the .NET error in the Event viewer.
        HttpRuntime runtime = (HttpRuntime)typeof(System.Web.HttpRuntime).InvokeMember("_theRuntime",
                                                                                    BindingFlags.NonPublic
                                                                                    | BindingFlags.Static
                                                                                    | BindingFlags.GetField,
                                                                                    null,
                                                                                    null,
                                                                                    null);

        if (runtime == null)
          return;

        string shutDownMessage = (string)runtime.GetType().InvokeMember("_shutDownMessage",
                                                                         BindingFlags.NonPublic
                                                                         | BindingFlags.Instance
                                                                         | BindingFlags.GetField,
                                                                         null,
                                                                         runtime,
                                                                         null);

        string shutDownStack = (string)runtime.GetType().InvokeMember("_shutDownStack",
                                                                       BindingFlags.NonPublic
                                                                       | BindingFlags.Instance
                                                                       | BindingFlags.GetField,
                                                                       null,
                                                                       runtime,
                                                                       null);

        if (!EventLog.SourceExists(".NET Runtime"))
        {
          EventLog.CreateEventSource(".NET Runtime", "Application");
        }

        EventLog log = new EventLog();
        log.Source = ".NET Runtime";
        log.WriteEntry(String.Format("\r\n\r\n_shutDownMessage={0}\r\n\r\n_shutDownStack={1}",
                                     shutDownMessage,
                                     shutDownStack),
                                     EventLogEntryType.Error);
        // Bug#8054 ANU  end - To log the .NET error in the Event viewer.
      }
      catch
      {
      }
    }

    #region Web Form Designer generated code
    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
    }
    #endregion
  }

}

