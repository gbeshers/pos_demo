// Bug #11543 Mike O
// Thick Client Peripherals Loader

function GetPeripherals() {
  return window.external.peripherals;
}

function GetIngenico() {
  return window.external.ingenico;
}

function GetXMLReadWrite() {
  return window.external.xml;
}