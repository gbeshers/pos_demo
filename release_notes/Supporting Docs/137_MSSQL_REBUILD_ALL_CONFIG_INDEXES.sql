/* ---------------------------------------------------------------------------
--    	Copyright 2015 Core Business Technologies
--
--  	SQL Server DB ONLY
--		DESCRIPTION:
--			Script rebuilds all NONCLUSTERED and CLUSTERED(PRIMARY KEYs) indexes and statistics in the configuration database.
--			Can be run on either Standard or Enterprise SQL Server and with and with the TG_USERSESSION_DATA table.
--			This must be ran on regular basis - weekly or bi-weekly.

-- 		TranSuite DB Version 4.0.9.0
--
-- 		Last Updated: 
--    	1.) TTS: 17339: FT: Created this script]. 
*/
DECLARE @compression VARCHAR(128)
set @compression = 'NONE'
IF (EXISTS (SELECT * FROM sys.dm_db_persisted_sku_features where feature_name = 'Compression'))
  set @compression = 'PAGE'
Exec ('ALTER INDEX ALL ON CBT_Flex REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
-- Bug 22934 UMN Remove Flex_bytes 
Exec ('ALTER INDEX ALL ON CBT_Revision REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON CONNECT_TEST REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON TG_DYNAMIC_CONFIG REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TG_USERSESSION_DATA]') AND type in (N'U'))
	Exec ('ALTER INDEX ALL ON TG_USERSESSION_DATA REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')')

Exec ('ALTER TABLE [dbo].[CBT_Flex] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')')
-- Bug 22934 UMN Remove Flex_bytes
Exec ('ALTER TABLE [dbo].[CBT_Revision] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')')
Exec ('ALTER TABLE [dbo].[CONNECT_TEST] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')')
Exec ('ALTER TABLE [dbo].[TG_DYNAMIC_CONFIG] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')')
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TG_USERSESSION_DATA]') AND type in (N'U'))
	Exec ('ALTER TABLE [dbo].[TG_USERSESSION_DATA] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')')

GO


UPDATE STATISTICS CBT_Flex 
GO
-- Bug 22934 UMN Remove Flex_bytes 
UPDATE STATISTICS CBT_Revision 
GO
UPDATE STATISTICS CONNECT_TEST 
GO
UPDATE STATISTICS TG_DYNAMIC_CONFIG
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TG_USERSESSION_DATA]') AND type in (N'U'))
	Exec ('UPDATE STATISTICS TG_USERSESSION_DATA')
GO
