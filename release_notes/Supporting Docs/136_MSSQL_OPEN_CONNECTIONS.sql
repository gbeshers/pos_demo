
/* ---------------------------------------------------------------------------
--    	Copyright 2014 Core Business Technologies
--
--  	SQL Server DB ONLY
--		DESCRIPTION:
--
--   Simple script to list the number of open connections
-- 		Last Updated:
--    1)  02/09/2015 FT. Bug: 16758. Created.
--    
*/

SELECT 
    DB_NAME(dbid) as DBName, 
    COUNT(dbid) as NumberOfConnections,
    loginame as LoginName
FROM
    sys.sysprocesses
WHERE 
    dbid > 0
GROUP BY 
    dbid, loginame
;

