/* ---------------------------------------------------------------------------
--    	Copyright 2015 Core Business Technologies
--
--  	SQL Server DB ONLY
--		DESCRIPTION:
--			Script rebuilds all NONCLUSTERED and CLUSTERED(PRIMARY KEYs) indexes and statistics on T4 Inquiry tables only.
--			Can be run on either Standard or Enterprise SQL Server and with and with the TG_USERSESSION_DATA table.
--			This must be ran on regular basis - weekly or bi-weekly.

-- 		TranSuite DB Version 4.0.9.0
--
-- 		Last Updated: 
--    	1.)  TTS: 17339: June, 2015 by FT 
*/


DECLARE @compression VARCHAR(128)
set @compression = 'NONE'
IF (EXISTS (SELECT * FROM sys.dm_db_persisted_sku_features where feature_name = 'Compression'))
  set @compression = 'PAGE'
Exec ('ALTER INDEX ALL ON SI_DATA1 REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON SI_DATA2 REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON SI_KEY1 REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON SI_KEY2 REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON SI_NAMES REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON SI_WEIGHTS1 REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON SI_WEIGHTS2 REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 

Exec ('ALTER TABLE [dbo].[SI_KEY1] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')')
Exec ('ALTER TABLE [dbo].[SI_KEY2] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')')
Exec ('ALTER TABLE [dbo].[SI_DATA1] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')')
Exec ('ALTER TABLE [dbo].[SI_DATA2] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')')
Exec ('ALTER TABLE [dbo].[SI_NAMES] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')')
Exec ('ALTER TABLE [dbo].[SI_WEIGHTS1] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')')
Exec ('ALTER TABLE [dbo].[SI_WEIGHTS2] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')')
GO


UPDATE STATISTICS SI_DATA1
GO
UPDATE STATISTICS SI_DATA2 
GO
UPDATE STATISTICS SI_KEY1 
GO
UPDATE STATISTICS SI_KEY2 
GO
UPDATE STATISTICS SI_NAMES 
GO
UPDATE STATISTICS SI_WEIGHTS1
GO
UPDATE STATISTICS SI_WEIGHTS2
GO
