/* ---------------------------------------------------------------------------
--    	Copyright 2016 Core Business Technologies
--
--  	SQL Server DB ONLY
--		DESCRIPTION:
--
--   This script was created by Daniel Hofman on 3/27/2012 for bug# 9385.
--    It upgrades the older database to the newer higher performance database.
--    It can also be re-used since it implements a conditional control flow for all the objects created. 
--	  This means that you can run it to upgrade an older version or re-configure a current version.

-- 		Last Updated:
--    1)  02/05/2014 FT. Bug: 15248. Added non-clustered index to Activity Log.
--    2)  09/04/2014 UMN Add various indexes for performance optimization from various projects. Also sanitize
                         index names, and include indexes from the NONCLUSTERED_INDEXES.sql file.
--    3)  10/17/2014 FT. Bug: 16303 Changed TG_PAYEVENT_DATA clustered index to be on the UNIQUEID
--    4)  02/13/2015 FT. Bug 16771. Removed the CUSTLABEL non-clustered because it seems to have caused Geisinger timeouts during INSERTS and does NOT help the DISTINCT query
--    5)  03/11/2015 FT. Bug 16771. Changed Uttam's IX_CUST_FIELD_DATA_DEPFILENBR_DEPFILESEQ_EVENTNBR to include all the columns.
--    6)  10/13/2015 FT. Bug 17796. Added NTirety's recommended indexes.
--    7)  10/14/2015 FT. Bug 17796. Added index recommended by NTirety for TG_DEPOSIT_DATA and used for reporting.
--    8)  01/28/2015 FT. Bug 18615. Fixed ordering in the CUST_FIELD_DATA non-clustered index
--    9)  04/15/2015 FT. Bug 18766. Fixed primary key on PAYEVENT.  Moved EVENTNBR up IX_TG_PAYEVENT_DATA_DEPFILENBR_DEPFILESEQ_USERID_EVENTNBR_STATUS
*/

-- UMN suppress rows affected message in favor of useful messages that indicate what is going on
SET NOCOUNT ON;

DECLARE @database nvarchar(50)
DECLARE @tableName VARCHAR(128)
DECLARE @sql nvarchar(255)
set @database = db_name() -- Use the currently connected

-- Remove the primary keys =================================================================================
set @tableName = 'GR_CUST_FIELD_DATA'
WHILE EXISTS (select 1 from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where constraint_catalog = @database and table_name = @tableName and CONSTRAINT_TYPE = 'PRIMARY KEY' ) 
BEGIN     
select  @sql = 'ALTER TABLE [' + table_name + '] DROP CONSTRAINT [' + CONSTRAINT_NAME  + ']'     
from    INFORMATION_SCHEMA.TABLE_CONSTRAINTS      
where    constraint_catalog = @database and table_name = @tableName and CONSTRAINT_TYPE = 'PRIMARY KEY'     
exec    sp_executesql @sql 
END 

set @tableName = 'CUST_FIELD_DATA'
WHILE EXISTS (select 1 from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where constraint_catalog = @database and table_name = @tableName and CONSTRAINT_TYPE = 'PRIMARY KEY' ) 
BEGIN     
select  @sql = 'ALTER TABLE [' + table_name + '] DROP CONSTRAINT [' + CONSTRAINT_NAME  + ']'     
from    INFORMATION_SCHEMA.TABLE_CONSTRAINTS      
where    constraint_catalog = @database and table_name = @tableName and CONSTRAINT_TYPE = 'PRIMARY KEY'     
exec    sp_executesql @sql 
END 

set @tableName = 'TG_ITEM_DATA'
WHILE EXISTS (select 1 from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where constraint_catalog = @database and table_name = @tableName and CONSTRAINT_TYPE = 'PRIMARY KEY' ) 
BEGIN     
select  @sql = 'ALTER TABLE [' + table_name + '] DROP CONSTRAINT [' + CONSTRAINT_NAME  + ']'     
from    INFORMATION_SCHEMA.TABLE_CONSTRAINTS      
where    constraint_catalog = @database and table_name = @tableName and CONSTRAINT_TYPE = 'PRIMARY KEY'     
exec    sp_executesql @sql 
END

set @tableName = 'TG_TENDER_DATA'
WHILE EXISTS (select 1 from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where constraint_catalog = @database and table_name = @tableName and CONSTRAINT_TYPE = 'PRIMARY KEY' ) 
BEGIN     
select  @sql = 'ALTER TABLE [' + table_name + '] DROP CONSTRAINT [' + CONSTRAINT_NAME  + ']'     
from    INFORMATION_SCHEMA.TABLE_CONSTRAINTS      
where    constraint_catalog = @database and table_name = @tableName and CONSTRAINT_TYPE = 'PRIMARY KEY'     
exec    sp_executesql @sql 
END

set @tableName = 'TG_TRAN_DATA'
WHILE EXISTS (select 1 from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where constraint_catalog = @database and table_name = @tableName and CONSTRAINT_TYPE = 'PRIMARY KEY' ) 
BEGIN     
select  @sql = 'ALTER TABLE [' + table_name + '] DROP CONSTRAINT [' + CONSTRAINT_NAME  + ']'     
from    INFORMATION_SCHEMA.TABLE_CONSTRAINTS      
where    constraint_catalog = @database and table_name = @tableName and CONSTRAINT_TYPE = 'PRIMARY KEY'     
exec    sp_executesql @sql 
END

set @tableName = 'TG_PAYEVENT_DATA'
WHILE EXISTS (select 1 from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where constraint_catalog = @database and table_name = @tableName and CONSTRAINT_TYPE = 'PRIMARY KEY' ) 
BEGIN     
select  @sql = 'ALTER TABLE [' + table_name + '] DROP CONSTRAINT [' + CONSTRAINT_NAME  + ']'     
from    INFORMATION_SCHEMA.TABLE_CONSTRAINTS      
where    constraint_catalog = @database and table_name = @tableName and CONSTRAINT_TYPE = 'PRIMARY KEY'     
exec    sp_executesql @sql 
END

set @tableName = 'TG_DEPFILE_DATA'
WHILE EXISTS (select 1 from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where constraint_catalog = @database and table_name = @tableName and CONSTRAINT_TYPE = 'PRIMARY KEY' ) 
BEGIN     
select  @sql = 'ALTER TABLE [' + table_name + '] DROP CONSTRAINT [' + CONSTRAINT_NAME  + ']'     
from    INFORMATION_SCHEMA.TABLE_CONSTRAINTS      
where    constraint_catalog = @database and table_name = @tableName and CONSTRAINT_TYPE = 'PRIMARY KEY'     
exec    sp_executesql @sql 
END

set @tableName = 'TG_USERSESSION_DATA'
WHILE EXISTS (select 1 from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where constraint_catalog = @database and table_name = @tableName and CONSTRAINT_TYPE = 'PRIMARY KEY' ) 
BEGIN     
select  @sql = 'ALTER TABLE [' + table_name + '] DROP CONSTRAINT [' + CONSTRAINT_NAME  + ']'     
from    INFORMATION_SCHEMA.TABLE_CONSTRAINTS      
where    constraint_catalog = @database and table_name = @tableName and CONSTRAINT_TYPE = 'PRIMARY KEY'     
exec    sp_executesql @sql 
END

set @tableName = 'TG_TTA_MAP_DATA'
WHILE EXISTS (select 1 from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where constraint_catalog = @database and table_name = @tableName and CONSTRAINT_TYPE = 'PRIMARY KEY' ) 
BEGIN     
select  @sql = 'ALTER TABLE [' + table_name + '] DROP CONSTRAINT [' + CONSTRAINT_NAME  + ']'     
from    INFORMATION_SCHEMA.TABLE_CONSTRAINTS      
where    constraint_catalog = @database and table_name = @tableName and CONSTRAINT_TYPE = 'PRIMARY KEY'     
exec    sp_executesql @sql 
END

set @tableName = 'TG_ACTLOG_DATA'
WHILE EXISTS (select 1 from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where constraint_catalog = @database and table_name = @tableName and CONSTRAINT_TYPE = 'PRIMARY KEY' ) 
BEGIN     
select  @sql = 'ALTER TABLE [' + table_name + '] DROP CONSTRAINT [' + CONSTRAINT_NAME  + ']'     
from    INFORMATION_SCHEMA.TABLE_CONSTRAINTS      
where    constraint_catalog = @database and table_name = @tableName and CONSTRAINT_TYPE = 'PRIMARY KEY'     
exec    sp_executesql @sql 
END 
GO

PRINT 'Removed the primary keys.'
-- END Remove the primary keys =================================================================================

-- Remove the indexes =================================================================================
DECLARE @tableName VARCHAR(128)
DECLARE @table_name VARCHAR(128)
DECLARE @indexName VARCHAR(128)

set @table_name = 'GR_CUST_FIELD_DATA'
DECLARE [indexes] CURSOR FOR SELECT [sysindexes].[name] AS [Index],[sysobjects].[name] AS [Table]
FROM [sysindexes] INNER JOIN [sysobjects] ON [sysindexes].[id] = [sysobjects].[id] and [sysobjects].[name] = @table_name
WHERE [sysindexes].[name] IS NOT NULL AND [sysobjects].[type] = 'U' and [sysindexes].[name] NOT like '%_WA_%' AND [sysindexes].[name] NOT like '%_dta_stat_%'
OPEN [indexes]
FETCH NEXT FROM [indexes] INTO @indexName, @tableName WHILE @@FETCH_STATUS = 0 
BEGIN    
Exec ('DROP INDEX [' + @indexName + '] ON [' + @tableName + ']') FETCH NEXT FROM [indexes]
INTO @indexName, @tableName 
END 
CLOSE [indexes]
DEALLOCATE [indexes]

set @table_name = 'CUST_FIELD_DATA'
DECLARE [indexes] CURSOR FOR SELECT [sysindexes].[name] AS [Index],[sysobjects].[name] AS [Table]
FROM [sysindexes] INNER JOIN [sysobjects] ON [sysindexes].[id] = [sysobjects].[id] and [sysobjects].[name] = @table_name
WHERE [sysindexes].[name] IS NOT NULL AND [sysobjects].[type] = 'U' and [sysindexes].[name] NOT like '%_WA_%' AND [sysindexes].[name] NOT like '%_dta_stat_%'
OPEN [indexes]
FETCH NEXT FROM [indexes] INTO @indexName, @tableName WHILE @@FETCH_STATUS = 0 
BEGIN    
Exec ('DROP INDEX [' + @indexName + '] ON [' + @tableName + ']') FETCH NEXT FROM [indexes]
INTO @indexName, @tableName 
END 
CLOSE [indexes]
DEALLOCATE [indexes]

set @table_name = 'TG_ITEM_DATA'
DECLARE [indexes] CURSOR FOR SELECT [sysindexes].[name] AS [Index],[sysobjects].[name] AS [Table]
FROM [sysindexes] INNER JOIN [sysobjects] ON [sysindexes].[id] = [sysobjects].[id] and [sysobjects].[name] = @table_name
WHERE [sysindexes].[name] IS NOT NULL AND [sysobjects].[type] = 'U' and [sysindexes].[name] NOT like '%_WA_%' AND [sysindexes].[name] NOT like '%_dta_stat_%'
OPEN [indexes]
FETCH NEXT FROM [indexes] INTO @indexName, @tableName WHILE @@FETCH_STATUS = 0 
BEGIN    
Exec ('DROP INDEX [' + @indexName + '] ON [' + @tableName + ']') FETCH NEXT FROM [indexes]
INTO @indexName, @tableName 
END 
CLOSE [indexes]
DEALLOCATE [indexes]

set @table_name = 'TG_TENDER_DATA'
DECLARE [indexes] CURSOR FOR SELECT [sysindexes].[name] AS [Index],[sysobjects].[name] AS [Table]
FROM [sysindexes] INNER JOIN [sysobjects] ON [sysindexes].[id] = [sysobjects].[id] and [sysobjects].[name] = @table_name
WHERE [sysindexes].[name] IS NOT NULL AND [sysobjects].[type] = 'U' and [sysindexes].[name] NOT like '%_WA_%' AND [sysindexes].[name] NOT like '%_dta_stat_%'
OPEN [indexes]
FETCH NEXT FROM [indexes] INTO @indexName, @tableName WHILE @@FETCH_STATUS = 0 
BEGIN    
Exec ('DROP INDEX [' + @indexName + '] ON [' + @tableName + ']') FETCH NEXT FROM [indexes]
INTO @indexName, @tableName 
END 
CLOSE [indexes]
DEALLOCATE [indexes]

set @table_name = 'TG_TRAN_DATA'
DECLARE [indexes] CURSOR FOR SELECT [sysindexes].[name] AS [Index],[sysobjects].[name] AS [Table]
FROM [sysindexes] INNER JOIN [sysobjects] ON [sysindexes].[id] = [sysobjects].[id] and [sysobjects].[name] = @table_name
WHERE [sysindexes].[name] IS NOT NULL AND [sysobjects].[type] = 'U' and [sysindexes].[name] NOT like '%_WA_%' AND [sysindexes].[name] NOT like '%_dta_stat_%'
OPEN [indexes]
FETCH NEXT FROM [indexes] INTO @indexName, @tableName WHILE @@FETCH_STATUS = 0 
BEGIN    
Exec ('DROP INDEX [' + @indexName + '] ON [' + @tableName + ']') FETCH NEXT FROM [indexes]
INTO @indexName, @tableName 
END 
CLOSE [indexes]
DEALLOCATE [indexes]

set @table_name = 'TG_PAYEVENT_DATA'
DECLARE [indexes] CURSOR FOR SELECT [sysindexes].[name] AS [Index],[sysobjects].[name] AS [Table]
FROM [sysindexes] INNER JOIN [sysobjects] ON [sysindexes].[id] = [sysobjects].[id] and [sysobjects].[name] = @table_name
WHERE [sysindexes].[name] IS NOT NULL AND [sysobjects].[type] = 'U' and [sysindexes].[name] NOT like '%_WA_%' AND [sysindexes].[name] NOT like '%_dta_stat_%'
OPEN [indexes]
FETCH NEXT FROM [indexes] INTO @indexName, @tableName WHILE @@FETCH_STATUS = 0 
BEGIN    
Exec ('DROP INDEX [' + @indexName + '] ON [' + @tableName + ']') FETCH NEXT FROM [indexes]
INTO @indexName, @tableName 
END 
CLOSE [indexes]
DEALLOCATE [indexes]

set @table_name = 'TG_DEPFILE_DATA'
DECLARE [indexes] CURSOR FOR SELECT [sysindexes].[name] AS [Index],[sysobjects].[name] AS [Table]
FROM [sysindexes] INNER JOIN [sysobjects] ON [sysindexes].[id] = [sysobjects].[id] and [sysobjects].[name] = @table_name
WHERE [sysindexes].[name] IS NOT NULL AND [sysobjects].[type] = 'U' and [sysindexes].[name] NOT like '%_WA_%' AND [sysindexes].[name] NOT like '%_dta_stat_%'
OPEN [indexes]
FETCH NEXT FROM [indexes] INTO @indexName, @tableName WHILE @@FETCH_STATUS = 0 
BEGIN    
Exec ('DROP INDEX [' + @indexName + '] ON [' + @tableName + ']') FETCH NEXT FROM [indexes]
INTO @indexName, @tableName 
END 
CLOSE [indexes]
DEALLOCATE [indexes]

set @table_name = 'TG_TTA_MAP_DATA'
DECLARE [indexes] CURSOR FOR SELECT [sysindexes].[name] AS [Index],[sysobjects].[name] AS [Table]
FROM [sysindexes] INNER JOIN [sysobjects] ON [sysindexes].[id] = [sysobjects].[id] and [sysobjects].[name] = @table_name
WHERE [sysindexes].[name] IS NOT NULL AND [sysobjects].[type] = 'U' and [sysindexes].[name] NOT like '%_WA_%' AND [sysindexes].[name] NOT like '%_dta_stat_%'
OPEN [indexes]
FETCH NEXT FROM [indexes] INTO @indexName, @tableName WHILE @@FETCH_STATUS = 0 
BEGIN    
Exec ('DROP INDEX [' + @indexName + '] ON [' + @tableName + ']') FETCH NEXT FROM [indexes]
INTO @indexName, @tableName 
END 
CLOSE [indexes]
DEALLOCATE [indexes]

set @table_name = 'TG_ACTLOG_DATA'
DECLARE [indexes] CURSOR FOR SELECT [sysindexes].[name] AS [Index],[sysobjects].[name] AS [Table]
FROM [sysindexes] INNER JOIN [sysobjects] ON [sysindexes].[id] = [sysobjects].[id] and [sysobjects].[name] = @table_name
WHERE [sysindexes].[name] IS NOT NULL AND [sysobjects].[type] = 'U' and [sysindexes].[name] NOT like '%_WA_%' AND [sysindexes].[name] NOT like '%_dta_stat_%'
OPEN [indexes]
FETCH NEXT FROM [indexes] INTO @indexName, @tableName WHILE @@FETCH_STATUS = 0 
BEGIN    
Exec ('DROP INDEX [' + @indexName + '] ON [' + @tableName + ']') FETCH NEXT FROM [indexes]
INTO @indexName, @tableName 
END 
CLOSE [indexes]
DEALLOCATE [indexes]

-- Non-standard tables

set @table_name = 'TG_SYSTEM_INTERFACE_TRACKING'
DECLARE [indexes] CURSOR FOR SELECT [sysindexes].[name] AS [Index],[sysobjects].[name] AS [Table]
FROM [sysindexes] INNER JOIN [sysobjects] ON [sysindexes].[id] = [sysobjects].[id] and [sysobjects].[name] = @table_name
WHERE [sysindexes].[name] IS NOT NULL AND [sysobjects].[type] = 'U' and [sysindexes].[name] NOT like '%_WA_%' AND [sysindexes].[name] NOT like '%_dta_stat_%'
AND INDEXPROPERTY([sysindexes].id, [sysindexes].name, 'IsClustered') = 0
OPEN [indexes]
FETCH NEXT FROM [indexes] INTO @indexName, @tableName WHILE @@FETCH_STATUS = 0 
BEGIN    
Exec ('DROP INDEX [' + @indexName + '] ON [' + @tableName + ']') FETCH NEXT FROM [indexes]
INTO @indexName, @tableName 
END 
CLOSE [indexes]
DEALLOCATE [indexes]

set @table_name = 'TG_SUSPEVENT_DATA'
DECLARE [indexes] CURSOR FOR SELECT [sysindexes].[name] AS [Index],[sysobjects].[name] AS [Table]
FROM [sysindexes] INNER JOIN [sysobjects] ON [sysindexes].[id] = [sysobjects].[id] and [sysobjects].[name] = @table_name
WHERE [sysindexes].[name] IS NOT NULL AND [sysobjects].[type] = 'U' and [sysindexes].[name] NOT like '%_WA_%' AND [sysindexes].[name] NOT like '%_dta_stat_%'
AND INDEXPROPERTY([sysindexes].id, [sysindexes].name, 'IsClustered') = 0
OPEN [indexes]
FETCH NEXT FROM [indexes] INTO @indexName, @tableName WHILE @@FETCH_STATUS = 0 
BEGIN    
Exec ('DROP INDEX [' + @indexName + '] ON [' + @tableName + ']') FETCH NEXT FROM [indexes]
INTO @indexName, @tableName 
END 
CLOSE [indexes]
DEALLOCATE [indexes]

set @table_name = 'TG_FILESOURCE_DATA'
DECLARE [indexes] CURSOR FOR SELECT [sysindexes].[name] AS [Index],[sysobjects].[name] AS [Table]
FROM [sysindexes] INNER JOIN [sysobjects] ON [sysindexes].[id] = [sysobjects].[id] and [sysobjects].[name] = @table_name
WHERE [sysindexes].[name] IS NOT NULL AND [sysobjects].[type] = 'U' and [sysindexes].[name] NOT like '%_WA_%' AND [sysindexes].[name] NOT like '%_dta_stat_%'
AND INDEXPROPERTY([sysindexes].id, [sysindexes].name, 'IsClustered') = 0
OPEN [indexes]
FETCH NEXT FROM [indexes] INTO @indexName, @tableName WHILE @@FETCH_STATUS = 0 
BEGIN    
Exec ('DROP INDEX [' + @indexName + '] ON [' + @tableName + ']') FETCH NEXT FROM [indexes]
INTO @indexName, @tableName 
END 
CLOSE [indexes]
DEALLOCATE [indexes]

set @table_name = 'TG_BANK_DEPOSIT_DATA'
DECLARE [indexes] CURSOR FOR SELECT [sysindexes].[name] AS [Index],[sysobjects].[name] AS [Table]
FROM [sysindexes] INNER JOIN [sysobjects] ON [sysindexes].[id] = [sysobjects].[id] and [sysobjects].[name] = @table_name
WHERE [sysindexes].[name] IS NOT NULL AND [sysobjects].[type] = 'U' and [sysindexes].[name] NOT like '%_WA_%' AND [sysindexes].[name] NOT like '%_dta_stat_%'
AND INDEXPROPERTY([sysindexes].id, [sysindexes].name, 'IsClustered') = 0
OPEN [indexes]
FETCH NEXT FROM [indexes] INTO @indexName, @tableName WHILE @@FETCH_STATUS = 0 
BEGIN    
Exec ('DROP INDEX [' + @indexName + '] ON [' + @tableName + ']') FETCH NEXT FROM [indexes]
INTO @indexName, @tableName 
END 
CLOSE [indexes]
DEALLOCATE [indexes]

set @table_name = 'TG_BANK_DEPOSIT_DATA'
DECLARE [indexes] CURSOR FOR SELECT [sysindexes].[name] AS [Index],[sysobjects].[name] AS [Table]
FROM [sysindexes] INNER JOIN [sysobjects] ON [sysindexes].[id] = [sysobjects].[id] and [sysobjects].[name] = @table_name
WHERE [sysindexes].[name] IS NOT NULL AND [sysobjects].[type] = 'U' and [sysindexes].[name] NOT like '%_WA_%' AND [sysindexes].[name] NOT like '%_dta_stat_%'
AND INDEXPROPERTY([sysindexes].id, [sysindexes].name, 'IsClustered') = 0
OPEN [indexes]
FETCH NEXT FROM [indexes] INTO @indexName, @tableName WHILE @@FETCH_STATUS = 0 
BEGIN    
Exec ('DROP INDEX [' + @indexName + '] ON [' + @tableName + ']') FETCH NEXT FROM [indexes]
INTO @indexName, @tableName 
END 
CLOSE [indexes]
DEALLOCATE [indexes]

set @table_name = 'TG_DEPOSIT_DATA'
DECLARE [indexes] CURSOR FOR SELECT [sysindexes].[name] AS [Index],[sysobjects].[name] AS [Table]
FROM [sysindexes] INNER JOIN [sysobjects] ON [sysindexes].[id] = [sysobjects].[id] and [sysobjects].[name] = @table_name
WHERE [sysindexes].[name] IS NOT NULL AND [sysobjects].[type] = 'U' and [sysindexes].[name] NOT like '%_WA_%' AND [sysindexes].[name] NOT like '%_dta_stat_%'
AND INDEXPROPERTY([sysindexes].id, [sysindexes].name, 'IsClustered') = 0
OPEN [indexes]
FETCH NEXT FROM [indexes] INTO @indexName, @tableName WHILE @@FETCH_STATUS = 0 
BEGIN    
Exec ('DROP INDEX [' + @indexName + '] ON [' + @tableName + ']') FETCH NEXT FROM [indexes]
INTO @indexName, @tableName 
END 
CLOSE [indexes]
DEALLOCATE [indexes]

set @table_name = 'TG_RECONCILED_BANK_DATA'
DECLARE [indexes] CURSOR FOR SELECT [sysindexes].[name] AS [Index],[sysobjects].[name] AS [Table]
FROM [sysindexes] INNER JOIN [sysobjects] ON [sysindexes].[id] = [sysobjects].[id] and [sysobjects].[name] = @table_name
WHERE [sysindexes].[name] IS NOT NULL AND [sysobjects].[type] = 'U' and [sysindexes].[name] NOT like '%_WA_%' AND [sysindexes].[name] NOT like '%_dta_stat_%'
AND INDEXPROPERTY([sysindexes].id, [sysindexes].name, 'IsClustered') = 0
OPEN [indexes]
FETCH NEXT FROM [indexes] INTO @indexName, @tableName WHILE @@FETCH_STATUS = 0 
BEGIN    
Exec ('DROP INDEX [' + @indexName + '] ON [' + @tableName + ']') FETCH NEXT FROM [indexes]
INTO @indexName, @tableName 
END 
CLOSE [indexes]
DEALLOCATE [indexes]

set @table_name = 'TG_RECONCILED_EXPECTED_DATA'
DECLARE [indexes] CURSOR FOR SELECT [sysindexes].[name] AS [Index],[sysobjects].[name] AS [Table]
FROM [sysindexes] INNER JOIN [sysobjects] ON [sysindexes].[id] = [sysobjects].[id] and [sysobjects].[name] = @table_name
WHERE [sysindexes].[name] IS NOT NULL AND [sysobjects].[type] = 'U' and [sysindexes].[name] NOT like '%_WA_%' AND [sysindexes].[name] NOT like '%_dta_stat_%'
AND INDEXPROPERTY([sysindexes].id, [sysindexes].name, 'IsClustered') = 0
OPEN [indexes]
FETCH NEXT FROM [indexes] INTO @indexName, @tableName WHILE @@FETCH_STATUS = 0 
BEGIN    
Exec ('DROP INDEX [' + @indexName + '] ON [' + @tableName + ']') FETCH NEXT FROM [indexes]
INTO @indexName, @tableName 
END 
CLOSE [indexes]
DEALLOCATE [indexes]

set @table_name = 'TG_RECONCILED_DATA'
DECLARE [indexes] CURSOR FOR SELECT [sysindexes].[name] AS [Index],[sysobjects].[name] AS [Table]
FROM [sysindexes] INNER JOIN [sysobjects] ON [sysindexes].[id] = [sysobjects].[id] and [sysobjects].[name] = @table_name
WHERE [sysindexes].[name] IS NOT NULL AND [sysobjects].[type] = 'U' and [sysindexes].[name] NOT like '%_WA_%' AND [sysindexes].[name] NOT like '%_dta_stat_%'
AND INDEXPROPERTY([sysindexes].id, [sysindexes].name, 'IsClustered') = 0
OPEN [indexes]
FETCH NEXT FROM [indexes] INTO @indexName, @tableName WHILE @@FETCH_STATUS = 0 
BEGIN    
Exec ('DROP INDEX [' + @indexName + '] ON [' + @tableName + ']') FETCH NEXT FROM [indexes]
INTO @indexName, @tableName 
END 
CLOSE [indexes]
DEALLOCATE [indexes]

GO
PRINT 'Removed existing indexes.'
-- END Remove the indexes =================================================================================

-- Create primary keys after removing the fields =====================================================
if EXISTS (select column_name from INFORMATION_SCHEMA.columns where table_name = 'GR_CUST_FIELD_DATA' and column_name = 'PKID') 
  ALTER TABLE GR_CUST_FIELD_DATA DROP COLUMN PKID

if EXISTS (select column_name from INFORMATION_SCHEMA.columns where table_name = 'CUST_FIELD_DATA' and column_name = 'PKID') 
  ALTER TABLE CUST_FIELD_DATA DROP COLUMN PKID

if EXISTS (select column_name from INFORMATION_SCHEMA.columns where table_name = 'TG_ITEM_DATA' and column_name = 'PKID') 
  ALTER TABLE TG_ITEM_DATA DROP COLUMN PKID
  
if EXISTS (select column_name from INFORMATION_SCHEMA.columns where table_name = 'TG_TENDER_DATA' and column_name = 'PKID') 
  ALTER TABLE TG_TENDER_DATA DROP COLUMN PKID
    
if EXISTS (select column_name from INFORMATION_SCHEMA.columns where table_name = 'TG_TRAN_DATA' and column_name = 'PKID') 
  ALTER TABLE TG_TRAN_DATA DROP COLUMN PKID 
  
if EXISTS (select column_name from INFORMATION_SCHEMA.columns where table_name = 'TG_DEPFILE_DATA' and column_name = 'PKID') 
  ALTER TABLE TG_DEPFILE_DATA DROP COLUMN PKID 

if EXISTS (select column_name from INFORMATION_SCHEMA.columns where table_name = 'TG_TTA_MAP_DATA' and column_name = 'PKID') 
  ALTER TABLE TG_TTA_MAP_DATA DROP COLUMN PKID 
  
if EXISTS (select column_name from INFORMATION_SCHEMA.columns where table_name = 'TG_USERSESSION_DATA' and column_name = 'PKID') 
  ALTER TABLE TG_USERSESSION_DATA DROP COLUMN PKID 
  
if EXISTS (select column_name from INFORMATION_SCHEMA.columns where table_name = 'TG_ACTLOG_DATA' and column_name = 'PKID') 
  ALTER TABLE TG_ACTLOG_DATA DROP COLUMN PKID
  
if EXISTS (select column_name from INFORMATION_SCHEMA.columns where table_name = 'TG_ACTLOG_DATA' and column_name = 'SERIAL_ID') 
  ALTER TABLE TG_ACTLOG_DATA DROP COLUMN SERIAL_ID
  
ALTER TABLE GR_CUST_FIELD_DATA ADD PKID INTEGER NOT NULL IDENTITY(1,1) CONSTRAINT GR_CUST_FIELD_DATA_PK PRIMARY KEY
ALTER TABLE CUST_FIELD_DATA ADD PKID INTEGER NOT NULL IDENTITY(1,1) CONSTRAINT CUST_FIELD_DATA_PK PRIMARY KEY
ALTER TABLE TG_ITEM_DATA ADD PKID INTEGER NOT NULL IDENTITY(1,1) CONSTRAINT TG_ITEM_DATA_PK PRIMARY KEY
ALTER TABLE TG_TENDER_DATA ADD PKID INTEGER NOT NULL IDENTITY(1,1) CONSTRAINT TG_TENDER_DATA_PK PRIMARY KEY
ALTER TABLE TG_TRAN_DATA ADD PKID INTEGER NOT NULL IDENTITY(1,1) CONSTRAINT TG_TRAN_DATA_PK PRIMARY KEY
ALTER TABLE TG_DEPFILE_DATA ADD PKID INTEGER NOT NULL IDENTITY(1,1) CONSTRAINT TG_DEPFILE_DATA_PK PRIMARY KEY
ALTER TABLE TG_TTA_MAP_DATA ADD PKID INTEGER NOT NULL IDENTITY(1,1) CONSTRAINT TG_TTA_MAP_DATA_PK PRIMARY KEY
ALTER TABLE TG_ACTLOG_DATA ADD PKID INTEGER NOT NULL IDENTITY(1,1) CONSTRAINT TG_ACTLOG_DATA_PK PRIMARY KEY
GO

-- FT. Bug 16303
-- This is a non-optimal clustered index
-- ALTER TABLE TG_PAYEVENT_DATA ADD PRIMARY KEY (DEPFILESEQ, EVENTNBR, DEPFILENBR);
-- Need to make the clustered index on an auto-incremented field
-- UNIQUEID is already there as an auto-incremented so I cannot use PKID, so use the UNIQUEID
--CREATE CLUSTERED INDEX [TG_PAYEVENT_DATA_PK] ON [dbo].[TG_PAYEVENT_DATA] 
--(
--	[UNIQUEID] ASC
--)
--ON [PRIMARY]
--GO

-- Bug 18229: Make sure that the UNIQUEID is the PRIMARY KEY
ALTER TABLE TG_PAYEVENT_DATA
ADD PRIMARY KEY (UNIQUEID)


-- Modify the tables to add page compression ====================================================================
-- UMN Apply Table Compression only if SQL Server version supports it
DECLARE @sql nvarchar(255)
DECLARE @compression VARCHAR(128)
set @compression = 'NONE'
IF (EXISTS (SELECT * FROM sys.dm_db_persisted_sku_features where feature_name = 'Compression'))
  set @compression = 'PAGE'
Exec ('ALTER TABLE GR_CUST_FIELD_DATA REBUILD PARTITION = ALL WITH (Data_Compression = ' + @compression + ')') 
Exec ('ALTER TABLE CUST_FIELD_DATA REBUILD PARTITION = ALL WITH (Data_Compression = ' + @compression + ')')
Exec ('ALTER TABLE TG_ITEM_DATA REBUILD PARTITION = ALL WITH (Data_Compression = ' + @compression + ')')
Exec ('ALTER TABLE TG_PAYEVENT_DATA REBUILD PARTITION = ALL WITH (Data_Compression = ' + @compression + ')')
Exec ('ALTER TABLE TG_TENDER_DATA REBUILD PARTITION = ALL WITH (Data_Compression = ' + @compression + ')')
Exec ('ALTER TABLE TG_TRAN_DATA REBUILD PARTITION = ALL WITH (Data_Compression = ' + @compression + ')')
Exec ('ALTER TABLE TG_IMAGE_DATA REBUILD PARTITION = ALL WITH (Data_Compression = ' + @compression + ')')
Exec ('ALTER TABLE TG_DEPFILE_DATA REBUILD PARTITION = ALL WITH (Data_Compression = ' + @compression + ')')
Exec ('ALTER TABLE TG_TTA_MAP_DATA REBUILD PARTITION = ALL WITH (Data_Compression = ' + @compression + ')')
Exec ('ALTER TABLE TG_ACTLOG_DATA REBUILD PARTITION = ALL WITH (Data_Compression = ' + @compression + ')')
GO

-- Create the indexes ===========================================================================================

/*
-- Create an index on the POSTDT column.  Do not bother with included columns to cut down the size.  
-- This will be the default sorting column when ActivityLog is first brought up
-- and will speed up initial pagination.  Bug 13613.
-- TODO: Make this the clustered index instead of PKID
*/
-- TG_ACTLOG_DATA
CREATE NONCLUSTERED INDEX [IX_TG_ACTLOG_DATA_POSTDT] ON [dbo].[TG_ACTLOG_DATA] 
(
	[POSTDT] DESC
) ON [PRIMARY]
GO     
-- END TG_ACTLOG_DATA

-- GR_CUST_FIELD_DATA
CREATE NONCLUSTERED INDEX [IX_GR_CUST_FIELD_DATA_DEPFILENBR_DEPFILESEQ_EVENTNBR_TRANNBR_SCREENINDEX] ON [dbo].[GR_CUST_FIELD_DATA] 
(
	[DEPFILENBR] ASC, -- UMN updated order according to UPHS Bug 14060
	[DEPFILESEQ] ASC,
	[EVENTNBR] ASC,
	[TRANNBR] ASC,
	[SCREENINDEX] ASC
)
INCLUDE ( [TTID],
[CUSTID],
[CUSTLABEL],
[CUSTVALUE],
[CUSTTAG],
[CUSTATTR],
[CHECKSUM],
[PKID]) ON [PRIMARY]
GO

-- UMN optimize reporting criteria SELECT DISTINCT CUSTTAG, CUSTLABEL FROM GR_CUST_FIELD_DATA WHERE NOT(CUSTTAG IS NULL) ORDER BY CUSTLABEL
CREATE NONCLUSTERED INDEX [IX_GR_CUST_FIELD_DATA_CUSTLABEL_CUSTTAG] ON [dbo].[GR_CUST_FIELD_DATA]
(
	[CUSTLABEL] ASC,
	[CUSTTAG] ASC
) ON [PRIMARY]
GO
-- END GR_CUST_FIELD_DATA

-- CUST_FIELD_DATA - UMN added for search queries, see Bug 15516
-- Bug 16771. FT.  Changed Uttam's IX_CUST_FIELD_DATA_DEPFILENBR_DEPFILESEQ_EVENTNBR to include all the 
-- fields in the table (needed for the checksum query).  This seems to prevent deadlock as seen in Geisinger
-- Bug 18615. FT. Fixed ordering of DEPFILESEQ and DEPFILEBNBR so that DEPFILENBR is first
CREATE NONCLUSTERED INDEX [IX_CUST_FIELD_DATA_DEPFILENBR_DEPFILESEQ_EVENTNBR] ON [dbo].[CUST_FIELD_DATA]
(
	[DEPFILENBR] ASC,
	[DEPFILESEQ] ASC,
	[EVENTNBR] ASC
)
INCLUDE ( 	[POSTNBR],
	[POST_TYPE],
	[SCREENINDEX],
	[POSTID],
	[CUSTID],
	[CUSTLABEL],
	[CUSTVALUE],
	[CUSTTAG],
	[CUSTATTR],
	[CHECKSUM],
	[PKID]) ON [PRIMARY]
GO


/* Bug 16771. FT.  This index seems to have caused Geisinger timeouts during INSERTS 
   Note: this index WILL help if you are using the old Reporting, so add it in for legacy, pre-Argos sites.
-- UMN optimize reporting criteria SELECT DISTINCT CUSTTAG, CUSTLABEL FROM CUST_FIELD_DATA WHERE NOT(CUSTTAG IS NULL) ORDER BY CUSTLABEL
CREATE NONCLUSTERED INDEX [IX_CUST_FIELD_DATA_CUSTLABEL_CUSTTAG] ON [dbo].[CUST_FIELD_DATA]
(
	[CUSTLABEL] ASC,
	[CUSTTAG] ASC
) ON [PRIMARY]
GO
-- END CUST_FIELD_DATA
*/

-- TG_ITEM_DATA
CREATE NONCLUSTERED INDEX [IX_TG_ITEM_DATA_DEPFILENBR_DEPFILESEQ_EVENTNBR_TRANNBR_ITEMNBR] ON [dbo].[TG_ITEM_DATA] 
(
	[DEPFILENBR] ASC,
	[DEPFILESEQ] ASC,
	[EVENTNBR] ASC,
	[TRANNBR] ASC,
	[ITEMNBR] ASC
)
INCLUDE ( [ITEMID],
[GLACCTNBR],
[ACCTID],
[ITEMACCTID],
[ITEMDESC],
[AMOUNT],
[QTY],
[TAXED],
[TOTAL],
[CHECKSUM],
[ITEMCONFIGID],
[PKID]) ON [PRIMARY]
GO
-- END TG_ITEM_DATA

-- TG_TENDER_DATA
CREATE NONCLUSTERED INDEX [IX_TG_TENDER_DATA_DEPFILENBR_DEPFILESEQ_EVENTNBR_TNDRNBR] ON [dbo].[TG_TENDER_DATA] 
(
	[DEPFILENBR] ASC, -- UMN updated order according to UPHS Bug 14060
	[DEPFILESEQ] ASC,
	[EVENTNBR] ASC,
	[TNDRNBR] ASC
)
INCLUDE ( [TNDRID],
[TNDRDESC],
[TYPEIND],
[AMOUNT],
[CC_CK_NBR],
[EXPMONTH],
[EXPYEAR],
[CCNAME],
[AUTHNBR],
[AUTHSTRING],
[AUTHACTION],
[BANKROUTINGNBR],
[BANKACCTNBR],
[ADDRESS],
[SYSTEXT],
[POSTDT],
[VOIDDT],
[VOIDUSERID],
[SOURCE_TYPE],
[SOURCE_GROUP],
[SOURCE_REFID],
[SOURCE_DATE],
[CHECKSUM],
[BANKACCTID],
[PKID]) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_TG_TENDER_DATA_DEPFILENBR_DEPFILESEQ_VOIDDT_EVENTNBR_BANKACCTID_TNDRID_TYPEIND] ON [dbo].[TG_TENDER_DATA] 
(
	[DEPFILENBR] ASC, -- UMN updated order according to UPHS Bug 14060
	[DEPFILESEQ] ASC,
	[VOIDDT] ASC,
	[EVENTNBR] ASC,
	[BANKACCTID] ASC,
	[TNDRID] ASC,
	[TYPEIND] ASC
)
INCLUDE ( [AMOUNT],
[PKID]) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_TG_TENDER_DATA_TNDRID_TNDRDESC] ON [TG_TENDER_DATA] 
(
	[TNDRID] ASC,
	[TNDRDESC] ASC
) ON [PRIMARY]
GO
-- END TG_TENDER_DATA

-- TG_TRAN_DATA
CREATE NONCLUSTERED INDEX [IX_TG_TRAN_DATA_TTID_TTDESC_ITEMIND] ON [TG_TRAN_DATA] 
(
	[TTID] ASC,
	[TTDESC] ASC,
	[ITEMIND] ASC
) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_TG_TRAN_DATA_DEPFILENBR_DEPFILESEQ_EVENTNBR_TRANNBR] ON [dbo].[TG_TRAN_DATA] 
(
	[DEPFILENBR] ASC, -- UMN updated order according to UPHS Bug 14060
	[DEPFILESEQ] ASC,
	[EVENTNBR] ASC,
	[TRANNBR] ASC
)
INCLUDE ( [CONTENTTYPE],
[TTID],
[TTDESC],
[TAXEXIND],
[DISTAMT],
[TRANAMT],
[ITEMIND],
[COMMENTS],
[SYSTEXT],
[POSTDT],
[VOIDDT],
[VOIDUSERID],
[SOURCE_TYPE],
[SOURCE_GROUP],
[SOURCE_REFID],
[SOURCE_DATE],
[CHECKSUM],
[PKID]) ON [PRIMARY]
GO

-- UMN - Create index for pending payments
CREATE NONCLUSTERED INDEX [IX_TG_TRAN_DATA_SYSTEXT_VOIDDT_DEPFILENBR_DEPFILESEQ] ON [dbo].[TG_TRAN_DATA]
(
  [SYSTEXT] ASC,
  [VOIDDT] ASC,
  [DEPFILENBR] ASC,
  [DEPFILESEQ] ASC
)
INCLUDE ([TRANAMT]) ON [PRIMARY];
GO

-- UMN - Create indexes for reporting and file criteria
CREATE NONCLUSTERED INDEX [IX_TG_TRAN_DATA_DEPFILENBR_DEPFILESEQ_ITEMIND_TTID_VOIDDT_TTDESC] ON [dbo].[TG_TRAN_DATA]
(
	[DEPFILENBR] ASC,
	[DEPFILESEQ] ASC,
	[ITEMIND] ASC,
	[TTID] ASC,
	[VOIDDT] ASC,
	[TTDESC] ASC
)
INCLUDE ([TRANAMT]) ON [PRIMARY];
GO
-- END TG_TRAN_DATA

-- TG_TTA_MAP_DATA
CREATE NONCLUSTERED INDEX [IX_TG_TTA_MAP_DATA_DEPFILENBR_DEPFILESEQ_EVENTNBR] ON [dbo].[TG_TTA_MAP_DATA] 
(
	[DEPFILENBR] ASC, -- UMN updated order according to UPHS Bug 14060
	[DEPFILESEQ] ASC,
	[EVENTNBR] ASC
)
INCLUDE ( [TTAMAPNBR],
[CREDITNBR],
[CREDITITEMNBR],
[DEBITNBR],
[DEBITITEMNBR],
[GLACCTNBR],
[AMOUNT]) ON [PRIMARY]
GO
-- TG_TTA_MAP_DATA

-- TG_PAYEVENT_DATA
-- Moved the USERID should to be *after* the EVENTNBR. 
CREATE NONCLUSTERED INDEX [IX_TG_PAYEVENT_DATA_DEPFILENBR_DEPFILESEQ_EVENTNBR_USERID_STATUS] ON [dbo].[TG_PAYEVENT_DATA] 
(
	[DEPFILENBR] ASC, -- UMN updated order according to UPHS Bug 14060
	[DEPFILESEQ] ASC,
	[EVENTNBR] ASC,
	[USERID] ASC,
	[STATUS] ASC
)
INCLUDE ([CREATION_DT])
ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_TG_PAYEVENT_DATA_MOD_DT] ON [dbo].[TG_PAYEVENT_DATA] 
(
	[MOD_DT] ASC
) ON [PRIMARY]
GO

-- END TG_PAYEVENT_DATA

-- TG_DEPFILE_DATA
-- Added by DH for bug# 13503 11-02-2012
CREATE NONCLUSTERED INDEX [IX_TG_DEPFILE_DATA_DEPFILENBR_DEPFILESEQ_BALDT] ON [dbo].[TG_DEPFILE_DATA] 
(
	[DEPFILENBR] ASC,
	[DEPFILESEQ] ASC,
	[BALDT] ASC
) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_TG_DEPFILE_DATA_DEPFILENBR_DEPFILESEQ_CLOSEDT] ON [dbo].[TG_DEPFILE_DATA] 
(
	[DEPFILENBR] ASC, -- UMN updated order according to UPHS Bug 14060
	[DEPFILESEQ] ASC,
	[CLOSEDT] ASC
) ON [PRIMARY]
GO

-- UMN added for pending payments
CREATE NONCLUSTERED INDEX [IX_TG_DEPFILE_DATA_DEPFILENBR_DEPFILESEQ_UPDATEDT] ON [dbo].[TG_DEPFILE_DATA]
(
  [DEPFILENBR] ASC,
  [DEPFILESEQ] ASC,
  [UPDATEDT] ASC
) ON [PRIMARY]
GO

-- UMN optimize SELECT DISTINCT OPEN_USERID FROM TG_DEPFILE_DATA during transfer LOGIN
CREATE NONCLUSTERED INDEX [IX_TG_DEPFILE_DATA_OPEN_USERID] ON [dbo].[TG_DEPFILE_DATA]
(
	[OPEN_USERID] ASC
) ON [PRIMARY]
GO

-- FT: Bug 17796: NTirety recommendation
-- Designed to optimize this query:
-- SELECT * from TG_DEPFILE_DATA where FILENAME='2015266215'
-- Not used if table is too small
CREATE NONCLUSTERED INDEX [IX_TG_DEPFILE_DATA_FILENAME] ON [dbo].[TG_DEPFILE_DATA] 
(
	[FILENAME]
) ON [PRIMARY]
GO


-- FT: Bug 17796: NTirety recommendation
-- Designed to optimize this query:
-- SELECT DEPFILENBR, DEPFILESEQ FROM TG_DEPFILE_DATA 
-- WHERE SOURCE_TYPE='Cashier' AND DEPTID= '165' AND OPENDT >= '09/23/2015 12:00:00AM' AND BALDT IS NOT NULL
CREATE NONCLUSTERED INDEX [IX_TG_DEPFILE_DATA_DEPID_SOURCE_TYPE_OPENDT_BALDT_CLOSEDT]
ON [dbo].[TG_DEPFILE_DATA] 
(
	[DEPTID],
	[SOURCE_TYPE],
	[OPENDT],
	[BALDT],
	[CLOSEDT]
)
INCLUDE ([DEPFILENBR],[DEPFILESEQ])
ON [PRIMARY]
GO

-- FT: Bug 17796: NTirety recommendation
-- Designed to optimize this query:
-- SELECT * FROM TG_DEPFILE_DATA f 
-- WHERE f.EFFECTIVEDT >='09/16/2015' AND f.EFFECTIVEDT <'09/24/2015'  
-- AND  f.OPENDT IS NOT NULL AND f.BALDT IS NULL AND f.UPDATEDT IS NULL AND f.FILETYPE IN ('I','S') 
-- AND (f.OPEN_USERID='aelliott' OR f.FILETYPE='S') AND (f.DEPTID='081') ORDER BY f.OPENDT DESC
-- This will only be used for large tables
CREATE NONCLUSTERED INDEX [IX_TG_DEPFILE_DATA_DEPTID_BALDT_UPDATEDT_FILETYPE_EFFECTIVEDT_OPENDT] 
ON [dbo].[TG_DEPFILE_DATA] 
(
	[DEPTID],
	[BALDT],
	[UPDATEDT],
	[FILETYPE],
	[EFFECTIVEDT],
	[OPENDT]
)
ON [PRIMARY]
GO


-- END TG_DEPFILE_DATA

-- end Added by DH for bug# 13503 11-02-2012

-- TG_SYSTEM_INTERFACE_TRACKING - UMN added
CREATE NONCLUSTERED INDEX [IX_TG_SYSTEM_INTERFACE_TRACKING_DEPFILENBR_DEPFILESEQ_EVENTNBR] ON [dbo].[TG_SYSTEM_INTERFACE_TRACKING]
(
  [DEPFILENBR] ASC,
  [DEPFILESEQ] ASC,
  [EVENTNBR] ASC
) ON [PRIMARY]
GO
-- END TG_SYSTEM_INTERFACE_TRACKING

-- TG_SUSPEVENT_DATA
-- UMN optimize SELECT DISTINCT USERID FROM TG_SUSPEVENT_DATA during transfer LOGIN
CREATE NONCLUSTERED INDEX [IX_TG_SUSPEVENT_DATA_USERID] ON [dbo].[TG_SUSPEVENT_DATA]
(
	[USERID] ASC
) ON [PRIMARY]
GO
-- END TG_SUSPEVENT_DATA

-- TG_FILESOURCE_DATA
-- UMN optimize SELECT DISTINCT SOURCE_TYPE FROM TG_FILESOURCE_DATA during transfer LOGIN
CREATE NONCLUSTERED INDEX [IX_TG_FILESOURCE_DATA_SOURCE_TYPE] ON [dbo].[TG_FILESOURCE_DATA]
(
	[SOURCE_TYPE] ASC
) ON [PRIMARY]
GO
-- END TG_FILESOURCE_DATA

-- TG_BANK_DEPOSIT_DATA
-- Index for TG_BANK_DEPOSIT_DATA
-- To Speed up list of unreconciled bank deposits in the Bank Rec Open Deposit screen.
-- Bug 11099.
CREATE NONCLUSTERED INDEX [IX_TG_BANK_DEPOSIT_DATA_STATUS_DEPOSIT_DATE] ON [dbo].[TG_BANK_DEPOSIT_DATA] 
(
	[STATUS] ASC,
	[DEPOSIT_DATE] DESC
) ON [PRIMARY]
GO

-- Index to speed up duplicate check.  This is needed since every record
-- in the input runs a query looking for duplicates
-- Bug 11078: FT: 6/16/2010
CREATE NONCLUSTERED INDEX [IX_TG_BANK_DEPOSIT_DATA_DEPOSIT_DATE_ACCOUNT_ID_REF_NUM_BANK_REFERENCE] ON [dbo].[TG_BANK_DEPOSIT_DATA] 
(
	[DEPOSIT_DATE] ASC,
	[ACCOUNT_ID] ASC,
	[REF_NUM] ASC,
	[BANK_REFERENCE] ASC
)
INCLUDE ( [BANK_DEPOSIT_ID],
[STATUS]) ON [PRIMARY]
GO
-- end TG_BANK_DEPOSIT_DATA

-- TG_DEPOSIT_DATA 
-- index needed for Bank Rec (recommended by the Database Engine Tuning Advisor) 
-- Used to speed up the Expected Deposit List in the Open Deposits Bank Rec window
-- Bug 11099: FT
CREATE NONCLUSTERED INDEX [IX_TG_DEPOSIT_DATA_POSTDT_DEPOSITTYPE_VOIDDT] ON [dbo].[TG_DEPOSIT_DATA] 
(
	[POSTDT] ASC,
	[DEPOSITTYPE] ASC,
	[VOIDDT] ASC
) ON [PRIMARY]
GO

-- FT. Bug 17796. Added NTirety's recommended index for reporting queries
-- For this reporting query:
-- SELECT SUM(AMOUNT) as BANK_TOTAL             
-- FROM TG_DEPOSIT_DATA
-- WHERE(DEPFILENBR=2015265 AND DEPFILESEQ=219) AND NOT( DEPOSITTYPE='PRE_DEPOSIT') AND VOIDDT IS NULL
-- Note: I am not including all the included columns that NTirety recommended; all that is needed is the AMOUNT.
IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_TG_DEPOSIT_DATA_DEPFILENBR_DEPFILESEQ_VOIDDT_DEPOSITTYPE')
	DROP INDEX TG_DEPOSIT_DATA.IX_TG_DEPOSIT_DATA_DEPFILENBR_DEPFILESEQ_VOIDDT_DEPOSITTYPE
CREATE NONCLUSTERED INDEX [IX_TG_DEPOSIT_DATA_DEPFILENBR_DEPFILESEQ_VOIDDT_DEPOSITTYPE]  
ON [dbo].[TG_DEPOSIT_DATA] 
(
	[DEPFILENBR],
	[DEPFILESEQ],
	[VOIDDT],
	[DEPOSITTYPE]
)
INCLUDE ([AMOUNT])
ON [PRIMARY]
GO

-- END TG_DEPOSIT_DATA 

-- TG_RECONCILED_BANK_DATA 
-- Bug 11099
CREATE NONCLUSTERED INDEX [IX_TG_RECONCILED_BANK_DATA_RECON_ID] ON [dbo].[TG_RECONCILED_BANK_DATA] 
(
	[RECON_ID]
) ON [PRIMARY]
GO
-- END TG_RECONCILED_BANK_DATA 

-- TG_RECONCILED_BANK_DATA 
-- Bug 11099
CREATE NONCLUSTERED INDEX [IX_TG_RECONCILED_EXPECTED_DATA_RECON_ID] ON [dbo].[TG_RECONCILED_EXPECTED_DATA] 
(
	[RECON_ID]
) ON [PRIMARY]
GO
-- TG_RECONCILED_DATA 

-- END TG_RECONCILED_BANK_DATA 
-- Bug 11099: To speed up sort / top() query for the Reconciled Deposit screen in Bank Rec.
-- The reconciled deposit list is sorted on the RECON_DATE field (DESC) and the TOP function
-- skims off only the first 512 records or so (depending on the db limit config setting).  
-- This gives an order of magnitude speedup.
CREATE NONCLUSTERED INDEX [IX_TG_RECONCILED_DATA_RECON_DATE] ON [dbo].[TG_RECONCILED_DATA] 
(
	[RECON_DATE] DESC
) ON [PRIMARY]
GO
-- END TG_RECONCILED_DATA 

PRINT 'Created Indexes.'
GO
-- END Create the indexes ===========================================================================================

-- Rebuild the indexes adding compression and fillfactor  ===========================================================
-- UMN Apply Table Compression only if SQL Server version supports it
DECLARE @compression VARCHAR(128)
set @compression = 'NONE'
IF (EXISTS (SELECT * FROM sys.dm_db_persisted_sku_features where feature_name = 'Compression'))
  set @compression = 'PAGE'
Exec ('ALTER INDEX ALL ON TG_BANK_DEPOSIT_DATA REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON TG_DEPOSIT_DATA REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON TG_DEPOSITTNDR_DATA REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON TG_FILESOURCE_DATA REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON TG_GL_VALID_DATA REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON TG_IMAGE_DATA REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON TG_ITEM_DATA REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON CUST_FIELD_DATA REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON GR_CUST_FIELD_DATA REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON TG_ACTLOG_DATA REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON TG_DEPFILE_DATA REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON TG_TRAN_DATA REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON TG_TENDER_DATA REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON TG_SUSPEVENT_DATA REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
-- Index is not a single identity column, need to have some space left over
Exec ('ALTER INDEX ALL ON TG_PAYEVENT_DATA REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')')
Exec ('ALTER INDEX ALL ON TG_TTA_MAP_DATA REBUILD WITH (FILLFACTOR = 90, Data_Compression = ' + @compression + ')')
Exec ('ALTER INDEX ALL ON TG_RECONCILED_BANK_DATA REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON TG_RECONCILED_DATA REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON TG_RECONCILED_EXPECTED_DATA REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON TG_REVERSAL_EVENT_DATA REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON TG_REVERSAL_TRAN_DATA REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON TG_SUSPTRAN_DATA REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON TG_SYSTEM_INTERFACE_TRACKING REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON TG_UPDATE_DATA REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON TG_UPDATEFILE_DATA REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
PRINT 'Altered indexes to set page-level data compression.'
GO
-- END Rebuild the indexes adding compression and fillfactor  ===========================================================

-- CREATE NEW STATISTICS ==========================================================================
IF EXISTS (SELECT name FROM sys.stats
    WHERE name = N'_dta_stat_597577167_2_3'
    AND object_id = OBJECT_ID(N'GR_CUST_FIELD_DATA'))
DROP STATISTICS GR_CUST_FIELD_DATA._dta_stat_597577167_2_3
CREATE STATISTICS [_dta_stat_597577167_2_3] ON [dbo].[GR_CUST_FIELD_DATA]([EVENTNBR], [DEPFILENBR])
GO

IF EXISTS (SELECT name FROM sys.stats
    WHERE name = N'_dta_stat_597577167_1_5_2_3'
    AND object_id = OBJECT_ID(N'GR_CUST_FIELD_DATA'))
DROP STATISTICS GR_CUST_FIELD_DATA._dta_stat_597577167_1_5_2_3
CREATE STATISTICS [_dta_stat_597577167_1_5_2_3] ON [GR_CUST_FIELD_DATA]([TRANNBR], [SCREENINDEX], [EVENTNBR], [DEPFILENBR])
GO

IF EXISTS (SELECT name FROM sys.stats
    WHERE name = N'_dta_stat_821577965_3_4'
    AND object_id = OBJECT_ID(N'[TG_ITEM_DATA]'))
DROP STATISTICS [TG_ITEM_DATA]._dta_stat_821577965_3_4
CREATE STATISTICS [_dta_stat_821577965_3_4] ON [TG_ITEM_DATA]([EVENTNBR], [DEPFILENBR])
GO

IF EXISTS (SELECT name FROM sys.stats
    WHERE name = N'_dta_stat_821577965_5_4_3'
    AND object_id = OBJECT_ID(N'[TG_ITEM_DATA]'))
DROP STATISTICS [TG_ITEM_DATA]._dta_stat_821577965_5_4_3
CREATE STATISTICS [_dta_stat_821577965_5_4_3] ON [TG_ITEM_DATA]([DEPFILESEQ], [DEPFILENBR], [EVENTNBR])
GO

IF EXISTS (SELECT name FROM sys.stats
    WHERE name = N'_dta_stat_821577965_2_3_4_5'
    AND object_id = OBJECT_ID(N'[TG_ITEM_DATA]'))
DROP STATISTICS [TG_ITEM_DATA]._dta_stat_821577965_2_3_4_5
CREATE STATISTICS [_dta_stat_821577965_2_3_4_5] ON [TG_ITEM_DATA]([TRANNBR], [EVENTNBR], [DEPFILENBR], [DEPFILESEQ])
GO

IF EXISTS (SELECT name FROM sys.stats
    WHERE name = N'_dta_stat_853578079_2_3'
    AND object_id = OBJECT_ID(N'[TG_PAYEVENT_DATA]'))
DROP STATISTICS [TG_PAYEVENT_DATA]._dta_stat_853578079_2_3
CREATE STATISTICS [_dta_stat_853578079_2_3] ON [TG_PAYEVENT_DATA]([DEPFILENBR], [DEPFILESEQ])
GO

IF EXISTS (SELECT name FROM sys.stats
    WHERE name = N'_dta_stat_853578079_5_6'
    AND object_id = OBJECT_ID(N'[TG_PAYEVENT_DATA]'))
DROP STATISTICS [TG_PAYEVENT_DATA]._dta_stat_853578079_5_6
CREATE STATISTICS [_dta_stat_853578079_5_6] ON [TG_PAYEVENT_DATA]([STATUS], [MOD_DT])
GO

IF EXISTS (SELECT name FROM sys.stats
    WHERE name = N'_dta_stat_853578079_4_2_3'
    AND object_id = OBJECT_ID(N'[TG_PAYEVENT_DATA]'))
DROP STATISTICS [TG_PAYEVENT_DATA]._dta_stat_853578079_4_2_3
CREATE STATISTICS [_dta_stat_853578079_4_2_3] ON [TG_PAYEVENT_DATA]([USERID], [DEPFILENBR], [DEPFILESEQ])
GO

IF EXISTS (SELECT name FROM sys.stats
    WHERE name = N'_dta_stat_853578079_1_5_4_2'
    AND object_id = OBJECT_ID(N'[TG_PAYEVENT_DATA]'))
DROP STATISTICS [TG_PAYEVENT_DATA]._dta_stat_853578079_1_5_4_2
CREATE STATISTICS [_dta_stat_853578079_1_5_4_2] ON [TG_PAYEVENT_DATA]([EVENTNBR], [STATUS], [USERID], [DEPFILENBR])
GO

IF EXISTS (SELECT name FROM sys.stats
    WHERE name = N'_dta_stat_853578079_5_4_2_3_1'
    AND object_id = OBJECT_ID(N'[TG_PAYEVENT_DATA]'))
DROP STATISTICS [TG_PAYEVENT_DATA]._dta_stat_853578079_5_4_2_3_1
CREATE STATISTICS [_dta_stat_853578079_5_4_2_3_1] ON [TG_PAYEVENT_DATA]([STATUS], [USERID], [DEPFILENBR], [DEPFILESEQ], [EVENTNBR])
GO

IF EXISTS (SELECT name FROM sys.stats
    WHERE name = N'_dta_stat_981578535_4_3_2'
    AND object_id = OBJECT_ID(N'[TG_TRAN_DATA]'))
DROP STATISTICS [TG_TRAN_DATA]._dta_stat_981578535_4_3_2
CREATE STATISTICS [_dta_stat_981578535_4_3_2] ON [TG_TRAN_DATA]([DEPFILESEQ], [DEPFILENBR], [EVENTNBR])
GO

IF EXISTS (SELECT name FROM sys.stats
    WHERE name = N'_dta_stat_949578421_2_3'
    AND object_id = OBJECT_ID(N'[TG_TENDER_DATA]'))
DROP STATISTICS [TG_TENDER_DATA]._dta_stat_949578421_2_3
CREATE STATISTICS [_dta_stat_949578421_2_3] ON [TG_TENDER_DATA]([EVENTNBR], [DEPFILENBR])
GO

IF EXISTS (SELECT name FROM sys.stats
    WHERE name = N'_dta_stat_949578421_21_2_4'
    AND object_id = OBJECT_ID(N'[TG_TENDER_DATA]'))
DROP STATISTICS [TG_TENDER_DATA]._dta_stat_949578421_21_2_4
CREATE STATISTICS [_dta_stat_949578421_21_2_4] ON [TG_TENDER_DATA]([VOIDDT], [EVENTNBR], [DEPFILESEQ])
GO

IF EXISTS (SELECT name FROM sys.stats
    WHERE name = N'_dta_stat_949578421_28_5_7_3_4'
    AND object_id = OBJECT_ID(N'[TG_TENDER_DATA]'))
DROP STATISTICS [TG_TENDER_DATA]._dta_stat_949578421_28_5_7_3_4
CREATE STATISTICS [_dta_stat_949578421_28_5_7_3_4] ON [TG_TENDER_DATA]([BANKACCTID], [TNDRID], [TYPEIND], [DEPFILENBR], [DEPFILESEQ])
GO

IF EXISTS (SELECT name FROM sys.stats
    WHERE name = N'_dta_stat_949578421_6_5'
    AND object_id = OBJECT_ID(N'[TG_TENDER_DATA]'))
DROP STATISTICS [TG_TENDER_DATA]._dta_stat_949578421_6_5
CREATE STATISTICS [_dta_stat_949578421_6_5] ON [TG_TENDER_DATA]([TNDRDESC], [TNDRID])
GO

IF EXISTS (SELECT name FROM sys.stats
    WHERE name = N'_dta_stat_981578535_11_6_7'
    AND object_id = OBJECT_ID(N'[TG_TRAN_DATA]'))
DROP STATISTICS [TG_TRAN_DATA]._dta_stat_981578535_11_6_7
CREATE STATISTICS [_dta_stat_981578535_11_6_7] ON [TG_TRAN_DATA]([ITEMIND], [TTID], [TTDESC])
GO

IF EXISTS (SELECT name FROM sys.stats
    WHERE name = N'_dta_stat_1205579333_3_2'
    AND object_id = OBJECT_ID(N'[TG_TTA_MAP_DATA]'))
DROP STATISTICS [TG_TTA_MAP_DATA]._dta_stat_1205579333_3_2
CREATE STATISTICS [_dta_stat_1205579333_3_2] ON [TG_TTA_MAP_DATA]([DEPFILENBR], [EVENTNBR])
GO

IF EXISTS (SELECT name FROM sys.stats
    WHERE name = N'_dta_stat_1205579333_3_4_2'
    AND object_id = OBJECT_ID(N'[TG_TTA_MAP_DATA]'))
DROP STATISTICS [TG_TTA_MAP_DATA]._dta_stat_1205579333_3_4_2
CREATE STATISTICS [_dta_stat_1205579333_3_4_2] ON [TG_TTA_MAP_DATA]([DEPFILENBR], [DEPFILESEQ], [EVENTNBR])
GO

-- Added by DH for bug# 13503 11-02-2012
IF EXISTS (SELECT name FROM sys.stats
    WHERE name = N'_dta_stat_629577281_7_1_2_16'
    AND object_id = OBJECT_ID(N'[TG_DEPFILE_DATA]'))
DROP STATISTICS [TG_DEPFILE_DATA]._dta_stat_629577281_7_1_2_16
CREATE STATISTICS [_dta_stat_629577281_7_1_2_16] ON [dbo].[TG_DEPFILE_DATA]([AUTOBALANCE], [DEPFILENBR], [DEPFILESEQ], [BALDT])
GO

-- Added by DH for bug# 13503 11-02-2012
IF EXISTS (SELECT name FROM sys.stats
    WHERE name = N'_dta_stat_629577281_7_1_2_19'
    AND object_id = OBJECT_ID(N'[TG_DEPFILE_DATA]'))
DROP STATISTICS [TG_DEPFILE_DATA]._dta_stat_629577281_7_1_2_19
CREATE STATISTICS [_dta_stat_629577281_7_1_2_19] ON [dbo].[TG_DEPFILE_DATA]([AUTOBALANCE], [DEPFILENBR], [DEPFILESEQ], [CLOSEDT])
GO
PRINT 'Created statistics.'
-- END CREATE NEW STATISTICS ==========================================================================

-- Full update of stats ======================================================================
UPDATE STATISTICS TG_BANK_DEPOSIT_DATA WITH FULLSCAN
GO
UPDATE STATISTICS TG_DEPOSIT_DATA WITH FULLSCAN
GO
UPDATE STATISTICS TG_DEPOSITTNDR_DATA WITH FULLSCAN
GO
UPDATE STATISTICS TG_FILESOURCE_DATA WITH FULLSCAN
GO
UPDATE STATISTICS TG_GL_VALID_DATA WITH FULLSCAN
GO
UPDATE STATISTICS TG_IMAGE_DATA WITH FULLSCAN
GO
UPDATE STATISTICS TG_ITEM_DATA WITH FULLSCAN
GO
UPDATE STATISTICS CUST_FIELD_DATA WITH FULLSCAN
GO
UPDATE STATISTICS GR_CUST_FIELD_DATA WITH FULLSCAN
GO
UPDATE STATISTICS TG_ACTLOG_DATA WITH FULLSCAN
GO
UPDATE STATISTICS TG_DEPFILE_DATA WITH FULLSCAN
GO
UPDATE STATISTICS TG_TRAN_DATA WITH FULLSCAN
GO
UPDATE STATISTICS TG_TENDER_DATA WITH FULLSCAN
GO
UPDATE STATISTICS TG_SUSPEVENT_DATA WITH FULLSCAN
GO
UPDATE STATISTICS TG_PAYEVENT_DATA WITH FULLSCAN
GO
UPDATE STATISTICS TG_TTA_MAP_DATA WITH FULLSCAN
GO
UPDATE STATISTICS TG_RECONCILED_BANK_DATA WITH FULLSCAN
GO
UPDATE STATISTICS TG_RECONCILED_DATA WITH FULLSCAN
GO
UPDATE STATISTICS TG_RECONCILED_EXPECTED_DATA WITH FULLSCAN
GO
UPDATE STATISTICS TG_REVERSAL_EVENT_DATA WITH FULLSCAN
GO
UPDATE STATISTICS TG_REVERSAL_TRAN_DATA WITH FULLSCAN
GO
UPDATE STATISTICS TG_SUSPTRAN_DATA WITH FULLSCAN
GO
UPDATE STATISTICS TG_SYSTEM_INTERFACE_TRACKING WITH FULLSCAN
GO
UPDATE STATISTICS TG_UPDATE_DATA WITH FULLSCAN
GO
UPDATE STATISTICS TG_UPDATEFILE_DATA WITH FULLSCAN
GO
PRINT 'Updated statistics.';
-- END Full update of stats =====================================================================================

PRINT 'Finished performance upgrade script.';
GO

-- Print out a list of the new clustered and non-clustered indexes, and stats
select OBJECT_NAME(i.id) as 'TABLE_NAME', i.name as 'CLUSTERED_INDEX'
from sysindexes i
where i.indid between 1 and 249
and INDEXPROPERTY(i.id, i.name, 'IsClustered') = 1
and INDEXPROPERTY(i.id, i.name, 'IsStatistics') = 0
and INDEXPROPERTY(i.id, i.name, 'IsAutoStatistics') = 0
and INDEXPROPERTY(i.id, i.name, 'IsHypothetical') = 0
and OBJECTPROPERTY(i.id,'IsMSShipped') = 0
and not exists (select *
from sysobjects o
where xtype = 'UQ'
and o.name = i.name ) 

select OBJECT_NAME(i.id) as 'TABLE_NAME', i.name as 'NON_CLUSTERED_INDEX'
from sysindexes i
where i.indid between 1 and 249
and INDEXPROPERTY(i.id, i.name, 'IsClustered') = 0
and INDEXPROPERTY(i.id, i.name, 'IsStatistics') = 0
and INDEXPROPERTY(i.id, i.name, 'IsAutoStatistics') = 0
and INDEXPROPERTY(i.id, i.name, 'IsHypothetical') = 0
and OBJECTPROPERTY(i.id,'IsMSShipped') = 0
and not exists (select *
from sysobjects o
where xtype = 'UQ'
and o.name = i.name ) 

select OBJECT_NAME(i.id) as 'TABLE_NAME', i.name as 'STATISTICS'
from sysindexes i
where i.indid between 1 and 249
and INDEXPROPERTY(i.id, i.name, 'IsStatistics') = 1
and INDEXPROPERTY(i.id, i.name, 'IsAutoStatistics') = 0
and INDEXPROPERTY(i.id, i.name, 'IsHypothetical') = 0
and OBJECTPROPERTY(i.id,'IsMSShipped') = 0
and not exists (select *
from sysobjects o
where xtype = 'UQ'
and o.name = i.name )
