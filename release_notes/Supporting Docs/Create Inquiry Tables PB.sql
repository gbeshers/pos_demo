-- This script creates the Project Baseline database tables in the new format used by the
-- always active inquiry import technology (Bug#12994). It ENFORCES being run against a separate database,
-- NOT the transactional database, so that inquirying can be done while importing is happening.
-- If it was in the same database, bulk imports will escalate to a table lock, thus blocking
-- out payment transactions. Moreover, we can optimize the inquiry database to have simple
-- logging and page compression turned on, so that read inquiries against it are fast.
-- Also, our FAEs like to have a separate config database, a transactional database, and
-- now a database for PB inquiry data. Having three separate databases allows us to manage
-- backups more easily.

-- How to use this script:
-- 1. First create an empty transactional database named "PROJECT_type".
--    Examples might include: Geisinger_prod, SW_beta, SF_TEST.
-- 2. Now, run the transactional database script 
--    (example: T4_DATABASE/Scripts/127_MSSQL_data_COMPLETE.sql) against your new database.
-- 3. Run the appropriate script to generate indexes for the transactional and config databases.
-- 4. Create an empty config database named "PROJECT_config". (I'm assuming that production,
--    test, and beta configs are shared. If that's not the case, then use "PROJECT_config_type")
--    Examples might include: Geisinger_config, SW_config_beta, SF_CONFIG.
-- 5. Run the config database script (example:
--    T4_DATABASE/Scripts/124_MSSQL_config_COMPLETE.sql) against your new database.
-- 6. Create an empty inquiry database named "PROJECT_inquiry_type".
--    Examples might include: Geisinger_inquiry_prod, SW_inquiry_beta, SF_INQUIRY_TEST.
-- 7. COPY this script (T4_DATABASE/Scripts/127_MSSQL_inquiry_COMPLETE.sql) to a new query window
--    in SQL Server Management Console.
-- 8. Edit the two variable names (see below) to point to your inquiry database and your config database.
-- 9. Execute the script in the new query window of SQL Server Management Console.

-- Dan wants this to be independent of SQL Server/Oracle, but the problem is finding 
-- a matching datatype in SQL Server for bigint and tinyint. SQL Server's Numeric 
-- doesn't cut it because of the greater storage size. So to use this on Oracle:
-- 1. Search and replace [bigint] with Number(19,0)
-- 2. Search and replace [tinyint] with Number(3,0)
-- 3. Note, that I didn't try to parameterize all the queries with these changes
--    because there isn't a way to check for Sql Server or Oracle from the database,
--    at least, no reliable way.

-- Check if being run against a transactional db. If so, then exit.
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TG_DEPFILE_DATA]') AND type in (N'U'))
BEGIN
  PRINT 'Cannot run this script against a transactional database. it must be a separate inquiry database.'
  SET noexec ON
END
  
-- Check if being run against a config db. If so, then exit.
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CBT_Flex]') AND type in (N'U'))
BEGIN
  PRINT 'Cannot run this script against a config database. it must be a separate inquiry database.'
  SET noexec ON
END

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SI_KEY1]') AND type in (N'U'))
	DROP TABLE [dbo].[SI_KEY1]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SI_KEY2]') AND type in (N'U'))
	DROP TABLE [dbo].[SI_KEY2]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SI_DATA1]') AND type in (N'U'))
	DROP TABLE [dbo].[SI_DATA1]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SI_DATA2]') AND type in (N'U'))
	DROP TABLE [dbo].[SI_DATA2]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SI_WEIGHTS1]') AND type in (N'U'))
	DROP TABLE [dbo].[SI_WEIGHTS1]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SI_WEIGHTS2]') AND type in (N'U'))
	DROP TABLE [dbo].[SI_WEIGHTS2]

-- remove any table with old SI_Inq_Names name
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SI_Inq_Names]') AND type in (N'U'))
	DROP TABLE [dbo].[SI_Inq_Names]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].SI_NAMES') AND type in (N'U'))
	DROP TABLE [dbo].[SI_NAMES]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON

CREATE TABLE [dbo].[SI_DATA1]
(
	DATA_ID [bigint] NOT NULL,
	GROUP_NAME [VARCHAR](30) NULL,
	INQ_DATA [VARCHAR](MAX) NULL,
  CONSTRAINT [PK_SI_DATA1] PRIMARY KEY CLUSTERED 
  (
    DATA_ID ASC
  )
  WITH 
  (
    PAD_INDEX  = OFF, 
    STATISTICS_NORECOMPUTE  = OFF, 
    IGNORE_DUP_KEY = OFF, 
    ALLOW_ROW_LOCKS  = ON, 
    ALLOW_PAGE_LOCKS  = ON
  ) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[SI_DATA2]
(
	DATA_ID [bigint] NOT NULL,
	GROUP_NAME [VARCHAR](30) NULL,
	INQ_DATA [VARCHAR](MAX) NULL,
  CONSTRAINT [PK_SI_DATA2] PRIMARY KEY CLUSTERED 
  (
    DATA_ID ASC
  )
  WITH 
  (
    PAD_INDEX  = OFF, 
    STATISTICS_NORECOMPUTE  = OFF, 
    IGNORE_DUP_KEY = OFF, 
    ALLOW_ROW_LOCKS  = ON, 
    ALLOW_PAGE_LOCKS  = ON
  ) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[SI_KEY1]
(
	TAG [VARCHAR](20) NOT NULL,
	TAG_VALUE [VARCHAR](50) NULL,
	DATA_ID [bigint] NOT NULL,
  CONSTRAINT [IX_SI_KEY1] PRIMARY KEY NONCLUSTERED 
  (
    TAG ASC,
    DATA_ID ASC
  )
  WITH 
  (
    PAD_INDEX  = OFF, 
    STATISTICS_NORECOMPUTE  = OFF, 
    IGNORE_DUP_KEY = OFF, 
    ALLOW_ROW_LOCKS  = ON, 
    ALLOW_PAGE_LOCKS  = ON
  ) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[SI_KEY2]
(
	TAG [VARCHAR](20) NOT NULL,
	TAG_VALUE [VARCHAR](50) NULL,
	DATA_ID [bigint] NOT NULL,
  CONSTRAINT [IX_SI_KEY2] PRIMARY KEY NONCLUSTERED 
  (
    TAG ASC,
    DATA_ID ASC
  )
  WITH 
  (
    PAD_INDEX  = OFF, 
    STATISTICS_NORECOMPUTE  = OFF, 
    IGNORE_DUP_KEY = OFF, 
    ALLOW_ROW_LOCKS  = ON, 
    ALLOW_PAGE_LOCKS  = ON
  ) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[SI_WEIGHTS1]
(
	WEIGHTS_ID [INT] IDENTITY(1,1) NOT NULL,
	GROUP_NAME [VARCHAR](50) NOT NULL,
	TAG [VARCHAR](50) NOT NULL,
	UNIQUE_RANK [INT] NOT NULL,
	IS_QUERY_KEY [VARCHAR](1) NOT NULL
  CONSTRAINT [PK_SI_WEIGHTS1] PRIMARY KEY CLUSTERED 
  (
    WEIGHTS_ID ASC
  )
  WITH 
  (
    PAD_INDEX  = OFF, 
    STATISTICS_NORECOMPUTE  = OFF, 
    IGNORE_DUP_KEY = OFF, 
    ALLOW_ROW_LOCKS  = ON, 
    ALLOW_PAGE_LOCKS  = ON
  ) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[SI_WEIGHTS2]
(
	WEIGHTS_ID [INT] IDENTITY(1,1) NOT NULL,
	GROUP_NAME [VARCHAR](50) NOT NULL,
	TAG [VARCHAR](50) NOT NULL,
	UNIQUE_RANK [INT] NOT NULL,
	IS_QUERY_KEY [VARCHAR](1) NOT NULL
  CONSTRAINT [PK_SI_WEIGHTS2] PRIMARY KEY CLUSTERED 
  (
    WEIGHTS_ID ASC
  )
  WITH 
  (
    PAD_INDEX  = OFF, 
    STATISTICS_NORECOMPUTE  = OFF, 
    IGNORE_DUP_KEY = OFF, 
    ALLOW_ROW_LOCKS  = ON, 
    ALLOW_PAGE_LOCKS  = ON
  ) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].SI_NAMES
(
	ROW_ID [tinyint] IDENTITY(1,1) NOT NULL,
  NEXT_HI [bigint] NOT NULL,
	INQ_SI_DATA [VARCHAR](32) NOT NULL,
	INQ_SI_KEY [VARCHAR](32) NOT NULL,
	INQ_SI_WEIGHTS [VARCHAR](32) NOT NULL,
	IMP_SI_DATA [VARCHAR](32) NOT NULL,
	IMP_SI_KEY [VARCHAR](32) NOT NULL,
	IMP_SI_WEIGHTS [VARCHAR](32) NOT NULL,
  CONSTRAINT [PK_SI_INQ_NAMES] PRIMARY KEY CLUSTERED 
  (
    ROW_ID ASC
  )
  WITH 
  (
    PAD_INDEX  = OFF, 
    STATISTICS_NORECOMPUTE  = OFF, 
    IGNORE_DUP_KEY = OFF, 
    ALLOW_ROW_LOCKS  = ON, 
    ALLOW_PAGE_LOCKS  = ON
  ) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_PADDING OFF
PRINT 'Created PB Inquiry tables.'
GO

-- Insert the only row, which will get updated by PB file imports
INSERT INTO [dbo].[SI_NAMES]
           ([NEXT_HI]
           ,[INQ_SI_DATA]
           ,[INQ_SI_KEY]
           ,[INQ_SI_WEIGHTS]
           ,[IMP_SI_DATA]
           ,[IMP_SI_KEY]
           ,[IMP_SI_WEIGHTS])
VALUES(
    0,           -- start the hilo generator with 0
    'SI_DATA1',
    'SI_KEY1',
    'SI_WEIGHTS1',
    'SI_DATA2',
    'SI_KEY2',
    'SI_WEIGHTS2')
GO
PRINT 'Inserted single row into SI_NAMES with reset hilo.'

-- Set the recovery model to be simple, since the PB data does not need to be recoverable
DECLARE @sql varchar(255) 
SET @sql = 'ALTER DATABASE ' + DB_NAME() + ' SET RECOVERY SIMPLE'
EXEC (@sql)
PRINT 'Altered DB to use minimal logging (simple recovery mode).'
GO

-- Table Compression
ALTER TABLE SI_DATA1 REBUILD PARTITION = ALL WITH (Data_Compression = PAGE) 
ALTER TABLE SI_KEY1 REBUILD PARTITION = ALL WITH (Data_Compression = PAGE) 
ALTER TABLE SI_WEIGHTS1 REBUILD PARTITION = ALL WITH (Data_Compression = PAGE)
ALTER TABLE SI_DATA2 REBUILD PARTITION = ALL WITH (Data_Compression = PAGE)
ALTER TABLE SI_KEY2 REBUILD PARTITION = ALL WITH (Data_Compression = PAGE) 
ALTER TABLE SI_WEIGHTS2 REBUILD PARTITION = ALL WITH (Data_Compression = PAGE)
GO

PRINT 'Altered inquiry tables to set page-level data compression.'
GO

-- Following MUST be last line of script!
SET noexec OFF -- Turn execution back on; only needed in SSMS, so as to be able to run this script again in the same session.

