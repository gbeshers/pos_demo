/*Bug 13824 - iPayment Baseline: Device ID Enhancement*/

CREATE TABLE TG_DEVICE_ID(
	DEVICE_ID int IDENTITY(1,1) NOT NULL,
	DEVICE_TYPE varchar(100) NOT NULL,
	SERIAL_NBR varchar(100) NOT NULL,
 CONSTRAINT PK_TG_DEVICE_ID PRIMARY KEY CLUSTERED 
(
	SERIAL_NBR ASC,
	DEVICE_TYPE ASC
)
)
GO

/*Bug 15417 - Add MERCHANT_KEY column to TG_SYSTEM_INTERFACE_TRACKING*/

ALTER TABLE TG_SYSTEM_INTERFACE_TRACKING
ADD MERCHANT_KEY VARCHAR(MAX) NULL
GO

/*Bug 15479 - Notify user when there's an outstanding credit card charge, and attempt to reuse it*/

ALTER TABLE TG_SYSTEM_INTERFACE_TRACKING
ADD TENDER_TYPE VARCHAR(25)

ALTER TABLE TG_SYSTEM_INTERFACE_TRACKING
ADD IS_FEE BIT
GO

/* ---------------------------------------------------------------------------
--    	Copyright 2012 Core Business Technologies
--
-- Add index on POSTDT to the activity log if not there already.
-- For NYC and other sites without this index.
--
-- TranSuite DB Version 4.0.9.0
--
-- change based on sql script 4.0.9.0
-- Last Updated: 
--    1.)  02/04/14 FT. TTS: 15248. Created.  This should ameliorate 15248. 
*/


/*
-- Create an index on the POSTDT column.  
-- This will be the default sorting column when ActivityLog is first brought up
-- and will speed up initial pagination.
-- TODO: Make this the clustered index instead of serial_ID
*/
IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = '_dta_index_TG_ACTLOG_DATA_POSTDT')
CREATE NONCLUSTERED INDEX [_dta_index_TG_ACTLOG_DATA_POSTDT] ON [dbo].[TG_ACTLOG_DATA] 
(
	[POSTDT] DESC
	--,[SUMMARY] ASC	-- This can be added to the index to speed retrievals when doing date plus Summary filtering
	--,[PKID] ASC		-- This can also be marginally useful
) WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

