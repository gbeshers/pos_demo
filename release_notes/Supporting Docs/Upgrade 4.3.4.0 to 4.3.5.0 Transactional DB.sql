/*
	Bug 12674 NJ - 8/29/2012
	This script will update the file type for existing business center files to 'W' to indicate the new file type for them.
*/
UPDATE TG_DEPFILE_DATA
SET FILETYPE='W'
WHERE SOURCE_TYPE LIKE 'OC'
