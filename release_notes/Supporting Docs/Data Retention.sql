/*
    Copyright 2012 Core Business Technologies

  	SQL Server DB ONLY
  	
	This script was created by Daniel Hofman on 6/4/2012 for bug# 3772.
    Stored Procedure Name:  sp_TransactionalRetention
    Archive Database Name:  T4ArchiveDB
    Purpose:				Stored procedure to be called from T4 or SQL Server maintenance job for the retention period processing
    
DESCRIPTION:

The data retention page in the Config application under general settings configures few control parameters:

*	"Archive Data" YES/NO
This determines if the data should be archived before it is deleted.
Setting it to YES will archive all the data to an archive database first. The archive database is read-only and its 
value is set to "T4ArchiveDB" due to limitations in the SQL Server stored procedure capabilities.

*	"ARCHIVE DATABASE NAME"
This is the read-only display of the archive database name.

*	"Number of Payfiles to Process"
This is the number of payfiles to be processed each time this procedure runs, allowing for gradual update of the system.

*	"Transactional Data Retention Period"

*	"Image Data Retention Period"

*	"Activity Log Data Retention Period"
These are the numeric values for the number of days the data should remain in the system before it is deleted or 
deleted and archived. Setting the above three to the value of zero will result in no action.


Return value:
Success: The number of files processed
Failure: The SQL Server error message combined with the file number and the file sequence number

Other values are printed to the screen as the procedure runs. Here is an example of two files processed and the 
printout in the SQL Server management studio:
	PAYFILE# 1110049 Sequence# 5 Archived
	PAYFILE# 1110049 Sequence# 5 Purged
	PAYFILE# 1110053 Sequence# 1 Archived
	PAYFILE# 1110053 Sequence# 1 Purged
	Processing TG_ACTLOG_DATA completed succesfully!
	Processing TG_IMAGE_DATA completed succesfully!


Here is the flow of the procedure in 4 steps:

Step 1: 
Configuration data is retrieved from the CBT_flex table and stored in the following variables:
@DoArchive
@NbrOfPayfilesToProcessThisTime
@TranDataRetention
@ImageDataRetention
@ActivityLogDataRetention

Step 2 (@TranDataRetention): 
The main control loop is created and it iterates through all payfiles in the TG_DEPFILE_DATA matching the 
@TranDataRetention criteria using "EFFECTIVEDT" column. At this point the DoArchive flag is checked and if set to true, 
all the data from the tables below is copied over to the archive database. 
If the destination table has an old identical record already there, it is first deleted before a new or updated record is 
copied over. The identity column values are preserved and copied over to the destination table. 
This allows for moving records back to the primary database if needed. The deletion of the original records from the primary 
database takes place after all data has been copied without any errors. Here is the list of tables that are copied or 
purged as part of the transactional data:

	TG_DEPFILE_DATA
	TG_PAYEVENT_DATA
	TG_TRAN_DATA
	TG_TENDER_DATA
	GR_CUST_FIELD_DATA
	CUST_FIELD_DATA
	TG_DEPOSIT_DATA  
	TG_DEPOSITTNDR_DATA <<-- There is an additional step to getting this data based on TG_DEPOSIT_DATA (DEPOSITID and DEPOSITNBR)
	TG_FILESOURCE_DATA
	TG_UPDATEFILE_DATA
	TG_TTA_MAP_DATA
	TG_ITEM_DATA
	TG_REVERSAL_EVENT_DATA
	TG_REVERSAL_TRAN_DATA
	TG_SUSPEVENT_DATA
	TG_SUSPTRAN_DATA
	TG_SYSTEM_INTERFACE_TRACKING
	TG_IMAGE_DATA

The TG_IMAGE_DATA table is processed as part of the transactional data in the main control loop, and then again 
outside of the main loop but with a TG_IMAGE_DATA specific retention period. This happens due to the fact that the 
users will keep the image data for a shorter length of time than the rest of the transactional data.

Note:
Any error conditions will terminate the main loop and return from the stored procedure with a SQL Server error message, 
the current file number and the file sequence number. The data is committed on per file basis, so any previously processed 
files would have already been committed.

Step 3 (@ActivityLogDataRetention):
The activity log data is optionally copied over to the T4ArchiveDB using the "POSTDT" column and the original records are 
deleted from the primary database. This step takes place after the main loop finished copying and deleting all records in 
the transactional section. The process is basically the same, but takes place outside of the main loop due to the different 
column used for the criteria selection, the lock of the payfile number and the payfile sequence number.

Note: Any errors here will only roll back the operation of the activity log retention cleanup. 
The transactional retention process in the main loop will not be affected. It has already been done and is committed at this 
point. The return value here will be the SQL Server error message. The overall return value of the stored procedure will show 
the number of files processed in the main loop. The activity data is not accounted for.

Step 4 (@ImageDataRetention):
The image data is optionally copied over to the T4ArchiveDB using the "DATE_INSERTED" column and the original records are 
deleted from the primary database. This step takes place after the main loop and the activity log finished copying and 
deleting all records in the previous steps. The process is basically the same, but takes place outside of the main loop.

The TG_IMAGE_DATA table is processed as part of the transactional data in the main loop and then again here outside of the 
main loop but with a TG_IMAGE_DATA specific retention period. The reason for this is that the users will keep the image 
data for a shorter length of time than the rest of the transactional data.

Note: Any errors here will only roll back the operation of image retention cleanup. The transactional retention and 
activity log retention processes will not be affected. They have already been done and are committed at this point. 
The return value here will be the SQL Server error message. The overall return value of the stored procedure will show the 
number of files processed in the main loop. The image data is not accounted for.
*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET NOCOUNT ON
GO
IF OBJECT_ID('sp_TransactionalRetention') IS NOT NULL 
	DROP PROCEDURE sp_TransactionalRetention;
GO

CREATE PROCEDURE sp_TransactionalRetention
AS
-- Possible return values:
---- Success: @NumberOfPayfilesProcessed
---- Error: @ERROR_MSG

DECLARE @depfilenbr int;
DECLARE @depfileseq int;
DECLARE @deptid varchar(40);
DECLARE @effective_date date; -- Hopefully the customer will not keep it open for a long time
DECLARE @NumberOfPayfilesProcessed int = 0;

-- The three retention periods
DECLARE @TranDataRetention int = 0;
DECLARE @ImageDataRetention int = 0;
DECLARE @ActivityLogDataRetention int = 0;
DECLARE @container varchar(255);
DECLARE @field_key varchar(255);
DECLARE @field_value varchar(400);
DECLARE @DoArchive varchar(5) = 'true'; -- Some customers may only want to delete the data. Default is TRUE.
DECLARE @NbrOfPayfilesToProcessThisTime int = 50; -- Control how many files to process at one time. This will prevent the process from running too long

-- Errors
DECLARE @PROCESSING_ERROR BIT;
DECLARE @ERROR_MSG varchar(MAX);

-- ==================== Retrieve the configuration settings ==============
SET @TranDataRetention = 0; -- OFF by default
SET @ImageDataRetention = 0; -- OFF by default
SET @ActivityLogDataRetention = 0; -- OFF by default

-- Get the transaction data retention setting
SET @container = NULL;
SET @field_key = NULL;
SET @field_value = NULL;
DECLARE TranDataRetention CURSOR FOR
SELECT container, field_key, field_value FROM CBT_Flex WHERE container='Business.data_retention.data' AND (field_key = '"transaction_data_retention"')
OPEN TranDataRetention
FETCH FROM TranDataRetention INTO @container, @field_key, @field_value
	IF @field_value IS NOT NULL AND @@FETCH_STATUS = 0
	BEGIN
		SET @TranDataRetention=@field_value
	END
CLOSE TranDataRetention;
DEALLOCATE TranDataRetention;

-- Get the image data retention setting
SET @container = NULL;
SET @field_key = NULL;
SET @field_value = NULL;
DECLARE ImageDataRetention CURSOR FOR
SELECT container, field_key, field_value FROM CBT_Flex WHERE container='Business.data_retention.data' AND (field_key = '"image_data_retention"')
OPEN ImageDataRetention
FETCH FROM ImageDataRetention INTO @container, @field_key, @field_value
	IF @field_value IS NOT NULL AND @@FETCH_STATUS = 0
	BEGIN
		SET @ImageDataRetention=@field_value
	END
CLOSE ImageDataRetention;
DEALLOCATE ImageDataRetention;

-- Get the activity data retention setting
SET @container = NULL;
SET @field_key = NULL;
SET @field_value = NULL;
DECLARE ActivityDataRetention CURSOR FOR
SELECT container, field_key, field_value FROM CBT_Flex WHERE container='Business.data_retention.data' AND (field_key = '"activity_data_retention"')
OPEN ActivityDataRetention
FETCH FROM ActivityDataRetention INTO @container, @field_key, @field_value
	IF @field_value IS NOT NULL AND @@FETCH_STATUS = 0
	BEGIN
		SET @ActivityLogDataRetention=@field_value
	END
CLOSE ActivityDataRetention;
DEALLOCATE ActivityDataRetention;

-- Should we archive data as well
SET @DoArchive = 'true';
DECLARE ArchiveData CURSOR FOR
SELECT container, field_key, field_value FROM CBT_Flex WHERE container='Business.data_retention.data' AND (field_key = '"do_archive"')
OPEN ArchiveData
FETCH FROM ArchiveData INTO @container, @field_key, @field_value
	IF @field_value IS NOT NULL AND @@FETCH_STATUS = 0
	BEGIN
		SET @DoArchive=@field_value
	END
CLOSE ArchiveData;
DEALLOCATE ArchiveData;

-- How many files to process this time
SET @NbrOfPayfilesToProcessThisTime = 50;
DECLARE FilesToProcessData CURSOR FOR
SELECT container, field_key, field_value FROM CBT_Flex WHERE container='Business.data_retention.data' AND (field_key = '"nbr_files_to_process"')
OPEN FilesToProcessData
FETCH FROM FilesToProcessData INTO @container, @field_key, @field_value
	IF @field_value IS NOT NULL AND @@FETCH_STATUS = 0
	BEGIN
		SET @NbrOfPayfilesToProcessThisTime=@field_value
	END
CLOSE FilesToProcessData;
DEALLOCATE FilesToProcessData;
-- ==================== END Retrieving the configuration settings ==============

DECLARE files CURSOR FOR
SELECT DEPFILENBR, DEPFILESEQ, DEPTID, EFFECTIVEDT FROM TG_DEPFILE_DATA WHERE datediff(day, EFFECTIVEDT, getdate()) >= @TranDataRetention
OPEN files
FETCH NEXT FROM files 
INTO @depfilenbr, @depfileseq, @deptid, @effective_date
WHILE @@FETCH_STATUS = 0
BEGIN

-- Create the error message
SET @ERROR_MSG = 'Error processing data. Payfile number# ' + CAST(@depfilenbr AS nvarchar(8)) + ' Sequence# ' + CAST(@depfileseq AS nvarchar(8)) + ' Error: ';

BEGIN TRANSACTION;	
	IF @TranDataRetention <> 0 AND @TranDataRetention IS NOT NULL
	
	-- TG_DEPFILE_DATA Data
	BEGIN TRY
	IF @DoArchive = 'true'
	BEGIN
	-- Delete the old records so they can be updated in case they have changed
	DELETE FROM T4ArchiveDB.dbo.TG_DEPFILE_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	
	SET IDENTITY_INSERT T4ArchiveDB.dbo.TG_DEPFILE_DATA OFF;
	SET IDENTITY_INSERT T4ArchiveDB.dbo.TG_DEPFILE_DATA ON;
	
	INSERT INTO T4ArchiveDB.dbo.TG_DEPFILE_DATA ([DEPFILENBR],[DEPFILESEQ],[DEPTID],[FILENAME],[FILETYPE],[FILEDESC],[AUTOBALANCE],[OPEN_USERID],[CREATOR_ID],[BAL_USERID],[ACC_USERID],[UPDATE_USERID],[CLOSE_USERID],[EFFECTIVEDT],[OPENDT],[BALDT],[ACCDT],[UPDATEDT],[CLOSEDT],[BALTOTAL],[GROUPID],[GROUPNBR],[SOURCE_TYPE],[SOURCE_GROUP],[SOURCE_DATE],[CHECKSUM],[PKID])
	SELECT [DEPFILENBR],[DEPFILESEQ],[DEPTID],[FILENAME],[FILETYPE],[FILEDESC],[AUTOBALANCE],[OPEN_USERID],[CREATOR_ID],[BAL_USERID],[ACC_USERID],[UPDATE_USERID],[CLOSE_USERID],[EFFECTIVEDT],[OPENDT],[BALDT],[ACCDT],[UPDATEDT],[CLOSEDT],[BALTOTAL],[GROUPID],[GROUPNBR],[SOURCE_TYPE],[SOURCE_GROUP],[SOURCE_DATE],[CHECKSUM],[PKID] FROM TG_DEPFILE_DATA
	WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	
	SET IDENTITY_INSERT T4ArchiveDB.dbo.TG_DEPFILE_DATA OFF;
	-- end TG_DEPFILE_DATA Data
	
	-- TG_PAYEVENT_DATA Data
	DELETE FROM T4ArchiveDB.dbo.TG_PAYEVENT_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	
	SET IDENTITY_INSERT T4ArchiveDB.dbo.TG_PAYEVENT_DATA OFF;
	SET IDENTITY_INSERT T4ArchiveDB.dbo.TG_PAYEVENT_DATA ON;
	
	INSERT INTO T4ArchiveDB.dbo.TG_PAYEVENT_DATA ([EVENTNBR],[DEPFILENBR],[DEPFILESEQ],[USERID],[STATUS],[MOD_DT],SOURCE_TYPE,SOURCE_GROUP,[SOURCE_REFID],[SOURCE_DATE],[CHECKSUM],[UNIQUEID],[CREATION_DT])
	SELECT TG_PAYEVENT_DATA.[EVENTNBR],TG_PAYEVENT_DATA.[DEPFILENBR],TG_PAYEVENT_DATA.[DEPFILESEQ],TG_PAYEVENT_DATA.[USERID],TG_PAYEVENT_DATA.[STATUS],TG_PAYEVENT_DATA.[MOD_DT],TG_PAYEVENT_DATA.SOURCE_TYPE,TG_PAYEVENT_DATA.SOURCE_GROUP,TG_PAYEVENT_DATA.[SOURCE_REFID],TG_PAYEVENT_DATA.SOURCE_DATE,TG_PAYEVENT_DATA.[CHECKSUM],TG_PAYEVENT_DATA.UNIQUEID,TG_PAYEVENT_DATA.CREATION_DT FROM TG_PAYEVENT_DATA
	WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	
	SET IDENTITY_INSERT T4ArchiveDB.dbo.TG_PAYEVENT_DATA OFF;
	-- end TG_PAYEVENT_DATA

	-- TG_TRAN_DATA
	DELETE FROM T4ArchiveDB.dbo.TG_TRAN_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq		
	
	SET IDENTITY_INSERT T4ArchiveDB.dbo.TG_TRAN_DATA OFF;
	SET IDENTITY_INSERT T4ArchiveDB.dbo.TG_TRAN_DATA ON;
	
	INSERT INTO T4ArchiveDB.dbo.TG_TRAN_DATA([TRANNBR],[EVENTNBR],[DEPFILENBR],[DEPFILESEQ],[CONTENTTYPE],[TTID],[TTDESC],[TAXEXIND],[DISTAMT],[TRANAMT],[ITEMIND],[COMMENTS],[SYSTEXT],[POSTDT],[VOIDDT],[VOIDUSERID],[SOURCE_TYPE],[SOURCE_GROUP],[SOURCE_REFID],[SOURCE_DATE],[CHECKSUM],[PKID])
	SELECT [TRANNBR],[EVENTNBR],[DEPFILENBR],[DEPFILESEQ],[CONTENTTYPE],[TTID],[TTDESC],[TAXEXIND],[DISTAMT],[TRANAMT],[ITEMIND],[COMMENTS],[SYSTEXT],[POSTDT],[VOIDDT],[VOIDUSERID],[SOURCE_TYPE],[SOURCE_GROUP],[SOURCE_REFID],[SOURCE_DATE],[CHECKSUM],[PKID] FROM TG_TRAN_DATA
	WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	
	SET IDENTITY_INSERT T4ArchiveDB.dbo.TG_TRAN_DATA OFF;
	-- end TG_TRAN_DATA
	
	-- TG_TENDER_DATA
	DELETE FROM T4ArchiveDB.dbo.TG_TENDER_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq		

	SET IDENTITY_INSERT T4ArchiveDB.dbo.TG_TENDER_DATA OFF;
	SET IDENTITY_INSERT T4ArchiveDB.dbo.TG_TENDER_DATA ON;
	
	INSERT INTO T4ArchiveDB.dbo.TG_TENDER_DATA([TNDRNBR] ,[EVENTNBR] ,[DEPFILENBR] ,[DEPFILESEQ] ,[TNDRID] ,[TNDRDESC] ,[TYPEIND] ,[AMOUNT] ,[CC_CK_NBR] ,[EXPMONTH] ,[EXPYEAR] ,[CCNAME] ,[AUTHNBR] ,[AUTHSTRING] ,[AUTHACTION] ,[BANKROUTINGNBR] ,[BANKACCTNBR] ,[ADDRESS] ,[SYSTEXT], [POSTDT] ,[VOIDDT] ,[VOIDUSERID] ,[SOURCE_TYPE] ,[SOURCE_GROUP] ,[SOURCE_REFID] ,[SOURCE_DATE] ,[CHECKSUM] ,[BANKACCTID] ,[PKID])
	SELECT [TNDRNBR] ,[EVENTNBR] ,[DEPFILENBR] ,[DEPFILESEQ] ,[TNDRID] ,[TNDRDESC] ,[TYPEIND] ,[AMOUNT] ,[CC_CK_NBR] ,[EXPMONTH] ,[EXPYEAR] ,[CCNAME] ,[AUTHNBR] ,[AUTHSTRING] ,[AUTHACTION] ,[BANKROUTINGNBR] ,[BANKACCTNBR] ,[ADDRESS] ,[SYSTEXT], [POSTDT] ,[VOIDDT] ,[VOIDUSERID] ,[SOURCE_TYPE] ,[SOURCE_GROUP] ,[SOURCE_REFID] ,[SOURCE_DATE] ,[CHECKSUM] ,[BANKACCTID] ,[PKID] FROM TG_TENDER_DATA
	WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	
	SET IDENTITY_INSERT T4ArchiveDB.dbo.TG_TENDER_DATA OFF
	-- end TG_TENDER_DATA
	
	-- GR_CUST_FIELD_DATA
	DELETE FROM T4ArchiveDB.dbo.GR_CUST_FIELD_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq		

	SET IDENTITY_INSERT T4ArchiveDB.dbo.GR_CUST_FIELD_DATA OFF;
	SET IDENTITY_INSERT T4ArchiveDB.dbo.GR_CUST_FIELD_DATA ON;
	
	INSERT INTO T4ArchiveDB.dbo.GR_CUST_FIELD_DATA([TRANNBR] ,[EVENTNBR] ,[DEPFILENBR] ,[DEPFILESEQ] ,[SCREENINDEX] ,[TTID] ,[CUSTID] ,[CUSTLABEL] ,[CUSTVALUE] ,[CUSTTAG] ,[CUSTATTR] ,[CHECKSUM] ,[PKID])
	SELECT [TRANNBR] ,[EVENTNBR] ,[DEPFILENBR] ,[DEPFILESEQ] ,[SCREENINDEX] ,[TTID] ,[CUSTID] ,[CUSTLABEL] ,[CUSTVALUE] ,[CUSTTAG] ,[CUSTATTR] ,[CHECKSUM] ,[PKID] FROM GR_CUST_FIELD_DATA
	WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	
	SET IDENTITY_INSERT T4ArchiveDB.dbo.GR_CUST_FIELD_DATA OFF;
	-- end GR_CUST_FIELD_DATA
	
	-- CUST_FIELD_DATA
	DELETE FROM T4ArchiveDB.dbo.CUST_FIELD_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq		
	
	SET IDENTITY_INSERT T4ArchiveDB.dbo.CUST_FIELD_DATA OFF;
	SET IDENTITY_INSERT T4ArchiveDB.dbo.CUST_FIELD_DATA ON;
	
	INSERT INTO T4ArchiveDB.dbo.CUST_FIELD_DATA([POSTNBR] ,[POST_TYPE] ,[EVENTNBR] ,[DEPFILENBR] ,[DEPFILESEQ] ,[SCREENINDEX] ,[POSTID] ,[CUSTID] ,[CUSTLABEL] ,[CUSTVALUE] ,[CUSTTAG] ,[CUSTATTR] ,[CHECKSUM] ,[PKID])
	SELECT [POSTNBR] ,[POST_TYPE] ,[EVENTNBR] ,[DEPFILENBR] ,[DEPFILESEQ] ,[SCREENINDEX] ,[POSTID] ,[CUSTID] ,[CUSTLABEL] ,[CUSTVALUE] ,[CUSTTAG] ,[CUSTATTR] ,[CHECKSUM] ,[PKID] FROM CUST_FIELD_DATA
	WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	
	SET IDENTITY_INSERT T4ArchiveDB.dbo.CUST_FIELD_DATA OFF;
	-- end CUST_FIELD_DATA
	
	-- TG_DEPOSIT_DATA
		-- First copy all deposits associated with this file
		DECLARE @DepositID varchar(12);
		DECLARE @DepositNbr int;
		DECLARE Deposits CURSOR FOR
		SELECT DEPOSITID, DEPOSITNBR FROM TG_DEPOSIT_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
		OPEN Deposits
		FETCH FROM Deposits INTO @DepositID, @DepositNbr
		WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM T4ArchiveDB.dbo.TG_DEPOSITTNDR_DATA WHERE DEPOSITID=@DepositID AND DEPOSITNBR=@DepositNbr	
			
			INSERT INTO T4ArchiveDB.dbo.TG_DEPOSITTNDR_DATA([DEPOSITID],[DEPOSITNBR],[SEQNBR],[TNDRPROP],[TNDR],[TNDRDESC],[AMOUNT],[SOURCE_REFID],[SOURCE_DATE],[CHECKSUM])
			SELECT [DEPOSITID],[DEPOSITNBR],[SEQNBR],[TNDRPROP],[TNDR],[TNDRDESC],[AMOUNT],[SOURCE_REFID],[SOURCE_DATE],[CHECKSUM] FROM TG_DEPOSITTNDR_DATA
			WHERE DEPOSITID=@DepositID AND DEPOSITNBR=@DepositNbr
			
			-- Also delete from here:
			DELETE FROM TG_DEPOSITTNDR_DATA WHERE DEPOSITID=@DepositID AND DEPOSITNBR=@DepositNbr
			
		FETCH NEXT FROM Deposits 
		INTO @DepositID, @DepositNbr
		END
		CLOSE Deposits;
		DEALLOCATE Deposits;
		-- end Pull all deposits associated with this file
	DELETE FROM T4ArchiveDB.dbo.TG_DEPOSIT_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq		
	
	SET IDENTITY_INSERT T4ArchiveDB.dbo.TG_DEPOSIT_DATA OFF;
	SET IDENTITY_INSERT T4ArchiveDB.dbo.TG_DEPOSIT_DATA ON;
	
	INSERT INTO T4ArchiveDB.dbo.TG_DEPOSIT_DATA([DEPOSITID],[DEPOSITNBR],[DEPOSITTYPE],[GROUPID],[GROUPNBR],[DEPFILENBR],[DEPFILESEQ],[OWNERUSERID],[CREATERUSERID],[AMOUNT],[POSTDT],[VOIDDT],[VOIDUSERID],[ACCDT],[ACC_USERID],[COMMENTS],[DEPSLIPNBR],[BANKID],[SOURCE_TYPE],[SOURCE_GROUP],[SOURCE_REFID],[SOURCE_DATE],[CHECKSUM],[UNIQUEID])
	SELECT [DEPOSITID],[DEPOSITNBR],[DEPOSITTYPE],[GROUPID],[GROUPNBR],[DEPFILENBR],[DEPFILESEQ],[OWNERUSERID],[CREATERUSERID],[AMOUNT],[POSTDT],[VOIDDT],[VOIDUSERID],[ACCDT],[ACC_USERID],[COMMENTS],[DEPSLIPNBR],[BANKID],[SOURCE_TYPE],[SOURCE_GROUP],[SOURCE_REFID],[SOURCE_DATE],[CHECKSUM],[UNIQUEID] FROM TG_DEPOSIT_DATA
	WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	
	SET IDENTITY_INSERT T4ArchiveDB.dbo.TG_DEPOSIT_DATA OFF;
	-- end TG_DEPOSIT_DATA
	
	-- TG_FILESOURCE_DATA
	DELETE FROM T4ArchiveDB.dbo.TG_FILESOURCE_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq		
			
	INSERT INTO T4ArchiveDB.dbo.TG_FILESOURCE_DATA([DEPFILENBR],[DEPFILESEQ],[SOURCE_TYPE],[SOURCE_GROUP],[STATUS],[CUSTFORMAT])
	SELECT [DEPFILENBR],[DEPFILESEQ],[SOURCE_TYPE],[SOURCE_GROUP],[STATUS],[CUSTFORMAT] FROM TG_FILESOURCE_DATA
	WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	-- end TG_FILESOURCE_DATA
	
	-- TG_UPDATEFILE_DATA
	DELETE FROM T4ArchiveDB.dbo.TG_UPDATEFILE_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq		
			
	INSERT INTO T4ArchiveDB.dbo.TG_UPDATEFILE_DATA([UPDATE_ID],[DEPFILENBR],[DEPFILESEQ],[INTERFACE_ID],[STATUS],[COMMENTS],[UPDATE_COUNT],[UPDATE_TOTAL],[CHECKSUM])
	SELECT [UPDATE_ID],[DEPFILENBR],[DEPFILESEQ],[INTERFACE_ID],[STATUS],[COMMENTS],[UPDATE_COUNT],[UPDATE_TOTAL],[CHECKSUM] FROM TG_UPDATEFILE_DATA
	WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	-- end TG_UPDATEFILE_DATA
	
	-- TG_TTA_MAP_DATA
	DELETE FROM T4ArchiveDB.dbo.TG_TTA_MAP_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq		
			
	INSERT INTO T4ArchiveDB.dbo.TG_TTA_MAP_DATA([TTAMAPNBR],[EVENTNBR],[DEPFILENBR],[DEPFILESEQ],[CREDITNBR],[CREDITITEMNBR],[DEBITNBR],[DEBITITEMNBR],[GLACCTNBR],[AMOUNT])
	SELECT [TTAMAPNBR],[EVENTNBR],[DEPFILENBR],[DEPFILESEQ],[CREDITNBR],[CREDITITEMNBR],[DEBITNBR],[DEBITITEMNBR],[GLACCTNBR],[AMOUNT] FROM TG_TTA_MAP_DATA
	WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	-- end TG_TTA_MAP_DATA
	
	-- TG_ITEM_DATA
	DELETE FROM T4ArchiveDB.dbo.TG_ITEM_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq		
	
	SET IDENTITY_INSERT T4ArchiveDB.dbo.TG_ITEM_DATA OFF;
	SET IDENTITY_INSERT T4ArchiveDB.dbo.TG_ITEM_DATA ON;
	
	INSERT INTO T4ArchiveDB.dbo.TG_ITEM_DATA([ITEMNBR],[TRANNBR],[EVENTNBR],[DEPFILENBR],[DEPFILESEQ],[ITEMID],[GLACCTNBR],[ACCTID],[ITEMACCTID],[ITEMDESC],[AMOUNT],[QTY],[TAXED],[TOTAL],[CHECKSUM],[ITEMCONFIGID],[PKID] )
	SELECT [ITEMNBR],[TRANNBR],[EVENTNBR],[DEPFILENBR],[DEPFILESEQ],[ITEMID],[GLACCTNBR],[ACCTID],[ITEMACCTID],[ITEMDESC],[AMOUNT],[QTY],[TAXED],[TOTAL],[CHECKSUM],[ITEMCONFIGID],[PKID] FROM TG_ITEM_DATA
	WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	
	SET IDENTITY_INSERT T4ArchiveDB.dbo.TG_ITEM_DATA OFF;
	-- end TG_ITEM_DATA
	
	-- TG_REVERSAL_EVENT_DATA
	DELETE FROM T4ArchiveDB.dbo.TG_REVERSAL_EVENT_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq		
			
	INSERT INTO T4ArchiveDB.dbo.TG_REVERSAL_EVENT_DATA([EVENTNBR],[DEPFILENBR],[DEPFILESEQ],[USERID],[REVERSED_EVENTNBR],[REVERSED_FILENBR],[REVERSED_FILESEQ],[VOIDED],[CHECKSUM])
	SELECT [EVENTNBR],[DEPFILENBR],[DEPFILESEQ],[USERID],[REVERSED_EVENTNBR],[REVERSED_FILENBR],[REVERSED_FILESEQ],[VOIDED],[CHECKSUM] FROM TG_REVERSAL_EVENT_DATA
	WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	-- end TG_REVERSAL_EVENT_DATA
	
	-- TG_REVERSAL_TRAN_DATA
	DELETE FROM T4ArchiveDB.dbo.TG_REVERSAL_TRAN_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq		
			
	INSERT INTO T4ArchiveDB.dbo.TG_REVERSAL_TRAN_DATA([TRANNBR],[EVENTNBR],[DEPFILENBR],[DEPFILESEQ],[REVERSED_TRANNBR],[CHECKSUM])
	SELECT [TRANNBR],[EVENTNBR],[DEPFILENBR],[DEPFILESEQ],[REVERSED_TRANNBR],[CHECKSUM] FROM TG_REVERSAL_TRAN_DATA
	WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	-- end TG_REVERSAL_TRAN_DATA
	
	-- TG_SUSPEVENT_DATA
	DELETE FROM T4ArchiveDB.dbo.TG_SUSPEVENT_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq		
			
	INSERT INTO T4ArchiveDB.dbo.TG_SUSPEVENT_DATA([EVENTNBR],[DEPFILENBR],[DEPFILESEQ],[SUSPEND_DT],[USERID],[RESUME_EVENTNBR],[RESUME_FILENBR],[RESUME_FILESEQ],[CHECKSUM])
	SELECT [EVENTNBR],[DEPFILENBR],[DEPFILESEQ],[SUSPEND_DT],[USERID],[RESUME_EVENTNBR],[RESUME_FILENBR],[RESUME_FILESEQ],[CHECKSUM] FROM TG_SUSPEVENT_DATA
	WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	-- end TG_SUSPEVENT_DATA
	
	-- TG_SUSPTRAN_DATA
	DELETE FROM T4ArchiveDB.dbo.TG_SUSPTRAN_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq		
			
	INSERT INTO T4ArchiveDB.dbo.TG_SUSPTRAN_DATA([TRANNBR],[EVENTNBR],[DEPFILENBR],[DEPFILESEQ],[CHECKSUM])
	SELECT [TRANNBR],[EVENTNBR],[DEPFILENBR],[DEPFILESEQ],[CHECKSUM] FROM TG_SUSPTRAN_DATA
	WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	-- end TG_SUSPTRAN_DATA
	
	-- TG_SYSTEM_INTERFACE_TRACKING
	DELETE FROM T4ArchiveDB.dbo.TG_SYSTEM_INTERFACE_TRACKING WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq		
			
	SET IDENTITY_INSERT T4ArchiveDB.dbo.TG_SYSTEM_INTERFACE_TRACKING OFF;
	SET IDENTITY_INSERT T4ArchiveDB.dbo.TG_SYSTEM_INTERFACE_TRACKING ON;

	INSERT INTO T4ArchiveDB.dbo.TG_SYSTEM_INTERFACE_TRACKING([SERIAL_ID],[USERID],[INITIAL_DATETIME],[INITIAL_SESSION_ID],[DEPFILENBR],[DEPFILESEQ],[EVENTNBR],[SYSTEM_TYPE],[ACTIVITY],[ACCOUNT_NUMBER],[AMOUNT],[STATUS],[LAST_DATETIME],[LAST_SESSION_ID],[REFERENCE],[COMMENTS])
	SELECT [SERIAL_ID],[USERID],[INITIAL_DATETIME],[INITIAL_SESSION_ID],[DEPFILENBR],[DEPFILESEQ],[EVENTNBR],[SYSTEM_TYPE],[ACTIVITY],[ACCOUNT_NUMBER],[AMOUNT],[STATUS],[LAST_DATETIME],[LAST_SESSION_ID],[REFERENCE],[COMMENTS] FROM TG_SYSTEM_INTERFACE_TRACKING
	WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	
	SET IDENTITY_INSERT T4ArchiveDB.dbo.TG_SYSTEM_INTERFACE_TRACKING OFF;
	-- end TG_SYSTEM_INTERFACE_TRACKING
	
	-- AGR_TENDER_TYPES
	DELETE FROM T4ArchiveDB.dbo.AGR_TENDER_TYPES WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq			

	INSERT INTO T4ArchiveDB.dbo.AGR_TENDER_TYPES([DEPTID],[DEPFILENBR],[DEPFILESEQ],[TNDRID],[TNDRDESC])
	SELECT [DEPTID],[DEPFILENBR],[DEPFILESEQ],[TNDRID],[TNDRDESC] FROM AGR_TENDER_TYPES
	WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	-- end AGR_TENDER_TYPES
	
	-- AGR_TRANSACTION_TYPES
	DELETE FROM T4ArchiveDB.dbo.AGR_TRANSACTION_TYPES WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq			

	INSERT INTO T4ArchiveDB.dbo.AGR_TRANSACTION_TYPES([DEPTID],[DEPFILENBR],[DEPFILESEQ],[TTID],[TTDESC])
	SELECT [DEPTID],[DEPFILENBR],[DEPFILESEQ],[TTID],[TTDESC] FROM AGR_TRANSACTION_TYPES
	WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	-- end AGR_TRANSACTION_TYPES
	
	-- TG_IMAGE_DATA
	-- Users will tipically keep the images for a shorter length of time than the transactional data. 
	-- This is why the second retention period setting. But handle them here as well in case there were any leftovers.
	DELETE FROM T4ArchiveDB.dbo.TG_IMAGE_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq		
			
	INSERT INTO T4ArchiveDB.dbo.TG_IMAGE_DATA([DEPFILENBR],[DEPFILESEQ],[EVENTNBR],[POSTNBR],[POST_TYPE],[SEQ_NBR],[DOC_IMAGE],[IMAGING_TYPE],[COMPLETE_MICR_LINE],[BANK_ROUTING_NUMBER],[ACCOUNT_NUMBER],[CHECK_NUMBER],[DATE_INSERTED],[USERID],[COMMENTS],[SOURCE_TYPE],[SOURCE_GROUP],[SOURCE_REFID],[SOURCE_DATE],[SCAN_DATA],[DOC_IMAGE_REF],[IMAGE_FORMAT],[CHECKSUM],[DOC_ID])
	SELECT [DEPFILENBR],[DEPFILESEQ],[EVENTNBR],[POSTNBR],[POST_TYPE],[SEQ_NBR],[DOC_IMAGE],[IMAGING_TYPE],[COMPLETE_MICR_LINE],[BANK_ROUTING_NUMBER],[ACCOUNT_NUMBER],[CHECK_NUMBER],[DATE_INSERTED],[USERID],[COMMENTS],[SOURCE_TYPE],[SOURCE_GROUP],[SOURCE_REFID],[SOURCE_DATE],[SCAN_DATA],[DOC_IMAGE_REF],[IMAGE_FORMAT],[CHECKSUM],[DOC_ID] FROM TG_IMAGE_DATA
	WHERE datediff(day, DATE_INSERTED, getdate()) >= @ImageDataRetention
	-- end TG_IMAGE_DATA
	
	-- AGR_USERS
	DELETE FROM T4ArchiveDB.dbo.AGR_USERS WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq			

	INSERT INTO T4ArchiveDB.dbo.AGR_USERS([DEPTID],[DEPFILENBR],[DEPFILESEQ],[USERID])
	SELECT [DEPTID],[DEPFILENBR],[DEPFILESEQ],[USERID] FROM AGR_USERS
	WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	-- end AGR_USERS
	
	PRINT 'PAYFILE# ' + CAST(@depfilenbr AS varchar(8)) + ' Sequence# ' + CAST(@depfileseq AS varchar(8)) + ' Archived';
	END
		
    -- Delete the original records now
	delete from TG_PAYEVENT_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	delete from TG_TRAN_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	delete from TG_TENDER_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	delete from TG_DEPFILE_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	delete from GR_CUST_FIELD_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	delete from CUST_FIELD_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	delete from TG_DEPOSIT_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	
	-- This has been processed as part of the TG_DEPOSIT_DATA. There is an additional step to getting this data based on DEPOSITID=@DepositID AND DEPOSITNBR=@DepositNbr.
	-- delete from TG_DEPOSITTNDR_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	
	delete from TG_UPDATEFILE_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	delete from TG_TTA_MAP_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	delete from TG_ITEM_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	delete from TG_REVERSAL_EVENT_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	delete from TG_REVERSAL_TRAN_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	delete from TG_SUSPEVENT_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	delete from TG_SUSPTRAN_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	delete from TG_SYSTEM_INTERFACE_TRACKING WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	delete from AGR_TENDER_TYPES WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	delete from AGR_TRANSACTION_TYPES WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	delete from AGR_USERS WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	delete from TG_IMAGE_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
	
	PRINT 'PAYFILE# ' + CAST(@depfilenbr AS nvarchar(8)) + ' Sequence# ' + CAST(@depfileseq AS nvarchar(8)) + ' Purged';
	END TRY
	BEGIN CATCH
	ROLLBACK TRANSACTION;
	CLOSE files;
	DEALLOCATE files;
	RETURN @ERROR_MSG + ERROR_MESSAGE();
	END CATCH

COMMIT TRANSACTION;	

SET @NumberOfPayfilesProcessed = @NumberOfPayfilesProcessed + 1;

-- Do not process more than configured number of files
IF @NumberOfPayfilesProcessed >= @NbrOfPayfilesToProcessThisTime
	BREAK
	
FETCH NEXT FROM files 
INTO @depfilenbr, @depfileseq, @deptid, @effective_date
END 
CLOSE files;
DEALLOCATE files;

-- Remove the TG_ACTLOG_DATA entries based on post date ==================================================
BEGIN TRY
    IF @ActivityLogDataRetention > 0
    BEGIN
	BEGIN TRANSACTION
	IF @DoArchive = 'true'
	BEGIN	
		SET IDENTITY_INSERT T4ArchiveDB.dbo.TG_ACTLOG_DATA OFF;
		SET IDENTITY_INSERT T4ArchiveDB.dbo.TG_ACTLOG_DATA ON;

		DELETE FROM T4ArchiveDB.dbo.TG_ACTLOG_DATA WHERE datediff(day, POSTDT, getdate()) >= @ActivityLogDataRetention			

		INSERT INTO T4ArchiveDB.dbo.TG_ACTLOG_DATA([POSTDT],[SESSION_ID],[USERID],[APP],[SUBJECT],[ACTION_NAME],[ARGS],[SUMMARY],[DETAIL],[PKID])
		SELECT [POSTDT],[SESSION_ID],[USERID],[APP],[SUBJECT],[ACTION_NAME],[ARGS],[SUMMARY],[DETAIL],[PKID] FROM TG_ACTLOG_DATA
		WHERE datediff(day, POSTDT, getdate()) >= @ActivityLogDataRetention

		SET IDENTITY_INSERT T4ArchiveDB.dbo.TG_ACTLOG_DATA OFF;
	END
	
	delete from TG_ACTLOG_DATA WHERE datediff(day, POSTDT, getdate()) >= @ActivityLogDataRetention
	COMMIT TRANSACTION;
	END
	PRINT 'Processing TG_ACTLOG_DATA completed succesfully!'
END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION;
	RETURN 'Unable to process TG_ACTLOG_DATA table. Error: ' + ERROR_MESSAGE();
END CATCH
-- END Remove the TG_ACTLOG_DATA entries based on post date ================================================

-- Remove the TG_IMAGE_DATA entries based on post date ==================================================
-- Users will tipically keep the images for a shorter length of time than the transactional data. This is why the second retention period setting.
BEGIN TRY
    IF @ImageDataRetention > 0
    BEGIN
	BEGIN TRANSACTION
	IF @DoArchive = 'true'
	BEGIN	
		DELETE FROM T4ArchiveDB.dbo.TG_IMAGE_DATA WHERE datediff(day, DATE_INSERTED, getdate()) >= @ImageDataRetention			

		INSERT INTO T4ArchiveDB.dbo.TG_IMAGE_DATA([DEPFILENBR],[DEPFILESEQ],[EVENTNBR],[POSTNBR],[POST_TYPE],[SEQ_NBR],[DOC_IMAGE],[IMAGING_TYPE],[COMPLETE_MICR_LINE],[BANK_ROUTING_NUMBER],[ACCOUNT_NUMBER],[CHECK_NUMBER],[DATE_INSERTED],[USERID],[COMMENTS],[SOURCE_TYPE],[SOURCE_GROUP],[SOURCE_REFID],[SOURCE_DATE],[SCAN_DATA],[DOC_IMAGE_REF],[IMAGE_FORMAT],[CHECKSUM],[DOC_ID])
		SELECT [DEPFILENBR],[DEPFILESEQ],[EVENTNBR],[POSTNBR],[POST_TYPE],[SEQ_NBR],[DOC_IMAGE],[IMAGING_TYPE],[COMPLETE_MICR_LINE],[BANK_ROUTING_NUMBER],[ACCOUNT_NUMBER],[CHECK_NUMBER],[DATE_INSERTED],[USERID],[COMMENTS],[SOURCE_TYPE],[SOURCE_GROUP],[SOURCE_REFID],[SOURCE_DATE],[SCAN_DATA],[DOC_IMAGE_REF],[IMAGE_FORMAT],[CHECKSUM],[DOC_ID] FROM TG_IMAGE_DATA
		WHERE datediff(day, DATE_INSERTED, getdate()) >= @ImageDataRetention
	END
	
	delete from TG_IMAGE_DATA WHERE datediff(day, DATE_INSERTED, getdate()) >= @ImageDataRetention
	COMMIT TRANSACTION;
	END
	PRINT 'Processing TG_IMAGE_DATA completed succesfully!'
END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION;
	RETURN 'Unable to process TG_IMAGE_DATA table. Error: ' + ERROR_MESSAGE();
END CATCH
-- END Remove the TG_IMAGE_DATA entries based on post date ================================================
PRINT 'PayFiles processed:' + CAST(@NumberOfPayfilesProcessed AS varchar(8));
RETURN (@NumberOfPayfilesProcessed);
GO
