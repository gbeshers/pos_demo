﻿using System.Reflection;

[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("CORE Business Technologies")]
[assembly: AssemblyProduct("iPayment Enterprise")]
[assembly: AssemblyCopyright("Copyright (c) 2020-21 CORE Business Technologies")]
[assembly: AssemblyTrademark("iPayment Enterprise (r)")]

// due to bug in .NET, leave this blank until we partition bin dir into subdirs for culture
// see: http://support.microsoft.com/kb/924731
[assembly: AssemblyCulture("")]

//
// Version information for an iPayment DLL consists of the following four values:
//
//      Major Version (product generation & major version, eg. 45)
//      Minor Version (security element, eg. 1)
//      Build Number (minor version number, eg. 0)
//      Revision (maintenance release number, eg. 37515). Product is typically 0, but for projects, use the SVN revision info
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
//  
// Bug 26506 HX: Update version info to 4.5.1.7.0 in the VersionInfo.cs files, load.casl, and applicable page titles
// Bug 25381 HX: Update version info to 4.5.1.5.0 in the AssemblyInfo.cs files, load.casl, and applicable page titles
// Bug 23959 HX: Update version info to 4.5.1.4.0 in the AssemblyInfo.cs files, load.casl, and applicable page titles
// Bug 23533 HX: Update version info to 4.5.1.3.0 in the AssemblyInfo.cs files and load.casl
// Bug 22924 HX: Update version info to 4.5.1.2.0 in the AssemblyInfo.cs files and load.casl
// Bug 22656 HX: Update version info to 4.5.1.1.0 in the AssemblyInfo.cs files and load.casl
// IPAY-238 UMN: 4.5.1.8.0 in the VersionInfo.cs files, load.casl, and applicable page titles
[assembly: AssemblyVersion("4.5.1.8")]
[assembly: AssemblyFileVersion("4.5.1.8")]
[assembly: AssemblyInformationalVersion("4.5.1.8.0")]

