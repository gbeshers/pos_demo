var WAIT = 0;
var READ = 1;

var state = WAIT;
var barcodeString = "";

var ignore_barcode_reader = false;

function ParseKey(event) {
  if ( !ignore_barcode_reader ) {
    key = event.keyCode;
    switch( state ) {
      case WAIT:
        if ( key == 42 ) {
          state = READ;
          event.cancelBubble = true;
        }
        break;
      case READ:
        if ( key == 42 ) {
          state = WAIT;
          ParseBarcode(barcodeString);
          barcodeString = "";
        } else {
          barcodeString += unescape("%" + key.toString(16));
        }
        break;
    }
  }
}

function ParseBarcode(barcodeString) {
  alert(barcodeString);
}