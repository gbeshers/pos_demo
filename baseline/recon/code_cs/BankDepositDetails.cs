﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using CASL_engine;

namespace recon
{
  public class BankDepositDetails : BankDeposit
  {
    public String text { get; set; }       // "Text" field from BAI file
    public String bankReference { get; set; }  // Unique ID from bank
    public String matchKey { get; set; }   // Bug 10250: FT: Added drop down details.  Does not effect checksum.
    public String checksum { get; set; }

    /// <summary>
    /// Bug 18982
    /// </summary>
    public DateTime importTimestamp { get; set; }   

    override public string ToString()
    {
      var sb = new StringBuilder();
      sb.Append("ID: ").Append(databaseID.databaseKey).Append("\n")
        .Append("Date: ").Append(depDate).Append("\n")
        .Append("Ref: ").Append(refNum).Append("\n")
        .Append("Amount: ").Append(String.Format("{0:C}", amount)).Append("\n")
        .Append("Account: ").Append(account).Append("\n")
        .Append("Age: ").Append(age).Append("\n")
        .Append("Status: ").Append(bStatusToString(depositStatus)).Append("\n")
        .Append("Text: ").Append(text).Append("\n")
        .Append("Bank Reference: ").Append(bankReference).Append("\n")
        .Append("Import Timestamp: ").Append(importTimestamp)
        .Append("Match Key: ").Append(matchKey).Append("\n");
      return sb.ToString();
    }

    /// <summary>
    /// Output a compressed version of ToString() suitable for stuffing in 
    /// the DETAIL field of the activity log.
    /// Bug 12482.
    /// </summary>
    /// <returns>String version record in compressed form</returns>
    public String ToAuditString()
    {
      var sb = new StringBuilder();

      sb.Append(databaseID.databaseKey).Append(':')
      .Append(depDate).Append(':')
      .Append(refNum).Append(':')
      .Append(amount).Append(':')
      .Append(account).Append(':')
      .Append(age).Append(':')
      .Append(bStatusToString(depositStatus)).Append(':')
      .Append(text).Append(':')
      .Append(bankReference).Append(':')
      .Append(matchKey);
      return sb.ToString();
    }

    /// <summary>
    /// Compute and validate checksums with this utility method.
    /// Bug 9808: FT: Here is where the checksums for the TG_BANK_DEPOSIT_DATA table are done
    /// </summary>
    /// <returns></returns>
    private String computeChecksumString(bool includeTextField)
    {
      StringBuilder checksumBuilder = new StringBuilder();
      // Be very, very careful when you format decimals. 
      // StringBuilder.Append does variable things with .00 extensions! 
      // Force a .00 extension
      String amountStr = String.Format("{0:0.00}", this.amount);

      // Also be very careful of dates.
      // Use the standardized "ShortDate+LongTime" format 0:G (M/d/yyyy h:mm:ss tt)
      String dateStr = String.Format("{0:G}", this.depDate);

      checksumBuilder
      .Append(dateStr)
      .Append(this.account)
      .Append(this.refNum)
      .Append(this.bankReference)
      .Append(amountStr)
      .Append(convertStatus(this.depositStatus));

      // Bug 10938: FT: Took Text field out of checksum to easy Match Key work on site at UPHS.
      // A dubious thing to do but we are stuck with it!
      if (includeTextField)
        checksumBuilder.Append(this.text);     

      return checksumBuilder.ToString();
    }

    /// <summary>
    /// Compute the checksum before writing it.
    /// This uses the new (as of UPHS) method of not including 
    /// the Text field in the checksum.
    /// See 10938.
    /// </summary>
    public void computeChecksum()
    {
      String checksumString = computeChecksumString(false);

      this.checksum = Crypto.ComputeHash("data", checksumString);
    }

    /// <summary>
    /// Validate the checksum in the TG_BANK_DEPOSIT_TABLE.
    /// This checksum has been complicated by the unfortunate decision
    /// in the UPenn port (10938) to not include the Text field in the checksum.
    /// This caused an incompatibility so that this function now has to
    /// check both with and without the Text field.  
    /// Going forward, the field will not be checked.
    /// </summary>
    /// <returns></returns>
    public Boolean validateChecksum()
    {
      // First try without the Text field
      String checksumString = computeChecksumString(false);
      String recomputedChecksum = Crypto.ComputeHash("data", checksumString);
      if (this.checksum.Equals(recomputedChecksum))
        return true;

      // Second pass with the Text field for older code
      checksumString = computeChecksumString(true);
      recomputedChecksum = Crypto.ComputeHash("data", checksumString);
      return this.checksum.Equals(recomputedChecksum);
    }

  }
}
