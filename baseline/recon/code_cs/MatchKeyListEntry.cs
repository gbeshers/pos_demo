﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace recon
{
  /// <summary>
  /// Holder for new Automatch entries that are not binned.
  /// In addition, compound Bank Reconciliation Keys (match keys) can 
  /// be specified in the Workgroup configuration.
  /// Bug 11302
  /// </summary>
  class MatchKeyListEntry
  {
    /// <summary>
    /// The real account ID from config.  
    /// Account ID is stored as BANKID field in TG_DEPOSIT_DATA as a small number
    /// but this needs to be transformed to the real bank account number with the
    /// "account_nbr" config variable.
    /// </summary>
    public string accountId { get; set; }

    /// <summary>
    /// Can be single string or comma-separated compound string
    /// </summary>
    public string matchKey { get; set; }

    public Decimal amount { get; set; }

    /// <summary>
    /// A way to attach the above keys to real database records
    /// </summary>
    public DatabaseID databaseId { get; set; }

    /// <summary>
    /// Use this to flag entries that have already been reconciled
    /// when looping through a list of these objects in automatch.
    /// </summary>
    public bool reconciled { get; set; }

    /// <summary>
    /// ctor for creating a new object.
    /// </summary>
    /// <param name="matchKey"></param>
    /// <param name="amount"></param>
    /// <param name="databaseId"></param>
    public MatchKeyListEntry(string matchKey, Decimal amount, DatabaseID databaseId)
    {
      this.matchKey = matchKey;
      this.amount = amount;
      this.databaseId = databaseId;
      this.reconciled = false;
    }

    public override string ToString()
    {
      return string.Format("MatchKey: {0}\tAmount: {1:c}\tdatabaseID: {2}\treconciled {3}", 
        matchKey, amount, databaseId.databaseKey.ToString(), reconciled);
    }
  }
}
