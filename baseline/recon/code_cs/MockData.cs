﻿using System;
using System.Collections.Generic;
using System.Text;

namespace recon
{
  public class MockData
  {
    /// <summary>
    /// Mock reconcilation list
    /// </summary>
    private List<ReconciledDeposit> reconList = new List<ReconciledDeposit>() { 
            new ReconciledDeposit { 
                reconDate = Convert.ToDateTime("07/01/2010"), 
                user = "Jake",     
                reconType=ReconciledDeposit.rType.auto,
                reconStatus=ReconciledDeposit.rStatus.reconciled, 
                refNum = "102195",  
                amount=123.00M, 
                overShort=0,
                databaseID = new DatabaseID("reconKey1")
            }, 

            new ReconciledDeposit { 
                reconDate = Convert.ToDateTime("07/02/2010"), 
                user = "Harlow",   
                reconType=ReconciledDeposit.rType.manual,   
                reconStatus=ReconciledDeposit.rStatus.reconciled, 
                refNum = "10154",   
                amount=2011.43M,
                overShort=0, 
                databaseID = new DatabaseID("reconKey2")
            }, 

            new ReconciledDeposit { 
                reconDate = Convert.ToDateTime("07/08/2010"), 
                user = "Harlow",   
                reconType=ReconciledDeposit.rType.forced,   
                reconStatus=ReconciledDeposit.rStatus.reconciled, 
                refNum = "0021954", 
                amount=100.00M, 
                overShort=0, 
                databaseID = new DatabaseID("reconKey3")
            }, 

            new ReconciledDeposit { 
                reconDate = Convert.ToDateTime("07/08/2010"), 
                user = "Marlow",   
                reconType=ReconciledDeposit.rType.auto,   
                reconStatus=ReconciledDeposit.rStatus.reconciled, 
                refNum = "0021955", 
                amount=10.00M,  
                overShort=0,
                databaseID = new DatabaseID("reconKey4")
            }, 

            new ReconciledDeposit { 
                reconDate = Convert.ToDateTime("07/08/2010"), 
                user = "Barlow",   
                reconType=ReconciledDeposit.rType.auto,   
                reconStatus=ReconciledDeposit.rStatus.reconciled, 
                refNum = "0021960", 
                amount=200.00M, 
                overShort=1.55M, 
                databaseID = new DatabaseID("reconKey5")
            }, 
        };

    /// <summary>
    /// Mock bank deposit list
    /// </summary>
    private List<BankDeposit> bankDepositList = new List<BankDeposit>() { 
            new BankDeposit { 
                depDate = Convert.ToDateTime("07/01/2010"), 
                refNum="001223",
                amount=101.75M, 
                account = "102195",  
                databaseID = new DatabaseID("bank1"), 
                age = 15,
                depositStatus = BankDeposit.bStatus.unreconciled
            }, 

            new BankDeposit { 
                depDate = Convert.ToDateTime("07/02/2010"), 
                refNum="001224",
                amount=222.33M, 
                account = "102195",  
                databaseID = new DatabaseID("bank2"), 
                age = 14,
                depositStatus = BankDeposit.bStatus.unreconciled
            }, 

            new BankDeposit { 
                depDate = Convert.ToDateTime("07/03/2010"), 
                refNum="001224",
                amount=100.00M, 
                account = "200111",  
                databaseID = new DatabaseID("bank3"), 
                age = 8,
                depositStatus = BankDeposit.bStatus.newDeposit
            }, 

            new BankDeposit { 
                depDate = Convert.ToDateTime("07/04/2010"), 
                refNum="001225",
                amount=500.00M, 
                account = "200112",  
                databaseID = new DatabaseID("bank4"), 
                age = 7,
                depositStatus = BankDeposit.bStatus.newDeposit
            }, 
        };


    /// <summary>
    /// Mock expected deposit list
    /// </summary>
    private List<ExpectedDeposit> expectedDepositList = new List<ExpectedDeposit>() { 
            new ExpectedDeposit { 
                depDate = Convert.ToDateTime("07/01/2010"), 
                refNum="2001345",
                amount=101.75M, 
                account = "102195",  
                file = "3300213",
                databaseID = new DatabaseID("expected1"), 
                age = 21
            },

            new ExpectedDeposit { 
                depDate = Convert.ToDateTime("07/01/2010"), 
                refNum="2001346",
                amount=3000.00M, 
                account = "102195",  
                file = "3300214",
                databaseID = new DatabaseID("expected2"), 
                age = 20
            },

            new ExpectedDeposit { 
                depDate = Convert.ToDateTime("07/01/2010"), 
                refNum="2001347",
                amount=222.15M, 
                account = "102195",  
                file = "3300215",
                databaseID = new DatabaseID("expected3"), 
                age = 14
            },

            new ExpectedDeposit { 
                depDate = Convert.ToDateTime("07/01/2010"), 
                refNum="2001348",
                amount=345.34M, 
                account = "102195",  
                file = "3300261",
                databaseID = new DatabaseID("expected4"), 
                age = 13
            },

            new ExpectedDeposit { 
                depDate = Convert.ToDateTime("07/01/2010"), 
                refNum="2001349",
                amount=500.00M, 
                account = "102195",  
                file = "3300362",
                databaseID = new DatabaseID("expected5"), 
                age = 12
            },

            new ExpectedDeposit { 
                depDate = Convert.ToDateTime("07/01/2010"), 
                refNum="2001350",
                amount=190.75M, 
                account = "102195",  
                file = "33002263",
                databaseID = new DatabaseID("expected6"), 
                age = 11
            },

            new ExpectedDeposit { 
                depDate = Convert.ToDateTime("07/01/2010"), 
                refNum="2001351",
                amount=200.00M, 
                account = "102195",  
                file = "3300364",
                databaseID = new DatabaseID("expected7"), 
                age = 10
            },

        };


    public List<ReconciledDeposit> getReconListMock()
    {
      return reconList;
    }

    public List<BankDeposit> getBankDepositListMock()
    {
      return bankDepositList;
    }

    public List<ExpectedDeposit> getExpectedDepositListMock()
    {
      return expectedDepositList;
    }

  }
}
