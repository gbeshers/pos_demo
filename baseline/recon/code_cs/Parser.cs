﻿using System;
using System.Collections.Generic;
using System.Text;

namespace recon
{
  abstract class Parser
  {
    // Bug 17179: need to pull the bank account from configuration
    public abstract List<TransactionRecord> parse(String path, string bankAccount, bool checkLineNumbers, out ReconStatus status);
  }
}
