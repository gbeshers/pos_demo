﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace recon
{
  class BinnedMatchValue : IComparable<BinnedMatchValue>
  {
    public string matchKey { get; set; }
    public Decimal amount { get; set; }
    public bool reconciled { get; set; }
    public List<DatabaseID> bankDepositIds { get; set; }
    public string depositDay { get; set; }   // for debugging

    public int CompareTo(BinnedMatchValue other)
    {
      // Alphabetic sort if matchKey is equal then check reconciled
      if (other.matchKey == this.matchKey)
      {
        if (other.amount == this.amount)
          return other.reconciled.CompareTo(this.reconciled);
        return other.amount.CompareTo(this.amount);
      }
      return other.matchKey.CompareTo(this.matchKey);
    }


    public BinnedMatchValue(string matchKey, Decimal amount, List<DatabaseID> bankDepositIds)
    {
      this.matchKey = matchKey;
      this.amount = amount;
      this.bankDepositIds = bankDepositIds;
      this.reconciled = false;
    }

    /// <summary>
    /// For storing Expected Deposit IDs
    /// </summary>
    /// <param name="expectedID"></param>
    /// <param name="amount"></param>
    public void add(DatabaseID expectedID, Decimal amount)
    {
      bankDepositIds.Add(expectedID);
      this.amount += amount;
    }

    /// <summary>
    /// Override the .Equals.  Avoid C++ style operator overloads! 
    /// </summary>
    /// <param name="idObject"></param>
    /// <returns></returns>
    /// 
    public override bool Equals(object MatchValue)
    {
      BinnedMatchValue matchValue = (BinnedMatchValue)MatchValue;

      if (matchValue.matchKey != this.matchKey)
        return false;
      if (matchValue.amount != this.amount)
        return false;
      return matchValue.reconciled == this.reconciled;
    }

    /// <summary>
    /// A rough attempt at a hashcode.  This will return the same
    /// code for objects with the same data.
    /// </summary>
    /// <returns></returns>
    public override int GetHashCode()
    {
      var builder = new StringBuilder();
      builder.Append(matchKey).Append(amount).Append(reconciled);
      foreach (DatabaseID databaseID in bankDepositIds)
        builder.Append(databaseID);
      return builder.ToString().GetHashCode();
    }
  }
}
