﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;


namespace recon
{
  /// <summary>
  /// Holds the status codes for return values for the recon API calls
  /// </summary>
  public class ReconStatus
  {
    internal bool success { get; set; }
    /// <summary>
    /// Can be set when success is true
    /// </summary>
    internal String comments { get; set; }
    /// <summary>
    /// Can be set when success is true
    /// </summary>
    internal String warning { get; set; }

    internal String humanReadableError { get; set; }
    internal String detailedError { get; set; }
    internal String code { get; set; }
    internal String trace { get; set; }

    internal enum severityCode {no_error, fatal, error, warning, info};
    internal severityCode severity { get; set; }

    /// <summary>
    /// Special purpose flag used only for checksum errors.  
    /// Used so that the servityCode does not get tainted by checksum "situations".
    /// </summary>
    internal bool checksumError { get; set; }

    public ReconStatus(bool success)
    {
      this.success = success;
      if (success)
        this.severity = severityCode.no_error;
      else
        this.severity = severityCode.error;
      checksumError = false;
    }

    internal void setErr(String humanReadable, String error, String code, bool dumpStack)
    {
      setErr(humanReadable, error, code, dumpStack, severityCode.error);
    }

    internal void setErr(String humanReadable, String error, String code, bool dumpStack, severityCode severity)
    {
      if (severity == severityCode.warning)
      {
        this.success = true;
      }
      else
      {
        this.success = false;
      }
      this.humanReadableError = humanReadable;
      this.detailedError = error;
      this.code = code;

      if (dumpStack)
      {
        var stackInfo = new StringBuilder();
        var stackTrace = new StackTrace(true);
        String indent = "";
        int counter = 0;
        foreach (var r in stackTrace.GetFrames())
        {
          // Skip the setErr() method
          if (counter++ == 0)
            continue;
          // Enough is enough
          if (counter > 6)
            break;
          stackInfo.
              Append(indent).
              Append(String.Format("Filename: {0} Method: {1} Line: {2} Column: {3}",
              r.GetFileName(), r.GetMethod(), r.GetFileLineNumber(),
              r.GetFileColumnNumber())).
              Append("\n\n");
          indent = indent + "  ";
        }
        this.trace = stackInfo.ToString();
      }
      else
      {
        trace = "";
      }
    }

    internal void setWarning(string warning, string code)
    {
      this.severity = severityCode.warning;
      this.warning = warning;
      this.code = code;
    }

    internal void setChecksumError(string warning, string code)
    {
      checksumError = true;
      this.warning = warning;
      this.code = code;
    }
  }
}
