﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace recon
{
  /// <summary>
  /// Class to hold the values that were configured with the new Import Options List (import_options_lists).
  /// Bug 17179.
  /// </summary>
  public class ImportSetting
  {
    public string localPath { get; set; }
    public string ftpPath { get; set; }
    public string fileFormat { get; set; }
    public string accountNumber { get; set; }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="localPath"></param>
    /// <param name="ftpPath"></param>
    /// <param name="fileFormat"></param>
    /// <param name="accountNumber"></param>
    public ImportSetting(string localPath, string ftpPath, string fileFormat, string accountNumber)
    {
      this.localPath = localPath;
      this.ftpPath = ftpPath;
      this.fileFormat = fileFormat;
      this.accountNumber = accountNumber;
    }
  }
}
