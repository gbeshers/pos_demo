﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace recon
{
    /// <summary>
    /// For Automatch first pass.
    /// Bug 11417 and 11422. Used to preserve (pickle?) original expected BANKID 
    /// and bank Reference number.
    /// Bug 22128: Pulled out of main class and added date for temporal proximity matching
    /// </summary>
    public class AmountBankIDBucket
    {
        public Decimal amount { get; set; }
        public string bankID { get; set; }
        public string originalRefNum { get; set; }
        public DateTime depositDate { get; set; }           // Bug 22128: Needed for temporal proximity matching

        public AmountBankIDBucket(Decimal amount, string bankID, string originalRefNum, DateTime depositDate)
        {
            this.amount = amount;
            this.bankID = bankID;
            this.originalRefNum = originalRefNum;
            this.depositDate = depositDate;   // Bug 22128: Needed for temporal proximity matching
        }
    }


}
