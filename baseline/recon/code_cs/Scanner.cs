﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CASL_engine;


namespace recon
{
  class Scanner
  {
            const String EOL = "/";
        const char DELIMITER = ',';

        int lineNumber = 0;
        int accountRecords = 0;
        int groupRecords = 0;
        int currentFieldNumber = 0;   // Bug 11489: Capture field number

        String currentOrginatorGroupBankID = "Error: no group set";
        String currentCustomerAccountNumber = "Error: no account set";

        // TODO: read totals from file, group, and account and make sure they are correct
        public List<List<Token>> lexicalScanBAI(string path, bool checkLineNumbers)
        {
            List<List<Token>> lexData = new List<List<Token>>();

            try  // Bug 11489: Catch any errors, log, and re-throw
            {
                using (System.IO.StreamReader readFile = new System.IO.StreamReader(path))
                {
                    String record;
                    String[] fields;

                    while ((record = readFile.ReadLine()) != null)
                    {
                        lineNumber++;
                        fields = record.Split(DELIMITER);
                        int fieldNumber = 0;
                        List<Token> fieldList = new List<Token>();
                        foreach (String field in fields)
                        {
                            fieldNumber++;
                            currentFieldNumber = fieldNumber;
                            Token token = new Token();
                            token.field = field;
                            token.lineNumber = lineNumber;
                            token.fieldNumber = fieldNumber;
                            fieldList.Add(token);
                        }

                        if (fieldList.Count == 0)  // protect against blank lines
                            continue;

                        checkLineCounts(path, fieldList, checkLineNumbers);

                        // Remove the pesky trailing "/"
                        removePeskyTrialingSlash(fieldList);


                        // Check for the tricky continuation record code
                        if (fields.First().Equals(RecordCodes.Continuation))  
                        {
                            processContinuationCode(lexData, fieldList);
                        }
                        else
                        {
                            lexData.Add(fieldList);
                        }
                    }
                }
            }
            catch (Exception e)
            {
              // Bug 11489: Catch any errors, log, and re-throw
              // I know that this style of re-throw loses the stack, but we log the stack here:
              Logger.cs_log(string.Format("Lexical Parsing error: Line: {0} Field: {1}  Error: {2}", lineNumber, currentFieldNumber, e.ToString()));
              throw new Exception(string.Format("Lexical Parsing error: Line: {0} Field: {1}  Error: {2}", lineNumber, currentFieldNumber, e.Message));
            }

            return lexData;
        }

        // Make sure the record counts are correct
        private void checkLineCounts(string path, List<Token> fieldList, bool checkLineNumbers)
        {
            Token typeCodeToken = fieldList.ElementAt(RecordCodes.recordCodeField);

            switch (typeCodeToken.field)
            {
                case RecordCodes.FileTrailer:
                    Token numRecordsToken = fieldList.ElementAt(RecordCodes.FileTrailerNumRecordsField);
                    String numRecordsString = removePeskyTrialingSlash(numRecordsToken.field);
                    int numRecs = Convert.ToInt32(numRecordsString);
                    if (numRecs != lineNumber)
                    {
                        if (checkLineNumbers) 
                        {
                            throw new Exception (String.Format(
                                "ERROR: The {0} BAI file is supposed to have {1} records but {2} were found",
                                            path, numRecs, lineNumber));
                        }

                    }
                    break;
                case RecordCodes.Group:
                    groupRecords = 1;       // Reset counter
                    Token originatorGroupBankToken = fieldList.ElementAt(RecordCodes.originatorGroupBankField);
                    currentOrginatorGroupBankID = originatorGroupBankToken.field;
                    break;
                case RecordCodes.GrpTrailer:
                    groupRecords++;
                    Token numGroupToken = fieldList.ElementAt(3);
                    String numGroupString = removePeskyTrialingSlash(numGroupToken.field);
                    int numGroups = Convert.ToInt32(numGroupString);
                    if (numGroups != groupRecords)
                    {
                        throw new Exception(String.Format(
                            "ERROR: The '{0}' group at line number {1} is supposed to have {2} records but {3} were found",
                            currentOrginatorGroupBankID,
                            lineNumber,
                            numGroups, groupRecords));
                    }
                    accountRecords = 0;     // Not in account state
                    groupRecords = 0;       // Not in a group state either
                    break;
                case RecordCodes.Account:
                    groupRecords++;         // Count all records in current group
                    accountRecords = 1;     // Reset counter
                    Token customerAccountNumberToken = fieldList.ElementAt(RecordCodes.customerAccountNumberField);
                    currentCustomerAccountNumber = customerAccountNumberToken.field;
                    break;
                case RecordCodes.Continuation:
                case RecordCodes.Trans:
                    groupRecords++;
                    accountRecords++;
                    break;
                case RecordCodes.AccTrailer:
                    groupRecords++;
                    accountRecords++;
                    Token numAccountToken = fieldList.ElementAt(RecordCodes.AccountTrailerNumRecordsField);
                    String numAccountString = removePeskyTrialingSlash(numAccountToken.field);
                    int numAccounts = Convert.ToInt32(numAccountString);
                    if (numAccounts != accountRecords)
                    {
                        throw new Exception(String.Format(
                            "ERROR: The {0} account ending at line {1} is supposed to have {2} records but {3} were found",
                            currentCustomerAccountNumber,
                            lineNumber,
                            numAccounts, accountRecords));
                    }
                    accountRecords = 0;  // Not in an account state anymore
                    break;
                default:
                    // Count erroneous or unrecognized lines also and let
                    // the parser straighten it out
                    if (groupRecords > 0)
                        groupRecords++;
                    if (accountRecords > 0)
                        accountRecords++;
                    break;

            }

        }

        private static void processContinuationCode(List<List<Token>> lexData, List<Token> fieldList)
        {
            // TODO: check for empty lexData and throw exception

            // add to previous line
            // First fetch previous record
            List<Token> previousRecord = lexData.Last();

            // Remove the 88 record code from the current record
            fieldList.RemoveAt(0);

            // Concatenate current record to end of previous
            previousRecord.AddRange(fieldList);
        }


        private static void removePeskyTrialingSlash(List<Token> fieldList)
        {
            Token lastToken = fieldList.Last();
            String lastFieldValue = lastToken.field;
            lastFieldValue = lastFieldValue.TrimEnd(); // Bug 11308
            if (lastFieldValue.EndsWith(EOL))
            {
                // Remove / in String
                lastFieldValue = removePeskyTrialingSlash(lastFieldValue);  // TODO check for 0-len strings
                // Get rid of last token in list with /
                fieldList.RemoveAt(fieldList.Count - 1);
                // Correct last token
                lastToken.field = lastFieldValue;
                // Append at the end of the list
                fieldList.Add(lastToken);
            }
        }

        private static string removePeskyTrialingSlash(String peskyString)
        {
          peskyString = peskyString.TrimEnd();  // Bug 11308
          return peskyString.Remove(peskyString.Length - 1);
        }

        [Obsolete]
        internal int getCurrentLineNumber()
        {
            return lineNumber;
        }
        
    }
  
}
