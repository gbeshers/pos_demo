﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using CASL_engine;


namespace recon
{
  /// <summary>
  /// Generate Automatch reports for both Text and CSV format reports.
  /// Bug 10262
  /// </summary>
  public class AutomatchReports
  {
    /// <summary>
    /// Type of action reported in CSV Automatch report.  Bug 10262.
    /// </summary>
    public enum MatchAction
    {
      match,
      match_key,    // Match found during second-pass during "Match Key" processing
      no_match,
      skip,         // Null slip number or similar problem
      error_key,    // Error during match-key processing
      error
    };

    /// <summary>
    /// Running list of actions of Automatch for the use of the CVS report
    /// </summary>
    public List<MatchInfo> m_matchList = null;

    /// <summary>
    /// Stream connection to text report
    /// </summary>
    private System.IO.StreamWriter textFile = null;


    /// <summary>
    /// Setup the report writer and clear list of report items
    /// </summary>
    public AutomatchReports()
    {
      // Clear global list for CSV report
      m_matchList = new List<MatchInfo>();

      // Make sure it is null so it is openned
      textFile = null;
    }

    /// <summary>
    /// Write the AutoMatch report to a file. 
    /// The location of the file will be the Reports subdirectory in 
    /// the "File Import Location" as specified in the config; 
    /// this is the same place that hold import files (tpyically BAI files).
    /// </summary>
    /// <param name="report"></param>
    public void writeAutomatchReport(ReconcileDeposits reconcileDeposits)
    {
      const String reportDirectory = "Reports";

      // Bug 11515
      //GenericObject reconGeo = (GenericObject)c_CASL.c_object("Business.bank_deposit_recon.data");
      //bool writeReport = (bool)reconGeo.get("enable_automatch_report", false);
      string writeReportString = reconcileDeposits.getConfigValue("enable_automatch_report", "false");
      bool writeReport = Convert.ToBoolean(writeReportString);
      // End 11515


      if (!writeReport)
        return;

      // Bug 11515
      //string reportFormat = (string)reconGeo.get("automatch_format", "Text");
      string reportFormat = reconcileDeposits.getConfigValue("automatch_format", "Text");

      //String importPath = (String)reconGeo.get("file_import_location", "");
      string importPath = reconcileDeposits.getConfigValue("file_import_location", "");
      // End 11515

      // Create the path to the Reports directory
      String reportPath = System.IO.Path.Combine(importPath, reportDirectory);

      // Create it if it does not exist
      if (!System.IO.Directory.Exists(reportPath))
      {
        System.IO.Directory.CreateDirectory(reportPath);
      }

      if (reportFormat == "CSV")
      {
        writeCSVReport(reportPath, m_matchList);
      }
      else
      {
        // The text report.  The file has already been written to; just close.
        if (textFile != null)
          textFile.Close();
        textFile = null;
      }

    }

    /// <summary>
    /// Write the automatch report in CSV format.  
    /// Scan the accumulated list of MatchInfo objects
    /// and write into a simple comma-separated file.
    /// </summary>
    /// <param name="reportPath"></param>
    private void writeCSVReport(String reportPath, List<MatchInfo> matchList)
    {
      String reportFullPath = ReconcileDeposits.createTimeStampedPath(reportPath, "AutoMatch.csv");

      matchList.Sort();

      using (StreamWriter theWriter = new StreamWriter(reportFullPath))
      {
        theWriter.WriteLine("Action, Account, Slip #, Amount, Details");
        foreach (MatchInfo match in matchList)
        {
          StringBuilder line = new StringBuilder();
          line.Append(match.action).Append(',')
            .Append(match.accountId).Append(',')
            .Append(match.slip).Append(',')
            .Append(match.amount).Append(',')
            .Append(match.comment);
          theWriter.WriteLine(line);
        }
      }
    }


    /// <summary>
    /// Collect automatch information for CSV report.
    /// Bug 10262.
    /// </summary>
    /// <param name="action"></param>
    /// <param name="accountId"></param>
    /// <param name="slip"></param>
    /// <param name="amount"></param>
    /// <param name="comment"></param>
    public void matchReport(MatchAction action, String accountId, String slip, Decimal? amount, String comment)
    {
      String actionString = "";
      switch (action)
      {
        case MatchAction.match:
          actionString = "MATCH";
          break;
        case MatchAction.match_key:
          // Match during second pass "match key" processing
          actionString = "MATCH_KEY";
          break;
        case MatchAction.no_match:
          actionString = "NO_MATCH";
          break;
        case MatchAction.skip:
          actionString = "SKIP";
          break;
        case MatchAction.error_key:
          // Error during the second-pass "match key" processing
          actionString = "ERROR_KEY";
          break;
        default:
          actionString = "ERROR";
          break;
      }
      var matchRecord = new MatchInfo(actionString, accountId, slip, amount, comment);
      m_matchList.Add(matchRecord);
    }

    /// <summary>
    /// Overloaded version of matchReport with a double value for the amount.
    /// Bug 10262.
    /// </summary>
    /// <param name="action"></param>
    /// <param name="accountId"></param>
    /// <param name="slip"></param>
    /// <param name="amount"></param>
    /// <param name="comment"></param>
    public void matchReport(MatchAction action, String accountId, String slip, double amount, String comment)
    {
      Decimal? decimalAmount = Convert.ToDecimal(amount);
      matchReport(action, accountId, slip, decimalAmount, comment);
    }


    /// <summary>
    /// Collect a Text report line and write it out to a file.
    /// Bug 11169.
    /// TODO: this has duplicated code from the write report method;
    /// need to unify this.
    /// </summary>
    /// <param name="list"></param>
    public void textReport(params object[] list)
    {
      if (textFile == null)
      {
        GenericObject reconGeo = (GenericObject)c_CASL.c_object("Business.bank_deposit_recon.data");
        bool writeReport = (bool)reconGeo.get("enable_automatch_report", false);

        if (!writeReport)
          return;

        string reportFormat = (string)reconGeo.get("automatch_format", "Text");
        if (reportFormat != "Text")
          return;

        const String fileName = "AutoMatchReport.txt";
        const String reportDirectory = "Reports";

        String importPath = (String)reconGeo.get("file_import_location", "");

        // Create the path to the Reports directory
        String reportPath = System.IO.Path.Combine(importPath, reportDirectory);

        // Create it if it does not exist
        if (!System.IO.Directory.Exists(reportPath))
        {
          System.IO.Directory.CreateDirectory(reportPath);
        }
        String reportFullPath = ReconcileDeposits.createTimeStampedPath(reportPath, fileName);
        textFile = new System.IO.StreamWriter(reportFullPath);
        textFile.WriteLine("AutoMatch Report\n" + DateTime.Now);
      }

      // Write line to text report file
      StringBuilder reportLine = new StringBuilder();
      foreach (object reportItem in list)
        reportLine.Append(reportItem);

      textFile.WriteLine(reportLine.ToString());
      textFile.Flush();                 // Bug 22128: Needed to flush each line
    }
  }
}
