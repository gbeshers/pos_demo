using System;
using System.Data;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Text;


using CASL_engine;
using ftp;

//bug 12018 NJ-disable compiler warnings.
#pragma warning disable 162
//end bug 12081
namespace recon
{
  public static class BankDepositCASL
  {
    private const bool m_debug = false;

    // MUST be instantiated with GEO args
    private static ReconcileDeposits rd = null;

    //private static String m_strConnectionClass = "";
    //private static String m_strConnectionDataSource = "";
    // Database specific variables 

    //private static db_vector_reader_sql m_vectorReader = null;


    /// <summary>
    /// Not thread safe!
    /// </summary>
    private static GenericObject m_dbConnection;

    private static int m_errorCounter = 1;

    static BankDepositCASL()
    {
      try
      {
        if (rd == null)
          rd = new ReconcileDeposits(
          "db_connection", c_CASL.c_object("TranSuite_DB.Data.db_connection_info"),
          "db_config_connection", c_CASL.c_object("TranSuite_DB.Config.db_connection_info")
          );
      }
      catch (Exception e)
      {
        Logger.cs_log("Bank Deposit Reconciliation Constructor Error (Exception thrown): " + e.Message + Environment.NewLine +
        e.StackTrace + Environment.NewLine +
        "Please correct the error, then restart the application if necessary");

        // Bug 17849
        //Logger.cs_log_trace("Bank Deposit Reconciliation Constructor Error (Exception thrown)", e);
        //"Please correct the error, then restart the application if necessary");
        throw;      // Re-throw and preserve stack
      }
    }

    public static GenericObject Connect(params object[] argPairs)
    {
      GenericObject geoParams = misc.convert_args(argPairs);
      m_dbConnection = (GenericObject)geoParams.get("db_connection");
      return new GenericObject();
      /*
      /*
      GenericObject db_connection = misc.convert_args(argPairs);
      m_strConnectionClass = (string)db_connection.get("_name", "", true);
      m_strConnectionDataSource = (string)db_connection.get("datasource_name", "");
      string strConnectionString = (string)db_connection.get("db_connection_string", "");

      System.Console.WriteLine(
      "m_strConnectionClass: {0}\tm_strConnectionDataSource: {1}\tstrConnectionString: {2}",
      m_strConnectionClass, m_strConnectionDataSource, strConnectionString);

      */
      //GenericObject geo = new GenericObject();
      //geo.Add("errorMessage", errorMessage);
      //return geo;

      /*
      db_connection.Add("_a", "Passed_args");
      return db_connection;
      * */
    }


    public static GenericObject TestRecon(params object[] argPairs)
    {
      try
      {
        GenericObject args = (GenericObject)argPairs[0];
        args.Add("bai_codes", "165, 301");
        //args.Add("import_file_type", "BAI");
        argPairs[0] = args;

        // Re-use here for all the error messages:
        GenericObject returnInfo = new GenericObject();

        int length = argPairs.Length;

        ReconcileDeposits recon = new ReconcileDeposits(argPairs);
        int count = 1;

        /*

        List<ReconciledDeposit> reconDeps;
        reconDeps = getReconciledDepositList(out status);
        dumpErrorInfo("A", returnInfo, status);
        foreach (ReconciledDeposit rd in reconDeps)
        {
        GenericObject rdGEO = ConvertToGEO(rd);
        returnInfo.Add("A: getReconciledDepositList: " + count++, rdGEO);  //A
        }


        count = 1;
        reconDeps = getReconciledDepositList(new DateTime(2010, 7, 16), out status);
        dumpErrorInfo("B", returnInfo, status);
        foreach (ReconciledDeposit rd in reconDeps)
        {
        GenericObject rdGEO = ConvertToGEO(rd);
        returnInfo.Add("B: getReconciledDepositList(asOfDate): " + count++, rdGEO);   // B
        }

        count = 1;
        ReconciledDepositDetails details = getReconciliationDetails(new DatabaseID(3), out status);
        dumpErrorInfo("C", returnInfo, status);
        GenericObject rd2GEO = ConvertToGEO(details);
        returnInfo.Add("C: getReconciliationDetails: " + count++, rd2GEO);  // C


        count = 1;
        List<BankDeposit> banks = getBankDepositList(out status);
        dumpErrorInfo("D", returnInfo, status);
        foreach (BankDeposit bank in banks)
        {
        GenericObject geo = ConvertToGEO(bank);
        returnInfo.Add("D: getBankDepositList: " + count++, geo);
        }


        count = 1;
        List<ExpectedDeposit> expected = getExpectedDepositList(out status);
        dumpErrorInfo("E", returnInfo, status);
        foreach (ExpectedDeposit ed in expected)
        {
        GenericObject geo = ConvertToGEO(ed);
        returnInfo.Add("E: getExpectedDepositList: " + count++, geo);
        }


        //List<ExpectedDeposit> expected = getExpectedDepositList(new DateTime());//////wrong


        List<DatabaseID> expectedDeposits = new List<DatabaseID>() {
        new DatabaseID("TSFD20081215", 2, 40),
        new DatabaseID("TSFD20081215", 1, 39)
        };
        List<DatabaseID> bankDeposits = new List<DatabaseID>() {
        new DatabaseID("10"),
        new DatabaseID("13"),
        new DatabaseID("12"),
        new DatabaseID("11")
        };
        reconcileForced(expectedDeposits, bankDeposits, "Frederick", 0.50M, "Testing forced", out status);
        dumpErrorInfo("F", returnInfo, status);

        reconcileExpectedIgnore(new DatabaseID("TSFD20090413", 1, 41), "Frederick", "Testing expected ignored", out status);
        dumpErrorInfo("G", returnInfo, status);

        reconcileBankIgnore(new DatabaseID(14), "Frederick", "Testing bank ignored", out status);
        dumpErrorInfo("H", returnInfo, status);

        reconciliationUndo(new DatabaseID("1"), out status);
        dumpErrorInfo("I", returnInfo, status);
        reconciliationUndo(new DatabaseID("9"), out status);
        dumpErrorInfo("I-1", returnInfo, status);
        reconciliationUndo(new DatabaseID("0"), out status);
        dumpErrorInfo("I-2", returnInfo, status);
        reconciliationUndo(new DatabaseID("-1231423"), out status);
        dumpErrorInfo("I-3", returnInfo, status);

        reconDeps = getReconciledDepositList(out status);
        dumpErrorInfo("J", returnInfo, status);
        count = 1;
        foreach (ReconciledDeposit rd in reconDeps)
        {
        GenericObject rdGEO = ConvertToGEO(rd);
        returnInfo.Add("J: getReconciledDepositList: " + count++, rdGEO);
        }

        reconDeps = getReconciledDepositList(out status);
        dumpErrorInfo("K", returnInfo, status);
        int reconCount = 1;
        foreach (ReconciledDeposit rd in reconDeps)
        {
        GenericObject rdGEO = ConvertToGEO(rd);
        returnInfo.Add("K: Before reconciled list[" + reconCount++ + "]", rdGEO);
        }


        List<ReconciledDeposit> reconList = autoMatch(out status);
        dumpErrorInfo("L", returnInfo, status);
        count = 1;
        foreach (ReconciledDeposit rd in reconList)
        {
        GenericObject rdGEO = ConvertToGEO(rd);
        returnInfo.Add("L: Reconciling this record " + count++, rdGEO);
        }

        reconDeps = getReconciledDepositList(out status);
        dumpErrorInfo("M", returnInfo, status);
        foreach (ReconciledDeposit rd in reconDeps)
        {
        GenericObject rdGEO = ConvertToGEO(rd);
        returnInfo.Add("M: After: reconciled list[" + reconCount++ + "]", rdGEO);
        }

        ReconciledDepositDetails depDetails = getReconciliationDetails(new DatabaseID(2), out status);
        dumpErrorInfo("N", returnInfo, status);
        GenericObject bankGeo = ConvertToGEO(depDetails);
        returnInfo.Add("N: reconcilation details for 2", bankGeo);

        count = 1;
        List<String> accounts = getExpectedBankAccounts(out status);
        dumpErrorInfo("O", returnInfo, status);
        foreach (String account in accounts)
        {
        returnInfo.Add("O: expected account: " + count++, account);
        }

        count = 1;
        var bankAccounts = getBankDepositAccounts(out status);
        dumpErrorInfo("P", returnInfo, status);
        foreach (String account in bankAccounts)
        {
        returnInfo.Add("P: actual account: " + count++, account);
        }

        count = 1;
        List<String> files = readBankFiles("D:\\My Documents\\Bank Recon\\BAIReader\\data", out status);
        dumpErrorInfo("Q", returnInfo, status);
        foreach (String file in files)
        {
        returnInfo.Add("Q: found file: " + count++, file);
        }
        */

        count = 1;
        List<ReconStatus> statusList;
        long start = DateTime.Now.Ticks;
        HashSet<int> baiCodesSet = new HashSet<int>();
        HashSet<int> withdramalCodesSet = new HashSet<int>();
        HashSet<string> emptySet = new HashSet<string>();
        List<String> importedFiles = recon.importBankFiles("D:\\My Documents\\Bank Recon\\BAIReader\\data", "bai", "remote-path", baiCodesSet, withdramalCodesSet, new HashSet<int>(), false, emptySet, "testuser", "testBankAccount", baiCodesSet, "", "", false, "", out statusList);
        long end = DateTime.Now.Ticks;
        long elapsedTicks = end - start;
        TimeSpan elapsedSpan = new TimeSpan(elapsedTicks);

        returnInfo.Add("R: parsed files in this many millseconds: ", elapsedSpan.Milliseconds);
        dumpErrorInfo("R: returned status", returnInfo, statusList);
        foreach (String file in importedFiles)
        {
          returnInfo.Add("R: returned file list: " + count++, file);
        }

        /*
        testTrace(out status);
        dumpErrorInfo("S", returnInfo, status);
        returnInfo.Add("S: test trace: ", status.trace);

        count = 1;
        BankDepositDetails bankDetails = getBankDepositDetails(new DatabaseID(15), out status);
        dumpErrorInfo("T", returnInfo, status);
        returnInfo.Add("T: " + count++, bankDetails.account);
        returnInfo.Add("T: " + count++, bankDetails.age);
        returnInfo.Add("T: " + count++, bankDetails.amount);
        returnInfo.Add("T: " + count++, bankDetails.databaseID);
        returnInfo.Add("T: " + count++, bankDetails.depDate);
        returnInfo.Add("T: " + count++, bankDetails.depositStatus);
        returnInfo.Add("T: " + count++, bankDetails.refNum);
        returnInfo.Add("T: " + count++, bankDetails.text);

        count = 1;
        DatabaseID id = new DatabaseID("TSFD20081215", 1, 39);
        ExpectedDepositDetails expectedDetails = getExpectedDepositDetails(id, out status);
        dumpErrorInfo("U", returnInfo, status);
        returnInfo.Add("U: " + count++, expectedDetails.account);
        returnInfo.Add("U: " + count++, expectedDetails.age);
        returnInfo.Add("U: " + count++, expectedDetails.amount);
        returnInfo.Add("U: " + count++, expectedDetails.databaseID);
        returnInfo.Add("U: " + count++, expectedDetails.depDate);
        returnInfo.Add("U: " + count++, expectedDetails.refNum);
        */
        return returnInfo;
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in API test", e);

        GenericObject errGeo = new GenericObject();
        errGeo.Add("Error in API test", e.ToString());
        errGeo.Add("message", e.Message);
        errGeo.Add("data", e.Data);
        errGeo.Add("inner", e.InnerException);
        errGeo.Add("source", e.Source);
        return errGeo;
      }
    }

    private static void dumpErrorInfo(String id, GenericObject returnInfo, List<ReconStatus> statusList)
    {
      foreach (ReconStatus status in statusList)
      {
        dumpErrorInfo(id, returnInfo, status);
      }
    }

    private static void dumpErrorInfo(String id, GenericObject returnInfo, ReconStatus status)
    {
      if (status.severity == ReconStatus.severityCode.warning)
      {
        returnInfo.Add(id + ": " + m_errorCounter++ + ": Warning", status.warning);
        returnInfo.Add(id + ": " + m_errorCounter++ + ": Warning code", status.code);
        return;
      }

      if (status.success)
      {
        returnInfo.Add(id + ": " + m_errorCounter++, "No error: " + status.humanReadableError + " comment: " + status.comments);
        return;
      }

      returnInfo.Add(id + ": " + m_errorCounter++ + ": ERROR: Human-readable ", status.humanReadableError);
      returnInfo.Add(id + ": " + m_errorCounter++ + ": ERROR: detailed ", status.detailedError);
      returnInfo.Add(id + ": " + m_errorCounter++ + ": ERROR: traceback ", status.trace);
      returnInfo.Add(id + ": " + m_errorCounter++ + ": ERROR: code ", status.code);

    }

    public static GenericObject AutoMatch(params object[] args)
    {
      System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
      if (m_debug)
      {
        watch.Start();
      }

      // Bug 9778: FT: changed to pass in the real user id
      GenericObject geo_args = misc.convert_args(args);
      string user = geo_args.get("user", null).ToString();
      // Bug 10250: FT: Get the match keys and regex so that automatch 
      // translates the Expected DeptIDs and can extract the match key.
      GenericObject matchKeys = geo_args.get("dept_match_key") as GenericObject;
      string recon_regex = (string)((GenericObject)c_CASL.c_object("Business.bank_deposit_recon.data")).get("recon_regex", "");

      ReconStatus status;

      List<ReconciledDeposit> reconDeposits = new List<ReconciledDeposit>();

      try
      {
        // Bug 10250: FT: pass in match keys and regex so that automatch can use them
        reconDeposits = rd.autoMatch(user, matchKeys, recon_regex, out status);
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in AutoMatch", e);

        string msg = "AutoMatch Error (Exception thrown): " + e.Message;
        //Logger.cs_log(msg + Environment.NewLine +e.StackTrace);
        return misc.MakeCASLError("AutoMatch Error", "REC-207", msg);
      }

      if (!status.success)
      {
        Logger.cs_log("AutoMatch Error (" + status.code + "): " + status.detailedError);
        return MakeCASLError("AutoMatch Error", status);
      }

      // Transform the reconciled deposit list into GEOs
      GenericObject reconciledDepositsGEO = new GenericObject();
      foreach (ReconciledDeposit bd in reconDeposits)
      {
        GenericObject bdGEO = ConvertToGEO(bd);
        reconciledDepositsGEO.insert(bdGEO);
      }

      if (m_debug)
      {
        watch.Stop();
        Logger.cs_log("Automatch took this many Milliseconds: " + watch.ElapsedMilliseconds);
      }
      return reconciledDepositsGEO;
    }



    // Get a GEO that contains all the unreconciled expected deposits
    [Obsolete("Has been superceded by the DataTables ready GetExpectedDepositsAsJSArray", true)]
    public static GenericObject GetExpectedDeposits(params object[] args)
    {
      System.Diagnostics.Stopwatch watch;
      if (m_debug)
      {
        watch = new System.Diagnostics.Stopwatch();
        watch.Start();
      }

      // Bug 10250: FT: changed to pass in the workgroup ID --> match key mappings to shown in Details dropdown
      GenericObject geo_args = misc.convert_args(args);
      GenericObject matchKeys = geo_args.get("dept_match_key") as GenericObject;

      // Bug 11479: Pass bank account number in to filter on
      string bank_account_filter = geo_args.get("bank_account", "") as string;

      // Bug 11426 Extension to filter on the work group to cut down on list size
      string workgroup_filter
      = geo_args.get("workgroup_id", "") as string;

      GenericObject deposits = new GenericObject();

      // Retrieve the list of deposits
      ReconStatus status;
      List<ExpectedDeposit> expectedDeposits = new List<ExpectedDeposit>();

      try
      {
        // Bug 11411: Added matchKeys so we can add this to reference num field if blank
        expectedDeposits = rd.getExpectedDepositList(bank_account_filter, matchKeys, workgroup_filter, out status);
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in GetExpectedDeposites", e);
        string msg = "GetExpectedDeposits Error (Exception thrown): " + e.Message;
        //Logger.cs_log(msg + Environment.NewLine + e.StackTrace);
        return misc.MakeCASLError("Error Reading Expected Deposits", "REC-208", msg);
      }

      if (!status.success)
      {
        Logger.cs_log("GetExpectedDeposits Error (" + status.code + "): " + status.detailedError);
        return MakeCASLError("Error Reading Expected Deposits", status);
      }

      // Transform the bank deposit list into GEOs
      GenericObject expectedDepositsGEO = new GenericObject();
      foreach (ExpectedDeposit ed in expectedDeposits)
      {
        ExpectedDepositDetails edd = null;

        try
        {
          edd = rd.getExpectedDepositDetails(ed.databaseID, matchKeys, out status);
          edd.depDate = ed.depDate; // Super hack for Bug 11473: Need to fix getExpectedDepositDetails to pull effective date instead of this hack!
          edd.refNum = ed.refNum;   // Super hack for Bug 11411; do not let this field get overwritten!
        }
        catch (Exception e)
        {
          // Bug 17849
          Logger.cs_log_trace("Error in GetExpectedDepositDetails", e);

          string msg = "GetExpectedDepositDetails Error (Exception thrown): " + e.Message;
          //Logger.cs_log(msg + Environment.NewLine + e.StackTrace);
          return misc.MakeCASLError("Canot Read Expected Deposit Details", "REC-209", msg);
        }

        if (!status.success)
        {
          Logger.cs_log("GetExpectedDepositDetails Error (" + status.code + "): " + status.detailedError);
          return MakeCASLError("Canot Read Expected Deposit Details", status);
        }

        GenericObject eddGEO = ConvertToGEO(edd);
        expectedDepositsGEO.insert(eddGEO);
      }

      if (m_debug)
      {
        watch.Stop();
        Logger.cs_log("GetExpectedDeposits: Expected list query took this many Milliseconds: " + watch.ElapsedMilliseconds);
      }
      return expectedDepositsGEO;
    }

    /// <summary>
    /// Get the full detailed record for a single bank deposit.
    /// </summary>
    /// <param name="database_id">String version of the database identifier</param>
    /// <returns>GEO containing each fields in the detail record</returns>
    public static GenericObject GetBankDepositDetails(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      String database_id = (string)geo_args.get("database_id", "");

      var databaseID = new DatabaseID(database_id);
      ReconStatus status;
      BankDepositDetails bankDepositDetails = rd.getBankDepositDetails(databaseID, out status);
      if (!status.success)
        return MakeCASLError("Error reading Bank Deposit Details", status);

      return ConvertToGEO(bankDepositDetails);
    }

    /// <summary>
    /// Get the detailed information for an expected deposit.
    /// </summary>
    /// <param name="args">The database_id as a string</param>
    /// <returns>A GenericObject containing all the fields in an expected deposit</returns>
    public static GenericObject GetExpectedDepositDetails(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      String database_id = (string)geo_args.get("database_id", "");
      GenericObject dept_match_key = (GenericObject)geo_args.get("dept_match_key", new GenericObject());
      GenericObject dept_names = (GenericObject)geo_args.get("dept_names", new GenericObject());

      var databaseID = new DatabaseID(database_id);
      ReconStatus status;
      ExpectedDepositDetails expectedDepositDetails = rd.getExpectedDepositDetails(databaseID, dept_match_key, out status);
      if (!status.success)
        return MakeCASLError("Error reading Expected Deposit Details", status);

      return ConvertToGEO(expectedDepositDetails, dept_names);
    }


    // Get a GEO that contains all the unreconciled bank deposits
    public static GenericObject GetBankDeposits(params object[] args)
    {
      // Bug 11473: Pass in bank account number so it can be filtered on server side
      GenericObject geo_args = misc.convert_args(args);
      string bank_account = geo_args.get("bank_account", "") as string;
      // End Bug 11473

      System.Diagnostics.Stopwatch watch;
      if (m_debug)
      {
        watch = new System.Diagnostics.Stopwatch();
        watch.Start();
      }
      GenericObject deposits = new GenericObject();

      // Retrieve the list of deposits
      ReconStatus status;
      List<BankDeposit> bankDeposits = new List<BankDeposit>();

      try
      {
        // Bug 11473: Pass in bank account number so it can be filtered on server side
        if (bank_account == "")
          bankDeposits = rd.getBankDepositList(out status);
        else
        {
          var bankAccounts = new List<String>();
          bankAccounts.Add(bank_account);
          bankDeposits = rd.getBankDepositList(bankAccounts, out status);
        }
        // End Bug 11473
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in GetBankDepositList", e);
        string msg = "GetBankDepositList Error (Exception thrown): " + e.Message;
        //Logger.cs_log(msg + Environment.NewLine + e.StackTrace);
        return misc.MakeCASLError("Error Reading Bank Deposit List", "REC-210", msg);
      }


      if (!status.success)
      {
        Logger.cs_log("GetBankDepositList Error (" + status.code + "): " + status.detailedError);
        return MakeCASLError("Error Reading Bank Deposit List", status);
      }

      // Transform the bank deposit list into GEOs
      GenericObject bankDepositsGEO = new GenericObject();

      foreach (BankDeposit bd in bankDeposits)
      {
        BankDepositDetails bdd = new BankDepositDetails();

        try
        {
          bdd = rd.getBankDepositDetails(bd.databaseID, out status);
        }
        catch (Exception e)
        {
          // Bug 17849
          Logger.cs_log_trace("Error in GetBankDepositDetails", e);
          string msg = "GetBankDepositDetails Error (Exception thrown): " + e.Message;
          //Logger.cs_log(msg + Environment.NewLine +e.StackTrace);
          return misc.MakeCASLError("Error Reading Bank Deposit Details", "REC-211", msg);
        }

        if (!status.success)
        {
          Logger.cs_log("GetBankDepositDetails Error (" + status.code + "): " + status.detailedError);
          return MakeCASLError("Error Reading Bank Deposit Details", status);
        }

        GenericObject bddGEO = ConvertToGEO(bdd);

        bankDepositsGEO.insert(bddGEO);
      }

      if (m_debug)
      {
        watch.Stop();
        Logger.cs_log("Bank list query took this many Milliseconds: " + watch.ElapsedMilliseconds);
      }
      return bankDepositsGEO;
    }

    /// <summary>
    /// Get a GEO that contains all the reconciled deposits
    /// Switch to do date filtering on the server side and support
    /// start/end dates.
    /// Bug 11451
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    [System.Obsolete("Use GetReconciledDepositsAsJSArray if possible (requires a DataTable on the client side)")]
    public static GenericObject GetReconciledDeposits(params object[] args)
    {
      // Bug 11451
      GenericObject geo_args = misc.convert_args(args);
      string startDate = geo_args.get("start_date", "") as string;
      string endDate = geo_args.get("end_date", "") as string;
      // End 11451
      System.Diagnostics.Stopwatch watch;
      if (m_debug)
      {
        watch = new System.Diagnostics.Stopwatch();
        watch.Start();
      }

      GenericObject deposits = new GenericObject();

      // Retrieve the list of deposits
      ReconStatus status;
      List<ReconciledDeposit> reconciledDeposits;
      try
      {
        // Bug 11451
        var matchKeys = new GenericObject();   // This an obsolete method so skip the match key stuff
        reconciledDeposits = rd.getReconciledDepositList(startDate, endDate, matchKeys, out status);
      }

      catch (Exception e)
      {
        string msg = "GetReconciledDeposits Error (Exception thrown): " + e.Message;
        Logger.cs_log(msg + Environment.NewLine + e.StackTrace);
        return misc.MakeCASLError("Error Reading Reconciled Deposits", "REC-212", msg);
      }

      if (!status.success)
      {
        Logger.cs_log("GetReconciledDeposits Error (" + status.code + "): " + status.detailedError);
        return MakeCASLError("Error Reading Reconciled Deposits", status);
      }

      // Transform the bank deposit list into GEOs
      GenericObject reconciledDepositsGEO = new GenericObject();
      foreach (ReconciledDeposit bd in reconciledDeposits)
      {
        GenericObject bdGEO = ConvertToGEO(bd);
        reconciledDepositsGEO.insert(bdGEO);
      }

      if (m_debug)
      {
        watch.Stop();
        Logger.cs_log("Reconciled list query took this many Milliseconds: " + watch.ElapsedMilliseconds);
      }
      return reconciledDepositsGEO;
    }

    /// <summary>
    /// Interface into Reconciled List for Reporting
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static GenericObject GetReconciledDepositListForReporting(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      DateTime startDate = (DateTime)((GenericObject)geo_args.get("start_date")).get("__native_obj");
      DateTime endDate = (DateTime)((GenericObject)geo_args.get("end_date")).get("__native_obj");

      // Bug: 9717: FT
      // CASL always sets the time portion of the datetime to 12:00 AM.
      // Round up the enddate make these queries cover the full day.
      endDate = endDate.AddHours(24.0);
      endDate = endDate.AddMilliseconds(-1.0);

      // Bug 10040: FT: Get the real user list 
      String user = (String)geo_args.get("users", "");
      List<string> users = new List<string>();
      users.Add(user);
      // End Bug 10040

      string strStatus = (string)geo_args.get("status", "");
      // Bug 26165 Change from db_connection to db_connection_info 
      // because of the error CASL_engine.GenericObject' to type 'System.String'
      string conString = (string)geo_args.get("db_connection_info", "");    // Bug 26046/25849 UMN 
      ReconciledDeposit.rStatus status = ReconciledDeposit.rStatus.unset;
      if (strStatus.Equals("reconciled"))
        status = ReconciledDeposit.rStatus.reconciled;
      else if (strStatus.Equals("voided"))
        status = ReconciledDeposit.rStatus.voided;

      GenericObject geoTypes = (GenericObject)geo_args.get("type", new GenericObject());
      List<ReconciledDeposit.rType> types = new List<ReconciledDeposit.rType>();
      foreach (object type in geoTypes.vectors)
      {
        string strType = (string)type;
        if (strType.Equals("auto"))
          types.Add(ReconciledDeposit.rType.auto);
        if (strType.Equals("match_key"))
          types.Add(ReconciledDeposit.rType.matchkey);    // Bug 10324. Make sure that the list gets the new match key type
        else if (strType.Equals("manual"))
          types.Add(ReconciledDeposit.rType.manual);
        else if (strType.Equals("forced"))
          types.Add(ReconciledDeposit.rType.forced);
        else if (strType.Equals("ignored"))
          types.Add(ReconciledDeposit.rType.ignored);
      }

      if (types.Count == 0)
        types = null;

      GenericObject matchKeys = geo_args.get("dept_match_key") as GenericObject;  // Bug 11549

      string workgroupID = (string)geo_args.get("workgroup_id", "");     // Bug 15157

      string groupBy = (string)geo_args.get("group_by", "");             // Bug 15157: Sorting option

      ReconStatus reconStatus;
      List<ReconciledDeposit> deposits = new List<ReconciledDeposit>();

      try
      {
        // Bug 15157: Added workgroup filter and return the Workgroup and File information
        bool extraFields = true;        // Include workgroup and file information
        deposits = rd.getReconciledDepositListForReporting(startDate, endDate, users, status, types, matchKeys, workgroupID, extraFields, groupBy, out reconStatus);
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in GetReconciledDepositListForReporting", e);
        string msg = "GetReconciledDepositListForReporting Error (Exception thrown): " + e.Message;
        //Logger.cs_log(msg + Environment.NewLine +e.StackTrace);
        return misc.MakeCASLError("Error Reading Reconciled List", "REC-213", msg);
      }

      if (!reconStatus.success)
      {
        Logger.cs_log("GetReconciledDepositListForReporting Error (" + reconStatus.code + "): " + reconStatus.detailedError);
        return MakeCASLError("Error Reading Reconciled List", reconStatus);
      }

      GenericObject geoDeposits = new GenericObject();
      foreach (ReconciledDeposit deposit in deposits)
        geoDeposits.insert(ConvertToGEO(deposit));

      return geoDeposits;
    }

    // Get a GEO that contains all the reconciled deposit details
    public static GenericObject GetReconciledDepositDetails(params object[] args)
    {
      try
      {
        GenericObject geo_args = misc.convert_args(args);
        // Bug #12055 Mike O - Use misc.Parse to log errors
        DatabaseID id = new DatabaseID(misc.Parse<Int32>(geo_args.get("id")));
        GenericObject matchKeys = geo_args.get("dept_match_key") as GenericObject;  // Bug 11549

        ReconStatus status;
        ReconciledDepositDetails details = null;

        try
        {
          details = rd.getReconciliationDetails(id, matchKeys, out status);   // Bug 11549: passed in matchkey geo so that we can display match keys
        }
        catch (Exception e)
        {
          // Bug 17849
          Logger.cs_log_trace("Error in GetReconciledDepositDetails", e);
          string msg = "GetReconciledDepositDetails Error (Exception thrown): " + e.Message;
          //Logger.cs_log(msg + Environment.NewLine + e.StackTrace);
          return misc.MakeCASLError("Error Reading Reconciled Details", "REC-214", msg);
        }

        if (!status.success)
        {
          Logger.cs_log("GetReconciledDepositDetails Error (" + status.code + "): " + status.detailedError);
          return MakeCASLError("Error Reading Reconciled Details", status);
        }

        return ConvertToGEO(details);
      }
      catch (Exception e)
      {
        // Bug 17849
        //Logger.cs_log_trace("Error in GetReconciledDepositDetails", e);
        string msg = "GetReconciledDepositDetails Error (Exception thrown): " + e.ToString();
        Logger.cs_log(msg);
        throw CASL_error.create("title", "Error Reconciled Details", "code", "REC-220", "message", msg);
      }
    }

    // Pass in databaseIDs + over/short and reason, and perform the correct reconciliation
    public static GenericObject ReconcileDeposits(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      GenericObject eIds = (GenericObject)geo_args.get("expected_ids", new GenericObject());
      GenericObject bIds = (GenericObject)geo_args.get("bank_ids", new GenericObject());

      string user = geo_args.get("user", null).ToString();
      // Bug #12055 Mike O - Use misc.Parse to log errors
      decimal overShort = misc.Parse<Decimal>(geo_args.get("over_short", 0));
      string reason = geo_args.get("reason", null).ToString();

      List<DatabaseID> eList = new List<DatabaseID>();
      List<DatabaseID> bList = new List<DatabaseID>();

      foreach (object eId in eIds.vectors)
        eList.Add(new DatabaseID((string)eId));

      foreach (object bId in bIds.vectors)
        bList.Add(new DatabaseID((string)bId));

      if (eList.Count == 0)
      {
        foreach (DatabaseID id in bList)
        {
          ReconStatus status;

          try
          {
            rd.reconcileBankIgnore(id, user, reason, out status);
          }
          catch (Exception e)
          {
            // Bug 17849
            Logger.cs_log_trace("Error in reconcileBankIgnore", e);
            string msg = "reconcileBankIgnore Error (Exception thrown): " + e.Message;
            //Logger.cs_log(msg + Environment.NewLine + e.StackTrace);
            return misc.MakeCASLError("Error Reconciling Deposit", "REC-215", msg);
          }

          if (!status.success)
          {
            Logger.cs_log("ReconcileBankIgnore Error (" + status.code + "): " + status.detailedError);
            return MakeCASLError("Error Reconciling Deposit", status);
          }
        }
      }
      else if (bList.Count == 0)
      {
        foreach (DatabaseID id in eList)
        {
          ReconStatus status;

          try
          {
            rd.reconcileExpectedIgnore(id, user, reason, out status);
          }
          catch (Exception e)
          {
            // Bug 17849
            Logger.cs_log_trace("Error in ReconcileExpectedIgnore", e);
            string msg = "ReconcileExpectedIgnore Error (Exception thrown): " + e.Message;
            //Logger.cs_log(msg + Environment.NewLine + e.StackTrace);
            return misc.MakeCASLError("Error Reconciling Deposit", "REC-216", msg);
          }

          if (!status.success)
          {
            Logger.cs_log("ReconcileExpectedIgnore Error (" + status.code + "): " + status.detailedError);
            return MakeCASLError("Error Reconciling Deposit", status);
          }
        }
      }
      else
      {
        ReconStatus status;

        try
        {
          // Bug: 9575:  Thurber:  Formally all of these were be processed as forced
          // Bug: 9768: FT: Make sure that overages are also considered forced
          if (overShort != 0.00M)
            rd.reconcileForced(eList, bList, user, overShort, reason, out status);
          else
            // Bug 9695: FT: Store the reason on manual recons
            rd.reconcileManual(eList, bList, user, reason, out status);
        }
        catch (Exception e)
        {
          // Bug 17849
          Logger.cs_log_trace("Error in ReconcileForced", e);
          string msg = "ReconcileForced Error (Exception thrown): " + e.Message;
          //Logger.cs_log(msg + Environment.NewLine + e.StackTrace);
          return misc.MakeCASLError("Error Creating Forced Reconciliation", "REC-217", msg);
        }

        if (!status.success)
        {
          Logger.cs_log("ReconcileForced Error (" + status.code + "): " + status.detailedError);
          return MakeCASLError("Error Creating Forced Reconciliation", status);
        }
      }

      return null;
    }

    /// <summary>
    /// Retrieve all the unique bank accounts referenced in the 
    /// expected and actual bank deposits.  Return in a GEO
    /// 
    /// Called by bank_deposit_recon_f_controller.casl: get_all_accounts 
    /// which is called by the open deposit htm_inst to gather all 
    /// the unique bank accounts for the bank account dropdown filter.
    /// </summary>
    /// <returns></returns>
    public static GenericObject GetAllAccounts(params Object[] args)
    {
      ReconStatus status;
      List<string> accounts = new List<string>();

      try
      {
        accounts = rd.getAllBankAccounts(out status);
      }
      catch (Exception e)
      {
        // Bug 17849
        //Logger.cs_log_trace("Error in getAllBankAccounts", e);
        string msg = "getAllBankAccounts Error (Exception thrown): " + e.Message;
        Logger.cs_log(msg + Environment.NewLine + e.StackTrace);
        // Bug 10965: FT: Returning a CASL error does not work here; stronger measures are needed
        throw CASL_error.create("title", "Error Reading Bank Accounts", "code", "REC-205", "message", "GetAllAccounts Error (Exception thrown): " + e.Message);
      }

      if (!status.success)
      {
        Logger.cs_log("getAllBankAccounts Error (" + status.code + "): " + status.detailedError);
        // Bug 10965: FT: Returning a CASL error does not work here; stronger measures are needed
        throw CASL_error.create("title", "Error Reading Bank Accounts", "code", status.code, "message", status.humanReadableError + ".  SQL or conversion error.  Check CS log for details.");
      }

      GenericObject geoAccounts = new GenericObject();
      foreach (string account in accounts)
        geoAccounts.insert(account);

      return geoAccounts;
    }

#if deadcode
    // Get a GEO that contains all the bank account names
    [Obsolete("Use GetAllAccounts.  This does not appear to be used.  Called by bank_deposit_recon_f_controller.casl's get_bank_accounts(), but that does not appear to be called")]
    public static GenericObject GetBankAccounts(params Object[] args)
    {
      ReconStatus status;
      List<string> accounts = new List<string>();

      try
      {
        accounts = rd.getBankDepositAccounts(out status);
      }
      catch (Exception e)
      {
        string msg = "GetBankAccounts Error (Exception thrown): " + e.Message;
        Logger.cs_log(msg + Environment.NewLine +
        e.StackTrace);
        return misc.MakeCASLError("Error Reading Bank Accounts", "REC-218", msg);
      }

      if (!status.success)
      {
        Logger.cs_log("GetBankAccounts Error (" + status.code + "): " + status.detailedError);
        return MakeCASLError("Error Reading Bank Accounts", status);
      }

      GenericObject geoAccounts = new GenericObject();
      foreach (string account in accounts)
        geoAccounts.insert(account);

      return geoAccounts;
    }
#endif    

    // Bug: 9808: FT: Added checksums to bank rec code
    public static GenericObject ValidateBankRecChecksums()
    {
      ReconStatus status;
      GenericObject geo = new GenericObject();

      rd.validateChecksums(out status);

      if (!status.success)
      {
        geo.Add("Bank Reconciliation checksums did NOT validated correctly", status.detailedError);
      }
      else
      {
        geo.Add("Bank Reconciliation account checksums validated correctly", status.comments);
      }

      return geo;
    }

    /// <summary>
    /// Bug: 9808: FT: Added checksums to bank rec code
    /// Upgrade / fix all checksums in bank rec database.  Take this out of production code.
    /// </summary>
    /// <returns></returns>
    /* Bug 9937: FT: remove this from production code
    public static GenericObject UpdateAllBankRecChecksums()
    {
    ReconStatus status;
    GenericObject geo = new GenericObject();

    rd.updateAllChecksums(out status);

    if (!status.success)
    {
    geo.Add("Bank Reconciliation updated.  These checksums were found to be invalid", status.detailedError);
    }
    else
    {
    geo.Add("Bank Reconciliation account checksums updated correctly", status.comments);
    }

    return geo;
    }
    */



    public static GenericObject VoidReconciledDeposits(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      GenericObject ids = (GenericObject)geo_args.get("ids", new GenericObject());

      GenericObject voided = new GenericObject();
      GenericObject failed = new GenericObject();
      string user = geo_args.get("user", "").ToString();


      foreach (object id in ids.vectors)
      {
        ReconStatus status;
        DatabaseID dID = new DatabaseID((string)id);

        ReconciledDeposit deposit = rd.getReconciliationDetails(dID, new GenericObject(), out status);

        string errorMessage = null;

        if (!status.success)
        {
          errorMessage = "ReconciliationUndo Error (" + status.code + "): " + status.detailedError;
          Logger.cs_log(errorMessage);
          failed.insert(deposit.refNum);
        }

        try
        {
          rd.reconciliationUndo(dID, out status);
        }
        catch (Exception e)
        {
          // Bug 17849
          Logger.cs_log_trace("Error in ReconciliationUndo", e);
          //errorMessage = "ReconciliationUndo Error (Exception thrown): " + e.Message + Environment.NewLine + e.StackTrace;
          //Logger.cs_log(errorMessage);
          failed.insert(deposit.refNum);

        }

        if (!status.success)
        {
          errorMessage = "ReconciliationUndo Error (" + status.code + "): " + status.detailedError;
          Logger.cs_log(errorMessage);
          failed.insert(deposit.refNum);
        }
        else
        {
          voided.insert(deposit.refNum);
        }

        // Bug 13613: Write activity log record
        string voidStatus = errorMessage == null ? "Void" : "Void Error";
        string errorDetail = errorMessage == null ? "" : ", Error:" + errorMessage;
        string overShortDetail = deposit.overShort == Decimal.Zero ? "" : string.Format(",Overshort: {0:C}", deposit.overShort);
        string commentDetail = deposit.reason == "" ? "" : ",Comment:" + deposit.reason;
        string details = "Amount:" + deposit.amount.ToString("C")
        + ",Reconciled:" + deposit.reconDate
        + ",By:" + deposit.user
        + ",Type:" + deposit.reconType
        + errorDetail
        + overShortDetail
        + commentDetail;

        misc.CASL_call("a_method", "actlog", "args",
        new GenericObject("user", user, "app", "Portal", "action", "Bank Rec", "summary", voidStatus, "detail", details));
        // End Bug 13613

      }

      GenericObject results = new GenericObject();
      results.Add("voided", voided);
      results.Add("failed", failed);

      return results;
    }

    /// <summary>
    /// Read the "Import Directory List" and construct a list (Dictionary actually)
    /// of the subdirectories and they file types for the parsers to use.
    /// Bug 17179
    /// </summary>
    /// <param name="rd"></param>
    /// <returns></returns>
    private static List<ImportSetting> getImportSettings(recon.ReconcileDeposits rd)
    {
      string importOptionsList = rd.getConfigValue("import_options_lists", "");
      string ftpRemotePath = rd.getConfigValue("file_import_ftp_remote_path", ""); ;
      string localPath = rd.getConfigValue("file_import_location", "");
      string importMethod = rd.getConfigValue("file_import_method", "local").ToLower();

      List<ImportSetting> directoryList = new List<ImportSetting>();

      // OK here we go.  Protect against bad format in the "Import Directory List"
      // with this try block
      try
      {
        // If the directory list is not set, just return the single directory as before
        // This is the way it used to work
        if (string.IsNullOrEmpty(importOptionsList))
        {
          string importFormat = rd.getConfigValue("file_import_format", "BAI");

          if (importMethod == "folder")
          {
            ImportSetting importSettings = new ImportSetting(localPath, localPath, importFormat, "");
            directoryList.Add(importSettings);
          }
          else
          {
            ImportSetting importSettings = new ImportSetting(localPath, ftpRemotePath, importFormat, "");
            directoryList.Add(importSettings);

          }
          return directoryList;
        }

        foreach (string tuple in importOptionsList.Split(','))
        {
          string[] components = tuple.Split('|');
          string remotePath = components[0];
          string newLocalPath = "";



          string fullPath = "";
          if (importMethod == "folder")
          {
            // Everything is local
            fullPath = remotePath;
            newLocalPath = remotePath;
          }
          else
          {
            string[] pathParts = remotePath.Split('/');   // Parse out something like /BBT/TO_CORE/ConeHealth
            if (pathParts.Length < 2)
              throw new Exception(string.Format("getImportSettings '{0}' is not correct. The path '{1}' should be at least one level deep", tuple, remotePath));
            string bankName = pathParts[1];          // Get the 'BBT' part
            fullPath = remotePath;
            newLocalPath = System.IO.Path.Combine(localPath, bankName);
          }

          string file_import_format = components[1];

          string bankAccount = components[2];
          ImportSetting importSettings = new ImportSetting(newLocalPath, fullPath, file_import_format, bankAccount);
          directoryList.Add(importSettings);

        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in getImportSettings", e);
        string msg = string.Format("getImportSettings '{0} or other configuration is not correct: {1} ", importOptionsList, e.ToString());
        Logger.cs_log(msg);
        //KeyValuePair<string, string> kvp = new KeyValuePair<string, string>(file_import_location, rd.getConfigValue("file_import_format", "BAI"));
        //directoryList[ftpRemotePath] = kvp;

        ImportSetting importSettings = new ImportSetting(localPath, ftpRemotePath, rd.getConfigValue("file_import_format", "BAI"), "");
        directoryList.Add(importSettings);
      }

      return directoryList;
    }

    /// <summary>
    /// A new wrapper around the ImportFiles method. 
    /// This version loops through all the subdirectories 
    /// in the "Import Directory List" and calls ImportFiles
    /// for each one.  In this case the "File Import Format"
    /// and "File Import Extension"are ignored.  
    /// Please do not have any junk files in these subdirectories; this method will try to import
    /// them all.
    /// If the "Import Directory List" is not set, then just a single
    /// directory is looked at, "File Import Location" and the "File Import Format"
    /// and "File Import Extension" are used.
    /// Bug 17179
    /// </summary>
    /// <returns></returns>
    public static GenericObject ImportFiles(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      string userid = geo_args.get("userid") as string;

      // Read the "Import Directory List" to get the subdirectories and file types
      //Dictionary<string, KeyValuePair<string, string>> importDirectoryList = getDirectoryList(rd);
      List<ImportSetting> importSettings = getImportSettings(rd);

      ArrayList importedList = new ArrayList();
      ArrayList failedList = new ArrayList();

      // Loop through each subdirectory
      foreach (ImportSetting importSetting in importSettings)
      {
        string ftpRemotePath = importSetting.ftpPath;
        string path = importSetting.localPath;
        string import_file_type = importSetting.fileFormat;
        GenericObject results = ImportFiles_internal(path, ftpRemotePath, import_file_type, userid, importSetting.accountNumber);

        // Now accumulate the results
        collectImportResults(importedList, failedList, results);
      }

      GenericObject cumlativeResults = setReturnValue(importedList, failedList);
      return cumlativeResults;
    }

    /// <summary>
    /// Given an ArrayList of the imported and failed files,
    /// return a new geo with these setup correctly for the
    /// overlying CASL code to read them.
    /// Bug 17179
    /// </summary>
    /// <param name="importedList"></param>
    /// <param name="failedList"></param>
    /// <returns></returns>
    private static GenericObject setReturnValue(ArrayList importedList, ArrayList failedList)
    {
      GenericObject cumlativeResults = new GenericObject();

      GenericObject imported = new GenericObject();
      imported.vectors = importedList;
      GenericObject failed = new GenericObject();
      failed.vectors = failedList;

      cumlativeResults.set("imported", imported);
      cumlativeResults.set("failed", failed);
      return cumlativeResults;
    }

    /// <summary>
    /// The import process returns a geo (results) that has two sub-geo's, imported and failed.
    /// Collect the vectors of those geos and save them so they can be accumulated.
    /// Bug 17179
    /// </summary>
    /// <param name="importedList"></param>
    /// <param name="failedList"></param>
    /// <param name="results"></param>
    private static void collectImportResults(ArrayList importedList, ArrayList failedList, GenericObject results)
    {
      if (results.has("imported"))
      {
        ArrayList importedFileList = (results.get("imported") as GenericObject).vectors;
        importedList.AddRange(importedFileList);
      }
      if (results.has("failed"))
      {
        ArrayList importedFailedList = (results.get("failed") as GenericObject).vectors;
        failedList.AddRange(importedFailedList);
      }
    }

    /// <summary>
    /// Import all available bank deposit files, using the path provided in confi
    /// Bug 13050: UMN, add FTP support. See attached mod in TTS.
    /// Bug 13638: UMN - add SFTP support
    /// Bug 13690. FT. Ported the FTP and SFTP code for Geisinger
    /// Bug 17179. FT. Made this callable from higherup so that we can parameterize the remotePath & import_file_type and run it for mulitple sources.
    /// </summary>
    /// <returns></returns>
    public static GenericObject ImportFiles_internal(string localPath, string ftpRemotePath, string import_file_type, string userid, string bankAccount)
    {
      // Bug 18137: This is grossly inefficient; each call to getConfigValue hits the database!
      // Bug 11515
      //string path = (string)((GenericObject)c_CASL.c_object("Business.bank_deposit_recon.data")).get("file_import_location");
      string importMethod = rd.getConfigValue("file_import_method", "local").ToLower();
      string ftpAddr = rd.getConfigValue("file_import_ftp_address", "");
      string ftpPort = rd.getConfigValue("file_import_ftp_port", "21");
      string ftpUser = rd.getConfigValue("file_import_ftp_uid", "");
      string ftpPass = rd.getConfigValue("file_import_ftp_password", "");
      string ftpFileEx = rd.getConfigValue("file_import_ftp_fileex", "");
      string ftpdelFiles = rd.getConfigValue("file_import_del_files", "");
      string sftpPrivateKeypath = rd.getConfigValue("file_import_sftp_private_keypath", "");
      string sftpPassphrase = rd.getConfigValue("file_import_sftp_passphrase", "");
      string sftpHostKeyFingerprint = rd.getConfigValue("file_import_sftp_host_key_fingerprint", "");

      // Bug 18136; Automagically pull the bank account filter list from the configuration
      bool enableBankAccountFiltering = Convert.ToBoolean(rd.getConfigValue("enable_file_import_bank_account_filtering", "false"));


      FTP ftp = null;
      SFTP sftp = null;
      bool pwdIsEncrypted = true;

      // Bug 15186: Moved these here
      List<ReconStatus> statuses;

      GenericObject imported = new GenericObject();
      GenericObject failed = new GenericObject();

      // Bug 15186: Add overall enclosing try/finally block to close SFTP/FTP only at the end
      try
      {

        // Use user/password to login if not set
        if (importMethod == "sftp")
        {
          // SFTP
          // If key path is not set, do not use the key file and key file password
          if (sftpPrivateKeypath == "")
          {
            sftp = new SFTP(ftpAddr, ftpUser, ftpPass, ftpPort, true, pwdIsEncrypted,
            "", "", "");
          }
          else
          {
            sftp = new SFTP(ftpAddr, ftpUser, ftpPass, ftpPort, true, pwdIsEncrypted,
            sftpPrivateKeypath, sftpHostKeyFingerprint, sftpPassphrase);
          }
        }
        else
        {
          // Not SFTP.  Use straight FTP
          ftp = new FTP(ftpAddr, ftpUser, ftpPass, ftpPort, true, pwdIsEncrypted, false, false, false);
        }

        if (importMethod == "ftp" || importMethod == "sftp") // Bug 13638: UMN - add SFTP support
        {
          // Use a wildcard with the file extension.  No need for regex...
          string ftpRegex = "*" + ftpFileEx;

          try
          {
            // ignore whether files are fetched or not. Bank rec will sort it out
            // don't delete files here (last param). Delete only on successful/duplicate import later.
            if (importMethod == "ftp")
            {
              ftp.Connect();
              if (ftpRemotePath != "")
                ftp.ChangeRemoteDirectory(ftpRemotePath);
              ftp.GetFiles(ftpRegex, localPath);
            }
            else if (importMethod == "sftp") // Bug 13638: UMN - add SFTP support
            {
              sftp.Connect();
              if (ftpRemotePath != "")
                sftp.ChangeRemoteDirectory(ftpRemotePath);
              sftp.GetFiles(ftpRegex, localPath);
            }

            Logger.cs_log("ImportBankFiles: imported files");
          }
          catch (Exception e)
          {
            // Bug 17849
            Logger.cs_log_trace("Error in ImportBankFiles", e);
            string msg = "ImportBankFiles Error (Exception thrown): " + e.Message;
            //Logger.cs_log(msg + Environment.NewLine + e.StackTrace);

            misc.CASL_call("a_method", "actlog", "args",
              new GenericObject("user", userid, "app", "Portal", "action", "Bank Rec Import", "summary", "Error", "detail", msg));


            return misc.MakeCASLError("Error Importing Files", "REC-219", msg);
          }
        }
        // END Bug 13050: UMN, add FTP support. See attached mod in TTS.

        // Bug 10996: FT: Make the BAI codes variable work
        //string baiCodes = (string)((GenericObject)c_CASL.c_object("Business.bank_deposit_recon.data")).get("bai_codes", "");
        string baiCodes = rd.getConfigValue("bai_codes", "");
        // End 11515
        HashSet<int> baiCodesSet = getBAICodes(baiCodes);
        // End of 10996


        // Bug 11570: 
        string withdrawalCodes = rd.getConfigValue("withdrawal_codes", "");
        HashSet<int> withdrawalCodesSet = getWithdrawalCodes(withdrawalCodes);
        // End 11570

      // Bug 25037
      string achCodes = rd.getConfigValue("ach_bai_codes", "");
      HashSet<int> achCodesSet = getBAICodes(achCodes);
      // End 25037

        // Bug 17238/ 18136
        HashSet<string> bankAccountWhitelistSet = getBankAccountWhiteList(enableBankAccountFiltering);
        // End Bug 17238

        // Bug 17658: Get the import BAI Text field filtering options
        string baiTypeCodesFilterList = rd.getConfigValue("file_import_filter_pattern_filter_type_codes", "");
        HashSet<int> fileImportFilterTypeCodesSet = getBaiTypeCodeFilterList(baiTypeCodesFilterList);
        string fileImportFilterPattern = rd.getConfigValue("recon_regex", "");      // Bug 18136: Use the Regex field instead of a new one
                                                                                    // End Bug 17658

        // Bug 18061  Setup costomer reference (deposit slip number) filtering
        string errMsg = "";
        string depositSlipFilter = "";
        bool enableDepositSlipFiltering = false;
        bool gotFieldValueList = setupDepositSlipFilter(userid, ref depositSlipFilter, ref enableDepositSlipFiltering, ref errMsg);
        if (!gotFieldValueList)
        {
          failed.insert(errMsg);      // Add the human-readable message
          GenericObject errorGeo = new GenericObject();
          errorGeo.Add("imported", imported);
          errorGeo.Add("failed", failed);
          return errorGeo;
        }
        // End Bug 18061

      // Bug 25037
      string achRegexPattern = rd.getConfigValue("ach_regex", "");      // Bug 18136: Use the Regex field instead of a new one

        try
        {
          List<String> importedFiles = rd.importBankFiles(localPath, ftpRemotePath, import_file_type, baiCodesSet, withdrawalCodesSet, achCodesSet, enableBankAccountFiltering, bankAccountWhitelistSet, userid, bankAccount, fileImportFilterTypeCodesSet, fileImportFilterPattern, depositSlipFilter, enableDepositSlipFiltering, achRegexPattern, out statuses);   // Bug 10996 FT: Modified to pass the codes set


          // Bug 13050: UMN add ftp support; Bug 13638: UMN - add SFTP support
          if ((importMethod == "ftp" || importMethod == "sftp") && ftpdelFiles == "true")
          {
            // delete files on FTP server but only the files that successfully imported,
            // or were duplicates
            foreach (string file in importedFiles)
            {
              try
              {
                if (importMethod == "ftp")
                {
                  ftp.DeleteFile(file);
                }
                else if (importMethod == "sftp") // Bug 13638: UMN - add SFTP support
                {
                  sftp.DeleteFile(file);
                }
              }
              catch (Exception e)
              {
                // Bug 17849
                Logger.cs_log_trace("Error in delete file on FTP/SFTP Server", e);
                string msg = "Failed to delete file on FTP/SFTP Server: " + file;
                Logger.Log(msg, "BankRec: ImportBankFiles", "err");
              }
            }
          }
        }
        catch (Exception e)
        {
          // Bug 17849
          Logger.cs_log_trace("Error in ImportBankFiles", e);
          string msg = "ImportBankFiles Error (Exception thrown): " + e.Message;
          return misc.MakeCASLError("Error Importing File", "REC-219", msg);
        }
      }
      finally
      {
        // Destructor will call Disconnect, but with .NET it's async scheduled. So calling
        // disconnect here will free the connection faster
        if (ftp != null) ftp.Disconnect();
        if (sftp != null) sftp.Disconnect();
      }

      foreach (ReconStatus status in statuses)
      {
        if (status.success)
        {
          imported.insert(status.humanReadableError);
        }
        else
        {
          Logger.cs_log("ImportBankFiles Error (" + status.code + "): " + status.detailedError);

          failed.insert(status.humanReadableError);
        }
      }

      GenericObject results = new GenericObject();
      results.Add("imported", imported);
      results.Add("failed", failed);

      return results;
    }

    /// <summary>
    /// Get the BANK_REC_DEPOSIT_SLIP_PREFIXES field value list and change to a regular expression
    /// Bug 18061
    /// </summary>
    /// <returns></returns>
    private static bool setupDepositSlipFilter(
      string userid,
      ref string depositSlipFilter,
      ref bool enableDepositSlipFiltering,
      ref string errMsg)
    {
      depositSlipFilter = "";
      errMsg = "";
      enableDepositSlipFiltering = false;

      try
      {
        // See deposit slip fintering is turned on  
        enableDepositSlipFiltering = Convert.ToBoolean(rd.getConfigValue("enable_file_import_deposit_slip_filtering", "false"));
        if (!enableDepositSlipFiltering)
          return true;

        GenericObject allFieldValueLists = (GenericObject)c_CASL.c_object("Business.Field_value_list.of");
        if (!allFieldValueLists.has("BANK_REC_DEPOSIT_SLIP_PREFIXES"))
        {
          string msg = "ImportBankFiles Error: cannot find the BANK_REC_DEPOSIT_SLIP_PREFIXES field value list; no Customer Reference (Deposit List) filtering will be done during import.";

          misc.CASL_call("a_method", "actlog", "args",
            new GenericObject("user", userid, "app", "Portal", "action", "Bank Rec Import", "summary", "Error", "detail", msg));
          Logger.cs_log(msg);
          return true;
        }

        GenericObject depositSlipPrefixes = (GenericObject)c_CASL.c_object("Business.Field_value_list.of.BANK_REC_DEPOSIT_SLIP_PREFIXES");
        GenericObject values = depositSlipPrefixes.get("values", new GenericObject()) as GenericObject;
        bool first = true;

        StringBuilder sb = new StringBuilder();
        sb.Append("^[0]{0,9}(");                    // Bug 19953: Ignore up to 9 leading zeros when filtering so I added "[0]{0,9}"
        foreach (GenericObject field_value in values.vectors)
        {
          if (first)
            first = false;
          else
            sb.Append('|');
          if (field_value.has("value"))
            sb.Append((field_value.get("value") as string).Trim());
        }
        sb.Append(')');

        depositSlipFilter = sb.ToString();
        return true;

      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in ImportBankFiles, cannot read or translate the BANK_REC_DEPOSIT_SLIP_PREFIXES field value list", e);
        string msg = "ImportBankFiles Error: cannot read or translate the BANK_REC_DEPOSIT_SLIP_PREFIXES field value list: " + e.Message;
        //Logger.cs_log(msg + Environment.NewLine + e.StackTrace);

        misc.CASL_call("a_method", "actlog", "args",
          new GenericObject("user", userid, "app", "Portal", "action", "Bank Rec Import", "summary", "Error", "detail", msg));

        errMsg = "Error Importing Files: REC-221:  Cannot decode field value list";
        return false;
      }
    }

    /// <summary>
    /// Change a comma-separated list of BAI type code to apply the regex to
    /// into a nice neat HashSet for quick searching.
    /// Bug 17658.
    /// </summary>
    /// <param name="baiTypeCodesFilterList"></param>
    /// <returns></returns>
    private static HashSet<int> getBaiTypeCodeFilterList(string baiTypeCodesFilterList)
    {
      HashSet<int> baiCodesSet = new HashSet<int>();
      String[] codes = baiTypeCodesFilterList.Split(',');
      foreach (String code in codes)
      {
        try
        {
          if (!code.Equals(""))
          {
            int baiCode = Convert.ToInt32(code);
            baiCodesSet.Add(baiCode);
          }
        }
        catch (FormatException)
        {
          // skip and ignore bogus entries
        }
      }
      return baiCodesSet;

    }

    /// <summary>
    /// Get the set of bank accounts to include in the import.
    /// Bug 17238
    /// </summary>
    /// <param name="bankAccountWhitelist"></param>
    /// <returns></returns>
    private static HashSet<string> getBankAccountWhiteList(bool enableBankAccountFiltering)
    {
      HashSet<string> whitelistSet = new HashSet<string>();

      // First get all the accounts in the config
      GenericObject accountList = (GenericObject)c_CASL.c_object("Business.Bank_account.of");

      // Loop through each account and check the reconcilable value
      foreach (DictionaryEntry dirEntry in accountList)
      {
        String bankid = (String)dirEntry.Key;

        // Skip accounts such as _parent and _name (which is a string)
        if (bankid.StartsWith("_"))
          continue;

        // Get the actual geo for the bank account
        GenericObject bankAccountGEO = (GenericObject)dirEntry.Value;

        // Skip accounts without a account number; being generous here also
        // Should this be Contains or Has?
        if (!bankAccountGEO.Contains("account_nbr"))
          continue;
        if (!bankAccountGEO.has("account_nbr"))
          continue;


        string accountNbr = bankAccountGEO.get("account_nbr") as string;
        if (string.IsNullOrEmpty(accountNbr))
          continue;

        try
        {
          whitelistSet.Add(accountNbr.Trim());
        }
        catch (FormatException)
        {
          // skip and ignore bogus entries
        }

      }

      return whitelistSet;
    }

    /// <summary>
    /// Get the set of bank account numbers to exclude from the import
    /// Bug 17238
    /// </summary>
    /// <param name="bankAccountWhitelist"></param>
    /// <returns></returns>
    private static HashSet<string> getBankAccountExclustionList(string bankAccountWhitelist)
    {
      HashSet<string> exclusionList = new HashSet<string>();

      String[] bankAccounts = bankAccountWhitelist.Split(',');
      foreach (String bankAccount in bankAccounts)
      {
        try
        {
          if (!bankAccount.Equals(""))
          {
            exclusionList.Add(bankAccount.Trim());
          }
        }
        catch (FormatException)
        {
          // skip and ignore bogus entries
        }
      }
      return exclusionList;
    }

    /// <summary>
    /// Get set of BAI codes to include
    /// </summary>
    /// <param name="baiCodes"></param>
    /// <returns></returns>
    private static HashSet<int> getBAICodes(string baiCodes)
    {
      HashSet<int> baiCodesSet = new HashSet<int>();

      String[] codes = baiCodes.Split(',');
      foreach (String code in codes)
      {
        try
        {
          if (!code.Equals(""))
          {
            int baiCode = Convert.ToInt32(code);
            baiCodesSet.Add(baiCode);
          }
        }
        catch (FormatException)
        {
          // skip and ignore bogus entries
        }
      }
      return baiCodesSet;
    }

    private static HashSet<int> getWithdrawalCodes(string withdrawalCodes)
    {
      HashSet<int> withdrawalCodesSet = new HashSet<int>();

      String[] wcodes = withdrawalCodes.Split(',');
      foreach (String code in wcodes)
      {
        try
        {
          if (!code.Equals(""))
          {
            int withdrawalCode = Convert.ToInt32(code);
            withdrawalCodesSet.Add(withdrawalCode);
          }
        }
        catch (FormatException)
        {
          // skip and ignore bogus entries
        }
      }
      return withdrawalCodesSet;
    }


    private static GenericObject ConvertToGEO(ExpectedDeposit deposit)
    {
      GenericObject geo = new GenericObject();

      if (deposit == null)
      {
        geo.Add("diagnostic", "No data");
        return geo;
      }

      geo.Add("account", deposit.account == null ? "" : deposit.account);
      geo.Add("age", deposit.age);
      geo.Add("amount", deposit.amount);
      geo.Add("database_id", deposit.databaseID == null ? "" : deposit.databaseID.databaseKey);
      geo.Add("dep_date", deposit.depDate.ToString("MM/dd/yyyy"));
      geo.Add("file", deposit.file == null ? "" : deposit.file);
      geo.Add("workgroup", deposit.workgroup == null ? "" : deposit.workgroup);   // Bug 15157
      geo.Add("ref_num", deposit.refNum == null ? "" : deposit.refNum);

      return geo;
    }

    /// <summary>
    /// Used to convert Expected Deposit Details to something that can be used
    /// for the details button on the Open Deposit screen.
    /// This first needs to flatten out the Tenders and total them.
    /// </summary>
    /// <param name="deposit">Details object</param>
    /// <param name="dept_names">Mapping between Workgroup ID and workgroup Name</param>
    /// <returns></returns>
    private static GenericObject ConvertToGEO(ExpectedDepositDetails deposit, GenericObject dept_names)
    {
      GenericObject depositGEO = new GenericObject();

      if (deposit == null)
      {
        depositGEO.Add("diagnostic", "No data");
        return depositGEO;
      }

      depositGEO = ConvertToGEO((ExpectedDeposit)deposit);

      GenericObject tenders = new GenericObject();
      GenericObject tenderAmounts = new GenericObject();

      foreach (Tender tender in deposit.tenders)
      {
        GenericObject tenderGEO = new GenericObject();

        tenderGEO.Add("amount", tender.amount);
        tenderGEO.Add("description", tender.description);
        tenderGEO.Add("kind", tender.kind);
        tenderGEO.Add("prop", tender.prop);
        tenderGEO.Add("source_date", tender.sourceDate.ToString("MM/dd/yyyy"));
        tenderGEO.Add("source_time", tender.sourceDate.ToShortTimeString());
        tenderGEO.Add("ref_id", tender.sourceRefID);

        Decimal newAmount = tender.amount + (Decimal)tenderAmounts.remove(tender.description, new Decimal(0));

        tenderAmounts.Add(tender.description, newAmount);

        tenders.insert(tenderGEO);
      }

      depositGEO.Add("tenders", tenders);
      depositGEO.Add("tender_amounts", tenderAmounts);
      depositGEO.Add("comments", deposit.comments);
      depositGEO.Add("owner_id", deposit.ownerUserId);
      depositGEO.Add("creator_id", deposit.createrUserId);   // Bug 9608 and Bug 10251: FT Change name from creater_id
      depositGEO.Add("bank_reconcilation_key", deposit.matchKey);         // Bug 10250 FT
      // Bug 15157 FT
      if (dept_names.has(deposit.workgroup))
      {
        StringBuilder sb = new StringBuilder();
        sb.Append(dept_names.get(deposit.workgroup));
        sb.Append(' ').Append('(').Append(deposit.workgroup).Append(')');
        depositGEO["workgroup"] = sb.ToString();  // Either add or update the workgroup value 
      }
      else
        depositGEO["workgroup"] = deposit.workgroup;
      // End Bug 15157

      // Bug 18511: FT: needed to explain bin'ing
      depositGEO.Add("bank_id", deposit.bankId);

      return depositGEO;
    }

    private static GenericObject ConvertToGEO(BankDeposit deposit)
    {
      GenericObject geo = new GenericObject();

      if (deposit == null)
      {
        geo.Add("diagnostic", "No data");
        return geo;
      }

      geo.Add("account", deposit.account == null ? "unset" : deposit.account);
      geo.Add("age", deposit.age);
      geo.Add("amount", deposit.amount);
      geo.Add("database_id", deposit.databaseID.databaseKey);
      geo.Add("dep_date", deposit.depDate.ToString("MM/dd/yyyy"));
      geo.Add("deposit_status", deposit.bStatusToString(deposit.depositStatus));
      geo.Add("ref_num", deposit.refNum == null ? "unset" : deposit.refNum);
      geo.Add("status", BankDeposit.convertStatus(deposit.depositStatus));

      return geo;
    }

    private static GenericObject ConvertToGEO(BankDepositDetails deposit)
    {
      GenericObject depositGEO = new GenericObject();

      if (deposit == null)
      {
        depositGEO.Add("diagnostic", "No data");
        return depositGEO;
      }

      depositGEO = ConvertToGEO((BankDeposit)deposit);

      depositGEO.Add("text", deposit.text);
      depositGEO.Add("bank_reference", deposit.bankReference);
      depositGEO.Add("bank_reconcilation_key", deposit.matchKey);        // Bug 10250: FT: extract match_key for details drop down
      depositGEO.Add("import_timestamp", deposit.importTimestamp);        // Bug 18982: FT

      return depositGEO;
    }

    private static GenericObject ConvertToGEO(ReconciledDeposit deposit)
    {
      GenericObject geo = new GenericObject();

      if (deposit == null)
      {
        geo.Add("diagnostic", "No data");
        return geo;
      }

      geo.Add("amount", deposit.amount);
      geo.Add("ref_num", deposit.refNum == null ? "" : deposit.refNum);
      geo.Add("database_id", deposit.databaseID == null ? "" : deposit.databaseID.databaseKey);
      geo.Add("over_short", deposit.overShort);
      geo.Add("reason", deposit.reason == null ? "" : deposit.reason);
      geo.Add("recon_date", deposit.reconDate.ToString("MM/dd/yyyy"));
      geo.Add("recon_time", deposit.reconDate.ToShortTimeString());

      geo.Add("recon_status", ToString(deposit.reconStatus));
      geo.Add("recon_type", ToString(deposit.reconType));
      geo.Add("user", deposit.user == null ? "" : deposit.user);

      geo.Add("workgroup_id", deposit.workgroup);   // Bug 15157
      geo.Add("file", deposit.file);                // Bug 15157

      return geo;
    }

    private static string ToString(ReconciledDeposit.rStatus rStatus)
    {
      switch (rStatus)
      {
        case ReconciledDeposit.rStatus.reconciled:
          return "reconciled";
        case ReconciledDeposit.rStatus.voided:
          return "voided";
        default:
          throw new Exception("Unknown reconciled status: " + rStatus);
      }
    }

    private static string ToString(ReconciledDeposit.rType rType)
    {
      switch (rType)
      {
        case (ReconciledDeposit.rType.auto):
          return "Auto";
        case (ReconciledDeposit.rType.matchkey):    // Bug 10324
          return "Key";
        case (ReconciledDeposit.rType.forced):
          return "Forced";
        case (ReconciledDeposit.rType.ignored):
          return "Ignored";
        case (ReconciledDeposit.rType.manual):
          return "Manual";
        default:
          throw new Exception("Unrecognized reconciliation type: " + rType);
      };
    }

    private static GenericObject ConvertToGEO(ReconciledDepositDetails deposit)
    {
      GenericObject geo = new GenericObject();

      if (deposit == null)
      {
        geo.Add("diagnostic", "No data");
        return geo;
      }

      geo = ConvertToGEO((ReconciledDeposit)deposit);

      List<BankDepositDetails> bankDetailsList = deposit.getBankDetailList();
      GenericObject bankDetails = new GenericObject();
      foreach (BankDepositDetails details in bankDetailsList)
      {
        GenericObject bankGeo = ConvertToGEO(details);
        bankDetails.insert(bankGeo);
      }
      geo.Add("bank_details", bankDetails);

      List<ExpectedDepositDetails> expectedDetailsList = deposit.getExpectedDetailList();
      GenericObject expectedDetails = new GenericObject();
      foreach (ExpectedDepositDetails details in expectedDetailsList)
      {
        GenericObject expectedGeo = ConvertToGEO(details);
        expectedDetails.insert(expectedGeo);
      }
      geo.Add("expected_details", expectedDetails);

      return geo;
    }

    /// <summary>
    /// Bug 11492
    /// Expected deposit records as a string representation of a Javascript array 
    /// suitable for jQuery's DataTable.
    /// Each record is color coded based on its configurable age:
    /// R--Red
    /// Y--Yellow
    /// W--White
    /// </summary>
    /// <param name="args"></param>
    /// <returns>Expected deposit records as a string representation of a Javascript array suitable for jQuery's DataTable</returns>
    public static String GetExpectedDepositsAsJSArray(params object[] args)
    {
      System.Diagnostics.Stopwatch watch = startWatch();

      //      warning_cutoff=Business.bank_deposit_recon.data.warning_cutoff
      //late_cutoff=Business.bank_deposit_recon.data.late_cutoff
      int warning_cutoff = ((int)((GenericObject)c_CASL.c_object("Business.bank_deposit_recon.data")).get("warning_cutoff", "0"));
      int late_cutoff = ((int)((GenericObject)c_CASL.c_object("Business.bank_deposit_recon.data")).get("late_cutoff", "0"));

      // Bug 10250: FT: changed to pass in the workgroup ID --> match key mappings to shown in Details dropdown
      GenericObject geo_args = misc.convert_args(args);
      GenericObject matchKeys = geo_args.get("dept_match_key") as GenericObject;

      // Bug 11479: Pass bank account number in to filter on
      string bank_account = geo_args.get("bank_account", "") as string;

      // Bug ???? Extension to filter on the work group
      string workgroup_id = geo_args.get("workgroup_id", "") as string;

      // Retrieve the list of deposits
      ReconStatus status;
      List<ExpectedDeposit> expectedDeposits = new List<ExpectedDeposit>();

      StringBuilder jsArray = new StringBuilder();
      jsArray.Append('[');
      try
      {
        // Bug 11411: Added matchKeys so we can add this to reference num field if blank
        expectedDeposits = rd.getExpectedDepositList(bank_account, matchKeys, workgroup_id, out status);
      }
      catch (Exception e)
      {
        // Bug 17849 throw a new exception, don't log here
        //Logger.cs_log_trace("Error in GetExpectedDepositsAsJSArray", e);

        string msg = "GetExpectedDepositsAsJSArray Error (Exception thrown): ";
        //Logger.cs_log(msg + e.Message + Environment.NewLine + e.StackTrace);
        throw CASL_error.create("title", "Error Reading Expected Deposit Array", "code", "REC-206", "message", msg + e.Message);
      }

      if (!status.success)
      {
        Logger.cs_log("GetExpectedDepositsAsJSArray Error (" + status.code + "): " + status.detailedError);
        throw CASL_error.create("title", "Error Reading Expected Deposit Array", "code", status.code, "message", status.humanReadableError);
      }

      bool first = true;
      foreach (ExpectedDeposit ed in expectedDeposits)
      {
        if (first)
          first = false;
        else
          jsArray.Append(',');  // Prepend trailing comma

        // Format like this: ["07/14/2011", "", "23423423", 7.00, "2011195003"], 
        jsArray.Append('[');
        jsArray.Append("\"\", "); // Placeholder column for details button
        jsArray.Append('"').Append(ed.depDate.ToShortDateString()).Append('"').Append(", ");
        jsArray.Append("\"1\", ");
        jsArray.Append('"').Append(ed.refNum).Append('"').Append(", ");
        jsArray.Append('"').Append(ed.amount).Append('"').Append(", ");
        jsArray.Append('"').Append(ed.file).Append('"').Append(", ");
        jsArray.Append('"').Append(getExpectedDepositColorCode(ed.age, warning_cutoff, late_cutoff)).Append('"').Append(", ");
        jsArray.Append('"').Append(ed.databaseID.databaseKey).Append('"').Append(", ");
        jsArray.Append('"').Append(ed.workgroup).Append('"');
        jsArray.Append(']');
      }

      if (m_debug)
      {
        watch.Stop();
        Logger.cs_log("GetExpectedDepositsAsJSArray: took this many Milliseconds to get JS array: " + watch.ElapsedMilliseconds);
      }
      jsArray.Append(']');
      return jsArray.ToString();

    }

    /// <summary>
    /// Color code the JS array records based on age.
    /// Bug 11492.
    /// </summary>
    /// <param name="age"></param>
    /// <param name="warning_cutoff"></param>
    /// <param name="late_cutoff"></param>
    /// <returns></returns>
    private static char getExpectedDepositColorCode(int age, int warning_cutoff, int late_cutoff)
    {
      if (age >= late_cutoff)
        return 'R';
      if (age >= warning_cutoff)
        return 'Y';
      else
        return 'W';
    }

    /// <summary>
    /// Bug 11492
    /// Bank deposit records as a string representation of a Javascript array 
    /// suitable for jQuery's DataTable.
    /// Each record is color coded based its status
    /// G--New (freshly imported) records
    /// W--Records that did not automatch
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static String GetBankDepositsAsJSArray(params object[] args)
    {
      // Bug 11473: Pass in bank account number so it can be filtered on server side
      GenericObject geo_args = misc.convert_args(args);
      string bank_account = geo_args.get("bank_account", "") as string;
      // End Bug 11473

      System.Diagnostics.Stopwatch watch = startWatch();
      GenericObject deposits = new GenericObject();

      // Retrieve the list of deposits
      ReconStatus status;
      List<BankDeposit> bankDeposits = new List<BankDeposit>();

      try
      {
        // Bug 11473: Pass in bank account number so it can be filtered on server side
        // Bug 12060: Return all the bank accounts if the account is unset
        if (bank_account == "" || rd.isUnconfigured(bank_account))
          bankDeposits = rd.getBankDepositList(out status);
        else
        {
          var bankAccounts = new List<String>();
          bankAccounts.Add(bank_account);
          bankDeposits = rd.getBankDepositList(bankAccounts, out status);
        }
        // End Bug 11473
      }
      catch (Exception e)
      {
        // Bug 17849
        //Logger.cs_log_trace("Error in GetBankDepositsAsJSArray", e);
        string msg = String.Format("GetBankDepositsAsJSArray Error (Exception thrown).  Bank Account: {0}  ",bank_account);
        Logger.cs_log(msg + e.Message + Environment.NewLine +e.StackTrace);
        throw CASL_error.create("title", "Error Reading Bank Deposit Array", "code", "REC-201", "message", msg + e.Message);
      }


      if (!status.success)
      {
        Logger.cs_log("GetBankDepositsAsJSArray Error (" + status.code + "): " + status.detailedError);
        throw CASL_error.create("title", "Error Bank Deposit Array", "code", status.code, "message", status.humanReadableError);
      }


      StringBuilder jsArray = new StringBuilder();
      jsArray.Append('[');

      try
      {
        int maxReferenceLength = Convert.ToInt32(rd.getConfigValue("bank_deposit_ref_length", "12"));  // Bug 21276
        bool first = true;
        foreach (BankDeposit bd in bankDeposits)
        {
          //START BUG#11498 
          BankDepositDetails bdd = new BankDepositDetails();
          try
          {
            bdd = rd.getBankDepositDetails(bd.databaseID, out status);
          }
          catch (Exception e)
          {
            string msg = string.Format(
            "Cannot read the JS Array information for Bank Deposit: Exception reading the bank deposit details for deposit {0}: see the CS log for details: {1}",
            bd.databaseID.ToString(), e.Message);
            Logger.cs_log(msg + Environment.NewLine + e.StackTrace);
            throw CASL_error.create("title", "Error Reading Bank Deposit Details", "code", "REC-203", "message", msg);
          }

          if (!status.success)
          {
            string msg = string.Format("Cannot read the JS Array information for Bank Deposit: Error reading the bank deposit details for deposit {0} ({1}): {2} ",
            bd.databaseID.ToString(), status.code, status.detailedError);
            Logger.cs_log(msg);
            throw CASL_error.create("title", "Error Reading Bank Deposit Details", "code", status.code, "message", msg);
          }
          GenericObject bddGEO = ConvertToGEO(bdd);  // TODO: take out this pointless conversion
          //END BUG#11498 

          // Format like this: ["07/22/2011", "1", "00000824336", 6761.80, "databaseID"],
          if (first)
            first = false;
          else
            jsArray.Append(',');  // Prepend trailing comma
          jsArray.Append('[');
          jsArray.Append("\"\", "); // Placeholder column for details button
          jsArray.Append('"').Append(bd.depDate.ToShortDateString()).Append('"').Append(", ");
          jsArray.Append("\"1\", ");

          string sRef = bddGEO.get("bank_reconcilation_key", "") as string;//BUG#11498
          if (string.IsNullOrEmpty(sRef))//BUG#11498
          {
            string clipped = bd.refNum.PadRight(maxReferenceLength).Substring(0, maxReferenceLength);     // Bug 21276
            jsArray.Append('"').Append(clipped).Append('"').Append(", ");
          }
          else
          {
            string clipped = sRef.PadRight(maxReferenceLength).Substring(0, maxReferenceLength);          // Bug 21276
            jsArray.Append('"').Append(clipped).Append('"').Append(", ");//BUG#11498
          }
          jsArray.Append('"').Append(bd.amount).Append('"').Append(", ");
          jsArray.Append('"').Append(getBankDepositColorCode(bd.depositStatus)).Append('"').Append(", ");
          jsArray.Append('"').Append(bd.databaseID.databaseKey).Append('"');
          jsArray.Append(']');
        }
      }
      catch (Exception e)
      {
        string msg = "GetBankDepositsAsJSArray.  Cannot generate JS array. Error (Exception thrown): ";
        Logger.cs_log(msg + e.Message + Environment.NewLine +
        e.StackTrace);
        throw CASL_error.create("title", "Error Reading Bank Deposit Array", "code", "REC-202", "message", msg + e.Message);
      }

      if (m_debug)
      {
        watch.Stop();
        Logger.cs_log("GetBankDepositsAsJSArray query took this many Milliseconds: " + watch.ElapsedMilliseconds);
      }
      jsArray.Append(']');
      return jsArray.ToString();

    }

    /// <summary>
    /// Bug 11492
    /// </summary>
    /// <param name="bStatus"></param>
    /// <returns></returns>
    private static char getBankDepositColorCode(BankDeposit.bStatus bStatus)
    {
      if (bStatus == BankDeposit.bStatus.newDeposit)
        return 'G';
      else
        return 'W';
    }


    /// <summary>
    /// Return the reconciled deposits list as a Javascript array suitable for jQuery DataTables.
    /// Filter by the start and end date. Either the start_date or end_date can be am empty string
    /// in which case a default will be chosen.
    /// If both the start_date and end_date are empty, only the reconciliations for today
    /// will be returned.  If the the date is mis-typed or in an unrecognized format, 
    /// only the reconciliations for today will be returned.
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static String GetReconciledDepositsAsJSArray(params object[] args)
    {
      System.Diagnostics.Stopwatch watch = startWatch();

      GenericObject geo_args = misc.convert_args(args);

      // Bug 11451: Filter on start and end date
      string start_date = geo_args.get("start_date", "") as string;
      string end_date = geo_args.get("end_date", "") as string;

      // Bug 11549
      GenericObject matchKeys = geo_args.get("dept_match_key") as GenericObject;

      // Retrieve the list of deposits
      ReconStatus status;
      List<ReconciledDeposit> reconciledDeposits = new List<ReconciledDeposit>();

      StringBuilder jsArray = new StringBuilder();
      jsArray.Append('[');
      try
      {
        reconciledDeposits = rd.getReconciledDepositList(start_date, end_date, matchKeys, out status);
      }
      catch (Exception e)
      {
        string msg = "GetReconciledDepositsAsJSArray Error (Exception thrown): ";
        Logger.cs_log(msg + e.Message + Environment.NewLine +
        e.StackTrace);
        throw CASL_error.create("title", "Error Reading Reconciled Deposit Array", "code", "REC-204", "message", msg + e.Message);
      }

      if (!status.success)
      {
        Logger.cs_log("GetReconciledDepositsAsJSArray Error (" + status.code + "): " + status.detailedError);
        throw CASL_error.create("title", "Error Reading Reconciled Deposit Array", "code", status.code, "message", status.humanReadableError);
      }

      bool first = true;
      foreach (ReconciledDeposit rd in reconciledDeposits)
      {
        if (first)
          first = false;
        else
          jsArray.Append(',');  // Prepend trailing comma and break line.  Removed newline to save space.

        // Format like this: ["07/14/2011", "", "23423423", 7.00, "2011195003"], 
        jsArray.Append('[');
        jsArray.Append("\"1\",");
        jsArray.Append('"').Append(rd.reconDate.ToShortDateString()).Append('"').Append(",");
        jsArray.Append('"').Append(rd.reconDate.ToShortTimeString()).Append('"').Append(",");
        jsArray.Append('"').Append(rd.user).Append('"').Append(",");
        jsArray.Append('"').Append(rd.reconType).Append('"').Append(",");
        jsArray.Append('"').Append(rd.refNum).Append('"').Append(",");
        jsArray.Append('"').Append(String.Format("{0:C}", rd.amount)).Append('"').Append(",");
        jsArray.Append('"').Append(String.Format("{0:C}", rd.overShort)).Append('"').Append(",");
        jsArray.Append('"').Append(rd.databaseID.databaseKey).Append('"');
        jsArray.Append(']');
      }

      if (m_debug)
      {
        watch.Stop();
        Logger.cs_log("GetReconciledDepositsAsJSArray: took this many Milliseconds to get JS array: " + watch.ElapsedMilliseconds);
      }
      jsArray.Append(']');
      string jsArrayString = jsArray.ToString();
      return jsArrayString;
    }

    private static System.Diagnostics.Stopwatch startWatch()
    {
      if (m_debug)
      {
        System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
        watch.Start();
        return watch;
      }
      else
        return null;
    }

    /// <summary>
    /// Utility method to simply error reporting.
    /// </summary>
    /// <param name="title"></param>
    /// <param name="status"></param>
    /// <returns></returns>
    public static GenericObject MakeCASLError(string title, ReconStatus status)
    {
      if (m_debug)
        return misc.MakeCASLError(title, status.code, status.detailedError);
      else
        return misc.MakeCASLError(title, status.code, status.humanReadableError);
    }


  }
}
//bug 12018 NJ-restore compiler warnings.
#pragma warning restore 162
//end bug 12081
