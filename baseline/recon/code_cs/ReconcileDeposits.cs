﻿
using System;
using System.Data;
using System.Text;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using CASL_engine;
using System.IO;
using System.Linq;

using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Diagnostics;

using System.Text.RegularExpressions;

/*
  ERROR CODES FOR Bank Reconcilation ARE IN THE FORMAT REC-750.... PLEASE ALWAYS USE A SPECIFIC ERROR CODE FOR EACH ERROR.
  ALSO PLEASE UPDATE THIS SECTION WITH THE LARGEST ERROR YOU HAVE USED TO DATE. WHEN THE NEXT PERSON 
  CREATES ONE, THEY WILL BE ABLE TO ASSIGN ERRORS IN SEQUENCE./

  REC-100 - "TG_RECONCILED_DATA select error"
  REC-101 - "GEO retured from ReadIntoRecordset is not correct"
  REC-102 - "ReadIntoRecordset() or Data conversion failed"
  REC-103 - "TG_RECONCILED_DATA filtered select error"
  REC-104 - "ReadIntoRecordset() or Data conversion failed"
  REC-105 - "Cannot void Reconcilation"
  REC-106 - "Cannot void a reconciliation"
  REC-107 - "Cannot read the reconcilation details from the database for Database ID: "
  REC-108 - "TG_RECONCILED_DATA update failed"
  REC-109 - "Auto reconcilation failed"
  REC-110 - "Cannot read the bank deposit list"
  REC-111 - "getBankDepositList failed: SQL or conversion error"
  REC-112 - "Cannot read the filtered bank deposit list"
  REC-113 - "Filtered Bank deposit list failed"
  REC-114 - "Cannot read the reconciled deposit list"
  REC-115 - "Error reading the reconciled deposit list"
  REC-116 - "Cannot read the filtered reconciled deposit list"
  REC-117 - "Error reading the filtered reconciled deposit list"
  REC-118 - "Cannot reconcile deposit"
  REC-119 - "Cannot reconcile deposit. Internal error: "
  REC-120 - "Cannot reconcile deposit"
  REC-121 - "Cannot read expected bank accounts"
  REC-122 - "Cannot get list of expected bank accounts"
  REC-123 - "Cannot read bank accounts from bank list"
  REC-124 - "Cannot get list of bank accounts from bank list
  REC-125 - "Cannot list the Bank/BAI imput files"
  REC-126 - "Cannot process the Bank/BAI imput files"
  REC-127 - "Cannot read bank deposit details"
  REC-128 - "Cannot read bank deposit details and convert data"
  REC-129 - "Cannot read details for bank deposit"
  REC-130 - "Error reading details for bank deposit"
  REC-131 - "Cannot read expected deposit details"
  REC-132 - "Cannot read details for expected deposit"
  REC-133 - "Error reading details for expected deposit"
  REC-134 - "Failed to read reconciled deposits details"
  REC-135 - "Cannot read reconcilation details"
  REC-136 - "No reconcilation details found"
  REC-137 - "Error reading reconcilation details"
  REC-138 - "Cannot save imported bank file"
  REC-139 - "Error saving bank deposits to the database for file"
  REC-140 - "Cannot archive Bank/BAI file"
  REC-141 - "Cannot move failed Bank/BAI file to error subdirectory"
  REC-142 - "Cannot save bank deposits to the database"
  REC-143 - "Cannot parse bank file"
  REC-144 - "Error detected during attempt to void a Reconciled Deposit"
  REC-145 - "Cannot read tender information"
  REC-146 - "Cannot read tender information for expected deposit"
  REC-147 - "Cannot parse the bank file"
  REC-148 - "Cannot read the reference number from the bank deposit file"
  REC-149 - "Failure reading the reference / slip number from the BANK_DEPOSIT or DEPOSIT_DATA table"
  REC-150 - "Cannot translate bank account"
  REC-151 - "Cannot decode the account number configuration data"
  REC-152 - "Error reading comfiguration for bank account number"
  REC-153 - "Cannot translate bank ID to bank account number. getBankDepositDetails failed: SQL or conversion error: Error:"
  REC-155 - "Cannot translate bank account to bank ID"
  REC-156 - "Cannot read id from CBT_FLEX for this bank account:"
  REC-157 - "Error reading comfiguration for bank ID number:"
  REC-158 - "Error translating bank account to bank ID getBankDepositDetails failed: SQL or conversion error"
  REC-159 - "Error saving bank deposits to the database for file"
  REC-160 - "Duplicate record found in file {0} when importing bank files. Record number {1}"
  REC-161 - "Duplicate Bank/BAI records cannot be imported into the database.  Filename {0}:  Line: {1}"
  REC-162 - "Internal error: cannot reconcile deposits because database identifiers were not found"
  REC-163 - "Internal error: cannot reconcile deposits because database query failed"
  REC-164 - "Cannot reconcile expected deposit; not found in the database"
  REC-165 - "Internal error: cannot reconcile deposits because duplicate reconciliation query failed"
  REC-166 - "The expected deposit has already been reconciled: Cannot reconcile again"
  REC-167 - "Cannot reconcile deposit.  This deposit was already reconciled on :"
  REC-168 - "Internal error: cannot reconcile bank deposits because database query failed"
  REC-169 - "Internal error: Cannot reconcile bank deposit; this deposit not found in the database"
  REC-170 - "Internal error: cannot reconcile bank deposits because duplicate reconciliation query failed"
  REC-171 - "Cannot reconcile deposit.  This bank deposit was already reconciled on"
  REC-172 - "Automatch failed: Cannot read last auto-reconcilated record"
  REC-173 - "Automatch failed: When reading the last auto-reconcilated record found zero records"
  REC-174 - "Internal error: the expected deposit ID in not in the correct format"
  REC-175 - "The 'File Import Location' field in the Bank Deposit Reconciliation page of iPayment Config is not set to a valid directory"
  REC-176 - "Cannot Void reconciliation"
  REC-177 - "Cannot update bank deposit new flags"
  REC-178 - "Internal error: cannot get bank deposit total amount because database identifiers were not found"
  REC-179 - "Internal error: cannot get expected deposit total amount because database identifiers were not found"
  REC-180 - "Cannot find the earliest reconciled deposit to set the Reconciliation Cutoff Period"
  REC-181 - "Automatch failed: Cannot retrieve the last auto-reconciled record"
  REC-182 - "Cannot open bank deposit table to validate checksums"
  REC-183 - unused
  REC-184 - "Cannot process bank deposit table to validate checksums"
  REC-185 - "Checksum failure reading the bank deposit details list from TG_BANK_DEPOSIT_DATA table",
  REC-186 - "Checksum failure reading the bank deposit details from TG_BANK_DEPOSIT_DATA table"
  REC-187 - "Checksum failure reading the bank deposits list from the TG_BANK_DEPOSIT_DATA table",
  REC-188 - "Cannot read details for bank deposit from reconciliation ID"
  REC-189 - "Checksum failure reading bank deposit details from the ID. "
  REC-190 - "Cannot update bank status flag and checksum"
  REC-191 - "Checksum failure when reading Reconciled Deposit list from the TG_RECONCILED_DATA table. Error detected in record: "
  REC-192 - "Cannot read the bank deposit IDs"
  REC-193 - "Checksum error reading the TG_RECONCILED_DATA table"
  REC-194 - "Cannot update checksum in reconciliation table"
  REC-195 - "Error updating bank deposit checksum"
  REC-196 - "Automatch failed: Cannot read last bank match key records"
  REC-197 - "Automatch failed: Cannot collate bank match key records"
  REC-198 - "Error clearing the Bank Reconciliation Tables"
  REC-199 - "Automatch failed: Cannot collate non-binned bank match key records"
  REC-200 - "Error retrieving match key from expected deposit table"
  REC-201 - "Bank Deposit Reconciliation GetBankDepositsAsJSArray Error.  Bank Account:"
  REC-202 - "Bank Deposit Reconciliation GetBankDepositsAsJSArray.  Cannot generate JS array. Error: "
  REC-203 - "Exception reading the bank deposit details for deposit {0}: see the CS log for details: {1}
  REC-204 - "Bank Deposit Reconciliation GetReconciledDepositsAsJSArray Error"
  REC-205 - "Bank Deposit Reconciliation GetAllAccounts Error:"
  REC-206 - "Bank Deposit Reconciliation GetExpectedDepositsAsJSArray Error"
  REC-207 - "Bank Deposit Reconciliation AutoMatch Error: "
  REC-208 - "Bank Deposit Reconciliation GetExpectedDeposits Error: "
  REC-209 - "Bank Deposit Reconciliation GetExpectedDepositDetails Error: "
  REC-210 - "Bank Deposit Reconciliation GetBankDepositList Error:"
  REC-211 - "Bank Deposit Reconciliation GetBankDepositDetails Error: "
  REC-212 - "Bank Deposit Reconciliation GetReconciledDeposits Error: "
  REC-213 - "Bank Deposit Reconciliation GetReconciledDepositListForReporting Error: "
  REC-214 - "Bank Deposit Reconciliation GetReconciliationDetails Error: "
  REC-215 - "Bank Deposit Reconciliation ReconcileBankIgnore Error: "
  REC-216 - "Bank Deposit Reconciliation ReconcileExpectedIgnore Error: "
  REC-217 - "Bank Deposit Reconciliation ReconcileForced Error: "
  REC-218 - "Bank Deposit Reconciliation GetBankAccounts Error: "
  REC-219 - "Bank Deposit Reconciliation ImportBankFiles Error:"
  REC-220 - "Bank Deposit Reconciliation Get Details Error: "
  REC-221 - "Bank Deposit Reconciliation Cannot Read the BANK_REC_DEPOSIT_SLIP_PREFIXES field value list: "
 */

namespace recon
{

  public partial class ReconcileDeposits
  {
    /// <summary>
    /// The subdirectory to hold archive processed (imported) bank files.
    /// </summary>
    private const String m_processedSubDirectory = "ProcessedFiles";

    /// <summary>
    /// The subdirectory to hold BAI/Bank files that did not parse or
    /// could not be save to the database
    /// </summary>
    private const String m_erroneousSubDirectory = "FailedFiles";

    /// <summary>
    /// Cache the connection info here
    /// </summary>
    private GenericObject m_dbConnection;

    /// <summary>
    /// Cache the Config DB connection GEO here.
    /// Bug 9640: Thurber
    /// </summary>
    private GenericObject m_configConnection;

    /// <summary>
    /// Field widths for the TG_BANK_DEPOSIT_DATA table.
    /// These will be read from the database
    /// </summary>
    private int m_bankDepositTextFieldLength, m_accountIdLenBankTable, m_refNumLenBankTable, m_bankReferenceLenBankTable;

    /// <summary>
    /// The maximum number of records to pull from the database
    /// </summary>
    ///
    private const String DATABASE_SELECT_LIMIT = "512";

    /// <summary>
    /// Cache the real bank account numbers here so we
    /// do not bang on the database for every account.
    /// </summary>
    private Dictionary<String, String> m_cachedBankAccounts;

    /// <summary>
    /// Reverse lookup cache for BankIDs based on accounts.
    /// </summary>
    private Dictionary<String, String> m_cachedBankIDs;

    /// <summary>
    /// Schema information queried with GetSchemaTable to be used in clipping fields.
    /// </summary>
    private int m_userIdLenReconciledTable, m_commentLenReconciledTable;

    /// <summary>
    /// Special character used to separate the bank account ID 
    /// from the slip (reference) number when doing automatches.
    /// Bug 10497.
    /// </summary>
    private const char m_bankAccount_ref_delimiter = '|';

    private const string m_unsetAccountNbr = "(Unconfigured Account)";
    private const string m_unsetBankAccountNbr = "(Unset Bank Account)";

    /// <summary>
    /// Bug 17200: collect the filtered count for the Activity Log
    /// </summary>
    private int m_filteredRecordCount = 0;

    /// <summary>
    /// Constructor for Bank Reconciliation.
    /// This ctor will set the configuration variables from the passed argPairs.
    /// The following config variables are used:
    /// <list type="bullet>"
    /// <item>db_connection: Database connection information</item>
    /// <item>bai_codes: A comma separated list of BAI "Type Codes" of interest.  
    /// Only records with these codes will be pulled in when importing Bank/BAI files.  
    /// The default is to pull in all the codes.</item>
    /// </list>
    /// <list>import_file_type: The type of parse/import needed to import files such as "Group Health", "CSV_header", etc.</list>
    /// <list>database_select_limit: The number of records returned for typical queries such as reconciled deposits.  Default of 512.</list>
    /// </summary>
    /// <param name="argPairs"></param>
    public ReconcileDeposits(params object[] argPairs)
    {
      m_cachedBankAccounts = new Dictionary<String, String>();
      m_cachedBankIDs = new Dictionary<String, String>();

      if (argPairs == null)
      {
        throw new Exception("ReconcileDeposits constructor called with no params");
      }

      GenericObject geoParams = misc.convert_args(argPairs);

      // Bug 9640
      m_configConnection = geoParams.get("db_config_connection", c_CASL.c_undefined) as GenericObject;

      m_dbConnection = (GenericObject)geoParams.get("db_connection");
      if (m_dbConnection == null)
      {
        throw new Exception("db_connection not in params passed to ReconcileDeposits constructor");
      }

      // Read the real size of the fields in the TG_BANK_DEPOSIT_DATA table
      getBankTableFieldSizes(out m_accountIdLenBankTable, out m_refNumLenBankTable, out m_bankReferenceLenBankTable, out m_bankDepositTextFieldLength);

      // Get the field sizes for the TG_RECONCILED_DATA table
      getDepositTableFieldSizes(out m_userIdLenReconciledTable, out m_commentLenReconciledTable);
    }


    /// <summary>
    /// Get a default cutoff date for reconcilation candidates.
    /// Bug 9934: FT: Changed this to not use the global
    /// </summary>
    private DateTime getDefaultExpectedStartDate()
    {
      // If the date is unset or in a bad format, try other approaches
      ReconStatus status;
      DateTime expectedStartDate = getEarliestDeposit(out status);
      // If it fails, consider all dates
      if (!status.success)
      {
        return DateTime.MinValue;
      }

      // roll date back by a fudge factor of 2 weeks
      expectedStartDate = expectedStartDate - TimeSpan.FromDays(14);
      return expectedStartDate;
    }

    private DateTime getEarliestDeposit(out ReconStatus status)
    {
      status = new ReconStatus(true);

      DateTime twoWeeksAgo = DateTime.Now.Subtract(TimeSpan.FromDays(14));
      DateTime firstBankDeposit = DateTime.Now;

      String reconSql = "SELECT MIN(DEPOSIT_DATE) as FIRST_DEPOSIT FROM TG_BANK_DEPOSIT_DATA";

      SqlDataReader dataReader = null;
      try
      {
        dataReader = setupReader(reconSql, ref status);

        if (dataReader.Read())
        {
          firstBankDeposit = dataReader.IsDBNull(0) ? twoWeeksAgo : dataReader.GetDateTime(0);
          return firstBankDeposit;
        }
        else
        {
          return twoWeeksAgo;
        }

      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in getEarliestDeposit", e);

        status.setErr("Cannot find the earliest reconciled deposit to set the Reconciliation Cutoff Period",
            String.Format("getEarliestDeposit failed: Error: {0}: Traceback: {1}", e, e.StackTrace),
            "REC-180", false);
      }
      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
      }
      return twoWeeksAgo;
    }

    /// <summary>
    /// Retrieve a list of the Slip/reference number for a deposit.
    /// First check to see if a slip number can be found in the bank deposits
    /// table.  Failing that then check the expected deposits table.
    /// Re-wrote for BUG 9692: FT: Get all the ref nums from both tables and merge & sort 
    /// and elminate duplicates or blank/null numbers.  This will look better when formated 
    /// in a single column in the Reconciliations page.
    /// </summary>
    /// <param name="databaseID"></param>
    /// <param name="status"></param>
    /// <returns></returns>    
    private String getBankRefNumbers(DatabaseID databaseID, out ReconStatus status)
    {
      status = new ReconStatus(true);
      var bankList = new List<String>();
      var expectedList = new List<String>();

      // First see if there are any bank slip numbers for this recon
      // Bug 11099: Change this to a normal join instead of IN query
      String reconSql =
        @"SELECT REF_NUM FROM TG_BANK_DEPOSIT_DATA bank, TG_RECONCILED_BANK_DATA xref 
          WHERE bank.BANK_DEPOSIT_ID = xref.BANK_DEPOSIT_ID
          AND xref.RECON_ID = @reconID";

      SqlDataReader dataReader = null;
      try
      {
        dataReader = setupReader(reconSql, ref status,
             param("reconID", SqlDbType.Int, databaseID.databaseKey));

        while (dataReader.Read())
        {
          String refNum = dataReader.GetString(0);
          if (refNum.Length > 0)
            bankList.Add(refNum);
        }

        // Close and re-use for expected ref_num's
        dataReader.Close();

        // Next see if there are any expected slip numbers
        reconSql =
          @"SELECT DEPSLIPNBR FROM TG_DEPOSIT_DATA as expected, TG_RECONCILED_EXPECTED_DATA as xref WHERE
	                expected.DEPOSITID = xref.DEPOSITID 
	                AND expected.DEPOSITNBR = xref.DEPOSITNBR 
	                AND expected.UNIQUEID = xref.UNIQUEID 
	                AND xref.RECON_ID = @reconID";

        dataReader = setupReader(reconSql, ref status,
             param("reconID", SqlDbType.Int, databaseID.databaseKey));

        while (dataReader.Read())
        {
          // read the DEPSLIPNBR (not the REF_NUM!) which can be null
          if (dataReader.IsDBNull(0))
            continue;
          String refNum = dataReader.GetString(0);
          if (refNum.Length > 0)
            expectedList.Add(refNum);
        }

        // Exit early if no refs
        if (expectedList.Count == 0 && bankList.Count == 0)
          return "";

        // Merge and sort list and eliminate dups using Linq Union operator
        List<String> merged = new List<String>(bankList.Union(expectedList));
        merged.Sort();   // Just to make sure

        // Change into a comma-separated list
        String refs = String.Join(",", merged.ToArray());

        return refs;
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in getBankRefNumbers", e);

        status.setErr("Failure reading the reference / slip number from the BANK_DEPOSIT or DEPOSIT_DATA table",
            String.Format("Failure reading the REF_NUM/DEPSLIPNBR from the TG_BANK_DEPOSIT_DATA or TG_DEPOSIT_DATA table {0} sql: {1}", e.ToString(), reconSql),
            "REC-149", true);
        return "";
      }
      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
      }
    }

    public void testTrace(out ReconStatus status)
    {
      status = new ReconStatus(true);
      status.setErr("Human: test trace", "detailed: test trace", "no-op-1", true);
    }

    /// <summary>
    /// Change the status of a reconciliation to void ('v').  
    /// Note that this will leave the reconcilation details in the 
    /// TG_RECONCILED_EXPECTED_DATA and TG_RECONCILED_BANK_DATA as is.
    /// BUG 9592: Thurber: Changed this to make sure that the BANK_DEPOSIT_DATA's status
    /// field is changed back to 'u'.
    /// </summary>
    /// <param name="id">Unique ID for this reconcilation</param>
    /// <param name="status">Output parameter for the error status</param>
    public void reconciliationUndo(DatabaseID reconId, out ReconStatus status)
    {
      status = new ReconStatus(true);

      String undoReconSql = @"UPDATE TG_RECONCILED_DATA 
                                  SET STATUS = 'v',
                                  CHECKSUM = @checksum 
                                WHERE RECON_ID = @reconID";

      // Bug 11099: Added RECONCILED_STATUS to DEPOSIT_DATA to speed up queries
      String undoExpected =
                            @"UPDATE TG_DEPOSIT_DATA set RECONCILED_STATUS = 'v'
                              FROM TG_DEPOSIT_DATA ddata, TG_RECONCILED_EXPECTED_DATA xref
                              WHERE 
	                              ddata.DEPOSITID = xref.DEPOSITID AND
	                              ddata.DEPOSITNBR = xref.DEPOSITNBR AND
	                              ddata.UNIQUEID = xref.UNIQUEID AND
	                              xref.recon_id = @reconID";
      // End 11099

      String undoBankSql = @"UPDATE TG_BANK_DEPOSIT_DATA 
                                SET STATUS = @status,
                                CHECKSUM = @checksum 
                              WHERE BANK_DEPOSIT_ID  = @bankID";


      // Cache the connections here so that finally can close
      SqlConnection updateConnection = new SqlConnection();
      SqlTransaction transaction = null;
      try
      {
        // Fetch the reconciled record so we can update the checksum
        ReconciledDeposit reconDetails = getReconciledDeposit(reconId, out status);
        reconDetails.reconStatus = ReconciledDeposit.rStatus.voided;
        reconDetails.computeChecksum();


        // Open a connection for the update
        updateConnection.ConnectionString = (String)m_dbConnection.get("db_connection_string");
        updateConnection.Open();
        SqlCommand updateReconCommand = new SqlCommand(undoReconSql, updateConnection);
        updateReconCommand.Parameters.Add(new SqlParameter("@checksum", SqlDbType.VarChar));
        updateReconCommand.Parameters.Add(new SqlParameter("@reconID", SqlDbType.Int));
        updateReconCommand.Parameters[0].Value = reconDetails.checksum;
        // Bug #12055 Mike O - Use misc.Parse to log errors
        updateReconCommand.Parameters[1].Value = misc.Parse<Int32>(reconId.databaseKey);

        // Bug 11099: Set RECONCILED_STATUS in DEPOSIT_DATA 
        SqlCommand updateExpectedCommand = new SqlCommand(undoExpected, updateConnection);
        updateExpectedCommand.Parameters.Add(new SqlParameter("@reconID", SqlDbType.Int));
        updateExpectedCommand.Parameters[0].Value = Convert.ToInt32(reconId.databaseKey);
        // End bug 11099

        SqlCommand updateBankCommand = new SqlCommand(undoBankSql, updateConnection);
        updateBankCommand.Parameters.Add(new SqlParameter("@status", SqlDbType.VarChar));
        updateBankCommand.Parameters.Add(new SqlParameter("@checksum", SqlDbType.VarChar));
        updateBankCommand.Parameters.Add(new SqlParameter("@bankID", SqlDbType.Int));

        transaction = updateConnection.BeginTransaction();

        updateReconCommand.Transaction = transaction;
        updateExpectedCommand.Transaction = transaction;   // Bug 11099
        updateBankCommand.Transaction = transaction;

        updateReconCommand.ExecuteNonQuery();

        // Bug 11099: This query can update 0 or more expected records to change reconciled_status to 'v'
        updateExpectedCommand.ExecuteNonQuery();

        // Now update the bank deposit records changing the status 'r' back to 'u'
        // First get a list of the bank deposits corresponding with this recon
        List<int> bankIds = getVoidedBankDeposit(reconId, out status);

        // Loop through each deposit and reset the status and checksum
        foreach (int id in bankIds)
        {
          // Fetch the entire record so we can recompute the checksum
          BankDepositDetails bankDetails = getBankDepositDetails(new DatabaseID(id), out status);
          bankDetails.depositStatus = BankDeposit.bStatus.unreconciled;
          bankDetails.computeChecksum();
          updateBankCommand.Parameters[0].Value = BankDeposit.convertStatus(bankDetails.depositStatus);
          updateBankCommand.Parameters[1].Value = bankDetails.checksum;
          updateBankCommand.Parameters[2].Value = id;
          updateBankCommand.ExecuteNonQuery();
        }

        transaction.Commit();
      }
      catch (Exception e)
      {
        bool rolledBack = false;
        if (transaction != null)
        {
          transaction.Rollback();
          rolledBack = true;
        }

        // Bug 17849
        Logger.cs_log_trace("Error in reconciliationUndo", e);

        status.setErr("Cannot Void reconciliation",
          "Reconciliation undo failed: " + e.ToString() + " : rolled back successfully: " + rolledBack,
          "REC-176", false);
      }
      finally
      {
        if (updateConnection != null)
          updateConnection.Close();
      }
    }

    /// <summary>
    /// Get a list of all the available database fields associated with this database handle.
    /// </summary>
    /// <param name="id">Opaque database handle</param>
    /// <returns>ReconcilationDetails or null if it could not be found</returns>
    public ReconciledDepositDetails getReconciliationDetails(DatabaseID id, GenericObject deptID2MatchKey, out ReconStatus status)
    {
      status = new ReconStatus(true);
      var details = new ReconciledDepositDetails();
      try
      {

        // Get a base ReconciledDepositDetails object
        details = getReconciledDeposit(id, out status);
        if (!status.success)
          return details;

        // Bug 11467
        bool matchKey = false;
        if (details.reconType == ReconciledDeposit.rType.matchkey)
          matchKey = true;

        // Add the bank deposit details
        List<BankDepositDetails> bankDetailsList = getBankDepositDetailsList(id, matchKey);
        details.addBankDetailList(bankDetailsList);

        // Add the expected deposit details
        List<ExpectedDepositDetails> expectedDetailsList = getExpectedDepositDetailsList(id, matchKey, deptID2MatchKey);
        details.addExpectedDepositDetailsList(expectedDetailsList);

      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in getReconciliationDetails", e);

        String msg = "Cannot read the reconcilation details from the database for Database ID: " + id.databaseKey;
        status.setErr(msg,
            String.Format(msg + ".  Exception {0}: {1}: ", e.ToString(), e.StackTrace),
            "REC-107", false);
      }
      return details;
    }

    /// <summary>
    /// Get the <b>base</b> information from the TG_RECONCILED_DATA table
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    private ReconciledDepositDetails getReconciledDeposit(DatabaseID id, out ReconStatus status)
    {
      status = new ReconStatus(true);
      var reconDetails = new ReconciledDepositDetails();

      String reconSql = "SELECT * FROM TG_RECONCILED_DATA WHERE RECON_ID = @databaseID";

      SqlDataReader dataReader = null;
      try
      {
        dataReader = setupReader(reconSql, ref status,
            param("databaseID", SqlDbType.Int, id.databaseKey)
          );

        // There was an error in the SQL format but this was not set!?
        if (!status.success)
        {
          status.setErr("Cannot read reconcilation details",
              String.Format(@"Cannot query the TG_RECONCILED_DATA table for reconciliation details: 
                                    SQL: {0}: database ID: {1}", reconSql, id.databaseKey),
              "REC-135", true);
          return reconDetails;
        }


        if (!dataReader.HasRows)
        {
          status.setErr("No reconcilation details found",
              String.Format(@"No records found in TG_RECONCILED_DATA table for reconciliation details: 
                                    SQL: {0}: database ID: {1}", reconSql, id.databaseKey),
              "REC-136", true);
          return reconDetails;
        }

        dataReader.Read();
        readReconRecord(reconDetails, dataReader);
        if (!reconDetails.validateChecksum())
        {
          // Bug 11467: setting the ref number to the match key will screw up the checksum 
          // so had to hack/change this to only put out a message.
          // TODO: fix this situation in CASL--do not overwrite the ref number with the match key
          // Bug 12303: FT: Log checksum error to activity log
          string recordInfo = string.Format("ID:{0}", id.databaseKey);
          status.setChecksumError("TG_RECONCILED_DATA: Checksum failed for "
            + recordInfo
            + ": "
            + reconDetails.ToString(),
          "REC-191");
          TranSuiteServices.TranSuiteServices.LogChecksumError("TG_RECONCILED_DATA", "GetReconciledDeposit", recordInfo, reconDetails.ToAuditString());
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in getReconciledDeposit", e);

        status.setErr("Failed to read reconciled deposits details",
            String.Format("getReconciledDeposit failed: Error: {0}: Traceback: {1}", e, e.StackTrace),
            "REC-134", false);
      }
      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
      }
      return reconDetails;
    }

    /// <summary>
    /// Do all the work for reading deposit details out of a DataReader results.
    /// </summary>
    /// <param name="reconDetails">Output parameter: fill this object with fields from database</param>
    /// <param name="dataReader"></param>
    private void readReconRecord(ReconciledDepositDetails reconDetails, SqlDataReader dataReader)
    {
      int commentOrd = dataReader.GetOrdinal("COMMENT");
      int overshortOrd = dataReader.GetOrdinal("OVERSHORT");
      int checksumOrd = dataReader.GetOrdinal("CHECKSUM");

      reconDetails.databaseID = new DatabaseID(dataReader.GetInt32(dataReader.GetOrdinal("RECON_ID")));
      reconDetails.user = dataReader.GetString(dataReader.GetOrdinal("USERID"));

      String comment = dataReader.IsDBNull(commentOrd) ? "" : dataReader.GetString(commentOrd);
      reconDetails.reason = comment;

      reconDetails.reconDate = dataReader.GetDateTime(dataReader.GetOrdinal("RECON_DATE"));

      String reconStatus = dataReader.GetString(dataReader.GetOrdinal("STATUS"));
      reconDetails.reconStatus = ReconciledDeposit.convertReconStatus(reconStatus);

      String reconType = dataReader.GetString(dataReader.GetOrdinal("TYPE"));
      reconDetails.reconType = ReconciledDeposit.convertReconType(reconType);

      double totalAmount = dataReader.GetDouble(dataReader.GetOrdinal("TOTAL_AMOUNT"));
      // Bug #12055 Mike O - Use misc.Parse to log errors
      reconDetails.amount = misc.Parse<Decimal>(totalAmount);

      double overShort = dataReader.IsDBNull(overshortOrd) ? 0.00 : dataReader.GetDouble(overshortOrd);
      // Bug #12055 Mike O - Use misc.Parse to log errors
      reconDetails.overShort = misc.Parse<Decimal>(overShort);

      int reconId = dataReader.GetInt32(dataReader.GetOrdinal("RECON_ID"));
      var status = new ReconStatus(true);
      reconDetails.refNum = getBankRefNumbers(new DatabaseID(reconId), out status);

      reconDetails.checksum = dataReader.IsDBNull(checksumOrd) ? "" : dataReader.GetString(checksumOrd);
    }


    /// <summary>
    /// Join expected deposit table (TG_DEPOSIT_DATA)and 
    /// reconciled (TG_RECONCILED_EXPECTED_DATA)table based on database ID.
    /// to get the expected deposit details.
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    private List<ExpectedDepositDetails> getExpectedDepositDetailsList(DatabaseID id, bool matchKey, GenericObject deptID2MatchKey)
    {
      var status = new ReconStatus(true);
      // Bug 11549: hadd to change this to pull the DEPTID from the depfile table
      String expectedDepSql = @"
        SELECT TG_DEPOSIT_DATA.DEPOSITID, TG_DEPOSIT_DATA.DEPOSITNBR, TG_DEPOSIT_DATA.UNIQUEID, DEPSLIPNBR, BANKID, TG_DEPOSIT_DATA.DEPFILENBR, POSTDT, AMOUNT, DEPTID, TG_DEPOSIT_DATA.DEPFILESEQ
          FROM TG_DEPOSIT_DATA, TG_RECONCILED_EXPECTED_DATA, TG_DEPFILE_DATA 
	        WHERE
            TG_DEPOSIT_DATA.DEPOSITID = TG_RECONCILED_EXPECTED_DATA.DEPOSITID 
            AND TG_DEPOSIT_DATA.DEPOSITNBR = TG_RECONCILED_EXPECTED_DATA.DEPOSITNBR 
            AND TG_DEPOSIT_DATA.UNIQUEID = TG_RECONCILED_EXPECTED_DATA.UNIQUEID
		        AND TG_DEPOSIT_DATA.DEPFILENBR = TG_DEPFILE_DATA.DEPFILENBR
		        AND TG_DEPOSIT_DATA.DEPFILESEQ = TG_DEPFILE_DATA.DEPFILESEQ
            AND TG_RECONCILED_EXPECTED_DATA.RECON_ID = @databaseID";

      List<ExpectedDepositDetails> expectedDepositDetailsList = new List<ExpectedDepositDetails>();

      SqlDataReader dataReader = null;
      try
      {
        Dictionary<int, List<string>> expectedSlipNumbers = getExpectedReconciledSlipNumbers(deptID2MatchKey);

        dataReader = setupReader(expectedDepSql, ref status,
            param("databaseID", SqlDbType.Int, id.databaseKey)
          );
        if (!status.success)
        {
          throw new Exception(status.detailedError + " : " + status.code);
        }

        // Cache field ordinals
        int slipOrd = dataReader.GetOrdinal("DEPSLIPNBR");
        int bankIdOrd = dataReader.GetOrdinal("BANKID");
        int fileNbrOrd = dataReader.GetOrdinal("DEPFILENBR");
        int fileSeqOrd = dataReader.GetOrdinal("DEPFILESEQ");
        int postDtOrd = dataReader.GetOrdinal("POSTDT");
        int amountOrd = dataReader.GetOrdinal("AMOUNT");
        // Bug 11549
        int deptidOrdinal = dataReader.GetOrdinal("DEPTID");

        while (dataReader.Read())
        {
          ExpectedDepositDetails ed = new ExpectedDepositDetails();
          String depositId = dataReader.GetString(dataReader.GetOrdinal("DEPOSITID"));
          int depositNbr = dataReader.GetInt32(dataReader.GetOrdinal("DEPOSITNBR"));
          ed.databaseID = new DatabaseID(
              depositId,
              depositNbr,
              dataReader.GetInt32(dataReader.GetOrdinal("UNIQUEID"))
              );
          ed.refNum = dataReader.IsDBNull(slipOrd) ? "" : dataReader.GetString(slipOrd);
          // Bug 11467
          if (matchKey)
            ed.refNum = getExpectedMatchKey(ed.databaseID, null);

          String bankID = dataReader.IsDBNull(bankIdOrd) ? "" : dataReader.GetString(bankIdOrd);

          // Bug 15157
          String deptID = dataReader.IsDBNull(deptidOrdinal) ? "" : dataReader.GetString(deptidOrdinal);        // "DEPTID from tg_depfile_data"
          ed.workgroup = deptID;
          // End Bug 15157
          if (ed.refNum == "")  // Bug 11549
          {
            string merchantKey = getWorkgroupMatchKeyUPHS(deptID2MatchKey, deptID, bankID);
            ed.refNum = merchantKey; // Could still be ""
          }

          bool reconcilable;   // Bug 10635: FT
          ed.account = bankID2Account(bankID, out reconcilable, ref status);
          // Skip records that do not have reconcilable set in the bank account config
          if (!reconcilable)
            continue;

          double amount = dataReader.IsDBNull(amountOrd) ? 0.0f : dataReader.GetDouble(amountOrd);
          // Bug #12055 Mike O - Use misc.Parse to log errors
          ed.amount = misc.Parse<Decimal>(amount);

          // Bug 15157.  Add the sequence number to the filename
          string depfilenbr = Convert.ToString(dataReader.IsDBNull(fileNbrOrd) ? 0 : dataReader.GetInt32(fileNbrOrd));
          string depfileseq = dataReader.IsDBNull(fileSeqOrd) ? "" : dataReader.GetInt32(fileSeqOrd).ToString("D3");
          // End Bug 15157
          ed.file = depfilenbr + depfileseq;
          // Get posting date or NOW if not set.  
          ed.depDate = dataReader.IsDBNull(postDtOrd) ? DateTime.Now : dataReader.GetDateTime(postDtOrd);
          ed.age = calculateAge(ed.depDate);
          // Bug 9562: Thurber:  The tenders member variable was not being set and defaulted to null
          ReconStatus tenderStatus;
          ed.tenders = getTenderList(depositId, depositNbr, out tenderStatus);
          // TODO: deal with a bad status
          expectedDepositDetailsList.Add(ed);


        }
        return expectedDepositDetailsList;
      }
      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
      }
    }

    /// <summary>
    /// Calculate the number of <b>working</b> days since today.
    /// Note: Only counts holidays up through 2015
    /// </summary>
    /// <param name="depositDate"></param>
    /// <returns></returns>
    private int calculateAge(DateTime depositDate)
    {
      int workDays = 0;
      DateTime now = System.DateTime.Now;

      while (depositDate.ToFileTime() <= now.ToFileTime())
      {
        // Skip weekends and holidays
        // TODO: Extend for 2016, 2017, etc.
        if (!isAWorkDay(depositDate))
        {
          depositDate = depositDate.AddDays(1);
          continue;
        }
        workDays++;
        depositDate = depositDate.AddDays(1);
      }

      return workDays;
    }

    /// <summary>
    /// Jump ahead the specified number of days.
    /// <seealso cref="http://172.16.1.141/bugzilla/show_bug.cgi?id=12326"/>
    /// </summary>
    /// <param name="expectedDay"></param>
    /// <param name="lagDays"></param>
    /// <returns></returns>
    private DateTime jumpAheadNWorkDays(DateTime expectedDay, int lagDays)
    {
      // Jump ahead the number of lag days
      while (lagDays > 0)
      {
        TimeSpan oneDay = new TimeSpan(1, 0, 0, 0);
        expectedDay += oneDay;
        bool workDay = isAWorkDay(expectedDay);
        if (workDay)
          lagDays--;
      }
      return expectedDay;
    }

    /// <summary>
    /// Determine if the day is a weekend or holiday.
    /// Based on Federal Holidays:
    /// <seealso cref="http://www.opm.gov/Operating_Status_Schedules/fedhol/2016.asp">http://www.opm.gov/Operating_Status_Schedules/fedhol/2016.asp</seealso>
    /// </summary>
    /// <param name="day"></param>
    /// <returns>False if it is a weekend or holiday, true otherwise</returns>
    bool isAWorkDay(DateTime day)
    {
      if (day.DayOfWeek == DayOfWeek.Sunday ||
          day.DayOfWeek == DayOfWeek.Saturday
        ||
        (day.Year == 2012 && day.Month == 1 && day.Day == 2) ||  // New years
        (day.Year == 2012 && day.Month == 1 && day.Day == 16) ||  // M. Luther
        (day.Year == 2012 && day.Month == 2 && day.Day == 20) || // Presidents
        (day.Year == 2012 && day.Month == 5 && day.Day == 28) || // Memorial
        (day.Year == 2012 && day.Month == 7 && day.Day == 4) || // July 4
        (day.Year == 2012 && day.Month == 9 && day.Day == 3) || // Labor
        (day.Year == 2012 && day.Month == 10 && day.Day == 8) || // Columbus
        (day.Year == 2012 && day.Month == 11 && day.Day == 12) || // Vets
        (day.Year == 2012 && day.Month == 11 && day.Day == 22) || // Thanks
        (day.Year == 2012 && day.Month == 12 && day.Day == 25)  // XMas
        ||
        (day.Year == 2013 && day.Month == 1 && day.Day == 1) ||  // New years
        (day.Year == 2013 && day.Month == 1 && day.Day == 21) ||  // M. Luther
        (day.Year == 2013 && day.Month == 2 && day.Day == 18) || // Presidents
        (day.Year == 2013 && day.Month == 5 && day.Day == 27) || // Memorial
        (day.Year == 2013 && day.Month == 7 && day.Day == 4) || // July 4
        (day.Year == 2013 && day.Month == 9 && day.Day == 2) || // Labor
        (day.Year == 2013 && day.Month == 10 && day.Day == 14) || // Columbus
        (day.Year == 2013 && day.Month == 11 && day.Day == 11) || // Vets
        (day.Year == 2013 && day.Month == 11 && day.Day == 28) || // Thanks
        (day.Year == 2013 && day.Month == 12 && day.Day == 25)  // XMas
        ||
        (day.Year == 2014 && day.Month == 1 && day.Day == 1) ||  // New years
        (day.Year == 2014 && day.Month == 1 && day.Day == 20) ||  // M. Luther
        (day.Year == 2014 && day.Month == 2 && day.Day == 17) || // Presidents
        (day.Year == 2014 && day.Month == 5 && day.Day == 26) || // Memorial
        (day.Year == 2014 && day.Month == 7 && day.Day == 4) || // July 4
        (day.Year == 2014 && day.Month == 9 && day.Day == 1) || // Labor
        (day.Year == 2014 && day.Month == 10 && day.Day == 13) || // Columbus
        (day.Year == 2014 && day.Month == 11 && day.Day == 11) || // Vets
        (day.Year == 2014 && day.Month == 11 && day.Day == 27) || // Thanks
        (day.Year == 2014 && day.Month == 12 && day.Day == 25)  // XMas
        ||
        (day.Year == 2015 && day.Month == 1 && day.Day == 1) ||  // New years
        (day.Year == 2015 && day.Month == 1 && day.Day == 19) ||  // M. Luther
        (day.Year == 2015 && day.Month == 2 && day.Day == 16) || // Presidents
        (day.Year == 2015 && day.Month == 5 && day.Day == 25) || // Memorial
        (day.Year == 2015 && day.Month == 7 && day.Day == 3) || // July 4
        (day.Year == 2015 && day.Month == 9 && day.Day == 7) || // Labor
        (day.Year == 2015 && day.Month == 10 && day.Day == 12) || // Columbus
        (day.Year == 2015 && day.Month == 11 && day.Day == 11) || // Vets
        (day.Year == 2015 && day.Month == 11 && day.Day == 26) || // Thanks
        (day.Year == 2015 && day.Month == 12 && day.Day == 25)  // XMas
        ||
        (day.Year == 2016 && day.Month == 1 && day.Day == 1) ||  // New years
        (day.Year == 2016 && day.Month == 1 && day.Day == 18) ||  // M. Luther
        (day.Year == 2016 && day.Month == 2 && day.Day == 15) || // Presidents
        (day.Year == 2016 && day.Month == 5 && day.Day == 30) || // Memorial
        (day.Year == 2016 && day.Month == 7 && day.Day == 4) || // July 4
        (day.Year == 2016 && day.Month == 9 && day.Day == 5) || // Labor
        (day.Year == 2016 && day.Month == 10 && day.Day == 10) || // Columbus
        (day.Year == 2016 && day.Month == 11 && day.Day == 11) || // Vets
        (day.Year == 2016 && day.Month == 11 && day.Day == 24) || // Thanks
        (day.Year == 2016 && day.Month == 12 && day.Day == 26)  // XMas
        ||
        (day.Year == 2017 && day.Month == 1 && day.Day == 1) ||  // New years
        (day.Year == 2017 && day.Month == 1 && day.Day == 16) ||  // M. Luther
        (day.Year == 2017 && day.Month == 2 && day.Day == 20) || // Presidents
        (day.Year == 2017 && day.Month == 5 && day.Day == 29) || // Memorial
        (day.Year == 2017 && day.Month == 7 && day.Day == 4) || // July 4
        (day.Year == 2017 && day.Month == 9 && day.Day == 4) || // Labor
        (day.Year == 2017 && day.Month == 10 && day.Day == 9) || // Columbus
        (day.Year == 2017 && day.Month == 11 && day.Day == 10) || // Vets
        (day.Year == 2017 && day.Month == 11 && day.Day == 23) || // Thanks
        (day.Year == 2017 && day.Month == 12 && day.Day == 25)  // XMas
        ||
        (day.Year == 2018 && day.Month == 1 && day.Day == 1) ||  // New years
        (day.Year == 2018 && day.Month == 1 && day.Day == 15) ||  // M. Luther
        (day.Year == 2018 && day.Month == 2 && day.Day == 19) || // Presidents
        (day.Year == 2018 && day.Month == 5 && day.Day == 28) || // Memorial
        (day.Year == 2018 && day.Month == 7 && day.Day == 4) || // July 4
        (day.Year == 2018 && day.Month == 9 && day.Day == 3) || // Labor
        (day.Year == 2018 && day.Month == 10 && day.Day == 8) || // Columbus
        (day.Year == 2018 && day.Month == 11 && day.Day == 12) || // Vets
        (day.Year == 2018 && day.Month == 11 && day.Day == 22) || // Thanks
        (day.Year == 2018 && day.Month == 12 && day.Day == 25)  // XMas
        ||
        (day.Year == 2019 && day.Month == 1 && day.Day == 1) ||  // New years
        (day.Year == 2019 && day.Month == 1 && day.Day == 21) ||  // M. Luther
        (day.Year == 2019 && day.Month == 2 && day.Day == 18) || // Presidents
        (day.Year == 2019 && day.Month == 5 && day.Day == 27) || // Memorial
        (day.Year == 2019 && day.Month == 7 && day.Day == 4) || // July 4
        (day.Year == 2019 && day.Month == 9 && day.Day == 2) || // Labor
        (day.Year == 2019 && day.Month == 10 && day.Day == 14) || // Columbus
        (day.Year == 2019 && day.Month == 11 && day.Day == 11) || // Vets
        (day.Year == 2019 && day.Month == 11 && day.Day == 28) || // Thanks
        (day.Year == 2019 && day.Month == 12 && day.Day == 25)  // XMas
        ||
        (day.Year == 2020 && day.Month == 1 && day.Day == 1) ||  // New years
        (day.Year == 2020 && day.Month == 1 && day.Day == 20) ||  // M. Luther
        (day.Year == 2020 && day.Month == 2 && day.Day == 17) || // Presidents
        (day.Year == 2020 && day.Month == 5 && day.Day == 25) || // Memorial
        (day.Year == 2020 && day.Month == 7 && day.Day == 3) || // July 4
        (day.Year == 2020 && day.Month == 9 && day.Day == 7) || // Labor
        (day.Year == 2020 && day.Month == 10 && day.Day == 12) || // Columbus
        (day.Year == 2020 && day.Month == 11 && day.Day == 11) || // Vets
        (day.Year == 2020 && day.Month == 11 && day.Day == 26) || // Thanks
        (day.Year == 2020 && day.Month == 12 && day.Day == 25)  // XMas
        ||
        (day.Year == 2021 && day.Month == 1 && day.Day == 1) ||  // New years
        (day.Year == 2021 && day.Month == 1 && day.Day == 18) ||  // M. Luther
        (day.Year == 2021 && day.Month == 2 && day.Day == 15) || // Presidents
        (day.Year == 2021 && day.Month == 5 && day.Day == 31) || // Memorial
        (day.Year == 2021 && day.Month == 7 && day.Day == 5) || // July 4
        (day.Year == 2021 && day.Month == 9 && day.Day == 6) || // Labor
        (day.Year == 2021 && day.Month == 10 && day.Day == 11) || // Columbus
        (day.Year == 2021 && day.Month == 11 && day.Day == 11) || // Vets
        (day.Year == 2021 && day.Month == 11 && day.Day == 25) || // Thanks
        (day.Year == 2021 && day.Month == 12 && day.Day == 31)  // XMas
        ||
        (day.Year == 2022 && day.Month == 1 && day.Day == 1) ||  // New years
        (day.Year == 2022 && day.Month == 1 && day.Day == 17) ||  // M. Luther
        (day.Year == 2022 && day.Month == 2 && day.Day == 21) || // Presidents
        (day.Year == 2022 && day.Month == 5 && day.Day == 30) || // Memorial
        (day.Year == 2022 && day.Month == 7 && day.Day == 4) || // July 4
        (day.Year == 2022 && day.Month == 9 && day.Day == 5) || // Labor
        (day.Year == 2022 && day.Month == 10 && day.Day == 10) || // Columbus
        (day.Year == 2022 && day.Month == 11 && day.Day == 11) || // Vets
        (day.Year == 2022 && day.Month == 11 && day.Day == 24) || // Thanks
        (day.Year == 2022 && day.Month == 12 && day.Day == 25)  // XMas
        )
        return false;
      else
        return true;
    }


    /// <summary>
    /// Get the details for a bank deposit
    /// TODO: change this to call getBankDepositDetails for each record!
    /// </summary>
    /// <param name="id">Database record key</param>
    /// <returns></returns>
    private List<BankDepositDetails> getBankDepositDetailsList(DatabaseID id, bool matchKey)
    {
      ReconStatus status = new ReconStatus(true);

      // Bug 11467: FT: Setup regex pattern matcher here
      string recon_regex = (string)((GenericObject)c_CASL.c_object("Business.bank_deposit_recon.data")).get("recon_regex", "");
      Regex regexPattern = null;
      if (recon_regex != null && recon_regex.Length > 0)
        regexPattern = new Regex(recon_regex);

      var bankDetailsList = new List<BankDepositDetails>();
      String bankDepSql = @"
                SELECT * FROM TG_BANK_DEPOSIT_DATA WHERE BANK_DEPOSIT_ID IN (
	                SELECT BANK_DEPOSIT_ID FROM TG_RECONCILED_BANK_DATA 
		                WHERE RECON_ID = @databaseID)";

      SqlDataReader dataReader = null;
      try
      {
        // Bug #12055 Mike O - Use misc.Parse to log errors
        dataReader = setupReader(bankDepSql, ref status, param("databaseID", SqlDbType.Int, misc.Parse<Int32>(id.databaseKey)));

        if (!status.success)
        {
          throw new Exception("Cannot query the TG_BANK_DEPOSIT table for deposit details: " + status.detailedError);
        }

        // Cache frequently-used field positions
        int idOrd = dataReader.GetOrdinal("BANK_DEPOSIT_ID");
        int refOrd = dataReader.GetOrdinal("REF_NUM");
        int textOrd = dataReader.GetOrdinal("TEXT");
        int bankRefOrd = dataReader.GetOrdinal("BANK_REFERENCE");
        int checksumOrd = dataReader.GetOrdinal("CHECKSUM");
        int statusOrd = dataReader.GetOrdinal("STATUS");

        while (dataReader.Read())
        {
          BankDepositDetails bd = new BankDepositDetails();
          bd.databaseID = new DatabaseID(dataReader.GetInt32(idOrd));
          bd.depDate = dataReader.GetDateTime(dataReader.GetOrdinal("DEPOSIT_DATE"));
          bd.age = calculateAge(bd.depDate);
          bd.account = dataReader.GetString(dataReader.GetOrdinal("ACCOUNT_ID"));
          bd.refNum = dataReader.GetString(refOrd);
          // Bug #12055 Mike O - Use misc.Parse to log errors
          bd.amount = misc.Parse<Decimal>(dataReader.GetDouble(dataReader.GetOrdinal("AMOUNT")));
          String bankStatus = dataReader.GetString(statusOrd);
          bd.depositStatus = BankDeposit.convertStatus(bankStatus);
          bd.text = dataReader.IsDBNull(textOrd) ? "" : dataReader.GetString(textOrd);
          bd.bankReference = dataReader.IsDBNull(bankRefOrd) ? "" : dataReader.GetString(bankRefOrd);
          bd.checksum = dataReader.IsDBNull(checksumOrd) ? "" : dataReader.GetString(checksumOrd);

          if (!bd.validateChecksum())
          {
            // Bug 12303: FT: Log checksum error to activity log
            status.setChecksumError("TG_BANK_DEPOSIT_DATA: Checksum failure: " + bd.ToString(),
              "REC-185");
            string recordInfo = string.Format("ID:{0}", bd.databaseID.databaseKey);
            TranSuiteServices.TranSuiteServices.LogChecksumError("TG_BANK_DEPOSIT_DATA", "GetBankDepositDetailsList", recordInfo, bd.ToAuditString());
          }

          // Bug 11467: FT: Extract match key if it is there
          // Do this after the checksum!
          if (matchKey)
          {
            bd.matchKey = "";
            if (regexPattern != null)
            {
              Match match = regexPattern.Match(bd.text);
              if (match.Success)
              {
                String matchKeyString = match.Groups[1].Value;
                bd.matchKey = matchKeyString;
                // A bit dodgy, but it is easier than figuring this out in CASL
                bd.refNum = bd.matchKey;
              }
            }
          }

          bankDetailsList.Add(bd);
        }
        return bankDetailsList;
      }
      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
      }
    }


    /// <summary>
    /// Automatically reconciles deposits.
    /// The ref/slip number must match and the amounts must total to the same.
    /// Compares the expected and bank deposits and tries to reconcile them.
    /// Bug 10250: FT: added the match key regex
    /// </summary>
    /// <returns>List of reconciled deposits.  Null list if none were reconciled.</returns>
    ///
    public List<ReconciledDeposit> autoMatch(String user, GenericObject deptID2MatchKey, String recon_regex, out ReconStatus status)
    {
      status = new ReconStatus(true);

      // Bug 10262: FT: Keep a running list of automatch info
      AutomatchReports reporter = new AutomatchReports();

      List<ReconciledDeposit> reconciled = new List<ReconciledDeposit>();
      // Bug 10497: Bin amounts on BANKID plus DEPSLIPNBR instead of just DEPSLIPNBR
      try
      {
        // Get all the bank deposits grouped by ref/slip # and total amounts
        // and put in a Dictionary for fast lookup to prevent N-Squared compare
        // Bug 11417: Stash original ref number in bucket
        Dictionary<String, AmountBankIDBucket> bankDeposits = getBankSlipTotals();

        // Now get a list of the ref / amount pairs for expected deposits
        List<KeyValuePair<String, AmountBankIDBucket>> expectedDeposits = getExpectedSlipTotals(reporter);

        // Bin slip numbers that are the same if leading zeros are stripped
        List<KeyValuePair<String, AmountBankIDBucket>> compressedExpectedDeposits = foldLeadingZeroSlipNumbers(expectedDeposits);

        // Compare the expected against the bank
        foreach (KeyValuePair<String, AmountBankIDBucket> expectedDeposit in compressedExpectedDeposits)
        {
          // Bug 10497: tease apart bankId_plus_RefNum to get the account id plus refNum
          // The bin'ing is now done on these two values.
          String expectedDepositKey = expectedDeposit.Key;
          String[] keyParts = expectedDepositKey.Split(m_bankAccount_ref_delimiter);
          String accountID = keyParts[0];
          String strippedRefNum = keyParts[1];
          // End bug 10497

          // Skip null ref numbers
          if (strippedRefNum == null || strippedRefNum == "")
          {
            // Bug 10262: Report on any automatch misses
            // Bug 11169: Changed to write each line to the file
            reporter.textReport("No match: ", expectedDepositKey,
              " expected deposit slip number is not set.");
            reporter.matchReport(AutomatchReports.MatchAction.no_match, accountID, strippedRefNum, null, "Expected deposit slip number is null");
            continue;
          }

          // Lookup ref number in Bank refs. Skip if the ref number is not found in the bank map.
          // Bug 11417/11422: Changed to bucket to preserve orig. ref number
          AmountBankIDBucket bankBucket;
          if (!bankDeposits.TryGetValue(expectedDepositKey, out bankBucket))
          {
            // Bug 10262: Report on any automatch misses
            // Bug 11169: Changed to write each line to the file
            var bucket = expectedDeposit.Value;   // Bug 11417 / 11422: Pull amount from bucket
            reporter.textReport("No match: For Expected Deposit(s) for Account: '",
              accountID,
              "' (Bank ID: ",
              bucket.bankID,
              ")\tslip: '",
              bucket.originalRefNum,
              "' for total amount: ",
              String.Format("{0:C}", bucket.amount),
              " not in bank deposit list.");
            reporter.matchReport(AutomatchReports.MatchAction.no_match, accountID, strippedRefNum, bucket.amount, "Expected deposit not in bank deposit list");
            continue;
          }


          // Bug 11422/11417 Pull amount from bucket
          Decimal bankTotalAmount = bankBucket.amount;

          // Skip if Amounts do not match
          // Bug 11422/11417 Pull amount from bucket
          AmountBankIDBucket amountBankIDBucket = expectedDeposit.Value;
          Decimal expectedTotalAmount = amountBankIDBucket.amount;
          if (expectedTotalAmount != bankTotalAmount)
          {
            // Bug 10262: Report on any automatch misses
            // Bug 11169: Changed to write each line to the file
            reporter.textReport("Account: ", accountID,
              " and Bank ID: ", amountBankIDBucket.bankID,          // Bug 18513
              " and slip number: ", strippedRefNum,
              " match, but expected total: ", String.Format("{0:C}", expectedTotalAmount),
              " does not match bank total: ", String.Format("{0:C}", bankTotalAmount));

            reporter.matchReport(AutomatchReports.MatchAction.no_match, accountID, strippedRefNum, expectedTotalAmount,
              "Account and slip number match but total expected amount: " + String.Format("{0:C}", expectedTotalAmount)
              + " does not match bank total: " + String.Format("{0:C}", bankTotalAmount));
            continue;
          }

          // Bug 22128: Do not match bank deposit that are earlier than expected deposit
          if (expectedDeposit.Value.depositDate > bankBucket.depositDate)
          {
              reporter.textReport("Account: ", accountID,
              " and Bank ID: ", amountBankIDBucket.bankID,          // Bug 18513
              " and slip number: ", strippedRefNum,
              " and amount: ", String.Format("{0:C}", expectedTotalAmount),
              " match, but Bank Deposit took place before the Expected Deposit: "
              + "Last Expected Deposit took place on: '" + expectedDeposit.Value.depositDate + "'"
              + " while the first Bank Deposit took place on: '" + bankBucket.depositDate + "'");

              reporter.matchReport(AutomatchReports.MatchAction.no_match, accountID, strippedRefNum, expectedTotalAmount,
                "Account, slip number, and totals match but the Bank Deposit took place before the Expected Deposit: "
              + "Last Expected Deposit took place on: '" + expectedDeposit.Value.depositDate + "'"
              + " while the first Bank Deposit took place on: '" + bankBucket.depositDate + "'"
              );
              continue;
          }
          // End Bug 22128


          // Reference number and amounts match: Match found!
          // Bug 10497: Bin with Bank_ID plus REF_NUM, not just REF_NUM
          // Bug 11417 and 11422:  Preserve orginal bankid's and refnums
          List<DatabaseID> expectedIDs = getExpectedIDs(amountBankIDBucket.bankID, amountBankIDBucket.originalRefNum);
          List<DatabaseID> bankIDs = getBankIDs(accountID, bankBucket.originalRefNum);
          // End 10497
          reconcileAuto(expectedIDs, bankIDs, user, out status);
          // Bail out immediatly if there is an error. 
          if (!status.success)
          {
            status.humanReadableError = "Automatch failed: " + status.humanReadableError;
            // Bug 10262: Report on any automatch misses
            // Bug 11169: Changed to write each line to the file
            reporter.textReport("Records matched but reconciliation failed: " + status.detailedError);
            reporter.matchReport(AutomatchReports.MatchAction.error, accountID, strippedRefNum, null, "Record matched but reconciliation failed: " + status.detailedError);
            return reconciled;
          }

          ReconciledDeposit recon = getLastReconciledRecord(out status);
          // Bail out immediatly if there is an error.  Is this correct?
          if (!status.success)
          {
            // Bug 10262: Report on any automatch misses
            // Bug 11169: Changed to write each line to the file
            reporter.textReport("AutoMatch get last record failed:", status.detailedError);
            reporter.matchReport(AutomatchReports.MatchAction.error, accountID, strippedRefNum, null, "AutoMatch get last record failed:" + status.detailedError);
            return reconciled;
          }

          // Bug 10262: Report on successful automatches
          // Bug 11169: Changed to write each line to the file
          reporter.textReport("Matched: Bank account: ", accountID, " slip number: ", strippedRefNum,
            " total amount: ", String.Format("{0:C}", bankTotalAmount));
          reporter.matchReport(AutomatchReports.MatchAction.match, accountID, strippedRefNum, bankTotalAmount, "");
          reconciled.Add(recon);

          // Bug 11417 / 21970: Remove matched bank record from hash table so it does NOT match again! Whew, what a greivous error....
          bool bankRecordDeleted = bankDeposits.Remove(expectedDepositKey);
          if (!bankRecordDeleted)
          {
            Logger.cs_log("Warning: autoMatch() Failed trying to remove this record from the bankDeposits hash: '" + expectedDepositKey + "'");
          }
        }

        reportUnreconciledBankDeposits(reporter, bankDeposits, reconciled);

        // Bug 10250: FT: Second pass trying to match against the bank recon key
        /*
        autoMatchBankReconKey(user, deptID2MatchKey, recon_regex, reconciled, reporter, out status);
        if (!status.success)
        {
          // Bug 10262: Report on any automatch misses
          // Bug 11169: Changed to write each line to the file
          reporter.textReport("AutoMatch failed:",status.detailedError);
          reporter.matchReport(AutomatchReports.MatchAction.error, "", "", null, "AutoMatch reconcilation key failed:" + status.detailedError);
          return reconciled;
        }
         * */

        // This 
        // nonBinnedAutoMatchBankReconKey(user, deptID2MatchKey, recon_regex, reconciled, reporter, out status);

        // Bug 21882, sort of: You can turn the second pass on and off with the enable_automatch_second_pass option
        string doSecondPassString = getConfigValue("enable_automatch_second_pass", "false");
        bool doSecondPass = Convert.ToBoolean(doSecondPassString);

        if (doSecondPass)
        {
          autoMatchBankReconKeyDateBinned(user, deptID2MatchKey, recon_regex, reconciled, reporter, out status);
        }


        // Change the color of remaining entries in the Bank Deposit list from green to white
        clearBankStatusNewFlags(ref status);
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in Auto reconcilation", e);

        status.setErr("Automatch failed",
            String.Format("Auto reconcilation failed: Error: {0}: Traceback: {1}", e, e.StackTrace),
            "REC-109", false);
      }
      finally
      {
        // Bug 10262
        // Bug 11169: Changed to write each line to the file
        reporter.writeAutomatchReport(this);
      }
      return reconciled;
    }

    /// <summary>
    /// Bug 11417, 21882
    /// If there are slip numbers that match, but have different numbers of leading zeros, they will have 
    /// separate entries in the expectedDeposit list.  This method will fold together these separate entries.
    /// This is not the most efficient way to do it; I could convert the slip number to a numeric in the database query and 
    /// GROUP BY that, but would be tricky to implement.  If performance/memory is an issue, then revisit that, but it will
    /// put more stress on the DB.
    /// </summary>
    /// <param name="expectedDeposits"></param>
    /// <returns></returns>
    private List<KeyValuePair<string, AmountBankIDBucket>> foldLeadingZeroSlipNumbers(List<KeyValuePair<string, AmountBankIDBucket>> expectedDeposits)
    {
      List<KeyValuePair<string, AmountBankIDBucket>> foldedList = new List<KeyValuePair<string,AmountBankIDBucket>>();

      Dictionary<string, AmountBankIDBucket> slipMatch = new Dictionary<string, AmountBankIDBucket>();

      // Look for duplicate keys and squeeze them into one
      foreach (KeyValuePair<String, AmountBankIDBucket> expectedDeposit in expectedDeposits)
      {
        String expectedDepositKey = expectedDeposit.Key;
        AmountBankIDBucket amountBucket;
        if (slipMatch.TryGetValue(expectedDepositKey, out amountBucket))
        {
          // Collision: do the fold
          amountBucket.amount = Decimal.Add(amountBucket.amount, expectedDeposit.Value.amount);
        }
        else
        {
          // Unique key, add to hash
          slipMatch[expectedDepositKey] = expectedDeposit.Value;
        }

      }

      // Now output the compressed list
      foreach (KeyValuePair<String, AmountBankIDBucket> expectedDeposit in slipMatch)
      {
        foldedList.Add(expectedDeposit);
      }

      return foldedList;
    }






    /// <summary>
    /// Get the reconciliation information for display in the GUI for each
    /// deposit that was reconciled.
    /// </summary>
    /// <param name="status"></param>
    /// <returns></returns>
    private ReconciledDeposit getLastReconciledRecord(out ReconStatus status)
    {
      status = new ReconStatus(true);
      var reconDetails = new ReconciledDepositDetails();

      String reconSql = "SELECT * FROM TG_RECONCILED_DATA WHERE RECON_ID = IDENT_Current('TG_RECONCILED_DATA')";

      SqlDataReader dataReader = null;
      try
      {
        dataReader = setupReader(reconSql, ref status);

        // There was an error in the SQL format but this was not set!?
        if (!status.success)
        {
          status.setErr("Automatch failed: Cannot read last auto-reconcilated record",
              String.Format(@"Cannot query the TG_RECONCILED_DATA table for expected reconciliation details: 
                                    SQL: {0}", reconSql),
              "REC-172", true);
          return reconDetails;
        }


        if (!dataReader.HasRows)
        {
          status.setErr("Automatch failed: When reading the last auto-reconcilated record found zero records",
              String.Format(@"No records found in TG_RECONCILED_DATA table for expected reconciliation details: 
                                    SQL: {0}", reconSql),
              "REC-173", true);
          return reconDetails;
        }

        dataReader.Read();
        readReconRecord(reconDetails, dataReader);
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in getLastReconciledRecord", e);

        status.setErr("Automatch failed: Cannot retrieve the last auto-reconciled record",
            String.Format("getLastReconciledRecord failed: Error: {0}: Traceback: {1}", e, e.StackTrace),
            "REC-181", false);
      }
      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
      }
      // Upcast since autoMatch does not need the details
      return (ReconciledDeposit)reconDetails;
    }

    /// <summary>
    /// Get the database IDs for each matching ref number
    /// </summary>
    /// <param name="accountId">The bank account ID. Bug 10497</param>
    /// <param name="refNum">The slip or reference number</param>
    /// <returns></returns>
    private List<DatabaseID> getBankIDs(String accountId, String refNum)
    {
      ReconStatus status = new ReconStatus(true);

      // Bug 9758: FT: The old SQL was too simplistic and did not take into 
      // account voided deposits so that voided deposits were returned in
      // this list causing the validation to bomb.  This SQL should be as 
      // close as possible to the one used for getBankDepositList()...
      // Bug 10497: Bin with ACCOUNT_ID plus REF_NUM, but just REF_NUM
      // Bug 11099: Simplified and optimized by removing subquery and using RECONCILED_STATUS instead
      String bankSql =
        @"SELECT BANK_DEPOSIT_ID FROM TG_BANK_DEPOSIT_DATA 
                     WHERE 
						STATUS != 'r'
						AND REF_NUM = @refNum
						AND ACCOUNT_ID = @accountId";

      if (refNum == null)
      {
        return new List<DatabaseID>();
      }


      SqlDataReader dataReader = null;
      try
      {
        dataReader = setupReader(bankSql, ref status,
          param("refNum", SqlDbType.VarChar, refNum),
          param("accountId", SqlDbType.VarChar, accountId));   // Bug 10497: Bin with ACCOUNT_ID plus REF_NUM    
        if (!status.success)
        {
          // TODO: consider changing this to return a status flag instead
          throw new Exception("Cannot query the TG_BANK_DEPOSIT_DATA for IDs for Auto Reconcilation for this ref: " + refNum);
        }

        var bankIDList = new List<DatabaseID>();
        while (dataReader.Read())
        {
          int bankDepositID = dataReader.GetInt32(0);
          bankIDList.Add(new DatabaseID(bankDepositID));
        }
        return bankIDList;
      }
      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
      }
    }

    /// <summary>
    /// Get the list of database IDs (keys) from the expected deposits for a reference number.
    /// Used by auto-match.
    /// </summary>
    /// <param name="accountID">Use the BANKID as an extra discriminator. Bug 10497</param>
    /// <param name="refNum"></param>
    /// <returns></returns>
    private List<DatabaseID> getExpectedIDs(string accountID, string refNum)
    {
      ReconStatus status = new ReconStatus(true);

      DateTime expectedStartDate = getExpectedStartDate();      // Bug 18317: Make sure we respect the Expect Deposit Sutoff date

      // Bug 9758: FT: The old SQL was too simplistic and did not take into 
      // account voided deposits so that voided deposits were returned in
      // this list causing the validation to bomb.  This SQL should be as 
      // close as possible to the one used for getExpectedDepositList()...
      // Bug 10497: Filtered based on BANKID for the new automatch based BANKID and DEPSLIPNBR
      // Bug 11099: Simplified and optimized by removing subquery and using RECONCILED_STATUS instead
      String expectedSql =
        @"SELECT distinct DEPOSITID, DEPOSITNBR, UNIQUEID 
            FROM TG_DEPOSIT_DATA 
              WHERE isNull(RECONCILED_STATUS,'u') != 'r'            
              AND DEPSLIPNBR =  @refNum   -- Bug 23125 / 23051: Leading zeros will be stripped from BAIs and in the Deposit module so do this strip now at automatch time. Let's first do not strip at all.  Also see bug 21882
              AND BANKID = @bankID 
              AND DEPOSITTYPE = 'FILE' AND VOIDDT is null
	            AND POSTDT IS NOT NULL
              AND POSTDT >= @expectedStartDate
              {0}";

      //  Bug 10635: FT: Also need to filter out accounts that are not reconcilable.
      // We do not want to automatch those.
      String unReconcilableBankIDs = getUnreconcilableBankIDs("");
      expectedSql = String.Format(expectedSql, unReconcilableBankIDs);


      SqlDataReader dataReader = null;
      try
      {
        dataReader = setupReader(expectedSql, ref status,
          param("refNum", SqlDbType.VarChar, refNum),       // Bug 10497
          param("bankID", SqlDbType.VarChar, accountID),   // Bug 10497
          param("expectedStartDate", SqlDbType.DateTime, expectedStartDate)   // Bug 18317:
          );   
        if (!status.success)
        {
          // Consider changing this to be more friendly and return status
          throw new Exception("Cannot query the TG_DEPOSIT_DATA for IDs for Auto Reconcilation for this ref: " + refNum);
        }
        var expectedIDList = new List<DatabaseID>();

        while (dataReader.Read())
        {
          String depositID = dataReader.GetString(dataReader.GetOrdinal("DEPOSITID"));
          int depositNBR = dataReader.GetInt32(dataReader.GetOrdinal("DEPOSITNBR"));
          int uniqueID = dataReader.GetInt32(dataReader.GetOrdinal("UNIQUEID"));
          expectedIDList.Add(new DatabaseID(depositID, depositNBR, uniqueID));
        }
        return expectedIDList;
      }
      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
      }
    }


    /// <summary>
    /// Scan the bank deposit tables and group by refNum/slip and 
    /// total amount.  Needed by autoreconciliation.
    /// </summary>
    /// Bug 11417: Changed to put original refNum in bucket so it can be preserved before stripping
    /// <returns>Map of ref / total amounts</returns>
    private Dictionary<String, AmountBankIDBucket> getBankSlipTotals()
    {
      ReconStatus status = new ReconStatus(true);

      // Bug 10497: Bin amounts based on ACCOUNT_ID plus REF_NUM
      // Bug 22128: Added the EARLIEST_DEPOSIT_DATE
      String bankSql =
          @"SELECT ACCOUNT_ID, REF_NUM, SUM(AMOUNT) as TOTAL_AMOUNT, MIN(DEPOSIT_DATE) as EARLIEST_DEPOSIT_DATE
                    FROM TG_BANK_DEPOSIT_DATA
                    WHERE status != 'r'
	                GROUP BY REF_NUM, ACCOUNT_ID";      // Bug 11417: The SQL GROUP BY will NOT strip leading zeros so this could be an issue.


      // Put in map to avoid N-Squared lookups
      // Bug 11417: Preserve original refNum before stripping
      Dictionary<String, AmountBankIDBucket> bankMap = new Dictionary<String, AmountBankIDBucket>();

      SqlDataReader dataReader = null;
      try
      {
        dataReader = setupReader(bankSql, ref status);
        if (!status.success)
        {
          throw new Exception("Cannot query the TG_BANK_DEPOSIT_DATA for total amount for Auto Reconcilation: " + status.detailedError);
        }

        while (dataReader.Read())
        {
          // Bug 11417: strip leading zeros
          String originalRefNum = dataReader.GetString(dataReader.GetOrdinal("REF_NUM"));
          //String refNum = stripLeadingZeros(originalRefNum);      // Bug 25288: Do not strip leading zeros
          // Bug #12055 Mike O - Use misc.Parse to log errors
          Decimal totalAmount = misc.Parse<Decimal>(dataReader.GetDouble(dataReader.GetOrdinal("TOTAL_AMOUNT")));
          // Start Bug 10497
          String accountId = dataReader.GetString(dataReader.GetOrdinal("ACCOUNT_ID"));

          // Bug 22128: Earlier deposit date for this reference number bucket: needed for temporal proximity matching
          DateTime depositDate = dataReader.GetDateTime(dataReader.GetOrdinal("EARLIEST_DEPOSIT_DATE"));

          // Bug 11417: changed to preserve the original refNum before it is stripped.
          AmountBankIDBucket bucket = new AmountBankIDBucket(totalAmount, "", originalRefNum, depositDate);
          bankMap.Add(accountId + m_bankAccount_ref_delimiter + originalRefNum, bucket);        // Bug 25288: Do not used stripped ref numbers to avoid dictionary collisions
          // End 10497
        }
        return bankMap;
      }
      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
      }
    }

    /// <summary>
    /// Bug 11417. Need to strip leading zeros from ref numbers for UPHS because
    /// iPayment was stripping them while they were not stripped in BAI.
    /// </summary>
    /// <param name="refNum"></param>
    /// <returns></returns>
    private string stripLeadingZeros(string refNum)
    {
      // Bug: 11417--reopenned: Guard against reference numbers that are all zero!

      // First filter the null ref number entry
      if (refNum == "")
        return refNum;

      // Now strip and see what happens
      string stripped = refNum.TrimStart('0');
      if (stripped == "")
        return refNum;     // If it stripped to nothing, return the unstripped string...
      else
        return stripped;
      // End bug 11416--reopenned
    }

    /// <summary>
    /// Scans the expected table (TG_BANK_DEPOSIT) for un-reconciled 
    /// deposits and returns a list of pair of ref/slip numbers and total amounts.
    /// This is used for auto-match to match up against bank deposits. 
    /// Note: Can save memory and time if we pass the bank deposit map in here and do
    /// the comparison here thus skip the creation of the List, but it hardly seems
    /// worth the trouble.
    /// </summary>
    /// Bug 11422: Changed return value to preserve the original bank ID in the bucket
    /// <param name="report">Runing list of actions taken for AutoMatch Report.  Bug 10262</param>
    /// <returns></returns>
    /// 
    private List<KeyValuePair<string, AmountBankIDBucket>> getExpectedSlipTotals(AutomatchReports reporter)
    {
      // Bug 9610: Thurber: enchanced SQL query to parallel the one used for retrieving
      // the expected deposit list.  This includes an upgrade to recognize voided
      // reconciliations. It also switches from Balance Date to Post Date.
      // Bug 10635: FT: Filter out Pre-deposits (DEPOSITTYPE of PRE_DEPOSIT)
      // Bug 11099: Simplified and optimized by removing subquery and using RECONCILED_STATUS instead
      // Bug #14729 FT - Don't include transferred deposits (where dfile.ACC_FILENBR is not null)
      // Bug 22128: Added the LATEST_DEPOSIT_DATE
      String expectedSql =
        @"SELECT BANKID, DEPSLIPNBR, SUM(AMOUNT) as TOTAL_AMOUNT, MAX(POSTDT) as LATEST_DEPOSIT_DATE
            FROM TG_DEPOSIT_DATA as ddata, TG_DEPFILE_DATA as dfile 
              WHERE isNull(ddata.RECONCILED_STATUS,'u') != 'r'            
              AND DEPOSITTYPE = 'FILE' AND VOIDDT is null       -- Bug 10635
	            AND ddata.DEPFILENBR = dfile.DEPFILENBR
	            AND ddata.DEPFILESEQ = dfile.DEPFILESEQ
	            AND ddata.POSTDT IS NOT NULL
              AND ddata.POSTDT >= @expectedStartDate
              AND dfile.ACC_FILENBR is null     
              {0}
           GROUP BY DEPSLIPNBR, BANKID";            // The key to bin'ing by the slip number is this GROUP BY, but note that this does not handle leading zeros, bug 11417

      String unReconcilableBankIDs = getUnreconcilableBankIDs("ddata.");

      expectedSql = String.Format(expectedSql, unReconcilableBankIDs);

      // Put in list and compare to bank deposit map to avoid N-Squared lookups
      // Bug 11422: Need to preserve the original bankid number before uit is transformed to Account nbr
      var expectedList = new List<KeyValuePair<string, AmountBankIDBucket>>();

      SqlDataReader dataReader = null;

      try
      {
        ReconStatus status = new ReconStatus(true);

        // Bug 9934: FT: Query the CASL config property for the reconciliation_cutoff.
        DateTime expectedStartDate = getExpectedStartDate();

        dataReader = setupReader(expectedSql, ref status,
          param("expectedStartDate", SqlDbType.DateTime, expectedStartDate)
        );

        if (!status.success)
          throw new Exception(String.Format("Cannot get the expected slip & amount totals from TG_DEPOSIT_DATA.  Error: {0}: Code {1}",
                        status.detailedError, status.code));

        // Loop through each record one by one
        //dataReader = dataReaderCommand.ExecuteReader(CommandBehavior.CloseConnection);
        // The GetOrdinal() allows us to access column without hardwired column numbers
        int depSlipNbrOrdinal = dataReader.GetOrdinal("DEPSLIPNBR");
        int totalAmountOrdinal = dataReader.GetOrdinal("TOTAL_AMOUNT");
        int latestDepositDateOrdinal = dataReader.GetOrdinal("LATEST_DEPOSIT_DATE");   // Bug 22128: Added the LATEST_DEPOSIT_DATE
        // Bug 10497
        int bankIdOrdinal = dataReader.GetOrdinal("BANKID");
        bool reconcilable;
        ReconStatus bankIdStatus = new ReconStatus(true);
        // End 10497

        while (dataReader.Read())
        {
          double totalFloatAmount = dataReader.IsDBNull(totalAmountOrdinal) ? 0.00 : dataReader.GetDouble(totalAmountOrdinal);
          String bankId = dataReader.IsDBNull(bankIdOrdinal) ? "" : dataReader.GetString(bankIdOrdinal);
          if (dataReader.IsDBNull(depSlipNbrOrdinal))
          {
            // Bug 10262: Report on null slip numbers
            // Bug 11169: Changed to write each line to the file
            reporter.textReport("Skipped expected records on first pass for BANKID: ", bankId,
              " with a total amount of: ", String.Format("{0:C}", totalFloatAmount),
              "; slip numbers are null");
            reporter.matchReport(AutomatchReports.MatchAction.skip, bankId, "", totalFloatAmount, "Slip numbers null");
            continue;       // Skip null deposit slips; cannot automatch these
          }

          // Bug 11422: Need to preserve the original ref number before zeros are stripped
          String originalRefNum = dataReader.IsDBNull(depSlipNbrOrdinal) ? "" : dataReader.GetString(depSlipNbrOrdinal);
          string strippedRefNum = stripLeadingZeros(originalRefNum);
          // Bug #12055 Mike O - Use misc.Parse to log errors
          Decimal totalAmount = misc.Parse<Decimal>(totalFloatAmount);
          // Start Bug 10497: Bin amounts based on BANKID plus DEPSLIPNBR
          String bankAccount = bankID2Account(bankId, out reconcilable, ref bankIdStatus);
          if (!reconcilable)      // Skip non-reconcilable accounts and bogus bankIds
          {
            // Bug 11169: Changed to write each line to the file
            reporter.textReport("Skipped expected records totaling: ", String.Format("{0:C}", totalFloatAmount),
              "; account is not reconcilable or the BANKID: '",
              bankId, "' is not configured to a bank account");

            reporter.matchReport(AutomatchReports.MatchAction.skip, bankId, strippedRefNum, totalFloatAmount, "account is not reconcilable or the BANKID is not configured to a bank account");

            continue;
          }
          // Bug 22128: Lastest deposit date in this reference number bucket: needed for temporal proximity matching
          DateTime depositDate = dataReader.GetDateTime(latestDepositDateOrdinal);

          // Bug 11422: Need to preserve the orginal bankid
          AmountBankIDBucket bucket = new AmountBankIDBucket(totalAmount, bankId, originalRefNum, depositDate);
          expectedList.Add(new KeyValuePair<string, AmountBankIDBucket>(bankAccount + m_bankAccount_ref_delimiter + strippedRefNum, bucket));
          // End 10497
        }

        return expectedList;
      }
      finally
      {
        if (dataReader != null)
          dataReader.Close();
      }
    }

    /// <summary>
    /// Construct an SQL predicate to filter out bank accounts that
    /// are designated as non-reconcilable.
    /// Bug 10635.  FT.
    /// </summary>
    /// <returns>SQL Predicate string of the form "AND ddata.BANKID NOT IN (bankid1, bankid2, etc)"</returns>
    private string getUnreconcilableBankIDs(String bankdIdPrefix)
    {

      StringBuilder unreconcilable = new StringBuilder();
      unreconcilable.Append("AND ").Append(bankdIdPrefix).Append("BANKID NOT IN (");

      // First get all the account in the config
      GenericObject accountList = (GenericObject)c_CASL.c_object("Business.Bank_account.of");

      // Loop through each account and check the reconcilable value
      bool found = false;
      foreach (DictionaryEntry dirEntry in accountList)
      {
        bool reconcilable = true;
        String bankid = (String)dirEntry.Key;

        // Skip accounts such as _parent and _name (which is a string)
        if (bankid.StartsWith("_"))
          continue;

        GenericObject bankAccountGEO = (GenericObject)dirEntry.Value;


        if (bankAccountGEO.Contains("reconcilable"))
        {
          reconcilable = (bool)bankAccountGEO.get("reconcilable");
          if (reconcilable)
            continue;
        }
        else
          // Skip account without the reconcilable variable being set
          // This is being generous; we could add this to the unreconcilable list.
          continue;

        // Skip accounts without a bankID; being generous here also
        if (!bankAccountGEO.Contains("account_nbr"))
          continue;

        // Found an unreconcilable account
        if (found)
          unreconcilable.Append(", ");
        found = true;
        unreconcilable.Append("'").Append(bankid).Append("'");
      }

      if (found)
      {
        unreconcilable.Append(")");
        return unreconcilable.ToString();
      }
      else  // If not unreconcilable, do not use the predicate
        return "";
    }



    /// <summary>
    /// Return the details of a Bank Deposit
    /// </summary>
    /// <param name="databaseID">Opaque database key</param>
    /// <returns>Details or a null if it cannot be found</returns>
    public BankDepositDetails getBankDepositDetails(DatabaseID databaseID, out ReconStatus status)
    {
      status = new ReconStatus(true);

      // Bug 10250: FT: Setup regex pattern matcher here
      string recon_regex = (string)((GenericObject)c_CASL.c_object("Business.bank_deposit_recon.data")).get("recon_regex", "");
      Regex regexPattern = null;
      if (recon_regex != null && recon_regex.Length > 0)
        regexPattern = new Regex(recon_regex);

      var details = new BankDepositDetails();

      String bankSql =
          @"SELECT * FROM TG_BANK_DEPOSIT_DATA 
                    WHERE BANK_DEPOSIT_ID = @bank_deposit_id";


      SqlDataReader dataReader = null;
      try
      {
        // Bug #12055 Mike O - Use misc.Parse to log errors
        dataReader = setupReader(bankSql, ref status,
          param("bank_deposit_id", SqlDbType.Int, misc.Parse<Int32>(databaseID.databaseKey))
        );

        if (!status.success)
        {
          status.detailedError = status.detailedError +
              "Cannot read details from TG_BANK_DEPOSIT_DATA table for id: " + databaseID.databaseKey;
          return details;
        }


        if (!dataReader.HasRows)
        {
          status.setErr("Cannot read details for bank deposit",
              "Cannot read details for bank deposit: " + databaseID.databaseKey + " no records found",
              "REC-129", true);
          return details;
        }
        dataReader.Read();
        int id = dataReader.GetInt32(dataReader.GetOrdinal("BANK_DEPOSIT_ID"));
        details.databaseID = new DatabaseID(id);
        details.account = dataReader.GetString(dataReader.GetOrdinal("ACCOUNT_ID"));
        details.refNum = dataReader.GetString(dataReader.GetOrdinal("REF_NUM"));
        details.depDate = dataReader.GetDateTime(dataReader.GetOrdinal("DEPOSIT_DATE"));
        details.age = calculateAge(details.depDate);
        details.depositStatus = BankDeposit.convertStatus(dataReader.GetString(dataReader.GetOrdinal("STATUS")));
        // Bug #12055 Mike O - Use misc.Parse to log errors
        details.amount = misc.Parse<Decimal>(dataReader.GetDouble(dataReader.GetOrdinal("AMOUNT")));
        int textOrd = dataReader.GetOrdinal("TEXT");
        details.text = dataReader.IsDBNull(textOrd) ? "" : dataReader.GetString(textOrd);
        int bankRefOrd = dataReader.GetOrdinal("BANK_REFERENCE");
        details.bankReference = dataReader.IsDBNull(bankRefOrd) ? "" : dataReader.GetString(bankRefOrd);

        // Bug 10250: FT: Extract match key if it is there
        details.matchKey = "";
        if (regexPattern != null)
        {
          Match match = regexPattern.Match(details.text);
          if (match.Success)
          {
            String matchKey = match.Groups[1].Value;
            details.matchKey = matchKey;
          }
        }

        // Bug 18982
        int importChecksumOrd = dataReader.GetOrdinal("IMPORT_TIMESTAMP");
        details.importTimestamp = dataReader.IsDBNull(importChecksumOrd) ? DateTime.MinValue : dataReader.GetDateTime(importChecksumOrd);
        // End Bug 18982

        int checksumOrd = dataReader.GetOrdinal("CHECKSUM");
        details.checksum = dataReader.IsDBNull(checksumOrd) ? "" : dataReader.GetString(checksumOrd);

        if (!details.validateChecksum())
        {
          // Bug 12303: FT: Log checksum error to activity log
          status.setChecksumError("TG_BANK_DEPOSIT_DATA Checksum failure on: " + details.ToString(),
            "REC-186");
          string recordInfo = string.Format("ID:{0}", details.databaseID.databaseKey);
          TranSuiteServices.TranSuiteServices.LogChecksumError("TG_BANK_DEPOSIT_DATA", "GetBankDepositDetails", recordInfo, details.ToAuditString());
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in getLastReconciledRecord", e);

        status.setErr("Cannot read bank deposit details and convert data",
            String.Format("getBankDepositDetails failed: SQL or conversion error: Error: {0}: Traceback: {1}",
                e, e.StackTrace),
            "REC-128", false);
      }
      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
      }

      return details;
    }

    /// <summary>
    /// Get the expected deposits from the TG_DEPOSIT_DATA.  Do not return them all,
    /// just the one that have not been reconciled.
    /// Join the TG_DEPOSIT_DATA and TG_RECONCILED_EXPECTED_DATA to look for expected
    /// deposits that have not been reconciled.
    /// <param name="deptID2MatchKey">Needed for refNum--> match ckey Bug 11411</param>
    /// </summary>
    public List<ExpectedDeposit> getExpectedDepositList(string bank_account, GenericObject deptID2MatchKey, string workgroup_id, out ReconStatus status)
    {
      // Bug 11473: Pass in bank account number so it can be filtered on server side
      var bankAccounts = new List<string>();
      if (bank_account != "")
        bankAccounts.Add(bank_account);
      return getExpectedDepositList(bankAccounts, deptID2MatchKey, workgroup_id, out status);
      // End Bug 11473
    }

    /// <summary>
    /// Unified version of getExpectedDepositList.
    /// Get the expected deposits from the TG_DEPOSIT_DATA.  Do not return them all,
    /// just the one that have not been reconciled.
    /// Join the TG_DEPOSIT_DATA and TG_RECONCILED_EXPECTED_DATA to look for expected
    /// deposits that have not been reconciled.
    /// This version uses native API calls to the database instead of using Matt's
    /// transuite code to avoid failures when complexity of the SQL got too high.
    /// 
    /// </summary>
    /// <param name="bankAccounts"></param>
    /// <param name="deptID2MatchKey">Needed for refNum--> match ckey Bug 11411</param>
    /// <param name="status"></param>
    /// <returns></returns>
    private List<ExpectedDeposit> getExpectedDepositList(List<String> bankAccounts, GenericObject deptID2MatchKey, string workgroup_id, out ReconStatus status)
    {
      status = new ReconStatus(true);

      // Bug 9934: FT: Query the CASL config property for the reconciliation_cutoff.
      DateTime expectedStartDate = getExpectedStartDate();

      DateTime expectedEndDate = getExpectedEndDate(expectedStartDate);

      List<ExpectedDeposit> expectedList = new List<ExpectedDeposit>();

      // Make sure that changes to this query are reflected in the 
      // query used in getExpectedSlipTotals()!  See bug 9610.
      // These queries MUST be similar.
      // Bug 11099: Simplified and optimized by removing the subquery and using the RECONCILED_STATUS instead
      // Bug 11411: Pulled DEPTID to get match key
      // Bug 11473: use Effective date (EFFECTIVEDT) instead of POSTDT
      // Bug 11479: Added {2} to insert option workgroup id filter predicate
      // Bug #14729 Mike O - Don't include transferred deposits (where dfile.ACC_FILENBR is not null)
      String dataReaderSQL = @"SELECT TOP({3}) DEPOSITID, DEPOSITNBR, UNIQUEID, AMOUNT, DEPSLIPNBR, BANKID, EFFECTIVEDT, ddata.DEPFILENBR, ddata.DEPFILESEQ, DEPTID
            FROM TG_DEPOSIT_DATA as ddata, TG_DEPFILE_DATA as dfile 
              WHERE isNull(ddata.RECONCILED_STATUS,'u') != 'r'            
              AND DEPOSITTYPE = 'FILE' AND VOIDDT is null
	            AND ddata.DEPFILENBR = dfile.DEPFILENBR
	            AND ddata.DEPFILESEQ = dfile.DEPFILESEQ
              {0}                 
              AND EFFECTIVEDT >= @expectedStartDate
              AND EFFECTIVEDT <= @expectedEndDate
              {2}                 
              {1}
              AND dfile.ACC_FILENBR is null
       ORDER BY EFFECTIVEDT DESC";  // Bug 11115: Fix sort order

      // Filter by work group if specified
      string workGroupPredicate = createWorkgroupPredicate(workgroup_id);

      SqlDataReader dataReader = null;

      // Bug 10635: FT: Added predicate to filter out non-reconcilable accounts 
      String unReconcilableBankIDs = getUnreconcilableBankIDs("ddata.");

      bool bankIdFound;
      String bankIdPredicate = getBankIdPredicate(bankAccounts, out bankIdFound);
      if (!bankIdFound)
      {
        // No expected deposits with this bank account
        return expectedList;
      }

      dataReaderSQL = String.Format(dataReaderSQL,
        bankAccounts.Count == 0 ? "" : bankIdPredicate, 
        unReconcilableBankIDs, 
        workGroupPredicate, 
        getDBLimit());   // Bug 11473, 11479, 27450

      try
      {
        //System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
        //watch.Start();

        // Loop through each record one by one
        if (bankAccounts.Count == 0)  // Bug 11473
          dataReader = setupReader(dataReaderSQL, ref status,
          param("expectedStartDate", SqlDbType.DateTime, expectedStartDate),
          param("expectedEndDate", SqlDbType.DateTime, expectedEndDate)
        );
        else
        {
          //ReconStatus a2IDStatus = new ReconStatus(true);
          //String bankID = bankAccount2bankID(bankAccounts[0], ref a2IDStatus);
          // Bug 27450: Do not parameterize TOP
          dataReader = setupReader(dataReaderSQL, ref status,
            param("expectedStartDate", SqlDbType.DateTime, expectedStartDate),
            param("expectedEndDate", SqlDbType.DateTime, expectedEndDate)   
          );
        }
        if (!status.success)
          return expectedList;

        //watch.Stop();
        //Logger.cs_log("Expected list query took this many Milliseconds: " + watch.ElapsedMilliseconds);

        while (dataReader.Read())
        {
          ExpectedDeposit rd = new ExpectedDeposit();
          // Read a record
          // Consider using this syntax: dataReader.GetString(dataReader.GetOrdinal("DEPOSITID"));
          // Be very, very careful to not change the order of these fields in the select
          // 0: DEPOSITID, 1: DEPOSITNBR, 2: UNIQUEID, 3: AMOUNT, 4: DEPSLIPNBR, 5:BANKID, 6:EFFECTIVEDT, 7: ddata.DEPFILENBR, 8: ddata.DEPFILESEQ
          rd.databaseID = new DatabaseID(
            dataReader.GetString(0),                      // DEPOSITID
            dataReader.GetInt32(1),                       // DEPOSITNBR
            dataReader.GetInt32(2));                      // UNIQUEID
          double amount = dataReader.IsDBNull(3) ? 0.00 : dataReader.GetDouble(3);  // "AMOUNT"
          // Bug #12055 Mike O - Use misc.Parse to log errors
          rd.amount = misc.Parse<Decimal>(amount);                                  // "AMOUNT"
          rd.refNum = dataReader.IsDBNull(4) ? "" : dataReader.GetString(4);           // "DEPSLIPNBR"
          String bankID = dataReader.IsDBNull(5) ? "" : dataReader.GetString(5);       // "BANKID"
          // Bug 15157
          String deptID = dataReader.IsDBNull(9) ? "" : dataReader.GetString(9);       // "DEPTID": Bug 23467: Fixed the lineup
          rd.workgroup = deptID;
          // End 15157
          // Bug 11411: If ref num is not set, set to match key
          if (rd.refNum == "")
          {
            string merchantKey = getWorkgroupMatchKeyUPHS(deptID2MatchKey, deptID, bankID);
            rd.refNum = merchantKey; // Could still be ""
          }

          bool reconcilable;    // Bug 10635: FT
          rd.account = bankID2Account(bankID, out reconcilable, ref status);
          // Skip records that do not have reconcilable set in the bank account config
          if (!reconcilable)
            continue;
          rd.depDate = dataReader.IsDBNull(6) ? DateTime.Now : dataReader.GetDateTime(6);  // Bug 11473: use Effective date
          rd.age = calculateAge(rd.depDate);                                           //  Bug 11473: use Effective date

          rd.file =
              Convert.ToString(dataReader.GetInt32(7))    // ddata.DEPFILENBR
            // Bug: 9616: Thurber: no dash; use leading zeros
              + dataReader.GetInt32(8).ToString("D3"); // ddata.DEPFILESEQ

          expectedList.Add(rd);
        }
      }
      finally
      {
        if (dataReader != null)
          dataReader.Close();
      }
      return expectedList;
    }

    /// <summary>
    /// Filter the expected deposits based on the bank account.
    /// Bug 11479.
    /// </summary>
    /// <param name="bankAccounts"></param>
    /// <returns></returns>
    private string getBankIdPredicate(List<string> bankAccounts, out bool bankIdFound)
    {
      bankIdFound = true;
      if (bankAccounts.Count == 0)
        return "";

      ReconStatus status = new ReconStatus(true);
      string bankAccount = bankAccounts[0];
      List<string> bankIDs = bankAccount2bankID(bankAccount, ref status);
      if (!status.success)
      {
        string msg = "Internal Error: cannot query the bankid";
        Logger.cs_log(msg);
        throw new Exception(msg);
      }
      // This account is not in the expected list.  
      // Do not show any expected deposits.
      if (bankIDs.Count == 0)
      {
        bankIdFound = false;
        return "";
      }

      bool first = true;
      StringBuilder predicate = new StringBuilder();
      predicate.Append(" AND BANKID IN (");
      foreach (string bankId in bankIDs)
      {
        if (first)
        {
          predicate.Append("'").Append(bankId).Append("'");
          first = false;
        }
        else
        {
          predicate.Append(',').Append("'").Append(bankId).Append("'");
        }
      }
      predicate.Append(")");
      return predicate.ToString();
    }

    /// <summary>
    /// Set up workgroup filter predicate for expected query.
    /// Bug 
    /// </summary>
    /// <param name="deptID2MatchKey"></param>
    /// <param name="workgroup_id"></param>
    /// <returns></returns>
    private string createWorkgroupPredicate(string deptid)
    {
      if (deptid == "")
        return "";

      return "AND DEPTID = '" + deptid + "'";
    }


    /// <summary>
    /// Detailed information on an Expected Deposit including the tender totals
    /// </summary>
    /// <param name="id">Database handle</param>
    /// <returns>Details or a null if it cannot be found</returns>
    public ExpectedDepositDetails getExpectedDepositDetails(DatabaseID id, GenericObject deptId2matchKey, out ReconStatus status)
    {
      status = new ReconStatus(true);

      // Bug 11549: Needed to join with the DEPFILE to get the DEPTID for the match key
      String detailsSql =
        @"SELECT BANKID, POSTDT, AMOUNT, TG_DEPOSIT_DATA.DEPFILENBR, 
		        TG_DEPOSIT_DATA.DEPFILESEQ, DEPSLIPNBR,
		        COMMENTS, CREATERUSERID, OWNERUSERID, TG_DEPOSIT_DATA.GROUPID, TG_DEPOSIT_DATA.GROUPNBR,
		        TG_DEPOSIT_DATA.ACC_USERID, SOURCE_REFID, VOIDDT, VOIDUSERID, DEPTID
	        FROM TG_DEPOSIT_DATA, TG_DEPFILE_DATA 
          WHERE DEPOSITID = @depositID
            AND DEPOSITNBR = @depositNbr 
            AND UNIQUEID = @uniqueId
		        AND TG_DEPOSIT_DATA.DEPFILENBR = TG_DEPFILE_DATA.DEPFILENBR
		        AND TG_DEPOSIT_DATA.DEPFILESEQ = TG_DEPFILE_DATA.DEPFILESEQ";


      var details = new ExpectedDepositDetails();

      String depositId;
      int depositNbr, uniqueId;
      id.parseExpectedKey(out depositId, out depositNbr, out uniqueId, ref status);
      if (!status.success)
        return details;

      SqlDataReader dataReader = null;
      try
      {
        dataReader = setupReader(detailsSql, ref status,
          param("depositID", SqlDbType.VarChar, depositId),
          param("depositNbr", SqlDbType.Int, depositNbr),
          param("uniqueId", SqlDbType.Int, uniqueId)
          );

        if (!status.success)
        {
          status.detailedError = status.detailedError +
              "Cannot read details from TG_DEPOSIT_DATA table for id: " + id.databaseKey;
          return details;
        }


        if (!dataReader.HasRows)
        {
          status.setErr("Cannot read details for expected deposit",
              "Cannot read details for expected deposit: " + id.databaseKey + " no records found",
              "REC-129", true);
          return details;
        }

        dataReader.Read();

        details.databaseID = id;

        int ord = dataReader.GetOrdinal("BANKID");
        String bankID = dataReader.IsDBNull(ord) ? "" : dataReader.GetString(ord);
        bool reconcilable;   // Bug 10635: FT
        details.account = bankID2Account(bankID, out reconcilable, ref status);
        // Don't worry about reconcilable; all of these records should have it set to true
        if (!reconcilable)
        {
          Logger.cs_log("Warning: Bank Deposit Reconciliation error: geting details on a record that is not reconcilable: Record: "
            + depositId + ": " + depositNbr);
        }

        ord = dataReader.GetOrdinal("POSTDT");   // Bug 9564: Thurber: ACCDT was being used before
        details.depDate = dataReader.IsDBNull(ord) ? DateTime.Now : dataReader.GetDateTime(ord);
        details.age = calculateAge(details.depDate);

        ord = dataReader.GetOrdinal("AMOUNT");
        // Bug #12055 Mike O - Use misc.Parse to log errors
        details.amount = misc.Parse<Decimal>(dataReader.IsDBNull(ord) ? 0.00 : dataReader.GetDouble(ord));

        ord = dataReader.GetOrdinal("DEPFILENBR");
        String depFileNum = dataReader.IsDBNull(ord) ? "" : Convert.ToString(dataReader.GetInt32(ord));
        ord = dataReader.GetOrdinal("DEPFILESEQ");
        // Bug 9616: No dash and add leading zeros to sequence number
        String depFileSeq = dataReader.IsDBNull(ord) ? "" :
          dataReader.GetInt32(ord).ToString("D3");
        details.file = depFileNum + depFileSeq;

        ord = dataReader.GetOrdinal("DEPSLIPNBR");
        details.refNum = dataReader.IsDBNull(ord) ? "" : dataReader.GetString(ord);
        // Bug 15157
        ord = dataReader.GetOrdinal("DEPTID");
        String deptID = dataReader.IsDBNull(ord) ? "" : dataReader.GetString(ord);        // "DEPTID from tg_depfile_data"
        details.workgroup = deptID;
        // End Bug 15157

        // Bug 11549
        if (details.refNum == "")
        {
          string merchantKey = getWorkgroupMatchKeyUPHS(deptId2matchKey, deptID, bankID);
          details.refNum = merchantKey; // Could still be ""
        }
        // End Bug 11549

        // Subclass Detail fields
        ord = dataReader.GetOrdinal("COMMENTS");
        details.comments = dataReader.IsDBNull(ord) ? "" : dataReader.GetString(ord);

        ord = dataReader.GetOrdinal("CREATERUSERID");   //Bug 9608
        details.createrUserId = dataReader.IsDBNull(ord) ? "" : dataReader.GetString(ord);

        ord = dataReader.GetOrdinal("OWNERUSERID");
        details.ownerUserId = dataReader.IsDBNull(ord) ? "" : dataReader.GetString(ord);

        ord = dataReader.GetOrdinal("GROUPID");
        details.groupId = dataReader.IsDBNull(ord) ? "" : dataReader.GetString(ord);

        ord = dataReader.GetOrdinal("GROUPNBR");
        details.groupNbr = dataReader.IsDBNull(ord) ? 0 : dataReader.GetInt32(ord);

        ord = dataReader.GetOrdinal("ACC_USERID");
        details.acctUserId = dataReader.IsDBNull(ord) ? "" : dataReader.GetString(ord);

        ord = dataReader.GetOrdinal("SOURCE_REFID");
        details.sourceRefId = dataReader.IsDBNull(ord) ? "" : dataReader.GetString(ord);

        ord = dataReader.GetOrdinal("VOIDDT");
        details.voidDt = dataReader.IsDBNull(ord) ? DateTime.MinValue : dataReader.GetDateTime(ord);

        ord = dataReader.GetOrdinal("VOIDUSERID");
        details.voidUserid = dataReader.IsDBNull(ord) ? "" : dataReader.GetString(ord);

        details.tenders = getTenderList(depositId, depositNbr, out status);

        // Bug 10250: FT: pull the match key here for use in the drop down detail lists
        details.matchKey = getExpectedMatchKey(id, deptId2matchKey);

        // Get the BANKID for automatch report
        ord = dataReader.GetOrdinal("BANKID");
        details.bankId = dataReader.IsDBNull(ord) ? "" : dataReader.GetString(ord);
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in getExpectedDepositDetails", e);

        status.setErr("Cannot read expected deposit details and convert data",
            String.Format("getExpectedDepositDetails failed: SQL or conversion error: Error: {0}: Traceback: {1}",
                e, e.StackTrace),
            "REC-134", false);
      }
      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
      }
      return details;
    }



    /// <summary>
    /// Get a list of tenders for the reconciled deposit details.
    /// </summary>
    /// <param name="depositId"></param>
    /// <param name="depositNbr"></param>
    /// <param name="status"></param>
    /// <returns></returns>
    private List<Tender> getTenderList(string depositId, int depositNbr, out ReconStatus status)
    {
      status = new ReconStatus(true);

      var tenderList = new List<Tender>();

      // NOTE: the SEQNBR is not specificed here
      String tenderSql =
          @"SELECT * FROM TG_DEPOSITTNDR_DATA 
                    WHERE DEPOSITID = @depositID 
                        AND DEPOSITNBR = @depositNbr";

      SqlDataReader dataReader = null;
      try
      {
        dataReader = setupReader(tenderSql, ref status,
          param("depositID", SqlDbType.VarChar, depositId),
          param("depositNbr", SqlDbType.Int, depositNbr)
        );
        if (!status.success)
        {
          status.setErr("Cannot read tender information",
              String.Format(@"Cannot read details from TG_DEPOSITTNDR_DATA table for id: {0}:{1} 
                            SQL: {2}: Error: {3}",
                            depositId, depositNbr,
                            tenderSql, status.detailedError),
              "REC-145", true);
          return tenderList;
        }

        while (dataReader.Read())
        {
          var tender = new Tender();

          int ord = dataReader.GetOrdinal("AMOUNT");
          // Bug #12055 Mike O - Use misc.Parse to log errors
          tender.amount = misc.Parse<Decimal>(dataReader.IsDBNull(ord) ? 0.00 : dataReader.GetDouble(ord));

          ord = dataReader.GetOrdinal("TNDRDESC");
          tender.description = dataReader.IsDBNull(ord) ? "" : dataReader.GetString(ord);

          ord = dataReader.GetOrdinal("TNDR");
          tender.kind = dataReader.IsDBNull(ord) ? "" : dataReader.GetString(ord);

          ord = dataReader.GetOrdinal("TNDRPROP");
          tender.prop = dataReader.IsDBNull(ord) ? "" : dataReader.GetString(ord);

          ord = dataReader.GetOrdinal("SOURCE_DATE");
          tender.sourceDate = dataReader.IsDBNull(ord) ? DateTime.Now : dataReader.GetDateTime(ord);

          ord = dataReader.GetOrdinal("SOURCE_REFID");
          tender.sourceRefID = dataReader.IsDBNull(ord) ? "" : dataReader.GetString(ord);

          tenderList.Add(tender);
        }

      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in getTenderList", e);

        status.setErr("Cannot read tenders and convert data",
            String.Format("getTenderList failed for id: {0}:{1} SQL: {2}  Error: {3}: Traceback: {4}",
              depositId, depositNbr,
              tenderSql,
              e, e.StackTrace),
            "REC-146", false);
      }
      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
      }
      return tenderList;
    }

    // Bug 9778: FT: changed to take the real user id
    public void reconcileAuto(
        List<DatabaseID> expectedReconList,
        List<DatabaseID> bankReconList,
        String user,
        out ReconStatus status)
    {
      reconcile(expectedReconList, bankReconList, user, ReconciledDeposit.rType.auto, "", 0.00M, out status, true);
    }

    /// <summary>
    /// Reconcile a deposit that has been matched in the second, match-key pass of automatch.
    /// Bug 10324.
    /// </summary>
    /// <param name="expectedReconList"></param>
    /// <param name="bankReconList"></param>
    /// <param name="user"></param>
    /// <param name="status"></param>
    public void reconcileAutoMatchKey(
    List<DatabaseID> expectedReconList,
    List<DatabaseID> bankReconList,
    String user,
    out ReconStatus status)
    {
      reconcile(expectedReconList, bankReconList, user, ReconciledDeposit.rType.matchkey, "", 0.00M, out status, false);
    }

    public void reconcileManual(
        List<DatabaseID> expectedReconList,
        List<DatabaseID> bankReconList,
        String user,
        out ReconStatus status)
    {
      reconcile(expectedReconList, bankReconList, user, ReconciledDeposit.rType.manual, "", 0.00M, out status, true);
    }

    /// <summary>
    /// Allow comments on manual, non-forced deposits.
    /// Bug: 9695: FT: Added the reason field.
    /// </summary>
    /// <param name="expectedReconList"></param>
    /// <param name="bankReconList"></param>
    /// <param name="user"></param>
    /// <param name="reason"></param>
    /// <param name="status"></param>
    public void reconcileManual(
        List<DatabaseID> expectedReconList,
        List<DatabaseID> bankReconList,
        String user,
        String reason,
        out ReconStatus status)
    {
      reconcile(expectedReconList, bankReconList, user, ReconciledDeposit.rType.manual, reason, 0.00M, out status, true);
    }

    public void reconcileForced(
        List<DatabaseID> expectedReconList,
        List<DatabaseID> bankReconList,
        String user,
        Decimal overShort,
        String reason,
        out ReconStatus status)
    {
      reconcile(expectedReconList, bankReconList, user, ReconciledDeposit.rType.forced, reason, overShort, out status, true);
    }

    /// <summary>
    /// Reconcile a single transaction which may consist of multiple bank or expected deposits.
    /// The TG_RECONCILED_DATA, TG_RECONCILED_EXPECTED_DATA, and TG_RECONCILED_BANK_DATA
    /// tables will get new records while a single record in the TG_RECONCILED_BANK_DATA 
    /// table will be updated; these will be done in a transaction so that they can be rolled back if there
    /// is a failure in one.
    /// Checks have been added so that the same transaction cannot be reconciled multiple times 
    /// and to make sure to the passed expected and bank IDs are correct.
    /// </summary>
    /// <param name="expectedReconList"></param>
    /// <param name="bankReconList"></param>
    /// <param name="user"></param>
    /// <param name="type"></param>
    /// <param name="comment"></param>
    /// <param name="overShort"></param>
    /// <param name="status"></param>
    private void reconcile(List<DatabaseID> expectedReconList, List<DatabaseID> bankReconList, String user, ReconciledDeposit.rType type, String comment, Decimal overShort, out ReconStatus status, bool validateReconciliations)
    {
      status = new ReconStatus(true);
      String error_message = "";
      String sub_error_message = "";
      IDBVectorReader reader = null;


      try
      {
        // Make sure that the bank deposits are against legit bank
        // deposits that have not been reconciled
        validateBankReconciliations(bankReconList, ref status);
        if (!status.success)
          return;

        // For match key we do not want to validate because it will fail for expected
        if (validateReconciliations)
        {
          // Make sure that the reconcilation is against legit records and 
          // protect again duplicate reconcilations
          validateExpectedReconciliations(expectedReconList, ref status);
          if (!status.success)
            return;
        }

        Decimal totalAmount = 0M;
        // Scan the bank records and total the amount deposited
        // Be sure to collect total if Ignored Recon (one list will be empty).
        // Bug 9611; Thurber
        if (bankReconList.Count > 0)
        {
          totalAmount = getTotalBankAmount(bankReconList, overShort, type, ref status);
          if (!status.success)
            return;
        }
        else
        {
          totalAmount = getTotalExpectedAmount(expectedReconList, overShort, type, ref status);
          if (!status.success)
            return;
        }

        // Transaction should start here
        reader = db_reader.MakeTransactionReader(m_dbConnection, 0, 0, 0, ref error_message);
        if (error_message.Length > 0)
        {
          status.setErr("Cannot reconcile deposit",
              "Database error: Cannot MakeTransactionReader: Cannot reconcile deposits error",
              "REC-118", true);
          return;
        }

        // Write a single record to the TG_RECONCILED_DATA table
        reconcileWriteReconTableData(user, type, comment, overShort, reader, totalAmount);

        // Write one or more records to the TG_RECONCILED_EXPECTED_DATA table
        string expectedDetails = reconcileWriteExpectedXref(expectedReconList, reader);   // Bug 25353

        // Bug 11099: Update one or more expected (DEPOSIT_DATA) records to set the RECONCILED_STATUS to 'r'
        updateExpectedStatus(expectedReconList, reader);

        // Write one or more records to the TG_RECONCILED_BANK_DATA table
        string bankDetails = reconcileWriteBankXref(bankReconList, reader);   // Bug 25353

        // Change the bank deposit from from "n" or "u" to reconciled, "r"
        updateBankStatusFlag(bankReconList, reader);

        // End transaction here
        writeRecordsInTransaction(ref error_message, ref sub_error_message, reader);
        if (error_message.Length > 0 || sub_error_message.Length > 0)
        {
          status.setErr("Cannot reconcile deposit",
              "Database error: Cannot writeRecordsInTransaction: Cannot reconcile deposits: "
                  + error_message + ": " + sub_error_message,
              "REC-120", true);
        }

        // Bug 25353
        StringBuilder details = new StringBuilder();
        details.Append("Total Amount:").Append(totalAmount.ToString("C"));
        details.Append(",OverShort:").Append(overShort.ToString("C"));
        if (!string.IsNullOrWhiteSpace(comment))
          details.Append(",Comment:").Append(comment);
        details.Append(",Type:").Append(type);
        details.Append(",Expected list:").Append(expectedDetails);
        details.Append(",Actual List:").Append(bankDetails);

        misc.CASL_call("a_method", "actlog", "args",
           new GenericObject("user", user, "app", null, "action", "Bank Rec", "summary", "Reconciliation", "detail", details.ToString()));
        // End Bug 25353

      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in reconcile", e);

        if (reader != null)
        {
          reader.RollbackTransaction(ref sub_error_message);
        }
        status.setErr("Cannot reconcile deposit. Internal error: " + e.ToString(),
                String.Format("Cannot reconcile deposit(s): SQL or conversion error: Error: {0}: Traceback: {1}",
                    e, e.StackTrace),
                "REC-119", false);
      }
    }

    /// <summary>
    /// Change the RECONCILED_STATUS field to 'r' when a record is reconciled.
    /// Bug 11099: Did this to speed up and simplify query.
    /// </summary>
    /// <param name="expectedReconList"></param>
    /// <param name="reader"></param>
    private void updateExpectedStatus(List<DatabaseID> expectedReconList, IDBVectorReader reader)
    {
      ReconStatus status = new ReconStatus(true);
      foreach (DatabaseID id in expectedReconList)
      {
        String expectedSQL = @"
              UPDATE TG_DEPOSIT_DATA 
                  SET RECONCILED_STATUS = 'r'
                WHERE DEPOSITID = @depositID AND DEPOSITNBR = @depositNbr AND UNIQUEID = @uniqueID";

        Hashtable expectedParams = new Hashtable();
        String depositId;
        int depositNbr, uniqueID;
        id.parseExpectedKey(out depositId, out depositNbr, out uniqueID, ref status);
        if (!status.success)
          continue;     // TODO: think of a better error response
        expectedParams.Add("depositID", depositId);
        expectedParams.Add("depositNbr", depositNbr);
        expectedParams.Add("uniqueID", uniqueID);
        reader.AddCommand(expectedSQL, CommandType.Text, expectedParams);
      }
    }


    private void updateBankStatusFlag(List<DatabaseID> bankReconList, IDBVectorReader reader)
    {

      ReconStatus status;

      String rawBankSQL = @"
                        UPDATE TG_BANK_DEPOSIT_DATA
                            SET STATUS = @status, 
                            CHECKSUM = @checksum
                            WHERE BANK_DEPOSIT_ID = @bankDepositID";
      String bankSQL = String.Format(rawBankSQL, BankDeposit.convertStatus(BankDeposit.bStatus.reconciled));

      foreach (DatabaseID id in bankReconList)
      {
        // Fetch the entire record so we can change the checksum
        BankDepositDetails details = getBankDepositDetails(id, out status);
        details.depositStatus = BankDeposit.bStatus.reconciled;
        details.computeChecksum();

        Hashtable bankParams = new Hashtable();
        bankParams.Add("status", BankDeposit.convertStatus(details.depositStatus));
        bankParams.Add("checksum", details.checksum);
        // Bug #12055 Mike O - Use misc.Parse to log errors
        bankParams.Add("bankDepositID", misc.Parse<Int32>(id.databaseKey));
        reader.AddCommand(bankSQL, CommandType.Text, bankParams);
      }
    }

    /// <summary>
    /// Transuite code for closing transaction from transuite.
    /// </summary>
    /// <param name="error_message"></param>
    /// <param name="sub_error_message"></param>
    /// <param name="reader"></param>
    /// <returns></returns>
    private static string writeRecordsInTransaction(ref String error_message, ref String sub_error_message, IDBVectorReader reader)
    {
      bool success = reader.EstablishTransaction(ref error_message);
      if (!success || error_message.Length > 0)
      {
        if (reader != null)
        {
          reader.RollbackTransaction(ref sub_error_message);
          if (sub_error_message != "")
          {
            error_message += sub_error_message;
          }
        }
        return "Database error: Cannot EstablishTransaction: " + error_message;
      }
      int row_count = 0;
      success = reader.ExecuteTransactionCommands(ref error_message, ref row_count, true);
      sub_error_message = "";
      if (!success || error_message.Length > 0)
      {
        if (reader != null)
        {
          reader.RollbackTransaction(ref sub_error_message);
          if (sub_error_message != "")
          {
            error_message += sub_error_message;
          }
        }
        return "Database error: Cannot ExecuteTransactionCommands: " + error_message;
      }
      success = reader.CommitTransaction(ref error_message);
      if (!success || error_message.Length > 0)
      {
        if (reader != null)
        {
          reader.RollbackTransaction(ref sub_error_message);
          if (sub_error_message.Length > 0)
          {
            error_message += sub_error_message;
          }
        }
        return "Database error: Cannot commit transaction: " + error_message;
      }
      return "";
    }

    private static string reconcileWriteBankXref(List<DatabaseID> bankReconList, IDBVectorReader reader)
    {
      StringBuilder actlogDetails = new StringBuilder();
      int counter = 1;
      foreach (DatabaseID id in bankReconList)
      {
        String bankSQL = @"
                        INSERT INTO TG_RECONCILED_BANK_DATA
                            (BANK_DEPOSIT_ID, RECON_ID)
	                        SELECT @BANK_DEPOSIT_ID, IDENT_CURRENT('TG_RECONCILED_DATA')";

        Hashtable bankParams = new Hashtable();
        // Bug #12055 Mike O - Use misc.Parse to log errors
        bankParams.Add("BANK_DEPOSIT_ID", misc.Parse<Int32>(id.databaseKey));
        reader.AddCommand(bankSQL, CommandType.Text, bankParams);

        // Bug 25353: Collect some details
        actlogDetails.Append(",Bank DepositID_").Append(counter).Append(":")
          .Append(id.databaseKey);
        counter++;
      }
      return actlogDetails.ToString();
    }

    private static string reconcileWriteExpectedXref(List<DatabaseID> expectedReconList, IDBVectorReader reader)
    {
      ReconStatus status = new ReconStatus(true);
      StringBuilder actlogDetails = new StringBuilder();
      int counter = 1;
      foreach (DatabaseID id in expectedReconList)
      {

        String expectedSQL = @"
                        INSERT INTO TG_RECONCILED_EXPECTED_DATA 
                            (DEPOSITID, DEPOSITNBR, UNIQUEID, RECON_ID)
	                        SELECT @depositID, @depositNbr, @uniqueID, IDENT_CURRENT('TG_RECONCILED_DATA')";

        Hashtable expectedParams = new Hashtable();
        String depositId;
        int depositNbr, uniqueID;
        id.parseExpectedKey(out depositId, out depositNbr, out uniqueID, ref status);
        if (!status.success)
          continue;     // TODO: think of a better error response
        expectedParams.Add("depositID", depositId);
        expectedParams.Add("depositNbr", depositNbr);
        expectedParams.Add("uniqueID", uniqueID);
        reader.AddCommand(expectedSQL, CommandType.Text, expectedParams);

        // Bug 25353: Collected some details
        actlogDetails.Append(",Expected DepositID_").Append(counter).Append(":")
          .Append(depositId).Append('-').Append(depositNbr).Append('-').Append(uniqueID);
        counter++;
      }
      return actlogDetails.ToString();
    }

    private void reconcileWriteReconTableData(String user, ReconciledDeposit.rType type, String comment, Decimal overShort, IDBVectorReader reader, Decimal totalAmount)
    {
      var deposit = new ReconciledDeposit();
      deposit.reconDate = DateTime.Now;
      deposit.user = clip(user, m_userIdLenReconciledTable);
      deposit.reconStatus = ReconciledDeposit.rStatus.reconciled; // Start off as reconciled obviously
      deposit.reconType = type;
      deposit.reason = clip(comment, m_commentLenReconciledTable);
      deposit.amount = totalAmount;
      deposit.overShort = overShort;
      deposit.computeChecksum();

      String reconSql = @"
                    INSERT TG_RECONCILED_DATA VALUES (@RECON_DATE, @USERID, @STATUS, @TYPE, @COMMENT, @TOTAL_AMOUNT, @OVERSHORT, @CHECKSUM)";
      Hashtable reconParams = new Hashtable();
      reconParams.Add("RECON_DATE", deposit.reconDate);
      reconParams.Add("USERID", deposit.user);
      reconParams.Add("STATUS", ReconciledDeposit.convertReconStatus(deposit.reconStatus));
      reconParams.Add("TYPE", ReconciledDeposit.convertReconType(deposit.reconType));
      reconParams.Add("COMMENT", deposit.reason);
      reconParams.Add("TOTAL_AMOUNT", deposit.amount);
      reconParams.Add("OVERSHORT", deposit.overShort);
      reconParams.Add("CHECKSUM", deposit.checksum);
      reader.AddCommand(reconSql, CommandType.Text, reconParams);
    }

    private String clip(string stringField, int fieldMax)
    {
      if (stringField == null)
        return "";

      if (stringField.Length <= fieldMax)
        return stringField;

      return stringField.Substring(0, fieldMax - 1);
    }


    /// <summary>
    /// Mark an expected deposit as ignored.
    /// </summary>
    /// <param name="expectedKey"></param>
    /// <param name="user"></param>
    /// <param name="reason"></param>
    /// <returns></returns>
    public void reconcileExpectedIgnore(
        DatabaseID expectedKey,
        String user,
        String reason,
        out ReconStatus status)
    {
      List<DatabaseID> expectedDeposits = new List<DatabaseID>() {
                expectedKey
            };
      List<DatabaseID> bankDeposits = new List<DatabaseID>();

      reconcile(expectedDeposits, bankDeposits, user, ReconciledDeposit.rType.ignored, reason, 0.0M, out status, true);
    }

    /// <summary>
    /// Make a bank reconciliation as ignored
    /// </summary>
    /// <param name="bankKey"></param>
    /// <param name="user"></param>
    /// <param name="reason"></param>
    /// <returns></returns>
    public void reconcileBankIgnore(
        DatabaseID bankKey,
        String user,
        String reason,
        out ReconStatus status)
    {
      List<DatabaseID> expectedDeposits = new List<DatabaseID>();
      List<DatabaseID> bankDeposits = new List<DatabaseID>() { bankKey };

      reconcile(expectedDeposits, bankDeposits, user, ReconciledDeposit.rType.ignored, reason, 0.0M, out status, true);
    }

    /// <summary>
    /// Get a list of the unique bank accounts in the expected bank list.
    /// This list will be suitable for using in a drop-down box in the GUI
    /// </summary>
    /// <returns>List of unique accounts</returns>
    public List<String> getExpectedBankAccounts(out ReconStatus status)
    {
      status = new ReconStatus(true);
      var accounts = new List<String>();
      // Expected deposits, and expected start date.
      // This query could crush the db with a full-table scan; limited the list with
      // expectedStartDate.  Bug 10635: FT
      String accountsSql =
        @"SELECT DISTINCT BANKID FROM TG_DEPOSIT_DATA
          WHERE
            POSTDT IS NOT NULL
            AND POSTDT >= @expectedStartDate
            AND DEPOSITTYPE = 'FILE' 
            AND VOIDDT is null
          ";


      SqlDataReader dataReader = null;
      try
      {
        DateTime expectedStartDate = getExpectedStartDate();

        dataReader = setupReader(accountsSql, ref status,
          param("expectedStartDate", SqlDbType.DateTime, expectedStartDate)
        );
        if (!status.success)
        {
          status.setErr("Cannot read expected bank accounts",
              "Cannot query the TG_DEPOSIT_DATA table for distinct accounts",
              "REC-121", true);
          return new List<String>();
        }

        while (dataReader.Read())
        {
          String bankID = dataReader.IsDBNull(0) ? "" : dataReader.GetString(0);
          bool reconcilable;  // Bug 10635: FT
          String account = bankID2Account(bankID, out reconcilable, ref status);
          // Skip records that do not have reconcilable set in the bank account config
          // or accounts with reconcilable set to false.
          if (!reconcilable)
          {
            // Only show reconcilable accounts in drop down.
            continue;
          }
          accounts.Add(account);
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in getExpectedBankAccounts", e);

        status.setErr("Cannot get list of expected bank accounts",
            String.Format("Cannot get list of bank accounts: SQL or conversion error: Error: {0}: Traceback: {1}",
            e, e.StackTrace),
            "REC-122", false);
      }
      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
      }
      return accounts;
    }

    /// <summary>
    /// Get a list of the Bank accounts in the unreconciled bank deposit list.
    /// This list will be suitable for using in a drop-down box in the GUI
    /// </summary>
    /// <returns>List of unique accounts</returns>
    public List<String> getBankDepositAccounts(out ReconStatus status)
    {
      status = new ReconStatus(true);

      var bankAccounts = new List<String>();
      String accountsSql = "SELECT DISTINCT ACCOUNT_ID FROM TG_BANK_DEPOSIT_DATA ORDER BY ACCOUNT_ID";

      SqlDataReader dataReader = null;
      try
      {
        dataReader = setupReader(accountsSql, ref status);
        if (!status.success)
        {
          status.setErr("Cannot read bank accounts from bank list",
              "Cannot query the TG_BANK_DEPOSIT_DATA table for distinct accounts",
              "REC-123", true);
          return new List<String>();
        }

        while (dataReader.Read())
        {
          String account = dataReader.GetString(0);
          if (account == "")
            account = m_unsetAccountNbr;
          bankAccounts.Add(account);
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in getBankDepositAccounts", e);

        status.setErr("Cannot get list of bank accounts from bank list",
            String.Format("Cannot get list of bank accounts from TG_BANK_DEPOSIT_DATA: SQL or conversion error: Error: {0}: Traceback: {1}",
            e, e.StackTrace),
            "REC-124", false);

      }
      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
      }
      return bankAccounts;
    }


    /// <summary>
    /// Get a list of all the unique Bank accounts in the bank deposit 
    /// and expected lists.
    /// This list will be suitable for using in a drop-down box in the GUI
    /// Part of the fix for BUG: 9563: Thurber; allow the user to filter on
    /// both lists.
    /// </summary>
    /// <returns>List of unique accounts</returns>
    public List<String> getAllBankAccounts(out ReconStatus status)
    {
      status = new ReconStatus(true);

      // Start with an ordered bank deposit accounts list
      List<String> bankDepositAccounts = getBankDepositAccounts(out status);

      // Get an unordered list of expected accounts
      List<String> expectedDepositAccounts = getExpectedBankAccounts(out status);

      // Use the Linq Union operator to merge the lists
      List<String> merged = new List<String>(bankDepositAccounts.Union(expectedDepositAccounts));
      merged.Sort();
      return merged;
    }



    /// <summary>
    /// Reads the import directory looking for new BAI or other bank files.
    /// </summary>
    /// <param name="importDirectoryPath">Path to a directory containing the BAI/bank files</param>
    /// <returns>List of BAI or other bank files that can be parsed</returns>
    private List<String> readBankFiles(String importDirectoryPath, out ReconStatus status)
    {
      status = new ReconStatus(true);
      var files = new List<String>();

      try
      {
        if (!Directory.Exists(importDirectoryPath))
        {
          String msg = String.Format(@"The 'File Import Location' field in the Bank Deposit Reconciliation 
                          page of iPayment Config is not set to a valid directory: '{0}'.  
                          This path must point to the location of the import files", importDirectoryPath);
          status.setErr(msg, msg, "REC-175", true);
          return files;
        }

        string[] filePaths = Directory.GetFiles(importDirectoryPath);

        foreach (String file in filePaths)
        {
          files.Add(file);
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in readBankFiles", e);

        status.setErr("Cannot locate the Bank/BAI input files",
            String.Format("Cannot find the Bank/BAI input files: Error: {0}: Traceback: {1}",
            e, e.StackTrace),
            "REC-125", false);
      }

      return files;
    }

    /// <summary>
    ///  Parse BAI/CSV Bank files and save to the database
    /// </summary>
    /// <param name="importDirectoryPath"></param>
    /// <param name="ftpRemotePath"></param>
    /// <param name="import_file_type"></param>
    /// <param name="baiCodesSet"></param>
    /// <param name="withdrawalCodeSet"></param>
    /// <param name="bankAccountWhitelistSet"></param>
    /// <param name="bankAccountExclusionlistSet"></param>
    /// <param name="userid"></param>
    /// <param name="bankAccount"></param>
    /// <param name="fileImportFilterTypeCodesSet"></param>
    /// <param name="fileImportFilterPattern"></param>
    /// <param name="depositSlipFilter">Bug 18061 - regex pattern to be used </param>
    /// <param name="statusList"></param>
    /// <returns>List of BAI/Bank files that were imported or error message</returns>
    public List<String> importBankFiles(String importDirectoryPath, String ftpRemotePath,
     string import_file_type,
     HashSet<int> baiCodesSet, // Bug 10996: FT: Pass in the codes every time so that a restart is not needed
     HashSet<int> withdrawalCodeSet,  // Bug 11570
     HashSet<int> achCodesSet,                       // Bug 25037: List of qualifying BAI that indicate a ACH deposit
     bool enableBankAccountFiltering,                // Bug 18136
     HashSet<string> bankAccountWhitelistSet,        // Bug 17238
     string userid,                  // Bug 17200
     string bankAccount,             // Bug 17179
     HashSet<int> fileImportFilterTypeCodesSet,      // Bug 17658
     string fileImportFilterPattern,                 // Bug 17658
     string depositSlipFilter,                       // Bug 18061
     bool enableDepositSlipFiltering,                // Bug 18061
     string achRegexPattern,                         // Bug 25037: To look for the Company Discretionary Data in the Text field

     out List<ReconStatus> statusList)
    {
      statusList = new List<ReconStatus>();
      var addedFiles = new List<String>();

      // Protection error?
      ReconStatus status;
      List<String> newFiles = readBankFiles(importDirectoryPath, out status);
      if (!status.success)
      {
        statusList.Add(status);
        return addedFiles;
      }

      // A state variable to provide better error messages
      String currentFile = "";
      bool savingFile = false;      // Bug 10907: Trying to isolate save errors
      bool achivingFile = false;    // Bug 10907: Trying to isolate archive errors
      string movePath = "";            // Ditto: Tell use what path failed
      try
      {
        List<TransactionRecord> transactionList = null;
        // Bug 11080: Only filter the BAI parser
        Logger.cs_log("importBankFiles: using this parser: '" + import_file_type);
        Parser parser = instantiateParser(import_file_type);

        foreach (String file in newFiles)
        {
          // Skip the subdirectories
          if (file.Equals(m_processedSubDirectory) || file.Equals(m_erroneousSubDirectory))
            continue;

          // Bug 17200: Need count for Activity Log. TODO: This slow and memory-wasteful....I should build this counter into the parsers...
          int inputFileLength = File.ReadAllLines(file).Length;

          currentFile = file;
          // Check for parse failure and move file to error directory
          transactionList = parser.parse(file, bankAccount, true, out status);
          if (!status.success)
          {
            statusList.Add(status);
            recordImportInActivityLog(userid, file, ftpRemotePath, false, 0, inputFileLength, status);
            copyErroneousBankFile(importDirectoryPath, file, out status);
            continue;
          }

          m_filteredRecordCount = 0;
          // Do all the deposits as one in a transaction
          List<BankDepositDetails> deposits = applyBusinessRules(transactionList, baiCodesSet, withdrawalCodeSet, achCodesSet, enableBankAccountFiltering, bankAccountWhitelistSet, import_file_type, fileImportFilterTypeCodesSet, fileImportFilterPattern, depositSlipFilter, enableDepositSlipFiltering, achRegexPattern);

          // Save the records
          savingFile = true;  // Bug 10907
          int recordCounter;
          bool duplicate;
          saveImportedBankDeposits(file, deposits, out duplicate, out recordCounter, out status);
          savingFile = false;  // Bug 10907
          if (!status.success)
          {
            statusList.Add(status);
            // Bug 13050: UMN add ftp support
            if (status.severity == ReconStatus.severityCode.warning)
              archiveProcessedBankFile(importDirectoryPath, file, out movePath, out status);
            else
              copyErroneousBankFile(importDirectoryPath, file, out status);

            recordImportInActivityLog(userid, file, ftpRemotePath, duplicate, recordCounter, inputFileLength, status);

            continue;
          }

          if (duplicate)
            recordImportInActivityLog(userid, file, ftpRemotePath, duplicate, recordCounter, inputFileLength, status);

          achivingFile = true;   // Bug 10907
          archiveProcessedBankFile(importDirectoryPath, file, out movePath, out status);
          achivingFile = false;   // // Bug 10907

          // List the successful imports in the human-readable file
          FileInfo fileInfo = new FileInfo(file);
          status.humanReadableError = fileInfo.Name;
          statusList.Add(status);
          addedFiles.Add(fileInfo.Name);
          if (!duplicate)
            recordImportInActivityLog(userid, file, ftpRemotePath, duplicate, recordCounter, inputFileLength, status);
        }
      }
      catch (Exception e)
      {
        String humanMessage;
        FileInfo fileInfo = new FileInfo(currentFile);
        // Bug 10907: FT: improve error messages during import
        if (achivingFile)
        {
          humanMessage = String.Format("Cannot move '{0}' to '{1}' after the parse: Error: {2}",
            fileInfo.Name, movePath, e.Message);
        }
        else if (savingFile)
        {
          humanMessage = String.Format("Cannot save '{0}' to the database: Error: {1}",
            fileInfo.Name, e.Message);
        }
        else
        {
          if (currentFile.Length > 0)
            humanMessage = String.Format("Cannot import the {0} bank file", fileInfo.Name);
          else
            humanMessage = "Cannot process the Bank/BAI imput files";
        }
        // End Bug 10970

        // Bug 17849
        Logger.cs_log_trace("Error in importBankFiles", e);

        status.setErr(humanMessage,
            String.Format(@"Cannot parse and save the Bank/BAI imput files: 
                            Failed on file: {0}: 
                            Error: {1}: Traceback: {2}",
                currentFile, e, e.StackTrace),
            "REC-126", false);
        statusList.Add(status);

      }
      return addedFiles;
    }

    /// <summary>
    /// Capture information about file import into the Activity Log.
    /// Bug 17200.
    /// </summary>
    /// <param name="fileInfo"></param>
    /// <param name="recordCounter"></param>
    /// <param name="inputFileLength"></param>
    /// <param name="status"></param>
    private void recordImportInActivityLog(string userid, string file, string ftpRemotePath, bool duplicate, int recordCounter, int inputFileLength, ReconStatus status)
    {
      string summary;
      StringBuilder details = new StringBuilder("Local filename:" + file);
      details.Append(",Remote path:").Append(ftpRemotePath);

      if (status.success)
      {
        if (duplicate)
        {
          summary = "Warning";
          details.Append(',').Append("Warning:").Append("Cannot import this file; a duplicate record was found in the database.  ").Append(status.warning);
        }
        else
        {
          summary = "Success";
        }
      }
      else
      {
        summary = "Failure";
        details.Append(',').Append("Error:").Append(status.humanReadableError.Replace(',', '|').Replace(':', ' '));
      }

      details.Append(',').Append("Records stored:").Append(recordCounter);
      details.Append(',').Append("Records in input file:").Append(inputFileLength);
      details.Append(',').Append("Records filtered out:").Append(m_filteredRecordCount);

      misc.CASL_call("a_method", "actlog", "args",
         new GenericObject("user", userid, "app", "Portal", "action", "Bank Rec Import", "summary", summary, "detail", details.ToString()));

    }

    /// <summary>
    /// Read the schema data for the bank table and find the field sizes.
    /// </summary>
    /// <param name="accountIdLen"></param>
    /// <param name="refNumLen"></param>
    /// <param name="bankReferenceLen"></param>
    /// <param name="textLen"></param>
    private void getBankTableFieldSizes(out int accountIdLen, out int refNumLen, out int bankReferenceLen, out int textLen)
    {
      const int DEFAULT_FIELD_SIZE = 64;
      const int DEFAULT_TEXT_FIELD_SIZE = 128;

      // Use the quickest possible SQL query for all the columns
      String dataReaderSQL = "SELECT TOP(1) * FROM TG_BANK_DEPOSIT_DATA";

      SqlConnection dataReaderConnection = new SqlConnection();
      SqlDataReader dataReader = null;
      accountIdLen = refNumLen = bankReferenceLen = DEFAULT_FIELD_SIZE;
      textLen = DEFAULT_TEXT_FIELD_SIZE;
      try
      {
        String conString = (String)m_dbConnection.get("db_connection_string");

        // Open a connection for the DataReader
        dataReaderConnection.ConnectionString = conString;
        dataReaderConnection.Open();

        SqlCommand dataReaderCommand = new SqlCommand(dataReaderSQL, dataReaderConnection);
        dataReader = dataReaderCommand.ExecuteReader();
        DataTable schema = dataReader.GetSchemaTable();

        // Bug #12055 Mike O - Use misc.Parse to log errors
        for (int i = 0; i < dataReader.FieldCount; i++)
        {
          string name = schema.Rows[i]["ColumnName"].ToString();
          if (System.String.Compare(name, "ACCOUNT_ID", true) == 0)
            accountIdLen = misc.Parse<Int32>(schema.Rows[i]["ColumnSize"]);
          if (System.String.Compare(name, "REF_NUM", true) == 0)
            refNumLen = misc.Parse<Int32>(schema.Rows[i]["ColumnSize"]);
          if (System.String.Compare(name, "BANK_REFERENCE", true) == 0)
            bankReferenceLen = misc.Parse<Int32>(schema.Rows[i]["ColumnSize"]);
          if (System.String.Compare(name, "TEXT", true) == 0)
            textLen = misc.Parse<Int32>(schema.Rows[i]["ColumnSize"]);
        }
      }
      finally
      {
        if (dataReader != null && !dataReader.IsClosed)
          dataReader.Close();
        dataReaderConnection.Close();
      }
    }

    /// <summary>
    /// Figure out which parser to use and instanciate it.
    /// Do this dynamcially: Bug 11064.
    /// </summary>
    /// <returns></returns>
    private Parser instantiateParser(string import_file_type)
    {
      switch (import_file_type)
      {
        case "csv":
        case "CSV":  // For older Config DBs
        case "CSV_Header":  // Bug 21882: From Cone
          return new GroupHealthParser();
        case "bai":
        case "BAI":      // For older Config DBs
          return new ParklandParser();
        case "healthshare":             // Bug 17179
        case "HealthShare":
          return new HealthShareParser();
        case "bbt":             // Bug 17179: Still need to add BB&T parser
        case "BBT":
          return new BBTParser();
        default:
          Logger.cs_log("Warning: instantiateParser: Bank Rec: Unknown file type set in the File Import Subdirectories or the File Import Format: '" + import_file_type + "'");
          return new ParklandParser();
      }
      // End of Bug 10509
    }

    /// <summary>
    /// Copy an defective file to the error subdirectory.  Do not move it;
    /// this is done so the user can re-try the import at a later time.
    /// </summary>
    /// <param name="importDirectoryPath"></param>
    /// <param name="bankFilePath"></param>
    /// <param name="status"></param>
    private void copyErroneousBankFile(string importDirectoryPath, string bankFilePath, out ReconStatus status)
    {
      status = new ReconStatus(true);


      String existingPath = System.IO.Path.Combine(importDirectoryPath, Path.GetFileName(bankFilePath));

      //Create a new subfolder under the BAI folder if it does not exist
      string newPath = System.IO.Path.Combine(importDirectoryPath, m_erroneousSubDirectory);
      if (!System.IO.Directory.Exists(newPath))
      {
        System.IO.Directory.CreateDirectory(newPath);
      }

      String newFilePath = createTimeStampedPath(newPath, bankFilePath);

      System.IO.File.Move(existingPath, newFilePath);

      if (!System.IO.File.Exists(newFilePath))
      {
        status.setWarning("Cannot copy failed Bank/BAI file to error subdirectory: " + bankFilePath,
            "REC-141");
      }

    }


    private void archiveProcessedBankFile(string importDirectoryPath, string bankFilePath, out string movePath, out ReconStatus status)
    {
      status = new ReconStatus(true);


      String existingPath = System.IO.Path.Combine(importDirectoryPath, Path.GetFileName(bankFilePath));

      //Create a new subfolder under the BAI folder if it does not exist
      string newPath = System.IO.Path.Combine(importDirectoryPath, m_processedSubDirectory);
      if (!System.IO.Directory.Exists(newPath))
      {
        System.IO.Directory.CreateDirectory(newPath);
      }

      String newFilePath = createTimeStampedPath(newPath, bankFilePath);
      movePath = newFilePath;     // Bug 10907; improve error message if there is a failure

      System.IO.File.Move(existingPath, newFilePath);

      if (!System.IO.File.Exists(newFilePath))
      {
        status.setWarning(
            String.Format("Cannot archive Bank/BAI file '{0}' to '{1}': ", existingPath, newFilePath),
            "REC-140");
      }

    }

    public static String createTimeStampedPath(string newPath, string fullFileName)
    {
      // Make the file unique and mark the processing time
      String filename = Path.GetFileName(fullFileName);
      String baseName = Path.GetFileNameWithoutExtension(fullFileName);
      String extension = Path.GetExtension(fullFileName);

      DateTime suffixDate = DateTime.Now;
      String suffix = suffixDate.ToString("MMMM_dd_yyyy_H_mm_ss");
      String newFilename = baseName + "." + suffix + extension;
      String newFilePath = System.IO.Path.Combine(newPath, newFilename);

      return newFilePath;
    }


    private Regex m_regexMatchKeyPattern = null;

    /// <summary>
    /// Apply business rules for things like filtering out extraneous records
    /// </summary>
    /// <param name="records"></param>
    /// <param name="baiCodesSet"></param>
    /// <param name="withdrawalCodeSet"></param>
    /// <param name="bankAccountWhitelistSet"></param>
    /// <param name="bankAccountExclusionlistSet"></param>
    /// <param name="import_file_type"></param>
    /// <param name="fileImportFilterTypeCodesSet"></param>
    /// <param name="fileImportFilterPattern"></param>    
    /// <param name="depositSlipFilter">Bug 18061 Regex pattern for filtering deposit slip number</param>
    /// <returns></returns>
    private List<BankDepositDetails> applyBusinessRules(
      List<TransactionRecord> records,
      HashSet<int> baiCodesSet,    // Bug 10996: FT: Connect the live bai codes
      HashSet<int> withdrawalCodeSet, // Bug 11570: FT: Change amounts to negative for withdrawals
      HashSet<int> achCodesSet,
      bool enableBankAccountFiltering,              // Bug 18136
      HashSet<string> bankAccountWhitelistSet,      // Bug 17238
      string import_file_type,      // Bug 11080: 6/16/11: Special case each type as needed
      HashSet<int> fileImportFilterTypeCodesSet,    // Bug 17658
      string fileImportFilterPattern,               // Bug 17658
      string depositSlipFilter,                     // Bug 18061
      bool enableDepositSlipFiltering,               // Bug 18061
      string achRegexPattern
      )
    {

      // Bug 17658: Setup regex pattern matcher here
      Regex regexPattern = new Regex(fileImportFilterPattern);

      // Bug 18061
      Regex depositSlipPattern = new Regex(depositSlipFilter);

      // Bug 18061: The Customer Reference number will be all zeros for credit card transaction
      Regex zeroCustomerRefPattern = new Regex(@"^[0]*$");      // Match all zeros 

      // Bug 25037: To look for the Company Discretionary Data in the Text field
      Regex achRegex = new Regex(achRegexPattern);

      string recon_regex = (string)((GenericObject)c_CASL.c_object("Business.bank_deposit_recon.data")).get("recon_regex", "");
      if (recon_regex != null && recon_regex.Length > 0)
          m_regexMatchKeyPattern = new Regex(recon_regex);


      var canonicalDeposit = new List<BankDepositDetails>();
      foreach (TransactionRecord record in records)
      {
        // Strip trailing slash
        if (!string.IsNullOrEmpty(record.customerRefNum))
            record.customerRefNum = record.customerRefNum.TrimEnd('/');         // Bug 25518

        // Bug 18061: If the Customer Reference Number is all zeros, then this is a credit card transaction
        bool isZeroRefenceField = isAllZeros(zeroCustomerRefPattern, record.customerRefNum);

        // Bug 25037: I figured we should also check for empty slip numbers
        bool isEmptySlipNumber = string.IsNullOrWhiteSpace(record.customerRefNum) ? true : false;

        // Bug 19953: Strip the leading zero from the slip number
        // This will do it for all records, not just BAI files
        stripLeadingZeros(record, isZeroRefenceField, isEmptySlipNumber);

        // Bug 18136: Simplify bank account filtering
        if (enableBankAccountFiltering)
        {
          string bankAccount = record.account;
          if (!bankAccountWhitelistSet.Contains(bankAccount))
            continue;
        }

        // Bug 22460: Set slip number from match key.  From Cone and CCF
        if (String.Compare(import_file_type, "BAI", true) == 0 || string.Compare(import_file_type, "CSV_Header", true) == 0)
        {
            setDepositSlipNumberFromMatchKey(record);
        }

        // Bug 11080: Only apply the BAI set against BAI input files
        //if (import_file_type == "BAI" || import_file_type == "Parkland")
        if (String.Compare(import_file_type, "BAI", true) == 0 || string.Compare(import_file_type, "CSV_Header", true) == 0)
        {
          // Only store records with relevant codes
          if (baiCodesSet.Count == 0 || baiCodesSet.Contains(record.baiCode))
          {
            // Bug 11570: Set amount to negative if this is a withdrawal
            if (withdrawalCodeSet.Contains(record.baiCode) && Math.Sign(record.amount) == 1)
            {
              record.amount = 0m - record.amount;  // Change to negative
            }
            // End 11570

            // If there is a blank Customer Reference number, try to fill it in 
            // with the ACH Discretionary Data field
            // Bug 25037
            bool isACHDeposit = processACHDeposit(record, isZeroRefenceField, isEmptySlipNumber, achCodesSet, achRegex);
            if (isACHDeposit)
            {
                BankDepositDetails deposit = applyBusinessRule(record);
                canonicalDeposit.Add(deposit);

            }
            // Bug 17658: One last filter: filter the Text field on a regular expression
            // so that only credit card records with match key are included.
            else if (isZeroRefenceField)        // Bug 18061: Only do this for credit card deposits
            {
              bool includeRecord = true;
              if (fileImportFilterTypeCodesSet.Contains(record.baiCode))
              {
                Match match = regexPattern.Match(record.text);
                if (!match.Success)
                  includeRecord = false;
              }

              if (includeRecord)      // Bug 17658
              {
                BankDepositDetails deposit = applyBusinessRule(record);
                canonicalDeposit.Add(deposit);
              }
            }
            else    // Bug 18061: all the non-credit card records should be included if they have gotten this far (past filtering ont the cust ref)
            {
              // Bug 18061: apply the final filter, this one based on the deposit slip
              if (isMatchingDepositSlip(record, enableDepositSlipFiltering, isZeroRefenceField, depositSlipFilter, import_file_type, depositSlipPattern))
              {
                BankDepositDetails deposit = applyBusinessRule(record);
                canonicalDeposit.Add(deposit);
              }
            }
          }
        }
        else
        {
          // Bug 18049: Not a BAI: do not apply any filters
          BankDepositDetails deposit = applyBusinessRule(record);
          canonicalDeposit.Add(deposit);
        }


      }
      // Bug 17200: Record the number of records filtered out
      int totalCount = records.Count;
      int recordsSaved = canonicalDeposit.Count;
      m_filteredRecordCount = totalCount - recordsSaved;

      return canonicalDeposit;
    }

    /// <summary>
    /// Remove leading zeros in deposit slip numnber.
    /// Bug 19953 from Cone.
    /// </summary>
    /// <param name="record"></param>
    /// <param name="isZeroRefenceField"></param>
    /// <param name="isEmptySlipNumber"></param>
    private void stripLeadingZeros(TransactionRecord record, bool isZeroRefenceField, bool isEmptySlipNumber)
    {
      if (isZeroRefenceField)
        return;
      if (isEmptySlipNumber)
        return;
      string depSlipNbr = record.customerRefNum;
      record.customerRefNum = depSlipNbr.TrimStart('0');
    }

    /// <summary>
    /// If the Cust Ref (slip number) is not set,
    /// grab it from the Text field with the regex.
    /// Bug 22460
    /// </summary>
    /// <param name="record"></param>
    private void setDepositSlipNumberFromMatchKey(TransactionRecord record)
    {
       // only do this for blank slip numbers
        if (!string.IsNullOrWhiteSpace(record.customerRefNum))
        {
            if (!IsAllZeros(record.customerRefNum))
                return;
        }

        // Only if the regex was setup correctly
        if (m_regexMatchKeyPattern == null)
        {
            Logger.cs_log("setDepositSlipNumberFromMatchKey: the Reconciliation Key Pattern was not set or set incorrectly and cannot be parsed.  Deposit Slip numbers will not be set from the Text field during import.");
            return;
        }

        // Look for the match key in the Text field
        Match match = m_regexMatchKeyPattern.Match(record.text);
        if (!match.Success)
            return;

        // Set the Slip Number to the match key
        String matchKeyString = match.Groups[1].Value;
        record.customerRefNum = matchKeyString; 
    }

    /// <summary>
    /// If the Slip Number is all zeros, it is of no use for automatching
    /// so change to the match key.
    /// </summary>
    /// <param name="slipNumber"></param>
    /// <returns></returns>
    static bool IsAllZeros(String slipNumber)
    {
        for (int i = 0; i < slipNumber.Length; i++)
            if (slipNumber[i] != '0')
                return false;

        return true;
    }

    /// <summary>
    /// First determine if this is an ACH deposit.  
    /// To do this, check the BAI code and the Text field.
    /// If this is an ACH deposit, grab the Company Discretionary Data field from the
    /// Text field (using the regular expression) and set the 
    /// Customer Reference field (before it is written to the database).
    /// Bug 25037
    /// </summary>
    /// <param name="record"></param>
    /// <param name="isZeroRefenceField"></param>
    /// <param name="isEmptySlipNumber"></param>
    /// <param name="achCodesSet"></param>
    /// <param name="achRegex"></param>
    /// <returns></returns>
    private bool processACHDeposit( 
        TransactionRecord record, 
        bool isZeroRefenceField,
        bool isEmptySlipNumber,
        HashSet<int> achCodesSet, 
        Regex achRegex)
    {
        // Bail out early if the slip numner is already set
        bool missingSlipNumber = (isZeroRefenceField || isEmptySlipNumber) ? true : false;
        if (!missingSlipNumber)
            return false;

        // If this is not an ACH deposit, exit
        int typeCode = record.baiCode;
        if (!achCodesSet.Contains(typeCode))
        {
            return false;
        }

        Match match = achRegex.Match(record.text);
        // If there is no Discretionary Data Field, exit
        if (!match.Success)
            return false;

        String discretionaryData = match.Groups[1].Value;
        record.customerRefNum = discretionaryData;

        return true;
    }



    /// <summary>
    /// Deposit Slip Filtering.  MRF "Bank Rec BAI File Filtering Adjustments", 11/23/2015
    /// Skip record if these conditions are true:
    ///    1) Deposit slip filtering is switched on
    ///    2) The deposit is not a credit card transactions
    ///    3) If there is a field value list of called BANK_REC_DEPOSIT_SLIP_PREFIXES
    ///    4) If the deposit slip number of blank (not zeros) reject the record
    ///    5) If the deposit slip number does not start with the numbers in the BANK_REC_DEPOSIT_SLIP_PREFIXES field value list
    ///    
    /// Bug 18061
    /// </summary>
    /// <param name="record"></param>
    /// <param name="enableDepositSlipFiltering">From the Bank Rec configuration</param>
    /// <param name="isCreditCardDeposit">Not used at this time, but may in the future</param>
    /// <param name="depositSlipFilter">String version of the regex to make sure it is really set</param>
    /// <param name="import_file_type">Not used at this time, but it might be in the fuiture</param>
    /// <param name="depositSlipPattern">The Regular Expression to match against</param>
    /// <returns></returns>
    private bool isMatchingDepositSlip(
      TransactionRecord record,
      bool enableDepositSlipFiltering,
      bool isCreditCardDeposit,
      string depositSlipFilter,
      string import_file_type,
      Regex depositSlipPattern)
    {
      // Let all the records through if filtering is turned off
      if (!enableDepositSlipFiltering)
        return true;

      // If the filter is not set correctly, let all records through.  Refer to error log for why
      if (string.IsNullOrEmpty(depositSlipFilter))   // Bug 18061 Do not filter if the field value list is null or empty or defective
        return true;

      // Bug 18061: If the customer ref number is null, filter out.  
      // I made a judgement call here, but it sort of seems logical
      if (string.IsNullOrEmpty(record.customerRefNum))
      {
        return false;
      }

      // Bug 18061: Do the actual deposit slip filtering.  
      Match match = depositSlipPattern.Match(record.customerRefNum);
      if (!match.Success)
        return false;
      else
        return true;
    }

    /// <summary>
    /// Credit Card transactions will have all zeros for the Customer Reference number
    /// Bug 18061
    /// </summary>
    /// <param name="zeroCustomerRefPattern"></param>
    /// <param name="customerRefNum"></param>
    /// <returns></returns>
    private bool isAllZeros(Regex zeroCustomerRefPattern, string customerRefNum)
    {
      // Empty string are not credit card transactions by design because the cust ref is not all zeros
      if (string.IsNullOrEmpty(customerRefNum))
        return false;

      Match match = zeroCustomerRefPattern.Match(customerRefNum);
      if (match.Success)
        return true;
      else
        return false;
    }

    /// <summary>
    /// Translate the raw data from the bank/BAI file to the 
    /// canonical format. 
    /// Note how the customer reference number is used for the ref/slip
    /// number instead of the bank reference number.
    /// </summary>
    /// <param name="transaction"></param>
    /// <returns></returns>
    private BankDepositDetails applyBusinessRule(TransactionRecord transaction)
    {
      var deposit = new BankDepositDetails();
      deposit.account = transaction.account;
      deposit.age = 0;
      deposit.amount = transaction.amount;
      deposit.depDate = transaction.date;
      deposit.depositStatus = BankDeposit.bStatus.newDeposit;
      deposit.refNum = transaction.customerRefNum;
      deposit.text = transaction.text;
      deposit.bankReference = transaction.bankRefNum;
      return deposit;
    }


    private String trimBankField(BankDepositDetails deposit, int fieldLen)
    {
      if (deposit == null || deposit.text == null)
        return "";

      return clip(deposit.text, fieldLen);
    }


    /// <summary>
    /// Translate bank IDs such as "001", "003", etc. to 
    /// real bank account numbers such as "0000000789".
    /// Improved version that queries CASL and also
    /// checked the reconcilable variable.
    /// Bug 10635: FT: Added check for the reconcilable config variable.
    /// </summary>
    /// <param name="bankID"></param>
    /// <param name="reconcilable"></param>
    /// <param name="status"></param>
    /// <returns></returns>
    private String bankID2Account(String bankID, out bool reconcilable, ref ReconStatus status)
    {
      // First see if this account exists!
      GenericObject accountList = (GenericObject)c_CASL.c_object("Business.Bank_account.of");
      if (!accountList.Contains(bankID))
      {
        reconcilable = false;
        return bankID;
      }

      // Get the specific account
      String account = String.Format("Business.Bank_account.of.\"{0}\"", bankID);
      GenericObject bankAccountGEO = (GenericObject)c_CASL.c_object(account);


      // Now see if it contains the reconcilable bool
      if (!bankAccountGEO.Contains("reconcilable"))
        reconcilable = true;    // Be friendly and default to true if not defined
      else
        reconcilable = (bool)bankAccountGEO.get("reconcilable");

      // Now see if it contains the reconcilable bool
      if (!bankAccountGEO.Contains("account_nbr"))
        return bankID;
      else
      {
        string accountNbr = Convert.ToString(bankAccountGEO.get("account_nbr"));
        if (accountNbr == "")
          accountNbr = m_unsetAccountNbr;
        return accountNbr;
      }
    }


    /// <summary>
    /// Version of getReconciledDepositList used by the Reporting app.  
    /// This supports a wider array of filtering options.
    /// </summary>
    /// <param name="startDate"></param>
    /// <param name="endDate"></param>
    /// <param name="users"></param>
    /// <param name="rStatus"></param>
    /// <param name="types"></param>
    /// <param name="status"></param>
    /// <returns></returns>
    public List<ReconciledDeposit> getReconciledDepositListForReporting(
      DateTime startDate,
      DateTime endDate,
      List<String> users,
      ReconciledDeposit.rStatus rStatus,
      List<ReconciledDeposit.rType> types,
      GenericObject deptID2MatchKey,    // Bug 11549
      string workgroupID,               // Bug 15157
      bool extraFields,
      string groupBy,                   // Bug 15157
      out ReconStatus status)
    {
      status = new ReconStatus(true);
      var reconList = new List<ReconciledDeposit>();

      String conString = (String)m_dbConnection.get("db_connection_string");

      String orderBy = " ORDER BY RECON_DATE DESC, USERID, TOTAL_AMOUNT DESC, OVERSHORT DESC";

      // Bug 27450: Do not parameterize TOP
      var sqlBuilder = new StringBuilder(
        String.Format("SELECT TOP({2}) * FROM TG_RECONCILED_DATA WHERE RECON_DATE >= '{0}' AND RECON_DATE <= '{1}' ",
          startDate, endDate, getDBLimit()));

      if (users != null)
      {
        // Bug 10040: FT: do not filter if All is specified
        if (!users.Contains("all"))
        {
          // Sort by USERID if a user list is specified
          orderBy = " ORDER BY USERID, RECON_DATE DESC, TYPE, STATUS";
          var usersBuilder = new StringBuilder(" AND USERID IN (");
          int counter = 0;
          foreach (String user in users)
          {
            if (counter++ > 0)
              usersBuilder.Append(", ");
            usersBuilder.Append("'").Append(user).Append("'");
          }
          usersBuilder.Append(")");
          sqlBuilder.Append(usersBuilder).Append(" ");
        }
      }

      if (rStatus != ReconciledDeposit.rStatus.unset)
      {
        String strStatus = ReconciledDeposit.convertReconStatus(rStatus);
        sqlBuilder.Append(" AND STATUS = '").Append(strStatus).Append("' ");
      }

      if (types != null)
      {
        int counter = 0;
        var typesBuilder = new StringBuilder(" AND TYPE IN (");
        foreach (ReconciledDeposit.rType type in types)
        {
          if (type != ReconciledDeposit.rType.unset)
          {
            String strType = ReconciledDeposit.convertReconType(type);
            if (counter++ > 0)
              typesBuilder.Append(", ");
            typesBuilder.Append("'").Append(strType).Append("'");
          }
        }
        typesBuilder.Append(")");
        sqlBuilder.Append(typesBuilder).Append(" ");
      }

      sqlBuilder.Append(orderBy);

      String dataReaderSQL = sqlBuilder.ToString();
      List<ReconciledDeposit> reconciledList = getReconciledDepositListInternal(dataReaderSQL, startDate, endDate, false, deptID2MatchKey, true, ref status);

      if (workgroupID == "" || workgroupID == "all" || workgroupID == "All")
      {
        // Don't do nothing now
      }
      else
      {
        // Workgroup has been set. Filter on it.
        reconciledList = filterReconciledListByWorkgroup(reconciledList, workgroupID);
      }

      return sortReconciledList(reconciledList, groupBy);
    }

    /// <summary>
    /// Sort Reconciled Deposits list before returning it to the report screen.
    /// This sort cannot easily be done in the database because of the funky lists of filenames and reference numbers.
    /// Sort on the Workgroup, date, and status.
    /// TODO: Do eal grouping, not just sorting
    /// Bug 15157
    /// </summary>
    /// <param name="reconciledList"></param>
    /// <returns></returns>
    private List<ReconciledDeposit> sortReconciledList(List<ReconciledDeposit> reconciledList, string groupBy)
    {
      switch (groupBy)
      {
        case "workgroup":
          return reconciledList.OrderBy(x => x.workgroup).ThenByDescending(x => x.reconDate).ToList();
        case "file":
          return reconciledList.OrderByDescending(x => x.file).ThenByDescending(x => x.reconDate).ToList();
        case "date":
          return reconciledList.OrderByDescending(x => x.reconDate).ThenBy(x => x.workgroup).ToList();
        case "user":
          return reconciledList.OrderBy(x => x.user).ThenByDescending(x => x.reconDate).ToList();
        case "type":
          return reconciledList.OrderByDescending(x => x.reconType).ThenByDescending(x => x.workgroup).ThenBy(x => x.reconDate).ToList();
        default:
          return reconciledList.OrderBy(x => x.workgroup).ThenByDescending(x => x.reconDate).ToList();
      }
    }

    /// <summary>
    /// Filter the Reconciled Deposit list by the workgroup (DEPTID).
    /// This cannot easily be done in the database so doing it manaual in memory.
    /// </summary>
    /// <param name="reconciledList"></param>
    /// <param name="workgroupID"></param>
    /// <returns></returns>
    private List<ReconciledDeposit> filterReconciledListByWorkgroup(List<ReconciledDeposit> reconciledList, string workgroupID)
    {
      // If we want to filter on the workgroup, add the predicate here
      // Filter out ReconciledDeposits that do not contain the workgroup
      List<ReconciledDeposit> filteredList = new List<ReconciledDeposit>();
      foreach (ReconciledDeposit rd in reconciledList)
      {
        string workgroup = rd.workgroup;

        // Ignored Bank deposits will have not Expected component and thus no workgroup
        // If there are no workgroups and this is an ignore reconcilation, assume it is an ignored bank deposit so include
        if (workgroup == "" && rd.reconType == ReconciledDeposit.rType.ignored)
        {
          //filteredList.Add(rd);   // Not sure here, but I think we should skip these if we are filtering.
          continue;  // Goto the next ReconciledDeposit
        }

        // Skip deposits with no workgroup; we are trying to filter
        if (string.IsNullOrEmpty(workgroup))
          continue;


        // Now see if there is a match
        string[] workgroups = workgroup.Split(',');
        foreach (string w in workgroups)
        {
          if (workgroupID == w)
          {
            filteredList.Add(rd);
          }
        }
      }
      return filteredList;
    }


    /// <summary>
    /// Save the list of parsed bank deposits to the TG_BANK_DEPOSIT_DATA.
    /// Uses a parameterize & prepared statement to avoid date format
    /// problems, security, and performance.
    /// </summary>
    /// <param name="filename"></param>
    /// <param name="bankDeposits"></param>
    /// <param name="status"></param>
    private void saveImportedBankDeposits(String filename, List<BankDepositDetails> bankDeposits, out bool duplicate, out int recordCounter, out ReconStatus status)
    {
      recordCounter = 0;
      duplicate = false;
      status = new ReconStatus(true);

      SqlDateTime importTimestamp = new SqlDateTime(DateTime.Now);

      string duplicateInfo = checkForDuplicates(bankDeposits, ref status);
      if (duplicateInfo != "")
      {
        //String msg = "Duplicate Bank/BAI records cannot be imported into the database";

        // Bug 13050: UMN make error a warning, so duplicate files that are imported do
        // get deleted on the customer's FTP server, assuming delete files on server
        // is set in bank rec config in General Info
        status.setWarning(duplicateInfo, "REC-161");
        duplicate = true;
        return;
      }

      String conString = (String)m_dbConnection.get("db_connection_string");

      String bankSql =
         @"INSERT INTO TG_BANK_DEPOSIT_DATA
                  (DEPOSIT_DATE, ACCOUNT_ID, REF_NUM, BANK_REFERENCE, AMOUNT, STATUS, TEXT, IMPORT_TIMESTAMP, CHECKSUM) 
              VALUES
                  (@deposit_date, @account_id, @ref_num, @bankReference, @amount, @status, @text, @import_timestamp, @checksum)";

      // Cache the connections here so that finally can close
      SqlConnection updateConnection = new SqlConnection();
      SqlTransaction transaction = null;
      BankDepositDetails currentDetails = null;
      try
      {
        // Open a connection for the update
        updateConnection.ConnectionString = conString;
        updateConnection.Open();
        SqlCommand updateCommand = new SqlCommand(bankSql, updateConnection);

        // Prepare the statement once and only once
        updateCommand.Prepare();

        // Create and setup parameters once and only once
        // but do not set their values here
        updateCommand.Parameters.Add(new SqlParameter("@deposit_date", SqlDbType.DateTime));
        updateCommand.Parameters.Add(new SqlParameter("@account_id", SqlDbType.VarChar));
        updateCommand.Parameters.Add(new SqlParameter("@ref_num", SqlDbType.VarChar));
        updateCommand.Parameters.Add(new SqlParameter("@bankReference", SqlDbType.VarChar));
        updateCommand.Parameters.Add(new SqlParameter("@amount", SqlDbType.Float));
        updateCommand.Parameters.Add(new SqlParameter("@status", SqlDbType.VarChar));
        updateCommand.Parameters.Add(new SqlParameter("@text", SqlDbType.VarChar));
        updateCommand.Parameters.Add(new SqlParameter("@import_timestamp", SqlDbType.DateTime));
        updateCommand.Parameters.Add(new SqlParameter("@checksum", SqlDbType.VarChar));

        transaction = updateConnection.BeginTransaction();
        updateCommand.Transaction = transaction;
        foreach (BankDepositDetails deposit in bankDeposits)
        {
          currentDetails = deposit;
          // Trim and clip inplace so checksum can run
          deposit.account = clip(deposit.account, m_accountIdLenBankTable);
          deposit.refNum = clip(deposit.refNum, m_refNumLenBankTable);
          deposit.bankReference = clip(deposit.bankReference, m_bankReferenceLenBankTable);
          deposit.text = trimBankField(deposit, m_bankDepositTextFieldLength);
          deposit.computeChecksum();

          updateCommand.Parameters[0].Value = deposit.depDate;
          updateCommand.Parameters[1].Value = deposit.account;
          updateCommand.Parameters[2].Value = deposit.refNum;
          updateCommand.Parameters[3].Value = deposit.bankReference;
          updateCommand.Parameters[4].Value = deposit.amount;  // Implicit conversion of Decimal to Float
          updateCommand.Parameters[5].Value = BankDeposit.convertStatus(deposit.depositStatus);
          updateCommand.Parameters[6].Value = deposit.text;
          updateCommand.Parameters[7].Value = importTimestamp;
          updateCommand.Parameters[8].Value = deposit.checksum;

          updateCommand.ExecuteNonQuery();
          recordCounter++;
        }
        transaction.Commit();
      }
      catch (Exception e)
      {
        FileInfo fileInfo = new FileInfo(filename);

        String moveMessage = "  This file has been moved to the FailedFiles subdirectory.  ";
        if (transaction != null)
          transaction.Rollback();
        if (e.Message.StartsWith("Cannot insert duplicate key row in object"))
        {
          String duplicateExceptionInfo = getDuplicateInfo(currentDetails);

          String msg = String.Format("Duplicate Bank/BAI records cannot be imported into the database.  Filename '{0}':  Line: {1} ",
            fileInfo.Name, recordCounter);
          status.setErr(msg, msg + ": " + filename + moveMessage + duplicateExceptionInfo,
             "REC-161", false);
        }
        else
        {
          String msg = String.Format("Error saving bank deposits to the database for {0}: Line: {1}: Error {2} ", fileInfo.Name, recordCounter, e.Message);
          status.setErr(msg, msg + ": " + filename + moveMessage +
            "saveImportedBankDeposits failed: parameterized INSERT failed: " + e.ToString() + ": " + e.StackTrace,
             "REC-159", false);
        }

        // Bug 17849
        Logger.cs_log_trace("Error in saveImportedBankDeposits", e);
      }
      finally
      {
        if (updateConnection != null)
          updateConnection.Close();
      }
    }

    /// <summary>
    /// Make sure that the import records do not collide with existing records.
    /// Take the list of imported records and loop through them and query each one
    /// in the database to make sure there is no duplicate.  Duplicate records within the same
    /// import file are OK, but not between new records and old existing records
    /// in DB.
    /// This can be toggled in the Bank Rec Config page.
    /// Bug 11078
    /// </summary>
    /// <param name="bankDeposits"></param>
    /// <param name="status"></param>
    /// <returns></returns>
    private string checkForDuplicates(List<BankDepositDetails> bankDeposits, ref ReconStatus status)
    {
      // Bug 11515
      //GenericObject reconGeo = (GenericObject)c_CASL.c_object("Business.bank_deposit_recon.data");
      //bool checkForDuplicates = (bool)reconGeo.get("enable_import_duplicate_check", false);
      string checkForDuplicatesString = getConfigValue("enable_import_duplicate_check", "false");
      bool checkForDuplicates = Convert.ToBoolean(checkForDuplicatesString);
      // End 11515
      if (checkForDuplicates == false)
        return "";

      foreach (BankDepositDetails details in bankDeposits)
      {
        string duplicateInfo = getDuplicateInfo(details);
        if (duplicateInfo != "")
          return duplicateInfo;
      }
      return "";
    }

    /// <summary>
    /// Get more information if a duplicate is found.
    /// BUG: 9583: Thurber: Improve the file import error messages when a duplicate is found.
    /// </summary>
    /// <param name="deposit"></param>
    /// <returns></returns>
    private string getDuplicateInfo(BankDepositDetails deposit)
    {
      String dupMsg = "";

      String conString = (String)m_dbConnection.get("db_connection_string");

      String duplicateSql =
         @"select BANK_DEPOSIT_ID, STATUS from TG_BANK_DEPOSIT_DATA WHERE 
	            DEPOSIT_DATE = @deposit_date
	            AND ACCOUNT_ID = @account_id
	            AND REF_NUM = @ref_num
	            AND BANK_REFERENCE = @bankReference
	            AND TEXT = @text";

      // Cache the connections here so that finally can close
      SqlDataReader dataReader = null;
      var status = new ReconStatus(true);
      try
      {
        // Bug 17179: Protect against unset fields.  Cannot use DBNull because there are no nulls in Bank Rec
        string bankRef = deposit.bankReference;
        if (bankRef == null)
          bankRef = "";

        // Bug 17551
        string refNum = deposit.refNum;
        if (refNum == null)
          refNum = "";
        // End bug 17551

        string textField = trimBankField(deposit, m_bankDepositTextFieldLength);
        if (textField == null)
          textField = "";
        // End Bug 17179

        dataReader = setupReader(duplicateSql, ref status,
             param("deposit_date", SqlDbType.DateTime, deposit.depDate),
             param("account_id", SqlDbType.VarChar, deposit.account),
             param("ref_num", SqlDbType.VarChar, refNum),           // Bug 17551
             param("bankReference", SqlDbType.VarChar, bankRef),
             param("text", SqlDbType.VarChar, textField)
             );

        if (!status.success)
          return " Internal error: cannot query for duplicate information: " + status.detailedError;

        // Most records will not be found; bail out here
        if (!dataReader.HasRows)
          return "";

        string humanReadable = "";
        if (dataReader.Read())
        {
          int id = dataReader.GetInt32(0);
          String bankStatus = dataReader.GetString(1);
          BankDeposit.bStatus bStatus = BankDeposit.convertStatus(bankStatus);
          if (bStatus == BankDeposit.bStatus.reconciled)
          {
            dupMsg = " This deposit record has already been imported and reconciled. ";
            humanReadable = "Cannot import file; this deposit record has already been imported and reconciled. ";
          }
          else
          {
            dupMsg = " This deposit record has already been imported and not yet reconciled. ";
            humanReadable = "Cannot import file; this deposit record has already been imported and not yet reconciled. ";
          }


          dupMsg += "Deposit Date: '" + deposit.depDate +
              "' Account: '" + deposit.account +
              "' Customer number: '" + deposit.refNum +
              "' Bank Reference Number: '" + deposit.bankReference +
              "' Amount: '" + deposit.amount.ToString("C") +
              "' Text: '" + deposit.text + "'";

          Logger.cs_log("Bank Rec Warning: Import Failed: " + dupMsg);      // Record the detailed information here
          humanReadable += "Deposit Date '" + deposit.depDate.ToString().Replace(':', ' ') + "' Amount '" + deposit.amount + "'";
          return humanReadable;
        }
        else
        {
          dupMsg = " Internal ERROR: cannot query for duplicate error. ";
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in getDuplicateInfo", e);

        dupMsg = " Internal ERROR: cannot query the TG_BANK_DEPOSIT_DATA table for a duplicate record:  " + e.ToString();
      }
      finally
      {
        if (dataReader != null)
          dataReader.Close();
      }
      return dupMsg;
    }

    /// <summary>
    /// Read the schema data for the bank table and find the field sizes.
    /// </summary>
    /// <param name="accountIdLen"></param>
    /// <param name="refNumLen"></param>
    /// <param name="bankReferenceLen"></param>
    /// <param name="textLen"></param>
    private void getDepositTableFieldSizes(out int userIdLen, out int commentLen)
    {
      const int DEFAULT_USERID_SIZE = 32;
      const int DEFAULT_COMMENT_FIELD_SIZE = 128;

      // Use the quickest possible SQL query for all the columns
      String dataReaderSQL = "SELECT TOP(1) * FROM TG_RECONCILED_DATA";

      SqlConnection dataReaderConnection = new SqlConnection();
      SqlDataReader dataReader = null;
      userIdLen = DEFAULT_USERID_SIZE;
      commentLen = DEFAULT_COMMENT_FIELD_SIZE;
      try
      {
        String conString = (String)m_dbConnection.get("db_connection_string");

        // Open a connection for the DataReader
        dataReaderConnection.ConnectionString = conString;
        dataReaderConnection.Open();

        SqlCommand dataReaderCommand = new SqlCommand(dataReaderSQL, dataReaderConnection);
        dataReader = dataReaderCommand.ExecuteReader();
        DataTable schema = dataReader.GetSchemaTable();

        // Bug #12055 Mike O - Use misc.Parse to log errors
        for (int i = 0; i < dataReader.FieldCount; i++)
        {
          string name = schema.Rows[i]["ColumnName"].ToString();
          if (System.String.Compare(name, "USERID", true) == 0)
            userIdLen = misc.Parse<Int32>(schema.Rows[i]["ColumnSize"]);
          if (System.String.Compare(name, "COMMENT", true) == 0)
            commentLen = misc.Parse<Int32>(schema.Rows[i]["ColumnSize"]);
        }
      }
      finally
      {
        if (dataReader != null && !dataReader.IsClosed)
          dataReader.Close();
        dataReaderConnection.Close();
      }
    }


    private Decimal getTotalBankAmount(
      List<DatabaseID> bankReconList,
      Decimal overShort,
      ReconciledDeposit.rType type,
      ref ReconStatus status)
    {
      status.success = true;

      // Bug 9611: Do not zero this out for Ignored.
      // Set amount to 0 if this is an Ignored transaction
      //if (type == ReconciledDeposit.rType.ignored)
      //{
      //  return 0.0M;
      //}

      StringBuilder ids = new StringBuilder();
      ids.Append('(');
      foreach (DatabaseID id in bankReconList)
      {
        if (ids.Length > 1)
        {
          ids.Append(',');
        }
        ids.Append(id.databaseKey);
      }
      ids.Append(')');
      // TODO: Not sure how overshorts would figure in here 
      String dataReaderSQL = @"
                        SELECT SUM(AMOUNT) AS TOTAL_AMOUNT FROM TG_BANK_DEPOSIT_DATA 
                            WHERE BANK_DEPOSIT_ID IN " + ids;

      String conString = (String)m_dbConnection.get("db_connection_string");

      // Cache the connections here so that finally can close
      SqlConnection dataReaderConnection = new SqlConnection();
      SqlDataReader dataReader = null;
      try
      {
        // Open a connection for DataReader
        dataReaderConnection.ConnectionString = conString;
        dataReaderConnection.Open();
        SqlCommand dataReaderCommand = new SqlCommand(dataReaderSQL, dataReaderConnection);

        dataReader = dataReaderCommand.ExecuteReader(CommandBehavior.CloseConnection);
        while (dataReader.Read())
        {
          if (dataReader.IsDBNull(0))
          {
            status.setErr("Internal error: cannot get bank deposit total amount because database identifiers were not found",
              "getTotalBankAmount() failed: no database ID found in this list: " + ids + " with this sql: " + dataReaderSQL,
              "REC-178", true);
            return 0M;
          }
          double totalAmount = dataReader.GetDouble(0);
          // Bug #12055 Mike O - Use misc.Parse to log errors
          return misc.Parse<Decimal>(totalAmount);
        }
      }
      finally
      {
        if (dataReader != null)
          dataReader.Close();
        dataReaderConnection.Close();
      }
      return 0M;
    }

    /// <summary>
    /// Get total amount from expected deposit table.
    /// Bug 9611; Thurber
    /// </summary>
    /// <param name="expectedReconList"></param>
    /// <param name="overShort"></param>
    /// <param name="type"></param>
    /// <param name="status"></param>
    /// <returns></returns>
    private Decimal getTotalExpectedAmount(
      List<DatabaseID> expectedReconList,
      Decimal overShort,
      ReconciledDeposit.rType type,
      ref ReconStatus status)
    {
      double totalAmount = 0.0f;

      foreach (DatabaseID id in expectedReconList)
      {
        totalAmount += getExpectedAmount(id, ref status);
      }
      // Bug #12055 Mike O - Use misc.Parse to log errors
      return misc.Parse<Decimal>(totalAmount);
    }

    private double getExpectedAmount(
      DatabaseID id,
      ref ReconStatus status)
    {
      status.success = true;

      String depositId;
      int depositNbr, uniqueId;
      id.parseExpectedKey(out depositId, out depositNbr, out uniqueId, ref status);
      if (!status.success)
        return 0.0f;  // Having a very bad day

      String dataReaderSQL =
        @"SELECT AMOUNT FROM TG_DEPOSIT_DATA WHERE 
	          DEPOSITID = @depositId 
            AND DEPOSITNBR = @depositNbr 
            AND UNIQUEID = @uniqueId";

      // Cache the connections here so that finally can close it
      SqlConnection dataReaderConnection = new SqlConnection();
      SqlDataReader dataReader = null;
      try
      {
        // Open a connection for DataReader
        String conString = (String)m_dbConnection.get("db_connection_string");
        dataReaderConnection.ConnectionString = conString;
        dataReaderConnection.Open();
        SqlCommand dataReaderCommand = new SqlCommand(dataReaderSQL, dataReaderConnection);

        // Set parameters
        SqlParameter param = new SqlParameter("@depositId", SqlDbType.VarChar);
        param.Value = depositId;
        dataReaderCommand.Parameters.Add(param);
        param = new SqlParameter("@depositNbr", SqlDbType.Int);
        param.Value = depositNbr;
        dataReaderCommand.Parameters.Add(param);
        param = new SqlParameter("@uniqueId", SqlDbType.Int);
        param.Value = uniqueId;
        dataReaderCommand.Parameters.Add(param);

        dataReader = dataReaderCommand.ExecuteReader(CommandBehavior.CloseConnection);
        while (dataReader.Read())
        {
          if (dataReader.IsDBNull(0))
          {
            return 0.0f;
          }
          double amount = dataReader.GetDouble(0);
          return amount;
        }
      }
      finally
      {
        if (dataReader != null)
          dataReader.Close();
        dataReaderConnection.Close();
      }
      return 0.0f;
    }

    private bool expectedDepositExists(String depositId, int depositNbr, int uniqueId, ref ReconStatus status)
    {
      status.success = true;
      String dataReaderSQL =
        @"select count(*) FROM TG_DEPOSIT_DATA WHERE 
	          DEPOSITID = @depositId 
            AND DEPOSITNBR = @depositNbr 
            AND UNIQUEID = @uniqueId";

      // Cache the connections here so that finally can close it
      SqlConnection dataReaderConnection = new SqlConnection();
      SqlDataReader dataReader = null;
      try
      {
        // Open a connection for DataReader
        String conString = (String)m_dbConnection.get("db_connection_string");
        dataReaderConnection.ConnectionString = conString;
        dataReaderConnection.Open();
        SqlCommand dataReaderCommand = new SqlCommand(dataReaderSQL, dataReaderConnection);

        // Set parameters
        SqlParameter param = new SqlParameter("@depositId", SqlDbType.VarChar);
        param.Value = depositId;
        dataReaderCommand.Parameters.Add(param);
        param = new SqlParameter("@depositNbr", SqlDbType.Int);
        param.Value = depositNbr;
        dataReaderCommand.Parameters.Add(param);
        param = new SqlParameter("@uniqueId", SqlDbType.Int);
        param.Value = uniqueId;
        dataReaderCommand.Parameters.Add(param);

        dataReader = dataReaderCommand.ExecuteReader(CommandBehavior.CloseConnection);
        while (dataReader.Read())
        {
          if (dataReader.IsDBNull(0))
          {
            status.setErr("Internal error: cannot reconcile deposits because database query failed",
              "expectedDepositExists() failed: null return from this sql: " + dataReaderSQL,
              "REC-163", true);
            return false;
          }
          int count = dataReader.GetInt32(0);
          return count > 0 ? true : false;
        }
      }
      finally
      {
        if (dataReader != null)
          dataReader.Close();
        dataReaderConnection.Close();
      }
      return false;
    }

    private void validateExpectedReconciliations(List<DatabaseID> expectedReconList, ref ReconStatus status)
    {
      status.success = true;

      // First make sure that deposit exists
      foreach (DatabaseID id in expectedReconList)
      {
        String depositId;
        int depositNbr;
        int uniqueId;
        id.parseExpectedKey(out depositId, out depositNbr, out uniqueId, ref status);
        if (!status.success)
          return;
        bool found = expectedDepositExists(depositId, depositNbr, uniqueId, ref status);
        if (!status.success)
          return;
        if (!found)
        {
          status.setErr("Internal error: Cannot reconcile expected deposit; deposit not found in the database",
            String.Format(@"Reconciliation failed: this expected deposit does not exist: 
              depositId: {0}  depositNbr: {1} uniqueId: {2}",
              depositId, depositNbr, uniqueId), "REC-164", true);
          return;
        }
      }
      // Next make sure it has not been reconciled already
      foreach (DatabaseID id in expectedReconList)
      {
        String depositId;
        int depositNbr;
        int uniqueId;
        id.parseExpectedKey(out depositId, out depositNbr, out uniqueId, ref status);
        if (!status.success)
          return;
        bool reconciled = expectedDepositReconciled(depositId, depositNbr, uniqueId, ref status);
        if (!status.success || reconciled)
          return;
      }
    }

    /// <summary>
    /// Determine if an expected deposit has been reconciled
    /// </summary>
    /// <param name="depositId"></param>
    /// <param name="depositNbr"></param>
    /// <param name="uniqueId"></param>
    /// <param name="status"></param>
    /// <returns>True if this expected deposit has been reconciled, false otherwise</returns>
    private bool expectedDepositReconciled(string depositId, int depositNbr, int uniqueId, ref ReconStatus status)
    {
      status.success = true;

      // Reverse the usual expected deposit search and start by looking at the TG_RECONCILED_DATA
      // to get the actual, real deposits.
      // Join the xref table to get the expected databaseID and look through that
      // to find a match among the reconciled.
      String dataReaderSQL =
        @"SELECT recon.RECON_ID, RECON_DATE, USERID, TYPE, COMMENT 
	        FROM TG_RECONCILED_DATA as recon, TG_RECONCILED_EXPECTED_DATA as xref
	        WHERE recon.RECON_ID IN 
               (SELECT xref.RECON_ID FROM TG_RECONCILED_EXPECTED_DATA)
			      AND xref.DEPOSITID = @depositId 
            AND xref.DEPOSITNBR = @depositNbr 
            AND xref.UNIQUEID = @uniqueId
            AND recon.STATUS != 'v'";

      // Cache the connections here so that finally can close it
      SqlConnection dataReaderConnection = new SqlConnection();
      SqlDataReader dataReader = null;
      try
      {
        // Open a connection for DataReader
        String conString = (String)m_dbConnection.get("db_connection_string");
        dataReaderConnection.ConnectionString = conString;
        dataReaderConnection.Open();
        SqlCommand dataReaderCommand = new SqlCommand(dataReaderSQL, dataReaderConnection);

        // Set parameters
        SqlParameter param = new SqlParameter("@depositId", SqlDbType.VarChar);
        param.Value = depositId;
        dataReaderCommand.Parameters.Add(param);
        param = new SqlParameter("@depositNbr", SqlDbType.Int);
        param.Value = depositNbr;
        dataReaderCommand.Parameters.Add(param);
        param = new SqlParameter("@uniqueId", SqlDbType.Int);
        param.Value = uniqueId;
        dataReaderCommand.Parameters.Add(param);

        dataReader = dataReaderCommand.ExecuteReader(CommandBehavior.CloseConnection);

        // If there are no rows then the deposit has not been reconciled
        bool hasRows = dataReader.HasRows;
        if (!hasRows)
          return false;

        // Rows found.  Return diagnostics
        StringBuilder humanReadable = new StringBuilder();
        humanReadable.Append("Cannot reconcile deposit.  This deposit was already reconciled on ");
        StringBuilder systemMessge = new StringBuilder();
        systemMessge.Append("Expected deposit already reconciled: ");
        systemMessge.Append(" DEPOSITID: ").Append(depositId);
        systemMessge.Append(" DEPOSITNBR: ").Append(depositNbr);
        systemMessge.Append(" UNIQUEID: ").Append(uniqueId);
        systemMessge.Append(": ");


        int commentOrd = dataReader.GetOrdinal("COMMENT");

        while (dataReader.Read())
        {
          if (dataReader.IsDBNull(0))
          {
            status.setErr("Internal error: cannot reconcile deposits because duplicate reconciliation query failed",
              "expectedDepositReconciled() failed: null return from this sql: " + dataReaderSQL,
              "REC-165", true);
            return false;
          }
          int reconId = dataReader.GetInt32(dataReader.GetOrdinal("RECON_ID"));
          DateTime reconDate = dataReader.GetDateTime(dataReader.GetOrdinal("RECON_DATE"));
          String userid = dataReader.GetString(dataReader.GetOrdinal("USERID"));
          String reconType = dataReader.GetString(dataReader.GetOrdinal("TYPE"));
          String comment = dataReader.IsDBNull(commentOrd) ? "" : dataReader.GetString(commentOrd);
          humanReadable.Append(reconDate).Append("   by: ").Append(userid);
          systemMessge.Append("\nRECONID: ").Append(reconId)
            .Append("\nRECON_DATE: ").Append(reconDate)
            .Append("\nUSERID: ").Append(userid)
            .Append("\nType of reconciliation: ").Append(ReconciledDeposit.convertReconType(reconType))
            .Append("\nCOMMENT: ").Append(comment);
        }
        status.setErr(humanReadable.ToString(), systemMessge.ToString(), "REC-167", false);
      }
      finally
      {
        if (dataReader != null)
          dataReader.Close();
        dataReaderConnection.Close();
      }
      return false;
    }

    private void validateBankReconciliations(List<DatabaseID> bankReconList, ref ReconStatus status)
    {
      status.success = true;

      // First make sure that deposit exists
      foreach (DatabaseID id in bankReconList)
      {
        bool exists = bankDepositExists(id, ref status);
        if (!status.success)
          return;  // Found a nasty
        if (!exists)
        {
          status.setErr("Internal error: Cannot reconcile bank deposit; this deposit not found in the database",
            "Reconciliation failed: this bank deposit does not exist: ID: " + id.databaseKey, "REC-169", true);
          return;
        }
      }

      // Next make sure it has not been reconciled already
      foreach (DatabaseID id in bankReconList)
      {
        bool reconciled = bankDepositReconciled(id, ref status);
        if (!status.success || reconciled)
          return;
      }

    }

    private bool bankDepositReconciled(DatabaseID id, ref ReconStatus status)
    {
      status.success = true;

      // Reverse the usual bank deposit search and start by looking at the TG_RECONCILED_DATA
      // to get the actual, real deposits.
      // Join the xref table to get the bank deposit ID and look through that
      // to find a match among the reconciled.
      String dataReaderSQL =
        @"SELECT recon.RECON_ID, RECON_DATE, USERID, TYPE, COMMENT 
	        FROM TG_RECONCILED_DATA as recon, TG_RECONCILED_BANK_DATA as xref
	        WHERE recon.RECON_ID IN 
               (SELECT xref.RECON_ID FROM TG_RECONCILED_BANK_DATA)
			      AND xref.BANK_DEPOSIT_ID = @id
            AND recon.STATUS != 'v'";

      // Cache the connections here so that finally can close it
      SqlConnection dataReaderConnection = new SqlConnection();
      SqlDataReader dataReader = null;
      try
      {
        // Open a connection for DataReader
        String conString = (String)m_dbConnection.get("db_connection_string");
        dataReaderConnection.ConnectionString = conString;
        dataReaderConnection.Open();
        SqlCommand dataReaderCommand = new SqlCommand(dataReaderSQL, dataReaderConnection);

        // Set parameter
        SqlParameter param = new SqlParameter("@id", SqlDbType.Int);
        // Bug #12055 Mike O - Use misc.Parse to log errors
        param.Value = misc.Parse<Int32>(id.databaseKey);
        dataReaderCommand.Parameters.Add(param);

        dataReader = dataReaderCommand.ExecuteReader(CommandBehavior.CloseConnection);

        // If there are no rows then the deposit has not been reconciled
        bool hasRows = dataReader.HasRows;
        if (!hasRows)
          return false;

        // Rows found.  Return diagnostics
        StringBuilder humanReadable = new StringBuilder();
        humanReadable.Append("Cannot reconcile deposit.  This bank deposit was already reconciled on ");
        StringBuilder systemMessge = new StringBuilder();
        systemMessge.Append("Bank deposit already reconciled: ");

        int commentOrd = dataReader.GetOrdinal("COMMENT");

        while (dataReader.Read())
        {
          if (dataReader.IsDBNull(0))
          {
            status.setErr("Internal error: cannot reconcile bank deposits because duplicate reconciliation query failed",
              "bankDepositReconciled() failed: null return from this sql: " + dataReaderSQL,
              "REC-170", true);
            return false;
          }
          int reconId = dataReader.GetInt32(dataReader.GetOrdinal("RECON_ID"));
          DateTime reconDate = dataReader.GetDateTime(dataReader.GetOrdinal("RECON_DATE"));
          String userid = dataReader.GetString(dataReader.GetOrdinal("USERID"));
          String reconType = dataReader.GetString(dataReader.GetOrdinal("TYPE"));
          String comment = dataReader.IsDBNull(commentOrd) ? "" : dataReader.GetString(commentOrd);
          humanReadable.Append(reconDate).Append("   by: ").Append(userid);
          systemMessge.Append("\nRECONID: ").Append(reconId)
            .Append("\nRECON_DATE: ").Append(reconDate)
            .Append("\nUSERID: ").Append(userid)
            .Append("\nType of reconciliation: ").Append(ReconciledDeposit.convertReconType(reconType))
            .Append("\nCOMMENT: ").Append(comment);
        }
        status.setErr(humanReadable.ToString(), systemMessge.ToString(), "REC-171", false);
      }
      finally
      {
        if (dataReader != null)
          dataReader.Close();
        dataReaderConnection.Close();
      }
      return false;
    }

    /// <summary>
    /// Check to make sure a bank deposit really exists
    /// </summary>
    /// <param name="id"></param>
    /// <param name="status"></param>
    /// <returns>true if deposit exists, false otherwise</returns>
    private bool bankDepositExists(DatabaseID id, ref ReconStatus status)
    {
      status.success = true;
      String dataReaderSQL =
        @"select count(*) FROM TG_BANK_DEPOSIT_DATA WHERE 
	          BANK_DEPOSIT_ID = @id";

      // Cache the connections here so that finally can close it
      SqlConnection dataReaderConnection = new SqlConnection();
      SqlDataReader dataReader = null;
      try
      {
        // Open a connection for DataReader
        String conString = (String)m_dbConnection.get("db_connection_string");
        dataReaderConnection.ConnectionString = conString;
        dataReaderConnection.Open();
        SqlCommand dataReaderCommand = new SqlCommand(dataReaderSQL, dataReaderConnection);

        // Set parameter
        SqlParameter param = new SqlParameter("@id", SqlDbType.Int);
        // Bug #12055 Mike O - Use misc.Parse to log errors
        param.Value = misc.Parse<Int32>(id.databaseKey);
        dataReaderCommand.Parameters.Add(param);

        dataReader = dataReaderCommand.ExecuteReader(CommandBehavior.CloseConnection);
        while (dataReader.Read())
        {
          if (dataReader.IsDBNull(0))
          {
            status.setErr("Internal error: cannot reconcile bank deposits because database query failed",
              "expectedDepositExists() failed: null return from this sql: " + dataReaderSQL + " for id: " + id,
              "REC-168", true);
            return false;
          }
          int count = dataReader.GetInt32(0);
          return count > 0 ? true : false;
        }
      }
      finally
      {
        if (dataReader != null)
          dataReader.Close();
        dataReaderConnection.Close();
      }
      return false;
    }

    /// <summary>
    /// Translate from the bank account (such as "001234") to Bank ID (such as "002").
    /// May never be called!
    /// </summary>
    /// <param name="accountNumber"></param>
    /// <param name="status"></param>
    /// <returns></returns>
    private List<string> bankAccount2bankID(string accountNumber, ref ReconStatus status)
    {
      // Grab if cached
      //String bankID;
      // TODO: see if this really caches
      //if (m_cachedBankIDs.TryGetValue(accountNumber, out bankID))
      // return bankID;

      SqlDataReader dataReader = null;

      // This might be overkill
      // TODO: review this and see bug 9602; can the subquery return more than one
      // value?
      // Definitely yes!!!  Had to fix this for Bug 11479.   A single bank account 
      // can have multiple BANKIDs.
      String flexStr =
          @"SELECT field_value from CBT_FLEX 
	            WHERE container IN (SELECT container FROM CBT_FLEX 
							                    WHERE field_key = '""account_nbr""' 
							                    AND field_value = @fieldValue)
	            AND field_key = '""id""'";

      var bankIDs = new List<string>();
      try
      {
        if (accountNumber == m_unsetAccountNbr)
          accountNumber = "";
        dataReader = setupReader(flexStr, true, ref status,
             param("fieldValue", SqlDbType.VarChar, addQuotes(accountNumber))
          );

        while (dataReader.Read())
        {
          string bankID = dataReader.GetString(0);
          bankID = stripQuotes(bankID);
          bankIDs.Add(bankID);
        }
        /*
        // Update caches
        if (!m_cachedBankAccounts.ContainsKey(bankID))
          m_cachedBankAccounts.Add(bankID, accountNumber);
        if (!m_cachedBankIDs.ContainsKey(accountNumber))
          m_cachedBankIDs.Add(accountNumber, bankID);
         */

        return bankIDs;
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in bankAccount2bankID", e);

        status.setWarning("Error translating bank account to bank ID: " +
            String.Format("bankAccount2bankID failed: SQL or conversion error: Error: {0}: Traceback: {1}",
                e, e.StackTrace),
            "REC-158");
      }
      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
      }
      return bankIDs;
    }


    /// <summary>
    /// Get a range of reconciled deposits.
    /// If the startDate and endDate are not set, just get the last 24 hours.
    /// Assume that the startDate and endDate are just the date with no time;
    /// in this case we need to round the end time up.
    /// </summary>
    /// <param name="startDate"></param>
    /// <param name="endDate"></param>
    /// <param name="deptID2MatchKey">A mapping between the depfile's DEPTID 
    /// and the workgroup's Bank Reconciliation Key (BRK).  
    /// This must be calculated in CASL beforehand.  
    /// This is needed to insert the match key (BRK) when the reference number is blank 
    /// for credit card deposits.  Bug 11549</param>
    /// <param name="status"></param>
    /// <returns></returns>
    public List<ReconciledDeposit> getReconciledDepositList(string startDate, string endDate, GenericObject deptID2MatchKey, out ReconStatus status)
    {
      status = new ReconStatus(true);

      // Bug 9777: FT: Reversed sort order
      String dataReaderSQL;

      // Special case the default to be the last 24 hours
      if (startDate == "" && endDate == "")
      {
        return getDefaultReconcilatedDateRange(ref status, deptID2MatchKey);
      }

      DateTime start, end;

      // Otherwise explicitly set start and end dates
      try
      {
        // If the start date is not set, use a small value.
        // Cannot use DateTime.MinDate since this is too low for SQL Server.  
        // Used Unix min date instead.
        if (startDate == "")
          start = DateTime.Parse("1/1/1970");
        else
          start = DateTime.Parse(startDate);

        // Check the end date.  Use the current date if not set.
        if (endDate == "")
          end = DateTime.Now;
        else
        {
          // End date was set.  Need to round up to the end of the day
          DateTime endDateTruncated = DateTime.Parse(endDate);
          end = new DateTime(endDateTruncated.Year, endDateTruncated.Month, endDateTruncated.Day, 23, 59, 59);
        }
      }
      catch (FormatException e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in getReconciledDepositList", e);
        // Protect against bogus dates
        Logger.cs_log(String.Format("Bad date entered for Reconciled Desposit date filters: {0} : {1}",
          startDate, endDate));
        return getDefaultReconcilatedDateRange(ref status, deptID2MatchKey);
      }

      dataReaderSQL =
        @"SELECT TOP({0}) * FROM TG_RECONCILED_DATA 
          WHERE RECON_DATE >= @startDate AND RECON_DATE <= @endDate
          AND STATUS != 'v'
          ORDER BY RECON_DATE DESC";
      dataReaderSQL = string.Format(dataReaderSQL, getDBLimit());     // Bug 27450: Do not parameterize TOP
      return getReconciledDepositListInternal(dataReaderSQL, start, end, true, deptID2MatchKey, false, ref status);
    }

    private List<ReconciledDeposit> getDefaultReconcilatedDateRange(ref ReconStatus status, GenericObject deptID2MatchKey)
    {
      // Default to today if nothing specified (or erroneous specification)
      //DateTime yesterday = DateTime.Now - TimeSpan.FromHours(24.0);
      DateTime beginningOfToday = DateTime.Now.Date;


      string dataReaderSQL =
        @"SELECT TOP({0}) * FROM TG_RECONCILED_DATA 
            WHERE RECON_DATE >= @startDate AND RECON_DATE <= @endDate
            AND STATUS != 'v'
            ORDER BY RECON_DATE DESC";
      dataReaderSQL = string.Format(dataReaderSQL, getDBLimit());   // Bug 27450: Do not parameterize TOP
      return getReconciledDepositListInternal(dataReaderSQL, beginningOfToday, DateTime.Now, true, deptID2MatchKey, false, ref status);
    }

    private List<ReconciledDeposit> getReconciledDepositListInternal(String reconSql, DateTime startDate, DateTime endDate, bool dateRange, GenericObject deptID2MatchKey, bool extraFields, ref ReconStatus status)
    {

      var reconList = new List<ReconciledDeposit>();

      SqlDataReader dataReader = null;

      try
      {
        // Bug 11099: It was found that getting the slip numbers for each record
        // seriously slowed things down.  This was change to fetch all the relevant 
        // slip numbers once at the beginning
        var bankSlipNumbers = getBankReconciledSlipNumbers();

        var expectedSlipNumbers = getExpectedReconciledSlipNumbers(deptID2MatchKey);

        // Get the workgroups and files associated with each reconcilation
        Dictionary<int, HashSet<string>> workgroupDict = new Dictionary<int, HashSet<string>>();
        Dictionary<int, HashSet<string>> fileDict = new Dictionary<int, HashSet<string>>();
        if (extraFields)
          getWorkgroupsAndFiles_ExtraFields(startDate, endDate, workgroupDict, fileDict, dateRange);


        // End 11099

        if (!dateRange)
        {
          dataReader = setupReader(reconSql, ref status);
        }
        else
        {
          //System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
          //watch.Start();
          // Bug 27450: Do not parameterize TOP
          dataReader = setupReader(reconSql, ref status,
             param("startDate", SqlDbType.DateTime, startDate),
             param("endDate", SqlDbType.DateTime, endDate)
          );
          //watch.Stop();
          //Logger.cs_log("getReconciledDepositListInternal: took this many Milliseconds to read database for JS Array: " + watch.ElapsedMilliseconds);
        }
        if (!status.success)
          return reconList;

        int commentOrd = dataReader.GetOrdinal("COMMENT");
        int osOrd = dataReader.GetOrdinal("OVERSHORT");
        int checksumOrd = dataReader.GetOrdinal("CHECKSUM");
        while (dataReader.Read())
        {
          ReconciledDeposit rd = new ReconciledDeposit();
          rd.databaseID = new DatabaseID(dataReader.GetInt32(dataReader.GetOrdinal("RECON_ID")));
          rd.user = dataReader.GetString(dataReader.GetOrdinal("USERID"));
          // Comment is a null-able field.
          String comment = dataReader["COMMENT"].ToString();
          rd.reason = dataReader.IsDBNull(commentOrd) ? "" : dataReader.GetString(commentOrd);
          rd.reconDate = dataReader.GetDateTime(dataReader.GetOrdinal("RECON_DATE"));
          rd.reconStatus = ReconciledDeposit.convertReconStatus(dataReader.GetString(dataReader.GetOrdinal("STATUS")));
          rd.reconType = ReconciledDeposit.convertReconType(dataReader.GetString(dataReader.GetOrdinal("TYPE")));
          // Bug #12055 Mike O - Use misc.Parse to log errors
          rd.amount = misc.Parse<Decimal>(dataReader.GetDouble(dataReader.GetOrdinal("TOTAL_AMOUNT")));
          // OVERSHORT is a null-able field
          double overShort = dataReader.IsDBNull(osOrd) ? 0.00 : dataReader.GetDouble(osOrd);
          // Bug #12055 Mike O - Use misc.Parse to log errors
          rd.overShort = misc.Parse<Decimal>(overShort);
          // Bug 11099: getRefAndSlipNumbers was very slow so changed to use cached/hashed slip #'s
          // Bug 11433: Modified to put match/merchant account number in ref-num if match key exists
          rd.refNum = getRefAndSlipNumbersFromHashes(rd.databaseID, bankSlipNumbers, expectedSlipNumbers, out status);
          rd.checksum = dataReader.IsDBNull(checksumOrd) ? "" : dataReader.GetString(checksumOrd);
          addExtraFields(rd, extraFields, workgroupDict, fileDict);
          if (!rd.validateChecksum())
          {
            // Bug 11095: Log this error and keep going
            string msg = "Checksum failure when reading Reconciled Deposit list from the TG_RECONCILED_DATA table. Error detected in record: "
              + rd.ToString();
            status.setChecksumError(msg, "REC-191");
            // Bug 12303: FT: Write checksum error to activity log
            string recordInfo = string.Format("ID:{0}", rd.databaseID.databaseKey);
            TranSuiteServices.TranSuiteServices.LogChecksumError("TG_RECONCILED_DATA", "getReconciledDepositListInternal", recordInfo, rd.ToAuditString());
          }
          reconList.Add(rd);
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in getReconciledDepositListInternal", e);

        status.setErr("Cannot read the Reconciled Deposit list",
            String.Format("ReadIntoRecordset() or Data conversion failed {0}: {1}",
                e.ToString(), e.StackTrace),
                "REC-104", false);
      }
      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
      }

      return reconList;
    }

    /// <summary>
    /// Given a HashSet of the workgroups and a HashSet of filenames,
    /// flatten them to into comma-separated strings
    /// </summary>
    /// <param name="rd"></param>
    /// <param name="extraFields"></param>
    /// <param name="workgroupDict"></param>
    /// <param name="fileDict"></param>
    private void addExtraFields(ReconciledDeposit rd, bool extraFields,
      Dictionary<int, HashSet<string>> workgroupDict, Dictionary<int, HashSet<string>> fileDict)
    {
      // Get the reconID
      var databaseID = rd.databaseID;
      int reconId = Convert.ToInt32(databaseID.databaseKey);

      // Use the reconID to fetch the workgroup and file info
      HashSet<String> workgroupList = null;
      if (workgroupDict.TryGetValue(reconId, out workgroupList))
      {
        // Now convert the list to a string
        StringBuilder workgroupID = new StringBuilder();
        foreach (string workgroup in workgroupList)
        {
          if (workgroupID.Length == 0)
            workgroupID.Append(workgroup);
          else
            workgroupID.Append(',').Append(workgroup);
        }
        rd.workgroup = workgroupID.ToString();
      }

      // Use the reconID to fetch the workgroup and file info
      HashSet<String> filenameList = null;
      if (fileDict.TryGetValue(reconId, out filenameList))
      {
        // Now convert the list to a string
        StringBuilder filenames = new StringBuilder();
        foreach (string filename in filenameList)
        {
          if (filenames.Length == 0)
            filenames.Append(filename);
          else
            filenames.Append(',').Append(filename);
        }
        rd.file = filenames.ToString();
      }
    }

    /// <summary>
    /// Here is the key technique reading for workgroups and filenames in an efficient manner without 
    /// a super-complicated SQL statement.
    /// Each recon_id can have zero (Ignored bank deposit) or more filenames associated with them.
    /// Each recon_id can have zero or more workgroups, although this seems a litte farfetched, but this 
    /// code can handle it.
    /// </summary>
    /// <param name="startDate"></param>
    /// <param name="endDate"></param>
    /// <param name="workgroupDict"></param>
    /// <param name="fileDict"></param>
    /// <param name="daterange"></param>
    private void getWorkgroupsAndFiles_ExtraFields(DateTime startDate, DateTime endDate,
      Dictionary<int, HashSet<string>> workgroupDict, Dictionary<int, HashSet<string>> fileDict, bool daterange)
    {
      // TODO: if I also filtered on the DEPTID it would help performance a bit, maybe
      String sqlString =
            @"SELECT RECON_ID, DEPTID, expected.DEPFILENBR, expected.DEPFILESEQ
              FROM TG_DEPOSIT_DATA as expected, TG_RECONCILED_EXPECTED_DATA as xref, TG_DEPFILE_DATA as dfile
              WHERE
	                    expected.DEPOSITID = xref.DEPOSITID 
	                AND expected.DEPOSITNBR = xref.DEPOSITNBR 
	                AND expected.UNIQUEID = xref.UNIQUEID 
	                AND expected.DEPFILENBR = dfile.DEPFILENBR
	                AND expected.DEPFILESEQ = dfile.DEPFILESEQ
	                AND xref.RECON_ID IN (
						        SELECT TOP({2}) RECON_ID FROM TG_RECONCILED_DATA 
								WHERE RECON_DATE >= '{0}' AND RECON_DATE <= '{1}'
								ORDER BY RECON_DATE DESC
					        )";
      string formattedSql = string.Format(sqlString, startDate, endDate, getDBLimit());   // Bug 27450: Do not parameterize TOP

      SqlDataReader dataReader = null;
      try
      {
        var status = new ReconStatus(true);
        dataReader = setupReader(formattedSql, ref status);   // Bug 27450: Do not parameterize TOP

        while (dataReader.Read())
        {
          int reconId = dataReader.GetInt32(0);
          string deptId = dataReader.IsDBNull(1) ? "" : dataReader.GetString(1);
          int depfilenbr = dataReader.IsDBNull(2) ? 0 : dataReader.GetInt32(2);
          int depfileseq = dataReader.IsDBNull(3) ? 0 : dataReader.GetInt32(3);

          // Add the workgroup, if any, the list
          if (deptId != "")
          {
            HashSet<String> workgroupHashSet = null;
            if (workgroupDict.TryGetValue(reconId, out workgroupHashSet))
            {
              workgroupHashSet.Add(deptId);
              workgroupDict[reconId] = workgroupHashSet;  // Do I really need to do this???
            }
            else
            {
              // ReconID not found.  Add workgroup the HashSet and add to Dictionary
              workgroupHashSet = new HashSet<String>();
              workgroupHashSet.Add(deptId);
              workgroupDict.Add(reconId, workgroupHashSet);
            }
          }

          if (depfilenbr != 0)
          {
            // Create the full filename with the sequence number
            string filename = depfilenbr.ToString() + depfileseq.ToString("D3");

            // HashSet to file unique filenames
            HashSet<String> filenameHashSet = null;

            // Query the Filename Dictionary
            if (fileDict.TryGetValue(reconId, out filenameHashSet))
            {
              // The Filename Dictionary has the recon_id
              // Add the new filename
              filenameHashSet.Add(filename);         // Add with no duplicates?
              fileDict[reconId] = filenameHashSet;  // Do I really need to do this???
            }
            else
            {
              // ReconID not found.  Use an empty HashSet
              filenameHashSet = new HashSet<String>();
              filenameHashSet.Add(filename);
              fileDict.Add(reconId, filenameHashSet);
            }
          }
        }
      }
      finally
      {
        if (dataReader != null)
          dataReader.Close();
      }
    }


    public List<BankDeposit> getBankDepositList(out ReconStatus status)
    {
      status = new ReconStatus(true);
      // Bug 11129: Fixed sorting order so that DEPOSIT_DATE is DESC
      // Bug 11099:  Simplified and optimized this SQL
      // Bug 27450: Do not parameterize TOP
      String bankSql =
          "SELECT TOP(" + getDBLimit() + ") * FROM TG_BANK_DEPOSIT_DATA WHERE STATUS != 'r' {0} ORDER BY DEPOSIT_DATE DESC";  // Bug 11129

      return getBankDepositListInternal(bankSql, null, ref status);
    }

    /// <summary>
    /// Retrieve list of bank deposits that match a single bank account.
    /// In the future this will have to be modified to take a list of 
    /// bank accounts.
    /// </summary>
    /// <param name="bankAccounts">A single bank account</param>
    /// <param name="status"></param>
    /// <returns></returns>
    public List<BankDeposit> getBankDepositList(List<String> bankAccounts, out ReconStatus status)
    {
      status = new ReconStatus(true);
      // Bug 11129: Fixed sorting order so that DEPOSIT_DATE is DESC
      // Bug 11099: Took out subqueries to speed this up
      // Bug 27450: Do not parameterize TOP
      String bankSql =
        "SELECT TOP(" + getDBLimit() + ") * FROM TG_BANK_DEPOSIT_DATA WHERE STATUS != 'r' AND @account = ACCOUNT_ID {0} ORDER BY DEPOSIT_DATE DESC";  // Bug 11129

      return getBankDepositListInternal(bankSql, bankAccounts, ref status);
    }


    private SqlParameter param(String name, SqlDbType type, object value)
    {
      SqlParameter param = new SqlParameter(name, type);
      param.Value = value;
      return param;
    }

    private List<BankDeposit> getBankDepositListInternal(String bankSql, List<String> bankAccounts, ref ReconStatus status)
    {
      var bankDepositList = new List<BankDeposit>();

      SqlDataReader dataReader = null;

      // Bug 11493: Added date predicate.  TODO: move to paramater to speed query
      DateTime startDate, endDate;
      String datePredicate = "";
      bool startDateSpecified = getBankStartDate(out startDate);
      if (startDateSpecified)
      {
        endDate = getBankEndDate(startDate);
        datePredicate = String.Format("AND DEPOSIT_DATE >= '{0}' AND DEPOSIT_DATE <= '{1}'", startDate.ToShortDateString(), endDate.ToShortDateString());
      }
      bankSql = String.Format(bankSql, datePredicate);
      // End Bug 11493 

      try
      {
        if (bankAccounts == null)
        {
          dataReader = setupReader(bankSql, ref status);  // Bug 27450: Do not parameterize TOP
        }
        else
        {
          if (bankAccounts[0] == m_unsetAccountNbr)
            bankAccounts[0] = "";
          dataReader = setupReader(bankSql, ref status,
            param("account", SqlDbType.VarChar, bankAccounts[0])    // Bug 27450: Do not parameterize TOP
          );
        }

        if (!status.success)
          return bankDepositList;

        int idOrd = dataReader.GetOrdinal("BANK_DEPOSIT_ID");
        int amountOrd = dataReader.GetOrdinal("AMOUNT");
        int statusOrd = dataReader.GetOrdinal("STATUS");
        int bankRefOrd = dataReader.GetOrdinal("BANK_REFERENCE");
        int textOrd = dataReader.GetOrdinal("TEXT");
        int checksumOrd = dataReader.GetOrdinal("CHECKSUM");

        while (dataReader.Read())
        {
          BankDepositDetails bankDeposit = new BankDepositDetails();
          bankDeposit.databaseID = new DatabaseID(dataReader.GetInt32(idOrd));
          bankDeposit.depDate = dataReader.GetDateTime(dataReader.GetOrdinal("DEPOSIT_DATE"));
          bankDeposit.refNum = dataReader.GetString(dataReader.GetOrdinal("REF_NUM"));
          double amount = dataReader.IsDBNull(amountOrd) ? 0.00 : dataReader.GetDouble(amountOrd);
          // Bug #12055 Mike O - Use misc.Parse to log errors
          bankDeposit.amount = misc.Parse<Decimal>(amount);
          bankDeposit.account = dataReader.GetString(dataReader.GetOrdinal("ACCOUNT_ID"));
          bankDeposit.age = calculateAge(bankDeposit.depDate);
          bankDeposit.depositStatus = BankDeposit.convertStatus(dataReader.GetString(statusOrd));

          // Get the two extra fields needed for validation
          bankDeposit.bankReference = dataReader.IsDBNull(bankRefOrd) ? "" : dataReader.GetString(bankRefOrd);
          bankDeposit.text = dataReader.IsDBNull(textOrd) ? "" : dataReader.GetString(textOrd);


          bankDeposit.checksum = dataReader.IsDBNull(checksumOrd) ? "" : dataReader.GetString(checksumOrd);

          if (!bankDeposit.validateChecksum())
          {
            // Bug 12303: FT: Do not error out.  Log checksum error to activity log and continue
            status.setChecksumError("TG_BANK_DEPOSIT_DATA: Checksum failure: " + bankDeposit.ToString(),
              "REC-187");
            string recordInfo = string.Format("ID:{0}", bankDeposit.databaseID.databaseKey);
            TranSuiteServices.TranSuiteServices.LogChecksumError("TG_BANK_DEPOSIT_DATA", "GetBankDepositListInternals", recordInfo, bankDeposit.ToAuditString());
          }



          bankDepositList.Add((BankDeposit)bankDeposit);
        }

      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in getBankDepositListInternal", e);

        // Bug 10965
        status.setErr("Bank deposit list failed: Check CS log for details",
            String.Format("getBankDepositList failed: SQL or conversion error: Error: {0}: Traceback: {1}",
                e, e.StackTrace),
            "REC-111", false);
      }

      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
      }
      return bankDepositList;
    }


    /// <summary>
    /// Open a SqlDataReader for the client given
    /// an SQL string and a list of params.
    /// The CommandBehavior.CloseConnection is used to open the reader so that the connection
    /// is closed when the reader is closed.
    /// </summary>
    /// <param name="dataReaderSQL"></param>
    /// <param name="status"></param>
    /// <param name="parameters">Variable number of SqlParameter's</param>
    /// <returns></returns>
    private SqlDataReader setupReader(String dataReaderSQL, ref ReconStatus status, params object[] parameters)
    {
      return setupReader(dataReaderSQL, false, ref status, parameters);
    }

    private SqlDataReader setupReader(String dataReaderSQL, bool configDb, ref ReconStatus status, params object[] parameters)
    {
      status.success = true;

      // Should we be worried that this connection will be closed when it goes out of scope?
      SqlConnection dataReaderConnection = new SqlConnection();
      SqlDataReader dataReader = null;

      // Open a connection for DataReader, either to the config database or the transaction db.
      String conString;
      if (configDb)
        conString = (string)m_configConnection.get("db_connection_string", "");  // Bug 9640: Thurber
      else
        conString = (String)m_dbConnection.get("db_connection_string");


      dataReaderConnection.ConnectionString = conString;
      dataReaderConnection.Open();

      // Create the command
      SqlCommand dataReaderCommand = new SqlCommand(dataReaderSQL, dataReaderConnection);

      // Bug 11108: Get the timeout from Web.config.  This is typically set to 0
      object timeout = c_CASL.GEO.get("sql_query_timeout", "360");      // Default to 6 minutes if not set
      int commandTimeout = System.Convert.ToInt32(timeout);
      dataReaderCommand.CommandTimeout = commandTimeout;
      // End Bug 11108

      // Set the parameters

      foreach (object paramObj in parameters)
      {
        SqlParameter pm = (SqlParameter)paramObj;
        dataReaderCommand.Parameters.Add(pm);
      }

      // Note the CommandBehavior.CloseConnection; 
      // this will allow us to close the connection by closing the reader
      dataReader = dataReaderCommand.ExecuteReader(CommandBehavior.CloseConnection);
      return dataReader;
    }

    // Bug 9808: FT: Added checksums to bank rec
    private void updateBankChecksum(DatabaseID id, BankDeposit.bStatus bankStatus, out ReconStatus status)
    {
      status = new ReconStatus(true);

      ReconStatus updateStatus;
      BankDepositDetails details = getBankDepositDetails(id, out updateStatus);
      if (!updateStatus.success)
      {
        status = updateStatus;
        return;
      }
      details.depositStatus = bankStatus;
      details.computeChecksum();
      updateBankStatusFlag(details, out updateStatus);
      if (!updateStatus.success)
      {
        status = updateStatus;
        return;
      }
      return;
    }

    // Bug 9808: FT: Added checksums to bank rec
    private List<int> getVoidedBankDeposit(DatabaseID reconID, out ReconStatus status)
    {
      status = new ReconStatus(true);

      String bankSql = @"SELECT BANK_DEPOSIT_ID FROM TG_BANK_DEPOSIT_DATA 
                            WHERE BANK_DEPOSIT_ID IN 
                            (SELECT BANK_DEPOSIT_ID FROM TG_RECONCILED_BANK_DATA WHERE RECON_ID = @reconID)";

      var bankIds = new List<int>();

      SqlDataReader dataReader = null;
      try
      {
        // Bug #12055 Mike O - Use misc.Parse to log errors
        dataReader = setupReader(bankSql, ref status,
          param("reconID", SqlDbType.Int, misc.Parse<Int32>(reconID.databaseKey)));

        while (dataReader.Read())
        {
          int bankDepositID = dataReader.GetInt32(0);
          bankIds.Add(bankDepositID);
        }
        return bankIds;

      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in getVoidedBankDeposit", e);

        status.setErr("Cannot read the bank deposit IDs",
            String.Format("getVoidedBankDeposit failed: Error: {0}: Traceback: {1}", e, e.StackTrace),
            "REC-192", false);
        return bankIds;
      }
      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
      }
    }

    // Bug 9808: FT: Added checksums to bank rec
    private BankDepositDetails getBankDepositDetailsFromReconID(DatabaseID reconId, out ReconStatus status)
    {
      status = new ReconStatus(true);

      var details = new BankDepositDetails();

      String bankSql =
          @"SELECT * FROM TG_BANK_DEPOSIT_DATA 
                            WHERE BANK_DEPOSIT_ID IN 
                            (SELECT BANK_DEPOSIT_ID FROM TG_RECONCILED_BANK_DATA WHERE RECON_ID = @reconID)";



      SqlDataReader dataReader = null;
      try
      {
        // Bug #12055 Mike O - Use misc.Parse to log errors
        dataReader = setupReader(bankSql, ref status,
          param("reconID", SqlDbType.Int, misc.Parse<Int32>(reconId.databaseKey))
        );

        if (!status.success)
        {
          return details;
        }


        if (!dataReader.HasRows)
        {
          // No bank deposits in this void; must have been an Ignored recon
          return null;
        }
        dataReader.Read();
        int id = dataReader.GetInt32(dataReader.GetOrdinal("BANK_DEPOSIT_ID"));
        details.databaseID = new DatabaseID(id);
        details.account = dataReader.GetString(dataReader.GetOrdinal("ACCOUNT_ID"));
        details.refNum = dataReader.GetString(dataReader.GetOrdinal("REF_NUM"));
        details.depDate = dataReader.GetDateTime(dataReader.GetOrdinal("DEPOSIT_DATE"));
        details.age = calculateAge(details.depDate);
        details.depositStatus = BankDeposit.convertStatus(dataReader.GetString(dataReader.GetOrdinal("STATUS")));
        // Bug #12055 Mike O - Use misc.Parse to log errors
        details.amount = misc.Parse<Decimal>(dataReader.GetDouble(dataReader.GetOrdinal("AMOUNT")));
        int textOrd = dataReader.GetOrdinal("TEXT");
        details.text = dataReader.IsDBNull(textOrd) ? "" : dataReader.GetString(textOrd);
        int bankRefOrd = dataReader.GetOrdinal("BANK_REFERENCE");
        details.bankReference = dataReader.IsDBNull(bankRefOrd) ? "" : dataReader.GetString(bankRefOrd);
        int checksumOrd = dataReader.GetOrdinal("CHECKSUM");
        details.checksum = dataReader.IsDBNull(checksumOrd) ? "" : dataReader.GetString(checksumOrd);

        if (!details.validateChecksum())
        {
          // Bug 12303: FT: Do not error out.  Log checksum error to activity log and continue.
          status.setChecksumError("TG_RECONCILED_DATA: Checksum failure: " + details.ToString(),
            "REC-189");
          string recordInfo = string.Format("ID:{0}", details.databaseID.databaseKey);
          TranSuiteServices.TranSuiteServices.LogChecksumError("TG_BANK_DEPOSIT_DATA", "GetBankDepositDetailsFromReconID", recordInfo, details.ToAuditString());
        }

      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in getBankDepositDetailsFromReconID", e);

        status.setErr("Cannot read bank deposit details fron reconciliation ID and convert data ",
            String.Format("getBankDepositDetailsFromReconID failed: SQL or conversion error: Error: {0}: Traceback: {1}",
                e, e.StackTrace),
            "REC-128", false);
      }
      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
      }

      return details;
    }

    /// <summary>
    /// Loop through all the new (colored green) bank deposits
    /// and change them to white.  This happens after an automatch.
    /// If a new ('n') record has been automatched, 
    /// then the status will be changed to 
    /// 'r', otherwise it will stay at 'n'.  This will loop through all the
    /// 'n' records and change them to 'u' (white).
    /// Bug 9808: FT: Reworked to reset checksum
    /// </summary>
    /// <param name="status"></param>
    private void clearBankStatusNewFlags(ref ReconStatus status)
    {
      status = new ReconStatus(true);


      // Create a query to fetch all the new bank deposits
      String bankSql =
          String.Format("SELECT bank_deposit_id FROM TG_BANK_DEPOSIT_DATA WHERE STATUS = '{0}'",
          BankDeposit.convertStatus(BankDeposit.bStatus.newDeposit));


      SqlDataReader dataReader = null;
      try
      {
        dataReader = setupReader(bankSql, ref status);

        // TODO: do I need more info here?
        if (!status.success)
        {
          return;
        }


        if (!dataReader.HasRows)
        {
          return;
        }

        while (dataReader.Read())
        {
          ReconStatus updateStatus;
          int id = dataReader.GetInt32(dataReader.GetOrdinal("BANK_DEPOSIT_ID"));
          updateBankChecksum(new DatabaseID(id), BankDeposit.bStatus.unreconciled, out updateStatus);
          if (!updateStatus.success)
          {
            status = updateStatus;
            return;
          }
        }

      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in clearBankStatusNewFlags", e);

        status.setErr("Cannot read bank deposit details and convert data",
            String.Format("getBankDepositDetails failed: SQL or conversion error: Error: {0}: Traceback: {1}",
                e, e.StackTrace),
            "REC-128", false);
      }
      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
      }
    }

    /// <summary>
    /// Typically used to change the status field to change the color of the 
    /// entry from green ('n') to white ('u').
    /// Bug 9808: FT: Re-worked to also update checksum
    /// </summary>
    /// <param name="details"></param>
    /// <param name="status"></param>
    private void updateBankStatusFlag(BankDepositDetails details, out ReconStatus status)
    {
      status = new ReconStatus(true);

      String updateBankDepositSql = @"UPDATE TG_BANK_DEPOSIT_DATA 
                                SET STATUS = @status,
                                CHECKSUM = @checksum 
                              WHERE BANK_DEPOSIT_ID = @id";


      // Cache the connections here so that finally can close
      SqlConnection updateConnection = new SqlConnection();
      try
      {
        // Open a connection for the update
        updateConnection.ConnectionString = (String)m_dbConnection.get("db_connection_string");
        updateConnection.Open();

        SqlCommand updateBankCommand = new SqlCommand(updateBankDepositSql, updateConnection);
        updateBankCommand.Parameters.Add(new SqlParameter("@status", SqlDbType.VarChar));
        updateBankCommand.Parameters.Add(new SqlParameter("@checksum", SqlDbType.VarChar));
        updateBankCommand.Parameters.Add(new SqlParameter("@id", SqlDbType.Int));
        updateBankCommand.Parameters[0].Value = BankDeposit.convertStatus(details.depositStatus);
        updateBankCommand.Parameters[1].Value = details.checksum;
        // Bug #12055 Mike O - Use misc.Parse to log errors
        updateBankCommand.Parameters[2].Value = misc.Parse<Int32>(details.databaseID.databaseKey);

        updateBankCommand.ExecuteNonQuery();
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in updateBankStatusFlag", e);

        status.setErr("Cannot update bank status flag and checksum",
          "Cannot update bank status flag and checksum for id: " + details.databaseID.databaseKey +
          ": " + e.ToString(),
          "REC-190", false);
      }
      finally
      {
        if (updateConnection != null)
          updateConnection.Close();
      }
    }

    // Bug 9808: FT: Allow us to validate checksums from CASL IDE
    public void validateChecksums(out ReconStatus status)
    {
      status = new ReconStatus(true);
      int errorCount = 0;
      int recordCount = 0;
      String bankSql = "SELECT BANK_DEPOSIT_ID FROM TG_BANK_DEPOSIT_DATA";
      String reconSql = "SELECT RECON_ID FROM TG_RECONCILED_DATA";

      StringBuilder checksumFailure = new StringBuilder();

      SqlDataReader bankDataReader = null;
      SqlDataReader reconDataReader = null;
      try
      {
        bankDataReader = setupReader(bankSql, ref status);
        while (bankDataReader.Read())
        {
          int bankDepositId = bankDataReader.GetInt32(0);
          ReconStatus checksumStatus;
          BankDepositDetails details = getBankDepositDetails(new DatabaseID(bankDepositId), out checksumStatus);
          if (checksumStatus.checksumError)
          {
            errorCount++;
            checksumFailure.Append(checksumStatus.warning).Append("\n");
          }
          recordCount++;
        }

        reconDataReader = setupReader(reconSql, ref status);
        while (reconDataReader.Read())
        {
          int reconID = reconDataReader.GetInt32(0);
          ReconStatus checksumStatus;
          ReconciledDepositDetails details = getReconciledDeposit(new DatabaseID(reconID), out checksumStatus);
          if (checksumStatus.checksumError)
          {
            errorCount++;
            checksumFailure.Append(checksumStatus.warning).Append("\n");
          }
          recordCount++;
        }

      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in validateChecksums", e);

        errorCount++;
        checksumFailure.Append("validateChecksums: failed: Error: ").Append(e.ToString());
      }
      finally
      {
        if (bankDataReader != null)
        {
          bankDataReader.Close();
        }
        if (reconDataReader != null)
        {
          reconDataReader.Close();
        }
      }
      status.success = errorCount == 0;
      if (status.success)
        status.comments = String.Format("Scanned {0} records.  No checksum errors detected", recordCount);
      status.detailedError = checksumFailure.ToString();
    }

    // Bug 9808: FT: Added checksums to bank rec
    private void updateBankDepositChecksum(BankDepositDetails details, out ReconStatus status)
    {
      status = new ReconStatus(true);

      String conString = (String)m_dbConnection.get("db_connection_string");

      String bankSql =
         @"UPDATE TG_BANK_DEPOSIT_DATA
                  SET CHECKSUM = @checksum
              WHERE bank_deposit_id = @bank_deposit_id";

      // Cache the connections here so that finally can close
      SqlConnection updateConnection = new SqlConnection();
      try
      {
        // Open a connection for the update
        updateConnection.ConnectionString = conString;
        updateConnection.Open();
        SqlCommand updateCommand = new SqlCommand(bankSql, updateConnection);

        // Prepare the statement once and only once
        updateCommand.Prepare();

        // Create and setup parameters once and only once
        // but do not set their values here
        updateCommand.Parameters.Add(new SqlParameter("@checksum", SqlDbType.VarChar));
        updateCommand.Parameters.Add(new SqlParameter("@bank_deposit_id", SqlDbType.Int));

        updateCommand.Parameters[0].Value = details.checksum;
        // Bug #12055 Mike O - Use misc.Parse to log errors
        updateCommand.Parameters[1].Value = misc.Parse<Int32>(details.databaseID.databaseKey);

        updateCommand.ExecuteNonQuery();
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in updateBankDepositChecksum", e);

        status.setErr("Error updating bank deposit checksum",
          "Error updating bank deposit checksum for bank_deposit_id: " + details.databaseID.databaseKey +
          "error: " + e.ToString(),
           "REC-195", false);
      }
      finally
      {
        if (updateConnection != null)
          updateConnection.Close();
      }
    }

    /*
    // Bug 9808: FT: Added checksums to bank rec
    private void updateReconciledChecksum(ReconciledDeposit reconDetails, out ReconStatus status)
    {
      status = new ReconStatus(true);

      String undoReconSql = @"UPDATE TG_RECONCILED_DATA 
                                SET CHECKSUM = @checksum 
                                WHERE RECON_ID = @reconID";

      // Cache the connections here so that finally can close
      SqlConnection updateConnection = new SqlConnection();
      try
      {
        DatabaseID reconId = reconDetails.databaseID;

        // Open a connection for the update
        updateConnection.ConnectionString = (String)m_dbConnection.get("db_connection_string");
        updateConnection.Open();
        SqlCommand updateReconCommand = new SqlCommand(undoReconSql, updateConnection);
        updateReconCommand.Parameters.Add(new SqlParameter("@checksum", SqlDbType.VarChar));
        updateReconCommand.Parameters.Add(new SqlParameter("@reconID", SqlDbType.Int));
        updateReconCommand.Parameters[0].Value = reconDetails.checksum;
        // Bug #12055 Mike O - Use misc.Parse to log errors
        updateReconCommand.Parameters[1].Value = misc.Parse<Int32>(reconId.databaseKey);

        updateReconCommand.ExecuteNonQuery();
      }
      catch (Exception e)
      {
        status.setErr("Cannot update checksum in reconciliation table", "Reconciliation checksum update failed: " + e.ToString(),
          "REC-194", false);
      }
      finally
      {
        if (updateConnection != null)
          updateConnection.Close();
      }
    }

    private int updateAllReconciledChecksums()
    {
      ReconStatus status;
      status = new ReconStatus(true);

      int recordCount = 0;
      int logCounter = 0;

      String readReconSql = "SELECT RECON_ID FROM TG_RECONCILED_DATA";

      String updateReconChecksum = @"UPDATE TG_RECONCILED_DATA 
                                SET CHECKSUM = @checksum 
                                WHERE RECON_ID = @reconID";

      SqlDataReader reconDataReader = null;

      // Cache the connections here so that finally can close
      SqlConnection updateConnection = new SqlConnection();
      try
      {
        updateConnection.ConnectionString = (String)m_dbConnection.get("db_connection_string");
        updateConnection.Open();
        // Open a connection for the update
        SqlCommand updateReconCommand = new SqlCommand(updateReconChecksum, updateConnection);
        updateReconCommand.Parameters.Add(new SqlParameter("@checksum", SqlDbType.VarChar));
        updateReconCommand.Parameters.Add(new SqlParameter("@reconID", SqlDbType.Int));

        reconDataReader = setupReader(readReconSql, ref status);
        while (reconDataReader.Read())
        {
          int reconID = reconDataReader.GetInt32(0);
          ReconStatus checksumStatus;
          ReconciledDepositDetails details = getReconciledDeposit(new DatabaseID(reconID), out checksumStatus);
          if (!checksumStatus.success)
          {
            Logger.Log($"Error getting reconciled record: {checksumStatus.detailedError}", "BankRec", "err");
          }
          recordCount++;
          details.computeChecksum();  // reset here

          DatabaseID reconId = details.databaseID;
          // Set the data
          updateReconCommand.Parameters[0].Value = details.checksum;
          updateReconCommand.Parameters[1].Value = Convert.ToInt32(reconId.databaseKey);
          // Write out the checksum
          updateReconCommand.ExecuteNonQuery();
          logCounter++;
          if (logCounter > 1000)
          {
            logCounter = 0;
            Logger.Log($"Working TG_RECONCILED_DATA checksum: record: {recordCount}", "ReconcileDeposits", "err");
          }

        }
        return recordCount;
      }
      catch (Exception e)
      {
        status.setErr("Cannot update checksum in reconciliation table", "Reconciliation checksum update failed: " + e.ToString(),
          "REC-194", false);
        return recordCount;
      }
      finally
      {
        if (reconDataReader != null)
          reconDataReader.Close();
        if (updateConnection != null)
          updateConnection.Close();
      }
    }

    private void updateAllBankChecksums()
    {
      //BankDepositDetails reconDetails;
      ReconStatus status = new ReconStatus(true);

      int recordCount = 0;
      int logCounter = 0;

      String readBankSql = "SELECT BANK_DEPOSIT_ID FROM TG_BANK_DEPOSIT_DATA";

      String updateBankSql =
         @"UPDATE TG_BANK_DEPOSIT_DATA
                  SET CHECKSUM = @checksum
              WHERE bank_deposit_id = @bank_deposit_id";

      SqlDataReader bankDataReader = null;

      // Cache the connections here so that finally can close
      SqlConnection updateConnection = new SqlConnection();
      try
      {
        Logger.Log("updateAllBankChecksums: Starting checksum update of TG_BANK_DEPOSIT_DATA", "BankRec", "info");
        updateConnection.ConnectionString = (String)m_dbConnection.get("db_connection_string");
        updateConnection.Open();
        // Open a connection for the update
        SqlCommand updateBankCommand = new SqlCommand(updateBankSql, updateConnection);
        updateBankCommand.Parameters.Add(new SqlParameter("@checksum", SqlDbType.VarChar));
        updateBankCommand.Parameters.Add(new SqlParameter("@bank_deposit_id", SqlDbType.Int));

        bankDataReader = setupReader(readBankSql, ref status);
        while (bankDataReader.Read())
        {
          int bankDepositId = bankDataReader.GetInt32(0);
          ReconStatus checksumStatus;
          // TODO: Too slow!!!  no need to re-read record
          BankDepositDetails details = getBankDepositDetails(new DatabaseID(bankDepositId), out checksumStatus);
          if (!checksumStatus.success)
          {
            Logger.Log($"Update of TG_BANK_DEPOSIT_DATA failed: {checksumStatus.detailedError}", "BankRec", "err");
          }
          recordCount++;
          details.computeChecksum();  // reset here

          DatabaseID databaseId = details.databaseID;
          // Set the data
          updateBankCommand.Parameters[0].Value = details.checksum;
          updateBankCommand.Parameters[1].Value = Convert.ToInt32(databaseId.databaseKey);
          // Write out the checksum
          updateBankCommand.ExecuteNonQuery();
          logCounter++;
          if (logCounter > 1000)
          {
            logCounter = 0;
            Logger.Log($"Working TG_BANK_DEPOSIT_DATA checksum: record: {recordCount}", "BankRec", "info");
          }

        }
        Logger.Log("Finished checksum update of TG_BANK_DEPOSIT_DATA", "BankRec", "info");
      }
      catch (Exception e)
      {
        status.setErr("Cannot update checksum in bank deposit table", "Bank deposit checksum update failed: " + e.ToString(),
          "REC-194", false);
        Logger.Log($"Bank Deposit Checksume failed: {e.ToString()}", "BankRec", "err");
      }
      finally
      {
        if (bankDataReader != null)
          bankDataReader.Close();
        if (updateConnection != null)
          updateConnection.Close();
      }
    }


    // Allow us to fix checksums from CASL; take this out of production code
    
    public void updateAllChecksums(out ReconStatus status)
    {
      status = new ReconStatus(true);
      int errorCount = 0;
      int recordCount = 0;
      int logCounter = 0;

      Logger.Log("Starting checksum update of Bank Rec tables", "BankRec", "info");

      String bankSql = "SELECT BANK_DEPOSIT_ID FROM TG_BANK_DEPOSIT_DATA";
      //String reconSql = "SELECT RECON_ID FROM TG_RECONCILED_DATA";

      StringBuilder checksumFailure = new StringBuilder();

      SqlDataReader bankDataReader = null;
      SqlDataReader reconDataReader = null;
      try
      {
        bankDataReader = setupReader(bankSql, ref status);
        while (bankDataReader.Read())
        {
          int bankDepositId = bankDataReader.GetInt32(0);
          ReconStatus checksumStatus;
          BankDepositDetails details = getBankDepositDetails(new DatabaseID(bankDepositId), out checksumStatus);
          if (!checksumStatus.success)
          {
            errorCount++;
            checksumFailure.Append(checksumStatus.detailedError).Append("\n");
          }
          details.computeChecksum();

          updateBankDepositChecksum(details, out status);
          if (!status.success)
          {
            errorCount++;
            checksumFailure.Append("Unable to update bank deposit checksum: ")
              .Append(status.detailedError).Append("\n");
          }

          recordCount++;

          logCounter++;
          if (logCounter > 10000)
          {
            logCounter = 0;
            Logger.Log($"Working TG_BANK_DEPOSIT_DATA checksum: record: {recordCount}", "BankRec", "info");
          }
        }

        Logger.Log("Starting checksum update of TG_RECONCILED_DATA", "BankRec", "info");
        recordCount = recordCount + updateAllReconciledChecksums();
        Logger.Log($"Finished Bank Rec checksum update: this many records total: {recordCount}", "BankRec", "info");
      }
      catch (Exception e)
      {
        errorCount++;
        checksumFailure.Append("updateAllChecksums: failed: Error: ").Append(e.ToString());
      }
      finally
      {
        if (bankDataReader != null)
        {
          bankDataReader.Close();
        }
        if (reconDataReader != null)
        {
          reconDataReader.Close();
        }
      }
      status.success = errorCount == 0;
      if (status.success)
        status.comments = String.Format("Updated the checksums for {0} records.  No checksum errors detected", recordCount);
      status.detailedError = checksumFailure.ToString();
    }
    
    */

    /// <summary>
    /// The expected start date is the cutoff date for reconciliation candidates 
    /// so that old records do not flood the GUI.
    /// 
    /// Bug 9934: FT: Query the CASL config property for the reconciliation_cutoff.
    /// BUG 10544: FT: Changed this to a hard date (cutoff_date)
    /// </summary>
    /// <returns></returns>
    private DateTime getExpectedStartDate()
    {
      // Pull the cutoff date directly from the CASL config variable.
      // If the date has not been set, use a suitable default.
      string strCutoffDate = "";
      try
      {
        // Bug 11515: read directly from DB
        //strCutoffDate = ((string)((GenericObject)c_CASL.c_object("Business.bank_deposit_recon.data")).get("cutoff_date"));
        strCutoffDate = getConfigValue("cutoff_date", "");
        if (strCutoffDate == null || strCutoffDate == "")
        {
          // Bug 12107: FT: change the 2 week-before-earliest-bank_deposit to be all expected deposits
          //return getDefaultExpectedStartDate();
          return SqlDateTime.MinValue.Value;
        }
        DateTime dtCutoffDate = DateTime.Parse(strCutoffDate);
        return dtCutoffDate;
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in getExpectedStartDate", e);

        Logger.cs_log(String.Format("Error: The Reconciliation Cutoff Date configuration value: {0} is not in a supported format", strCutoffDate));
        // In the unlikely event that this fails, use a default
        // Bug 12107: FT: change the 2 week-before-earliest-bank_deposit to be all expected deposits
        //return getDefaultExpectedStartDate();  
        return SqlDateTime.MinValue.Value;
      }

    }


    /// <summary>
    /// Retrieve the value specified in the "Expected Deposit Range" and parse it.
    /// Then add it to the cutoff date and return to getExpectedDepositList so that
    /// it can be used to constrain the amount of records returned.
    /// </summary>
    /// <param name="cutoffDate">This can be an integer for the number of days or a number followed by an "h" to specify hours</param>
    /// <returns></returns>
    private DateTime getExpectedEndDate(DateTime cutoffDate)
    {
      String originalRangeString = "";
      try
      {
        // Bug 11515
        //originalRangeString = ((String)((GenericObject)c_CASL.c_object("Business.bank_deposit_recon.data")).get("expected_range"));
        originalRangeString = getConfigValue("expected_range", "");
        if (originalRangeString == null || originalRangeString == "" || originalRangeString == "0")
          return DateTime.Now;

        if (originalRangeString.EndsWith("h"))
        {
          String range = originalRangeString.Substring(0, originalRangeString.Length - 1);
          int rangeInt = Convert.ToInt32(range);
          TimeSpan timeSpanHours = new TimeSpan(0, rangeInt, 0, 0);
          return cutoffDate + timeSpanHours;
        }

        int dayRangeInt = Convert.ToInt32(originalRangeString);
        TimeSpan timeSpanDays = new TimeSpan(dayRangeInt, 0, 0, 0);
        return cutoffDate + timeSpanDays;
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in getExpectedEndDate", e);
        Logger.cs_log(String.Format("Error: The Expected Deposit Range field: {0} is not in a supported format", originalRangeString));
        return DateTime.Now;
      }
    }

    /// <summary>
    /// Return a new date representing the the end date range for bank deposits.
    /// Bug 11493 
    /// </summary>
    /// <param name="startDate"></param>
    /// <returns></returns>
    private DateTime getBankEndDate(DateTime startDate)
    {
      String originalRangeString = "";
      try
      {
        // Bug 11515
        //originalRangeString = ((String)((GenericObject)c_CASL.c_object("Business.bank_deposit_recon.data")).get("bank_range"));
        originalRangeString = getConfigValue("bank_range", "");
        if (originalRangeString == null || originalRangeString == "" || originalRangeString == "0")
          return DateTime.Now;

        if (originalRangeString.EndsWith("h"))
        {
          String range = originalRangeString.Substring(0, originalRangeString.Length - 1);
          int rangeInt = Convert.ToInt32(range);
          TimeSpan timeSpanHours = new TimeSpan(0, rangeInt, 0, 0);
          return startDate + timeSpanHours;
        }

        int dayRangeInt = Convert.ToInt32(originalRangeString);
        TimeSpan timeSpanDays = new TimeSpan(dayRangeInt, 0, 0, 0);
        return startDate + timeSpanDays;
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in getBankEndDate", e);

        Logger.cs_log(String.Format("Error: The Bank Deposit Range field: {0} is not in a supported format", originalRangeString));
        return DateTime.Now;
      }
    }

    /// <summary>
    /// Pull the bank start date directly from the CASL config variable.
    /// If the date has not been set, return false.
    /// Bug 11493
    /// </summary>
    /// <param name="bankStartDateValue"></param>
    /// <returns></returns>
    private bool getBankStartDate(out DateTime startDate)
    {
      string bankStartDateString = "";
      startDate = DateTime.MinValue;
      try
      {
        // Bug 11515
        //bankStartDateString = ((string)((GenericObject)c_CASL.c_object("Business.bank_deposit_recon.data")).get("bank_start_date"));
        bankStartDateString = getConfigValue("bank_start_date", "");
        if (bankStartDateString == null || bankStartDateString == "")
          return false;
        startDate = DateTime.Parse(bankStartDateString);
        return true;
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error ingetBankStartDate", e);

        Logger.cs_log(String.Format("Error: The Bank Cutoff Date configuration value: {0} is not in a supported format", bankStartDateString));
        // In the unlikely event that this fails, use a default
        return false;
      }

    }






    /// <summary>
    /// Pull the database_select_limit so it can be read dynamically.
    /// Bug 11064.
    /// </summary>
    /// <returns></returns>
    private int getDBLimit()
    {
      int limit;
      // Get the max amount of expected, bank, and reconciled records to return
      // to the user
      // Bug 10996: FT: Make the database_select_limit variable work
      try
      {
        // Bug 11515
        //limit = (int)((GenericObject)c_CASL.c_object("Business.bank_deposit_recon.data")).get("database_select_limit");
        string limitString = getConfigValue("database_select_limit", DATABASE_SELECT_LIMIT);
        limit = Convert.ToInt32(limitString);
        // End 11515
      }
      catch (FormatException e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in getDBLimit", e);

        limit = Convert.ToInt32(DATABASE_SELECT_LIMIT);
      }
      return limit;
    }

    /// <summary>
    /// Get a hash of the TG_BANK_DEPOSIT_DATA REFNUM's for each RECON_ID in the
    /// reconciled list.  All this thrashing about is used to display the
    /// slip/ref numbers in the reconciled page.
    /// <remarks>
    /// Previously, 2 SQL calls were made for each and every reconciled record!
    /// This led to 2-3 minute wait every time the reconciled window was openned.
    /// This and the next method only need to be called once for each reconciled page;
    /// it will grab all the recon-->slip number mappings for the window.
    /// Please note the tricky TOP() method here.  The running time and memory
    /// requirements for this were excessive if I did not limit the output.  
    /// This TOP is an attempt to skim off only the relevant records and discard the
    /// ones that will not be shown in the reconciled window.  
    /// Otherwise a reconciled table with millions of records would swamp this call.
    /// </remarks>
    /// Bug 11099.
    /// </summary>
    /// <returns></returns>
    private Dictionary<int, List<String>> getBankReconciledSlipNumbers()
    {
      // Bug 11326: Put the bank rec key in Reconciled window.
      // Had to enhance SQL here pull the TYPE from the TG_RECONCILED_DATA field
      String newSqlString =
        @"SELECT TOP(128) xref.RECON_ID, bank.REF_NUM, recon.TYPE, bank.TEXT 
          FROM TG_BANK_DEPOSIT_DATA bank, TG_RECONCILED_BANK_DATA xref, TG_RECONCILED_DATA recon
          WHERE 
	          bank.BANK_DEPOSIT_ID = xref.BANK_DEPOSIT_ID AND
	          xref.RECON_ID = recon.RECON_ID
          ORDER BY RECON_DATE DESC";

      return getReconciledSlipNumbers(newSqlString, true, false, new GenericObject());
    }


    /// <summary>
    /// Get a hash of the TG_DEPOSIT_DATA DEPSLIPNBR's for each RECON_ID in the
    /// reconciled list.
    /// Bug 11099.
    /// </summary>
    /// <returns>A hashmap of recon IDs and the slips numbers / match keys for each reconciliations</returns>
    private Dictionary<int, List<String>> getExpectedReconciledSlipNumbers(GenericObject deptID2MatchKey)
    {
      // TODO: investigate why this is not filtering out voided reconciliations!?
      // Should the last line be: SELECT TOP({0}) RECON_ID FROM TG_RECONCILED_DATA WHERE STATUS != 'v' ORDER BY RECON_DATE DESC
      // Bug 11549: Enhanced the sql to get the DEPTID so we can use it to get the match key
      String sqlString =
            @"SELECT RECON_ID, DEPSLIPNBR, BANKID, DEPTID
              FROM TG_DEPOSIT_DATA as expected, TG_RECONCILED_EXPECTED_DATA as xref, TG_DEPFILE_DATA as dfile
              WHERE
	                    expected.DEPOSITID = xref.DEPOSITID 
	                AND expected.DEPOSITNBR = xref.DEPOSITNBR 
	                AND expected.UNIQUEID = xref.UNIQUEID 
	                AND expected.DEPFILENBR = dfile.DEPFILENBR
	                AND expected.DEPFILESEQ = dfile.DEPFILESEQ
	                AND xref.RECON_ID IN (
						        SELECT TOP({0}) RECON_ID FROM TG_RECONCILED_DATA ORDER BY RECON_DATE DESC
					        )";
      sqlString = string.Format(sqlString, getDBLimit());     // Bug 27450
      return getReconciledSlipNumbers(sqlString, false, true, deptID2MatchKey);
    }

    /// <summary>
    /// Return a Dictionary with a mapping between the RECON_ID and a list 
    /// of its associated Reference / Slip numbers.  This list will be cached in
    /// getReconciledDepositListInternal().
    /// This is done for performance reasons so that getReconciledDepositListInternal()
    /// did not have to do a seperate SQL call for each recon_id.  This used to take 
    /// 34 seconds for 128 records!  Now it is less than a second.Bug 11099.
    /// 
    /// Bug 11326: Enhanced this to put the bank rec key in the ref num where needed.
    /// </summary>
    /// <param name="sqlString"></param>
    /// <returns>Dictionary of RECON_ID's keys with lists of slip numbers values</returns>
    private Dictionary<int, List<String>> getReconciledSlipNumbers(String sqlString, bool matchKey, bool expectedTable, GenericObject deptID2MatchKey)
    {
      var slipNumbers = new Dictionary<int, List<String>>();

      // Bug 11326
      string recon_regex = null;
      Regex regexPattern = null;
      if (matchKey)  // Bug 11326
      {
        recon_regex = (string)((GenericObject)c_CASL.c_object("Business.bank_deposit_recon.data")).get("recon_regex", "");
        // Setup regex pattern matcher here
        regexPattern = new Regex(recon_regex);
      }
      // End of bug 11326

      SqlDataReader dataReader = null;
      try
      {
        var status = new ReconStatus(true);
        dataReader = setupReader(sqlString, ref status);      // Bug 27450: Do not parameterize TOP

        string recon_text = null, recon_type = null;   // For match key processing

        while (dataReader.Read())
        {
          int reconId = dataReader.GetInt32(0);
          String refNumber = dataReader.IsDBNull(1) ? "" : dataReader.GetString(1);
          if (matchKey)
          {
            recon_type = dataReader.IsDBNull(2) ? "" : dataReader.GetString(2);
            recon_text = dataReader.IsDBNull(3) ? "" : dataReader.GetString(3);
            if (recon_type == "k")
            {
              Match match = regexPattern.Match(recon_text);
              // If match key is found, overwrite the refNumber
              if (match.Success)
                refNumber = match.Groups[1].Value;
            }
          }
          // Try to insert the expected match key
          if (expectedTable)
          {
            // Bug 11549: If ref num is not set, set to Expected (not bank) match key
            if (refNumber == "")
            {
              String bankID = dataReader.IsDBNull(2) ? "" : dataReader.GetString(2);       // "BANKID from tg_deposit_data"
              String deptID = dataReader.IsDBNull(3) ? "" : dataReader.GetString(3);        // "DEPTID from tg_depfile_data"
              string merchantKey = getWorkgroupMatchKeyUPHS(deptID2MatchKey, deptID, bankID);
              refNumber = merchantKey; // Could still be ""
            }
          }

          List<String> slips;
          if (slipNumbers.TryGetValue(reconId, out slips))
          {
            slips.Add(refNumber);
            slipNumbers[reconId] = slips;  // Do I really need to do this???
          }
          else
          {
            var newSlips = new List<String>();
            newSlips.Add(refNumber);
            slipNumbers.Add(reconId, newSlips);
          }
        }
      }
      finally
      {
        if (dataReader != null)
          dataReader.Close();
      }

      return slipNumbers;
    }

    /// <summary>
    /// Optimized version of getRefAndSlipNumbers()
    /// that looks up the slip numbers from hash tables
    /// instead of hitting the database for each one.
    /// Bug 11099
    /// </summary>
    /// <param name="databaseID"></param>
    /// <param name="bankSlips"></param>
    /// <param name="expectedSlips"></param>
    /// <param name="status"></param>
    /// <returns></returns>
    private String getRefAndSlipNumbersFromHashes(DatabaseID databaseID,
      Dictionary<int, List<String>> bankSlips,
      Dictionary<int, List<String>> expectedSlips,
      out ReconStatus status)
    {
      status = new ReconStatus(true);
      var masterList = new List<String>();

      int reconId = Convert.ToInt32(databaseID.databaseKey);

      List<String> slips;

      if (expectedSlips.TryGetValue(reconId, out slips))
      {
        foreach (String slip in slips)
        {
          masterList.Add(slip);
        }
      }

      if (bankSlips.TryGetValue(reconId, out slips))
      {
        foreach (String slip in slips)
        {
          masterList.Add(slip);
        }
      }

      masterList = masterList.Distinct().ToList(); // Bug 11346: removed duplicates from list
      masterList.Sort();   // Make it easy to read

      // Change into a comma-separated list
      String refs = String.Join(",", masterList.ToArray());

      return refs;
    }

    /// <summary>
    /// Determine whether the bank account filter is set to unconfigured.
    /// Bug 12060.
    /// </summary>
    /// <param name="accountNbr"></param>
    /// <returns>True if the account is not configured.</returns>
    public bool isUnconfigured(string accountNbr)
    {
      if (accountNbr == m_unsetAccountNbr)
        return true;

      return false;
    }

    /// <summary>
    /// Show a list of <b>binned</b> bank deposits.  
    /// They are binned by a concatenation of the bank ID (not the bank account)
    /// and the slip (reference) number.
    /// </summary>
    /// <remarks>TODO: Use the passed in reconciled list and not show bank deposits
    /// that have been reconciled.</remarks>
    /// <param name="bankDeposits"></param>
    /// <param name="reconciled"></param>
    public void reportUnreconciledBankDeposits(AutomatchReports reporter, Dictionary<string, AmountBankIDBucket> bankDeposits, List<ReconciledDeposit> reconciled)
    {
      string writeDetailedReportString = getConfigValue("enable_automatch_detailed_report", "false");
      bool writeDetailedReport = Convert.ToBoolean(writeDetailedReportString);
      if (!writeDetailedReport)
        return;   // This is too nitty-gritty for the general report

      reporter.textReport("\nAll Bank Deposits (including matched ones), Binned by Bank Account / slip number:");
      foreach (string bucketKey in bankDeposits.Keys)
      {
        if (!bucketKey.Contains('|'))
          continue;
        AmountBankIDBucket bucket = bankDeposits[bucketKey];
        string[] bucketKeys = bucketKey.Split('|');
        reporter.textReport(
          "Account: '", bucketKeys[0],
          "' Slip: '", bucketKeys[1],
          "' Total Amount: ", String.Format("{0:C}", bucket.amount)
          );
      }
    }

  }


}
