﻿using System;
using System.Collections.Generic;
using System.Text;

namespace recon
{
  public class ExpectedDepositDetails : ExpectedDeposit
  {
    /// <summary>
    /// Tender list
    /// </summary>
    public List<Tender> tenders { get; set; }
    public String comments { get; set; }
    public String createrUserId { get; set; }  // Bug 9608
    public String ownerUserId { get; set; }
    public String groupId { get; set; }
    public int groupNbr { get; set; }
    public String acctUserId { get; set; }
    public String sourceRefId { get; set; }
    public DateTime voidDt { get; set; }
    public String voidUserid { get; set; }
    public String matchKey { get; set; }   // BUG 10250 FT
    public String bankId { get; set; }  // Bug 11302
  }
}
