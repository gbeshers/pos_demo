﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using CASL_engine;

namespace recon
{
  /// <summary>
  /// Custom parser for the HealthShare non-standard CSV files.
  /// These files only have Date, Amount, and Slip number with 
  /// no Account, Record Type or Text fields.  The Account number is
  /// set in configuration and it will be the same for all files from HealthShare.
  /// Bug 17179
  /// </summary>
  class HealthShareParser : Parser
  {
    private int m_lineCounter = 0;
    private int m_fieldCounter = 0;
    private String m_currentField = "";

    /// <summary>
    /// Override the parser for HealthShare.
    /// Note how the BankAccount parameter was added, but only used in this parser so far.
    /// </summary>
    /// <param name="path"></param>
    /// <param name="bankAccount"></param>
    /// <param name="checkLineNumbers"></param>
    /// <param name="status"></param>
    /// <returns></returns>
    public override List<TransactionRecord> parse(String path, string bankAccount, bool checkLineNumbers, out ReconStatus status)
    {
      status = new ReconStatus(true);

      var mappings = new List<KeyValuePair<String, int>>();

      var transactionList = new List<TransactionRecord>();
      System.IO.StreamReader file = null;
      m_lineCounter = 0;
      m_fieldCounter = 0;
      m_currentField = "";
      try
      {
        createColumnMappings(mappings);

        String line;
        file = new System.IO.StreamReader(path);
        while ((line = file.ReadLine()) != null)
        {
          // Skip blank lines
          if (line.Trim().Length == 0)
            continue;
          bool blankLine;
          TransactionRecord transaction = readColumns(mappings, line, bankAccount, out blankLine);
          if (blankLine)
            continue;
          transactionList.Add(transaction);
        }


      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in  parse bank file", e);

        FileInfo fileInfo = new FileInfo(path);

        // Bug 11080
        status.setErr(
            String.Format("Health Share CSV Parser: Cannot parse bank file: {4}: '{0}': Error on line: {1}  field: {2}  field value: '{3}' ",
                fileInfo.Name, m_lineCounter, m_fieldCounter, m_currentField, e.Message),
            String.Format("Health Share CSV Parser: Cannot parse bank file: '{0}' line: {1} field: {2}: field value: '{3}' Exception: {4}",
                fileInfo.FullName, m_lineCounter, m_fieldCounter, m_currentField, e.ToString()),
            "REC-143", true);
      }
      finally
      {
        if (file != null)
          file.Close();
      }
      return transactionList;
    }

    private TransactionRecord readColumns(List<KeyValuePair<string, int>> mappings, String line, string bankAccount, out bool emptyRecord)
    {
      var transaction = new TransactionRecord();
      transaction.account = bankAccount;          // bank account is set at the Configuration level

      bool amountSet = false;
      bool dateSet = false;
      bool depositSlipNumberSet = false;

      String[] columns = line.Split(',');
      foreach (KeyValuePair<String, int> kvp in mappings)
      {
        m_fieldCounter = kvp.Value;
        switch (kvp.Key)
        {
          case "As Of":
            m_currentField = readColumn(columns, kvp.Value);
            if (string.IsNullOrEmpty(m_currentField))
              continue;         // Skip blank lines
            // Bug #12055 Mike O - Use misc.Parse to log errors
            transaction.date = misc.Parse<DateTime>(m_currentField);
            dateSet = true;
            break;

          case "Amount":
            m_currentField = readColumn(columns, kvp.Value);
            if (string.IsNullOrEmpty(m_currentField))
              continue;         // Skip blank lines

            // Bug #12055 Mike O - Use misc.Parse to log errors
            transaction.amount = misc.Parse<Decimal>(m_currentField.Trim('"'));   // Bug 19556: Fixed this along with a similar bug in the BBT parser
            amountSet = true;
            break;

          case "Customer Reference":
            if (string.IsNullOrEmpty(m_currentField))
              continue;         // Skip blank lines
            m_currentField = readColumn(columns, kvp.Value);
            transaction.customerRefNum = m_currentField;
            depositSlipNumberSet = true;
            break;

          default:
            throw new Exception("Health Share CSV parser failure: unexpected column name: " + kvp.Key);
        }
      }

      // Check for blank lines
      if (amountSet == false && dateSet == false && depositSlipNumberSet == false)
      {
        emptyRecord = true;
        return transaction;
      }
      else
        emptyRecord = false;

      if (amountSet == false)
        throw new Exception("Health Share CSV parser failure: the amount value is not set in line: " + line);

      if (dateSet == false)
        throw new Exception("Health Share CSV parser failure: the date value is not set in line: " + line);

      // Allow a blank deposit slip number

      return transaction;
    }

    private String readColumn(String[] columns, int index)
    {
      // Handle truncated lines (missing fields at the end)
      if (index >= columns.Length)
        return "";

      return columns[index];
    }


    /// <summary>
    /// Reworked the column mappings so that they are hardwired for grabbing 
    /// only 3 relevant columns.
    /// The HealthShare CSV files does NOT have the first line as column headings
    /// so they had to be hardwired here.
    /// </summary>
    /// <param name="mappings"></param>
    private void createColumnMappings(List<KeyValuePair<string, int>> mappings)
    {

      KeyValuePair<String, int> kvp;

      kvp = new KeyValuePair<String, int>("As Of", 0);
      mappings.Add(kvp);

      kvp = new KeyValuePair<String, int>("Amount", 1);
      mappings.Add(kvp);

      kvp = new KeyValuePair<String, int>("Customer Reference", 4);
      mappings.Add(kvp);

    }
  }
}
