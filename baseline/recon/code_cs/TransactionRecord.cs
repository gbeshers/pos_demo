﻿using System;
using System.Collections.Generic;
using System.Text;

namespace recon
{
  class TransactionRecord
  {
    public String account { get; set; }
    public DateTime date { get; set; }
    public Decimal amount { get; set; }
    public String bankRefNum { get; set; }
    public String customerRefNum { get; set; }
    public String text { get; set; }
    public int baiCode { get; set; }
  }
}
