﻿using System;
using System.Data;
using System.IO;
using System.Linq;

using System.Data.SqlClient;
using System.Data.SqlTypes;

using CASL_engine;


/*
  ERROR CODES FOR Bank Reconcilation ARE IN THE FORMAT REC-750.... PLEASE ALWAYS USE A SPECIFIC ERROR CODE FOR EACH ERROR.
  ALSO PLEASE UPDATE THIS SECTION WITH THE LARGEST ERROR YOU HAVE USED TO DATE. WHEN THE NEXT PERSON 
  CREATES ONE, THEY WILL BE ABLE TO ASSIGN ERRORS IN SEQUENCE./

  RECCONFIG-100 - "Error reading the Bank Reconciliation configuration database:"
  RECCONFIG-101 - "Warning: getConfigValue(): the '{0}' config variable was not found. Using default of {1}"
  RECCONFIG-102 - "Error reading the Bank Reconciliation configuration database: {0}: {1}"
 */

namespace recon
{
  public partial class ReconcileDeposits
  {
    /// <summary>
    /// Strip the pesky quotes are used by the CBT_Flex config table.
    /// </summary>
    /// <param name="bankID"></param>
    /// <returns></returns>
    private String stripQuotes(String bankID)
    {
      char[] quotes = new char[] { '"' };

      bankID = bankID.TrimStart(quotes);
      bankID = bankID.TrimEnd(quotes);
      return bankID;
    }

    /// <summary>
    /// Add the pesky quote needed by CBT_Flex config table.
    /// </summary>
    /// <param name="bankInfo"></param>
    /// <returns></returns>
    private String addQuotes(String bankInfo)
    {
      if (bankInfo == "")
        return "\"\"";
      if (!bankInfo.StartsWith("\""))
        bankInfo = '"' + bankInfo;
      if (!bankInfo.EndsWith("\""))
        bankInfo = bankInfo + '"';
      return bankInfo;
    }

    /// <summary>
    /// Pulls config value directly from the CBT_flex table
    /// and returns it as a string (possibly stripped).
    /// </summary>
    /// <param name="configKey">Config key</param>
    /// <param name="defaultValue">Default value if key is not found</param>
    /// <returns></returns>
    public string getConfigValue(string configKey, string defaultValue)
    {
      SqlDataReader dataReader = null;

      string flexSql = @"SELECT field_value FROM CBT_Flex 
                            WHERE container = 'Business.bank_deposit_recon.data' 
                            AND field_key = @fieldValue";

      string configValue = null;
      ReconStatus status = new ReconStatus(true);
      try
      {
        dataReader = setupReader(flexSql, true, ref status,
             param("fieldValue", SqlDbType.VarChar, addQuotes(configKey))
          );
        if (!status.success)
          throw new Exception(
            String.Format("Error reading the Bank Reconciliation configuration database: {0}: {1}", status.detailedError, "RECCONFIG-100"));

        if (!dataReader.HasRows)
        {
          Logger.cs_log(string.Format("Warning: getConfigValue(): the '{0}' config variable was not found. Using default of '{1}': Error number: {2}", configKey, defaultValue, "RECCONFIG-101"));
          return defaultValue;
        }

        dataReader.Read();
        configValue = stripQuotes(dataReader.GetString(0));
      }
      catch (Exception e)
      {
        throw new Exception(
          String.Format("Error reading the Bank Reconciliation configuration database: {0}: {1}", e.ToString(), "RECCONFIG-102"));
      }
      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
      }
      return configValue;
    }


    [Obsolete("use getConfigValue(string, default)", true)]
    private object getConfigValue(string configKey, bool stringType)
    {
      SqlDataReader dataReader = null;

      string flexSql = @"SELECT field_value FROM CBT_Flex 
                            WHERE container = 'Business.bank_deposit_recon.data' 
                            AND field_key = @fieldValue";

      object configValue = null;
      ReconStatus status = new ReconStatus(true);
      try
      {
        dataReader = setupReader(flexSql, true, ref status,
             param("fieldValue", SqlDbType.VarChar, addQuotes(configKey))
          );
        if (!status.success)
          throw new Exception("Error reading the Bank Reconciliation configuration database: " + status.detailedError);

        while (dataReader.Read())
        {
          if (stringType)
          {
            string stringValue = dataReader.GetString(0);
            configValue = stripQuotes(stringValue);
          }
          else
          {
            int intValue = dataReader.GetInt32(0);
            configValue = intValue;
          }
        }

      }
      catch (Exception e)
      {
        throw new Exception("Error reading the Bank Reconciliation configuration database: " + e.ToString());
      }
      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
      }
      return configValue;
    }

  }
}
