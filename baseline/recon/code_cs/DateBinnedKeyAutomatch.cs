﻿using System;
using System.Data;
using System.Text;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using CASL_engine;
using System.IO;
using System.Linq;

using System.Data.SqlClient;
using System.Diagnostics;

using System.Text.RegularExpressions;


namespace recon
{
  /// <summary>
  /// Variant of bank rec match key for UPHS that features multiple match keys for 
  /// workgroups.
  /// Bug 11302.
  /// </summary>
  public partial class ReconcileDeposits
  {

    /// <summary>
    /// Bin the Bank Deposits based on the account and match key, but do not use the date.
    /// Return a list of bank deposits binned by account and match key.  The amount will be in each bin 
    /// (along with the date which is just used for reports).
    /// </summary>
    /// <param name="user"></param>
    /// <param name="recon_regex">Regular expression for finding match key in text field</param>
    /// <param name="bankID2matchKey">Out param: The all important mapping between the match key and the database ID</param>
    /// <param name="status"></param>
    /// <returns>A list of BinnedMatchValues which will contain account and match key</returns>
    private List<BinnedMatchValue> getBankDepositMatchKeysNOTDateBinned(string user, string recon_regex, out Dictionary<int, string> bankID2matchKey, out ReconStatus status)
    {
      status = new ReconStatus(true);

      var binnedList = new List<BinnedMatchValue>();

      // If the match key is compound, keep track of components
      // This is extremely dubious...
      bankID2matchKey = new Dictionary<int, string>();

      // Create Dictionary of match key against MatchValue 
      var matchKeyMap = new Dictionary<String, MatchValue>();

      // Setup regex pattern matcher here
      Regex regexPattern = new Regex(recon_regex);

      // Bug 10497: Pull the Account_Id so we can match against that also
      // Bug 11099: Simplified and optimized by removing subquery and using RECONCILED_STATUS instead
      String bankSql = @"SELECT BANK_DEPOSIT_ID, AMOUNT, TEXT, ACCOUNT_ID, DEPOSIT_DATE FROM TG_BANK_DEPOSIT_DATA 
                     WHERE STATUS != 'r'";

      SqlDataReader dataReader = null;
      try
      {
        dataReader = setupReader(bankSql, ref status);

        // There was an error in the SQL format but this was not set!?
        if (!status.success)
        {
          status.setErr("Automatch failed: Cannot read bank match key records",
             "Cannot query the TG_BANK_DEPOSIT_DATA table for the Reconciliation Key", "REC-172", true);
          return binnedList;
        }
        while (dataReader.Read())
        {
          int bankDepositId = dataReader.GetInt32(0);
          Decimal amount = Convert.ToDecimal(dataReader.GetDouble(1));
          // Bug 11100: Prevent crash if null Text field
          String textField = dataReader.IsDBNull(2) ? "" : dataReader.GetString(2);
          Match match = regexPattern.Match(textField);
          if (!match.Success)
            continue;

          String nakedMatchKey = match.Groups[1].Value;

          // Keep track of which real key each deposit is with
          bankID2matchKey.Add(bankDepositId, nakedMatchKey);

          //if (compoundMatchKeys.ContainsKey(nakedMatchKey))
          //  nakedMatchKey = compoundMatchKeys[nakedMatchKey];

          // Bug 10497: The bank account must be part of the automatch process even for match keys
          String accountId = dataReader.GetString(3);

          // Build a match key composed of the bank account and match key
          string matchKey = accountId + m_bankAccount_ref_delimiter + nakedMatchKey;
          // End bug 10497

          // Grab the date but only for reporting purposes here
          DateTime depositDate = dataReader.GetDateTime(4);
          string dayBin = convertToBinnedDay(depositDate);

          // Now here is the tricky part. I used to make the day prefix of the key
          // but this has been change to not do that.
          // The binnedMatchKey will be Account | matchkey
          string binnedMatchKey = /*dayBin + '#' + */ matchKey;

          // Make a list of Bank IDs that has a length of only one
          // since there is no binning
          List<DatabaseID> bankDepositIds = new List<DatabaseID>();
          bankDepositIds.Add(new DatabaseID(bankDepositId));

          BinnedMatchValue binnedMatchValue = new BinnedMatchValue(binnedMatchKey, amount, bankDepositIds);
          binnedMatchValue.depositDay = dayBin;    // We only use the date now for automatch reporting
          binnedList.Add(binnedMatchValue);
        }

      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in getBankDepositMatchKeysNOTDateBinned; ", e);

        status.setErr("Automatch failed: Cannot collate bank match key records",
            String.Format("getLastReconciledRecord failed: Error: {0}: Traceback: {1}", e, e.StackTrace),
            "REC-197", false);
      }
      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
      }

      return binnedList;
    }


    /// <summary>
    /// Take a DEPOSIT_DATE (for bank deposits) 
    /// or EFFECTIVEDT (for expected deposits)
    /// and truncate down to a string
    /// representation of the day.  Clip off the time.
    /// If deposit processing will extend after midnight,
    /// we need to rethink this.
    /// </summary>
    /// <param name="depositDate"></param>
    /// <returns></returns>
    private string convertToBinnedDay(DateTime depositDate)
    {
      DateTime day = depositDate.Date;
      return day.ToShortDateString();
    }


    /// <summary>
    /// Scan the expected deposits and return the ones with match keys bin'ed by day.
    /// </summary>
    /// <param name="deptID2MatchKey"></param>
    /// <returns></returns>
    private List<BinnedMatchValue> getExpectedDepositMatchKeysDateBinned(GenericObject deptID2MatchKey)
    {
      var binnedList = new List<BinnedMatchValue>();

      // Bug 11099: Simplified and optimized by removing subquery and using RECONCILED_STATUS instead
      // Bug #14729 FT - Don't include transferred deposits (where dfile.ACC_FILENBR is not null)
      String expectedSql =
        @"SELECT ddata.DEPOSITID, ddata.DEPOSITNBR, ddata.UNIQUEID, dfile.DEPTID, ddata.AMOUNT, ddata.BANKID, dfile.EFFECTIVEDT
            FROM TG_DEPOSIT_DATA as ddata, TG_DEPFILE_DATA as dfile 
            WHERE isNull(ddata.RECONCILED_STATUS,'u') != 'r'            
            AND DEPOSITTYPE = 'FILE' AND VOIDDT is null
            AND ddata.DEPFILENBR = dfile.DEPFILENBR
            AND ddata.DEPFILESEQ = dfile.DEPFILESEQ
            AND ddata.POSTDT IS NOT NULL
            AND ddata.POSTDT >= @expectedStartDate 
            AND dfile.ACC_FILENBR is null
            {0}";

      String unReconcilableBankIDs = getUnreconcilableBankIDs("ddata.");
      expectedSql = String.Format(expectedSql, unReconcilableBankIDs);

      // Put in map for bin'ing 
      var matchKeyMap = new Dictionary<String, MatchValue>();

      SqlDataReader dataReader = null;

      try
      {
        ReconStatus status = new ReconStatus(true);

        // Bug 9934: FT: Query the CASL config property for the reconciliation_cutoff.
        DateTime expectedStartDate = getExpectedStartDate();

        dataReader = setupReader(expectedSql, ref status,
          param("expectedStartDate", SqlDbType.DateTime, expectedStartDate)
        );

        if (!status.success)
          return binnedList;     // TODO: Do I need to do more here?

        while (dataReader.Read())
        {
          DatabaseID databaseID = new DatabaseID(
            dataReader.GetString(0), dataReader.GetInt32(1), dataReader.GetInt32(2)
            );
          String deptID = dataReader.GetString(3);
          Decimal amount = Convert.ToDecimal(dataReader.GetDouble(4));

          // Change workgroup ID to match key via CBT_Flex
          String bankId = dataReader.IsDBNull(5) ? "" : dataReader.GetString(5);
          String workgroupMatchKey = getWorkgroupMatchKeyUPHS(deptID2MatchKey, deptID, bankId);

          // Do not collect deposits with no match key
          if (workgroupMatchKey == null || workgroupMatchKey == "")
            continue;

          // Bug 10497: Bin amounts by Bank Account plus match key
          bool reconcilable;
          String bankAccount = bankID2Account(bankId, out reconcilable, ref status);
          // If the Reconcilable configuration variable is false, ignore this record
          if (!reconcilable)
            continue;

          workgroupMatchKey = bankAccount + m_bankAccount_ref_delimiter + workgroupMatchKey;

          DateTime effectiveDate = dataReader.GetDateTime(6);
          string dayBin = convertToBinnedDay(effectiveDate);

          string binnedMatchKey = dayBin + '#' + workgroupMatchKey;

          MatchValue matchValue;
          if (matchKeyMap.TryGetValue(binnedMatchKey, out matchValue))
          {
            matchValue.add(databaseID, amount);
            if (matchValue.matchDay != dayBin)
              throw new Exception("Foobar slpatz");
            matchKeyMap[binnedMatchKey] = matchValue;
          }
          else
          {
            MatchValue newMatchValue = new MatchValue(databaseID, amount);
            newMatchValue.matchDay = dayBin;
            matchKeyMap.Add(binnedMatchKey, newMatchValue);
          }

        }
        // OK, we now have a hash of all the match keys binned by deposit date, account, amount
        // Convert to a list and strip the date
        foreach (string binnedMatchKey in matchKeyMap.Keys)
        {
          MatchValue matchValue = matchKeyMap[binnedMatchKey];
          string[] originalMatchKey = binnedMatchKey.Split('#');
          BinnedMatchValue binnedMatchValue =
            new BinnedMatchValue(originalMatchKey[1], matchValue.totalAmount, matchValue.bankDepositIds);
          binnedMatchValue.depositDay = matchValue.matchDay;
          binnedList.Add(binnedMatchValue);
        }

        return binnedList;
      }
      finally
      {
        if (dataReader != null)
          dataReader.Close();
      }
    }

    private List<BinnedMatchValue> getExpectedDepositMatchKeysFilenameBinned(GenericObject deptID2MatchKey)
    {
      var binnedList = new List<BinnedMatchValue>();

      // Bug 11099: Simplified and optimized by removing subquery and using RECONCILED_STATUS instead
      // Bug #14729 FT - Don't include transferred deposits (where dfile.ACC_FILENBR is not null)
      String expectedSql =
        @"SELECT ddata.DEPOSITID, ddata.DEPOSITNBR, ddata.UNIQUEID, dfile.DEPTID, ddata.AMOUNT, ddata.BANKID, 
          dfile.EFFECTIVEDT, 
          dfile.DEPFILENBR, dfile.DEPFILESEQ
            FROM TG_DEPOSIT_DATA as ddata, TG_DEPFILE_DATA as dfile 
            WHERE isNull(ddata.RECONCILED_STATUS,'u') != 'r'            
            AND DEPOSITTYPE = 'FILE' AND VOIDDT is null
            AND ddata.DEPFILENBR = dfile.DEPFILENBR
            AND ddata.DEPFILESEQ = dfile.DEPFILESEQ
            AND ddata.POSTDT IS NOT NULL
            AND ddata.POSTDT >= @expectedStartDate 
            AND dfile.ACC_FILENBR is null
            {0}";

      String unReconcilableBankIDs = getUnreconcilableBankIDs("ddata.");
      expectedSql = String.Format(expectedSql, unReconcilableBankIDs);

      // Put in map for bin'ing 
      var matchKeyMap = new Dictionary<String, MatchValue>();

      SqlDataReader dataReader = null;

      try
      {
        ReconStatus status = new ReconStatus(true);

        // Bug 9934: FT: Query the CASL config property for the reconciliation_cutoff.
        DateTime expectedStartDate = getExpectedStartDate();

        dataReader = setupReader(expectedSql, ref status,
          param("expectedStartDate", SqlDbType.DateTime, expectedStartDate)
        );

        if (!status.success)
          return binnedList;     // TODO: Do I need to do more here?

        while (dataReader.Read())
        {
          DatabaseID databaseID = new DatabaseID(
            dataReader.GetString(0), dataReader.GetInt32(1), dataReader.GetInt32(2)
            );
          String deptID = dataReader.GetString(3);
          Decimal amount = Convert.ToDecimal(dataReader.GetDouble(4));

          // Change workgroup ID to match key via CBT_Flex
          String bankId = dataReader.IsDBNull(5) ? "" : dataReader.GetString(5);
          String workgroupMatchKey = getWorkgroupMatchKeyUPHS(deptID2MatchKey, deptID, bankId);

          // Do not collect deposits with no match key
          if (workgroupMatchKey == null || workgroupMatchKey == "")
            continue;

          // Bug 10497: Bin amounts by Bank Account plus match key
          bool reconcilable;
          String bankAccount = bankID2Account(bankId, out reconcilable, ref status);
          // If the Reconcilable configuration variable is false, ignore this record
          if (!reconcilable)
            continue;

          StringBuilder filenumber = new StringBuilder();
          filenumber.Append(dataReader.GetInt32(7)).Append(dataReader.GetInt32(8));

          workgroupMatchKey = bankAccount + m_bankAccount_ref_delimiter + workgroupMatchKey;

          DateTime effectiveDate = dataReader.GetDateTime(6);
          string dayBin = convertToBinnedDay(effectiveDate);

          string binnedMatchKey = dayBin + '#' + workgroupMatchKey;

          MatchValue matchValue;
          if (matchKeyMap.TryGetValue(binnedMatchKey, out matchValue))
          {
            matchValue.add(databaseID, amount);
            if (matchValue.matchDay != dayBin)
              throw new Exception("Foobar slpatz");
            matchKeyMap[binnedMatchKey] = matchValue;
          }
          else
          {
            MatchValue newMatchValue = new MatchValue(databaseID, amount);
            newMatchValue.matchDay = dayBin;
            matchKeyMap.Add(binnedMatchKey, newMatchValue);
          }

        }
        // OK, we now have a hash of all the match keys binned by deposit date, account, amount
        // Convert to a list and strip the date
        foreach (string binnedMatchKey in matchKeyMap.Keys)
        {
          MatchValue matchValue = matchKeyMap[binnedMatchKey];
          string[] originalMatchKey = binnedMatchKey.Split('#');
          BinnedMatchValue binnedMatchValue =
            new BinnedMatchValue(originalMatchKey[1], matchValue.totalAmount, matchValue.bankDepositIds);
          binnedMatchValue.depositDay = matchValue.matchDay;
          binnedList.Add(binnedMatchValue);
        }

        return binnedList;
      }
      finally
      {
        if (dataReader != null)
          dataReader.Close();
      }
    }

    /// <summary>
    /// Take the comma-separate Bank Reconciliation Key from the Workgroup config and translate it to 
    /// the real match key (the "Merchant Key" type of number) based on the deposit file's DEPTID.
    /// So start with something like "CPUPAMEX:000123, CPUPCredit:000124" and return 00123
    /// for "CPUPAMEX".
    /// </summary>
    /// <param name="deptID2MatchKey"></param>
    /// <param name="originalDeptID"></param>
    /// <param name="bankID"></param>
    /// <returns></returns>
    private string getWorkgroupMatchKeyUPHS(GenericObject deptID2MatchKey, string originalDeptID, string bankID)
    {
      if (deptID2MatchKey == null || originalDeptID == null || bankID == null)
        return "";

      // Try to get the comma-separated match key.  If not found, then this is a no-go
      String bankReconciliationKey = deptID2MatchKey.get(originalDeptID, "") as String;
      if (bankReconciliationKey == null || bankReconciliationKey == "")
        return "";

      // The bankReconciliationKey will be something like CPUPAMEX:000123.  
      // The CPUPAMEX is the BANKID, but we need the number following the :.
      // This is the real match key
      string[] bankRecKeys = bankReconciliationKey.Split(',');
      foreach (string bankRecKey in bankRecKeys)
      {
        if (!bankRecKey.Contains(':'))
          continue;
        string[] parts = bankRecKey.Split(':');
        if (parts[0] == null || parts[0] == "" || parts[1] == null || parts[1] == "")
          continue;
        string bankIdPartOfKey = parts[0].Trim();
        if (bankIdPartOfKey == bankID)
          return parts[1];
      }
      return "";
    }

    /// <summary>
    /// Mainline for match key processing for UPHS.
    /// This method tries to match the expected and bank deposits based on the match key.
    /// It uses the following steps:
    /// <list type="numbered">
    /// 
    /// <item><i>Bank Deposit List:</i> Get a list of the bank deposits binned by the account and match key.  
    /// This list is NOT binned based on date.  Remove duplicates.  This also returns a mapping between the bank ID and the match key </item>
    /// </list>
    /// 
    /// <item><i>Expected Deposit List:</i> Get a list of the expected deposits that have match keys.  
    /// The expected deposits are binned by the day they were made.  Remove duplicates.</item>
    /// 
    /// <item>
    /// <i>Match:</i> Now loop through the expected deposits and try to match them to the 
    /// bank deposits.
    /// </item>
    /// 
    /// <item>
    /// If a match is found, then pull all the database IDs of the matched expected and bank deposits.  
    /// Remember that the bank deposits for an account and match key can represent multiple records.
    /// Likewise all the expected deposits binned by a day can represent multiple records.
    /// </item>
    /// 
    /// <item>
    /// When these bank record IDs are found, bin them again.  This time organize them in bins with 
    /// the match key being the key and a list of bank database IDs as the value.
    /// </item>
    /// 
    /// <item>
    /// Loop through the <i>values</i> in the above list.
    /// </item>
    /// 
    /// <item>
    /// For each list of bank database IDs, do match against the list of expected IDs.
    /// </item>
    /// 
    /// </list>
    /// </summary>
    /// <remarks>
    /// See the <i>As-built Spec</i> for more information about this matching: 
    /// <seealso>http://172.16.1.141/bugzilla/show_bug.cgi?id=11723</seealso> 
    /// </remarks>
    /// <param name="user">User ID from CASL</param>
    /// <param name="deptID2MatchKey">Mapping between department ID (work group) and match key</param>
    /// <param name="recon_regex">Regular expression for extracting match key from Bank Deposit Text fields</param>
    /// <param name="reconciled">List of reconciled deposits</param>
    /// <param name="reporter">Handle to reporting class</param>
    /// <param name="status">Output of status</param>
    public void autoMatchBankReconKeyDateBinned(string user, GenericObject deptID2MatchKey, string recon_regex, List<ReconciledDeposit> reconciled, AutomatchReports reporter, out ReconStatus status)
    {
      status = new ReconStatus(true);

      reporter.textReport("\nMatch Key Configuration:");

      reporter.textReport("\nRegular Expression: " + recon_regex);

      string writeDetailedReportString = getConfigValue("enable_automatch_detailed_report", "false");
      bool writeDetailedReport = Convert.ToBoolean(writeDetailedReportString);
      if (writeDetailedReport)
      {
        foreach (string deptID in deptID2MatchKey.getStringKeys())
        {
          reporter.textReport("Department ID: " + deptID + " Match Key: " + deptID2MatchKey.get(deptID, ""));
        }
      }
      reporter.textReport("\n\nStarting Automatch using the Match Key (Bank Reconciliation Keys)");

      // Only do this match if there is a Regular Express in the config
      if (recon_regex == null || recon_regex.Length == 0)
        return;

      // Start off and get a map of the match keys against the amount in the Bank Deposits
      Dictionary<int, string> bankID2matchKey;

      // Get a list of bank deposits, not binned by the match key.  
      // Each bin has a list of database IDs.
      List<BinnedMatchValue> bankDepositList = getBankDepositMatchKeysNOTDateBinned(user, recon_regex, out bankID2matchKey, out status);
      if (!status.success)
        return;

      bankDepositList = removeDuplicates(bankDepositList, reporter, "Bank Deposit");

      // Now get a list of the ref / amount pairs for expected deposits
      List<BinnedMatchValue> expectedDepositsList = getExpectedDepositMatchKeysDateBinned(deptID2MatchKey);
      expectedDepositsList = removeDuplicates(expectedDepositsList, reporter, "Expected Deposit");

      // Now we have two lists: The expected binned by day | account | match key and the bank list binned by account | match key.
      // Do a list merge by using binary search since we can have duplicates in the lists
      bankDepositList.Sort();
      bool reportLagTime = true;

      foreach (BinnedMatchValue expectedDepositValue in expectedDepositsList)
      {
        int index = bankDepositList.BinarySearch(expectedDepositValue);
        if (index < 0)
          continue;

        // Match found!!
        // Bug 12326: FT: Get the best match; 
        // the bank deposit with the closest date to the expected deposit
        bool matched;
        BinnedMatchValue bankDepositValue = findBestBankDepositMatch(bankDepositList, expectedDepositValue, reporter, reportLagTime, out matched);
        reportLagTime = false;
        if (!matched)
          continue;
        // End 12336

        // Get all the database records
        List<DatabaseID> expectedIDs = expectedDepositValue.bankDepositIds;
        List<DatabaseID> bankIDsFullList = bankDepositValue.bankDepositIds;

        // Bin banks match keys with a list of Database ID in each bin
        Dictionary<string, List<DatabaseID>> bankIDBins = binBankIDsByMatchKey(bankIDsFullList, bankID2matchKey);
        foreach (List<DatabaseID> bankIDs in bankIDBins.Values)
        {
          // Now do an automatch for each list of bank IDs
          string[] matchkeys = expectedDepositValue.matchKey.Split(m_bankAccount_ref_delimiter);
          string matchedAccountId = matchkeys[0];
          string matchedMatchKey = matchkeys[1];
          Decimal expectedAmount = expectedDepositValue.amount;
          Decimal bankAmount = bankDepositValue.amount;

          reconcileAutoMatchKey(expectedIDs, bankIDs, user, out status);
          // Bail out immediatly if there is an error.
          if (!status.success)
          {
            // Bug 10262
            // Bug 11169: Changed to write each line to the file
            reporter.textReport("Cannot reconcile automatched records. Error: ", status.detailedError,
              " Account: ", matchedAccountId,
              " key: '", matchedMatchKey,
              "' amount: ", expectedAmount);
            reporter.matchReport(AutomatchReports.MatchAction.error, matchedAccountId, matchedMatchKey, expectedAmount, "Cannot reconciled automatched records: " + status.detailedError);
            continue;
          }

          ReconciledDeposit recon = getLastReconciledRecord(out status);
          // If there is an error here, then the reconcile() probably failed.
          if (!status.success)
          {
            // Bug 10262
            // Bug 11169: Changed to write each line to the file
            reporter.textReport("Record matched with key, but get last record failed. Error: ", status.detailedError,
              " Account: ", matchedAccountId,
              " key: '", matchedMatchKey,
              "' amount: ", expectedAmount);
            reporter.matchReport(AutomatchReports.MatchAction.error, matchedAccountId, matchedMatchKey, expectedAmount, "Record matched with key, but get last record failed: " + status.detailedError);
            continue;
          }

          // Found a match.  Bug 10262: Log action
          // Bug 11169: Changed to write each line to the file
          reporter.textReport("Automatched this record:",
            " Account: ", matchedAccountId,
            " Key: '", matchedMatchKey,
            "' Expected Effective Date: ", expectedDepositValue.depositDay,
            " Bank As-of Day: ", bankDepositValue.depositDay,
            " Total amount: ", String.Format("{0:C}", expectedAmount));
          reporter.matchReport(AutomatchReports.MatchAction.match_key, matchedAccountId, matchedMatchKey, bankAmount, "");
          reconciled.Add(recon);
          // Found this bug in S&W testing. Bug 12482: 
          // Set this so that the automatch report is accurate
          expectedDepositValue.reconciled = true; 

          // Bug 10262: Prune bank deposit map so we will have a list of leftovers at the end
          // and so we don't reconcile them twice.
          bankDepositValue.reconciled = true;
        }
      }
      // Dump the all-important report on what did not automatch
      postMortemUnreconciled(deptID2MatchKey, reporter, bankDepositList, expectedDepositsList);
    }

    /// <summary>
    /// Finds the bank deposit that best matches the expected deposit.
    /// There may be multiple bank deposits that match so
    /// search through the matches and pick the one 
    /// that has the closest date (day actually) to the expected
    /// date, but after or on the expected date.
    /// <para>
    /// If there are no expected deposits that on or after the expected date,
    /// set the "matched" flag to false and return.
    /// </para>
    /// <remarks>
    /// See <seealso cref="http://172.16.1.141/bugzilla/show_bug.cgi?id=12326">http://172.16.1.141/bugzilla/show_bug.cgi?id=12326</seealso>
    /// </remarks>
    /// <remarks>
    /// If it turns out that some match dates that are too close 
    /// (expected on 6/22 and bank on 6/22 or 6/23), so a new configuration variable was created called
    /// expected_bank_lag_time ("Expected / Bank Lag Time) which can be set the anticipated lag time between
    /// expected and bank deposits.
    /// </remarks>
    /// </summary>
    /// <param name="bankDepositList">List of all the bank deposit bins</param>
    /// <param name="expectedDepositValue">Single expected deposit bin</param>
    /// <param name="matched">False is none of the bank deposits were on/after the expected deposit, true otherwise</param>
    /// <returns>Bank Deposit that is closest to expected deposit, but not before it.</returns>
    private BinnedMatchValue findBestBankDepositMatch(List<BinnedMatchValue> bankDepositList, BinnedMatchValue expectedDepositValue, AutomatchReports reporter, bool reportLagTime, out bool matched)
    {
      matched = true;

      // First get a list of matching bank deposits using the handy FindAll method on List
      List<BinnedMatchValue> matchingBankDeposits = bankDepositList.FindAll(
        delegate(BinnedMatchValue match)
        {
          return match.Equals(expectedDepositValue);   // Use the overloaded Equals in BinnedMatchValue
        }
      );

      // Very, very unlikely.  Should be an Assert if Asserts worked on the server side
      if (matchingBankDeposits.Count < 1)
        throw new Exception("Automatch Error: findBestBankDepositMatch could not find any matching bank deposits");

      // More than one bank deposit had the same match key and amount.
      // Find the bank deposit date that is closest, but *after/on* the expected deposit.
      DateTime expectedDay = DateTime.Parse(expectedDepositValue.depositDay);

      // Add in the lag time
      int lagDays = getLagDays();

      // Jump ahead the lag days
      expectedDay = jumpAheadNWorkDays(expectedDay, lagDays);
      if (reportLagTime && lagDays > 0) 
        reporter.textReport(string.Format("Expected / Bank Lag Time set to: {0} working days.",
          lagDays));

      TimeSpan closestSpan = TimeSpan.MaxValue;   // Start off with highest value
      BinnedMatchValue closestBankDeposit = null;

      // Loop through and find bank deposit that is closest (time-wise) to expected deposit
      foreach (BinnedMatchValue bankDeposit in matchingBankDeposits)
      {
        DateTime bankDay = DateTime.Parse(bankDeposit.depositDay);
        // Do not match any bank deposit before the expected deposit day
        if (bankDay < expectedDay)
          continue;
        TimeSpan span = bankDay.Subtract(expectedDay);
        if (span < closestSpan)
        {
          closestSpan = span;
          closestBankDeposit = bankDeposit;
        }
      }
      
      // No matches found.  This means that there were no bank deposits on or after the expected deposit day
      if (closestBankDeposit == null)
      {
        matched = false;
        return null;
      }

      return closestBankDeposit;
    }

    /// <summary>
    /// Read the Expected / bank lag time variable
    /// and handle bad formats.
    /// <seealso cref="http://172.16.1.141/bugzilla/show_bug.cgi?id=12326"/>
    /// </summary>
    /// <returns></returns>
    private int getLagDays()
    {
      string lagDaysString = getConfigValue("expected_bank_lag_time", "0");
      try
      {
        int lagDays = Convert.ToInt32(lagDaysString);
        return lagDays;
      }
      catch
      {
        return 0;
      }
    }

    /// <summary>
    /// If there are duplicates in the list, toss them and write up in the report.
    /// Duplicates introduce ambiguity which banks do not like.
    /// </summary>
    /// <param name="bankDepositList"></param>
    /// <returns></returns>
    private List<BinnedMatchValue> removeDuplicates(List<BinnedMatchValue> depositList, AutomatchReports reporter, string listName)
    {
      // Bug 11515
      //GenericObject reconGeo = (GenericObject)c_CASL.c_object("Business.bank_deposit_recon.data");
      //bool allowDuplicates = (bool)reconGeo.get("allow_automatch_duplicates", false);
      string allowDuplicatesString = getConfigValue("allow_automatch_duplicates", "false");
      bool allowDuplicates = Convert.ToBoolean(allowDuplicatesString);
      // End 11515
      if (allowDuplicates)
        return depositList;

      var dupMap = new Dictionary<string, List<BinnedMatchValue>>();

      // Loop through the list and accumulate dups
      foreach (BinnedMatchValue deposit in depositList)
      {
        string matchKey = deposit.matchKey + "|" + deposit.amount;
        List<BinnedMatchValue> value;
        if (dupMap.TryGetValue(matchKey, out value))
        {
          // Found duplicate: store here
          value.Add(deposit);
        }
        else
        {
          // No dup, create new record
          value = new List<BinnedMatchValue>();
          value.Add(deposit);
          dupMap.Add(matchKey, value);
        }
      }

      // Now we have accumulated duplicates
      // Output dups and return trimmed list
      var trimmedList = new List<BinnedMatchValue>();
      foreach (string matchKey in dupMap.Keys)
      {
        List<BinnedMatchValue> dupList = dupMap[matchKey];
        if (dupList.Count == 1)
          trimmedList.Add(dupList.ElementAt(0));
        else
        {
          foreach (BinnedMatchValue dupEntry in dupList)
          {
            reporter.textReport(string.Format("Deposit against {0} for {1:C} on {2} has a duplicate in the {3} list. This deposit cannot be matched automatically.",
              dupEntry.matchKey,  dupEntry.amount, dupEntry.depositDay, listName));
          }
        }

      }
      return trimmedList;
    }

    private void postMortemUnreconciled(GenericObject deptID2MatchKey, AutomatchReports reporter, List<BinnedMatchValue> bankDepositList, List<BinnedMatchValue> expectedDepositsList)
    {
      // Sort on the expected or As-of date
      bankDepositList.Sort((x, y) => DateTime.Parse(x.depositDay).CompareTo(DateTime.Parse(y.depositDay)));
      expectedDepositsList.Sort((x, y) => DateTime.Parse(x.depositDay).CompareTo(DateTime.Parse(y.depositDay)));

      // Bug 11515
      //GenericObject reconGeo = (GenericObject)c_CASL.c_object("Business.bank_deposit_recon.data");
      //bool writeDetailedReport = (bool)reconGeo.get("enable_automatch_detailed_report", false);
      string writeDetailedReportString = getConfigValue("enable_automatch_detailed_report", "false");
      bool writeDetailedReport = Convert.ToBoolean(writeDetailedReportString);
      // End 11515

      reporter.textReport("\nPost mortem for Bank Reconciliation Key pass:\n");
      reporter.textReport("Unreconciled Expected Deposit List (binned by Day/Account/Match Key):");
      reporter.textReport(
              "Eff. Date".PadRight(12)
              + "Account".PadRight(13)
              + "Match Key".PadRight(16)
              + "Amount");

      reporter.textReport("--------------------------------------------------------");
      foreach (BinnedMatchValue deposit in expectedDepositsList)
      {
        if (!deposit.reconciled)
        {
          string[] parts = deposit.matchKey.Split('|');
          string matchKey = "'" + parts[1] + "'";
          reporter.textReport(
            deposit.depositDay.PadRight(12) +
            parts[0].PadRight(13) + 
            matchKey.PadRight(16) + 
            String.Format("total amount: {0:C}", deposit.amount) + string.Format("\tfor {0} deposits", deposit.bankDepositIds.Count));

          if (writeDetailedReport)
          {
            // Dump the details
            foreach (DatabaseID databaseID in deposit.bankDepositIds)
            {
              ReconStatus localStatus;
              ExpectedDepositDetails details = getExpectedDepositDetails(databaseID, deptID2MatchKey, out localStatus);
              reporter.textReport("\t" + details.depDate
                + String.Format(":\t{0:C}", details.amount)
                + ":\t" + details.bankId);
            }
          }
        }
      }

      reporter.textReport("\n\nUnreconciled Bank Deposit List (not binned):");
      reporter.textReport(
              "As-of Date".PadRight(12)
              + "Account".PadRight(13)
              + "Match Key".PadRight(16)
              + "Amount");
      reporter.textReport("--------------------------------------------------------");
      foreach (BinnedMatchValue deposit in bankDepositList)
      {
        if (!deposit.reconciled)
        {
          string[] parts = deposit.matchKey.Split('|');
          string matchKey = "'" + parts[1] + "'";
          reporter.textReport(
            deposit.depositDay.PadRight(12) +
            parts[0].PadRight(13) +
            matchKey.PadRight(16) + 
            String.Format("total amount: {0:C}", deposit.amount) + string.Format("\tfor {0} deposits", deposit.bankDepositIds.Count));

          if (writeDetailedReport)
          {
            // Dump the details
            foreach (DatabaseID databaseID in deposit.bankDepositIds)
            {
              ReconStatus localStatus;
              BankDepositDetails details = getBankDepositDetails(databaseID, out localStatus);
              reporter.textReport("\t"
                + String.Format("{0:C}", details.amount)
                + "\tBank Ref: " + details.bankReference
                + "\tCust Ref: " + details.refNum);
            }
          }

        }
      }
    }
  }
}
