﻿using System;
using System.Collections.Generic;
using System.Text;

namespace recon
{
  public class Tender
  {
    public String prop { get; set; }
    public String kind { get; set; }
    public String description { get; set; }
    public Decimal amount { get; set; }
    public String sourceRefID { get; set; }
    public DateTime sourceDate { get; set; }
  }
}
