﻿using System;
using System.Collections.Generic;
using System.Text;

namespace recon
{
  public class ReconciledDepositDetails : ReconciledDeposit
  {
    private List<ExpectedDepositDetails> expectedDetails = new List<ExpectedDepositDetails>();
    private List<BankDepositDetails> bankDetails = new List<BankDepositDetails>();

    public void addExpected(ExpectedDepositDetails details)
    {
      expectedDetails.Add(details);
    }

    public void addExpectedDepositDetailsList(List<ExpectedDepositDetails> detailsList)
    {
      expectedDetails = detailsList;
    }

    public List<ExpectedDepositDetails> getExpectedDetailList()
    {
      return expectedDetails;
    }

    public void addBank(BankDepositDetails details)
    {
      bankDetails.Add(details);
    }
    public void addBankDetailList(List<BankDepositDetails> detailsList)
    {
      bankDetails = detailsList;
    }
    public List<BankDepositDetails> getBankDetailList()
    {
      return bankDetails;
    }
  }
}
