﻿using System;
using System.Collections.Generic;
using System.Text;

namespace recon
{
  class RecordCodes
  {
    public const String File = "01";
    public const String Group = "02";
    public const String Account = "03";
    public const String Trans = "16";
    public const String AccTrailer = "49";
    public const String Continuation = "88";
    public const String GrpTrailer = "98";
    public const String FileTrailer = "99";

    // field position in record
    public const int recordCodeField = 0;
    public const int typeCodeField = 1;
    public const int AccountTrailerNumRecordsField = 2;
    public const int FileTrailerNumRecordsField = 3;

    // For error checking in the lexical scanner
    public const int customerAccountNumberField = 1;
    public const int originatorGroupBankField = 2;
  }
}
