﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using CASL_engine;

namespace recon
{
  class ParklandParser : Parser
  {

    enum State
    {
      Start = 0,
      File = 1,
      Group = 2,
      Account = 3,
      Trans = 4,
      Err = 5,
      End = 6,
      NoGrpTrl,
      NoAccTrl
    };

    enum Transition
    {
      FileHeader = 0,
      GroupHeader = 1,
      AccountHeader = 2,
      TransDetail = 3,
      AccountTrailer = 4,
      GroupTrailer = 5,
      FileTrailer = 6,
      Error = 7,
      NotStarted = 8
    };

    State[][] stateTable_strict_but_erroneous_maybe = {
            // Transitions        STATES-->   Start        File         Group          Account        Trans
            /* FileHeader     */ new State[] {State.File,  State.File,  State.Err,     State.Err,     State.Err},
            /* Group Header   */ new State[] {State.Err,   State.Group, State.NoGrpTrl,State.Err,     State.Err},
            /* Account Header */ new State[] {State.Err,   State.Err,   State.Account, State.NoAccTrl,State.Err},
            /* Trans Detail   */ new State[] {State.Err,   State.Err,   State.Err,     State.Trans,   State.Trans},
            /* Account Trailr */ new State[] {State.Err,   State.Err,   State.Err,     State.Account, State.Account},
            /* Group Trailr   */ new State[] {State.Err,   State.Err,   State.Group,   State.Group,   State.Err},
            /* File Trailr    */ new State[] {State.Err,   State.File,  State.File,    State.Err,     State.Err},
            };

    State[][] stateTable = {
            // Transitions        STATES-->   Start        File         Group          Account        Trans
            /* FileHeader     */ new State[] {State.File,  State.File,  State.Err,     State.Err,     State.Err},
            /* Group Header   */ new State[] {State.Err,   State.Group, State.Group,   State.Err,     State.Err},
            /* Account Header */ new State[] {State.Err,   State.Err,   State.Account, State.Account, State.Err},
            /* Trans Detail   */ new State[] {State.Err,   State.Err,   State.Err,     State.Trans,   State.Trans},
            /* Account Trailr */ new State[] {State.Err,   State.Err,   State.Err,     State.Account, State.Account},
            /* Group Trailr   */ new State[] {State.Err,   State.Err,   State.Group,   State.Group,   State.Err},
            /* File Trailr    */ new State[] {State.Err,   State.File,  State.File,    State.Err,     State.Err},
            };

    State currentState = State.Start;
    State previousState = State.Start;

    Transition nextTransition = Transition.NotStarted;
    Transition previousTransition = Transition.NotStarted;


    enum groupField
    {
      recordCode = 0,
      customerID = 1,
      date = 4,       // As-of-Date
      time = 5,       // As-of Time
      dateModifier = 7 // As-of Date modifier
    };

    enum accountField
    {
      recordCode = 0,
      account = 1         // Customer Account Number
    };

    // Position of fields in the record
    enum transField
    {
      recordCode = 0,
      typeCode = 1,
      amount = 2,
      fundsType = 3,
      xFundsType = 4,
      bankRefNum = 4,
      customerRefNum = 5,
      text = 6
    };


    const String unset = "ERROR: unset value";
    String currentAccountNumber = unset;
    DateTime currentDepositDate = Convert.ToDateTime("1/1/1");


    Scanner scannerHandle;

    public override List<TransactionRecord> parse(String path, string bankAccount, bool checkLineNumbers, out ReconStatus status)
    {
      status = new ReconStatus(true);
      var currentRecord = new List<Token>();

      // List of deposit records
      var transactionList = new List<TransactionRecord>();

      try
      {
        // Chop file into records and fields (tokens)
        scannerHandle = new Scanner();
        // Get list and set global
        List<List<Token>> lex = scannerHandle.lexicalScanBAI(path, checkLineNumbers);


        foreach (List<Token> record in lex)
        {
          currentRecord = record;
          //System.Console.WriteLine("Record: " + record.ElementAt(0).field);
          previousTransition = nextTransition;
          nextTransition = getTransition(record);
          //System.Console.WriteLine("Transition: " + nextTransition);
          if (nextTransition == Transition.Error)
          {
            // Get the erroneous record code
            Token erroneousToken = record.ElementAt(RecordCodes.recordCodeField);
            throw new Exception(String.Format("ERROR: BAI format error: line number {0}.  Incorrect hierarchy: Transition Error:\nPrevious State: {1}\tPrevious Transition: {2}\tNext Transition: {3}",
                erroneousToken.lineNumber,
                previousState, previousTransition, nextTransition));
          }

          previousState = currentState;
          currentState = stateTable[(int)nextTransition][(int)currentState];
          if (currentState == State.Err)
          {
            Token erroneousToken = record.ElementAt(RecordCodes.recordCodeField);
            throw new Exception(String.Format("ERROR: BAI format error: line number {0}.  Incorrect hierarchy: State Error:\nPrevious State: {1}\tPrevious Transition: {2}\tNext Transition: {3}",
                erroneousToken.lineNumber,
                previousState, previousTransition, nextTransition));
          }
          stateProcessing(record, currentState, transactionList);

          //System.Console.WriteLine("State: " + currentState);
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in  parse the bank file", e);

        FileInfo fileInfo = new FileInfo(path);

        // Bug 10907: FT: improve error messages
        int lineNumber = 0;
        if (currentRecord.Count > 0)
        {
          Token erroneousToken = currentRecord.ElementAt(RecordCodes.recordCodeField);
          lineNumber = erroneousToken.lineNumber;
        }
        string humanReadable = String.Format("Error: {0}, line {1}. BAI Parser: Cannot parse the bank file: '{2}'. ",
          e.Message, lineNumber, fileInfo.Name);  // Bug 13921. General cleanup 
        string detailedError = String.Format("BAI Parser failed on line {0}: Error: {1} of file {2}",
            lineNumber, e, fileInfo.FullName);
        status.setErr(humanReadable, detailedError, "REC-147", false);
        // End bug 10907
      }
      return transactionList;
    }

    private void stateProcessing(List<Token> record, State currentState, List<TransactionRecord> depositInfo)
    {
      Token erroneousToken;
      switch (currentState)
      {
        case State.Start:
          // Do nothing for now
          break;
        case State.File:
          // do nothing 
          break;
        case State.Group:
          processGroup(record);
          break;
        case State.Account:
          processAccount(record);
          break;
        case State.Trans:
          processTransaction(record, depositInfo);
          break;
        case State.Err:
          erroneousToken = record.ElementAt(RecordCodes.recordCodeField);
          throw new Exception(String.Format(
              "Error on line {0}. BAI format error:\nPrevious State: {1}\tPrevious Transition: {2}\tNext Transition: {3}",
              erroneousToken.lineNumber,
              previousState, previousTransition, nextTransition));
        case State.End:
          break;
        case State.NoGrpTrl:
          erroneousToken = record.ElementAt(RecordCodes.recordCodeField);
          throw new Exception(String.Format(
              "Error on line {0}. BAI format error: New Group started before trailer. \nPrevious State: {1}\tPrevious Transition: {2}\tNext Transition: {3}",
              erroneousToken.lineNumber,
              previousState, previousTransition, nextTransition));
        case State.NoAccTrl:
          erroneousToken = record.ElementAt(RecordCodes.recordCodeField);
          throw new Exception(String.Format(
              "Error on line {0}. BAI format error: New Account started before trailer. \nPrevious State: {1}\tPrevious Transition: {2}\tNext Transition: {3}",
              erroneousToken.lineNumber,
              previousState, previousTransition, nextTransition));
        default:
          erroneousToken = record.ElementAt(RecordCodes.recordCodeField);
          throw new Exception(String.Format(
              "Error on line {0}. BAI format error: Unrecognized state: {1}",
              erroneousToken.lineNumber,
              currentState));
      }
    }

    private void processGroup(List<Token> record)
    {
      Token recordCodeToken = record.ElementAt(Convert.ToInt32(groupField.recordCode)); // Bug 13921. Cannot use Parse<> on enum types
      if (!recordCodeToken.field.Equals(RecordCodes.Group))
        return;
      Token dateToken = record.ElementAt(Convert.ToInt32(groupField.date));
      String dateString = dateToken.field;
      Token timeToken = record.ElementAt(Convert.ToInt32(groupField.time));

      // Bug 12890: The As-of Time field can be unset--do not let this crash the parse
      string timeString = timeToken.field;
      if (timeString == null || timeString == "")
      {
        currentDepositDate = DateTime.ParseExact(dateString, "yyMMdd",
                            null);
      }
      else
      {
        string dateTimeString = dateString + " " + timeString;
      currentDepositDate = DateTime.ParseExact(dateTimeString, "yyMMdd HHmm",
                            null);
      }
      // End Bug 12890
    }

    private void processAccount(List<Token> record)
    {
      Token recordCodeToken = record.ElementAt(Convert.ToInt32(groupField.recordCode)); // Bug 13921. Cannot use Parse<> on enum types
      if (!recordCodeToken.field.Equals(RecordCodes.Account))
        return;
      Token accountToken = record.ElementAt(Convert.ToInt32(accountField.account)); // Bug 13921.
      currentAccountNumber = accountToken.field;
    }

    private void processTransaction(List<Token> record, List<TransactionRecord> transactionList)
    {
      int typeCode = getTypeCode(record);
      TransactionRecord depositInfo = new TransactionRecord();

      // Get amount
      Decimal amount = getAmount(record);
      depositInfo.amount = amount;

      int fundsTypeOffset = getFundsTypeOffset(record);

      // Bug 13921.  
      int bankRefOffset = Convert.ToInt32(transField.bankRefNum) + fundsTypeOffset;
      String bankRefNumber = getField(record, bankRefOffset);

      // Bug 13921.  FT.
      int customerRefOffset = Convert.ToInt32(transField.customerRefNum) + fundsTypeOffset;
      String customerRefNumber = getField(record, customerRefOffset);

      String text = readTextField(record, fundsTypeOffset);

      depositInfo.account = currentAccountNumber;
      depositInfo.date = currentDepositDate;
      depositInfo.amount = amount;
      depositInfo.bankRefNum = bankRefNumber;
      depositInfo.customerRefNum = customerRefNumber;
      depositInfo.text = text;
      depositInfo.baiCode = getTypeCode(record);
      transactionList.Add(depositInfo);
    }

    private int getFundsTypeOffset(List<Token> record)
    {
      // Do the special processing for the Fund Type code
      // Bug 13921.  FT. Cannot use Parse<> on enum types
      Token fundsTypeToken = record.ElementAt(Convert.ToInt32(transField.fundsType));
      String fundsType = fundsTypeToken.field;

      // The Funds Type file may have expanded the record
      int fundsTypeOffset = getFundsTypeOffset(record, fundsType);
      return fundsTypeOffset;
    }

    /// <summary>
    /// Convert a string amount value to a decimal value.
    /// The number in the file will be in cents so the decimal 
    /// point has to inserted
    /// </summary>
    /// <param name="record"></param>
    /// <returns></returns>
    private static Decimal getAmount(List<Token> record)
    {
      // Bug 13921.  FT.
      Token amountToken = record.ElementAt(Convert.ToInt32(transField.amount));
      String amountString = amountToken.field;
      if (amountString == null | amountString == "")
        return 0M;

      Decimal cents = Convert.ToDecimal(amountString);

      return cents / 100M;    // Shift decimal point
    }

    private int getTypeCode(List<Token> record)
    {
      // Bug 13921.  FT. Cannot use Parse<> on enum types
      Token typeCodeToken = record.ElementAt(Convert.ToInt32(transField.typeCode));
      int typeCode = Convert.ToInt32(typeCodeToken.field);  // TODO: catch conversion error
      return typeCode;
    }


    // Get the Text field.  This may be split among the last tokens if the Text has 
    // Embedded commas; join them together here
    private String readTextField(List<Token> record, int fundsTypeOffset)
    {
      // Bug 13921.  FT.
      int textRefOffset = Convert.ToInt32(transField.text) + fundsTypeOffset;
      String text = "";
      for (int i = textRefOffset; i < record.Count; i++)
      {
        //text = text + (i == textRefOffset ? "" : ",") + getField(record, i);
        // Bug 11344: Do not add any commas to the Text field; 
        // extra commas will be mistakenly added after the 88 record.
        text = text + getField(record, i);
      }

      return text;
    }

    private string getField(List<Token> record, int bankRefOffset)
    {
      Token token = record.ElementAt(bankRefOffset);
      return token.field;
    }
    private String getField(List<Token> record, transField fieldEnum)
    {
      // Bug 13921.  FT. Cannot use Parse<> on enum types
      int fieldOffset = Convert.ToInt32(fieldEnum);
      Token token = record.ElementAt(fieldOffset);
      return token.field;
    }

    private int getFundsTypeOffset(List<Token> record, string fundsType)
    {
      switch (fundsType)
      {
        case "":
          return 0;
        case "0":
          return 0;
        case "1":
          return 0;
        case "2":
          return 0;
        case "S":
          return 3;
        case "V":
          return 2;
        case "D":
          // Format after D is X,a,$,a,$,... depending on the value of X
          // Variable number of a,$ pairs.  Number of pairs is X
          Token xFieldToken = record.ElementAt(toElementNumber(transField.xFundsType));
          String xFieldString = xFieldToken.field;
          int xField = Convert.ToInt32(xFieldString);
          return 1 + xField * 2;
        case "Z":
          return 0;
        default:
          throw new Exception(String.Format("ERROR: BAI Parser: Unexpected Funds Type: Field: {0}: Line {1} Field {2}", fundsType,
              record.ElementAt(toElementNumber(transField.fundsType)).lineNumber,
              toElementNumber(transField.fundsType) + 1));
      }

    }


    private static int toElementNumber(transField fieldEnum)
    {
      // Bug 13921.  FT. Cannot use Parse<> on enum types
      return Convert.ToInt32(fieldEnum);
    }

    private Transition getTransition(List<Token> record)
    {
      Token fieldToken = record.ElementAt(getRecordCode());
      String recordCode = fieldToken.field;
      switch (recordCode)
      {
        case RecordCodes.File:
          return Transition.FileHeader;
        case RecordCodes.Group:
          return Transition.GroupHeader;
        case RecordCodes.Account:
          return Transition.AccountHeader;
        case RecordCodes.Trans:
          return Transition.TransDetail;
        case RecordCodes.AccTrailer:
          return Transition.AccountTrailer;
        case RecordCodes.GrpTrailer:
          return Transition.GroupTrailer;
        case RecordCodes.FileTrailer:
          return Transition.FileTrailer;
        default:
          throw new Exception(String.Format(
              "ERROR: BAI Parser: Unrecognized record code: '{0}' at in the first field on line {1}",
              recordCode, fieldToken.lineNumber));
      }

    }

    /// <summary>
    /// Translate Transaction record code to int
    /// </summary>
    /// <returns></returns>
    private static int getRecordCode()
    {
      // Bug 13921.  FT. Cannot use Parse<> on enum types
      return Convert.ToInt32(transField.recordCode);
    }

    private static void parseRecord(List<string> record)
    {

    }

    [Obsolete("Always reports last line in file")]
    internal int getCurrentLine()
    {
      return scannerHandle.getCurrentLineNumber();
    }
  }
}
