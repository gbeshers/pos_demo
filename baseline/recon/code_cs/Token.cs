﻿using System;
using System.Collections.Generic;
using System.Text;

namespace recon
{
  class Token
  {
    public String field { get; set; }
    public int lineNumber { get; set; }
    public int fieldNumber { get; set; }
  }
}
