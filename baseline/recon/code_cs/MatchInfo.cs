﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace recon
{
  /// <summary>
  /// Class to hold sortable automatch report information for the CSV format report.
  /// Bug 10262.
  /// </summary>
  public class MatchInfo : IComparable<MatchInfo>
  {
    internal String action { get; set; }
    internal String accountId { get; set; }
    internal String slip { get; set; }
    internal Decimal? amount { get; set; }
    internal String comment { get; set; }
    public MatchInfo(String action, String accountId, String slip, Decimal? amount, String comment)
    {
      this.action = action;
      this.accountId = accountId;
      this.slip = slip;
      this.amount = amount;
      this.comment = comment.Replace(",","");    // Strip out an commas so that it does not create CSV columns
    }

    /// <summary>
    /// Sort entries so that they intially come up in a sensible order.
    /// Sort based on action, comment, account ID, and slip number.
    /// This is by no means optimal or the perfect way to setup a 
    /// Compare() or CompareTo(), but this sorting is low priority.
    /// At some point this could be extended to also sort on the 
    /// amount field but that is a nullable primative.
    /// </summary>
    /// <param name="other"></param>
    /// <returns></returns>
    public int CompareTo(MatchInfo other)
    {

      if (action != other.action)
        return action.CompareTo(other.action);
      else
        if (comment != other.comment)
          return comment.CompareTo(other.comment);
        else
          if (accountId != other.accountId)
            return accountId.CompareTo(other.accountId);
          else
            //if (slip != other.slip)
              return slip.CompareTo(other.slip);

    }
  }

}
