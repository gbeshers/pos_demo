﻿using System;
using System.Data;
using System.Text;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using CASL_engine;
using System.IO;
using System.Linq;

using System.Data.SqlClient;
using System.Diagnostics;

using System.Text.RegularExpressions;


namespace recon
{
  public partial class ReconcileDeposits
  {
    /// <summary>
    /// Bug 10250: FT
    /// Translate the DEPTID field from the TG_DEPFILE_DATA field to the 
    /// match key via the CBT Flex table.
    /// Note: This mapping may already be in CASL and it may not be necessary to
    /// directly read the database.  It would be something like this:
    /// <set>bank_reconcilation_key=Department.of."001".<get>'bank_reconcilation_key'</get></set>
    /// The CASL code that calls automatch could do a loop on the Department.of values and store Geo map
    /// of deptid verses bank_reconcilation_key.  See open_deposits_app for an example.
    /// </summary>
    /// <param name="originalDeptID"></param>
    /// <returns></returns>
    private string getWorkgroupMatchKey(GenericObject deptID2MatchKey, string originalDeptID)
    {
      // Try to get the match key.  If not found, use the original Dept ID.
      String matchKey = deptID2MatchKey.get(originalDeptID, originalDeptID) as String;
      return matchKey;
    }

    /// <summary>
    /// Bug 11302: FT: Added compound (comma-separated) match keys and took out bin'ing
    /// Look for compound match keys and split them out.  
    /// Returns a dictionary keyed on the card name and a value of the compound key.
    /// If a key is not compound, it ignores it.
    /// If there is a duplicate key, then TBD.
    /// Bank accounts should be also added to the mix, but this is TBD.
    /// </summary>
    /// <param name="deptID2MatchKey"></param>
    /// <returns>Dictionary with a key of individual card ID and a value of the compount match key</returns>
    //[Obsolete("extractAndHashCompoundKeys is deprecated; this did not work with overlapping match keys.", true)] 
    private Dictionary<string, string> extractAndHashCompoundKeys(GenericObject deptID2MatchKey)
    {
      // First get all the match keys 
      ArrayList keys = deptID2MatchKey.getStringKeys();
      Dictionary<string, string> compoundBins = new Dictionary<string, string>();

      // Loop through all the match keys
      foreach (string departmentId in keys)
      {
        // Get a potential compound key
        String compoundMatchKey = deptID2MatchKey.get(departmentId, departmentId) as String;
        // Ignore if not a cmpound key
        if (!compoundMatchKey.Contains(','))
          continue;

        var matchKeys = compoundMatchKey.Split(',');

        // Loop through again and create mapping
        foreach (string matchKey in matchKeys)
        {
          Logger.cs_log("extractAndHashCompoundKeys: match key: " + matchKey + " mapped to compound key: " + compoundMatchKey);
          compoundBins.Add(matchKey.Trim(), compoundMatchKey);
        }

      }
      Logger.cs_log("compoundBins: " + compoundBins);
      return compoundBins;
    }

    /// <summary>
    /// Get a list of bin'ed match keys / amount fo the Bank Deposits.
    /// Bug 10250: FT
    /// </summary>
    /// <param name="user"></param>
    /// <param name="recon_regex"></param>
    /// <param name="status"></param>
    /// <returns></returns>
    private Dictionary<String, MatchValue> getBankDepositMatchKeys(string user, string recon_regex, Dictionary<String, String> compoundMatchKeys, out Dictionary<int, string> bankID2matchKey, out ReconStatus status)
    {
      status = new ReconStatus(true);

      // If the match key is compound, key track of components
      bankID2matchKey = new Dictionary<int, string>();

      // Pair of match key against MatchValue 
      var matchKeyMap = new Dictionary<String, MatchValue>();

      // Setup regex pattern matcher here
      Regex regexPattern = new Regex(recon_regex);

      // Bug 10497: Pull the Account_Id so we can match against that also
      // Bug 11099: Simplified and optimized by removing subquery and using RECONCILED_STATUS instead
      String bankSql = @"SELECT BANK_DEPOSIT_ID, AMOUNT, TEXT, ACCOUNT_ID FROM TG_BANK_DEPOSIT_DATA 
                     WHERE STATUS != 'r'";

      SqlDataReader dataReader = null;
      try
      {
        dataReader = setupReader(bankSql, ref status);

        // There was an error in the SQL format but this was not set!?
        if (!status.success)
        {
          status.setErr("Automatch failed: Cannot read bank match key records",
             "Cannot query the TG_BANK_DEPOSIT_DATA table for the Reconciliation Key", "REC-172", true);
          return matchKeyMap;
        }
        while (dataReader.Read())
        {
          int bankDepositId = dataReader.GetInt32(0);
          Decimal amount = Convert.ToDecimal(dataReader.GetDouble(1));
          // Bug 11100: Prevent crash if null Text field
          String textField = dataReader.IsDBNull(2) ? "" : dataReader.GetString(2);
          Match match = regexPattern.Match(textField);
          if (!match.Success)
            continue;

          String nakedMatchKey = match.Groups[1].Value;

          // Keep track of which real key each deposit is with
          bankID2matchKey.Add(bankDepositId, nakedMatchKey);


          if (compoundMatchKeys.ContainsKey(nakedMatchKey))
            nakedMatchKey = compoundMatchKeys[nakedMatchKey];

          // Bug 10497: The bank account must be part of the automatch process even for match keys
          String accountId = dataReader.GetString(3);

          string matchKey = accountId + m_bankAccount_ref_delimiter + nakedMatchKey;
          // End bug 10497


          MatchValue matchValue;
          if (matchKeyMap.TryGetValue(matchKey, out matchValue))
          {
            matchValue.add(bankDepositId, amount);
            matchKeyMap[matchKey] = matchValue;
          }
          else
          {
            MatchValue newMatchValue = new MatchValue(bankDepositId, amount);
            matchKeyMap.Add(matchKey, newMatchValue);
          }

        }

      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in collate bank match key records", e);

        status.setErr("Automatch failed: Cannot collate bank match key records",
            String.Format("getLastReconciledRecord failed: Error: {0}: Traceback: {1}", e, e.StackTrace),
            "REC-197", false);
      }
      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
      }

      return matchKeyMap;
    }

    private Dictionary<String, MatchValue> getExpectedDepositMatchKeys(GenericObject deptID2MatchKey)
    {
      // Bug 11099: Simplified and optimized by removing subquery and using RECONCILED_STATUS instead
      // Bug #14729 FT - Don't include transferred deposits (where dfile.ACC_FILENBR is not null)
      String expectedSql =
        @"SELECT ddata.DEPOSITID, ddata.DEPOSITNBR, ddata.UNIQUEID, dfile.DEPTID, ddata.AMOUNT, ddata.BANKID
            FROM TG_DEPOSIT_DATA as ddata, TG_DEPFILE_DATA as dfile 
            WHERE isNull(ddata.RECONCILED_STATUS,'u') != 'r'            
            AND DEPOSITTYPE = 'FILE' AND VOIDDT is null
            AND ddata.DEPFILENBR = dfile.DEPFILENBR
            AND ddata.DEPFILESEQ = dfile.DEPFILESEQ
            AND ddata.POSTDT IS NOT NULL
            AND ddata.POSTDT >= @expectedStartDate 
            AND dfile.ACC_FILENBR is null
            {0}";

      String unReconcilableBankIDs = getUnreconcilableBankIDs("ddata.");
      expectedSql = String.Format(expectedSql, unReconcilableBankIDs);

      // Put in map for bin'ing 
      var matchKeyMap = new Dictionary<String, MatchValue>();

      SqlDataReader dataReader = null;

      try
      {
        ReconStatus status = new ReconStatus(true);

        // Bug 9934: FT: Query the CASL config property for the reconciliation_cutoff.
        DateTime expectedStartDate = getExpectedStartDate();

        dataReader = setupReader(expectedSql, ref status,
          param("expectedStartDate", SqlDbType.DateTime, expectedStartDate)
        );

        if (!status.success)
          return matchKeyMap;     // TODO: Do I need to do more here?

        while (dataReader.Read())
        {
          DatabaseID databaseID = new DatabaseID(
            dataReader.GetString(0), dataReader.GetInt32(1), dataReader.GetInt32(2)
            );
          String deptID = dataReader.GetString(3);
          Decimal amount = Convert.ToDecimal(dataReader.GetDouble(4));

          // Change workgroup ID to match key via CBT_Flex
          String workgroupMatchKey = getWorkgroupMatchKey(deptID2MatchKey, deptID);

          // Do not collect deposits with no match key
          if (workgroupMatchKey == null || workgroupMatchKey == "")
            continue;

          // Bug 10497: Bin amounts by Bank Account plus match key
          String bankId = dataReader.IsDBNull(5) ? "" : dataReader.GetString(5);
          bool reconcilable;
          String bankAccount = bankID2Account(bankId, out reconcilable, ref status);
          // If the Reconcilable configuration variable is false, ignore this record
          if (!reconcilable)
            continue;

          workgroupMatchKey = bankAccount + m_bankAccount_ref_delimiter + workgroupMatchKey;
          // End 10497

          MatchValue matchValue;
          if (matchKeyMap.TryGetValue(workgroupMatchKey, out matchValue))
          {
            matchValue.add(databaseID, amount);
            matchKeyMap[workgroupMatchKey] = matchValue;
          }
          else
          {
            MatchValue newMatchValue = new MatchValue(databaseID, amount);
            matchKeyMap.Add(workgroupMatchKey, newMatchValue);
          }

        }
        return matchKeyMap;
      }
      finally
      {
        if (dataReader != null)
          dataReader.Close();
      }
    }

    /// <summary>
    /// Bug 10250: FT: Pull the match key from the DEPFILE table based on the DEPOSIT table key.
    /// First retrieve the DEPTID from the TG_DEPFILE_DATA table
    /// by joining with the TG_DEPOSIT_DATA table.
    /// Then tranlate this to the match key via the CBT_Flex table.
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    private String getExpectedMatchKey(DatabaseID id, GenericObject deptId2matchKey)
    {
      String deptsIDSql =
        @"SELECT dfile.DEPTID
            FROM TG_DEPOSIT_DATA as ddata, TG_DEPFILE_DATA as dfile 
            WHERE 
              ddata.DEPOSITID = @depositID
              AND ddata.DEPOSITNBR = @depositNbr
              AND ddata.UNIQUEID = @uniqueID
              AND ddata.DEPFILENBR = dfile.DEPFILENBR
              AND ddata.DEPFILESEQ = dfile.DEPFILESEQ";

      String depositId;
      int depositNbr, uniqueId;
      ReconStatus status = new ReconStatus(true);
      id.parseExpectedKey(out depositId, out depositNbr, out uniqueId, ref status);
      if (!status.success)
      {
        // Unlikely....
        Logger.cs_log("getExpectedMatchKey: Internal error: cannot parse DatabaseID: " + id.ToString());
        return "";
      }

      SqlDataReader dataReader = null;
      try
      {
        dataReader = setupReader(deptsIDSql, ref status,
          param("depositID", SqlDbType.VarChar, depositId),
          param("depositNbr", SqlDbType.Int, depositNbr),
          param("uniqueId", SqlDbType.Int, uniqueId)
          );

        if (!status.success)
        {
          status.detailedError = status.detailedError +
              "Cannot read details from TG_DEPOSIT_DATA table for id: " + id.databaseKey;
          return "";
        }


        if (!dataReader.HasRows)
        {
          // TODO: figure out a better way to communicate error conditions to the user...
          String msg = "Cannot read DeptID for this expected deposit: DatabaseID: " + id.ToString();
          Logger.cs_log(msg);
          return "";
        }

        dataReader.Read();

        String deptID = dataReader.GetString(0);
        if (deptId2matchKey == null)
          return getWorkgroupMatchKeyFromCBTFlex(deptID);   // Bug 11467
        else
          return getWorkgroupMatchKey(deptId2matchKey, deptID);
      }
      finally
      {
        if (dataReader != null)
          dataReader.Close();
      }
    }

    /// <summary>
    /// Get the match key for an expected deposit directly querying the CBT_Flex table.
    /// Bug 11467
    /// </summary>
    /// <param name="deptID"></param>
    /// <returns></returns>
    private string getWorkgroupMatchKeyFromCBTFlex(string deptID)
    {
      /*
        SELECT field_value from CBT_FLEX 
          WHERE container IN (SELECT container FROM CBT_FLEX 
            WHERE field_key = '"id"' 
              AND field_value = '"001"'
		          AND container LIKE 'Business.Department.of.%')
          AND field_key = '"bank_reconcilation_key"'
       */

            SqlDataReader dataReader = null;

      String flexStr =
          @"SELECT field_value from CBT_FLEX 
	            WHERE container IN (SELECT container FROM CBT_FLEX 
							                    WHERE field_key = '""id""' 
							                    AND field_value = @fieldValue
                                  AND container LIKE 'Business.Department.of.%'
                                  )
	            AND field_key = '""bank_reconcilation_key""'";

      ReconStatus status = new ReconStatus(true);
      var bankIDs = new List<string>();
      try
      {
        dataReader = setupReader(flexStr, true, ref status,
             param("fieldValue", SqlDbType.VarChar, addQuotes(deptID))
          );

        while (dataReader.Read())
        {
          string matchKey = dataReader.GetString(0);
          matchKey = stripQuotes(matchKey);
          return matchKey;
        }

      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in retrieving match key from expected deposit table", e);

        status.setWarning("Error retrieving match key from expected deposit table: " +
            String.Format("getWorkgroupMatchKeyFromCBTFlex failed: SQL or conversion error: Error: {0}: Traceback: {1}",
                e, e.StackTrace),
            "REC-200");
      }
      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
      }
      return "";
    }

    /// <summary>
    /// Bug 10250: FT: Added a secondary auto match pass.
    /// Match based on the match key in the expected and bank deposits.
    /// 
    /// The match key in the expected is stored in the DEPTID in the TG_DEPFILE_DATA
    /// but this must be translated via the CBT flex table to a specific match key
    /// specified by the user.  If there is no CBT Flex entry, the DEPTID is used
    /// instead.
    /// 
    /// The match key in the bank deposit data is embedded in the "Text" field in the BAI 
    /// input fields.  The match key is extracted from the Text field with the recon_regex pattern
    /// set in the bank rec configuration page.
    /// 
    /// Once these two lists of match keys are compiled, they are compared.  First a match on the match
    /// key is attemted.  If this succeeds, the total amounts are compared.  If they match, then the 
    /// automatch takes place.
    /// 
    /// </summary>
    /// <param name="user"></param>
    /// <param name="recon_regex"></param>
    /// <param name="reconciled"></param>
    /// <param name="status"></param>
    [Obsolete("Use autoMatchBankReconKeyDateBinned() instead", true)] 
    private void autoMatchBankReconKey(string user, GenericObject deptID2MatchKey, string recon_regex, List<ReconciledDeposit> reconciled, AutomatchReports reporter, out ReconStatus status)
    {
      status = new ReconStatus(true);

      // Only do this match if there is a Regular Express in the config
      if (recon_regex == null || recon_regex.Length == 0)
        return;

      // Looks for compound match keys and split them out so getBankDepositMatchKeys can bin
      Dictionary<String, String> compoundMatchKeys = extractAndHashCompoundKeys(deptID2MatchKey);

      // Start off and get a map of the match keys against the amount in the Bank Deposits
      Dictionary<int, string> bankID2matchKey;
      Dictionary<String, MatchValue> bankDepositMap = getBankDepositMatchKeys(user, recon_regex, compoundMatchKeys, out bankID2matchKey, out status);
      if (!status.success)
        return;

      // Bug 10262: Do not bail out but rather let the loop 
      // run its course so we can report on the number of unmatched expected records
      // If there are no unreconciled bank deposits, return
      //if (bankDepositMap.Count == 0)
      //  return;

      // Now get a list of the ref / amount pairs for expected deposits
      Dictionary<String, MatchValue> expectedDepositsMap = getExpectedDepositMatchKeys(deptID2MatchKey);

      // Bail out if there are no unreconciled deposits
      if (expectedDepositsMap.Count == 0)
        return;

      // Now we have two bin'ed lists of MatchKey / amount tuples.
      // One list for Expected Deposits and one for Bank Deposits.
      // Match the two lists.  
      // I considered Generic Linq Intersect but it is not right for this.
      // First loop through the expected deposits; 
      // might want to loop on bank deposits instead for performance reasons
      foreach (String expectedMatchKey in expectedDepositsMap.Keys)
      {
        // Extract the bank ID and match key.  Bug 10497
        string[] matchkeys = expectedMatchKey.Split(m_bankAccount_ref_delimiter);
        string matchedAccountId = matchkeys[0];
        string matchedMatchKey = matchkeys[1];

        // Next get the expected amount. Bug 10262: moved here so we will have an amount for logging
        MatchValue expectedDepositValue;
        expectedDepositsMap.TryGetValue(expectedMatchKey, out expectedDepositValue);
        Decimal expectedAmount = expectedDepositValue.totalAmount;

        // First see if the expected MatchKey is also in the bank deposit map
        MatchValue bankDepositValue;
        if (bankDepositMap.TryGetValue(expectedMatchKey, out bankDepositValue))
        {
          // OK, found a match of the match keys; compare amounts.
          // First get the bank amount
          Decimal bankAmount = bankDepositValue.totalAmount;


          // If amounts are the same then there is a match. Auto-reconcile them!
          if (bankAmount == expectedAmount)
          {
            // The nice thing is that the bank deposit ID are already stored
            List<DatabaseID> expectedIDs = expectedDepositValue.bankDepositIds;
            List<DatabaseID> bankIDsFullList = bankDepositValue.bankDepositIds;
            Dictionary<string, List<DatabaseID>> bankIDBins = binBankIDsByMatchKey(bankIDsFullList, bankID2matchKey);

            foreach (List<DatabaseID> bankIDs in bankIDBins.Values)
            {

            reconcileAutoMatchKey(expectedIDs, bankIDs, user, out status);
            // Bail out immediatly if there is an error.
            if (!status.success)
            {
              // Bug 10262
              // Bug 11169: Changed to write each line to the file
              reporter.textReport("Cannot reconcile automatched records. Error: ", status.detailedError,
                " Account: ", matchedAccountId,
                " key: ", matchedMatchKey,
                " amount: ", expectedAmount);
              reporter.matchReport(AutomatchReports.MatchAction.error, matchedAccountId, matchedMatchKey, expectedAmount, "Cannot reconciled automatched records: " + status.detailedError);
              continue;
            }

            ReconciledDeposit recon = getLastReconciledRecord(out status);
            // If there is an error here, then the reconcile() probably failed.
            if (!status.success)
            {
              // Bug 10262
              // Bug 11169: Changed to write each line to the file
              reporter.textReport("Record matched with key, but get last record failed. Error: ", status.detailedError,
                " Account: ", matchedAccountId,
                " key: ", matchedMatchKey,
                " amount: ", expectedAmount);
              reporter.matchReport(AutomatchReports.MatchAction.error, matchedAccountId, matchedMatchKey, expectedAmount, "Record matched with key, but get last record failed: " + status.detailedError);
              continue;
            }

            // Found a match.  Bug 10262: Log action
            // Bug 11169: Changed to write each line to the file
            reporter.textReport("Matched: Account: ", matchedAccountId, " / key: ", matchedMatchKey,
              " total amount: ", String.Format("{0:C}", expectedAmount));
            reporter.matchReport(AutomatchReports.MatchAction.match_key, matchedAccountId, matchedMatchKey, bankAmount, "");
            reconciled.Add(recon);

            // Bug 10262: Prune bank deposit map so we will have a list of leftovers at the end
            bankDepositMap.Remove(expectedMatchKey);
          }
          }
          else
          {
            // Bug 10262
            // Bug 11169: Changed to write each line to the file
            reporter.textReport("No match: key: expected account: ", matchedAccountId,
              " key: ", matchedMatchKey,
              " matches but the total expected amount: ", String.Format("{0:C}", expectedAmount),
              " does not match total bank account: ", bankAmount);
            reporter.matchReport(AutomatchReports.MatchAction.no_match, matchedAccountId, matchedMatchKey, expectedAmount, "Matched key but bank total amount: " + bankAmount + " does not match");

          }
        }
        else
        {
          // Bug 10262: Report on missed expected matches
          // Bug 11169: Changed to write each line to the file
          reporter.textReport("No match: key: expected account: ", matchedAccountId,
            " key: ", matchedMatchKey,
            " amount: ", String.Format("{0:C}", expectedAmount),
            " did not match any Bank Deposits");
          reporter.matchReport(AutomatchReports.MatchAction.no_match, matchedAccountId, matchedMatchKey, expectedAmount, "Expected Account / Reconciliation key not found in bank deposit table");

          // This match key not found in bank deposits; skip to next
          continue;
        }
      }

      // Bug 10262: Report on bank deposit match keys that were not matched
      foreach (String matchKey in bankDepositMap.Keys)
      {
        string[] matchKeys = matchKey.Split(m_bankAccount_ref_delimiter);
        MatchValue matchValue = bankDepositMap[matchKey];
        // Bug 11169: Changed to write each line to the file
        reporter.textReport("No match: key: bank account: ", matchKeys[0],
          " key: ", matchKeys[1],
          " amount: ", String.Format("{0:C}", matchValue.totalAmount),
          " did not match any expected deposit records");
        reporter.matchReport(AutomatchReports.MatchAction.no_match, matchKeys[0], matchKeys[1], matchValue.totalAmount, "Account / key not found in expected table");
      }

    }

    /// <summary>
    /// Bin the Bank Database ID list and return a hash of
    /// the matchkeys as the keys and a list of bank database IDs as the values.
    /// Remarkably enough, this is the database IDs (i.e. key fields of TG_BANK_DEPOSIT_DATA records), 
    /// not the bank IDs.  
    /// </summary>
    /// <param name="bankIDs">List of all the bank database IDs</param>
    /// <param name="bankID2matchKey">Mapping between Bank database ID (key) and match key</param>
    /// <returns>A dictionary with the key as a match key and the value as a list of Bank Database IDs (keys)</returns>
    private Dictionary<string, List<DatabaseID>> binBankIDsByMatchKey(List<DatabaseID> bankIDs, Dictionary<int, string> bankID2matchKey)
    {
      Dictionary<string, List<DatabaseID>> matchKeyMap = new Dictionary<string, List<DatabaseID>>();

      foreach (DatabaseID databaseID in bankIDs)
      {
        int bankId = Convert.ToInt32(databaseID.databaseKey);
        string matchKey = bankID2matchKey[bankId];
        if (matchKeyMap.ContainsKey((string)matchKey))
        {
          List<DatabaseID> ids = matchKeyMap[matchKey];
          ids.Add(databaseID);
        }
        else
        {
          List<DatabaseID> ids = new List<DatabaseID>();
          ids.Add(databaseID);
          matchKeyMap.Add(matchKey, ids);
        }
      }
      return matchKeyMap;
    }




  }
}
