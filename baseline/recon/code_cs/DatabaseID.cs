﻿using System;
using System.Collections.Generic;
using System.Text;
using CASL_engine;

namespace recon
{
  public class DatabaseID
  {
    /// <summary>
    /// Opaque database key field(s) value. 
    /// TODO: change this to a map for compound key
    /// </summary>
    public String databaseKey { get; set; }

    public DatabaseID(String key)
    {
      this.databaseKey = key;
    }

    public DatabaseID(object key)
    {
      int numberKey = (int)key;
      this.databaseKey = Convert.ToString(key);
    }

    /// <summary>
    /// Ctor for TG_DEPOSIT_DATA compound key.  
    /// This key will probably not be needed but is
    /// here just in case.
    /// </summary>
    /// <param name="key"></param>
    public DatabaseID(object depositId, object depositNbr, object uniqueId)
    {
      this.databaseKey = String.Format("{0}|{1}|{2}", depositId, depositNbr, uniqueId);
    }

    /// <summary>
    /// Take the database key for the TG_DEPOSIT_DATA table and parse out fields.
    /// Reverse what was done in the DatabaseID(object, object, object) ctor.
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="depositId"></param>
    /// <param name="depositNbr"></param>
    /// <param name="uniqueID"></param>
    public void parseExpectedKey(out String depositId, out int depositNbr, out int uniqueID, ref ReconStatus status)
    {
      status.success = true;
      try
      {
        String id = this.databaseKey;
        String[] keys = id.Split('|');
        depositId = keys[0];
        // Bug #12055 Mike O - Use misc.Parse to log errors
        depositNbr = misc.Parse<Int32>(keys[1]);
        uniqueID = misc.Parse<Int32>(keys[2]);
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in parseExpectedKey", e);

        String msg = String.Format("Internal error: the expected deposit ID: '{0}' is not in the correct format",
          databaseKey);
        depositId = "";
        depositNbr = 0;
        uniqueID = 0;
        status.setErr(msg, 
          String.Format("parseExpectedKey(): {0}: Error: {1}", msg, e),
          "REC-174", false);
      }
    }

    /// <summary>
    /// Override the .Equals.  Avoid C++ style operator overloads! 
    /// </summary>
    /// <param name="idObject"></param>
    /// <returns></returns>
    public override bool Equals(object idObject)
    {
      DatabaseID id = (DatabaseID)idObject;
      return id.databaseKey.Equals(databaseKey);
    }

    public override int GetHashCode()
    {
      return databaseKey.GetHashCode();
    }
  }
}
