﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using CASL_engine;


namespace recon
{
  class GroupHealthParser : Parser
  {
    private int m_lineCounter = 0;
    private int m_fieldCounter = 0;
    private String m_currentField = "";


    public override List<TransactionRecord> parse(String path, string BankAccount, bool checkLineNumbers, out ReconStatus status)
    {
      status = new ReconStatus(true);

      var mappings = new List<KeyValuePair<String, int>>();

      var transactionList = new List<TransactionRecord>();
      System.IO.StreamReader file = null;
      m_lineCounter = 0;
      m_fieldCounter = 0;
      m_currentField = "";
      try
      {
        String line;
        file = new System.IO.StreamReader(path);
        while ((line = file.ReadLine()) != null)
        {
          if (m_lineCounter++ == 0)
          {
            createColumnMappings(mappings, line);
            continue;
          }
          // Skip blank lines
          if (line.Trim().Length == 0)
            continue;
          TransactionRecord transaction = readColumns(mappings, line);
          transactionList.Add(transaction);
        }


      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in  parse bank file", e);

        FileInfo fileInfo = new FileInfo(path);
        // Bug 11080
        status.setErr(
            String.Format("CSV Parser: Cannot parse bank file: {4}: '{0}': Error on line: {1}  field: {2}  field value: '{3}' ",
                fileInfo.Name, m_lineCounter, m_fieldCounter, m_currentField, e.Message),
            String.Format("CSV Parser: Cannot parse bank file: '{0}' line: {1} field: {2}: field value: '{3}' Exception: {4}",
                fileInfo.FullName, m_lineCounter, m_fieldCounter, m_currentField, e.ToString()),
            "REC-143", true);
      }
      finally
      {
        if (file != null)
          file.Close();
      }
      return transactionList;
    }

    private TransactionRecord readColumns(List<KeyValuePair<string, int>> mappings, String line)
    {
      var transaction = new TransactionRecord();

      String[] columns = line.Split(',');
      foreach (KeyValuePair<String, int> kvp in mappings)
      {
        m_fieldCounter = kvp.Value;
        switch (kvp.Key)
        {
          case "As Of":
            m_currentField = readColumn(columns, kvp.Value);
            // Bug #12055 Mike O - Use misc.Parse to log errors
            transaction.date = misc.Parse<DateTime>(m_currentField);
            break;
          case "Account":
            m_currentField = readColumn(columns, kvp.Value);
            // Bug 25498: The account number can come in as a scientific notation such as this: 7.61221E+12.
            double account = misc.Parse<double>(m_currentField);
            transaction.account = account.ToString();
            // End Bug 25498
            break;
          case "BAI code":
            m_currentField = readColumn(columns, kvp.Value);
            // Bug #12055 Mike O - Use misc.Parse to log errors
            transaction.baiCode = misc.Parse<Int32>(m_currentField);
            break;
          case "Amount":
            m_currentField = readColumn(columns, kvp.Value);
            // Bug #12055 Mike O - Use misc.Parse to log errors
            transaction.amount = misc.Parse<Decimal>(m_currentField);
            break;
          case "Bank Reference":
            m_currentField = readColumn(columns, kvp.Value);
            if (string.IsNullOrWhiteSpace(m_currentField))
              continue;         // Skip blank lines
            double bankRef = misc.Parse<double>(m_currentField);    // Bug 25498
            transaction.bankRefNum = bankRef.ToString();
            break;
          case "Customer Reference":
            m_currentField = readColumn(columns, kvp.Value);
            double custRef = misc.Parse<double>(m_currentField);    // Bug 25498
            transaction.customerRefNum = custRef.ToString();
            break;
          case "Text":
            m_currentField = readColumn(columns, kvp.Value);
            transaction.text = m_currentField;
            break;
          default:
            throw new Exception("CSV parser failure: unexpected column name: " + kvp.Key);
        }
      }
      return transaction;
    }

    private String readColumn(String[] columns, int index)
    {
      // Handle truncated lines (missing fields at the end)
      if (index >= columns.Length)
        return "";

      return columns[index];
    }



    private void createColumnMappings(List<KeyValuePair<string, int>> mappings, String line)
    {
      String[] headings = line.Split(',');
      for (int i = 0; i < headings.Length; i++)
      {
        String heading = headings[i];
        KeyValuePair<String, int> kvp;
        switch (heading)
        {
          case "As Of":
          case "AS OF":
            kvp = new KeyValuePair<String, int>("As Of", i);
            mappings.Add(kvp);
            break;
          case "Account":
          case "ACCOUNT":
            kvp = new KeyValuePair<String, int>("Account", i);
            mappings.Add(kvp);
            break;
          case "BAI Code":
          case "BAI CODE":
            kvp = new KeyValuePair<String, int>("BAI code", i);
            mappings.Add(kvp);
            break;
          case "Amount":
          case "AMOUNT":
            kvp = new KeyValuePair<String, int>("Amount", i);
            mappings.Add(kvp);
            break;
          case "Bank Reference":
          case "BANK REFERENCE":
            kvp = new KeyValuePair<String, int>("Bank Reference", i);
            mappings.Add(kvp);
            break;
          case "Customer Reference":
          case "CUSTOMER REFERENCE":
            kvp = new KeyValuePair<String, int>("Customer Reference", i);
            mappings.Add(kvp);
            break;
          case "Text":
          case "TEXT":
            kvp = new KeyValuePair<String, int>("Text", i);
            mappings.Add(kvp);
            break;
          default:
            // Ignore the other columns
            break;
        }
      }
      // Bug 11080: Improve error message
      int numberOfMatches = mappings.Count;
      if (numberOfMatches == 0)
        throw new Exception("CSV Parser: Input file is not in CSV Header format");
      if (numberOfMatches < 7)
        throw new Exception("CSV Parser: Input file header error.  This may not be a CSV Header file.");
      // End 11080
    }
  }
}



