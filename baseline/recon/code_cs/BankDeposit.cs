﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Text;

namespace recon
{
  /// <summary>
  /// Record to hold bank deposit fields that are needed by the GUI.
  /// Details such as tender totals may be subclassed off this
  /// </summary>
  public class BankDeposit
  {
    // Database fields
    public DateTime depDate { get; set; }
    public String refNum { get; set; }
    public Decimal amount { get; set; }
    public String account { get; set; }

    // Metadata
    public DatabaseID databaseID { get; set; }
    public int age { get; set; }

    public enum bStatus { newDeposit, unreconciled, reconciled };
    public bStatus depositStatus { get; set; }

    public String bStatusToString(bStatus status)
    {
      switch (status)
      {
        case bStatus.newDeposit:
          return "newDeposit";
        case bStatus.unreconciled:
          return "unreconciled";
        case bStatus.reconciled:
          return "reconciled";
        default:
          return "unset";
      }
    }

    public static BankDeposit.bStatus convertStatus(object status)
    {
      switch ((String)status)
      {
        case "n":
          return BankDeposit.bStatus.newDeposit;
        case "u":
          return BankDeposit.bStatus.unreconciled;
        case "r":
          return BankDeposit.bStatus.reconciled;
        default:
          throw new Exception("Unrecognized Bank Deposit status: " + status);
      }
    }

    public static String convertStatus(BankDeposit.bStatus status)
    {
      switch (status)
      {
        case BankDeposit.bStatus.newDeposit:
          return "n";
        case BankDeposit.bStatus.unreconciled:
          return "u";
        case BankDeposit.bStatus.reconciled:
          return "r";
        default:
          throw new Exception("Unrecognized Bank Deposit status: " + status);
      }
    }


  }
}