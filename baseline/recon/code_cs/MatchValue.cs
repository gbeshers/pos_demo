﻿
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Text;

namespace recon
{
  /// <summary>
  /// Bug 10236: FT
  /// Object to hold deposit identifier / amount tuples that are needed for 
  /// the second-pass, match-key automatch.
  /// </summary>
  public class MatchValue
  {
    public List<DatabaseID> bankDepositIds { get; set; }
    public Decimal totalAmount { get; set; }

    // For debugging only
    public string matchDay { get; set; }

    public MatchValue()
    {
      bankDepositIds = new List<DatabaseID>();
    }

    public MatchValue(int bankDepositId, Decimal amount)
    {
      bankDepositIds = new List<DatabaseID>();
      var databaseID = new DatabaseID(bankDepositId);
      bankDepositIds.Add(databaseID);
      totalAmount = amount;
    }

    public MatchValue(DatabaseID databaseID, Decimal amount)
    {
      bankDepositIds = new List<DatabaseID>();
      bankDepositIds.Add(databaseID);
      totalAmount = amount;
    }


    /// <summary>
    /// Add in a Bank Reconciliation ID
    /// </summary>
    /// <param name="bankDepositId"></param>
    /// <param name="amount"></param>
    public void add(int bankDepositId, Decimal amount)
    {
      var databaseID = new DatabaseID(bankDepositId);
      bankDepositIds.Add(databaseID);
      totalAmount += amount;
    }

    
    /// <summary>
    /// For storing Expected Deposit IDs
    /// </summary>
    /// <param name="expectedID"></param>
    /// <param name="amount"></param>
    public void add(DatabaseID expectedID, Decimal amount)
    {
      bankDepositIds.Add(expectedID);
      totalAmount += amount;
    }

    /// <summary>
    /// Override the .Equals.  Avoid C++ style operator overloads! 
    /// </summary>
    /// <param name="idObject"></param>
    /// <returns></returns>
    /// 
    public override bool Equals(object MatchValue)
    {
      MatchValue matchValue = (MatchValue) MatchValue;
      return matchValue.totalAmount.Equals(totalAmount);
    }

    /// <summary>
    /// A rough (and slow) attempt at a hashcode.  This will return the same
    /// code for objects with the same data.
    /// </summary>
    /// <returns></returns>
    public override int GetHashCode()
    {
      var builder = new StringBuilder();
      builder.Append(totalAmount);
      foreach (DatabaseID databaseID in bankDepositIds)
        builder.Append(databaseID);
      return builder.ToString().GetHashCode();
    }

  }
}
