﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Collections.Generic;
using System.Text;

namespace recon
{
  /// <summary>
  /// Record to iPayment Expected deposit fields that are needed by the GUI.
  /// Details may be subclassed off this
  /// </summary>
  public class ExpectedDeposit
  {
    // Database fields
    public DateTime depDate { get; set; }
    public String refNum { get; set; }
    public Decimal amount { get; set; }
    public String account { get; set; }
    public String file { get; set; }
    public String workgroup { get; set; }    // Bug 15157

    // Metadata
    public DatabaseID databaseID { get; set; }
    public int age { get; set; }
  }
}

