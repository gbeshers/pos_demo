﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using CASL_engine;

namespace recon
{
  /// <summary>
  /// Record for reconciled fields needed by the GUI.  
  /// The detail record will subclass this.
  /// </summary>
  public class ReconciledDeposit
  {
    // Metadata fields
    /// <summary>
    /// Opaque database handle.  This is the key field
    /// </summary>
    public DatabaseID databaseID { get; set; }

    public DateTime reconDate { get; set; }

    public String user { get; set; }

    // From bank table
    public String refNum { get; set; }

    // From bank table
    public Decimal amount { get; set; }

    public Decimal overShort { get; set; }

    public String reason { get; set; }    // Justification for forced or ignored recon

    public String checksum { get; set; }   // Bug 9808: Added checksum fields

    public string file { get; set; }  // Bug 15157.  FT For reporting

    public string workgroup { get; set; }  // Bug 15157.  FT For reporting

    override public string ToString()
    {
      var sb = new StringBuilder();
      sb.Append("ID: ").Append(databaseID.databaseKey).Append("\n")
        .Append("Date: ").Append(reconDate).Append("\n")
        .Append("User: ").Append(user).Append("\n")
        .Append("Ref: ").Append(refNum).Append("\n")
        .Append("Amount: ").Append(String.Format("{0:C}", amount)).Append("\n")
        .Append("Overshort: ").Append(String.Format("{0:C}", overShort)).Append("\n")
        .Append("Status: ").Append(rStatusToString(reconStatus)).Append("\n")
        .Append("Type: ").Append(convertReconType(reconType));
      if (reason.Length > 0)
        sb.Append("\n").Append("reason: ").Append(reason);
      return sb.ToString();
    }

    /// <summary>
    /// Output a compressed version of ToString() suitable for stuffing in 
    /// the DETAIL field of the activity log.
    /// Bug 12482.
    /// </summary>
    /// <returns>String version record in compressed form</returns>
    public string ToAuditString()
    {
      var sb = new StringBuilder();
      sb.Append(databaseID.databaseKey).Append(':')
        .Append(reconDate).Append(':')
        .Append(user).Append(':')
        .Append(refNum).Append(':')
        .Append(amount).Append(':')
        .Append(overShort).Append(':')
        .Append(convertReconStatus(reconStatus)).Append(':')
        .Append(convertReconType(reconType));
      if (reason.Length > 0)
        sb.Append(':').Append(reason);
      return sb.ToString();
    }

    // Reconciliation status values 
    public enum rStatus { unset, reconciled, voided, error };
    /// <summary>
    /// The status of the reconcilation. 
    /// It is not clear whether voided reconcilations should be returned via the API.
    /// For now they will be.
    /// </summary>
    public rStatus reconStatus { get; set; }

    public String rStatusToString(rStatus status)
    {
      switch (status)
      {
        case rStatus.reconciled:
          return "reconciled";
        case rStatus.voided:
          return "voided";
        default:
          return "unset";
      }
    }

    public static ReconciledDeposit.rStatus convertReconStatus(object code)
    {
      switch ((String)code)
      {
        case "r":
          return ReconciledDeposit.rStatus.reconciled;
        case "v":
          return ReconciledDeposit.rStatus.voided;
        default:
          return ReconciledDeposit.rStatus.error;
      }
    }

    public static String convertReconStatus(ReconciledDeposit.rStatus code)
    {
      switch (code)
      {
        case ReconciledDeposit.rStatus.reconciled:
          return "r";
        case ReconciledDeposit.rStatus.voided:
          return "v";
        case ReconciledDeposit.rStatus.unset:
          return "x";
        default:
          throw new Exception("convertReconStatus Unrecognized reconciliation status: " + code);
      }
    }

    // Reconciliation type values 
    public enum rType { unset, auto, manual, forced, ignored, error, matchkey };  // Bug 10324: Added matchKey
    public rType reconType { get; set; }
    public static String convertReconType(ReconciledDeposit.rType code)
    {
      switch (code)
      {
        case ReconciledDeposit.rType.auto:
          return "a";
        case ReconciledDeposit.rType.manual:
          return "m";
        case ReconciledDeposit.rType.forced:
          return "f";
        case ReconciledDeposit.rType.ignored:
          return "i";
        case ReconciledDeposit.rType.unset:
          return "x";
        case ReconciledDeposit.rType.matchkey:    // Bug 10324
          return "k";
        default:
          throw new Exception(
              String.Format("ERROR: unrecognized reconcilation type {0}: cannot reconcile transaction",
                  code));
      }
    }

    public static ReconciledDeposit.rType convertReconType(object code)
    {
      switch ((String)code)
      {
        case "a":
          return ReconciledDeposit.rType.auto;
        case "m":
          return ReconciledDeposit.rType.manual;
        case "f":
          return ReconciledDeposit.rType.forced;
        case "i":
          return ReconciledDeposit.rType.ignored;
        case "k":                                     // Bug 10324
          return ReconciledDeposit.rType.matchkey;
        default:
          return ReconciledDeposit.rType.error;
      }
    }

    public String rTypeToString(rType type)
    {
      switch (type)
      {
        case rType.auto:
          return "auto";
        case rType.forced:
          return "forced";
        case rType.ignored:
          return "ignored";
        case rType.manual:
          return "manual";
        case rType.unset:
          return "unset";
        case rType.error:
          return "error";
        case rType.matchkey:          // Bug 10324
          return "match_key"; 
        default:
          return "unknown";
      }
    }

    /// <summary>
    /// Compute and validate checksums with this utility method.
    /// Bug 9808: FT: Compute / validate checksums for the TG_RECONCILED_DATA table here.
    /// </summary>
    /// <returns></returns>
    private String computeChecksumString()
    {

      // Format decimal uniformly with .00 extension.  
      // StringBuilder.Append does variable things with .00 extensions
      // leading to mysterious checksum errors.
      String amountStr = String.Format("{0:0.00}", this.amount);
      String overshortStr = String.Format("{0:0.00}", this.overShort);

      // Also be very careful of dates:
      // Use the standardized "ShortDate+LongTime" format 0:G (M/d/yyyy h:mm:ss tt)
      String dateStr = String.Format("{0:G}", this.reconDate);

      StringBuilder checksumBuilder = new StringBuilder();

      checksumBuilder
      .Append(dateStr)
      .Append(this.user)
      .Append(convertReconStatus(this.reconStatus))
      .Append(convertReconType(this.reconType))
      .Append(amountStr)
      .Append(this.reason)
      .Append(overshortStr);

      return checksumBuilder.ToString();
    }

    public void computeChecksum()
    {
      String checksumString = computeChecksumString();
      this.checksum = Crypto.ComputeHash("data", checksumString);
    }

    public Boolean validateChecksum()
    {
      String checksumString = computeChecksumString();
      // Bug 11095: Took out the unneeded ToString();
      String recomputedChecksum = Crypto.ComputeHash("data", checksumString);
      return this.checksum.Equals(recomputedChecksum);
    }

  }
}
