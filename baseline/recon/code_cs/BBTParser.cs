﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using CASL_engine;


namespace recon
{
  class BBTParser : Parser
  {
    private int m_lineCounter = 0;
    private int m_fieldCounter = 0;
    private String m_currentField = "";

    /// <summary>
    /// Override the parser for BBT.
    /// Note how the BankAccount parameter was added, but only used in this parser so far.
    /// Bug 17179.
    /// </summary>
    /// <param name="path"></param>
    /// <param name="bankAccount"></param>
    /// <param name="checkLineNumbers"></param>
    /// <param name="status"></param>
    /// <returns></returns>
    public override List<TransactionRecord> parse(String path, string bankAccount, bool checkLineNumbers, out ReconStatus status)
    {
      status = new ReconStatus(true);

      var mappings = new List<KeyValuePair<String, int>>();

      var transactionList = new List<TransactionRecord>();
      System.IO.StreamReader file = null;
      m_lineCounter = 0;
      m_fieldCounter = 0;
      m_currentField = "";
      try
      {

        String line;
        file = new System.IO.StreamReader(path);
        while ((line = file.ReadLine()) != null)
        {
          m_lineCounter++;
          if (m_lineCounter == 1)
          {
            createColumnMappings(mappings, line);
            continue;
          }
          // Skip blank lines
          if (line.Trim().Length == 0)
            continue;
          bool blankLine;
          TransactionRecord transaction = readColumns(mappings, line, bankAccount, out blankLine);
          if (blankLine)
            continue;
          transactionList.Add(transaction);
        }


      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in Parse", e);

        FileInfo fileInfo = new FileInfo(path);
        // Bug 11080
        status.setErr(
            String.Format("BBT CSV Parser: Cannot parse bank file: {4}: '{0}': Error on line: {1}  field: {2}  field value: '{3}' ",
                fileInfo.Name, m_lineCounter, m_fieldCounter, m_currentField, e.Message),
            String.Format("BBT CSV Parser: Cannot parse bank file: '{0}' line: {1} field: {2}: field value: '{3}' Exception: {4}",
                fileInfo.FullName, m_lineCounter, m_fieldCounter, m_currentField, e.ToString()),
            "REC-143", true);
      }
      finally
      {
        if (file != null)
          file.Close();
      }
      return transactionList;
    }

    private TransactionRecord readColumns(List<KeyValuePair<string, int>> mappings, String line, string bankAccount, out bool emptyRecord)
    {
      var transaction = new TransactionRecord();
      transaction.account = bankAccount;          // bank account is set at the Configuration level

      bool amountSet = false;
      bool dateSet = false;
      bool depositSlipNumberSet = false;

      String[] columns = line.Split(',');
      foreach (KeyValuePair<String, int> kvp in mappings)
      {
        m_fieldCounter = kvp.Value;
        switch (kvp.Key)
        {
          case "As Of":
            m_currentField = readColumn(columns, kvp.Value);
            if (string.IsNullOrEmpty(m_currentField))
              continue;         // Skip blank lines
            // Bug #12055 Mike O - Use misc.Parse to log errors
            transaction.date = misc.Parse<DateTime>(m_currentField);
            dateSet = true;
            break;

          case "Account":
            m_currentField = readColumn(columns, kvp.Value);
            if (string.IsNullOrEmpty(m_currentField))
              continue;         // Skip blank lines
            // Bug #12055 Mike O - Use misc.Parse to log errors
            transaction.account = m_currentField;
            break;

          case "Amount":
            m_currentField = readColumn(columns, kvp.Value);
            if (string.IsNullOrEmpty(m_currentField))
              continue;         // Skip blank lines

            // Bug #12055 Mike O - Use misc.Parse to log errors
            transaction.amount = misc.Parse<Decimal>(m_currentField.Trim('"'));     // Bug 19556: Get rid of enclosing quotes for amounts over 999
            amountSet = true;
            break;

          case "BAI code":
          case "BAI Code":
            m_currentField = readColumn(columns, kvp.Value);
            if (string.IsNullOrEmpty(m_currentField))
              continue;         // Skip blank lines

            // Bug #12055 Mike O - Use misc.Parse to log errors
            transaction.baiCode = Convert.ToInt32(m_currentField);
            break;


          case "Customer Reference":
            m_currentField = readColumn(columns, kvp.Value);
            if (string.IsNullOrEmpty(m_currentField))
              continue;         // Skip blank lines
            transaction.customerRefNum = m_currentField;
            depositSlipNumberSet = true;
            break;

          case "Bank Reference":
            m_currentField = readColumn(columns, kvp.Value);
            if (string.IsNullOrEmpty(m_currentField))
              continue;         // Skip blank lines
            transaction.bankRefNum = m_currentField;
            break;

          case "Text":
            m_currentField = readColumn(columns, kvp.Value);
            if (string.IsNullOrEmpty(m_currentField))
              continue;         // Skip blank lines
            transaction.text = m_currentField;
            break;

          default:
            throw new Exception("BBT CSV parser failure: unexpected column name: " + kvp.Key);
        }
      }

      // Check for blank lines
      if (amountSet == false && dateSet == false && depositSlipNumberSet == false)
      {
        emptyRecord = true;
        return transaction;
      }
      else
        emptyRecord = false;

      if (amountSet == false)
        throw new Exception("BBT CSV parser failure: the amount value is not set in line: " + line);

      if (dateSet == false)
        throw new Exception("BBT CSV parser failure: the date value is not set in line: " + line);

      // Allow a blank deposit slip number

      return transaction;
    }

    private String readColumn(String[] columns, int index)
    {
      // Handle truncated lines (missing fields at the end)
      if (index >= columns.Length)
        return "";

      return columns[index];
    }


    private void createColumnMappings(List<KeyValuePair<string, int>> mappings, String line)
    {
      String[] headings = line.Split(',');
      for (int i = 0; i < headings.Length; i++)
      {
        String heading = headings[i].Trim();        // Bug 19556: Leading space in header screwed up Citizen's file
        KeyValuePair<String, int> kvp;
        switch (heading)
        {
          case "Date":
          case "DATE":
            kvp = new KeyValuePair<String, int>("As Of", i);
            mappings.Add(kvp);
            break;
          case "Bank Account":
          case "BANK ACCOUNT":
            kvp = new KeyValuePair<String, int>("Account", i);
            mappings.Add(kvp);
            break;
          case "Check Number":
          case "CHECK NUMBER":
            kvp = new KeyValuePair<String, int>("Customer Reference", i);
            mappings.Add(kvp);
            break;
          case "Description":
          case "DESCRIPTION":
            kvp = new KeyValuePair<String, int>("Text", i);
            mappings.Add(kvp);
            break;
          case "Amount":
          case "AMOUNT":
            kvp = new KeyValuePair<String, int>("Amount", i);
            mappings.Add(kvp);
            break;
          default:
            // Ignore the other columns
            break;
        }
      }
      // Bug 11080: Improve error message
      int numberOfMatches = mappings.Count;
      if (numberOfMatches == 0)
        throw new Exception("BBT Parser: Input file is not in CSV Header format");
      if (numberOfMatches < 5)
        throw new Exception("BBT Parser: Input file header error.  Did not find the 6 required column headers (Date, Bank Account, Check Number, Description, and Amount).");
      // End 11080
    }

    private void createColumnMappings_aug_3(List<KeyValuePair<string, int>> mappings, String line)
    {
      String[] headings = line.Split(',');
      for (int i = 0; i < headings.Length; i++)
      {
        String heading = headings[i];
        KeyValuePair<String, int> kvp;
        switch (heading)
        {
          case "As Of":
          case "AS OF":
            kvp = new KeyValuePair<String, int>("As Of", i);
            mappings.Add(kvp);
            break;
          case "Account":
          case "ACCOUNT":
            kvp = new KeyValuePair<String, int>("Account", i);
            mappings.Add(kvp);
            break;
          case "BAI Code":
          case "BAI CODE":
            kvp = new KeyValuePair<String, int>("BAI code", i);
            mappings.Add(kvp);
            break;
          case "Customer Reference":
          case "CUSTOMER REFERENCE":
            kvp = new KeyValuePair<String, int>("Customer Reference", i);
            mappings.Add(kvp);
            break;
          case "Bank Reference":
          case "BANK REFERENCE":
            kvp = new KeyValuePair<String, int>("Bank Reference", i);
            mappings.Add(kvp);
            break;
          case "Text":
          case "TEXT":
            kvp = new KeyValuePair<String, int>("Text", i);
            mappings.Add(kvp);
            break;
          case "Amount":
          case "AMOUNT":
            kvp = new KeyValuePair<String, int>("Amount", i);
            mappings.Add(kvp);
            break;
          default:
            // Ignore the other columns
            break;
        }
      }
      // Bug 11080: Improve error message
      int numberOfMatches = mappings.Count;
      if (numberOfMatches == 0)
        throw new Exception("BBT Parser: Input file is not in CSV Header format");
      if (numberOfMatches < 6)
        throw new Exception("BBT Parser: Input file header error.  Did not find the 6 required column headers (As Of, Account, BAI Code, Customer Reference, Description, and Amount).");
      // End 11080
    }


    /// <summary>
    /// Reworked the column mappings so that they are hardwired for grabbing 
    /// only 3 relevant columns.
    /// The BBT CSV files does NOT have the first line as column headings
    /// so they had to be hardwired here.
    /// NOTE: As of 5/11/2015 the BBT column mappings are still up in the air since they have not provided a sample
    /// </summary>
    /// <param name="mappings"></param>
    [Obsolete("Use the newer createColumnMappings which is more flexible", true)]
    private void createColumnMappings_hardwired(List<KeyValuePair<string, int>> mappings)
    {

      KeyValuePair<String, int> kvp;

      kvp = new KeyValuePair<String, int>("As Of", 0);
      mappings.Add(kvp);

      kvp = new KeyValuePair<String, int>("Amount", 8);
      mappings.Add(kvp);

      kvp = new KeyValuePair<String, int>("Customer Reference", 10);
      mappings.Add(kvp);

    }
  }
}
