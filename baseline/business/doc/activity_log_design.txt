activity_log:

ON HOLD.  

Restructuring and restating of requirements from Cashier Design Specification document.

Any method can have a _log attribute which indicates the call should be logged.
It saves the request information into the activity_log database table.

The arguments are saved in an XML GEO (same as how the logging works now with only one level of primitives shown)




Requirements:
o auditing: someone changes security settings.  
o could record all requests that affect system state
   - inquiry responses not important
o tracking all requests
   - 
o each log entry can be associated with a named logical log 
  (not necessily different files or tables for storage) to separate different points
   - "in" (inbound) entry often leads to entry in "system_interface".  When replaying, only want
     to replay "inbound" entries.
o Could be used for restoring state
   - can 'replay' activity that occured after an old state to get back to new state
   - For "inbound", must be able to update iPayment state without updating system interfaces.
   - For "system_interface", must only update selected system interfaces
o activity entry captures the datetime of the request, the session, and the call/operation.
   - all data is primitive or paths that are relative to user's session (my)
o recording in database and/or file 
   - can fully construct database entries from XML file
   - can fully construct XML file from database entries
o if primary database holds the log, be careful about growth of logging data.
o FUTURE: for database, has retention period for the data
o prevent tampering of data
   - checksums on each new entry and for archived file
o can filter log entries based on any criteria such as user, time, action, department
    using standard SQL tools.
o support stateful sessions where two sessions might use same username
o encrypt specific data such as credit-card numbers while leaving other data readable
o all applications use same activity logging system

Design:
o all public methods (app controller method) need to be marked as whether they need to be put into the journal
  - Business.cbc.post.<set> _log="in" </set>

XML file:


Compressed XML file: (1/2 the size)
Header:
<set> DT=Datetime AT=activity MS=make_session </set>

<AT>"in"<DT>2006 11 11 10 10 10</DT><MS>"web_user" "222222222"</MS>Business.Core_event.<post>_subject=my.0 3</post></AT>


Database table: ACTIVITY_LOG
o LOGNAME= name of the log that the activity belongs to
o TIMESTAMP=datetime of activity
o USERID = request.session_id.userid
o METHOD = full path to method (method)
o CALLED_ON= subject of the call (called_on??)
o ARGS=XML string encoding the arguments of a call.  Blank if no arguments.
o SUMMARY=subset of arguments that needs to be searchable.  Separator?


Implementation:
o inbound logging done in casl_server.cs right before execute_code

Questions:
o what about activity log entries where the user session crosses log files.
    Initially, avoid problem by restarted Web server when new file created.
    Other option: flag when session object is created, and use 'create_session'
  









