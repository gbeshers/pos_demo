Validation design:

TODO:
Goal: Add validation to T4.

Design goals:
o great user experience:
   - immediate and clear feedback
   - reducing error responses from the server.
   - good looking, clean design
     o style sheets for display and layout of errors
     o summary of errors displayed at the top (and perhaps at the bottom)
     o error messages displayed at individual fields
   - very specific error messages that make it clear what is wrong with the input
     o only show one error per field at a time.  Ex: If email is empty,
       then say "email is required", rather than "email is required, email is invalid"
   
o increased security by preventing attacks based on invalid user input
  o default type should be 'safe'.  one_line_text

o reduced development time and higher quality product through:
  o standard validation specification system which makes plug-n-play 
      easy to support new types.
  o use of logical contracts to describe data constraints
  o support both client-side and server-side validation with one code base
  o single set of validation error messages used by client and server
  o can use/leverage 3rd party JavaScript code for validation
  o composite types: all_of, one_of

Work steps:
o design
o static prototype
o integration into CASL
o implement various types

one_line_text
  Characters: 0-9, a-z, A-Z, space, comma, dash, dot
  
one_line_text_100 = <all_of> one_line_text <max_length 100/> </all_of>

Type.<text> alpha length=100 </text>
Type.<text> numeric length=100 </numeric>  === string with digits.

This message is shown at the top of the page when
there are server-side error messages:

----------
Please correct the following and click submit:
 * Email address is invalid.
 * Date of birth is required.
 
Email Address |__________________
              * Email address is invalid.
        Stuff |_this_is_ok______
        
Date of Birth |__________________
              * Date of birth is required.
              
       Stuff2 |_2838328_ok_______

Please correct the following before clicking Continue:
 * Email address is invalid.
 * Date of birth is required.

[ Continue ]

KEY CONCEPTS:
o Type.<defclass _name='test_type'> 
   <defmethod _name='is_type_for'> a_geo=req 
    <if> a_geo.<is> 10 </is> <false "something wrong"/> </if>
   </defmethod> 
  </defclass>
  CASL method: <validate> args=<GEO> x=10 </GEO> 
                          contract=<defmethod _name='foo'> x=req=test_type </> 
               </validate>
  Returns <GEO> x="something wrong" </GEO>.<to_js/>
  
o contract with types for fields
o when generating a HTML widget, add event handler 
o event handler on HTML widget that calls 'validate' with 'this' (HTML widget)
  and 'key_path'
o validate looks up the types for that field in field_types global data object.
  (that object was generated when the form was created)
o validate calls JS methods for each type passing the data value, and type params.
    To get the value, JS calls get_value which is passed a key_path and 
    returns a primitive or complex object created from INPUT fields.
    one_of_is_type_for ( data_value, args=['red', 'blue'] );
      data_values: "100", { x: 10, y: 20 }
o is_type_for returns 
    true, a replacement value, or instance of false with
    validation error message.   <false message="stuff"/>
o validate returns a geo-like object with validation error messages to display.
   validate calls 'show_error_messages' with validation_result.
o validation_result: { x: "x is required", y: { z: "z is invalid"} } 
    OR { x: "x is required", "y.z":  "z is invalid" }
o show_error_messages will put the message in the appropriate location, and change styles, etc.

Allows multiple validations per field.
  validate(this,'req','email')

How to validate across multiple fields?
 key_path points to a object with multiple fields, type looks at multiple fields.
 validate(this, key_path="car_appointment");
  valid_odometer(data_value={color: "red", starting_odo: 3000, ending_odo: 3010, age: 10})
    var DIFF = data_value.ending_odo - data_value.starting_odo;
    return ( DIFF < 100 && (data_value.starting_odo < data_value.ending_odo) ); 

validate will get the containing form.  
Need to map field types to keys.  

<GEO>
 x=req=<all_of> email <max_length 100/> </all_of>
</GEO>

x: ['req', 'email', ['max_length', 100]]

req_is_type_for(data_value);
email_is_type_for(data_value);
max_length_is_type_for(data_value, 100);

<SCRIPT>
var field_types = {
  email: ['email_address', 'req'],
  street: ['req', 'one_line_text'],
  gender: ['req', ['one_of', 'red', 'blue']],
  age: [['range_of', 1, 120]],   // not required
  "nested.data": ['req'],
  lname: [['max_length', 20]]
}
</SCRIPT>

OR:

<SCRIPT> var field_types= {}; </SCRIPT>
<SCRIPT> field_types.email = ['email_address', 'req']; </SCRIPT>

Describing:
For each field, can list the name of a type
which refers to a single JavaScript funtion.
'req' => req_is_type_for(a_geo);

How to support parameterized types such as:
 <range_of> 10 30 </range_of>
 OR
 <one_of> "red" "blue" </one_of>
 
Need method to take a type, and then generate the
JavaScript entry.

JavaScript function can take any number of arguments
for defining the type:

['one_of', 'red', 'blue']
=> one_of_is_type_for(a_geo, 'red', 'blue');

['range_of', 'red', 'blue']
=> one_of_is_type_for(a_geo, 'red', 'blue');

Above JavaScript object is generated during page
creation.  If field is required, that gets added.

Each field may have one or more validations.
Each validation might add zero or more errors that will
appear as messages to the user.

JavaScript will be used to change styles and show validation
error messages to the user.  

<FORM action="">
 <INPUT name="email" value="" onblur="validate(this,'email')"/>
</FORM>
  

Generate JS file during load process.
 name of file: validate.js
 Finds all is_type_for methods, and any method with a "javascript_impl"
  is added to the file.
 
req.<defmethod _name='is_type_for'> a_geo=req
  javascript_impl="a_geo == ''"
  
  a_geo.<to_htm/>.<is> "" </is>
</defmethod>

<!-- if no return, then add return result -->
GEO.<defmethod _name='create_javascript'>
 <if> .<has> "javascript_impl" </has>
        .javascript_impl
      else
        "xxx"
 </if>
</defmethod>

function req_is_type_for ( data_value ) {
  return a_geo == '';
}


---------------------------------------------------
Design and debugging for client-side Validation

Overall:
o Types are assigned to fields using Config tool or CASL config

o validation.js is generated from the types that are used.
    The file is actually in memory at Business.validation
    To see the file contents as the browser would see it, use: 
      Business.validation.<to_js_top/>
    It is a JavaScript database of error messages for types.

o When INPUT is created (make_input?), it will insert a SCRIPT that assigns
  field types to a field key:
  
    <SCRIPT>field_types.zip={label:"Zip",types:["alphanumeric"]};</SCRIPT>

o Validation JS method in main.js is called.
    It looks up type definitions in validation.js

DEBUGGING:
o Does red asterisk (*) show before the field key?  yes. seems good.
    That uses obj.<is_required> "some_key" </>
o Does the HTML Script have it in types?  No.
    <SCRIPT>field_types.zip={label:"Zip",types:["alphanumeric"]};</SCRIPT>
o Does the in-memory transaction type have it? yes
    http://localhost/pos_demo/Business/Transaction/ACCTPAY/zip_f_req
o Does the config have it? Yes (because the in-memory verison has it)
o Where is the field_types set?
   in make_input_js_script
       <if> a_geo.<is_required> a_key </is_required> ... 
o Got only generic "is invalid" message.  Where is custom message used?

Type.<defclass _name='required'>
 _new_class.<set>
   error_message=<GEO> empty=" is required."</GEO>
 </set>
 
Look at business/include/validation.js and verify that the script data is correct:
    type_errors.required= {};
    type_errors.required.empty=' is required.';

Cause of client-side validation not working: 
  scripts in page required type_errors and field_types to be globally initialized, but
  you can't depend on the order of scripts being loaded in the page.
  Initialized the global variables in a SCRIPT tag (not file) so that you can depend upon them
  or in the Script file that needs them.
  
  


