http://localhost/a_cbc/foo/bar.htm

a_cbc is a app because it has htm_wrap.  


-------
Notion: wrap is what demands authentication.  Perhaps first path part MUST need an authn
  method if no auth token is given.
  all subapps need to 'trust' authentication
  
part of server pipeline process will authenticate based on fluid.session_id

my.current_request.authenticated.<is> true </is>

uses authenticated flag in get_status method

when processing 

how to force authentication when the top wrap fails authentication.

session timeout, but get back saved session.


GOAL:
 1. app code avoids having to check security/authn.  can't accidentally leave security holes.
 2. if expires and you unlock, continue with last state  (optional)
 3. only check auth token once per request.    auth_token="234234234"

 my.current_request.is_authenticated = "expired", false, "234234234"

DESIGN: all subapps will trust the outermost app's authentication testing.
 
Optional goals:
 o can use different authentication schemes depending upon entry point

Design:
 o cookie with a session_id
 o flag on my.current_request with authenticated_request=true
 o status of app uses my.current_request.is_authenticated value.
 
<get_cookie "foo"/>
<set_cookie> "foo" <v> 23 23 23 32 </v> </>

server_pipeline.      decode, authenticate, execute_code, format

show login-page, call login-function with username and password, check to database,
  don't my.current_request.auth_flag   
  cookie has auth_token "q234132423141234"
  
  one authentication system per server (all apps)
  
serverpipeline authenticates auth_token cookie
  if invalid, show login:

    login page calls sign_in (controller method) that checks against database and sets cookie.
       redirects to original request (optional)
  else
    continue processing
    
IDEAL: working filling out a form 30 minutes, press submit, shows login, shows origainl result.
  

/cbc/foo.htm
and no auth_token ==> show login page. call some public method, set cookie auth_token

-----
session_id

every page checks login flag, and redirect to login page.

create own login screen, call custom function on database to validate username/password,
  set authentication_flag   login=true  




<do>
GEO.<defmethod> _name="set_cookie" key=req value=req duration=opt
 _impl=cs.CASL_engine.misc.CASL_set_cookie
</defmethod>

GEO.<defmethod> _name="get_cookie" key=req 
 _impl=cs.CASL_engine.misc.CASL_get_cookie
</defmethod>
<!--
<test> 
<process_request> 
 "http://localhost/site/set_cookie?key=abc&amp;value=43222" 
</process_request>

<process_request> 
 "http://localhost/site/get_cookie?key=abc" 
</process_request>.a_response.body_byte

43222
</test>-->
<!-- persisting 'my' -->
GEO.<defclass> _name='User_space'  <!-- My   User_space.of.234  My.of.234 -->
 id=req
 user=opt
 
 _new_class.<set> 
   of=opt 
 </set>
</defclass>


GEO.<defclass> _name='Workstation'
 id=req
 ip_address=opt
 name=opt
 
 
</defclass>

GEO.<defclass> _name='User'
 id=req
 name=opt
 password=opt
 first_name=opt
 
 _new_class.<set> 
   of=opt <!-- cache instances --> 
 </set>
</defclass>

GEO.<defclass> _name="Password_lookup"

 <defmethod> _name="add_user_password"
  user_id=req
  password=req 
 </defmethod>
 
</defclass>

GEO.<defmethod> _name='authenticate'
token=req <!-- get from get_cookie("authentication_cookie")-->
<!--


   expired
   invalid
   valid
-->

</defmethod>
Server.<defmethod> _name='pre_process'
<!--
check 
authentication token(cookie) : if not, show login station, if has, validate
   doesn't have it
   has it, but expired
   has it, but invalid

user_id(cookie): identify a person

session_id (IIS)(cookie) :  always have it, check it?  authentication_token
  does it have a flag for whether they are logged in.
  need to know the user_space_id for that session_id.  

user_space_id(cookie) :  if not avaiable, load pre
  default_user_space
  only accessed through one session_id at a time.  
  
Scenarios:
o one user has two user_space/sessions(one user_space pre session) 
on same machine, same wo
o user come in
+ authenticate return valid, expire, invalid, 
+ user provide new authentication info and valid for authenticate
+ check cookie.user_space_id,  if no user_space_id
use user_id to find last presisted user_space_id 
if has presisted user_space_id, 
   load its presisted user_space, 
   set session_id as cross-reference with user_id, user_space_id. (R: session_id indicate what the current user's user_space)
if not avaiable, load default user_space for user_id

+ pass session_id, user_id in all following action.

app has
independent user_space
one machine support multiple user_spaces
application has instance which saved in user_space. 
 applicaiton action will change only in instance. 

--------------------------------
user_id session_id user_space_id

--------------------------------

user
machine
brower
server


workstation_id :
  can have multiple user_spaces per workstation.

-->

</defmethod>

<!-- server space-->
GEO.<defmethod> _name='authenticate' </defmethod>
GEO.<defclass> _name='app_A' </defclass>

GEO.<defclass> _name='app_B' </defclass>

GEO.<defclass> _name='My'
 authentication_token=""
 user_id=""

</defclass>

My.apps=<v>
  app_A 
  app_B
</set>

My.<set> user_spaces=<v>
 "ann1"=<user_space> user_space_id="ann1" 
   a_app_A= <app_A> status='tendering' </app_A> 
   a_app_B= <app_A> status='active' </app_A> 
 </user_space>
 "bob1"=<user_space> user_space_id="bob1" a_app_A= <app_A> status='receipted' </app_A> 
 </user_space>
 </v>

</v>

GEO.<defclass> _name='user'
  user_id=req
  of=<v> 
   <user> user_id='ann'</user>
   <user> user_id='bob'</user>
  </v>
  
</defclass>

<!-- handle request -->
my = <My> 
<!-- my include both session variable and cookie
where session variable is user_space for one browser.  (session cookie)
where pre cookie is a set of info for one client machine. 
cookie can be set on domain or even specific uri path. 
 -->
  session_id="32432"  <!-- session variable -->
  user_id=""          <!-- session variable -->
  authentication_token=""<!-- session variable -->
  user_space_id="" <!--  what is it ?? 
    user_space can contain state of one or more running applications (e.g. portal apps interact with each other)
    one user can have multiple user_space
  -->
  workstation_id  <!-- cookie use <v/> for multiple applications --> 
</My>

<app_A/>

app_A.authenticate.<htm/> 

server.<handle_request> 
 server.a_request
</handle_request>

GEO.<defclass> _name='request'

 request_obj
 session
 
</defclass>

GEO.<defclass> _name='server'

 a_request=opt
 a_resonse=opt
 last_request=my.last_request
 
</defclass>


MY.<defmethod> _name='load_user_space_id'
  user_space_id=req

  my.<set_inside> My.of.<get> user_space_id </get>
  </set_inside>
</defmethod>

server.<defmethod> _name='handle_request'
  a_request=req
  
 <set> app = a_request.request_object </set>
 <set> AU= app.<authenticate>user_id=a_request.args.user_id pwd=a_request.args.pwd</authenticate></set>
 
 <if> AU.<is> "valid" </is> 
   <do>
     my.<set> 
       user_id = a_request.args.user_id
       authenticate_token="y"
     </set>
     <!-- check session_id for user_id -->
     
     <set> user_space_id = <get_user_space_id> user_id </get_user_space_id></set>
     <!-- missing function how to find user_space for user_id-->
     <if> user_space_id
        <do> 
          my.<set> user_space_id=user_space_id</set>
          my.<load_user_space_id> user_space_id </load_user_space_id>
          .<handle_request> .last_request </handle_request>
        </do>
     </if> 
   </do>
 </if>
</defmethod>

<app_B/>

<app_A/>

"loaded"

</do>