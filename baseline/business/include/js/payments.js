// Disable copy and paste in the Bank Account Number Field
$('.bank_account_nbr, .bank_account_nbr2').on("paste", function(e) {
  e.preventDefault();
  var elem = this;
  var name = $(elem).attr('name');

  // Clear the old error label first then add the new one
  // IPAY-1085 UMN fix a11y issues with duplicate labels by label -> div
  if ($(elem).closest(".field").siblings(".copy-paste-label").length <= 0) {
    $('.error[for="' + name + '"]').before('<div class="copy-paste-label error">Keyboard entry is required</div>');
  }
});

$('.bank_account_nbr, .bank_account_nbr2').keypress(function() {
  var elem = this; 
  $(elem).closest(".field").siblings(".copy-paste-label").remove();
});

// For expanding payment details on mobile, tablet

$('button.details').click(function () {
  $('.transactions-table').addClass('expanded');
});

$('button.retract').click(function () {
  $('.transactions-table').removeClass('expanded');
});

// Bank Name should not be editable
$('.bank_name').prop( "disabled", true );

// Disable the a element when the acknowledgement modal
// is showing
$('.modal .action-buttons-wrapper a').click(function() {
  var elem = this;

  setTimeout(function() {
    $(elem).attr('disabled', 'disabled');
  }, 50);
});

// Close Icon Prompt
$('.close-icon-link').click(function(e) {
  e.preventDefault();
  e.stopPropagation();
  var elem = this; 
  var deleteAmount = $(elem).siblings('.amount').text();
  var deleteLink = $(elem).attr('href');
  $('.delete-confirmation-link').attr('href', deleteLink);
  $('.delete-confirmation-amount').text(deleteAmount);
  $('.modal-state').prop('checked', true);
});

$('#delete-cancel_btn').click(function() {
  $('.modal-state').prop('checked', false);
});

// Bug 26770 NAM: removed

function Adumbrate(s) {
  var r = [];
  for (var i = 0; i < s.length; i++) {
    var j = s.charCodeAt(i);
    if ((j >= 33) && (j <= 126)) {
      r[i] = String.fromCharCode(33 + ((j + 14) % 94));
    }
    else {
      r[i] = String.fromCharCode(j);
    }
  }
  return r.join('');
}

function setCCNbrEn() {
  var ccNbr = $("#args\\.credit_card_nbr");
  var ccNbrEn = $("#args\\.credit_card_nbr_en");

  if (ccNbr.val() !== undefined)
    ccNbrEn.val(Base64.encode(Adumbrate(ccNbr.val())));
}


/*****
*
*   Base64.js
*
*   copyright 2003, Kevin Lindsey
*   licensing info available at: http://www.kevlindev.com/license.txt
*
*****/

/*****
*
*   encoding table
*
*****/
Base64.encoding = [
    "A", "B", "C", "D", "E", "F", "G", "H",
    "I", "J", "K", "L", "M", "N", "O", "P",
    "Q", "R", "S", "T", "U", "V", "W", "X",
    "Y", "Z", "a", "b", "c", "d", "e", "f",
    "g", "h", "i", "j", "k", "l", "m", "n",
    "o", "p", "q", "r", "s", "t", "u", "v",
    "w", "x", "y", "z", "0", "1", "2", "3",
    "4", "5", "6", "7", "8", "9", "+", "/"
];

/*****
*
*   constructor
*
*****/
function Base64() {}

/*****
*
*   encode
*
*****/
Base64.encode = function(data) {
  var result = [];
  var ip57   = Math.floor(data.length / 57);
  var fp57   = data.length % 57;
  var ip3    = Math.floor(fp57 / 3);
  var fp3    = fp57 % 3;
  var index  = 0;
  var num;
  
  for ( var i = 0; i < ip57; i++ ) {
    for ( j = 0; j < 19; j++, index += 3 ) {
      num = data.charCodeAt(index) << 16 | data.charCodeAt(index+1) << 8 | data.charCodeAt(index+2);
      result.push(Base64.encoding[ ( num & 0xFC0000 ) >> 18 ]);
      result.push(Base64.encoding[ ( num & 0x03F000 ) >> 12 ]);
      result.push(Base64.encoding[ ( num & 0x0FC0   ) >>  6 ]);
      result.push(Base64.encoding[ ( num & 0x3F     )       ]);
    }
    result.push("\n");
  }

  for ( i = 0; i < ip3; i++, index += 3 ) {
    num = data.charCodeAt(index) << 16 | data.charCodeAt(index+1) << 8 | data.charCodeAt(index+2);
    result.push(Base64.encoding[ ( num & 0xFC0000 ) >> 18 ]);
    result.push(Base64.encoding[ ( num & 0x03F000 ) >> 12 ]);
    result.push(Base64.encoding[ ( num & 0x0FC0   ) >>  6 ]);
    result.push(Base64.encoding[ ( num & 0x3F     )       ]);
  }

  if ( fp3 == 1 ) {
    num = data.charCodeAt(index) << 16;
    result.push(Base64.encoding[ ( num & 0xFC0000 ) >> 18 ]);
    result.push(Base64.encoding[ ( num & 0x03F000 ) >> 12 ]);
    result.push("==");
  } else if ( fp3 == 2 ) {
    num = data.charCodeAt(index) << 16 | data.charCodeAt(index+1) << 8;
    result.push(Base64.encoding[ ( num & 0xFC0000 ) >> 18 ]);
    result.push(Base64.encoding[ ( num & 0x03F000 ) >> 12 ]);
    result.push(Base64.encoding[ ( num & 0x0FC0   ) >>  6 ]);
    result.push("=");
  }

  return result.join("");
};
//////// Bug 23569 NAM/18719 UMN add AJAX Details call for getting group details for a cell

// <summary>
//   Calls Create_core_item_app.detail_group_inquiry which will invoke the outer_details template
// </summary>
// <param name="oButtonElement" type="Object"> jQuery button element </param>
//
function get_outer_details(oButtonElement)
{
  var oArgs = {};
  var jBtnSelector = $(oButtonElement);
  
  // something like "__43987" or "__PresFillCurrentLoc_0"
  oArgs.group_name = jBtnSelector.prop('id');
  var sURL = jBtnSelector.attr("data-url");  // URL with doublesubmit args
  oArgs.cbt_id = jBtnSelector.attr("data-cbtid");

  // Bug 20490 pass custom field id so doesn't have to be encoded in group name anymore
  var sFullURL = sURL + "&group_name=" + oArgs.group_name + "&cbt_id=" + oArgs.cbt_id + "&level=outer";

  // for responsive UI, will always be a modal, except for inner details
  window.open(sFullURL, "_self", "", false);

  return true;
}

/* 
  fetchedDetails indicates if an inner detail has been fetched. It is keyed by the group name, 
  so may be something like detailRow  "__[BC_TERMSUMMARY_ALL_PB][0][1][Details]".
*/
var fetchedDetails = {};

// <summary>
//   Calls Create_core_item_app.detail_group_inquiry which will invoke the inner details template
//   via AJAX. The results are merged into the outer_details template.
// </summary>
// <param name="oButtonElement" type="Object"> jQuery button element </param>
//
function get_inner_details(oButtonElement)
{
  var oArgs = {};
  var jBtnSelector = $(oButtonElement);
  
  // something like "__43987" or "__PresFillCurrentLoc_0"
  oArgs.group_name = jBtnSelector.prop('id');

  // Bug 20490 UMN do case-insensitive compare in case user messes up in Config
  oArgs.cbt_id = jBtnSelector.attr("data-cbtid");

  var sURL = jBtnSelector.attr("data-url");  // URL with doublesubmit args
  var detailType = oArgs.cbt_id; 
  var detailRow = oArgs.group_name;   // eg: "__43987" or "__PresFillCurrentLoc_0"

  // Bug 20490 pass custom field id so doesn't have to be encoded in group name anymore
  var sFullURL = sURL + "&group_name=" + oArgs.group_name + "&cbt_id=" + oArgs.cbt_id + "&level=inner";

  // have we fetched the data before? If so, then parent hide/unhide will display data, so just return
  if (fetchedDetails[detailRow] !== undefined)
  {
    return;
  }
  else 
  {
    $.ajax( 
    {
      "url": sFullURL,
      "data": {},
      "success": function (data) {
        // insert after the button
        jBtnSelector.after(data).slideDown('fast');
        fetchedDetails[detailRow] = detailType;
      },
      "dataType": "html",
      "cache": false,
      "error": function (xhr, error, thrown) {
        var err = "<div class='item-group-detail-wrapper expander-content'>Unable to get details from server (AJAX Error E-02).</div>";
        jBtnSelector.after(err).slideDown('fast');
      }
    });
  }
}

//////// END Bug 18719 UMN Detail inquiries

// Bug 25590 MJO - Paypal support (Braintree API)
function braintree_init(client_token, amount, enable_paypal, enable_venmo, line_items) {
  amount = parseFloat(amount);

  if (amount <= 0.00) {
    alert("PayPal cannot be used to tender the remaining balance. Please select another tender.");
    return;
  }

  $button = $('button[name="_method_name.continue"], #form_class button:contains("OK"), button#tender_next');
  var $email = $('.EMAIL_RECEIPT');

  // Bug IPAY-682 MJO - Initially hide e-mail field and Next button
  $button.parent().css('display', 'none');
  $email.parent().css('display', 'none');

  $button.prop('disabled', true);

  braintree.dropin.create({
    authorization: client_token,
    container: '#dropin-container',
    paypal: enable_paypal ? {
      flow: 'checkout',
      amount: amount,
      currency: 'USD',
      singleUse: true,
      lineItems: line_items
    } : null,
    card: false,
    venmo: enable_venmo ? { allowNewBrowserTab: false } : null,
    dataCollector: {
      paypal: true
    }
  }, function (createErr, instance) {
    if (createErr != null) {
      if (createErr.message.indexOf("No valid payment options available") > -1)
        alert("No valid payment options available. Please select a different tender.");
      else
        alert("Error creating PayPal widget: " + createErr.message + "\n" + "Please cancel and try again.");
    }
    else {
      $button.prop('disabled', false);

      // Bug 26030 MJO - Populate e-mail field if present
      instance.on('paymentMethodRequestable', function (event) {
        if (event.paymentMethodIsSelected) {
          instance.requestPaymentMethod(function (err, payload) {
            if (err) {
              alert("Error: " + err.message + "\n" + "Please try again.");
              return;
            }

            if (payload.details.email && payload.details.email !== null && payload.details.email.length > 0)
              $email.val(payload.details.email);

            // Bug IPAY-682 MJO - Display e-mail field and Next button on successful processing
            $button.parent().css('display', 'block');
            $email.parent().css('display', 'table');
          });
        }
      });

      $button.click(function (e) {
        e.preventDefault();
        instance.requestPaymentMethod(function (err, payload) {
          // Submit payload.nonce to your server
          if (err === null) {
            $('input#paypal_payload').val(JSON.stringify(payload));

            if ($('button[name="_method_name.continue"]').length > 0)
              get_top().main.form_inst['_method_name.continue'].value = 'continue';

            $('form[name="form_class"], form#form_class, form.form_inst').submit();
          }
          else {
            alert("Error: " + err.message + "\n" + "Please try again.");
            return;
          }
        });
      });
    }
  });
}

//////// Bug 26537 UMN - Eigen MiraPay support
// Bug IPAY-1543 MJO - Switched to new encryption method
function mirasecure_init(get_token_url, message) {  

  $('body').append(
    '<DIV style="display:none;">' +
    '<FORM ' +
    'id="mirasecure"' +
    ' target = "mirasecure_iframe"' +
    ' action = "' + get_token_url + '"' +
    ' method = "get"' +
    '>' +
    '<INPUT name="data" value="' + message + '"></INPUT>' +
    '</FORM>' +
    '</DIV>'
  );

  // If we have a mirasecure_iframe_validation input, just hide Next button initially.
  // When the user submits the mirasecure_iframe, the mirasecure_iframe_validation will
  // go to true and we will re-enable Next.
  if ($('#mirasecure_iframe_validation').length > 0)
    $('#tender_next').hide();
  
  setTimeout(function () {
    var form = $('form#mirasecure');
    form.find("input#__DOUBLESUBMIT__").remove();
    form.submit();
  }, 500);
}

function mirasecure_finish() {
  $("span#mirasecure_iframe_span").hide();
  $("span#mirasecure_finished").show();
  $('#tender_next').show();
}

function execute_waiting_js()
{
  if (window.execute_on_load && window.execute_on_load.length > 0)
  {
    window.page_loaded = true;
    for (var index in window.execute_on_load) {
      try
      {
        var func_str = window.execute_on_load[index];
        eval(func_str);
      }
      catch(err)
      {
        alert(err.message);
      }
    }
    window.execute_on_load = new Array();
  }
}
//////// END Bug 26537 UMN - Eigen MiraPay support