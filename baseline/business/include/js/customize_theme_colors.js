function customize_theme_colors(themeColorsJson) {
    // themeColorsJson is an array of kvps, with the key "description" mapping to the css var name and the key "url" mapping to the cssValue
    if(themeColorsJson != null && themeColorsJson != "") {
        var cssVarKey = "description";
        var cssValueKey = "url";
        var themeColorsArr = JSON.parse(themeColorsJson);
        var i;
        for (i = 0; i < themeColorsArr.length; i++) {
            var themeColorObj = themeColorsArr[i];
            var cssVar = themeColorObj[cssVarKey];
            var cssValue = themeColorObj[cssValueKey];
            document.documentElement.style.setProperty(cssVar, cssValue);
        }
    }
}