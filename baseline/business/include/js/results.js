  var totalAmountOwed = 0.00;
  var totalAmountToday = 0.00;
  var inputArr = $('.item-payment-input');
  var itemValueArr = $('.item-value');
  var currentCartItems = [];

  // Add class to Item in each row 
  $('.item-details-single-title').each(function(index, element) {
    if ($(element).text().search(/item/ig) >= 0) {
      $(element).siblings('.item-details-single-value1').addClass("item-number");
    }
  });
  
  // Get Current Cart Items and push to the 
  // currentCartItems array
  $('.nested-detail-key').each(function(index, element) {
    if ($(element).text().search(/item/ig) >= 0) {
      currentCartItems.push($(element).siblings('.nested-detail-value').text());
    }
  });

  function checkCurrentCartItems(elem) {
    // Check to see if Item is already in the cart 
    // if so, return false out of the function
    var optionsWrapper = $(elem).closest(".item-payment-options-wrapper");
    var detailsWrapper = $(optionsWrapper).siblings(".item-details-wrapper");
    
    if (currentCartItems.length > 0) {
      for (var i = 0; i < currentCartItems.length; i++) {
        if ($(".item-number", detailsWrapper).text() === currentCartItems[i]) {
          // If this item is in the cart, we change the PAYMENT_SELECTOR to false
          // so that it is not submitted to the server again as an item to add to the cart
          $('.item-payment-input-selector', optionsWrapper).val(false);

          // Add In cart label if not already there
          if ($('.in-cart-label', optionsWrapper).length <= 0) {
            $('.add-single-button', optionsWrapper).remove();
            $('.item-payment-options', optionsWrapper).append('<label class="in-cart-label">In Cart</label>');
          }

          // Show and set in cart summary info
          $('.in-cart-summary-label').show();
          $('.in-cart-summary-value').text(currentCartItems.length);

          // If there are Items in the Cart then show the Page Now Button
          disableEnablePayButton();

          // If there is a match, we should break out of the selectAndDeselect Function
          return false;
        } 
      } 
    }
  }

  $('.item-payment-input').each(function(index, element) {
    checkCurrentCartItems(element);
  });


  // Run this function on page load 
  function reconcileBalanceAndInput() {
    // FYI, there is some dependence on configuration as some projects use RESERVED_ADJUSTED_BALANCE and some use RESERVED_BALANCE

    // Get Balance from that Row
    // Get Input from that Row
    // Compare Values
    // If Different set Input Value to Balance Value
    
    $('.prop_RESERVED_BALANCE').each(function(index, element) {
      var inputSpan = $(element).siblings(".prop_RESERVED_AMOUNT_PAID");
      $('.item-payment-input', inputSpan).val($(element).text());
    });

    // Bug 23831 UMN change bad RESERVED_ADJ_BALANCE to correct RESERVED_ADJUSTED_BALANCE
    $('.prop_RESERVED_ADJUSTED_BALANCE').each(function(index, element) {
      var inputSpan = $(element).siblings(".prop_RESERVED_AMOUNT_PAID");
      $('.item-payment-input', inputSpan).val($(element).text());
    });
  }

  reconcileBalanceAndInput();

  $('.item-value').each(function(index, element) {
    var amount = $(element).text();
  });

  function addDollarSign(num) {
    //BUG 26195 NAM: negative sign before dollar sign for negative amounts
    var amount = num.replace('$', '');
    if (amount <= 0) {
      var result = accounting.formatMoney(amount);
      return result.replace('$-', '-$');
    }
    return accounting.formatMoney(num);
  }

  // TODO: Consolidate these for loops

  function printTotalAmountOwed() {
    totalAmountOwed = 0.00;

    for (var i = 0; i < itemValueArr.length; ++i) {
      var value = Number($(itemValueArr[i]).text().replace('$', ''));
      totalAmountOwed += value;
    }

    totalAmountOwed = String(totalAmountOwed);

    $('.total-amount-owed-value').text(addDollarSign(totalAmountOwed));
  }

  printTotalAmountOwed();

  function printItemTotal() {
    var itemGroupWrapperArr = $('.item-group-wrapper');

    for (var i = 0; i < itemGroupWrapperArr.length; ++i) {
      var itemTotal = 0.00;
      var itemValueGroup = $('.item-value', $(itemGroupWrapperArr[i]));

      for (var x = 0; x < itemValueGroup.length; ++x) {
        var value = Number($(itemValueGroup[x]).text().replace('$', ''));
        itemTotal += value;
      }

      itemTotal = String(itemTotal);
      $('.item-group-description-value', $(itemGroupWrapperArr[i])).text(addDollarSign(itemTotal));
    }
  }

  printItemTotal();

  function printTotalAmountToday() {
    totalAmountToday = 0.00;
    var selectedInputArr = $('.item-payment-input', '.item-group-detail[data-selected=true]');
    var numSelected = $('.item-group-detail[data-selected=true]').length;

    for (var i = 0; i < selectedInputArr.length; ++i) {
      var value = Number($(selectedInputArr[i]).val().replace('$', ''));
      totalAmountToday += value;
    }

    totalAmountToday = String(totalAmountToday);
    $('.total-amount-today-value').text(addDollarSign(totalAmountToday));
    $('.total-selected-summary-value').text(numSelected);
  }

  printTotalAmountToday();

  function printAmountToday() {
    var itemGroupWrapperArr = $('.item-group-wrapper');

    for (var i = 0; i < itemGroupWrapperArr.length; ++i) {
      var amountToday = 0.00;
      var numSelected = $('.item-group-detail[data-selected=true]', itemGroupWrapperArr[i]).length;
      var selectedGroupDetailArr = $('.item-group-detail[data-selected=true]', $(itemGroupWrapperArr[i]));

      $('.selected-summary-value', itemGroupWrapperArr[i]).text(numSelected);
      //if all group items are selected change button caption to 'remove all'. If all unselected change to 'add all'
      if ($('.item-group-detail', $(itemGroupWrapperArr[i])).length == numSelected){
        $(selectedGroupDetailArr).parent().parent().find('button.add-all-button').text("- REMOVE ALL")
      }else if(numSelected == 0 &&  selectedGroupDetailArr.length == 0){
        var btnAddAll = $(itemGroupWrapperArr[i]).find('button.add-all-button');
        if (btnAddAll.length > 0){
          btnAddAll.text("+ ADD ALL")    
        }          
      }
      for (var x = 0; x < selectedGroupDetailArr.length; ++x) {
        var selectedInputArr = $('.item-payment-input', $(selectedGroupDetailArr[x]));

        for (var y = 0; y < selectedInputArr.length; ++y) {
          var value = Number($(selectedInputArr[y]).val().replace('$', ''));
          amountToday += value;
        }
      }

      amountToday = String(amountToday);
      $('.amount-today-value', $(itemGroupWrapperArr[i])).text(addDollarSign(amountToday));
    }   
  }

  printAmountToday();

  function disableEnablePayButton() { 
    var selected = Number($('.total-selected-summary-value').text());
    if ( selected > 0 && totalAmountToday > 0) {
      // $('.results .pay-now-button').removeAttr('disabled');
      // $('.results .results-next-button').removeAttr('disabled');
      $('.results .pay-now-button').show();
      $('.results .results-next-button').show();
      $('.results .pay-now-button-finish').hide();
    } else if (selected <= 0 && currentCartItems.length > 0) {
      $('.results .pay-now-button').hide();
      $('.results .results-next-button').hide();
      $('.results .pay-now-button-finish').show();
      $('.add-all-button').text("+ ADD ALL");
    } else {
      // $('.results .pay-now-button').attr('disabled', 'disabled');
      // $('.results .results-next-button').attr('disabled', 'disabled');

      $('.results .pay-now-button').hide();
      $('.results .results-next-button').hide();
      $('.results .pay-now-button-finish').hide();
      $('.total-add-all-button').first().text("+ ADD ALL")
      if (selected <= 0){
        $('.add-all-button').text("+ ADD ALL");
      }
    }
    //if all items selected change button back to '- REMOVE ALL'
    if (selected == Number($('.item-payment-input').length)){
        $('.total-add-all-button').text("- REMOVE ALL");
    }
  }

  disableEnablePayButton();

  function printUpdatedValues() {
    printAmountToday();
    printTotalAmountToday();
    disableEnablePayButton();
  }

  function clearAllTotals() {
    var amountTodayValueArr = $('.amount-today-value');

    $('.total-amount-today-value').text("$0.00");
    $('.total-selected-summary-value').text(0);

    for (var i = 0; i < amountTodayValueArr.length; ++i) {
      var label = $(amountTodayValueArr[i]).siblings('.selected-summary-label');
      $('.selected-summary-value', label).text(0);
      $(amountTodayValueArr[i]).text("$0.00");
    }
  }

  // Bug 26346 UMN removed unused condition argument
  function selectAndDeselectRow(elem, parentAction) {
    var groupDetail = $(elem).closest('.item-group-detail');
    var btn = $(groupDetail).find('.add-single-button');
    var inputWrapper = $(btn).siblings('.item-payment-input-wrapper');
    var input = $(groupDetail).find('.item-payment-input');
    var selector = $('.item-payment-input-selector', inputWrapper); // Bug 26346 UMN
    var response = jQueryvalidate(input);

    // Bug 26346 UMN if payment selector is read only *and* amount is read only don't give user choice to add/remove
    var isForceSelected = $(selector).prop('readonly') && $(input).prop('readonly');
    
    // Check to see if Item is already in the cart 
    // if so, return false out of the function
    var optionsWrapper = $(elem).closest(".item-payment-options-wrapper");
    var detailsWrapper = $(optionsWrapper).siblings(".item-details-wrapper");
    
    for (var i = 0; i < currentCartItems.length; i++) {
      if ($(".item-number", detailsWrapper).text() === currentCartItems[i]) {
        if ($('.in-cart-label', optionsWrapper).length <= 0) {
          $('.add-single-button', optionsWrapper).hide();
          $('.item-payment-options', optionsWrapper).append('<label class="in-cart-label">In Cart</label>');
        }

        $('.in-cart-summary-label').show();
        $('.in-cart-summary-value').text(currentCartItems.length);

        // If there are Items in the Cart then show the Page Now Button
        if (currentCartItems.length > 0 && $('.selected').length <= 0) {
          $('.results .pay-now-button').show();
        }

        return false; 
      }
    } 

    // Current Cart Item Check (UMN-- why is this call here, already did check above)
    checkCurrentCartItems(elem);

    // if jQuery validated element
    if (response === true) {
      if ($(btn).text().search(/add/i) >= 0) {
        $(btn).text("- REMOVE");
        $(groupDetail).removeClass('selected');
        $(groupDetail).addClass("selected");
        $(groupDetail).attr("data-selected", "true");
        $('.item-payment-input', inputWrapper).addClass("selected");
        $(selector).val(true);

      } else if ($(btn).text().search(/remove/i) >= 0 && parentAction === "add") {
        $(btn).text("- REMOVE");
        $(groupDetail).removeClass('selected');
        $(groupDetail).addClass("selected");
        $(groupDetail).attr("data-selected", "true");
        $('.item-payment-input', inputWrapper).addClass("selected");
        $(selector).val(true);

      } else {
        $(btn).text("+ ADD");
        $(groupDetail).removeClass("selected");
        $(groupDetail).attr("data-selected", "false");
        $('.item-payment-input', inputWrapper).removeClass("selected");
        $(selector).val(false);
      }
    } else {  // failed jQuery validation
      $(btn).text("+ ADD");
      $(groupDetail).removeClass("selected");
      $(groupDetail).attr("data-selected", "false");
      $('.item-payment-input', inputWrapper).removeClass("selected");
      $(selector).val(false);
    }
    
    // Bug 26346 UMN
    if (isForceSelected) 
      $(btn).hide();
  }

  //////// Bug 26346 UMN add allocations support
  
  // Returns true if all the group's payment selectors are read only *and* amounts are read only
  function isGroupForceSelected(group)        // group is of class item-group-wrapper
  {
    //Bug 26903 NAM: return true/false using a variable
    var retValue = true;
    $(group).find(".item-payment-input-wrapper").each(function() {
      var inputWrapper = this;
      var input = $(inputWrapper).find('.item-payment-input');
      var selector = $('.item-payment-input-selector', inputWrapper);

      // UMN if not (payment selector is read only *and* amount is read only) return false
      if (! ($(selector).prop('readonly') && $(input).prop('readonly'))) {
        retValue = false; //store return value
        return false;     //breakout out of loop
      }
    });
    return retValue;
  }
  
  // Disables the total and group Add all buttons if ALL payment selectors are read only *and* amounts are read only
  function disableIfAllForceSelected()
  {
    var disableTotalAllBtn = true;
    $(".item-group-wrapper").each(function() {
      var groupWrapper = this;
      if (isGroupForceSelected(groupWrapper)) {
        // use visibility hidden so that the layout doesn't change unlike with hide
        $(groupWrapper).find(".add-all-button").css("visibility", "hidden");
      }
      else {
        disableTotalAllBtn = false;
      }
    });
    if (disableTotalAllBtn) {
      // use visibility hidden so that the layout doesn't change unlike with hide
      $(".total-add-all-button").css("visibility", "hidden");
    }
  }
  
  disableIfAllForceSelected();
  
  // End Bug 26346 UMN add allocations support

  // This each loops is running selectAndDeselectRow function on all the elements
  // that have been assigned the data-reserved-selected attribute
  // from the liquid template with a value of "true". this scenario occurs when the role
  // is RESERVED_SELECTED and value is "true"

  // this is just for testing to see if the true value for the attribute
  // actually kicks off the selectAndDeselectRow function
  // $('[data-reserved-selected]').eq(0).attr('data-reserved-selected', 'true');

  $('.prop_RESERVED_SELECTED input').each(function() {
    var elem = this;
    var reservedSelectedValue = $(elem).val(); 
    if (reservedSelectedValue === "true") {
      selectAndDeselectRow(elem, "add");
      printUpdatedValues();
    }
  });

  ////////////////////////
  // Expander Functions //
  ////////////////////////

  $('.expander-trigger').click(function(e) {
    e.stopPropagation();
    var elem = this;
    $(elem).toggleClass('expander-hidden');
  });

  //////////////////////////
  // Add Button Functions //
  //////////////////////////

  $('.total-add-all-button').click(function(e) {
    e.stopPropagation();

    var btn = this;
    var groupDetailAll = $('.item-group-detail');
    var inputAll = $('.item-payment-input');
    var inputAllLength = inputAll.length;
    var inputSelectorAll = $('.item-payment-input-selector'); // UMN added
    var addAllBtnArr = $('.add-all-button');
    var addSingleBtnArr = $('.add-single-button');
    var i = 0;

    // Open All the Groups of Items
    $(".item-group.expander-hidden").removeClass("expander-hidden");

    if ($(btn).text().search(/add/i) >= 0) {
      for (i = 0; i < inputAllLength; i++) {
        selectAndDeselectRow(inputAll[i], "add");
      }

      $(btn).text("- REMOVE ALL");
      $('.add-all-button').text("- REMOVE ALL");

    } else {

      $(btn).text("+ ADD ALL");
      $('.add-all-button').text("+ ADD ALL");

      for (i = 0; i < addAllBtnArr.length; ++i) {
        $(addAllBtnArr[i]).text("+ ADD ALL");
      }

      for (i = 0; i < addSingleBtnArr.length; ++i) {
        $(addSingleBtnArr[i]).text("+ ADD");
      }

      $(groupDetailAll).removeClass("selected");
      $(groupDetailAll).attr("data-selected", "false");
      $(inputAll).removeClass("selected");
      $(inputSelectorAll).val(false);

      clearAllTotals();
    }

    printUpdatedValues();

  });

  // UMN this add all button is for the block/group
  $('.add-all-button').click(function(e) {
    e.stopPropagation();

    var btn = this;
    var group = $(btn).closest('.item-group');
    var groupDetailWrapper = $(group).siblings('.item-group-detail-wrapper');
    var groupDetail = $('.item-group-detail', groupDetailWrapper);
    var inputWrapper = $('.item-payment-input-wrapper', groupDetail);
    var addAllBtnArr = $('.add-all-button', groupDetail);
    var addSingleBtnArr = $('.add-single-button', groupDetail);
    var inputArr = $('.item-payment-input', inputWrapper);
    var inputArrLength = inputArr.length;
    var i = 0;
    
    // Open the Specific Group of Items
    $(group).removeClass("expander-hidden");

    if ($(btn).text().search(/add/i) >= 0) {
      for (i = 0; i < inputArrLength; i++) {
        selectAndDeselectRow(inputArr[i], "add");
      }

      $(btn).text("- REMOVE ALL");

    } else {

      $(btn).text("+ ADD ALL");

      for (i = 0; i < addSingleBtnArr.length; ++i) {
        $(addSingleBtnArr[i]).text("+ ADD");
      }

      $(groupDetail).removeClass("selected");
      $(groupDetail).attr("data-selected", "false");
      $('.item-payment-input', inputWrapper).removeClass("selected");
      $('.item-payment-input-selector', inputWrapper).val(false); // UMN added

      var clearGroupTotals = function() {
        var wrapper = $(btn).siblings('.item-group-payment-wrapper');
        $('.selected-summary-value', wrapper).text(0);
        $('.amount-today-value', wrapper).text("$0.00");
      };

      clearGroupTotals();
    }

    printUpdatedValues();
  });

  $('.add-single-button').click(function(e) {
    e.stopPropagation();

    var elem = this;
    selectAndDeselectRow(elem);
    printUpdatedValues();
  });

  $('.item-payment-input').change(function() {
    var elem = this;
    var origValue = $(elem).val();
    var value = origValue.replace("$", "");
    var num;

    if ($.isNumeric(value)) {
      num = Number(value);
      var newValue = num.toFixed(2);
      $(elem).val(newValue);
    }

    if ($(elem).hasClass("selected")) {
      selectAndDeselectRow(elem);
    } else {
      jQueryvalidate(elem);
      $(elem).val(origValue);
    }
    
    //Bug 23363 NAM: update Total Paid with new value entered
    var elemPaid = $(elem).parentsUntil(" .item-group-detail ").parent().find(" li:contains('Total Paid')");
    if ( typeof elemPaid !== "undefined"){
      var elemFee = $(elem).parentsUntil(" .item-group-detail ").parent().find(" li:contains('Fee')");
      if (typeof elemFee !== "undefined"){
        var fee = $(elemFee).find(" .item-details-single-value1" ).text();
        var amtPaid = Number(value) + Number(fee.replace("$", ""));
        if (typeof elemPaid !== "undefined" && elemPaid.length > 0){ //Bug 26460 NAM: add $ sign to value entered
          $(elemPaid).find(" .item-details-single-value1" ).text(addDollarSign(String(amtPaid)));
        }
        else
          $(elem).val(addDollarSign(String(amtPaid)));
      }
    }
    
    printUpdatedValues();
  });

  // Submission
  function submitForm() {
    $('#resultsForm').submit();
  }

  $('.results-next-button').click(function() {
    submitForm();
  });