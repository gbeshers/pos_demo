$(function() {

  // Spinner Setup
  var opts = {
    lines: 17 // The number of lines to draw
  , length: 0 // The length of each line
  , width: 36 // The line thickness
  , radius: 57 // The radius of the inner circle
  , scale: 0.33 // Scales overall size of the spinner
  , corners: 0 // Corner roundness (0..1)
  , color: '#777' // #rgb or #rrggbb or array of colors
  , opacity: 0.25 // Opacity of the lines
  , rotate: 0 // The rotation offset
  , direction: 1 // 1: clockwise, -1: counterclockwise
  , speed: 1 // Rounds per second
  , trail: 60 // Afterglow percentage
  , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
  , zIndex: 2e9 // The z-index (defaults to 2000000000)
  , className: 'spinner' // The CSS class to assign to the spinner
  , top: '50%' // Top position relative to parent
  , left: '50%' // Left position relative to parent
  , shadow: false // Whether to render a shadow
  , hwaccel: false // Whether to use hardware acceleration
  , position: 'absolute' // Element positioning
  }

  if (typeof $('#theme-main-color').css("color") !== 'undefined' && $('#theme-main-color').css("color") !== null) {
    opts["color"] = $('#theme-main-color').css("color");
  }

  var target = document.getElementById('spinnerWrapper');
  var spinner = new Spinner(opts).spin(target);

  $('a:not(".tab-link, .no-loader")').click(function() {
    var elem = this;

    if ($(elem).filter("[name='href']").length > 0) {    //Bug 26389 NAM
      $('.loading-overlay').show();
    }
  });

  //RDM Bug 24331 - Remove ability to hide loading overlay

  // Tooltip Actions

  $('.tooltip-item').click(function(e) {
    var elem = this;
    $('.tooltip', elem).toggle();
  });

  // Trimming Option Text 
  $('option').each(function(index, element) {
    var trimmedTxt = $(element).text().trim();
    $(element).text(trimmedTxt);
  });

  // Bug 26786 MJO tool-tip changes
  $(document).ready(function () {
    repositionTooltips();
  });

  function repositionTooltips() {
    $('.tooltip').each(function (index, element) {
      var rect = element.getBoundingClientRect();

      $element = $(element);

      var rightEdge = $element.width() + $element.offset().left;
      var screenWidth = $(window).width();

      if (rect.left < 0) {
        $($element).css('left', (parseInt($element.css('left')) + (rect.left * -1)) + 'px');
      }
      else if (rightEdge - screenWidth > 0) {
        $($element).css('left', ((parseInt($element.css('left')) + (rightEdge - screenWidth)) * -1) + 'px');
      }
    });
  }

  // Field Width Algorithm

  $('fieldset').each(function(index, fieldsetElem) {
    var countObj = {};
    var currentLineNum = $('.field', fieldsetElem).first().attr('data-line');
    var lineNumArr = [currentLineNum];

    countObj[eval(currentLineNum)] = 0;

    $('[data-line]', fieldsetElem).each(function(index, element) {
      if (Number($(element).attr('data-line')) == currentLineNum) {
        ++countObj[eval(currentLineNum)];
      } else {
        currentLineNum = Number($(element).attr('data-line'));
        countObj[eval(currentLineNum)] = 1;
        lineNumArr.push(currentLineNum);
      }
    });

    $('[data-line]', fieldsetElem).each(function(index, element) {
      var lineNum = Number($(element).attr('data-line'));
      var numOfItems = Number(countObj[eval(lineNum)]);
      var width = 12 / numOfItems;
      var className = "span-width-" + eval(width);
      $(element).addClass(className);
    });

    // Add Omega
    for (var i = 0; i < lineNumArr.length; i++) {
      $('[data-line="' + lineNumArr[i] + '"]', fieldsetElem).last().addClass('omega');
    }
  });

  // Bug 23457 RDM - Updating for multiselect
  var s = $('select:not([multiple])').comboSelect({
    // Combo Select initialization options
    // comboClass         : 'combo-select', /* outer container class */
    // comboArrowClass    : 'combo-select-arrow', /* arrow class */
    // comboDropDownClass : 'combo-drop-down', /* dropdown class */
    // inputClass         : 'combobox-input text-input', /* Input element class */
    // disabledClass      : 'option-disabled', /* Disabled class */
    // hoverClass         : 'option-hover', /* dropdown list hover class */
    // selectedClass      : 'option-selected', /* dropdown list selected class */
    // markerClass        : 'combo-marker', /* Search marker class */
    // maxHeight          : 200, /* Max height of dropdown */
    // themeClass         : '', /* Theme using external classes */
    // extendStyle        : true /* Copy all inline styles from original select */
  });
  var s_multiple = $('select[multiple]').chosen({
    no_results_text: "Oops, nothing found!",
    display_disabled_options: false,
    width: "80%"
  });
  
  // This helps with accessibility by closing the Combo Select Dropdown once the Select 
  // changes it's value. then the Combo Select Dropdown will register that new value
  // as the selected option
  // $('select').change(function() {
    // var elem = this; 

    // $(elem).siblings('.combo-input').val($(elem).val());
    // $('[data-value="' + $(elem).val() + '"]').addClass('option-selected'); 
    // $('.combo-select').removeClass('combo-open');
  // });

  // Trimming Option Item Text in the Combo Select Widget
  $('.option-item').each(function(index, element) {
    var trimmedTxt = $(element).text().trim();
    $(element).text(trimmedTxt);
  });

  var setMaxWidth = function() {
    $('.chosen-single', '.field').each(function(index, element) {
      var chosenDropdown = $(element).siblings('.chosen-drop');
      var width = $(element).closest('.field').width() - $(element).closest('.chosen-container').siblings('label').width() - 5 + 'px';

      $(element).css('width', width);
      $(chosenDropdown).css('width', width);
    });
  }

  // Nav (from Refills)

  var menuToggle = $('#js-mobile-menu').unbind();
  $('#js-navigation-menu').removeClass("show");

  menuToggle.on('click', function(e) {
    e.preventDefault();
    $('#js-navigation-menu').slideToggle(function() {
      if ($('#js-navigation-menu').is(':hidden')) {
        $('#js-navigation-menu').removeAttr('style');
      }
    });
  });

  // Button groups (custom)

  $("input[name=button-group]").on("click", function() {
    $('.loading-overlay').show();
  });

  // Modal (from refills)

  $(".modal-close").on("click", function() {
    $(".modal-state:checked").prop("checked", false).change();
  });

  $(".modal-inner").on("click", function(e) {
    e.stopPropagation();
  });

  // BUG 22500 RDM - Added modal state functions for confirm modal

  $(".modal-close-confirm").on("click", function() {
    $('.modal-state').prop('checked', false);
  });

  $(".modal-open-confirm").on("click", function() {
    $('.modal-state').prop('checked', true);
  });

  // Tabs from Refills

  $('.accordion-tabs-minimal').each(function(index) {
    var elem = this;
    $(elem).children('a.tab-link').first().addClass('is-active');
    $(elem).children('li').first().find('.tab-content').addClass('is-open').show();
  });

  $('.accordion-tabs-minimal').on('click', 'a.tab-link', function(event) {
    var elem = this;
    var dataIndex = $(elem).attr('data-index');

    if (!$(elem).hasClass('is-active')) {
      event.preventDefault();
      var accordionTabs = $(elem).closest('.accordion-tabs-minimal');
      accordionTabs.find('.is-open').removeClass('is-open').hide();

      $('li[data-index="' + dataIndex + '"]').find('.tab-content').toggleClass('is-open').toggle();
      // $(elem).next().toggleClass('is-open').toggle();
      accordionTabs.find('.is-active').removeClass('is-active');
      $(elem).addClass('is-active');
    } else {
      event.preventDefault();
    }

    setMaxWidth();
  });

  // jQueryvalidate Change Handler
  $('input', 'form').change(function() {
    var elem = this;

    jQueryvalidate(elem);
  });

  $('select', 'form').change(function() {
    var elem = this;
    
    // We do a setTimeout here just to make sure the 
    // ComboSelect plugin has completed its change
    // functions first
    setTimeout(function() {
      jQueryvalidate(elem);
    }, 300);
  });

  // Check For Error Message Function

  function checkForErrorMessage(num) {
    var num = num || 0;

    if ($('[id$="-error"]').length) {
      $('.loading-overlay').hide();
    } else if (num < 10) {
      checkForErrorMessage(num++);
    } else {}
  }

  // Form Submit Handler

  $('form').submit(function(e) {
    $('.global-item-prompt').remove();
    $('.loading-overlay').hide();
    $('.loading-overlay').show();
    var emptyForm = true;

    //Bug 23200 NAM: removed

    $('input', 'form').each(function(index, element) {
      var elem = this;

      jQueryvalidate(elem);

      if ($(elem).val().length > 0) {
        emptyForm = false;
      }
    });

    $('select', 'form').each(function(index, element) {
      var elem = this;

      jQueryvalidate(elem);

      if ($(elem).val().length > 0) {
        emptyForm = false;
      }
    });
      
    // IPAY-1085 UMN fix a11y issues with duplicate labels by label -> div
    if (!emptyForm && (formIsValid || $('.error').text() === "" || $('.results form').length > 0)) {
      //Bug 23200 NAM: removed   
  
      if ($('div.g-recaptcha').length > 0 && grecaptcha.getResponse().length <= 0) {
        $('label[for="captcha"]').text('You must check the checkbox below before continuing.').show();
        $('.loading-overlay').hide();
        return false;
      }

      $('label[for="captcha"]').text('').hide();
      
      // re: || $('.results form').length > 0
      // This allows the results form to pass through
      // the form submit because it is checked before
      // submit. And there could be other inputs that
      // are not selected as a violation to pay that
      // make the form invalid.

      // deletes duplicate
      // query parameters created by mobile layout
      // to avoid form submit errors

      $('[name]').each(function(index, element) {
        var inputtedVal = $(element).val();
        var nameAttr = $(element).attr('name');

        if (inputtedVal.length > 0) {
          $('[name="' + nameAttr + '"]').val(inputtedVal);
        }
        //Bug 23200 NAM: removed
      });
    } else {
      $('.loading-overlay').hide();
      return false;
    }
  });
});

// Bug 23569 NAM: Called by: GEO.cbutton, casl_link in business/include/main.js
function go ( location, target, hide_cover ) {  // show_cover default is true.  show_cover not used??
  if (hide_cover) {} else hide_cover=false;
  
  //lert("go: location: " + location + ", target: " + target);
  var TARGET = window;
  if ( target != null ) {
    if ( typeof(target)=="string" ) {
      TARGET = top[target];
    } else {
      TARGET = target;
    }
  }

  if ( TARGET==window && hide_cover==false && is_defined("cover", TARGET) ) {
    TARGET.cover();
  }

  if ( typeof( location ) == "string" && location.indexOf("__DOUBLESUBMIT__") != -1 ) {
    TARGET.location = location;
  } else {
    TARGET.location = location.href;
  }
}

function is_defined ( variable_name, target ) {
  if ( target == null ) target=window;
  return ( typeof( target[ variable_name ] ) != "undefined" );
}

  // Bug 23080 NAM: functions called when session times out
  function handle_session_timeout (return_uri_logoff) {   
    var uri = window.location.origin + return_uri_logoff;
    go(uri, null, false);
  }
    
  function session_timeout(logout_url, timeout_minutes)
  {
    if ( typeof(TimerID) !== "undefined" && TimerID !== null ){       
         clearTimeout(TimerID);
    }  
    TimerID = setTimeout('handle_session_timeout("' + logout_url + '");', timeout_minutes);  
  }
  //BUG 26123 NAM: validate form before submiting
  function cover_validate(elem)
  {
    $(".search-cancel-button").attr('disabled', true);
    $(".results-cancel-button").attr('disabled', true);
    $(".choose-button").attr('disabled', true);
    $(".results-next-button").attr('disabled', true);
    
    var valid = jQueryvalidate(elem);
    if (valid === true){
          $(elem).submit();
        }
     else{
      $(".search-cancel-button").attr('disabled', false);
      $(".results-cancel-button").attr('disabled', false);
      $(".choose-button").attr('disabled', false);
      $(".results-next-button").attr('disabled', false);      
     }
  }
  //BUG 23488 NAM: prevent double clicking by disabling button
  function cover()
  {
    $(".search-cancel-button").attr('disabled', true);
    $(".results-cancel-button").attr('disabled', true);
    $(".choose-button").attr('disabled', true);
    $(".results-next-button").attr('disabled', true);
  }
  //Bug 22506 NAM: add datepicker to responsive UI
  $(document).ready(function(){
    $( ".date" ).each(function(){
       $( ".date" ).datepicker({ // BUG 24785 RDM 
          beforeShow: function(inst) { 
            if ($(inst).attr('readonly')) { return false; } else { return true; } 
          } 
        }); 
    });   
  });
  //Bug 24708 NAM: prevent navigating back with backspace key 
  function handle_backspace(e) {     
    $(document).keydown(function(e) {
      if ((e.target.readOnly == true) && (e.target.type !== 'textarea')) {
        var code = e.keyCode || e.which;
        if (code === 8) { //BACKSPACE keycode
          //e.cancelBubble is supported by IE - this will kill the bubbling process.
          e.cancelBubble = true;
          e.returnValue = false;
          //e.stopPropagation works only in Firefox.
          if (e.stopPropagation) {
            e.stopPropagation();
            e.preventDefault();
          }
          return false;
        }
      }
    });
  }

function SendReceipt(event) {
  var emailField = document.getElementById("email_receipt");
  var email = emailField.value;
  var isValid = ValidateEmail(email);
  if(isValid) {
    var emailSentField = document.getElementById("email_sent");
    var emailSentToField = document.getElementById("email_sent_to");
    var emailNotSentField = document.getElementById("email_not_sent");
    var emailNotSentToField = document.getElementById("email_not_sent_to");
    var emailPending = document.getElementById("email_pending");
    var emailInvalid = document.getElementById("email_invalid");

    emailPending.style.display = "";
    emailSentField.style.display = "none";
    emailNotSentField.style.display = "none";
    emailInvalid.style.display = "none";

    var dest = window.location.href.replace(".htm", "/send_optional_email.htm");
    console.log(dest);
    var doublesubmit = dest.split("?")[1].split("&")[0];
    dest = dest.substring(0, dest.indexOf("?"));
    var xhr = new XMLHttpRequest();
    xhr.open("POST", dest, true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
          var resp = xhr.response;
          if(resp.toLowerCase().indexOf("invalid") >= 0)
          {
            emailSentToField.innerHTML = "";
            emailSentField.style.display = "none";
            emailPending.style.display = "none";
            emailNotSentToField.innerHTML = email;
            emailNotSentField.style.display = "";
          } else {
            emailSentToField.innerHTML = email;
            emailSentField.style.display = "";
            emailNotSentToField.innerHTML = "";
            emailNotSentField.style.display = "none";
            emailPending.style.display = "none";
          }
        } else if (xhr.readyState === XMLHttpRequest.DONE) {
          emailSentToField.innerHTML = "";
          emailSentField.style.display = "none";
          emailPending.style.display = "none";
          emailNotSentToField.innerHTML = email;
          emailNotSentField.style.display = "";
        }
    }
    // xhr.send("");
    xhr.send("recipient=" + encodeURIComponent(email)  + "&" + doublesubmit);
  }
  event.stopPropagation();
}
function ValidateEmailBlur() {
  var emailField = document.getElementById("email_receipt");
  var sendBtn = document.getElementById("send_email_receipt");
  var emailInvalid = document.getElementById("email_invalid");
  var email = emailField.value;
  var isValid = ValidateEmail(email);
  if(!isValid) {
    emailInvalid.style.display = "block"
    emailField.style.borderColor = "red";
  }
  else {
    emailInvalid.style.display = "none"
    emailField.style.borderColor = "green";
  }
}
function ValidateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}