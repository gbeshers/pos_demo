$(function() {

	// Fieldset button actions

	$('.fieldset-button', '.search-by-title1a-label').click(function(e) {
		e.stopPropagation();
		var elem = this; 

		$('#title1a-fieldset').removeClass('show');
		$('#title1a-fieldset').addClass('show');
		$('#title1b-fieldset').removeClass('show');
	});

	$('.fieldset-button', '.search-by-title1b-label').click(function(e) {
		e.stopPropagation();
		var elem = this; 

		$('#title1b-fieldset').removeClass('show');
		$('#title1b-fieldset').addClass('show');
		$('#title1a-fieldset').removeClass('show');
	});

	// Tooltip Actions

	$('.tooltip-item').click(function(e) {
		var elem = this; 
		$('.tooltip', elem).toggle();
	});
});
