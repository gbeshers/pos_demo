/*
Validation_rules.js
*/
// PB standard validation 13157 

if ($('.results form').length > 0) { 
  $('.results form').validate({
    onsubmit: false, 
    ignore: ':hidden:not("select")', // Bug 23457 RDM: Tells the validator to check the hidden select
    errorPlacement: function(error, element) {
      // Hide the loading overlay if there is an error
      $('.loading-overlay').hide();

      if ($(element).closest('.field').length) {
        $(error).insertBefore($(element).closest('.field'));
      } else {
        $(error).insertBefore($(element));
      }
    }
  });
} else { 
  $('form').validate({
    ignore: ':hidden:not("select")', // Bug 23457 RDM: Tells the validator to check the hidden select
    errorPlacement: function(error, element) {
      // Hide the loading overlay if there is an error
      $('.loading-overlay').hide();

      if ($(element).closest('.field').length) {
        $(error).insertBefore($(element).closest('.field'));
      } else {
        $(error).insertBefore($(element));
      }
    }
  });
}

// START of jquery validation methods or rules mapping to CORE configurable iPayment Custom field Types Plus Required 
// UMN Bug 20642 mobile rewrite so works on old and new UI

// Bug 24311 UMN some changes and documentation
// We use these functions because we don't want to validate rows that won't be submitted to the server.  So
// if this function returns true than the rest of the validation will continue (look at the minlengthInRow validation
// below, it first checks isValidRow). If isValidRow returns false, then validation doesn't continue for that input
// element.

function isValidRow(element) {
  var rowSel = $(element).parent().parent();
  var amountPaidSel = rowSel.find('.prop_RESERVED_AMOUNT_PAID input');
  if (amountPaidSel.length === 0) return false; // Bug 24311 UMN check before applying regex
  var AmountPaid = amountPaidSel.val().replace(/[$,]/g, "");
  if (isNaN(AmountPaid)) return true; 
  var isSelected = rowSel.find('.prop_RESERVED_SELECTED input[type=hidden]').val();
  if (isSelected === true && (AmountPaid === undefined || AmountPaid === 0)) return true;  // Bug 15423 UMN
  if (AmountPaid !== undefined) AmountPaid = parseFloat(AmountPaid.replace(/[$,]/g, ""));
  if (AmountPaid === undefined || AmountPaid == 0) return false;
  else return true;
}

$.validator.addMethod("minlengthInRow", function(value, element, param) {
  // only valid minlength for valid row 
  return (isValidRow(element)) ? this.getLength($.trim(value), element) >= param : true;
}, $.validator.format("Please enter at least {0} characters."));

$.validator.addMethod("maxlengthInRow", function(value, element, param) {
  // only valid maxlength for valid row 
  return (isValidRow(element)) ? this.getLength($.trim(value), element) <= param : true;
}, $.validator.format("Please enter no more than {0} characters."));

jQuery.validator.addMethod("typicaltext", function(value, element) {
  return this.optional(element) || /^[-\w.,()'\"\s!:;\?#&]+$/i.test(value);
}, "Please enter valid characters");

jQuery.validator.addMethod("integernegative", function(value, element) {
  return this.optional(element) || /^-\d+$/.test(value);
}, "A negative integer number please");

jQuery.validator.addMethod("numericwseparators", function(value, element) {
  return this.optional(element) || /^[0-9-\s]+$/i.test(value);
}, "numbers, spaces, dash only please");

jQuery.validator.addMethod("citychars", function(value, element) {
  return this.optional(element) || /^[-\w.\s]+$/i.test(value);
}, "Letters, spaces, dash only please");

jQuery.validator.addMethod("currency", function(value, element) {
  return this.optional(element) || /^-?\$?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d{1,2})?$/.test(value);
}, "Currency only please");

jQuery.validator.addMethod("currencypositive", function(value, element) {
  // $0.00 is not positive, so exit the validation with false
  if (/^\$?0(\.)?0{0,2}$/.test(value)) {
    return false
  }

  return this.optional(element) || /^\$?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d{1,2})?$/.test(value);
}, "Positive Currency only please");

jQuery.validator.addMethod("currencypositivenegative", function(value, element) {
  return this.optional(element) || /^-?\$?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d{1,2})?$/.test(value); //BUG#13435  handle -$1.00
}, "Positive or Negative Currency only please");

jQuery.validator.addMethod("currencypositivezeroandnegative", function(value, element) {
  return this.optional(element) || /^-?\$?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d{1,2})?$/.test(value); //BUG#13435 //BUG#23554
}, "Currency only please");

jQuery.validator.addMethod("currencynegative", function(value, element) {
  return this.optional(element) || /^-\$-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d{1,2})?$/.test(value); //BUG#13435 //BUG#23554
}, "Negative Currency only please");
//Bug 17994 NAM: auto convert no negative amount
jQuery.validator.addMethod("currencyautonegative", function(value, element) {
   input_value = value
   var amt = input_value.replace('-','').replace('$','').replace(',', '');
   var fAmt = parseFloat(amt);		   
   if ( isNaN(fAmt) )
      return false;
      //create negative amount 
   if (fAmt  !=0){
      input_value = "-$".concat(amt);
   }
   element.value = input_value.toLocaleString('en-US', { style: 'currency', currency: 'USD' });   
   return jQuery.validator.methods.currencynegative.call(this, element.value, element);
}, "Negative Currency only please");

jQuery.validator.addMethod("currencynegativeandzero", function(value, element) {
  return this.optional(element) || /^-\$?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d{1,2})?$/.test(value) || /^\$?0(\.)?0{0,2}$/.test(value); //BUG#13435
}, "Negative Currency or Zero only please");

jQuery.validator.addMethod("currencyzero", function(value, element) {
  return this.optional(element) || /^\$?0(\.)?0{0,2}$/.test(value);
}, "Zero Only  please");

jQuery.validator.addMethod("currencypositiveandzero", function(value, element) {
    return this.optional(element) || /^\$?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d{1,2})?$/.test(value);
}, "Positive Currency or Zero Only please");

// New Method Added to check if value is number-only
jQuery.validator.addMethod("numberOnly", function(value, element) {
  if (/[^0-9\-]+/.test(value)) {
    return false;
  } else {
    return true;
  }
}, 'Please enter numbers only.');

var bankRoutingErr = 'ABA Validation error';
var bankAccountNumberErr = 'Invalid bank account number';

function checkBankRoutingNbr(value, element) {
  function routing_checksum(value) {
    if (value.length != 9) {
      bankRoutingErr = "Bank Routing # must have exactly 9 characters.";
      return false;
    }

    // Run through each digit and calculate the total.
    var i, n, t;
    t = value;
    n = 0;

    for (i = 0; i < t.length; i += 3) {
      n += parseInt(t.charAt(i), 10) * 3 + parseInt(t.charAt(i + 1), 10) * 7 + parseInt(t.charAt(i + 2), 10);
    }

    // If the resulting sum is an even multiple of ten (but not zero),
    // the aba routing number is good.
    if (n != 0 && n % 10 == 0.00)
      return true;
    else
      bankRoutingErr = "Not a valid routing number.";
    return false;
  }

  var chk = routing_checksum(value);

  if (chk !== true) {
    // IPAY-1085 UMN fix a11y issues with duplicate labels by label -> div
    $('.error[for*=bank_name]').html(chk);
    $(".bank_name").val('');
    $(".bank_routing_nbr").focus();
    bankRoutingErr = chk;
    return false;
  }

  var oArgs = {}; 

  oArgs['validateValue'] = value;

  //Bug 26122 NAM: call server to validate ABA only when data-aba-url is provided
  if ((element.getAttribute("data-aba-url"))&&(element.getAttribute("data-aba-url").trim()!='')){
    $.ajax({
      "url": element.getAttribute("data-aba-url").replace(/\?.*$/, ""),
      "data": oArgs,
      "success": function(data) {
        if (data.ResultType == "Success") {
          // IPAY-1085 UMN fix a11y issues with duplicate labels by label -> div
          $('.error[for*=bank_name]').html('');
          $('.error[for*=bank_routing_nbr]').html('');
          $('.bank_routing_nbr').removeClass("error");
          $(".bank_name").val(data.Result.bank_name);
          return true;
        } else if (data.ResultType == "Failure") {
          $(".bank_name").val('');
          $('.error[for*=bank_name]').html(data.ErrorSummary);
          $(".bank_routing_nbr").focus();
          bankRoutingErr = data.ErrorSummary;
          return false;
        } else {
          
        }
      },
      "dataType": "json",
      "cache": false,
      "error": function(xhr, error, thrown) {
        console.log(error);
        return false; 
      }
    });
  }

  return true; 
}

// New Class Rule Added for ABA validation
jQuery.validator.addMethod("bankRoutingNbr", function(value, element) {
    return checkBankRoutingNbr(value, element);
}, bankRoutingErr);

  //Bug 23522 NAM: compare input fields value and return 'dynamic' response containing field label and message
  (function() {
      var msg = "";
      var ruleMsg = function() {
        return msg;
      };
      // New Class Rule Added for required second entry matching/validation
      jQuery.validator.addMethod("compareInputFields", function(value, element) {
        msg = "";        
        var enter_field = $(element).attr('name').replace(/2$/, '');
        var reenter_field = $(element).attr('name');    
        var firstfieldvalue = $('input[name="'+ enter_field +'"]').val();
        if (typeof firstfieldvalue == 'undefined' || firstfieldvalue == (null || "")){
          return true;  //nothing to compare yet
        }            
        var secondfieldvalue = $('input[name="'+reenter_field+'"]').val();
        // Compare fields
        if (typeof secondfieldvalue !== 'undefined' && secondfieldvalue !== (null || "")
            && (firstfieldvalue != secondfieldvalue)) {             
              msg = $('label[for="'+ reenter_field +'"]').last().first().text() + " - fields must be the same."; //extract field label
            return false;     
        }       
        return true;
    }, ruleMsg);   //call function and return latest message
  })();

jQuery.validator.addMethod("creditCardCheck", function(value, key) {
  // value is the passed CC number; eg: "6767"
  // key is the id of the form element; eg: "key "args.credit_card_nbr". is apparently unused.

  var s = value;
  var i, n, c, r, t;
  r = "";
  for (i = 0; i < s.length; i++) {
    c = parseInt(s.charAt(i), 10);
    if (c >= 0 && c <= 9)
    r = c + r;
  }
  t = "";
  for (i = 0; i < r.length; i++) {
    c = parseInt(r.charAt(i), 10);
    if (i % 2 != 0)
    c *= 2;
    t = t + c;
  }
  n = 0;
  for (i = 0; i < t.length; i++) {
    c = parseInt(t.charAt(i), 10);
    n = n + c;
  }
  if (n != 0 && n % 10 == 0)
    return true;
  else
    return false;
}, "Please enter a valid credit card number.");

//Bug 25774 NAM: check ccv field and return 'dynamic' message with field label
(function(){
  var msg = "fID must be exaclty fLen characters.";
  var ruleMsg = function(){
    return msg;
  };
  // New Class Rule Added for CCV validation
  jQuery.validator.addMethod("ccvCheck", function(value, element) {
    var maxLen = $(element).attr('maxlength');
    var minLen = $(element).attr('minlength');
    var len = value.length; 
    if (parseInt(minLen) == 0 && (parseInt(len) <  parseInt(maxLen))){
        var ccvField = $(element).attr('name');
        var ccvLabel = $('label[for="'+ ccvField +'"]').last().first().text(); //extract field label
        msg = msg.replace('fID', ccvLabel).replace('fLen', maxLen);
        return false;
    }
    else if (parseInt(len) <  parseInt(minLen)){
        var ccvField = $(element).attr('name');
        var ccvLabel = $('label[for="'+ ccvField +'"]').last().first().text(); //extract field label
        msg = ccvLabel + " must be at least " + minLen + " characters in length.";
        return false;
    }
    return true;
  }, ruleMsg); //return dynamic message
})();

jQuery.validator.addMethod("gRecaptchaResponse", function(value, element) {
  var captchaSel = $('#g-recaptcha-response');
  if (captchaSel.length === 0) return true;
  if (captchaSel.val().length === 0) return false;
  grecaptcha.reset();
  return true;
}, 'Please solve the captcha.');

// Bug 23124 NAM: hide/show state & zip labels based on country
jQuery.validator.addMethod("countryCheck", function(value, element) {
  var countrySel = $("#args\\.country");
  var stateSel = $("#args\\.state");
  var zipSel = $("#args\\.zip");
  var country = countrySel.val();   //Bug 27148 NAM
  if (country === "US" || country === "United States" || country === "CA" || country === "Canada"){ 
    //make zip and state required and add asterisk
    zipSel.addClass("required"); 
    zipSel.parent().addClass("required"); 
    stateSel.addClass('required');  //Bug 23124 NAM: rule based on country
    stateSel.parent().parent().addClass("required");
    $('label[for="args.zip"]').first().show();
    $('label[for="args.state"]').first().show();	
  }
  else{
    //make zip and state not required and remove asterisk
    zipSel.removeClass('required');
    zipSel.parent().removeClass("required");
    stateSel.removeClass('required');   //Bug 23124 NAM: rule based on country   
    stateSel.parent().parent().removeClass("required");
    $('label[for="args.zip"]').first().hide();
    $('label[for="args.state"]').first().hide();
  }
  return true;
}, 'The specified COUNTRY Code is invalid.');

// Bug 20135 UMN //Bug 22996 NAM
jQuery.validator.addMethod("zipCheck", function(value, element) {
  var res = true;
  var countrySel = $("#args\\.country");
  var zipSel = $("#args\\.zip");
  var country = countrySel.val();   //Bug 27148 NAM
  if (country === "US" || country === "United States" || country === "CA" || country === "Canada"){ 
  //Bug 23534 NAM: clear zip field error label before checking again
  //Bug 22545 RDM: only clear the zip error label for when the error = "The specified ZIP Code is invalid." Don't want to delete the "Required" msg
  if ($('label[for="args.zip"]').first()[0].textContent == "The specified ZIP Code is invalid.") $('label[for="args.zip"]').first()[0].textContent = "";  
	if (country === "US" || country === "United States") {  //Bug 27148 NAM
		res = jQuery.validator.methods.zipcodeUS.call(this, $(zipSel).val(), $(zipSel)[0]);
	}
	else if(country === "CA" || country === "Canada"){  //Bug 27148 NAM
		res = jQuery.validator.methods.postalCodeCA.call(this, $(zipSel).val().toUpperCase(), $(zipSel)[0]); //Bug 23532 NAM
	}
  }
  return res;
}, 'The specified ZIP Code is invalid.');

// Bug 22502 UMN //Bug 22652 NAM
jQuery.validator.addMethod("ccExpDateCheck", function(value, element) {
  var expMonSel = $("#args\\.exp_month");
  var expYearSel = $("#args\\.exp_year");
  var m = $(expMonSel).parent().children("input").val();
	var y = $(expYearSel).parent().children("input").val();
	//Bug 22652 NAM: correct invalid entries
	if ($.isNumeric(m) === true){
		//BUG 22652 NAM: if month entered is not in drop down default to current month
		if (($("#args\\.exp_month option[value=" + m + "]").length > 0) == false){
			var month = new Date().getMonth();
			$("#args\\.exp_month :nth(" + month + ")").prop("selected","selected").change()
		}	
	}
	else if ($.isNumeric(m) === false || m == ''){
		//BUG 22652 NAM: if month entered is not numeric default to current month
		var month = new Date().getMonth();
		$("#args\\.exp_month :nth(" + month + ")").prop("selected","selected").change()	
	}
	if ($.isNumeric(y) === true){
		//BUG 22652 NAM: if year entered is not in drop down default to first entry
		if (($("#args\\.exp_year option[value=" + y + "]").length > 0) == false){
			$("#args\\.exp_year :nth(0)").prop("selected","selected").change()
		}		
	}
	else if ($.isNumeric(y) === false || y == ''){
		//BUG 22652 NAM: if year entered is not numeric default to first entry
		$("#args\\.exp_year :nth(0)").prop("selected","selected").change()	
	}

  // Fix user input.
  var FullYear = '20';
  if (y.length == 1)
    FullYear = '200';
  
  var UserDate = new Date(FullYear+y, m-1 /*zero based*/);
  var now = new Date();
  var TodayDate = new Date(now.getFullYear(), now.getMonth());
    
  if (UserDate < TodayDate)
    return false;

  $(expMonSel).removeClass("error");
  $(expYearSel).removeClass("error");
  return true;
}, 'Expired Date.');

jQuery.validator.addClassRules({
  requiredInRow: { required: { depends: function(element) {
        return isValidRow(element) } } },
  typicaltextInRow: { typicaltext: { depends: function(element) {
        return isValidRow(element) } } },
  integernegativeInRow: { integernegative: { depends: function(element) {
        return isValidRow(element) } } },
  numericwseparatorsInRow: { numericwseparators: { depends: function(element) {
        return isValidRow(element) } } },
  citycharsInRow: { citychars: { depends: function(element) {
        return isValidRow(element) } } },
  currencyInRow: { currency: { depends: function(element) {
        return isValidRow(element) } } },
  currencypositiveInRow: { currencypositive: { depends: function(element) {
        return isValidRow(element) } } },
  currencypositivenegativeInRow: { currencypositivenegative: { depends: function(element) {
        return isValidRow(element) } } },
  currencypositivezeroandnegativeInRow: { currencypositivezeroandnegative: { depends: function(element) {
        return isValidRow(element) } } },
  currencynegativeInRow: { currencynegative: { depends: function(element) {
        return isValidRow(element) } } },
  currencynegativeandzeroInRow: { currencynegativeandzero: { depends: function(element) {
        return isValidRow(element) } } },
  currencyzeroInRow: { currencyzero: { depends: function(element) {
        return isValidRow(element) } } },
  currencypositiveandzeroInRow: { currencypositiveandzero: { depends: function(element) {
        return isValidRow(element) } } },
  letterswithbasicpuncInRow: { letterswithbasicpunc: { depends: function(element) {
        return isValidRow(element) } } },
  digitsInRow: { digits: { depends: function(element) {
        return isValidRow(element) } } },
  numberInRow: { number: { depends: function(element) {
        return isValidRow(element) } } },
  dateInRow: { date: { depends: function(element) {
        return isValidRow(element) } } },
  emailInRow: { email: { depends: function(element) {
        return isValidRow(element) } } },
  lettersonlyInRow: { lettersonly: { depends: function(element) {
        return isValidRow(element) } } },
  alphanumericInRow: { alphanumeric: { depends: function(element) {
        return isValidRow(element) } } },
  integerInRow: { integer: { depends: function(element) {
        return isValidRow(element) } } },
  phoneUSInRow: { phoneUS: { depends: function(element) {
        return isValidRow(element) } } },
  time12hInRow: { time12h: { depends: function(element) {
        return isValidRow(element) } } }
});

// New Class Rule Added to check if value is number-only
jQuery.validator.addClassRules("number-only", {
  numberOnly: true
});

// New Class Rule Added to check if value is number-only
jQuery.validator.addClassRules("credit_card_nbr", {
  creditCardCheck: true
  // numberOnly: true
});

// New Class Rule Added to check if value meets ccv requirement 
jQuery.validator.addClassRules("ccv", {
  numberOnly: true,
  ccvCheck: true
});

// New Class Rule Added for ABA validation
jQuery.validator.addClassRules("bank_routing_nbr", {
  bankRoutingNbr: true
});

// New Class Rule Added for Bank Account Nbr Confirm
/* jQuery.validator.addClassRules("bank_account_nbr", {
  bankAccountNbr: true
});
 */
// New Class Rule Added for Bank Account Nbr Confirm
jQuery.validator.addClassRules("check_duplicate_fields", {
  compareInputFields: true
});

jQuery.validator.addClassRules("g-recaptcha-response", {
  gRecaptchaResponse: true
});

// Bug 20135 UMN only check zip code if country is US
jQuery.validator.addClassRules("zipcode", {
  zipCheck: true
});

//BUG 23124 NAM: make state/zip required based on country
// Bug 20135 UMN revalidate zip if country changes
jQuery.validator.addClassRules("country", {
  zipCheck: true,    //Bug 23534 NAM: revalidate zip when country changes
  countryCheck: true
});

// Bug 23124 NAM revalidate zip if state changes
jQuery.validator.addClassRules("state", {
  zipCheck: true
});

jQuery.validator.addClassRules("exp_year", {
  ccExpDateCheck: true
});

// Bug 20135 UMN revalidate CC year if month changes
jQuery.validator.addClassRules("exp_month", {
  ccExpDateCheck: true
});

// END of jquery validation methods or rules 


// Check whether page is valid and return error message
// Called by validate_and_cover()
function jQueryvalidate(tag) {
  bShowWarningMessage = true;
  //BUG 22652 NAM: check exp_month/exp_year values selected/entered
	//this call will correct and update values on the page once focus moves away from field
  expDateCheck(tag);
  formIsValid = $(tag).valid();

  if (formIsValid) {
    bShowWarningMessage = false;
    return true;
  } else {
    bShowWarningMessage = false;
    var validator = $(tag).validate();
    $('.loading-overlay').hide();
    if (validator.errorList.length > 0) return validator.errorList[0].message;
    else return 'Unknown error in jQuery validation';
  }
}
//BUG 22652 NAM: verify values entered directly in exp_month/year drop down inputs
function expDateCheck(tag) {
	if ($("tag .combo-input,.text-input").length > 0){
		var expMonth = $(tag).siblings(".exp_month").attr("id");
		var expYear = $(tag).siblings(".exp_year").attr("id");
		if (expMonth === "args.exp_month" || expYear === "args.exp_year") {
			var bValidDate = jQuery.validator.methods.ccExpDateCheck.call(this,tag);				
		}
	} 
}
