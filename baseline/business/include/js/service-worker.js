/**
 * Copyright 2015 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// This generated service worker JavaScript will precache your site's resources.
// The code needs to be saved in a .js file at the top-level of your site, and registered
// from your pages in order to be used. See
// https://github.com/googlechrome/sw-precache/blob/master/demo/app/js/service-worker-registration.js
// for an example of how you can register this script and handle various service worker events.

/* eslint-env worker, serviceworker */
/* eslint-disable indent, no-unused-vars, no-multiple-empty-lines, max-nested-callbacks, space-before-function-paren */
'use strict';


importScripts("js/sw/sw-toolbox.js","js/sw/runtime-caching.js");


/* eslint-disable quotes, comma-spacing */
var PrecacheConfig = [["css/chosen.css","4a988558b63fc40014366d50bc4e3cb0"],["css/fontastic.css","cd769b2d7b5664e8b79c372e180c355d"],["css/main.css","11301f8cb1fad36cbd9ed6d7465e0807"],["img/amex.png","8a5ade08365dcc7e5fa39a946bb76ab8"],["img/arrow-active.png","97684fe1216dc0b28a6f22ce0d6955bf"],["img/arrow-down-thin.svg","61b4b38a0eecd06ed5749531bcbf4a09"],["img/arrow-inactive.png","adf38913173461b8be81aac4f7be9350"],["img/arrow-right-thin.svg","c06a89a8726a5fa54723ab8b107c89dc"],["img/back-arrow-icon.png","ecae14d2d60f58e1e1528e489caf48fd"],["img/bkg.jpg","dc7be62d5320252d1afa483b97240532"],["img/cancel-x-icon.png","de7106f6612d6b9a422cc82dec9dbf41"],["img/checking_example.png","04df96137c814b4c5190bca09a7686a9"],["img/corebt.gif","61cab98ee9595225be17d8434a5fc939"],["img/credit.png","cec82a3f2606773b46c3ce89ab549a1d"],["img/dankort.png","0cd32291ec69bc2ba0e9d9f3e59e4ed5"],["img/diners.png","03afaaa2d75264e332dc28309b7410b9"],["img/discover.png","f89468f36ba1a9080b3bb05b9587d470"],["img/forbru.png","9e44dd9a096de2eac4642a85eb19ae3a"],["img/google.png","8c474ff5a88dc4cf62e73e233d579589"],["img/jcb.png","58f43e5f1fb8c6a4e7e76a17e7824e29"],["img/laser.png","9215492fc8de12e639913e979116a2d3"],["img/lock-icon.png","65176a7f3a9932c5f60a159055f5347e"],["img/logo_nyc.png","3759fc72030d27d5841ea0e4bb4cfd8f"],["img/maestro.png","6996b1fd5be9bf94f076a125af016d28"],["img/mastercard.png","373e4f1ac72b50605183e8216edde845"],["img/menu-icon.png","31cc6d1d91528ef77e8ef88468b98751"],["img/minus-icon.png","552ae53ae500e7c037fa0a497c3ffe74"],["img/money.png","863fb37d47d8c8510f4205c2da07a861"],["img/next-arrow-icon.png","8246eafc93f137b169cc9f2c6ef0b7ab"],["img/paypa.png","dd5d7dd7e530593b4562fd6984396788"],["img/plus-icon.png","4b2719e3c94f5b5bf619f920b12de8e6"],["img/search-icon.png","f77cce5b84855ede32e07d053fc1f169"],["img/search-icon_gray.png","e4af56886562f41eb7fad583e6c732fc"],["img/shopify.png","cfa8f223e6cb0d2b3bbd8202a81de20b"],["img/solo.png","3d804c2e4acbcab82f51836aaa8b44e3"],["img/spinner.gif","4aacc375a626c4f8be24fedc42cdba3a"],["img/spinner2.gif","40dff4328c82472f7fa6e02332802663"],["img/visa.png","26bcf191ee12e711aa540ba8d0c901b7"],["js/main.min.js","209d151e0bdcdcb7c816227d12a3546a"],["js/sw/runtime-caching.js","e3e34dcb62b5d62453b9215961585488"],["js/sw/sw-toolbox.js","42dd9073ba0a0c8e0ae2230432870678"]];
/* eslint-enable quotes, comma-spacing */
var CacheNamePrefix = 'sw-precache-v1-web-starter-kit-' + (self.registration ? self.registration.scope : '') + '-';


var IgnoreUrlParametersMatching = [/^utm_/];



var addDirectoryIndex = function (originalUrl, index) {
    var url = new URL(originalUrl);
    if (url.pathname.slice(-1) === '/') {
      url.pathname += index;
    }
    return url.toString();
  };

var getCacheBustedUrl = function (url, now) {
    now = now || Date.now();

    var urlWithCacheBusting = new URL(url);
    urlWithCacheBusting.search += (urlWithCacheBusting.search ? '&' : '') + 'sw-precache=' + now;

    return urlWithCacheBusting.toString();
  };

var populateCurrentCacheNames = function (precacheConfig,
    cacheNamePrefix, baseUrl) {
    var absoluteUrlToCacheName = {};
    var currentCacheNamesToAbsoluteUrl = {};

    precacheConfig.forEach(function(cacheOption) {
      var absoluteUrl = new URL(cacheOption[0], baseUrl).toString();
      var cacheName = cacheNamePrefix + absoluteUrl + '-' + cacheOption[1];
      currentCacheNamesToAbsoluteUrl[cacheName] = absoluteUrl;
      absoluteUrlToCacheName[absoluteUrl] = cacheName;
    });

    return {
      absoluteUrlToCacheName: absoluteUrlToCacheName,
      currentCacheNamesToAbsoluteUrl: currentCacheNamesToAbsoluteUrl
    };
  };

var stripIgnoredUrlParameters = function (originalUrl,
    ignoreUrlParametersMatching) {
    var url = new URL(originalUrl);

    url.search = url.search.slice(1) // Exclude initial '?'
      .split('&') // Split into an array of 'key=value' strings
      .map(function(kv) {
        return kv.split('='); // Split each 'key=value' string into a [key, value] array
      })
      .filter(function(kv) {
        return ignoreUrlParametersMatching.every(function(ignoredRegex) {
          return !ignoredRegex.test(kv[0]); // Return true iff the key doesn't match any of the regexes.
        });
      })
      .map(function(kv) {
        return kv.join('='); // Join each [key, value] array into a 'key=value' string
      })
      .join('&'); // Join the array of 'key=value' strings into a string with '&' in between each

    return url.toString();
  };


var mappings = populateCurrentCacheNames(PrecacheConfig, CacheNamePrefix, self.location);
var AbsoluteUrlToCacheName = mappings.absoluteUrlToCacheName;
var CurrentCacheNamesToAbsoluteUrl = mappings.currentCacheNamesToAbsoluteUrl;

function deleteAllCaches() {
  return caches.keys().then(function(cacheNames) {
    return Promise.all(
      cacheNames.map(function(cacheName) {
        return caches.delete(cacheName);
      })
    );
  });
}

self.addEventListener('install', function(event) {
  var now = Date.now();

  event.waitUntil(
    caches.keys().then(function(allCacheNames) {
      return Promise.all(
        Object.keys(CurrentCacheNamesToAbsoluteUrl).filter(function(cacheName) {
          return allCacheNames.indexOf(cacheName) === -1;
        }).map(function(cacheName) {
          var urlWithCacheBusting = getCacheBustedUrl(CurrentCacheNamesToAbsoluteUrl[cacheName],
            now);

          return caches.open(cacheName).then(function(cache) {
            var request = new Request(urlWithCacheBusting, {credentials: 'same-origin'});
            return fetch(request).then(function(response) {
              if (response.ok) {
                return cache.put(CurrentCacheNamesToAbsoluteUrl[cacheName], response);
              }

              console.error('Request for %s returned a response with status %d, so not attempting to cache it.',
                urlWithCacheBusting, response.status);
              // Get rid of the empty cache if we can't add a successful response to it.
              return caches.delete(cacheName);
            });
          });
        })
      ).then(function() {
        return Promise.all(
          allCacheNames.filter(function(cacheName) {
            return cacheName.indexOf(CacheNamePrefix) === 0 &&
                   !(cacheName in CurrentCacheNamesToAbsoluteUrl);
          }).map(function(cacheName) {
            return caches.delete(cacheName);
          })
        );
      });
    }).then(function() {
      if (typeof self.skipWaiting === 'function') {
        // Force the SW to transition from installing -> active state
        self.skipWaiting();
      }
    })
  );
});

if (self.clients && (typeof self.clients.claim === 'function')) {
  self.addEventListener('activate', function(event) {
    event.waitUntil(self.clients.claim());
  });
}

self.addEventListener('message', function(event) {
  if (event.data.command === 'delete_all') {
    console.log('About to delete all caches...');
    deleteAllCaches().then(function() {
      console.log('Caches deleted.');
      event.ports[0].postMessage({
        error: null
      });
    }).catch(function(error) {
      console.log('Caches not deleted:', error);
      event.ports[0].postMessage({
        error: error
      });
    });
  }
});


self.addEventListener('fetch', function(event) {
  if (event.request.method === 'GET') {
    var urlWithoutIgnoredParameters = stripIgnoredUrlParameters(event.request.url,
      IgnoreUrlParametersMatching);

    var cacheName = AbsoluteUrlToCacheName[urlWithoutIgnoredParameters];
    var directoryIndex = 'index.html';
    if (!cacheName && directoryIndex) {
      urlWithoutIgnoredParameters = addDirectoryIndex(urlWithoutIgnoredParameters, directoryIndex);
      cacheName = AbsoluteUrlToCacheName[urlWithoutIgnoredParameters];
    }

    var navigateFallback = '';
    // Ideally, this would check for event.request.mode === 'navigate', but that is not widely
    // supported yet:
    // https://code.google.com/p/chromium/issues/detail?id=540967
    // https://bugzilla.mozilla.org/show_bug.cgi?id=1209081
    if (!cacheName && navigateFallback && event.request.headers.has('accept') &&
        event.request.headers.get('accept').includes('text/html')) {
      var navigateFallbackUrl = new URL(navigateFallback, self.location);
      cacheName = AbsoluteUrlToCacheName[navigateFallbackUrl.toString()];
    }

    if (cacheName) {
      event.respondWith(
        // Rely on the fact that each cache we manage should only have one entry, and return that.
        caches.open(cacheName).then(function(cache) {
          return cache.keys().then(function(keys) {
            return cache.match(keys[0]).then(function(response) {
              if (response) {
                return response;
              }
              // If for some reason the response was deleted from the cache,
              // raise and exception and fall back to the fetch() triggered in the catch().
              throw Error('The cache ' + cacheName + ' is empty.');
            });
          });
        }).catch(function(e) {
          console.warn('Couldn\'t serve response for "%s" from cache: %O', event.request.url, e);
          return fetch(event.request);
        })
      );
    }
  }
});

