function user_analytics_init(username) {
    var pendo_client_key = "d08f6c1e-52b0-467b-5d39-12e1487d9bd3";

    console.log("username: ", username);
    var global_use_cached_pseudo_page = false;
    var global_pseudo_page = null;
    (function(apiKey){
        (function(p,e,n,d,o){var v,w,x,y,z;o=p[d]=p[d]||{};o._q=[];
        v=['initialize','identify','updateOptions','pageLoad'];for(w=0,x=v.length;w<x;++w)(function(m){
            o[m]=o[m]||function(){o._q[m===v[0]?'unshift':'push']([m].concat([].slice.call(arguments,0)));};})(v[w]);
            y=e.createElement(n);y.async=!0;y.src='https://cdn.pendo.io/agent/static/'+apiKey+'/pendo.js';
            z=e.getElementsByTagName(n)[0];z.parentNode.insertBefore(y,z);})(window,document,'script','pendo');

            // Call this whenever information about your visitors becomes available
            // Please use Strings, Numbers, or Bools for value types.
            pendo.initialize({
                visitor: {
                    id: username   // Required if user is logged in
                    // email:        // Recommended if using Pendo Feedback, or NPS Email
                    // full_name:    // Recommended if using Pendo Feedback
                    // role:         // Optional

                    // You can add any additional visitor level key-values here,
                    // as long as it's not one of the above reserved names.
                },

                account: {
                    id: 'iPayment'   // Highly recommended
                    // name:         // Optional
                    // is_paying:    // Recommended if using Pendo Feedback
                    // monthly_value:// Recommended if using Pendo Feedback
                    // planLevel:    // Optional
                    // planPrice:    // Optional
                    // creationDate: // Optional

                    // You can add any additional account level key-values here,
                    // as long as it's not one of the above reserved names.
                },

                annotateUrl: function() {
                    if(global_use_cached_pseudo_page === false) {
                    console.log("tagging page");
                    var pseudo_user_analytics_page_nodes = document.querySelectorAll('[data-user_analytics-page]');
                    var pseudo_user_analytics_page_node = pseudo_user_analytics_page_nodes[pseudo_user_analytics_page_nodes.length - 1];
                    var pseudo_user_analytics_page = pseudo_user_analytics_page_node.dataset.user_analyticsPage;
                    global_pseudo_page = pseudo_user_analytics_page;
                    global_use_cached_pseudo_page = true;
                    console.log("Page set to: ", pseudo_user_analytics_page);
                    return { page: pseudo_user_analytics_page };
                    } else {
                    return { page: global_pseudo_page };
                    }
                }
            });
    })(pendo_client_key);
}

function changePseudoPage(userAnalyticsPageNodeId, newPageName) {
  var userAnalyticsPageNode = document.querySelector("#" + userAnalyticsPageNodeId);
  if(userAnalyticsPageNode != null) {
    userAnalyticsPageNode.dataset.userAnalyticsPage = newPageName;
    global_use_cached_pseudo_page = false;
  }
}