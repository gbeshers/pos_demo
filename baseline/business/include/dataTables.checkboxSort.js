/*
 * Function: checkbox-asc and checkbox-desc, checkbox sorting plug-ins
 * Purpose:  Sorts based on whether checked attribute is present or not 
 * Returns:  -1, 1, or 0
 */
 
 // handle different browsers. Some just define ' checked ' in the input element's HTML,
 // others use ' checked="checked" '.
 var CKCHECKED = /\schecked(=|\s)/i;
 
jQuery.fn.dataTableExt.oSort['checkbox-desc'] = function(a,b) {
	var x = CKCHECKED.test(a);
	var y = CKCHECKED.test(b);
	return ((x < y) ? -1 : ((x > y) ? 1 : 0));
};

jQuery.fn.dataTableExt.oSort['checkbox-asc'] = function(a,b) {
	var x = CKCHECKED.test(a);
	var y = CKCHECKED.test(b);
	return ((x < y) ? 1 : ((x > y) ? -1 : 0));
};