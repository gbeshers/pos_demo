
// This file: business/include/main.js   

// UMN add endsWith if doesn't exist in browser
if (!String.prototype.endsWith) {
  String.prototype.endsWith = function(searchString, position) {
      var subjectString = this.toString();
      if (typeof position !== 'number' || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
        position = subjectString.length;
      }
      position -= searchString.length;
      var lastIndex = subjectString.lastIndexOf(searchString, position);
      return lastIndex !== -1 && lastIndex === position;
  };
}

var use_client = false;
var current_obj = null;
//var wait_time_for_session_expiration_notice = 90000;  // msec  default: 90000 // Bug #11869 Mike O - Not used
// Bug #8668 Mike O - Fix keep_alive for session expiration
var keep_alive_interval = 30000; // msec default: 30000 // Bug #11869 Mike O - Bumped up max
var keep_alive_interval_id = 0;
var keep_alive_request_pending = false; // Bug #11731 Mike O - Don't send out multiple keep alive requests
//BUG 4368 BLL 9/11/08
var bExpanded=false;
var bExpandedSet=false;
var lastPostRemote="";
var bStopProgressBar = false; // Bug 17505 MJO - Stop processing bar when wait dialog is hidden

var tags_tabs = null;

// Bug #7638 -- Queue for validating inputs
var validation_queue = new Array();

// Bug 16648 MJO - Return this with all requests
var double_submit_value = null;

// Bug 23116 MJO - Prevent processing bar from drawing over messages
var prevent_wait_dialog = false;

function make_xml_request() {
  var xml_obj=null;
	if (window.XMLHttpRequest) {
		xml_obj=new XMLHttpRequest();
	} else if (window.ActiveXObject) {
		xml_obj = new ActiveXObject("Microsoft.XMLHTTP");
	}
	return xml_obj;
}

// Bug #8668 Mike O - Brought keep_alive back
function keep_alive() {
  // Bug #11731 Mike O - Don't send out multiple keep alive requests
  if (keep_alive_request_pending)
    return;
  
  // Bug #11880 Mike O - Handle undefined main
  if (get_top().main === null || get_top().main === undefined) // Business Center
    var element = get_top().document.getElementById('session_validator');
  else
    var element = get_top().main.document.getElementById('session_validator');

  if (element === null)
    return;
  
  var args = {};
  args['app_name'] = element.getAttribute('app_name');
  args['id'] = element.getAttribute('app_id');

  post_remote("GEO", "remote_keep_alive", args, "session_validator_frame", true);
  
  // Bug #11731 Mike O - Don't send out multiple keep alive requests
  keep_alive_request_pending = true;
}


//********************************************************************************************************
// BUG# 7564 Batch Update DH
var g_BatchUpdateProgressWindow = null;
var g_xml_obj=null;
var g_bu_progress_handler=null;
function BatchUpdateWithProgressBar() {
    if (g_BatchUpdateProgressWindow == null) {
    g_bu_progress_handler=get_current_site() + "/get_batch_update_activity_progress.htm?";
    
    if(window.XMLHttpRequest) 
      g_xml_obj = new XMLHttpRequest()
    else if (window.ActiveXObject) 
      g_xml_obj = new ActiveXObject("Microsoft.XMLHTTP"); 
  
    var width = 600;
    var height = 150;
    var left = parseInt((screen.availWidth/2) - (width/2));
    var top = parseInt((screen.availHeight/2) - (height/2));
    var WinParams = "titlebar=0,resizable=no, scrollbars=0," + "width=" + width + ",height=" + height + ",status=0,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;
    g_BatchUpdateProgressWindow = window.open(get_current_site()+'/GEO?/'/*BUG# 11062 DH - Make sure we use SSL.*/, "BUWin", WinParams);
    g_BatchUpdateProgressWindow.document.write('<H3> Batch Update in Progress. Please Wait...</H3>');
    var t=setTimeout("GetProgressBarUpdate()",1000);
    return;
  }
}

function GetProgressBarUpdate() {
  // If the window does not exist then get out.
  if (g_BatchUpdateProgressWindow == null)
    return;
    
  if (typeof resetTimeout == 'function') resetTimeout();// BUG# 9001 DH - Don't timeout while processing batch update.
  
  var strWindowContents = '<H3> Batch Update in Progress. Please Wait...</H3>'; 
    
  g_xml_obj.open("POST", g_bu_progress_handler, false);
  g_xml_obj.send();
  var strRV = g_xml_obj.responseText;
  
  // Finish updating.
    if (strRV == "end") {
    g_BatchUpdateProgressWindow.close();
    
    // Now run the report
    RunBatchUpdateReport();
    return;
  }
  
  // Begin Bug 15464 - Batch Update Overhaul: DJD
  // The following will display an error message and exit the BU processing screen (No report displayed).
  // Error message should be formatted as follows: "end_bu_error: This is a test error message."
  var RVlen = strRV.length;
    if (RVlen > 13) {
    var strERR = strRV.substr(0, 13);
        if (strERR == "end_bu_error:") {
      g_BatchUpdateProgressWindow.close();
      
      // Get the Error Message
      strERR = strRV.substr(13);
      strERR = trim(strERR);
      
      // Display the Error Message
      alert(strERR);

      // Exit the web page without running BU report
      if (typeof (get_top().main) != "undefined")
        get_top().main.document.getElementById("exit_batchupdate_process").click();
      else
        get_top().document.getElementById("exit_batchupdate_process").click();
      return;
    }
  }
  // End Bug 15464
  
  g_BatchUpdateProgressWindow.document.close();
  g_BatchUpdateProgressWindow.document.write(strWindowContents + "<center>" + strRV + "</center>");
  
  var t=setTimeout("GetProgressBarUpdate()",500);// Refresh the report data
}

// Bug #10538 Mike O - Changed to just click a hidden cbutton instead of using post_remote
//                     This way, page structure is not messed up when showing the report
function RunBatchUpdateReport() {

//Bug 13873-use jquery approach to access the hidden hyperlink.the former approach does not work in non-IE browser.
    if (jQuery) {        
        $('#finish_batchupdate_process')[0].click();
    }
    else {

  // Bug #12648 Mike O - Made this work when the frame structure is not in place (eg. opened from Cashiering)
  if (typeof(get_top().main) != "undefined")
  get_top().main.document.getElementById("finish_batchupdate_process").click();
  else
    get_top().document.getElementById("finish_batchupdate_process").click();
}
}

// end BUG# 7564 DH
//********************************************************************************************************


// BUG 20115 MALCOLM BYRD - Add Javascript function for handling barcode scan input (pos)
// Input data is now padded with pre- and post-ambles '~' and '&'
// Bug 24482 MJO - e355 barcode scanning support
function EnhancedScanData(valid_doc, document_type, device_name, prompt, ocr_coordinate_1, ocr_coordinate_2, ocr_coordinate_3, ocr_coordinate_4, has_point_scanner, pertech_ocr_recognizer/*Bug 25830 DH*/) // Bug# 21370 DH - Added scan doc and the ability to process OCR document in the device
{
  var scanvalue = get_top().main.document.getElementById("scanner").value;// Bug# 21370 DH
      
  // Bug# 21370 DH
  if ((scanvalue == "" || scanvalue == undefined) && !has_point_scanner)
  {
    // Bug 24160 - Check "false"
    if (!valid_doc || valid_doc == "false")
    {
      // Bug# 26230 DH
      if (get_top().pos.g_PeripheralsConnected)
        alert("No scan input data has been entered; a Scan input document is not configured.");

      return;
    }
    else
    {
      setTimeout(function () {
        get_top().pos.DocOCR_ScanAsync(prompt, document_type, device_name, ocr_coordinate_1, ocr_coordinate_2, ocr_coordinate_3, ocr_coordinate_4, pertech_ocr_recognizer/*Bug 25830 DH*/)
      }, 250// Bug# 25997 DH - If not chained properly then use a longer timer to allow for the refresh.
      );
      return;
    }
  }
  // end Bug# 21370 DH

  if (has_point_scanner && (scanvalue == "" || scanvalue == undefined)) {
    post_remote(get_top().main.get_current_obj(), "Business_app.pos.ready_point_barcode", {}, 'pos_frame');
    return;
  }

    var sv = '~' + scanvalue + '&';// Bug# 21370 DH
    data = get_top().pos.Base64.encode(Adumbrate(sv)); //Bug 25913 NK Uppercase letter V
    
    var args = {
        e_data: data
    };

    post_remote(get_top().main.get_current_obj(), "Business_app.pos.device_swipe_receive", args, 'main');
}

// END BUG 20115 **********************************************************************


//====================================
function edit(img, target_element) {  /* switch between +Edit and -Use Default */
  var tag=document.getElementsByName(target_element)[0];
  if(img.constructor == String) {
    img=document.getElementById(img + "_f_img");
  }
  if(tag.disabled == true) {  /* [-] Use Default */
	  tag.disabled = false;
	  tag.className=""
    img.src = img.src.replace("plus", "minus");
    img.title="Use Default";
  } else {    /* [+] Edit and override default */
   tag.disabled = true;
   tag.className="inherited"
   img.src = img.src.replace("minus", "plus");
   img.title="Edit and Override Default";
  }
}
// Called by: GEO.cbutton, casl_link in business/include/main.js
function go ( location, target, hide_cover ) {  // show_cover default is true.  show_cover not used??
  if (hide_cover) {} else hide_cover=false;
  
  //lert("go: location: " + location + ", target: " + target);
  var TARGET = window;
  if ( target != null ) {
    if ( typeof(target)=="string" ) {
      TARGET = top[target];
    } else {
      TARGET = target;
    }
  }

  if ( TARGET==window && hide_cover==false && is_defined("cover", TARGET) ) {
    TARGET.cover();
  }

  // Bug 16648 MJO - Return double-submit value with all requests
  if (location.indexOf("__DOUBLESUBMIT__") == -1 && typeof double_submit_value !== 'undefined')
    location += "&__DOUBLESUBMIT__=" + double_submit_value;
  else if (location.indexOf("__DOUBLESUBMIT__") == -1 && typeof get_top().main !== 'undefined' && typeof get_top().main.double_submit_value !== 'undefined')
    location += "&__DOUBLESUBMIT__=" + get_top().main.double_submit_value;

  if ( typeof( location ) == "string" ) {
    TARGET.location = location;
  } else {
    TARGET.location = location.href;
  }
}

/* Testing existence*/
function is_defined ( variable_name, target ) {
  if ( target == null ) target=window;
  return ( typeof( target[ variable_name ] ) != "undefined" );
}
/* Object cloning */
function clone (obj, deep) {
  var objectClone = new obj.constructor();
  for (var property in obj) {
    if (!deep) {
      objectClone[property] = obj[property];
    } else if (typeof obj[property] == 'object') {
      objectClone[property] = clone(obj[property], deep);
    } else {
      objectClone[property] = obj[property];
    }
  }
  return objectClone;
}


/* WAIT DIALOG */
var waiting = false;
// Called by: GEO.cbutton, js.parse_scan_code, validate_and_show_dialog and others
// Bug 27205 MJO - Added optional delay argument
// Bug 27301 NK - Clicking "CSV Export" button produces a JavaScript error
function cover(delay) {  // Was: block_actions_and_wait
  // Bug 26334 MJO - Never cover up messages  
    if (document.getElementById("message_dialog")) {
      if (document.getElementById("message_dialog").style.display !== "none" && document.getElementById("message_dialog").style.display !== "")
        return;

      if (delay === undefined)
        delay = 500;

      waiting = true;
      show_cover();
      prevent_wait_dialog = false; // Bug 23116 MJO - Enable processing bar display
      setTimeout(show_wait_dialog, delay);  // This doesn't get called when is_use_client() because ActiveX call blocks all threads.
    }
}

// Called by: cover
function show_wait_dialog() {
  // Bug 23116 MJO - Prevent processing bar from drawing over messages
  if (prevent_wait_dialog) {
    prevent_wait_dialog = false;
    return;
  }

  var wait_dialog = document.getElementById("wait_dialog");
  // DJD: Bug #6900 - Javascript Runtime error when clicking Reload 
  // If there is no element with id 'wait_dialog' then getElementById returns null
  if(wait_dialog) {
    if(getInternetExplorerVersion() == -1)
    wait_dialog.style.display = "table";
    else
      wait_dialog.style.display = "block";
    set_focus_to("wait_dialog");
  }
}

// Bug 17505 MJO - Hide processing dialog
function hide_wait_dialog () {
  var wait_dialog = document.getElementById("wait_dialog");
  
  if (wait_dialog) {
    bStopProgressBar = true;
    wait_dialog.style.display = "none";
  }
}

/* DIALOG COVER */
function show_cover () {
  var cover = document.getElementById("cover");
    //Bug 15003 NJ - removed focus as it was slow. Just change classname and it brings it into focus. 
    if (cover)
        cover.className = "coverVisible";

  }
function hide_cover () {
    document.getElementById("cover").className = "cover"; //Bug 15003 NJ - removed focus as it was slow. Just change classname and it brings it into focus.
}
function show_message_cover() {
  // Bug 23116 MJO - Disable processing bar display as we're showing a message
  prevent_wait_dialog = true;

  var message_cover = document.getElementById("message_cover");
    if (message_cover)
        message_cover.className = "coverVisible" //Bug 15003 NJ - removed focus as it was slow. Just change classname and it brings it into focus.
}
function hide_message_cover () {
    document.getElementById("message_cover").className = "cover"; //Bug 15003 NJ - removed focus as it was slow. Just change classname and it brings it into focus.
}

/* MESSAGING */
// Called by: CASL Message_list.to_js   Example: setTimeout("enqueue_message('can_add_tender_error');", 10);
function enqueue_message ( msg ) {
  if ( !is_defined("message_queue") ) {
    return false;
  }

  // Bug# 13720 Can't find where this silly thing is set. Someone probably set some defaults and messed this up. DH.
  if(msg == "GEO.req")
  {
    return;
  }
  
  if ( message_queue.length == 0 ) {
    show_message_cover();
    document.activeElement.blur(); // Bug 24594 MJO - Remove focus so users can't mess with inputs behind the message
  }
  
  message_queue.push( msg );
  
    if (message_queue.length == 1) {
    //BUG 5793 Pop Cash Drawer: message too late, changes per CL BLL
    var sMessage = get_message(message_queue[0]);
    
    if(sMessage == undefined)// BUG# 10707 DH - This really does not apply to this bug but should be here so I added it. It makes the IE not crash but display a message instead.
    {    
      alert("Message cannot be displayed because it is undefined");
      dequeue_message();
      return true;
    }
    
    set_message( sMessage );
    show_message();
    if (typeof sMessage != "undefined") { //Bug 11924 NJ 
      if (sMessage.onshow ) {
          setTimeout(sMessage.onshow, 15);
      }
    }
    //end BUG 5793
  }
  
  return true;
}

// Called by: Device_Scanner_receive_callback, RaiseErrorEvent, set_message
function dequeue_message (msg) {
  if ( !is_defined("message_queue") ) {
    return false;
  }
  
  if( msg != null ) {
    for(var i = 0; i < message_queue.length; i++) {
      if( message_queue[i] == msg ) {
        message_queue = message_queue.slice(0, i).concat(message_queue.slice(i + 1));
        break;
      }    
    }  
  } else {
    message_queue.shift();
  }
  
  if ( message_queue.length > 0 ) {
    //BUG 5793 Pop Cash Drawer: message showing too late, changes per CL BLL
    var sMessage = get_message(message_queue[0]);
    set_message( sMessage );
    setTimeout("show_message();", 1);
    if ( sMessage.onshow ) {
      setTimeout( sMessage.onshow, 15 );
    }
    //end BUG 5793
  } else {
    hide_message();
    hide_message_cover();
    hide_cover(); // Bug 22881 MJO - Hide regular cover too
    set_focus_to_first_input(false); // Bug 24594 MJO - Set focus back to form when finished showing messages
    if (message_onclose != "") {
      var run_message=message_onclose;
      message_onclose="";
      eval(run_message);
    }
  }
  
  return true;
}

//BUG 5793 Pop Cash Drawer: message showing too late, changes per CL BLL
function get_message ( msg ) {
  if ( typeof( msg ) == "string" ) {
    return message_list[msg];
  } else {
    return msg;
  }
}
//end BUG 5793

// Called by: enqueue_message, dequeue_message
function set_message ( message ) {
  
  if(message==undefined) {
    enqueue_alert_message("There was an error loading message.");
    dequeue_message();
    return;
  }  
  
  var inner_dialog = document.getElementById("message_dialog_inner");
  var inner_action = document.getElementById("message_action_inner");
  
  var content_html = to_html( message.content );
  // Bug #10059 Mike O - Construct action HTML using the JS equivalent of htm_button_row
  var actions_html = action_array_to_html( message.actions );
  
  inner_dialog.innerHTML = content_html.replace(/&quot;/g, '"');
  inner_action.innerHTML = actions_html.replace(/&quot;/g, '"');
  
  /* BUG 5793 Pop Cash Drawer: message showing too late, changes per CL BLL
  if ( message.onshow ) {
    return eval( message.onshow );
  }
  end BUG 5793*/
}

function clear_message() {
  var inner_dialog = document.getElementById("message_dialog_inner");
  inner_dialog.innerHTML = "";
  var inner_action = document.getElementById("message_action_inner");
  inner_action.innerHTML = "";
}

function show_message() {
  // Bug 17505 MJO - Hide wait dialog when displaying a message so it doesn't cover it
  hide_wait_dialog();

  var message_dialog = document.getElementById("message_dialog");
  
  var IEVersion = getInternetExplorerVersion();
  
  if(IEVersion == -1 || IEVersion == undefined) // Bug 13720 DH - iPayment Peripheral Overhaul - Added undefined.
    message_dialog.style.display = "table";  
  else
    message_dialog.style.display = "block";
    
  set_focus_to("message_dialog");
  if(IEVersion == -1)// Bug 13720 DH - iPayment Peripheral Overhaul - Changed variable
    setTimeout('if(document.getElementById("message_dialog").style.display == "table") set_focus_to("message_dialog");', 150);
  else
  setTimeout('if(document.getElementById("message_dialog").style.display == "block") set_focus_to("message_dialog");', 150);
}
function hide_message() {
  //var message_dialog = document.getElementById("message_dialog");
  //setTimeout('document.getElementById("message_dialog").style.display = "none";', 1);
  document.getElementById("message_dialog").style.display = "none";
}

function enqueue_alert_message( msg ) {
  if ( !is_defined("message_list") ) {
    return false;
  }
  
  enqueue_message( { content:msg, actions:message_list.alert.actions } );
  return true;
}

// Bug 23022 MJO - Added hrefs for data-btn-url attributes
function enqueue_confirm_message( msg, if_true, if_false, true_href, false_href ) {
  if ( !is_defined("message_list") ) {
    return false;
  }
  
  var actions = clone( message_list.confirm.actions, true );
  
    //Bug 15367 - revert 15362, it was caused due to double and single quotes being in actions html,so no longer needed .
  // Bug 23491 MJO - Rewrote to take into account something other than the return already being present in onclick
  // Bug 23022 MJO - Inner quotes are now double and outer is single
  var true_str = if_true.replace(/"/g, "\\x22") + "'";
  var false_str = if_false.replace(/"/g, "\\x22") + "'";
  var true_href = true_href.replace(/"/g, "\\x22") + "'";
  var false_href = false_href.replace(/"/g, "\\x22") + "'";

  actions[0] = actions[0].replace("return(true);'", true_str);

  if (actions[0].indexOf('data-btn-url=""') > -1)
    actions[0] = actions[0].replace('data-btn-url=""', 'data-btn-url="' + true_href + '"');
  else
    actions[0] = actions[0].replace('data-btn-url=&quot;&quot;', 'data-btn-url=&quot;' + true_href + '&quot;');

  actions[1] = actions[1].replace("return(true);'", true_str);

  if (actions[1].indexOf('data-btn-url=""') > -1)
    actions[1] = actions[1].replace('data-btn-url=""', 'data-btn-url="' + false_href + '"');
  else
    actions[1] = actions[1].replace('data-btn-url=&quot;&quot;', 'data-btn-url=&quot;' + false_href + '&quot;');

  false_str = false_str.substr(1, false_str.length - 1);
  
  enqueue_message( { content:msg, actions:actions } );  
  return true;
}

/* Begin validation */
function blur_all (formname) {
 var inputs = document.forms[formname].getElementsByTagName("INPUT");
 var input_tag = null;
    for (var i in inputs) {
    input_tag = inputs[i];
    if (input_tag.onblur)
       input_tag.onblur();
 }
}

function include_main_script_and_style (baseline_js) {
 return "<" + "SCRIPT src='" + baseline_js + "/business/include/ipayment.min.js'><" + "/SCRIPT>"
        + "<LINK type='text/css' rel='stylesheet' href='"  + baseline_js + "/cbc/include/ipayment.min.css'></LINK>"; 
}

function session_about_to_timeout (baseline_js, last_uri, return_uri, entry_uri, return_uri_logoff, ignore_incomplete) {
  window.onbeforeunload = null; // Bug #13681 Mike O - Ignore warning, if present
  clearInterval(keep_alive_interval_id); // Bug 19572 MJO - Stop calling keep alive if calling logout
  // Bug 23078 MJO - Only add argument for Cashiering
  var uri = get_current_page_root() + "/logout.htm?"; //Bug#8623 ANU To resolve session to be abandoned after timeout. // Required so that IIS sees the query string BUG 5413
  if (ignore_incomplete)
    uri += "ignore_incomplete_event=true";
  casl_link( uri );
}

function closeWindow(){  
// Bug #8219 Mike O - Removing window.open call in case someone tries to use this
// window.open('','_parent','');
window.close(); 
}

function session_timeout (baseline_js, return_uri, entry_uri, return_uri_logoff) {
 var new_page =  "<BODY class='dialog_box'><STYLE> * { font-family: 'Trebuchet MS', Trebuchet, Verdana, Arial, Helvetica, 'Lucida Grande', sans-serif; } </STYLE>"
     + include_main_script_and_style(baseline_js)
     + "<DIV class='dialog_box'><H3>Your session has expired.</H3>";
 if (entry_uri != "")
    new_page = new_page + "<H3><A href='"+ entry_uri + "'>Re-enter</A></H3>";
 if (return_uri != "")
    new_page = new_page + "<H3><A href='"+ return_uri + "'>Back</A></H3>";
 if (return_uri_logoff != "")
    new_page = new_page + "<H3><A href='"+ return_uri_logoff + "'>Exit</A></H3>";
 new_page = new_page + "</DIV></BODY>";
 document.write(new_page);
 document.close();
}

/*window.onload = function(){
  if(document.getElementsByTagName){
   var el = document.getElementsByTagName("INPUT");
    for(var i=0; i<el.length; i++){
     if(el[i].type == "text")
     {
      el[i].onfocus = function () {  // Code commented out since it clashes with the validation handler
         this.id = "focus"
         //this.oldClassName = this.className;
         //this.className = this.className + " focus ";
      }
      el[i].onblur = function () {
         this.id = ""
         //this.className = this.oldClassName;
       }
    } //if
  }  // for
 }  // if
}  // function
*/

/* shouldn't need
function isNotEmpty(elem) {
	var str = elem.value;
    var re = /.+/;
    if(!str.match(re)) {
        //lert("Please fill in the required field.");
        setTimeout("focusElement('" + elem.form.name + "', '" + elem.name + "')", 0);
        return false;
    } else {
        return true;
    }
}
*/

function IsImageOk(img) {
    if (!img.complete) { return false; }
    if (typeof img.naturalWidth != "undefined" && img.naturalWidth == 0) { return false; }
    return true;
}
function check_user_icon () { 
  var user_img=document.getElementById("user_image");
	if (user_img && !IsImageOk(user_img)) {
	  user_img.src=user_image_default_user_uri
	}
}
 
function formatNumber (num, decplaces) {
    // convert in case it arrives as a string value
    num = parseFloat(num);
    // make sure it passes conversion
    if (!isNaN(num)) {
        // multiply value by 10 to the decplaces power;
        // round the result to the nearest integer;
        // convert the result to a string
        var str = "" + Math.round (eval(num) * Math.pow(10,decplaces));
        // exponent means value is too big or small for this routine
        if (str.indexOf("e") != -1) {
            return "Out of Range";
        }
        // if needed for small values, pad zeros
        // to the left of the number
        while (str.length <= decplaces) {
            str = "0" + str;
        }
        // calculate decimal point position
        var decpoint = str.length - decplaces;
        // assemble final result from: (a) the string up to the position of
        // the decimal point; (b) the decimal point; and (c) the balance
        // of the string. Return finished product.
        return str.substring(0,decpoint) + "." + str.substring(decpoint,str.length);
    } else {
        return "NaN";
    }
}


//Bug#7549 ANU Added formatCurrency Function 
function formatCurrency(num) {
num = num.toString().replace(/\$|\,/g,'');
if(num.indexOf('(') != -1 && num.indexOf(')') != -1 )
num= '-'+ num.replace('(','').replace(')','');
if(isNaN(num))
  // Bug #10449 Mike O - If this isn't a number, just let error handling deal with it.  Don't change it to 0.
  return num;
sign = (num == (num = Math.abs(num)));
num = Math.floor(num*100+0.50000000001);
cents = num%100;
num = Math.floor(num/100).toString();
if(cents<10)
cents = "0" + cents;
for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
num = num.substring(0,num.length-(4*i+3))+','+
num.substring(num.length-(4*i+3));
//var result;
//if(sign)
//result=  '$' + num + '.' + cents;
//else
//result =  '($' + num + '.' + cents + ')';
return (((sign)?'':'-') + '$' + num + '.' + cents);
}
//<!-- Bug#7549 () Support ANU-->
function formatAmount(value) {
result = formatCurrency(value);
    if (result.indexOf('-') != -1) {
result = result.replace('-','');
result = '(' + result + ')' ;
}
return result;
}
//<!-- Bug#7549 () Support ANU-->
//Bug#7549 end 

// BUG#14761 
function formatDate(date) {
  if (date === undefined)
    date =  new Date();
  var dd = date.getDate();
  var mm = date.getMonth() + 1;  // January is 0!
  var yyyy = date.getFullYear();
  if (dd < 10) { dd = '0' + dd } if (mm < 10) {mm = '0' + mm } 
  var sDate = mm + '/' + dd + '/' + yyyy;
  return sDate;
}


// var field_types = { field_key: { label:"a label", types:[ "type1", ["type2","arg1","arg2"], "type3" ] }
// Typical Example:
//  var field_types = { last_name: [label:"Last Name",types:["required",["text",1,25,"alphanumeric"]] }
// var type_errors = { a_type: { error1: "bad", error2: "invalid" }}

function get_value (element) {
 var R = element["value"];
 //BUG 24811 NAM: convert value to negative equivalent/
 if (R){
   if ($(element).data('type') === 'currencyautonegative'){
     if (R.indexOf('-') < 0 ){
       R = '-'.concat(R);
     }
   }
   return R;
 }
 else return "";
}

var do_validation = true;

function validate_and_cover (tag)  // was: validate_tender_fields
{
  // DJD: Bug #6900 - Javascript Runtime error when clicking Reload 
  // Check if a Javascript function resetTimeout exists before calling it:
  if (typeof resetTimeout == 'function') resetTimeout();
  
  //keep_alive(); Bug #8668 Mike O - Call at a regular interval

   //document.pressed is set in OnClick for the button
    if (document.pressed != 'Clear') {
    var validate_result = true;
    if(tag.tagName=="FORM") {
      validate_result=validate_all(tag);
        //Bug#13143 QG jQuery validation
        if (validate_result == true) {
        var jQueryvalidateMsg = jQueryvalidate(tag);
            if (jQueryvalidateMsg != true)
            validate_result = jQueryvalidateMsg;
        }
        //End of Bug#13143
    } else {
      validate_result=validate_all(tag.form);
    }
    
    if ( validate_result == true) {  /* if (validate_and_show_dialog(this) == true) { return submit_once(this);} else { return false;} */
      cover();
      if (is_use_client()) {
          casl_form(tag);
          return false;
        }
      else
   		  return(true);
    } else {
       if ( is_defined( "enqueue_alert_message" ) ) {
         enqueue_alert_message(validate_result);
       } else {
         alert(validate_result);
       }
       
       // Bug #10407 Mike O - In case a validation error message pushes the frame into a bad position, refresh it
            // refresh_scrollable_all();
       
       return(false);
    }
  }
  
}

function validate_chars (value, valid_chars, message) { 
  // Bug 6787 - Validation errors clear screen contents (related fix)
  // DJD: This function uses actual characters (not special character codes): the
  // following will replace codes with characters in the valid_chars string sent.
  valid_chars=valid_chars.replace(/&quot;/g,'"');
  valid_chars=valid_chars.replace(/&amp;/g,'&');
  valid_chars=valid_chars.replace(/&lt;/g,'<');
  valid_chars=valid_chars.replace(/&gt;/g,'>');
  
    for (var i = 0; i < value.length; i++) {
     var letter = value.charAt(i);
     if (valid_chars.indexOf(letter) == -1)
      if(letter != "\r" && letter != "\n" ) //BUG 6044 ignore carriage return and new line
        if(value.charAt(i) == " ")
         return message + ". Spaces are not allowed.";
        else
         return message + " " + value.charAt(i) + ".";
   }
   return true;
}

// Bug 17028 UMN - use for more specific validation
function validate_regex(value, regex, message) {
  var re = new RegExp(regex);
  if (re.test(value)) return true;
  return message;
}

// Bug 18948 UMN
function validate_captcha(message) {
  var captchaSel = $('#g-recaptcha-response');
  if (captchaSel.length === 0) return true;
  if (captchaSel.val().length === 0) return message;

  // sigh, copy value
  $('#captcha').val(captchaSel.val());
  return true;
}

// Called by: Allocation.htm_inst and htm_add_allocation in main_pos.js
function validate_sub_items (tag, id_path) {  // id_path example: "args.0.0"
  // go through all validation fields whose id starts with a_key
  
  var a_field = null;
  var path = "";
  var TAG = null;
  var input_value = "";
  
  var the_form=tag.form;
  // get the form's elements, and validate ones that start with 
  //lert("the_form: " + the_form);
  if (the_form) { 
       var form_elements=the_form.elements;
       for (var i=0; i<form_elements.length; i++) {
         TAG=form_elements[i];    
         // Bug #10205 Mike O - Must use getAttribute in non-IE browsers
         if (TAG && (TAG.getAttribute("name").substring(0, id_path.length) == id_path)) {  // should verify it is an INPUT tag and starts with the given id_path
           validate_tag(TAG);
           //lert("validate_all key: " + TAG.name);
         }
       } 
  }
}

/* No longer used.  Delete this.
function get_doc_tag (key) {
 return ( document.getElementById(key) ||
            document.getElementById("a_core_file_inputs." + key) ||  // this is quite a hack to specify different input field names
            document.getElementById("args." + key) 
        )
} */

function MaskCCVal(s) {
  // Added for BUG# 8705 DH - Mask the credit card data
  var s_masked = "";
  for(var i=0; i< s.length-4; i++)
    s_masked += "*";

  for(i=4; i>0; i--)
    s_masked += s.charAt(s.length-i);
  
return s_masked;
}

// Called by: Core_event.htm_total_and_balance, validate_and_cover
// in main.js
function validate_all(a_form) {
  // Bug #10264 Mike O - Clear out the delayed validation queue so we don't validate fields twice
  validation_queue = new Array();
  
  var a_form=a_form?a_form:document.forms[0];  // If no form given, use first form.
  //end bug 9931
  // go through all validation fields and show
  // message if not valid field_types
  
  // CHANGE GLOBAL VARS
  do_validation = true;
  var cc_validation_is_done = false;
  var is_valid = false;
    
  // LOCAL VARIABLES
  var form_is_valid = true;
  var a_field=null;
  var path="";
  var input_value="";

  // VALIDATE EACH KEY/field
  var input_tag=null; var selected=false; var selected_name=""; var selected_tag=null;
  for(var i=0; i < a_form.elements.length; i++) { // validate all tags, unless group/object is not selected.
    input_tag=a_form.elements[i];
    
    // TAG found, validate it, if selected.
    selected_name="";  // given args.0.0.foo, will be args.0.0
    // Bug #10205 Mike O - Must use getAttribute in non-IE browsers
    if (input_tag.getAttribute("name") && input_tag.getAttribute("name").indexOf(".")>=0) {
  selected_name=input_tag.getAttribute("name").substring(0,input_tag.getAttribute("name").lastIndexOf("."));
  }
  selected_tag=document.getElementById(selected_name + ".selected_f_value");  // INPUT with name like args.0.0.selected_f_value

    var tags_to_remove = []; // Bug 25404 MJO

  // BUG# 8705 DH
  // Bug #10205 Mike O - Must use getAttribute in non-IE browsers
  if (input_tag.getAttribute("name") == "args.credit_card_nbr") {
    // Bug #13218 Mike O - Changed approach to getting credit_card_nbr_en field
    var cc_nbr = a_form["args.credit_card_nbr_en"];
    if (input_tag.value.charAt(0) != '*') {
      if (cc_nbr != null) {
        //var v = ConvertToHex(input_tag.value);
        //BUG 12666 NK for new Adumbrate encryption
        // Bug 21181 MJO - Added base64 encoding to make it consistent with other calls
        var v = Base64.encode(Adumbrate(input_tag.value));
        cc_nbr.value = v;
        cc_nbr.readOnly = "true";
        cc_validation_is_done = true;
      // Bug #10205 Mike O - Must use getAttribute in non-IE browsers
      is_valid = validation(input_tag.value, input_tag.getAttribute("name")?input_tag.getAttribute("name"):"", input_tag, true);
        // Bug #12992 Mike O - Don't overwrite form_is_valid, as it will clear out any previous validation errors
        if(input_tag.value != "")
          input_tag.value = MaskCCVal(input_tag.value);
       }
    }
    else if (input_tag.value.charAt(0) == '*') {
      cc_validation_is_done = true;
      is_valid = true;
    }
    else {
      // Probably empty.
    }
  }
  // Bug #12992 Mike O - Credit card number fields shouldn't go through normal validation, and shouldn't prevent other fields from being validated
  else if (selected_tag) {
  if (selected_tag.checked) {
  //lert("selected_name:" + selected_name + " selected_tag.checked:" + selected_tag.checked);
  // should verify it is an INPUT tag
  var input_value=input_tag.value;

    // Bug #10205 Mike O - Must use getAttribute in non-IE browsers
    is_valid=validation( input_value, input_tag.getAttribute("name")?input_tag.getAttribute("name"):"", input_tag, true );

  //alert("validate_all key: " + input_tag + " is_valid:" + is_valid);
    }
      // do not submit the form value if not selected...
      // Bug 22327 MJO - Unless this is the selected field itself
    // Bug 25404 MJO - Don't remove name attributes unless validation succeeds
    else if ($(input_tag).attr("name") && $(input_tag).attr("name").length > 9 && $(input_tag).attr("name").slice(-9) !== ".selected") {
      tags_to_remove.push(input_tag);
  }
  }
  else {  // handle normal case where item is not selected
  var input_value=input_tag.value;
  var is_valid=true;
    if (!input_tag.disabled) {
    // Bug #10205 Mike O - Must use getAttribute in non-IE browsers
    is_valid=validation( input_value, input_tag.getAttribute("name")?input_tag.getAttribute("name"):"", input_tag, true);
  }
  }
  if( form_is_valid==true ) {
    //TAG.focus();
    form_is_valid = is_valid;

    // Bug 25404 MJO - Don't remove name attributes unless validation succeeds
    for (var j = 0; j < tags_to_remove.length; j++)
      tags_to_remove[j].removeAttribute("name");
  }
  }

  //------------------------------------------------------------------------
  // BUG# 8223 DH - HTML Editor
  // This has to happen at the end, or it will convert fields too soon and 
  // not submit the form.
    if (form_is_valid == true) {
        for (var i = 0; i < a_form.elements.length; i++) {
      var input_tag=a_form.elements[i];
      var cust_flag = "_htmldisplay";  
      var cust_flag2 = "_formuladisplay"; // Bug 12098 - iPayment Baseline: Addition of Formulas [DJD]
      // Bug #10205 Mike O - Must use getAttribute in non-IE browsers
      if(input_tag.getAttribute("name") != null &&
        (input_tag.getAttribute("name").match(cust_flag)  != null ||
         input_tag.getAttribute("name").match(cust_flag2) != null )) // Bug 12098 - iPayment Baseline: Addition of Formulas [DJD]
      {
         var val = input_tag.value;
         if(val.substr(0, 2) != "0x" && val != "")
           input_tag.value = ConvertToBase16(input_tag.value);
      }
    }
  }
  // end BUG# 8223 DH - HTML Editor
  //------------------------------------------------------------------------
    
  return form_is_valid;
  }

function ConvertToASCII(s) // BUG 8705 D.H
{
  var r = "";
  for (var i= (s.substr(0, 2)=="0x")?2:0; i<s.length; i+=2) {r += String.fromCharCode (parseInt (s.substr (i, 2), 16));}
  return r;
}
                                                      
function ConvertToHex (s) // BUG# 9386 DH
{
  var r = "";
  var hexes = new Array ("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f");
  for (var i=0; i<s.length; i++) {r += hexes [s.charCodeAt(i) >> 4] + hexes [s.charCodeAt(i) & 0xf];}
  return r;
}

//BUG 12666 NK crdit card number Adumbrate

function Adumbrate(s) {
    var r = [];
    for (var i = 0; i < s.length; i++) {
        var j = s.charCodeAt(i);
        if ((j >= 33) && (j <= 126)) {
            r[i] = String.fromCharCode(33 + ((j + 14) % 94));
        }
        else {
            r[i] = String.fromCharCode(j);
        }
    }
    return r.join('');

}
// Bug 12098 - iPayment Baseline: Addition of Formulas [DJD]
// Bug 20434 MJO - Use the URI passed in from CASL instead of the static one
function formula_editor(formula_display_ctl, formula_hidden_ctl, uri) {
 var args = new Object(); 
 args.formula_data = document.getElementById(formula_hidden_ctl).value;
 args.formula_validation_handler=get_current_site() + "/get_formula_validation.htm?";
 
 window.showModalDialog(uri,args,"dialogHeight:375px; dialogWidth: 400px; help:no; scroll:no;"+"resizable:no;"+"status:off;"+"edge:raised;"+"center:yes");
 
 // NOTE: Setting "innerText" will display the formula as text - i.e., not
 // try to render it as HTML.
    if (typeof args.returnvalue != "undefined") {
   document.getElementById(formula_display_ctl).innerText = args.returnvalue;
   document.getElementById(formula_hidden_ctl).value = args.returnvalue;
 }
}
// end Bug 12098 - iPayment Baseline: Addition of Formulas [DJD]

// BUG# 8223 DH - HTML Editor
// Bug 20434 MJO - Use the URI passed in from CASL instead of the static one
function html_editor(html_display_ctl, html_hidden_ctl, uri) {
 var args = new Object(); 
 args.html_data = document.getElementById(html_hidden_ctl).value;
 
 // Bug# 12718 - Added the site to prevent cross domain mix-up that sometimes occurs DH 5/10/2012
 window.showModalDialog(uri,args,"dialogHeight:375px; dialogWidth: 400px; help:no; scroll:no;"+"resizable:no;"+"status:off;"+"edge:raised;"+"center:yes");
 
 // Bug #9973 Mike O - There's no returnvalue when the dialog is closed with the X
 if(args.returnvalue != null) { 
 document.getElementById(html_display_ctl).innerHTML = ConvertFromBase16(args.returnvalue); // RDM bug 24456
 document.getElementById(html_hidden_ctl).value = ConvertFromBase16(args.returnvalue);
}
}
// end BUG# 8223 DH - HTML Editor

// BUG# 8223 DH - HTML Editor
function ConvertFromBase16(s) {
  if(s == "")
    return s;
    
  var r = "0x";
  for (var i= (s.substr(0, 2)=="0x")?2:0; i<s.length; i+=2) {r += String.fromCharCode (parseInt (s.substr (i, 2), 16));}
  var str = r.substr(2);
  return str;
}         
// end BUG# 8223 DH - HTML Editor
        
        
// end BUG# 8223 DH - HTML Editor                                     
function ConvertToBase16(s) // BUG# 8223 DH - HTML Editor
{
  // Bug# 25780 DH - There is another version of it now in the pos_interface.js, but it's probably safer to leave this here.

  var r = "0x";
  var hexes = new Array ("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f");
  for (var i=0; i<s.length; i++) {r += hexes [s.charCodeAt(i) >> 4] + hexes [s.charCodeAt(i) & 0xf];}
  return r;
}
// end BUG# 8223 DH - HTML Editor


  // Called by: get_scrollable_container (recursive) and ???
  function get_scrollable_container (a_tag) {  // returns first scrollable tag containing a given tag
  if (a_tag.id=="scrollable")
  return a_tag;
  else if (a_tag.parentElement)
  return get_scrollable_container(a_tag.parentElement);
  else
  return false;
  }

// Called by: CASL's make_input and validate_sub_items   Eventually calls: display_error
function validate_tag (input_element) {
  if (input_element != null) {
		validation_queue.push(input_element);
        //setTimeout(delayed_validation, 100); //Bug 10668 NJ
    }
    //Bug 10668 NJ
    else
        validation_queue.push(this);
    delayed_validation(); //Bug 13934. removed set timeout which was removing other validation.
		//setTimeout(delayed_validation, 100);
    //end Bug 10668 NJ
                  
  }

// Bug 17187 UMN format date
function TryParseInt(str, defaultValue) {
  var retValue = defaultValue;
  if (str !== null) {
    if (str.length > 0) {
      if (!isNaN(str)) {
        retValue = parseInt(str);
      }
    }
  }
  return retValue;
}

function IsDateType(type_list)
{
  for (var type_key in type_list)
    for (var type in type_list[type_key])
      if (type_list[type_key][type] == 'date') return true;
  return false;
}

function delayed_validation() {
	var input_element=validation_queue.shift();
  if (input_element != null) {
		var input_value=get_value(input_element);
		var input_name=input_element.id;
		
 
  var result = false;

  // Bug 17187 UMN fix date input if user types in short form date
  var a_field = get_js_object(input_element, "field_info");
  var is_date = IsDateType(a_field.types);
  if (input_value != "" && is_date)
  {
    // fix year if someone types in 02/13/14
    var dt = input_value.split("/");
    if (dt.length === 3 && dt[2].length < 4)  // Bug 14022 UMN
    {
      var yr = TryParseInt(dt[2], 0);
      dt[2] = yr + 2000;
      input_value = dt[0] + "/" + dt[1] + "/" + dt[2].toString();
    }
  }
  
  // BUG# 8705 - Don't validate masked values
  // Bug #10205 Mike O - Must use getAttribute in non-IE browsers
  if (input_element.getAttribute("name") == "args.credit_card_nbr" && input_element.value.charAt(0) == '*')
    result = true;
  else
    result = validation(input_value, input_name, input_element, false);
  //Bug#8810 ANU to avoid config field issues. 

  // Bug 17187 UMN format date
  // Bug 12512 MJO - Allow "current" date
  if (result === true && input_value != "" && is_date && input_value != "current") // Bug 14022 UMN
  {
    // Bug 18510 MJO - Update the value directly on the element instead of trying to go find it
    input_element.value = formatDate(new Date(input_value));
  }

  if((result === true && (input_name.toLowerCase().endsWith("amount") || input_name.toLowerCase().endsWith("total"))) 
      && !input_name.toLowerCase().endsWith("trans_or_tender_total")) //Bug 9996 NJ // Bug#8826 ANU
  {         
    // Bug 18510 MJO - Update the value directly on the element instead of trying to go find it
    input_element.value = formatAmount(input_value);//<!-- Bug#7549 () Support ANU-->
  }
 //Bug#7549 ANU end 
 
  //BUG 24811 NAM: convert value to negative equivalent 
  if(result === true && $(input_element).data('type') === "currencyautonegative") {  
    //BUG 24823 NAM: if already formatted as negative amount, remove formatting
    if(input_value.indexOf('\($')>0 && input_value.indexOf('\)') == input_value.length-1 ){ 
      input_value = input_value.replace('\($','');
      input_value = input_value.replace('\)','');
    }
    else if(input_value.indexOf('\(')>0 && input_value.indexOf('\)') == input_value.length-1 ){ 
      input_value = input_value.replace('\(','');
      input_value = input_value.replace('\)','');
    }     
    input_element.value = formatAmount(input_value);
  }
}
    //refresh_scrollable_all();
}

function refresh_scrollable_all () {
  //BUG 4836 Adjust tabs when sizing screen BLL
  // Bug #10053 Mike O - Wait a bit to allow the collapsed tabs to be redrawn if using IE, moved code into function, and removed bAlreadySized
  if (getInternetExplorerVersion() != -1)
    setTimeout( function(){ refresh_tabs(false)}, 20); // Bug #10809 Mike O - Decreased wait
  else
    refresh_tabs(false);
  //end BUG 4836
  var tags=document.getElementsByName("scrollable");
  for (var i=0; i<tags.length; i++) {
    // Bug #10407 Mike O - Always use delay
    refresh_scrollable(tags[i], true);
  }
}

function refresh_scrollable (tag, delay) { 
  //status+=" refresh_scrollable"; // COUNT++ + ":refresh_S; " + status; //DEBUG 
  delay=delay?delay:false;
  var scrollable_tag=get_scrollable_container(tag);
  if (scrollable_tag && scrollable_tag.style && scrollable_tag.style.width) {
    //status+=" refresh_scrollable"; 
    var orig_width=scrollable_tag.style.width;
    // Bug #10407 Mike O - Setting to "auto" works fine
    scrollable_tag.style.width="auto";
    if (delay) {  // needed for bug 3939
      var anonymous_function=function() { scrollable_tag.style.width="100%"; };  // scrollable_tag is from local variable of refresh_scrollable.  Can read about JavaScript closure and anonymous functions.
      //lert("GLOBAL_tag.style.width='" + orig_width + "';");
      // Bug #10407 Mike O - Increase delay time
      setTimeout(anonymous_function,100);
    } else {
      scrollable_tag.style.width=orig_width;  // This resets it back to what it should be.
    }
  }
}

//BUG 4836 BLL
function collapse_tabs() {
  // Bug #10053 Mike O - Move the code inside the function and removed bAlreadySized
  refresh_tabs(true);
    }

/*Bug 15003 NJ - Removed solution of bug 10053. Use jquery outerwidth as it is faster than offsetwidth.
Calculate outerwidth and store in an array because calculating outerwidth and changing width 
alternatively causes multiple reflows whereas calculating outerwidth 
all together causes only one reflow.
*/
var tabs = null,
 tabWidths = [];
function Tab(element, outerwidth) {
    this.element = element;
    this.width = outerwidth - 60;
}
function refresh_tabs (bCollapse) {
  
    if (tabs == null)
        tabs = $("div[name=tabs]");
    var r = 0;
    if (tabWidths.length == 0) {
        tabs.each(function () {
            var tab = $(this);
            tab.attr("id", "tab" + r);
            var tab1 = new Tab(tab, tab.outerWidth(true));
            tabWidths[tabWidths.length] = tab1;
        });
    }
    for (var i = 0; i < tabWidths.length; i++) {
        var tab = tabWidths[i];
        tab.element.children(":last").width(tab.width + "px");
  }
}
/*end bug 15003*/
//end BUG 4836

function find_selected_element ( input_path ) {
  if ( input_path.length == 0 ) {
    return null;
  }
  input_path.pop();
  input_path.push("selected"); //Replace the last bit with selected to see if the object referenced is selected
  var is_selected_element = document.getElementById(input_path.join("."));
  if( is_selected_element ) {
    return is_selected_element;
  } else {
    input_path.pop();
    return find_selected_element( input_path );
  }
}

function get_js_object (tag, key) {
    // Bug #10205 Mike O - Must use getAttribute in non-IE browsers
    // Bug #14011 Mike O - If this has already been converted, getAttribute will just overwrite it. Avoid that if possible
	var V=tag[key] || tag.getAttribute(key);
	var R=null;
	if (!V) { return false; }
	if ((typeof V)=="string") {
		tag[key]=eval("var m=" + V + "; m;");
		//Undid commenting to restore validation -- Anu's looking into the original issue to solve.
	}
	return tag[key];
}

// Called by: validate_tag    Assumes input_element has a full path to the field in 'obj' attribute.
function validation (input_value, key, input_element, validate_all)  // validate_all means it is called from validate_all
{
  // Bug #8736 Mike O - Bypass Javascript validation on username and password fields (may not catch 100% of cases)
  // Bug #10205 Mike O - Must use getAttribute in non-IE browsers
  // Bug 22006 NK - Check to see if this is a password custom field. If so, validate
  // IPAY-734 NK Mask data on entry, CF attribute needs to be validate
    if (input_element != null && $(input_element).attr("validate_masked") == "false") {
        if( (input_element != null && (input_element.type == "password" || input_element.getAttribute("name") == "username") && $(input_element).attr("validate_password") == undefined) || (input_element != null && $(input_element).attr("validate_masked") == "false"))
            return true;
    }
  // DJD: Bug #6900 - Javascript Runtime error when clicking Reload 
  // Check if a Javascript function resetTimeout exists before calling it:
  // Resets the timeout when validating so that any action prevents the user from timing out.
  if (typeof resetTimeout == 'function') resetTimeout(); 

  //keep_alive(); Bug #8668 Mike O - Call at a regular interval
  
  // BUG# 8705 - Don't validate masked values
  // Bug #10205 Mike O - Must use getAttribute in non-IE browsers
  if (input_element.getAttribute("name") == "args.credit_card_nbr" && input_element.value.charAt(0) == '*')
    result = true;  
  
  if (input_element.getAttribute("name") == "args.state" && 
      input_element.getAttribute("data-required") == "true" && input_element.value.length == 0)
  {
    return "State is required";
  }
  
  // Bug 5098/22519 DH/UMN Validate CC date as a whole in addition to month and year as individual integers in certain range DH 07/10/2008
  if (input_element.getAttribute("name") == "args.exp_year") {
    
    var m = document.getElementById("args.exp_month").value
    var y = input_element.value;
    // Fix user input.
    var FullYear = '20';
    if (y.length == 1)
      FullYear = '200';
    
    var UserDate = new Date(FullYear+y, m-1/*zero based*/);
    var now = new Date();
    var TodayDate = new Date(now.getFullYear(), now.getMonth());  
            
    if (UserDate < TodayDate) {
      display_error(key, 'Expired Date', '', input_element);
      return 'Expired Date';  // exit when first validation error occurs
    }
    else {
      return true;
    }
  }

  // Needs to ping the server here.
  validate_all=validate_all?true:false; // default validate_all=false;
  //lert(input_element.obj);
  //lert("IN validation: \ninput_value: " + input_value + "\nkey: " + key + "\ninput_element: " + input_element + "\nobj:" + input_element.obj );
  key = key.replace(/_manual/g, ""); //HACK FOR MANUAL MICR INPUT
  if (do_validation == false) return (true);
  //var error = new error_msg();
  var type_args = new Array(input_value, key);  // always has value and key
  var error_code;
  var error_message = "";
  var type_method = null;
  var type_name = null;
  //lert("key: " + key + " a_field:" + a_field);
  var a_field=get_js_object(input_element, "field_info");
 
  if (!a_field) return (true);  // no validation information
  //lert("key: " + key + ", a_field: " + to_js(a_field));
  
  //If the field doesn't have a label this field cannot be validated
  if ( !is_defined("label", a_field) ) {
    return true;
  }
  var label = a_field.label;
  var type_list = a_field.types;
  
  // if element's group/row is not selected, then ignore it
  var is_selected_element = find_selected_element( input_element.id.split(".") );

  // Bug 22327 MJO - Try the name as a backup
  if (is_selected_element == null)
    is_selected_element = find_selected_element(input_element.name.split("."));

  var is_selected = true;  
  if (is_selected_element) {
    is_selected = is_selected_element.value;
        if (is_selected == false || is_selected == "false") {
      display_error(key,"","",input_element);
      //return true; //Bug 24417 NK- Amount fields are validated prematurely during subsequent changes
    }
  }
 display_error(key,'','',input_element,false);  // clear out any validation error messages  

 if (input_value=='')
   if (type_list[0][0]=='required')
     type_list = ['required'];
   else
     type_list = [];
 
 for(var type_key in type_list) {  // go through all types for the field and validate each one
     var a_type = type_list[type_key];     
     var error_code = true;
     var strength = "error";
    
     if(typeof a_type == 'object' ) { // not a string       
       type_name = a_type[0];
       type_method = window[type_name + '_is_type_for'];  // Calling MMM_is_type_for (  methods that are generated or in main.js below     
       if (type_method) {
       
       //BUG 24811 NAM: convert amount to negative equivalent (removed)
       //BUG 6044 BLL: deleted 5805 change...alphanumeric_and_space should allow a space
       // end BUG 5098
            
         var parameterized_type_args = new Array();
         strength = a_type[a_type.length - 1];
         for(var i=1; i < a_type.length - 1; i++) {  // (var i in a_type)  // make type_args is all but first field
           parameterized_type_args[i-1] = a_type[i];
         } 
         type_args[2] = parameterized_type_args;  // insert type_args
                  
                if (add_decimals(type_name, input_element, key)) {
           type_args[0]=add_decimals(type_name, input_element,key);
          }
         error_code = type_method.apply(this, type_args);   // Calling MMM_is_type_for( where MMM is the name of the type
        
       }
     } 
        else {  // type is a string representing the name of the type
		 
       type_name = a_type;             
       type_method = window[type_name + '_is_type_for'];            
       if (type_method) {
                if (add_decimals(type_name, input_element, key)) {
           type_args[0]=add_decimals(type_name, input_element,key);
          }
          error_code = type_method.apply(this, type_args);        
       }
      
     }
     // currently, doesn't skip once one validation fails, but only sees 'last' validation that failed.
  
     // ASSERT: error_code is true or string of the error message
     //lert(" key: " + key + " error_code: " + error_code);
     if(error_code == true) {  // successful validation
       error_message = '';
     } else {
       if (error_code.indexOf(" ") == -1) { // if no space in error_code
          //lert("error_code:" + error_code + " key:" + key + " type_key:" + type_key + " a_type:" + a_type + " type_name:" + type_name);
          var type_error_list = type_errors[type_name];
          //lert("type_error_list:" + to_js(type_error_list) + " type_errors: " + to_js(type_errors));
          if (type_error_list) {
             var error_message_temp = type_error_list[error_code];
             if (error_message_temp) 
                error_message = error_message_temp;
             else
                error_message = "is invalid."
          } else {
            error_message = "is invalid."
          }
         }
        else
          error_message = error_code;  // error_code is the message          
       
       if (!(error_message.indexOf("is required")>0 && validate_all==false)) // Don't display "is required" if not validate_all
         	
           display_error(key, error_message, label, input_element); 
         
         // will put error message on the page       
       if ( strength == 'error' ) {
          return( label + " " + error_message );  // exit when first validation error occurs
       }
     }
 }
      
 return true;
}

function show_tender_message (message_str) {
  var a_tag=document.forms["form_class"]["args.total"];
  display_error("args.total", message_str, "", a_tag);
}

function add_decimals(type_name, input_element, id) {
  /*if(!right_input)
   var right_input = document.getElementById(id);*/
 if(type_name=='currency' || type_name=='quantity'){
 
   var error_code = false;
   var type_args = new Array(input_element.value, id);  
   var two_parts=input_element.value.split(".");
   var result = "";
   var negative = false;
   
   
   if (two_parts.length == 1) two_parts[1] = "";
   
   if(two_parts[0].substring(0,1) == "-") {
     negative = true;
     two_parts[0] = two_parts[0].substring(1);
   }
   
   if(two_parts[0].length == 0) two_parts[0] = "0";     
   
   if(negative) two_parts[0]="-" + two_parts[0];
    
   if(two_parts[1].length < 2) {
     for( var i = two_parts[1].length; i < 2; i++ ) {
         result = result + "0";
     }   
      two_parts[1] = two_parts[1] + result;
      
      input_element.value = two_parts[0] + "." + two_parts[1] + result; 
   }
   
   input_element.value = two_parts[0] + "." + two_parts[1];
   
 
   return input_element.value;  
   
  }
  
 return false;   
}

function getForm (a_tag) {
var container=a_tag.parentElement;
if (container && container.tagName=="FORM")
   return container;
else if (container)
   return getForm(container);
else
   return false;
}

function getElementByIdInSameForm (a_tag, id) {   
       var the_form=a_tag.tagName=="FORM"?a_tag:getForm(a_tag);
       var tags=document.getElementsByName(id);
       for (var i=0; i<tags.length; i++) {
         if (getForm(tags[i])==the_form)
           return tags[i];       
   }
   return(false);
}

// Called by: validation, show_tender_message
function display_error(span_id, error_message, label, input_element, delay) {
  delay=delay?delay:true;
  
  // Bug 18916 MJO - Actually use the delay to prevent validation errors from bumping buttons that users are trying to click on
  if (delay)
    setTimeout( function(){ display_error_aux(span_id, error_message, label, input_element) }, 250);
  else
    display_error_aux(span_id, error_message, label, input_element);
    
  return null;
}     

// Bug 18916 MJO - Split this out into its own function so it can be delayed
function display_error_aux(span_id, error_message, label, input_element) {
  // Bug 25704 MJO - Try to find element if it's not passed in
  if ($(input_element).length == 0 && document.getElementById(span_id) !== null)
    input_element = document.getElementById(span_id);

 // Bug 18480 and 24849 SM - This prevents the error message span issue and the credit card data page issue. 
 // Bug 24888 SM - The above fix caused bug 24888.  Added  typeof check for 'undefined' className parent.
  if (typeof $(input_element).closest('tr').get(0) != "undefined")
   {     
     var check_if_row = $(input_element).closest('tr').get(0).className;
     check_if_row = String(check_if_row);
     if(check_if_row.indexOf("htm_row_input") != -1){
     var error_tag = $(input_element).next();
        }
       else
       {
          var error_tag = getElementByIdInSameForm(input_element, span_id + '_error');
       }
    }
    else
    {      
       //BUG 19067 SM  I added the return here instead to stop execution if the input_element property Im checking above is undefined.
       return;
    }    
    // End bug 18480 and 24849 SM
          
 //Bug#6172 Changed "input_element.id was "span_id" to display Error Messages in Business Center ANU.

  if(!error_tag) var error_tag = document.getElementById(input_element.id + '_error');

 //Bug#6172 ann added for GL Validation system interface for Cashiering - ANU 
  if(!error_tag) var error_tag = document.getElementById(span_id + '_error');   
   
  if (error_tag) {
    if (error_message!=error_tag.innerHTML){
            //Bug 15003 NJ - detach the node from the DOM, make changes and reattach for performance gains.
            var $parent = $(error_tag).parent();
            $(error_tag).detach();
       if (error_message!="" )  {
         //Bug#7063 ANU 
                if (error_message.indexOf("Valid GL") == 0) {
            error_tag.className = "gl_row";
            error_tag.style.fontWeight="bold";
            error_message= error_message.replace("Valid GL","");
          }
           //Bug#7063 end
          // Bug #10264 Mike O - Use proper DOM code instead of innerHTML
                $(error_tag).html("<div>" + label + " " + error_message + "</div>");

          // End Bug #10264 Mike O
       }
       else {
                $(error_tag).text("");
       }
            $parent.append($(error_tag));
            //end bug 15003
    }

  }
  // Bug #10407 Mike O - Refresh in case the validation message has messed up the containing table
    //refresh_scrollable_all();
    
  return null;
}

/* ----------------- Parameterized types below.  Same signature. */

function one_of_is_type_for (value, key, args){
    for (d in args) {
   if(value == args[d])
     return true;
 }
 return "must be one of the following: " + args;  
}

function one_of_type_is_type_for (value, key, args){

  var type_args = new Array(value, key);
  var type_method = null;
  for(e in args) {
       type_method = window[args[e] + '_is_type_for'];
       if (type_method)
          if ( type_method.apply(this,type_args) == true) 
             return true;
  }
  return 'must be one of: ' + args;
}

function range_of_is_type_for (value, key, args) {
  //lert('value:' + value + "\nkey:" + key + "\nargs:" + args);
  var VF  = value;
  var result = true;
  var all_args = args[2];

  var min = args[0];
  var max = args[1];
  var include_max = args[2];
  var pad_char = args[3];
 
  if (typeof min == 'string') {
    VF = pad(VF,max.length,"right",pad_char);
    if (VF < min)
      result = "must be at least " + min + ".";
    else if (include_max && VF > max )
      result = "must be less than or equal to " + max + ".";
    else if ( !include_max && VF >= max)
      result = "must be less than " + max + ".";
  } else {
    var VF = parseFloat(VF);
    if ( isNaN(VF) ) 
      result = "must be a number.";
    min = args[0] + 0.0;
    max = args[1] + 0.0;
    //lert(VF + " - " + min + " - " + max);
    if (VF < min)
      result = "must be at least " + min + ".";
    else if (include_max && VF > (max +0.000001) )
      result = "must be less than or equal to " + max + ".";
    else if ( !include_max && VF > (max - 0.000001) )
      result = "must be less than " + max + ".";
  }
  return result;
}

/* Renamed from text_is_type for such that instances of Type.text can properly
   make use of size and alternate character set constraints */
function text_is_type_for (value, key, args) {
  //lert('value:' + value + "\nkey:" + key + "\nargs:" + args);
  if ( typeof value != "string" ) {
    result = "must be a string."
  } else {
    var value = trim(value);
    var min_size = args[0];
    var max_size = args[1];
    var chars    = args[2];
    //lert(max_size == min_size);
    //aert(VF + " - " + min + " - " + max);
    var result = true;
    var type_method = window[chars + "_is_type_for"];
    if (type_method) {
        var R = type_method.apply(this, [value, key]);
        if ( R != true )
          result = R;
    }
    if (result == true) 
    {
        if (max_size==min_size && (value.length!=min_size))
          result = "must have exactly " + min_size + " characters.";
        else if (value.length < min_size)
          result = "must be at least " + min_size + " characters in length.";
        else if (value.length > max_size)
        result = "must be at most " + max_size + " characters in length.";
    }
  }

  // Bug 26783 DH - ABA Validation: invalid routing number can appear as if it's recognized
  if (result.toString().indexOf("must") != -1 || result.toString().indexOf("invalid") != -1 || result.toString().indexOf("true") != -1)// It can be a primitive val
  {
    if (key === "args.bank_routing_nbr")
    {
      var el = document.getElementById("args.bank_name");
      if (el != undefined)
        el.value = "";
    }
  }
  // end Bug 26783 DH

  return result;
}

function numeric_with_separators_is_type_for (value, key, args) {
  //lert('value:' + value + "\nkey:" + key + "\nargs:" + args);
  if ( typeof value != "string" )
    result = "must be a string."
  else {
    var min_size = args[0];
    var max_size = args[1];
    var max_separators = args[2];
    var chars = "numeric_with_separator"
    //lert(max_size == min_size);
    //lert(VF + " - " + min + " - " + max);
    var result = true;
    var type_method = window[chars + "_is_type_for"];
    if (type_method) {
        var R = type_method.apply(this, [value, key]);
        if ( R != true )
          return "can only have digits with dash and space separators.";
    }
   var validchars =  '0123456789';
   var s = "";
        for (var i = 0; i < value.length; i++) {
    var letter = value.charAt(i);
     if (validchars.indexOf(letter) != -1)
        s = s + letter;
   }
   var number_of_separators = value.length - s.length;
   if (number_of_separators > max_separators) {
     result="can only have " + max_separators + " separator";
     if(max_separators > 1) result += "s";
     result += ".";
   }
   else if (max_size==min_size && (s.length!=min_size))
     result = "must have exactly " + min_size + " characters, but you have " + s.length + ".";
   else if (s.length < min_size)
     result = "needs " + min_size + " or more digits, but you have only " + s.length + ".";
   else if (s.length > max_size)
     result = "needs " + max_size + " or fewer digits, but you have " + s.length + ".";
  }
  return result;
}


function mask_is_type_for (value, key, args) {
  //lert('value:' + value + "\nkey:" + key + "\nargs:" + args);
  var pattern = args[0];
  var result = true;
  if (pattern.length != value.length) {
    result = "invalid format.";
  }
  return result;
}

/* test
var field_types = { amount: [ "numeric", ["range_of",1,10] ] }
var XX = validation("11", "amount", "Amount paid");
//lert(XX == ";; must be less than 10");

var field_types = { amount: [ ["range_of",1,10] ] }
var XX = validation("11", "amount", "Amount paid");
//lert(XX == "; must be less than 10");

var field_types = { amount: [ ["range_of",1,10] ] }
var XX = validation("1", "amount", "Amount paid");
//lert(XX == ";");
*/

/* end of parameterized is_type_for functions */


 function select_all_toggle (tag) {
    if (are_all_selected(tag)) {
        set_checkboxes(tag, false);
     }
   else
     set_checkboxes(tag,true);
   CalcTotal(tag);
 }
 function are_all_selected (tag) {
   var inputs = document.getElementsByTagName("INPUT");
   for (var tag in inputs) {
     if (inputs[tag].className == "checkbox")
       if(inputs[tag].checked == false && inputs[tag].disabled == false)
          return false;
   }
   return true;
 }
 function set_checkboxes (tag, a_bool) {
   var inputs = document.getElementsByTagName("INPUT");
   for (var tag in inputs) {
     if (inputs[tag].className == "checkbox" && inputs[tag].disabled == false )
       inputs[tag].checked = a_bool;
   }
 }
 
function trim (a_string){
 return trim_end(trim_start(a_string));
}

function trim_end (VALUE){
var w_space = String.fromCharCode(32);
var v_length = VALUE.length;
var strTemp = "";
if(v_length < 0){
return"";
}
var iTemp = v_length -1;
while(iTemp > -1){
if(VALUE.charAt(iTemp) == w_space){
}
else{
strTemp = VALUE.substring(0,iTemp +1);
break;
}
iTemp = iTemp-1;
}
return strTemp;
}

function trim_start (VALUE) {
var w_space = String.fromCharCode(32);
if(v_length < 1){
return"";
}
var v_length = VALUE.length;
var strTemp = "";
var iTemp = 0;
while(iTemp < v_length){
if(VALUE.charAt(iTemp) == w_space){
}
else{
strTemp = VALUE.substring(iTemp,v_length);
break;
}
iTemp = iTemp + 1;
}
return strTemp;
}
function pad (VALUE,length,align,a_char) {
var padding ="";   
for(var i=VALUE.length; i < length; i++)
  padding += a_char;
if(align == "left")
  return VALUE + padding; // left-align
else
  return padding + VALUE; // right-align
}

//Function to check valid date with fix format MM/DD/YYYY
// Called by: validate_batch_update_form, CASL's Type.date.is_type_for_js, file_modify_app.htm_inst
function ValidateDate (date){
	var datePat = /^(\d{1,2})(\/)(\d{1,2})(\/)(\d{4})$/;
	var matchArray = date.match(datePat); // is the format ok?
	if (matchArray == null)
	  return "must be entered in date format mm/dd/yyyy."; 
	
	month = matchArray[1]; // p@rse date into variables
	day = matchArray[3];
	year = matchArray[5];

	if (month < 1 || month > 12)  // check month range
		return "month must be between 1 and 12.";
	
	if (day < 1 || day > 31) 
		return "day must be between 1 and 31.";
	
	if ((month==4 || month==6 || month==9 || month==11) && day==31)
		return "month "+month+" doesn`t have 31 days!";
	
	if (month == 2) { // check for february 29th
		var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
		if (day > 29 || (day==29 && !isleap))
			return "February " + year + " doesn`t have " + day + " days!";
	}
  // Bug 26094 NAM:	
  if (date_in_range(date) == false){    
    return "can't be more than 120 years ago or 10 years in the future"
  }
	return ""; // date is valid	
}
//Function to determine if value passed is a number
function IsDigit(txtChar) {
	if( txtChar.length != 1 )
		return false;
	if( txtChar != "1" && txtChar != "2" && txtChar != "3" && txtChar != "4" && txtChar != "5" && 
		txtChar != "6" && txtChar != "7" && txtChar != "8" && txtChar != "9" && txtChar != "0")
		return false;
	return true;
}

// No longer used  var GLOBAL_script_to_execute_after_refresh="";

function set_focus_to(tag_id) {
 // Bug #10208 Mike O - Commented out this line
 //if ( !document.hasFocus() ) { return null; }
 //lert('should not be here in set_focus_to_aux'); // MP TEMP
 var tag_id=tag_id.replace(/\./g,"_");
 //lert("set_focus_to:" + tag_id);
 var current_app_tag=document.getElementById(tag_id);
  if (current_app_tag) {
    // Bug 24389 MJO - Only focus if the element is found
    window.focus(); // Bug 21430 MJO - Sometimes the window itself loses focus
    set_focus_within(current_app_tag);
 }
 //else 
    //lert("could not find tag: " + tag_id);   // for debugging
 return null;
}

function set_focus_within (a_tag) {
  // get first form inside of tag, then find first user text input or submit button
  // look for form, and then fine first input or button
  // if no form, then look for first "A" tag
   var tag_focus=get_has_focus_inside(a_tag);
   var forms_in_tag=tag_focus.getElementsByTagName("FORM");
   //lert("a_form:" + a_form.length);
   if ( forms_in_tag && (forms_in_tag.length >= 1) ) {
     set_focus_to_first_input(forms_in_tag[0]);
   } else {
     var links_in_tag = tag_focus.getElementsByTagName("A");
     if (links_in_tag && (links_in_tag.length >= 1)) {
       //lert(links_in_tag[0].onclick);
       focus_and_select(links_in_tag[0]);
     }
   }
}

function focus_and_select (a_tag) {
  try {
    if (a_tag.focus) a_tag.focus(); 
  } catch (e) {}
  try {
    if (a_tag.select) a_tag.select();
  } catch (e) {}
}

function x_contains_y (x, y) {
 if (x==y.parentElement) 
     return true;
 else if (y.parentElement)
     return x_contains_y(x, y.parentElement);
 else
   return false;
}

function get_has_focus_inside (a_tag) {
  var possible_focus=document.getElementsByName("has_focus");
  if (possible_focus) {
    var has_focus_tag=null;
    for(var i=0; i < possible_focus.length; i++) {
      has_focus_tag=possible_focus[i];
      if (x_contains_y(a_tag,has_focus_tag)) {
         return(has_focus_tag);
      }
    }
  }
  return a_tag;
}

function set_focus_to_first_input(aForm) {
 if (!document.forms[0]) return null;
	if (!aForm) {
	     aForm = document.forms[0];
  }
	if( aForm.elements[0]!=null) {
		var i;
		var max = aForm.length;
		for( i = 0; i < max; i++ ) {
		  if( aForm.elements[ i ].type != "hidden" &&
				!aForm.elements[ i ].disabled &&
				!aForm.elements[ i ].readOnly &&
				// Bug #10208 Mike O - Only select text fields, as the others are never used in practice anyway
				// Bug 16410 MJO - Select password fields as well
				(aForm.elements[ i ].type == "text" || aForm.elements[ i ].type == "password")) 
			{
				focus_and_select(aForm.elements[ i ]);
				break;
			}
		}
	}
}

function scroll_div_contents(aDiv){
  //lert("scroll_div_contents");
  if(aDiv!=null)
    aDiv.scrollTop=aDiv.scrollHeight-aDiv.clientHeight;
}

// May not be used anymore because new keyboard sends key sequence to process
// Only called by: BODY.onkeydown in Business_app.cacheable_htm_wrap
function process_function_key () {
     var keycode = event ? event.keyCode : keyStroke.which;
     var f_key= keycode-111;
    if ((f_key > 0) && (f_key < 13)) {
      var key_desc="F"+f_key;
      if (event.ctrlKey) key_desc="control-"+key_desc;
      if (event.shiftKey) key_desc="shift-"+key_desc;
      var CU=window.location.href;  // CU = current URI
      //lert(CU);
      var CU_array = CU.split("/");
      if (CU_array[CU_array.length-1]=='pos.htm')
        return false;
      var ext=CU.substring(CU.length-4); // ext=extension
      if ( CU.indexOf("?")==-1 ) {
        var uri=CU.substring(0,CU.length-4) + "/process_function_key" + ext + "?key=" + key_desc;
        //var uri="process_function_key" + ext + "?key=" + key_desc;
        //lert(uri);
        window.location.href=uri; // MP: Should call casl_link, not this.
      }
     }
   }



// ========================== previous main.js ======================
/* ================= form routine ================= */
// var field_types = {};  MP: DO NOT SET THIS HERE.  Is is set in an in-line SCRIPT tag found in Business_app.cacheable_htm_wrap
var selected_file_checkbox_id="no file selected";
function getItem(id) {
    var itm = false;
    if(document.getElementById)
        itm = document.getElementById(id);
    else if(document.all)
        itm = document.all[id];
    else if(document.layers)
        itm = document.layers[id];

    return itm;
} 

//BUG 4368 GL numbers not expanding/collapsing correctly BLL 9/11/08
function toggleItems(id, on) {
  if (id.match("gl_numbers_expanded")!=null) {
    if(!bExpandedSet && on!=null) {
      bExpanded=!on;
      bExpandedSet=true;
    }
  } else {
    bExpandedSet=false;
  }
  var itms = document.getElementsByTagName("div");
    for (var i = 0; i < itms.length; i++) {
  //lert("called 1" + i);
  // Bug #10205 Mike O - Must use getAttribute in non-IE browsers
        if (itms[i].getAttribute("name")) {
    //lert("called 2" + i);
      if ( itms[i].getAttribute("name") == id )
        toggleItem( itms[i].id, on );
    }
  }
  if (bExpandedSet) {
    if (bExpanded)
      bExpanded=false;
    else
      bExpanded=true;
  }
}
//end BUG 4368

// Bug 23022 MJO - Pass in button to toggle to simplify jQuery
function toggleItem(id, on, btn) {
    itm = getItem(id);
    if(!itm)
        return false;
    //BUG 4368 BLL 9/11/08
    if (bExpandedSet && id.match("gl_numbers_expanded")!=null) {
      if(bExpanded)
        itm.style.display = '';
      else
        itm.style.display = 'none';
    } else {
      if(itm.style.display == 'none')
        itm.style.display = '';
      else
        itm.style.display = 'none';
    }
    //end BUG 4368

  if ($(btn).hasClass("b_on"))
    $(btn).removeClass("b_on").addClass("b_off");
  else if ($(btn).hasClass("b_off"))
    $(btn).removeClass("b_off").addClass("b_on");

    return false;
}

function SlideToggleItem(btn) {
  if ($(btn).hasClass("b_on"))
    $(btn).removeClass("b_on").addClass("b_off");
  else if ($(btn).hasClass("b_off"))
    $(btn).removeClass("b_off").addClass("b_on");
}
 
function selectall(id) {
	var oSelection = getItem(id); //document.reporting_form.elements[select_name];
	for( var i=0; i<oSelection.length; i++ ){
		oSelection.options[i].selected='selected';
	}
	//lert(oSelection.length);
	return true;
}

function deselectall(id) {
	var oSelection = getItem(id); //document.reporting_form.elements[select_name];
	for( var i=0; i<oSelection.length; i++ ){
		oSelection.options[i].selected=false;
	}
	return true;
}

//bug 6392 files in batchupdate and reports will be selected and deselected.Siree
function tableselectall(tblname, sel) {
    var tbl = document.getElementById(tblname);
    //MPT bug 21292 added check for unable to get property 'rows' of undefined or null reference 
    if(tbl != null)
    {
        for (var i = 1; i < tbl.rows.length; i++) {
            // HX Fixed: Bug 22747 select all & remove all is not working, 
            // for batch update, childNodes[0] is "checkbox"; for report, childNodes[1] is "checkbox"
            if (typeof tbl.rows[i].cells[0].childNodes[0].type != "undefined") {
                if (tbl.rows[i].cells[0].childNodes[0].type == "checkbox")
                    tbl.rows[i].cells[0].childNodes[0].checked = sel;
            }
            else if (typeof tbl.rows[i].cells[0].childNodes[1].type != "undefined") {
                if (tbl.rows[i].cells[0].childNodes[1].type == "checkbox")
                    tbl.rows[i].cells[0].childNodes[1].checked = sel;
            }
          }
    }
    else
    return true;
}

function set_self_target (my) {
     my.form.target='_self';
}
function set_blank_target (my) {
     my.form.target='_blank'; 
}
function hide_date_range(date_select_id, from_date_name, to_date_name, from_date_calendar_name, to_date_calendar_name){
  var date_select = getItem(date_select_id); 
	 
  var from_date = getItem(from_date_name); 
  var to_date = getItem(to_date_name);
	 
  // Bug #14011 Mike O - Make sure the field_info attributes are parsed into objects
  get_js_object(from_date, "field_info");
  get_js_object(to_date, "field_info");
	 
  // BUG 5784 Dates keep resetting to today. They need to stay the same between refreshes of pages.
  var strFromDateValue = from_date.value;
  var strToDateValue = to_date.value;
  // end BUG 5784
	
  // Bug 17019 DJD/UMN - fix date ranges
  if (date_select.value == "daterange")
  {
    var strDate = $.datepicker.formatDate("mm/dd/yy", new Date());
		  
    // BUG 5784 Dates keep resetting to today. They need to stay the same between refreshes of pages.
    if (strFromDateValue.length == 0) from_date.value = strDate;
    if (to_date.value.length == 0)      to_date.value = strDate;
    // end BUG 5784.

    // Bug #14011 Mike O - Make date fields required (this is kind of hacky, and could break if the type changes)
    from_date.field_info.types[0][0] = "required";
    to_date.field_info.types[0][0] = "required";
  }
  else if (date_select.value == "all_today") 
  {
    from_date.value = to_date.value = $.datepicker.formatDate("mm/dd/yy", new Date());
  }
  else if (date_select.value == "all_today_plus_three") // Bug 26224 UMN
  {
    var today = new Date();
    from_date.value = $.datepicker.formatDate("mm/dd/yy", new Date());
    to_date.value = $.datepicker.formatDate("mm/dd/yy", new Date(today.getFullYear(), today.getMonth(), today.getDate() + 3));
  }
  else if (date_select.value == "all_yesterday") // all my troubles seemed so far away
  {
    var today = new Date();
    from_date.value = to_date.value = $.datepicker.formatDate("mm/dd/yy", new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1));
  }
  else if (date_select.value == "week_to_date")
  {
    var today = new Date();
    to_date.value = $.datepicker.formatDate("mm/dd/yy", today);
    from_date.value = $.datepicker.formatDate("mm/dd/yy", new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay()));
  }
  else if (date_select.value == "month_to_date") 
  {
    var today = new Date();
    from_date.value = $.datepicker.formatDate("mm/dd/yy", new Date(today.getFullYear(), today.getMonth(), 1));
    to_date.value = $.datepicker.formatDate("mm/dd/yy", new Date());
  }
  else if (date_select.value == "quarter_to_date") 
  {
    var today = new Date();

    // Bug# 17621 DH
    //var month = (((today.getMonth() - 1) / 3) * 3) + 1; // Dennis magic - // Bug# 17621 DH - Disabled
    var month = (Math.floor((today.getMonth()) / 3) * 3) + 1; //TTS17621 SX javascript month is already minus 1
    from_date.value = $.datepicker.formatDate("mm/dd/yy", new Date(today.getFullYear(), month-1, 1));
    // end Bug# 17621 DH 
    
    to_date.value = $.datepicker.formatDate("mm/dd/yy", new Date());
  }
  else if (date_select.value == "year_to_date")
  {
    var today = new Date();
    from_date.value = $.datepicker.formatDate("mm/dd/yy", new Date(today.getFullYear(), 0, 1));
    to_date.value = $.datepicker.formatDate("mm/dd/yy", today);
  }
  // END Bug 17019 DJD/UMN - fix date ranges

  return true;
}

// UMN is this needed anymore? 
function not_blur_on_other_control(me, control_id, control_value) {
	 var a_control = getItem(control_id); 
	 if ( a_control &&
	      a_control.value != control_value ) {
		 me.blur();
	 }
	 return true;
}

 function hide_core_files(date_select_id, core_files){
	 var date_select = getItem(date_select_id); 
	 var core_file = getItem(core_files); 
	 if( date_select.checked != true) {
		 //change the color of from and to fields and lost focus too.
		 core_file.disabled = true;
	 }
	 else {
		 core_file.disabled = false;
	 }
	 return true;
 }
 
function show_prompt_for_selection(me, prompt_id, selection_name,
  no_selection_message, single_selection_message, multiple_selection_message, all_selection_message) {
 if(getStringField(no_selection_message) == "") var no_selection_message="No " + selection_name + " is selected.";
 if(getStringField(single_selection_message) == "") var single_selection_message="One " + selection_name + " is selected.";
 if(getStringField(multiple_selection_message) == "") var multiple_selection_message="Multiple " + selection_name + "s are selected.";
 if(getStringField(all_selection_message) == "") var all_selection_message="All  " + selection_name + "s are selected.";

 var selected_options = new Array();
	for( var i=0; i<me.length; i++ ){
        if (me.options[i].selected == true) {
		
	    var the_value = ""+me.options[i].innerHTML;
	     
	    // cutoff the string value after the first two space
	    var two_space_index = the_value.indexOf("&nbsp;&nbsp;");
     if (two_space_index != -1) {
          the_value= the_value.substring(0,two_space_index);
      }	    
	    selected_options.push(the_value);
	  }
	 
	}
	var prompt_html = getItem(prompt_id);
	if( selected_options.length == 0 )
	   prompt_html.innerHTML=no_selection_message;
	else if( selected_options.length == 1 )
	   prompt_html.innerHTML=single_selection_message + "(" + selected_options.join("") + ")" ;
	else if( selected_options.length == me.length )
	   prompt_html.innerHTML=all_selection_message;	   
	else // >1 && < me.length
	   prompt_html.innerHTML=multiple_selection_message;
	return true;
}

//bug 6407 Siree
function show_prompt_for_selection_table(tbl, prompt_id, selection_name, no_selection_message, single_selection_message, multiple_selection_message, all_selection_message) {
	if(getStringField(no_selection_message) == "") var no_selection_message="No " + selection_name + " is selected.";
	if(getStringField(single_selection_message) == "") var single_selection_message="One " + selection_name + " is selected.";
	if(getStringField(multiple_selection_message) == "") var multiple_selection_message="Multiple " + selection_name + "s are selected.";
	if(getStringField(all_selection_message) == "") var all_selection_message="All  " + selection_name + "s are selected.";
	var selected_options = new Array();
    for (var i = 1; i < tbl.rows.length; i++) {
	    tbl.rows[i].cells[0].childNodes[0];
		var tbl_row=tbl.rows[i];
		var tbl_cell=tbl_row.cells[0];
		var chkBox = tbl_cell.childNodes[0];

        if (chkBox.checked) {
		var tbl_cell2 = tbl_row.cells[1]; // Need to find which TD Element
		var the_value = "" + tbl_cell2.innerHTML;
		selected_options.push(the_value);
		}
	}
	var prompt_html = document.getElementById(prompt_id);
	if( selected_options.length == 0 )
	prompt_html.innerHTML=no_selection_message;
	else if( selected_options.length == 1 )
	prompt_html.innerHTML=single_selection_message + "(" + selected_options.join("") + ")" ;
	else if( selected_options.length == tbl.rows.length-1 )
	prompt_html.innerHTML=all_selection_message;    
	else if( selected_options.length  >1 && selected_options.length < tbl.rows.length )
	prompt_html.innerHTML=multiple_selection_message;
	return true;
 }

///////////////////////////////////////////////////////////////////
//
// getStringField(str) is an optional function that can be used to 
//  convert JScript "null" and "undefined" to a string.  This 
//  function is very useful for all Recordset fields.  Remember
//  a Recordset field is not a string as it is returned from 
//  SQL.
// 
// Parameter: theRSField - the parameter to convert into a string
//
// Comments: use this function for 
//		- Request.Form("xxx") and Request.QueryString("xxx")
//		- database Recordset queryRslt("xxx")
//		- Sessiong("xxx")
function getStringField(theRSField) {
  var theStr = String(""+theRSField);
  if (theStr == "null" || theStr == "undefined") return "";
  else return theStr;
}
         
function enable_input(id) {
    itm = getItem(id);
    itm.disabled=false;
    return true;
}

/* ================= end of form routine ================= */

/* cbutton actions Adding the refresh after changing the class 
of a tag avoid the weird repaint bug. M.P. 04/20/07
*/
function button_on (tag) {
  if( tag.className.indexOf('b_off') != -1 )
     tag.className=tag.className.replace(/b_off/g,"b_on");  // Bug 3939: Dynamically changing the style could cause white border bug.
  else
    tag.className=tag.className.replace(/b_on/g,"b_off"); 
  refresh_scrollable(tag);  
  return null;
}

/* UNUSED*/
function button_off (tag) {
 tag.className=tag.className.replace(/b_on/g,"b_off");
}

function button_submit (tag) {
  // Bug 19567 MJO - Moved submit button next to this element to prevent bubbling into itself
  var button = $(tag).next()[0];
 
 button["clicked"] = true; // Identify this as the clicked button!
 button.click();  // this won't submit the button's value: form.submit();
}
/* end cbutton actions */

function toggle_visibility (tag) {
    var collapse_control = tag;
    //lert(tag.id);
    var collapseRow1name = tag.id.replace('_top', '_actions');
    var collapseRow2name = tag.id.replace('_top', '_details');
    var collapseRow3name = tag.id.replace('_top', '_images');
    var collapseRow4name = tag.id.replace('_top', '_transactionnbr');  //BUG#12663
    
    var collapseRow1 = document.getElementById(collapseRow1name);
    var collapseRow2 = document.getElementById(collapseRow2name);
    var collapseRow3 = document.getElementById(collapseRow3name);
    var collapseRow4 = document.getElementById(collapseRow4name);  //BUG#12663
    
    if (collapseRow1.style.display=="none") {
        collapse_control.className=collapse_control.className.replace('_off','_on');
        collapseRow1.style.display = "";
        if (collapseRow2!=null)
           collapseRow2.style.display = "";
        if (collapseRow3!=null)
           collapseRow3.style.display = "";
        if (collapseRow4!=null)       //BUG#12663
           collapseRow4.style.display = "";
   } else {
        collapse_control.className=collapse_control.className.replace('_on','_off');
        collapseRow1.style.display = "none";
        if (collapseRow2!=null) 
          collapseRow2.style.display = "none";
        if (collapseRow3!=null)
            collapseRow3.style.display = "none";
        if (collapseRow4 != null)
            collapseRow4.style.display = "none";  // BUG#12663
   }
   if(jQuery)//BUG#12201
   {
      var collapseRowNestedCSS = "." + tag.id.replace('_top', '_nested');
      var collapseRowNestedDetailCSS ="." + tag.id.replace('_top', '_nested_details');
      $(collapseRowNestedCSS).toggle();  
      $(collapseRowNestedDetailCSS).toggle();   
   }
}

/* start of JS remote calling */
function function_get_name (a_function) {
  return a_function.toString().match(/function (\w*)/)[1];
}

// Called by session_about_to_timeout
function get_current_page_root () {
  var T;
 
  if (is_use_client()) {
    T = get_current_site() + "/" + get_current_obj().replace(/\./g,"/");
    return T;
  }

  var full_href=window.location.href;
  var start_of_extension=full_href.lastIndexOf(".");
  var R;
  if (start_of_extension==-1) {
    R = full_href;
  } else {
    var R = full_href.substring(0,start_of_extension);
  }
  
  return R;
}
function get_current_site () {  // http://localhost/pos_demo/foo/bar => http://localhost/pos_demo
// TODO: simplify using location.pathname;
 var full_href=window.location.href;
 var start_protocol=full_href.indexOf("//");
 var start_site=full_href.indexOf("/",start_protocol+2);
 var start_path=full_href.indexOf("/",start_site+1);
 if (start_path==-1)
   return full_href;
 else
   return full_href.substring(0,start_path);//+1);
}

// Bug 25938 UMN init JS logger
try {
  JL.setOptions({
    "defaultAjaxUrl": get_current_site() + "/jsnlog.logger.js_c?"
  });
} catch (e) { } // Do nothing on failure


function is_use_client () {
  return get_pos() != null && get_pos().use_client;
}
function get_current_obj () {  // http://localhost/pos_demo/foo/bar => my.foo.bar
// TODO: simplify using location.pathname;

  if ( get_pos() != null && is_use_client() && get_pos().current_obj != null ) {
    return get_pos().current_obj;
  }

 var full_href=window.location.href;
 var start_protocol=full_href.indexOf("//");
 var start_site=full_href.indexOf("/",start_protocol+2);
 var start_path=full_href.indexOf("/",start_site+1);
 if (start_path==-1)
   return "my";
 else {
   var path_no_ext=full_href.substring(start_path+1,full_href.lastIndexOf("."));
   return path_no_ext.split("/").join(".");
 }
}

function to_query_string (args) {
 var R="";
 for (i in args) {
   V = args[i];
   if ((i=="_parent") && ((V.length<4) || (V.substring(0,4)!="GEO.")) )
     V = "GEO." + V;
   R = R + i + "=" + escape(V) + "&";
 }
 return R.substring(0,R.length-1);
}

/* Can't use getXMLHTTPObject to POST a request and set the URL properly because you
   can't get at the 303 redirect and you can't get the headers
function getXMLHTTPObject (){
  var objhttp=(window.XMLHttpRequest)?new XMLHttpRequest():new ActiveXObject('Microsoft.XMLHTTP');
  if(!objhttp){return};
  objhttp.onreadystatechange=displayStatus;
  return objhttp;
}
function sendRequest (url, data, method, mode, header) {  // See replacement of send_using_form below
  if(!data){data=''};
  if(!method){method='post'};
  if(!mode){mode=true};
  if(!header){header='Content-Type:application/x-www-form-urlencoded; charset=UTF-8'};
  objhttp=getXMLHTTPObject();
  objhttp.open(method,url,mode);
  objhttp.setRequestHeader(header.split(':')[0],header.split(':')[1]);
  objhttp.send(data);
}
function displayStatus () {
  if(objhttp.readyState==4){
    var X=window.open();
    X.document.open();
    X.document.write(objhttp.responseText);
    X.document.close();
  }
}
*/
function submit_using_form (action, args, http_method, target) {
  /* create FORM with action and method and display:none, for each arg, create input, then submit FORM */
  var target_str="";
  if (target!="") {
    target_str=" target='" + target + "'";
  }
  var form_text="<FORM style='display:none' action='" + action +
    "' method='" + http_method + "'" +
    target_str + "/>";
  var a_form=document.createElement(form_text);
  var a_input=null;
  for (i in args) {
    V = args[i];
    if ((i=="_parent") && ((V.length<4) || (V.substring(0,4)!="GEO.")) ) {
      V = "GEO." + V;
    }
    a_input=document.createElement("<INPUT name='" + i + "' value='" + V + "'/>");
    a_form.appendChild(a_input);
  }
  document.appendChild(a_form);
  if (is_use_client()) {
    //lert("using clientside :) !");
    casl_form(a_form, window[target]);
  } else {
    //lert("using serverside :( !");
    a_form.submit();
  }
}

// takes associative array and returns string representation
function to_js(obj) {
 var result="{";
  for (K in obj) {
    var V=obj[K];
    if(typeof(V)=="object") {
      result=result+" "+K+":"+to_js(V);
    } else {
      result=result+" "+K+":"+V;
    }
  }
 return(result+"}");
}

// Creates a copy of obj where {args.arg1:'foo'} becomes {args:{arg1:'foo'}}
function inflate(obj) {
  var nestedObj={};
  var nestedSubObjs=new Array();
  for(K in obj) {
    var V=obj[K];
    
    if(K.indexOf(".")==-1) {
      nestedObj[K]=V;
      if(typeof(V)=="object") {
        nestedSubObjs.push(K);
      }
    } else {
      var key=K.substring(0, K.indexOf("."));
      var subKey=K.substring(K.indexOf(".")+1);
      if(nestedObj[key]==null) {
        nestedObj[key]={};
        nestedSubObjs.push(key);
      }
      nestedObj[key][subKey]=V;
    }
  }
  
  for(i in nestedSubObjs) {
    var K=nestedSubObjs[i];
    var V=nestedObj[K];
    nestedObj[K]=inflate(V);
  }
  
  return nestedObj;
}

// takes associative array and returns geo string representation
function to_geo(obj) {
  var parent="GEO";
  var arg_string=" ";
  
  //Unflatten the object
  var nestedObj=inflate(obj);
  
  //Form the args string  
  for(i in nestedObj) {
    var K=i;
    var V=nestedObj[K];
    if(K == "_parent") {
      parent=V;
    } else {
      var V_string;
      //lert("Type of " + V + ": " + typeof(V));
      if(typeof(V)=="string"){
        V_string=to_js_string(V);
      } else if(typeof(V)=="object") {
        V_string=to_geo(V);
      } else {
        V_string=V;
     } // TODO: Add recursion here for nested objects.

     //lert("K: [" + K + "], V: [" + V + "], VS: [" + V_string + "]");
     arg_string=arg_string + K + "=" + V_string + " ";
    }
  }
  var result="<" + parent + ">" + arg_string + "</" + parent + ">";
  //lert(result);
  return result;
}

// smart quotes a js string
function to_js_string(str) {
  //lert( '\"' + html_encode(str).replace(/"/g, '&amp;quot;') + '\"' );
  return "'" + html_encode(str).replace(/'/g, "\\'") + "'";
}

// call_method=foo.bar.baz    base/foo/bar/baz  
// February 26, 2010, Bug #8530, CLee
// post_remote rewritten to use XMLHTTPRequest
function post_remote(subject,call_method, args, target, asynchronous, success, failure) {
  if(!target) { target=""; }
	submit_post_remote_xml(subject, call_method, args, target, asynchronous, success, failure);
}

var POST_REMOTE_MAX_RETRY = 5;
function submit_post_remote_xml(subject, call_method, args, target, asynchronous, success, failure) {
  if (asynchronous != true) { asynchronous = false; }
	// Bug#9100 Mike O
	var target_window = get_top();
	var target_string = "";
	// Set up the target to receive the result
  if (typeof (target) == "string" && target != "") {
		target_string = target;
    if (target == "pos_frame") { // Hack
      try {
        target_window = get_pos().GetWindow("pos_frame");
        // Bug#9100 Mike O
        if (target_window == null) target_window = get_top().pos.pos_frame;
      } catch (e) { } // Do nothing on failure
    } else if (target != "_self") {
      try {
        target_window = GetWindow(target);
        if (target_window == null) target_window = eval(target);
      } catch (e) { } // Do nothing on failure
    }
  }
    // Bug 23970 MJO - Support no target
  // Bug 25482 MJO - This didn't do anything before
  else if (target === null || target == "") {
      target_window = null;
    }

	// Construct the url and body
	// Bug #8987 - Removed extra 'r' argument that was breaking post_remote
	var url = get_current_site() + "/post_remote.htm?";
	var body = "e_args=" + encodeURIComponent(Base64.encode(to_geo(args))) + "&a_method=" + call_method;
	body += "&a_target=" + target_string;
  if (typeof (subject) == "string" && subject != null) {
    // Bug 23275 MJO - Encode subject in case there are quotes
    body += "&subject=" + encodeURIComponent(subject);
  }
  
  // Bug 16648 MJO - Return double-submit value with all requests
  // Bug 19911 MJO - Check to see if main exists
  if (body.indexOf("__DOUBLESUBMIT__") == -1) {
    if (get_top().main != null && typeof(get_top().main != null) != "undefined")
    body += "&__DOUBLESUBMIT__=" + get_top().main.double_submit_value;
    else
      body += "&__DOUBLESUBMIT__=" + get_top().double_submit_value;
  }
  
  // Bug #11869 Mike O - Pass in session validation info
  // Bug #11880 Mike O - Handle the case where there's no frameset
  var session_validator = null;
  if (get_top().main === null || get_top().main === undefined) // Business Center
    session_validator = get_top().document.getElementById('session_validator');
  else
    session_validator = get_top().main.document.getElementById('session_validator');
  if (session_validator != null) {
    body += "&app_name=" + session_validator.getAttribute("app_name") + "&app_id=" + session_validator.getAttribute("app_id");
  }
  
	// Construct the XML object
  var xml_obj = make_xml_request();
  xml_obj.open("POST", url, asynchronous);
  if (xml_obj.timeout) {
		xml_obj.timeout = 10000;
  }

	// BUG# 9523 DH - Replaced the content type on the client and modified the server parsing code
  // to recreate key-value pair form data structure.
	xml_obj.setRequestHeader("Content-type", "text/xml");
	//xml_obj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

  // Bug 18095 UMN Removed, can't set those headers from JS!
	//xml_obj.setRequestHeader("Content-length", body.length);
	//xml_obj.setRequestHeader("Connection", "close");

  if (asynchronous) {
    {
      lastPostRemote = body;
    xml_obj.onreadystatechange = createHandler(xml_obj, target_window, success, failure);
    }
		xml_obj.send(body);
  } else {
    // Bug #13677 Mike O - Show processing message during wait
    if (target_window.cover)
      target_window.cover();
		var status = 0;
		var retries = 0;
    var handler = createHandler(xml_obj, target_window, success, failure);
		while (status == 0 && retries < POST_REMOTE_MAX_RETRY) {
			xml_obj.send(body);
      if (xml_obj.status) {
				status = xml_obj.status;
  }
    }
    
    // Bug #8668 Mike O - Handle both async and sync in the same way: populate target_window and call success/failure
    handler();
    // Bug #13677 Mike O - Hide processing bar
    if (target_window.hide_cover)
      target_window.hide_cover();
	}
}

/* Example:
GEO.<defmethod _name='foo'> total=0=Integer card_number=""=String 
  GEO.<set_part> xxxx=<get_args/> </set_part> 
  <echo> xxxx </echo>
  GEO.xxxx
</defmethod>
GEO.<defmethod _name='show_foo'>
   <BODY>
    <SCRIPT> src=<join> baseline_static "/business/include/main.js" </join> </SCRIPT>
    <SCRIPT> foo.<to_javascript_proxy/> </SCRIPT>
    <SPAN> onclick='foo( { total: 10, card_number: "234234234"});' "Call Foo 1" </SPAN>
   </BODY>
</defmethod>

foo.<to_javascript_proxy/> RETURNS:
function foo (args) { post_remote('foo', args); }

 On page: http://localhost/pos_demo/show_foo.htm?
 that includes this JavaScript function defintion,
 function foo (args) { post_remote('foo', args); }
 clicking on this span:
 <SPAN onclick='foo( { total: 10, card_number: "234234234"});'>Call Foo 1</SPAN>
 
 Would post to this URL:
 http://localhost/pos_demo/foo.htm
 a content of form-encoded data like this:
 total=10&card_number=234234234
 
 Which shows this URL after a redirect:
 http://localhost/pos_demo/xxxx.htm
 
 Could be extended to handle complex nested data objects in JavaScript.
*/
/* end of remote calling */

function execute_only_once (a_call_str, call_count) {
  // Bug#9100 Mike O
  if (get_top() == window) return;  // lert('You are not in a frame.');
  if (!get_top().client_calls_count) get_top().client_calls_count=0;
  if ( call_count > get_top().client_calls_count ) {
    //lert("will execute: " + a_call_str);
    get_top().client_calls_count=call_count;
    eval(a_call_str);
  }
  //else
  //lert('server call_count is: ' + call_count + ', but this call was already executed because client has: ' + window.top.client_calls_count);
  //lert(request_uri);
  //window.location.href=request_uri; 
}

/*
function prompt(prompt_txt) {
  if (prompt_box != null) {
    prompt_box.innerHTML = prompt_txt;
  }
}
*/
function prompt(prompt_txt) {
  alert("Function prompt deprecated");
}


/*
function open_file_status(){ 
 var test = true;
 if((selected_file_checkbox_id.indexOf("balanced_")!=-1) && (document.getElementById(selected_file_checkbox_id).value=="on"))
   test = confirm("You are about to reopen a balanced file. Are you sure you want to do that?");
 selected_file_checkbox_id="";
 return test
}*/

// used in file_selection_app
function is_selected_file_active(){
 if((selected_file_checkbox_id.indexOf("balanced_")!=-1) && (document.getElementById(selected_file_checkbox_id).checked))
     return true; 
 else
    return false;
}

/* start of methods for showing message on old pages */
function get_cookie ( name ) {
  //lert("GET_COOKIE: " + document.cookie);
  var start = document.cookie.lastIndexOf( name + "=" );
  var len = start + name.length + 1;
  if ( ( !start ) &&
       ( name != document.cookie.substring( 0, name.length ) ) 
     )
  { return null; }
  if ( start == -1 ) return null;
  var end = document.cookie.indexOf( ";", len );
  if ( end == -1 ) end = document.cookie.length;
  return unescape( document.cookie.substring( len, end ) );
}

function get_most_recent_uri () {  // was: get_most_recent_page_id
 var UR=get_cookie(window.name + "_last_page");  
 if (UR==false || UR==null) return "";
 //var a_num=parseInt(cookie1);
 //lert("get cookie value for get_most_recent_uri:" + window.name + "_last_page=" + UR );
 //if (a_num) return a_num; else return 0;
 return UR;
}
function go_to_most_recent_page () {
 window.location=get_most_recent_uri(); // GLOBAL_top_app + "/h/" + get_most_recent_timestamp() + ".htm";
}
function save_recent_uri_cookie () {
 document.cookie=window.name + "_last_page=" + escape(location.href) + "; path=/" ;
}
/* See doc in: CASL: js_setup_page_history_blocking  */
function setup_page_history_blocking () {  // No longer called.  MP 2007-05-04
  //return null;  // comment in to disable history blocking
  var last_page=get_most_recent_uri(true);
  var the_referrer=document.referrer;
  var this_page=location.href; 
  //lert("this_page: " + this_page + ", referrer: " + the_referrer + ", last_page: " + last_page );
  if ( (the_referrer==last_page) || 
       ( (last_page=="") &&
         (the_referrer=="" || the_referrer.indexOf("/my/")==-1)  // no referrer or referrer from static page
       ) 
     ) { // This page is new.  
    // Save uri and timestamp in cookie: {window_name}_last_page
    //lert('this is latest page. set cookie: ' + window.name + "_last_page=" + this_page );
    save_recent_uri_cookie();
  } else if (this_page==last_page || (document.referrer=="" && last_page=="")) {
    //lert("same page -- do nothing");
  } else {   // showing old page 
    //lert("this is old page.  Current page: " + get_most_recent_uri(true) + ", this_page: " + this_page);
    show_cover();   // don't let user click
    if(getInternetExplorerVersion() == -1)
      document.getElementById("old_page").style.display="table";  // show yellow warning message
    else    
    document.getElementById("old_page").style.display="block";  // show yellow warning message
  } 
  // return nothing 
}

// proxy to calling casl in pos.htm
function casl_form (the_form, target) {
  //lert("cf0");
  // Bug#9100 Mike O
  var pos_window=get_top().frames.ipayment_client;
  if (pos_window)
    pos_window.casl_form_aux(the_form, target);
  else 
    alert("Frame with CASL runtime not found.");
}

// 22074 Get Reason Code called by Search_result.<defmethod _name='htm_show_void'> in Admin Portal
function getreasoncode(link) {
    var sel = document.getElementById('actionreason');
    var code = document.getElementById('actionreason').value;
    if (!(code == '' || code == ' ')) {
        desc = sel.options[sel.selectedIndex].text;
        code += '|';
        code += desc;
        link = link.replace('actionreason=', 'actionreason=' + code);
    }
    var comment = document.getElementById('void_comment').value;
    if (!(comment == '' || comment == ' ')) {
        link = link.replace('void_comment=', 'void_comment=' + comment);
    }
    return link;
}

// 24018 SX support ewallet item remove and update button 
function removewalletitem(thebutton) {

  var r = confirm("Are you sure to remove Payment Item?");
  if (r == true) {
    var rowSel = $(thebutton).parent().parent();
    var errSel = rowSel.find('.REMOVE_ITEM_BTN span.error');

    var itemname = thebutton.name.replace(".REMOVE_ITEM_BTN", "").replace("args.", "");

    var theurl = oCBTBlocksData.sURLRootApp.replace(".htm?", "/removewalletitem.htm?ItemPath=" + itemname + "&");
    $.ajax({
      "url": theurl,
      type: "POST",
      "dataType": "json",
      "success": function (data) {
        if (data.e_status == "approved") {
          errSel.html('');
          errSel.append('<img id="removewalletitemresult" style="width: 20px; height: 20px;margin-bottom:-5px;" alt="WalletRemoved" src="../baseline/batch_update/images/checked_box.png">')
        } else {
          errSel.html('');
          errSel.append('<img id="removewalletitemerror" style="width: 20px; height: 20px;margin-bottom:-5px;" alt="WalletFailure" src="../baseline/batch_update/images/unchecked_box.png">')
        }
      }
    });
  }
}

function updatewalletitem(thebutton) {

  var r = confirm("Are you sure to update Payment Item?");
  if (r == true) {
    var rowSel = $(thebutton).parent().parent();
    var EXPIRATION_DATE_MMYY = rowSel.find('.EXPIRATION_DATE_MMYY input').val();
    var ADDRESS = rowSel.find('.EXT_06 input').val();
    if (typeof ADDRESS === 'undefined') { ADDRESS = ""; }
    var ZIP = rowSel.find('.EXT_09 input').val();
    if (typeof ZIP === 'undefined') { ZIP = ""; }
    var ITEM_NAME = rowSel.find('.ITEM_NAME input').val();
    var errSel = rowSel.find('.UPDATE_ITEM_BTN span.error');

    var itemname = thebutton.name.replace(".UPDATE_ITEM_BTN", "").replace("args.", "");
    var theurl = oCBTBlocksData.sURLRootApp.replace(".htm?", "/updatewalletitem.htm?ItemPath=" + itemname + "&EXPIRATION_DATE_MMYY=" + EXPIRATION_DATE_MMYY +
      "&ADDRESS=" + ADDRESS + "&ZIP_CODE=" + ZIP + "&ITEM_NAME=" + ITEM_NAME + "&");
    $.ajax({
      "url": theurl,
      type: "POST",
      "dataType": "json",
      "success": function (data) {
        if (data.e_status == "approved") {
          errSel.html('');
          errSel.append('<img id="updatewalletitemresult" style="width: 20px; height: 20px;margin-bottom:-5px;" alt="WalletUpdated" src="../baseline/batch_update/images/checked_box.png">')
        } else {
          errSel.html('');
          errSel.append('<img id="updatewalletitemerror"style="width: 20px; height: 20px;margin-bottom:-5px;" alt="WalletFailure" src="../baseline/batch_update/images/unchecked_box.png">')
        }
      }
    });
  }
}

// Called by: many places in JS
function casl_link (the_link, target, hide_cover) {  // cover was hide_wait_dialog
  if (target) {} else target=window;
  if (hide_cover) {} else hide_cover=false; // defaults: target=window hide_wait_dialog=false
  
  // Bug#9100 Mike O
  var pos_window=get_top().frames.ipayment_client;

  // Moved here from go so that it works for casl_link_aux as well.
  var TARGET = window;
  if ( target != null ) {
    if ( typeof(target)=="string" ) {
      if (target=="pos_frame") {
        TARGET = get_pos().pos_frame//window.
      } else {
        TARGET = top[target];
      }
    } else {
      TARGET = target;
    }
  }
  if ( TARGET==window && hide_cover==false && is_defined("cover", TARGET) ) {
    TARGET.cover();
  }
  if ( is_use_client() && pos_window ) {
    pos_window.casl_link_aux(the_link, target);
  } else {
    //lert("Go in casl_link, is_use_client(): " + is_use_client() + " pos_window: " + pos_window);
    go(the_link, target, hide_cover);
  }
}
 
function isInteger(a_value) {
  return ( a_value % 1 == 0 );
}

function scroll_to(target_id){
  var target=document.getElementById(target_id);
  var container=null;
  if(target) {
    container=get_scrollable_container(target);
  }
  //var x=0;
  var y=0;
  
  if(!target || !container) {
    return;
  }
              
  while(target != container && target != null){
    //x += target.offsetLeft;
    y += target.offsetTop;
    target=target.offsetParent;
  }
  //lert(x + "," + y);
  //lert(to_js(container));
  
  container.scrollTop = y;
}

function logout () {
  var logout_location=get_current_site() + "/" +
                      get_current_obj().replace(".","/") +
                      "/logout.htm?&";
  var xml_obj=null;
  
  //lert(logout_location);

    if (window.XMLHttpRequest) {
        xml_obj = new XMLHttpRequest()
  } else if (window.ActiveXObject) { xml_obj = new ActiveXObject("Microsoft.XMLHTTP"); }
  
  xml_obj.open("GET", logout_location);
  xml_obj.send();
}

/*function test_method() {
  alert("test_method");
}*/

function get_pos() 
{
  MAX_POS_DEPTH=10;
  var search_queue=new Array();
  // Bug 16101 MJO - Start in current window, as the parent might be in another domain
  search_queue[0]=window;
  var p_window=null;
  // Bug 16101 MJO - Stop if the search_queue is empty
  for(i=0;i<MAX_POS_DEPTH && p_window==null && search_queue.length > 0;i++)
  {
    current=search_queue.shift();
    
    // Bug# 18706 DH - I had to pull in 18241
    // Bug 18241 MJO - HACK: Make sure pos isn't a primitive
    if(current.pos && current.pos.name != null)
      p_window=current.pos;
    else {
      // Bug #13857/14964 Mike O - Don't jump into another domain
	    if(current.opener && !IsCrossDomain(current.opener))
	      search_queue.push(current.opener);
	    else if(current.parent && !IsCrossDomain(current.parent))
	      search_queue.push(current.parent);
	    // End Bug #13857/14964 Mike O
    }
        //end bug 14144
  }
  
  return p_window;
}

function GetWindow(target) {
  // Bug #9100 Mike O - Special-case pos_frame, as it doesn't work properly when in an iframe
    if (target == "pos_frame") {
    var the_top = get_top();
    return the_top.pos.pos_frame;
  }  
  
  // Bug #13681 Mike O - Try to grab the window properly first
  $iframes = $('iframe[name="' + target + '"]');
  if ($iframes.length == 1)
    return $iframes[0].contentWindow;
  $frames = $('frame[name="' + target + '"]');
  if ($frames.length == 1)
    return $frames[0].contentWindow;
  
  var MAX_DEPTH=5;
  var search_queue=new Array();
  // Bug#9100 Mike O
  search_queue[0]=get_top();
  var p_window=null;
    for (i = 0; i < MAX_DEPTH && p_window == null; i++) {
    current=search_queue.shift();
    if(current[target])
      p_window=current[target]
        else {
	    if(current.opener)
	    search_queue.push(current.opener);
	    if(current.parent)
	    search_queue.push(current.parent);
    }
  }
  return p_window;
}

// Bug #9100 Mike O - Use this instead of top, as the bottom-up approach will work in an iframe
function get_top() {
  var current_top = parent;
    
  // Bug #10441 Mike O - HACK: Just check the name for when we're in the Business Center, which doesn't use frames    
  //also resolves Bug 10346 NJ
  // Bug #14882 Mike O - Replaced name check with a simple parent check, as the window name is no longer set
  while(
  !(
    current_top.main ||
    current_top === current_top.parent
  ))
  current_top = current_top.parent;

  // Bug 23862 MJO - Make sure main is always set as a lot of code assumes it's there
  if (typeof current_top.main === "undefined")
    current_top.main = current_top;

  return current_top;
}

function html_encode(str) {
  var temp = document.createElement("div");
  temp.innerHTML=".";
  temp.childNodes[0].nodeValue=str;
  e_str=temp.innerHTML
  return e_str;
}
//Decode may not work with single quotes.
function html_decode(e_str) {
  var temp = document.createElement("div");
  temp.innerHTML=e_str;
  var str = temp.childNodes[0].nodeValue;
  temp.removeChild(temp.firstChild)
  return str;
}

// Bug #7510 Mike O
// Called by: Create_core_item_app.create_document_messages
function submit_manual_micr(args) {
  var bank_routing_nbr = '';
  var bank_account_nbr = '';
  var check_nbr = '';
    for (var i = 0; i < args.length; i++) {
    var arg=args[i];
        switch (arg.className) {
		  case 'text bank_routing_nbr': bank_routing_nbr=arg.value; break;
		  case 'text bank_account_nbr': bank_account_nbr=arg.value; break;
		  case 'text check_nbr': check_nbr=arg.value; break;
    }
  }
  get_pos().Device_Micr_receive_callback(bank_routing_nbr,bank_account_nbr,check_nbr);

  // HACK: Form field assignments don't work on the CASL side (handle_micr) for some reason, so do it here
  // Bug#9100 Mike O
  try// Bug 22729 DH
  {
    if (get_top().main.document.forms.form_inst["args.bank_routing_nbr"])
      get_top().main.document.forms.form_inst["args.bank_routing_nbr"].value = bank_routing_nbr;
    if (get_top().main.document.forms.form_inst["args.bank_account_nbr"])
      get_top().main.document.forms.form_inst["args.bank_account_nbr"].value = bank_account_nbr;
    if (get_top().main.document.forms.form_inst["args.check_nbr"])
      get_top().main.document.forms.form_inst["args.check_nbr"].value = check_nbr;
  }
  catch (e)
  {
    //...
  }
}

/*****
*
*   Base64.js
*
*   copyright 2003, Kevin Lindsey
*   licensing info available at: http://www.kevlindev.com/license.txt
*
*****/

/*****
*
*   encoding table
*
*****/
Base64.encoding = [
    "A", "B", "C", "D", "E", "F", "G", "H",
    "I", "J", "K", "L", "M", "N", "O", "P",
    "Q", "R", "S", "T", "U", "V", "W", "X",
    "Y", "Z", "a", "b", "c", "d", "e", "f",
    "g", "h", "i", "j", "k", "l", "m", "n",
    "o", "p", "q", "r", "s", "t", "u", "v",
    "w", "x", "y", "z", "0", "1", "2", "3",
    "4", "5", "6", "7", "8", "9", "+", "/"
];

/*****
*
*   constructor
*
*****/
function Base64() {}

/*****
*
*   encode
*
*****/
Base64.encode = function(data) {
    var result = [];
    var ip57   = Math.floor(data.length / 57);
    var fp57   = data.length % 57;
    var ip3    = Math.floor(fp57 / 3);
    var fp3    = fp57 % 3;
    var index  = 0;
    var num;
    
    for ( var i = 0; i < ip57; i++ ) {
        for ( j = 0; j < 19; j++, index += 3 ) {
            num = data.charCodeAt(index) << 16 | data.charCodeAt(index+1) << 8 | data.charCodeAt(index+2);
            result.push(Base64.encoding[ ( num & 0xFC0000 ) >> 18 ]);
            result.push(Base64.encoding[ ( num & 0x03F000 ) >> 12 ]);
            result.push(Base64.encoding[ ( num & 0x0FC0   ) >>  6 ]);
            result.push(Base64.encoding[ ( num & 0x3F     )       ]);
        }
        result.push("\n");
    }

    for ( i = 0; i < ip3; i++, index += 3 ) {
        num = data.charCodeAt(index) << 16 | data.charCodeAt(index+1) << 8 | data.charCodeAt(index+2);
        result.push(Base64.encoding[ ( num & 0xFC0000 ) >> 18 ]);
        result.push(Base64.encoding[ ( num & 0x03F000 ) >> 12 ]);
        result.push(Base64.encoding[ ( num & 0x0FC0   ) >>  6 ]);
        result.push(Base64.encoding[ ( num & 0x3F     )       ]);
    }

    if ( fp3 == 1 ) {
        num = data.charCodeAt(index) << 16;
        result.push(Base64.encoding[ ( num & 0xFC0000 ) >> 18 ]);
        result.push(Base64.encoding[ ( num & 0x03F000 ) >> 12 ]);
        result.push("==");
    } else if ( fp3 == 2 ) {
        num = data.charCodeAt(index) << 16 | data.charCodeAt(index+1) << 8;
        result.push(Base64.encoding[ ( num & 0xFC0000 ) >> 18 ]);
        result.push(Base64.encoding[ ( num & 0x03F000 ) >> 12 ]);
        result.push(Base64.encoding[ ( num & 0x0FC0   ) >>  6 ]);
        result.push("=");
    }

    return result.join("");
};
	
// Bug 19372 MJO - Moved InitializePeripheralsModal(Aux) to pos_interface.js

// Async XMLHTTPRequest handling
// March 1, 2010, Bug #8530, CLee
function createHandler(request, target_window, success, failure) {
	if(success == null) { success = emptyHandler; }
	if(failure == null) { failure = emptyHandler; }
    
	var handler = function() {
		if (request.readyState == 4) { // Complete
      if (request.status == 200) { // Only succeed on 200 (OK)
        // Bug 23970 MJO - Support no target
        if (target_window !== null) {
          // Bug #10565 Mike O - Set page_loaded back to false in case the response will be running scripts on load
          // Bug #8668 Mike O - Put the response text into the target_window (so async post_remote has a point)
          var sessValidFail = (request.responseText == "__session_validation_failure"); // Bug 16531 DJD Preh Keypad Not Working
          get_top().window.page_loaded = false;
          target_window.window.page_loaded = false;

          responseText = request.responseText;

          // Use results
          // Bug 18737 MJO - document.write into main makes IE11 implode when using HTTPS, so just reload main instead of writing HTML into it this way
          // Bug 24537 MJO - If in a child window, main may end up being named window_GEO...
          if (target_window.name == "main" || (target_window.name.indexOf("window_GEO") != -1 && responseText.length == 0)) {
            get_top().main.cover();
            // Bug 20733 MJO - Changed reload approach to get rid of white screen
            setTimeout(function () { target_window.location = target_window.location; }, 750);
          }
          else {
            target_window.document.write(responseText);
            target_window.document.close();
          }

          // Bug #11869 Mike O - If session validation failed, show the corresponding message
          if (sessValidFail) {  // Bug 16531 DJD Preh Keypad Not Working
            // Bug #13681 Mike O - Clear out any warnings before leaving the page
            window.onbeforeunload = null;
            // Bug #11880 Mike O - Handle the case where there's no frameset
            if (get_top().main === null || get_top().main === undefined) { // Business Center
              get_top().clearInterval(keep_alive_interval_id);
              get_top().enqueue_message('invalid_session');
            }
            else {
              get_top().main.clearInterval(keep_alive_interval_id);
              get_top().main.enqueue_message('invalid_session');
            }
            return;
          }
        }

				success(request);
			} else {	// Fail on all other terminal states
				failure(request);
			}
    }
  }
  return handler;
}

function emptyHandler(request) {
	return true;
}

  // Bug #11845 Mike O - Changed top to get_top() for iframe support
function displayResult(request) {
  // Bug# 12654 - JavaScript error on IE8 only. I just added try catch block to fix it. No problems in processing.
  try{
  get_top().main.document.write(request.responseText);
  get_top().main.document.close();
  }
    catch (e) {
    //  Bug# 12654 - This is only visible on IE8.
  }
}

// Bug #9345 Mike O - Code from MSDN
// Bug 15003 UMN optimize by saving in global since is called on every click!
ie_version = null;
function getInternetExplorerVersion()
// Returns the version of Internet Explorer or a -1
// (indicating the use of another browser).
{
  // Bug 15905 MJO - Also check for undefined
  if (ie_version !== null && typeof ie_version != 'undefined')
    return ie_version;
    
  var ie_version = -1; // Return value assumes failure.
    if (navigator.appName == 'Microsoft Internet Explorer') {
    var ua = navigator.userAgent;
    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
    if (re.exec(ua) != null)
      ie_version = parseFloat( RegExp.$1 );
  }
  return ie_version;
}

// Bug #12112 Mike O - Copy text from clipboard to a specified field
function copy_clipboard_contents(field_name) {
  var contents = window.clipboardData.getData('Text');
  
  // Bug #12768 Mike O - Update all input fields with a matching tagname
  $("input[name=args\\." + field_name + "]").val(contents);
}

//Bug 12441 NJ- submit the form on clicking anywhere in the form
// Bug 25504 MJO - Don't submit the page if the user dragged the cursor
var buttonSubmitPageX = 0;
var buttonSubmitPageY = 0;
function buttonSubmitRow(event, tag) { 
  var row = $(tag);

  buttonSubmitPageX = event.clientX;
  buttonSubmitPageY = event.clientY;

  row.on('mousemove', function handler(event) {
    buttonSubmitPageX = event.pageX;
    buttonSubmitPageY = event.pageY;

    $(this).off('mousemove', handler);
  });

  row.on('mouseup', function handler(event) {
    $(this).off('mouseup', handler);

    if (Math.abs(event.pageX - buttonSubmitPageX) > 5 || Math.abs(event.pageY - buttonSubmitPageY) > 5)
      return;

    event = window.event || event;
    var source = event.target || event.srcElement;
    if (source.tagName == "INPUT" || source.tagName == "SELECT") {
      return;
    }
    //Bug 15003 NJ- code simplified. Remove jquery event handler for form submit for performance and manually validate and submit.
    var form = row.closest("form");
    if (form.length == 0) {
      // Bug 26715 MJO - Look for nested forms too
      form = row.find('form');

      if (form.length == 0) {
        form = row.find("button:submit")[0].form; //For non-IE
      }

      $(form).off("submit");
      if (validate_and_cover(form))
        form.submit();
    }
    else {
      if (validate_and_cover(form[0]))
        form[0].submit();
    }
  });
}
//end bug 12441 NJ
//Bug 12843 validate ABA with ajax 
var mErrorABA = false; // false for initial value, true for ABA success, error string for ABA failure
function validateABA(url, args, elementName) 
{
    var elementName_ABA = elementName;
    var elementValue_ABA = $('INPUT.' + elementName_ABA).val() ;
    //BUG17415
    //var the_doublesubmit_arg = "__DOUBLESUBMIT__=" + html_encode(double_submit_value);
    // var the_url =  url.replace("__DOUBLESUBMIT__=",the_doublesubmit_arg)+elementValue_ABA;
    var the_url =  url.replace("__DOUBLESUBMIT__=&","")+elementValue_ABA;
   
   if (elementValue_ABA !="") {
        var request = $.ajax({
            async:false,
            beforeSend: function() { },// TODO show progress bar cover(10); },
            url: the_url,
            data: args,
            type: "POST",       
            dataType: "json",  
            contentType: "application/x-www-form-urlencoded",           
            success: function(oResult) { 
                    if (oResult["ResultType"] == "Success") {
                 $('INPUT.bank_name').val(oResult["Result"]["bank_name"]);
                 mErrorABA = true;
                 }
                    else if (oResult["ResultType"] == "Failure") {
                var ErrorSummary= oResult["ErrorSummary"];
                $('INPUT.bank_name').val("");
                  mErrorABA = " is not a valid routing number. " + ErrorSummary;
                }
                else
                  mErrorABA = " Error Occured upon ABA Validation ";
            }
        });
        request.done(function(jqXHR, textStatus) {
            //$('#wait_dialog').hide(); //document.getElementById("wait_dialog");
            //alert(dialog.length);
            //if (dialog) {                
            //    dialog.style.display = "none";
            //}
        });

    }
    if(mErrorABA!=false)
      return mErrorABA;
    return mErrorABA; 
  }  // End Bug 12843

  // Bug #9889 Mike O - Populate the OCR data field, and display the image
  function ShowEpsonOCRData(ocrStr, imageHTML) 
  {
     // Bug 21811 DH
    if(ocrStr == undefined || ocrStr == "")
      return;

    if(imageHTML == undefined || imageHTML == "")
      return;
     // end Bug 21811 DH

    OCR_DATA = get_top().main.document.forms['form_inst']['args.OCR_DATA'];
    OCR_DATA.value = ocrStr;

    if (imageHTML != null) {
      ocr_image = $(OCR_DATA).closest('tbody').find("#ocr_image");
      if (ocr_image.length == 0) {
        $(OCR_DATA).closest('tbody').append(imageHTML).contents();
      }
      else {
        ocr_image.html(imageHTML);
      }
    }
}

 //Bug 13934-Recalculate reversible tender or transactions totals.Called on blur of textbox 
    function RecalculateTotal(type, id) {
      //tax object
      // oTaxes store category tax type and its taxable transaction amount array e.g. {"A":{10},"B":{100.00,200.00}}
      var oTaxes = new Object();  
        
        if ($('#' + id + '_error').text() == '') {
      var total=0.0;
      var $inputs;
      var spanId='';
            switch (type) {
          case 'tran': $inputs = $('input[id^="args.tran"]');// document.getElementById('args.tran');
                      spanId = 'tran_total';
                      break;

         case 'tender': $inputs = $('input[id^="args.tender"]'); //document.getElementById('args.tender');
                      spanId = 'tender_total';
                     break;
      }
      $inputs.each(function() {
          var amt = $(this).val().replace('$', '').replace(',', '');
          var isNegative = (amt.indexOf('(') != -1 && amt.indexOf(')') != -1);
          if (isNegative) {
              amt = '-' + amt.replace('(', '').replace(')', '');
          }      
          var fAmt = parseFloat(amt);
          //find all taxable transaction,  put taxable transactions total into oTaxes by taxtype 
          var sTaxinfo= this.getAttribute("taxinfo");
          if (fAmt >= 0.001 && sTaxinfo != null && sTaxinfo.indexOf("taxable") != -1) {
            aTaxinfo =  sTaxinfo.split(" ");
            var sTaxtype = aTaxinfo[1];
            
            var sTaxAmount = aTaxinfo[2];
            fTaxAmt = parseFloat(sTaxAmount);
            if(oTaxes[sTaxtype]==undefined)
                oTaxes[sTaxtype] = new Array();
            if (fAmt <fTaxAmt) fTaxAmt = fAmt;
            oTaxes[sTaxtype].push(fTaxAmt);
         }
         if(type=='tender' || (sTaxinfo !=null && sTaxinfo.indexOf("taxtransaction")==-1))
           total = total + fAmt;
      });
      
      //set all tax type amount (fixed or percentage)  
      if(type=='tran' )    {
        var sTaxAmountSelector = "INPUT.taxtransaction"; 
        $(sTaxAmountSelector).each(function(iRowIndex) {
           var sTaxTranTaxinfo= this.getAttribute("taxinfo");
                    if (sTaxTranTaxinfo != null) {
             var asTaxInfo = sTaxTranTaxinfo.split(" ");
             var sTaxtype = asTaxInfo[1];
             var sTaxcategory = asTaxInfo[2];
             var sTaxrate = asTaxInfo[3];
             var fTaxAmount = 0.00;
                        if (oTaxes[sTaxtype] != undefined && sTaxcategory == "fixed") {
                fTaxAmount=parseFloat(sTaxrate)*oTaxes[sTaxtype].length;
             }
                        else if (oTaxes[sTaxtype] != undefined && sTaxcategory == "percentage") {
               var sumTaxableTrans = 0.00;
               for (i = 0; i < oTaxes[sTaxtype].length; i++) { 
                   sumTaxableTrans  = oTaxes[sTaxtype][i]+sumTaxableTrans;
               }
               fTaxAmount = sumTaxableTrans*parseFloat(sTaxrate);
             }
             total = total + fTaxAmount;
            this.value=formatCurrency(fTaxAmount.toFixed(2));
           }
        });
      }

            if (!isNaN(total)) {
        $('#' + spanId).html(formatCurrency(total));
      }      
       
    }
  }

  function ValidateReversibleAmount(id, reversibleAmount, maxAmount) {

      /*Called on blur of the textbox after validating for currency type.
      1. Validates the reversed amount should not be more than the reversible balance for a tender or transaction.
      2. Validates if the reversed amount is not more than the maximum reversible amount. This applies only to tenders. 
      */
      var reversedAmount = document.getElementById(id).value;
      var spanId = id + '_error';
      var element = (document.getElementById(id));
      //clear any errors
      display_error(spanId, "", "", element, true);   
      
      if (!isNaN(reversedAmount) && !isNaN(reversibleAmount) && (parseFloat(reversedAmount) > parseFloat(reversibleAmount))) {
          
          var msg = "Only " + formatCurrency(reversibleAmount) + " can be reversed.";
          
          display_error(spanId, msg, "", element, true);
          return false;
      }
      if (!isNaN(reversedAmount) && !isNaN(reversibleAmount) && (parseFloat(reversedAmount) > parseFloat(maxAmount))) {

          if (parseFloat(maxAmount) > 0) {              
              var msg = "Maximum reversible amount is " + formatCurrency(maxAmount) + ".";              
              display_error(spanId, msg, "", element, true);
              return false;
          }
      } 
      {
          
      }
      return true;
        
  }
    
  //end bug 13934  }
  
  // Bug #13857/14964 Mike O - Use to avoid accidental cross-site scripting
  function IsCrossDomain(frame) {
    try {
      var test = frame.document.location.href;
      return false;
    } catch (e) {
      return true;
    }
  }

// Bug 16648 MJO - Append the double-submit value to all forms and a elements
function AppendDoubleSubmitValue() {
  $("form").each(function (index) {
    if (!$(this).find("#__DOUBLESUBMIT__").val()) {
      // Bug 17321 MJO - Corrected method name
      $(this).append($("<input type='hidden' id='__DOUBLESUBMIT__' name='__DOUBLESUBMIT__' />").val(double_submit_value));
    }
  });

  $("a").attr('href', function (i, h) {
    // Bug 17513 MJO - Don't add __DOUBLESUBMIT__ to Javascript links
    if (h && h.indexOf("__DOUBLESUBMIT__") == -1 && h.toLowerCase().indexOf("javascript:") == -1 && h.length > 0) {
      // Bug 23568 MJO - Don't append to absolute links
      var pat = /^https?:\/\//i;
      if (!pat.test(h)) {
        if (h.indexOf('?') != -1)
          return h + "&__DOUBLESUBMIT__=" + html_encode(double_submit_value);
        else
          return h + "?__DOUBLESUBMIT__=" + html_encode(double_submit_value) + "&__RQM__=t";
      }
    }
    else
      return h;
  });
}

// Bug 17321 MJO - Override window.open so we can add in the __DOUBLESUBMIT__ argument as appropriate
// Code based on https://stackoverflow.com/questions/9172505/how-to-override-the-window-open-functionality
window.open = function (open) {
  return function (url, name, features) {
    name = name || "_blank";

    var site_split = get_current_site().split("/");
    var site_end = "/" + site_split[site_split.length - 1];

    // Bug 23862 MJO - Don't add it to blank.htm
    if (url.indexOf("__DOUBLESUBMIT__") == -1 && url.indexOf(site_end) != -1 && url.indexOf(".htm") != -1 && !url.endsWith("blank.htm")) {
      
      // Bug 17410 MJO - Moved this inside the conditional
      if ((double_submit_value || (get_top().main && get_top().main.double_submit_value)) == null) {
        setTimeout("window.open('" + url + "','" + name + "','" + features + "');", 500);
        return;
      }
      
      if (url.indexOf('?') != -1)
        url = url + "&__DOUBLESUBMIT__=" + html_encode(double_submit_value || get_top().main.double_submit_value);
      else
        url = url + "?__DOUBLESUBMIT__=" + html_encode(double_submit_value || get_top().main.double_submit_value) + "&__RQM__=t";
    }

    return open(url, name, features);
  };
}(window.open);

// Bug 19695 MByrd - (Admin Center)Print check image only, hide other elements on page
function printCheckImg() {
    var printContents = document.getElementById("checkImg").innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}

function hide_buttons() {
    var button1 = document.getElementById("closeButton");
    button1.style.display = "none";
    var button2 = document.getElementById("printButton");
    button2.style.display = "none";
}

function show_buttons() {
    var button1 = document.getElementById("closeButton");
    button1.style.display = "inline-block";
    var button2 = document.getElementById("printButton");
    button2.style.display = "inline-block";
}

// Bug 23345 MJO - Check whether the user has entered a duplicate field
// Bug 24268 NK - Prevent duplicate serial numbers when one has dashes and the other doesn't.
// existing serial number contains dashes, but not new one does't and vice versa//

var point_serial_nbrs = new Array();
var point_device_ips = new Array();
var point_mac_addresses = new Array();

function validate_point_duplicates() {
  isValid = true;
  for (var i = 0; i < point_serial_nbrs.length; i++) {
    var temp = point_serial_nbrs[i].replace("-", "");
    point_serial_nbrs[i] = temp;
  }

  $('input.serial_nbr').each(function (index, input) {
      if ($.inArray(this.value.trim().replace(/-/g, ""), point_serial_nbrs) !== -1) {
      isValid = false;
      display_error(input.value, "A device is already registered with this serial number.", '', input);
      return false;
    }
  });
  $('input.device_ip').each(function (index, input) {
    if ($.inArray(this.value.trim(), point_device_ips) !== -1) {
      isValid = false;
      display_error(input.value, "A device is already registered with this device IP address.", '', input);
      return false;
    }
  });
  $('input.mac_address').each(function (index, input) {
    if ($.inArray(input.value.trim().replace("-", "").toUpperCase(), point_mac_addresses) !== -1) {
      isValid = false;
      display_error(input.value, "A device is already registered with this MAC address.", '', input);
      return false;
    }
  });
          
  return isValid;
};

function decode_html_chars (str) {
  str=str.replace(/&quot;/g,'"');
  str=str.replace(/&amp;/g,'&');
  str=str.replace(/&lt;/g,'<');
  str=str.replace(/&gt;/g,'>');

  return str;
}
//Bug16884 SX
    function clearInputFile(clearbutton){
                var f = $('#upload-file');
        f.val(''); 
    }
  function verifyInputFile(inputfile){
  var uploadfilename = inputfile.value.match(/\.([^\.]+)$/);
 var ext =""
 if(!!uploadfilename && uploadfilename.length>1)
          ext = uploadfilename[1];
      ext = ext.toLowerCase(); // Bug 24808  - File Upload: files are rejected if extension is not lowercase
    switch(ext)
    {
        case 'txt':
        case 'rtf':
        case 'pdf':
        case 'doc':
        case 'docx':
        case 'xls':
        case 'xlsx':
        case 'ppt':
        case 'pptx':
        case 'zip':
        case 'gif':
        case 'jpeg':
        case 'jpg':
        case 'bmp':
        case 'png':
        case 'tif':
        case 'tiff':
            break;
        default:
            if(inputfile.value!='') 
                {
                alert('Invalid File Type to Upload.');
                 inputfile.value='';
                };
    }
}

// Bug 23022 MJO - Button-specific code to support target
// Bug 26202 MJO - Support for custom window.open args
function button_click(href, target, onclick, callCover, callDequeue, thisObj, event, windowOpenArgs) {
  
  // Bug 24537 UMN thisObj is not always passed!
  if (thisObj !== undefined)  {
    // Bug 24604 MJO - Removed jQuery in case it's not available
    if (thisObj["clicked"] == "clicked")
      return;
    thisObj["clicked"] = "clicked";
    setTimeout(function () { delete thisObj["clicked"]; }, 1000);
  }

  if (windowOpenArgs === undefined) { windowOpenArgs = "resizable=1,scrollbars=1"; }
  
  // Bug 24537 UMN event is not always passed!
  if (event !== undefined && !(!event.detail || event.detail == 1)) { event.stopPropagation(); return false; }

  if (callDequeue)
    setTimeout(function() { if (typeof resetTimeout !== 'undefined') resetTimeout(); dequeue_message(); }, 100);

  // Bug 24537 UMN onclick is not always passed!
  if (onclick !== undefined && onclick.length > 0) {
    // Bug 24628 MJO - Make sure there's a semicolon after the onclick
    var evalFunc = new Function("href", onclick + "; return href;");

    if (typeof href === 'string' || href instanceof String)
      href = evalFunc.call(thisObj, href);
    else
      evalFunc.call(thisObj, href);
  }

  if (href !== null) {
    if (target == null || target == "") {
      window.onbeforeunload = null;
      window.location.href = href;
    }
    else if (target == "_blank") {
      window.open(href, "_blank", windowOpenArgs);
    }
    else {
      if (typeof (target) == "string") {
        if (target == "pos_frame") { // Hack
          try {
            target_window = get_pos().GetWindow("pos_frame");
            // Bug#9100 Mike O
            if (target_window == null) target_window = get_top().pos.pos_frame;
          } catch (e) { } // Do nothing on failure
        } else if (target != "_self") {
          try {
            target_window = GetWindow(target);
            if (target_window == null) target_window = eval(target);
          } catch (e) { } // Do nothing on failure
        }
      }

      target_window.location.href = href;
    }
  }

  if (callCover)
    cover();
}

function button_keydown(e, btn)
{
  if ($(btn).prop("disableClick") === undefined) {
    $(btn).prop("disableClick", true);

    setTimeout(function () {
      $(btn).removeProp("disableClick");
    }, 250);

    return true;
  }
  else {
    e.stopPropagation();

    return false;
  }
}

// Bug 24518 MJO - End of event printing datatable initialization
function end_of_event_datatable(data, columns) {
  $('table#end_of_event').dataTable({
    searching: false, info: false,
    // Bug 26924 MJO - Enable pagination
    pageLength: 10,
    bLengthChange: false,
    data: data,
    columns: columns,
    createdRow: function (row, data, dataIndex) {
      $(row).find('td').each(function (index) {
        if ($(this).text().indexOf('/') != -1) {
          var split = $(this).text().split('/');

          if (split.length == 2 && split[0] == split[1])
            $(row).addClass("end_of_event_finished");
          else if (split.length == 2 && split[0] != "0")
            $(row).addClass("end_of_event_in_progress");
          else
            $(row).addClass("end_of_event_unfinished");

          return false;
        }
      });
    }
  });
}

// Bug 24518 MJO - Toggle details arrow
function end_of_event_toggle_arrow(element) {
  if (element.src.match("plus"))
    element.src = element.src.replace("plus", "minus");
  else
    element.src = element.src.replace("minus", "plus");
}
//Bug 24708 NAM: prevent navigating back with backspace key
function handle_backspace(e) {    
    e = e || event;
    var target = e.target || e.srcElement;    
        if ((e.keyCode == 8 || e.keyCode == 13) &&
            ((target.tagName == "TEXTAREA") || (target.tagName == "INPUT")))
            return false;
        return true;
}

// Bug 25590 MJO - Paypal support (Braintree API)
function braintree_init(client_token, amount, enable_paypal, enable_venmo, line_items) {
  amount = parseFloat(amount);

  if (amount <= 0.00) {
    alert("PayPal cannot be used to tender the remaining balance. Please select another tender.");
    return;
  }

  $button = $('button[name="_method_name.continue"], #form_class button:contains("OK"), button#tender_next');

  $button.prop('disabled', true);

  braintree.dropin.create({
    authorization: client_token,
    container: '#dropin-container',
    paypal: enable_paypal ? {
      flow: 'checkout',
      amount: amount,
      currency: 'USD',
      singleUse: true,
      lineItems: line_items
    } : null,
    card: false,
    venmo: enable_venmo ? { allowNewBrowserTab: false } : null,
    dataCollector: {
      paypal: true
    }
  }, function (createErr, instance) {
    if (createErr != null) {
      if (createErr.message.indexOf("No valid payment options available") > -1)
        alert("No valid payment options available. Please select a different tender.");
      else
      alert("Error creating PayPal widget: " + createErr.message + "\n" + "Please cancel and try again.");
    }
    else {
      $button.prop('disabled', false);

      // Bug 26030 MJO - Populate e-mail field if present
      instance.on('paymentMethodRequestable', function (event) {
        if (event.paymentMethodIsSelected) {
          instance.requestPaymentMethod(function (err, payload) {
            if (err) {
              alert("Error: " + err.message + "\n" + "Please try again.");
              return;
            }
            
            if (payload.details.email && payload.details.email !== null && payload.details.email.length > 0)
              $('.EMAIL_RECEIPT').attr('readonly', true).val(payload.details.email);
          });
        }
      });

      $button.click(function (e) {
        e.preventDefault();
        instance.requestPaymentMethod(function (err, payload) {
          // Submit payload.nonce to your server
          if (err === null) {
            $('input#paypal_payload').val(JSON.stringify(payload));

            if ($('button[name="_method_name.continue"]').length > 0)
              get_top().main.form_inst['_method_name.continue'].value = 'continue';

            $('form#form_class, form.form_inst').submit();
          }
          else {
            alert("Error: " + err.message + "\n" + "Please try again.");
            return;
          }
        });
      });
    }
  });
}

// Bug 26246 MJO - Moved to JS file and fixed it in general
function ready_suspend_datepicker(recall_days) {
  $("#suspend_filter_date_from, #suspend_filter_date_to").datepicker({
    dateFormat: 'mm/dd/yy',
    showOn: "button",
    buttonImage: site_static + "/baseline/business/images/calendar.gif",
    buttonImageOnly: true,
    buttonText: "Select date",
    changeMonth: true,
    changeYear: true,
    minDate: recall_days * -1
  }); 
}
// Bug 26094 NAM: date entered must be within 130-year date range. 
// Not more then 120 years ago and not more then 10 years in the future
function date_in_range(date){
  var date_check = new Date(date.toString());
  var date_today = new Date();
  date_today.setFullYear(date_today.getFullYear() + 10);
  if (new Date(date_check) > new Date(date_today))
      return false 
  var date_today = new Date();
  date_today.setFullYear(date_today.getFullYear() - 120);
  date_today.setHours(0);
  date_today.setMinutes(0);
  date_today.setSeconds(0);
  date_today.setMilliseconds(0);
  if (new Date(date_check) < new Date(date_today))
      return false
 
  return true;
}

function toggleTraditionalSignIn() {
  var toggleElements = document.getElementsByClassName("sso_sign_in_toggle_element");
  var i;
  for(i = 0; i < toggleElements.length; i++) {
    var itm = toggleElements[i];

    if(itm.style.display == 'none') {
      itm.style.display = '';
    } else {
      itm.style.display = 'none';
    }
  }
  var toggleLink = document.getElementById("toggle_sso_sign_in");
  if(toggleLink.innerHTML.toLowerCase().indexOf("traditional") != -1) {
    toggleLink.innerHTML = "Sign in via SSO";
  } else {
    toggleLink.innerHTML = "Traditional Sign In";
  }
  var focusElement = document.getElementById("username");
  if(focusElement != null) {
    focusElement.focus();
  }
}

//////// Bug 26537 UMN - Eigen MiraPay support
// Bug IPAY-1543 MJO - Switched to new encryption method
function mirasecure_init(get_token_url, message) {

  $('body').append(
    '<DIV style="display:none;">' +
    '<FORM ' +
    'id="mirasecure"' +
    ' target = "mirasecure_iframe"' +
    ' action = "' + get_token_url + '"' +
    ' method = "get"' +
    '>' +
    '<INPUT name="data" value="' + message + '"></INPUT>' +
    '</FORM>' +
    '</DIV>'
  );

  // If we have a mirasecure_iframe_validation input, just hide Next button initially.
  // When the user submits the mirasecure_iframe, the mirasecure_iframe_validation will
  // go to true and we will re-enable Next.
  if ($('#mirasecure_iframe_validation').length > 0)
    $('#tender_next').hide();

  setTimeout(function () {
    var form = $('form#mirasecure');
    form.find("input#__DOUBLESUBMIT__").remove();
    form.submit();
  }, 500);
}

function mirasecure_finish() {
  $("span#mirasecure_iframe_span").hide();
  $("span#mirasecure_finished").show();
  $('#tender_next').show();
}

// Bug IPAY-648 MJO - Update the fee and total amount as the user changes the total
function initialize_dynamic_fee() {
  var tbl = $("table[ui = 'Tender.htm_input_class']");
  var info = $("span#display_info");

  var fee_percentage = parseFloat(info.find("input#fee_percentage").attr("value"));
  var fee_fixed = parseFloat(info.find("input#fee_fixed").attr("value"));

  tbl.find("input.total").on("propertychange change click keyup input paste", function (evt) {
    var total = parseFloat($(this).val().replace("$", "").replace(",", ""));

    if (!isNaN(total)) {
      var fee_amount = parseFloat((total * (fee_percentage / 100.00)) + fee_fixed);

      if (fee_amount < 0.00)
        fee_amount = 0.00;

      info.find("td#original_amount").text(formatCurrency(total.toFixed(2)));
      info.find("td#fee_amount").text(formatCurrency(fee_amount.toFixed(2)));
      info.find("td#total_amount").text(formatCurrency((total + fee_amount).toFixed(2)));
    }
  });
}