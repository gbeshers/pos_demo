<do>

  <!-- This file: Document_f_epson.casl -->
  <!-- Bug #12931 Mike O - Subclass documents -->
  
Business.Document.<defclass _name='Epson'>

  <!-- BUG# 9721 - Epson OCR support DH -->
  <!--Pixels-->
  ocr_x=0=Type.decimal<!-- BUG# 8223 DH -->
  ocr_x_f_label="OCR x coordinate"

  ocr_y=0=Type.decimal
  ocr_y_f_label="OCR y coordinate"

  ocr_width=0=Type.decimal
  ocr_width_f_label="OCR width"

  ocr_height=0=Type.decimal
  ocr_height_f_label="OCR height"
  <!-- end BUG# 9721 - Epson OCR support DH -->
  scandata_usage=opt=<vector_of> Business.Scandata_usage </vector_of> <!--Bug# 19440 OCR Parsing NK-->

  prompt_txt=""=Type.typical_text
  icon_path=""=Type.path
  document_type=""=Type.description

  document_type_d=false=Boolean
  document_type_d_f_label="Slip Image"
  document_type_m=false=Boolean
  document_type_m_f_label="MICR"
  document_type_b=false=Boolean
  document_type_b_f_label="Print Data Back"
  document_type_f=false=Boolean
  document_type_f_f_label="Print Data Front"
  document_type_c=false=Boolean
  document_type_c_f_label="Photo ID"

  print_data_front =""=Type.typical_text
  print_data_front_f_doc = "Map to PRINT_DATA_FRONT"
  print_data_front_f_ui_config=TEXTAREA
  print_data_back=""=Type.typical_text
  print_data_back_f_doc = "Map to PRINT_DATA_BACK"
  print_data_back_f_ui_config=TEXTAREA

  <!-- TTS 20641 DH -->
  franking=false=Boolean
  franking_f_label="Franking"
  <!-- end TS 20641 DH -->

  <!--Bug 17825 DH-->
  endorsement_char_size=""=<one_of> "small" "large"</one_of>
  endorsement_char_size_f_label="Endorsement Char Size"
  <!--end Bug 17825 DH-->

  <!--Bug 21811 DH-->
  doc_side=""=<one_of>"Front" "Back" "Front and Back" </one_of>
  doc_side_f_label="Image Sides"
  <!--end Bug 21811 DH-->
  
  _new_class.<set>
    _db_field_order=
    <v>
      "_parent"
      "description"
      "document_id"
      "document_type"

      "document_type_d"
      "document_type_m"
      "document_type_b"
      "document_type_f"
      "document_type_c"

      "doc_side"<!--Bug 21811 DH-->

      "print_data_back"
      "print_data_front"     

      "prompt_txt"
      "icon_path"
      "tenders"
      "transactions"

      <!-- BUG# 9721 - Epson OCR support DH -->
      "ocr_x"
      "ocr_y"
      "ocr_width"
      "ocr_height"
      <!-- end BUG# 9721 - Epson OCR support DH -->
      "scandata_usage"  <!--Bug# 19440 OCR Parsing NK-->
      "franking"<!-- TTS 20641 DH -->
      "endorsement_char_size"<!--Bug 17825 DH-->
    </v>

    _config_field_order=
    <v>
      "transactions"
      "tenders"

      "document_id"
      "description"

      "document_type_d"
      "document_type_m"
      "document_type_b"
      "document_type_f"
      "document_type_c"

      "doc_side"<!--Bug 21811 DH-->

      "print_data_back"
      "print_data_front"

      "prompt_txt"
      "icon_path"

      <!-- BUG# 9721 - Epson OCR support DH -->
      "ocr_x"
      "ocr_y"
      "ocr_width"
      "ocr_height"
      <!-- end BUG# 9721 - Epson OCR support DH -->
      "scandata_usage" <!--Bug# 19440 OCR Parsing NK-->
      "franking"<!-- TTS 20641 DH -->
      "endorsement_char_size"<!--Bug 17825 DH-->
    </v>
  </set>
</defclass>

  Document.Epson.<defmethod _name='post_update'>
    <set>
      document_types=<v/>
    </set>
    
    .<for_each>
      include=<v> "document_type_d" "document_type_m" "document_type_b" "document_type_f" "document_type_c" </v>
      if_missing=false

      <if>
        value
        document_types.<insert>
          key.<subvector> start=key.<length/>.<minus> 1 </minus> </subvector>.<to_uppercase/>
        </insert>
      </if>
    </for_each>
    
    .<set>
      document_type=<if>
        document_types.<length/>.<is> 0 </is>
        ""
        else
        <join> _args=document_types </join>
      </if>
    </set>
  </defmethod>

  <!-- end SignaturePad BUG# 6368 Added by DH -->
  Document.Epson.<defmethod _name='ready_cardscanner'>
    message=opt
    app=opt
    
    <!-- Bug# 23002 DH -->
    <!-- Bug# 22486 DH -->
    <set>confirming_documents=false</set>
    <set>doc_count=0</set>
    <set>auto_continue=false</set>
    <if>app.<is_not>opt</is_not>
      <do>
        <set>confirming_documents = app.<get_current_focus/>.<get_status/>.<get>"_is_confirming_documents" if_missing=false</get></set>
        <set>doc_count=app.<get_current_focus/>.<get>'data_obj'</get>.<get>'documents'</get>.<length/></set>
        <set>auto_continue=app.<get_current_focus/>.<get_status/>.<get>"_is_auto_continue" if_missing=false</get></set> 
      </do>
    </if>
    <!-- end Bug# 22486 DH -->
    <!-- end Bug# 23002 DH -->
    
  <!-- Bug# 22486 DH -->
  <set>resolved_message=.<get>'prompt_txt' if_missing=""</get></set>
  <!-- end Bug# 22486 DH -->
    
    <!-- Bug #12077 - Reset the scanner before scanning the card -->
    <!-- Bug 13720 DH - iPayment Peripheral Overhaul -->

    <!--Bug 21811 DH-->
    <set>
      doc_side= .<get>'doc_side' if_missing="Front and Back"</get>.<to_uppercase/>
    </set>
    <client_call>
      <get_top/>.pos.<EpsonSetDocImagingSide>doc_side</EpsonSetDocImagingSide>
    </client_call>
    <!--end Bug 21811 DH-->

    <client_call>
      <get_top/>.pos.<EpsonScanCardSetMessage>message resolved_message app</EpsonScanCardSetMessage><!-- Bug# 22486 DH -->
    </client_call>

    <!-- Bug# 22486 DH -->
    <client_call>
      <get_top/>.pos.<EpsonSetCurrectDocumentState>confirming_documents doc_count auto_continue</EpsonSetCurrectDocumentState><!-- Bug# 22486 DH -->
    </client_call>
    <!-- end Bug# 22486 DH -->
    
    <client_call>
      <get_top/>.pos.<EpsonScanCard/>
    </client_call>
  </defmethod>

  Document.Epson.<defmethod _name='ready_checkscanner'>
    message=opt
    app=opt

    <!-- Bug# 23002 DH -->
    <!-- Bug# 22486 DH -->
    <set>confirming_documents=false</set>
    <set>doc_count=0</set>
    <set>auto_continue=false</set>
    <if>app.<is_not>opt</is_not>
      <do>
        <set> confirming_documents = app.<get_current_focus/>.<get_status/>.<get>"_is_confirming_documents" if_missing=false</get> </set>
        <set> doc_count=app.<get_current_focus/>.<get>'data_obj'</get>.<get>'documents'</get>.<length/> </set>
        <set> auto_continue=app.<get_current_focus/>.<get_status/>.<get>"_is_auto_continue" if_missing=false</get> </set>
      </do>
    </if>
    <!-- end Bug# 22486 DH -->
    <!-- end Bug# 23002 DH -->
    
    <!-- BUG# 9721 - Epson OCR support DH -->
    <if>
      .document_type.<is>"D"</is>
      <do>
        <!--Measurements are in pixels-->
        <set>
          iOCR_x = .<get>"ocr_x"</get>
        </set>
        <set>
          iOCR_y = .<get>"ocr_y"</get>
        </set>
        <set>
          iOCR_width = .<get>"ocr_width"</get>
        </set>
        <set>
          iOCR_height = .<get>"ocr_height"</get>
        </set>

        <if>
          iOCR_x.<is>false</is><set>iOCR_x=0</set>
        </if>
        <if>
          iOCR_y.<is>false</is><set>iOCR_y=0</set>
        </if>
        <if>
          iOCR_width.<is>false</is><set>iOCR_width=0</set>
        </if>
        <if>
          iOCR_height.<is>false</is><set>iOCR_height=0</set>
        </if>
        <client_call>
          <get_pos/>.<DefineOCRCoordinates>iOCR_x iOCR_y iOCR_width iOCR_height</DefineOCRCoordinates>
        </client_call>
      </do>
    </if>

    <!--Bug 21811 DH-->
    <set>
      doc_side= .<get>'doc_side' if_missing="Front and Back"</get>.<to_uppercase/>
    </set>
    <client_call>
      <get_top/>.pos.<EpsonSetDocImagingSide>doc_side</EpsonSetDocImagingSide>
    </client_call>
    <!--end Bug 21811 DH-->

    <client_call>
      <get_pos/>.<ScannerChangeMode>
        <SCRIPT> "get_pos().CHK_DI_MODE_CHECKSCANNER" </SCRIPT>
      </ScannerChangeMode>
    </client_call>

    <!-- Bug# 22486 DH -->
    <client_call>
      <get_top/>.pos.<EpsonSetCurrectDocumentState>confirming_documents doc_count auto_continue</EpsonSetCurrectDocumentState><!-- Bug 22715 DH -->
    </client_call>
    <!-- end Bug# 22486 DH -->
    
    <client_call>
      <get_pos/>.<ScannerReady>
        .document_type message<!-- Bug 12797 DH - OCR image disappears after it is displayed for a second. -->
      </ScannerReady>
      
    </client_call>

  </defmethod>


  Document.Epson.<defmethod _name='ready_printer'>
    message=opt
    h2000=opt<!--IPAY-198 DH-->

    <!--Bug 21811 DH-->
    <set>
      doc_side= .<get>'doc_side' if_missing="Front and Back"</get>.<to_uppercase/>
    </set>
    <client_call>
      <get_top/>.pos.<EpsonSetDocImagingSide>doc_side</EpsonSetDocImagingSide>
    </client_call>
    <!--end Bug 21811 DH-->

    <client_call>
      <get_pos/>.<ScannerChangeMode>
        <SCRIPT> "get_pos().CHK_DI_MODE_CHECKSCANNER" </SCRIPT>
      </ScannerChangeMode>
    </client_call>    
    
    <!--IPAY-198 DH - The scanner may not be available and this could be a validation stamp type insert.-->
    <if>
      <and>
        h2000
      <or>
        .document_type.<key_of> "F" </key_of>
        .document_type.<key_of> "B" </key_of>
      </or>
      </and>
       <client_call><get_pos/>.<PrinterInsertH2000> .document_type </PrinterInsertH2000></client_call>
      else
       <client_call><get_pos/>.<PrinterReady/> </client_call>
    </if>
    <!--End IPAY-198 - DH The scanner may not be available and this could be a validation stamp type insert.-->
        
    <!-- Bug#9100 Mike O -->
    <if>
      message.<is> opt </is>.<not/> <client_call>
        <get_top/>.main.<enqueue_message> message </enqueue_message>
      </client_call>
    </if>
    
  </defmethod>

  Document.Epson.<defmethod _name='ready_micr'>
    message=opt
    document_type=opt<!-- Bug# 18706 DH -->
    no_micr_selected_in_config=opt <!--Bug 21375 DH-->
    app=opt <!-- Bug 22699 NK-->
    aba_validation=false <!--Bug 22729 DH-->
    
    <!--Bug# 24361 DH - Validate endorsement formatting ahead of check processing to prevent incomplete check scanning.
    Prevent scanning partial check data if the endorsement formatting fails. This way user will not get any data on the screen and will need to correct the message formatting in the Confi for this document.
    It will also prevent users from processing checks without the endorsement message. -->
    <set>data_back=""</set>
    <if>
      app.<is_not>opt</is_not>
      <if>
        <and>
          document_type.<is_not>opt</is_not>
          document_type.<key_of> "B" </key_of>
          document_type.<key_of> "F" </key_of>.<not/>
        </and>
        <do>
          <set>
            <!-- Bug 25134 MJO - Call directly on data_obj -->
            data_back=app.data_obj.<format_validation> .print_data_back </format_validation>
          </set>
          <if>
            data_back.<contains>'Invalid format'</contains>
            <do>
              <Message>
                id="invalid_validation_format"
                content=<message_warning_box>
                  message="Unable to process endorsement"<!--Bug 25075 - Epson check document: Unable to tender before a transaction or tender a negative balance; validation error results-->
                  submessage="The endorsement message configured is in an incorrect format. Please correct the message text and try again. See log for details."
                </message_warning_box>
                actions=Message.Alert.actions
                condition=true
              </Message>.<insert_message>
                app.<get_app/>
              </insert_message>
              <return/>
            </do>
          </if>
        </do>
      </if>
    </if>
    <!--End Bug# 24361 DH-->
    
    <!-- Bug 22729 DH -->
    <set>auto_continue=false</set>
    <if>app.<is_not>opt</is_not>
      <set>auto_continue=app.<get_current_focus/>.<get_status/>.<get>"_is_auto_continue" if_missing=false</get> </set><!-- Bug 22715 DH -->
    </if>
    <!-- end Bug 22729 DH -->

    <!--Bug# 23520 DH --> <!-- Bug# 23002 DH - app is passed into this function, but it's not set to the correct object when deposits are created -->
    <set>confirming_documents=false</set>
    <set>doc_count=0</set>
    <if>app.<is_not>opt</is_not>
      <do>
        <set>confirming_documents = app.<get_current_focus/>.<get_status/>.<get>"_is_confirming_documents" if_missing=false</get></set>
        <set>doc_count=app.<get_current_focus/>.<get>'data_obj'</get>.<get>'documents'</get>.<length/></set>
      </do>
    </if>
    <!--Bug# 23520 DH --> <!-- Bug# 23002 DH -->

    <!-- Bug# 10113 - Only show peripherals failed to connect messages when "Reset Peripherals" button is clicked. -->

    <set>franking=.<get>'franking' if_missing=false</get></set><!-- TTS 20641 DH --> 
    
    <if>
      my.<get>'has_printer' if_missing=false</get>.<is>true</is>
      <do>
        
        <!--Bug# 23520 DH-->
        <client_call>
          <get_top/>.pos.<EpsonSetCurrectDocumentState>confirming_documents doc_count auto_continue</EpsonSetCurrectDocumentState><!-- Bug# 22486 DH -->
        </client_call>
        <set>doc_side= .<get>'doc_side' if_missing="Front and Back"</get>.<to_uppercase/></set>
        <client_call>
          <get_top/>.pos.<EpsonSetDocImagingSide>doc_side</EpsonSetDocImagingSide>
        </client_call>
        <!--end Bug# 23520 DH-->
        
        <!-- Bug 21811 & 22496 DH -->
        <if>
          my.<get>'epson_s9000_connected' if_missing=false</get>.<is>true</is>
          <do>
            <!-- Bug 22729 DH -->
            <set>data_back=""</set>
            <if>app.<is_not>opt</is_not>
              <!-- Bug 25134 MJO - Call directly on data_obj -->
              <set> data_back=app.data_obj.<format_validation> .print_data_back </format_validation>  <!-- Bug 22699 NK--></set>
            </if>
            <!-- end Bug 22729 DH -->

            <client_call>
              <get_pos/>.<Epson_PrintEndorsementOnS9000>
                data_back .endorsement_char_size .document_type_b <!--Bug# 23916 DH--></Epson_PrintEndorsementOnS9000>
            </client_call>
          </do>
        </if>
        <!-- end Bug# 21811 & 22496 DH -->

        <client_call>
          <get_pos/>.<ScannerChangeMode>
            <SCRIPT> "get_pos().CHK_DI_MODE_CHECKSCANNER" </SCRIPT>
          </ScannerChangeMode>
        </client_call>

        <!-- Bug# 19053 DH --> <!-- Bug# 19053 DH --> <!--Bug# 25080 DH-->
        <set>doctype=.document_type</set>
        <if>document_type.<is>"deposit"</is>
          <set>doctype=document_type</set>
        </if>
        
        <client_call>
          <get_pos/>.<MicrReady>doctype message franking no_micr_selected_in_config auto_continue aba_validation .document_type_b</MicrReady><!-- Bug 20641 DH --><!--Bug 21375 DH--><!-- Bug 22715 DH --><!--Bug 22729 DH--> <!--Bug# 23916 DH--><!--Bug# 24736 DH-->
        </client_call>
        <!-- Bug# 19053 DH --> <!--Bug# 25080 DH-->

      </do>
    </if>
  </defmethod>

  Document.Epson.<defmethod _name='remove_document'>
    message=opt

    <!--Bug 21811 DH-->
    <set>
      doc_side= .<get>'doc_side' if_missing="Front and Back"</get>.<to_uppercase/>
    </set>
    <client_call>
      <get_top/>.pos.<EpsonSetDocImagingSide>doc_side</EpsonSetDocImagingSide>
    </client_call>
    <!--end Bug 21811 DH-->

    <client_call>
      <get_pos/>.<ScannerRemove/>
    </client_call>
    <!-- Bug#9100 Mike O -->
    <if>
      message.<is> opt </is>.<not/> <client_call>
        <get_top/>.main.<enqueue_message> message </enqueue_message>
      </client_call>
    </if>
  </defmethod>

  Document.Epson.<defmethod _name='print_scan_remove'>
    message=opt
    app=opt

    <!--Bug 21811 DH-->
    <set>
      doc_side= .<get>'doc_side' if_missing="Front and Back"</get>.<to_uppercase/>
    </set>
    <client_call>
      <get_top/>.pos.<EpsonSetDocImagingSide>doc_side</EpsonSetDocImagingSide>
    </client_call>
    <!--end Bug 21811 DH-->

    <!-- Bug 25134 MJO - Call directly on data_obj -->
    <set>
      data_back=app.data_obj.<format_validation> .print_data_back </format_validation>
      data_front=app.data_obj.<format_validation> .print_data_front </format_validation>
    </set>

    <!-- Bug 20145 MJO - Support printing on both sides -->
    <if>
      <!--Bug 13720 DH - iPayment Peripheral Overhaul-->
      <and>
      .document_type.<key_of> "B" </key_of>
        .document_type.<key_of> "F" </key_of>.<not/>
      </and>
      <client_call>
        <get_pos/>.<Epson_PrintEndorsementOrValidation>data_back "PTR_DI_SLIP_ENDORSEMENT" .document_type message false .endorsement_char_size</Epson_PrintEndorsementOrValidation><!--Bug 17825 DH-->
      </client_call>
    </if>

    <if>
      <!--Bug 13720 DH - iPayment Peripheral Overhaul-->
      .document_type.<key_of> "F" </key_of>
      <if>
        .document_type.<key_of> "B" </key_of>
        <do>
      <client_call>
            <get_pos/>.<Epson_PrintEndorsementOrValidationBothSides>data_back "PTR_DI_SLIP_ENDORSEMENT" .document_type message false .endorsement_char_size</Epson_PrintEndorsementOrValidationBothSides><!--Bug 17825 DH-->
          </client_call>
          <client_call>
            <get_pos/>.<Epson_PrintEndorsementOrValidation>data_front "PTR_DI_SLIP_VALIDATION" .document_type message true .endorsement_char_size</Epson_PrintEndorsementOrValidation><!--Bug 17825 DH-->
          </client_call>
        </do>
        else
        <client_call>
        <get_pos/>.<Epson_PrintEndorsementOrValidation>data_front "PTR_DI_SLIP_VALIDATION" .document_type message false .endorsement_char_size</Epson_PrintEndorsementOrValidation><!--Bug 17825 DH-->
      </client_call>
    </if>
    </if>
    <!-- End Bug 20145 MJO -->

    true
  </defmethod>



</do>