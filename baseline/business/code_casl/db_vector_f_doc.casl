<do>

<!--
Any GEO/cs.CASL_engine.GenericObject can have paging feature.
   paging: automatically show vector values in seperate pages 
   with "prev" and "next" links. 
   fields: _ui_first, _ui_last, _ui_size
   methods: ui_next, ui_prev
   
use db_vector without database interaction
  <GEO> "a" "b" "c" _ui_size=5 </GEO>.<htm_inst_paging/>
  
TODO: write test cases about GEO paging. 
-->
db_vector.<doc> 
<![CDATA[
db_vector is the combination of retrieving database records and 
show data in pages. It provides the functionality to intelliently 
retrieve data as needed, or cache some or all data in advance to show it 
later. 
   - calling query() create a build_in _reader which retrieve the data
     as needed. 
   - query() parameters provide optional buffer feature 
      see example below for details. 
      when doing query with buffer means that db_vector
      show the current view of data while a database reader 
      stay open at background to continue retrieving database records. 
      see more information in cs.CASL_engine.db_vector_reader class
   - provide htm_inst automatically show data with paging. 
   
 the overall picture of db_vector
     there are three layers of abstractions 
     db_vector, cs.CASL_engine.db_vector_reader, database
     database reader 
     db_vector initiate db_vector_reader and copy data from db_vector_reader 
     as request. 
     
 query method params: 
        "db_connection"     (required - connection string or connection object for OdbcConnection,
                             if passed db_connection, db_connection is not closed after query)
       "sql_statement"      (required - const SQL statement) 
       "class"              (optional or may be null - CASL) - 
	                               type or parent of individual retrieved record.
       "more_data_size"     (optional or 0(infinite/get all data) or 1 to 2,147,483,647 
	                                - nbr of rows to retrieve at a time.
	                                Subsequent calls to CASL_insert_data() will retrieve next group when needed.
	                                Worker thread will continue to run in the background until 
	                                group is retrieved. This has nothing to do with display 
                                on screen. Only buffering of data to be ready for display.)
       "more_data_timeout"  (optional) - in seconds? - 1 .1 .01 .001 are also possible.
                               db_vector_reader.retrieve(params) will wait until group of records set by
                                "NextGroupCount" is retrieved or group_timeout expires.
                                Although db_request.make_db_request(params) returns, worker thread will 
                                still attempt to finish retrieving the last group.)
       "is_convert_to_jpg"  (optional) true/false All images must be converted to JPG format before they can be displayed on web pages.

  
samples to use db vector 
1. call query to database and without buffer feature (db_requrest.get_all)
<db_vector/>.
  <query>
     db_connection="DSN=cbc;UID=;PWD=;"
     sql_statement="select * from TG_TRAN_DATA"
     class=TranSuite_DB.Data.TG_TRAN_DATA
     more_data_size=0
  </query>
  
2. call query to database and with buffer feature (db_requrest.get_buffer)
<db_vector/>.
   <query>
      db_connection="DSN=cbc;UID=;PWD=;"
      sql_statement="select * from TG_TRAN_DATA"
      class=TranSuite_DB.Data.TG_TRAN_DATA
      more_data_size=5 
      more_data_timeout=30
   </query>
   
3. extra: call query on db_vector class, query returns db_vector instance
db_vector.
   <query>
      db_connection="DSN=cbc;UID=;PWD=;"
      sql_statement="select * from TG_TRAN_DATA"
      class=TranSuite_DB.Data.TG_TRAN_DATA
      more_data_size=5 
      more_data_timeout=30
   </query>
   
TODO: 
  write feature test cases for diferent scenarioes. 
  like calling insert_data when different state of db_vector_reader
   (state includes "working", "stopped", "paused", "done")
  ]]>
   
  </doc> 

db_vector.sql_verify.<doc>
  "Check for potentially harmful characters in the sql string."
  "Currently, [;, --, \, $, '''] are excluded characters."
</doc>

</do>