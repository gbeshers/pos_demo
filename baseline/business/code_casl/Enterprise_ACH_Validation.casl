<!-- Bug MSD-181 DH Enterprise level ACH validation -->
<do>
  Business.<defclass _name='Enterprise_ACH_Validation'>
    ach_validation = "Disabled" = <one_of>"Enabled" "Disabled"</one_of>
    ach_validation_f_config_prompt = "Enable/Disable ACH validations. Workgroup level can override these settings here."
    ach_merchant_tracking = "" = Type.typical_text
    ach_merchant_tracking_f_config_prompt = "Merchant ID GUID used for tracking ACH Verification Service calls for reporting"    
    ach_merchant_guid = "" = String
    ach_merchant_guid_f_ui_config = Type.guid_generator
    ach_base_uri = "" = String
    ach_base_uri_f_config_prompt = "Full URI/URL including protocol (http), domain name, port."

    <!--Disabled for now until we have more ACH verification third-party services added-->
    <!--ach_tiers_uri = "" = String
    ach_tiers_uri_f_config_prompt = "Endpoint address from which to receive the available tiers."-->

    ach_verify_rtn_uri = "" = String
    ach_verify_rtn_uri_f_config_prompt = "Enter the API endpoint name."
    ach_available_tiers = "Basic" = <one_of>"Basic" "Premium" "Stub"</one_of>
    ach_available_tiers_f_config_prompt = "Please select the ACH verification tier."
    ach_override_failure = "No" = <one_of>"Yes" "No"</one_of>
    ach_override_failure_f_config_prompt = "Allow the bank account details to accepted even if account verification fails."
    ach_max_tries = 0 = Integer
    ach_max_tries_f_config_prompt = "Allow reentering of the bank account details up to this amount of time. May be different for guest users and authenticated users."
    ach_all_times = "No" = <one_of>"Yes" "No"</one_of>
    ach_all_times_f_config_prompt =  "At all times, if not using the failure override, subsequent entries should be different from previous entries made."
    ach_decline_level = "High Risk" = <one_of>"Limited Risk" "Low Risk" "Medium Risk" "High Risk"</one_of>
    ach_decline_level_f_config_prompt = "Decline transaction when the risk is at a specific level."
    ach_warn_level = "Medium Risk" = <one_of>"Limited Risk" "Low Risk" "Medium Risk" "High Risk" "No Warning"</one_of> <!--MSD-190 DH-->
    ach_warn_level_f_config_prompt = "Display user warning when the risk is at a specific level."
    ach_basic_custom_adverse_msg = "" = String
    ach_basic_custom_adverse_msg_f_config_prompt = "Basic service tier message that can be appended to the FCRA (Fair Credit Reporting Act) required adverse action notice."
    ach_premium_custom_adverse_msg = "" = String
    ach_premium_custom_adverse_msg_f_config_prompt = "Premium service tier message that can be appended to the FCRA (Fair Credit Reporting Act) required adverse action notice."
    ach_force_lookup = "No" = <one_of>"Yes" "No"</one_of>
    ach_force_lookup_f_config_prompt = "Force a third-party bank account lookup instead of using the cached data."
        
    _new_class.<set>
      _label="Enterprise Level ACH Validation Settings"
      _db_field_order=<v>
      "ach_validation"
      "ach_base_uri"
      <!--"ach_tiers_uri"-->
      "ach_available_tiers"
      "ach_decline_level"
      "ach_warn_level"
      "ach_verify_rtn_uri"
      "ach_override_failure"
      "ach_max_tries"
      "ach_all_times"
      "ach_force_lookup"
      "ach_basic_custom_adverse_msg"
      "ach_premium_custom_adverse_msg"
      "ach_merchant_tracking"
      </v>
      
      _row_field_order=<v/>
      _config_field_order=<v>
      "ach_validation"
      "ach_base_uri"
      <!--"ach_tiers_uri"-->
      "ach_available_tiers"
      "ach_decline_level"
      "ach_warn_level"
      "ach_verify_rtn_uri"
      "ach_override_failure"
      "ach_max_tries"
      "ach_all_times"
      "ach_force_lookup"
      "ach_basic_custom_adverse_msg"
      "ach_premium_custom_adverse_msg"
      "ach_merchant_tracking"
      "ach_merchant_guid"
      </v>
      _is_copyable=false
    </set>
  </defclass>

  Business.Enterprise_ACH_Validation.<defmethod _name="htm_label">
    ._label
  </defmethod>

  Business.Enterprise_ACH_Validation.<set_part>
    data=<Business.Enterprise_ACH_Validation/>
  </set_part>

</do>