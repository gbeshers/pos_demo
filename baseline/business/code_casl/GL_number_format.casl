<!-- This file: GL_number_format.casl -->
<!-- Virtually everything in this file was modified or written for Bug #6013 - Mike O -->

<do>

Business.<defclass _name='Gl_number_segment'> 
  name=req=Type.id <!-- BUG 6044 -->
  description=opt=Type.path <!-- BUG 6044 BLL: needs to be path because it needs / -->
  chars=Type.typical_text=<one_of>
    Type.numeric
    Type.alpha
    Type.alphanumeric
    Type.alphanumeric_and_space
    Type.typical_text
  </one_of>
    chars_f_label="Characters Allowed"
  min_size=0=Type.integer <!-- BUG 6079/6044 -->  <!-- type needs to be Integer and  not numeric because numeric is a string -->
    min_size_f_label="Minimum length"
    min_size_f_req_config=true <!-- BUG 6044 -->
  max_size=10=Type.integer <!-- BUG 6079/6044 -->  <!-- type needs to be Integer and  not numeric because numeric is a string -->
    max_size_f_label="Maxiumum length"
    max_size_f_req_config=true <!-- BUG 6044 -->
  read_only=false=Boolean
  required=false=Boolean
  <!--ui_editable=true=Boolean-->
  <!--pattern=opt -->
  
  _new_class.<set>
    _db_field_order=<v> "_parent" "name" "description" "chars" "min_size" "max_size" "read_only" "required" </v>
    _create_field_order=<v> "name" "description" "chars" "min_size" "max_size" "read_only" "required" </v>
    _config_field_order=<v> "name" "description" "chars" "min_size" "max_size" "read_only" "required" </v>
    _row_field_order=<v> "name" "description" "chars" "min_size" "max_size" "read_only" "required" </v>
  </set>
</defclass>
Gl_number_segment.<defmethod _name='ui_config'>  <!-- copied from Type.text -->
  key_path=req
  a_key=req
  a_value=req
  a_type=opt
  obj=opt
  owner=opt
  <if> a_value.<is_a> Type.text </is_a>
          <return> GEO.<ui_config> _args=<get_args/> </ui_config> </return>
  </if>  
  <set> name=<join> key_path a_key separator="." </join> </set>
  <set> result=<INPUT type="text" class="Gl_number_segment-ui_config"> name=name value=a_value </INPUT> </set>
  <if> a_value.<is> false </is> result.<set> value="" </set> </if>
  <if> a_type.<has> "max_size" lookup=true </has>
        result.<set> maxlength=a_type.max_size
                     style=<STYLE> font-weight="normal" width=<join> Integer.<from> a_type.max_size </from>.<times> 0.75 </times> "em" </join> </STYLE>
        </set>
  </if>
  result
</defmethod>

Business.<defclass _name='Gl_number_format'> <!--id=req=String--> 
  id=req=Type.id <!-- BUG 6044 -->
    id_f_read_only_config=true
  name=opt=Type.description <!-- BUG 6044 -->
  segments=opt=<vector_of> Business.Gl_number_segment </vector_of>  <!-- MP: should be vector as part.  _other_unkeyed_f_ui_config=GEO.ui_config-->
  override_character=""=Type.<text> max_size=1 </text> <!-- Bug 19059 MJO - GL number override functionality -->
  <!-- _label="GL number format" -->
  _new_class.<set>
    _primary_key="id"
    _db_field_order=<v> "_parent" "id" "name" "segments"
      "override_character" <!-- Bug 19059 MJO -->
    </v>
    _config_field_order=<v> "id" "name" "segments"
      "override_character" <!-- Bug 19059 MJO -->
    </v>
    _create_field_order=<v> "id" "name" </v>
    _row_field_order=<v> "id" "name" </v>
    ui=vector.ui <!-- HACK: Needs better UI -->
    <!--_create_field_order=<v> "id" "_other_unkeyed" </v>-->
  </set>
   
  _new_class.<set_part> of=<GEO/> </set_part>
</defclass>

<!-- Bug#5144 PCI ANU Type Gl_number_format doesn't require javascript validation. -->
Gl_number_format.<defmethod _name='is_type_for'>
  a_geo=req
  <and>
    primitive.<is_type_for> a_geo </is_type_for>.<not/>
    a_geo.<length/>.<is> .segments.<length/> </is>
    <and>
      _args=.segments.<for_each>
        returns="all"
        <set> g_value=a_geo.<get> key </get> </set>
        <or> <!-- the segment, value, is valid if: -->
          <and> <!-- it's blank and not required -->
            g_value.<is> "" </is>
            value.required.<not/>
          </and>
          <and> <!-- or if the characters and size constraints match -->
            value.chars.<is_type_for> g_value </is_type_for>
            value.min_size.<more> g_value.<length/> </more>.<not/>
            value.max_size.<less> g_value.<length/> </less>.<not/>
          </and>
        </or>
      </for_each>
    </and>
  </and>
</defmethod>

Gl_number_format.<defmethod _name='init'>
 .<cache_instance_2/>
 .<set_part> segments=.<get> "segments" if_missing=<v/> </get> </set_part> <!-- should be part of copy_down_data -->
 .segments.<for_each>
   value.<make_part> in=.segments name=key </make_part>
 </for_each>
 _subject
</defmethod>

Gl_number_format.<defmethod _name='from'>
  _other_keyed=opt
  _other_unkeyed=opt
  <get_args/>.0
</defmethod>

Gl_number_format.<defmethod _name='create_default_primary'>  <!-- sets Gl_number_format.of.primary -->
  <Gl_number_format> id="primary" name="" 
    segments=<v> 
      <Gl_number_segment>
        name=""
        description="Please edit.  This is a placeholder for the first GL segment."
        chars=Type.alphanumeric
        max_size=10
        min_size=0
      </Gl_number_segment>
    </v>
  </Gl_number_format> 
</defmethod>

<!-- Called by: Create_core_item_app.htm_inst -->
Gl_number_format.<defmethod _name='add_allocation_config_js'>  <!-- see also: htm_add_allocation -->
 <join> "["
   .segments.<for_each> returns="all"
   <!-- Bug #6013 Mike O - Added min length to arguments -->
    <join> "{len:" value.max_size ", minlen:" value.min_size ", type:'" value.chars._name "',name:'" value.name "' }" </join> <!-- TTS20855 SX  Add allocation labels to "added" allocation  -->
   </for_each>.<join> separator="," </join>
   "]" 
 </join>
</defmethod>
<!--
<test> name="Gl_number_format.add_allocation_config_js"
<Gl_number_format>
  segments=<v> 
   <Gl_number_segment> max_size=4 </Gl_number_segment> 
   <Gl_number_segment> max_size=6 </Gl_number_segment> 
   <Gl_number_segment> max_size=1 </Gl_number_segment> 
   <Gl_number_segment> max_size=8 </Gl_number_segment> 
  </v> 
</Gl_number_format>.<add_allocation_config_js/>
"[{len:4},{len:6},{len:1},{len:8}]"
</test>
-->

Gl_number_format.<defmethod _name='ui_config'>
  key_path=req
  a_key=req
  a_value=req
  a_type=req
  obj=req
  owner=req
  
  <!-- By changing to one_of this interaction was fixed -->
  <!--<if> <or> a_value.<is> opt </is>
            a_value.<is> req </is> </or>
        
  </if>-->
  <set> result_v=<v/> </set>
  a_type.segments.<for_each>
    <set> ui=a_value.<get_ui> key=key type=value ui_type="config" </get_ui> </set>
    <set> args=<GEO>
      key_path=key_path.<join> "." a_key </join>
      a_key=key
      a_value = 
        <if> 
          <or> 
            a_value.<is> opt </is>
            a_value.<is> req </is> 
          </or>
            a_value
          else 
            <!-- Bug 17128 UMN fix if_missing=opt bad code smell -->
            <if> a_value.<has> key </has> a_value.<get> key </get> else opt </if>
        </if>
      a_type=value
      obj=a_value
      owner=owner
    </GEO> </set>
    result_v.<insert>
      <SPAN> class="gl_segment" value.<get> "name" if_missing="" </get> </SPAN>
      <if> ui.<is_a> method </is_a> <ui> _args=args </ui> <!-- For searching: .<ui_config/> -->
           else ui.<make_widget2> _args=args </make_widget2>
      </if>
    </insert>
  </for_each>
  result_v
</defmethod>

Business.<defclass _name='Gl_number_segment_inheritor'> 
  name=req=Type.id <!-- BUG 6044 -->
    name_f_read_only_config=true
  inherits_from=req=Gl_number_segment
  description=opt=Type.path <!-- BUG 6044 BLL: needs to be path because it needs / -->
    description_f_read_only_config=true
  chars=''
    chars_f_label="Default Value"
  min_size=0=Type.integer <!-- BUG 6079/6044 -->  <!-- type needs to be Integer and  not numeric because numeric is a string -->
    min_size_f_label="Minimum length"
    min_size_f_req_config=true <!-- BUG 6044 -->
  max_size=10=Type.integer <!-- BUG 6079/6044 -->  <!-- type needs to be Integer and  not numeric because numeric is a string -->
    max_size_f_label="Maxiumum length"
    max_size_f_req_config=true <!-- BUG 6044 -->
  <!-- read_only and required may be evaluated using the accessor functions in Gl_number_format_inheritor -->
  read_only="Inherited"=<one_of>
		"Inherited"
		true
		false
	</one_of>
  required="Inherited"=<one_of>
		"Inherited"
		true
		false
	</one_of>
  
  _new_class.<set>
    _db_field_order=<v> "_parent" "name" "inherits_from" "description" "chars" "min_size" "max_size" "read_only" "required" </v>
    _create_field_order=<v> "description" "chars" "read_only" "required" </v>
    _config_field_order=<v> "description" "chars" "read_only" "required" </v>
    _row_field_order=<v> "name" "description" "chars" "min_size" "max_size" "read_only" "required" </v>
  </set>
</defclass>

Business.Gl_number_segment_inheritor.<defmethod _name='make_widget2'>
  key_path=req
  a_key=req
  a_value=req
  a_type=opt
  obj=opt
  owner=opt
  config=opt <!-- widget being used for config.  Currently make_widget2 is only called by config, but that won't always be the case. -->
  maxlength=opt
  style=opt
  
  <set> name=<join> key_path a_key separator="." </join> </set>
 
  <set> a_input=<INPUT> ui="Gl_number_segment_inheritor-make_widget2" name=name value=a_value </INPUT> </set>
  <if> maxlength.<is> opt </is>.<not/>
		a_input.<set> maxlength=maxlength </set>
	</if>
	<if> style.<is> opt </is>.<not/>
		a_input.<set> style=style </set>
	</if>
  
  <set> a_widget=<v> a_input </v> </set>

  <!-- Bug 20391 MJO - Skip validation on the client -->

  obj.<set> chars_f_type=Type.anything </set>
  
  <add_validation> a_geo=obj a_key=a_key a_widget=a_widget keypath=key_path.<join> "." a_key </join> label=a_key.<to_label/> config=config </add_validation>
</defmethod>

Business.Gl_number_segment_inheritor.<defmethod _name='is_read_only_segment'>
	<if> .read_only.<is> "Inherited" </is>
		.inherits_from.read_only
	else
		.read_only
	</if>
</defmethod>

Business.Gl_number_segment_inheritor.<defmethod _name='is_required_segment'>
	<if> .required.<is> "Inherited" </is>
		.inherits_from.required
	else
		.required
	</if>
</defmethod>

Business.<defclass _name='Gl_number_format_inheritor'> <!--id=req=String--> 
	inherits_from=opt=<one_of_type> Gl_number_format </one_of_type>
	segments=opt=<vector_of> Gl_number_segment_inheritor </vector_of>
  _new_class.<set>
    _db_field_order=<v> "_parent" "inherits_from" "segments" </v>
    _config_field_order=<v> "inherits_from" "segments" </v>
    _create_field_order=<v> "inherits_from" </v>
    _row_field_order=<v> "inherits_from" </v>
    ui=vector.ui <!-- HACK: Needs better UI -->
  </set>
</defclass>

Business.Gl_number_format_inheritor.<defmethod _name='rebuild_segments'>
	.<set_part> segments = <v/> </set_part>
  .segments.<set_value> "_db_action" "modified" </set_value> <!-- Bug #12631 Mike O - Set vector to modified -->
	.inherits_from.segments.<for_each>
    <!-- Bug #12631 Mike O - Set each segment to modified -->
		.segments.<set_part> <Gl_number_segment_inheritor>
			name=value.name
			description=value.description
			min_size=value.min_size
			max_size=value.max_size
			inherits_from=value
		</Gl_number_segment_inheritor>.<set_value> "_db_action" "modified" </set_value> </set_part>
	</for_each>
</defmethod>

Business.Gl_number_format_inheritor.<defmethod _name='init'>
	<if> .<has> "inherits_from" </has>.<not/>
		.<set> inherits_from=Gl_number_format.of.<get> Gl_number_format.of.<string_keys/>.<get> 0 </get> </get> </set>
	</if>
	<if> .<has> "segments" </has>.<not/>
		.<rebuild_segments/>
	</if>
	_subject
</defmethod>

Business.Gl_number_format_inheritor.<defmethod _name='ui_config'>
  key_path=req
  a_key=req
  a_value=req
  a_type=req
  obj=req
  owner=req
  
  <!-- Ensure that the child segments are in sync with the parents -->
	<if> a_value.segments.<length/>.<is> a_value.inherits_from.segments.<length/> </is>.<not/>
		a_value.<rebuild_segments/>
	else
		a_value.segments.<vector_keys/>.<for_each>
			
			<set>
				segment=a_value.segments.<get> value </get>
				parent=a_value.inherits_from.segments.<get> value </get>
			</set>
			
      <!-- Bug 19161 MJO - Fixed syntax -->
      segment.<set>
        name=parent.name
        description=parent.description
        min_size=parent.min_size
        max_size=parent.max_size
			</set>


		</for_each>
	</if>

  <TABLE border="1">
		<TR>
			<TD/>
			<TD> "Default Value" </TD>
			<TD> "Read-Only" </TD>
			<TD> "Required" </TD>
		</TR>
		a_value.segments.<for_each> returns="all"
			<TR>
				<TD> value.name </TD>
				<TD>
					<!-- HACK: make_widget doesn't currently support styles, etc -->
					Gl_number_segment_inheritor.<make_widget2>
						a_key="chars"
						<!-- Bug 19161 MJO - Allow anything due to possible override characters -->
						a_type=Type.anything
						a_value=value.chars
						key_path=<join> separator="." key_path "GL_nbr" "segments" key </join>
						obj=value
						maxlength=value.max_size
						style=<STYLE> font-weight="normal" width=<join> Integer.<from> value.max_size </from>.<times> 0.75 </times> "em" </join> </STYLE>
					</make_widget2>
				</TD>
				<TD>
					<set> read_only_type=<Type.one_of>
						<OPTION> <concat> "Default " <if> value.inherits_from.read_only "(Yes)" else "(No)" </if>  </concat> value="Inherited"  </OPTION>
						<OPTION> "Yes" value=true </OPTION>
						<OPTION> "No" value=false </OPTION>
					</Type.one_of> </set>
					<set> read_only_value=<if> value.read_only.<is> "Inherited" </is> read_only_type.<get> 0 </get>
						value.read_only read_only_type.<get> 1 </get>
						else read_only_type.<get> 2 </get>
					</if> </set>
					
					value.<get_type> "read_only" config=true </get_type>.<make_widget2>
						a_key="read_only"
						a_type=read_only_type
						a_value=read_only_value
						key_path=<join> separator="." key_path "GL_nbr" "segments" key </join>
						obj=value
					</make_widget2>
				</TD>
				<TD>
					<set> required_type=<Type.one_of>
						<OPTION> <concat> "Default " <if> value.inherits_from.required "(Yes)" else "(No)" </if>  </concat> value="Inherited"  </OPTION>
						<OPTION> "Yes" value=true </OPTION>
						<OPTION> "No" value=false </OPTION>
					</Type.one_of> </set>
					<set> required_value=<if> value.required.<is> "Inherited" </is> required_type.<get> 0 </get>
						value.required required_type.<get> 1 </get>
						else required_type.<get> 2 </get>
					</if> </set>
					value.<get_type> "required" config=true </get_type>.<make_widget2>
						a_key="required"
						a_type=required_type
						a_value=required_value
						key_path=<join> separator="." key_path "GL_nbr" "segments" key </join>
						obj=value
					</make_widget2>
				</TD>
			</TR>
		</for_each>
	</TABLE>
  
</defmethod>

<!--Gl_number_segment_inheritor.<defclass _name='gl_number_segment_inheritor_widget'/>
Gl_number_segment_inheritor.<set> ui=Gl_number_segment_inheritor.gl_number_segment_inheritor_widget </set>-->
  
  <!-- Bug #11244 Mike O - Added support for include_other_attributes -->
Gl_number_segment_inheritor.<defmethod _name='make_widget'>
  key_path=req
  a_key=req
  a_value=req
  obj=req <!-- =Gl_number_segment_inheritor -->
  a_type=opt
  values=opt
  opt_value=opt
  include_other_attributes=<GEO/>=GEO
  line_nbr = opt <!-- Bug 20642 UMN need to pass on line_nbr -->	
  masked = opt = false<!--IPAY-734 NK Mask data on entry, CF attribute-->
	<set>
    style_class = <join> "text " a_key </join>
    </set>
    <set>
    input_vector = <v/>
  </set>

  <set>
      a_input=<INPUT>
        type='text'
        name=key_path
        id=<join>
            separator="."
            <!-- Bug #10205 Mike O - If the obj has no path, don't try to include it -->
            <if>
              obj.<to_path/>
                obj.<to_path/>
              else
                ""
            </if>
            key_path
          </join>
      obj=key_path
      value=a_value
      class=style_class
      </INPUT>
  </set>
  
  <!-- Bug# 26095 DH - Allocation GL number segments cannot be set to read-only when left blank.
  I am removing the empty condition since it block all manual and GL Format inherited settings.
  <set>
      is_read_only_segment=<and>obj.<is_read_only_segment/>
                            a_value.<is_not> "" </is_not> Bug 13601 - don't disable the textbox if there is no value.
      </and>
  </set>--> 
  <set>is_read_only_segment=obj.<is_read_only_segment/></set>
  
	<set> allocation=obj._container._container._container </set> <!-- ugly hack, but it works -->
		<!-- BUG#6708 description: For the GL segment is configured as not readonly, if login user do not have security item 193, then set GL segment UI as readonly  --> 
    <if>
      <and>
        is_read_only_segment.<not/>
        allocation.<get_transaction/>.<has> "app_obj" </has> <!-- BUG16503 -->
      </and>
      <do>
        <set>
          user_id = allocation.<get_transaction/>.app_obj.<get_app/>.a_user.id
        </set>
        <if>
          <and>
            user_id
													Security_item.<user_may> security_item_id="193" user_id=user_id </user_may>.<not/>
										</and>
												<set> is_read_only_segment = true </set>
							</if>
						</do>
		</if>
		<!-- END OF BUG#6708 --> 
    a_input.<set>
      field_parent=obj.<to_path/>
    </set>
	  
		<!-- set readonly -->
    <if>
      is_read_only_segment  <!-- BUG#4892 -->
      a_input.<set>
        readonly=true disabled=true class=<join> style_class " read_only" </join>
        tabindex=-1 <!-- Bug #12920 Mike O - Exclude read-only fields from tabbing -->
      </set>
		</if>
	  
		<!-- set maxlength and size -->
		<!-- Bug #6013 Mike O - Removed a lot of unnecessary logic, as there will always be a max length -->
		a_input.<set>
			maxlength=obj.max_size
      size=<if>
        obj.max_size.<more> 40 </more> 45 else obj.max_size.<plus> 4 </plus>
      </if>
		</set>
		<!-- Validation (the odd part) -->
    <set>
      label_index=key_path.<split> separator="." </split>.<length/>.<minus> 2 </minus>
    </set>
		.<make_input_js_script>
			tag=<v> a_input </v>
			a_geo=obj
			a_key=""
			keypath=key_path
      label=<concat>
        allocation.description " GL # part " <Integer.from>
          key_path.<split> separator="." </split>.<get> label_index </get>
        </Integer.from>.<plus> 1 </plus>
      </concat>
		</make_input_js_script>
    a_input.<set>
      onblur="validate_tag(this);"  <!--onfocus="do_focus(this)"-->
    </set> <!-- do_focus? -->
    
    include_other_attributes.<for_each>
      include="string_key"
      a_input.<set_value> key=key value=value </set_value>
    </for_each>
    <!-- Bug #10205 Mike O - Restore the missing _error tag -->
    <add_validation> a_geo=_subject a_key=a_key a_widget=a_input include_other_attributes=include_other_attributes keypath=key_path label="" </add_validation>
</defmethod>

Gl_number_segment_inheritor.<defmethod _name='make_input_js_script'>
  tag=req
  a_geo=req
  a_key=req
  keypath=req
  label=req
  config=opt
  
  <set>
    field_types = <v/>
    class_default = a_geo._parent.<get> a_key lookup=true </get>
  </set>
  
  <set> required_obj=a_geo.<is_required_segment/> </set>
  <set> field_type_obj=a_geo.<get_type> a_key </get_type> </set>

  <set> a_field_types=<v/> </set>
  
  <if> a_geo.<is_required_segment/>
        a_field_types.<insert> Type.required </insert>
    </if>

  <!-- Ensure that the minimum size is validated -->
  <!-- This is a workaround and can be removed when minimum size validation is implemented for all possible types -->
  a_field_types.<insert> <Type.text> min_size=a_geo.min_size max_size=a_geo.max_size </Type.text> </insert>
      
  <!-- Bug 19161 MJO - Don't do this validation -->
  <!--a_field_types.<insert> a_geo.<get_type> "chars" if_missing=Type.alphanumeric_and_space </get_type> </insert>-->
    
    field_types.<insert> _args=a_field_types </insert>
  
    <set_field_info_js>
      tag=tag
        key_path=<join> keypath "." a_key </join>
        <!-- Bug # 6193 To Pass the correct GL Segment for Validation ANU -->
        <!-- label=<join> label " part " a_key.<plus> 1 </plus> </join> -->
      label=label
        field_types=a_field_types
        <!-- Bug # 6193 -->
    </set_field_info_js>
</defmethod>

Gl_number_format_inheritor.<defmethod _name='htm_inst_cell'>
 <TD class="Gl_number_format_inheritor-htm_inst_cell"> .<join> separator="-" </join> </TD>
</defmethod>

<!-- Bug#5144 PCI ANU Type Gl_number_format_inheritor doesn't require javascript validation. -->
Gl_number_format_inheritor.<defmethod _name='is_type_for'>
  a_geo=req

  <!-- Bug 19059 MJO - Replace override characters with valid characters so validation passes -->
  <!-- Bug 19161 MJO - Rewrote most of this to fix various issues -->
  <set>
    primary = Gl_number_format.of.primary
  </set>
  
  <set>
    replace_override_character = primary.override_character.<length/>.<more> 0 </more>
  </set>
  
  <and>
    _args = a_geo.segments.<for_each>
      returns="all"
      <set>
        primary_segment = primary.segments.<get> key </get>
        g_value = value.chars
      </set>
        
      <!-- Bug 19059 MJO - Replace override characters -->
      <if>
        replace_override_character
          <set>
            g_value = g_value.<replace>
              primary.override_character primary_segment.chars.char_set.<subvector> 0 1 </subvector>
            </replace>
          </set>
      </if>
        
      <or> <!-- the segment, value, is valid if: -->
        <and> <!-- it's blank and not required -->
          g_value.<is> "" </is>
          primary_segment.required.<not/> <!-- Bug 22243 UMN -->
        </and>
        <and> <!-- or if the characters and size constraints match -->
          primary_segment.chars.<is_type_for> g_value </is_type_for>
          primary_segment.min_size.<more> g_value.<length/> </more>.<not/>
          primary_segment.max_size.<less> g_value.<length/> </less>.<not/>
        </and>
      </or>
    </for_each>
  </and>
</defmethod>

Gl_number_format_inheritor.<defmethod _name='manual_set_part'>
	key=req
	<set> segments=key.segments </set>
	key.<remove> "segments" </remove>
	key.<set_part> segments=segments </set_part>
</defmethod>

Gl_number_format_inheritor.<defmethod _name='copy'>
	new_object=opt
	include=.<all_keys/>
	exclude=<GEO/>
	<set> new_segments=<v/> </set>
	.segments.<for_each>
	  value.<set> "chars_f_type"=value.inherits_from.chars </set>
		new_segments.<set_part> value.<copy/> </set_part>
	</for_each>
	<!-- If new_object is provided, use it -->
	<if> new_object.<is> opt </is>.<not/>
		<try>
			.<for_each> exclude=exclude if_missing="skip" include=include 
				new_object.<set_value> key=key value=value </set_value>
			</for_each>
			new_object.<set> segments=new_segments </set>
			<if_error> <return> the_error </return> </if_error>
			<return> new_object </return>
		</try>
	</if>
	<Gl_number_format_inheritor> inherits_from=.inherits_from segments=new_segments </Gl_number_format_inheritor>
</defmethod>

Gl_number_format_inheritor.<defmethod _name='get_vector'>
	.segments.<for_each> returns="all"
		value.chars
	</for_each>
</defmethod>

<!-- Bug 13561 UMN - add GL print method 
     I wanted to make this a method of Gl_number_format_inheritor but
     I couldn't figure out how to instantiate it in the CASL IDE. So I
     gave up and made it take the string representation of the GL instead.
     So you typically call it after calling get_vector. Inefficient, but
     at least it's testable in the IDE! I tested it with all variations of
     GL numbers I could think of: empty first segment, empty first and last segment,
     empty middle segments, empty end segments, and no empty segments.
  -->
GEO.<defmethod _name='GL_as_string'>
  GL_nbr = req = GEO
  prefix = opt = String
  separator = opt = String
  <set> 
    res = <if> prefix.<is_not> opt </is_not> prefix else "" </if>
    separator = <if> separator.<is_not> opt </is_not> separator else "." </if>
    len = GL_nbr.<length/>.<minus> 1 </minus>
  </set>
  GL_nbr.<for_each>
    <set> res = 
      res.<join> 
        value.<replace> " " "" </replace> 
        <if> key.<is_not> len </is_not> separator else "" </if> <!-- don't want final dot -->
      </join> 
    </set>
  </for_each>
  <return> res </return>
</defmethod>

Gl_number_format_inheritor.<defmethod _name='from_segment_vector'>
	vec=req=vector
	<set> R=<Gl_number_format_inheritor/> </set>
	vec.<vector_keys/>.<for_each>
		R.segments.<get> value </get>.<set> chars=vec.<get> value </get> </set>
	</for_each>
	R
</defmethod>

  <!-- Bug #12598 Mike O - Easy way to get a readable GL number string -->
  <!-- Bug #14079 Mike O - Made padding optional -->
  Gl_number_format_inheritor.<defmethod _name='to_string'>
    separator="|"=String <!-- Bug #14079 Mike O - Switched default to pipe -->
    pad=true
    
    .segments.<for_each>
      returns="all"

      <if>
        pad
      value.chars.<pad>
        length=value.max_size
        a_char=" "
      </pad>
        else
        value.chars.<pad>
        length=1
        a_char=" "
      </pad>
      </if>
    </for_each>.<join> separator=separator </join>.<replace> " " "" </replace>
  </defmethod>
  
</do>
