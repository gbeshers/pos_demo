﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;


namespace baseline
{
  /// <summary>
  /// Holding class for logging records
  /// TTS 13613
  /// </summary>
  public class LogRecord
  {
    public DateTime timestamp { get; set; }
    public string session { get; set; }
    public string user { get; set; }
    public string subject { get; set; }
    public string app { get; set; }
    public string action { get; set; }
    public string argsString { get; set; }
    public string summary { get; set; }
    public string detail { get; set; }
    public int serial_id { get; set; }

    public LogRecord()
    {
    }

    public LogRecord(string session, string user, string app, string subject, string action, string argsString, string summary, string detail)
    {
      this.timestamp = DateTime.Now;
      this.session = session;
      this.user = user;
      this.subject = subject;
      this.app = app;
      this.action = action;
      this.argsString = argsString;
      this.summary = summary;
      this.detail = maskSensitiveData(app, detail);
      this.serial_id = -1;  // Not sure what to set this to...
    }

    /// <summary>
    /// Remove passwords and credit card info from the URL before logging.
    /// See Bug #8120.  I cloned this (in C#) for the URL.
    /// </summary>
    /// <param name="app"></param>
    /// <param name="uri"></param>
    /// <returns></returns>
    private string maskSensitiveData(string app, string uri)
    {
      // Do not log any credit card info. See Bugs 6207 and 10026 and security.casl 
      //credit_card_nbr=4111111111111111&args.ccv=123
      //uri = Regex.Replace(uri, "credit_card_nbr=\\d+", "credit_card_nbr=HIDDEN");
      //uri = Regex.Replace(uri, "ccv=\\d+", "ccv=HIDDEN");
      if (uri.Contains("credit_card_nbr="))
        return "Credit Card Payment";

      // Change password and new_password to HIDDEN
      uri = Regex.Replace(uri, "password=\\w+", "password=HIDDEN");
      uri = Regex.Replace(uri, "new_password=\\w+", "new_password=HIDDEN");


      if (app.Contains("Editing_hashed_password"))
      {
        // new_value="HIDDEN"
        uri = Regex.Replace(uri, "new_value=\\w+", "new_value=HIDDEN");
        // new_value_confirm="HIDDEN"
        uri = Regex.Replace(uri, "new_value_confirm=\\w+", "new_value_confirm=HIDDEN");
      }
      return uri;
    }
  }

}
