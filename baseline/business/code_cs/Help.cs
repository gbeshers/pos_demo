// Bug 21952 UMN add help

using System;
using CASL_engine;

namespace baseline
{
  public class Help
  {
    private const String guidance = @"
        <!DOCTYPE html>
        <html>
        <head>
          <meta http-equiv='x-ua-compatible' content='IE=edge'>
          <meta charset='utf-8'>
          <meta name='viewport' content='width=device-width, initial-scale=1, minimal-ui'>
          <link rel='stylesheet' href='{2}'>
          <title>{0} Help</title>
        </head>
        <body>
          <article class='markdown-body'>
            <h2>Sorry, no help was found for {0}-{1}</h2>
            <p>To configure help for this item:</p> 
            <ol>
              <li>Verify that you are not in View-only mode.</li>
              <li>Verify that you have access to create help items.</li>
              <li>Choose Help from the main Configuration menu.</li>
              <li>Click the Create button.</li>
              <li>Next, select the class <mark>{0}</mark> or <mark>Shared_help</mark> in the drop-down menu.</li>
              <li>Fill in the Property field with <mark>{1}</mark>.</li>
              <li>Click the Create button.</li>
              <li>Edit the help item in the markdown editor, or provide a URL in the help_link field.</li>
              <li>When finished editing, click the Update button.</li>
              <li>Click the Continue button.</li>
              <li>Finally, commit the changes by clicking the Commit button.</li>
            </ol>
          </article>
        </body>
        </html>";

    private const String theHelp = @"
        <!DOCTYPE html>
        <html>
        <head>
          <meta http-equiv='x-ua-compatible' content='IE=edge'>
          <meta charset='utf-8'>
          <meta name='viewport' content='width=device-width, initial-scale=1, minimal-ui'>
          <link rel='stylesheet' href='{2}'> 
          <title>{0} Help</title>
        </head>
        <body>
          <article class='markdown-body'>
            <h2>{0} Help</h2>
            {1}
          </article>
        </body>
        </html>";

    public static string RenderHelpHtm(params object[] args)
    {
      GenericObject geoArgs = misc.convert_args(args);
      GenericObject helpObj = geoArgs.get("helpObj", c_CASL.c_opt) as GenericObject;
      string cssPath = c_CASL.GEO.get("baseline_static") + "/business/include/github-markdown.css";

      if (!helpObj.Equals(c_CASL.c_opt))
      {
        string helpID = String.Format("{0}-{1}", helpObj.get("class", "No class found") as string, helpObj.get("property", "No prop found") as string);
        String markdownHelp = helpObj.get("help", "No help stored") as String;
        String htmlHelp = CommonMark.CommonMarkConverter.Convert(markdownHelp);
        return String.Format(theHelp, helpID, htmlHelp, cssPath);
      }
      else
      {
        String helpClass = geoArgs.get("helpClass", "No class found") as String;
        String helpProp = geoArgs.get("helpProp", "No prop found") as String;
        return String.Format(guidance, helpClass, helpProp, cssPath);
      }
    }
  }
}