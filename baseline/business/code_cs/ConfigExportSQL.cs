﻿using System;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using CASL_engine;

// IPAY-737 UMN Add ability to create a file with exported SQL objects from Config DB.
// Functions a bit like the Tasks->Generate Scripts feature in SQL Server Management Studio.

namespace baseline
{
  public class ConfigExportSQL
  {
    private const String insertPrefix = "INSERT CBT_Flex(container, field_key, field_value, revision, is_root) VALUES(";
    private const String updatePrefix = "UPDATE CBT_Flex SET ";
    
    public static object ExportSQLToFile(params object[] args)
    {
      StringBuilder returnedSQL = new StringBuilder();
      GenericObject geoArgs = misc.convert_args(args);

      // Let's have some nice syntax highlighting!
      string cssPath = c_CASL.GEO.get("baseline_static") + "/business/include/prism.v1.23.min.css";
      string jsPath = c_CASL.GEO.get("baseline_static") + "/business/include/prism.v1.23.min.js";
      string jsMainPath = c_CASL.GEO.get("baseline_static") + "/business/include/main.js";
      
      // IPAY-978 UMN fix copying issue on Chrome/Chromium Edge
      string jsClipboardPath = c_CASL.GEO.get("baseline_static") + "/business/include/clipboard.v2.0.8.min.js";

      // objectsGEO will look something like this:
      // 0 = <GEO> do_update = "false" path = Business.User.of.unarsu </GEO>
      // 1 = <GEO> do_update = "true" path = Business.Department.of."001" </GEO>
      // NOTE: the path is actually the real object.
      GenericObject objectsGEO = geoArgs.get("objectsGEO") as GenericObject;
      int objectCount = objectsGEO.getIntKeyLength();

      GenericObject configDB = c_CASL.c_object("TranSuite_DB.Config.db_connection_info") as GenericObject;
      SqlDataReader dataReader = null;
      SqlConnection selectConnection = null;

      // Strategy is to query the DB for each path, retrieving the values, then write them out as SQL insert or update statements
      try
      {
        // Write the HTML header
        returnedSQL.AppendLine("<!DOCTYPE html>")
          .AppendLine("<html lang='en'>")
          .AppendLine("<head>")
          .AppendLine($"<link href='{cssPath}' rel='stylesheet' />")
          .AppendLine("</head>")
          .AppendLine("<body class='language-sql'>");

        // Because we save out CASL elements sometimes, don't want to have to escape the &lt; etc, so use unescaped plugin
        returnedSQL.AppendLine("<script type='text/plain' class='language-sql'>");

        if (objectCount <= 0)
        {
          returnedSQL.AppendLine("-- Ummm. Your export list is empty. Open Users, Workgroups etc., select objects and click the Export Selected button.");
          goto trailer;
        }
        else 
        { 
          returnedSQL.AppendLine($"SET NOCOUNT ON;{Environment.NewLine}");  // shut up SSMS
        }

        // use container plus percent (pct) cause want to save process pages for Transactions, etc.
        using (selectConnection = new SqlConnection())
        {
          // Open a connection for the select
          selectConnection.ConnectionString = (string)configDB.get("db_connection_string");
          selectConnection.Open();

          // Prepare SqlCommand only once and repeat select multiple times. Hope it will reuse the query plan!
          SqlCommand selectCommand = PrepareSql(selectConnection);

          // see above for what objectsGEO looks like
          // walk through each GEO in the objectsGEO array
          for (int i = 0; i < objectCount; i++)
          {
            // pathAndUpdateFlag will look like this: <GEO> do_update = false path = Business.User.of.unarsu </GEO>
            GenericObject pathAndUpdateFlag = objectsGEO.get(i) as GenericObject;
            string pathString = "";
            bool doUpdate = false;

            // need to get the path of the object as a string
            object P = ((GenericObject)pathAndUpdateFlag.get("path")).to_path();
            if (false.Equals(P))
              continue;
            else if ("".Equals(P))
              continue;
            else
              pathString = P as string;

            // get the update flag. Of course the casl machinery turns it to a truthy string
            object U = pathAndUpdateFlag.get("do_update");
            if (false.Equals(U))
              continue;
            doUpdate = (string)U == "true";

            // Set the container percent sql argument to read from config DB
            selectCommand.Parameters["@container"].Value = pathString;
            selectCommand.Parameters["@container_pct"].Value = pathString + ".%";
            dataReader = selectCommand.ExecuteReader();

            // User chose to do an insert of this object
            if (!doUpdate)
            {
              returnedSQL.AppendLine($"---- INSERT OBJECT {pathString}")
                .AppendLine($"IF NOT EXISTS (SELECT 1 FROM CBT_Flex WHERE container = '{pathString}' and field_key = 'null' and field_value = 'null')")
                .AppendLine("BEGIN");
            }
            else  // User chose to do an update of this object
            {
              returnedSQL.AppendLine($"---- UPDATE OBJECT {pathString}")
                .AppendLine($"IF EXISTS (SELECT 1 FROM CBT_Flex WHERE container = '{pathString}' and field_key = 'null' and field_value = 'null')")
                .AppendLine("BEGIN");
            }
            while (dataReader.Read())
            {
              string container = $"N'{dataReader.GetString(0)}'";
              string field_key = $"N'{dataReader.GetString(1)}'";
              string field_value = $"N'{dataReader.GetString(2)}'";

              // following might be NULL
              string revision = dataReader.IsDBNull(3) ? "NULL" : dataReader.GetInt32(3).ToString();
              string is_root = dataReader.IsDBNull(4) ? "NULL" : dataReader.GetInt32(4).ToString();
              if (!doUpdate)
              {
                returnedSQL.Append(insertPrefix)
                  .AppendLine($"{container},{field_key},{field_value},{revision},{is_root});");
              }
              else
              {
                returnedSQL.Append(updatePrefix)
                  .AppendLine($"field_key={field_key},field_value={field_value},revision={revision},is_root={is_root} WHERE container={container};");
              }
            }
            if (!doUpdate)
            {
              // provide meaningful output for the script
              returnedSQL.AppendLine($"PRINT 'Inserted {pathString}'")
                .AppendLine("END")
                .AppendLine("ELSE BEGIN")
                .AppendLine($"  PRINT 'Cannot insert object {pathString} as it already exists.'")
                .AppendLine($"END{Environment.NewLine}");
            }
            else
            {
              // provide meaningful output for the script
              returnedSQL.AppendLine($"PRINT 'Updated {pathString}'")
                .AppendLine("END")
                .AppendLine("ELSE BEGIN")
                .AppendLine($"  PRINT 'Cannot update object {pathString} as it does not exist.'")
                .AppendLine($"END{Environment.NewLine}");
            }
            dataReader.Close();
          }
        }

        // Write the HTML trailer and
        // IPAY-978 UMN fix copying issue on Chrome/Chromium Edge
        trailer:
          returnedSQL.AppendLine("</script>")
            .AppendLine($"<script src='{jsClipboardPath}'></script>")
            .AppendLine($"<script src='{jsPath}'></script>")
            .AppendLine($"<script src='{jsMainPath}'></script>")
            .AppendLine("</body>")
            .AppendLine("</html>");
      }
      catch (Exception e)
      {
        returnedSQL.AppendLine($"{Environment.NewLine}**** Exception: {e.Message}.");
        Logger.cs_log_trace("Error writing Exported Config SQL", e);
      }
      finally
      {
        if (dataReader != null && !dataReader.IsClosed)
          dataReader.Close();
        if (selectConnection != null)
          selectConnection.Close();
      }

      return returnedSQL.ToString();
    }

    /// <summary>
    /// Prepare a parameterized SELECT call.
    /// </summary>
    /// <param name="selectConnection"></param>
    /// <returns></returns>
    private static SqlCommand PrepareSql(SqlConnection selectConnection)
    {
      // Include fix suggested by DJD
      String containerSQL = $"SELECT container, field_key, field_value, revision, is_root FROM CBT_Flex WHERE container = @container OR container like @container_pct";
      SqlCommand selectCommand = new SqlCommand(containerSQL, selectConnection);

      // Prepare the statement once and only once
      selectCommand.Prepare();

      // Create and setup parameters once and only once
      // but do not set their values here. Add length per Fred's recommendation.
      // Add fix from DJD
      selectCommand.Parameters.Add(new SqlParameter("@container", SqlDbType.VarChar, 255));
      selectCommand.Parameters.Add(new SqlParameter("@container_pct", SqlDbType.VarChar, 255));
      return selectCommand;
    }
  }
}
