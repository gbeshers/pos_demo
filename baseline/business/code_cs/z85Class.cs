﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

/*
 * The ToZ85String and FromZ85String extension method can be brought into scope with
 * using directive for example:
 * using z85
 * Guid tokenGuid = System.Guid.NewGuid();
 * string token = tokenGuid.ToZ85String();
 * Guid guid = token.FromZ85String();
 */
namespace z85
{
    public class z85Class
    {
        //  Maps base 256 to base 85 encoder 
        static char[] encoder = {
        '0','1','2','3','4','5','6','7','8','9',
        'a','b','c','d','e','f','g','h','i','j',
        'k','l','m','n','o','p','q','r','s','t', 
        'u','v','w','x','y','z','A','B','C','D',
        'E','F','G','H','I','J','K','L','M','N', 
        'O','P','Q','R','S','T','U','V','W','X', 
        'Y','Z','.','-',':','+','=','^','!','/', 
        '*','?','&','<','>','(',')','[',']','{', 
        '}','@','%','$','#'
        };

        //  Maps base 85 to base 256
        //  We chop off lower 32 and higher 128 ranges
        static byte[] decoder = {
        0x00, 0x44, 0x00, 0x54, 0x53, 0x52, 0x48, 0x00, 
        0x4B, 0x4C, 0x46, 0x41, 0x00, 0x3F, 0x3E, 0x45, 
        0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 
        0x08, 0x09, 0x40, 0x00, 0x49, 0x42, 0x4A, 0x47, 
        0x51, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2A, 
        0x2B, 0x2C, 0x2D, 0x2E, 0x2F, 0x30, 0x31, 0x32, 
        0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 
        0x3B, 0x3C, 0x3D, 0x4D, 0x00, 0x4E, 0x43, 0x00, 
        0x00, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 
        0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 
        0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F, 0x20, 
        0x21, 0x22, 0x23, 0x4F, 0x00, 0x50, 0x00, 0x00
        };

        /// <summary>
        /// encoding a byte array as a string
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
       public  string z85_encode(byte[] data)
        {
            string sEncoded = "";
            int size = data.Count();

            // accepts only byte arrays bounded to 4 bytes
            if (size % 4 != 0)
                return null;

            int encoded_size = size * 5 / 4;

            char[] encoded = new char[encoded_size + 1];
            uint char_nbr = 0;
            uint byte_nbr = 0;
            UInt32 value = 0;

            while (byte_nbr < size)
            {
                // accumulate value in base 256 (binary)
                value = value * 256 + data[byte_nbr++];
                if (byte_nbr % 4 == 0)
                {
                    // output value in base 85
                    uint divisor = 85 * 85 * 85 * 85;
                    while (divisor >= 1)
                    {
                        encoded[char_nbr++] = encoder[value / divisor % 85];
                        divisor /= 85;
                    }

                    value = 0;
                }
            }
            Debug.Assert(char_nbr == encoded_size);

            encoded[char_nbr] = '\0';
            string s = new string(encoded);
            sEncoded = s;
            return sEncoded;
        }

        /// <summary>
        /// Decode an encoded string into a byte array; size of array will be
        /// string.length * 4 / 5
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public byte[] z85_decode(string s)
        {
            int iLen = s.Length;
            if ( iLen % 5 != 0)
                return null;
            int decoded_size = iLen * 4 / 5;

            byte[] decoded = new byte[decoded_size];

            uint byte_nbr = 0;
            uint char_nbr = 0;
            UInt32 value = 0;
            while (char_nbr < iLen)
            {
                // accumulate value in base 85
                value = value * 85 + decoder[(byte)s[(int)char_nbr++] - 32];

                if (char_nbr % 5 == 0)
                {
                    // output value in base 256
                    uint divisor = 256 * 256 * 256;
                    while (divisor >= 1)
                    {
                        byte b =  Convert.ToByte(value / divisor % 256);
                        decoded[byte_nbr++] = b;
                        divisor /= 256;
                    }

                    value = 0;
                }

            }

            Debug.Assert(byte_nbr == decoded_size);
            return decoded;
        }

    }

    public static class GuidExtensions
    {
        public static string ToZ85String(this Guid guid)
        {
            z85Class z85 = new z85Class();
            return z85.z85_encode(guid.ToByteArray());
        }

        public static Guid FromZ85String(this string sZ85)
        {
            z85Class z85 = new z85Class();
            byte[] b = z85.z85_decode(sZ85);

            return new Guid(b);
        }
    }
    

}
