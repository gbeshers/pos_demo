﻿//Bug#14235
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Text;
using CASL_engine;
using System.Collections.Concurrent;
using DeviceManagement;
using System.Web;
/*
  ERROR CODES FOR CORE PAYMENT GATEWA ARE IN THE FORMAT CPG-200.... 

  CPG-200 - CASL Exception error 
  CPG-201 - Exception Error 
  CPG-202 - "Failed to pos CORE Event to DB"
 
 
*/

namespace baseline
{
  public class PaymentGateway
  {
    public PaymentGateway() { }

    private static ConcurrentDictionary<string, string> DictForDuplicateChecking = new ConcurrentDictionary<string, string>();
    const string FMT = "O";

    /// <summary>
    /// 
    /// </summary>
    /// <param name="args"> request_args=req</param>
    /// <returns></returns>
    public static string DetectDuplicateRequest(params object[] args)
    {
        string R = "";
        try
        {
            GenericObject geoARGS = misc.convert_args(args);
            String sDuplicateFieldToCheck = "";
            GenericObject geoRequestargs = geoARGS.get("request_args") as GenericObject;
            string sTransactiontype = geoRequestargs.get("e_transactiontype", "") as string;

           // if (sTransactiontype != "sale") return R; // only apply to sale transaction type

            string sDuplicateDetectionFields = (string)c_CASL.c_object("Business.CORE_Payment_Gateway_config.data.duplicate_detection_unique_fields");
            if (sDuplicateDetectionFields == "") return R; // only apply if configured
            int iDuplicateDetectionTimeout = (int)c_CASL.c_object("Business.CORE_Payment_Gateway_config.data.duplicate_detection_timeout");

            // create duplicatefieldtocheck as duplicat flag key 
            GenericObject geoFields = c_CASL.c_GEO();
            string[] arDuplicateDetectionFields = sDuplicateDetectionFields.Split(',');
            foreach (string sFieldKey in arDuplicateDetectionFields)
            {
                string sFieldValue = String.Format("{0}", geoRequestargs.get(sFieldKey, "")).Trim();
                if (sFieldValue != "")
                { 
                    geoFields.set(sFieldKey,sFieldValue);
                    sDuplicateFieldToCheck = sDuplicateFieldToCheck + sFieldKey + "="+sFieldValue+" "; 
                }
            }
            if (sDuplicateFieldToCheck == "")//if no duplicate key available, ignore request
            {
                Logger.cs_log("NO DUPLICATE DETECTION KEY AVAILABLE FOR " + sDuplicateDetectionFields);
                return R;
            }
            sDuplicateFieldToCheck = sDuplicateFieldToCheck+ sTransactiontype;

            if (DictForDuplicateChecking.Keys.Contains(sDuplicateFieldToCheck))
            {
                String sPreviousRequestTimeStamp = DictForDuplicateChecking[sDuplicateFieldToCheck];
                DateTime dtPreviousRequest = DateTime.ParseExact(sPreviousRequestTimeStamp, FMT, System.Globalization.CultureInfo.InvariantCulture);
                DateTime dtRequestTimout = dtPreviousRequest.AddSeconds(iDuplicateDetectionTimeout);
                if ( DateTime.Now > dtRequestTimout) //upon exceeding Duplicate Detection Timeout, it is not duplicate, reset original dupliate flag with current timestamp  
                {
                    String sRequestTimeStamp = DateTime.Now.ToString(FMT);
                    DictForDuplicateChecking[sDuplicateFieldToCheck] = sRequestTimeStamp;
                    //geoRequestargs.set("isremoveduplicateflaguponsuccess", true);
                    geoRequestargs.set("addnewduplicateflagtocheck", sDuplicateFieldToCheck);
                    R = "";
                }
                else // Detect duplicate request within Duplicate Pretection Timeout
                {
                    string sDuplicateMessage = string.Format("Duplicate request detected {0} (CPG-104)", sDuplicateFieldToCheck);
                    Logger.cs_log(sDuplicateMessage);
                    R = sDuplicateMessage;
                }
            }
            else
            {

                String sRequestTimeStamp = DateTime.Now.ToString(FMT);
                DictForDuplicateChecking[sDuplicateFieldToCheck] = sRequestTimeStamp;
                geoRequestargs.set("addnewduplicateflagtocheck", sDuplicateFieldToCheck);
                R = "";
            }
        }
        catch (Exception e)
        {
            Logger.cs_log_trace("Error in DetectDuplicateRequest", e);
            R = "Error upon duplicate request prevention"; 
        }
        return R;
    }
    // remove duplicaterequest flag for failure
    public static string RemoveDuplicateRequest(params object[] args)
    {
        try
        {
            GenericObject geoARGS = misc.convert_args(args);
            GenericObject geoRequestargs = geoARGS.get("request_args") as GenericObject;
            String sDuplicateFieldToCheck = (string)geoRequestargs.get("addnewduplicateflagtocheck", "");
            if (sDuplicateFieldToCheck!="" && DictForDuplicateChecking.Keys.Contains(sDuplicateFieldToCheck))
            {
                string outvalue;
                DictForDuplicateChecking.TryRemove(sDuplicateFieldToCheck, out outvalue);
            }
        }
        catch (Exception e)
        {
            Logger.cs_log_trace("Error in RemoveDuplicateRequest", e);
            return "";
        }
        return "";
    }
    /// <summary>
    /// process core event
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static Object ProcessCoreEventWithoutUI(params object[] args)
    {
      GenericObject geoError = null;
      try
      {
        GenericObject geoARGS = misc.convert_args(args);
        String user_id = (string)geoARGS.get("user_id", "");
        String department_id = (string)geoARGS.get("department_id", "");
        String core_file_id = (string)geoARGS.get("core_file_id", "");
        String source_type = (string)geoARGS.get("source_type", "");
        String transaction_id = (string)geoARGS.get("transaction_id", "");
        String tender_id = (string)geoARGS.get("tender_id", "");
        GenericObject geoTransaction = geoARGS.get("transaction_data", c_CASL.c_GEO()) as GenericObject;
        GenericObject geoTender = geoARGS.get("tender_data", c_CASL.c_GEO()) as GenericObject;
        String session_id = (string)geoARGS.get("session_id", "");
        
        // Bug IPAY-1151 MJO - Check for invalid tender type
        decimal amount = (Decimal)geoTransaction.get("amount", 0.0);

        if (amount > 0.00M && !(c_CASL.c_object("Tender.of") as GenericObject).has(tender_id))
        {
          return (GenericObject)c_CASL.c_make("error", "code", "CPG-203", "message", "Invalid tender type", "title", "CORE Gateway Error", "throw", false);
        }
        else if (amount > 0.00M) // IPAY-1739 DJD: Check if amount exceeds the tender maximum
        {
          GenericObject tenderClass = (GenericObject)((GenericObject)c_CASL.c_object("Tender.of")).get(tender_id);
          if (tenderClass != null)
          {
            decimal? maxTender = (decimal?)tenderClass.get("max_tender", 0.00M);
            if (maxTender.HasValue && maxTender > 0.00M && maxTender < amount) 
            {
              string maxTenderMessage = string.Format("The {0} tender allows a maximum amount of ${1:0.00} per tender configuration."
                , tenderClass.get("description", "selected").ToString(), maxTender);
              return (GenericObject)c_CASL.c_make("error", "code", "CPG-204", "message", maxTenderMessage, "title", "CORE Gateway Error", "throw", false);
            }
          }
        }
                
        // Bug 25664 MJO - Fee support
        GenericObject geoFeeTender = geoARGS.get("tender_fee_data") as GenericObject;
        GenericObject geoFeeTransaction = geoARGS.get("transaction_fee_data") as GenericObject;
        String transactionFeeId = geoARGS.get("transaction_fee_id") as String;

        GenericObject geoEvent = null;
        geoEvent = CreateIpaymentEventPhysics(user_id, department_id, core_file_id, source_type, transaction_id, 
          tender_id, geoTransaction, geoTender, transactionFeeId, geoFeeTransaction, geoFeeTender);
        object R = TranSuiteServices.TranSuiteServices.PostEventToT3_cs("_subject", geoEvent,
            "data", c_CASL.c_object("TranSuite_DB.Data.db_connection_info"),
            "config", c_CASL.c_object("TranSuite_DB.Config.db_connection_info"),
            "session_id", session_id
            );
        
        // Bug 14927 UMN: return receipt ref#, not TROUTD in e_transactionid.
        Int64 eventNbr = 0;
        if (Int64.TryParse(R.ToString(), out eventNbr))
        {
          return String.Format("{0}-{1}", core_file_id, eventNbr);
        }
        else
        {
         geoError = (GenericObject)c_CASL.c_make("error", "code", "CPG-201", "message", "Failed to pos CORE Event to DB", "title", "CORE Gateway Error", "throw", false);
          return geoError;
        }
      }
      catch (CASL_error ce)
      {
        GenericObject  the_error = ce.casl_object;
        return the_error;
      }
      catch (Exception ex)
      {
        Logger.cs_log_trace("Payment Gateway error", ex);
        geoError = (GenericObject)c_CASL.c_make("error", "code", "CPG-202", "message", ex.Message, "title", "CORE Gateway Error", "throw", false);
        return geoError;
      }
      finally
      {
      }
    }

    /// <summary>
    /// Create Core Event Object
    /// </summary>
    /// <param name="strUserID"></param>
    /// <param name="strDeptID"></param>
    /// <param name="strCoreFileID"></param>
    /// <param name="strSourceType"></param>
    /// <param name="strTTID"></param>
    /// <param name="strtenderid"></param>
    /// <param name="geoTransaction"></param>
    /// <param name="geoTender"></param>
    /// <returns></returns>
    public static GenericObject CreateIpaymentEventPhysics(String strUserID, String strDeptID, 
      String strCoreFileID, String strSourceType, String strTTID, String strtenderid, 
      GenericObject geoTransaction, GenericObject geoTender,
      String strFeeTTID, GenericObject geoFeeTransaction, GenericObject geoFeeTender)
    {
      #region get parameters
      Decimal dAmount = (Decimal)geoTransaction.get("amount", 0.0);
      //GenericObject geoDEPFILE = null;
      #endregion

      //# region ipayment physical instance TG_DEPFILE_DATA
      //// TG_DEPFILE_DATA
      //geoDEPFILE = c_CASL.c_instance("TranSuite_DBAPI.TG_DEPFILE_DATA");
      //geoDEPFILE.set("FILETYPE", "");
      //geoDEPFILE.set("FILEDESC", "");
      //geoDEPFILE.set("STATUS", "");
      //geoDEPFILE.set("EFFECTIVEDT", DateTime.Now.ToString());
      //geoDEPFILE.set("SOURCE_TYPE", strSourceType);
      //geoDEPFILE.set("SOURCE_GROUP", "");
      //geoDEPFILE.set("SOURCE_DATE", "");
      //geoDEPFILE.set("DEPTID", strDeptID);
      //geoDEPFILE.set("CREATOR_ID", strUserID);
      //geoDEPFILE.set("OPEN_USERID", strUserID);
      //geoDEPFILE.set("LOGIN_ID", strUserID);
      //#endregion

      // TG_PAYEVENT_DATA
      //GenericObject geoPAYEVENTs = c_CASL.c_instance("vector");
      //geoDEPFILE.set("table_TG_PAYEVENT_DATA", geoPAYEVENTs);
      GenericObject geoPAYEVENT = c_CASL.c_instance("TranSuite_DBAPI.TG_PAYEVENT_DATA");
      //geoPAYEVENTs.insert(geoPAYEVENT);
      geoPAYEVENT.set("EVENTNBR", "");
      geoPAYEVENT.set("FILENAME", strCoreFileID);
      geoPAYEVENT.set("USERID", strUserID);
      geoPAYEVENT.set("SOURCE_DATE", "");
      geoPAYEVENT.set("SOURCE_GROUP", "");
      geoPAYEVENT.set("SOURCE_REFID", "");
      geoPAYEVENT.set("SOURCE_TYPE", strSourceType);

      // TG_TRAN_DATA
      GenericObject geoTRANs = c_CASL.c_instance("vector");
      geoPAYEVENT.set("table_TG_TRAN_DATA", geoTRANs);
      GenericObject geoTranConfig = null;
      string strTranPath = string.Format("Transaction.of.\"{0}\"", strTTID);
      geoTranConfig = c_CASL.get_GEO().get_path(strTranPath, true, null) as GenericObject;
      if (geoTranConfig == null)
        throw new Exception(" Can not find Transaction (" + strTranPath + ").");

      // Bug 25664 MJO - TRAN creation moved into its own function so it can be called twice for fees
      geoTRANs.insert(CreateIpaymentTransactionPhysics(strUserID, strDeptID, strCoreFileID, strSourceType, strTTID, geoTransaction, dAmount, geoTranConfig, 1));

      GenericObject geoFeeTranConfig = null;
      if (geoFeeTransaction != null)
      {
        Decimal dFeeAmount = (Decimal)geoFeeTransaction.get("amount", 0.0);
        string strFeeTranPath = string.Format("Transaction.of.\"{0}\"", strTTID);
        geoFeeTranConfig = c_CASL.get_GEO().get_path(strFeeTranPath, true, null) as GenericObject;
        if (geoFeeTranConfig == null)
          throw new Exception(" Can not find Transaction (" + strFeeTranPath + ").");

        geoTRANs.insert(CreateIpaymentTransactionPhysics(strUserID, strDeptID, strCoreFileID, strSourceType, strFeeTTID, geoFeeTransaction, dFeeAmount, geoFeeTranConfig, 2));
      }

      #region tg_tender_data
      // Bug 25664 MJO - TENDER creation moved into its own function so it can be called twice for fees
      GenericObject geoTENDERs = c_CASL.c_instance("vector");
      geoPAYEVENT.set("table_TG_TENDER_DATA", geoTENDERs);
      if (strtenderid != "")
      {
        geoTENDERs.insert(CreateIpaymentTenderPhysics(strUserID, strDeptID, strCoreFileID, strSourceType, strtenderid, geoTender, geoTranConfig, dAmount, 1));
      }
      if (geoFeeTender != null)
      {
        Decimal dFeeAmount = (Decimal)geoFeeTender.get("amount", 0.0);
        geoTENDERs.insert(CreateIpaymentTenderPhysics(strUserID, strDeptID, strCoreFileID, strSourceType, strtenderid, geoFeeTender, geoTranConfig, dFeeAmount, 2));
      }
      #endregion
      return geoPAYEVENT;
    }

    // Bug 25664 MJO - Create TRAN and its dependent objects
    public static GenericObject CreateIpaymentTransactionPhysics(String strUserID, String strDeptID,
      String strCoreFileID, String strSourceType, String strTTID, GenericObject geoTransaction,
      decimal dAmount, GenericObject geoTranConfig, int tranNbr)
    {
      GenericObject geoTRAN = c_CASL.c_instance("TranSuite_DBAPI.TG_TRAN_DATA");
      geoTRAN.set("TRANNBR", tranNbr);
      geoTRAN.set("TTID", strTTID);
      String sDescription = geoTransaction.get("description", geoTranConfig.get("description")) as String;
      geoTRAN.set("TTDESC", sDescription);
      geoTRAN.set("TRANAMT", dAmount);
      geoTRAN.set("DISTAMT", 0.00);
      geoTRAN.set("ITEMIND", "S");
      geoTRAN.set("COMMENTS", "");
      geoTRAN.set("CONTENTTYPE", "1");
      geoTRAN.set("TAXEXIND", "N");
      geoTRAN.set("SYSTEXT", "");
      geoTRAN.set("SOURCE_DATE", "");
      geoTRAN.set("SOURCE_REFID", "");

      #region tran cf
      GenericObject geoCustomFields = c_CASL.c_GEO();
      geoTRAN.set("_custom_keys", geoCustomFields);

      foreach (string key in geoTransaction.getStringKeys())
      {
        object value = geoTransaction.get(key, "");
        string strTTIDFORMATED = strTTID;
        int n;
        bool isNumeric = int.TryParse(strTTID.Substring(0, 1), out n);
        if (isNumeric) strTTIDFORMATED = string.Format("\"{0}\"", strTTID);
        string strFieldPath = string.Format("Transaction.of.{0}.{1}_f_custom_field", strTTIDFORMATED, key);
        GenericObject CFConfig = c_CASL.get_GEO().get_path(strFieldPath, true, new GenericObject()) as GenericObject;

        if (CFConfig.Count > 0 || key == "REVERSAL_OF")
        {
          geoCustomFields.insert(key);
          // [custom_field]=value
          geoTRAN.set(key, value);
          // [custom_field]_f_id=..
          string strCFIDKey = string.Format("{0}_f_id", key);
          string strCFIDValue = CFConfig.get("id", "") as string;
          //geoTRAN.set(strCFIDKey, strCFIDValue); BUG15754 do not set _f_id, CUSTID column is still int type in GH DATABASE
          // [custom_field]_f_label=..
          string strCFLabelKey = string.Format("{0}_f_label", key);
          string strCFLabelValue = CFConfig.get("label", "") as string;
          geoTRAN.set(strCFLabelKey, strCFLabelValue);
          if (key == "REVERSAL_OF")
          {
            string strCFStoreDBKey = string.Format("{0}_f_store_in_DB", key);
            object strCFStoreDBValue = true;
            geoTRAN.set(strCFStoreDBKey, strCFStoreDBValue);
          }
        }
      }

      #region Handle system custom field update_system_interface_list BUG#7665
      string strUpdateSIListPath = string.Format("Business.Transaction.of.{0}.update_system_interface_list", strTTID);
      GenericObject geoUpdateSIListConfig = c_CASL.get_GEO().get_path(strUpdateSIListPath, true,
        c_CASL.c_instance("vector")) as GenericObject;
      if (geoUpdateSIListConfig.vectors.Count > 0)
      {
        string xmlUpdateSIList = misc.CASL_to_xml_large("_subject", geoUpdateSIListConfig);
        string strUpdateSICFTagName = "update_system_interface_list";
        geoCustomFields.insert(strUpdateSICFTagName);
        // [custom_field]=value
        geoTRAN.set(strUpdateSICFTagName, xmlUpdateSIList);
        // [custom_field]_f_id=..
        string strUpdateSICFIDKey = string.Format("{0}_f_id", strUpdateSICFTagName);
        geoTRAN.set(strUpdateSICFIDKey, "");
        // [custom_field]_f_label=..
        string strUpdateSICFLabelKey = string.Format("{0}_f_label", strUpdateSICFTagName);
        geoTRAN.set(strUpdateSICFLabelKey, "");
      }
      #endregion
      #endregion
      #region tg_item_data
      //TG_ITEM_DATA		
      string strAGPath = string.Format("Business.Transaction.of.{0}.allocation_group_classes._objects.0.allocation_classes", strTTID);
      GenericObject geoAGConfig = c_CASL.get_GEO().get_path(strAGPath, true, new GenericObject()) as GenericObject;
      decimal decTranAmount = dAmount;
      string strDescription = "";
      string strGLACCTNBR = "";
      GenericObject geoITEMs = c_CASL.c_instance("vector");
      geoTRAN.set("table_TG_ITEM_DATA", geoITEMs);
      GenericObject geoItemConfig = null;
      if (geoAGConfig.getLength() == 0) // WITHOUT CONFIGURED ALLOCATION
      {
        GenericObject geoITEM = c_CASL.c_instance("TranSuite_DBAPI.TG_ITEM_DATA");
        geoITEMs.insert(geoITEM);
        geoITEM.set("ITEMNBR", 1);
        geoITEM.set("AMOUNT", decTranAmount);
        geoITEM.set("TOTAL", decTranAmount);
        geoITEM.set("QTY", 1);
        geoITEM.set("GLACCTNBR", strGLACCTNBR);
        geoITEM.set("ITEMDESC", strDescription);
        geoITEM.set("ACCTID", "");
        geoITEM.set("ITEMACCTID", "");
        geoITEM.set("ITEMID", "0");
        geoITEM.set("TAXED", "N");
      }
      else // WITH CONFIGURED ALLOCATIONS
      {
        int iITEMNBR = 1;
        foreach (object objItemConfig in geoAGConfig.vectors)
        {
          geoItemConfig = objItemConfig as GenericObject;

          strGLACCTNBR = GetGLACCTNBRFromGEO(geoItemConfig.get("GL_nbr", new GenericObject()) as GenericObject);//BUG15952
          strDescription = geoItemConfig.get("description", "") as string;
          decimal decDistPercentage = (decimal)geoItemConfig.get("distribution_percentage", 1.00M);
          decimal decItemAmount = decTranAmount * decDistPercentage; //TODO ROUND IT?

          GenericObject geoITEM = c_CASL.c_instance("TranSuite_DBAPI.TG_ITEM_DATA");
          geoITEMs.insert(geoITEM);
          geoITEM.set("ITEMNBR", iITEMNBR);
          geoITEM.set("AMOUNT", decItemAmount);
          geoITEM.set("TOTAL", decItemAmount);
          geoITEM.set("QTY", 1);
          geoITEM.set("GLACCTNBR", strGLACCTNBR);
          geoITEM.set("ITEMDESC", strDescription);
          geoITEM.set("ACCTID", "");
          geoITEM.set("ITEMACCTID", "");
          geoITEM.set("ITEMID", "0");
          geoITEM.set("TAXED", "FALSE");
          iITEMNBR = iITEMNBR + 1;
        }
      }
      #endregion

      return geoTRAN;
    }

    // Bug 25664 MJO - Create TENDER and its dependent objects
    public static GenericObject CreateIpaymentTenderPhysics(String strUserID, String strDeptID,
      String strCoreFileID, String strSourceType, String strtenderid, GenericObject geoTender,
      GenericObject geoTranConfig, decimal dAmount, int tndrNbr)
    {
      // TG_TENDER_DATA
      string strTenderPath = string.Format("Business.Tender.of.\"{0}\"", strtenderid);
      GenericObject geoTenderConfig = c_CASL.get_GEO().get_path(strTenderPath, true, new GenericObject()) as GenericObject;

      // find bank id through transaction bank rule
      string strBANKACCTID = "";
      GenericObject geoBRConfig = geoTranConfig.get("bank_rule", new GenericObject()) as GenericObject;
      GenericObject geoBRItemsConfig = geoBRConfig.get("bank_rule_items", new GenericObject()) as GenericObject;
      foreach (Object objBRItemConfig in geoBRItemsConfig.vectors)
      {
        GenericObject geoBRItemConfig = objBRItemConfig as GenericObject;
        GenericObject geoTenderInBR = geoBRItemConfig.get("tenders") as GenericObject;
        if (geoTenderInBR.Contains(geoTenderConfig))
        {
          GenericObject geoBankConfig = geoBRItemConfig.get("bank_account", null) as GenericObject;
          if (geoBankConfig != null)
            strBANKACCTID = geoBankConfig.get("id", "") as string;
          break;
        }
      }

      GenericObject geoTENDER = c_CASL.c_instance("TranSuite_DBAPI.TG_TENDER_DATA");
      // HANDLE TENDER FIELDS
      GenericObject geoTenderStdFields = new GenericObject();
      GenericObject geoTenderCustFields = new GenericObject();
      GetTenderFields(geoTenderStdFields, geoTenderCustFields, geoTenderConfig, geoTender);

      geoTENDER.set("AMOUNT", dAmount);
      geoTENDER.set("TNDRNBR", tndrNbr);
      geoTENDER.set("TNDRID", strtenderid);
      geoTENDER.set("TNDRDESC", geoTenderConfig.get("description", ""));
      geoTENDER.set("TYPEIND", geoTenderConfig.get("TYPEIND", "", true));
      geoTENDER.set("ADDRESS", geoTenderStdFields.get("ADDRESS", ""));
      geoTENDER.set("AUTHACTION", geoTenderStdFields.get("AUTHACTION", ""));
      geoTENDER.set("AUTHNBR", geoTenderStdFields.get("AUTHNBR", ""));
      geoTENDER.set("AUTHSTRING", geoTenderStdFields.get("AUTHSTRING", ""));
      geoTENDER.set("BANKACCTNBR", geoTenderStdFields.get("BANKACCTNBR", ""));
      geoTENDER.set("BANKROUTINGNBR", geoTenderStdFields.get("BANKROUTINGNBR", ""));
      geoTENDER.set("CC_CK_NBR", geoTenderStdFields.get("CC_CK_NBR", ""));
      geoTENDER.set("CCNAME", geoTenderStdFields.get("CCNAME", ""));
      geoTENDER.set("EXPMONTH", geoTenderStdFields.get("EXPMONTH", ""));
      geoTENDER.set("EXPYEAR", geoTenderStdFields.get("EXPYEAR", ""));
      geoTENDER.set("SYSTEXT", geoTenderStdFields.get("SYSTEXT", ""));
      geoTENDER.set("BR_BANK_ACCT_ID", strBANKACCTID);
      geoTENDER.set("SOURCE_DATE", "");
      geoTENDER.set("SOURCE_REFID", "");
      GenericObject geoTENDERCustomFields = c_CASL.c_GEO();
      geoTENDER.set("_custom_keys", geoTENDERCustomFields);
      // Bug 25434 MJO - Image support
      geoTENDER.set("images", geoTenderCustFields.get("images", new GenericObject()));
      // Handle Tender custom fields 
      IDictionaryEnumerator myEnumerator = geoTenderCustFields.GetEnumerator() as IDictionaryEnumerator;
      while (myEnumerator.MoveNext())
      {
        string strTCFName = myEnumerator.Key as string;
        string strTCFValue = "" + myEnumerator.Value;
        if (!strTCFName.StartsWith("_"))
        {
          string strField = string.Format("{0}_f_custom_field", strTCFName);
          //BUG16191 
          GenericObject CFConfig = null;
          if (geoTenderConfig.has(strField))
          {
            CFConfig = geoTenderConfig.get(strField, null) as GenericObject;

            geoTENDERCustomFields.insert(strTCFName);
            // [custom_field]=value
            geoTENDER.set(strTCFName, strTCFValue);
            // [custom_field]_f_id=..
            string strCFIDKey = string.Format("{0}_f_id", strTCFName);
            string strCFIDValue = CFConfig.get("id", "") as string;
            geoTENDER.set(strCFIDKey, "");
            // [custom_field]_f_label=..
            string strCFLabelKey = string.Format("{0}_f_label", strTCFName);
            string strCFLabelValue = CFConfig.get("label", "") as string;
            geoTENDER.set(strCFLabelKey, strCFLabelValue);
            // [custom_field]_f_store_in_DB=..
            string strCFStoreDBKey = string.Format("{0}_f_store_in_DB", strTCFName);
            object strCFStoreDBValue = CFConfig.get("store_in_DB", false);
            geoTENDER.set(strCFStoreDBKey, strCFStoreDBValue);

          }
          else if (strTCFName == "entry_method")
          {
            geoTENDERCustomFields.insert(strTCFName);
            geoTENDER.set(strTCFName, strTCFValue);
            // [custom_field]_f_id=..
            string strCFIDKey = string.Format("{0}_f_id", strTCFName);
            string strCFIDValue = strTCFName;
            geoTENDER.set(strCFIDKey, strCFIDValue);
            // [custom_field]_f_label=..
            string strCFLabelKey = string.Format("{0}_f_label", strTCFName);
            string strCFLabelValue = "";
            geoTENDER.set(strCFLabelKey, strCFLabelValue);
            // [custom_field]_f_store_in_DB=..
            string strCFStoreDBKey = string.Format("{0}_f_store_in_DB", strTCFName);
            object strCFStoreDBValue = true;
            geoTENDER.set(strCFStoreDBKey, strCFStoreDBValue);
          }
        }
      }
      // END of Handle Tender custom fields

      return geoTENDER;
    }

    /// <summary>
    /// seperate Tender Standard Fields and Custom Fields based on Tender Type configuration and TenderToImport Custom Fields
    /// </summary>
    /// <param name="geoTenderStdFields"></param>
    /// <param name="geoTenderCustFields"></param>
    /// <param name="geoTenderConfig"></param>
    /// <param name="ITender"></param>
    /// <returns></returns>
    public static string GetTenderFields(GenericObject geoTenderStdFields, GenericObject geoTenderCustFields, 
      GenericObject geoTenderConfig, GenericObject geoTender)
    {
      string strCCNAME_fname = "";
      string strCCNAME_lname = "";
      string strADDRESS_address = "";
      string strADDRESS_address2 = "";
      string strADDRESS_city = "";
      string strADDRESS_state = "";
      string strADDRESS_zip = "";
      string strADDRESS_country = "";

      foreach (string key in geoTender.getStringKeys())
      {
        object value = geoTender.get(key);
        if (key == "credit_card_nbr" || key == "check_nbr")
          geoTenderStdFields.set("CC_CK_NBR", value); //standard field: CC_CK_NBR TODO: mask it CC_CK_NBR 
        else if (key == "exp_month")
          geoTenderStdFields.set("EXPMONTH", value);//standard field: EXPMONTH
        else if (key == "exp_year")
          geoTenderStdFields.set("EXPYEAR", value);//standard field: EXPYEAR
        else if (key == "bank_routing_nbr")
          geoTenderStdFields.set("BANKROUTINGNBR", value);//standard field: BANKROUTINGNBR
        else if (key == "bank_account_nbr")
          geoTenderStdFields.set("BANKACCTNBR", value); //standard field: BANKACCTNBR TODO: mask it BANKACCTNBR 
        else if (key == "system_comments")
          geoTenderStdFields.set("SYSTEXT", value);//standard field: SYSTEXT
        else if (key == "auth_nbr")
          geoTenderStdFields.set("AUTHNBR", value);//standard field: AUTHNBR
        else if (key == "auth_str")
          geoTenderStdFields.set("AUTHSTRING", value);//standard field: AUTHSTRING
        else if (key == "auth_action")
          geoTenderStdFields.set("AUTHACTION", value);//standard field: AUTHACTION
        else if (key == "payer_fname")
          strCCNAME_fname = value.ToString();
        else if (key == "payer_lname")
          strCCNAME_lname = value.ToString();
        else if (key == "address")
          strADDRESS_address = value.ToString();
        else if (key == "address2")
          strADDRESS_address2 = value.ToString();
        else if (key == "city")
          strADDRESS_city = value.ToString();
        else if (key == "state")
          strADDRESS_state = value.ToString();
        else if (key == "zip")
          strADDRESS_zip = value.ToString();
        else if (key == "country")
          strADDRESS_country = value.ToString();
        else  //BUG16191
        // CUSTOM FIELD
        geoTenderCustFields.set(key, value);
      }
      //standard field: CCNAME
      string strCCNAME = (strCCNAME_fname + " " + strCCNAME_lname) as string;
      if (strCCNAME.Length > 40)
        strCCNAME = strCCNAME.Substring(0, 40);
      geoTenderStdFields.set("CCNAME", strCCNAME);
      //standard field: ADDRESS
      string strADDRESS =
            strADDRESS_address + " "
          + strADDRESS_address2 + " "
          + strADDRESS_city + " "
          + strADDRESS_state + " "
          + strADDRESS_zip + " "
          + strADDRESS_country;
      if (strADDRESS.Length > 100)
        strADDRESS = strADDRESS.Substring(0, 100);
      geoTenderStdFields.set("ADDRESS", strADDRESS);

      return "";
    }
    /// <summary>
    /// BUG15952
    /// </summary>
    /// <param name="geoGLACCTNBR"></param>
    /// <returns></returns>
    public static string GetGLACCTNBRFromGEO(GenericObject geoGLACCTNBR)
    {

      string strGLACCTNBR = "";
      ArrayList alGLACCTNBR = new ArrayList();
      GenericObject geoGLACCTNBRSEGMENTS = geoGLACCTNBR.get("segments", new GenericObject()) as GenericObject;
      foreach (object oSegments in geoGLACCTNBRSEGMENTS.vectors)
      {
        GenericObject geoSegments = oSegments as GenericObject;
        string strGLPart = geoSegments.get("chars", "") as string;
        string strPadding = "";
        int iPadLength = (int)geoSegments.get("max_size", 0);
        for (int i = strGLPart.Length; i < iPadLength; i++)
          strPadding += " ";
        strGLPart = strPadding + strGLPart;
        strGLACCTNBR = strGLACCTNBR + strGLPart;
      }
      return strGLACCTNBR;
    }

    public static object fCASL_StartServerSentEvents(CASL_Frame frame)
    {
      Point point = frame.args.get("DMS_Point") as Point;

      StartServerSentEvents(point);

      return null;
    }

    public static void StartServerSentEvents(Point point)
    {
      // handle_request_2 will make sure that the final output of a_server for the GATEWAYPOST request will not try to override the status-code/content-type set in this ServerSentEvent flow
      HttpResponse res = HttpContext.Current.Response;
      res.StatusCode = 200;
      res.ContentType = "text/event-stream";
      void sendEvent(string e)
      {
        if (!String.IsNullOrEmpty(e))
        {
          res.Write("event: status" + "\n");
          res.Write("data: " + e + "\n\n");
          res.Flush();
        }
        else
        {
          res.Write("event: status" + "\n");
          res.Write("data: NO STATUS\n\n");
          res.Flush();
        }
      }
      Progress<string> statusProgress = new Progress<string>(sendEvent);
      point.StartGetStatusLoop(statusProgress, 3000);
    }
    public static void FinishServerSentEvents(Point point, string jsonifiedPayload)
    {
      point.StopGetStatusLoop();
      
      HttpResponse res = HttpContext.Current.Response;
      // Bug IPAY-429 RDM - Fix error scenario where content-type doesn't get set
      if(res.ContentType != "text/event-stream")
      {
        res.StatusCode = 200;
        res.ContentType = "text/event-stream";
      }
      res.Write("event: response" + "\n");
      res.Write("data: " + jsonifiedPayload + "\n\n");
      res.Flush();
    }
  }
}