using System;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Web;
using System.Net;
using CASL_engine;
using System.Text.RegularExpressions;
using System.Text;
using System.Collections.Specialized;

namespace baseline
{
  public class XMLIntegration
  {

    private static XmlReaderSettings _validatingReader = CreateValidatorSettings();

    /// <summary>
    /// Process XML integration arguments into a GEO.
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static object query_xml_to_object(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      string query_string = geo_args.get("query_string").ToString();

      // UMN TODO - remove when fully debugged
      //Logger.cs_log(query_string);

      // return value of this method
      GenericObject geoReturn = c_CASL.c_GEO();
      geoReturn.Add("global", c_CASL.c_GEO());
      geoReturn.Add("transactions", c_CASL.c_GEO());
      geoReturn.Add("transaction_types", c_CASL.c_GEO());
      geoReturn.Add("tenders", c_CASL.c_GEO());
      geoReturn.Add("refunds", c_CASL.c_GEO());

      try
      {
        query_string = DecodeQueryString(query_string);
        bool isEpicHyperspaceRequest = query_string.Contains("epicsystems-com");
        bool isEpicXMLDevicesRequest = query_string.Contains("schemas.epic.com/xmldevices");

        var urlArgs = HttpUtility.ParseQueryString(query_string);

        // Process globals
        foreach (string key in urlArgs.AllKeys)
        {
          string value = urlArgs[key];
          if (key.StartsWith("global.") && !String.IsNullOrEmpty(value))
          {
            ((GenericObject)geoReturn["global"]).Add(key.Replace("global.", ""), value);
          }
        }

        // Bug 23135 UMN if urlArgs has a req, it came through an HTTP POST, so might need to decode it
        if (urlArgs["req"] != null)
        {
          query_string = DecodeQueryString(urlArgs["req"]);
        }
        // Preserve the original request (it's in the req arg of the query string) so can
        // later write to transfer table
        geoReturn.Add("xml_req", query_string);

        // From Epic's Hyperspace POS API or XML Devices API
        if (!query_string.Contains("urn:corebt-com:iPayment.TransferAPI"))
        {
          // transform Epic's request to something we can use
          query_string = TransformEpicRequestToCoreRequest(query_string, urlArgs);
        }

        // UMN TODO - remove when fully debugged
        Logger.cs_log(query_string);

        // Set up the XSD schema to use for validation
        ParseXMLUsingXSD(query_string, geoReturn);
      }
      catch (Exception e)
      {
        string message = e.ToMessageAndCompleteStacktrace();

        Logger.cs_log("Error in query_xml_to_object: " + message);

        // Bug #8349 Mike O - Don't show error details when show_verbose_error is turned off
        if ((bool)c_CASL.GEO.get("show_verbose_error", false))
        {
          throw CASL_error.create("title", "Server invalid Integration API request", "message", message, "code", "SV-322");
        }
        else
        {
          throw CASL_error.create("title", "Server invalid Integration API request", "code", "SV-322");
        }
      }

      return geoReturn;
    }

    /// <summary>
    /// Decode the query string depending on how it was encoded
    /// </summary>
    /// <param name="query_string"></param>
    /// <returns></returns>
    private static string DecodeQueryString(string query_string)
    {
      if (query_string.StartsWith("%3C")) // It's been UrlEncoded last
      {
        query_string = WebUtility.UrlDecode(query_string);
        query_string = WebUtility.HtmlDecode(query_string);
      }
      else if (query_string.StartsWith("&lt;")) // It's been HtmlEncoded last
      {
        query_string = WebUtility.HtmlDecode(query_string);
        query_string = WebUtility.UrlDecode(query_string);
      }
      else if (query_string.StartsWith("%26lt%3B")) // It's been HtmlEncoded then UrlEncoded
      {
        query_string = WebUtility.HtmlDecode(WebUtility.UrlDecode(query_string));
      }
      return query_string;
    }

    /// <summary>
    /// Parse the XML using XSD to validate
    /// </summary>
    /// <param name="query_string"></param>
    /// <param name="geoReturn"></param>
    private static void ParseXMLUsingXSD(string query_string, GenericObject geoReturn)
    {
      // Deal with data at the root level is invalid
      string byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
      if (query_string.StartsWith(byteOrderMarkUtf8, StringComparison.Ordinal))
        query_string = query_string.Remove(0, byteOrderMarkUtf8.Length);

      // Bug #11692 Mike O - Dispose of reader when done
      using (StringReader stringReader = new StringReader(query_string))
      {
        using (XmlReader inputReader = XmlReader.Create(stringReader, _validatingReader))
        {
          // Load the input for reading using XPath
          XPathDocument inputDoc = new XPathDocument(inputReader);
          XPathNavigator nav = inputDoc.CreateNavigator();

          XmlNamespaceManager manager = new XmlNamespaceManager(nav.NameTable);
          manager.AddNamespace("cbt", "urn:corebt-com:iPayment.TransferAPI");

          // Start parsing the nodes of the navigator
          parseSystemTag(nav.SelectSingleNode("/cbt:request/cbt:system", manager), geoReturn);
          parseEventTag(nav, manager, "/cbt:request/cbt:event",
            geoReturn.get("global") as GenericObject,
            geoReturn.get("transaction_types") as GenericObject);

          parseTransactions(nav, manager, "/cbt:request/cbt:transactions/cbt:transaction",
              geoReturn.get("transactions") as GenericObject,
              geoReturn.get("transaction_types") as GenericObject);

          parseContainers(nav, manager, "/cbt:request/cbt:transactions/cbt:container",
              geoReturn.get("transactions") as GenericObject,
              geoReturn.get("transaction_types") as GenericObject);

          XPathNodeIterator tenderNodeIterator = nav.Select("/cbt:request/cbt:tenders/cbt:tender", manager);
          while (tenderNodeIterator.MoveNext())
            parseTenderTag(tenderNodeIterator.Current, geoReturn.get("tenders") as GenericObject);

          XPathNodeIterator refundNodeIterator = nav.Select("/cbt:request/cbt:refunds/cbt:refund", manager);
          while (refundNodeIterator.MoveNext())
            parseRefundTag(refundNodeIterator.Current, geoReturn.get("refunds") as GenericObject);
        }
      }
    }

    // Regex for changing Amount to negative for refunds
    private static readonly Regex amtRgx = new Regex(@"<Amount[^>]*>([^<]*)</Amount>", RegexOptions.Compiled);

    private static string TransformEpicRequestToCoreRequest(string req, NameValueCollection urlArgs)
    {
      bool isEpicHyperspaceRequest = req.Contains("epicsystems-com");
      bool isEpicXMLDevicesRequest = req.Contains("schemas.epic.com/xmldevices");

      // transform Epic's request to something we can use
      if (isEpicHyperspaceRequest)
      {
        req = TransformEpicInternal(req, "/Epic2Core.xsl", urlArgs);
      }
      else if (isEpicXMLDevicesRequest)
      {
        req = TransformEpicInternal(req, "/EpicXMLDevices2Core.xsl", urlArgs);
      }
      return req;
    }

    /// <summary>
    /// Transform Epic any Epic request to an XML integration API request
    /// </summary>
    /// <param name="msg"></param>
    /// <param name="xstName">Name of XSLT file</param>
    /// <param name="urlArgs"></param>
    /// <returns></returns>
    private static string TransformEpicInternal(string msg, string xstName, NameValueCollection urlArgs)
    {
      string iPaymentRequest = String.Empty;

      if (msg.Contains("POSGiveRefundRequest"))
      {
        msg = amtRgx.Replace(msg, "<Amount>-$1</Amount>");
      }

      using (StringReader strReader = new StringReader(msg))
      {
        using (XmlReader xmlReader = XmlReader.Create(strReader))
        {
          XslCompiledTransform xslt = new XslCompiledTransform();
          xslt.Load(c_CASL.code_base + xstName);
          using (StringWriter strWriter = new StringWriter())
          using (XmlWriter xmlWriter = XmlWriter.Create(strWriter))
          {
            xslt.Transform(xmlReader, xmlWriter);
            iPaymentRequest = strWriter.ToString();
          }
        }
      }

      // replace the variables that the xsl puts in
      iPaymentRequest = iPaymentRequest
        .Replace("xmlns:epic=\"urn:epicsystems-com:ResolutePB.2004-12.Services.PointOfSaleInterface\"", "xmlns=\"urn:corebt-com:iPayment.TransferAPI\"")
        .Replace("xmlns:epic=\"http://schemas.epic.com/xmldevices\"", "xmlns=\"urn:corebt-com:iPayment.TransferAPI\"")
        .Replace(" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\"", "")
        .Replace("$user_id", urlArgs["user_id"])
        .Replace("$dept_id", urlArgs["department_id"])
        .Replace("$transfer_user_id", urlArgs["transfer_user_id"])
        .Replace("$transfer_dept_id", urlArgs["transfer_location_id"])
        .Replace("$ctt_id", urlArgs["ctt_id"])
        .Replace("$group_name", urlArgs["group_name"])
        .Replace("$ext_ref_id", urlArgs["global.ext_ref_id"]);

      return iPaymentRequest;
    }

    // Private helper functions for query_xml_to_object
    private static void parseSystemTag(XPathNavigator systemNode, GenericObject geoReturn)
    {
      if (systemNode != null && systemNode.MoveToFirstChild())
      {
        do
        {
          switch (systemNode.LocalName)
          {
            case "user_id":
              string uidType = systemNode.GetAttribute("type", String.Empty).Trim();
              if (!String.IsNullOrEmpty(uidType) && uidType == "transfer")
                geoReturn.Add("transfer_user_id", systemNode.Value.Trim());
              geoReturn.Add("user_id", systemNode.Value.Trim());
              break;

            case "user_pwd": geoReturn.Add("user_pwd", systemNode.Value.Trim()); break;
            case "auth_token": geoReturn.Add("auth_token", systemNode.Value.Trim()); break;

            case "department_id":
            case "workgroup_id":
              string locType = systemNode.GetAttribute("type", String.Empty).Trim();
              if (!String.IsNullOrEmpty(locType) && locType == "transfer")
                geoReturn.Add("transfer_location_id", systemNode.Value.Trim());
              geoReturn.Add("department_id", systemNode.Value.Trim());
              break;

            case "finished_buying": geoReturn.Add("finished_buying", systemNode.ValueAsBoolean); break;
            case "auto_return": geoReturn.Add("auto_return", systemNode.Value); break;
            case "auto_exit": geoReturn.Add("auto_exit", systemNode.Value); break;
            case "show_ui": geoReturn.Add("show_ui", systemNode.ValueAsInt); break;
            case "ui_mode": geoReturn.Add("ui_mode", systemNode.Value.Trim()); break;
          }
        } while (systemNode.MoveToNext());
      }
    }

    private static void parseIdTag(XPathNavigator tranNode, GenericObject geoReturn)
    {
      if (tranNode.MoveToFirstChild())
      {
        do
        {
          switch (tranNode.LocalName)
          {
            case "id": geoReturn.Add("id", tranNode.ValueAsInt); break;
          }
        } while (tranNode.MoveToNext());
      }
    }

    private static void parseEventTag(XPathNavigator nav, XmlNamespaceManager manager, String path, GenericObject current, GenericObject transaction_types)
    {
      var node = nav.SelectSingleNode(path, manager);
      if (node != null)
        parseFields(node, manager, current, transaction_types);
    }

    /// <summary>
    /// Parse field elements into the current generic object
    /// </summary>
    /// <param name="elementNode"></param>
    /// <param name="current"></param>
    /// <param name="transaction_types"></param>
    private static void parseFields(XPathNavigator nav, XmlNamespaceManager manager, GenericObject current, GenericObject transaction_types)
    {
      XPathNodeIterator fieldIter = nav.Select("cbt:field", manager);
      foreach (XPathNavigator field in fieldIter)
      {
        string key = field.GetAttribute("key", String.Empty).Trim();
        string value = field.Value.Trim();

        switch (key)
        {
          case "tran_type":
            current.Add(key, value);
            if (!transaction_types.Contains(value)) transaction_types.Add(value, true);
            break;

          case "bank_rule":
            current.Add("BankRuleID", value);
            break;

          case "id":
            current.Add("ID", value);
            break;

          case "amount":
          case "total":
            decimal dAmt = 0.0M;
            if (decimal.TryParse(value, out dAmt))
              current.Add(key, dAmt);
            else
              current.Add(key, value);  // Let CASL try to deal with it
            break;

          case "quantity":
            int iAmt = 0;
            if (int.TryParse(value, out iAmt))
              current.Add(key, iAmt);
            else
              current.Add(key, value);  // Let CASL try to deal with it
            break;

          default:
            if (value.Equals("true"))
              current[key] = true;
            else if (value.Equals("false"))
              current[key] = false;
            else
              current[key] = value.Replace("\"", "");
            break;
        }
      }
    }

    /// <summary>
    /// Parse a transaction tag.
    /// </summary>
    /// <param name="nav"></param>
    /// <param name="path"></param>
    /// <param name="tranNode"></param>
    /// <param name="parent"></param>
    /// <param name="transaction_types"></param>
    private static void parseTransactions(XPathNavigator nav, XmlNamespaceManager manager, String path, GenericObject parent, GenericObject transaction_types)
    {
      XPathNodeIterator nodeIter = nav.Select(path, manager);
      foreach (XPathNavigator tran in nodeIter)
      {
        XPathNavigator copy = tran.Clone();
        GenericObject aTran = c_CASL.c_GEO();
        parseIdTag(copy, aTran);
        parseFields(tran, manager, aTran, transaction_types);
        parent.insert(aTran);
      }
    }

    /// <summary>
    /// Parse a container tag.
    /// </summary>
    /// <param name="nav"></param>
    /// <param name="path"></param>
    /// <param name="containerNode"></param>
    /// <param name="parent"></param>
    /// <param name="transaction_types"></param>
    private static void parseContainers(XPathNavigator nav, XmlNamespaceManager manager, String path, GenericObject parent, GenericObject transaction_types)
    {
      XPathNodeIterator nodeIter = nav.Select(path, manager);
      foreach (XPathNavigator cont in nodeIter)
      {
        XPathNavigator copy = cont.Clone();
        GenericObject aTran = c_CASL.c_GEO();
        parseIdTag(copy, aTran);
        aTran.Add("_name", "DetailData");
        parseFields(cont, manager, aTran, transaction_types);
        parseGroups(cont, manager, aTran, transaction_types);
        parent.insert(aTran);
      }
    }

    private static void parseGroups(XPathNavigator nav, XmlNamespaceManager manager, GenericObject parent, GenericObject transaction_types)
    {
      XPathNodeIterator groupIter = nav.Select("cbt:group", manager);
      foreach (XPathNavigator group in groupIter)
      {
        GenericObject aGroup = c_CASL.c_GEO();
        aGroup.Add("_name", "Group");
        parseFields(group, manager, aGroup, transaction_types);
        parseItems(group, manager, aGroup, transaction_types);
        parent.insert(aGroup);
      }
    }

    private static void parseItems(XPathNavigator nav, XmlNamespaceManager manager, GenericObject parent, GenericObject transaction_types)
    {
      XPathNodeIterator itemIter = nav.Select("cbt:item", manager);
      foreach (XPathNavigator item in itemIter)
      {
        GenericObject anItem = c_CASL.c_GEO();
        anItem.Add("_name", "DetailLine");
        parseFields(item, manager, anItem, transaction_types);
        parent.insert(anItem);
      }
    }

    private static void parseTenderTag(XPathNavigator tenderNode, GenericObject tenders)
    {
      tenderNode.MoveToFirstAttribute();
      string value = tenderNode.Value.Trim();
      if (!tenders.Contains(value)) tenders.Add(value, true);
    }

    private static void parseRefundTag(XPathNavigator refundNode, GenericObject refunds)
    {
      refundNode.MoveToFirstAttribute();
      string value = refundNode.Value.Trim();
      if (!refunds.Contains(value)) refunds.Add(value, true);
    }

    /// <summary>
    /// Create the validator settings once, and reuse on every request. If you change the XSD,
    /// you'll need to reload the site!
    /// </summary>
    /// <returns></returns>
    private static XmlReaderSettings CreateValidatorSettings()
    {
      XmlReaderSettings settings = new XmlReaderSettings();
      XmlSchemaSet schemaSet = new XmlSchemaSet();
      schemaSet.Add("urn:corebt-com:iPayment.TransferAPI", c_CASL.code_base + "/IntegrationAPI.xsd");
      schemaSet.Compile();
      settings.Schemas.Add(schemaSet);
      settings.ValidationType = ValidationType.Schema;
      settings.ValidationFlags = XmlSchemaValidationFlags.ReportValidationWarnings
        | XmlSchemaValidationFlags.ProcessIdentityConstraints;
      settings.IgnoreComments = settings.IgnoreWhitespace = settings.IgnoreProcessingInstructions = true;

      return settings;
    }
  }
}
