﻿using System;
using System.Data;
using System.Text;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using CASL_engine;
using System.IO;
using System.Linq;

using System.Web;
using System.Web.Configuration;
using System.Collections.Specialized;

using System.Text.RegularExpressions;

using System.Data.SqlClient;
using System.Data.SqlTypes;

using TranSuiteServices;

namespace baseline
{
  /// <summary>
  /// C# Singleton version of the low-level Activity Log writes.
  /// Bug 13613.
  /// </summary>
  public class ActivityLog
  {
    // BUG 25978 RDM - Need way to set a cookie for the Safari iframe issue. Putting on Activity log class after discussion with Uttam/M.Okola. 
    public static Object SetTestCookie(CASL_Frame args)
    {
      var cookie = CreateSecureCookie("ipaymenttestcookie", "1", HttpContext.Current.Request.IsSecureConnection);
      HttpContext.Current.Response.Cookies.Add(cookie);
      return null;
    }
    
    // BUG 25978 RDM - Need way to verify the test cookie for the Safari iframe issue. Putting on Activity log class after discussion with Uttam/M.Okola. 
    public static Object VerifyTestCookie(CASL_Frame args)
    {
      var testCookie = HttpContext.Current.Request.Cookies.Get("ipaymenttestcookie");
      return (testCookie != null);
    }

    private static HttpCookie CreateSecureCookie(string name, string value, bool SecureCookies)
    {
      var cookie = new HttpCookie(name, value);
      cookie.HttpOnly = true;                // Bug 17062 UMN PCI-DSS make cookie not accessible in Javascript

      // Bug 27175 MJO - Set SameSite for HTTPS only
      if (SecureCookies)
      {
        cookie.Secure = true;         // Bug 16648 MJO PCI-DSS make cookie secure
        cookie.SameSite = SameSiteMode.None; // Bug 26151 RDM Add SameSite=None to all cookies.
      }

      return cookie;
    }

    // Bug 15971: Lock object for delayed instantiation (double-check locking)
    private static object syncRoot = new Object();

    /// <summary>
    /// Bug 15971: Static initializate will NOT work;
    /// it can cause errors when it is run on system startup and 
    /// the config database is not up yet so I HAVE to use double-checked
    /// locking.  Note the volatile keyword.
    /// </summary>
    private static volatile ActivityLog instance = null;       // Bug 15971. Changed to delay until actual write called for.

    /// <summary>
    /// TG_ACTLOG_DATA Field sizes and their defaults if we cannot find them in the database schema.
    /// </summary>
    private const int ARGS_LEN_MIN = 50;
    private const int DETAIL_LEN_MIN = 100;

    // IPAY-583: Set defaults in case we cannot get the metadata.  Updated to current database settings
    private int sessionIdLen = 25;       // IPAY-583
    private int useridLen = 20;       // Bug 16945
    private int appLen = 500;
    private int subjectLen = 500;
    private int actionNameLen = 500;
    private int argsLen = ARGS_LEN_MIN;
    private int summaryLen = 1000;
    private int detailLen = DETAIL_LEN_MIN;
    // End IPAY-583

    /// <summary>
    /// Flag to allow us to filter out logins and logout.
    /// Bug 16870.
    /// </summary>
    private bool m_filterLoginLogout = false;

    /// <summary>
    /// Buffer write.  This is currently turned off (by setting QUEUE_MAX to 1)
    /// because we are worried that if there is a crash, buffered entries would be
    /// lost thus making it difficult to diagnose the crash.
    /// Note that this is not thread safe either so let's disable it for now (bug 15767)
    /// consider using this when we get to .NET 4.0 and use a concurrent queue.
    /// </summary>
    //private Queue<LogRecord> recordQueue = new Queue<LogRecord>();

    /// <summary>
    /// Turn off buffering for now.  When we move to .NET 4.0, switch to 
    /// something like a concurrent blocking queue so that writeRecord()
    /// can just write to the queue without having to wait for the database 
    /// write and have a separate thread read the queue and do the database writes
    /// (possibly batched).  This background thread would block while waiting
    /// for records to be written to the queue.
    /// </summary>
    private const int QUEUE_MAX = 1;

    private static string m_postDateFormat = "G";

    /// <summary>
    /// Toggles whether to sandwich search terms in %.
    /// If off, search terms such as Config% on Action column 
    /// will be much faster if there an index on the Action column.
    /// Bug 16915
    /// </summary>
    private static bool m_wildcardQueries = true;

    private static bool m_legacyDatabase;
    private static bool m_sqlServer2012;

    private ActivityLog()
    {
      getActivityLogTableFieldSizes();

      readActivityLogWebConfigSettings();

      readDatabaseVersion();

      readConfiguation();
    }

    /// <summary>
    /// Ping the database to get the version number. 
    /// It should return a response such as 11.0.3128.0.
    /// Anything over 11.... should be SQL Server 2012.
    /// If it is 2012, then we can use OFFSET/FETCH.
    /// Bug 14247. FT. Activity log should be compatible with legacy databases
    /// </summary>
    private void readDatabaseVersion()
    {
      String dataReaderSQL = "SELECT SERVERPROPERTY('productversion')";

      SqlConnection dataReaderConnection = new SqlConnection();
      SqlDataReader dataReader = null;
      try
      {
        GenericObject dbConnection = c_CASL.c_object("TranSuite_DB.Data.db_connection_info") as GenericObject;
        String conString = (String)dbConnection.get("db_connection_string");

        // Open a connection for the DataReader
        dataReaderConnection.ConnectionString = conString;
        dataReaderConnection.Open();

        SqlCommand dataReaderCommand = new SqlCommand(dataReaderSQL, dataReaderConnection);
        dataReader = dataReaderCommand.ExecuteReader();

        if (!dataReader.Read())
        {
          string msg = "readDatabaseVersion: This query did not return a result: " + dataReaderSQL;
          Logger.LogInfo(msg, "Activity Logging");
          m_sqlServer2012 = false;
          return;
        }

        string productversion = dataReader.GetString(0);
        string[] versionParts = productversion.Split('.');
        int majorVersion = Convert.ToInt32(versionParts[0]);
        if (majorVersion >= 11)
          m_sqlServer2012 = true;
        else
          m_sqlServer2012 = false;
      }
      catch (Exception e)
      {
        // Bug 14248
        string baseMsg = "readDatabaseVersion: Cannot run or parse this query: " + dataReaderSQL + " Error: ";
        Logger.LogError(baseMsg + e.Message, "Activity Logging"); 
        Logger.cs_log(baseMsg + e.ToMessageAndCompleteStacktrace());      // Bug 16992 // Bug 17849
        m_sqlServer2012 = false;
        // End 14248
      }
      finally
      {
        if (dataReader != null && !dataReader.IsClosed)
          dataReader.Close();
        dataReaderConnection.Close();
      }
    }    


    /// <summary>
    /// Allow for alternate date format in the Activity log output.
    /// For instance, MM/dd/yyyy hh:mm:ss.fff tt should give the milliseconds.
    /// </summary>
    private static void readConfiguation()
    {
      object business_misc_data_object = c_CASL.c_object("Business.Misc.data");
      if (!(business_misc_data_object is GenericObject))
      {
        Logger.LogInfo("Activity Log.readConfiguation(): Cannot read Business.Misc.data", "Activity Logging");
        m_postDateFormat = "G";
        m_legacyDatabase = true;
        m_sqlServer2012 = false;
        return;
      }
      // Do the expected cast
      GenericObject business_misc_data = business_misc_data_object as GenericObject;

      // Get the date format; default to .NET "G" for standard timestamp
      m_postDateFormat = business_misc_data.get("activity_log_date_format", "G") as string;

      // Bug 16915
      m_wildcardQueries = Convert.ToBoolean(business_misc_data.get("activity_log_wildcard_all_queries", true));

    }

    /// <summary>
    /// Read the activity_log_args_length and activity_log_detail_length from Web.config
    /// for values to limit the size these fields.  If these values are less than the 
    /// minimum (the pre-13613 value) or larger than the actual varchar(n) fields
    /// size in the database, then this config value will be ignored.
    /// </summary>
    private void readActivityLogWebConfigSettings()
    {
      // Pull the Key/Value settings from the Web.config.
      NameValueCollection appSettings = WebConfigurationManager.AppSettings;

      // Bug 16870.  Filter out logins and logouts
      string configuredFilterLoginLogout = appSettings.Get("activity_log_filter_login_logout");
      if (configuredFilterLoginLogout != null && configuredFilterLoginLogout != "")
      {
        try
        {
          m_filterLoginLogout = Convert.ToBoolean(configuredFilterLoginLogout);
        }
        catch
        {
          m_filterLoginLogout = false;
        }
      }
      // End Bug 16870

      string configuredArgsLength = appSettings.Get("activity_log_args_length");
      if (configuredArgsLength != null && configuredArgsLength != "")
      {
        try
        {
          int configuredArgsLengthInt = Convert.ToInt32(configuredArgsLength);
          // If the length is over the minumum and under the length that can fit in the db,
          // overwrite the global length and clip fields to this length to save space.
          if (configuredArgsLengthInt > ARGS_LEN_MIN && configuredArgsLengthInt < argsLen)
          {
            argsLen = configuredArgsLengthInt;
          }
        }
        catch
        {
          // Ignore non-numeric settings and use database ARGS length
        }
      }
      string configuredDetailLength = appSettings.Get("activity_log_detail_length");
      if (configuredDetailLength != null && configuredDetailLength != "")
      {
        try
        {
          int configuredDetailLengthInt = Convert.ToInt32(configuredDetailLength);

          // If the length is over the minumum and under the length that can fit in the db,
          // overwrite the global length and clip fields to this length to save space.
          if (configuredDetailLengthInt == 0)
          {
            // A length of zero means to not write the field at all.
            detailLen = 0;
          }
          else
          {
            if (configuredDetailLengthInt > DETAIL_LEN_MIN && configuredDetailLengthInt < detailLen)
            {
              detailLen = configuredDetailLengthInt;
            }
          }
        }
        catch
        {
          // Ignore non-numeric settings and use database DETAIL length
        }
      }
    }



    /// <summary>
    /// Simplified GetInstance to avoid double-check locking
    /// </summary>
    /// <returns></returns>
    public static ActivityLog GetInstance()
    {
      // Bug 15971.  Delay instantiation to here
      if (instance == null)
      {
        lock (syncRoot)
        {
          if (instance == null)
            instance = new ActivityLog();
        }
      }
      // End Bug 15971
      return instance;
    }

    /// <summary>
    /// Entry point to write activity log records.
    /// Currently only called from ActivityLogCasl which is called from security.casl
    /// </summary>
    /// <param name="conString"></param>
    /// <param name="session"></param>
    /// <param name="user"></param>
    /// <param name="subject"></param>
    /// <param name="app"></param>
    /// <param name="action"></param>
    /// <param name="argsString"></param>
    /// <param name="summary"></param>
    /// <param name="detail"></param>
    public void writeRecord(String conString,
      string session,
      string user,
      string subject,
      string app,
      string action,
      string argsString,
      string summary,
      string detail)
    {
      if (string.IsNullOrEmpty(session)) session = misc.get_masked_session_id(); // Bug 20435 UMN PCI-DSS

      // Bug 15767: Clear up any problems passed in by CASL
      if (session == null)    session = "";
      if (user == null)       user = "";
      if (subject == null)    subject = "";
      if (app == null)        app = "";
      if (action == null)     action = "";
      if (argsString == null) argsString = "";
      if (summary == null)    summary = "";
      if (detail == null)     detail = "";
      // End bug 15767


      // Filter out the cruft and massage the rest
      if (!filterLog(ref session, ref user, ref subject, ref app, ref action, ref argsString, ref summary, ref detail))
        return;

      // Buffer record
      LogRecord record = new LogRecord(session,
        clip(user, useridLen),            // Bug 16945
        clip(app, appLen),
        clip(subject, subjectLen),
        clip(action, actionNameLen),
        clip(argsString, argsLen),
        clip(summary, summaryLen),
        clip(detail, detailLen));
      //recordQueue.Enqueue(logRecord);     // Bug 15767.  Disable for now.  Consider using in .NET with a concurrent queue


      // Consider a lock here
      //if (recordQueue.Count >= QUEUE_MAX)
      //{
        try
        {

          if (conString == null || conString == "")
          {
            GenericObject dbConnection = (GenericObject)c_CASL.c_object("TranSuite_DB.Data.db_connection_info");
            if (dbConnection == null)
            {
              Logger.cs_log("writeRecord: activity log: cannot read the TranSuite_DB.Data.db_connection_info");
              return;
            }
            if (dbConnection.has("db_connection_string"))
              conString = (String)dbConnection.get("db_connection_string");
            else
            {
              Logger.cs_log("writeRecord: activity log: cannot read the db_connection_string from the TranSuite_DB.Data.db_connection_info");
              return;
            }
          }

          using (SqlConnection insertConnection = new SqlConnection())
          {
            // Open a connection for the update
            insertConnection.ConnectionString = conString;
            insertConnection.Open();

            // Prepare SqlCommand only once and repeat write multiple times....if we use buffering
            SqlCommand insertCommand = prepareSql(insertConnection);

            //while (recordQueue.Count > 0)
            //{
              //LogRecord record = recordQueue.Dequeue();
              insertCommand.Parameters[0].Value = record.timestamp;
              insertCommand.Parameters[1].Value = record.session;
              insertCommand.Parameters[2].Value = record.user;
              insertCommand.Parameters[3].Value = record.subject;
              insertCommand.Parameters[4].Value = record.app;
              insertCommand.Parameters[5].Value = record.action;
              insertCommand.Parameters[6].Value = record.argsString;
              //insertCommand.Parameters[7].Value = createSummary(record.subject, record.action, record.argsString, record.summary, first_section_list, tender_section_list);
              insertCommand.Parameters[7].Value = record.summary;
              insertCommand.Parameters[8].Value = record.detail;

              insertCommand.ExecuteNonQuery();
            //}
          }
        }
        catch (Exception e)
        {
          string baseMsg = "Error writing to the activity log: ";  // Bug 14248
          Logger.LogError(baseMsg + e.Message, "Activity Logging Write Record");
          Logger.cs_log(baseMsg + e.ToMessageAndCompleteStacktrace());      // Bug 16992 // Bug 17849 includes more detail info
      }
      //}
    }

    private bool filterLog(ref string session, ref string user, ref string subject, ref string app, ref string action, ref string argsString, ref string summary, ref string detail)
    {
      string logWhat = determineLogAction(action, summary);

      try
      {
        app = expandAppName(app, ref action, detail);

        switch (logWhat)
        {

          case "Logon":
            // Bug 16870
            if (m_filterLoginLogout)
              return false;

            // THIS IS REALLY REALLY DODGY!!! TODO
            if (summary == null || summary == String.Empty)
              return false;

            /*
            if (summary.Contains("Login failure"))
            {
              string[] actionParts = action.Split('.');
              action = "Login failure";
              app = getAppNameOnLoginFailure(app, actionParts);
              user = getUserNameOnLoginFailure(user, argsString);
              argsString = detail = "";   // Leave summary; it will be like this: "Login failure: No user with userId"
            }
            else
            {
              app = expandAppName(app, ref action, detail);

              user = getUserNameOnLogin(user, argsString);
              argsString = detail = "";
            }
             */
            return true;

          case "Logout":
            // Bug 16870
            if (m_filterLoginLogout)
              return false;
            // Detail has the URL but we do not need this yet.
            argsString = detail = subject = "";
            action = "Logout";
            return true;

          case "Ignore":
            return false;

          default:
            return true;    // Transaction, Tender, Open File, etc.
        }
      }
      catch (Exception e)
      {
        string baseMsg = "Error when parsing logging info: ";  // Bug 14248
        Logger.LogError(baseMsg + e.Message, "Activity Logging FilterLog");
        Logger.cs_log(baseMsg + e.ToMessageAndCompleteStacktrace());        // Bug 16992 // Bug 17849 includes more detail info
        return false;
      }
    }


    /// <summary>
    /// Filter out extraneous log actions.
    /// This was pruned as part of bug 14259.
    /// </summary>
    /// <param name="action"></param>
    /// <param name="summary"></param>
    /// <returns></returns>
    private string determineLogAction(string action, string summary)
    {
      if (action.EndsWith(".logout"))
        return "Logout";

      if (summary.StartsWith("LDAP"))
        return "LDAP";

      return action;
    }
    // End 14259.

    /// <summary>
    /// The App may say 
    /// </summary>
    /// <param name="app"></param>
    /// <param name="actionParts"></param>
    /// <returns></returns>
    private string getAppNameOnLoginFailure(string app, string[] actionParts)
    {
      if (app == "pos")
        app = "Cashiering";
      else if (app == "cbc")  // Never called!
        app = "Business Center";
      else if (app == "Config")
        app = "Config";
      else if (app == "Portal")
        app = "Admin";
      else if (app == "Authentication.Without_department")
        app = "Portal";


      return app;
    }


    private string getAppNameOnLogout(string app, ref string action)
    {
      string[] actionParts = action.Split('.');

      // Simplify the action
      action = "Logout";

      app = actionParts[1];
      if (app == "pos")
        app = "Cashiering";
      else if (app == "cbc")  // Not called if Exit button is used.  See 13735
        app = "Business Center";
      else if (app == "Config")
        app = "Config";
      else if (app == "Portal")
        //app = "Admin";
        app = "Portal";
      return app;
    }

    private string expandAppName(string app, ref string action, string detail)
    {
      if (app == "pos")
        return "Cashiering";
      else if (app == "cbc")
        return "Business Center";
      else if (app == "Portal")
        return app;
      else if (app == "Config")
        return app;

      // New it get hairy--will we EVER get here?????????  I hope not....
      string[] actionParts = action.Split('.');

      // Use the app if set
      string computedApp = "";
      if (detail.Contains("App:"))
      {
        Regex regexPattern = new Regex("App:(\\w+)");
        Match match = regexPattern.Match(detail);
        if (match.Success)
          return match.Groups[1].Value;
        else
          return app;
      }
      else if (app != null || app != String.Empty)
        computedApp = app;
      else
        computedApp = actionParts[1];

      if (computedApp == "pos")
        return "Cashiering";
      else if (computedApp == "cbc")
        return "Business Center";
      else if (computedApp == "Authentication")
        return "Portal";
      else if (computedApp == "Authentication.Without_department")
        return "Portal";
      return computedApp;
    }

    private static string getUserNameOnLoginFailure(string user, string argsString)
    {
      // userId="terrhwvsvsd"
      Regex regexPattern = new Regex("userId=\"(\\w+)\"");
      Match match = regexPattern.Match(argsString);
      if (match.Success)
      {
        String matchKey = match.Groups[1].Value;
        return matchKey;
      }
      return user;
    }
    private static string getUserNameOnLogin(string user, string argsString)
    {
      Regex regexPattern = new Regex("username=\"(\\w+)\"");
      Match match = regexPattern.Match(argsString);
      if (match.Success)
      {
        String matchKey = match.Groups[1].Value;
        return matchKey;
      }
      return user;
    }

    private static string createSummary(String subject, string action, string args, string summary,
      GenericObject first_section_list,
      GenericObject tender_section_list)
    {
      string prefix = "";

      try
      {
        ArrayList tenders = tender_section_list.vectors;
        foreach (GenericObject item in tenders)
        {
          if (item.has("description"))
            prefix += "tender_section_list: " + item.get("description") as string;
          else
          {
            prefix += "no high-level tender description";
            if (item.has("data_class"))
            {
              GenericObject data_class = item.get("data_class") as GenericObject;
              prefix += data_class.get("description", "no description");
            }
          }

        }
        ArrayList transactions = first_section_list.vectors;
        foreach (GenericObject item in transactions)
        {
          if (item.has("label"))
            prefix += "first_section_list: " + item.get("label") as string;
        }


        if (summary != null && summary.Length > 0)
          return prefix + summary;

        if (subject == null)
          return prefix + "Null Subject";

        if (!subject.Contains("."))
          return prefix + "Simple summary: " + subject;

        string[] subjectParts = subject.Split('.');

        GenericObject my = CASLInterpreter.get_my();
        if (my.has("login_department"))
        {
          GenericObject workgroup = my.get("a_department") as GenericObject;
          return prefix + "Found departments";
        }
        else
          return prefix + "No department";
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in createSummary", e);

        return prefix + "Cannot read the my session: " + e.ToString();

        
      }

      /*
      string workgroup = my.get("login_department") as String;

      string subjectStr = "";
      string subject = "";
      if (subjectParts.Length > 2) {
        subject = subjectParts[2];
        if (subject == "tender_section_list")
        {
          StringBuilder globalSearchClause = new StringBuilder();
          globalSearchClause.Append("Business.Department.of.").Append('"').Append(workgroup).Append('"');

          GenericObject tsl = ((GenericObject)c_CASL.c_object(globalSearchClause.ToString())).get("tender_section_list") as GenericObject;

          int tenderNbr = Convert.ToInt32(subjectParts[3]);

          GenericObject tender = tsl[tenderNbr] as GenericObject;

          string desc = tender.get("description", "un-named") as string;
          return desc;
        }
      }
      return "";
       * */
    }


    /// <summary>
    /// Prepare a parameterized INSERT call.
    /// </summary>
    /// <param name="conString"></param>
    /// <param name="insertConnection"></param>
    /// <returns></returns>
    private SqlCommand prepareSql(SqlConnection insertConnection)
    {
      String activityLogInsertSql =
         @"INSERT INTO TG_ACTLOG_DATA 
            (POSTDT, SESSION_ID, USERID, SUBJECT, APP, ACTION_NAME, ARGS, SUMMARY, DETAIL)
            VALUES (
              @timestamp, @session, @user, @subject, @app, @action, @argsString, @summary, @detail)";

      SqlCommand insertCommand = new SqlCommand(activityLogInsertSql, insertConnection);

      // Prepare the statement once and only once
      insertCommand.Prepare();

      // Create and setup parameters once and only once
      // but do not set their values here
      // IPAY-582: FT: Set the length of all string parameters to encourage reuse of query plans
      insertCommand.Parameters.Add(new SqlParameter("@timestamp", SqlDbType.DateTime));
      insertCommand.Parameters.Add(new SqlParameter("@session", SqlDbType.VarChar, sessionIdLen));
      insertCommand.Parameters.Add(new SqlParameter("@user", SqlDbType.VarChar, useridLen));
      insertCommand.Parameters.Add(new SqlParameter("@subject", SqlDbType.VarChar, subjectLen));
      insertCommand.Parameters.Add(new SqlParameter("@app", SqlDbType.VarChar, appLen));
      insertCommand.Parameters.Add(new SqlParameter("@action", SqlDbType.VarChar, actionNameLen));
      insertCommand.Parameters.Add(new SqlParameter("@argsString", SqlDbType.VarChar, argsLen));
      insertCommand.Parameters.Add(new SqlParameter("@summary", SqlDbType.VarChar, summaryLen));
      insertCommand.Parameters.Add(new SqlParameter("@detail", SqlDbType.VarChar, detailLen));
      return insertCommand;
    }


    /// <summary>
    /// Read the schema data (only once) for the activity log table and find the field sizes.
    /// This will allow us to resize the table without changing the code.
    /// </summary>
    /// <param name="accountIdLen"></param>
    /// <param name="refNumLen"></param>
    /// <param name="bankReferenceLen"></param>
    /// <param name="textLen"></param>
    private void getActivityLogTableFieldSizes()
    {
      // Use the quickest possible SQL query for all the columns
      String dataReaderSQL = "SELECT TOP(1) * FROM TG_ACTLOG_DATA";

      SqlConnection dataReaderConnection = new SqlConnection();
      SqlDataReader dataReader = null;
      try
      {
        GenericObject dbConnection = c_CASL.c_object("TranSuite_DB.Data.db_connection_info") as GenericObject;
        String conString = (String)dbConnection.get("db_connection_string");

        // Open a connection for the DataReader
        dataReaderConnection.ConnectionString = conString;
        dataReaderConnection.Open();

        SqlCommand dataReaderCommand = new SqlCommand(dataReaderSQL, dataReaderConnection);
        dataReader = dataReaderCommand.ExecuteReader();
        DataTable schema = dataReader.GetSchemaTable();
        bool serial_id = false;
        bool pkid = false;

        for (int i = 0; i < dataReader.FieldCount; i++)
        {
          string name = schema.Rows[i]["ColumnName"].ToString();
          if (System.String.Compare(name, "SESSION_ID", true) == 0)
            sessionIdLen = Convert.ToInt32(schema.Rows[i]["ColumnSize"]);    // IPAY-583: Get all string field sizes
          if (System.String.Compare(name, "USERID", true) == 0)
            useridLen = Convert.ToInt32(schema.Rows[i]["ColumnSize"]);    // Bug 16945
          if (System.String.Compare(name, "APP", true) == 0)
            appLen = Convert.ToInt32(schema.Rows[i]["ColumnSize"]);
          if (System.String.Compare(name, "SUBJECT", true) == 0)
            subjectLen = Convert.ToInt32(schema.Rows[i]["ColumnSize"]);
          if (System.String.Compare(name, "ACTION_NAME", true) == 0)
            actionNameLen = Convert.ToInt32(schema.Rows[i]["ColumnSize"]);
          if (System.String.Compare(name, "ARGS", true) == 0)
            argsLen = Convert.ToInt32(schema.Rows[i]["ColumnSize"]);
          if (System.String.Compare(name, "SUMMARY", true) == 0)
            summaryLen = Convert.ToInt32(schema.Rows[i]["ColumnSize"]);
          if (System.String.Compare(name, "DETAIL", true) == 0)
            detailLen = Convert.ToInt32(schema.Rows[i]["ColumnSize"]);
          if (System.String.Compare(name, "PKID", true) == 0)
            pkid = true;
          if (System.String.Compare(name, "SERIAL_ID", true) == 0)
            serial_id = true;
        }
        if (serial_id)
        {
          m_legacyDatabase = true;
          if (pkid)
            Logger.cs_log("ERROR: TG_ACTLOG_DATA has both SERIAL_ID and PKID");
        }
        else
        {
          m_legacyDatabase = false;
          if (!pkid)
            Logger.cs_log("ERROR: TG_ACTLOG_DATA has neither SERIAL_ID or PKID");
        }

      }
      // Bug 14685/14619: Do not let this exception bubble up through CASL which will cause the white screen of death
      catch (Exception e)
      {
        string baseMsg = "Cannot read activity log meta-data. Using default field sizes. Error: ";
        Logger.LogError(baseMsg + e.Message, "Activity Logging GetDetailRecord");
        Logger.cs_log(baseMsg + e.ToMessageAndCompleteStacktrace());        // Bug 16992 and bug 17849 which includes more detail info
      }  // End 14685
      finally
      {
        if (dataReader != null && !dataReader.IsClosed)
          dataReader.Close();
        dataReaderConnection.Close();
      }
    }

    /// <summary>
    /// Clip field sizes to the actual field size as determined by reading the schema.
    /// </summary>
    /// <param name="stringField"></param>
    /// <param name="fieldMax"></param>
    /// <returns></returns>
    private String clip(string stringField, int fieldMax)
    {
      if (stringField == null)
        return "";

      if (fieldMax == 0)
        return "";

      if (stringField.Length <= fieldMax)
        return stringField;

      return stringField.Substring(0, fieldMax - 1);
    }


    /// <summary>
    /// Note: TODO: change this to a straight Ajax call
    /// </summary>
    /// <param name="conString"></param>
    /// <param name="serial_id"></param>
    /// <param name="status"></param>
    /// <returns></returns>
    internal static GenericObject getDetailRecords(string conString, string serial_id, out bool status)
    {
      status = true;

      // Read the SERIAL_ID or PKID (depending on the Legacy DB config setting). Bug 14247.
      String actlogSql =
          @"SELECT SUBJECT, ARGS, DETAIL, SESSION_ID FROM TG_ACTLOG_DATA WHERE {0} = @pkid";
      string serialIdName = m_legacyDatabase == true ? "SERIAL_ID" : "PKID";
      actlogSql = string.Format(actlogSql, serialIdName);

      HybridDictionary sql_args = new HybridDictionary();
      sql_args["pkid"] = serial_id;



      GenericObject detailsRecord = new GenericObject();
      try
      {
        using (SqlConnection connection = new SqlConnection(conString))
        {
          connection.Open();

          using (SqlCommand command = ParamUtils.CreateParamCommand(connection, actlogSql, sql_args))      // Bug 17639: Used parameterized query
          {
            command.Connection = connection;
            command.CommandType = CommandType.Text;

            using (SqlDataReader dataReader = command.ExecuteReader())
            {
              // No need to read the results, just check to see if there is any results
              if (!dataReader.HasRows)
                throw new Exception("getDetailRecord: cannot find a record for PKID: " + serial_id);

              dataReader.Read();

              // Start off with the session
              string session = dataReader.GetString(3);
              detailsRecord.Add("Session", session);

              string detailField = dataReader.GetString(2);

              // This will match any dollar amount with a comma:
              string dollarCommaPattern = @"(\$(?:\d{1,3}\,)?\d{1,3}\,\d{1,3}\.\d\d)";

              Regex regexPattern = new Regex(dollarCommaPattern);

              // Prime the loop
              Match match = regexPattern.Match(detailField);
              // Loop throug all the big dollars
              while (match.Success)
              {
                String dollarAmount = match.Groups[1].Value;
                dollarAmount = dollarAmount.Replace(',', '|');

                detailField = regexPattern.Replace(detailField, dollarAmount, 1); // Just replace the first match
                match = regexPattern.Match(detailField);  // Look for any additional big dollar amounts
              }


              char[] charSeparators = new char[] { ',' };
              string[] details = detailField.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);

              foreach (string rawDetail in details)
              {
                string detail = rawDetail.Trim();
                if (detail.Contains(':'))
                {
                  string key = detail.Substring(0, detail.IndexOf(':'));
                  string value = detail.Substring(detail.IndexOf(':') + 1);
                  if (value.Length > 0)
                  {
                                        // Protect against duplicate keys
                                        if (detailsRecord.has(key))
                                        {
                                            string existingRecordValue = detailsRecord[key] as string;
                                            existingRecordValue = existingRecordValue + ',' + value.Replace('|', ',').Trim('"');
                                            detailsRecord[key] = existingRecordValue;
                                        }
                                        else
                                        {
                    detailsRecord.Add(key, value.Replace('|', ',').Trim('"'));   // Had to use | instead of , for delimiters so change back here
                }
                  }
                }
                else
                {
                  if (detailsRecord.has("Value"))
                  {
                    string value = detailsRecord.get("Value") as string;
                    value = value + ", " + detail;
                    detailsRecord.set("Value", value);
                  }
                  else
                    detailsRecord.Add("Value", detail);

                }
              }

              string subject = dataReader.GetString(0);
              if (subject != null && subject != "")
                detailsRecord.Add("Subject", subject);

              string args = dataReader.GetString(1);
              if (args != null && args != "")
                detailsRecord.Add("Args", args);

              return detailsRecord;
            }
          }
        }
      }
      catch (Exception e)
      {
        // Bug 14248
        string baseMsg = "Cannot read Activity Log: ";
        Logger.LogError(baseMsg + e.Message, "Activity Logging GetDetailRecord");
        Logger.cs_log(baseMsg + e.ToMessageAndCompleteStacktrace());      // Bug 16992; bug 17849 which includes more detail info
        detailsRecord.Add("Error", baseMsg);
        // End bug 14248
        return detailsRecord;
      }

    }

    /// <summary>
    /// As Dan noted, this can be improved by keeping a persistant connection
    /// and keeping the reader active between calls so then we can just crank
    /// forward with the data reader without re-fetching.
    /// </summary>
    /// <param name="iDisplayLength"></param>
    /// <param name="iDisplayStart"></param>
    /// <param name="iSortDir"></param>
    /// <param name="sSearch"></param>
    /// <param name="request"></param>
    /// <param name="iTotalDisplayRecords"></param>
    /// <param name="iTotalRecords"></param>
    /// <returns></returns>
    public static string getServerSideData(int iDisplayLength, int iDisplayStart, string iSortDir, string sSearch, HttpRequest request, out int iTotalDisplayRecords, out int iTotalRecords)
    {
      // Initialize first thing
      iTotalDisplayRecords = iTotalRecords = 0;
      string dataReaderSQL = "";

      // Pre-make the caret image call and re-use
      string strSiteStatic = c_CASL.c_object("GEO.site_static") as string;
      string caret = string.Format("<img src=\\\"{0}\\/baseline\\/business\\/images\\/arrow_right_alpha.gif\\\" onclick=\\\"toggle_details('activity_log_table', this, '{0}');\\\"/>", strSiteStatic);

      HybridDictionary sql_args = new HybridDictionary();     // Bug 18766: Parameterization

      SqlConnection dataReaderConnection = new SqlConnection();
      SqlDataReader dataReader = null;
      try
      {

      GenericObject dbConnection = c_CASL.c_object("TranSuite_DB.Data.db_connection_info") as GenericObject;
      String conString = (String)dbConnection.get("db_connection_string");


      string orderBy = buildOrderByClause(request);

      string whereClause = buildWhereClause(sSearch, request, sql_args);

      getBaseSQLQuery(iDisplayLength, iDisplayStart, orderBy, ref dataReaderSQL, whereClause);

      iTotalDisplayRecords = getTotalDisplayRecordCount(conString, whereClause, sql_args);  // Bug 14226: return table total record count for pagination controls

      iTotalRecords = getRecordCount(conString);

        // Open a connection for the DataReader
        dataReaderConnection.ConnectionString = conString;
        dataReaderConnection.Open();

        //SqlCommand dataReaderCommand = new SqlCommand(dataReaderSQL, dataReaderConnection);

        SqlCommand dataReaderCommand = ParamUtils.CreateParamCommand(dataReaderConnection, dataReaderSQL, sql_args);

        dataReader = dataReaderCommand.ExecuteReader();

        StringBuilder jsArray = new StringBuilder();
        jsArray.Append('[');

        bool first = true;
        int recordCount = 0;

        // Read alternative date format if not already read
        if (m_postDateFormat == null)
          readConfiguation();

        while (dataReader.Read())
        {
          if (first)
            first = false;
          else
            jsArray.Append(',');  // Prepend trailing comma

          jsArray.Append('{');

          jsArray.Append('"').Append(0).Append('"').Append(": ").Append('"').Append(caret).Append('"').Append(",");   // Pre-load the caret here
          DateTime postDt = dataReader.GetDateTime(0);
          jsArray.Append('"').Append(1).Append('"').Append(": ").Append('"').Append(postDt.ToString(m_postDateFormat)).Append('"').Append(",");
          jsArray.Append('"').Append(2).Append('"').Append(": ").Append('"').Append(dataReader.GetString(1)).Append('"').Append(",");
          jsArray.Append('"').Append(3).Append('"').Append(": ").Append('"').Append(dataReader.GetString(2)).Append('"').Append(",");
          jsArray.Append('"').Append(4).Append('"').Append(": ").Append('"').Append(dataReader.GetString(3)).Append('"').Append(",");
          jsArray.Append('"').Append(5).Append('"').Append(": ").Append('"').Append(dataReader.GetString(4)).Append('"').Append(",");
          jsArray.Append('"').Append(6).Append('"').Append(": ").Append('"').Append(dataReader.GetString(7)).Append('"').Append(",");   // Optional Session
          int serial_id = dataReader.GetInt32(6);
          jsArray.Append('"').Append("DT_RowId").Append('"').Append(": ").Append('"').Append(serial_id).Append('"');

          jsArray.Append('}');
          recordCount++;
        }
        jsArray.Append(']');
        return jsArray.ToString();
      }
      catch (Exception e)
      {
        // Bug 14248: return a descriptive error message
        string baseMsg = "Cannot read Activity Log data: with this SQL: " + dataReaderSQL + ' ';
        Logger.LogError(baseMsg + e.Message, "Activity Logging GetServerSideData");
        Logger.cs_log(baseMsg + e.ToMessageAndCompleteStacktrace());      // 16992; bug 17849 which includes more detail info
        Logger.cs_log("sql_args:");
        foreach (object argsKey in sql_args.Keys)
        {
          Logger.cs_log("Key: " + argsKey + " Value: " + sql_args[argsKey]);
        }


        // Just return a single error record
        iTotalDisplayRecords = 1;

        StringBuilder jsArray = new StringBuilder();
        jsArray.Append('[');
        jsArray.Append('{');

        jsArray.Append('"').Append(0).Append('"').Append(": ").Append('"').Append(caret).Append('"').Append(",");   // Pre-load the caret here
        DateTime postDt = DateTime.Now;
        jsArray.Append('"').Append(1).Append('"').Append(": ").Append('"').Append(postDt.ToString(m_postDateFormat)).Append('"').Append(",");
        jsArray.Append('"').Append(2).Append('"').Append(": ").Append('"').Append("ERROR").Append('"').Append(",");
        jsArray.Append('"').Append(3).Append('"').Append(": ").Append('"').Append("Cannot read Activity Log data").Append('"').Append(",");
        jsArray.Append('"').Append(4).Append('"').Append(": ").Append('"').Append(e.Message).Append('"').Append(",");
        jsArray.Append('"').Append(5).Append('"').Append(": ").Append('"').Append("See error log for details").Append('"').Append(",");
        jsArray.Append('"').Append(6).Append('"').Append(": ").Append('"').Append("See error log for details").Append('"').Append(",");   // Optional Session
        jsArray.Append('"').Append("DT_RowId").Append('"').Append(": ").Append('"').Append(0).Append('"');

        jsArray.Append('}');
        jsArray.Append(']');
        return jsArray.ToString();
        // End 14248
      }
      finally
      {
        if (dataReader != null && !dataReader.IsClosed)
          dataReader.Close();
        dataReaderConnection.Close();
      }
    }


    /// <summary>
    /// Build up the base SQL query based on a whether the 
    /// we have detected SQL Server 2012.  If using SQL 2012 or later,
    /// use the OFFSET/FETCH.
    /// If not, use the ROW_NUMBER / TOP method.
    /// The ROW_NUMBER / TOP may require a full index scan.
    /// Use TOP for the first 4 pages and then use ROW_NUMBER for pages after that.
    /// Bug 14247: Activity log should be compatible with legacy databases.
    /// </summary>
    /// <param name="iDisplayLength"></param>
    /// <param name="iDisplayStart"></param>
    /// <param name="orderBy"></param>
    /// <param name="dataReaderSQL"></param>
    /// <param name="whereClause"></param>
    /// <returns></returns>
    private static void getBaseSQLQuery(
      int iDisplayLength, int iDisplayStart,
      string orderBy,
      ref string dataReaderSQL,
      string whereClause)
    {
      // Bug 14247: FT: Activity log should be compatible with legacy databases
      string serial_pkid = (m_legacyDatabase) ? "SERIAL_ID" : "PKID AS SERIAL_ID";


      // Use the most desirable technique (OFFSET/FETCH) if at SQL Server 2012
      if (m_sqlServer2012)
      {
        const string dataReaderSQL2012 =
          @"SELECT POSTDT, USERID, APP, ACTION_NAME, SUMMARY, DETAIL, {0}, SESSION_ID 
            FROM TG_ACTLOG_DATA 
            {1}
            {2}
            OFFSET {3} ROWS FETCH NEXT {4} ROWS ONLY";

        dataReaderSQL = String.Format(
          dataReaderSQL2012,
          serial_pkid,
          whereClause,
          orderBy,
            iDisplayStart,
            iDisplayLength);

        return;
      }
      // Else use the old hokey way of doing paging
      else
      {
        const string dataReaderSQLFormat = @"SELECT * FROM ( 
            SELECT POSTDT, USERID, APP, ACTION_NAME, SUMMARY, DETAIL, {4}, SESSION_ID, ROW_NUMBER() OVER ({3}) as row FROM TG_ACTLOG_DATA {0}
          ) row_select WHERE row_select.row BETWEEN {1} and {2}";
        dataReaderSQL = String.Format(
            dataReaderSQLFormat,
            whereClause,
            iDisplayStart,
            iDisplayStart + iDisplayLength,
            orderBy,
            serial_pkid);
      }
      return;
    }


    /// <summary>
    /// Return the total number of records in the table based on the where clause so that 
    /// the pagination controls in dataTables can display the proper numbers.
    /// Bug 14226.
    /// </summary>
    /// <param name="conString"></param>
    /// <param name="whereClause"></param>
    /// <returns></returns>
    private static int getTotalDisplayRecordCount(string conString, string whereClause, HybridDictionary sql_args)
    {
      string dataReaderSQL = @"SELECT COUNT(*) FROM TG_ACTLOG_DATA " + whereClause;
      //return getRecordCountInternal(conString, dataReaderSQL);

      using (SqlConnection connection = new SqlConnection(conString))
      {
        connection.Open();
        using (SqlCommand paramCommand = ParamUtils.CreateParamCommand(connection, dataReaderSQL, sql_args))
        {
          var count = paramCommand.ExecuteScalar();
          return Convert.ToInt32(count);
        }
      }
    }


    // the first POSTDT is a dummy, unsearchable column column
    private static List<string> aColumns = new List<string> { "POSTDT", "POSTDT", "USERID", "APP", "ACTION_NAME", "SUMMARY", "SESSION_ID" };

    private static string buildOrderByClause(HttpRequest request)
    {

      int iSortingCols = int.Parse(request["iSortingCols"]);

      // If no sorting directive, return an empty ORDER BY clause
      if (iSortingCols == 0 || request["iSortCol_0"] == null || request["iSortCol_0"] == "")
        return "";

      StringBuilder sb = new StringBuilder();
      for (int i = 0; i < iSortingCols; i++)
      {
        int iSortCol = Convert.ToInt32(request["iSortCol_" + i]);  // Change to Parse<>
        string column = aColumns.ElementAt(iSortCol);
        string sSortDir = request["sSortDir_" + i];

        string direction = sSortDir == "asc" ? "ASC" : "DESC";
        sb.Append(' ').Append(column).Append(' ').Append(direction);
      }

      if (sb.Length == 0)
        return "";

      return " ORDER BY " + sb.ToString();
    }


    private static string buildWhereClause(string sSearch, HttpRequest request, HybridDictionary sql_args)
    {
      StringBuilder parameterizedSearchClause = new StringBuilder();

      // See if there is any column filtering first
      for (int i = 0; i < aColumns.Count; i++)
      {
        string paramName = "sSearch_" + i;
        string searchTerm = request[paramName];
        string columnName = aColumns.ElementAt(i);
        // TODO: need to add a test on bSearchable_ + i to make sure we can search on the column

        // Bug 14301 HX
        // hard code here if searchTerm is empty , set date range as today, 
        // because I initial today's date in acitivity log
        if (i == 1 && String.IsNullOrEmpty(searchTerm))
        {
          string sNow = DateTime.Today.ToString("MM/dd/yyyy");
          searchTerm = string.Format("{0}~{1}", sNow, sNow);
        }

        if (searchTerm != null && searchTerm != "")
        {
          // OK. Found a column to search on
          if (parameterizedSearchClause.Length > 0)
          {
            parameterizedSearchClause.Append(" AND ");
          }
          if (searchTerm.Contains('~'))
            constructDateRangeQuery(ref parameterizedSearchClause, columnName, searchTerm, sql_args);
          else
          {
            parameterizedSearchClause.Append(' ').Append(columnName).Append(" LIKE ").Append('@').Append(columnName);
            // Bug 16915
            if (m_wildcardQueries)
              sql_args[columnName] = '%' + searchTerm + '%';
            else
              sql_args[columnName] = searchTerm;
          }
        }
      }

      // Let's see if there is no searching and bail out
      if ((sSearch == null || sSearch == "") && parameterizedSearchClause.Length == 0)
        return "";


      // If no global, then just do the column search and date range search
      if ((sSearch == null || sSearch == "") && parameterizedSearchClause.Length > 0)
        return " WHERE " + parameterizedSearchClause.ToString();

      
      // If there is a global search,  add this clause to the WHERE
      if (sSearch != null && sSearch != "")
      {
        sSearch = sqlInjectionWhitelist(sSearch);

        if (parameterizedSearchClause.Length > 0)
        {
          parameterizedSearchClause.Append(" AND ");
        }

        parameterizedSearchClause.Append('(')
          // Include the Detail first and session second since these are the most likely to be the searched data
        .Append(" DETAIL LIKE @global_detail ")      // Include DETAIL even if it not immediatly visible
        .Append(" OR SESSION_ID LIKE @global_sessionId ")
          //  These other predicates seems like they should not be included since they are availble via column filtering.  
        .Append(" OR USERID LIKE @global_userid")
        .Append(" OR APP LIKE @global_app")
        .Append(" OR ACTION_NAME LIKE @global_actionName")
        .Append(" OR SUMMARY LIKE @global_summary ")
          // End optional predicates
        .Append(')');

        // Bug 16915
        string wildcardSearch = sSearch;
        if (m_wildcardQueries)
          wildcardSearch = '%' + wildcardSearch + '%';      // Bug 16915
        sql_args["global_detail"] = wildcardSearch;
        sql_args["global_sessionId"] = wildcardSearch;
        sql_args["global_userid"] = wildcardSearch;
        sql_args["global_app"] = wildcardSearch;
        sql_args["global_actionName"] = wildcardSearch;
        sql_args["global_summary"] = wildcardSearch;
      }
       
      return " WHERE " + parameterizedSearchClause.ToString();
    }

    /// <summary>
    /// Whitelist acceptable SQL characters.  
    /// This is not ideal since the hyphen and ' characters are not allowed.
    /// TODO: This is not the best approach.  
    /// Change the queries to be parameterized and combine with this whitelist.
    /// Bug 16915.  Allowing limited regular expression proccessing using native
    /// SQL Server LIKE operators, []-%^
    /// </summary>
    /// <param name="searchTerm"></param>
    /// <returns></returns>
    private static string sqlInjectionWhitelist(string searchTerm)
    {
      /* Bug 16665 allow any character in the search
      // Not clear what to do here
      if (searchTerm == "")
        return "";

      // Only allow the safest characters (alphanumeric, spaces, dot, and tilde)
      //if (!Regex.IsMatch(searchTerm, @"^[\w\s\d.]+$"))
      if (!Regex.IsMatch(searchTerm, @"^[\w\s\d.\%\-\[\]\^]+$"))
        throw new FormatException("Invalid search/filter format");

       */
      return searchTerm;
    }


    /// <summary>
    /// Parse the date-range parameters and convert to SQL.
    /// Special processing will be needed for becasue the ~ character is used 
    /// such as this for a range: "03/01/2013~03/22/2013".  
    /// Also "03/01/2013~" and "~03/01/2013" are used.
    /// 
    /// Note: this was changed on May 9, 2013 so that if one or both 
    /// ranges is omitted, it will default to a range of a SINGLE day.
    /// This change was made to avoid creating huge result sets and slowing
    /// down the system.
    /// 
    /// This was changed to use a parameterized query, but there is still some cruft
    /// lying around (startDate and endDate variables) lying around that could be cleaned up
    /// if I really cared, but leaving them in makes it easier to see the change 
    /// I made for the parameterization.
    /// </summary>
    /// <param name="parameterizedSearchClause"></param>
    /// <param name="columnName"></param>
    /// <param name="searchTerm"></param>
    private static void constructDateRangeQuery(ref StringBuilder parameterizedSearchClause, string columnName, string searchTerm, HybridDictionary sql_args)
    {
      string startDate;
      string endDate;

      // No date range specified
      if (searchTerm == "~")
      {
        string now = DateTime.Now.Date.ToShortDateString();
        startDate = now;
        endDate = now + " 23:59:59:999";

        sql_args["startDate"] = startDate;
        sql_args["endDate"] = endDate;
      }

      else if (searchTerm.StartsWith("~"))
      {
        // No starting range; use the beginning of the day
        // Set the max date to the end of the day
        string day = searchTerm.Substring(1);
        startDate = day;
        if (day.Length > 10)
          endDate = day;
        else
          endDate = day + " 23:59:59:999";
        sql_args["startDate"] = startDate;
        sql_args["endDate"] = endDate;
      }

      else if (searchTerm.EndsWith("~"))
      {
        // No ending range; use end of day
        string day = searchTerm.Substring(0, searchTerm.Length - 1);
        startDate = day;
        if (day.Length > 10)
          endDate = day;
        else
          endDate = day + " 23:59:59:999";
        sql_args["startDate"] = startDate;
        sql_args["endDate"] = endDate;
      }
      else
      {
        // Both ranges specified.  Use end of day for ending range
        string[] dates = searchTerm.Split('~');
        startDate = dates[0];
        // Bug 24047: The dates can come in this format: 01/29/2019 12:00
        // In this case do not add the 23:59:59:999
        if (dates[1].Length > 10)
          endDate = dates[1];
        else
          endDate = dates[1] + " 23:59:59:999";
        sql_args["startDate"] = startDate;
        sql_args["endDate"] = endDate;
      }

      parameterizedSearchClause.Append(' ').Append(columnName).Append(" BETWEEN @startDate AND @endDate ");
    }

    /// <summary>
    /// Most efficient way of counting total number of records in table, hopefully.
    /// SELECT count(*) will do a full table scan.
    /// </summary>
    /// <param name="conString"></param>
    /// <returns></returns>
    private static int getRecordCount(String conString)
    {
      String dataReaderSQL =
        @"SELECT SUM(row_count)
            FROM sys.dm_db_partition_stats
            WHERE [object_id] = OBJECT_ID('dbo.TG_ACTLOG_DATA')
            AND index_id IN (0,1)";
      return getRecordCountInternal(conString, dataReaderSQL);
    }

    /// <summary>
    /// Simple rountine to run record count query
    /// Bug 14226.
    /// </summary>
    /// <param name="conString"></param>
    /// <param name="dataReaderSQL"></param>
    /// <returns></returns>
    private static int getRecordCountInternal(String conString, string dataReaderSQL)
    {

      SqlConnection dataReaderConnection = new SqlConnection();
      SqlDataReader dataReader = null;
      try
      {
        // Open a connection for the DataReader
        dataReaderConnection.ConnectionString = conString;
        dataReaderConnection.Open();

        SqlCommand dataReaderCommand = new SqlCommand(dataReaderSQL, dataReaderConnection);
        dataReader = dataReaderCommand.ExecuteReader();

        if (dataReader.Read())
        {
          // For some weird reason I cannot read this with GetInt32...
          int recordCount = Convert.ToInt32(dataReader.GetValue(0));
          return recordCount;
        }
        else
        {
          // Bug 14248
          string baseMsg = "ERROR: cannot read database with this query: " + dataReaderSQL;
          Logger.LogError(baseMsg, "Activity Logging GetRecordCountInternal"); 
          return 0;
          // End 14248
        }
      }
      finally
      {
        if (dataReader != null && !dataReader.IsClosed)
          dataReader.Close();
        dataReaderConnection.Close();
      }
    }
  }
}

