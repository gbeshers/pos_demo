using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CASL_engine;

namespace baseline
{
  public class ActivityLogCasl
  {
    /// <summary>
    /// Interface between security.casl and ActivityLog.cs.
    /// Bug 13613.
    /// </summary>
    /// <param name="args"></param>
    public static Object writeLogRecord(params object[] args)
    {
      string subject = "";
      string app = "";
      string action = "";
      string argsString = "";    // Args is a reserved word in C#
      string summary = "";
      string detail = "";

      try
      {
        GenericObject geo_args = misc.convert_args(args);
        // Bug 15688: Automated batch update does not set the session ID
        string session = safeStringGet(geo_args, "session");
        string user = safeStringGet(geo_args, "user");
        subject = safeStringGet(geo_args, "subject");
        app = safeStringGet(geo_args, "app");
        action = safeStringGet(geo_args, "action");
        argsString = safeStringGet(geo_args, "args");    // Args is a reserved word in C#
        summary = safeStringGet(geo_args, "summary");
        detail = safeStringGet(geo_args, "detail");
        detail = trimWastedFields(detail);                      // Bug 16362 / 15452
        // End Bug 15688: To be safe I protected all the conversions

        if (string.IsNullOrWhiteSpace(user))
        {
          user = misc.getLoginName();
        }

        if (string.IsNullOrWhiteSpace(app))
        {
          app = misc.getApplicationName();
        }

        GenericObject dbConnection = (GenericObject)geo_args.get("db_connection");
        if (dbConnection == null)
        {
          // Bug 14248.  Do not throw error into CASL
          Logger.LogError("db_connection not in params passed to writeLogRecord", "Activity Log writeLogRecord");
          return null;
          // End Bug 14248
        }

        String conString = (String)dbConnection.get("db_connection_string");
        ActivityLog.GetInstance().writeRecord(conString,
          session, user, subject, app, action, argsString, summary, detail);

        return null;
      }
      catch (Exception e)
      {
        Logger.cs_log_trace("Cannot write activity log record", e);
        return null;
      }

    }

    /// <summary>
    /// Remove fields the have only a key (before the colon) and no value
    /// Bug 16362 / 15452.
    /// </summary>
    /// <param name="detailField"></param>
    /// <returns></returns>
    private static string trimWastedFields(string detailField)
    {
      if (string.IsNullOrEmpty(detailField))
        return "";

      char[] charSeparators = new char[] { ',' };
      string[] details = detailField.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);
      StringBuilder cleaned = new StringBuilder();

      bool first = true;

      foreach (string rawDetail in details)
      {
        string detail = rawDetail.Trim();
        if (detail.EndsWith(":"))
          continue;
        if (first)
        {
          cleaned.Append(detail);
          first = false;
        }
        else
        {
          cleaned.Append(',').Append(detail);
        }
      }
      return cleaned.ToString();
    }

    /// <summary>
    /// CASL cannot be trusted to set a real value.
    /// Null values were passed as the session when Automated batch update was run
    /// via the auto_batch_update script.
    /// Protect against nulls and other problems.
    /// TODO: also consider putting this in a try block 
    /// to protect against the value being a non-string.
    /// Bug 15688
    /// </summary>
    /// <param name="geo_args"></param>
    /// <param name="geoKey"></param>
    /// <returns></returns>
    private static string safeStringGet(GenericObject geo_args, string geoKey)
    {
      string gottenString = "";

      if (geo_args.has(geoKey))
      {
        object potentialValue = geo_args.get(geoKey);
        if (potentialValue != null)
          gottenString = Convert.ToString(potentialValue);
      }

      return gottenString;
    }

    /// <summary>
    /// Used for expanding a record in the UI to get the details.
    /// TODO: convert to an Ajax call using iHttpHandler.
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static GenericObject GetActivityLogDetails(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      // Bug 11451: Filter on start and end date
      string serial_id = geo_args.get("database_id", "") as string;
      GenericObject dbConnection = (GenericObject)geo_args.get("db_connection");

      if (dbConnection == null)
      {
        // Bug 14248
        Logger.LogError("db_connection not in params passed to GetActivityLogDetails", "Activity Log GetActivityLogDetails");
        GenericObject detailsRecord = new GenericObject();
        detailsRecord.Add("Error", "GetActivityLogDetails error: dbConnection not set.");
        return detailsRecord;
        // End Bug 14248
      }

      String conString = (String)dbConnection.get("db_connection_string");


      // Retrieve the list of deposits
      bool status;

      try
      {
        GenericObject detailsRecord = ActivityLog.getDetailRecords(conString, serial_id, out status);
        if (!status)
        {
          string msg = "GetActivityLogDetails: Cannot read activity log";
          Logger.LogError(msg, "Activity Log getActivityLogDetails");
          return detailsRecord;
        }
        return detailsRecord;
      }
      catch (Exception e)
      {
        // Bug 14248
        string msg = "GetActivityLogDetails Error (Exception thrown): " + e.Message + Environment.NewLine + e.StackTrace;
        Logger.LogError(msg, "Activity Log getActivityLogDetails");

        // Bug 17849
        Logger.cs_log_trace("Error in GetActivityLogDetails", e);

        // Bug 15555: Return a geo with the details, but do not throw an exception.
        //throw CASL_error.create("title", "Error Reading Activity Log Details", "code", "Act-1", "message", msg + e.Message);
        GenericObject detailsRecord = new GenericObject();
        detailsRecord.Add("Error", "GetActivityLogDetails caught exception.  See the error log");
        return detailsRecord;
        // End bug 15555
        // End 14248
      }
    }


    /// <summary>
    /// CASL interface to activity log. Replaces ServerSide.ashx.
    /// Bug 20426 UMN PCI/PA-DSS remove use of ServerSide.ashx. This method is a translation of Fred's original method.
    /// </summary>
    /// <param name="args"></param>
    public static string CASL_getServerSideData(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      int echo = int.Parse(geo_args.get("sEcho") as String);
      int iDisplayLength = int.Parse(geo_args.get("iDisplayLength") as String);
      int iDisplayStart = int.Parse(geo_args.get("iDisplayStart") as String);
      string iSortDir = geo_args.get("sSortDir_0", "") as string;
      string sSearch = geo_args.get("sSearch", "") as string;

      // Total number of records returned in database query
      int iTotalDisplayRecords, iTotalRecords;

      string dynamicJsArray = baseline.ActivityLog.getServerSideData(iDisplayLength, iDisplayStart, iSortDir, sSearch,
        System.Web.HttpContext.Current.Request, out iTotalDisplayRecords, out iTotalRecords);

      string outputJson = dynamicJsArray;
      StringBuilder sb = new StringBuilder();

      // Add the header response fields
      sb.Append("{");
      sb.Append("\"sEcho\": ");
      sb.Append(echo);
      sb.Append(",");
      sb.Append("\"iTotalRecords\": ");
      sb.Append(iTotalRecords);
      sb.Append(",");
      sb.Append("\"iTotalDisplayRecords\": ");
      sb.Append(iTotalDisplayRecords);
      sb.Append(",");
      sb.Append("\"aaData\": ");
      sb.Append(outputJson);
      sb.Append("}");
      outputJson = sb.ToString();

      return outputJson;
    }
  }
}
