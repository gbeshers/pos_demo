﻿/*
  CBTBLOCK.js
*/

// Blocks ROOT JSON Object used to generate JQuery Datatables
var oCBTBlocksData = { 
  "asBlockNames": [],
  "aaBlockDist": [],          // Bug 19024 UMN added for auto distribution
  "oBlockFilteredIds": undefined     // Bug 19024 UMN added for auto distribution
};

// Bug 19677 UMN map of user checked checkboxes. Used to restore state after a refilter_records
// key is checkbox's name attribute in DOM, value is a JS object. Example:
// { "checked" : true, "amount" : 324.97 } -- amount is a float
var oUserCheckedBoxes = {};

// timeout/waits for keyup when filtering, so doesn't fire till user stops typing
var iFilterWait = 500; // number of milliseconds to wait before firing filter

var exclusiveBlockId = null;
var jLastCheckedSelector = null;

function update_PB_ENTERPRISE_PAYMENT_SCREEN_info() 
{
  "use strict";
  var fGrandAmountPaid = 0.00;
  var fGrandBalance = 0.00;
  var sGrandAmountPaidLabel = "";
  var sGrandBalanceLabel = "";
  var sGrandAmountPaidSelector = "#GRAND_AMOUNT_PAID";
  var sGrandBalanceSelector = "#GRAND_BALANCE";
  var sGrandAmountPaidLabelSelector = "#GRAND_AMOUNT_PAID_label";
  var sGrandBalanceLabelSelector = "#GRAND_BALANCE_label";
   
  for (var sBlockIndex in oCBTBlocksData.asBlockNames) 
  {
    var sBlockName = oCBTBlocksData.asBlockNames[sBlockIndex];
    var oBlock = oCBTBlocksData[sBlockName];
    var oCBTDataTableParams = oBlock.oCBTDataTableParams;
    var sBlockCss = "block_" + oCBTDataTableParams.sCBTBlockID; // Bug 13220

    var sBlockAmountPaidSelector = "#" + sBlockCss + "_GROUP_LEVEL_AMOUNT_PAID"; /*sample selector:  ".block_Physician .prop_GROUP_LEVEL_AMOUNT_PAID" */
    var sBlockAmountPaidLabelSelector = "#" + sBlockCss + "_GROUP_LEVEL_AMOUNT_PAID_label"; /*sample selector:  ".block_Physician .prop_GROUP_LEVEL_AMOUNT_PAID_label" */
    var sRowAmountPaidSelector = "." + sBlockCss + " .prop_RESERVED_AMOUNT_PAID input"; /*sample selector:  ".block_Physician .prop_RESERVED_AMOUNT_PAID input"  Bug 12752 QG*/
    var fBlockAmountPaid = 0.00;
    var sBlockBalanceSelector = "#" + sBlockCss + "_GROUP_LEVEL_BALANCE"; // id="block_BCSClinic_PB_GROUP_LEVEL_BALANCE"
    var sBlockBalanceLabelSelector = "#" + sBlockCss + "_GROUP_LEVEL_BALANCE_label"; // id="block_BCSClinic_PB_GROUP_LEVEL_BALANCE_label"
    var sRowBalanceSelector = "." + sBlockCss + " .prop_RESERVED_ADJUSTED_BALANCE"; 
    if ($(sRowBalanceSelector).length < 1)
      sRowBalanceSelector = "." + sBlockCss + " .prop_RESERVED_BALANCE";  /* use prop_RESERVED_BALANCE as second choice */
    var fBlockBalance = 0.00;

    // update GROUP_LEVEL_AMOUNT_PAID field, looping over each amount paid input field
    $(sRowAmountPaidSelector).each(function() 
    {
      var sRowAmountPaid = $.trim($(this).val()); // undefined for this.value 
      if (sRowAmountPaid) 
      {
        sRowAmountPaid = sRowAmountPaid.replace(/[$,]/g, ""); // Bug 12809
        sRowAmountPaid = sRowAmountPaid.replace(/^\s+|\s+$/g, ""); // Bug 13435 trim all space
      }
      var fRowAmountPaid = parseFloat(sRowAmountPaid);
      // Bug 12809 format amount paid input field with nbr "100.00" but leave illegal character in place for validate_tag 
      if (sRowAmountPaid === "") this.value = "$0.00";
      if (!isNaN(fRowAmountPaid)) this.value = formatCurrency(fRowAmountPaid);

      // Bug 12697 Qi Fix a problem in Firefox that Attribute "value" cannot pass correctly.
      if (isNaN(fRowAmountPaid)) fRowAmountPaid = parseFloat(this.getAttribute("value"));  
      if (isNaN(fRowAmountPaid)) fRowAmountPaid = 0.00;
      
      // Bug 15423 UMN update check-box to selected if amt paid is changed or starts out non-zero
      var jPaymentSelector = $(this).parent().parent().find('.prop_RESERVED_SELECTED');
      if (jPaymentSelector.length === 1)
      {
        var jTrSelector = jPaymentSelector.closest('tr');
        var jSelHidden = jPaymentSelector.find('input');
        var jChkBox = jSelHidden.next();
        var jBalanceSelector = jTrSelector.find(sRowBalanceSelector);
        
        if (Math.abs(fRowAmountPaid) > 0.001) // The payment is non-zero
        {
          jSelHidden.val(true);
          jChkBox.prop('checked', true);
          jTrSelector.addClass("highlight");
        }
        else
        {
          jSelHidden.val(false);
          jChkBox.prop('checked', false);
          this.value = formatCurrency("0.00");
          fRowAmountPaid = 0;
          // de-highlight row
          jTrSelector.removeClass("highlight");
        }
      }
      
      // update RESERVED_FEE
      var sRowProcessingFeeRate = this.getAttribute("feerate"); // this.feerate; // $(this).parent().parent().find('.prop_RESERVED_FEE_RATE').text()+"";// this.feerate
      var fRowProcessingFee = 0.00;
      if (sRowProcessingFeeRate != "FIXED")
      {
        fRowProcessingFee = fRowAmountPaid * parseFloat(sRowProcessingFeeRate);
      }
      else 
      {
        var sRowProcessingFee = $(this).parent().parent().find('.prop_RESERVED_FEE').text() + "";
        fRowProcessingFee = parseFloat(sRowProcessingFee.replace(/[$,]/g, ""));
      }
      if ($(this).parent().parent().find('.prop_RESERVED_FEE').length == 1)
        $(this).parent().parent().find('.prop_RESERVED_FEE').html(formatCurrency(fRowProcessingFee.toFixed(2)));
      
      // update RESERVED_TOTAL_PAID
      var bHasRowTotalPaid = ($(this).parent().parent().find('.prop_RESERVED_TOTAL_PAID').length == 1);
      var fRowTotalPaid = fRowAmountPaid + fRowProcessingFee;
      
      // no fee if AmountPaid is 0.00 Bug 13706 & 13708 Previous 0 check is inappropriate, negative amount should be considered.
      if (Math.abs(fRowAmountPaid) < 0.001) fRowTotalPaid = 0.00;
      if (bHasRowTotalPaid)
      {
        $(this).parent().parent().find('.prop_RESERVED_TOTAL_PAID').html(formatCurrency(fRowTotalPaid.toFixed(2)));
      }
      // increment GROUP_LEVEL_AMOUNT_PAID based on RESERVED_TOTAL_PAID or RESERVED_AMOUNT_PAID
      if (bHasRowTotalPaid) fBlockAmountPaid += fRowTotalPaid;
      else fBlockAmountPaid += fRowAmountPaid;
    });
    
    // Bug 19677 UMN Add to amount paid any checkboxes hidden by a filter
    for (var key in oUserCheckedBoxes)
    {
      var checkedAmount = oUserCheckedBoxes[key];
      var hiddenByFilter = document.getElementsByName(key).length === 0;
      if (hiddenByFilter && checkedAmount.checked === true && checkedAmount.classNames.indexOf(sBlockCss) >= 0)
        fBlockAmountPaid += checkedAmount.amount;
    }

    // update GROUP_LEVEL_AMOUNT_PAID UI field
    $(sBlockAmountPaidSelector).html(formatCurrency(fBlockAmountPaid.toFixed(2)));
    var sBlockAmountPaidLabel = oCBTDataTableParams.sCBTBlockLabels.GROUP_LEVEL_AMOUNT_PAID;
    if (sBlockAmountPaidLabel !== undefined && sBlockAmountPaidLabel !== "") 
    {
      $(sBlockAmountPaidLabelSelector).html(sBlockAmountPaidLabel + ": ");
      if (sGrandAmountPaidLabel === "") sGrandAmountPaidLabel = sBlockAmountPaidLabel;
    }

    fGrandAmountPaid += fBlockAmountPaid;
    
    // update GROUP_LEVEL_BALANCE UI field 
    $(sRowBalanceSelector).each(function() 
    {
      // Bug 23831 UMN change from innerHtml to innerText to accommodate inline styles which have spans in them
      var fRowBalance = parseFloat(this.innerText.replace(/[$,]/g, ""));
      if (isNaN(fRowBalance))
        fRowBalance = 0.00;
      fBlockBalance += fRowBalance;
    });
    
    // update Block level Balance UI field
    $(sBlockBalanceSelector).html(formatCurrency(fBlockBalance.toFixed(2)));
    var sBlockBalanceLabel = oCBTDataTableParams.sCBTBlockLabels.GROUP_LEVEL_BALANCE;
    if (sBlockBalanceLabel !== undefined && sBlockBalanceLabel !== "") 
    {
      $(sBlockBalanceLabelSelector).html(sBlockBalanceLabel + ": ");
      if (sGrandBalanceLabel === "") sGrandBalanceLabel = sBlockBalanceLabel;
    }
    else // BTT has no balance, so don't show group level balance
    {
      $(sBlockBalanceSelector).html("");
      $(sBlockBalanceLabelSelector).html("");
    }
    fGrandBalance += fBlockBalance;
  }
  
  // update Grand Balance and Total UI field
  $(sGrandAmountPaidSelector).html(formatCurrency(fGrandAmountPaid.toFixed(2)));
  $(sGrandBalanceSelector).html(formatCurrency(fGrandBalance.toFixed(2)));
  if (sGrandAmountPaidLabel !== "") $(sGrandAmountPaidLabelSelector).html(sGrandAmountPaidLabel + ": ");
  if (sGrandBalanceLabel !== "") $(sGrandBalanceLabelSelector).html(sGrandBalanceLabel + ": ");

} // end update_PB_ENTERPRISE_PAYMENT_SCREEN_info

/*   
  initialize_CBTBlock_tables uses $(document).ready to create the jquery datatables. 
    a.  Initialize jquery datatables w oBlock[BLOCKNAME], oDataTableParams & aoColumns & aaData
    b.  Process oCBTBlocksData.oBlock[BLOCKID].aaData to make fields editable and format
    c.  Add JQuery Datatables Event Handler 
    d.  Initialization and Validation
*/
function initialize_CBTBlock_tables()
{
  $(document).ready(function()
  {   
    //Bug 26113 NAM: removed
    
    // Bug 15423 Checkbox sorting - make it use the DOM by creating an array with 
    //           the values of all the checkboxes in a column
    jQuery.fn.dataTableExt.afnSortData['dom-checkbox'] = function(oSettings, iColumn)
    {
      return this.api().column(iColumn, { order: 'index' }).nodes().map(function (td, i) {
        return $('input:checkbox', td).prop('checked') ? '1' : '0';
      });
    };
      
    var asDrillDownToClick = []; // Bug 19655 SX
    if (oCBTBlocksData !== null)
    {
      for (var sBlockIndex in oCBTBlocksData.asBlockNames)
      {
        var sBlockName = oCBTBlocksData.asBlockNames[sBlockIndex];
        var oBlock = oCBTBlocksData[sBlockName];
        var oDataTableParams = oBlock.oDataTableParams;
        var oCBTDataTableParams = oBlock.oCBTDataTableParams;
  /* JQUERY DATATABLE SECTION A: Pre-process block jquery dataset (oCBTBlocksData.oBlock[BLOCKID].aaData &
     aoCBTColumns) to create editable fields for all non-calculated custom fields including AmountPaid */
        var oBlockaaData = oBlock.aaData;
        for (var i = 0; i < oBlockaaData.length; i++)
        {
          // oRow = oBlockaaData[i];
          var aoCBTColumns = oBlock.aoCBTColumns;
          var aoColumns = oBlock.aoColumns;
          for (var j = 0; j < aoCBTColumns.length; j++)
          {
            // UMN keep JSHint/JSLint happy
            var sReservedName, sFieldName, sInputID, sErrorSpanID, sFieldInfo;

            // Format the readonly and currency type column
            var sCBTTagName = aoCBTColumns[j].sCBTTagName;
            var sCBTPrompt = aoCBTColumns[j].sCBTPrompt;
            var sCBTDefault = aoCBTColumns[j].sCBTDefault; // Bug 14761 
            var sCBTType = aoCBTColumns[j].sCBTType; // Bug 14761 
            var mColumnValue = oBlockaaData[i][sCBTTagName];
            if (mColumnValue=="") mColumnValue =  aoCBTColumns[j].sCBTDefault; // Bug 24018
            var bCBTReadOnly = aoCBTColumns[j].bCBTReadOnly;
            var bCBTReq = aoCBTColumns[j].bCBTReq; // Bug 19655 SX
            var oCBTValueList = aoCBTColumns[j].sCBTValueList;   // Bug 12697
            var sCBTValueListUI = aoCBTColumns[j].sCBTValueListUI;  // Bug 13061
            //IPAY-1215 SM Modified 'sCBTColumnInlineStyle' below. Accessibility color change. Has to be changed here because this is defined in JQuery tables.
            var sCBTColumnInlineStyle = "width:5em; color:#000000;" // Bug 18007 SX 

            if (bCBTReadOnly && aoColumns[j].sType == "currency")
            {
              oBlockaaData[i][sCBTTagName] = formatCurrency(mColumnValue);
            }
            if (sCBTType == "Type.date") // Bug 14761 set default value as current for date 
            {
              if (mColumnValue === "") 
              {
                mColumnValue = sCBTDefault;
              }
              else if (mColumnValue == "current")
              { 
                mColumnValue = formatDate(new Date());
              }
              oBlockaaData[i][sCBTTagName] = mColumnValue;
            }

            // Make editable column
            sReservedName = "RESERVED_BALANCE;RESERVED_PENDING_PAYMENT;RESERVED_ADJUSTED_BALANCE;RESERVED_FEE;RESERVED_TOTAL_PAID";
            var mFeeRate = oBlockaaData[i].RESERVED_FEE_RATE; // value can be "FIXED" or Percentage as Decimal e.g. 0.06 for 6%
            if (sCBTPrompt == "RESERVED_AMOUNT_PAID") 
            {
              sFieldName = "amount";
              sInputID = "args." + oCBTDataTableParams.sID + "." + i + "." + sFieldName;
              sFieldInfo = aoCBTColumns[j].sCBTFieldInfo; // Validation info as INPUT field_info attribute
              sFieldInfo = sFieldInfo.replace("requiredInRow", "").replace(/InRow/g, "");
              sErrorSpanID = sInputID + "_error";
              if (bCBTReadOnly)
              {
                var formattedVal = oBlockaaData[i][sCBTTagName];
                // Bug 15423 UMN labels don't post, so need hidden input
                // IPAY-1215 Sm Accessibility Add aria label
                oBlockaaData[i][sCBTTagName] = '<input aria-label="payment" type="hidden" id="' + sInputID + '" name="' + sInputID + '" class="' +
                  sFieldInfo + ' ' + oCBTDataTableParams.sID + ' " feerate="' + mFeeRate + '" value="' + mColumnValue + '"/>';
                  
                // Bug 12752/12617 QG Use currency format oBlockaaData[i][sCBTTagName]
                oBlockaaData[i][sCBTTagName] += '<label type="text" for="' + sInputID + '" feerate="' + mFeeRate +
                  '" value="' + mColumnValue + '" style="' + sCBTColumnInlineStyle + '" >' + formattedVal + '</label>'; // Bug 18007 SX
              }
              else
              {
                 // IPAY-1215 Sm Accessibility Add aria label
                oBlockaaData[i][sCBTTagName] = '<input aria-label="payment" type="text" id="' + sInputID + '" name="' + sInputID + '" class="' + sFieldInfo + ' ' + 
                  oCBTDataTableParams.sID + '" feerate="' + mFeeRate + '" value="' + 
                  mColumnValue + '" style="' + sCBTColumnInlineStyle + '" > </input>' +
                  '<label type="text" for="' + sInputID + '" value="' + sInputID + '" style="display:none;">' + sInputID + '</label>' + // Bug 15925 UMN a11y
                  '<span class="error" id="' + sErrorSpanID + '"/>'; // Bug 18007 SX
              }
            }
            else if (oCBTValueList !== "" && oCBTValueList !== undefined) // For Drop Down List Bug 12697 
            {
              sFieldName = sCBTTagName;
              sInputID = "args." + oCBTDataTableParams.sID + "." + i + "." + sFieldName;
              sFieldInfo = aoCBTColumns[j].sCBTFieldInfo; // Validation info as INPUT field_info attribute
              
              // Bug 13061/13612 set default value; Bug 15423 pass bCBTReadOnly; Bug 18007 pass aoCBTColumns[j] for required and style 
              oBlockaaData[i][sCBTTagName] = makewidget(sInputID, oCBTValueList, sCBTValueListUI, mColumnValue, sCBTDefault, bCBTReadOnly, aoCBTColumns[j]);
            }
            else if (!(aoCBTColumns[j].bCBTReadOnly) && (sCBTPrompt === "" || sReservedName.indexOf(sCBTPrompt) < 0))
            {
              sFieldName = sCBTTagName;
              sInputID = "args." + oCBTDataTableParams.sID + "." + i + "." + sFieldName;
              sFieldInfo = aoCBTColumns[j].sCBTFieldInfo; // Validation info as INPUT field_info attribute
              var sCBTMinLength = aoCBTColumns[j].sCBTMinLength; // Bug 14452
              var sCBTMaxLength = aoCBTColumns[j].sCBTMaxLength; // Bug 14452
              sCBTButtonLabel = aoCBTColumns[j].sCBTButtonLabel;
              sErrorSpanID = sInputID + "_error";
              
              // Bug 19764 UMN - Add support for external and internal buttons
              if (sCBTPrompt === "RESERVED_BUTTON" || sCBTPrompt === "RESERVED_BUTTON_INTERNAL") 
              {
                var btnName = (sCBTButtonLabel === "") ? sCBTTagName : sCBTButtonLabel;
                var onclick = "";
                if (sCBTPrompt === "RESERVED_BUTTON") 
                  onclick = (mColumnValue !== "") ? 'onclick=window.open("' + mColumnValue + '")' : "";
                else
                  onclick = (mColumnValue !== "") ? 'onclick=' + mColumnValue : "";
                oBlockaaData[i][sCBTTagName] = '<button type=button ' + onclick + ' name='+sInputID+ ' sInputIDstyle="' + 
                  sCBTColumnInlineStyle + '">' + btnName + '</button>'+
                  '<span class="error" id="' + sErrorSpanID + '"/>'; // Bug 18007 SX
              }
              else if (sCBTPrompt === "RESERVED_DETAILS") // Bug 18719 UMN - Add detail group support
              {
                var sCBTID = aoCBTColumns[j].sCBTID;  // Bug 20490 UMN
                
                // if the server has a value that's not the icon image, then make a button
                if (mColumnValue != sCBTDefault && mColumnValue !== "")
                {
                  var sCBTDetailBtnURL = aoCBTColumns[j].sCBTDetailBtnURL;
                  var sCBTDetailBtnImageURL = aoCBTColumns[j].sCBTDetailBtnImageURL;
                  var sCBTDetailBtnTarget = aoCBTColumns[j].sCBTDetailBtnTarget;
                  var sGroupName = mColumnValue;
                  
                  // set a default width if style isn't set
                  if (!sCBTColumnInlineStyle) sCBTColumnInlineStyle = "height:2.3em;width:2.3em;";

                  oBlockaaData[i][sCBTTagName] = '<button class="det-button" type=button onclick="get_details(this)" style="border:0;background:url(' + 
                    sCBTDetailBtnImageURL + ') no-repeat 50% 0%;' + sCBTColumnInlineStyle + '" id="' + sGroupName + '" data-url="' + 
                    sCBTDetailBtnURL + '" data-cbtid="' + sCBTID + '" data-target="' + sCBTDetailBtnTarget + '"></button><span class="error" id="' +
                    sErrorSpanID + '"/>';
                    
                  // Bug 19655 SX a required custom field will automatically show the details  
                  if (bCBTReq) asDrillDownToClick.push(sGroupName);
                }
                else
                {
                  // clear out the value so the image icon path doesn't render
                  oBlockaaData[i][sCBTTagName] = "";
                }
              }
              else
              {
                //IPAY-1215 - Accessibility SM Added aria label in input below.
                oBlockaaData[i][sCBTTagName] = '<input aria-label="Physician Receivables" type="text" id="' + sInputID + '" name="' + sInputID +
                  '" minlength="' + sCBTMinLength + '" maxlength="' + sCBTMaxLength + // Bug 26376 UMN
                  '" class="' + sFieldInfo + '"' + oCBTDataTableParams.sID + '" data-rule-minlengthinrow="' + sCBTMinLength +
                  '" data-rule-maxlengthinrow="' + sCBTMaxLength + '" value="' + mColumnValue + '"  style="' + sCBTColumnInlineStyle + // Bug 18007 SX
                  '"> </input>' +
                  '<label type="text" for="' + sInputID + '" value="' + sInputID + '" style="display:none;">' + sInputID + '</label>' + // Bug 15925 UMN a11y
                  '<span class="error" id="' + sErrorSpanID + '"/>';
              }
            }
            else if (sCBTColumnInlineStyle !== "") // Bug 18007 UMN allow inline style for text elements too
            {
              oBlockaaData[i][sCBTTagName] = "<span style='" + sCBTColumnInlineStyle + "'>" + oBlockaaData[i][sCBTTagName] +
                "</span>";
            }
          } // END for with j
        } // END for with i
        /* END OF JQUERY DATATABLE SECTION A */

  /* JQUERY DATATABLE SECTION B: initialize jquery datatables based on JSON data oCBTBlocksData */
        var sBlockTableSelector = "#" + oCBTDataTableParams.sTableID; /*sample selector:  "#TableBlockPhysician" */
        var oTable = $(sBlockTableSelector).DataTable(
        {
          'paging': oDataTableParams.bPaginate,                 // false
          'lengthChange': oDataTableParams.bLengthChange,       // false
          'processing': oDataTableParams.bProcessing,           // false
          'autoWidth': oDataTableParams.bAutoWidth,             // false
          'stateSave': oDataTableParams.bStateSave,             // false
          'retrieve': oDataTableParams.bRetrieve,               // true
          'searching': oDataTableParams.bFilter,                // true
          'info': oDataTableParams.bInfo,                       // false
          'dom': 'lrtip', // Bug 15423 UMN remove default filters
          'order': [], // Bug 13449 no sort, keep original order
          'data': oBlock.aaData,
          'columns': oBlock.aoColumns
        }); // END dataTable
      } // END for with sBlockIndex

      // Bug 16045 UMN remove slideUp/slideDown. It was doing n(n-1)/2 calls!
    } // END if oCBTBlocksData != null
  /* END JQUERY DATATABLE SECTION B: initialize jquery datatables based on JSON data oCBTBlocksData */
    
  /* JQUERY DATATABLE SECTION C: ADD EVENT HANDLERS  */
    // bind change event on all fields with "prop_RESERVED_AMOUNT_PAID" class
    $(".prop_RESERVED_AMOUNT_PAID input").change(function()
    {
      // update group level totals and grand total
      update_PB_ENTERPRISE_PAYMENT_SCREEN_info();
    });

    // Bug 15423 UMN ADD payment selector and filter handlers
    var oChkSelector = $(".prop_RESERVED_SELECTED input:checkbox");
    if (oChkSelector.length > 0) 
    {
      // Bug 18448 UMN any input, or dropdown select on row will cause selector to be set now 
      $("input, select").change(function()
      {
        var jPaymentSelector = $(this).parent().parent().find('.prop_RESERVED_SELECTED');
        if (jPaymentSelector.length === 1)
        {
          var jTrSelector = jPaymentSelector.closest('tr');
          var jAmtPaidSelector = jTrSelector.find('.prop_RESERVED_AMOUNT_PAID input');
          var jSelHidden = jPaymentSelector.find('input');
          var jChkBox = jSelHidden.next();
          jSelHidden.val(true);
          jChkBox.prop('checked', true);
          jTrSelector.addClass("highlight");

          // Bug 19677 UMN store the amounts so we don't lose them during filtering
          update_user_checked_boxes(jTrSelector, jChkBox, jAmtPaidSelector); 

          // Bug 23344 MJO - Trigger exclusive block checking
          on_excl_chkbox_change(jChkBox);
        }
      });
      add_payment_selector_handlers(oChkSelector);       
    }

    // Bug 22787 UMN restore deleted DOM nodes before submit
    $(".form_inst").on('submit', function() {
      reset_all_filters();
      restore_selected(); // Bug 19677 UMN
    });
    
  /* END JQUERY DATATABLE SECTION C: ADD EVENT HANDLERS  */

  /* JQUERY DATATABLE SECTION D: INITIALIZATION and VALIDATION */

    // Bug 23229 UMN
    add_payment_filter_handlers();
    add_payment_combo_filter_handlers();

    if (oChkSelector.length > 0) init_checkboxes();
    
    if ($('#autodist-manual').length > 0) init_autodistribute();
    
    // Bug 15423 UMN Calling update_PB_ENTERPRISE_PAYMENT_SCREEN_info must be last 
    // because checkboxes may have changed the value of RESERVED_AMOUNT_PAID fields!
    if ($(".prop_RESERVED_AMOUNT_PAID input").length > 0)  // Bug 12752/15423
    {
      update_PB_ENTERPRISE_PAYMENT_SCREEN_info();
    }
  /* END OF JQUERY DATATABLE SECTION D: INITIALIZATION and VALIDATION */

    // Bug 13143 QG JQuery Validation
    $(".form_inst").validate(
    {
      errorPlacement: function(error, element)
      {
        element.parent('td').find('span:first').append(error);
      },
      errorElement: "div",
      // focusCleanup: true,
      //Bug 24417 NK Amount fields are validated prematurely during subsequent changes 
      onkeyup: false,
      onkeydown: false,
      onsubmit: false,
      ignore: ""  // Bug 13804 QG fix the issue that collapse block transaction can disable validation
    });
    
    // Bug 17187/17595 SX datepicker add icon trigger and change year and month option
    // Bug 17201 UMN need to test if dateInRow is on page
    if ( $(".dateInRow").length > 0)
    {
      $.datepicker.setDefaults($.datepicker.regional['']);
      $(".dateInRow").datepicker({
        dateFormat: 'mm/dd/yy',
        showOn: "button",
        buttonImage: site_static + "/baseline/business/images/calendar.gif",
        buttonImageOnly: true,
        buttonText: "Select date",
        changeMonth: true,
        changeYear: true
      }); 
    }
    
    // Bug 19655 SX auto click any drill downs that were required
    click_drilldowns(asDrillDownToClick);
    
  });  // END document ready

} // END function initialize_CBTBlock_tables


$(document).ready(function () {
  // Bug 17201 UMN need to test if date-p is on page
  if ( $(".date-p").length > 0)
  {
    var buttonimageuri = "";
    if (typeof site_static !== undefined)
      buttonimageuri = site_static + "/baseline/business/images/calendar.gif";
    
    $.datepicker.setDefaults($.datepicker.regional['']);
    $(".date-p").datepicker({
      yearRange: "-120:+10",    //Bug 26078 NAM: set range from 120 years in the past to 10 years in the future
      dateFormat: 'mm/dd/yy',
      showOn: "button",
      buttonImage: buttonimageuri,
      buttonImageOnly: true,
      buttonText: "Select date",
      changeMonth: true,
      changeYear: true
    });
  }
});

// UI Widget  Bug 13061 QG
// Bug 13612 QG/UMN If there is a default value for the field, select it.
// Bug 15423 UMN add boolean checkbox; Bug 12697
function makewidget(id, ValueList, ValueListUI, colValue, defaultValue, readOnly, oCBTColumn)
{
  // Bug 18250 SX make dropdown required field for jquery validation
  var checkboxID = 0;// IPAY-1215 SM Accessibility. Variable to allow a unique id on check box's.  This is used starting on line 588.
  var sErrorSpanID = id + "_error";
  var sRequired = "";
  var sCBTColumnInlineStyle = oCBTColumn.sCBTColumnInlineStyle; // Bug 18007 SX
  if (oCBTColumn.bCBTReq) sRequired = "requiredInRow"; // Bug 25979 MJO - Check correct field

  var value; // keep JSHint/JSLint happy
  if (!(ValueListUI === "boolean" || ValueListUI === "image") && !ValueList.hasOwnProperty(colValue))    // Bug 23001 Bug 13612 UMN handle defaults
  {
    colValue = defaultValue;
  }
  
  // Bug 16127/22886 MJO/UMN - If it's read only *and not a boolean*, just show it as a label
  if (readOnly && ValueListUI !== "boolean")
  {
    return '<label type="text" name="' + id + '" value="' + colValue + '" >' + ValueList[colValue] + '</label>';
  }
  
  switch (ValueListUI)
  {
    case "popup":
      var sReturn = '<select id="' + id + '" name="' + id + '" class="' + sRequired + '" style="' + sCBTColumnInlineStyle + '">'; // Bug 18007 SX
      for (value in ValueList)
      {
        // Bug 13612
        if (value == colValue)  // #13612 Check value instead of description
        {
          sReturn += ('<option value="' + value + '" selected="selected">' + ValueList[value] + '</option>');
        }
        else
        {
          sReturn += ('<option value="' + value + '">' + ValueList[value] + '</option>');
        }
      }
      sReturn += '</select>';
      sReturn += '<label type="text" for="' + id + '" value="' + id + '" style="display:none;">' + id + '</label>'; // Bug 15925 UMN a11y
      sReturn += '<span class="error" id="' + sErrorSpanID + '"/>'; // Bug 25979 MJO - Added error span
      return sReturn;
    
    // Bug 15423 payment selector
    case "boolean":
      // html only submits checkboxes that are checked, so use hidden input as real state
      var nonArgsId = id.replace("args.", "") + "_x";
      sReturn = '<span class="checkbox_sel boolean"><input type="hidden" name="';
      sReturn += id + '"/><input type="checkbox" name="' + nonArgsId + '" id="' + nonArgsId + '"';
      if (colValue.toLowerCase() !== "false") sReturn += ' checked="checked"';
      if (readOnly) sReturn += ' disabled="disabled"';
      sReturn += ' style="' + sCBTColumnInlineStyle + '"/>';
      sReturn += '<label type="text" for="' + nonArgsId + '" value="' + nonArgsId + '" style="display:none;">' + nonArgsId + '</label>'; // Bug 15925 UMN a11y
      sReturn += '</span>';
      return sReturn;    
    
    case "image":
      sReturn = '<img class="image" style="' + sCBTColumnInlineStyle + '" src="' + colValue + '" data-tagname="' + 
        oCBTColumn.sCBTTagName + 'alt=' + oCBTColumn.sCBTTagName + '" >'; // Bug 15925 UMN a11y add alt
      return sReturn;
                        
    // non-payment selector horizontal or vertical checkbox
    case "checkbox":
      sReturn = '<span>';
      for (value in ValueList)
      {
        // Bug 13612
        if (value == colValue)  // #13612 Check value instead of description
        {
          // IPAY-1215 SM Accessibility Add label
          sReturn += ('<input aria-label="checkbox" type="checkbox" value="' + value + '" name="' + id + '" id="' + id + checkboxID + '" style="' + 
            sCBTColumnInlineStyle + '" checked="checked">' + ValueList[value] + '</input>');
        }
        else
        {
          // IPAY-1215 SM Accessibility Add label
          sReturn += ('<input aria-label="checkbox" type="checkbox" value="' + value + '" name="' + id + '" id="' + id + checkboxID + '" style="' + 
            sCBTColumnInlineStyle + '">' + ValueList[value] + '</input>');
        }
        sReturn += '<label type="text" for="' + id + '" value="' + id + '" style="display:none;">' + id + '</label>'; // Bug 15925 UMN a11y
        checkboxID++// IPAY-1215 SM Accessibility.
      }
      sReturn += '</span>';
      return sReturn;
      
      
    case "vertical_checkbox":
      sReturn = '<span>';


      for (value in ValueList)
      {
        
        // Bug 13612
        if (value == colValue)  // #13612 Check value instead of description
        {
          // IPAY-1215 SM Accessibility Add label
          sReturn += ('<input  aria-label="checkbox" type="checkbox" value="' + value + '" name="' + id + '" id="' + id + checkboxID +  '" style="' + 
            sCBTColumnInlineStyle + '" checked="checked">' + ValueList[value] + '</input>');
        }
        else
        {
          // IPAY-1215 SM Accessibility Add label
          sReturn += ('<input aria-label="checkbox"  type="checkbox" value="' + value + '" name="' + id + '" id="' + id + checkboxID + '" style="' + 
            sCBTColumnInlineStyle + '">' + ValueList[value] + '</input>');
        }
        sReturn += '<label type="text" for="' + id + '" value="' + id + '" style="display:none;">' + id + checkboxID + '</label>'; // Bug 15925 UMN a11y
        sReturn += '<br/>';
        checkboxID++// IPAY-1215 SM Accessibility.
      }
      sReturn += '</span>';
      return sReturn;
      
    // Bug 12697 QG Support radio buttons   
    case "radio":
      sReturn = '<span>';
      for (value in ValueList)
      {
        // Bug 13612
        if (value == colValue)  // #13612 Check value instead of description
        {
          // IPAY-1215 SM Accessibility Add label
          sReturn += ('<input  aria-label="checkbox" type="radio" value="' + value + '" name="' + id + '" id="' + id + checkboxID + '" style="' + 
            sCBTColumnInlineStyle + '" checked="checked">' + ValueList[value] + '</input>');
        }
        else
        {
          // IPAY-1215 SM Accessibility Add label
          sReturn += ('<input  aria-label="checkbox" type="radio" value="' + value + '" name="' + id + '" id="' + id + checkboxID + '" style="' + 
            sCBTColumnInlineStyle + '">' + ValueList[value] + '</input>');
        }
        sReturn += '<label type="text" for="' + id + '" value="' + id + '" style="display:none;">' + id + checkboxID + '</label>'; // Bug 15925 UMN a11y
        checkboxID++// IPAY-1215 SM Accessibility.
      }
      sReturn += '</span>';
      return sReturn;
      
    case "vertical_radio":
      sReturn = '<span>';
      for (value in ValueList)
      {
        // Bug 13612
        if (value == colValue)  // #13612 Check value instead of description
        {
          sReturn += ('<input type="radio" value="' + value + '" name="' + id + '" id="' + id + checkboxID + '" style="' + 
            sCBTColumnInlineStyle + '" checked="checked">' + ValueList[value] + '</input>');
        }
        else
        {
          sReturn += ('<input type="radio" value="' + value + '" name="' + id + '" id="' + id + checkboxID + '" style="' + 
            sCBTColumnInlineStyle + '">' + ValueList[value] + '</input>');
        }
        sReturn += '<label type="text" for="' + id + '" value="' + id + '" style="display:none;">' + id + checkboxID + '</label>'; // Bug 15925 UMN a11y
        sReturn += '<br/>';
        checkboxID++// IPAY-1215 SM Accessibility.
      }
      sReturn += '</span>';
      return sReturn;
      
    // Bug 12697 QG  Support list and one_list
    case "one_list":
      var size = 0;
      for (value in ValueList)
      {
        size++;
        if (size == 10) break;
      }
      sReturn = '<select id="' + id + '" name="' + id + '" size=' + size + ' class="' + sRequired + '" style="' + sCBTColumnInlineStyle + '">';
      for (value in ValueList)
      {
        // Bug 13612
        if (value == colValue)  // #13612 Check value instead of description
        {
          sReturn += ('<option value="' + value + '" selected="selected">' + ValueList[value] + '</option>');
        }
        else
        {
          sReturn += ('<option value="' + value + '">' + ValueList[value] + '</option>');
      }
      }
      sReturn += '</select>';
      sReturn += '<label type="text" for="' + id + '" value="' + id + '" style="display:none;">' + id + '</label>'; // Bug 15925 UMN a11y
      sReturn += '<span class="error" id="' + sErrorSpanID + '"/>';
      return sReturn;
      
    case "list":
      size = 0;
      for (value in ValueList)
      {
        size++;
        if (size == 10) break;
      }
      sReturn = '<select id="' + id + '" name="' + id + '" style="' + sCBTColumnInlineStyle + '" size=' + 
        size + ' multiple="true" class="' + sRequired + '">';
      for (value in ValueList)
      {
        // Bug 13612 Check value instead of description
        if (value == colValue)
        {
          sReturn += ('<option value="' + value + '" selected="selected">' + ValueList[value] + '</option>');
        }
        else
        {
          sReturn += ('<option value="' + value + '">' + ValueList[value] + '</option>');
        }
      }
      sReturn += '</select>';
      sReturn += '<label type="text" for="' + id + '" value="' + id + '" style="display:none;">' + id + '</label>'; // Bug 15925 UMN a11y
      sReturn += '<span class="error" id="' + sErrorSpanID + '"/>';
      return sReturn;
      
    default:
      alert("Unsupported widget type: " + ValueListUI);
  }
  return "";
}

// 
// Bug 15423 UMN add payment selectors and filters
// <summary>
//     This function initializes the checkboxes on the page according to configured defaults.
// </summary>
// 
function init_checkboxes() {
  for (var sBlockIndex in oCBTBlocksData.asBlockNames) {
    var sBlockName = oCBTBlocksData.asBlockNames[sBlockIndex];
    var oBlock = oCBTBlocksData[sBlockName];
    var aoCBTColumns = oBlock.aoCBTColumns;
    var oCBTDataTableParams = oBlock.oCBTDataTableParams;
    var sBlockCss = "block_" + oCBTDataTableParams.sCBTBlockID; // Bug 13220
    var sRowSelectedSelector = "." + sBlockCss + " .prop_RESERVED_SELECTED input:checkbox";

    // set default for each block's checkAll and grandCheckAll
    for (var i = 0; i < aoCBTColumns.length; i++) {
      if (aoCBTColumns[i].sCBTPrompt === "RESERVED_SELECTED") {
        // Bug 15877 use row selector value from aaData 
        var bChkBoxDefault = (aoCBTColumns[i].sCBTDefault == "false") ? false : true;
        var sChkAllId = "#" + oCBTDataTableParams.sCBTBlockID + "_" + aoCBTColumns[i].sCBTTagName + "_xA";
        $(sChkAllId).prop('checked', bChkBoxDefault).val(bChkBoxDefault);
        // $('#grandCheckAll').prop('checked', bChkBoxDefault).val(bChkBoxDefault);  // TODO: need grand pay all to be configurable someday
      }
    }
    // Update the payment fields depending on whether the checkboxes were selected or sent back from inquiry
    // necessary to do this, as inquiry could send back values for payment field, but we don't want to
    // accept them; the selector controls whether they are accepted.
    $(sRowSelectedSelector).each(function () {
      // pass bFromInit as true so custom field defaults are applied properly
      update_selected($(this), null, false, false, true);
    });
  }

  // Bug 20309 UMN pre-select any items that had RESERVED_SELECT_QK
  var query_keys = get_query_keys(); // returns an object with tagname keys and values
  for (var key in query_keys) {
    $('td .' + key).each(function () {
      var jChkBox = $(this).closest('tr').find('.prop_RESERVED_SELECTED input:checkbox');
      if ($(this).text() === query_keys[key] && jChkBox.length > 0) {
        update_selected(jChkBox, true, true, true, false);
      }
    });
  }

  // Bug 23344 MJO - If exclusive block constraints are violated, show the message to the user to resolve them
  $('span.checkbox_sel input:checkbox').each(function () {
    $(this).data("exclusive", $(this).parents("table.exclusive").length == 1);
    $(this).data("block_id", $(this).closest("table.TABLE_block").attr("id"));
  }).change(function () { on_excl_chkbox_change(this); });
}

// Bug 20309 UMN get query_keys from page
// <summary>
//     This function gets the query_keys from the page if the user configured a custom field 
//     with a prompt = RESERVED_SELECT_QK.
// </summary>
// <returns> An object containing all the tagname keys with their values </returns>
function get_query_keys()
{
  var ret = {};
  var queryKeyDivs = $("div[id^='select_qk_'");
  $(queryKeyDivs).each(function() 
  {
    var query_key = $(this).attr("data-query-key");
    if (query_key !== undefined)
    {
      var qk = query_key.split(":");  // qk[0] will be key, qk[1] will be value
      if (qk.length === 2) ret[qk[0]] = qk[1];
    }
  });
  return ret;
}

// Bug 20309 UMN get query_key from page
// <summary>
//     This function gets the query_key from the page if the user configured a custom field 
//     with a prompt = RESERVED_SELECT_QK.
// </summary>
// <returns> An array with first element containing the key, and 2nd, the value </returns>
function get_query_value(tagname)
{
  var query_key = $("#select_qk_" + tagname).attr("data-query-key");
  if (query_key !== undefined)
  {
    var qk = query_key.split(":");  // qk[0] will be key, qk[1] will be value
    if (qk.length === 2)
      return qk[1];
  }
  return undefined;
}

// <summary>
//    This function updates the map of user checked checkboxes, which is used to restore state after a refilter_records.
//    We store them because datatables removes nodes from the DOM during filtering.
//    Added in Bug 19677 UMN
// </summary>
// <param name="jTrSelector" type="Object"> jQuery selector for the TR </param>
// <param name="jChkBox" type="Object"> jQuery selector for the checkbox </param>
// <param name="jAmtOrBalSelector" type="Object"> jQuery selector for the amount or the balance </param>
// <returns> </returns>
function update_user_checked_boxes(jTrSelector, jChkBox, jAmtOrBalSelector)
{
  if (jTrSelector.length < 1 || jChkBox < 1 || jAmtOrBalSelector < 1) return;
  
  // store the amounts so we don't lose them during filtering
  var sAmountOrBal = jAmtOrBalSelector.val() || "$0.00";
  var fRowAmountOrBal = parseFloat(sAmountOrBal.replace(/[$,]/g, ""));
  var classNames  = jTrSelector.closest(".TR_block").attr("class")
  if (isNaN(fRowAmountOrBal)) fRowAmountOrBal = 0.00;
  oUserCheckedBoxes[jChkBox.attr('name')] = { "checked": jChkBox.prop('checked'), "amount": fRowAmountOrBal, "classNames": classNames };
  // alert(JSON.stringify(oUserCheckedBoxes[jChkBox.attr('name')])); 
}

// <summary>
//    Refactoring. This function gets the balance selector to find a line-items balance.
// </summary>
// <param name="jTrSelector" type="Object"> jQuery selector for the TR </param>
// <returns> jBalanceSelector </returns>

function get_balance_selector(jTrSelector)
{
  var jAmtPaidSelector = jTrSelector.find('.prop_RESERVED_AMOUNT_PAID input');
  var jAmtPaidLblSelector = jTrSelector.find('.prop_RESERVED_AMOUNT_PAID label');
  var jBalanceSelector = jTrSelector.find('.prop_RESERVED_ADJUSTED_BALANCE');
  if (jBalanceSelector.length !== 1)
  {
    jBalanceSelector = jTrSelector.find('.prop_RESERVED_BALANCE');
    
    // if still don't have a balance, assume balance is in read_only amount paid field
    if (jBalanceSelector.length !== 1) jBalanceSelector = jAmtPaidSelector;

    // Bug 24180/25185 MJO - Fixed undefined value
    if (jBalanceSelector.attr("value") === undefined)
      jBalanceSelector.attr("value", jBalanceSelector.text());

    // Bug 23421/25185 UMN can't use isNaN on a string!
    // Bug 23469 MJO - Use the value that we just checked/set, not text
    var sRowBalance = jBalanceSelector.attr("value") || "$0.00";
    var fRowBalance = parseFloat(sRowBalance.replace(/[$,]/g, ""));
    
    // If we don't have a non-zero value in the input field, use the label
    if (jBalanceSelector.length !== 1 || isNaN(fRowBalance) || fRowBalance == "0.00")
      jBalanceSelector = jAmtPaidLblSelector;
  }
  return jBalanceSelector;
}
  
// <summary>
//    This function disables a line item from being paid if the balance is zero, and the BTT
//    amount type doesn't include zero in its currency type.
// </summary>
// <param name="jChkBox" type="Object"> jQuery selector for the checkbox </param>
// <returns> void </returns>
function disable_paying_lineitem(jChkBox)
{
  // true state is in hidden input field, cause forms only submit checkboxes that are true
  var jSelHidden = jChkBox.prev();
  var jTrSelector = jChkBox.closest('tr');
  var jAmtPaidSelector = jTrSelector.find('.prop_RESERVED_AMOUNT_PAID input');
  var jAmtPaidLblSelector = jTrSelector.find('.prop_RESERVED_AMOUNT_PAID label');
  var jBalanceSelector = get_balance_selector(jTrSelector);

  if (jAmtPaidSelector.length === 1 || jAmtPaidLblSelector.length === 1)
  {
    var sRowBalance = jBalanceSelector.text() || "$0.00";
    var fRowBalance = parseFloat(sRowBalance.replace(/[$,]/g, ""));
    if (isNaN(fRowBalance)) fRowBalance = 0.00;

    // Bug 17596/19240 disable $0 balances from being checkable (allowing positives or negatives)
    if (jBalanceSelector.length === 1 && fRowBalance === 0 &&
        (jAmtPaidSelector.hasClass("currencypositive") || 
         jAmtPaidSelector.hasClass("currencypositiveandnegative") || 
         jAmtPaidSelector.hasClass("currencynegative")))
    {
      jChkBox.prop('checked', false).val(false).attr("disabled", true);
      jAmtPaidSelector.attr("disabled", true);
      if (jSelHidden.length === 1) jSelHidden.val(false);
      jTrSelector.removeClass("highlight"); // de-highlight row
      return false;
    }
  }
}

// 
// Bug 15423 UMN add payment selectors and filters
// <summary>
//     This function updates the the checked state of a checkbox (payment selector).
//     It uses the (ugh) global to get the checked states.
// </summary>
// <param name="jChkBox" type="Object"> jQuery selector for the checkbox </param>
// <param name="tChkVal" type="truthiness"> 
//     If not null, a bool value to set checkbox to. If null, the value to set the checkbox is pulled from DOM.
// </param>
// <param name="bDoRecalc" type="bool"> Should the datatable totals be recalculated? </param>
// <param name="bDidUserCheck" type="bool"> Did the user check this box? If so, save it. </param>
// <param name="bFromInit" type="bool"> Is the caller an init routine? </param>
// 
function update_selected(jChkBox, tChkVal, bDoRecalc, bDidUserCheck, bFromInit)
{
  // don't change state if checkbox is disabled
  if (!bFromInit && jChkBox.prop('disabled')) return true;
  
  // true state is in hidden input field, cause forms only submit checkboxes
  // that are true
  var jSelHidden = jChkBox.prev();
  var jTrSelector = jChkBox.closest('tr');
  var jAmtPaidSelector = jTrSelector.find('.prop_RESERVED_AMOUNT_PAID input');
  var jBalanceSelector = get_balance_selector(jTrSelector);
  var jAmtPaidLblSelector = jTrSelector.find('.prop_RESERVED_AMOUNT_PAID label');

  // tChkVal is null when user clicks it; else true/false if called by another method
  if (tChkVal === null) tChkVal = jChkBox.is(":checked");

  // return if this is being called on a checkAll box
  if (jChkBox.attr('class') === "checkAll") 
  {
    // update the state of the checkbox 
    jChkBox.prop('checked', tChkVal).val(tChkVal);
    
    // update the hidden input field
    if (jSelHidden.length === 1) jSelHidden.val(tChkVal);
    return true;
  }
  
  // disable checkbox and amount if balance is 0, and amt paid currency type doesn't include 0
  // disable_paying_lineitem(jChkBox);
  
  // update the state of the checkbox 
  jChkBox.prop('checked', tChkVal).val(tChkVal);
  
  // update the hidden input field
  if (jSelHidden.length === 1) jSelHidden.val(tChkVal);
  
  // Finally update payment fields
  if (jAmtPaidSelector.length === 1)
  {
    if (tChkVal) 
    {
      // Bug 15877 only update amount as balance if user checked or we're coming from init
      if (jBalanceSelector.length === 1 && (bFromInit || bDidUserCheck))
      {
        // Bug 23421/25185 MJO - For amount field, get the value of the label
        var amtPaidVal = jBalanceSelector.text()  || "$0.00";

        // Bug 23421/25185 UMN can't use isNaN on a string!
        var fAmtPaidVal = parseFloat(amtPaidVal.replace(/[$,]/g, ""));
        if (fAmtPaidVal == "0.00" || isNaN(fAmtPaidVal)) 
        {
          attr = jBalanceSelector.attr("value");
          if (typeof attr !== typeof undefined && attr !== false)
            amtPaidVal = formatCurrency(attr);
        }
        jAmtPaidSelector.val(amtPaidVal);
        if (jAmtPaidLblSelector.length === 1) 
          jAmtPaidLblSelector.html(amtPaidVal);
      }
      // highlight row
      jTrSelector.addClass("highlight");
    }
    else 
    {
      // untick checkAll/grandChkAll since at least one payment is de-selected
      var jChkAllSelector = jTrSelector.parent().prev().find('.checkAll');
      $(jChkAllSelector).prop('checked', false).val(tChkVal); 
      $('#grandCheckAll').prop('checked', false).val(tChkVal);
      jAmtPaidSelector.val("0.00");
      if (jAmtPaidLblSelector.length === 1) jAmtPaidLblSelector.html("$0.00");
      
      // de-highlight row
      jTrSelector.removeClass("highlight");
    }
    // no need to update unless amtpaid has changed
    if (bDoRecalc) update_PB_ENTERPRISE_PAYMENT_SCREEN_info();
  }

  // remember that we checked this
  if (bDidUserCheck)
  {
    // Bug 19677 UMN store the amounts so we don't lose them during filtering
    update_user_checked_boxes(jTrSelector, jChkBox, jAmtPaidSelector);  
  }

  // alert(jSelHidden.attr('name') + ": " + jSelHidden.val());
  return true;
}

// 
// Bug 15423 UMN add payment selectors and filters
// Bug 19677 UMN rewrite
// <summary>
//     This function restores the checked state of any user-checked checkboxes.
//     It uses the (ugh) global to get the checked states.
// </summary>
// 
function restore_selected()
{
  for (var name in oUserCheckedBoxes)
  {
    var oCheckBox = document.getElementsByName(name);
    if (oCheckBox.length === 1 && oCheckBox !== undefined)
    {
      // can't call update_selected, because update_selected tries to save state
      // by calling update_user_checked_boxes, which is not good, Bob!
      // TODO: refactor below, which is taken and modified from update_selected
      var checkedAndAmount = oUserCheckedBoxes[name];
      var jChkBox = $(oCheckBox);
      var jSelHidden = $(oCheckBox).prev(); // true state
      var jTrSelector = jChkBox.closest('tr');
      var jAmtPaidSelector = jTrSelector.find('.prop_RESERVED_AMOUNT_PAID input');
      var jAmtPaidLblSelector = jTrSelector.find('.prop_RESERVED_AMOUNT_PAID label');

      jChkBox.prop('checked', checkedAndAmount.checked).val(checkedAndAmount.checked);
      if (jSelHidden.length === 1) jSelHidden.val(checkedAndAmount.checked);
      jAmtPaidSelector.val(checkedAndAmount.amount);
      if (jAmtPaidLblSelector.length === 1) jAmtPaidLblSelector.html(checkedAndAmount.amount);
      if (checkedAndAmount.checked)
        jTrSelector.addClass("highlight");
      else
        jTrSelector.removeClass("highlight");
    }
  }
}

// 
// Bug 15423 UMN add payment selectors and filters
// <summary>
//     This function sets the master/grand check all checkbox. Doing so will set every 
//     block's check all checkbox.
// </summary>
// <param name="oChkBox" type="Object"> DOM input node for the checkbox </param>
// <param name="tChkAllVal" type="truthiness"> 
//     If not null, a bool value to set checkbox to. If null, the value to set the checkbox is pulled from DOM.
// </param>
// <param name="bDoRecalc" type="bool"> Should the datatable totals be recalculated? </param>
// <param name="bDidUserCheck" type="bool"> Did the user check this box? </param>
// 
function set_grand_checkall(oChkBox, tChkAllVal, bDoRecalc, bDidUserCheck)
{
  var jChkBox = $(oChkBox);
  if (tChkAllVal === null) tChkAllVal = jChkBox.is(":checked");

  // finds each checkAll checkbox on page
  $('.TABLE_block .checkAll').each(function() 
  {
    if (!set_block_checkall($(this), tChkAllVal, false, bDidUserCheck, bDidUserCheck))
      return false;
  });
  if (bDoRecalc) update_PB_ENTERPRISE_PAYMENT_SCREEN_info();
  return true;
}

// 
// Bug 15423 UMN add payment selectors and filters
// <summary>
//     This function sets a block's check all checkbox. Doing so will set all of 
//     the block's checkboxes to the value in the block's check all checkbox.
// </summary>
// <param name="oChkBox" type="Object"> DOM input node for the checkbox </param>
// <param name="tChkAllVal" type="truthiness"> 
//     If not null, a bool value to set checkbox to. If null, the value to set the checkbox is pulled from DOM.
// </param>
// <param name="bDoRecalc" type="bool"> Should the datatable totals be recalculated? </param>
// <param name="bDidUserCheck" type="bool"> Did the user check this box? </param>
// <param name="bCheckExclusive" type="bool"> Should exclusive blocks be checked? </param>
// 
function set_block_checkall(oChkBox, tChkAllVal, bDoRecalc, bDidUserCheck, bCheckExclusive) 
{
  var jChkBox = $(oChkBox);
  
  // pull from DOM; no need to set
  if (tChkAllVal === null) 
  {
    tChkAllVal = jChkBox.is(":checked");  
  }
  else if (!jChkBox.prop('disabled'))
  {
    jChkBox.prop('checked', tChkAllVal).val(tChkAllVal);      // set to passed in val
  }
  
  // finds each checkbox in a checkbox_sel span
  var jBlockChkboxes = jChkBox.closest('.TABLE_block').find(".checkbox_sel input:checkbox");
  $(jBlockChkboxes).each(function() 
  {
    update_selected($(this), tChkAllVal, false, bDidUserCheck, false);
  });
  
  if (bDoRecalc) update_PB_ENTERPRISE_PAYMENT_SCREEN_info();

  if (bCheckExclusive === true) {
    $(jBlockChkboxes).each(function () {
      if (!on_excl_chkbox_change($(this)))
        return false;
    });
  }

  return true;
}

// 
// Bug 15423 UMN add payment selectors and filters
// <summary>
//     This function resets all the filters. Called by validate_and_cover in main.js, and refilter_records. 
// </summary>
// 
function reset_all_filters()
{
  $('.TABLE_block').each(function() 
  {
    // Bug 26333 UMN skip fixed blocks too
    if ($(this).hasClass("block_information") || $(this).hasClass("block_fixed")) // skip info and fixed blocks
	      return;
    var oTable = $(this).dataTable();
    var oSettings = oTable.fnSettings();
    for (iCol = 0; iCol < oSettings.aoPreSearchCols.length; iCol++) 
    {
      oSettings.aoPreSearchCols[ iCol ].sSearch = '';
    }
    // oTable.fnDraw();
    oTable.fnFilter("", null);
  });
}

// 
// Bug 15423/15560 UMN add payment selectors, filters and combo filters
// <summary>
//     This function filters/refilters a datatable given the search string and a tagname
//     representing the column to search. The column tagname will need to be mapped to a
//     column number for each table. 
// </summary>
// <param name="sSrchText" type="string"> The text to search for when filtering </param>
// <param name="sTagName" type="string"> The tagname of the column </param>
// 
function refilter_records(sSrchText, sTagName)
{
  var bResetFilter = false;
  
  // uncheck all checkboxes so user doesn't get confused, per Jeff; restore_selected will recalc
  set_grand_checkall($('#grandCheckAll')[0], false, false, false);
  
  // if the srchstring is the columnname, reset the filter to "" for the column
  if (sTagName === sSrchText) bResetFilter = true;
  
  // loop through each table, and apply filter
  $('.TABLE_block').each(function() 
  {
    // Bug 26333 UMN skip fixed blocks too
    if ($(this).hasClass("block_information") || $(this).hasClass("block_fixed")) // skip info and fixed blocks
      return; 
    var oTable = $(this).dataTable(); // get the DT instance
    var sTableId = oTable.prop('id').replace("TableBlock", "oBlock");    
    var iColNum = get_colnumber(sTableId, sTagName);

    // Bug 13293 UMN resolve Ann comment 13
    if (iColNum === null) return;
    
    if (bResetFilter) 
      oTable.fnFilter("", iColNum);
    else if (iColNum === 'all')  // Bug 23215 UMN
      oTable.fnFilter(unescape(sSrchText, null)); // handle escaped data
    else 
      oTable.fnFilter(unescape(sSrchText), iColNum); // handle escaped data
  });
  
  // Bug 19677 UMN restore any user-selected checkboxes
  restore_selected();

  update_PB_ENTERPRISE_PAYMENT_SCREEN_info();  
}

// 
// Bug 15423/15560 UMN add payment selectors, filters and combo filters
// <summary>
//     This function returns the the column number of a tagname, given the tagname and the sTableId.
//     The string sTableId will be in the form oBlock<BlockCustomFieldTagName>. Eg: oBlockAccount1.
//     Returns null if not found, or if sTagName is null
// </summary>
// <param name="sTableId" type="string"> The tableId that CASL inserts into page (oBlock...) </param>
// <param name="sTagName" type="string"> The tagname of the column </param>
// 
function get_colnumber(sTableId, sTagName)
{
  var iColNum = null;
  if (sTagName === null || sTagName === undefined) 
    return iColNum;
  else if (sTagName === 'all')  // Bug 23215 UMN
    return sTagName;
  
  for (var i = 0; i < oCBTBlocksData[sTableId].aoColumns.length && iColNum === null; i++)
  {
    if (oCBTBlocksData[sTableId].aoColumns[i].mDataProp === sTagName) iColNum = i;
  }
  return iColNum;
}

// 
// Bug 15423 UMN add payment selectors and filters
// <summary>
//     This function creates the event handlers for checkbox payment selectors.
// </summary>
// <param name="oFilterSelector" type="Object"> jQuery selector of a checkbox input element </param>
// 
function add_payment_selector_handlers(oChkSelector)
{
  if (oChkSelector.length > 0)
  {
    // individual checkbox handler
    oChkSelector.change(function()
    {
      // recalc, and update oUserCheckedBoxes cause user clicked this
      update_selected($(this), null, true, true, false);
    });
  }
}

// 
// Bug 15423 UMN add payment selectors and filters
// <summary>
//     This function creates the event handlers for a standard (not combo) filter. Standard 
//     filters search through all the hidden and visible fields in a given table.
// </summary>
// 
function add_payment_filter_handlers()
{
  $('.RESERVED_FILTER').each(function() 
  {
    var oFilterSelector = $(this);
    var oSrchBtnSelector = oFilterSelector.next();
    if (oSrchBtnSelector.length > 0 && oSrchBtnSelector.hasClass('filter_search'))
    {
      oFilterSelector.val(oSrchBtnSelector.val());
      oSrchBtnSelector.click(function()  // handler! don't depend on outer scope variables
      {
        refilter_records($(this).prev().val(), 'all');
      });
    }

    oFilterSelector.keyup(function(keyEvent) // handler! don't depend on outer scope variables
    {
      var _this = $(this);
      if (keyEvent.which == 13) keyEvent.preventDefault();
      setTimeout(function() {
        refilter_records(_this.val(), 'all');
      }, iFilterWait);
    }).keydown(function(keyEvent)
    {
      if (keyEvent.which == 13) keyEvent.preventDefault();
    });
  });
}

// 
// Bug 15560 UMN Add Combo filters for Base/Banner
// <summary>
//     This function creates a dropdown for the combo filter, by looping thru all the datatables
//     on a page, and grabbing all the column values in that column. It then sorts that array and
//     calls create_column_select_filter to create all the dropdown option values. The
//     function create_column_select_filter creates the change event hanlder for the dropdown.
// </summary>
// 
function add_payment_combo_filter_handlers()
{
  // loop thru each filter that the user has configured
  $('.COMBO_FILTER').each(function() 
  {
    var oFilterSelector = $(this);
    var sTagName = oFilterSelector.attr("data-colFilter-tagname");
    var sSelectedVal = oFilterSelector.attr("data-colFilter-selected");
    var sTagLabel = oFilterSelector.attr("data-colFilter-label");
    var aAllColumnValues = [];

    if (sTagName === undefined) return;
    
    // Bug 20309 UMN make the dropdown filter use any auto selected query keys as sSelectedVal
    var sQKSelVal = get_query_value(sTagName); // sQKSelVal[0] will be key, sQKSelVal[1] will be value
    if (sQKSelVal !== undefined)
    {
      sSelectedVal = sQKSelVal;
    }

    // loop thru each block and get all of the values for that column, and merge to the aAllColumnValues array
    $('.TABLE_block').each(function() 
    {
      var tableSelector = $(this);
      
      // Bug 26333 UMN skip fixed blocks too
      if (tableSelector.hasClass("block_information") || tableSelector.hasClass("block_fixed")) // skip info and fixed blocks
        return; 

      var oTable = tableSelector.dataTable(); // get the DT instance
      var sTableId = oTable.prop('id').replace("TableBlock", "oBlock");
      var iColNum = get_colnumber(sTableId, sTagName);
      
      // if table doesn't have the column, skip. Bug 23215 UMN add all
      if (iColNum === null || iColNum === 'all') return;
      
      var asTableColumnValues = get_all_column_values(oTable, sTagName, false, true);
      
      // add to aAllColumnValues only if not already there
      for (var i = 0; i < asTableColumnValues.length; i++)
      {
        if (jQuery.inArray(asTableColumnValues[i], aAllColumnValues) === -1) aAllColumnValues.push(asTableColumnValues[i]);
      }
    });
   
    // sort the resulting values array before creating the select drop-down
    if (aAllColumnValues.length !== 0) 
    {
      // eventually we may need to capture the type and sort depending on type
      aAllColumnValues.sort();

      // now create each dropdown select and change event handler
      create_column_select_filter(aAllColumnValues, oFilterSelector, sTagName, sTagLabel, sSelectedVal);
      
      // if sSelectedVal != sTagName, then user has specified a value to filter on, so refilter records
      refilter_records(sSelectedVal, sTagName);
    }
    else
    {
      // Bug 23229 UMN remove TR containing combo filter if no values 
      oFilterSelector.closest('tr').remove();
    }
  });
}

// 
// Bug 15560 UMN Add Combo filters for Base/Banner
// <summary>
//     This function was partly taken from the column filter plugin, but I had to
//     modify it because Jeff wants one filter dropdown for all PB tables on the
//     page. The challenge is that a given CF may be in col1 in tableA, but col3 in table2.
//     Anyway, this function creates a select dropdown populated with values from the column. 
//     It is passed the array of strings that is used to create all the option elements.
// </summary>
// <param name="asSortedColumnValues" type="Object"> 
//     Array of strings that will be used in the dropdown
// </param>
// <param name="oFilterSelector" type="Object"> jQuery selector of a <select> element </param>
// <param name="sTagName" type="string"> Tag name. Eg. "TPAY_TERM" </param>
// <param name="sTagLabel" type="string"> Dropdown label. Eg. "Term Code" </param>
// <param name="sSelectedVal" type="string"> Which item to select. Pass sTagName to set to default </param>
// 
function create_column_select_filter(asSortedColumnValues, oFilterSelector, sTagName, sTagLabel, sSelectedVal) 
{
  if (sTagLabel === "" || sTagLabel === undefined) sTagLabel = sTagName;
  var sSelected = (sSelectedVal == sTagName) ? 'selected' : '';
  var sOptions = '<option ' + sSelected + ' value="' + sTagName + '" class="search_init">' + sTagLabel + '</option>';
  for (var j = 0; j < asSortedColumnValues.length; j++) 
  {
    sSelected = (sSelectedVal == escape(asSortedColumnValues[j])) ? 'selected' : '';
    sOptions += '<option ' + sSelected + ' value="' + escape(asSortedColumnValues[j]) +
      '">' + asSortedColumnValues[j] + '</option>';
  }

  // append the options onto the oFilterSelector dropdown (created in CASL)
  oFilterSelector.append(sOptions);
  oFilterSelector.hide().show();    // handle stupid IE rendering issue

  // add change event handler
  oFilterSelector.change(function () 
  {
    var sVal = $(this).val();
    var sTagName = $(this).attr("data-colFilter-tagname");
    if (sVal !== "") 
    {
      $(this).removeClass("search_init");
    } 
    else 
    {
      $(this).addClass("search_init");
    }  
    refilter_records(sVal, sTagName);
  });
}

// 
// Bug 15560 UMN Add Combo filters for Base/Banner
// <summary>
//   Return an array of strings (a set of values for a column) from a given datatable
// </summary>
// <param name="oTable" type="Object"> jQuery DataTable object </param>
// <param name="sTagName" type="string"> tag name of the column </param>
// <param name="bFiltered" type="bool"> Return values only from the filtered rows </param>
// <param name="bIgnoreEmpty" type="bool"> Ignore empty cells </param>
// 
function get_all_column_values(oTable, sTagName, bFiltered, bIgnoreEmpty) 
{
  var oSettings = oTable.fnSettings();
  var asColumnValues = [];               // returned result

  if (sTagName === undefined || sTagName === null) return [];

  // by default we do want to only look at filtered data
  if (typeof bFiltered == "undefined") bFiltered = true;

  // by default we do not want to include empty values
  if (typeof bIgnoreEmpty == "undefined") bIgnoreEmpty = true;

  // list of rows which we're going to loop through
  var aiRows;
  if (bFiltered === true) aiRows = oSettings.aiDisplay;  // use only filtered rows
  else aiRows = oSettings.aiDisplayMaster;               // use all rows

  // loop thru every row
  for (var i = 0, c = aiRows.length; i < c; i++) 
  {
    var iRow = aiRows[i];
    var aData = oTable.fnGetData(iRow);  // get row's value from datatable
    var sValue = aData[sTagName];        // extract the column value from the row

    // may be trying to filter an input element, so get element's value
    if (sValue.indexOf("<") === 0)
    {
      sValue = $(sValue).val();
    }
    
    if (bIgnoreEmpty === true && sValue.length === 0) continue;     // ignore empty values?
    else if (jQuery.inArray(sValue, asColumnValues) > -1) continue; // don't add duplicates
    else asColumnValues.push(sValue);  // append value to array
  }
  return asColumnValues;
}

//////// Begin Bug 19024 UMN Autodistribute payments code

function change_to_manual_dist() 
{
  "use strict";
  var dist_total_span_ui_item = document.getElementById("autodistribute-dist-total_span");
  dist_total_span_ui_item.style.display = "none";
}

function change_to_auto_dist()
{
  "use strict";
  var dist_total_span_ui_item = document.getElementById("autodistribute-dist-total_span");
  dist_total_span_ui_item.style.display = "inline";
}

//
// Bug 19024 UMN Add Autodistribution
// <summary>
//   Find all the balance and adjusted balance items, and add ids to the TDs so that the autodistribute can use
//   those id in its processing (finding by ids is way more effecient than by class).
// </summary>
// 
function init_autodistribute()
{
  "use strict";
  for (var i = 0; i < oCBTBlocksData.asBlockNames.length; i++)
  {
    var sAdjBalName = "";
    var sBalName = "";
    var sBlockName = oCBTBlocksData.asBlockNames[i];
    var oCBTDataTableParams = oCBTBlocksData[sBlockName].oCBTDataTableParams;
    var sBlockTableSelector = $("#" + oCBTDataTableParams.sTableID);
    
    if (oCBTDataTableParams.sBlockType !== "block_payment") continue;
    
    // find the tagnames for bal and adjbal
    for (var j = 0; j < oCBTBlocksData[sBlockName].aoColumns.length; j++) 
    {
      if (oCBTBlocksData[sBlockName].aoColumns[j].sClass.indexOf("prop_RESERVED_ADJUSTED_BALANCE") !== -1)
        sAdjBalName = oCBTBlocksData[sBlockName].aoCBTColumns[j].sCBTTagName;
      else if (oCBTBlocksData[sBlockName].aoColumns[j].sClass.indexOf("prop_RESERVED_BALANCE") !== -1)
        sBalName = oCBTBlocksData[sBlockName].aoCBTColumns[j].sCBTTagName;
    }
    if (sAdjBalName !== "")
    {
      // traverse down block's adj balance COLUMN
      sBlockTableSelector.find('td.prop_RESERVED_ADJUSTED_BALANCE').each(function(iRowIndex) {
        var sBalId = "args." + oCBTDataTableParams.sID + "." + iRowIndex + "." + sAdjBalName;
        $(this).attr('id', sBalId);
      });
    }

    if (sBalName !== "")
    {
      // traverse down block's balance COLUMN
      sBlockTableSelector.find('td.prop_RESERVED_BALANCE').each(function(iRowIndex) {
        var sBalId = "args." + oCBTDataTableParams.sID + "." + iRowIndex + "." + sBalName;
        $(this).attr('id', sBalId);
      });
    }
  }
}

// UMN 19024 Removed unused ability to configure this
// Sets: oCBTBlocksData.aaBlockDist and oCBTBlocksData.oBlockFilteredIds

// UMN TODO: add error message if no groups are found!

function default_autodist_filter()
{
  "use strict";
  oCBTBlocksData.aaBlockDist = eval(auto_dist_get_attribute("autodistribute-dist-total", "data-autodist-groups", "[]"));
  
  // return if oCBTBlocksData.aaBlockDist is empty
  if (oCBTBlocksData.aaBlockDist.length === 0)
  {
    enqueue_alert_message("Error: No configured groups.\nThe configuration may have used group names instead of block custom field ids.");
  }
  
  oCBTBlocksData.aaBlockDist = validate_autodist_groups(oCBTBlocksData.aaBlockDist);
  oCBTBlocksData.oBlockFilteredIds = {};
  oCBTBlocksData.oBlockFilteredIds.AcrossGroup = [];
  
  // AcrossGroup will look like:
  //    [20110813,args.GC.3.amount,args.GC.0.AMOUNT_DUE,
  //    20111010,args.GC.0.amount,args.GC.1.AMOUNT_DUE]

  // process in order from Config
  for (var i = 0, iLen = oCBTBlocksData.aaBlockDist.length; i < iLen; i++)
  {
    if (!oCBTBlocksData.aaBlockDist[i])
      continue;
    var sBlockName = oCBTBlocksData.aaBlockDist[i][0];
    
    // Uncheck all in the group before applying filters or distribution
    var sOblockName = "oBlock" + sBlockName;
    
    // need to test if sOblockName is in oCBTBlocksData, because block may not be returned in inquiry
    if (oCBTBlocksData[sOblockName] !== undefined)
    {
    for (var j = 0, jLen = oCBTBlocksData[sOblockName].aoCBTColumns.length; j < jLen; j++)
    {
      if (oCBTBlocksData[sOblockName].aoCBTColumns[j].sCBTPrompt === "RESERVED_SELECTED")
      {
        var sGrpChkAllId = "#" + sBlockName + "_" + oCBTBlocksData[sOblockName].aoCBTColumns[j].sCBTID + "_xA";
        $(sGrpChkAllId).prop('checked', false).val(false);
        set_block_checkall($(sGrpChkAllId)[0], false, false, false); // might need recalc, so set to true
      }
    }   
    }
    oCBTBlocksData.oBlockFilteredIds[sBlockName] = default_block_filter(sBlockName, oCBTBlocksData.oBlockFilteredIds.AcrossGroup);
  }
}

function default_block_filter(sBlockName, oAcrossGroupDistList)
{
  "use strict";
  var aaAmtAdjBalIds = [];
  var sBlockPBName = "oBlock" + sBlockName;
  var sBlockSortFieldSelector;
  var sBlockSortFieldType;
  var oCBTBlockData = oCBTBlocksData[sBlockPBName];
  var sBlockSortField = "";
  var sBlockSortFieldDir = "";
  
  // group may be in Config, but not in inquiry results
  if (oCBTBlockData !== undefined)
  {
    var oCBTDataTableParams = oCBTBlockData.oCBTDataTableParams;
    var sBlockType = oCBTDataTableParams.sBlockType;
    var sBlockCss = "block_" + oCBTDataTableParams.sCBTBlockID; // Bug 13220
    var sRowAmountPaidSelector = "." + sBlockCss + " .prop_RESERVED_AMOUNT_PAID input";
    var sRowAdjBalanceSelector = ".prop_RESERVED_ADJUSTED_BALANCE";
    var sRowBalanceSelector = ".prop_RESERVED_BALANCE";  
    sBlockSortField = oCBTDataTableParams.sBlockSortField;
    sBlockSortFieldDir = oCBTDataTableParams.sBlockSortFieldDir;
    
    // Bug 24328 UMN check if it starts with block_information
    if (sBlockType.lastIndexOf("block_information", 0) === 0)
      return aaAmtAdjBalIds;
    
    // if not set, set it to first visible column
    if (sBlockSortField === "" || sBlockSortField === undefined)
    {
      sBlockSortField = oCBTBlockData.aoCBTColumns[0].sCBTTagName;
      sBlockSortFieldType = oCBTBlockData.aoCBTColumns[0].sCBTType;
    }
    sBlockSortFieldSelector = "." + sBlockSortField;
    
    if (sBlockSortFieldDir === "" && sBlockSortFieldDir === undefined)
      sBlockSortFieldDir = "asc";

    $(sRowAmountPaidSelector).each(function(iRowIndex) {
      var sAmtPaidID = $(this).attr('id');
      var sBalId;
      var sSortField = "";
      var rowSel = $(this).closest('tr');
      if (rowSel.find(sRowAdjBalanceSelector).length)
        sBalId = rowSel.find(sRowAdjBalanceSelector).attr('id');
      else
        sBalId = rowSel.find(sRowBalanceSelector).attr('id');
        
      if (sBlockSortFieldSelector !== undefined && rowSel.find(sBlockSortFieldSelector).length)
        sSortField = rowSel.find(sBlockSortFieldSelector).text();
      
      // get sort field's type
      if (sBlockSortFieldType === undefined)
      {
        for (var i = 0; i < oCBTBlockData.aoCBTColumns.length; i++) {
          if (oCBTBlockData.aoCBTColumns[i].sCBTTagName === sBlockSortField)
          {
            sBlockSortFieldType = oCBTBlockData.aoCBTColumns[i].sCBTType;
            break;
          }
        }
      }
      
      if (sBlockSortFieldType === "Type.date") sSortField = format_date_to_sort(sSortField);
      var aAmtAdjBalId = [sSortField, sAmtPaidID, sBalId];
      aaAmtAdjBalIds.push(aAmtAdjBalId);        // put record into specific group distribution list 
      oAcrossGroupDistList.push(aAmtAdjBalId);  // put record into all group distribution list 
    });    
  }
  
  // now sort the returned array
  if (sBlockSortField !== "" && sBlockSortField !== undefined)
  {
    if (sBlockSortFieldDir === "asc")
    {
      aaAmtAdjBalIds.sort(asc_sort);
      oAcrossGroupDistList.sort(asc_sort);
    }
    else
    {
      aaAmtAdjBalIds.sort(desc_sort);
      oAcrossGroupDistList.sort(desc_sort);
    }
  }
  
  return aaAmtAdjBalIds;   
}

// returns remaining unused balance
function auto_distribute_group(groupAmtToDist, aaAmtAdjBalIds)
{
  "use strict";
  var origGroupAmtToDist = groupAmtToDist;
  for (var i = 0; i < aaAmtAdjBalIds.length; i++)
  {
    if (aaAmtAdjBalIds[i][1] === undefined || aaAmtAdjBalIds[i][2] === undefined)
      continue;

    var amountSelector = getItem(aaAmtAdjBalIds[i][1]);
    var balance = getItem(aaAmtAdjBalIds[i][2]).innerHTML;
    
    balance = parseFloat(balance.replace(/[$,]/g, ""));
    
    amountSelector.value = 0.00;
    if (balance > 0.00 && groupAmtToDist >= balance)
    {
      amountSelector.value = Math.round(balance * 100) / 100;
      // Bug 19770 UMN update label
      if (amountSelector.nextSibling && amountSelector.nextSibling.nodeName == "LABEL")
        amountSelector.nextSibling.innerText = formatCurrency(amountSelector.value);
      groupAmtToDist = Math.round((groupAmtToDist - balance)* 100) / 100;
    }
    else if (balance > 0.00 && groupAmtToDist < balance)
    {
      amountSelector.value = Math.round(groupAmtToDist * 100) / 100;
      // Bug 19770 UMN update label
      if (amountSelector.nextSibling && amountSelector.nextSibling.nodeName == "LABEL")
        amountSelector.nextSibling.innerText = formatCurrency(amountSelector.value);
      groupAmtToDist = 0.00;
    }
  }
  return origGroupAmtToDist - groupAmtToDist;
}

function auto_distribute(amountToDist)
{
  "use strict";
  amountToDist = parseFloat(amountToDist.replace(/[$,]/g, ""));
  if (isNaN(amountToDist) || amountToDist < 0.00 || amountToDist === 0)
    amountToDist = 0.00;
  var amountToDistFromInput = amountToDist; // Bug 5223
  var amountActuallyDist = 0.00;
  var totAmountActuallyDist = 0.00;
  
  var sDistAcrossGroup = auto_dist_get_attribute("autodistribute-dist-total", "data-autodist-apply-mode", "Across"); 
  
  if (amountToDist === 0.00)
    return true;

  // Call query filter, and cache results
  if (oCBTBlocksData.oBlockFilteredIds === undefined)
    default_autodist_filter();
  
  // distribute amount across groups
  if (sDistAcrossGroup == "Across")  
  {
    amountActuallyDist = auto_distribute_group(amountToDist, oCBTBlocksData.oBlockFilteredIds.AcrossGroup);
    amountToDist -= amountActuallyDist;
    totAmountActuallyDist += amountActuallyDist;
  }
  else  // distribute Within
  {
    for (var i = 0; i < oCBTBlocksData.aaBlockDist.length; i++)
    {
      if (!oCBTBlocksData.aaBlockDist[i])
        continue;
      var sBlockName = oCBTBlocksData.aaBlockDist[i][0];
      var sBlockPBName = "oBlock" + sBlockName;
      var oCBTBlockData = oCBTBlocksData[sBlockPBName];
    
      // group may be in Config, but not in inquiry results
      if (oCBTBlockData === undefined)
        continue;

      var oCBTDataTableParams = oCBTBlockData.oCBTDataTableParams;
      if (oCBTDataTableParams.sBlockType !== "block_payment")
        continue;

      var blockDistPct = oCBTBlocksData.aaBlockDist[i][1];
      var blockAmountToDist = amountToDist;
      
      // blockdist % will be expressed as number from 0-100.
      if (blockDistPct !== null)
      {
        // UMN ensure percentage is taken from original amount input by user
        blockAmountToDist = amountToDistFromInput * (blockDistPct / 100.0);
      }
      // keep track
      amountActuallyDist = auto_distribute_group(blockAmountToDist, oCBTBlocksData.oBlockFilteredIds[sBlockName]);
      amountToDist -= amountActuallyDist;
      totAmountActuallyDist += amountActuallyDist;
    }  
  }

  // update section info
  update_PB_ENTERPRISE_PAYMENT_SCREEN_info();
  
  if (amountToDistFromInput > totAmountActuallyDist)
  {
    var alert_dist_total_message;
    var bBlockDistPctIsOn = auto_dist_get_attribute("autodistribute-dist-total", "data-autodist-pct-dist", "false"); 
    if (totAmountActuallyDist === 0)
      alert_dist_total_message = "No distribution was made likely due to configuration error.";
    else if (bBlockDistPctIsOn === "true")
      alert_dist_total_message = "Group percentage allocations have reduced the payment amount that was distributed.";
    else
      alert_dist_total_message = "The amount entered exceeds the adjusted balance. It has been changed to the adjusted balance.";
    enqueue_alert_message(alert_dist_total_message);
  }
  return true;  
}

function auto_dist_get_attribute(element_id, attrib, def_val)
{
  var el = document.getElementById(element_id);
  if (el === undefined || el === "") return def_val;
  else return el.getAttribute(attrib) || def_val;
}

function validate_autodist_groups(aaBlockDist)
{
  var aaBlockDistRet = [];
  var sMessage = "";
  var blockDistPctTotal = 0;
  
  for (var i = 0; i < aaBlockDist.length; i++)
  {
    var sBlockName = oCBTBlocksData.aaBlockDist[i][0];
    var blockDistPct = oCBTBlocksData.aaBlockDist[i][1];
    blockDistPctTotal += blockDistPct;
    
    if (blockDistPct < 0) sMessage += sBlockName + " percentage cannot be negative.\n";
    else if (blockDistPct > 100) sMessage += sBlockName + " percentage cannot be > 100%.\n";
    else if (blockDistPctTotal > 100) sMessage += "Total distributed percentage cannot be > 100%.\n";
    else aaBlockDistRet.push(aaBlockDist[i]);
  }
  if (sMessage !== "") enqueue_alert_message("Error in iPayment configuration. Ignoring groups with these errors:\n" + sMessage);
  return aaBlockDistRet;
}

// Bug 14052 SX return date in YYYYMMDD format so can be sorted
function format_date_to_sort(sDate)
{
  // UMN check if already in YYYYMMDD format (S&W). If so, just return.
  if (already_YYYYMMDD(sDate)) return sDate;
  
  var oDate = new Date(sDate);
  var m = (oDate.getMonth() + 1 );
    m = (m < 10) ? '0' + m : m;
  var d = (oDate.getDate());
    d = (d < 10) ? '0' + d : d;
  var y = (oDate.getFullYear());
  var R = '' + y + m + d ;
  return R;
}

function already_YYYYMMDD(str) {
  var y = str.substr(0,4),
      m = str.substr(4,2) - 1,
      d = str.substr(6,2);
  var D = new Date(y,m,d);
  return (D.getFullYear() == y && D.getMonth() == m && D.getDate() == d) ? true : false;
}

function asc_sort(a, b)
{
  return (a === b ? 0 : (a < b ? -1 : 1));
}

function desc_sort(a, b)
{
  return (a === b ? 0 : (a > b ? -1 : 1));
}

//////// END Bug 19024 UMN Autodistribute

//////// Bug 18719 UMN add AJAX Details call for getting group details for a cell

/* 
    openDetails indicates (for each row's shared details space), what item is open. So consider
    two groups, A and B. Both A and B have Flags, Notes and Details that when clicked will display details
    in the shared details space for a row. Intially, openDetails is empty. After the Flags for the first row in 
    A (A[0].Flags) is clicked, openDetails will have { __A_0 : Flags }. If A[0].Flags is clicked again, openDetails is emptied.
    After B[3].Notes is clicked, { __A_0 : Flags, __B_3 : Notes }. Etc. Since the space is shared, the code will have
    to close any nodes before reopening the details with the new info.
*/
var openDetails = {};

// <summary>
//   Calls Create_core_item_app.detail_group_inquiry via Ajax to get the html of the detail rows.
// </summary>
// <param name="oButtonElement" type="Object"> jQuery button element</param>
// <param name="sTarget" type="String"> Type of UI display to use: One of "Modal" "Inline" or "External" </param>
//
function get_details(oButtonElement)
{
  var oArgs = {};
  var jBtnSelector = $(oButtonElement);
  
  // something like "__43987" or "__PresFillCurrentLoc_0"
  oArgs.group_name = jBtnSelector.prop('id');

  // Bug 19655 SX doublesubmit to enable auto click drilldowns 
  // oArgs.__DOUBLESUBMIT__ = get_top().main.double_submit_value;

  var sURL = jBtnSelector.attr("data-url");
  
  // Bug 20490 UMN do case-insensitive compare in case user messes up in Config
  var sTarget = jBtnSelector.attr("data-target").toLowerCase();
  oArgs.cbt_id = jBtnSelector.attr("data-cbtid");  // Bug 20490 UMN

  // Bug 19655 SX remove doublesubmit to enable click drilldowns
  // Bug 20490 pass custom field id so doesn't have to be encoded in group name anymore
  var sFullURL = sURL + "&group_name=" + oArgs.group_name + "&cbt_id=" + oArgs.cbt_id;
  
  if (sTarget === undefined || sTarget === "") sTarget = "modal";
  
  if (sTarget === "modal")
  {
    // Bug 19811 UMN don't use showModalDialog
    $.ajax( 
    {
      "url": sFullURL,
      "data": {},
      "success": function (data) {
        enqueue_alert_message(data);
      },
      "dataType": "html",
      "cache": false,
      "error": function (xhr, error, thrown) {
        enqueue_alert_message("Unable to get details from server (AJAX Error E-01).");
      }
    });
  }
  else if (sTarget === "external")
  {
    window.open(sFullURL, oArgs.group_name, 'resizable=1,scrollbars=1,width=800,height=400,top=50,left=50');
  }
  else if (sTarget === "inline")
  {
    get_details_inline(jBtnSelector, sFullURL, oArgs);
  }
  
  return true;
}

// Bug 18719 UMN
function get_details_inline(jBtnSelector, sFullURL, oArgs)
{
  var nTr = jBtnSelector.closest('tr')[0];
  var oTable = jBtnSelector.closest('.TABLE_block').dataTable(); // get the DT instance
  var detailType = oArgs.cbt_id;      // eg: "Flags", or "Flags_RO" (the cust field id)
  var detailRow = oArgs.group_name;   // eg: "__43987" or "__PresFillCurrentLoc_0"
  
  var openDetailType = openDetails[detailRow];
  
  // If a detail is open for this row, close it.
  if (openDetailType !== undefined) 
  {
    $('div.innerDetails', $(nTr).next()[0]).slideUp(0, function () {
      oTable.fnClose(nTr);
      openDetails[detailRow] = undefined; // faster than delete, and since we only check if !== undefined, will be safe
    });
  }
  
  // If requested type is the same, it's a toggle, so ignore. Otherwise, open it
  if (openDetailType !== detailType)
  {
    $.ajax( 
    {
      "url": sFullURL,
      "data": {},
      "success": function (data) {
        var result = "<div class='innerDetails'>" + data + "</div>";
        var nDetailsRow = oTable.fnOpen(nTr, result, 'details');
        $('div.innerDetails', nDetailsRow).slideDown('fast', function () {
          $("div.dataTables_scrollBody").scrollTop(nTr.offsetTop);
        });
        openDetails[detailRow] = detailType;
      },
      "dataType": "html",
      "cache": false,
      "error": function (xhr, error, thrown) {
        var err = "<div class='innerDetails'>Unable to get details from server (AJAX Error E-02).</div>";
        var nDetailsRow = oTable.fnOpen(nTr, err, 'details');
        $('div.innerDetails', nDetailsRow).slideDown(function () {
          $("div.dataTables_scrollBody").scrollTop(nTr.offsetTop);
        });
        openDetails[detailRow] = detailType;
      }
    });
  }
}

// Bug 19655 SX support auto click drilldown icons 
function click_drilldowns(asDrillDownToClick) {
  for (var iDrillDownIndex in asDrillDownToClick) {
    var sDrillDownSelector = "#" + asDrillDownToClick[iDrillDownIndex];
    setTimeout(get_details(sDrillDownSelector), 1000);  
  }
  return true;
}

//////// END Bug 18719 UMN Detail inquiries

// Bug 23406 MJO - Change this to pass in the argument instead of using this
function on_excl_chkbox_change(obj) {

  $obj = typeof obj === 'undefined' ? $(this) : $(obj);

  isChecked = $obj.val() === "true";
  jLastCheckedSelector = $obj;

  if ($obj.length == 0)
    return true;
  
  //Bug 27532 NAM: synch up checkAll group level check box
  updateCheckAll(obj);

  if ($obj.data("exclusive")) {
    if (isChecked) {
      if (exclusiveBlockId == null) {
        exclusiveBlockId = $obj.data("block_id");

        nonexclusiveChecked = false;
        $(".checkbox_sel :checkbox:checked").each(function () {
          if ($(this).closest("table.TABLE_block").attr("id") !== exclusiveBlockId)
            nonexclusiveChecked = true;
        });

        if (nonexclusiveChecked) {
          get_top().main.enqueue_message("confirm_exclusive_" + $obj.data("block_id").substr(10));
          return false;
        }
      }
      else if (exclusiveBlockId !== $obj.data("block_id")) {
        get_top().main.enqueue_message("confirm_exclusive_" + exclusiveBlockId.substr(10));
        return false;
      }
    }
    else if ($("#" + exclusiveBlockId + " .checkbox_sel :checkbox:checked").length == 0) {
      exclusiveBlockId = null;
    }
  }
  else if (exclusiveBlockId != null) {
    get_top().main.enqueue_message("confirm_exclusive_" + exclusiveBlockId.substr(10));
    return false;
  }

  return true;
}

// Bug 23344 MJO - Take action on user's choice on exclusive block selection prompt
// Bug 23422 MJO - Switched over to the existing functions for modifying checkboxes
function process_exclusive(enableExclusive) {
  jExclusiveBlock = $("#" + exclusiveBlockId);

  if (enableExclusive) {
    $("table.TABLE_block").each(function () {
      if ($(this)[0] !== jExclusiveBlock[0]) {
        set_block_checkall($(this), false, false, true);
      }
    });
  }
  else {
    $chkboxes = jExclusiveBlock.find(".checkbox_sel input");

    set_block_checkall($chkboxes, false, false, true);
    exclusiveBlockId = null;

    $('span.checkbox_sel input:checkbox:checked').filter( function () {
      return $(this).data("exclusive");
    }).each(function () {
      exclusiveBlockId = $(this).data("block_id");
    });
  }

  update_PB_ENTERPRISE_PAYMENT_SCREEN_info();
}
//Bug 27532 NAM: update group level check box based on line item selections
function updateCheckAll(obj){
  var chkCnt = 0; 
  var childCnt = 0 ;
  var payBlock = $(obj).parentsUntil(".TR_block").find(".TABLE_block.block_payment.dataTable.no-footer");
  //find rows with a balance
  payBlock.find('tbody').children().each(function (index) {
    var balance = $(this).find('.prop_RESERVED_BALANCE').text();
    balance = parseFloat(balance.replace("$",""));
    if (balance !=  0.00){
        childCnt = childCnt + 1;
    }
  });
  //find selected rows.
  payBlock.find('tbody').children().each(function (index) {
    if ($(this).find('.payment_selector.prop_RESERVED_SELECTED .checkbox_sel.boolean input:checkbox').is(':checked')){
        chkCnt = chkCnt + 1;
    }
  });
  if (chkCnt > 0 && chkCnt == childCnt) {
     payBlock.find('.checkAll').prop( "checked", true );
  }
  else if (chkCnt == 0) {
    var objCheckAll =  payBlock.find('.checkAll');
    if (objCheckAll.is(':checked')){
        objCheckAll.prop( "checked", false );
    }   
  }    
}