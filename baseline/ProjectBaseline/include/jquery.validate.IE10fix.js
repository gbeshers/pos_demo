(function($) {

  $.extend($.fn, {

    validate: function( options ) {

      // if nothing is selected, return nothing; can't chain anyway
      if ( !this.length ) {
        if ( options && options.debug && window.console ) {
          console.warn( "Nothing selected, can't validate, returning nothing." );
        }
        return;
      }

      // check if a validator for this form was already created
      var validator = $.data( this[0], "validator" );
      if ( validator ) {
        return validator;
      }

      // Add novalidate tag if HTML5. UMN 08/27/13 fix IE10 quirks mode issue
      this.prop( "novalidate", "novalidate" );

      validator = new $.validator( options, this[0] );
      $.data( this[0], "validator", validator );

      if ( validator.settings.onsubmit ) {

        this.validateDelegate( ":submit", "click", function( event ) {
          if ( validator.settings.submitHandler ) {
            validator.submitButton = event.target;
          }
          // allow suppressing validation by adding a cancel class to the submit button
          if ( $(event.target).hasClass("cancel") ) {
            validator.cancelSubmit = true;
          }

          // allow suppressing validation by adding the html5 formnovalidate attribute to the submit button
          if ( $(event.target).attr("formnovalidate") !== undefined ) {
            validator.cancelSubmit = true;
          }
        });

        // validate the form on submit
        this.submit( function( event ) {
          if ( validator.settings.debug ) {
            // prevent form submit to be able to see console output
            event.preventDefault();
          }
          function handle() {
            var hidden;
            if ( validator.settings.submitHandler ) {
              if ( validator.submitButton ) {
                // insert a hidden input as a replacement for the missing submit button
                hidden = $("<input type='hidden'/>").attr("name", validator.submitButton.name).val( $(validator.submitButton).val() ).appendTo(validator.currentForm);
              }
              validator.settings.submitHandler.call( validator, validator.currentForm, event );
              if ( validator.submitButton ) {
                // and clean up afterwards; thanks to no-block-scope, hidden can be referenced
                hidden.remove();
              }
              return false;
            }
            return true;
          }

          // prevent submit for invalid forms or custom submit handlers
          if ( validator.cancelSubmit ) {
            validator.cancelSubmit = false;
            return handle();
          }
          if ( validator.form() ) {
            if ( validator.pendingRequest ) {
              validator.formSubmitted = true;
              return false;
            }
            return handle();
          } else {
            validator.focusInvalid();
            return false;
          }
        });
      }

      return validator;
    }
  });
})(jQuery);