﻿/*
Validation_rules.js
*/
// PB standard validation 13157 
// UMN: jslint/jshint fixes

// START of jquery validation methods or rules mapping to CORE configurable iPayment Custom field Types Plus Required 

// Bug 20642 UMN responsive rewrite so works on old and new UI

// Bug 24311 UMN some changes and documentation
// We use these functions because we don't want to validate rows that won't be submitted to the server.  So
// if this function returns true than the rest of the validation will continue (look at the minlengthInRow validation
// below, it first checks isValidRow). If isValidRow returns false, then validation doesn't continue for that input
// element.
function isValidRow(element) 
{
  var rowSel = $(element).parent().parent();
  var amountPaidSel = rowSel.find('.prop_RESERVED_AMOUNT_PAID input');
  // Bug 26376 MJO - Return true if there's no amount paid (it will be submitted)
  if (amountPaidSel.length === 0) return true; // Bug 24311 UMN check before applying regex
  var AmountPaid = amountPaidSel.val().replace(/[$,]/g, "");
  if (isNaN(AmountPaid)) return true; 
  // Bug 26376 MJO - Return true if there's no payment selector (it will be submitted)
  var selectedSel = rowSel.find('.prop_RESERVED_SELECTED input[type=hidden]');
  if (selectedSel.length === 0) return true;
  var isSelected = selectedSel.val();
  if (isSelected === true && (AmountPaid === undefined || AmountPaid === 0)) return true;  // Bug 15423 UMN
  if (AmountPaid !== undefined) AmountPaid = parseFloat(AmountPaid.replace(/[$,]/g, ""));
  if (AmountPaid === undefined || AmountPaid == 0) return false;
  else return true;
}

$.validator.addMethod("minlengthInRow", function(value, element, param) {
  // only valid minlength for valid row 
  return (isValidRow(element))&& !(this.optional(element)) ? this.getLength($.trim(value), element) >= param:true;
}, $.validator.format("Please enter at least {0} characters."));

$.validator.addMethod("maxlengthInRow", function(value, element, param) {
  // only valid maxlength for valid row 
  return (isValidRow(element))&& !(this.optional(element)) ? this.getLength($.trim(value), element) <= param:true;
}, $.validator.format("Please enter no more than {0} characters."));

jQuery.validator.addMethod("typicaltext", function(value, element) {
  return this.optional(element) || /^[\-\w.,()'\"\s!:;\?#&]+$/i.test(value);
}, "Please enter valid characters");

jQuery.validator.addMethod("integernegative", function(value, element) {
	return this.optional(element) || /^-\d+$/.test(value);
}, "A negative integer number please");

jQuery.validator.addMethod("numericwseparators", function(value, element) {
  return this.optional(element) || /^[0-9\-\s]+$/i.test(value);
}, "numbers, spaces, dash only please");

jQuery.validator.addMethod("citychars", function(value, element) {
  return this.optional(element) || /^[\-\w.\s]+$/i.test(value);
}, "Letters, spaces, dash only please");

jQuery.validator.addMethod("currency", function(value, element) {
    // Bug 13798 QG Convert to currency format automatically while validating
    value = $.trim(value);
    var result = this.optional(element) || /^-?\$?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d{1,2})?$/.test(value);
    if ((result) && (element != document.activeElement))
        $(element).val(formatCurrency(value));
    return result;
}, "Currency only please");

jQuery.validator.addMethod("currencypositive", function(value, element) {
	return this.optional(element) || /^\$?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d{1,2})?$/.test(value);
}, "Positive Currency only please");

jQuery.validator.addMethod("currencypositiveandnegative", function(value, element) {
	return this.optional(element) || /^-?\$?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d{1,2})?$/.test(value); // Bug 13435  handle -$1.00
}, "Positive or Negative Currency only please");

jQuery.validator.addMethod("currencypositivezeroandnegative", function(value, element) {
	return this.optional(element) || /^-?\$?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d{1,2})?$/.test(value); // Bug 13435
}, "Currency only please");

jQuery.validator.addMethod("currencynegative", function(value, element) {
	return this.optional(element) || /^-\$?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d{1,2})?$/.test(value); // Bug 13435
}, "Negative Currency only please");

//BUG 24811 NAM: convert amount to negative equivalent
jQuery.validator.addMethod("currency_autonegative", function(value, element) {   
    var auto_negative = value.replace('(','').replace(')','').replace('$','-');  
    var result = this.optional(element) || /^-\$?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d{1,2})?$/.test(auto_negative);
    if ((result) && (element != document.activeElement))
        $(element).val( formatAmount(value));
    return result;
}, "Negative Currency only please");

jQuery.validator.addMethod("currencynegativeandzero", function(value, element) {
	return this.optional(element) || /^-\$?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d{1,2})?$/.test(value)||/^\$?0(\.)?0{0,2}$/.test(value); // BUG 13435
}, "Negative Currency or Zero only please");

jQuery.validator.addMethod("currencyzero", function(value, element) {
	return this.optional(element) || /^\$?0(\.)?0{0,2}$/.test(value);
}, "Zero only please");

jQuery.validator.addMethod("currencypositiveandzero", function(value, element) {
	return this.optional(element) || /^\$?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d{1,2})?$/.test(value);
}, "Positive Currency or Zero only please");

// Bug IPAY-1116 MJO - Prevent overpayment of block transaction
jQuery.validator.addMethod("NoOverpayment", function (value, element) {
  var AmountDue = parseFloat($(element).closest('tr').find('.prop_RESERVED_BALANCE').text().replace(/[$,]/g, ""));
  var AmountPaid = parseFloat(value.replace(/[$,]/g, ""));
  return (AmountDue >= AmountPaid);
}, 'Overpayment is not allowed.');

jQuery.validator.addClassRules({
  requiredInRow: { required: { depends: function(element) {return isValidRow(element); }}},
  typicaltextInRow: { typicaltext: { depends: function(element) {return isValidRow(element); }}},
  integernegativeInRow: { integernegative: { depends: function(element) {return isValidRow(element); }}},
  numericwseparatorsInRow: { numericwseparators: { depends: function(element) {return isValidRow(element); }}},
  citycharsInRow: {citychars: { depends: function(element) {return isValidRow(element); }}},
  currencyInRow: { currency: { depends: function(element) { return isValidRow(element); }}},  // Bug 13798 QG Correct key to 'currency'
  currencypositiveInRow: {currencypositive: { depends: function(element) {return isValidRow(element) }}},
  currencypositiveandnegativeInRow: {currencypositiveandnegative: { depends: function(element) {return isValidRow(element) }}},
  currencypositivezeroandnegativeInRow: {currencypositivezeroandnegative: { depends: function(element) {return isValidRow(element) }}},
  currencynegativeInRow: {currencynegative: { depends: function(element) {return isValidRow(element) }}},
  currencyautonegativeInRow: {currencyautonegative: { depends: function(element) {return isValidRow(element) }}}, //BUG 24811 NAM: convert amount to negative equivalent
  currencynegativeandzeroInRow: {currencynegativeandzero: { depends: function(element) {return isValidRow(element) }}},
  currencyzeroInRow: {currencyzero: { depends: function(element) {return isValidRow(element) }}},
  currencypositiveandzeroInRow: {currencypositiveandzero: { depends: function(element) {return isValidRow(element) }}},
  letterswithbasicpuncInRow: {letterswithbasicpunc: { depends: function(element) {return isValidRow(element); }}},
  digitsInRow: {digits: { depends: function(element) {return isValidRow(element); }}},
  numberInRow: {number: { depends: function(element) {return isValidRow(element); }}},  
  dateInRow: {date: { depends: function(element) {return isValidRow(element); }}},
  emailInRow: {email: { depends: function(element) {return isValidRow(element); }}},  
  lettersonlyInRow: {lettersonly: {  depends: function(element) {return isValidRow(element); }}},
  alphanumericInRow: {alphanumeric: { depends: function(element) {return isValidRow(element); }}},
  integerInRow: {integer: { depends: function(element) { return isValidRow(element); }}},
  phoneUSInRow: {phoneUS: { depends: function(element) { return isValidRow(element); }}},
  time12hInRow: {time12h: { depends: function(element) { return isValidRow(element); }}}
});
// END of jquery validation methods or rules 


// Check whether page is valid and return error message
// Called by validate_and_cover()
function jQueryvalidate(tag) {
  bShowWarningMessage = true;
  
  // Bug 18948 UMN
  var msg = validate_captcha("Please solve the captcha.");
  if (msg !== true) {
    bShowWarningMessage = false;
    return msg;
  }
  
  if ($(tag).valid()) {
    bShowWarningMessage = false;
    return true;
  }
  else {
    bShowWarningMessage = false;
    var validator = $(tag).validate();
    if (validator.errorList.length > 0)
      return validator.errorList[0].message;
    else
      return 'Unknown error in jQuery validation';
  }
}




