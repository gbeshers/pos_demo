﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
#if DEBUG
[assembly: AssemblyTitle("ProjectBaseline (Debug)")]
#else
[assembly: AssemblyTitle("ProjectBaseline (Release)")]
#endif

// Assembly version info now in single place: pos_demo/VersionInfo.cs

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("63fd5694-ac92-486a-9c5e-25f045c4b0b7")]

