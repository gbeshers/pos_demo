﻿using System;
using System.Collections;
using System.IO;
using System.Text;
using CASL_engine;
using System.Xml;
using ftp;
using System.Security;

//TO DO GL DESC override need to increase size of CUSTVALUE from 100 to ...
//report base issue progress bar not moving across screen at bu completion (maybe only on failure test this)
//ask dennis batch header is all recs d+c or just d
//to docheck code logic for dc 60 10 debit credit
//TO DO CHECK THAT TRANS IS CONFIGURED FOR FIN BATCH UPDATE
//TO DO need base fix for FILE IS GETTING MARKED AS UPDATED ON FAILURE
//TO DO need base fix for update_sys_int.. is not reliable due to TOO LONG issue
//TO DO need base fix for TTA_MAP currently using my fix for this
//for all upon error BU make sure all paths write to log and return error message to product and 
/// DO NOT THROW EXCEPTION because refelection code exception is embeded in inner exception

namespace ProjectBaseline
{
  /// <summary>
  /// CXMLFileBatchInfo is used to save information of a CoreFile
  /// </summary>
  public class CXMLFileBatchInfo
  {
    //Bug# 12053 QG Add new properties for CORE FILE information
    #region private members
    private string _DeptID;
    private string _FileName;
    private string _FileType;
    private string _FileDesc;
    private string _AutoBalance;
    private string _OpenUserID;
    private string _CreatorID;
    private string _BalUserID;
    private string _AccUserID;
    private string _UpdateUserID;
    private string _CloseUserID;
    private Nullable<DateTime> _EffectiveDT;
    private Nullable<DateTime> _OpenDT;
    private Nullable<DateTime> _BalDT;
    private Nullable<DateTime> _AccDT;
    private Nullable<DateTime> _UpdateDT;
    private Nullable<DateTime> _CloseDT;
    private string _BalTotal;
    private string _SourceType;
    private Nullable<DateTime> _SourceDT;
    private Nullable<DateTime> _PostDT;
    #endregion

    #region Public Properties
    public string m_strDeptID { get { return _DeptID; } set { _DeptID = value; } }
    public string m_strFileName { get { return _FileName; } set { _FileName = value; } }
    public string m_strFileType { get { return _FileType; } set { _FileType = value; } }
    public string m_strFileDesc { get { return _FileDesc; } set { _FileDesc = value; } }
    public string m_strAutoBalance { get { return _AutoBalance; } set { _AutoBalance = value; } }
    public string m_strOpenUserID { get { return _OpenUserID; } set { _OpenUserID = value; } }
    public string m_strCreatorID { get { return _CreatorID; } set { _CreatorID = value; } }
    public string m_strBalUserID { get { return _BalUserID; } set { _BalUserID = value; } }
    public string m_strAccUserID { get { return _AccUserID; } set { _AccUserID = value; } }
    public string m_strUpdateUserID { get { return _UpdateUserID; } set { _UpdateUserID = value; } }
    public string m_strCloseUserID { get { return _CloseUserID; } set { _CloseUserID = value; } }
    public Nullable<DateTime> m_dtEffective { get { return _EffectiveDT; } set { _EffectiveDT = value; } }
    public Nullable<DateTime> m_dtOpen { get { return _OpenDT; } set { _OpenDT = value; } }
    public Nullable<DateTime> m_dtBal { get { return _BalDT; } set { _BalDT = value; } }
    public Nullable<DateTime> m_dtAcc { get { return _AccDT; } set { _AccDT = value; } }
    public Nullable<DateTime> m_dtUpdate { get { return _UpdateDT; } set { _UpdateDT = value; } }
    public Nullable<DateTime> m_dtClose { get { return _CloseDT; } set { _CloseDT = value; } }
    public string m_strBalTotal { get { return _BalTotal; } set { _BalTotal = value; } }
    public string m_strSourceType { get { return _SourceType; } set { _SourceType = value; } }
    public Nullable<DateTime> m_dtSource { get { return _SourceDT; } set { _SourceDT = value; } }
    public Nullable<DateTime> m_dtPOSTDT { get { return _PostDT; } set { _PostDT = value; } }
    #endregion

    public OutputConnection m_pOutputConnection = null;  // I/O
    public GenericObject m_geoXMLCoreEvents = new GenericObject();  //  collection of CoreEvent
    public CXMLFileBatchInfo()
    {
      //Bug#12053 Initialize Properties Here.
    }
  }  // End of class CXMLFileBatchInfo



  /// <summary>
  /// CXMLEventRecord contains all information for a specified Event
  /// including Transactions and Tenders
  /// </summary>
  public class CXMLEventRecord
  {
    //Bug#12053 QG Add new properties for CORE EVENT information
    #region private members
    private string _EventNbr;
    private string _DepfileSeq;
    private string _DepfileNbr;
    private string _UserID;
    private string _Status;
    private Nullable<DateTime> _Mod_DT;
    private Nullable<DateTime> _Creation_DT;
    private Nullable<DateTime> _Source_Date;
    private string _Source_Type;
    #endregion

    #region Public Properties
    public string m_strEventNbr { get { return _EventNbr; } set { _EventNbr = value; } }
    public string m_strDepfileNbr { get { return _DepfileNbr; } set { _DepfileNbr = value; } }
    public string m_strDepfileSeq { get { return _DepfileSeq; } set { _DepfileSeq = value; } }
    public string m_strUserID { get { return _UserID; } set { _UserID = value; } }
    public string m_strStatus { get { return _Status; } set { _Status = value; } }
    public Nullable<DateTime> m_dtModification { get { return _Mod_DT; } set { _Mod_DT = value; } }
    public Nullable<DateTime> m_dtCreation { get { return _Creation_DT; } set { _Creation_DT = value; } }
    public Nullable<DateTime> m_dtSource { get { return _Source_Date; } set { _Source_Date = value; } }
    public string m_strSourceType { get { return _Source_Type; } set { _Source_Type = value; } }
    public string m_strReceiptRefNbr { get { return String.Format("{0}{1}-{2}", _DepfileNbr, _DepfileSeq.PadLeft(3, '0'), _EventNbr); } }
    #endregion

    public GenericObject m_geoTransactions;
    public GenericObject m_geoTenders;

    public CXMLEventRecord()
    {
      //Bug#12053 Initialize Properties Here.
      m_strEventNbr = "";
      m_strDepfileNbr = "";
      m_strDepfileSeq = "";
      m_strUserID = "";
      m_strStatus = "";
      m_dtModification = null;
      m_dtCreation = null;
      m_dtSource = null;
      m_strSourceType = "";
      m_geoTenders = new GenericObject();
      m_geoTransactions = new GenericObject();
    }

    // Add a Transaction into a CXMLEventRecord(CoreEvent) object
    public bool AddXMLTransactionRecord(GenericObject geoTran, GenericObject geoCustData, GenericObject geoFile, string strSystemInterface, ref string strError, bool useFieldsElement)
    {
      CXMLTransactionRecord pXMLTransRec = new CXMLTransactionRecord();
      try
      {
        pXMLTransRec = BuildXMLTransRec(geoCustData, geoTran, geoFile, useFieldsElement);
        m_geoTransactions.insert(pXMLTransRec);
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in AddXMLTransactionRecord", e);

        strError = "Error in AddXMLTransactionRecord" + e.Message;
        return false;
      }
      return true;
    }

    // Build a Transaction Record
    public CXMLTransactionRecord BuildXMLTransRec(GenericObject geoCustData, GenericObject geoTran, GenericObject geoFile, bool useFieldsElement)
    {
      //Bug# 12053 QG Retrieve Transaction Information
      CXMLTransactionRecord pXMLTransRecord = new CXMLTransactionRecord();
      pXMLTransRecord.m_strTranNbr = geoTran.get("TRANNBR", "").ToString();
      pXMLTransRecord.m_strTTID = geoTran.get("TTID", "").ToString();
      pXMLTransRecord.m_strTranAMT = decimal.Parse(geoTran.get("TRANAMT", 0).ToString());
      pXMLTransRecord.m_strTTDesc = geoTran.get("TTDESC", "").ToString();
      pXMLTransRecord.m_strItemInd = geoTran.get("ITEMIND", "").ToString();
      pXMLTransRecord.m_dDistAMT = decimal.Parse(geoTran.get("DISTAMT", 0).ToString());  //Bug#13495
      pXMLTransRecord.m_strSourceType = geoTran.get("SOURCE_TYPE", "").ToString();        //Bug#13495
      pXMLTransRecord.m_dtPostDT = geoTran.get("POSTDT", null) as Nullable<DateTime>;    //Bug#13495
      pXMLTransRecord.m_geoCustomFields.Clear();
      ArrayList keys = geoCustData.getAllKeys();
      foreach (string key in keys)
      {
        // BEGIN: Bug 14772 (Bug 15464) - Update System Interface List for Container Overwriting Block Transaction Value
        // Modify the update_system_interface_list value with the CURRENTLY configured batch update system interface list for the 
        // transaction type (i.e., do NOT use the update SI list value stored with transaction).
        if ((key == "update_system_interface_list") && (geoTran != null))
        {
          GenericObject geoTranClass = ((GenericObject)c_CASL.c_object("Transaction.of")).get(geoTran.get("TTID", "").ToString(), "") as GenericObject;
          if (geoTranClass != null)
          {
            // Get the configured Batch Update list (i.e., "update_core_file_system_interface_list") for the Trans Type:
            GenericObject geoConfigSysInt = geoTranClass.get("update_core_file_system_interface_list", null) as GenericObject;
            if (geoConfigSysInt != null)
            {
              int iCountofSysInt = geoConfigSysInt.getIntKeyLength();
              string strConfigSysIntList = "";
              for (int i = 0; i < iCountofSysInt; i++)
              {
                // string strConfigSysInt = geoConfigSysInt.get(i).ToString();
                string strConfigSysInt = (string)((GenericObject)geoConfigSysInt.get(i)).get("_name", "");
                strConfigSysInt = strConfigSysInt.Trim();
                if (!string.IsNullOrEmpty(strConfigSysInt))
                {
                  strConfigSysIntList += string.Format(" \"{0}\"", strConfigSysInt);
                }
              }
              // Bug 26837 UMN don't output vector syntax per CoSD
              if (useFieldsElement)
                strConfigSysIntList = strConfigSysIntList.Trim();
              else
              strConfigSysIntList = string.Format("<v>{0}</v>", strConfigSysIntList.Trim());
              pXMLTransRecord.m_geoCustomFields.Add(key, strConfigSysIntList);
            }
          }
        }
        else // END: Bug 14772 (Bug 15464) - Update System Interface List for Container Overwriting Block Transaction Value
        {
				 pXMLTransRecord.m_geoCustomFields.Add(key,ProjectBaseline.ResponsiveUI.StripHTML( geoCustData.get(key, "").ToString())); //IPAY-129 NAM
        }
      }
      return pXMLTransRecord;
    }

    // Add a Tender into a CXMLEventRecord(CoreEvent) object
    public bool AddXMLTenderRecord(GenericObject geoTender, GenericObject geoCustData, GenericObject geoFile, string strSystemInterface, ref string strError)
    {
      CXMLTenderRecord pXMLTenderRec = new CXMLTenderRecord();
      try
      {
        pXMLTenderRec = BuildXMLTenderRec(geoCustData, geoTender, geoFile);
        m_geoTenders.insert(pXMLTenderRec);
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in AddXMLTenderRecord", e);
        strError = "Error in AddXMLTenderRecord" + e.Message;
        return false;
      }
      return true;
    }

    // Build a Tender Record
    public CXMLTenderRecord BuildXMLTenderRec(GenericObject geoCustData, GenericObject geoTender, GenericObject geoFile)
    {
      //Bug#12053 QG Retrived Tender Information
      CXMLTenderRecord pXMLTenderRecord = new CXMLTenderRecord();
      //pXMLTransRecord.m_strEFFDATE = String.Format("{0:MM/dd/yyyy}", geoFile.get("EFFECTIVEDT", ""));
      //pXMLTransRecord.m_strReceiptRefNbr = String.Format("{0}{1}-{2}", geoTran.get("DEPFILENBR", "").ToString(), geoTran.get("DEPFILESEQ", "").ToString().PadLeft(3, '0'), geoTran.get("EVENTNBR", "").ToString());
      pXMLTenderRecord.m_strTenderID = geoTender.get("TNDRID", "").ToString();
      pXMLTenderRecord.m_strTenderDesc = geoTender.get("TNDRDESC", "").ToString();
      pXMLTenderRecord.m_strTenderNbr = geoTender.get("TNDRNBR", "").ToString();
      pXMLTenderRecord.m_strTypeInd = geoTender.get("TYPEIND", "").ToString();
      pXMLTenderRecord.m_dAmount = decimal.Parse(geoTender.get("AMOUNT", "0.00").ToString());
      pXMLTenderRecord.m_strCCCKNbr = geoTender.get("CC_CK_NBR", "").ToString();
      pXMLTenderRecord.m_strCCName = geoTender.get("CCNAME", "").ToString();
      pXMLTenderRecord.m_strAuthNbr = geoTender.get("AUTHNBR", "").ToString();
      pXMLTenderRecord.m_strAuthString = geoTender.get("AUTHSTRING", "").ToString();
      pXMLTenderRecord.m_strAuthAction = geoTender.get("AUTHACTION", "").ToString();
      pXMLTenderRecord.m_strBankRouting = geoTender.get("BANKROUTING", "").ToString();
      pXMLTenderRecord.m_strBankAcctNbr = geoTender.get("BANKACCTNBR", "").ToString();
      pXMLTenderRecord.m_strBankAcctID = geoTender.get("BANKACCTID", "").ToString();
      pXMLTenderRecord.m_strAddress = geoTender.get("ADDRESS", "").ToString();
      pXMLTenderRecord.m_dtPostDT = geoTender.get("POSTDT", null) as Nullable<DateTime>;  //Bug#13495
      pXMLTenderRecord.m_geoCustomFields.Clear();
      ArrayList keys = geoCustData.getStringKeys();
      foreach (string key in keys)
        pXMLTenderRecord.m_geoCustomFields.Add(key, geoCustData.get(key, "").ToString()); // Bug 15464
      return pXMLTenderRecord;
    }

  } // end class CXMLEventRecord

  /// <summary>
  /// Transaction Record Class
  /// </summary>
  public class CXMLTransactionRecord
  {
    //Bug#12053 QG Add new properties for Transaction record
    #region private member
    private string _TranNbr;
    private string _TTID;
    private string _TTDesc;
    private decimal _TranAMT;
    private string _ItemInd;
    // Bug#13495 QG Add additional data elements
    private decimal _DistAMT;
    private Nullable<DateTime> _PostDT;
    private string _SourceType;
    #endregion

    #region public properties
    public string m_strTranNbr { get { return _TranNbr; } set { _TranNbr = value; } }
    public string m_strTTID { get { return _TTID; } set { _TTID = value; } }
    public string m_strTTDesc { get { return _TTDesc; } set { _TTDesc = value; } }
    public decimal m_strTranAMT { get { return _TranAMT; } set { _TranAMT = value; } }
    public string m_strItemInd { get { return _ItemInd; } set { _ItemInd = value; } }
    // Bug#13495 QG Add additional data elements
    public decimal m_dDistAMT { get { return _DistAMT; } set { _DistAMT = value; } }
    public string m_strSourceType { get { return _SourceType; } set { _SourceType = value; } }
    public Nullable<DateTime> m_dtPostDT { get { return _PostDT; } set { _PostDT = value; } }
    #endregion

    public GenericObject m_geoCustomFields = new GenericObject();

    public CXMLTransactionRecord()
    {
      //Bug#12053 Initialize Properties Here.
      m_strTranNbr = "";
      m_strTTID = "";
      m_strTTDesc = "";
      m_strTranAMT = 0;
      m_strItemInd = "";
      m_dDistAMT = 0;
      m_strSourceType = "";
      m_dtPostDT = null;
    }
  }

  /// <summary>
  /// Tender Record Class
  /// </summary>
  public class CXMLTenderRecord
  {
    //Bug#12053 QG Add new properties for Tender record
    #region private members
    private string _TNDRNBR;
    private string _TNDRID;
    private string _TNDRDESC;
    private string _TYPEIND;
    private decimal _AMOUNT;
    private string _CC_CK_NBR;
    private string _CCNAME;
    private string _AUTHNBR;
    private string _AUTHSTRING;
    private string _AUTHACTION;
    private string _BANKROUTING;
    private string _BANKACCTNBR;
    private string _ADDRESS;
    private string _BANKACCTID;
    //Bug#13495 QG Add addtional data element for tender
    private Nullable<DateTime> _PostDT;
    #endregion

    #region public properties
    public string m_strTenderNbr { get { return _TNDRNBR; } set { _TNDRNBR = value; } }
    public string m_strTenderID { get { return _TNDRID; } set { _TNDRID = value; } }
    public string m_strTenderDesc { get { return _TNDRDESC; } set { _TNDRDESC = value; } }
    public string m_strTypeInd { get { return _TYPEIND; } set { _TYPEIND = value; } }
    public decimal m_dAmount { get { return _AMOUNT; } set { _AMOUNT = value; } }
    public string m_strCCCKNbr { get { return _CC_CK_NBR; } set { _CC_CK_NBR = value; } }
    public string m_strCCName { get { return _CCNAME; } set { _CCNAME = value; } }
    public string m_strAuthNbr { get { return _AUTHNBR; } set { _AUTHNBR = value; } }
    public string m_strAuthString { get { return _AUTHSTRING; } set { _AUTHSTRING = value; } }
    public string m_strAuthAction { get { return _AUTHACTION; } set { _AUTHACTION = value; } }
    public string m_strBankRouting { get { return _BANKROUTING; } set { _BANKROUTING = value; } }
    public string m_strBankAcctNbr { get { return _BANKACCTNBR; } set { _BANKACCTNBR = value; } }
    public string m_strAddress { get { return _ADDRESS; } set { _ADDRESS = value; } }
    public string m_strBankAcctID { get { return _BANKACCTID; } set { _BANKACCTID = value; } }
    //Bug#13495 QG Add addtional data element for tender
    public Nullable<DateTime> m_dtPostDT { get { return _PostDT; } set { _PostDT = value; } }
    #endregion
    public GenericObject m_geoCustomFields = new GenericObject();

    public CXMLTenderRecord()
    {
      //Bug#12053 Initialize Properties Here.
    }
  }

  /// <summary>
  /// This is the core BatchUpdate class. 
  /// </summary>
  public class XMLFileBatchUpdate : AbstractBatchUpdate
  {
    public CXMLFileBatchInfo m_XMLFileBatchInfo = null; // For collection of CXMLEventRecord
    public GenericObject m_ConfigSystemInterface = null;
    public int m_iCountCOREFiles = 0;
    public XMLFileBatchUpdate()
    {
    }

    //Bug#12053 QG Retrieve Core File information
    public void SetCoreFileInfo(ref CXMLFileBatchInfo FileBatchInfo, GenericObject pFileGEO)
    {
      //Bug#13536 QG Check whether FileBatchInfo exist.
      if (FileBatchInfo == null)
        FileBatchInfo = new CXMLFileBatchInfo();
      else
        // Bug#13536 QG Clear CoreEvent if it's a new CoreFile
        FileBatchInfo.m_geoXMLCoreEvents.vectors.Clear();
      FileBatchInfo.m_strDeptID       = pFileGEO.get("DEPTID", "").ToString();   //Bug#12053 QG Correct the tagname
      FileBatchInfo.m_strFileName     = pFileGEO.get("FILENAME", "").ToString();
      FileBatchInfo.m_strFileType     = pFileGEO.get("FILETYPE", "").ToString();
      FileBatchInfo.m_strFileDesc     = pFileGEO.get("FILEDESC", "").ToString();
      FileBatchInfo.m_strAutoBalance  = pFileGEO.get("AUTOBALANCE", "").ToString();
      FileBatchInfo.m_strOpenUserID   = pFileGEO.get("OPEN_USERID", "").ToString();
      FileBatchInfo.m_strCreatorID    = pFileGEO.get("CREATOR_ID", "").ToString();
      FileBatchInfo.m_strBalUserID    = pFileGEO.get("BAL_USERID", "").ToString();
      FileBatchInfo.m_strAccUserID    = pFileGEO.get("ACC_USERID", "").ToString();
      FileBatchInfo.m_strUpdateUserID = pFileGEO.get("UPDATE_USERID", "").ToString();
      FileBatchInfo.m_strCloseUserID  = pFileGEO.get("CLOSE_USERID", "").ToString();
      FileBatchInfo.m_dtEffective     = pFileGEO.get("EFFECTIVEDT", null) as Nullable<DateTime>;
      FileBatchInfo.m_dtOpen          = pFileGEO.get("OPENDT", null) as Nullable<DateTime>;
      FileBatchInfo.m_dtBal           = pFileGEO.get("BALDT", null) as Nullable<DateTime>;
      FileBatchInfo.m_dtAcc           = pFileGEO.get("ACCDT", null) as Nullable<DateTime>;
      FileBatchInfo.m_dtUpdate        = pFileGEO.get("UPDATEDT", null) as Nullable<DateTime>;
      FileBatchInfo.m_dtClose         = pFileGEO.get("CLOSEDT", null) as Nullable<DateTime>;
      FileBatchInfo.m_strBalTotal     = pFileGEO.get("BALTOTAL", "").ToString();
      FileBatchInfo.m_strSourceType   = pFileGEO.get("SOURCE_TYPE", "").ToString();
      FileBatchInfo.m_dtSource        = pFileGEO.get("SOURCE_DATE", null) as Nullable<DateTime>;
      FileBatchInfo.m_dtPOSTDT        = DateTime.Now;
    }

    public override string GetCASLTypeName()
    {
      // Always instantiate this from a FlatFileBatchUpdate
      return "NEVERDIRECTLYCREATETHIS!";
    }

    // Build Up m_XML_FileBatchInfo for a CORE File
    public override bool Project_ProcessCoreEvent(string strSystemInterface, GenericObject pEventGEO, GenericObject pConfigSystemInterface, GenericObject pFileGEO, object pProductLevelMainClass, string strCurrentFile, ref eReturnAction ReturnAction) // Bug 15464
    {
      CASL_engine.BatchUpdate_ProductLevel pBatchUpdate_ProductLevel = (BatchUpdate_ProductLevel)pProductLevelMainClass;
      string strError = "";
      CXMLEventRecord pXMLEventRecord = new CXMLEventRecord();

      // Bug 26837 UMN
      bool useFieldsElement = ((string)pConfigSystemInterface.get("element_type", "NAMED") == "FIELDS") ? true : false;

      //If we return false from here call Cleanup to clear all batch level info
      //the product will abort processing remaining events so we do not want partial information 
      //for the CORE file in the batch update file.. the product code is being worked on 
      //so this area may need to be reworked
      bool bProcessEverything = (bool)pConfigSystemInterface.get("process_everything_in_file", false);
      if (bProcessEverything)
      {
        try
        {
          pFileGEO = pFileGEO.get(0, null) as GenericObject;
          if (m_ConfigSystemInterface == null)
            m_ConfigSystemInterface = pConfigSystemInterface;
          if ((m_XMLFileBatchInfo == null) || (pFileGEO.get("FILENAME").ToString() != m_XMLFileBatchInfo.m_strFileName))  //Bug#13536 QG If it's a new CoreFile,update BatchFileInfo.
            SetCoreFileInfo(ref m_XMLFileBatchInfo, pFileGEO);

          //Bug#12053 QG Retrieve CORE EVENT information
          pXMLEventRecord.m_strDepfileNbr = pEventGEO.get("DEPFILENBR", "").ToString();
          pXMLEventRecord.m_strDepfileSeq = pEventGEO.get("DEPFILESEQ", "").ToString();
          pXMLEventRecord.m_strEventNbr = pEventGEO.get("EVENTNBR", "").ToString();
          pXMLEventRecord.m_strUserID = pEventGEO.get("USERID", "").ToString();
          pXMLEventRecord.m_strStatus = pEventGEO.get("STATUS", "").ToString();
          pXMLEventRecord.m_strSourceType = pEventGEO.get("SOURCE_TYPE", "").ToString();
          pXMLEventRecord.m_dtModification = pEventGEO.get("MOD_DT", null) as Nullable<DateTime>;
          pXMLEventRecord.m_dtCreation = pEventGEO.get("CREATION_DT", null) as Nullable<DateTime>;
          pXMLEventRecord.m_dtSource = pEventGEO.get("SOURCE_DATE", null) as Nullable<DateTime>;

          GenericObject geoTrans = pEventGEO.get("TRANSACTIONS", new GenericObject()) as GenericObject;
          GenericObject geoTenders = pEventGEO.get("TENDERS", new GenericObject()) as GenericObject;
          int iCountofTransactions = geoTrans.getIntKeyLength();
          for (int i = 0; i < iCountofTransactions; i++)
          {
            GenericObject geoTran = (GenericObject)geoTrans.get(i);
            // geoTranClass is used to get custom_fields of Transaction Type
            GenericObject geoTranClass = ((GenericObject)c_CASL.c_object("Transaction.of")).get(geoTran.get("TTID", "").ToString(), "") as GenericObject;
            // TODO: We suppose transaction is either regular or tax, will rework this later
            if (geoTranClass == null)
              geoTranClass = ((GenericObject)c_CASL.c_object("Transaction.Tax.of")).get(geoTran.get("TTID", "").ToString(), "") as GenericObject;
            // Bug#13369 QG If transaction type isn't found, for instance, OVERSHORT Transaction, don't add custom fields. 
            GenericObject geoCustomFields = new GenericObject();
            GenericObject geoCustData = new GenericObject();
            if (geoTranClass != null)
            {
              geoCustomFields = (GenericObject)((GenericObject)geoTranClass.get("custom_fields", new GenericObject())).get("_objects", new GenericObject());
              geoCustData = GetCustFieldData(geoTran, "GR_CUST_FIELD_DATA"); // Bug#14300 (Bug 15464) QG Rewrite this function
            }
            // End of Bug#13369
            // process XMLFile Transaction
            if (!pXMLEventRecord.AddXMLTransactionRecord(geoTran, geoCustData, pFileGEO, strSystemInterface, ref strError, useFieldsElement))
            {
              //Bug#13532 Tweak error handling
              strError = String.Format("AddXMLTransactionRecord {0} {1}", strSystemInterface, strCurrentFile);
              throw new Exception(strError);
            }
          }
          int iCountofTenders = geoTenders.getIntKeyLength();
          for (int i = 0; i < iCountofTenders; i++)
          {
            GenericObject geoTender = (GenericObject)geoTenders.get(i);
            GenericObject geoTenderClass = (GenericObject)((GenericObject)c_CASL.c_object("Tender.of")).get(geoTender.get("TNDRID", "").ToString());
            //if (!IsXMLFileBatchUpdate(geoTenderClass, strSystemInterface))
            //    continue;
            GenericObject geoCustomFields = (GenericObject)((GenericObject)geoTenderClass.get("custom_fields", new GenericObject())).get("_objects", new GenericObject());
            GenericObject geoCustData = GetCustFieldData(geoTender, "CUST_FIELD_DATA");  //Bug#14300 (Bug 15464) QG Rewrite this function
            // process XMLFile tender
            if (!pXMLEventRecord.AddXMLTenderRecord(geoTender, geoCustData, pFileGEO, strSystemInterface, ref strError))
            {
              //Bug#13532 Tweak error handling
              strError = String.Format("AddXMLTenderRecord {0} {1}", strSystemInterface, strCurrentFile);
              throw new Exception(strError);
            }
          }
          m_XMLFileBatchInfo.m_geoXMLCoreEvents.insert(pXMLEventRecord);
        }

        catch (Exception e)
        {
          // Bug 17849
          Logger.cs_log_trace("Error in Project_ProcessCoreEvent", e);

          //Bug#13532 Tweak error handling
          strError = String.Format("Project_ProcessCoreEvent:{0} {1} {2}", strSystemInterface, strCurrentFile, e.Message).Replace("'", "");
          Logger.LogError(strError, "XML Batch Update");
          pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError(strCurrentFile, strSystemInterface, strError);
          return false;
        }
      }
      else
      {
        //Bug#13532 QG: process nothing and log error message.
        strError = String.Format("Project_ProcessEvent: Nothing is processed since bProcessEverything is set to NO in {0}, please change it to YES.", strSystemInterface);
        Logger.LogError(strError, "XML Batch Update");
        pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError("SYS_INTERFACE_SCOPE_ONLY", strSystemInterface, strError);
        return false;
      }
      return true;
    }

    // Reset CORE File Infomation
    public override bool Project_Cleanup(bool bEarlyTerminationRequestedByProject, object pProductLevelMainClass)
    {
      string strError = "";
      try
      {
        if (m_XMLFileBatchInfo != null) // Bug 15464
        {
          if (m_XMLFileBatchInfo.m_geoXMLCoreEvents != null) // Bug 15464
          {
            m_XMLFileBatchInfo.m_geoXMLCoreEvents.removeAll();
          }
          m_XMLFileBatchInfo = null;   //Bug#12053 QG 
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in XMLFile CleanUp", e);

        strError = "XMLFile CleanUp: " + e.Message;
        Logger.LogError(strError, "XML Batch Update");
        return false;
      }

      return true;
    }

    public bool IsXMLFileBatchUpdate(GenericObject geoTranClass, string strSystemInterface)
    {
      GenericObject geoConfigSysInt = geoTranClass.get("update_system_interface_list", "") as GenericObject;
      int iCountofSysInt = geoConfigSysInt.getIntKeyLength();
      for (int i = 0; i < iCountofSysInt; i++)
      {
        string strConfigSysInt = geoConfigSysInt.get(i).ToString();
        if (strConfigSysInt == strSystemInterface)
          return true;
      }
      return false;
    }

    // Bug#14300 (Bug 15464) Get all non-empty custom fields in strDBfield
    // Get Transaction/Tender's Custom Field
    public GenericObject GetCustFieldData(GenericObject geoTranOrTender, string strDBfield)
    {
      // Custom Fields of Transaction or Tender includes some standard fields we do not want.
      // geoCustomFields works as a filter
      // strDBfields is "GR_CUST_FIELD_DATA" for Transaction and "CUST_FIELD_DATA" for Tender
      GenericObject geoCustData = new GenericObject();
      // Get the needed custom fields using Filter.
      GenericObject geoCustFieldData = null;
      geoCustFieldData = geoTranOrTender.get(strDBfield, "") as GenericObject;
      if (geoCustFieldData != null)
      {

        int iCountofCFD = geoCustFieldData.getLength();
        for (int i = 0; i < iCountofCFD; i++)
        {
          GenericObject pCustomField = geoCustFieldData.get(i) as GenericObject;
          string strCUSTTAG = pCustomField.get("CUSTTAG", "") as string;
          string strCUSTVALUE = pCustomField.get("CUSTVALUE", "") as string;
          if (!string.IsNullOrEmpty(strCUSTVALUE))
            geoCustData.set(strCUSTTAG, strCUSTVALUE);
        }
      }
      return geoCustData;
    }



    // Write Raw XML into a temp file
    public override bool Project_ProcessDeposits(GenericObject pFileDeposits, GenericObject pConfigSystemInterface, object pProductLevelMainClass, string strSysInterface, GenericObject pCurrentFile, ref eReturnAction ReturnAction) // Bug 15464
    {
      string strError = "";
      string strFileName = pFileDeposits.get("FULL_FILE_NAME").ToString();
      CASL_engine.BatchUpdate_ProductLevel pBatchUpdate_ProductLevel = (BatchUpdate_ProductLevel)pProductLevelMainClass;

      if (m_ConfigSystemInterface == null)
        m_ConfigSystemInterface = pConfigSystemInterface;

      // BEGIN: Bug 14744 (Bug 15464) -Duplicate CORE Files in Standard PB Batch Update
      // PRIOR TO WRITING TO THE FILE: Make certain that the XML File Batch Info is correct for the Current File being processed
      bool CorrectCOREFile = true;
      if (m_XMLFileBatchInfo != null && pCurrentFile != null)
      {
        if (pCurrentFile.vectors.Count > 0)
        {
          string CurrentCOREFile = ((GenericObject)(pCurrentFile.vectors[0])).get("FILENAME", "").ToString().Trim();
          string BatchInfoCOREFile = m_XMLFileBatchInfo.m_strFileName.Trim();
          if (!string.IsNullOrEmpty(CurrentCOREFile) && !string.IsNullOrEmpty(BatchInfoCOREFile))
          {
            if (CurrentCOREFile.Length == BatchInfoCOREFile.Length) // Make certain they are formatted the same
            {
              CorrectCOREFile = (CurrentCOREFile == BatchInfoCOREFile);
            }
          }
        }
      }
      // END  : Bug 14744 (Bug 15464) -Duplicate CORE Files in Standard PB Batch Update

      bool bReturn = true;

      if (m_XMLFileBatchInfo == null) return true;//BUG13617  
      try
      {
        //Bug#13532 QG if bProcessEverything, an error has been logged already in ProcessEvents, just return false here.
        bool bProcessEverything = (bool)pConfigSystemInterface.get("process_everything_in_file", false);
        if (!bProcessEverything)
        {
          return false;
        }

        // Bug 26837 UMN
        bool useFieldsElement = ((string)pConfigSystemInterface.get("element_type", "NAMED") == "FIELDS") ? true : false;

        string strFileContent = BuildFileContentForTheCoreFile(useFieldsElement);
        if (!String.IsNullOrEmpty(strFileContent) && CorrectCOREFile)  // Bug#13617, Bug#14744 (Bug 15464)
        {
          string strTempPath = pConfigSystemInterface.get("local_file_path_for_temp", "").ToString();
          string strTempFilename = pConfigSystemInterface.get("local_file_name_for_temp", "").ToString();
          //Bug#13535 QG If error in CreateAndWriteFile, Call ProductLevel_AddSystemInterfaceError to terminate BatchUpdate.
          string strTempFile = Path.Combine(strTempPath, strTempFilename);
          if (!CreateAndWriteFile(strTempFile, strFileContent, ref strError))
            throw new Exception(strError.Replace("'", ""));
          m_iCountCOREFiles++;
        }

        // BEGIN: Bug 14744 (Bug 15464) -Duplicate CORE Files in Standard PB Batch Update
        // Clear the Batch Info for this CORE file after writing to temp flat file (However, do NOT set m_XMLFileBatchInfo to null)
        if (m_XMLFileBatchInfo != null)
        {
          m_XMLFileBatchInfo.m_geoXMLCoreEvents.removeAll();
        }
        // END  : Bug 14744 (Bug 15464) -Duplicate CORE Files in Standard PB Batch Update

      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in XMLFile Project_ProcessDeposits", e);

        //Bug 13535 QG change error handling.
        Logger.LogError(e.Message, "XML Batch Update");
        pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError(BU_Const.sSYS_INT_SCOPE, strSysInterface, "Project_ProcessDeposit:" + e.Message);
        return false;
      }
      return bReturn;
    }

    /// <summary>
    /// Bug 15964: XML Pmt File SFTP'd - Added method
    /// This method returns the file name with formatted date/time portion with today's date/time.
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    public static string FormatFileNameWithDateTime(string fileName)
    {
      if (fileName.Contains("[") && fileName.Contains("]"))
      {
        int idxStart = fileName.IndexOf("[");
        int idxFinish = fileName.IndexOf("]");
        if (idxStart < idxFinish)
        {
          string dateFormat = fileName.Substring(idxStart, (idxFinish - idxStart + 1));
          string datePortion = dateFormat.Length > 2 ? DateTime.Now.ToString(dateFormat.Substring(1, dateFormat.Length - 2)) : "";
          fileName = fileName.Replace(dateFormat, datePortion);
        }
      }
      return fileName;
    }


    // Merge XML in temp file to the destination 
    public override bool Project_WriteTrailer(object pProductLevelMainClass, string strSysInterface, ref bool bEarlyTerminationRequestedByProject)
    {
      if (bEarlyTerminationRequestedByProject) return false; //Bug#13535 QG Terminate the process 

      Logger.LogInfo("START XMLFileBatchUpdate Project_WriteTrailer", "XML Batch Update");
      CASL_engine.BatchUpdate_ProductLevel pBatchUpdate_ProductLevel = (BatchUpdate_ProductLevel)pProductLevelMainClass;

      //Bug#13617 (Bug 15464) QG If no Transaction/Tender has SI assigned, send a notice and return true(to mark core file as updated).
      if (m_XMLFileBatchInfo == null)
      {
        Logger.LogInfo("No valid transaction or tenders in the core file.", "XML Batch Update");
        pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError(BU_Const.sSYS_INT_SCOPE, strSysInterface, "Project_WriteTrailer: No valid transaction or tenders in the core file.");
        return true;
      }

      // The "SendFileToFTP" Method will be used to ftp/sftp the local file - all data was written in the Project_ProcessDeposits
      string strError = "";
      string strBatchTempFilePath = m_ConfigSystemInterface.get("local_file_path_for_temp", "") as string;
      string strBatchTempFileName = FormatFileNameWithDateTime(m_ConfigSystemInterface.get("local_file_name_for_temp", "") as string); // Bug 15964: XML Pmt File SFTP'd
      string strBatchLocalFilePath = m_ConfigSystemInterface.get("local_file_path", "") as string;
      string strBatchLocalFileName = FormatFileNameWithDateTime(m_ConfigSystemInterface.get("local_file_name", "") as string); // Bug 15964: XML Pmt File SFTP'd
      string strBatchIfFileExists = m_ConfigSystemInterface.get("if_local_XML_file_exists", "Append") as string; // Bug 14756
      string strBatchUpdateMethod = ((string)m_ConfigSystemInterface.get("method", "")).ToLower() as string;
      string strBatchFTPAddress = m_ConfigSystemInterface.get("ftp_address", "") as string;
      string strBatchFTPPort = m_ConfigSystemInterface.get("ftp_port", "") as string;
      // BEGIN Bug 15964: XML Pmt File SFTP'd
      string strBatchFTPPath = m_ConfigSystemInterface.get("ftp_path", "").ToString();
      string strBatchFTPFileName = FormatFileNameWithDateTime(m_ConfigSystemInterface.get("ftp_filename", "") as string);
      if (!string.IsNullOrEmpty(strBatchFTPPath.Trim()))
      {
        strBatchFTPFileName = Path.Combine(strBatchFTPPath, strBatchFTPFileName);
      }
      // END Bug 15964: XML Pmt File SFTP'd
      string strBatchFTPUserName = m_ConfigSystemInterface.get("ftp_username", "") as string;
      string strBatchFTPPassword = m_ConfigSystemInterface.get("ftp_password", "") as string;

      //Bug#13646 Decrypt FTP Password
      if (!string.IsNullOrEmpty(strBatchFTPPassword))
      {
        string secret_key = Crypto.get_secret_key();
        byte[] content_bytes = Convert.FromBase64String(strBatchFTPPassword);
        byte[] decrypted_bytes = Crypto.symmetric_decrypt("data", content_bytes, "secret_key", secret_key);
        strBatchFTPPassword = System.Text.Encoding.Default.GetString(decrypted_bytes);
      }
      string strBatchTempFile = Path.Combine(strBatchTempFilePath, strBatchTempFileName);
      try
      {
        if (strBatchUpdateMethod.ToLower() == "folder")
        {
          //Bug# 13535 QG Tweak code
          string strBatchLocalFile = Path.Combine(strBatchLocalFilePath, strBatchLocalFileName);

          // BEGIN: Bug 14756 (Bug 15464) - PB Batch Update SI - New Config Property for Existing Local XML File
          if (File.Exists(strBatchLocalFile))
          {
            switch (strBatchIfFileExists)
            {
              case "Stop": // Return Error message and stop the BU processing.
                {
                  strError = string.Format("Error: The local XML batch file already exists: {0}", strBatchLocalFile);
                  Logger.LogError(strError, "XML Batch Update Project_WriteTrailer");
                  bEarlyTerminationRequestedByProject = true;
                  pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError(BU_Const.sSYS_INT_SCOPE, strSysInterface, "Project_WriteTrailer" + strError);
                  return false;
                }

              case "Overwrite": // Delete the local file (if there's data to write from Temp file) so that a new one will be written.
                if (File.Exists(strBatchTempFile))
                {
                  File.Delete(strBatchLocalFile);
                }
                break;

              default: // Default to Append.
                break;
            }
          }
          // END  : Bug 14756 (Bug 15464) - PB Batch Update SI - New Config Property for Existing Local XML File

          if (File.Exists(strBatchTempFile))
          {
            //Bug#13646 QG Log error if fail to copy file locally.
            if (!SendFileToFolder(strBatchTempFile, strBatchLocalFile, ref strError))
              throw new Exception(strError);
          }
        }
        else if (strBatchUpdateMethod == "ftp" || strBatchUpdateMethod == "sftp")
        {
          // Bug 13646 QG Log error if fail to upload file via ftp.
          // Bug 15964: XML Pmt File SFTP'd
          if (!SendFileToFTP(strBatchUpdateMethod, strBatchTempFile, strBatchFTPAddress, strBatchFTPPort, strBatchFTPUserName, strBatchFTPPassword, strBatchFTPFileName, strBatchIfFileExists, ref strError))
          {
            throw new Exception(strError);
          }
        }
        // Can not get Method from System Interface
        else
        {
          strError = string.Format("Error getting Batch Update Method from PB Standard Batch Update.");
          throw new Exception(strError);
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error inXML Batch Update Project_WriteTrailer", e);

        strError = e.Message.Replace("'", " ");
        Logger.LogError(strError, "XML Batch Update Project_WriteTrailer");
        bEarlyTerminationRequestedByProject = true;
        pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError(BU_Const.sSYS_INT_SCOPE, strSysInterface, "Project_WriteTrailer: " + strError);
        return false;
      }
      finally // Bug 15464
      {
        if (File.Exists(strBatchTempFile))
          File.Delete(strBatchTempFile);
      }
      //End of Bug#13646
      return true;
    }

    public bool SendFileToFolder(string FilePathFrom, string FilePathTo, ref string strError)
    {
      try
      {
        string FileContent = File.ReadAllText(FilePathFrom);
        if (File.Exists(FilePathTo))
        {
          // Do nothing, ready to append
        }
        else
        {
          // BEGIN Bug 15964: XML Pmt File SFTP'd
          FileStream fs = null;
          try
          {
            fs = new FileStream(FilePathTo, FileMode.Create);
            using (XmlWriter xw = XmlWriter.Create(fs))
            {
              xw.WriteStartElement("BatchUpdate");
              xw.WriteEndElement();
              xw.Flush();
              xw.Close();
            }
            fs.Flush();
            fs.Close();
          }
          finally
          {
            if (fs != null)
              fs.Dispose();
          }
          // END Bug 15964: XML Pmt File SFTP'd
        }

        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(FilePathTo);
        XmlElement elmXML = xmlDoc.CreateElement("UpdateRequest");
        XmlAttribute attrType = xmlDoc.CreateAttribute("Type", "Type", elmXML.NamespaceURI);
        attrType.Value = "Batch Update";
        elmXML.Attributes.Append(attrType);
        XmlAttribute attrCount = xmlDoc.CreateAttribute("Count", "Count", elmXML.NamespaceURI);
        attrCount.Value = m_iCountCOREFiles.ToString();
        elmXML.Attributes.Append(attrCount);
        // Remove line feed to provide a clean InnerXml, so that it can be saved with indent format
        elmXML.InnerXml = FileContent.Replace("\r\n", "");
        xmlDoc.DocumentElement.AppendChild(elmXML);
        xmlDoc.Save(FilePathTo);
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in XML Batch Update SendFileToFolder", e);

        strError = string.Format("Cannot Write to File({0}):{1}", FilePathTo, e.Message);
        // Bug #14825 (Bug 15464) Mike O - Fixed typo
        Logger.LogError(strError, "XML Batch Update SendFileToFolder");
        return false;
      }
      return true;
    }

    /// <summary>
    /// Bug 15964: XML Pmt File SFTP'd - Added support for SFTP and the "File Exists" action (numerous changes below).
    /// NOTE: Configurable "File Exists" Action may be: "Stop" "Append" "Overwrite"
    /// </summary>
    /// <param name="mode">"ftp" or "sftp"</param>
    /// <param name="FilePathFrom"></param>
    /// <param name="strFTPAddress"></param>
    /// <param name="strFTPPort"></param>
    /// <param name="strFTPUsername"></param>
    /// <param name="strFTPPassword"></param>
    /// <param name="strFTPFilename"></param>
    /// <param name="strFileExistsAction">"Stop" "Append" or "Overwrite"</param>
    /// <param name="strError"></param>
    /// <returns></returns>
    public bool SendFileToFTP(string mode, string FilePathFrom, string strFTPAddress, string strFTPPort, string strFTPUsername, string strFTPPassword, string strFTPFilename, string strFileExistsAction, ref string strError)
    {
      FileTransfer file_transfer = null;

      try
      {
        bool bFileExists = false;
        string FileContent = File.ReadAllText(FilePathFrom);
        File.Delete(FilePathFrom);

        switch (mode)
        {
          case "ftp":
            file_transfer = new FTP(strFTPAddress, strFTPUsername, strFTPPassword, strFTPPort);
            break;

          case "sftp":
            file_transfer = new SFTP(strFTPAddress, strFTPUsername, strFTPPassword, strFTPPort);
            break;
        }
        file_transfer.Connect();
        bFileExists = file_transfer.FileExists(strFTPFilename);
        if (bFileExists && strFileExistsAction == "Append")
        {
          file_transfer.GetFile(strFTPFilename, FilePathFrom); // Get existing file to append new data
        }

        if (bFileExists && strFileExistsAction == "Stop") // Return Error message and stop the BU processing.
        {
          strError = string.Format("Error: The XML batch file already exists on {0} server: {1}"
          , mode.ToUpper(), strFTPFilename);
          Logger.LogError(strError, "XML Batch Update SendFileToFTP");
          return false;
        }

        // Create file if it does not exist or will be overwritten
        if (!bFileExists || (bFileExists && strFileExistsAction == "Overwrite"))
        {
          FileStream fs = null;
          try
          {
            fs = new FileStream(FilePathFrom, FileMode.Create);
            using (XmlWriter xw = XmlWriter.Create(fs))
            {
              xw.WriteStartElement("BatchUpdate");
              xw.WriteEndElement();
              xw.Flush();
              xw.Close();
              fs.Flush();
              fs.Close();
            }
          }
          finally
          {
            if (fs != null)
              fs.Dispose();
          }
        }

        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(FilePathFrom);
        XmlElement elmXML = xmlDoc.CreateElement("UpdateRequest");
        XmlAttribute attrType = xmlDoc.CreateAttribute("Type", "Type", elmXML.NamespaceURI);
        attrType.Value = "Batch Update";
        elmXML.Attributes.Append(attrType);
        XmlAttribute attrCount = xmlDoc.CreateAttribute("Count", "Count", elmXML.NamespaceURI);
        attrCount.Value = m_iCountCOREFiles.ToString();
        elmXML.Attributes.Append(attrCount);
        // Remove line feed to provide a clean InnerXml, so that it can be saved with indent format
        elmXML.InnerXml = FileContent.Replace("\r\n", "");
        xmlDoc.DocumentElement.AppendChild(elmXML);
        xmlDoc.Save(FilePathFrom);

        file_transfer.SendFile(FilePathFrom, strFTPFilename);
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in XMLFile SendFileToFTP", e);

        strError = string.Format("Error attempting to {0} File ({1}): {2}"
        , mode.ToUpper()
        , strFTPFilename
        , e.Message);
        return false;
      }
      finally
      {
        // Destructor will call Disconnect, with .NET it's async scheduled. Calling here will free the connection faster
        if (file_transfer != null) file_transfer.Disconnect();
      }
      return true;
    }

    //Bug#12053 QG Write Core File information into XML format
    public void BuildCoreFileContent(CXMLFileBatchInfo pXMLFileBatchInfo, ref StringBuilder sbFileContent)
    {
      sbFileContent.AppendLine(BuildXMLElement("DeptID", pXMLFileBatchInfo.m_strDeptID));
      sbFileContent.AppendLine(BuildXMLElement("FileName", pXMLFileBatchInfo.m_strFileName));
      sbFileContent.AppendLine(BuildXMLElement("FileType", pXMLFileBatchInfo.m_strFileType));
      sbFileContent.AppendLine(BuildXMLElement("FileDesc", pXMLFileBatchInfo.m_strFileDesc));
      sbFileContent.AppendLine(BuildXMLElement("AutoBalance", pXMLFileBatchInfo.m_strAutoBalance));
      sbFileContent.AppendLine(BuildXMLElement("Open_UserID", pXMLFileBatchInfo.m_strOpenUserID));
      sbFileContent.AppendLine(BuildXMLElement("Creator_ID", pXMLFileBatchInfo.m_strCreatorID));
      sbFileContent.AppendLine(BuildXMLElement("Bal_UserID", pXMLFileBatchInfo.m_strBalUserID));
      sbFileContent.AppendLine(BuildXMLElement("Acc_UserID", pXMLFileBatchInfo.m_strAccUserID));
      sbFileContent.AppendLine(BuildXMLElement("Update_UserID", pXMLFileBatchInfo.m_strUpdateUserID));
      sbFileContent.AppendLine(BuildXMLElement("Close_UserID", pXMLFileBatchInfo.m_strCloseUserID));
      sbFileContent.AppendLine(BuildXMLElement("EffectiveDT", string.Format("{0:MM/dd/yyyy}", pXMLFileBatchInfo.m_dtEffective)));
      sbFileContent.AppendLine(BuildXMLElement("OpenDT", string.Format("{0:MM/dd/yyyy}", pXMLFileBatchInfo.m_dtOpen)));
      sbFileContent.AppendLine(BuildXMLElement("BalDT", string.Format("{0:MM/dd/yyyy}", pXMLFileBatchInfo.m_dtBal)));
      sbFileContent.AppendLine(BuildXMLElement("AccDT", string.Format("{0:MM/dd/yyyy}", pXMLFileBatchInfo.m_dtAcc)));
      sbFileContent.AppendLine(BuildXMLElement("UpdateDT", string.Format("{0:MM/dd/yyyy}", pXMLFileBatchInfo.m_dtUpdate)));
      sbFileContent.AppendLine(BuildXMLElement("CloseDT", string.Format("{0:MM/dd/yyyy}", pXMLFileBatchInfo.m_dtClose)));
      sbFileContent.AppendLine(BuildXMLElement("BalTotal", pXMLFileBatchInfo.m_strBalTotal));
      sbFileContent.AppendLine(BuildXMLElement("Source_Type", pXMLFileBatchInfo.m_strSourceType));
      sbFileContent.AppendLine(BuildXMLElement("Source_Date", string.Format("{0:MM/dd/yyyy}", pXMLFileBatchInfo.m_dtSource)));
      sbFileContent.AppendLine(BuildXMLElement("POSTDT", string.Format("{0:MM/dd/yyyy}", pXMLFileBatchInfo.m_dtPOSTDT)));
      //Bug#14386 (Bug 15464) QG Add Workgroup name by pulling it out from Config
      GenericObject geoDepartment = ((GenericObject)c_CASL.c_object("Department.of")).get(pXMLFileBatchInfo.m_strDeptID, c_CASL.GEO) as GenericObject;
      sbFileContent.AppendLine(BuildXMLElement("DeptName", geoDepartment.get("name", "") as string));
    }

    //Bug#12053 QG Write Core Event information, including transactions/tenders into XML format
    public void BuildCoreEventContent(CXMLEventRecord pXMLEventRecord, ref StringBuilder sbFileContent, bool useFieldsElement)
    {
      string line;
      sbFileContent.AppendLine(BuildXMLElement("ReceiptReferenceNbr", pXMLEventRecord.m_strReceiptRefNbr));
      sbFileContent.AppendLine(BuildXMLElement("UserID", pXMLEventRecord.m_strUserID));
      sbFileContent.AppendLine(BuildXMLElement("Status", pXMLEventRecord.m_strStatus));
      sbFileContent.AppendLine(BuildXMLElement("Source_Type", pXMLEventRecord.m_strSourceType));
      sbFileContent.AppendLine(BuildXMLElement("Mod_DT", string.Format("{0:MM/dd/yyyy}", pXMLEventRecord.m_dtModification)));
      sbFileContent.AppendLine(BuildXMLElement("Creation_DT", string.Format("{0:MM/dd/yyyy}", pXMLEventRecord.m_dtCreation)));
      sbFileContent.AppendLine(BuildXMLElement("Source_Date", string.Format("{0:MM/dd/yyyy}", pXMLEventRecord.m_dtSource)));
      // More Event level Content here

      // Transactions
      int iCountTransaction = pXMLEventRecord.m_geoTransactions.getIntKeyLength();
      line = string.Format("<Transactions Count='{0}'>", iCountTransaction);
      sbFileContent.AppendLine(line);
      for (int i = 0; i < iCountTransaction; i++)
      {
        //Bug#12053 Write Transaction Information into XML format
        CXMLTransactionRecord pXMLTransRecord = pXMLEventRecord.m_geoTransactions.get(i) as CXMLTransactionRecord;
        sbFileContent.AppendLine("<Transaction>");
        sbFileContent.AppendLine(BuildXMLElement("TRANNBR", pXMLTransRecord.m_strTranNbr));
        sbFileContent.AppendLine(BuildXMLElement("TTID", pXMLTransRecord.m_strTTID));
        sbFileContent.AppendLine(BuildXMLElement("TTDESC", pXMLTransRecord.m_strTTDesc));
        sbFileContent.AppendLine(BuildXMLElement("AMOUNT", string.Format("{0:###0.00}", pXMLTransRecord.m_strTranAMT)));
        sbFileContent.AppendLine(BuildXMLElement("ITEMIND", pXMLTransRecord.m_strItemInd));
        sbFileContent.AppendLine(BuildXMLElement("DISTAMOUNT", string.Format("{0:###0.00}", pXMLTransRecord.m_dDistAMT)));  //Bug#13495
        sbFileContent.AppendLine(BuildXMLElement("SOURCE_TYPE", pXMLTransRecord.m_strSourceType));                          //Bug#13495
        sbFileContent.AppendLine(BuildXMLElement("POSTDATE", string.Format("{0:MM/dd/yyyy}", pXMLTransRecord.m_dtPostDT))); //Bug#13495
        sbFileContent.AppendLine(BuildXMLElement("POSTTIME", string.Format("{0:HH:mm:ss}", pXMLTransRecord.m_dtPostDT)));  //Bug#13495

        // Bug 26837 UMN use fields element if configured
        GenericObject custFields = pXMLTransRecord.m_geoCustomFields;
        AppendFields(sbFileContent, custFields, useFieldsElement);
        sbFileContent.AppendLine("</Transaction>");
      }
      sbFileContent.AppendLine("</Transactions>");
      // Tenders
      int iCountTender = pXMLEventRecord.m_geoTenders.getIntKeyLength();
      line = string.Format("<Tenders Count='{0}'>", iCountTender);
      sbFileContent.AppendLine(line);
      for (int i = 0; i < iCountTender; i++)
      {
        //Bug#12053 QG Write Tender Information into XML format.
        CXMLTenderRecord pXMLTenderRecord = pXMLEventRecord.m_geoTenders.get(i) as CXMLTenderRecord;
        sbFileContent.AppendLine("<Tender>");
        sbFileContent.AppendLine(BuildXMLElement("TNDRNBR", pXMLTenderRecord.m_strTenderNbr));
        sbFileContent.AppendLine(BuildXMLElement("TNDRID", pXMLTenderRecord.m_strTenderID));
        sbFileContent.AppendLine(BuildXMLElement("TNDRDESC", pXMLTenderRecord.m_strTenderDesc));
        sbFileContent.AppendLine(BuildXMLElement("TYPEIND", pXMLTenderRecord.m_strTypeInd));
        sbFileContent.AppendLine(BuildXMLElement("AMOUNT", string.Format("{0:###0.00}", pXMLTenderRecord.m_dAmount)));
        sbFileContent.AppendLine(BuildXMLElement("CC_CK_NBR", pXMLTenderRecord.m_strCCCKNbr));
        sbFileContent.AppendLine(BuildXMLElement("CCNAME", pXMLTenderRecord.m_strCCName));
        sbFileContent.AppendLine(BuildXMLElement("AUTHNBR", pXMLTenderRecord.m_strAuthNbr));
        sbFileContent.AppendLine(BuildXMLElement("AUTHSTRING", pXMLTenderRecord.m_strAuthString));
        sbFileContent.AppendLine(BuildXMLElement("AUTHACTION", pXMLTenderRecord.m_strAuthAction));
        sbFileContent.AppendLine(BuildXMLElement("BANKROUTINGNBR", pXMLTenderRecord.m_strBankRouting));
        sbFileContent.AppendLine(BuildXMLElement("BANKACCTNBR", pXMLTenderRecord.m_strBankAcctNbr));
        sbFileContent.AppendLine(BuildXMLElement("ADDRESS", pXMLTenderRecord.m_strAddress));
        sbFileContent.AppendLine(BuildXMLElement("BANKACCTID", pXMLTenderRecord.m_strBankAcctID));
        sbFileContent.AppendLine(BuildXMLElement("POSTDATE", string.Format("{0:MM/dd/yyyy}", pXMLTenderRecord.m_dtPostDT))); //Bug#13495
        sbFileContent.AppendLine(BuildXMLElement("POSTTIME", string.Format("{0:HH:mm:ss}", pXMLTenderRecord.m_dtPostDT))); //Bug#13495

        // Bug 26837 UMN use fields element if configured
        GenericObject custFields = pXMLTenderRecord.m_geoCustomFields;
        AppendFields(sbFileContent, custFields, useFieldsElement);
        sbFileContent.AppendLine("</Tender>");
      }
      sbFileContent.AppendLine("</Tenders>");
    }

    private void AppendFields(StringBuilder sbFileContent, GenericObject custFields, bool useFieldsElement)
    {
      if (useFieldsElement)
        sbFileContent.AppendLine("<Fields>");

      ArrayList keys = custFields.getAllKeys();
      foreach (string key in keys)
      {
        sbFileContent.AppendLine(BuildXMLFieldElement(key, custFields.get(key, "").ToString(), useFieldsElement));
      }
      if (useFieldsElement)
        sbFileContent.AppendLine("</Fields>");
    }

    public string BuildXMLElement(string strTag, string strValue)
    {
      if (String.IsNullOrEmpty(strValue.Trim())) return null;   //Bug#12053 QG Don't write XML Element if value is null or empty string. Trim the value
      // Bug #14825 (Bug 15464) Mike O - Escape all XML text, except for "<" and ">"
      return string.Format("<{0}>{1}</{0}>", strTag, SecurityElement.Escape(strValue).Replace("&lt;", "<").Replace("&gt;", ">"));
    }

    public string BuildXMLFieldElement(string strTag, string strValue, bool useFieldsElement)
    {
      if (!useFieldsElement)
        return BuildXMLElement(strTag, strValue);

      if (String.IsNullOrEmpty(strValue.Trim())) return null;   //Bug#12053 QG Don't write XML Element if value is null or empty string. Trim the value
      
      // Bug #14825 (Bug 15464) Mike O - Escape all XML text, except for "<" and ">"
      return string.Format("<Field key=\"{0}\">{1}</Field>", strTag, SecurityElement.Escape(strValue).Replace("&lt;", "<").Replace("&gt;", ">"));
    }

    public string BuildFileContentForTheCoreFile(bool useFieldsElement)
    {
      System.Text.StringBuilder sbFileContent = new StringBuilder();
      XmlDocument xmlDoc = new XmlDocument();
      // Bug#13617 (Bug 15464) QG If there is no valid transactions, x_XMLFileBatchInfo will be null, don't write anything.
      if (m_XMLFileBatchInfo != null)
      {
        int iCountEvents = m_XMLFileBatchInfo.m_geoXMLCoreEvents.getIntKeyLength();
        if (iCountEvents == 0)
          return null;
        string line;
        sbFileContent.AppendLine("<COREFile>");
        //Bug#14386 (Bug 15464) QG Remove duplicate POSTDT
        //line = string.Format("<POSTDT>{0:MM/dd/yyyy}</POSTDT>", m_XMLFileBatchInfo.m_dtPOSTDT);
        //sbFileContent.AppendLine(line);
        BuildCoreFileContent(m_XMLFileBatchInfo, ref sbFileContent);   //Bug#12053 QG
        line = string.Format("<COREEvents Count='{0}'>", iCountEvents);
        sbFileContent.AppendLine(line);
        for (int i = 0; i < iCountEvents; i++)
        {
          sbFileContent.AppendLine("<COREEvent>");
          CXMLEventRecord pXMLEventRecord = m_XMLFileBatchInfo.m_geoXMLCoreEvents.get(i) as CXMLEventRecord;
          BuildCoreEventContent(pXMLEventRecord, ref sbFileContent, useFieldsElement);    //Bud#12053 QG
          sbFileContent.AppendLine("</COREEvent>");
        }
        sbFileContent.AppendLine("</COREEvents>");
        sbFileContent.AppendLine("</COREFile>");
      }
      return sbFileContent.ToString();
    }

    public string GetReceiptRefNbr(GenericObject geoTran)
    {
      string pReceiptRefNbr = String.Format("{0}{1}-{2}",
                         geoTran.get("DEPFILENBR", "").ToString(),
                         geoTran.get("DEPFILESEQ", "").ToString().PadLeft(3, '0'),
                         geoTran.get("EVENTNBR", "").ToString());
      return pReceiptRefNbr;
    }

    public bool CreateAndWriteFile(string FilePath, string FileContent, ref string strError)
    {
      try
      {
        if (m_XMLFileBatchInfo.m_pOutputConnection == null)
          m_XMLFileBatchInfo.m_pOutputConnection = new OutputConnection();
        if (m_XMLFileBatchInfo.m_pOutputConnection.m_pOutputFile == null)
        {
          m_XMLFileBatchInfo.m_pOutputConnection.m_pOutputFile = new StreamWriter(FilePath);
        }
        else
        {
          if (File.Exists(FilePath))
          {
            m_XMLFileBatchInfo.m_pOutputConnection.m_pOutputFile = File.AppendText(FilePath);
          }
          else
          {
            m_XMLFileBatchInfo.m_pOutputConnection.m_pOutputFile = new StreamWriter(FilePath);
          }
        }
        m_XMLFileBatchInfo.m_pOutputConnection.m_pOutputFile.Write(FileContent);
        m_XMLFileBatchInfo.m_pOutputConnection.m_pOutputFile.Flush();
        // Bug 16433 [21921] UMN handle close in finally clause
        // m_XMLFileBatchInfo.m_pOutputConnection.m_pOutputFile.Close();
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in XMLFile CreateAndWriteFile", e);

        strError = string.Format("Cannot Write to File({0}):{1}", FilePath, e.Message);
        return false;
      }
      finally
      {
        // Bug 16433 [21921] UMN handle close in finally clause
        if ((m_XMLFileBatchInfo.m_pOutputConnection != null) && 
            (m_XMLFileBatchInfo.m_pOutputConnection.m_pOutputFile != null))
        {
          m_XMLFileBatchInfo.m_pOutputConnection.m_pOutputFile.Close();
        }
      }

      return true;
    }

    public override void Project_Rollback(object pProductLevelMainClass, bool bEarlyTerminationRequestedByProject)
    {
      // Add code here if you want to do something on failure (delete the file?)
    }
  }

}