﻿using System;
using System.Collections;
using System.Net;
using System.IO;
using System.Text;
using CASL_engine;
using System.Xml;
using System.Xml.Serialization;
using ftp;
using TranSuiteServices;

//TO DO GL DESC override need to increase size of CUSTVALUE from 100 to ...
//report base issue progress bar not moving across screen at bu completion (maybe only on failure test this)
//ask dennis batch header is all recs d+c or just d
//to docheck code logic for dc 60 10 debit credit
//TO DO CHECK THAT TRANS IS CONFIGURED FOR FIN BATCH UPDATE
//TO DO need base fix for FILE IS GETTING MARKED AS UPDATED ON FAILURE
//TO DO need base fix for update_sys_int.. is not reliable due to TOO LONG issue
//TO DO need base fix for TTA_MAP currently using my fix for this
//for all upon error BU make sure all paths write to log and return error message to product and 
/// DO NOT THROW EXCEPTION because refelection code exception is embeded in inner exception

namespace ProjectBaseline
{
  /// <summary>
  /// CFlatFileBatchInfo is used to save information of a CoreFile
  /// </summary>
  public class CFlatFileBatchInfo
  {
    public OutputConnection m_pOutputConnection = null;
    public GenericObject m_geoFlatFileBatch = new GenericObject(); // collection of FlatFileRecord
    public Nullable<DateTime> m_dtEFFECTIVEDT; // CORE File Effective Date

    public CFlatFileBatchInfo()
    {
    }

    // Extract required fields from Transaction 
    public CFlatFileRecord BuildFlatFileRec(GenericObject geoCustData, GenericObject geoTran, GenericObject geoFile)
    {
      CFlatFileRecord pFlatFileRecord = new CFlatFileRecord();
      pFlatFileRecord.m_strEFFDATE = String.Format("{0:MM/dd/yyyy}", geoFile.get("EFFECTIVEDT", ""));
      pFlatFileRecord.m_strReceiptRefNbr = String.Format("{0}{1}-{2}", geoTran.get("DEPFILENBR", "").ToString(), geoTran.get("DEPFILESEQ", "").ToString().PadLeft(3, '0'), geoTran.get("EVENTNBR", "").ToString());
      pFlatFileRecord.m_strTransTypeID = geoTran.get("TTID", "").ToString();
      pFlatFileRecord.m_strGroup = geoCustData.get("Group", "").ToString();
      pFlatFileRecord.m_dTRAN_AMT = (double)geoTran.get("TRANAMT", "");
      pFlatFileRecord.m_geoCustomFields.Clear();
      ArrayList keys = geoCustData.getStringKeys();
      foreach (string key in keys)
        if ((key != "Group") && (key != ""))
          pFlatFileRecord.m_geoCustomFields.Add(key, geoCustData.get(key, ""));
      return pFlatFileRecord;
    }

    //public static string Truncate(string source, int length)
    //{
    //    source = source.Trim();
    //    if (source.Length > length)
    //    {
    //        source = source.Substring(0, length);
    //    }
    //    return source;
    //}

    // Add a FlatFile Record into CFlatFileBatchInfo.m_geoFlatFileBatch
    public bool AddFlatFileRecord(GenericObject geoTran, GenericObject geoCustData, GenericObject geoFile, string strSystemInterface, ref string strError)
    {
      CFlatFileRecord pFlatFileRec = new CFlatFileRecord();
      try
      {
        pFlatFileRec = BuildFlatFileRec(geoCustData, geoTran, geoFile);
        m_geoFlatFileBatch.insert(pFlatFileRec);
      }
      catch (Exception e)
      {
        strError = "Error in AddFlatFileRecord" + e.Message;

        // Bug 17849
        Logger.cs_log_trace("Error in AddFlatFileRecord", e);
        return false;
      }
      return true;
    }
  }


  /// <summary>
  /// Details of a corefile
  /// </summary>
  public class CFlatFileRecord
  {
    public string m_strEFFDATE = "";         // MM/DD/YYYY iPayment Effective date for the CORE File
    public string m_strReceiptRefNbr = "";   // 2011222001-1 Receipt Reference Nubmer
    public string m_strTransTypeID = "";     // TTA  Transaction Type ID
    public string m_strGroup = "";           // Hospital  Group value of the payment as associated with the original SI Table
    public double m_dTRAN_AMT = 0.00;        // 100.00  Amount of the Transaction
    public GenericObject m_geoCustomFields = new GenericObject(); // detail value of the payment as associated with the original SI Table
    public CFlatFileRecord()
    {
    }
  } // end class CFlatFileRecord

  public class FlatFileBatchUpdate : AbstractBatchUpdate
  {
    public CFlatFileBatchInfo m_FlatFileBatchInfo = new CFlatFileBatchInfo(); // For collection of CFlatFileRecord
    public GenericObject m_ConfigSystemInterface = null;                      // System Interface Info
    public GenericObject m_FileContentList = new GenericObject();             // Records are saved in different files based on transaction type, 
    // transaction types are saved in this list.

    public FlatFileBatchUpdate()
    {
    }

    public override AbstractBatchUpdate GetInstance(GenericObject sysInt)
    {
      string strFileType = sysInt.get("file_type", "") as string;
      Type pClassType = null;
      if (strFileType.ToLower() == "flat file")
        pClassType = misc.get_type("ProjectBaseline", "FlatFileBatchUpdate");
      else if (strFileType.ToLower() == "xml file")
        pClassType = misc.get_type("ProjectBaseline", "XMLFileBatchUpdate");
      AbstractBatchUpdate pSIClassInstance = (AbstractBatchUpdate)Activator.CreateInstance(pClassType);

      return pSIClassInstance;
    }

    public override string GetCASLTypeName()
    {
      return "PBStandardBatchUpdate";
    }

    public override bool Project_ProcessCoreEvent(string strSystemInterface, GenericObject pEventGEO, GenericObject pConfigSystemInterface, GenericObject pFileGEO, object pProductLevelMainClass, string strCurrentFile, ref eReturnAction ReturnAction) // Bug 15464 DJD - Batch Update Overhaul
    {
      CASL_engine.BatchUpdate_ProductLevel pBatchUpdate_ProductLevel = (BatchUpdate_ProductLevel)pProductLevelMainClass;
      string strError = "";
      //If we return false from here call Cleanup to clear all batch level info
      //the product will abort processing remaining events so we do not want partial information 
      //for the CORE file in the batch update file.. the product code is being worked on 
      //so this area may need to be reworked
      try
      {
        pFileGEO = pFileGEO.get(0, null) as GenericObject;
        if (m_ConfigSystemInterface == null)
          m_ConfigSystemInterface = pConfigSystemInterface;
        if (m_FlatFileBatchInfo.m_dtEFFECTIVEDT == null)
          m_FlatFileBatchInfo.m_dtEFFECTIVEDT = DateTime.Now;
        GenericObject geoTrans = pEventGEO.get("TRANSACTIONS", new GenericObject()) as GenericObject;
        int iCountofTransactions = geoTrans.getIntKeyLength();
        for (int i = 0; i < iCountofTransactions; i++)
        {
          GenericObject geoTran = (GenericObject)geoTrans.get(i);
          GenericObject geoTranClass = ((GenericObject)c_CASL.c_object("Transaction.of")).get(geoTran.get("TTID", "").ToString(), "") as GenericObject;
          // TODO: We suppose transaction is either regular or tax, will rework this later
          if (geoTranClass == null)
            geoTranClass = ((GenericObject)c_CASL.c_object("Transaction.Tax.of")).get(geoTran.get("TTID", "").ToString()) as GenericObject;
          if (!IsFlatFileBatchUpdate(geoTranClass, strSystemInterface))
            continue;
          GenericObject geoCustomFields = (GenericObject)((GenericObject)geoTranClass.get("custom_fields", new GenericObject())).get("_objects", new GenericObject());
          GenericObject geoCustData = GetCustFieldData(geoTran, geoCustomFields);
          // process the FlatFile tran
          if (!m_FlatFileBatchInfo.AddFlatFileRecord(geoTran, geoCustData, pFileGEO, strSystemInterface, ref strError))
          {
            //Bug#13616 QG Move error handling in catch.
            throw new Exception(strError);
          }
        }
      }
      catch (Exception e)
      {
        if (string.IsNullOrEmpty(strError))
        {
          strError = String.Format("Project_ProcessCoreEvent: {0} {1} {2}", strSystemInterface, strCurrentFile, e.Message).Replace("'", "");
        }
        pBatchUpdate_ProductLevel.LogBatchUpdateError(e, string.Format("FlatFileBatchUpdate Error: SI = {0} File = {1}", strSystemInterface, strCurrentFile), true); // Bug 15464 DJD - Batch Update Overhaul
        pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError(strCurrentFile, strSystemInterface, strError);

        // Bug 17849
        Logger.cs_log_trace("Error in Project_ProcessCoreEvent", e);
        return false;
      }
      return true;
    }
    // Clean Up m_FlatFileBatchInfo
    public override bool Project_Cleanup(bool bEarlyTerminationRequestedByProject, object pProductLevelMainClass)
    {
      string strError = "";
      try
      {
        if (m_FlatFileBatchInfo == null) return true;//BUG13617 

        m_FlatFileBatchInfo.m_geoFlatFileBatch.removeAll();
        m_FlatFileBatchInfo.m_dtEFFECTIVEDT = null;
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in Project_CleanUp", e);

        strError = "FlatFile CleanUp" + e.Message;
        Logger.LogError(strError, "Flat File Batch Update");
        return false;
      }
      return true;
    }

    public bool IsFlatFileBatchUpdate(GenericObject geoTranClass, string strSystemInterface)
    {
      //Bug#13616 QG null value check
      if (geoTranClass != null)
      {
        GenericObject geoConfigSysInt = geoTranClass.get("update_system_interface_list", "") as GenericObject;
        if (geoConfigSysInt != null)
        {
          int iCountofSysInt = geoConfigSysInt.getIntKeyLength();
          for (int i = 0; i < iCountofSysInt; i++)
          {
            string strConfigSysInt = geoConfigSysInt.get(i).ToString();
            if (strConfigSysInt == strSystemInterface)
              return true;
          }
        }
      }
      return false;
    }

    public GenericObject GetCustFieldData(GenericObject geoTran, GenericObject geoCustomFields)
    {
      // Custom Fields of Transaction includes some standard fields we do not want.
      // geoCustomFields works as a filter
      GenericObject geoCustData = new GenericObject();
      if (geoCustomFields != null)
      {
        // Setup filter
        GenericObject geoFilter = new GenericObject();
        int iCountofCustomFields = geoCustomFields.getIntKeyLength();
        for (int i = 0; i < iCountofCustomFields; i++)
        {
          GenericObject pCustomField = geoCustomFields.get(i) as GenericObject;
          string strTagName = pCustomField.get("tagname", "") as string;
          if ((strTagName != "") && (!(geoFilter.has(strTagName))))
            geoFilter.Add(strTagName, "");
        }

        // Get the needed custom fields using Filter.
        GenericObject geoCustFieldData = null;
        geoCustFieldData = geoTran.get("GR_CUST_FIELD_DATA", "") as GenericObject;
        if (geoCustFieldData != null)
        {
          int iCountofCFD = geoCustFieldData.getLength();
          for (int i = 0; i < iCountofCFD; i++)
          {
            GenericObject pCustomField = geoCustFieldData.get(i) as GenericObject;
            string strCUSTTAG = pCustomField.get("CUSTTAG", "") as string;
            string strCUSTVALUE = pCustomField.get("CUSTVALUE", "") as string;
            if ((strCUSTTAG != "") && (geoFilter.has(strCUSTTAG)))
              geoCustData.set(strCUSTTAG, strCUSTVALUE);
          }
        }
      }
      return geoCustData;
    }
    public override bool Project_ProcessDeposits(GenericObject pFileDeposits, GenericObject pConfigSystemInterface, object pProductLevelMainClass, string strSysInterface, GenericObject pCurrentFile, ref eReturnAction ReturnAction) // Bug 15464 DJD - Batch Update Overhaul
    {
      string strError = "";
      string strFileName = pFileDeposits.get("FULL_FILE_NAME").ToString();
      CASL_engine.BatchUpdate_ProductLevel pBatchUpdate_ProductLevel = (BatchUpdate_ProductLevel)pProductLevelMainClass;

      if (m_ConfigSystemInterface == null)
        m_ConfigSystemInterface = pConfigSystemInterface;

      bool bReturn = true;
      if (m_FlatFileBatchInfo == null) return true;//BUG13617  
      try
      {
        GenericObject pFileContentList = BuildFileContentForTheCoreFile();
        ArrayList keys = pFileContentList.getStringKeys();
        foreach (object key in keys)
        {
          string strTempPath = pConfigSystemInterface.get("local_file_path_for_temp", "").ToString();
          string strTempFilename = pConfigSystemInterface.get("local_file_name_for_temp", "").ToString();
          int iDotPos = strTempFilename.IndexOf('.');
          string strTempFile = strTempPath + '\\' + strTempFilename.Substring(0, iDotPos) + '_' + key.ToString() + strTempFilename.Substring(iDotPos, strTempFilename.Length - iDotPos);
          if (!CreateAndWriteFile(strTempFile, pFileContentList.get(key, "").ToString(), ref strError)) // Bug#13616 
            throw new Exception(strError);
          if (!(m_FileContentList.Contains(key)))
            m_FileContentList.Add(key, pFileContentList.get(key, "").ToString());
        }
        //Bug#13666 QG Clean up core file info for reuse.
        m_FlatFileBatchInfo.m_geoFlatFileBatch.vectors.Clear();
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in Project_ProcessDeposits3", e);

        strError = string.Format("{0}:Project_ProcessDeposits3", e.Message);
        Logger.LogInfo("START FlatFileBatchUpdate Project_ProcessDeposits3", "Flat File Batch Update");
        pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError(strFileName, strSysInterface, strError);  // Bug#13616 QG Add SI error
        return false;
      }

      return bReturn;
    }

    public override bool Project_WriteTrailer(object pProductLevelMainClass, string strSysInterface, ref bool bEarlyTerminationRequestedByProject)
    {
      if (bEarlyTerminationRequestedByProject) return false; //Bug#13535 

      Logger.LogInfo("START FlatFileBatchUpdate Project_WriteTrailer", "Flat File Batch Update");

      CASL_engine.BatchUpdate_ProductLevel pBatchUpdate_ProductLevel = (BatchUpdate_ProductLevel)pProductLevelMainClass;

      //THIS Method will only be used to ftp the local file - all date was written in the Project_ProcessDeposits
      string strError = "";
      //BUG13617 
      if (m_FlatFileBatchInfo == null)
      {
        Logger.LogInfo("No valid transaction or tenders in the core file.", "Flat File Batch Update");
        pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError("SYS_INTERFACE_SCOPE_ONLY", strSysInterface, "Project_WriteTrailer: No valid transaction or tenders in the core file.");
        return true;
      }
      string strBatchTempFilePath = m_ConfigSystemInterface.get("local_file_path_for_temp", "") as string;
      string strBatchTempFileName = XMLFileBatchUpdate.FormatFileNameWithDateTime(m_ConfigSystemInterface.get("local_file_name_for_temp", "") as string); // Bug 15964: XML Pmt File SFTP'd
      string strBatchLocalFilePath = m_ConfigSystemInterface.get("local_file_path", "") as string;
      string strBatchLocalFileName = XMLFileBatchUpdate.FormatFileNameWithDateTime(m_ConfigSystemInterface.get("local_file_name", "") as string); // Bug 15964: XML Pmt File SFTP'd
      string strBatchIfFileExists = m_ConfigSystemInterface.get("if_local_XML_file_exists", "Append") as string; // Bug 14756
      string strBatchUpdateMethod = ((string)m_ConfigSystemInterface.get("method", "")).ToLower() as string;
      string strBatchFTPAddress = m_ConfigSystemInterface.get("ftp_address", "") as string;
      string strBatchFTPPort = m_ConfigSystemInterface.get("ftp_port", "") as string;
      string strBatchFTPFileName = XMLFileBatchUpdate.FormatFileNameWithDateTime(m_ConfigSystemInterface.get("ftp_filename", "") as string);
      string strBatchFTPPath = m_ConfigSystemInterface.get("ftp_path", "").ToString();
      if (!string.IsNullOrEmpty(strBatchFTPPath.Trim()))
      {
        strBatchFTPFileName = Path.Combine(strBatchFTPPath, strBatchFTPFileName);
      }
      string strBatchFTPUserName = m_ConfigSystemInterface.get("ftp_username", "") as string;
      string strBatchFTPPassword = m_ConfigSystemInterface.get("ftp_password", "") as string;

      // Bug#13646 Decrypt FTP Password
      if (!string.IsNullOrEmpty(strBatchFTPPassword))
      {
        string secret_key = Crypto.get_secret_key();
        byte[] content_bytes = Convert.FromBase64String(strBatchFTPPassword);
        byte[] decrypted_bytes = Crypto.symmetric_decrypt("data", content_bytes, "secret_key", secret_key);
        strBatchFTPPassword = System.Text.Encoding.Default.GetString(decrypted_bytes);
      }

      // Bug#13646 QG Revise file transfer code.
      FileTransfer file_transfer = null;

      try
      {
        if (strBatchUpdateMethod.ToLower() == "folder")
        {
          ArrayList keys = m_FileContentList.getStringKeys();
          foreach (object key in keys)
          {
            //Bug#13616 QG Combine path and filename.
            int iDotPos = strBatchTempFileName.IndexOf('.');
            string strBatchTempFile = Path.Combine(strBatchTempFilePath, strBatchTempFileName.Substring(0, iDotPos) + '_' + key.ToString() + strBatchTempFileName.Substring(iDotPos, strBatchTempFileName.Length - iDotPos));
            iDotPos = strBatchLocalFileName.IndexOf('.');
            string strBatchLocalFile = Path.Combine(strBatchLocalFilePath, strBatchLocalFileName.Substring(0, iDotPos) + '_' + key.ToString() + strBatchLocalFileName.Substring(iDotPos, strBatchLocalFileName.Length - iDotPos));

            if (File.Exists(strBatchLocalFile))
            {
              switch (strBatchIfFileExists)
              {
                case "Stop": // Return Error message and stop the BU processing.
                  {
                    strError = string.Format("Error: The local flat batch file already exists: {0}", strBatchLocalFile);
                    Logger.LogError(strError, "Flat File Batch Update Project_WriteTrailer");
                    bEarlyTerminationRequestedByProject = true;
                    pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError(BU_Const.sSYS_INT_SCOPE, strSysInterface, "Project_WriteTrailer" + strError);
                    return false;
                  }

                case "Overwrite": // Delete the local file (if there's data to write from Temp file) so that a new one will be written.
                  if (File.Exists(strBatchTempFile))
                  {
                    File.Delete(strBatchLocalFile);
                  }
                  break;

                default: // Default to Append.
                  break;
              }
            }

            if (!(AppendFileToFile(strBatchTempFile, strBatchLocalFile, ref strError)))
            {
              throw new Exception(strError);
            }
          }
        }
        else if (strBatchUpdateMethod == "ftp" || strBatchUpdateMethod == "sftp")
        {
          if (strBatchUpdateMethod == "ftp")
          {
            file_transfer = new FTP(strBatchFTPAddress, strBatchFTPUserName, strBatchFTPPassword, strBatchFTPPort);
          }
          else if (strBatchUpdateMethod == "sftp")
          {
            file_transfer = new SFTP(strBatchFTPAddress, strBatchFTPUserName, strBatchFTPPassword, strBatchFTPPort);
          }
          file_transfer.Connect();

          ArrayList keys = m_FileContentList.getStringKeys();
          foreach (object key in keys)
          {
            int iDotPos = strBatchTempFileName.IndexOf('.');
            string strBatchTempFile = Path.Combine(strBatchTempFilePath, strBatchTempFileName.Substring(0, iDotPos) + '_' + key.ToString() + strBatchTempFileName.Substring(iDotPos, strBatchTempFileName.Length - iDotPos));
            iDotPos = strBatchFTPFileName.IndexOf('.');
            string strBatchFTPFile = strBatchFTPFileName.Substring(0, iDotPos) + '_' + key.ToString() + strBatchFTPFileName.Substring(iDotPos, strBatchFTPFileName.Length - iDotPos);

            if (file_transfer.FileExists(strBatchFTPFile) && File.Exists(strBatchTempFile))
            {
              switch (strBatchIfFileExists)
              {
                case "Stop": // Return Error message and stop the BU processing.
                  {
                    strError = string.Format("Error: The remote batch flat file already exists: {0}", strBatchFTPFile);
                    Logger.LogError(strError, "Flat File Batch Update Project_WriteTrailer");
                    bEarlyTerminationRequestedByProject = true;
                    pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError(BU_Const.sSYS_INT_SCOPE, strSysInterface, "Project_WriteTrailer" + strError);
                    return false;
                  }

                case "Overwrite": // Delete & send the remote file so that a new one will be written.
                  {
                    file_transfer.DeleteFile(strBatchFTPFile);
                    file_transfer.SendFile(strBatchTempFile, strBatchFTPFile);
                  }
                  break;

                default: // case "Append":
                  {
                    file_transfer.AppendFile(strBatchTempFile, strBatchFTPFile);
                  }
                  break;
              }
            }
            else // Just send the file
            {
              if (File.Exists(strBatchTempFile))
              {
                file_transfer.SendFile(strBatchTempFile, strBatchFTPFile);
              }
            }
          }
        }
        // Can not get Method from System Interface
        else
        {
          throw new Exception("Error getting Batch Update Method from PB Standard Batch Update:Project_Write_Trailer");
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in Project_WriteTrailer", e);

        strError = string.Format("Error in Project_WriteTrailer: {0}", e.Message).Replace("'", " ");
        Logger.LogInfo("START FlatFileBatchUpdate Project_WriteTrailer", "Flat File Batch Update");
        bEarlyTerminationRequestedByProject = true;
        pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError("SYS_INTERFACE_SCOPE_ONLY", strSysInterface, "Project_WriteTrailer:" + strError);
      }
      finally
      {
        // Destructor will call Disconnect, but with .NET it's async scheduled. So calling
        // disconnect here will free the connection faster
        if (file_transfer != null) file_transfer.Disconnect();

        ArrayList keys = m_FileContentList.getStringKeys();
        foreach (object key in keys)
        {
          int iDotPos = strBatchTempFileName.IndexOf('.');
          string strBatchTempFile = Path.Combine(strBatchTempFilePath, strBatchTempFileName.Substring(0, iDotPos) + '_' + key.ToString() + strBatchTempFileName.Substring(iDotPos, strBatchTempFileName.Length - iDotPos));
          if (File.Exists(strBatchTempFile))
          {
            File.Delete(strBatchTempFile);
          }
        }
      }
      // End of Bug#13646 QG
      return true;
    }

    // Called by Project_WriteTrailer(...)
    public bool AppendFileToFile(string FilePathFrom, string FilePathTo, ref string strError)
    {
      StreamWriter sw = null;  // Bug 16433 [21921] UMN
      try
      {
        string FileContent = File.ReadAllText(FilePathFrom);
        if (File.Exists(FilePathTo))
        {
          sw = File.AppendText(FilePathTo);
        }
        else
        {
          sw = new StreamWriter(FilePathTo, true);
        }
        sw.Write(FileContent);
        sw.Flush();
        // Bug 16433 [21921] UMN handle close in finally clause
        //sw.Close();
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in AppendFileToFile", e);

        strError = string.Format("Cannot Write to File({0}):{1}", FilePathTo, e.Message);
        return false;
      }
      finally
      {
        // Bug 16433 [21921] UMN handle close in finally clause
        if (sw != null) sw.Close();
      }
      return true;
    }

    // Called By BuildFileContentForTheCoreFile()
    public string BuildFlatFileLine(CFlatFileRecord pFlatFileRecord)
    {
      System.Text.StringBuilder sb = new System.Text.StringBuilder();
      sb.AppendFormat("{0}|", pFlatFileRecord.m_strEFFDATE);
      sb.AppendFormat("{0}|", pFlatFileRecord.m_strReceiptRefNbr);
      sb.AppendFormat("{0}|", pFlatFileRecord.m_strTransTypeID);
      sb.AppendFormat("{0}|", pFlatFileRecord.m_strGroup);
      sb.AppendFormat("{0:###0.00}", pFlatFileRecord.m_dTRAN_AMT);
      ArrayList keys = pFlatFileRecord.m_geoCustomFields.getStringKeys();
      // Append Custom Fields
      foreach (string key in keys)
      {
        sb.AppendFormat("|{0}", pFlatFileRecord.m_geoCustomFields.get(key, ""));
      }
      return sb.ToString();

    }

    public GenericObject BuildFileContentForTheCoreFile()
    {
      GenericObject FileContentList = new GenericObject();
      System.Text.StringBuilder sbFileContent = new StringBuilder();

      //Summary Records

      ArrayList keys = m_FlatFileBatchInfo.m_geoFlatFileBatch.getStringKeys();
      foreach (object key in keys)
      {
        CFlatFileRecord FlatFileRec = m_FlatFileBatchInfo.m_geoFlatFileBatch.get(key) as CFlatFileRecord;
        if (FileContentList.Contains(FlatFileRec.m_strTransTypeID))
        {
          sbFileContent = new StringBuilder(FileContentList.get(FlatFileRec.m_strTransTypeID).ToString());
          sbFileContent.AppendLine(BuildFlatFileLine(FlatFileRec));
          FileContentList.set(FlatFileRec.m_strTransTypeID, sbFileContent.ToString());
        }
        else
        {
          sbFileContent = new StringBuilder();
          sbFileContent.AppendLine(BuildFlatFileLine(FlatFileRec));
          FileContentList.Add(FlatFileRec.m_strTransTypeID, sbFileContent.ToString());
        }
      }
      //Detail Records
      int iDetailCount = (int)m_FlatFileBatchInfo.m_geoFlatFileBatch.getIntKeyLength();
      for (int i = 0; i < iDetailCount; i++)
      {
        CFlatFileRecord FlatFileRec = m_FlatFileBatchInfo.m_geoFlatFileBatch.get(i) as CFlatFileRecord;
        if (FileContentList.Contains(FlatFileRec.m_strTransTypeID))
        {
          sbFileContent = new StringBuilder(FileContentList.get(FlatFileRec.m_strTransTypeID).ToString());
          sbFileContent.AppendLine(BuildFlatFileLine(FlatFileRec));
          FileContentList.set(FlatFileRec.m_strTransTypeID, sbFileContent.ToString());
        }
        else
        {
          sbFileContent = new StringBuilder();
          sbFileContent.AppendLine(BuildFlatFileLine(FlatFileRec));
          FileContentList.Add(FlatFileRec.m_strTransTypeID, sbFileContent.ToString());
        }
      }
      return FileContentList;
    }
    public bool CreateAndWriteFile(string FilePath, string FileContent, ref string strError)
    {
      try
      {
        if (m_FlatFileBatchInfo.m_pOutputConnection == null)
          m_FlatFileBatchInfo.m_pOutputConnection = new OutputConnection();
        if (m_FlatFileBatchInfo.m_pOutputConnection.m_pOutputFile == null)
        {
          m_FlatFileBatchInfo.m_pOutputConnection.m_pOutputFile = new StreamWriter(FilePath);
        }
        else
        {
          if (File.Exists(FilePath))
          {
            m_FlatFileBatchInfo.m_pOutputConnection.m_pOutputFile = File.AppendText(FilePath);
          }
          else
          {
            m_FlatFileBatchInfo.m_pOutputConnection.m_pOutputFile = new StreamWriter(FilePath);
          }
        }
        m_FlatFileBatchInfo.m_pOutputConnection.m_pOutputFile.Write(FileContent);
        m_FlatFileBatchInfo.m_pOutputConnection.m_pOutputFile.Flush();
        // Bug 16433 [21921] UMN handle close in finally clause
        // m_FlatFileBatchInfo.m_pOutputConnection.m_pOutputFile.Close();
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in CreateAndWriteFile", e);

        strError = string.Format("Cannot Write to File({0}):{1}", FilePath, e.Message);
        Logger.LogError(strError, "Flat File Batch Update CreateAndWriteFile");
        return false;
      }
      finally
      {
        // Bug 16433 [21921] UMN handle close in finally clause
        if ((m_FlatFileBatchInfo.m_pOutputConnection != null) && 
            (m_FlatFileBatchInfo.m_pOutputConnection.m_pOutputFile != null))
        {
          m_FlatFileBatchInfo.m_pOutputConnection.m_pOutputFile.Close();
        }
      }
      return true;
    }

    public override void Project_Rollback(object pProductLevelMainClass, bool bEarlyTerminationRequestedByProject)
    {
      // Add code here if you want to do something on failure (delete the file?)
    }
  }
}