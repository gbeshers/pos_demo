﻿using System;
using System.IO;
using CASL_engine;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Linq;
using ftp;
using Microsoft.VisualBasic.FileIO; // BUG

/************************

  This code performs an import of a single file into the PB standard database
  tables. The file may be fixed-length, or delimited. In either case, details
  about the file structure are taken from Config.

  This code was iterated through by AC, FGT, QI, and UMN. See any of us for
  questions, or bugfixes/enhancements. 
  
  THIS CODE IS NOT THREAD-SAFE!!!! because there are NO requirements to support
  multiple imports. When this code eventually is enabled in the ADMIN portal,
  it will require the addition of locking to guarantee thread safety against
  user error.

  This code has markedly sped up the import process. In most customer scenarios,
  import speed is determined by the speed of the FTP get, and not by this database
  code.

  This code provides detailed timing stats in the cs log file.

************************/

namespace ProjectBaseline
{
    // Bug 27153 DH - Made static
    public static class FileImport
    {
        /// <summary>
        /// Count moved here so that it can be monitored / timed 
        /// in ImportFile
        /// </summary>
        private static int recordCount = 0;

        // ughh. had to use this because replace/append is at the individual
        // import config level, so can't do ResetHiLo etc in ImportFileProcess
        private static bool fromFileProcess = false;

        // The number of INSERT's to lump together into database transaction
        const int TRANSACTION_BATCH_SIZE = 1024;
        const int BULK_COPY_TIMEOUT = 240;      // 6 minutes

        const int HILO_CAPACITY = TRANSACTION_BATCH_SIZE;

        ///////////////////////////////////
        // Begin CASL Hooks

        /// <summary>
        /// Hook from CASL. Imports all the files that are configured.
        /// Was in CASL, moved to C# for Bug#13040
        /// </summary>
        public static GenericObject ImportFileProcess(params Object[] arg_pairs)
        {
            GenericObject args = misc.convert_args(arg_pairs);
            GenericObject resultStatus = args.get("result_status") as GenericObject;
            GenericObject dbConn = args.get("db_connection_info") as GenericObject;

            // global config items
            string swapMode = ((string)((GenericObject)c_CASL.c_object("Business.PB_background_file_import.data")).get("swap_mode", "All")).ToLower();
            bool stopOnErr = (bool)((GenericObject)c_CASL.c_object("Business.PB_background_file_import.data")).get("stop_on_error", false);

            bool shouldSwap = true;       // will be true, unless an individual import fails and stopOnErr is true
            bool result = true;

            fromFileProcess = true;

            // Create the connection here so that finally can close
            SqlConnection sqlConnection = new SqlConnection();

            System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
            watch.Start();

            Logger.cs_log(Environment.NewLine + "===============================");
            Logger.cs_log("STARTED Standard import process");

            try
            {
                // Open a connection for the import
                sqlConnection.ConnectionString = (string)dbConn.get("db_connection_string");
                sqlConnection.Open();

                GenericObject stdInterfaces = (GenericObject)((GenericObject)c_CASL.c_object("Business.Std_interface_import.of"));
                foreach (string intfName in stdInterfaces.getStringKeys())
                {
                    GenericObject stdInterface = (GenericObject)stdInterfaces.get(intfName);
                    string descrip = stdInterface.get("Import_Config_Description") as string;
                    string groupName = stdInterface.get("Import_Config_Group") as string;
                    string fixedorVariable = ((string)stdInterface.get("Import_Config_File_Type")).ToLower();
                    string delimiter = "";
                    bool isVariableLength = (fixedorVariable == "variable length records") ? true : false;

                    if (isVariableLength)
                        delimiter = stdInterface.get("Import_Config_Delimiter", ",") as String;

                    Logger.LogInfo("Started " + descrip + " Import...", "Inquiry Import");

                    Logger.cs_log(Environment.NewLine + "-------------------------------");
                    Logger.cs_log("Started " + descrip + " Import...");

                    result = ImportFileAux(sqlConnection, groupName, stdInterface, isVariableLength,
                    delimiter, swapMode);

                    if (!result)
                    {
                        if (swapMode == "all")
                            shouldSwap = false;
                        Logger.cs_log("BFI: " + descrip + " Import FAILED");

                        Logger.LogError("... " + descrip + " Import FAILED.", "Inquiry Import");

                        if (stopOnErr)
                        {
                            Logger.LogError("STOPPING import process due to stop_on_error config setting.", "Inquiry Import");
                            Logger.cs_log("BFI: " + descrip + " STOPPING import process due to stop_on_error config setting");
                            resultStatus.set(descrip, "FAILED");
                            break;
                        }
                    }
                    else
                    {
                        Logger.cs_log("BFI: " + descrip + " Import SUCCEEDED");
                        Logger.LogInfo("... " + descrip + " Import SUCCEEDED.", "Inquiry Import");
                    }
                    resultStatus.set(descrip, (result) ? "SUCCEEDED" : "FAILED");

                } // END for_each

                // only swap the table names if all imports succeeded when 'All' is set
                if (swapMode == "all")
                {
                    if (shouldSwap)
                    {
                        SwapTableNames(sqlConnection);
                        Logger.LogInfo("SWAPPED table names during Standard Import.", "Inquiry Import");
                        resultStatus.set(" Swapped", true);
                    }
                    else
                    {
                        Logger.LogError("Error: table names NOT swapped during Standard Import.", "Inquiry Import");
                        resultStatus.set(" Swapped", false);
                        resultStatus.set(" Note", "Consult CS log files for error details.");
                    }
                }

                // log and store overall result
                string overallResult = (shouldSwap) ? "SUCCEEDED" : "FAILED";

                // Bug 14291: UMN don't fail the import if a group failed under Individual swap and no stop on error
                if (swapMode == "individual" && !stopOnErr)
                    overallResult = "SUCCEEDED";
                resultStatus.set(" Result", overallResult);

                Logger.LogInfo(overallResult + " Import of iPayment standard inquiry files.", "Inquiry Import");
            }
            catch (Exception e)
            {
                resultStatus.set(" Result", "FAILED - Exception. See cs log for details.");
                string msg = "BFI Error (Exception thrown): " + e.ToMessageAndCompleteStacktrace();// Bug 17849
                Logger.cs_log(msg);

                Logger.LogError("... Exception!", "Inquiry Import");
                return misc.MakeCASLError("BFI Error", "BFI-100", msg);
            }
            finally
            {
                watch.Stop();
                TimeSpan ts = watch.Elapsed;
                string elapsedTime = String.Format("Standard import process took {0:00} minutes, {1:00} seconds to complete.",
                ts.Minutes, ts.Seconds);

                Logger.cs_log(Environment.NewLine + "-------------------------------");
                Logger.cs_log(elapsedTime);
                Logger.cs_log("FINISHED Standard import process.");
                Logger.cs_log(Environment.NewLine + "===============================");

                if (sqlConnection != null)
                    sqlConnection.Close();
            }

            return resultStatus;
        }

        /// <summary>
        /// Hook from CASL. Imports a file based on System Interface Import Configuration.
        /// Returns true (Succeeded) or false (Failed).
        /// </summary>
        public static GenericObject ImportFile(params Object[] arg_pairs)
        {
            GenericObject args = misc.convert_args(arg_pairs);
            GenericObject importCfg = args.get("import_config") as GenericObject;
            GenericObject resultStatus = args.get("result_status") as GenericObject;
            GenericObject dbConn = args.get("db_connection_info") as GenericObject;

            fromFileProcess = false;

            // global config items
            // A single file import is treated as if it was 'individual' import of only one file
            string swapMode = "individual";
            bool stopOnErr = (bool)((GenericObject)c_CASL.c_object("Business.PB_background_file_import.data")).get("stop_on_error", false);

            string groupName = importCfg.get("Import_Config_Group") as string;
            string fixedorVariable = ((string)importCfg.get("Import_Config_File_Type")).ToLower();
            string delimiter = "";
            bool isVariableLength = (fixedorVariable == "variable length records") ? true : false;

            if (isVariableLength)
                delimiter = importCfg.get("Import_Config_Delimiter", ",") as String;

            // Create the connection here so that finally can close
            SqlConnection sqlConnection = new SqlConnection();

            try
            {
                // Open a connection for the import
                sqlConnection.ConnectionString = (string)dbConn.get("db_connection_string");
                sqlConnection.Open();

                Logger.cs_log(Environment.NewLine + "===============================");
                Logger.cs_log(string.Format("STARTING {0} import.", groupName));

                bool result = ImportFileAux(sqlConnection, groupName, importCfg, isVariableLength,
                    delimiter, swapMode);

                if (result)
                {
                    resultStatus.set(" Swapped", true);
                    Logger.cs_log("BFI: " + groupName + " Import SUCCEEDED");
                    Logger.LogInfo("Import SUCCEEDED", "Inquiry Import");
                }
                else
                {
                    resultStatus.set(" Swapped", false);
                    resultStatus.set(" Note", "Consult log files for error details.");
                    Logger.cs_log("BFI: " + groupName + " Import FAILED");
                    Logger.LogError("Import FAILED", "Inquiry Import");
                }
                resultStatus.set(groupName, (result) ? "SUCCEEDED" : "FAILED");
            }
            catch (Exception e)
            {
                string msg = "BFI Error (Exception thrown): " + e.ToMessageAndCompleteStacktrace(); // Bug 17849
                Logger.cs_log(msg);
                return misc.MakeCASLError("BFI Error", "BFI-100", msg);
            }
            finally
            {
                Logger.cs_log(string.Format("FINISHED {0} import.", groupName));
                Logger.cs_log("===============================" + Environment.NewLine);

                if (sqlConnection != null)
                    sqlConnection.Close();
            }

            return resultStatus;
        }

        // End CASL Hooks
        ///////////////////////////////////

        /// <summary>
        /// A generic routine that uses the C# SqlBulkCopy object
        /// so the BULK INSERTs can be done in managed code.  
        /// The following options were used/defaulted:
        /// <list>
        /// <item>
        /// <term>BatchSize property</term>
        /// <description>The was left unset so that the batch size defaults 
        /// to all the records in the datatable when WriteToServer is called.</description>
        /// </item>
        /// 
        /// <item>
        /// <term>UseInternalTransaction</term>
        /// <description>None used. We don't care about rollbacks, since we use truncate/swap tables</description>
        /// </item>
        /// 
        /// <item>
        /// <term>BulkCopyTimeout</term>
        /// <description>The 30 second default is too small; increased this.</description>
        /// </item>
        /// 
        /// </list>
        /// </summary>
        /// <param name="dbConn"></param>
        /// <param name="groupName"></param>
        /// <param name="importCfg"></param>
        /// <param name="isVariablelength"></param>
        /// <param name="delimiter"></param>
        /// <param name="swapMode"></param>
        /// <param name="fromImpProcess"></param>
        /// <returns>bool</returns>
        private static bool ImportFileAux(
        SqlConnection sqlConn,    // the open connection
        string groupName,         // group name (from config)
        GenericObject importCfg,  // import variables (from config)
        bool isVariablelength,    // does file have variable- or fixed-length records?
        string delimiter,         // if fixed-length, has delimiter, else ""
        string swapMode)          // either 'All' or 'Individual'
        {

            // initialization
            GenericObject dataElements = importCfg.get("data_elements", new GenericObject()) as GenericObject;
            string importMode = ((string)importCfg.get("Import_Config_Mode", "Replace")).ToLower();
            bool failIfZeroRows = (bool)importCfg.get("Import_Config_Fail_On_Zero_Rows", true);  // Bug 14291
            int numFieldsInFile = (int)importCfg.get("Import_Config_Num_Fields", 0);      // Bug 14810

            int maxErrsToLog = (int)((GenericObject)c_CASL.c_object("Business.PB_background_file_import.data")).get("max_errors_logged", 10);
            int numErrsB4Fail = (int)((GenericObject)c_CASL.c_object("Business.PB_background_file_import.data")).get("num_errors_fail", 20);

            if (maxErrsToLog == -1) maxErrsToLog = int.MaxValue;
            if (numErrsB4Fail == -1) numErrsB4Fail = int.MaxValue;
            int errCount = 0;
            recordCount = 0;

            string localFileName = "";
            string dataTblName = "";
            string weightsTblName = "";
            string keyTblName = "";
            bool deleteFileWhenDone = false;
            bool wroteGroupData = false;

            // HiLo init
            Int64 high = 0, low = 0;

            DataTable dataTable = new DataTable();
            DataTable keyTable = new DataTable();

            // Bug 16433 [21921] UMN dispose these
            SqlBulkCopy dataBulkCopy = null;
            SqlBulkCopy keyBulkCopy = null;

            System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
            watch.Start();

            try
            {
                // setup the data tables
                dataTable.Columns.Add("DATA_ID", typeof(Int64));
                dataTable.Columns.Add("GROUP_NAME", typeof(String));
                dataTable.Columns.Add("INQ_DATA", typeof(String));
                keyTable.Columns.Add("Tag", typeof(String));
                keyTable.Columns.Add("Value", typeof(String));
                keyTable.Columns.Add("DATA_ID", typeof(Int64));

                // get the ftp file. Time it separately. Will set deleteFileWhenDone based 
                // on whether is local or ftp file
                localFileName = GetLocalOrFtpFile(importCfg, ref deleteFileWhenDone);

                // Bug 14291 UMN: check for file existence first!
                FileInfo fi = new FileInfo(localFileName);
                if (!fi.Exists)
                {
                    string msg = String.Format("BFI-1110x: Import file {0} does not exist. Unable to import to {1}.",
                        localFileName, groupName);
                    Logger.cs_log(msg);
                    throw new Exception(msg);
                }

                // Bug 14672 UMN - Reopened to fix processing when file is empty
                if (fi.Length == 0)
                {
                    // if we're not failing on zero-rows, then just log and continue processing the next file
                    if (!failIfZeroRows)
                    {
                        string msg = String.Format("BFI-1110y: Import file {0} has zero-rows. Skipping...", localFileName);
                        Logger.cs_log(msg);
                        return true;
                    }
                    else
                    {
                        string msg = String.Format("BFI-1110z: Import file {0} has zero-rows. Unable to import to {1}.",
                        localFileName, groupName);
                        Logger.cs_log(msg);
                        throw new Exception(msg);
                    }
                }

                // sqlConn is opened by caller

                // Get the tablenames for import
                GetTableNames(sqlConn, ref dataTblName, ref keyTblName, ref weightsTblName);

                // Start by truncating all tables, but only if All is set
                // Can't do this in ImportFileProcess because replace/append
                // mode is associated with a specific import configuration
                if (swapMode == "all" && fromFileProcess)
                {
                    TruncateTables(sqlConn, dataTblName, keyTblName, weightsTblName);

                    // we can safely reset the hilo
                    if (importMode == "replace")
                    {
                        ResetHiLo(sqlConn);
                    }
                    else // append mode
                    {
                        // To handle appends, we have to copy data from the inquiry table.
                        CopyTableData(sqlConn, dataTblName, keyTblName, weightsTblName);
                    }

                    // reset fromFileProcess, so we don't truncate next time we're called
                    fromFileProcess = false;
                }

                // Turn off ANSI warnings to prevent clipping errors from stopping insert
                SetAnsiWarnings(sqlConn, false);

                // get the high & low for insert key generation
                GetNextHiLo(sqlConn, ref high, ref low);

                // handle individual imports
                if (swapMode == "individual")
                {
                    // First dispose of the old records and time it separately
                    TruncateTables(sqlConn, dataTblName, keyTblName, weightsTblName);

                    // Bug 14116 UMN - bit of a hacky fix, in that there will now be gaps in the DATA_IDs,
                    // but it's a large address space for the hi/lows, so it's ok.
                    GetNextHiLo(sqlConn, ref high, ref low);

                    // To handle individual file imports, we have to copy data from the
                    // inquiry table.
                    CopyTableData(sqlConn, dataTblName, keyTblName, weightsTblName);

                    // only delete tables if we're replacing them
                    if (importMode == "replace")
                    {
                        DeleteTableRows(sqlConn, groupName, dataTblName, keyTblName, weightsTblName);
                    }
                }

                // write the Weights file, time it separately
                WriteWeightsFile(sqlConn, weightsTblName, groupName, dataElements);

                // We get our speed from these bulk copies
                dataBulkCopy = new SqlBulkCopy(sqlConn, SqlBulkCopyOptions.TableLock, null);
                keyBulkCopy = new SqlBulkCopy(sqlConn, SqlBulkCopyOptions.TableLock, null);

                // Increase from default of 30 seconds
                dataBulkCopy.BulkCopyTimeout = BULK_COPY_TIMEOUT;
                keyBulkCopy.BulkCopyTimeout = BULK_COPY_TIMEOUT;

                // set the target table name to bulk copy into
                dataBulkCopy.DestinationTableName = dataTblName;
                keyBulkCopy.DestinationTableName = keyTblName;

                int transactionThreshold = 1;

                // Create a local context for file open so it is closed automatically
                using (System.IO.StreamReader fileReader = new System.IO.StreamReader(localFileName))
                {
                    // UMN TODO - with the FileInfo code checking for the length of the file, the next if block
                    // is unneeded. Take out in a future release?
                    if (fileReader.Peek() == -1)
                    {
                        string msg = String.Format("BFI-1110: Import file {0} is empty. Unable to import to {1}.",
                              localFileName, groupName);
                        Logger.cs_log(msg);
                        throw new Exception(msg);
                    }

                    string line = null;
                    string lineErrMsg = null;

                    // Process each line in the import file
                    while ((line = fileReader.ReadLine()) != null)
                    {
                        recordCount++;
                        lineErrMsg = null;

                        // Bug 14672 UMN skip blank lines
                        // Bug 21010 Heidi I need trim white spaces
                        string trim_line = line.Trim();
                        if (string.IsNullOrEmpty(trim_line))
                            continue;

                        // Process the line, returns false if line is to be skipped
                        if (!ProcessFixedOrVariableLine(ref dataTable, ref keyTable, ref line, dataElements, numFieldsInFile,
                        groupName, isVariablelength, delimiter, ref lineErrMsg, high, ref low))
                        {

                            // we tolerate some (configurable) number of line errors
                            if (lineErrMsg != null)
                            {
                                errCount++;
                                if (errCount <= maxErrsToLog)
                                    Logger.cs_log(lineErrMsg);      // recoverable line errors go to cs_log
                                if (errCount > numErrsB4Fail)
                                {
                                    // save file for post-mortem
                                    deleteFileWhenDone = false;
                                    string msg = String.Format("BFI-1111: Too many line errors:{0}; check config. Aborting import.", errCount.ToString());
                                    Logger.cs_log(msg);
                                    throw new Exception(msg);
                                }
                            }
                            continue;    // no non-recoverable errors, so skip the failed line
                        }

                        // Commit and re-open if at threshold
                        transactionThreshold++;
                        if (transactionThreshold == TRANSACTION_BATCH_SIZE)
                        {
                            // Int64 id = (high - 1)*HILO_CAPACITY + (low);
                            // string msg = String.Format("High:{0}, low:{1}, id:{2}", high.ToString(), low.ToString(), id.ToString());
                            // Logger.cs_log(msg);

                            // bulk write the SI_DATA and SI_KEY
                            BulkWriteTableToServer(dataBulkCopy, dataTable);
                            BulkWriteTableToServer(keyBulkCopy, keyTable);
                            wroteGroupData = true;

                            // Reset everything
                            transactionThreshold = 1;
                            dataTable.Clear();
                            keyTable.Clear();
                            GetNextHiLo(sqlConn, ref high, ref low);
                        }
                    } // end while
                } // end using

                // Write out any remaining records
                if (transactionThreshold > 1)
                {
                    // bulk write the SI_DATA and SI_KEY
                    if (dataTable.Rows.Count > 0)
                        BulkWriteTableToServer(dataBulkCopy, dataTable);
                    if (keyTable.Rows.Count > 0)
                        BulkWriteTableToServer(keyBulkCopy, keyTable);
                    if (dataTable.Rows.Count > 0 || keyTable.Rows.Count > 0)
                        wroteGroupData = true;
                }

                // Bug 14672 UMN - Handle case where file just has one or more blank lines
                if (!wroteGroupData)
                {
                    // if we're not failing on zero-rows, then just log and continue processing the next file
                    if (!failIfZeroRows)
                    {
                        string msg = String.Format("BFI-1111x: Import file {0} has only blank lines. Skipping...", localFileName);
                        Logger.cs_log(msg);
                        return true;
                    }
                    else
                    {
                        string msg = String.Format("BFI-1111y: Import file {0} has only blank lines. Unable to import to {1}.",
                        localFileName, groupName);
                        Logger.cs_log(msg);
                        throw new Exception(msg);
                    }
                }

                // Swapping after individual group imports are handled here. Swapping after All
                // imports is handled at a higher level.
                if (swapMode == "individual")
                {
                    bool res = SwapTableNames(sqlConn);
                    if (!res) throw new Exception(); // already logged, so just throw up
                    Logger.cs_log("BFI: " + groupName + " Import SWAPPED");
                    Logger.LogInfo("... " + groupName + " Import SWAPPED.", "Inquiry Import");
                }
            }

            // Must eat all exceptions so that we can continue to process other files
            catch (Exception e)
            {
                string msg = String.Format("BFI ERROR in ImportFileAux: {0}", e.ToMessageAndCompleteStacktrace()); // Bug 17849
                Logger.cs_log(msg);
                return false;
            }
            finally
            {
                // Bug 16433 [21921] UMN dispose of datatables and other iDisposable objects
                dataTable.Dispose();
                keyTable.Dispose();
                if (dataBulkCopy != null) dataBulkCopy.Close();
                if (keyBulkCopy != null) dataBulkCopy.Close();

                // Delete the local file
                DeleteFile(localFileName, deleteFileWhenDone);

                watch.Stop();
                Logger.cs_log(
                string.Format("BFI: took {0} seconds to process {1} records into {2}: ",
                watch.ElapsedMilliseconds / 1000.00, recordCount.ToString(), groupName));
            }
            return true;
        }

        /// <summary>
        /// Does what the method name says
        /// </summary>
        /// <param name="dataBulkCopy"></param>
        /// <param name="dataTable"></param>
        internal static void BulkWriteTableToServer(SqlBulkCopy bulkCopy, DataTable dataTable)
        {
            // stupid SQLBulkCopy has piss poor error handling, but it is blazing fast
            try
            {
                bulkCopy.WriteToServer(dataTable);
            }
            catch (Exception e)
            {
                int startLine = (recordCount <= TRANSACTION_BATCH_SIZE) ? 0 : (recordCount - TRANSACTION_BATCH_SIZE);
                // recoverable batch error, log it, and continue
                string msg = String.Format("BFI Batch error for Data between lines:{0}-{1}: {2}",
                startLine.ToString(), recordCount.ToString(), e.ToMessageAndCompleteStacktrace()); // Bug 17849
                Logger.cs_log(msg);
                throw new Exception(msg);
            }
        }

        /// <summary>
        /// Function that processes a line from the file. Returns true,
        /// or false on error
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="keyTable"></param>
        /// <param name="line"></param>
        /// <param name="lineError"></param>
        /// <param name="lineNumber"></param>
        /// <returns>bool</returns>
        private static bool ProcessFixedOrVariableLine(
        ref DataTable dataTable,            // SI_DATA datatable
        ref DataTable keyTable,             // SI_KEY datatable
        ref string line,                    // current line from the file
        GenericObject dataElements,         // the configured data elements
        int numFieldsInFile,                // field count in file; if 0, don't verify
        string groupName,                   // the group name
        bool isVariablelength,              // true if variable-length file
        string delimiter,                   // delimiter for variable, empty for fixed
        ref string lineErrorMsg,            // returned line error msg
        Int64 high,                         // high number in HiLo identity key
        ref Int64 low)                        // high number in HiLo (it's incremented)
        {
            string xmlData = "<Data>";

            string[] split = null;
            int nbrOfElements = 0;
            int fieldNbr = 0, length = 0, startPos = 0;

            // I refactored Qi's code a little, so that there isn't cloned code.
            // the penalty is a few extra if tests, but should be more maintainable
            if (isVariablelength)
            {
                // BUG17271 support quoted data element in delimited file
                TextFieldParser parser = new TextFieldParser(new StringReader(line));
                parser.HasFieldsEnclosedInQuotes = true;
                parser.SetDelimiters(delimiter);
                while (!parser.EndOfData)
                {
                    split = parser.ReadFields();
                }
                parser.Close();
                // BUGsplit = line.Split(stringSeparators, StringSplitOptions.None);
                nbrOfElements = split.Length;
                // Bug 14810 UMN - skip line if nbr of items doesn't match config
                if (numFieldsInFile > 0 && nbrOfElements != numFieldsInFile)
                {
                    lineErrorMsg =
                    string.Format("Line: {0}. The number of elements in file ({1}) and configuration ({2}) do not match.",
                      recordCount.ToString(), nbrOfElements.ToString(), numFieldsInFile.ToString());
                    return false;
                }
            }

            // Loop over all the configured data elements
            foreach (GenericObject Element in dataElements.vectors)
            {
                if (isVariablelength)
                {
                    fieldNbr = ((int)Element.get("FieldNum", 1)) - 1;
                }
                else  // Must be fixed-length
                {
                    startPos = (int)(Element.get("Starting_position", 0));
                    length = (int)(Element.get("Length", 0));
                }
                string strTag = Element.get("Tag_name", "") as String;
                bool bIsQueryKey = (bool)Element.get("QueryKey_Indicator", true);
                if (isVariablelength)
                {
                    if ((fieldNbr >= 0) && (fieldNbr < nbrOfElements))
                    {
                        xmlData += '<' + strTag + '>' +
                        System.Security.SecurityElement.Escape(split[fieldNbr].Trim()) // Bug 14415 Escape special chars
                        + "</" + strTag + '>';
                    }
                    else // Bug 14810 DJD: Skip line if nbr of elements in line doesn't match config
                    {
                        lineErrorMsg =
                        string.Format("Line: {0}. The number of elements in this line {1}. Configured data element number {2} cannot be found.",
                          recordCount.ToString(), nbrOfElements.ToString(), fieldNbr.ToString());//Bug 14810 NK changing message
                        return false;
                    }
                } else // Must be fixed-length
                {
                    if ((startPos > 0) && (startPos + length - 1 <= line.Length))
                    {
                        xmlData += '<' + strTag + '>' +
                        System.Security.SecurityElement.Escape(line.Substring(startPos - 1, length).Trim()) // Bug 14415 Escape special chars
                        + "</" + strTag + '>';
                    }
                    else
                    {
                        lineErrorMsg =
                        string.Format("Line: {0}. Invalid Starting position ({1}) or length ({2}).",
                        recordCount.ToString(), startPos.ToString(), length.ToString());
                        return false;
                    }
                }
            }
            xmlData += "</Data>";

            // Update SI_DATA datatable
            low++;
            DataRow dataRow = dataTable.NewRow();
            dataRow[0] = (high - 1) * HILO_CAPACITY + low;  // our ID because we can't use SQL server generated identity
            dataRow[1] = groupName;
            dataRow[2] = xmlData;
            dataTable.Rows.Add(dataRow);

            // Insert items into SI_KEY
            foreach (GenericObject Element in dataElements.vectors)
            {
                if (isVariablelength)
                {
                    fieldNbr = ((int)Element.get("FieldNum", 1)) - 1;
                }
                else
                {
                    startPos = (int)(Element.get("Starting_position", 0));
                    length = (int)(Element.get("Length", 0));
                }
                string strTag = Element.get("Tag_name", "") as String;
                bool bIsQueryKey = (bool)Element.get("QueryKey_Indicator", true);
                int iUniqueRank = (int)Element.get("Unique_Indicator", 0);
                string field = "";

                // Write to SI_KEY if Querykey_Indicator is true or Unique Indicator > 0 based on PB Spec 1.04
                if ((bIsQueryKey) || (iUniqueRank > 0))
                {
                    if (isVariablelength)
                    {
                        field = split[fieldNbr].Trim();
                    }
                    else
                    {
                        field = line.Substring(startPos - 1, length).Trim();
                    }

                    // Update SI_KEY datatable
                    DataRow keyRow = keyTable.NewRow();
                    keyRow[0] = strTag;         // tag, like MRN 
                    keyRow[1] = field;          // value of tag
                    keyRow[2] = dataRow[0];     // the same generated HILO id
                    keyTable.Rows.Add(keyRow);
                }
            }
            return true;
        }

        /// <summary>
        /// Query the config and return the filename of file to import.
        /// Will FTP the file if necessary.
        /// </summary>
        /// <param name="impConfig"></param>
        /// 
        internal static string GetLocalOrFtpFile(GenericObject impConfig, ref bool deleteFileWhenDone)
        {
            string localOrFTP = ((string)impConfig.get("Import_Config_Method")).ToLower();
            string localFileName = ""; // return value
            deleteFileWhenDone = false;

            System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
            watch.Start();

            try
            {
                // Get file based on Import File Method: Folder or ftp
                if (localOrFTP == "ftp" || localOrFTP == "sftp") // Bug 13638: UMN - add SFTP support
                {
                    // Validate required config settings.
                    if (!impConfig.has("Import_Config_Address")) throw new Exception("FTP Import_Config_Address missing");
                    if (!impConfig.has("Import_Config_UID")) throw new Exception("FTP Import_Config_UID missing");
                    if (!impConfig.has("Import_Config_FTP_Filename")) throw new Exception("FTP Import_Config_FTP_Filename missing");
                    if (!impConfig.has("Import_Config_FTP_Temp_Path")) throw new Exception("FTP Import_Config_FTP_Temp_Path missing");

                    string strHost = impConfig.get("Import_Config_Address") as String;
                    string strUserID = impConfig.get("Import_Config_UID") as String;
                    string strPwd = impConfig.get("Import_Config_password", "") as String;
                    string strPort = impConfig.get("Import_Config_Port", "21") as String;
                    string strServer = impConfig.get("Import_Config_FTP_Filename") as String;

                    // Validate parameter values.
                    if (strHost.Length == 0) throw new Exception("FTP Address name invalid");
                    if (strUserID.Length == 0) throw new Exception("FTP User name invalid");
                    if (strServer.Length == 0) throw new Exception("FTP server file name invalid");

                    // Bug 16857 BEGIN - SFTP/FTP Not Working for PB Import Process
                    FTP ftp = (localOrFTP == "ftp") ? new FTP(strHost, strUserID, strPwd, strPort, true, true, false, false, false) : null;
                    SFTP sftp = (localOrFTP == "sftp") ? new SFTP(strHost, strUserID, strPwd, strPort, true, true, "", "", "") : null;
                    // Bug 16857 END

                    // Download the ftp file to localFileName
                    localFileName = impConfig.get("Import_Config_FTP_Temp_Path") as String;

                    Logger.cs_log("S/FTPGet of " + strServer + " from host:" + strHost + " port:" + strPort + " to: " + localFileName);

                    try
                    {
                        if (localOrFTP == "ftp")
                        {
                            ftp.Connect();
                            ftp.GetFile(strServer, localFileName);
                        }
                        else if (localOrFTP == "sftp") // Bug 13638: UMN - add SFTP support
                        {
                            sftp.Connect();
                            sftp.GetFile(strServer, localFileName);
                        }
                        deleteFileWhenDone = true;
                    }
                    catch (Exception)
                    {
                        // Bug 16857 (fixed error message for sftp)
                        string msg = String.Format("BFI-1112: Error Getting S/FTP file:{0}: {1}", strServer, localOrFTP == "ftp" ? ftp.errMsg : sftp.errMsg);
                        Logger.cs_log(msg);
                        throw new Exception(msg);
                    }
                    finally
                    {
                        // Destructor will call Disconnect, but with .NET it's async scheduled. So calling
                        // disconnect here will free the connection faster
                        if (ftp != null) ftp.Disconnect();
                        if (sftp != null) sftp.Disconnect();
                    }
                }
                else if (localOrFTP == "folder")
                {
                    // Validate required parameters.
                    if (!impConfig.has("Import_Config_Folder_Path")) throw new Exception("FTP Import_Config_Folder_Path missing");
                    if (!impConfig.has("Import_Config_Folder_Filename")) throw new Exception("FTP Import_Config_Folder_Filename missing");

                    localFileName = Path.Combine(impConfig.get("Import_Config_Folder_Path").ToString().Trim(),
                                     impConfig.get("Import_Config_Folder_Filename").ToString().Trim());
                }
                else
                {
                    string msg = String.Format("BFI-1113: Import method is invalid:{0}; check config. Aborting import.", localOrFTP);
                    Logger.cs_log(msg);
                    throw new Exception(msg);
                }

                if (!(File.Exists(localFileName)))
                {
                    string msg = String.Format("BFI-1114: File {0} does not exist. Unable to import file.", localFileName);
                    Logger.cs_log(msg);
                    throw new Exception(msg);
                }
            }
            finally
            {
                watch.Stop();
                Logger.cs_log(string.Format("BFI: took {0} milliseconds to get local or S/FTP file.",
                watch.ElapsedMilliseconds));
            }
            return localFileName;
        }

        // Bug 27153 DH - Clean up warnings when building ipayment
        /// <summary>
        /// Write the Weights table given the tablenumber ('1' or '2'), group and dataElements.
        /// </summary>
        /// <param name="insertConnection"></param>
        /// <param name="weightsTblName"></param>
        /// <param name="groupName"></param>
        /// <param name="dataElements"></param>
        internal static void WriteWeightsFile(
        SqlConnection insertConnection,
        string weightsTblName,
        string groupName,
        GenericObject dataElements)
        {
            Int32 counter = 1;

            // Create the data table
            DataTable dataTable = new DataTable();
            SqlBulkCopy bulkCopy = null;       // Bug 16433 [21921] UMN
            dataTable.Columns.Add("Identity_key", typeof(Int32));
            dataTable.Columns.Add("GROUP_NAME", typeof(String));
            dataTable.Columns.Add("TAG", typeof(String));
            dataTable.Columns.Add("UNIQUE_RANK", typeof(Int32));
            dataTable.Columns.Add("IS_QUERY_KEY", typeof(String));

            System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
            watch.Start();

            try
            {
                // Turn off ANSI warnings to prevent clipping errors from stopping insert
                SetAnsiWarnings(insertConnection, false);

                bulkCopy = new SqlBulkCopy(insertConnection, SqlBulkCopyOptions.TableLock, null);

                // Increase from default of 30 seconds
                bulkCopy.BulkCopyTimeout = BULK_COPY_TIMEOUT;

                // set the target table name to bulk copy into
                bulkCopy.DestinationTableName = weightsTblName;

                // Write to SI_WEIGHTS
                foreach (GenericObject Element in dataElements.vectors)
                {
                    string strTag = Element.get("Tag_name", "") as String;
                    int iUniqueRank = (int)Element.get("Unique_Indicator", 0);
                    bool bIsQueryKey = (bool)Element.get("QueryKey_Indicator", true);
                    char cIsQueryKey = (bIsQueryKey) ? 'Y' : 'N';

                    // set the row and add it to the DataTable
                    DataRow row = dataTable.NewRow();
                    row[0] = counter;   // Hopefully will ignore this column
                    row[1] = groupName;
                    row[2] = strTag;
                    row[3] = iUniqueRank;
                    row[4] = cIsQueryKey;
                    dataTable.Rows.Add(row);
                    counter++;
                }

                // write the data in the dataTable
                bulkCopy.WriteToServer(dataTable);
            }
            catch (Exception)// Bug 27153 DH - Clean up warnings when building ipayment
            {
                //string msg = String.Format("BFI ERROR in WriteWeightsFile: {0}", e.ToMessageAndCompleteStacktrace()); // bug 17849
                //Logger.cs_log(msg);
                throw;
            }
            finally
            {
                // Bug 16433 [21921] UMN dispose and close
                if (bulkCopy != null)
                {
                    bulkCopy.Close();
                }
                dataTable.Dispose();
                watch.Stop();
                Logger.cs_log(string.Format("BFI: took {0} milliseconds to WRITE weights.",
                watch.ElapsedMilliseconds));
            }
        }

        /// <summary>
        /// Query the database and return the tablenames.
        /// </summary>
        /// <param name="insertConnection"></param>
        /// <param name="dataTblName"></param>
        /// <param name="keyTblName"></param>
        /// <param name="weightsTblName"></param>
        /// 
        internal static void GetTableNames(
        SqlConnection insertConnection,
        ref string dataTblName,
        ref string keyTblName,
        ref string weightsTblName)
        {
            SqlDataReader dataReader = null;
            System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
            watch.Start();

            try
            {
                // query was done this way so we get atomic update in database of Next_Hi
                string selSQL = "SELECT IMP_SI_DATA, IMP_SI_KEY, IMP_SI_WEIGHTS FROM SI_NAMES WHERE ROW_ID = 1";
                SqlCommand selCommand = new SqlCommand(selSQL, insertConnection);
                dataReader = selCommand.ExecuteReader();
                if (!dataReader.Read() || dataReader.IsDBNull(0))
                {
                    string msg = String.Format("BFI-1115: No rows in SI_NAMES. Invalid DB config.");
                    Logger.cs_log(msg);
                    throw new Exception(msg);
                }
                dataTblName = dataReader.GetString(0);
                keyTblName = dataReader.GetString(1);
                weightsTblName = dataReader.GetString(2);
            }
            catch (Exception e)
            {
                string msg = String.Format("BFI ERROR in GetTableNames: {0}", e.ToMessageAndCompleteStacktrace()); // Bug 17849
                Logger.cs_log(msg);
                throw;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
                watch.Stop();
                Logger.cs_log(string.Format("BFI: took {0} milliseconds to GetTableNames.",
                watch.ElapsedMilliseconds));
            }
        }

        /// <summary>
        /// Query the database and return the atomically updated HI value (for HiLo counter).
        /// </summary>
        /// <param name="dataTblName"></param>
        /// <param name="keyTblName"></param>
        /// <param name="weightsTblName"></param>
        /// 
        internal static void GetNextHiLo(
        SqlConnection insertConnection,
        ref Int64 high,
        ref Int64 low)
        {
            SqlDataReader dataReader = null;

            try
            {
                // query was done this way so we get atomic update in database of Next_Hi
                string updateSQL = "UPDATE SI_NAMES SET NEXT_HI = NEXT_HI + 1 OUTPUT INSERTED.NEXT_HI WHERE ROW_ID = 1";
                SqlCommand updateCommand = new SqlCommand(updateSQL, insertConnection);
                dataReader = updateCommand.ExecuteReader();
                if (!dataReader.Read() || dataReader.IsDBNull(0))
                {
                    string msg = String.Format("BFI-1115: No rows in SI_NAMES. Invalid DB config.");
                    Logger.cs_log(msg);
                    throw new Exception(msg);
                }
                high = dataReader.GetInt64(0);
                low = 0;
            }
            catch (Exception e)
            {
                string msg = String.Format("BFI ERROR in GetNextHiLo: {0}", e.ToMessageAndCompleteStacktrace()); // Bug 17849
                Logger.cs_log(msg);
                throw;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Resest the HI value in DB (for HiLo counter).
        /// </summary>
        /// <param name="insertConnection"></param>
        /// 
        internal static void ResetHiLo(SqlConnection insertConnection)
        {
            try
            {
                // query was done this way so we get atomic update in database of Next_Hi
                string updateSQL = "UPDATE SI_NAMES SET Next_Hi = 0 where Row_Id = 1";
                SqlCommand updateCommand = new SqlCommand(updateSQL, insertConnection);
                updateCommand.ExecuteNonQuery();
            }
            catch (Exception)// Bug 27153 DH - Clean up warnings when building ipayment
            {
                //string msg = String.Format("ERROR in ResetHiLo: {0}", e.ToMessageAndCompleteStacktrace()); // Bug 17849
                //Logger.cs_log(msg);
                throw;
            }
        }

        /// <summary>
        /// Swap the tablenames in the SI_NAMES table.
        /// </summary>
        /// <param name="updateConnection"></param>
        /// 
        internal static bool SwapTableNames(SqlConnection updateConnection)
        {
            bool swapReturnValue = true;  // no error

            System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
            watch.Start();

            try
            {
                // the connection is opened by the caller

                // Todo: change to Prepared statement?
                string updateString = "UPDATE SI_NAMES SET INQ_SI_DATA=IMP_SI_DATA, INQ_SI_KEY=IMP_SI_KEY, INQ_SI_WEIGHTS=IMP_SI_WEIGHTS, IMP_SI_DATA=INQ_SI_DATA, IMP_SI_KEY=INQ_SI_KEY, IMP_SI_WEIGHTS=INQ_SI_WEIGHTS";
                SqlCommand updateCommand = new SqlCommand(updateString, updateConnection);
                updateCommand.ExecuteNonQuery();
            }
            catch (Exception)// Bug 27153 DH - Clean up warnings when building ipayment
            {
                // swapReturnValue = false; // an error occurred
                //string msg = String.Format("BFI ERROR swapping table names: {0}", e.ToMessageAndCompleteStacktrace()); // Bug 17849
                //Logger.cs_log(msg);
                throw;
            }
            finally
            {
                watch.Stop();
                Logger.cs_log(string.Format("BFI: took {0} milliseconds to SwapTableNames.",
                watch.ElapsedMilliseconds));
            }

            return swapReturnValue;
        }

        /// <summary>
        /// Remove the FK_SI_KEY_SI_DATA constraint but only if it exists.
        /// IPAY-436
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="keyTblName"></param>
        internal static void removeConstraint(
            SqlConnection connection,
            string keyTblName
        )
        {
            try
            {
                string removeConstraintSQL = $@"IF (OBJECT_ID('dbo.FK_SI_KEY_SI_DATA', 'F') IS NOT NULL)
                BEGIN
                    ALTER TABLE {keyTblName} DROP CONSTRAINT [FK_SI_KEY_SI_DATA]
                END";

                using (SqlCommand removeConstraintCommand = new SqlCommand(removeConstraintSQL))
                {
                    removeConstraintCommand.Connection = connection;
                    removeConstraintCommand.CommandType = CommandType.Text;

                    removeConstraintCommand.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                Logger.LogError("Cannot remove the FK_SI_KEY_SI_DATA constraint: " + e.Message, "Inquiry Import");
                Logger.cs_log("ERROR: Cannot remove the FK_SI_KEY_SI_DATA constraint: " + e.ToString());
            }

        }

        /// <summary>
        /// Truncate tables using supplied connection parameters.
        /// Also log elapsed time.
        /// </summary>
        /// <param name="insertConnection"></param>
        /// <param name="truncateTableName"></param>
        /// 
        internal static void TruncateTables(
    SqlConnection insertConnection,
    string dataTblName,
    string keyTblName,
    string weightsTblName)
    {
      System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
      watch.Start();

      // IPAY-436: Make sure that the FK_SI_KEY_SI_DATA constraint has been removed before the TRUNCATEs
      removeConstraint(insertConnection, keyTblName);   

      string truncateSQL = "TRUNCATE TABLE " + keyTblName;

      try
      {
        // the connection is opened by the caller

        SqlCommand truncateCommand = new SqlCommand(truncateSQL, insertConnection);
        truncateCommand.ExecuteNonQuery();
        truncateCommand.CommandText = "TRUNCATE TABLE " + dataTblName;
        truncateCommand.ExecuteNonQuery();
        truncateCommand.CommandText = "TRUNCATE TABLE " + weightsTblName;
        truncateCommand.ExecuteNonQuery();
      }
      catch (Exception)// Bug 27153 DH - Clean up warnings when building ipayment
      {
        //string msg = String.Format("BFI ERROR in TruncateTables: {0}", e.ToMessageAndCompleteStacktrace()); // Bug 17849
        //Logger.cs_log(msg);
        throw;
      }
      finally
      {
        watch.Stop();
        Logger.cs_log(string.Format("BFI: took {0} milliseconds to TruncateTables.",
        watch.ElapsedMilliseconds));
      }
    }

    /// <summary>
    /// Copy the data from one set of tables to the other. Use select into.
    /// </summary>
    /// <param name="copyConnection"></param>
    internal static void CopyTableData(
    SqlConnection copyConnection,
    string destDataTblName,
    string destKeyTblName,
    string destWeightsTblName)
    {
      System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
      watch.Start();

      char tblNumber = destDataTblName[destDataTblName.Length - 1];
      tblNumber = (tblNumber == '1') ? '2' : '1';

      string srcDataTblName = destDataTblName.Substring(0, destDataTblName.Length - 1) + tblNumber;
      string srcKeyTblName = destKeyTblName.Substring(0, destKeyTblName.Length - 1) + tblNumber;
      string srcWeightsTblName = destWeightsTblName.Substring(0, destWeightsTblName.Length - 1) + tblNumber;

      // we don't care about rollback since the dest table is always truncated before a copy
      // SqlTransaction transaction = null;
      try
      {
        // the connection is opened by the caller

        string copyString = "INSERT INTO " + destDataTblName + " SELECT * FROM " + srcDataTblName;
        SqlCommand sqlCommand = new SqlCommand(copyString, copyConnection);
        sqlCommand.CommandTimeout = 1200; // 20 min time out

        // transaction = copyConnection.BeginTransaction();
        sqlCommand.ExecuteNonQuery();
        sqlCommand.CommandText = "INSERT INTO " + destKeyTblName + " SELECT * FROM " + srcKeyTblName;
        sqlCommand.ExecuteNonQuery();

        // weights table has an identity column
        string identityInsertOn = "SET IDENTITY_INSERT " + destWeightsTblName + " ON";
        string identityInsertOff = "SET IDENTITY_INSERT " + destWeightsTblName + " OFF";
        SqlCommand identCommand = new SqlCommand(identityInsertOn, copyConnection);
        identCommand.ExecuteNonQuery();

        sqlCommand.CommandText = "INSERT INTO " + destWeightsTblName +
        "(WEIGHTS_ID, GROUP_NAME, TAG, UNIQUE_RANK, IS_QUERY_KEY) SELECT WEIGHTS_ID, GROUP_NAME, TAG, UNIQUE_RANK, IS_QUERY_KEY FROM "
        + srcWeightsTblName;
        sqlCommand.ExecuteNonQuery();

        identCommand.CommandText = identityInsertOff;
        identCommand.ExecuteNonQuery();

        // transaction.Commit();
      }
      catch (Exception)// Bug 27153 DH - Clean up warnings when building ipayment
      {
        //string msg = String.Format("BFI ERROR in CopyTableData: {0}", e.ToMessageAndCompleteStacktrace()); // Bug 17849
        //Logger.cs_log(msg);
        throw;
      }
      finally
      {
        watch.Stop();
        Logger.cs_log(
        string.Format("BFI: took {0} seconds to CopyTables.", watch.ElapsedMilliseconds / 1000));
      }
    }

    /// <summary>
    /// Delete rows for the group. Only called when swapMode is Individual. If swapMode is All,
    /// Truncate is used.
    /// </summary>
    /// <param name="insertConnection"></param>
    /// <param name="dataTblName"></param>
    /// <param name="keyTblName"></param>
    /// <param name="weightsTblName"></param>
    /// 
    internal static void DeleteTableRows(
    SqlConnection insertConnection,
    string groupName,
    string dataTblName,
    string keyTblName,
    string weightsTblName)
    {
      System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
      watch.Start();

      // this code will be slow because of the delete cascade, but what to do
      // since the requirements are that we must support individual file imports!
      string alterSQL = "ALTER TABLE " + keyTblName
      + " WITH NOCHECK ADD CONSTRAINT [FK_SI_KEY_SI_DATA] FOREIGN KEY(DATA_ID) REFERENCES "
      + dataTblName + " (DATA_ID) ON DELETE CASCADE";

      string delSQL = "DELETE FROM " + dataTblName + " WHERE GROUP_NAME = '" + groupName + "'";

      // no need for transactions as this is all done to a table that was first truncated, 
      // then copied to, and now having group records deleted. Any error, will mean whole
      // process has to be redone.
      try
      {
        // the connection is opened by the caller

        // set up a delete cascade so the records in SI_KEY are deleted.
        SqlCommand alterCommand = new SqlCommand(alterSQL, insertConnection);
        SqlCommand delCommand = new SqlCommand(delSQL, insertConnection);
        delCommand.CommandTimeout = 1200; // 20 min time out


        try
        {
          // IPAY-436: Move this into the same try block as the drop constraint
          alterCommand.ExecuteNonQuery();      

          // Drop existing data for the Group
          delCommand.ExecuteNonQuery();

          // Drop existing weights data for the Group
          delCommand.CommandText = "DELETE FROM " + weightsTblName + " WHERE GROUP_NAME = '" + groupName + "'"; ;
          delCommand.ExecuteNonQuery();
        }
        finally
        {
          // have to set up ths way, so that if something fails, the drop constraint will still be done
          // if it succeeded in the first place
          removeConstraint(insertConnection, keyTblName);   // IPAY-436: Encapsulate the drop constraint
        }
      }
      catch (Exception)// Bug 27153 DH - Clean up warnings when building ipayment
      {
        //string msg = String.Format("BFI ERROR in DeleteTableRows: {0}", e.ToMessageAndCompleteStacktrace()); // Bug 17849
        //Logger.cs_log(msg);
        throw;
      }
      finally
      {
        watch.Stop();
        Logger.cs_log(string.Format("BFI: took {0} seconds to DeleteTableRows.",
        watch.ElapsedMilliseconds / 1000));
      }
    }

    /// <summary>
    /// Turn ANSI_WARNINGS on or off
    /// </summary>
    /// <param name="insertConnection"></param>
    /// <param name="off">Set to true to turn off ANSI warnings, false otherwise</param>
    internal static void SetAnsiWarnings(SqlConnection insertConnection, bool turnOn)
    {
      string ansiCommandString = turnOn ? "SET ANSI_WARNINGS ON" : "SET ANSI_WARNINGS OFF";
      try
      {
        // the connection is opened by the caller

        SqlCommand ansiCommand = new SqlCommand(ansiCommandString, insertConnection);
        ansiCommand.ExecuteNonQuery();
      }
      catch (Exception)// Bug 27153 DH - Clean up warnings when building ipayment
      {
        //string msg = String.Format("BFI ERROR in SET ANSI_WARNINGS: {0}", e.ToMessageAndCompleteStacktrace()); // Bug 17849
        //Logger.cs_log(msg);
        throw;
      }
    }

    /// <summary>
    /// Deletes a local file.
    /// </summary>
    /// <param name="strLocalFile">The filename to delete</param>
    /// <param name="deleteFile">Whether to keep file or delete it; useful when debugging</param>
    internal static bool DeleteFile(string strLocalFile, bool deleteFile)
    {
      if (!deleteFile)
        return true;

      try
      {
        if (File.Exists(strLocalFile))
        {
          File.Delete(strLocalFile);
          Logger.cs_log("BFI: Deleted temp file: " + strLocalFile);
        }
      }
      catch (Exception e)
      {
        string msg = String.Format("BFI ERROR in DeleteFile on {0}: {1}", strLocalFile, e.ToMessageAndCompleteStacktrace()); // Bug 17849
        Logger.cs_log(msg);
        return false;
      }

      return true;
    }
  }  // end class FileImport
}