﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CASL_engine;
using ProjectBaseline.ServiceReference;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Globalization; // Bug 14670 

namespace ProjectBaseline.ServiceReference
{
  // UMN -- add these constructors so code below is less grody and more readable
  public partial class TransactionItem : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged
  {
    public TransactionItem() { }
    public TransactionItem(string name, string value)
    {
      this.name = name;
      this.value = value;
    }
  }

  public partial class TenderItem : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged
  {
    public TenderItem() { }
    public TenderItem(string name, string value)
    {
      this.name = name;
      this.value = value;
    }
  }

  public partial class COREFileItem : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged
  {
    public COREFileItem() { }
    public COREFileItem(string name, string value)
    {
      this.name = name;
      this.value = value;
    }
  }

  public partial class COREEventItem : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged
  {
    public COREEventItem() { }
    public COREEventItem(string name, string value)
    {
      this.name = name;
      this.value = value;
    }
  }
}

namespace ProjectBaseline
{
  partial class CRealtimeUpdate //IPAY-313 NAM: PB Standard Validate
  {
    // Bug 14661 UMN add keys to skip on reversal or void. For performance, statically construct List
    private static readonly List<string> roKeysToSkip = new List<String>  // Bug 14661
    { 
      "total", 
      "ccv", 
      "cvv", 
      "TROUTD",
      "CTROUTD", // IPAY-13 DJD: Use PAYware CTROUTD for follow-on transactions
      "exp_date", 
      "exp_month", 
      "exp_year",
      "credit_card_nbr_en"
    };

    // These are used to test a failure response. They're based on a particularly ugly exception returned by web service.
    public static readonly string mockErrorCode = "4063";
    public static readonly string mockErrorDetail = "ORA-04063";
    public static readonly string mockErrorSummary = "Exception occured when binding was invoked";
    //IPAY-313 NAM: PB Standard Validate Hashtable to store Event Unique IDs when calling web service validation
    private static Hashtable m_htUniqueID = null;

    /// <summary>
    /// Gets Called by System_interface.PBStandardRealtimeUpdate.process_update_core_item_aux
    /// </summary>
    /// <param name="argPairs"></param>
    /// <returns></returns>
    // Bug 13213 QG:  Rework realtime update/void 
    public static object CASLProcess_Update(params object[] argPairs)
    {
      GenericObject args = misc.convert_args(argPairs);
      GenericObject oSystemInterface = args.get("_subject", "") as GenericObject;
      string strReceiptReferenceNbr = args.get("receipt_ref_nbr", "") as string;
      GenericObject geoCoreFile = args.get("a_core_file", "") as GenericObject;
      GenericObject geoCoreEvent = args.get("a_core_event", "") as GenericObject;
      GenericObject geoTrans = args.get("Transactions", "") as GenericObject;
      GenericObject geoTenders = args.get("Tenders", "") as GenericObject;
      // Bug 12208 QG Add tta mapping information
      GenericObject geoTTAMapping = args.get("tta_mapping", "") as GenericObject;
      return Process_Update(oSystemInterface, strReceiptReferenceNbr, geoCoreFile, geoCoreEvent, geoTrans, geoTenders, geoTTAMapping);
    }

    /// <summary>
    /// Gets Called by System_interface.PBStandardRealtimeUpdate.process_unupdate_core_item_aux
    /// </summary>
    /// <param name="argPairs"></param>
    /// <returns></returns>
    public static object CASLProcess_Unupdate(params object[] argPairs)
    {
      GenericObject args = misc.convert_args(argPairs);
      GenericObject oSystemInterface = args.get("_subject", "") as GenericObject;
      string update_reference_nbr = args.get("UpdateReference", "") as string;
      string strReceiptReferenceNbr = args.get("receipt_ref_nbr", "") as string;
      GenericObject geoCoreFile = args.get("a_core_file", "") as GenericObject;
      GenericObject geoCoreEvent = args.get("a_core_event", "") as GenericObject;
      GenericObject geoTrans = args.get("Transactions", "") as GenericObject;
      GenericObject geoTenders = args.get("Tenders", "") as GenericObject;
      // Bug 12208 add tta mapping info
      GenericObject geoTTAMapping = args.get("tta_mapping", "") as GenericObject;
      return Process_UnUpdate(oSystemInterface, update_reference_nbr, strReceiptReferenceNbr, geoCoreFile, geoCoreEvent, geoTrans, geoTenders, geoTTAMapping); // Bug# 12208
    }

    /// <summary>
    /// Does the actual realtime update for posts or reversals (reversals have negative amount)
    /// </summary>
    /// <param name="oSystemInterface"></param>
    /// <param name="strReceiptReferenceNbr"></param>
    /// <param name="geoCoreFile"></param>
    /// <param name="geoCoreEvent"></param>
    /// <param name="geoTrans"></param>
    /// <param name="geoTenders"></param>
    /// <param name="geoTTAMapping"></param>
    /// <returns></returns>
    public static object Process_Update(GenericObject oSystemInterface, string strReceiptReferenceNbr, GenericObject geoCoreFile, 
      GenericObject geoCoreEvent, GenericObject geoTrans, GenericObject geoTenders, GenericObject geoTTAMapping) // Bug 12208 Add TTA mapping info
    {
      string requestType = "Real Time Update";
      string excToThrow = oSystemInterface.get("exc_to_throw", "", true) as String;
      bool logTimings = (bool)oSystemInterface.get("log_timing_data", false); // Bug 15206 make timings configurable
      System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
      if (logTimings)
      {
        // Bug 15206 UMN add timing support
        watch.Start();
      }

      // Bug 13831 QG use try/catch to catch any exception.
      try
      {
        //IPAY-313 NAM: PB Standard Validate - always start with an empty Unique ID Hashtable
        if (m_htUniqueID == null)
            m_htUniqueID = new Hashtable();
        else
            m_htUniqueID.Clear();
        UpdateRequest request = new UpdateRequest();
        // Bug 12208 QG Add Type for web service use
        request.Type = requestType;

        // Bug 13194 QG
        request.SystemInterfaces = SetSystemInterfaces(oSystemInterface);
        request.COREFile = SetCoreFile();

        // Bug 14670 SX Add COREFile standard fields as array of COREFileItem 
        SetCoreFileItems(oSystemInterface, strReceiptReferenceNbr, geoCoreFile, geoCoreEvent, request, "UPDATEREQUEST");

        // Bug 13213 QG Modify code to process real time update on core event level
        SetCOREEventInfo(ref request.COREFile[0].COREEvents[0].COREEvent[0], geoCoreEvent, geoTrans, geoTenders, geoTTAMapping, 
        strReceiptReferenceNbr, request.Type); // Bug 12208 QG add tta mapping info; IPAY-1024 HX: added geoCoreEvent

        // Bug 14781 Support both http and https 
        SSIClient oSSIClient = CStandardSystemInterface.getSSICientFromEndPoint(oSystemInterface);
        UpdateResponse response = new UpdateResponse();

        LogObjXml(oSystemInterface, request, "UPDATEREQUEST");

        // Bug 15206 UMN - simulate failure response
        if (excToThrow == "failureresponse")
        {
          response.Type = "Failure";
          response.ErrorCode = CRealtimeUpdate.mockErrorCode;
          response.ErrorDetail = CRealtimeUpdate.mockErrorDetail;
          response.ErrorSummary = CRealtimeUpdate.mockErrorSummary;
        }
        else
        {
          response = CStandardSystemInterface.CallWebService<UpdateResponse, UpdateRequest>(oSystemInterface,
          ref oSSIClient, oSSIClient.UpdateRequestOperation, request, excToThrow);
        }

        LogObjXml(oSystemInterface, response, "UPDATERESPONSE");
        //Bug 26510 NAM: verify response has 'Type'
        if (string.IsNullOrEmpty(response.Type))
        {
          GenericObject geoReturn = new GenericObject();
          CStandardSystemInterface.CreateFailureResponseGEO(geoReturn, new GenericObject(), "Update Failure", "WS-101", "Invalid response type", "Response type was not present or recognized");
          return geoReturn;
        }
        return CreateCASLReturnGEO(response.Type, response.UpdateReference, response.ErrorCode, response.ErrorDetail, response.ErrorSummary);
      }
      catch (System.ServiceModel.FaultException ex) // Bug 13831 
      {
        // Bug 17849
        Logger.cs_log_trace("Error in Process_Update", ex);
        // Bug 19806 UMN
        //if (ex.InnerException != null)
        //  Logger.cs_log(ex.InnerException.StackTrace);
        // else
        //  Logger.cs_log(ex.StackTrace);
        Logger.cs_log(((FaultException)ex).CreateMessageFault().GetReaderAtDetailContents().Value);


        return GetCASLError("PB-113", requestType, ex); // possible incompatible web service configuration
      }
      catch (Exception ex)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in Process_Update", ex);

        // Bug 19806 UMN
        //if (ex.InnerException != null)
        //  Logger.cs_log(ex.InnerException.StackTrace);
        // else
        //  Logger.cs_log(ex.StackTrace);
        return GetCASLError("PB-107", requestType, ex);  // generic error occurred
      }
      // End of Bug 13831
      finally
      {
        if (logTimings)
        {
          watch.Stop();
          TimeSpan ts = watch.Elapsed;
          Logger.cs_log(string.Format("{0} took {1} seconds.", requestType, watch.ElapsedMilliseconds / 1000.00));
        }
      }
    }

    /// <summary>
    /// Does the real time void
    /// </summary>
    /// <param name="oSystemInterface"></param>
    /// <param name="update_reference_nbr"></param>
    /// <param name="strReceiptReferenceNbr"></param>
    /// <param name="geoTrans"></param>
    /// <param name="geoTenders"></param>
    /// <param name="geoTTAMapping"></param>
    /// <returns></returns>
    public static object Process_UnUpdate(GenericObject oSystemInterface, string update_reference_nbr, string strReceiptReferenceNbr, GenericObject geoCoreFile, 
        GenericObject geoCoreEvent, GenericObject geoTrans, GenericObject geoTenders, GenericObject geoTTAMapping) // Bug 12208 QG Add tta mapping info
    {
      string requestType = "Real Time Void";
      string excToThrow = oSystemInterface.get("exc_to_throw", "") as String;
      bool logTimings = (bool)oSystemInterface.get("log_timing_data", false); // Bug 15206 make timings configurable
      System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
      if (logTimings)
      {
        // Bug 15206 UMN add timing support
        watch.Start();
      }

      // Bug 13831 QG use try/catch to catch any exception.
      try
      {
        if (String.IsNullOrEmpty(update_reference_nbr))
        {
          throw new Exception("Update Reference is invalid (null or empty).");   // Bug 13831
        }
        VoidRequest request = new VoidRequest();
        request.Type = requestType;
        request.UpdateReference = update_reference_nbr;

        // Bug 13194 QG
        request.SystemInterfaces = SetSystemInterfaces(oSystemInterface);
        request.COREFile = SetCoreFile();

        // Bug 14838 SX Add COREFile standard fields as array of COREFileItem 
        SetCoreFileItems(oSystemInterface, strReceiptReferenceNbr, geoCoreFile, geoCoreEvent, request, "VOIDREQUEST");

        // Bug 13213 QG Modify code to process real time update on core event level
        SetCOREEventInfo(ref request.COREFile[0].COREEvents[0].COREEvent[0], geoCoreEvent,geoTrans, geoTenders, geoTTAMapping, 
        strReceiptReferenceNbr, request.Type); // Bug 12208; IPAY-1024 HX: added geoCoreEvent

        // Bug 14781 Support both http and https 
        SSIClient oSSIClient = CStandardSystemInterface.getSSICientFromEndPoint(oSystemInterface);
        VoidResponse response = new VoidResponse();

        LogObjXml(oSystemInterface, request, "VOIDREQUEST");

        // Bug 15206 UMN - simulate failure response
        if (excToThrow == "failureresponse")
        {
          response.Type = "Failure";
          response.ErrorCode = CRealtimeUpdate.mockErrorCode;
          response.ErrorDetail = CRealtimeUpdate.mockErrorDetail;
          response.ErrorSummary = CRealtimeUpdate.mockErrorSummary;
        }
        else
        {
          response = CStandardSystemInterface.CallWebService<VoidResponse, VoidRequest>(oSystemInterface,
          ref oSSIClient, oSSIClient.VoidRequestOperation, request, excToThrow);
        }

        LogObjXml(oSystemInterface, response, "VOIDRESPONSE");
        //Bug 26510 NAM: verify response has 'Type'
        if (string.IsNullOrEmpty(response.Type))
        {
          GenericObject geoReturn = new GenericObject();
          CStandardSystemInterface.CreateFailureResponseGEO(geoReturn, new GenericObject(), "Void Failure", "WS-102", "Invalid response type", "Response type was not present or recognized");
          return geoReturn;
        }

        return CreateCASLReturnGEO(response.Type, request.UpdateReference, response.ErrorCode, response.ErrorDetail, response.ErrorSummary);
      }
      catch (System.ServiceModel.FaultException ex) // Bug 13831 
      {
        // Bug 17849
        Logger.cs_log_trace("Error in Process_UnUpdate", ex);
        // Bug 19806 UMN
        //if (ex.InnerException != null)
        //  Logger.cs_log(ex.InnerException.StackTrace);
        //else
        //  Logger.cs_log(ex.StackTrace);
        Logger.cs_log(((FaultException)ex).CreateMessageFault().GetReaderAtDetailContents().Value);

        return GetCASLError("PB-113", requestType, ex); // possible incompatible web service configuration
      }
      catch (Exception ex)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in Process_UnUpdate", ex);
        // Bug 19806 UMN
        //if (ex.InnerException != null)
        //  Logger.cs_log(ex.InnerException.StackTrace);
        //else
        //  Logger.cs_log(ex.StackTrace);
        return GetCASLError("PB-107", requestType, ex);  // generic error occurred
      }
      finally
      {
        if (logTimings)
        {
          watch.Stop();
          TimeSpan ts = watch.Elapsed;
          Logger.cs_log(string.Format("{0} took {1} seconds.", requestType, watch.ElapsedMilliseconds / 1000.00));
        }
      }
    }

    /// <summary>
    /// Gets the error to return to CASL. UMN: refactor because cloning. Use Generic shared function
    /// </summary>
    /// <param name="errCode"></param>
    /// <param name="reqType"></param>
    /// <param name="e"></param>
    /// <returns></returns>
    public static object GetCASLError(string errCode, string reqType, Exception e)
    {
      GenericObject oErrorInConfig = (c_CASL.c_object("errors") as GenericObject).get(errCode, new GenericObject("message", "Error Message is not Configured.")) as GenericObject;
      string sErrorMessage = oErrorInConfig.get("details", "") as string;
      string sExceptionMessage;
      if (e.InnerException != null)
      {
        sExceptionMessage = e.Message + " (" + e.InnerException.GetType().Name + ") " + e.InnerException.Message;
      }
      else
      {
        sExceptionMessage = "(" + e.GetType().Name + ") " + e.Message;
      }
      sErrorMessage = sErrorMessage.Replace("{0}", reqType).Replace("{1}", sExceptionMessage);
      GenericObject oError = (GenericObject)c_CASL.c_make("error", "code", errCode, "message", sErrorMessage, "title", "Runtime", "throw", false, "user_message", oErrorInConfig.get("user_message", false)); // BUG#13029 
      Logger.LogError(sErrorMessage + "(" + errCode + ")", "");
      return oError;
    }

    /// <summary>
    /// Creates the return GEO that is sent back to CASL. UMN: refactor because cloning. Use Generic shared function
    /// </summary>
    /// <param name="status"></param>
    /// <param name="refType"></param>
    /// <param name="refValue"></param>
    /// <param name="errCode"></param>
    /// <param name="errorDetail"></param>
    /// <param name="errSummary"></param>
    /// <returns></returns>
    private static object CreateCASLReturnGEO(string status, string refValue, string errCode, 
            string errorDetail, string errSummary)
    {
      string statusLC = status.ToLower();
      if (statusLC == "success")
      {
        GenericObject geoReturn = new GenericObject();
        geoReturn.Add("Type", "success");
        geoReturn.Add("UpdateReference", refValue);
        return geoReturn;
      }
      else if (statusLC == "failure")
      {
        GenericObject geoReturn = new GenericObject();
        CStandardSystemInterface.CreateFailureResponseGEO(geoReturn, new GenericObject(), status, errCode, errSummary, errorDetail);
        return geoReturn;
      }
      else
      {
        throw (new Exception("Unknown web service Error."));  // Bug#13831
      }
    }


    private static void SetCoreFileItems(GenericObject oSystemInterface, string strReceiptReferenceNbr, GenericObject geoCoreFile,
    GenericObject geoCoreEvent, object req, string sType)
    {
        // Add REQUESTTYPE 
        ArrayList arrSystemInterfaceItems = SetRequestType(oSystemInterface);
        UpdateRequest ureq = null;
        VoidRequest vreq = null;
        switch (sType)
        {
            case "UPDATEREQUEST":
                ureq = req as UpdateRequest;
                break;
            case "VOIDREQUEST":
                vreq = req as VoidRequest;
                break;
        }

        if (ureq != null)
            ureq.SystemInterfaces.SystemInterface[0].SystemInterfaceItem = arrSystemInterfaceItems.ToArray(typeof(SystemInterfaceItem)) as SystemInterfaceItem[];
        else
            vreq.SystemInterfaces.SystemInterface[0].SystemInterfaceItem = arrSystemInterfaceItems.ToArray(typeof(SystemInterfaceItem)) as SystemInterfaceItem[];

        // Bug 14670  Add COREFile standard fields as array of COREFileItem 
        ArrayList arrCoreFileItems = new ArrayList();

        arrCoreFileItems.Add(new COREFileItem("DEPTID", geoCoreFile.get("department", "").ToString()));
        arrCoreFileItems.Add(new COREFileItem("FILENAME", geoCoreFile.get("id", "").ToString()));
        string sFileType = geoCoreFile.get("type", "").ToString();
       switch (sFileType)
        {
             case "user":
                sFileType = "I";
                break;           
           case "user_group":
                sFileType = "S";
                break;           
           case "vault_file":
                sFileType = "V";
                break;           
           case "web_file":
                sFileType = "W";
                break;
          }

       arrCoreFileItems.Add(new COREFileItem("FILETYPE", sFileType));
        arrCoreFileItems.Add(new COREFileItem("FILEDESC", geoCoreFile.get("description", "").ToString()));
        arrCoreFileItems.Add(new COREFileItem("OPEN_USERID",
          ((GenericObject)geoCoreFile.get("created_stamp", new GenericObject())).get("user", "").ToString()));
        string sCREATOR_USERID= geoCoreFile.get("creator_id", "").ToString();
        if(sCREATOR_USERID=="")
            sCREATOR_USERID= (geoCoreFile.get("created_stamp", c_CASL.c_GEO()) as GenericObject).get("user","").ToString();
        arrCoreFileItems.Add(new COREFileItem("CREATOR_USERID", sCREATOR_USERID));

        // Add OPENDT
        string sOPENDT = ((GenericObject)geoCoreFile.get("created_stamp", new GenericObject())).get("datetime", "").ToString();
        arrCoreFileItems.Add(new COREFileItem("OPENDT", toDateTimeExternalFormat(sOPENDT)));
        string sEffectiveDate = geoCoreFile.get("effective_date", "").ToString();
        DateTime dtEffectiveDate =DateTime.Parse(sEffectiveDate);
        sEffectiveDate = dtEffectiveDate.ToString("MM/dd/yyyy", DateTimeFormatInfo.InvariantInfo);
        arrCoreFileItems.Add(new COREFileItem("EFFECTIVEDT", sEffectiveDate));

        string sSource = "";
        object oSource = geoCoreFile.get("source", "");
        if (oSource is string)
            sSource = oSource.ToString();
        else if (oSource is GenericObject)
            sSource = (oSource as GenericObject).get("id", "").ToString();
        arrCoreFileItems.Add(new COREFileItem("SOURCE_TYPE", sSource));

        // end of Bug 14670 
        COREEvents[] arCOREEvents = new COREEvents[1];
        arCOREEvents[0] = new COREEvents();
        arCOREEvents[0].COREEvent = new COREEvent[1];
        arCOREEvents[0].COREEvent[0] = new COREEvent();
        arCOREEvents[0].COREEvent[0].ReceiptReferenceNbr = strReceiptReferenceNbr;

        // Bug 14670  Add COREEvent standard fields  as array of COREEventItem 
        ArrayList arrCoreEventItems = new ArrayList();
        // Add USERID
        string sEventUserID = ((GenericObject)geoCoreEvent.get("created_stamp", new GenericObject())).get("user", "").ToString();
        arrCoreEventItems.Add(new COREEventItem("USERID", sEventUserID));
        // Add USERSECUREPROFILEID
        string sUSERSECUREPROFILEID = "";
        // Bug 24902 MJO - SYSTEM user doesn't exist, so skip in that case
        if (sEventUserID != "" && sEventUserID != "SYSTEM")
        {
          // Bug 17340 MJO - If numeric, enclose the id in quotes
          int testInt = 0;
          if (int.TryParse(sEventUserID, out testInt))
            sEventUserID = "'" + sEventUserID + "'";

            sUSERSECUREPROFILEID = c_CASL.GEO.get_path("User.of." + sEventUserID + ".security_profile.id").ToString();
        }
        arrCoreEventItems.Add(new COREEventItem("USERSECUREPROFILEID", sUSERSECUREPROFILEID));
        // Add CREATION_DT
        string sCREATION_DT = ((GenericObject)geoCoreEvent.get("created_stamp", new GenericObject())).get("datetime", "").ToString();
        arrCoreEventItems.Add(new COREEventItem("CREATION_DT", toDateTimeExternalFormat(sCREATION_DT)));
        // Add REVERSAL_OF, only included for reversal receipt 
        if (geoCoreEvent.has("reversal_of"))
        {
            GenericObject geoREVERSAL_OF = geoCoreEvent.get("reversal_of", null) as GenericObject;
            // Bug 14661 UMN, comment 12
            string sREVERSAL_OF = String.Format("{0}{1}-{2}", geoREVERSAL_OF.get("DEPFILENBR", "").ToString(),
            geoREVERSAL_OF.get("DEPFILESEQ", "").ToString().PadLeft(3, '0'), geoREVERSAL_OF.get("EVENTNBR", "").ToString());
            arrCoreEventItems.Add(new COREEventItem("REVERSAL_OF", sREVERSAL_OF));
        }

        arCOREEvents[0].COREEvent[0].COREEventItem = arrCoreEventItems.ToArray(typeof(COREEventItem)) as COREEventItem[];

        if (ureq != null)
        {
            ureq.COREFile[0].COREFileItem = arrCoreFileItems.ToArray(typeof(COREFileItem)) as COREFileItem[];
            ureq.COREFile[0].COREEvents = arCOREEvents;
        }
        else
        {
            vreq.COREFile[0].COREFileItem = arrCoreFileItems.ToArray(typeof(COREFileItem)) as COREFileItem[];
            vreq.COREFile[0].COREEvents = arCOREEvents;
        }
    }

    /// <summary>
    /// Set the request type into the request. UMN: refactor to hide boring detail
    /// </summary>
    /// <param name="oSystemInterface"></param>
    /// <returns></returns>
    public static ArrayList SetRequestType(GenericObject oSystemInterface)
    {
      ArrayList arrSystemInterfaceItems = new ArrayList();
      SystemInterfaceItem systemInterfaceItem = new SystemInterfaceItem();
      systemInterfaceItem.name = "REQUESTTYPE";
      systemInterfaceItem.value = oSystemInterface.get("request_type", "") as string;
      arrSystemInterfaceItems.Add(systemInterfaceItem);
      return arrSystemInterfaceItems;
    }

    /// <summary>
    /// Sets up the system interfaces and optionally the core file.
    /// UMN: refactored because cloning. Is shared by update, unupdate and inquiry.
    /// </summary>
    /// <param name="oSystemInterface"> The system interface for this update </param>
    public static SystemInterfaces SetSystemInterfaces(GenericObject oSystemInterface)
    {
      SystemInterfaces sysints = new SystemInterfaces();
      sysints.SystemInterface = new SystemInterface[1];
      sysints.SystemInterface[0] = new SystemInterface();
      sysints.SystemInterface[0].ID = oSystemInterface.get("_name", "") as string;
      sysints.SystemInterface[0].Description = oSystemInterface.get("description", "") as string;
      sysints.SystemInterface[0].Type = "Web Service";
      sysints.SystemInterface[0].DatabaseType = oSystemInterface.get("db_type", "") as string;
      sysints.SystemInterface[0].DatasourceName = oSystemInterface.get("datasource_name", "") as string;
      sysints.SystemInterface[0].DatabaseName = oSystemInterface.get("database_name", "") as string;
      sysints.SystemInterface[0].OracleTns = oSystemInterface.get("tns", "") as string;
      sysints.SystemInterface[0].LoginName = oSystemInterface.get("login_name", "") as string;
      string sLoginPasswordEncrypted = oSystemInterface.get("login_password", "") as string;
      if (sLoginPasswordEncrypted != "")
      {
        byte[] bLoginPasswordEncrypted = Convert.FromBase64String(sLoginPasswordEncrypted);
        byte[] bLoginPasswordDecrypted = Crypto.symmetric_decrypt("data", bLoginPasswordEncrypted, "secret_key", Crypto.get_secret_key());
        string sLoginPasswordDecrypted = System.Text.Encoding.Default.GetString(bLoginPasswordDecrypted);
        sysints.SystemInterface[0].LoginPassword = sLoginPasswordDecrypted;
      }
      else
      {
        sysints.SystemInterface[0].LoginPassword = "";
      }
      return sysints;
    }

    /// <summary>
    /// Set Corefile info for request. Is shared by update and void.
    /// </summary>
    /// <returns></returns>
    public static COREFile[] SetCoreFile()
    {
      COREFile[] corefile = new COREFile[1];
      corefile[0] = new COREFile();
      // Bug 22984 NK/Bug 22764 UMN be consistent with format of date because Burnaby has an issue where the core file
      // date is displayed differently in production because they are deployed in Canada, and use en-ca versus 
      // our en-us culture.
      corefile[0].POSTDT = DateTime.Now.ToString("MM/dd/yyyy", DateTimeFormatInfo.InvariantInfo);
      return corefile;
    }

    /// <summary>
    /// Bug 14670 - Now used only for exceptional masking than the normal masking done in MaskTags.
    /// </summary>
    /// <param name="oField"></param>
    /// <returns></returns>
    public static void MaskFieldsWNameAndValue(Object oField)
    {
      if (oField is TenderItem)
      {
        TenderItem oTenderField = (TenderItem)oField;
        // mask bank_account_number with last 4 digits e.g. *****1234
        // Bug 15264 MJO - Don't try to mask if the account number is of length 4 or less
        // Bug 15834 UMN - also mask bank_account_nbr2
        if ((oTenderField.name == "bank_account_nbr" || oTenderField.name == "bank_account_nbr2")
            && !String.IsNullOrEmpty(oTenderField.value) && oTenderField.value.Length > 4)
        {
            //BUG20110 mask bank_account_number with last 4 digits e.g. *****1234
            string sFirstDigits = oTenderField.value.Substring(oTenderField.value.Length - 4, 4);
          oTenderField.value = sFirstDigits.PadLeft(oTenderField.value.Length, '*');
        }
      }
      return;
    }

    /// <summary>
    /// Return a parsed date and time from the passed in value. Bug 14670
    /// </summary>
    /// <param name="sDateTime"></param>
    /// <returns></returns>
    public static string toDateTimeExternalFormat(string sDateTime)
    {
      if (sDateTime == "")
      {
        return "";
      }
      else
      {
        return misc.Parse<DateTime>(sDateTime).ToString("MM/dd/yyyy HH:mm:ss", DateTimeFormatInfo.InvariantInfo);
      }
    }

    /// <summary>
    /// Log Web Service requests or responses. UMN: refactored because cloning.
    /// Now realtime and inquiry reuse the same call.
    /// </summary>
    /// <param name="oObjectToLog"></param>
    /// <param name="sType"></param>
    /// <param name="sMaskedTagNamesInLog"></param>
    /// <returns></returns>
    public static void LogObjXml(GenericObject oSystemInterface, Object oObjectToLog, string sType)
    {
      string sIsLog = oSystemInterface.get("is_log", "N") as string;
      string sSIname = oSystemInterface.get("_name", "PB") as string;
      string sLogPrefix = sSIname;

      if (sIsLog == "N")
      {
        return;
      }
      string sMaskedTagNamesInLog = oSystemInterface.get("masked_xml_tags_in_log", "") as string; // Bug 14666 default as empty string

      XmlSerializer oSerializerForLog = null;
      switch (sType)
      {
        case "REQUEST":
          sLogPrefix += " Inquiry " + sType;
          oSerializerForLog = new XmlSerializer(typeof(InquiryRequest));
          break;
        case "RESPONSE":
          sLogPrefix += " Inquiry " + sType;
          oSerializerForLog = new XmlSerializer(oObjectToLog.GetType());  //Bug 27072 NAM: serialize object based type. i.e. InquiryResponse2, InquiryResponse3...
          break;
        case "UPDATEREQUEST":
          sLogPrefix += " Update " + sType;
          oSerializerForLog = new XmlSerializer(typeof(UpdateRequest));
          break;
        case "UPDATERESPONSE":
          sLogPrefix += " Update " + sType;
          oSerializerForLog = new XmlSerializer(typeof(UpdateResponse));
          break;
        case "VOIDREQUEST":
          sLogPrefix += " Void " + sType;
          oSerializerForLog = new XmlSerializer(typeof(VoidRequest));
          break;
        case "VOIDRESPONSE":
          sLogPrefix += " Void " + sType;
          oSerializerForLog = new XmlSerializer(typeof(VoidResponse));
          break;
        // IPAY-313 NAM:  add VALIDATEREQUEST and VALIDATERESPONSE cases
        case "VALIDATEREQUEST":
          sLogPrefix += " Validate " + sType;
          oSerializerForLog = new XmlSerializer(typeof(UpdateRequest));
          break;
        case "VALIDATERESPONSE":
          sLogPrefix += " Validate " + sType;
          oSerializerForLog = new XmlSerializer(typeof(ValidateResponse));
          break;
        default:
          return;
      }
      StringWriter oStringWriterForLog = new StringWriter();
      try
      {
        oSerializerForLog.Serialize(oStringWriterForLog, oObjectToLog);
        String sXMLForLog = oStringWriterForLog.ToString();
        // BUG#14666 [Geisinger Bug 15420] mask senstive info in xml log; UMN fix so it works
        List<String> aAdditionTagNamesToMask = sMaskedTagNamesInLog.Split(',').Select(d => d.Trim()).ToList();
        CStandardSystemInterface.MaskTags(ref sXMLForLog, aAdditionTagNamesToMask);
        Logger.LogInfo(sXMLForLog, sLogPrefix);
      }
      catch (Exception ex)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in LogObjXml", ex);

        string sErrorMessage = "Error while logging XML:" + ex.Message;
        Logger.LogError(sErrorMessage, sLogPrefix);
      }
    }


    /// <summary>
    /// Set up transactions/tenders in a CORE Event for the request. Shared by update and void.
    /// </summary>
    /// <param name="pCoreEvent"></param>
    /// <param name="geoTrans"></param>
    /// <param name="geoTenders"></param>
    /// <param name="geoTTAMapping"></param>
    /// <param name="strReceiptReferenceNbr"></param>
    /// <param name="sReqType"></param>
    public static void SetCOREEventInfo(ref COREEvent pCoreEvent,  GenericObject geoCoreEvent,GenericObject geoTrans, GenericObject geoTenders, 
      GenericObject geoTTAMapping, string strReceiptReferenceNbr, string sReqType) // Bug 14661; IPAY-1024 HX: Add geoCoreEvent
    {
     // IPAY-313 NAM:  add VALIDATEREQUEST and VALIDATERESPONSE cases check TTA Mapping - during event validation TTA mapping is empty
     if (geoTTAMapping != null)
     {
      // Bug#12208 QG Add TTA mapping info
      int iTTACount = geoTTAMapping.getIntKeyLength();
      pCoreEvent.TTAMAPPING = new TTA[iTTACount];
      for (int i = 0; i < iTTACount; i++)
      {
        pCoreEvent.TTAMAPPING[i] = new TTA();
        GenericObject geoTTA = geoTTAMapping.vectors[i] as GenericObject;
        pCoreEvent.TTAMAPPING[i].AMOUNT = string.Format("{0:F2}", Decimal.Parse(geoTTA.get("amount", "").ToString())) ;
        pCoreEvent.TTAMAPPING[i].TRANNBR = geoTTA.get("TRANNBR", "").ToString();
        pCoreEvent.TTAMAPPING[i].TNDRNBR = geoTTA.get("TNDRNBR", "").ToString();
      }
     }
      // IPAY-313 NAM: always make sure object is not null before getting a count
      //i.e. There are no tenders during validation
      int iTranCount = 0, iTenderCount = 0; ;
      if (geoTrans != null)
          iTranCount = geoTrans.getIntKeyLength();
      if (geoTenders != null)
          iTenderCount = geoTenders.getIntKeyLength();
      // Add Transaction items
      pCoreEvent.Transactions = new Transactions[1];
      pCoreEvent.Transactions[0] = new Transactions();
      pCoreEvent.Transactions[0].Count = iTranCount.ToString();
      pCoreEvent.Transactions[0].Transaction = new Transaction[iTranCount];
      for (int i = 0; i < iTranCount; i++)
      {
        pCoreEvent.Transactions[0].Transaction[i] = new Transaction();
        GenericObject geoTran = geoTrans.vectors[i] as GenericObject;
        GenericObject geoCustomKeys = geoTran.get("_custom_keys", new GenericObject()) as GenericObject; // Bug#12208 if_missing return empty GEO
        // IPAY-313 NAM: Allocations are now part of Transactions
        GenericObject geoAllocations = geoTran.get("Allocations", new GenericObject()) as GenericObject;

        // Bug#12208 QG Rework Transaction Item creation: Create arraylist to store Transaction Items;
        ArrayList arrTranItems = new ArrayList();
        TransactionItem TranItem;
        string sTRANNBR = geoTran.get("seq_nbr", "").ToString();
        string sTransactionReferenceNbr = "";
        if (strReceiptReferenceNbr != "") sTransactionReferenceNbr = strReceiptReferenceNbr + "-" + sTRANNBR; //13883
        arrTranItems.Add(new TransactionItem("TransactionReferenceNbr", sTransactionReferenceNbr));
        arrTranItems.Add(new TransactionItem("TRANNBR", sTRANNBR));
        arrTranItems.Add(new TransactionItem("TTID", geoTran.get("id", "", true).ToString()));
        arrTranItems.Add(new TransactionItem("TTDESC", geoTran.get("description", "").ToString()));
        //IPAY-1672 NAM: add transaction status, validation in PB web service skipped if transaction is 'completed'(i.e. in shopping cart).
        GenericObject geoStatus = geoTran.get("status", new GenericObject()) as GenericObject;
        arrTranItems.Add(new TransactionItem("status", geoStatus.to_path().ToString().Substring(geoStatus.to_path().ToString().LastIndexOf(".") + 1)));
      // Add POSTDT  BUG#14670 
      // Bug 15834 UMN implement hack around BC not having a POSTDT
      string sPOSTDT = ((GenericObject)geoTran.get("created_stamp", new GenericObject())).get("datetime", "").ToString();
        if (String.IsNullOrEmpty(sPOSTDT))
        {
          sPOSTDT = toDateTimeExternalFormat(DateTime.Now.ToString());
          arrTranItems.Add(new TransactionItem("POSTDT", sPOSTDT));
        }
        else
        {
          arrTranItems.Add(new TransactionItem("POSTDT", 
            toDateTimeExternalFormat(((GenericObject)geoTran.get("created_stamp", new GenericObject())).get("datetime", "").ToString())));
        }
        // Add VOIDDT, VOIDUSERID BUG#14670
        if (geoTran.has("void_stamp"))
        {
          arrTranItems.Add(new TransactionItem("VOIDDT",
            toDateTimeExternalFormat(((GenericObject)geoTran.get("void_stamp", new GenericObject())).get("datetime", "").ToString())));
          arrTranItems.Add(new TransactionItem("VOIDUSERID",
            ((GenericObject)geoTran.get("void_stamp", new GenericObject())).get("user", "").ToString()));
        }
        // Add ITEMIND BUG#14670 BUG15993
        string sITEMIND = (geoTran.has("core_items") ? "T" : "S");
        if (misc.base_is_a(geoTran, c_CASL.c_object("Transaction.Tax")))
          sITEMIND = "X";
        arrTranItems.Add(new TransactionItem("ITEMIND", sITEMIND));

        // Add total as standard fields and escape total field from _custom_keys BUG#14758  
        // Bug 23482 MJO - $0 CTTs should always send 0.00
        arrTranItems.Add(new TransactionItem("total", string.Format("{0:F}",
          sITEMIND == "T" ? "0.00" : geoTran.get("total")
        )));

        int iCountCustomFields = geoCustomKeys.getIntKeyLength();

        pCoreEvent.Transactions[0].Transaction[i].TransactionItem = new TransactionItem[iCountCustomFields + 1];

        for (int j = 0; j < iCountCustomFields; j++)
        {
          string strKey = geoCustomKeys.vectors[j].ToString();
          string strValue = "";

          // Bug 23482 MJO - $0 CTTs should always send 0.00
          if (sITEMIND == "T" && strKey == "amount")
          {
            strValue = "0.00";
          }
          else if (geoTran.get(strKey, null) != null)
          {
            object oValue = geoTran.get(strKey);
            if (oValue is GenericObject)
            {
              StringBuilder sb = new StringBuilder();
              foreach (object v in ((GenericObject)oValue).vectors)
              {
                if (!(v is GenericObject))
                {
                  sb.Append(" ");
                  sb.Append(v.ToString());
                }
              }
              strValue = sb.ToString().TrimStart(' ');
            }
            else
              strValue = oValue.ToString();
          }

          if (!string.IsNullOrEmpty(strValue))
          {
            TranItem = new TransactionItem();
            TranItem.name = strKey;
            TranItem.value = strValue;
            MaskFieldsWNameAndValue(TranItem);  // BUG#12208 
            // BUG#14670 ignore allocations 
            if (strKey != "allocations" && strKey != "total")// BUG#14758
            {
              arrTranItems.Add(TranItem);
            }
          }
        }
        pCoreEvent.Transactions[0].Transaction[i].TransactionItem = arrTranItems.ToArray(typeof(TransactionItem)) as TransactionItem[];

        // IPAY-313 NAM: add Allocations to Transaction
        int iCountAllocations = geoAllocations.getIntKeyLength();
        if (iCountAllocations > 0)
        {
            try
            {
                //Allocations has Count and an array of Allocation objects
                int nAlloctions = geoAllocations.vectors.Count;
                pCoreEvent.Transactions[0].Transaction[i].Allocations = new Allocations();
                pCoreEvent.Transactions[0].Transaction[i].Allocations.Count = nAlloctions.ToString();
                pCoreEvent.Transactions[0].Transaction[i].Allocations.Allocation = new Allocation[nAlloctions];

                //add Allocation objects
                for (int j = 0; j < nAlloctions; j++)
                {
                    GenericObject geoAllocationItems = geoAllocations.vectors[j] as GenericObject;
                     //Allocation has Count and an array of AllocationItem objects
                     int nAllocationsItems = geoAllocationItems.Count;
                     pCoreEvent.Transactions[0].Transaction[i].Allocations.Allocation[j] = new Allocation();

                    //NOTE: exclude "_parent" and "_id" otherwise end up with two empty rows 
                    pCoreEvent.Transactions[0].Transaction[i].Allocations.Allocation[j].AllocationItem = new AllocationItem[nAllocationsItems - 2];

                    ArrayList arrKeys = geoAllocationItems.getKeys();
                    ArrayList arrValues = geoAllocationItems.getValues();

                    //add AllocationItem objects
                    int nAllocationItemIndex = 0;
                    string sID = string.Empty;
                    for (int k = 0; k < nAllocationsItems; k++)
                    {
                        string sKey = arrKeys[k].ToString();
                        string sValue = arrValues[k].ToString();

                        if (sKey.Equals("_parent"))     //exclude "_parent" key
                            continue;
                        else if (sKey.Equals("_id"))    //exclude "_is" key - value is stoted in hash table
                        {
                            sID = "COREEvent.0.Transactions.0.Transaction." + i.ToString() + ".Allocations.Allocation." + j.ToString();
                            m_htUniqueID.Add(sID, sValue);
                            continue;
                        }

                        //Add AllocationItem to Allocation
                        if (!string.IsNullOrEmpty(sKey))
                        {
                            AllocationItem allocItem = new AllocationItem();
                            allocItem.name = sKey;
                            allocItem.value = sValue;

                            //add AllocationItem to Allocation
                            pCoreEvent.Transactions[0].Transaction[i].Allocations.Allocation[j].AllocationItem[nAllocationItemIndex++] = allocItem;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                misc.LogEntry("EX_SetCOREEventInfo_Allocations: " + ex.Message);
                throw ex;
            }
        }//if
      }

      pCoreEvent.Tenders = new Tenders[1];
      pCoreEvent.Tenders[0] = new Tenders();
      pCoreEvent.Tenders[0].Count = iTenderCount.ToString();
      pCoreEvent.Tenders[0].Tender = new Tender[iTenderCount];
      for (int i = 0; i < iTenderCount; i++)
      {
        pCoreEvent.Tenders[0].Tender[i] = new Tender();
        GenericObject geoTender = geoTenders.vectors[i] as GenericObject;

        // Bug#12208 QG Rework Tender Item creation: Create arraylist to store Tender Items;
        ArrayList arrTenderItems = new ArrayList();
        TenderItem TdnrItem;
        string sTNDRNBR = geoTender.get("seq_nbr", "").ToString();
        string sTenderReferenceNbr = "";
        if (strReceiptReferenceNbr != "") sTenderReferenceNbr = strReceiptReferenceNbr + "-" + sTNDRNBR; // BUG13883
        arrTenderItems.Add(new TenderItem("TenderReferenceNbr", sTenderReferenceNbr));
        arrTenderItems.Add(new TenderItem("TNDRNBR", sTNDRNBR));
        arrTenderItems.Add(new TenderItem("TNDRID", geoTender.get("id", "", true).ToString()));
        arrTenderItems.Add(new TenderItem("TNDRDESC", geoTender.get("description", "").ToString()));
        // Add TYPEIND BUG#14670
        arrTenderItems.Add(new TenderItem("TYPEIND", geoTender.get("TYPEIND", "", true).ToString()));
        // Add POSTDT  BUG#14670 
        arrTenderItems.Add(new TenderItem("POSTDT", 
          toDateTimeExternalFormat(((GenericObject)geoTender.get("created_stamp", new GenericObject())).get("datetime", "").ToString())));
        // Add VOIDDT, VOIDUSERID BUG#14670
        if (geoTender.has("void_stamp"))
        {
          arrTenderItems.Add(new TenderItem("VOIDDT", 
            toDateTimeExternalFormat(((GenericObject)geoTender.get("void_stamp", new GenericObject())).get("datetime", "").ToString())));
          arrTenderItems.Add(new TenderItem("VOIDUSERID", 
            ((GenericObject)geoTender.get("void_stamp", new GenericObject())).get("user", "").ToString()));
        }
        // Add total as standard fields and escape total field from _custom_keys BUG#14661
        string strTotal = string.Format("{0:F}", geoTender.get("total")); // used below to detect a reversal
        arrTenderItems.Add(new TenderItem("total", strTotal));

        // Bug 14661 UMN on reversal or void, don't want lname to be set to the full name, so remove
        // fname is set to "" and country is already squished into address, so remove them too
        List<String> keysToSkip = new List<String>(roKeysToSkip);
        if ((sReqType == "Real Time Update" && strTotal.StartsWith("-") || sReqType == "Real Time Void"))
        {
          keysToSkip.Add("payer_lname");
          keysToSkip.Add("payer_fname");
          keysToSkip.Add("country");
        }
        // End Bug 14661

        GenericObject geoCustomKeys = geoTender.get("_custom_keys", new GenericObject()) as GenericObject; // Bug#12208 if_missing return empty GEO
        int iCountCustomFields = geoCustomKeys.getIntKeyLength();

        for (int j = 0; j < iCountCustomFields; j++)
        {
          string strKey = geoCustomKeys.vectors[j].ToString();

          // Bug 27093 MJO - Don't send EMV text
          if (strKey == "emv_text")
            continue;

          string strValue = "";
          if (geoTender.get(strKey, null) != null)
          {
            object oValue = geoTender.get(strKey);
            if (oValue is GenericObject)
            {
              StringBuilder sb = new StringBuilder();
              foreach (object v in ((GenericObject)oValue).vectors)
              {
                // BUG#14661 Remove CASL_engine.GenericObject in field value
                // BUG#22101 Upgrade by checking for GenericObject directly.
                if (! (v is GenericObject)) {
                  sb.Append(" ");
                  sb.Append(v.ToString());
		            }
              }
              strValue = sb.ToString();
            }
            else
              strValue = oValue.ToString();
          }

          if (!string.IsNullOrEmpty(strValue))
          {
            TdnrItem = new TenderItem();
            TdnrItem.name = strKey;
            if (strValue.StartsWith("Encrypted") && strKey == "bank_account_nbr") // BUG#14661 use decrypted bank_account_nbr for reversal receipt
            {
              strValue = geoTender.get("bank_account_nbr_decrypted", "") as string; // Bug 14661 ensure strValue is reset
            }
            TdnrItem.value = strValue;  // Bug 14661
            // ignore ccv/cvv, TROUD, exp_date/exp_month/exp_year
            MaskFieldsWNameAndValue(TdnrItem); // BUG#12208 
            if (!keysToSkip.Contains(strKey)        // Bug 14661 UMN fixup above
                && !String.IsNullOrEmpty(strValue)) // Bug 14661 UMN don't serialize blank fields
            {
              arrTenderItems.Add(TdnrItem);
            }
          }
        }
        // add auth_nbr 
        //   BUG#14661 Remove CASL_engine.GenericObject in field value
        //   BUG#22101 Change to check if auth_nbr's value is a GenericObject
        Object obj_sAuthNbr = geoTender.get("auth_nbr", "");
        string sAuthNbr = null;
        if (obj_sAuthNbr is GenericObject)
          sAuthNbr = "";
        else
          sAuthNbr = obj_sAuthNbr.ToString();

        if (sAuthNbr != "")
        {
          TdnrItem = new TenderItem();
          TdnrItem.name = "auth_nbr";
          TdnrItem.value = sAuthNbr;
          MaskFieldsWNameAndValue(TdnrItem); 
          arrTenderItems.Add(TdnrItem);
        }

        pCoreEvent.Tenders[0].Tender[i].TenderItem = arrTenderItems.ToArray(typeof(TenderItem)) as TenderItem[];
      }

      //IPAY-1024 AND  IPAY-1158 HX Set void or Reversal reasons
      GenericObject geoEventNotes = geoCoreEvent.get("event_notes", "") as GenericObject;
      if (geoEventNotes != null)
        pCoreEvent.Notes = SetEventNotes(geoEventNotes);
    }

    // IPAY-1024 and IPAY-1158 HX: Set void or Reversal reasons
    public static Notes SetEventNotes(GenericObject geoEventNotes)
    {
      Notes notes = new Notes();
      notes.Note = new Note[1];
      notes.Note[0] = new Note();

      if (geoEventNotes != null)
      {
        List<NoteItem> lstItem = new List<NoteItem>();

        // Note Type
        NoteItem item = new NoteItem();
        item.name = "TYPE";
        item.value = geoEventNotes.get("NOTETYPE", "") as string;
        lstItem.Add(item);

        // note reason
        item = new NoteItem();
        item.name = "REASON";
        item.value = geoEventNotes.get("VOIDREASON", "") as string;
        lstItem.Add(item);

        // note comment
        item = new NoteItem();
        item.name = "COMMENT";
        item.value = geoEventNotes.get("VOID_COMMENT", "") as string;
        lstItem.Add(item);

        // attachment
        GenericObject attachment = (GenericObject)geoEventNotes.get("ATTACHMENT", c_CASL.c_instance("GEO"));
        string ATTACHMENT_FORMAT = (string)attachment.get("FORMAT", "");
        string ATTACHMENT_CONTENT = "";
        if (attachment.has("CONTENT"))
          ATTACHMENT_CONTENT = Convert.ToBase64String((byte[])attachment.get("CONTENT"));

        if(!string.IsNullOrEmpty(ATTACHMENT_FORMAT) || !string.IsNullOrEmpty(ATTACHMENT_CONTENT))
        {
          // attachment format
          item = new NoteItem();
          item.name = "ATTACHMENT_FORMAT";
          item.value = ATTACHMENT_FORMAT;
          lstItem.Add(item);

          // attachment content
          item = new NoteItem();
          item.name = "ATTACHMENT_CONTENT";
          item.value = ATTACHMENT_CONTENT;
          lstItem.Add(item);
        }

        notes.Note[0].NoteItem = lstItem.ToArray();
      }

      return notes;
    }
  }
}
