﻿/* CORE Business Technologies
 * Texas A&M (TAMU)
 * Nuno Medeiros
 * June 2016
 */
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CASL_engine;
using ProjectBaseline.ServiceReference;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;

namespace ProjectBaseline.ServiceReference
{
  public partial class AllocationItem : object
  {
    public AllocationItem() { }
    public AllocationItem(string name, string value)
    {
      this.name = name;
      this.value = value;
    }
  }
}

namespace ProjectBaseline
{
    partial class CRealtimeUpdate
    {
        /// <summary>
        /// CASLProcess_Validate - bridge between CASL and C# for Core Event PB Validation
        /// </summary>
        /// <param name="argPairs"></param>
        /// <returns></returns>
        public static object CASLProcess_Validate(params object[] argPairs)
        {
            //misc.cs_log("CRealtimeUpdate.CASLProcess_Validate");

            GenericObject args = misc.convert_args(argPairs);
            //get Type and Source of validation requested
            string sType = args.get("validate_type", "") as string;
            string sSource = args.get("validate_source", "") as string;
            GenericObject oSystemInterface = args.get("_subject", "") as GenericObject;
            string strReceiptReferenceNbr = args.get("receipt_ref_nbr", "") as string;
            GenericObject geoCoreFile = args.get("a_core_file", "") as GenericObject;
            GenericObject geoCoreEvent = args.get("a_core_event", "") as GenericObject;
            GenericObject geoTrans = args.get("Transactions", "") as GenericObject;
            GenericObject geoTenders = args.get("Tenders", "") as GenericObject;
            GenericObject geoTTAMapping = args.get("tta_mapping", "") as GenericObject;
            GenericObject geoAllocations = args.get("Allocations", "") as GenericObject;

            return Process_Validate(oSystemInterface, strReceiptReferenceNbr, geoCoreFile, geoCoreEvent, geoTrans, geoTenders, geoTTAMapping, geoAllocations, sType, sSource);
        }
        /// <summary>
        /// Process_Validate - implements PB Standard Validation for Event Objects
        /// </summary>
        /// <param name="oSystemInterface"></param>
        /// <param name="strReceiptReferenceNbr"></param>
        /// <param name="geoCoreFile"></param>
        /// <param name="geoCoreEvent"></param>
        /// <param name="geoTrans"></param>
        /// <param name="geoTenders"></param>
        /// <param name="geoTTAMapping"></param>
        /// <param name="geoAllocations"></param>
        /// <param name="sType"></param>
        /// <param name="sSource"></param>
        /// <returns></returns>
        private static object Process_Validate(GenericObject oSystemInterface,
                                        string strReceiptReferenceNbr,
                                        GenericObject geoCoreFile,
                                        GenericObject geoCoreEvent,
                                        GenericObject geoTrans,
                                        GenericObject geoTenders,
                                        GenericObject geoTTAMapping,
                                        GenericObject geoAllocations,
                                        string sType,
                                        string sSource)
        {
            //misc.cs_log(string.Format("CRealtimeUpdate.Process_Validate: Type={0} Source={1}", sType, sSource));

            string sRequestType = "Real Time Validate";
            ValidateResponse geoResponse = new ValidateResponse();   

            try
            {
                //TTS 18322 NUNO 20160617: always start with an empty Unique ID Hashtable
                if (m_htUniqueID == null)
                    m_htUniqueID = new Hashtable();
                else
                    m_htUniqueID.Clear();

                //use existing Real-time update to set File and CORE Event data
                UpdateRequest geoRequest = new UpdateRequest();
                geoRequest.Type = sRequestType;
                geoRequest.SystemInterfaces = CRealtimeUpdate.SetSystemInterfaces(oSystemInterface);
                geoRequest.COREFile = CRealtimeUpdate.SetCoreFile();

                // Bug 14838 SX 
                CRealtimeUpdate.SetCoreFileItems(oSystemInterface,
                                                    strReceiptReferenceNbr,
                                                    geoCoreFile,
                                                    geoCoreEvent,
                                                    geoRequest, "UPDATEREQUEST");
                CRealtimeUpdate.SetCOREEventInfo(ref geoRequest.COREFile[0].COREEvents[0].COREEvent[0],
                                                    geoTrans,
                                                    geoCoreEvent, // IPAY-1024 IPAY-1058 HX: Added
                                                    geoTenders,
                                                    geoTTAMapping,
                                                    strReceiptReferenceNbr,
                                                    geoRequest.Type);

                //Add top level allocations (not imbeded at the transaction level) empty most of the time
                //Note: This is optional and allows validation of allocations that are not part of an event
                SetCOREEventAllocations(ref geoRequest.COREFile[0].COREEvents[0].COREEvent[0], geoAllocations);

                //TTS 19971 NUNO 20160902: Log xml validate request
                CRealtimeUpdate.LogObjXml(oSystemInterface, geoRequest, "VALIDATEREQUEST");
                
                //call web service
                SSIClient oSSIClient = CStandardSystemInterface.getSSICientFromEndPoint(oSystemInterface);
                geoResponse = oSSIClient.ValidateRequestOperation(geoRequest, sType, sSource);

                //TTS 19971 NUNO 20160902: Log xml validate response
                CRealtimeUpdate.LogObjXml(oSystemInterface, geoResponse, "VALIDATERESPONSE");
            }
            catch (Exception ex)
            {
                Logger.LogError("EX_CRealtimeUpdate.Process_Validate: " + ex.Message, "Process_Validate");
                throw ex;
            }

            return CreateValidateResponse(geoResponse);
        }

        /// <summary>
        /// SetCOREEventAllocations - handles Core Event level Allocations
        /// Allocations typically are set a the transaction level. If none are sent at the core event level this method won't do anything
        /// </summary>
        /// <param name="pCoreEvent"></param>
        /// <param name="geoAllocations"></param>
        static private void SetCOREEventAllocations(ref COREEvent pCoreEvent, GenericObject geoAllocations)
        {
            try
            {
                int nAllocationsCnt = 0;
                if (geoAllocations != null)
                    nAllocationsCnt = geoAllocations.getIntKeyLength();

                pCoreEvent.Allocations = new Allocations[1];
                pCoreEvent.Allocations[0] = new Allocations();
                pCoreEvent.Allocations[0].Count = nAllocationsCnt.ToString();
                pCoreEvent.Allocations[0].Allocation = new Allocation[nAllocationsCnt];

                for (int i = 0; i < nAllocationsCnt; i++)
                {
                    pCoreEvent.Allocations[0].Allocation[i] = new Allocation();
                    GenericObject geoAlloc = geoAllocations.vectors[i] as GenericObject;

                    ArrayList arrAllocationItems = new ArrayList();
                    arrAllocationItems.Add(new AllocationItem("GL_nbr", geoAlloc.get("GL_nbr", "").ToString()));
                    arrAllocationItems.Add(new AllocationItem("GL_desc", geoAlloc.get("GL_desc", "").ToString()));
                    arrAllocationItems.Add(new AllocationItem("amount", geoAlloc.get("amount", "").ToString()));
                    arrAllocationItems.Add(new AllocationItem("quantity", geoAlloc.get("quantity", "").ToString()));

                    pCoreEvent.Allocations[0].Allocation[i].AllocationItem = arrAllocationItems.ToArray(typeof(ProjectBaseline.ServiceReference.AllocationItem)) as ProjectBaseline.ServiceReference.AllocationItem[];
                }
            }
            catch (Exception ex)
            {
                Logger.LogError("EX_CRealtimeUpdate.SetCOREEventAllocations " + ex.Message, "SetCOREEventAllocations");
            }
        }

        /// <summary>
        /// CreateValidateResponse - convert ValidateResponse object from web service into a CASL/GEO type object
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        private static GenericObject CreateValidateResponse(ValidateResponse response)
        {
            GenericObject geoValidateResponse = c_CASL.c_GEO();
            geoValidateResponse.set("_name", "ValidateResponse");

            try
            {
                //get Result
                string sResult = response.Result;
                //misc.cs_log("CreateValidateResponse Result= " + sResult);
               
                geoValidateResponse.set("Result", response.Result.ToString());
                geoValidateResponse.set("UserMessage", response.UserMessage.ToString()); 
                geoValidateResponse.set("LogMessage", response.LogMessage.ToString());

                //Bug 22837 NAM 20180206: if allocations are pre-filled, return an 'empty' response
                if (sResult.ToUpper().Equals("PREFILLED"))
                {
                    GenericObject geoValidations = c_CASL.c_GEO();
                    geoValidations.set("_name", "Validations");
                    geoValidateResponse.insert(geoValidations);
                    return geoValidateResponse;
                }
                else if (sResult.ToUpper().Equals("SUCCESS") && response.Validations != null) //IPAY-931 NAM: check if Response contains Validations
        {
                    //get Validations
                    string sValidations = response.Validations.Count;   //only one expected
                    //misc.cs_log("Validations Cnt= " + sValidations);

                    GenericObject geoValidations = c_CASL.c_GEO();
                    geoValidations.set("_name", "Validations");
                    geoValidateResponse.insert(geoValidations);

                    //get Validation
                    int nValidation = response.Validations.Validation.Count();
                    //misc.cs_log("Validation Cnt= " + nValidation.ToString());

                    //get Updates
                    for (int i = 0; i < nValidation; i++)
                    {
                        if (response.Validations.Validation[i].Updates != null) //IPAY-1229 NAM: not all Validations have Updates
                        { 

                          string sUpdates = response.Validations.Validation[i].Updates.Count;
                          //misc.cs_log("Updates Cnt= " + sUpdates);

                          //create Validation Node
                          Validation validation = response.Validations.Validation[i];

                          //get actual CASL object path stored in hashtable that matches id from web service
                          string sObjPath = validation.ID;
                          int nIndex = sObjPath.IndexOf("COREEvent.");
                          string sHashID = sObjPath.Substring(nIndex);
                          if (m_htUniqueID.ContainsKey(sHashID))
                              sObjPath = (string)m_htUniqueID[sHashID];

                          GenericObject geoValidation = c_CASL.c_GEO();
                          geoValidation.Add("Type", validation.Type);
                          geoValidation.Add("ID", sObjPath);
                          geoValidation.set("_name", "Validation");
                          geoValidations.insert(geoValidation);

                          GenericObject geoUpdates = c_CASL.c_GEO();
                          geoUpdates.set("_name", "Updates");
                          geoValidation.insert(geoUpdates);

                          for (int j = 0; j < Convert.ToInt32(sUpdates); j++)
                          {
                              //create an instance of Update object
                              Update update = response.Validations.Validation[i].Updates.Update[j];
                              string sUpdate = string.Format("ID:{0} Name:{1} Operation:{2} Type:{3} Value;{4}", update.ID, update.Name, update.Operation, update.Type, update.Value);
                              //misc.cs_log("Update: " + sUpdate);

                              //create GEO to hold individual Update
                              GenericObject geoUpdate = c_CASL.c_GEO();
                              geoUpdate.set("_name", "Update");

                              geoUpdate.Add("ID", update.ID);
                              geoUpdate.Add("Name", update.Name);
                              geoUpdate.Add("Operation", update.Operation);
                              geoUpdate.Add("Type", update.Type);
                              geoUpdate.Add("Value", update.Value);

                              geoUpdates.insert(geoUpdate);
                          }
                        }
                    }
                }
                else if (sResult.ToUpper().Equals("FAILURE") && response.Validations != null) //IPAY-931 NAM: check if Response contains Validations
        {
                    //get Validations
                    string sValidations = response.Validations.Count;
                    //misc.cs_log("ValidationsCnt= " + sValidations);

                    GenericObject geoValidations = c_CASL.c_GEO();
                    geoValidations.set("_name", "Validations");
                    geoValidateResponse.insert(geoValidations);

                    //get Validation
                    int nValidation = response.Validations.Validation.Count();
                    //misc.cs_log("ValidationCnt= " + nValidation.ToString());

                    //get Errors
                    for (int i = 0; i < nValidation; i++)
                    {
                        if (response.Validations.Validation[i].Errors != null)  //IPAY-1229 NAM: not all Validations have Errors
            { 
                          string sErrors = response.Validations.Validation[i].Errors.Count;
                          //misc.cs_log("ErrorsCnt= " + sErrors);

                          //create Validation Node
                          Validation validation = response.Validations.Validation[i];

                          //get actual CASL object path stored in hashtable that matches id from web service
                          string sObjPath = validation.ID;
                          int nIndex = sObjPath.IndexOf("COREEvent.");
                          string sHashID = sObjPath.Substring(nIndex);
                          if (m_htUniqueID.ContainsKey(sHashID))
                          {
                              sObjPath = (string)m_htUniqueID[sHashID];
                          }

                          GenericObject geoValidation = c_CASL.c_GEO();
                          geoValidation.Add("Type", validation.Type);
                          geoValidation.Add("ID", sObjPath);

                          geoValidation.set("_name", "Validation");
                          geoValidations.insert(geoValidation);

                          GenericObject geoErrors = c_CASL.c_GEO();
                          geoErrors.set("_name", "Errors");
                          geoValidation.insert(geoErrors);

                          for (int j = 0; j < Convert.ToInt32(sErrors); j++)
                          {
                              Error error = response.Validations.Validation[i].Errors.Error[j];
                              string sUpdate = string.Format("ID:{0} Name:{1} Operation:{2} Type:{3} Value;{4}", error.ID, error.Name, error.Operation, error.Type, error.Value);
                              //misc.cs_log(sUpdate);

                              GenericObject geoError = c_CASL.c_GEO();
                              geoError.set("_name", "Error");

                              geoError.Add("ID", error.ID);
                              geoError.Add("Name", error.Name);
                              geoError.Add("Operation", error.Operation);
                              geoError.Add("Type", error.Type);
                              geoError.Add("Value", error.Value);

                              geoErrors.insert(geoError);
                          }
                        }
                    }
                }     
                //empty out hashtable
                m_htUniqueID.Clear();
            }
            catch (Exception ex)
            {
                Logger.LogError("EX_CRealtimeUpdate.CreateValidateResponse " + ex.Message, "CreateValidateResponse");
            }

            return geoValidateResponse;
        }
    }
}
