// Bug 20642 UMN - new responsive UI
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Concurrent;
using CASL_engine;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using Fluid;        //Bug 22708 NAM
using Fluid.Values; //Bug 22708 NAM
using Newtonsoft.Json;
using CsQuery;
using TidyManaged;

namespace ProjectBaseline
{
  //Bug 22708 NAM - removed

  /// <summary>
  /// TemplateLoader loads all the templates in the template directory and uses the DotLiquid engine
  /// to parse the templates. It then stores the resulting parsed templates in a Dictionary so that
  /// ResponsiveUI.ReturnResponsiveUI can retrieve the parsed template given the template name.
  /// TemplateLoader is a singleton.
  /// </summary>
  public sealed class TemplateLoader
  {
    private TemplateLoader() 
    {
      //Bug 22708 NAM: register Fluid money filter
      TemplateContext.GlobalFilters.AddFilter("money",MoneyFilter);
    }
    public static FluidValue MoneyFilter(FluidValue input, FilterArguments arguments, TemplateContext context)
    {
      Decimal dValue = 0;
      var sInput= input.ToStringValue().Replace("$", "");
      if (Decimal.TryParse(sInput, out dValue))
          return new StringValue(String.Format("{0:C}", dValue));
      return new StringValue(sInput);
    }

    // Explicit static constructor to tell C# compiler
    // not to mark type as beforefieldinit
    static TemplateLoader() {}

    private static readonly TemplateLoader _instance = new TemplateLoader();

    // Bug 22376 UMN now load all files found in directory path, not specific list
    private static List<string> templates = new List<string>();

    //Bug 22708 NAM - using FluidTemplate engine
    private static readonly ConcurrentDictionary<String, FluidTemplate> _parsedTemplates = new ConcurrentDictionary<String, FluidTemplate>(); 

    public static TemplateLoader Instance { get { return _instance; } }

    /// <summary>
    /// If nothing in dict, assume templates aren't loaded.
    /// </summary>
    /// <returns></returns>
    public bool AreTemplatesLoaded() { return _parsedTemplates.Count == 0 ? false : true; }

    /// <summary>
    /// Return the template given its key (the name of the template file).
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public FluidTemplate GetPreLoadedTemplate(string key)  //Bug 22708 NAM
    {
      return _parsedTemplates[key];
    }

    /// <summary>
    /// Get the template named by the key (template name). Will get it from the preloaded template
    /// cache, or by loading it directly from the file system.
    /// </summary>
    /// <param name="key">The name of the template</param>
    /// <returns></returns>
    public FluidTemplate GetTemplate(string key) //Bug 22708 NAM
    {
      System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
      TimeSpan ts;

      FluidTemplate page_template = null; //Bug 22708 NAM
      if ((bool)((GenericObject)c_CASL.c_object("Business.Business_center_settings.data")).get("cbc_cache_templates", false))
      {
        try
        {
          page_template = ProjectBaseline.TemplateLoader.Instance.GetPreLoadedTemplate(key);
          
          // Bug 26346 UMN log an error if template is null in regular log
          if (page_template == null)
          {
            Logger.LogError($"Template for {key} is null!", "GetTemplate");
          }
          return page_template;
        }
        catch
        {
        }
      }

      watch.Start();
      page_template = ProjectBaseline.TemplateLoader.Instance.LoadNamedTemplate(key);
      watch.Stop();
      ts = watch.Elapsed;
      if ((bool)((GenericObject)c_CASL.c_object("Business.Business_center_settings.data")).get("cbc_time_responsiveUI", false))
        Logger.cs_log(string.Format(" Template {0} parse took {1} seconds.", key, watch.ElapsedMilliseconds / 1000.00));

      return page_template;
    }

    /// <summary>
    /// Load all the .liquid templates and retain the parsed templates.
    /// </summary>
    public void LoadTemplates()
    {
      string path = "/business/include/";
      LoadTemplatesFromFilePath(path);
    }

    /// <summary>
    /// Load and return a named template.
    /// </summary>
    /// <param name="templateName">Full name of template file to load</param>
    public FluidTemplate LoadNamedTemplate(string templateName)  //Bug 22708 NAM
    {
      string path = "/business/include";
      string physicalPath = c_CASL.GEO.get("baseline_physical") + path;
      path = c_CASL.GEO.get("baseline_static_script") + path;
      return LoadTemplateFromFilePath(path, physicalPath, templateName);
    }

    /// <summary>
    /// Load the templates using a site relative file path. It will be quicker than loading the templates using
    /// LoadTemplatesFromWebPath.
    /// </summary>
    /// <param name="path">Site-relative path to the templates</param>
    private static void LoadTemplatesFromFilePath(string path)
    {
      string physicalPath = c_CASL.GEO.get("baseline_physical") + path;
      path = c_CASL.GEO.get("baseline_static_script") + path;

      // Bug 22376 UMN support custom templates in business/include path
      templates = new List<string>(Directory.GetFiles(physicalPath + "templates/", "*.liquid"));  //Bug 22708 NAM

      foreach (string templateName in templates)
      {
        FluidTemplate template = new FluidTemplate();
        template= LoadTemplateFromFilePath(path, physicalPath, templateName); //Bug 22708 NAM
        //Bug 22708 NAM: extract key from templateName and insert parsed template into ConcurrentDictionary
        string sTemplateKey = templateName.Substring(templateName.LastIndexOf("/") + 1);
        _parsedTemplates[sTemplateKey] = template;
      }
    }

    /// <summary>
    /// Loaad an individual template from a file path
    /// </summary>
    /// <param name="path">A site-specific path that will be used to replace any $path variables in a template</param>
    /// <param name="physicalPath"></param>
    /// <param name="templateName"></param>
    /// <returns></returns>
    private static FluidTemplate LoadTemplateFromFilePath(string path, string physicalPath, string templateName) //Bug 22708 NAM
    {
      // Bug 19702 UMN cache tfile templates into strings
      string templateStr = "";
      //Bug 22708 NAM 20180426: Using the Fluid template engine
      FluidTemplate return_template = null;
      try
      {
        var filePath = Path.Combine(physicalPath, "templates", templateName);
        if ((bool)((GenericObject)c_CASL.c_object("Business.Business_center_settings.data")).get("cbc_log_responsiveUI", false))
          Logger.cs_log(" Loading: " + filePath);

        templateStr = File.ReadAllText(filePath).Replace("$path", path);
        if (FluidTemplate.TryParse(templateStr, out var template, out var errors))  //Bug 22708 NAM
        { 
          return_template = template;
        }
        else   // TryParse failed - log the errors
        {
          string errMsg = "";
          foreach (var msg in errors)
            errMsg = String.Join(";", msg);
          Logger.cs_log($"Fluid Parse errors in {templateName}: {errMsg}");
        }
      }
      catch (Exception ex)
      {
        Logger.cs_log_trace("EX_LoadTemplateFromFilePath", ex);
      }
      
      // Bug 26346 UMN log an error if template is null in regular log
      if (return_template == null)
      {
        Logger.LogError($"Template for {templateName} is null!", "LoadTemplateFromFilePath");
      }
      
      return return_template;
    }
  }
}