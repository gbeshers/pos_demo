﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CASL_engine;
using ProjectBaseline.ServiceReference;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Security;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Globalization; // Bug 14670 
using System.Threading; // Bug 15206 UMN
using Fluid;                // Bug 22708 NAM
using Newtonsoft.Json;      // Bug 18719 UMN
using CsQuery;                         // Bug 20642 UMN Responsive UI
using System.Text.RegularExpressions;  // Bug 20642 UMN Responsive UI
using System.Web;                      // Bug 20642 UMN Responsive UI
using KVPair = System.Collections.Generic.KeyValuePair<string, string>; //Bug 23623 NAM

// UMN: TODO 1/31/2014. The flow on inquiries is kind of weird. We take GEOs and turn them into XML and then return
// XML from the standard Web service, and then make GEOs, which are then turned into JSON that is used to create
// datatables. Why not cut out the middleman (GEOs) on the inquiry response, and move page generation into C#?
// Moreover, we could add JSON endpoints that could optionally be used to cut out XML for a given customer.

namespace ProjectBaseline
{
  // Bug 20642 UMN add this for some new casl functions that will be used
  public class ResponsiveUI
  {
    static readonly Regex trimmer = new Regex(@"\s\s+", RegexOptions.Compiled);

    /// <summary>
    /// Sanitize html by stripping it out
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public static String StripHTML(string input)
    {
      // Bug 26894 UMN try to preserve spaces coming from BRs and nbsps
      input = HttpUtility.HtmlDecode(input).Replace("<br/>", " ").Replace("<BR/>", " ").Replace("&nbsp;", " ");
      string stripped = new CQ("<div/>").Html(input).Text();
      return trimmer.Replace(stripped, " ");
    }

    /// <summary>
    /// Add function callable from CASL. Need it to handle stupid prompts.
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static String CASL_strip_html(params Object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      String input = args.get("_subject") as String;
      return StripHTML(input);
    }

    /// <summary>
    /// Write a JSON representation of the C# object
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="prefix"></param>
    public static void WriteJSONToLogDir(object obj, string prefix)
    {
      //IPAY-1096 NAM: Don't create log file when show_verbose_error is turned off
      if ((!(bool)c_CASL.GEO.get("show_verbose_error", false)) || (!(bool)((GenericObject)c_CASL.c_object("Business.Business_center_settings.data")).get("cbc_log_responsiveUI", false)))
        return;

      string fileName = String.Format("{0}/{1}/{2}", c_CASL.GEO.get("site_physical"), "log", prefix);
      string JSONtext = "";
      TextWriter jsonWriter = TextWriter.Synchronized(File.CreateText(fileName + ".json.txt"));
      try
      {
        if (obj != null)
        {
          try
          {
            JSONtext = Newtonsoft.Json.JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.Indented, new JsonSerializerSettings()
            {
              ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
          }
          catch (Exception e)
          {
            // Bug 17849
            Logger.cs_log_trace("Error in  WriteJSONToLogDir", e);
            JSONtext = e.Message + e.StackTrace + Environment.NewLine;
          }
        }

        // output JSON or error message
        jsonWriter.Write(JSONtext + Environment.NewLine);
        jsonWriter.Close(); jsonWriter = null;
      }
      finally
      {
        if (jsonWriter != null) jsonWriter.Close();
      }
    }
  }

  public class CStandardSystemInterface
  {
    static bool m_bRegisterAccessStrategy = false;  //Bug 23683 NAM
    /// <summary>
    /// Bug 14666 [Geisinger Bug 15420] define standard sensitive xml tags (mask w #)
    /// exclude already masked value credit_card_nbr (masks w last four *), 
    /// bank_account_nbr (masks with first four *)
    /// UMN: for perf, staticly construct List.
    /// </summary>
    private static readonly List<String> aStandardSensitiveInfoTags = new List<String>
    {
      "bank_routing_nbr",
      "ccv",
      "credit_card_nbr_en", // Bug 15297 UMN
      "cvv",
      "debit_pin",
      "exp_date",
      "exp_month",
      "exp_year",
      "LoginName",
      "LoginPassword",
      "track_data"
    };

    /// <summary>
    /// A list of testable exceptions that can be thrown using the exception_thrown custom
    /// field when it is added to an inquiry page. Anything not in this list is thrown
    /// as a base class exception. So entering barf, would throw an Exception("barf") instance.
    /// Bug 15206 UMN
    /// </summary>
    private static readonly Dictionary<String, Type> testableExceptions = new Dictionary<String, Type>
    {
      // retry-able, will keep raising these until retry count is reached
      { "timeoutexception", typeof(TimeoutException) },
      { "channelterminatedexception", typeof(ChannelTerminatedException) },
      { "endpointnotfoundexception", typeof(EndpointNotFoundException) },
      { "servertoobusyexception", typeof(ServerTooBusyException) },
      { "messagesecurityexception", typeof(MessageSecurityException) },
      { "securitynegotiationexception", typeof(SecurityNegotiationException) },

      // Not retry-able, will immediately return these
      { "faultexception", typeof(FaultException) },
      { "communicationexception", typeof(CommunicationException) },
      { "argumentnullexception", typeof(ArgumentNullException) },
      { "invalidoperationexception", typeof(InvalidOperationException) },
      { "quotaexceededexception", typeof(QuotaExceededException) },
      { "objectdisposedexception", typeof(ObjectDisposedException) },
      { "communicationobjectfaultedexception", typeof(CommunicationObjectFaultedException) },
      { "communicationobjectabortedexception", typeof(CommunicationObjectAbortedException) }
    };

    public CStandardSystemInterface()
    {
    }

    #region CASL interface
    /// <summary>
    /// Called by CASL on an inquiry
    /// </summary>
    /// <param name="argPairs"></param>
    /// <returns></returns>
    public static object CASLDoInquiry(params object[] argPairs)
    {
      GenericObject args = misc.convert_args(argPairs);
      GenericObject oSystemInterface = args.get("_subject") as GenericObject;
      GenericObject oQueryKeys = args.get("oQueryKeys") as GenericObject;
      GenericObject oQueryInfo = args.get("oQueryInfo") as GenericObject;
      GenericObject oTran = args.get("oTran") as GenericObject; // BUG#14719 
      return DoStandardInquiry(oSystemInterface, oQueryKeys, oQueryInfo, oTran);
    }

    /// <summary>
    /// Called by CASL on an detail group inquiry. Bug 18719 UMN 
    /// </summary>
    /// <param name="argPairs"></param>
    /// <returns>HTML as rendered through a template</returns>
    public static string CASLDoDetailInquiry(params object[] argPairs)
    {
      GenericObject args = misc.convert_args(argPairs);
      string sGroupName = args.get("group_name") as string;
      string sCustomFieldID = args.get("cbt_id") as string;
      //Bug 23569 NAM: Master/Detail
      string sDetGroupInqURL = args.get("det_group_inq_url") as string;
      GenericObject oTopApp = args.get("top_app") as GenericObject;

      // generate a button with background image for pos. For responsiveUI, we generate a button that looks like a text link
      bool is_pos = false;
      if (misc.base_is_a(oTopApp, c_CASL.c_object("Business.pos"))) //Bug 23683 NAM:
      {
        is_pos = true;
      }

      // Bug 23569 NAM/20642 UMN responsive UI. Need to know if outer or inner details for Banner
      string level = args.get("level") as string;

      return DoDetailInquiry(sGroupName, sCustomFieldID, sDetGroupInqURL, is_pos, level);
    }

    /// <summary>
    /// Test WS connection by submitting an InquiryRequest2Operation. Used by Config to
    /// validate the WS connection. Caution! We will need to ensure that customers using their own Web service
    /// (like NYC) implements InquiryRequest2Operation.
    /// Bug 15512 UMN 
    /// UMN - modified 1/28/15 to use Heartbeat request parameters and log the response
    /// </summary>
    /// <param name="argPairs"></param>
    /// <returns></returns>
    public static object CASL_IsWSConnOK(params object[] argPairs)
    {
      GenericObject args = misc.convert_args(argPairs);
      GenericObject oSystemInterface = args.get("_subject") as GenericObject;
      return IsWSConnOK(oSystemInterface);
    }

    /// <summary>
    /// Invokes the Web service as either a test or to pre-warm the WCF cache.
    /// Moved into a new method to allow call from C#. Bug 15205 UMN.
    /// </summary>
    /// <param name="oSystemInterface"></param>
    /// <returns></returns>
    public static bool IsWSConnOK(GenericObject oSystemInterface)
    {
      bool itsAlive = true;
      string excToThrow = null;
      try
      {
        // Bug 14781 Support both http and https 
        SSIClient oSSIClient = CStandardSystemInterface.getSSICientFromEndPoint(oSystemInterface);
        InquiryRequest req = new InquiryRequest();

        // Top Level Request Fields
        req.UserID = "Admiral Narsu";
        req.WorkgroupID = "Starbase 12";
        req.TransType = "";
        req.QuerySourcePage = "HeartbeatCheck";
        req.CoreFileEffDT = "";
        req.QueryKeys = new QueryKeys();
        req.QueryKeys.QueryKey = new QueryKey[1];
        req.QueryKeys.QueryKey[0] = new QueryKey();
        req.QueryKeys.QueryKey[0].name = "CORE_Heartbeat";
        req.QueryKeys.QueryKey[0].value = "";

        CRealtimeUpdate.LogObjXml(oSystemInterface, req, "REQUEST");    //BUG 27072 NAM: Log Request
        InquiryResponse2 oWSInquiryResponse = CallWebService<InquiryResponse2, InquiryRequest>(oSystemInterface,
          ref oSSIClient, oSSIClient.InquiryRequest2Operation, req, excToThrow);

        //Bug 27072 NAM: Log Response - Web service response type passed in oWSInquiryResponse
        CRealtimeUpdate.LogObjXml(oSystemInterface, oWSInquiryResponse, "RESPONSE");
      }
      catch (Exception ex)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in IsWSConnOK", ex);
        itsAlive = false;
      }
      return itsAlive;
    }
    #endregion CASL interface

    /// <summary>
    /// This performs the standard inquiry
    /// </summary>
    /// <param name="oSystemInterface"></param>
    /// <param name="oQueryKeys"> Dictionary of query keys </param>
    /// <param name="oQueryInfo"> Dictionary of other info for the Inquiry </param>
    /// <returns></returns>
    public static object DoStandardInquiry(GenericObject oSystemInterface, GenericObject oQueryKeys, GenericObject oQueryInfo, GenericObject oTran)
    {
      string reqType = "Inquiry";
      string excToThrow = null;
      System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
      bool logTimings = (bool)oSystemInterface.get("log_timing_data", false); // Bug 15206 make timings configurable
      if (logTimings)
      {
        // Bug 15206 UMN add timing support
        watch.Start();
      }

      try
      {
        // Bug 14781 Support both http and https 
        SSIClient oSSIClient = CStandardSystemInterface.getSSICientFromEndPoint(oSystemInterface);

        InquiryRequest req = new InquiryRequest();
        // Top Level Request Fields
        req.UserID = oQueryInfo.get("UserID", "") as string;
        req.WorkgroupID = oQueryInfo.get("WorkgroupID", "") as string;
        req.TransType = oQueryInfo.get("TransType", "") as string;
        req.QuerySourcePage = oQueryInfo.get("QuerySourcePage", "InquiryPage") as string;
        req.CoreFileEffDT = oQueryInfo.get("CoreFileEffDT", "") as string; // Bug 13117 

#if true
        req.SystemInterfaces = CRealtimeUpdate.SetSystemInterfaces(oSystemInterface);
#else
        // George says ALL system interfaces should be output. Need to experiment with this.
        GenericObject allSysInts = oQueryInfo.get("AllSysInts") as GenericObject; // it's a vector 
        req.SystemInterfaces = CStandardSystemInterface.SetAllSystemInterfaces(oSystemInterface, allSysInts);
#endif

        // Add REQUESTTYPE 
        ArrayList arrSystemInterfaceItems = CRealtimeUpdate.SetRequestType(oSystemInterface);
        req.SystemInterfaces.SystemInterface[0].SystemInterfaceItem = arrSystemInterfaceItems.ToArray(typeof(SystemInterfaceItem)) as SystemInterfaceItem[];

        // QueryKeys  
        SetQueryKeys(oQueryKeys, ref req, ref excToThrow);

        InquiryResponse3 oWSInquiryResponse = new InquiryResponse3();

        // Bug 12384 Log Web Service Request XML based on System Interface Configuration Flag "is_log" 
        CRealtimeUpdate.LogObjXml(oSystemInterface, req, "REQUEST");

        // Bug 15206 UMN - simulate failure response
        if (excToThrow == "failureresponse")
        {
          oWSInquiryResponse.Type = "Failure";
          oWSInquiryResponse.ErrorCode = CRealtimeUpdate.mockErrorCode;
          oWSInquiryResponse.ErrorDetail = CRealtimeUpdate.mockErrorDetail;
          oWSInquiryResponse.ErrorSummary = CRealtimeUpdate.mockErrorSummary;
        }
        else
        {
          oWSInquiryResponse = CallWebService<InquiryResponse3, InquiryRequest>(oSystemInterface,
            ref oSSIClient, oSSIClient.InquiryRequest3Operation, req, excToThrow);

          // Bug 18719 UMN Filter out and cache detail groups for detail inquiries 
          if (oWSInquiryResponse.Type != "Failure") CacheDetailGroups(ref oWSInquiryResponse);
        }

        CRealtimeUpdate.LogObjXml(oSystemInterface, oWSInquiryResponse, "RESPONSE");

        return CreateInquiryResponse(oWSInquiryResponse, oTran);
      }
      catch (System.ServiceModel.FaultException ex) // Bug 13831 
      {
        // Bug 17849
        Logger.cs_log_trace("Error in DoStandardInquiry", ex);
        // Bug 19806 UMN
        if (ex.InnerException != null)
          Logger.cs_log(ex.InnerException.StackTrace);
        else
          Logger.cs_log(ex.StackTrace);
        MessageFault fault = ex.CreateMessageFault();
        if (fault.HasDetail)
          Logger.cs_log(fault.GetReaderAtDetailContents().Value);
        return CRealtimeUpdate.GetCASLError("PB-113", reqType, ex); // possible incompatible web service configuration
      }
      catch (Exception ex)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in DoStandardInquiry", ex);

        // Bug 19806 UMN
        if (ex.InnerException != null)
          Logger.cs_log(ex.InnerException.StackTrace);
        else
          Logger.cs_log(ex.StackTrace);
        return CRealtimeUpdate.GetCASLError("PB-107", reqType, ex);  // generic error occurred
      }
      finally
      {
        if (logTimings)
        {
          watch.Stop();
          TimeSpan ts = watch.Elapsed;
          Logger.cs_log(string.Format("{0} took {1} seconds.", reqType, watch.ElapsedMilliseconds / 1000.00));
        }
      }
    }

    /// <summary>
    /// Responsible for creating the inquiry response, depending on what type of response
    /// comes back from the standard Web service.
    /// </summary>
    /// <param name="oWSInquiryResponse"></param>
    /// <returns>A GEO that is ultimately passed back to CASL </returns>
    private static GenericObject CreateInquiryResponse(InquiryResponse3 oWSInquiryResponse, GenericObject oTran)
    {
      GenericObject oR = c_CASL.c_GEO();
      oR.set("_name", "InquiryResponse");
      //Bug 26510 NAM: detect Type in Inquiry Response
      if (string.IsNullOrEmpty(oWSInquiryResponse.Type))
      {
        CreateFailureResponseGEO(oR, oTran, "Failure", "WS-001", "Invalid response type", "Response type was not present or recognized");
        Logger.cs_log("Invalid inquiry response type: " + oWSInquiryResponse.ToString());
        return oR;
      }
      oR.set("Type", oWSInquiryResponse.Type);
      string sInquiryResponseType = oWSInquiryResponse.Type.ToUpper();
      // GeneralData: ignore for current PB Standard solution 

      switch (sInquiryResponseType)
      {
        case "SINGLEENTITYMATCH":
          //Bug 26510 NAM: detect GeneralData and/or DetailData in Inquiry Response
          if (oWSInquiryResponse.GeneralData is null && oWSInquiryResponse.DetailData is null)
          {
            CreateFailureResponseGEO(oR, oTran, "Failure", "WS-002", "Invalid response data", "Single-Match Data not present or recognized");
            Logger.cs_log("Invalid inquiry response data: " + oWSInquiryResponse.ToString());
          }
          else
          CreateSingleEntityResponseGEO(oWSInquiryResponse, oR);
          break;

        case "MULTIENTITYMATCH":
          //Bug 26510 NAM: detect Matches in Inquiry Response
          if (oWSInquiryResponse.Matches is null)
          {
            CreateFailureResponseGEO(oR, oTran, "Failure", "WS-003", "Invalid response data", "Multi-Match Data not present or recognized");
            Logger.cs_log("Invalid inquiry response data: " + oWSInquiryResponse.ToString());
          }
          else
          CreateMultiEntityResponseGEO(oWSInquiryResponse, oR);
          break;

        case "ZEROENTITYMATCH":
        case "FAILURE":
          CreateFailureResponseGEO(oR, oTran, oWSInquiryResponse.Type, oWSInquiryResponse.ErrorCode,
                                    oWSInquiryResponse.ErrorSummary, oWSInquiryResponse.ErrorDetail);
          break;

        default:
          CreateFailureResponseGEO(oR, oTran, "Failure", "001", "Invalid response type", "Response type was not recognized");
          Logger.cs_log("Invalid inquiry response type: " + oWSInquiryResponse.Type);
          break;
      }
      return oR;
    }

    /// <summary>
    /// Creates a response GEO for any type of failure.  Modifies passed in oR Geo, but doesn't return it
    /// to save a copy.
    /// </summary>
    /// <param name="oWSInquiryResponse"></param>
    /// <param name="oR"></param>
    /// <param name="oTran"></param>
    /// <param name="errType"></param>
    /// <param name="errCode"></param>
    /// <param name="errSummary"></param>
    /// <param name="errDetail"></param>
    public static void CreateFailureResponseGEO(GenericObject oR, GenericObject oTran, string errType,
                string errCode, string errSummary, string errDetail)
    {
      // Bug 13293 UMN Comment 16. Sometimes we are talking to an older Web service that doesn't return a 
      // zero match response. But if our transaction is expecting a zero match to not be an error, then
      // we should return a zero-match response.
      // Bug 15756/14320 UMN - zero-match when inquiry key doesn't exist 
      bool zeroMatchIsErr = (bool)oTran.get("zero_match_is_error", true, true);

      // Bug 19654 UMN Don't hide true errors with ZM response!
      // Bug 22045 DJD ZeroEntityMatch Inquiry Response From PB Web Service Not Working Correctly
      if (!zeroMatchIsErr && (errType == "ZeroEntityMatch" || (errCode == "001" && errSummary == "No match found.")))
      {
        oR.set("Type", "ZeroEntityMatch");
      }
      else
      {
        if (zeroMatchIsErr && errType == "ZeroEntityMatch") // Bug 22045 DJD ZeroEntityMatch Inquiry Response From PB Web Service Not Working Correctly
        {
          Logger.cs_log(string.Format("NOTE: Inquiry response returned 'ZeroEntityMatch' but the transaction [{0}] has 'Is No Match An Error' configured 'Yes'."
            , oTran.get("id", "", true).ToString()));
          oR.set("Type", "failure");
        }
        else
        {
          oR.set("Type", errType.ToLower());
        }
      }

      // Bug 15756 - Response will probably return error code etc.
      if (!String.IsNullOrEmpty(errCode))
      {
        oR.set("ErrorCode", errCode);
        oR.set("ErrorSummary", errSummary);
        oR.set("ErrorDetail", errDetail);
      }
      else
      {
        oR.set("ErrorCode", "001");
        oR.set("ErrorSummary", "No match found.");
        oR.set("ErrorDetail", "Search returned no results for values entered.");
      }
    }

    /// <summary>
    /// Create the response GEO for a single match. Modifies passed in oR Geo, but doesn't return it
    /// to save a copy.
    /// </summary>
    /// <param name="oWSInquiryResponse"></param>
    /// <param name="oR"></param>
    private static void CreateSingleEntityResponseGEO(InquiryResponse3 oWSInquiryResponse, GenericObject oR)
    {
      // GeneralData Tag
      GeneralData oWSGeneralData = oWSInquiryResponse.GeneralData;
      if (oWSGeneralData != null)
      {
        GenericObject oRGeneralData = c_CASL.c_GEO();
        oRGeneralData.set("_name", "GeneralData");
        oR.insert(oRGeneralData);
        for (int iGeneralLineItemIndex = 0; iGeneralLineItemIndex < oWSGeneralData.GeneralLineItem.Length; iGeneralLineItemIndex++)
        {
          // DetailLineItem Tag
          GeneralLineItem oWSGeneralLineItem = oWSGeneralData.GeneralLineItem[iGeneralLineItemIndex];
          oRGeneralData.set(oWSGeneralLineItem.name, oWSGeneralLineItem.value);
        }
      }
      // DetailData Tag
      DetailData oWSDetailData = oWSInquiryResponse.DetailData;
      if (oWSDetailData != null)
      {
        GenericObject oRDetailData = c_CASL.c_GEO();
        oRDetailData.set("_name", "DetailData");
        oR.insert(oRDetailData);
        for (int iGroupIndex = 0; iGroupIndex < oWSDetailData.Group.Length; iGroupIndex++)
        {
          // Bug 18719 UMN skip detail groups
          if (oWSDetailData.Group[iGroupIndex].ID.StartsWith("__")) continue;

          // Group Tag
          ProjectBaseline.ServiceReference.Group oWSGroup = oWSDetailData.Group[iGroupIndex];
          GenericObject oRGroup = c_CASL.c_GEO();
          oRGroup.set("_name", "Group");
          oRGroup.set("ID", oWSGroup.ID);
          oRGroup.set("ID_f_sm", true);  // Bug 24328 UMN
          oRGroup.set("Count", oWSGroup.count);
          if (oWSGroup.BankRuleID != null) oRGroup.set("BankRuleID", oWSGroup.BankRuleID); // Bug 13987
          oRDetailData.insert(oRGroup);
          for (int iDetailLineIndex = 0; iDetailLineIndex < oWSGroup.DetailLine.Length; iDetailLineIndex++)
          {
            // DetailLine Tag
            DetailLine oWSDetailLine = oWSGroup.DetailLine[iDetailLineIndex];
            GenericObject oRDetailLine = c_CASL.c_GEO();
            oRDetailLine.set("_name", "DetailLine");
            oRGroup.insert(oRDetailLine);
            for (int iDetailLineItemIndex = 0; iDetailLineItemIndex < oWSDetailLine.DetailLineItem.Length; iDetailLineItemIndex++)
            {
              // DetailLineItem Tag
              DetailLineItem oWSDetailLineItem = oWSDetailLine.DetailLineItem[iDetailLineItemIndex];
              if (oWSDetailLineItem != null && oWSDetailLineItem.name != null && oWSDetailLineItem.value != null)
              {
                oRDetailLine.set(oWSDetailLineItem.name, oWSDetailLineItem.value);
              }
            }
          }
        }
      }
    }

    /// <summary>
    /// Create the response GEO for a multi-match.  Modifies passed in oR Geo, but doesn't return it
    /// to save a copy.
    /// </summary>
    /// <param name="oWSInquiryResponse"></param>
    /// <param name="oR"></param>
    private static void CreateMultiEntityResponseGEO(InquiryResponse3 oWSInquiryResponse, GenericObject oR)
    {
      // Bug 24328 UMN add GeneralData/info block support for multimatch
      CreateSingleEntityResponseGEO(oWSInquiryResponse, oR);

      // <Matches> Tag
      Matches oWSMatches = oWSInquiryResponse.Matches;
      GenericObject oRMatches = c_CASL.c_GEO();
      oRMatches.set("_name", "Matches");
      oRMatches.set("Count", oWSMatches.Count); // 
      oR.insert(oRMatches);
      for (int iMatchIndex = 0; iMatchIndex < oWSMatches.Match.Length; iMatchIndex++)
      {
        // <Match> Tag
        ProjectBaseline.ServiceReference.Match oWSMatch = oWSMatches.Match[iMatchIndex];
        GenericObject oRMatch = c_CASL.c_GEO();
        oRMatch.set("_name", "Match");
        oRMatches.insert(oRMatch);
        for (int iMatchItemIndex = 0; iMatchItemIndex < oWSMatch.MatchItem.Length; iMatchItemIndex++)
        {
          // <MatchItem> Tag
          MatchItem oWSMatchItem = oWSMatch.MatchItem[iMatchItemIndex];
          if (oWSMatchItem != null && oWSMatchItem.name != null && oWSMatchItem.value != null)
          {
            oRMatch.set(oWSMatchItem.name, oWSMatchItem.value);
          }
        }
      }
    }

    //////////////// Bug 15206 - UMN refactor and genericize so callable for any WS request so can add retries in one place
    /// <summary>
    /// This call is a delegate for any of the calls in the standard Web service. Using this allows ups to have
    /// one function (CallWebServiceWithRetries) which works across all of the standard Web service calls.
    /// That means NO code cloning, and we won't have to fix bugs in more than one place in cloned code.
    /// </summary>
    /// <typeparam name="A"> This parameter represents type of Answer from the web service (R was taken for Request). </typeparam>
    /// <typeparam name="R"> This parameter represents type of Request made to the Web service. </typeparam>
    /// <param name="request"> This is the actual request. </param>
    /// <returns> A Web service response of type 'A' (see the call to know what that would be. </returns>
    public delegate A WSCall<A, R>(R request);

    /// <summary>
    /// Generic PB web service calling method.
    /// Example calling sequence:
    ///   SSIClient oSSIClient = CStandardSystemInterface.getSSICientFromEndPoint(oSystemInterface);
    ///   InquiryRequest req = new InquiryRequest();
    ///   InquiryResponse3 oWSInquiryResponse = CallWebService<InquiryResponse3, InquiryRequest>(oSystemInterface,
    ///      oSSIClient, oSSIClient.InquiryRequest3Operation, req);
    ///
    /// See CRealtimeUpdate.cs for other examples.
    /// </summary>
    /// <typeparam name="A"> This parameter represents type of Answer from the web service (R was taken for Request). </typeparam>
    /// <typeparam name="R"> This parameter represents type of Request made to the Web service. </typeparam>
    /// <param name="oSystemInterface"> The system interface associated with the request. </param>
    /// <param name="oSSIClient"> 
    ///   An *opened* client, ready for the first request. Make sure you pass one in! An SSI client instance is needed because
    ///   the delegate is a method (not a static) and without the client instance, the method can't be bound.
    /// </param>
    /// <param name="op"> The WS operation represented as a delegate. </param>
    /// <param name="req"> This is the actual request. </param>
    /// <returns> A Web service response of type 'A' (see the call to know what that would be. </returns>
    public static A CallWebService<A, R>(GenericObject oSystemInterface, ref SSIClient oSSIClient, WSCall<A, R> op, R req, string excToThrow)
    {
      A oWSResponse = default(A);

      // Bug 24028 UMN Remove problematic retry code
      try
      {
        // Bug 15206 UMN testing hook. Allows us to test client-side exception handling
        if (!String.IsNullOrEmpty(excToThrow)) MapExceptionStringToException(excToThrow);

        // Call the inquiry, realtime update or real time void.
        // Note: oSSIClient must be already created and ready to use, since op represents a bound instance!
        oWSResponse = op(req);
      }
      catch (Exception ex)
      {
        string msg = "Inquiry/RTUpdate Error (Exception thrown): " + ex.ToMessage();
        Logger.cs_log(msg);
        oSSIClient.Abort();
        oSSIClient = null;
        throw;
      }

      // don't put close in a finally block, because close can throw an EndpointNotFound exception
      try
      {
        if (oSSIClient != null) oSSIClient.Close();
      }
      catch
      {
        oSSIClient.Abort();
      }

      return oWSResponse;
    }

    /// <summary>
    /// This method is used to throw an exception that was sent along as a QueryKey in a special custom field
    /// named "exception_throw". When this custom field is added to an Inquiry page, the tester can type in
    /// one of the exceptions below into the field and that exception will be thrown *as if* it was thrown by
    /// the Inquiry Web service. This way we can test the retry logic, error handling, logging etc.
    /// </summary>
    /// <param name="excToThrow">
    /// See the testableExceptions dictionary at top of this file for a list of throwable exceptions.
    /// </param>
    private static void MapExceptionStringToException(string excToThrow)
    {
      string excToThrowLC = excToThrow.ToLower();
      if (testableExceptions.ContainsKey(excToThrow.ToLower()))
      {
        Type t = testableExceptions[excToThrowLC];
        Exception ex = (Exception)Activator.CreateInstance(t, String.Format("Simulated {0}.", excToThrow));
        throw ex;
      }
      else
      {
        throw new Exception(excToThrow);
      }
    }

    ////////////////  END Bug 15206 - UMN

    /// <summary>
    /// Utility function called by PB inquiry system interface and PB real time update system interface
    /// Bug 14781 get Web Service Client from Endpoint Address, use two separate EndPoints with different name for http and https
    /// Bug 15206 UMN - refactored to push knowledge of endpoints down into this function, rather than every caller.
    /// </summary>
    /// <param name="oSystemInterface"> The system interface containing the endpoint parameters. </param>
    /// <returns> returns an allocated SSIClient instance </returns>
    public static SSIClient getSSICientFromEndPoint(GenericObject oSystemInterface)
    {
      SSIClient oSSIClient = null;

      // Bug  12638 QG 
      string sEndpointAddress = oSystemInterface.get("path", "") as string; // e.g. http:// ssg-banner-dev/WcfServiceSSI/SSI.svc
      string sEndpointIndentity = oSystemInterface.get("endpoint_identity", "MachineA") as string; // e.g. "MachineA"

      if (sEndpointAddress.StartsWith("https:")) oSSIClient = new SSIClient("WSHttpBinding_ISSIA");
      else oSSIClient = new SSIClient("WSHttpBinding_ISSIB");

      EndpointAddress endpointAddress = new EndpointAddress(new Uri(sEndpointAddress), new AddressHeader[0]);
      oSSIClient.Endpoint.Address = endpointAddress;

      //IPAY-480 NAM: EndPoint UserName and Password required to access certain endpoints
      string sEndPointUserName = oSystemInterface.get("endpoint_user", "") as string;
      if (!String.IsNullOrWhiteSpace(sEndPointUserName))
      {
        oSSIClient.ClientCredentials.UserName.UserName = sEndPointUserName;
        //Bug 25693 NAM: the password entered in Config is encrypted. Must decrypt before sending over to web service in ClientCredentials
        oSSIClient.ClientCredentials.UserName.Password = "";
        string sLoginPasswordEncrypted = oSystemInterface.get("endpoint_password", "") as string;
        if (sLoginPasswordEncrypted != "")
        {
          byte[] bLoginPasswordEncrypted = Convert.FromBase64String(sLoginPasswordEncrypted);
          byte[] bLoginPasswordDecrypted = Crypto.symmetric_decrypt("data", bLoginPasswordEncrypted, "secret_key", Crypto.get_secret_key());
          string sLoginPasswordDecrypted = System.Text.Encoding.Default.GetString(bLoginPasswordDecrypted);
          oSSIClient.ClientCredentials.UserName.Password = sLoginPasswordDecrypted;
        }
      }
      // Bug 15205 UMN best practice suggests to always explicitly open proxy
      // (see: http:// blogs.msdn.com/b/wenlong/archive/2007/10/26/best-practice-always-open-wcf-client-proxy-explicitly-when-it-is-shared.aspx)
      oSSIClient.Open();
      return oSSIClient;
    }

    /// <summary>
    /// Utility function called by PB inquiry system interface and PB real time update system interface
    /// Bug 14666 [Geisinger Bug 15420] Mask sensitive information in XML string with #
    /// UMN: fix so that it actually works
    /// </summary>
    /// <param name="sXml"> The XML string to mask </param>
    /// <param name="aAdditionalTagNames"> Any additional tags to mask </param>
    /// <returns> Nothing according to Aristotle, but the Buddha disagrees, because the Void is real. </returns>
    public static void MaskTags(ref string sXml, List<String> aAdditionalTagNames)
    {
      XmlDocument oXMLDoc = new XmlDocument();
      oXMLDoc.PreserveWhitespace = true;
      oXMLDoc.LoadXml(sXml);

      // Add standard tags to mask to additional ones from config
      aAdditionalTagNames.AddRange(aStandardSensitiveInfoTags);

      // loop thru and mask the tags that match
      foreach (string sTagName in aAdditionalTagNames)
      {
        if (String.IsNullOrEmpty(sTagName)) continue;
        XmlNode oNodeToMask = oXMLDoc.SelectSingleNode("//" + sTagName);
        if (oNodeToMask != null) oNodeToMask.InnerText = "".PadLeft(oNodeToMask.InnerText.Length, '#');
      }

      // Mask name value Item Note e.g. COREFileItem,COREEventItem,TransactionItem,TenderItem,SystemInterfaceItem
      // mask list includs the standard sensitive information tags and additional tag names from ipayment configuration
      XmlNodeList itemNameNodes = oXMLDoc.SelectNodes("// name");
      foreach (XmlNode itemNameNode in itemNameNodes)
      {
        XmlNode itemNode = itemNameNode.ParentNode;

        // UMN exceptions slow down code, remove as part of normal flow; 
        //     instead check if itemNode["value"].InnerText could return null
        XmlElement elVal = itemNode["value"];
        XmlElement elName = itemNode["name"];
        if (elVal != null && elName != null)
        {
          string strItemName = elName.InnerText;
          string strItemValue = elVal.InnerText;
          if (aAdditionalTagNames.Contains<string>(strItemName))
          {
            itemNode["value"].InnerText = "".PadLeft(strItemValue.Length, '#');
          }
        }
      }
      sXml = oXMLDoc.InnerXml;
    }

    /// <summary>
    /// Set the query keys into the request object
    /// </summary>
    /// <param name="oQueryKeys"> Passed in query keys from CASL </param>
    /// <param name="req"> The inquiry request </param>
    private static void SetQueryKeys(GenericObject oQueryKeys, ref InquiryRequest req, ref string excToThrow)
    {
      req.QueryKeys = new QueryKeys();
      object[] aoKeys = oQueryKeys.getObjectArr_StringKeys();
      int iCountKeys = aoKeys.Length;
      req.QueryKeys.QueryKey = new QueryKey[iCountKeys];
      int iIndexOfKey = 0;
      foreach (Object oKey in aoKeys)
      {
        String sKey = oKey as string;
        String sValue = "" + oQueryKeys[sKey];

        // Bug 15206 UMN - make failure modes testable
        if (sKey != "exception_throw")
        {
          req.QueryKeys.QueryKey[iIndexOfKey] = new QueryKey();
          req.QueryKeys.QueryKey[iIndexOfKey].name = sKey;
          req.QueryKeys.QueryKey[iIndexOfKey].value = sValue;
        }
        else
        {
          excToThrow = sValue;
        }

        iIndexOfKey++;
      }
    }

    /// <summary>
    /// Add all the system interfaces to the request. This is currently unused. But George said that
    /// according to the PB spec, all of these should be passed. Note that for Epic/Banner support, we will
    /// need this.
    /// </summary>
    public static SystemInterfaces SetAllSystemInterfaces(GenericObject allSysInts)
    {
      // Bug 15206 UMN in future, process all system interfaces (EPIC/Banner etc support)
      int iSysIntCount = allSysInts.getIntKeyLength();
      SystemInterfaces sysints = new SystemInterfaces();
      sysints.SystemInterface = new SystemInterface[iSysIntCount];
      for (int i = 0; i < iSysIntCount; i++)
      {
        GenericObject geoSysInt = allSysInts.get(i) as GenericObject;
        sysints.SystemInterface[i] = new SystemInterface();
        sysints.SystemInterface[i].ID = geoSysInt.get("_name", "") as string;
        sysints.SystemInterface[i].Description = geoSysInt.get("description", "") as string;

        // eg. usually type will be PB_STANDARD_INQUIRY, or PB_EPIC_INQUIRY
        // sysints.SystemInterface[i].Type = "Web Service";
        sysints.SystemInterface[i].Type = geoSysInt.get("name", "") as string;

        sysints.SystemInterface[i].DatasourceName = geoSysInt.get("datasource_name", "") as string;
        sysints.SystemInterface[i].DatabaseName = geoSysInt.get("database_name", "") as string;
        sysints.SystemInterface[i].LoginName = geoSysInt.get("login_name", "") as string;
        string sLoginPasswordEncrypted = geoSysInt.get("login_password", "") as string;
        if (sLoginPasswordEncrypted != "")
        {
          byte[] bLoginPasswordEncrypted = Convert.FromBase64String(sLoginPasswordEncrypted);
          byte[] bLoginPasswordDecrypted = Crypto.symmetric_decrypt("data", bLoginPasswordEncrypted, "secret_key", Crypto.get_secret_key());
          string sLoginPasswordDecrypted = System.Text.Encoding.Default.GetString(bLoginPasswordDecrypted);
          sysints.SystemInterface[i].LoginPassword = sLoginPasswordDecrypted;
        }
        else
        {
          sysints.SystemInterface[i].LoginPassword = "";
        }
      }
      return sysints;
    }

    /// <summary>
    /// Caches detail groups for Detail Inquiries.
    /// Bug 18719 UMN Add support for detail (drill-down) inquiries 
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <param name="oWSInquiryResponse"></param>
    /// <returns></returns>
    private static void CacheDetailGroups(ref InquiryResponse3 oWSInquiryResponse)
    {
      GenericObject my = CASLInterpreter.get_my();
      List<ProjectBaseline.ServiceReference.Group> groups = new List<ProjectBaseline.ServiceReference.Group>();
      if (oWSInquiryResponse.DetailData == null) return;

      foreach (var group in oWSInquiryResponse.DetailData.Group)
      {
        if (group != null && group.ID.StartsWith("__"))
        {
          groups.Add(group);
        }
      }
      if (groups.Count > 0) my.set("detail_groups", groups);
    }

    //
    // Bug 20490 UMN Allow group names to be at discrtion of the implementor. Only requirement:
    //               They must begin with a double underscore.
    //
    // Examples:
    //    __49876af
    //   __PresFillCurrentLoc_0_Flags
    //

    /// <summary>
    /// Do the detail inquiry and return rendered HTML.
    /// Bug 18719 UMN Add support for detail (drill-down) inquiries 
    /// </summary>
    /// <param name="groupName">Name of the group to get the details for.</param>
    /// <param name="custFieldID">ID of the custom field that has the template (the one with RESERVED_DETAILS as the prompt)</param>
    /// <returns></returns>
    private static string DoDetailInquiry(string groupName, string custFieldID, string detGroupInqURL, bool isPos, string level)  // Bug 23569 NAM
    {
      GenericObject my = CASLInterpreter.get_my();

      if (my == null || !my.has("detail_groups"))
      {
        return "<html>No detail results found</html>";
      }

      return Render(custFieldID, groupName, my, detGroupInqURL, isPos, level);  // Bug 23569 NAM
    }

    /// <summary>
    /// Render the group details based upon a template.
    /// Bug 18719 UMN Add support for detail (drill-down) inquiries 
    /// </summary>
    /// <param name="lineNbr">The line nbr for the row in the block custom field which holds the details custom field</param>
    /// <param name="custFieldID">The custom field ID which contains the details and the details template</param>
    /// <param name="groupName">Name that identifies the group in the session cached detail groups.</param>
    /// <param name="my">The session's my variable</param>
    /// <returns></returns>
    private static string Render(string custFieldID, string groupName, GenericObject my, string detGroupInqURL, bool isPos, string level) // Bug 23569 NAM
    {
      System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
      TimeSpan ts;
      watch.Start();

      Grid grid = ParseIntoGrid(groupName, my, detGroupInqURL, isPos, level); // Bug 23569 NAM
      string detail_html = ApplyTemplate(custFieldID, grid, isPos, level);  // Bug 23569 NAM
      watch.Stop();
      ts = watch.Elapsed;
      Logger.cs_log(string.Format(" Details Render took {0} seconds.", watch.ElapsedMilliseconds / 1000.00));
      return detail_html;
    }

    /// <summary>
    /// Applies the template to the parsed grid structure. Renders a defualt template if
    /// no template is found, or there's an error parsing a template.
    /// Bug 18719 UMN Add support for detail (drill-down) inquiries 
    /// </summary>
    /// <param name="custFieldID">The custom field ID which contains the details and the details template</param>
    /// <param name="grid">The parsed Grid structure</param>
    /// <returns></returns>
    //Bug 23683 NAM: Render custom (Config defined) templates when Responsive UI is not ON 
    //Bug 23569 NAM: Master/Detail
    private static string ApplyTemplate(string custFieldID, Grid grid, bool isPos, string level)
    {
      string html = "";
      try
      {
        bool bResponsiveUI = (bool)((GenericObject)c_CASL.c_object("Business.Business_center_settings.data")).get("cbc_responsive_ui", false);
        // If the custom field that holds the detail template can't be found, return a default view (but not for responsiveUI)
        if (!((GenericObject)c_CASL.c_object("Custom_field.of")).has(custFieldID) && !bResponsiveUI)
        {
          return RenderNoTemplate(grid);
        }
        if (isPos || !bResponsiveUI)
        {
          GenericObject detGroupCustField = ((GenericObject)c_CASL.c_object("Custom_field.of")).get(custFieldID) as GenericObject;
          GenericObject tplWrapper = new GenericObject(); // needed to call ConvertToBase16ToASCII
          string templateStr = detGroupCustField.get("detail_template_htmldisplay", "") as String;
          if (String.IsNullOrWhiteSpace(templateStr))
          {
            // Bug 20490 UMN Customer didn't like seeing the message that the default template was rendered, so remove
            return RenderNoTemplate(grid);
          }
          tplWrapper.Add("str", templateStr);
          // Bug 20490 UMN Convert template from db-stored representation and replace any "$path" with the site_static path
          templateStr = misc.ConvertToBase16ToASCII(tplWrapper).Replace("$path", c_CASL.GEO.get("site_static", "") as string);
          FluidTemplate detailTemplate = new FluidTemplate();
          if (FluidTemplate.TryParse(templateStr, out var template))  //Bug 22708 NAM
          {
            if (!m_bRegisterAccessStrategy)
              RegisterAccessStrategy();
            detailTemplate = template;
            var context = new TemplateContext();
            context.MemberAccessStrategy.Register(grid.GetType());
            context.SetValue("grid", grid);
            html = FluidTemplateExtensions.Render(detailTemplate, context);
          }
          else
            return RenderNoTemplate(grid);
        }
        else
        {
          FluidTemplate page_template = new FluidTemplate();
          switch (level)
          {
            case "outer":
              page_template = ProjectBaseline.TemplateLoader.Instance.GetTemplate("details.liquid");
              break;
            case "inner":
              page_template = ProjectBaseline.TemplateLoader.Instance.GetTemplate("details_inner.liquid");
              break;
          }
          if (page_template != null)
          {
            //Bug 22708 NAM 20180426: Using the Fluid template engine
            var context = new TemplateContext();
            context.MemberAccessStrategy.Register(grid.GetType());
            context.SetValue("grid", grid);
            html = FluidTemplateExtensions.Render(page_template, context);
          }
        }
      }
      catch (Exception e)
      {
        string message = String.Format("Error processing template. Returning default view.{0}{1}{2}{0}", Environment.NewLine, e.Message, e.StackTrace);
        Logger.cs_log(message);
        // Bug 17849
        Logger.cs_log_trace("Error in ApplyTemplate", e);
        return RenderNoTemplate(grid);
      }
      return html;
    }

    /// <summary>
    /// Renders the detail group as a default table. Used when the template is not found, or 
    /// there's an error processing the template.
    /// Bug 18719 UMN Add support for detail (drill-down) inquiries 
    /// </summary>
    /// <param name="grid">The parsed Grid structure</param>
    /// <returns></returns>
    private static string RenderNoTemplate(Grid grid)
    {
      // Bug 20490 UMN Customer didn't like seeing the message that the default template was rendered, so remove message parameter
      StringBuilder detail_html = new StringBuilder();
      detail_html.Append("<html><body>");
      DefaultRender(detail_html, grid);
      detail_html.Append("</body></html>");
      return detail_html.ToString();
    }

    /// <summary>
    /// Parse the grop into a grid structure. The grid structure unifies Config info, and the detail group.
    /// Doing so allows us (in future) to support richer templates because we will have info on the field
    /// types in the Grid structure.
    /// Bug 18719 UMN Add support for detail (drill-down) inquiries 
    /// </summary>
    /// <param name="lineNbr">The line nbr for the row in the block custom field which holds the details custom field</param>
    /// <param name="groupName">Name that identifies the group in the session cached detail groups.</param>
    /// <param name="my">The session's my variable</param>
    /// <returns>Parsed C#/JSON grid object</returns>
    //Bug 23569 NAM: Master/Detail
    private static Grid ParseIntoGrid(string groupName, GenericObject my, string detGroupInqURL, bool isPos, string level)
    {
      // Get the cached details group from the session
      List<ProjectBaseline.ServiceReference.Group> detailGroups = my["detail_groups"] as List<ProjectBaseline.ServiceReference.Group>;
      GenericObject custFieldOf = (GenericObject)c_CASL.c_object("Custom_field.of");
      Grid grid = new Grid();

      // Loop through the cached groups looking for the cached detail
      foreach (var group in detailGroups)
      {
        // Yes, we found the detail
        if (groupName == group.ID)
        {
          int rowId = 0;

          // Following lines of code create the C# equivalent of the JSON structure that will be the data
          // passed to the template rendering, or the default no-template rendering.

          grid.gparams.name = groupName;
          foreach (var detLine in group.DetailLine)
          {
            Row row = new Row(rowId);

            foreach (var cell in detLine.DetailLineItem)
            {
              if (cell.name == null || cell.value == null) continue;

              string tagnameOrID = cell.name;

              if (rowId == 0)
              {
                grid.columns.Add(new Column(cell.name, "typicaltextInRow", "text"));
              }

              // Handle details within details
              if (cell.value.StartsWith("__") && custFieldOf.has(tagnameOrID))
              {
                // find the custom field from the id (ID must be same as tagname!) in cell.name
                GenericObject detCustField = (GenericObject)custFieldOf.get(tagnameOrID);
                string detButtonImageURL = (string)detCustField.get("default", "");
                if (!detButtonImageURL.StartsWith("http"))
                {
                  detButtonImageURL = String.Format("{0}/{1}", c_CASL.GEO.get("site_static", "") as string, detButtonImageURL);
                }
                string button;

                // generate a button with background image for pos. For responsiveUI, we generate a button that looks like a text link
                if (isPos /*|| !c_CASL.get_responsiveUI()*/)
                {
                  button = String.Format("<button class='det-button' id='{0}' style='background: url(\"{1}\") no-repeat 50% 0%; {2}' onclick='get_details(this)' type='button' data-cbtid='{3}' data-target='{4}' data-url='{5}' />",
                      cell.value,
                      detButtonImageURL,
                      detCustField.get("column_inline_style", ""),
                      tagnameOrID,
                      detCustField.get("detail_template", ""),
                      detGroupInqURL);
                }
                else
                {
                  // hardcode the responsiveUI rep. Will need to un-hardcode in future
                  button = String.Format("<div class='item-group expander-trigger expander-hidden' id='{0}' onclick='get_inner_details(this);' data-cbtid='{1}' data-url='{2}'>",
                      cell.value,
                      tagnameOrID,
                      detGroupInqURL);
                }
                row.data.Add(new KVPair(cell.name, button));
              }
              else
              {
                row.data.Add(new KVPair(cell.name, cell.value));
              }
            }
            grid.rows.Add(row);
            rowId = rowId + 1;
          }
        }
      }
      return grid;
    }

    /// <summary>
    /// Renders the grid as a default table.
    /// Bug 18719 UMN Add support for detail (drill-down) inquiries 
    /// </summary>
    /// <param name="html">Stringbuilder to append the rendering to</param>
    /// <param name="grid">The parsed grid structure</param>
    /// <returns>Nda</returns>
    private static void DefaultRender(StringBuilder html, Grid grid)
    {
      html.Append("<table><thead><tr>");

      // Loop through C# grid structure and construct a default html table 
      foreach (var col in grid.columns)
      {
        html.Append(String.Format("<th>{0}</th>", col.name));
      }
      html.Append("</tr></thead><tbody>");
      foreach (var row in grid.rows)
      {
        html.Append("<tr>");
        foreach (var datum in row.data)
        {
          html.Append(String.Format("<td>{0}</td>", datum.Value));  //Bug 23623 NAM
        }
        html.Append("</tr>");
      }
      html.Append("</tbody></table>");
      return;
    }
    //Bug 23683 NAM: Register objects to be rendered by Fluid engine
    /// <summary>
    /// RegisterAccessStrategy - make object access public
    /// </summary>
    static private void RegisterAccessStrategy()
    {
      TemplateContext.GlobalMemberAccessStrategy.Register<Grid>();
      TemplateContext.GlobalMemberAccessStrategy.Register<Row>();
      TemplateContext.GlobalMemberAccessStrategy.Register<Column>();
      TemplateContext.GlobalMemberAccessStrategy.Register<Gparams>();
      TemplateContext.GlobalMemberAccessStrategy.Register<KVPair>();
      m_bRegisterAccessStrategy = true;
    }
  }  // End class CStandardSystemInterface

  /**************************************************************
   * Bug 18719 UMN Add support for detail (drill-down) inquiries 
   * The following are the classes that unify the Config view of our custom fields, and the data
   * view that is returned from inquiry. It is analogus to what CBTBlocks.js does with the oCBTBlocksData,
   * but simpler. The Grid part has been pinched from the responsive UI code. 
   * When that code makes it way into base, this code may need some tweaking.
   */

  //Bug 23623 NAM - removed KVPair class

  [JsonObject(MemberSerialization.OptIn)]
  public class Gparams //Bug 22708 NAM
  {

    [JsonProperty]
    public string name { get; set; }

    [JsonProperty]
    public string block_type { get; set; }

    [JsonProperty]
    public List<KVPair> labels { get; set; }

    public Gparams()
    {
      name = block_type = "";
      labels = new List<KVPair>();
    }

    public override string ToString()
    {
      return JsonConvert.SerializeObject(this);
    }
  }

  [JsonObject(MemberSerialization.OptIn)]
  public class Column //Bug 22708 NAM
  {

    [JsonProperty]
    public string name { get; set; }

    [JsonProperty]
    public string column_type { get; set; }

    [JsonProperty]
    public string default_val { get; set; }

    [JsonProperty]
    public string description { get; set; }

    [JsonProperty]
    public bool hidden { get; set; }

    [JsonProperty]
    public int max_length { get; set; }

    [JsonProperty]
    public int min_length { get; set; }

    [JsonProperty]
    public bool read_only { get; set; }

    [JsonProperty]
    public bool required { get; set; }

    [JsonProperty]
    public string role { get; set; }

    [JsonProperty]
    public string widget_type { get; set; }

    public Column()
    {
      name = column_type = default_val = description = role = widget_type = "";
      hidden = read_only = required = false;
      min_length = max_length = 0;
    }

    public Column(string n, string ct, string wt)
    {
      name = n; widget_type = wt; column_type = ct;
    }

    public override string ToString()
    {
      return JsonConvert.SerializeObject(this);
    }
  }

  [JsonObject(MemberSerialization.OptIn)]
  public class Row //Bug 22708 NAM
  {

    [JsonProperty]
    public int row_id { get; set; }

    [JsonProperty]
    public List<KVPair> data { get; set; }

    public Row(int r)
    {
      row_id = r;
      data = new List<KVPair>();
    }

    public Row()
    {
      row_id = 0;
      data = new List<KVPair>();
    }

    public override string ToString()
    {
      return JsonConvert.SerializeObject(this);
    }
  }

  [JsonObject(MemberSerialization.OptIn)]
  public class Grid //Bug 22708 NAM
  {

    [JsonProperty]
    public Gparams gparams { get; set; }

    [JsonProperty]
    public List<Column> columns { get; set; }

    [JsonProperty]
    public List<Row> rows { get; set; }

    public Grid()
    {
      gparams = new Gparams();
      columns = new List<Column>();
      rows = new List<Row>();
    }

    public override string ToString()
    {
      return JsonConvert.SerializeObject(this);
    }
  }

  /*
   * END Bug 18719 UMN Add support for detail (drill-down) inquiries 
   ******************************************************************/

}  // end namespace ProjectBaseline
