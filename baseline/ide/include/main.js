// this file ide/include/main.js.   Called by: casl_ide.action_bar in ide.casl

// Utility
    
function get_current_site () {  // http://localhost/pos_demo/foo/bar => http://localhost/pos_demo
// TODO: simplify using location.pathname;
 var full_href=window.location.href;
 var start_protocol=full_href.indexOf("//");
 var start_site=full_href.indexOf("/",start_protocol+2);
 var start_path=full_href.indexOf("/",start_site+1);
 if (start_path==-1)
   return full_href;
 else
   return full_href.substring(0,start_path);//+1);
}
// End utility

/* Entry point:  ide_execute
Calling path:  When loading client from IDE, using casl_client.htm_class
  IDE execute button calls: ide_execute(this) calls ipayment_client_window.casl_client_init    
*/            
function ide_execute (a_tag) {            
  var the_form=document.getElementById("my_form");
  var run_at_server=the_form.run_at[1].checked;
  if (run_at_server) {
    the_form.submit();
  } else {
    //src=<casl> casl_client.<ipayment_client_start/> </casl>
    var client_href=top.frames.ipayment_client.location.href;  // "about:blank" if not started
    if (client_href=="about:blank")
      top.frames.ipayment_client.location=get_current_site() + "/casl_client/ipayment_client_start.htm?";
    else
      top.frames.ipayment_client.casl_form_aux(a_tag);
//    if ( top.ipayment_client_window pos_window.casl_client_init && pos_window.casl_client==null) {  // if init can be called, and casl_client not already set
//      pos_window.casl_client_init( { type:"form", the_form:a_tag } );
//    } else {
//      casl_form_aux(a_tag);
//    }
  }
  // return null (return value is ignored)
}
          
function fget (a_frame, a_id) {  // fget = frame get
 return window.top.frames[a_frame].document.getElementById(a_id);
}

// called by: htm_editor/editor.buffer.onchange and onmouseup
function save_selection () {
  var editor_window = window.top.frames.editor;
  var txt = "";
  
  // Bug UMN 19852 IE11 fix
  var buff = fget("editor", "buffer");
  txt = buff.value.substring(buff.selectionStart, buff.selectionEnd);
 
  if (txt === "" && editor_window.getSelection) {
    txt = editor_window.getSelection().toString();
  } else if (txt === "" && document.selection) {
    txt = editor_window.document.selection.createRange().text;
  }
  if (txt === "") {
    txt = fget("editor", "buffer").value;
  }
  fget("action_bar", "e_source").value = Base64.encode(txt);
  return null;
}

// called by: FORM.INPUT
function save_selection2 () {
 save_selection();
 fget("editor","buffer").focus();
 return null;
}

function font_increase () {
  var a_style=fget("editor","buffer").style;
  var new_size=Math.round(parseInt(a_style.fontSize)*1.2);
  status="Increased font size to " + new_size + "pt";
  a_style.fontSize=new_size;
}
function font_decrease () {
  var a_style=fget("editor","buffer").style;
  var new_size=Math.round( parseInt(a_style.fontSize)*0.83333 );
  status="Reduced font size to " + new_size + "pt";
  a_style.fontSize=new_size;
}
function ignore_tab (e) {
  //status=e.keyCode;
  if (e.keyCode==9)
    return false;
  else
    return true;
}

/*****
*
*   Base64.js
*
*   copyright 2003, Kevin Lindsey
*   licensing info available at: http://www.kevlindev.com/license.txt
*
*****/

/*****
*
*   encoding table
*
*****/
Base64.encoding = [
    "A", "B", "C", "D", "E", "F", "G", "H",
    "I", "J", "K", "L", "M", "N", "O", "P",
    "Q", "R", "S", "T", "U", "V", "W", "X",
    "Y", "Z", "a", "b", "c", "d", "e", "f",
    "g", "h", "i", "j", "k", "l", "m", "n",
    "o", "p", "q", "r", "s", "t", "u", "v",
    "w", "x", "y", "z", "0", "1", "2", "3",
    "4", "5", "6", "7", "8", "9", "+", "/"
];

/*****
*
*   constructor
*
*****/
function Base64() {}

/*****
*
*   encode
*
*****/
Base64.encode = function(data) {
    var result = [];
    var ip57   = Math.floor(data.length / 57);
    var fp57   = data.length % 57;
    var ip3    = Math.floor(fp57 / 3);
    var fp3    = fp57 % 3;
    var index  = 0;
    var num;
    
    for ( var i = 0; i < ip57; i++ ) {
        for ( j = 0; j < 19; j++, index += 3 ) {
            num = data.charCodeAt(index) << 16 | data.charCodeAt(index+1) << 8 | data.charCodeAt(index+2);
            result.push(Base64.encoding[ ( num & 0xFC0000 ) >> 18 ]);
            result.push(Base64.encoding[ ( num & 0x03F000 ) >> 12 ]);
            result.push(Base64.encoding[ ( num & 0x0FC0   ) >>  6 ]);
            result.push(Base64.encoding[ ( num & 0x3F     )       ]);
        }
        result.push("\n");
    }

    for ( i = 0; i < ip3; i++, index += 3 ) {
        num = data.charCodeAt(index) << 16 | data.charCodeAt(index+1) << 8 | data.charCodeAt(index+2);
        result.push(Base64.encoding[ ( num & 0xFC0000 ) >> 18 ]);
        result.push(Base64.encoding[ ( num & 0x03F000 ) >> 12 ]);
        result.push(Base64.encoding[ ( num & 0x0FC0   ) >>  6 ]);
        result.push(Base64.encoding[ ( num & 0x3F     )       ]);
    }

    if ( fp3 == 1 ) {
        num = data.charCodeAt(index) << 16;
        result.push(Base64.encoding[ ( num & 0xFC0000 ) >> 18 ]);
        result.push(Base64.encoding[ ( num & 0x03F000 ) >> 12 ]);
        result.push("==");
    } else if ( fp3 == 2 ) {
        num = data.charCodeAt(index) << 16 | data.charCodeAt(index+1) << 8;
        result.push(Base64.encoding[ ( num & 0xFC0000 ) >> 18 ]);
        result.push(Base64.encoding[ ( num & 0x03F000 ) >> 12 ]);
        result.push(Base64.encoding[ ( num & 0x0FC0   ) >>  6 ]);
        result.push("=");
    }

    return result.join("");
};
