using System;
using System.Data;
using System.Windows.Forms;
using System.Threading;
using System.Collections;
using CASL_engine;

namespace db_proc
{
  // Daniel wrote this class on 06-07-2006. 

  // Please do not change without talking to me first.
  // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

	/// <summary>
	/// Summary description for StoreProc.
	/// </summary>
	public class StoredProcClass
	{
    private GenericObject m_pDBConnInfo = null;
    private OracleConnection m_pOraConn = null;
    private OracleCommand m_pOraCmd = null;
    private OdbcConnection m_pOdbcConn = null;
    private OdbcCommand m_pOdbcCmd = null;
    int m_iDBType = 0;

		public StoredProcClass(params object [] args)
		{
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!
		}

    public static GenericObject CallStoredProc(params object [] arg_pairs)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      // This is the main entry point function for all CASL calls.

      StoredProcClass pThis = new StoredProcClass();
      GenericObject pReturnData = new GenericObject();
      string strErrorMsg = "";

      lock(pThis)
      {
        pThis.m_iDBType = 0;
        GenericObject args = misc.convert_args(arg_pairs);
        GenericObject geoData = args.get("_subject", c_CASL.c_undefined) as GenericObject;
        pThis.m_pDBConnInfo = (GenericObject)geoData.get("db_connection", c_CASL.c_undefined) as GenericObject;
        GenericObject pInputParams = args.get("args", c_CASL.c_undefined) as GenericObject;
        GenericObject pParamDir = (GenericObject)args.get("inout_params");
        
        // 0 = NOT SET
        // 1 = ORACLE
        // 2 = SQL SERVER
        // 3 = ACCESS =0;
        // 4 = ORACLE SQLNET
        pThis.m_iDBType = pThis.GetDBConnectionType();

        try
        {
          pThis.CreateCommandObj();// Command for stored proc.
          if(!pThis.EstablishDatabaseConnection(ref strErrorMsg))
            throw CASL_error.create("message", "Error opening database connection! Error: " + strErrorMsg, "code", "703");

          if(pThis.m_iDBType == 4)// Oracle tns or Oracle ODBC
          {
            if(!pThis.CallOraStoredProc(ref pReturnData, ref strErrorMsg, args, geoData, pInputParams, pParamDir, ref pThis.m_pOraConn, ref pThis.m_pOraCmd))
              throw CASL_error.create("message", "Unable to call stored procedure. Error: " + strErrorMsg, "code", "703");
           
          }
          else if(pThis.m_iDBType == 1 || pThis.m_iDBType == 2)// Oracle or SQL Server ODBC
          {
            if(!pThis.CallOdbcStoredProc(ref pReturnData, ref strErrorMsg, args, geoData, pInputParams, pParamDir, ref pThis.m_pOdbcConn, ref pThis.m_pOdbcCmd))
              throw CASL_error.create("message", "Unable to call stored procedure. Error: " + strErrorMsg, "code", "703");
          }
          else// RDBMS not supported. Only Oracle & SQL Server are supported.
            throw CASL_error.create("message", "Unable to call stored procedure. This RDBMS is not supported.", "code", "703");
        }
        catch(Exception e)
        {
          pThis.CloseDBConnection();
          string strProcName = (string)geoData.get("_name");
          string strTmp = string.Format("Unable to call stored procedure [{0}]\nReturn value is [{1}]\n[{2}]", strProcName, e.Message, strErrorMsg); 
          throw CASL_error.create("message",strTmp, "code", "703");
        }
      }

      pThis.CloseDBConnection();
      pThis.m_pOdbcConn = null;
      pThis.m_pOdbcCmd = null;
      pThis.m_pOraConn = null;
      pThis.m_pOraCmd = null;

      return pReturnData;
    }

    public bool CallOraStoredProc(ref GenericObject pReturnData, ref string strErrorMsg, GenericObject args, GenericObject geoData, GenericObject pInputParams, GenericObject pParamDir, ref OracleConnection pCnn, ref OracleCommand pCmd)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      int iCurrParam = 0;
      string strErr = "";
      
      // In case we are going against Oracle and packages.
      string strPackageName = "";
      if(geoData.has("package"))
        strPackageName = (string)geoData.get("package");
      if(strPackageName.Length > 0)
        strPackageName += ".";

      // Get the stored procedure name
      string strProcName = (string)geoData.get("_name");
      
      // Get the user id - mabe different than logon
      string strProcUID = "";
      if(geoData.has("userid"))
        strProcUID = (string)geoData.get("userid") + ".";
      
      // Contract info.
      GenericObject geoContract = (GenericObject)geoData.get("contract");
      GenericObject geoContractFieldOrder = (GenericObject)geoContract.get("_field_order");
      int iContractParamCnt = geoContractFieldOrder.getIntKeyLength();

      // Get the response object
      GenericObject geoResponse = (GenericObject)geoContract.get("_return_type");
      GenericObject geoResponseFieldOrder = (GenericObject)geoResponse.get("_field_order");
      int iResponseParamCnt = geoResponseFieldOrder.getIntKeyLength();
            
      try
      {
        //===========================================================
        pCmd.CommandText= strProcUID + strPackageName + strProcName;
        pCmd.CommandType= CommandType.StoredProcedure;
        
        //=============================
        // Add contract input/output parameters.
        for(iCurrParam=0; iCurrParam<iContractParamCnt; iCurrParam++)
        {
          OracleParameter pParam  = new OracleParameter();
          if(!CreateOraParameter(ref pParam, geoContract, pInputParams, geoContractFieldOrder, pParamDir, iCurrParam, ref strErr))
          {
            strErrorMsg = strErr;
            return false;
          }
          pCmd.Parameters.Add(pParam);            
        }
        //======================

        //======================
        // Add response input/output parameters.
        for(iCurrParam=0; iCurrParam<iResponseParamCnt; iCurrParam++)
        {  
          string strParamName = (string)geoResponseFieldOrder.get(iCurrParam);
          object o = geoContractFieldOrder.key_of(strParamName);
          if(!o.Equals(false))
          {
            int key = (int)o;
            if(key >= 0)
              continue;
          }

            OracleParameter pParam  = new OracleParameter();
            if(!CreateOraParameter(ref pParam, geoResponse, pInputParams, geoResponseFieldOrder, pParamDir, iCurrParam, ref strErr))
            {
              strErrorMsg = strErr;
              return false;
            }
            pCmd.Parameters.Add(pParam);  
        }

        // Execute the stored procedure
        pCmd.ExecuteNonQuery();

        // Now populate all response fields with return from stored proc.
        for(int i=0; i<iResponseParamCnt; i++)
        {
          string strParamName = geoResponseFieldOrder.get(i).ToString();
          object pVal = pCmd.Parameters[strParamName].Value;
          if(pVal != DBNull.Value)
          {
            object pNewVal = ConvertToCASLType(pVal);
            pReturnData.set(strParamName, pNewVal);
          }
        }
        //===========================================================
      }
      catch(Exception e)
      {
        strErrorMsg = e.Message;
        return false;
      }
	
      return true;
    }

    public bool CreateOraParameter(ref OracleParameter pParam, GenericObject pMain, GenericObject pInputParams, GenericObject pMainFieldOrder, GenericObject pParamDir, int iCurrParam, ref string strErr)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!
      
      try
      {
        pParam.ParameterName = pMainFieldOrder[iCurrParam].ToString();
        GenericObject pParamType = (GenericObject)pMain.get(pParam.ParameterName + "_f_type");          
      
        // Determin direction
        if(pParamDir.get(pParam.ParameterName).ToString()=="IN")
          pParam.Direction = ParameterDirection.Input;
        else if(pParamDir.get(pParam.ParameterName).ToString()=="OUT")
          pParam.Direction = ParameterDirection.Output;
        else
          pParam.Direction = ParameterDirection.InputOutput;

        // Determine data type
        if(pParamType.get("db_type", true).ToString() == "StringFixedLength")
					pParam.OracleType = System.Data.OracleClient.OracleType.VarChar;
        else if(pParamType.get("db_type", true).ToString() == "Int16")
          pParam.OracleType = System.Data.OracleClient.OracleType.UInt16;
        else if(pParamType.get("db_type", true).ToString() == "Int32")
          pParam.OracleType = System.Data.OracleClient.OracleType.UInt32;
        else if(pParamType.get("db_type", true).ToString() == "Decimal")
          pParam.OracleType = System.Data.OracleClient.OracleType.Double;

        if(pParamType.has("size"))
          pParam.Size = (int)Convert.ChangeType(pParamType.get("size",0, true), typeof(System.Int32));

        if(pParamType.has("is_nullable"))
          pParam.IsNullable = (bool)Convert.ChangeType(pParamType.get("is_nullable", true), typeof(System.Boolean));
      
        if(pParamType.has("precision"))
          pParam.Precision = (byte)Convert.ChangeType(pParamType.get("precision", true), typeof(System.Byte));
        else
          pParam.Precision = 0;

        if(pParamType.has("scale"))
          pParam.Scale = (byte)Convert.ChangeType(pParamType.get("scale", true), typeof(System.Byte));
        else
          pParam.Scale = 0;

        if(pParamType.has("source_column"))
          pParam.SourceColumn = (string)pParamType.get("source_column", true);
        else
          pParam.SourceColumn = "";

        pParam.SourceVersion = DataRowVersion.Current;
      
        if(pInputParams.has(pParam.ParameterName))
        {
          object o = pInputParams.get(pParam.ParameterName, true);
          if(o != null)
          {
            if(o.GetType() != typeof(GenericObject))// No val.
              pParam.Value = o;
          }
        }
      }
      catch(Exception e)
      {
        strErr = e.Message;
        return false;
      }

     return true;
    }

    public bool CreateOdbcParameter(ref OdbcParameter pParam, GenericObject pMain, GenericObject pInputParams, GenericObject pMainFieldOrder, GenericObject pParamDir, int iCurrParam, ref string strErr)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!
     
      try
      {
        pParam.ParameterName = pMainFieldOrder[iCurrParam].ToString();
        GenericObject pParamType = (GenericObject)pMain.get(pParam.ParameterName + "_f_type");          
      
        // Determin direction
        if(pParamDir.get(pParam.ParameterName).ToString()=="IN")
          pParam.Direction = ParameterDirection.Input;
        else if(pParamDir.get(pParam.ParameterName).ToString()=="OUT")
          pParam.Direction = ParameterDirection.Output;
        else
          pParam.Direction = ParameterDirection.InputOutput;

        // Determine data type
        if(pParamType.get("db_type", true).ToString() == "StringFixedLength")
          pParam.OdbcType = System.Data.Odbc.OdbcType.VarChar;
        else if(pParamType.get("db_type", true).ToString() == "Int16")
          pParam.OdbcType = System.Data.Odbc.OdbcType.Numeric;
        else if(pParamType.get("db_type", true).ToString() == "Int32")
          pParam.OdbcType = System.Data.Odbc.OdbcType.Numeric;
        else if(pParamType.get("db_type", true).ToString() == "Decimal")
          pParam.OdbcType = System.Data.Odbc.OdbcType.Numeric;

        if(pParamType.has("size"))
          pParam.Size = (int)Convert.ChangeType(pParamType.get("size",0, true), typeof(System.Int32));

        if(pParamType.has("is_nullable"))
          pParam.IsNullable = (bool)Convert.ChangeType(pParamType.get("is_nullable", true), typeof(System.Boolean));
      
        if(pParamType.has("precision"))
          pParam.Precision = (byte)Convert.ChangeType(pParamType.get("precision", true), typeof(System.Byte));
        else
          pParam.Precision = 0;

        if(pParamType.has("scale"))
          pParam.Scale = (byte)Convert.ChangeType(pParamType.get("scale", true), typeof(System.Byte));
        else
          pParam.Scale = 0;

        if(pParamType.has("source_column"))
          pParam.SourceColumn = (string)pParamType.get("source_column", true);
        else
          pParam.SourceColumn = "";

        pParam.SourceVersion = DataRowVersion.Current;
      
        if(pInputParams.has(pParam.ParameterName))
        {
          object o = pInputParams.get(pParam.ParameterName, true);
          if(o != null)
          {
            if(o.GetType() != typeof(GenericObject))// No val.
              pParam.Value = o;
          }
        }
        if(pParam.Value == null)
          pParam.Value = DBNull.Value;
      }
      catch(Exception e)
      {
        strErr = e.Message;
        return false;
      }

      return true;
    }

    public bool CallOdbcStoredProc(ref GenericObject pReturnData, ref string strErrorMsg, GenericObject args, GenericObject geoData, GenericObject pInputParams, GenericObject pParamDir, ref OdbcConnection pCnn, ref OdbcCommand pCmd)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      int iCurrParam = 0;
      string strErr = "";
      
      // Get the stored procedure name
      string strProcName = (string)geoData.get("_name");
      
      // Get the user id - mabe different than logon
      string strProcUID = "";
      if(geoData.has("userid"))
        strProcUID = (string)geoData.get("userid") + ".";
      
      // In case we are going against Oracle and packages.
      string strPackageName = "";
      if(geoData.has("package"))
        strPackageName = (string)geoData.get("package");
      if(strPackageName.Length > 0)
        strPackageName += ".";

      // Contract info.
      GenericObject geoContract = (GenericObject)geoData.get("contract");
      GenericObject geoContractFieldOrder = (GenericObject)geoContract.get("_field_order");
      int iContractParamCnt = geoContractFieldOrder.getIntKeyLength();

      // Get the response object
      GenericObject geoResponse = (GenericObject)geoContract.get("_return_type");
      GenericObject geoResponseFieldOrder = (GenericObject)geoResponse.get("_field_order");
      int iResponseParamCnt = geoResponseFieldOrder.getIntKeyLength();
      
      try
      {
        //===========================================================
        string strParamMask = "";
        
        int iInputCnt = pParamDir.getStringKeys().Count;
        for(int x=0; x<iInputCnt; x++)
        {
          strParamMask += "?";
          if(x+1 < iInputCnt)
            strParamMask += ",";
        }

        string strText = "";
        if(m_iDBType == 2)// SQL Server maybe has a return value
          strText = string.Format("?=call {0}({1})", (strProcUID + strPackageName + strProcName), strParamMask);
        else
          strText = string.Format("call {0}({1})", (strProcUID + strPackageName + strProcName), strParamMask);
        
        pCmd.CommandText = "{" + strText + "}";
        pCmd.CommandType= CommandType.StoredProcedure;
        
        //Setup the return value and type. We only support int types for now.
        if(m_iDBType == 2)// Only for SQL Server
        {
          if(geoData.has("return_value"))
          {
            // Add the return param
            pCmd.Parameters.Add(new OdbcParameter("return_value",
              OdbcType.SmallInt,
              2,
              ParameterDirection.ReturnValue,
              false,
              0,
              0,
              "registrar_id",
              DataRowVersion.Current,
              null));
          }
        }

        //=============================
        // Add contract input/output parameters.
        for(iCurrParam=0; iCurrParam<iContractParamCnt; iCurrParam++)
        {
          OdbcParameter pParam  = new OdbcParameter();
          if(!CreateOdbcParameter(ref pParam, geoContract, pInputParams, geoContractFieldOrder, pParamDir, iCurrParam, ref strErr))
          {
            strErrorMsg = strErr;
            return false;
          }
          pCmd.Parameters.Add(pParam);            
        }
        //======================

        //======================
        // Add response input/output parameters.
        for(iCurrParam=0; iCurrParam<iResponseParamCnt; iCurrParam++)
        {      
          string strParamName = (string)geoResponseFieldOrder.get(iCurrParam);
          int key = (int)geoContractFieldOrder.key_of(strParamName);
          if(key >= 0)
            continue;

          OdbcParameter pParam  = new OdbcParameter();
          if(!CreateOdbcParameter(ref pParam, geoResponse, pInputParams, geoResponseFieldOrder, pParamDir, iCurrParam, ref strErr))
          {
            strErrorMsg = strErr;
            return false;
          }
          pCmd.Parameters.Add(pParam);  
        }

        // Execute the stored procedure
        pCmd.ExecuteNonQuery();

        // Now populate all response fields with return from stored proc.
        for(int i=0; i<iResponseParamCnt; i++)
        {
          string strParamName = geoResponseFieldOrder.get(i).ToString();
          object pVal = pCmd.Parameters[strParamName].Value;
          if(pVal != DBNull.Value)
          {
            object pNewVal = ConvertToCASLType(pVal);
            pReturnData.set(strParamName, pNewVal);
          }
        }

        //Also add the return value
        if(m_iDBType == 2)
        {
          if(geoData.has("return_value"))
          {
            object pRV = ConvertToCASLType(pCmd.Parameters["return_value"].Value);
            if(pRV != null)
              pReturnData.set("return_value", pRV);
          }
        }
        //===========================================================
      }
      catch(Exception e)
      {
        strErrorMsg = e.Message;
        return false;
      }

      return true;
    }
    
    public string TranslateErrorMsg(int pReturnCode)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!
      
      string strErrorMsg = "";
      
      // SQL Server specific errors.
      if(m_iDBType == 2)// SQL Server
      {
        strErrorMsg = "Procedure returned Error: ";
        switch(pReturnCode)
        {
          case -1:
            strErrorMsg += " -1 Object missing";
          break;
          case -2:
            strErrorMsg += " -2 Datatype error occurred";
          break;
          case -3:
            strErrorMsg += " -3 Process was chosen as deadlock victim";
          break;
          case -4:
            strErrorMsg += " -4 Permission error occurred";
          break;
          case -5:
            strErrorMsg += " -5 Syntax error occurred";
          break;
          case -6:
            strErrorMsg += " -6 Miscellaneous user error occurred";
          break;
          case -7:
            strErrorMsg += " -7 Resource error, such as out of space, occurred";
          break;
          case -8:
            strErrorMsg += " -8 Nonfatal internal problem encountered";
          break;
          case -9:
            strErrorMsg += " -9 System limit was reached";
          break;
          case -10:
            strErrorMsg += " -10 Fatal internal inconsistency occurred";
          break;
          case -11:
            strErrorMsg += " -11 Fatal internal inconsistency occurred";
          break;
          case -12:
            strErrorMsg += " -12 Table or index is corrupt";
          break;
          case -13:
            strErrorMsg += " -13 Database is corrupt";
          break;
          case -14:
            strErrorMsg += " -14 Hardware error occurred";
          break;
          default:
            strErrorMsg = "";
          break;
        }
      }
      return strErrorMsg;
    }

    public object ConvertToCASLType(object pVal)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!
      
      // Need to convert some types to CASL compatable types.
      if(pVal == null || pVal == DBNull.Value)
        return null;

      object pRV = pVal;

      if(pVal.GetType() == typeof(System.Int16) || pVal.GetType() == typeof(System.UInt16))
        pRV = (int)Convert.ChangeType(pVal, typeof(System.Int32));
      else if(pVal.GetType() == typeof(System.Decimal))
      {
        string s = string.Format("{0}", pVal);
        if(s.IndexOf(".", 0, s.Length) == -1)
          pRV = Decimal.ToInt32((Decimal)pVal);             
      }

      return pRV;
    }

    public void CloseDBConnection()
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      if(m_iDBType == 1)// Oracle TNS
      {
        if(m_pOraConn != null)
          m_pOraConn.Close();
      }
      else//ODBC
        if(m_pOdbcConn != null)
          m_pOdbcConn.Close();
    }

    public void CreateCommandObj()
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      if(m_iDBType==4)
        m_pOraCmd = new OracleCommand();
      else
        m_pOdbcCmd = new OdbcCommand();
    }

    public bool EstablishDatabaseConnection(ref string strErr)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      bool bRV                  = false;
      string strDataSource      = "";
      string strUID             = "";
      string strPWD             = "";       
      string strConnString      = "";
      
      try
      {
        if(m_iDBType == 4)// Oracle SQL NET
        {
          strDataSource      = (string)m_pDBConnInfo.get("tns_entry");
          strUID             = (string)m_pDBConnInfo.get("login_name");
          strPWD             = (string)m_pDBConnInfo.get("login_password");       
          strConnString      = string.Format("DATA SOURCE={0};UID={1};PWD={2};Pooling=false;Enlist=false;", strDataSource,strUID,strPWD);
        
          m_pOraConn = new OracleConnection(strConnString);
          m_pOraConn.Open();
          m_pOraCmd.Connection = m_pOraConn;
          bRV = true;
        }
        else
        {
          strDataSource      = (string)m_pDBConnInfo.get("datasource_name");
          strUID             = (string)m_pDBConnInfo.get("login_name");
          strPWD             = (string)m_pDBConnInfo.get("login_password");       
          strConnString      = string.Format("DSN={0};UID={1};PWD={2};Pooling=false;Enlist=false;", strDataSource,strUID,strPWD);
        
          m_pOdbcConn = new OdbcConnection(strConnString);
          m_pOdbcConn.Open();
          m_pOdbcCmd.Connection = m_pOdbcConn;
          bRV = true;
        }
      }
      catch(Exception e)
      {
        strErr = e.Message;
        return false;
      }      
      
    
      return bRV;
    }

    private int GetDBConnectionType()
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!
      // 0 = NOT SET
      // 1 = ORACLE
      // 2 = SQL SERVER
      // 3 = ACCESS =0;
      // 4 = ORACLE SQLNET

      int iDBType = 0;

      GenericObject DBInfo  = (GenericObject)m_pDBConnInfo;
      string strConnctionClass = (string) DBInfo.get("_name","",true);
      if(strConnctionClass == "Oracle")
        iDBType = 1;
      else if(strConnctionClass == "Sql_server")
        iDBType = 2;
      else if(strConnctionClass == "Access")
        iDBType = 3;
      if(strConnctionClass == "Oracle_tns")
        iDBType = 4;

      return iDBType;
    }
	}
}


