﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using CASL_engine;
using System.Security.Cryptography;
using System.Xml.Linq;
using System.Xml;
using MiraServDotNet;
using baseline;
using System.Collections.Concurrent;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.IO.Compression;
using TranSuiteServices;
using System.Collections.Specialized;

// Bug 20733 MJO - Eigen MiraServ support

namespace baseline
{
  public class MiraServ
  {
    private static ConcurrentDictionary<string, DeviceEntry> Devices = PopulateDevices();

    private static ConcurrentDictionary<string, DeviceEntry> PopulateDevices()
    {
      // Bug 18177 MJO - Handle and log exceptions
      ConcurrentDictionary<string, DeviceEntry> devices = new ConcurrentDictionary<string, DeviceEntry>();

      try
      {
        GenericObject database = c_CASL.c_object("TranSuite_DB.Config.db_connection_info") as GenericObject;
        Hashtable sqlArgs = new Hashtable();

        string sql = "SELECT * FROM TG_DEVICE_ID WHERE DEVICE_TYPE='MIRASERV'";

        string error_message = "";
        IDBVectorReader reader = db_reader.Make(database, sql, CommandType.Text, 0,
          sqlArgs, 0, 0, null, ref error_message);

        GenericObject records = new GenericObject();
        reader.ReadIntoRecordset(ref records, 0, 0);
        reader.CloseReader();

        for (int i = 0; i < records.getLength(); i++)
        {
          DeviceEntry entry = new DeviceEntry((GenericObject)records.get(i));
          devices[entry.MACAddress] = entry;
          devices[entry.stationID] = entry;
        }
      }
      catch (Exception e)
      {
        devices.Clear();
        Logger.cs_log("An exception occurred when querying the TG_DEVICE_ID table: " + e.Message);
      }

      return devices;
    }

    public static object Add(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string MACAddress = (string)geo_args.get("mac_address");
      string stationID = (string)geo_args.get("station_id");
      string locationID = geo_args.get<string>("location_id"); // Bug IPAY-876 MJO - Added Location ID
      bool p2peFlag = geo_args.get<bool>("p2pe_flag"); // Bug IPAY-914 MJO - Added P2PE Flag

      if (Devices.ContainsKey(MACAddress))
        return LogAndReturnError("A device is already registered with this MAC address.");

      if (Devices.ContainsKey(stationID))
        return LogAndReturnError("A device already has this Station ID.");
      
      // Bug IPAY-876 MJO - Added Location ID
      // Bug IPAY-914 MJO - Added P2PE Flag
      DeviceEntry entry = new DeviceEntry(MACAddress, 0, stationID, locationID, p2peFlag);

      string err = entry.insertIntoDB();

      if (err == null)
      {
        Devices[MACAddress] = entry;
        Devices[stationID] = entry;
      }
      else
      {
        return LogAndReturnError("Error adding device to TG_DEVICE_ID table: " + err);
      }

      return true;
    }

    public static object Edit(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string MACAddress = (string)geo_args.get("mac_address");
      string stationID = (string)geo_args.get("station_id");
      string locationID = geo_args.get<string>("location_id"); // Bug IPAY-876 MJO - Added Location ID
      bool p2peFlag = geo_args.get<bool>("p2pe_flag"); // Bug IPAY-914 MJO - Added P2PE Flag

      // Bug IPAY-609 MJO - Allow the MAC address change if it's the same device
      if (Devices.ContainsKey(MACAddress) && Devices[MACAddress] != Devices[stationID])
        return LogAndReturnError("A device is already registered with this MAC address.");

      bool deviceUpdated = false;

      foreach (DeviceEntry device in Devices.Values)
      {
        if (device.stationID == stationID)
        {
          string oldMACAddress = device.MACAddress;

          device.MACAddress = MACAddress;
          device.locationID = locationID; // Bug IPAY-876 MJO - Added Location ID
          device.p2peFlag = p2peFlag; // Bug IPAY-914 MJO - Added P2PE Flag

          string err = device.updateDB();

          if (err != null)
          {
            Logger.cs_log("Error updating device in TG_DEVICE_ID: " + err);
          }
          else
          {
            deviceUpdated = true;

            if (oldMACAddress != MACAddress)
            {
              // Bug 20351 MJO - Updated to use .NET ConcurrentDictionary
              Devices.TryRemove(oldMACAddress, out _);
              Devices[MACAddress] = device;
            }
          }

          break;
        }
      }

      return deviceUpdated;
    }

    public static object Delete(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string stationID = (string)geo_args.get("station_id");
      XDocument request = new XDocument();

      bool deleted = false;

      foreach (DeviceEntry device in Devices.Values)
      {
        if (device.stationID == stationID)
        {
          string err = device.deleteFromDB();

          if (err != "")
          {
            return LogAndReturnError("Error deleting device from TG_DEVICE_ID: " + err);
          }
          else
          {
            // Bug 20351 MJO - Updated to use .NET ConcurrentDictionary
            DeviceEntry outEntry = null;
            Devices.TryRemove(device.MACAddress, out outEntry);
            Devices.TryRemove(device.stationID, out outEntry);
            deleted = true;
          }

          break;
        }
      }

      return deleted;
    }

    public static object GetStationId(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string MACAddresses = (string)geo_args.get("mac_addresses", "");

      if (MACAddresses != "")
      {
        foreach (string MACAddress in MACAddresses.Split(','))
        {
          if (Devices.ContainsKey(MACAddress))
          {
            return Devices[MACAddress].stationID;
          }
        }
      }

      return false;
    }

    private static void SetAndLogTField(Transaction transaction, StringBuilder sb, string key, string value)
    {
      transaction.SetTField(key, value);
      sb.AppendLine(String.Format("{0}: {1}", key, value));
    }

    // Bug 21216 MJO - Log full response
    private static string LogResponse(Transaction transaction)
    {
      StringBuilder sb = new StringBuilder();
      sb.AppendLine();

      foreach (string s in transaction.GetResponseFields())
      {
        sb.AppendLine(String.Format("{0}: {1}", s, transaction.GetTField(s)));
      }

      return sb.ToString();
    }

    public static object ProcessPurchase(params object[] args)
    {
      try
      {
        GenericObject geo_args = misc.convert_args(args);

        GenericObject sysint = (GenericObject)geo_args.get("_subject");
        GenericObject tender = (GenericObject)geo_args.get("data_obj");
        decimal total = (decimal)tender.get("total");
        string stationId = (string)geo_args.get("miraserv_station_id");
        Transaction transaction = new Transaction();

        DeviceEntry entry = Devices[stationId];

        string tndrRefNbr = (string)misc.CASL_call("a_method", "System_interface.IPCharge.get_tender_ref_nbr", "args", new GenericObject("_subject", c_CASL.c_object("System_interface.IPCharge"), "a_tender", tender));

        if (tndrRefNbr.Length > 12)
          tndrRefNbr = tndrRefNbr.Substring(tndrRefNbr.Length - 12);

        StringBuilder sb = new StringBuilder();
        sb.AppendLine();
        bool isPurchase = total > 0;

        transaction.Init();

        // Bug 21216 MJO - Added MiraServ logging
        if ((bool)c_CASL.execute("Business.Misc.data.enable_miraserv_logging"))
        {
          string logFilePath = ((string)c_CASL.get_GEO().get("code_base")) + @"/log/mira.log";

          SetAndLogTField(transaction, sb, "LOGPATH", logFilePath);
          SetAndLogTField(transaction, sb, "DEBUG", "1");
        }

        SetAndLogTField(transaction, sb, "StationID", stationId);
        SetAndLogTField(transaction, sb, "TransCode", isPurchase ? "01" : "04");
        SetAndLogTField(transaction, sb, "Amount_1", (Math.Abs(total) * 100).ToString("0.#"));
        SetAndLogTField(transaction, sb, "Invoice_num", tndrRefNbr);

        if (!isPurchase && tender.has("auth_str"))
        {
          string reverseToken = ((string)tender.get("auth_str")).Split('/')[2];
          SetAndLogTField(transaction, sb, "TK", reverseToken);
        }

        if (entry.seqNbr > 0)
          SetAndLogTField(transaction, sb, "TM", entry.seqNbr.ToString());

        Logger.LogInfo(sb.ToString(), "MiraServ " + (isPurchase ? "Purchase" : "Return") + " Request");

        transaction.Process();

        // Bug 21216 MJO - Log full response
        Logger.LogInfo(LogResponse(transaction), "MiraServ " + (isPurchase ? "Purchase" : "Return") + " Response");

        // Bug 21630 MJO - Return communications error
        if (transaction.GetTField("TK") == "")
          return LogAndReturnError(transaction.GetTField("OD") == "PINPAD COMM ERROR" ? "Unable to communicate with pinpad" : "MiraServ Purchase/Return error");

        entry.seqNbr = Convert.ToInt32(transaction.GetTField("TH"));
        entry.updateDB();

        string actionCode = transaction.GetTField("ActionCode");
        string displayMsg = transaction.GetTField("Display_Msg");
        string cardType = transaction.GetTField("AT");
        string creditDebit = transaction.GetTField("CT");
        string receiptRefNbr = transaction.GetTField("ReceiptRefNum");
        string approvalCode = transaction.GetTField("Approval_Cd");
        string name = transaction.GetTField("CardholderName");
        string exp = transaction.GetTField("ExpiryDate");
        string receipt_text = transaction.GetTField("Receipt_Msg_Txt");
        string cust_receipt_text = transaction.GetTField("Cust_Receipt_Msg_Txt");
        string token = transaction.GetTField("TK");

        if (name != null && name.Trim() == "")
          name = "/";

        tender.set("MiraServObj", transaction);

        if (actionCode == "A")
        {
          tender.set(
            "auth_str", approvalCode + "/" + receiptRefNbr + "/" + token,
            "approval_code", "TROUTD:" + receiptRefNbr,
            "miraserv_receipt_text", StringCompressor.CompressString(receipt_text),
            "miraserv_cust_receipt_text", StringCompressor.CompressString(cust_receipt_text)
          );

          if ((bool)sysint.get("optional_field_storage", true, true))
          {
            if (exp != null && exp.Length == 4)
              tender.set(
                "exp_month", Convert.ToInt32(exp.Substring(0, 2)),
                "exp_year", Convert.ToInt32(exp.Substring(2, 2))
              );
            else
              tender.set(
                "exp_month", c_CASL.c_opt,
                "exp_year", c_CASL.c_opt
              );

            if (name != null && name.Length > 0)
            {
              if (name.Split('/').Length == 2)
                tender.set(
                  "payer_name", name,
                  "payer_lname", name.Split('/')[0],
                  "payer_fname", name.Split('/')[1]
                );
              else
                tender.set(
                  "payer_name", name,
                  "payer_lname", name
                );
            }
            else
            {
              tender.set(
                "payer_name", c_CASL.c_opt,
                "payer_lname", c_CASL.c_opt,
                "payer_fname", c_CASL.c_opt
              );
            }
          }
          else
          {
            tender.set(
              "exp_month", c_CASL.c_opt,
              "exp_year", c_CASL.c_opt,
              "payer_name", c_CASL.c_opt,
              "payer_lname", c_CASL.c_opt,
              "payer_fname", c_CASL.c_opt
            );
          }
        }
        else
        {
          tender.set("miraserv_decline_receipt_text", cust_receipt_text);
        }

        return new GenericObject("action_code", actionCode, "display_msg", displayMsg, "card_type", cardType);
      }
      catch (Exception e)
      {
        return LogAndReturnError("MiraServ Purchase/Return error: " + e.Message);
      }
    }

    public static object ProcessConfirm(params object[] args)
    {
      try
      {
        GenericObject geo_args = misc.convert_args(args);

        GenericObject tender = (GenericObject)geo_args.get("data_obj");
        GenericObject coreEvent = (GenericObject)misc.CASL_call("subject", tender, "a_method", "get_core_event", "args", new GenericObject());
        Transaction transaction = (Transaction)tender.get("MiraServObj");

        if (transaction.GetTField("TransCode") == "03")
        {
          bool connectionOpened = false;
          ConnectionTypeSynch pDBConn_Data = new ConnectionTypeSynch((GenericObject)c_CASL.c_object("TranSuite_DB.Data.db_connection_info"));
          string strErr = "";

          if (!(connectionOpened = pDBConn_Data.EstablishDatabaseConnection(ref strErr)))  // Bug 15941
            TranSuiteServices.TranSuiteServices.HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1063", strErr);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix

          if (!pDBConn_Data.EstablishRollback(ref strErr))
            TranSuiteServices.TranSuiteServices.ThrowCASLErrorAndCloseDB("TS-1064", ref pDBConn_Data, "Error creating database transaction.", strErr);

          strErr = UpdateReceiptText(tender, (string)coreEvent.get("receipt_nbr"), "miraserv_receipt_text", pDBConn_Data);

          if (strErr != "")
            return LogAndReturnError(strErr);

          strErr = UpdateReceiptText(tender, (string)coreEvent.get("receipt_nbr"), "miraserv_cust_receipt_text", pDBConn_Data);

          if (strErr != "")
            return LogAndReturnError(strErr);

          if (!pDBConn_Data.CommitToDatabase(ref strErr))
            TranSuiteServices.TranSuiteServices.HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-1066", strErr);

          Logger.LogInfo("MiraServ void receipt text set in database.", null);
        }

        transaction.Confirm();

        tender.remove("MiraServObj");

        Logger.LogInfo("MiraServ transaction confirmed.", null);

        return true;
      }
      catch (Exception e)
      {
        return LogAndReturnError("Unable to confirm MiraServ transaction :" + e.ToMessageAndCompleteStacktrace());
      }
    }

    private static string UpdateReceiptText(GenericObject tender, string receiptNbr, string custid, ConnectionTypeSynch pDBConn_Data)
    {
      HybridDictionary sqlArgs = new HybridDictionary();

      string sql = "UPDATE CUST_FIELD_DATA SET CUSTVALUE=@receipt_text WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND EVENTNBR=@eventnbr AND POSTNBR=@postnbr and CUSTID=@custid";
      sqlArgs["receipt_text"] = (string)tender.get(custid);
      sqlArgs["depfilenbr"] = TranSuiteServices.TranSuiteServices.GetFileNumber(receiptNbr.Split('-')[0]);
      sqlArgs["depfileseq"] = TranSuiteServices.TranSuiteServices.GetFileSequence(receiptNbr.Split('-')[0]);
      sqlArgs["eventnbr"] = receiptNbr.Split('-')[1];
      sqlArgs["postnbr"] = (int)tender.get("seq_nbr");
      sqlArgs["custid"] = custid;

      string error_message = "";
      int count = ParamUtils.runExecuteNonQuery(pDBConn_Data, sql, sqlArgs);
      if (count != 1)
      {
        return "Failed to update " + custid;
      }

      return error_message;
    }

    public static object ProcessReverse(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      GenericObject tender = (GenericObject)geo_args.get("data_obj");

      try
      {
        Transaction transaction = (Transaction)tender.get("MiraServObj");

        transaction.Reverse();
      }
      catch (Exception e)
      {
        LogAndReturnError("Unable to reverse MiraServ transaction :" + e.ToMessageAndCompleteStacktrace());
        return false;
      }

      // Always remove the MiraServObj and return true, as it will automatically reverse after a timeout
      tender.remove("MiraServObj", false);

      Logger.LogInfo("MiraServ transaction reversed.", null);

      return true;
    }

    public static object CASL_ProcessVoid(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      GenericObject tender = (GenericObject)geo_args.get("data_obj");
      var stationId = geo_args.get("miraserv_station_id");

      // Bug 26678 MJO - If debit, this specific device must be used
      if (stationId is string)
      {
        return ProcessVoid(tender, stationId as string);
      }

      object R = null;

      foreach (string key in Devices.Keys)
      {
        R = ProcessVoid(tender, key);

        if (!(R is GenericObject && (R as GenericObject).isError() && ((string)(R as GenericObject).get("message")).Contains("error")))
          return R;
      }

      if (R == null)
        R = LogAndReturnError("No devices found!");

      return R;
    }

    public static object ProcessVoid(GenericObject tender, string stationId)
    {
      try
      {
        string approvalCode = ((string)tender.get("auth_str")).Split('/')[0].ToUpper().PadLeft(6, '0');
        string token = ((string)tender.get("auth_str")).Split('/')[2];

        Transaction transaction = new Transaction();

        DeviceEntry entry = Devices[stationId];

        decimal total = Convert.ToDecimal(tender.get("total"));
        bool isPurchase = total > 0;
        StringBuilder sb = new StringBuilder();
        sb.AppendLine();

        transaction.Init();
        SetAndLogTField(transaction, sb, "StationID", stationId);
        SetAndLogTField(transaction, sb, "TransCode", isPurchase ? "03" : "06");
        SetAndLogTField(transaction, sb, "Amount_1", (Math.Abs(total) * 100).ToString("0.#"));
        SetAndLogTField(transaction, sb, "Approval_Cd", approvalCode);
        SetAndLogTField(transaction, sb, "TK", token);

        if (entry.seqNbr > 0)
          SetAndLogTField(transaction, sb, "TM", entry.seqNbr.ToString());

        Logger.LogInfo(sb.ToString(), "MiraServ " + (isPurchase ? "Purchase" : "Return") + " Void Request");

        transaction.Process();

        // Bug 21216 MJO - Log full response
        Logger.LogInfo(LogResponse(transaction), "MiraServ " + (isPurchase ? "Purchase" : "Return") + " Void Response");

        if (transaction.GetTField("TK") == "")
          return LogAndReturnError("MiraServ Purchase/Return Void error");

        entry.seqNbr = Convert.ToInt32(transaction.GetTField("TH"));
        entry.updateDB();

        entry.seqNbr = Convert.ToInt32(transaction.GetTField("TH"));
        entry.updateDB();

        string actionCode = transaction.GetTField("ActionCode");
        string displayMsg = transaction.GetTField("Display_Msg");
        string approvalCodeVoid = transaction.GetTField("Approval_Cd");
        string receipt_text = transaction.GetTField("Receipt_Msg_Txt");
        string cust_receipt_text = transaction.GetTField("Cust_Receipt_Msg_Txt");

        if (actionCode == "A")
        {
          tender.set(
            "MiraServObj", transaction,
            "auth_str", (string)tender.get("auth_str") + "/" + approvalCodeVoid,
            "miraserv_receipt_text", StringCompressor.CompressString(receipt_text),
            "miraserv_cust_receipt_text", StringCompressor.CompressString(cust_receipt_text),
            "miraserv_update_receipt_text", true
          );
        }
        else if (actionCode == "D")
        {
          tender.set("miraserv_decline_receipt_text", cust_receipt_text);
        }

        return new GenericObject("action_code", actionCode, "display_msg", displayMsg);
      }
      catch (Exception e)
      {
        return LogAndReturnError("MiraServ Purchase/Return Void error: " + e.Message);
      }
    }

    private static GenericObject LogAndReturnError(string message)
    {
      Logger.cs_log(String.Format("Eigen MiraServ system interface error occurred: {0}",
        message));
      return (CASL_error.create_str(message)).casl_object;
    }

    // Bug IPAY-876 MJO - Added Location ID field/column
    // Bug IPAY-914 MJO - Added P2PE Flag field_column
    private class DeviceEntry
    {
      public string MACAddress;
      public int seqNbr;
      public string stationID;
      public string locationID { get; set; }
      public bool p2peFlag { get; set; }

      public DeviceEntry(string MACAddress, int seqNbr, string stationID, string locationID, bool p2peFlag)
      {
        this.MACAddress = MACAddress;
        this.seqNbr = seqNbr;
        this.stationID = stationID;
        this.locationID = locationID;
        this.p2peFlag = p2peFlag;
      }

      public DeviceEntry(GenericObject dbEntry)
      {
        stationID = (string)dbEntry.get("SERIAL_NBR");
        string[] args = ((string)dbEntry.get("EXTERNAL_ID")).Split('|');
        MACAddress = args[0];
        seqNbr = Convert.ToInt32(args[1]);
        locationID = dbEntry.get("LOCATION_ID", "").ToString();
        p2peFlag = (bool)dbEntry.get("P2PE_FLAG", false);
      }

      public string insertIntoDB()
      {
        GenericObject database = c_CASL.c_object("TranSuite_DB.Config.db_connection_info") as GenericObject;
        Hashtable sqlArgs = new Hashtable();

        // Bug IPAY-1431 MJO - Added missing column
        string sql = "INSERT INTO TG_DEVICE_ID (DEVICE_TYPE, SERIAL_NBR, EXTERNAL_ID, LOCATION_ID, P2PE_FLAG) VALUES ('MIRASERV', @serial_nbr, @external_id, @location_id, @p2pe_flag)";
        sqlArgs["serial_nbr"] = stationID;
        sqlArgs["external_id"] = MACAddress + "|" + seqNbr.ToString();
        sqlArgs["location_id"] = locationID;
        sqlArgs["p2pe_flag"] = p2peFlag;

        string error_message = null;
        IDBVectorReader reader = db_reader.Make(database, sql, CommandType.Text, 0,
          sqlArgs, 0, 0, null, ref error_message);

        reader.ExecuteNonQuery(ref error_message);
        reader.CloseReader();

        return error_message;
      }

      public string updateDB()
      {
        GenericObject database = c_CASL.c_object("TranSuite_DB.Config.db_connection_info") as GenericObject;
        Hashtable sqlArgs = new Hashtable();

        string sql = "UPDATE TG_DEVICE_ID SET EXTERNAL_ID=@external_id, LOCATION_ID = @location_id, P2PE_FLAG = @p2pe_flag WHERE DEVICE_TYPE='MIRASERV' AND SERIAL_NBR=@serial_nbr";
        sqlArgs["serial_nbr"] = stationID;
        sqlArgs["external_id"] = MACAddress + "|" + seqNbr.ToString();
        sqlArgs["location_id"] = locationID;
        sqlArgs["p2pe_flag"] = p2peFlag;

        string error_message = null;
        IDBVectorReader reader = db_reader.Make(database, sql, CommandType.Text, 0,
          sqlArgs, 0, 0, null, ref error_message);

        reader.ExecuteNonQuery(ref error_message);
        reader.CloseReader();

        return error_message;
      }

      public string deleteFromDB()
      {
        GenericObject database = c_CASL.c_object("TranSuite_DB.Config.db_connection_info") as GenericObject;
        Hashtable sqlArgs = new Hashtable();

        string sql = "DELETE FROM TG_DEVICE_ID WHERE DEVICE_TYPE='MIRASERV' AND SERIAL_NBR=@serial_nbr";
        sqlArgs["serial_nbr"] = stationID;

        string error_message = "";
        IDBVectorReader reader = db_reader.Make(database, sql, CommandType.Text, 0,
          sqlArgs, 0, 0, null, ref error_message);

        reader.ExecuteNonQuery(ref error_message);
        reader.CloseReader();

        return error_message;
      }
    }
  }
}
