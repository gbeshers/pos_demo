
<!--
Convert CASL object to a SQL insert statement.
First gets current database version.
If new, then insert.
If existing, then update rows that need to be changed.
How to handle multiple references to that object.
  Typically avoid them.  Containers in CASL could have multiple
  references.

Bank_rule.of."1".<to_path/>

Business.Bank_rule.of."1"

container                 field_key     field_value revision
========================= ============= =========== ========
Business.Bank_rule.of."1" null          null        0
Business.Bank_rule.of."1" "id"          "1"         0
Business.Bank_rule.of."1" "title"       "Standard Banking"
Business.Bank_rule.of."1" "description" "Cash and Check"

#1: create table
#2: insert data into table
#3: thaw
#4: freeze
#5: UI

Business_app.Manage_data
 Create
 Update

Business_app.Config
-->


db_vector.<query_all> 
 sql_statement=<![CDATA[
  INSERT INTO [flex] (container,field_key,field_value)  values('Business.Bank_rule.of."9"', null, null);
  INSERT INTO [flex] (container,field_key,field_value)  values('Business.Bank_rule.of."9"', '"_parent"', 'Business.Bank_rule');
  INSERT INTO [flex] (container,field_key,field_value)  values('Business.Bank_rule.of."9"', '"id"', '"9"');
  INSERT INTO [flex] (container,field_key,field_value)  values('Business.Bank_rule.of."9"', '"title"', '"Standard Banking"');
  INSERT INTO [flex] (container,field_key,field_value)  values('Business.Bank_rule.of."9"', '"description"', '"Cash and Check"');
 ]]>
 db_connection=TranSuite_DB.Config.db_connection_info
</query_all>

db_vector.<query_all> 
 sql_statement=<![CDATA[
  SELECT * FROM [flex] where container='Business.Bank_rule.of."9"';
 ]]>
 db_connection=TranSuite_DB.Config.db_connection_info
</query_all>

Business.Bank_rule.of."1".<to_path/>

<!-- Issues:
  Should _container be in data?  Yes.  Must be in lower levels.  In top-level, _container is the
  db object, but to_path will ignore the db object.
-->



