﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using CASL_engine;
using SQL_config_NS;
using Config_NG;

namespace baseline
{
  class Config
  {
    static public object fCASL_thaw_with_path_ng(CASL_Frame frame)
    {
      Object db_obj = frame.args.get("db_info");
      if (!(db_obj is GenericObject))
        throw CASL_error.create_str("thaw_with_path_ng requires a GenericObject");
      String rebase = frame.args.get("rebase", "") as String;
      if (rebase == null)
        throw CASL_error.create_str("rebase must be a string");
      int version = (int) frame.args.get("version", 1);
      Stopwatch t_w_p_Stopwatch = frame.args.get("t_w_p_stopwatch", null) as Stopwatch;

      GenericObject DB = db_obj as GenericObject;

      string database_name = DB.get<string>("database_name");
      string datasource_name = DB.get<string>("datasource_name");
      string login_name = DB.get<string>("login_name");
      string login_password = DB.get<string>("login_password");

      Db_Connection_Info database = new Db_Connection_Info(
                       database_name, datasource_name, login_name, login_password);

      Object obj = c_CASL.GEO.get_path("Business.Tender.Cash", false, null);
      if (obj == null)
        Debugger.Break();
            

      try {
        ConfigMod.thaw_with_path_ng<Db_Connection_Info>(DB, database, rebase, version, t_w_p_Stopwatch);
      }
      catch (CASL_error ce) {
        Logger.cs_log("fCASL_thaw_with_path_ng: " + ce.casl_object.get("message") as string);
      } catch (Exception e) {
        Logger.cs_log("fCASL_thaw_with_path_ng: " + e.ToMessageAndCompleteStacktrace());
      }
 
      return null;
    }

    static public object fCASL_acquire_app(CASL_Frame frame)
    {
      object obj = frame.args.get("_subject", null);
      string session_id = misc.get_session_id();
      GenericObject app_user = frame.args.get("app_user") as GenericObject;

      if (obj == null) {
        Logger.cs_log("fCASL_release_app no _subject");
        return false;
      }
      GenericObject config = obj as GenericObject;
      if (config == null) {
        Logger.cs_log("fCASL_release_app _subject is not a GenericObject");
        return false;
      }
      GenericObject lock_obj = config.get("_in_use_f_lock", null)
        as GenericObject;
      if (lock_obj == null) {
        Logger.cs_log("fCASL_release_app _subject is not a GenericObject");
        return false;
      }
      
      GenericObject previous_user = null;
      lock (lock_obj) {
        object in_use_obj = config.get("_in_use", null);
        GenericObject current_user =
          lock_obj.get("current_user", null) as GenericObject;

        if (in_use_obj is bool && current_user != null) {
          if ((bool) in_use_obj && current_user != app_user)
            previous_user = current_user;
          else {
            string mSID = c_CASL.GEO.get_path("my.SessionID") as string;
#if DEBUG
            if (session_id != mSID)
              Logger.cs_log("fCASL_aquire_app session_id != my.SessionID " +
                        session_id + " != " + mSID);
#endif
            config.set("_in_use", mSID);
            lock_obj.set("current_user", app_user);
          }
        } else
          Logger.cs_log("fCASL_acquire_app wrong types");
      }
      if (previous_user == null)
        return false;
      return previous_user.get("id", false);
    }

    static public object fCASL_release_app(CASL_Frame frame)
    {
      object obj = frame.args.get("_subject", null);
      if (obj == null) {
        Logger.cs_log("fCASL_release_app no _subject");
        return null;
      }
      GenericObject config = obj as GenericObject;
      if (config == null) {
        Logger.cs_log("fCASL_release_app _subject is not a GenericObject");
        return null;
      }
      GenericObject lock_obj = config.get("_in_use_f_lock", null)
        as GenericObject;
      if (lock_obj == null) {
        Logger.cs_log("fCASL_release_app _subject is not a GenericObject");
        return null;
      }

      lock (lock_obj) {
        config.set("_in_use", false);
      }
      return null;
    }
  }
}
