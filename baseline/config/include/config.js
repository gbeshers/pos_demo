// This file - config.js
// Config-specific JS should go here

// Bug 21952 UMN added this to handle help that has html in it so can use same
// technique for html editor and formula editor.
var help_easymde = null;
var label_easymde = null;
var prompt_easymde = null;
var prompt_bottom_easymde = null;
var template_easymde = null;
var formula_easymde = null;
var core_event_prompt_easymde = null;
var cbc_terms_and_conditions_easymde = null;  // IPAY-57 
var cbc_disclaimer_easymde = null;            // IPAY-57 
var total_f_label_easymde = null;
var description_easymde = null;
var btn_text_easymde = null;

// Bug 12098 - iPayment Baseline: Addition of Formulas [DJD]
//    The "formula_validation_handler" (current_site + "/get_formula_validation.htm?")
//    is used to POST the formula (the "Body" of request) to be validated by the CSharp function
//    "ProcessRequest(System.Web.HttpContext context)" in the "handle_request_2.cs" file.  The formula
//    is then passed to "misc.ValidateFormulaMethods_aux" for validation and the result is written to
//    the "responseText."  The string "VALID!" is returned if the formula successfully validates.
//    Otherwise: Either an XML parsing error message (identified by string "Parsing Error: ") or the
//    first invalid method found is returned.
function ValidateFormula(display_valid_alert) {
  "use strict";
  var formula_validation_handler = get_current_site() + "/get_formula_validation.htm?";
  var xml_obj = null;
  var strFormula = "";
  
  // need to get formula from the global formula easymde
  if (formula_easymde !== null)
  {
    strFormula = $.trim(formula_easymde.value());
  }
  else
  {
    var textareaSel = $('form').find("#amount_formuladisplay");
    strFormula = $.trim(textareaSel.val());
  }
  
  if (window.XMLHttpRequest) {
    xml_obj = new XMLHttpRequest();
  }
  else if (window.ActiveXObject) {
    xml_obj = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xml_obj.open("POST", formula_validation_handler, false);
  xml_obj.send(strFormula);
  var strRV = xml_obj.responseText;

  if (strRV.indexOf("VALID!") != -1) {
    var strMsg = "The formula is valid.";
    if (display_valid_alert) {
      if (strRV.indexOf("TEST VALUES RESULT: ") != -1) {
        strMsg = "\r\n\r\nFormula test values result: " + strRV.substring(strRV.indexOf(": ") + 1);
      }
      else {
        if (strRV.indexOf("TEST VALUES ERROR: ") != -1) {
          strMsg = "\r\n\r\nERROR - Formula execution with test values:\r\n\r\n" +
            strRV.substring(strRV.indexOf(": ") + 2);
        }
      }
    }
    enqueue_alert_message(strMsg);
    return true;
  }
  else {
    if (strRV.indexOf("Parsing Error: ") == -1) {
      enqueue_alert_message("The following method is not valid and cannot be used in a formula: " + strRV);
    }
    else {
      enqueue_alert_message(strRV);
    }
    return false;
  }
}

// Bug 18942 UMN Migrate the html editor to easymde so that we can support Chrome
// This function creates an easymde editor from a given textarea. It takes the
// textarea id, the id of the field where we place the encoded value that goes back to the server,
// and the placeholder text to be displayed in the easymde editor.
function make_easymde_textarea(textarea_id, enc_id, placeholder, is_help, is_formula, easymde)
{
  "use strict";
  var textareaSel = $('form').find(textarea_id);
  var tool_bar = [];
  if (textareaSel.length === 0) {
    return null;
  }
  var idEncSel = $('form').find(enc_id);
  if (idEncSel.length > 0)
  {
    // hide encoding field's span so can also hide the stupid dash that Config uses for input fields
    idEncSel.parent().hide();
  }

  if (textareaSel.is('[disabled=true]'))
    return null;
  
  if (is_help)
  {
    tool_bar = ['bold', 'italic', 'heading', 'heading-bigger', 'heading-smaller', 'horizontal-rule', 'code', 'quote',
      'unordered-list', 'ordered-list', 'table', 'clean-block', 'undo', 'redo', 'preview', 'side-by-side', 'fullscreen', 'guide' ];
  }
  else if (is_formula)
  {
    tool_bar = [{
      name: "validate",
      action: function validate_formula(editor){
        try {
          ValidateFormula(true);
        } catch (e) {
          alert("Couldn't call server to validate formula");
        }
      },
      className: "fa fa-check-circle",
      title: "Validate Formula"
    },
    "|", // Separator
    {
      name: "undo",
      action: EasyMDE.undo,
      className: "fa fa-undo",
      noDisable: true,
      title: "Undo",
    },
    {
      name: "redo",
      action: EasyMDE.redo,
      className: "fa fa-repeat fa-redo",
      noDisable: true,
      title: "Redo",
    },
    {
      name: "fullscreen",
      action: EasyMDE.toggleFullScreen,
      className: "fa fa-arrows-alt",
      noDisable: true,
      noMobile: true,
      title: "Toggle Fullscreen",
      "default": true,
    }
    ];
  }
  else
  {
    tool_bar = ['undo', 'redo', 'preview', 'side-by-side', 'fullscreen'];
  }

  if (textareaSel.length > 0)
  {
    return new EasyMDE({
      autofocus: false, // Bug 21952 MJO - Don't focus on it
      element: textareaSel[0],
      toolbar: tool_bar,
      lineWrapping: true,
      placeholder: placeholder
    });
  }
}

// Bug 18942 UMN Migrate the html editor to easymde so that we can support Chrome
// This function updates the encoded input field with the text from an easymde editor from a given textarea. It takes the
// textarea id, the id of the field where we place the encoded value that goes back to the server,
// a global reference for the easymde editor, and the encoding function.
function update_from_easymde(textarea_id, enc_id, which_easymde, which_encode)
{
  var textareaSel = $('form').find(textarea_id);
  var encIdSel = $('form').find(enc_id);
  if (textareaSel.length > 0 && encIdSel.length > 0)
  {
    // grab text from easymde editor
    if (which_easymde !== null)
    {
      which_easymde.toTextArea();
      which_easymde = null;       // set to null to keep event from firing
    }
    var value = $.trim(textareaSel.val());               // handle empty string which base encodes as 0x
    var enc = (value.length) ? which_encode(value): "";  // base64 or base16 encode (so backward compatible)
    encIdSel.val(enc);
    textareaSel.val("");
  }
}

// Bug 18942 UMN Migrate the html editor to easymde so that we can support Chrome
// This function sets up the easymde editors and the update function calls.
$(document).ready(function() {

  // limit to config editing form (ui="Editing-htm_inst") or formula form
  var editingFormSel = $("form[ui='Editing-htm_inst'],div[ui='Editing-htm_inst']");
  var amountFormulaSel = $("#amount_formuladisplay");
  if (editingFormSel.length > 0 || amountFormulaSel.length > 0)
  {
    // Make our various easymdes. Due to the form handler, we'll need some ugh globals
    // reverse typical order so first easymde is initialized last
    help_easymde = make_easymde_textarea("#help", "input.help_enc", "No help defined", true, false);
    btn_text_easymde = make_easymde_textarea("#btn_text_htmldisplay", "input.btn_text_htmldisplay_enc", 
      "No card view description defined", false, false);
    description_easymde = make_easymde_textarea("#description_htmldisplay", "input.description_htmldisplay_enc", 
      "No description defined", false, false);
    template_easymde = make_easymde_textarea("#detail_template_htmldisplay", "input.detail_template_htmldisplay_enc", 
      "No template defined", false, false);
    prompt_bottom_easymde = make_easymde_textarea("#prompt_bottom_htmldisplay", "input.prompt_bottom_htmldisplay_enc", 
      "No prompt bottom defined", false, false);
    prompt_easymde = make_easymde_textarea("#prompt_htmldisplay", "input.prompt_htmldisplay_enc", 
      "No prompt defined", false, false);
    label_easymde = make_easymde_textarea("#label_htmldisplay", "input.label_htmldisplay_enc", 
      "Label has not been customized", false, false);
    formula_easymde = make_easymde_textarea("#amount_formuladisplay", "input.amount_formuladisplay_enc", 
      "Formula has not been configured", false, true);
    core_event_prompt_easymde = make_easymde_textarea("#core_event_prompt_htmldisplay", "input.core_event_prompt_htmldisplay_enc", 
      "No core event prompt defined", false, false);
    cbc_terms_and_conditions_easymde = make_easymde_textarea("#cbc_terms_and_conditions_htmldisplay", "input.cbc_terms_and_conditions_htmldisplay_enc", 
      "No terms and conditions defined", false, false);   // IPAY-57    
    cbc_disclaimer_easymde = make_easymde_textarea("#cbc_disclaimer_htmldisplay", "input.cbc_disclaimer_htmldisplay_enc", 
      "No disclaimer defined", false, false);             // IPAY-57
    total_f_label_easymde = make_easymde_textarea("#total_f_label_htmldisplay", "input.total_f_label_htmldisplay_enc", 
      "No core event prompt defined", false, false);

    // set focus to first input
    $(':input:enabled:visible:not([readonly]).first').focus();

    $("form").on('submit', function(e) {
      //IPAY-357 NAM: if an error dialog is being displayed stop the form submit/post. Otherwise, the hmtl will be removed by update_from_easymde
      if ( hasValidationErrorMessage() == false)
      {
        // Help easymde editor
        update_from_easymde("#help", "input.help_enc", help_easymde, Base64.encode);

        // Custom field easymde editors
        update_from_easymde("#btn_text_htmldisplay", "input.btn_text_htmldisplay_enc", btn_text_easymde, ConvertToBase16);
        update_from_easymde("#description_htmldisplay", "input.description_htmldisplay_enc", description_easymde, ConvertToBase16);
        update_from_easymde("#detail_template_htmldisplay", "input.detail_template_htmldisplay_enc", template_easymde, ConvertToBase16);
        update_from_easymde("#prompt_bottom_htmldisplay", "input.prompt_bottom_htmldisplay_enc", prompt_bottom_easymde, ConvertToBase16);
        update_from_easymde("#prompt_htmldisplay", "input.prompt_htmldisplay_enc", prompt_easymde, ConvertToBase16);
        update_from_easymde("#core_event_prompt_htmldisplay", "input.core_event_prompt_htmldisplay_enc", core_event_prompt_easymde, ConvertToBase16);     
        update_from_easymde("#cbc_terms_and_conditions_htmldisplay", "input.cbc_terms_and_conditions_htmldisplay_enc", cbc_terms_and_conditions_easymde, ConvertToBase16);  // IPAY-57
        update_from_easymde("#cbc_disclaimer_htmldisplay", "input.cbc_disclaimer_htmldisplay_enc", cbc_disclaimer_easymde, ConvertToBase16);                                // IPAY-57
        update_from_easymde("#total_f_label_htmldisplay", "input.total_f_label_htmldisplay_enc", total_f_label_easymde, ConvertToBase16);
        update_from_easymde("#label_htmldisplay", "input.label_htmldisplay_enc", label_easymde, ConvertToBase16);

        // formula easymde editor
        update_from_easymde("#amount_formuladisplay", "input.amount_formuladisplay_enc", formula_easymde, ConvertToBase16);
      }
    });
  }
});

// Bug 24537 UMN next several functions
function make_instances_table(table_id, use_checkbox) {
  var tableSel = '#' + table_id;
  var tableFiltSel = tableSel + "_filter input";
  var emptyTable = table_id.replace(/htm_instances/, "No ").replace(/_/g, " ").replace(/$/, " configured");

  var table = $(tableSel).DataTable({
    data: data,
    autoWidth: false,
    responsive: false,
    stateSave: true,
    lengthMenu: [[25, 15, 50, -1], [25, 15, 50, 'All']],
    order: [[3, 'asc']],
    dom: 'filtp',
    columns: columnsTH,
    language: {
      "emptyTable": emptyTable
    },
    columnDefs: [
      {
        targets: 0,
        searchable: false,
        orderable: false,
        visible: true,
        data: null,
        width: '32px',      // set open button column width
        defaultContent: '<button type="button" class="b_off b_color1_b_off b_short_white button_link" style="min-width: 30px;" onkeydown="return button_keydown(event, this);" value="Open">Open</button>'
      },
      {
        targets: 1,         // checkbox
        visible: use_checkbox ? true : false,
        render: function ( data, type, row, meta ) {
          if (use_checkbox !== true)
            return "";
          return '<input type="checkbox" name="selected.' + "'" + data + "'" + '" id="selected.' + "'" + data + "'" + '" />';
        },
        searchable: false,
        width: '20px'       // set checkbox column width
      },
      {
        targets: 2,         // status (modified, created or deleted)
        visible: true,
        searchable: true,
        width: '60px'       // set status column width
      },
      { //Bug 25941 - NAM: use 'natural' sorting for columns
        targets: 3,
        type: 'natural'
      }
    ],
    'createdRow': function (row, data, dataIndex) {
      if (data[2] !== "") {
        $(row).addClass(data[2]);
      }
    }
  });

  $('.htm_instances tbody').on('click', 'button', function () {
    var data = table.row($(this).parents('tr')).data();
    var href = caller + data[0];
    if (!(!event.detail || event.detail == 1)) {
      e.stopPropagation(); return false;
    }
    window.onbeforeunload = null;
    setTimeout(function () {
      if (typeof resetTimeout !== 'undefined')
        resetTimeout();
      dequeue_message();
    }, 100);
    button_click(href, null);
    cover();
  });
  // table.responsive.recalc();
  $(tableFiltSel).focus();
}

// Used with Manage_vector
function make_objs_table(table_id, _caller, _columnsTH, _data, ordered, edit_inline, path) {
  var tableSel = '#' + table_id;
  var emptyTable = table_id.replace(/htm_objs/, "No ").replace(/_/g, " ").replace(/$/, " configured");

  for (i = 0; i < _columnsTH.length; i++)
    _columnsTH[i].title = decode_html_chars(_columnsTH[i].title);

  var editCols = [];

  for (i = 1; i < _columnsTH.length - 1; i++)
    editCols.push(i);

  var buttonHTML = '<button type="button" class="b_off b_color1_b_off b_short_white button_link delete_edit" style="width: 66px;" onkeydown="return button_keydown(event, this);"' +
    (edit_inline ? 'value="Delete" >Delete' : 'value="Edit" >Edit') +
    '</button>';

  var table = $(tableSel).DataTable({
    data: _data,
    scrollY: '500px',           // smooth scrolling when dragging hits edge
    scrollX: true,
    scrollCollapse: true,       // for small lists allow table to collapse down so footer isn't hanging far away
    autoWidth: true,
    paging: false,
    responsive: false,
    stateSave: false,
    rowReorder:                 // Drag and drop extension
      edit_inline ?
        ordered ?
          {
            snapX: true,
            selector: 'td.reorder',
            placeholder: 'sortable-placeholder'
          } : false
        : {
            selector: 'td.reorder'
          },
    dom: 'itp',                  // no filter etc because we have to move rows up by dragging/dropping
    columns: _columnsTH,
    language: {
      "emptyTable": emptyTable
    },
    columnDefs: [
      {
        searchable: false,
        orderable: false,
        visible: true,
        render: function (data, type, full, meta) {
          return $('<div/>').html(data).text();
        },
        className: edit_inline ? 'editable' : 'reorder',
        targets: editCols
      },
      {
        targets: -1,
        searchable: false,
        orderable: false,
        visible: true,
        data: null,
        defaultContent: buttonHTML
      },
      { orderable: ordered, className: ordered ? 'reorder' : '', visible: edit_inline ? true : false, width: '32px', targets: 0 }
    ]
  });

  // Call Edit/Delete button on row
  var onBtnClick = function () {
    if (edit_inline) {
      var deleteThis = $(this).text() === "Delete";
      var thisRow = $(this).closest("tr");

      if (deleteThis) {
        $(this).text("Undelete");

        if (thisRow.find("input.isDeleted").length === 0) {
          var index = thisRow.children().first().text();
          thisRow.find("button.delete_edit").parent().append($("<input type='hidden' name='data." + index + ".deleted' class='isDeleted' value='false' />"));
        }

        thisRow.addClass("deleted").find("input.isDeleted").val("true");
        thisRow.find("td.editable input").attr("disabled", true).css("text-decoration", "line-through");
      }
      else {
        $(this).text("Delete");
        thisRow.removeClass("deleted").find("input.isDeleted").val("false");
        thisRow.find("td.editable input").removeAttr("disabled").css("text-decoration", "");
      }
    }
    else {
      var data = table.row($(this).parents('tr')).data();

      // handle tender category restriction message
      if (data[data.length - 1].indexOf("Error:") === 0) {
        enqueue_alert_message(data[data.length - 1]);
        return false;
      }
      var href = _caller + data[data.length - 1];
      if (!(!event.detail || event.detail == 1)) {
        e.stopPropagation(); return false;
      }
      window.onbeforeunload = null;
      setTimeout(function () {
        if (typeof resetTimeout !== 'undefined')
          resetTimeout();
        dequeue_message();
      }, 100);
      button_click(href, null);
      cover();
    }
  };

  $(tableSel + ' button').on('click', onBtnClick);

  //table.responsive.recalc();

  if (edit_inline) {
    table.on('row-reorder', function (e, diff, edit) {
      for (var i = 0; i < diff.length; i++) {
        var rowDiff = diff[i];

        $(rowDiff.node).find("input").each(function (index, value) {
          $(value).attr("obj", $(value).attr("obj").replace("values." + rowDiff.oldPosition, "values." + rowDiff.newPosition))
            .attr("id", $(value).attr("id").replace("data." + rowDiff.oldPosition, "data." + rowDiff.newPosition))
            .attr("name", $(value).attr("name").replace("data." + rowDiff.oldPosition, "data." + rowDiff.newPosition));
        });

        // add modified class to any rows that have moved
        $(rowDiff.node).addClass('modified');
      }
    });

    // add modified class to any row that has edits
    $(tableSel).find('input, select').change(function() {
      $(this).closest('tr').addClass('modified');
    });

    // Rewrite Add New button
    $('button[value="Add New"]').replaceWith('<button id="AddNew" type="button" class="b_off b_color1_b_off  b_tall_white button_link" onkeydown="return button_keydown(event, this);" value="Add New">Add New</button>');
    $('#AddNew').on('click', function () {
      var dt = $(tableSel).DataTable();
      var lastRowData = dt.row(':last').data();
      var newRow = [];
      var lastIndex = lastRowData[0];
      var newIndex = lastIndex + 1;

      newRow[0] = newIndex;

      for (i = 1; i < lastRowData.length; i++) {
        newRow[i] = lastRowData[i].replace("values." + lastIndex, "values." + newIndex).replace(new RegExp("data." + lastIndex, "g"), "data." + newIndex);
      }

      dt.row.add(newRow).draw(false);

      $(tableSel).find("tr").last().find("td.editable input").val("");
      $(tableSel).find("tr").last().find("button").on("click", onBtnClick);
    });
  }
  else {
    // Rewrite update button
    $('button[value="Update"]').replaceWith('<button id="Update" type="button" class="b_off b_color1_b_off  b_tall_white button_link" onkeydown="return button_keydown(event, this);" value="Update">Update</button>');

    // add modified class to every reordered row
    table.on('row-reorder', function (e, diff, edit) {
      for (var i = 0; i < diff.length; i++) {
        $(diff[i].node).addClass('modified');
      }
    });

    // Call server update with all the paths.
    $('#Update').on('click', function () {
      var stringifiedList = "<v>";
      var href = _caller.replace("edit_nested", "update");
      var data = $(tableSel).DataTable().data();
      var paths = [];

      // construct mapping of new position -> path
      for (i = 0; i < data.length; i++) {
        var obj = {};
        obj.idx = data[i][0];
        obj.path = data[i][data[i].length - 1];
        paths.push(obj);
      }

      // sort so vector is ready for CASL
      paths.sort(function (a, b) {
        return a.idx - b.idx;
      });

      // make the vector string
      for (i = 0; i < paths.length; i++) {
        stringifiedList += " ";
        stringifiedList += paths[i].path;
      }
      stringifiedList += "</v>";
      //alert(stringifiedList);
      href += encodeURIComponent(Base64.encode(stringifiedList));

      if (!(!event.detail || event.detail == 1)) {
        e.stopPropagation(); return false;
      }
      window.onbeforeunload = null;
      setTimeout(function () {
        if (typeof resetTimeout !== 'undefined')
          resetTimeout();
        dequeue_message();
      }, 100);

      // Bug 24537 MJO - Use post_remote in case the argument is too long
      post_remote(path, 'Business_app.Manage_vector.update', { obj: Base64.encode(stringifiedList) }, 'main');

      cover();
    });
  }
}

// Used with Manage_multiple
function make_manage_current_table(table_id, _kind, _caller, _columnsTH, _data, ordered, path, update_path) {
  var tableSel = '#' + table_id;
  $(tableSel).DataTable({
    data: _data,
    scrollY: (window.innerHeight - 320) + "px", // Bug 25345 MJO - Dynamic height
    scrollX: true,
    scrollCollapse: true,       // for small lists allow table to collapse down so footer isn't hanging far away
    autoWidth: true,
    paging: false,
    responsive: false,
    stateSave: false,
    rowReorder:                 // Drag and drop extension
      ordered ?
        {
          selector: 'td.reorder',
          placeholder: "sortable-placeholder"
        } : false,
    dom: 'itp',                  // no filter etc because we have to move rows up by dragging/dropping
    columns: _columnsTH,
    order: [[0, 'asc']],
    language: {
      "emptyTable": "No current items are configured"
    },
    columnDefs: [
      { orderable: ordered, visible: false, type: 'num', targets: 0 }, // Bug 25827 MJO - Changed to num sort
      { orderable: false, visible: false, targets: -1 },
      { orderable: false, className: ordered ? 'reorder' : '', targets: 1 },
      { orderable: false, targets: '_all' }
    ],
    initComplete: function (settings, json) {
      $(settings.oInstance).closest(".dataTables_scrollBody").css("overflow-x", "hidden");
    }
  });

  // table.responsive.recalc();

  $('#manage_current').on('dblclick', 'tbody tr', function (e) {
    var availDT = $('#manage_available').DataTable();
    var currDT = $('#manage_current').DataTable();

    if (currDT.data().count() === 0)
      return;

    var row = $(this);
    var addRow = currDT.row(row);
    var newRow = availDT.row.add(addRow.data()).draw(false).node();
    $(newRow).addClass("modified");
    addRow.remove().draw('page');
  });

  // Bug 25345 MJO - Dynamic height
  $(window).resize(function () {
    $(".dataTables_scrollBody").css("max-height", (window.innerHeight - 320) + "px");
  });

  // Rewrite update button
  $('button[value="Update"]').replaceWith('<button id="Update" type="button" class="b_off b_color1_b_off  b_tall_white button_link" onkeydown="return button_keydown(event, this);" value="Update">Update</button>');

  // Call server update with all the paths.
  $('#Update').on('click', function () {
    var stringifiedList = "<" + _kind + ">";
    var href = _caller.replace("edit_nested", "update"); // unused??
    var data = $(tableSel).DataTable().data();
    var paths = [];

    // construct mapping of new position -> path
    for (i = 0; i < data.length; i++) {
      var obj = {};
      obj.idx = data[i][0];
      obj.path = data[i][data[i].length - 1];
      paths.push(obj);
    }

    // sort so vector is ready for CASL
    paths.sort(function (a, b) {
      return a.idx - b.idx;
    });

    // make the vector string
    for (i = 0; i < paths.length; i++) {
      stringifiedList += " ";
      stringifiedList += paths[i].path;
    }
    stringifiedList += "</" + _kind + ">";
    //alert(stringifiedList);

    if (!(!event.detail || event.detail == 1)) {
      e.stopPropagation(); return false;
    }
    window.onbeforeunload = null;
    setTimeout(function () {
      if (typeof resetTimeout !== 'undefined')
        resetTimeout();
      dequeue_message();
    }, 100);

    // Bug 24537 MJO - Use post_remote in case the argument is too long
    post_remote(path, update_path, { obj: Base64.encode(stringifiedList) }, 'main');

    cover();
  });
}

// Used with Manage_multiple
function make_manage_available_table(table_id, _kind, _caller, _columnsTH, _data) {
  var tableSel = '#' + table_id;
  var table = $(tableSel).DataTable({
    data: _data,
    autoWidth: true,
    scrollY: (window.innerHeight - 320) + "px", // Bug 25345 MJO - Dynamic height
    paging: false,
    responsive: false,
    stateSave: false,
    lengthMenu: [[25, 15, 50, -1], [25, 15, 50, 'All']],
    dom: 'filtp',
    columns: _columnsTH,
    order: [[0, 'asc']],
    language: {
      "emptyTable": "No items are available"
    },
    columnDefs: [
      { visible: false, type: 'string', targets: 0 },
      { visible: false, targets: -1 },
      { orderable: true, targets: '_all' }
    ]
  });

  //table.responsive.recalc();

  $('#manage_available').on('dblclick', 'tbody tr', function (e) {
    var availDT = $('#manage_available').DataTable();
    var currDT = $('#manage_current').DataTable();

    if (availDT.data().count() === 0)
      return;

    var row = $(this);
    var addRow = availDT.row(row);
    var rowData = addRow.data();
    rowData[0] = currDT.rows().data().length; // Bug 25991 MJO - Add to bottom of list
    var newRow = currDT.row.add(rowData).draw(false).node();
    $(newRow).addClass("modified");
    addRow.remove().draw('page');
  });
}

// Bug 21952 MJO - Pass in arguments to populate the new Help object
var config_obj_name = null;
var config_obj_key = null;
function create_help(config_path) {
  if (config_obj_name !== null && config_obj_key !== null) {
    var args = {
      "class": config_obj_name,
      property: config_obj_key
    };

    post_remote(config_path, "Config.create_new", { a_class: "Help", args: args }, "main", false);
  }
  else {
    alert("Unable to create help. Please try again.");
  }
}
//IPAY-357 NAM: Check for validation failure messages. Validation in ipayment.min.js but HTML easymde editor code here
//Validation failures contain error message - "has invalid character/other..."
function hasValidationErrorMessage()
{
  if( $(".message_dialog_outer").is(":visible")){
    if ($(".message_dialog").text().search('has invalid') != -1){
      return true;}
    }
   
  return false;
}
