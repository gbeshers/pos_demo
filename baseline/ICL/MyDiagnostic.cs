﻿using System;
using System.Diagnostics;
using System.IO;
using System.Globalization;
using System.Collections;
using System.Text;
//using System.Runtime.CompilerServices;

namespace ICL
{

  public class MyDiagnostic : IDisposable
  {

    protected bool m_bDebug = false;
    protected string m_sDebugPathFile;

    protected static Stream m_DiagnosticFile;

    public MyDiagnostic()
    {

    }

    public MyDiagnostic(bool bDebug, string sDebugPathFile)
    {
      m_bDebug = bDebug;
      m_sDebugPathFile = sDebugPathFile;

      if (File.Exists(m_sDebugPathFile))
        File.Delete(m_sDebugPathFile);
    }
    public void SetDebug(bool bDebug, string sDebugPathFile)
    {
      m_bDebug = bDebug;
      m_sDebugPathFile = sDebugPathFile;
    }

    public void TraceMe0(String sText, bool bInitial)
    {
      if (m_bDebug == false)
        return;

      if (bInitial)
      {
        String sPathFile = m_sDebugPathFile;

        // Create File
        if (File.Exists(sPathFile))
        {
          File.Delete(sPathFile);
        }

        m_DiagnosticFile = File.Create(sPathFile);

        // Create a new text writer using the output stream, and add it to
        // the trace listeners. 
        TextWriterTraceListener myTextListener = new TextWriterTraceListener(m_DiagnosticFile);
        Trace.Listeners.Add(myTextListener);
      }
      // Add Date Time
      DateTime dt = DateTime.Now;
      String sDateTime = dt.ToString("G", DateTimeFormatInfo.InvariantInfo);

      String sMsg = String.Format("[{0}] {1}\r\n", sDateTime, sText);

      // Write output to the file.
      Trace.Write(sMsg);

      // Flush the output.
      Trace.Flush();
      //Trace.Close();

      return;
    }

    public void TraceMe(String sText)
    {
      if (m_bDebug == false)
        return;

      // Create a file for output named TestFile.txt. 
      // using (FileStream myFileStream =
      // new FileStream(m_sDebugPathFile, FileMode.Append))
      using (StreamWriter myStream = new StreamWriter(m_sDebugPathFile, true, System.Text.Encoding.Default))
      {
        // Create a new text writer using the output stream  
        // and add it to the trace listeners.
        TextWriterTraceListener myTextListener =
            new TextWriterTraceListener(myStream);
        Trace.Listeners.Add(myTextListener);


        // Add Date Time
        DateTime dt = DateTime.Now;
        String sDateTime = dt.ToString("G", DateTimeFormatInfo.InvariantInfo);

        String sMsg = String.Format("[{0}] {1}", sDateTime, sText);
        // Write output to the file.
        Trace.WriteLine(sMsg);

        // Flush and close the output stream.
        Trace.Flush();
        Trace.Close();
      }



    }
    public void TraceMe2(string sTitle, string sMsg, bool bInsertLen)
    {
      if (m_bDebug == false)
        return;


      using (var bw = new System.IO.BinaryWriter(File.Open(m_sDebugPathFile, FileMode.Append)))
      {
        Encoding encoding = Encoding.Default;

        // Add Date Time and Title
        DateTime dt = DateTime.Now;
        String sDateTime = dt.ToString("G", DateTimeFormatInfo.InvariantInfo);
        String sText = String.Format("[{0}] {1}", sDateTime, sTitle);
        bw.Write(encoding.GetBytes(sText), 0, encoding.GetByteCount(sText));

        if (bInsertLen)
        {
          // add message length
          int length = encoding.GetByteCount(sMsg);
          byte[] bigEndianLength = convertToBigEndian(length);
          bw.Write(bigEndianLength);
        }

        // add message
        sMsg += "\r\n";
        bw.Write(encoding.GetBytes(sMsg), 0, encoding.GetByteCount(sMsg));

      }

    }
    public void Dispose()
    {
      if (m_DiagnosticFile != null)
      {
        m_DiagnosticFile.Close();
        m_DiagnosticFile = null;

      }


    }

    private byte[] convertToBigEndian(int length)
    {
      Byte[] bytes = BitConverter.GetBytes(length);
      Array.Reverse(bytes);
      return bytes;
    }

  }
}
