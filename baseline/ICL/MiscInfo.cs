﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICL
{
    public class MiscInfo
    {
        /// <summary>
        /// FilePath + File
        /// </summary>
        public String sBatchPathFile { get; set; }
        /// <summary>
        /// Client Name signed by BB&T
        /// </summary>
        public String sClientName { get; set; }
        /// <summary>
        /// Client ID provided by BB&T
        /// </summary>
        public String sClientID { get; set; }
        /// <summary>
        /// routing numbers - for example "053101121", For BB&T, they are all same
        /// </summary>
        public String sDestinationRN { get; set; }
        public String sOriginRN { get; set; }
        public String sReturnLocationRN { get; set; }

        /// <summary>
        /// Record 10 / Field 10.  3 Digit bank-specific location number.
        /// </summary>
        public string cashLetterIDBankLocation { get; set; }

        public String sECEInstitutionRN { get; set; }

        /// <summary>
        /// Record 50, field 3.
        /// Done for JP Morgan Chase, bug 22678
        /// </summary>
        public String sImageCreatorRN { get; set; }

        /// <summary>
        /// Destination Name "BB&T" for BB&T
        /// </summary>
        public String sDestinationName { get; set; }
        /// <summary>
        /// Origin Name "BB&T" for BB&T
        /// </summary>
        public String sOriginName { get; set; }
        public String sOriginatorContactName { get; set; }
        public String sOriginatorContactNumber { get; set; }

        /// <summary>
        /// Test or Production "T" for Test, "P" for Production
        /// </summary>
        public String sFileType { get; set; }
        /// <summary>
        /// Value that differentiates this file from other files sent the same day, see field 11 of File header
        /// it is filled with space for other projects, I do not know if it is requested by BB&T
        /// </summary>
        public String sFileIDModifier { get; set; }
        /// <summary>
        /// It is fixed value "U", but in other case it maybe a different value
        /// </summary>
        public String sBOFD_IND { get; set; }
        /// <summary>
        /// A Code that Indicates the conversion Type:
        /// 0 - did not convert physical document
        /// 2 - original paper converted to image
        /// 4 - IRD converted to image of IRD
        /// 6 - image converted to another image
        /// 7 - did not convert image
        /// </summary>
        public String sBOFD_Conversion_IND { get; set; }
        /// <summary>
        /// Maxium number of checks in bundle, if set it to -1, no limitaion 
        /// </summary>
        public Int32 iMaxCheckNumInBundle { get; set; }

        /// <summary>
        /// Some banks, such as Wells Fargo, want the record 61
        /// also known as the Credit Record.
        /// See the Include_Credit_Record in the ICL_BatchUpdate.casl
        /// Bug 
        /// </summary>
        public bool includeCreditRecord { get; set; }

        /// <summary>
        /// Credit (type 61) Record, field 3. The Credit Account Number.
        /// Bug 18994 for Miami / Wells Fargo
        /// </summary>
        public string creditRecordAccountNumber;

        /// <summary>
        /// Credit (type 61) Record, field 4. The Process Control field
        /// Bug 18994 for Miami / Wells Fargo
        /// </summary>
        public string creditRecordProcessControl;

        /// <summary>
        /// Credit (type 61) Record, field 5. The Payor Bank Routing Number
        /// Bug 18994 for Miami / Wells Fargo
        /// </summary>
        public string creditRecordRoutingNumber;

        /// <summary>
        /// Credit (type 61) Record, field 6. The Serial Number (Auxillary On-Us).  Deposit serial number or location identifier as represented in the 
        /// MICR auxillary on-us field on a deposit slip.  Leave blank if not used.
        /// Bug 18994 for Miami / Wells Fargo
        /// </summary>
        public string creditRecordSerialNumber;

        /// <summary>
        /// Optional On-Us symbol to use in the output
        /// </summary>
        public char onUsSymbol;

        /// <summary>
        /// debug mode
        /// </summary>
        public bool bDebug;
        public string sDebugPathFile;

        public bool bTurnOffPrefixLength = true;      // Bug 18994

        /// <summary>
        /// ALlow us to toggle between ASCII and EBCDIC
        /// Bug 17234
        /// </summary>
        public bool encodeEBCDIC;

        /// <summary>
        /// Allow us to toggle endians
        /// Bug 22678
        /// </summary>
        public bool useBigEndian;
    };

}
