﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace ICL
{
  public class CheckDetail
  {
    /// <summary>
    /// Deposit file number?
    /// </summary>
    public Int64 iFILENBR { get; set; }

    // Bug 21327: Needed for debugging
    public int iEventNbr { get; set; }
    public int iTndrNbr { get; set; }
    // End bug 21327

    /// <summary>
    /// Bug 26684: Use a string-based workgroup ID
    /// </summary>
    public string workgroupID { get; set; }

    /// <summary>
    /// POSTDT from iPayment Core File posting date
    /// see field 5 of bundle header record
    /// </summary>
    public DateTime dtFILE_POST_DATE { get; set; }

    /// <summary>
    /// This value is parsed from the scanned in check data 
    /// </summary>
    public String sAUXILIARY_ON_US { get; set; }

    /// <summary>
    /// Parsed from the check data (EPC)
    /// </summary>
    public String sEXT_PROCESSING_CODE { get; set; }

    /// <summary>
    /// Parsed from check data
    /// </summary>
    public String sROUTING_NUMBER { get; set; }

    /// <summary>
    /// Parsed from the check data
    /// </summary>
    public String sON_US { get; set; }

    /// <summary>
    /// Not sure what to set this to
    /// </summary>
    public String sCHECK_DIGIT { get; set; }


    /// <summary>
    /// Amount.  
    /// Bug 19713: Changed to Decimal
    /// </summary>
    public Decimal dITEM_AMOUNT { get; set; }

    /// <summary>
    /// See field 8 of check detail for detail it's format is YYJJJWWWNNNNSSS
    /// </summary>
    public String sECE_SEQUENCE_NUMBER { get; set; }

    /// <summary>
    /// Not used for Liberty, because it is always "00". 
    /// See field 13 of check detail record, however, in my OneStep, 
    /// it is determined by tender type. "1" or "0"
    /// </summary>
    public int iADDENDUM_COUNT { get; set; }

    /// <summary>
    /// Payee Name
    /// </summary>
    public String sPAYEE_NAME { get; set; }
    public int iFrontImageLen { get; set; }
    /// <summary>
    /// The scanned image of the check front.
    /// </summary>
    public byte[] FrontImage { get; set; }

    public int iBackImageLen { get; set; }
    /// <summary>
    /// The scanned image of the check back
    /// </summary>
    public byte[] BackImage { get; set; }

    /// <summary>
    /// Check Record, field 11
    /// Single letter code.  The KeyBank Spec says this:
    /// Electronic Return Acceptance Indicator
    /// Defined Values: ‘0’ Will not accept any electronic information
    /// Bug 17864
    /// </summary>
    public string sReturnAcceptanceIndicator { get; set; }


    /// <summary>
    /// Archive Type Indicator.
    /// KeyBank said this:
    /// In general a value of ‘B’ is used to indicate an image is present.
    /// If the client desires KeyBank to ARC the record and convert the image transaction to an outgoing ACH transaction
    /// the value must be ‘R’.
    /// Check Record, field 16
    /// Bug 17864
    /// </summary>
    public string sArchiveTypeIndicator { get; set; }

    /// <summary>
    /// Credit (type 61) Record, field 6. The Serial Number (Auxillary On-Us).  Deposit serial number or location identifier as represented in the 
    /// MICR auxillary on-us field on a deposit slip.  Leave blank if not used.
    /// Bug 18994 for Miami / Wells Fargo
    /// </summary>
    public string sCreditRecordSerialNumber;

    /// <summary>
    /// Flag to indicate to the ICL module whether this check is a deposit slip or just a regular check.
    /// Needed for the Credit (type 61) Record 
    /// Bug 18994 for Miami / Wells Fargo
    /// </summary>
    public bool bIsDepositSlip;

    public string sCheckIdentifier { get; set; }

    /// <summary>
    /// Initialize fields to defaults so there are no null fields.
    /// </summary>
    public CheckDetail()
    {
      //dtFILE_POST_DATE = DateTime.Now;    // No default; cannot guess this timestamp
      sAUXILIARY_ON_US = "";
      sEXT_PROCESSING_CODE = "";
      sROUTING_NUMBER = "";
      sPAYEE_NAME = "";
      sON_US = "";
      sCHECK_DIGIT = "";
      dITEM_AMOUNT = Decimal.Zero;
      sECE_SEQUENCE_NUMBER = "";
      iADDENDUM_COUNT = 0;  /// Not used for Liberty, because it is always "00".
      iFrontImageLen = 0;
      iBackImageLen = 0;
      sReturnAcceptanceIndicator = "0";   // Bug 17864
      sArchiveTypeIndicator = "B";        // Bug 17864
      sCreditRecordSerialNumber = "";
      bIsDepositSlip = false;
    }
  }

}