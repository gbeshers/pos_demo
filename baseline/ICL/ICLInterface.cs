﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;


namespace ICL
{
    public interface IICLInterface
    {

        bool CreateICLFile(List<CheckDetail> lstCheckDetail);
        String GetErrorMessage();

    }

    public class ICLClass : IICLInterface
    {
        /// <summary>
        /// Create variant for Cleveland Clinic
        /// </summary>
        private bool m_Cleveland = true;

        public ICLClass(MiscInfo miscInfo)
        {
            m_miscInfo = miscInfo;
            clearCountersForSingleFile();
        }

        private void clearCountersForSingleFile()
        {
            m_iTotalCLCntInFile = 0;
            m_iTotalRecCntInFile = 0;
            m_iTotalChkCntInFile = 0;
            m_dTotalAmtInFile = 0;
        }

        // attributes
        protected List<CheckDetail> m_lstCheckDetail;
        protected MiscInfo m_miscInfo;
        protected ILookup<Int32, CheckDetail> m_checksByWorkgroup;
        protected string m_sErrorMsg;
        protected Int32 m_iTotalCLCntInFile;
        public Int32 m_iTotalRecCntInFile;
        public Int32 m_iTotalChkCntInFile;
        public Decimal m_dTotalAmtInFile;
        protected DateTime m_dtFileCreatedTime;
        protected MyDiagnostic m_Diagnostic;


        public bool CreateICLFile(List<CheckDetail> lstCheckDetail)
        {
            m_lstCheckDetail = lstCheckDetail;

            bool bResult = false;
            using (m_Diagnostic = new MyDiagnostic(m_miscInfo.bDebug, m_miscInfo.sDebugPathFile))
            {
                m_Diagnostic.TraceMe2("************************************************************************************************************************", "", false);
                bResult = ProcessICLBatch();
                m_Diagnostic.TraceMe2("************************************************************************************************************************", "", false);
            }

            return bResult;


        }
        public String GetErrorMessage() { return m_sErrorMsg; }


        /// <summary>
        /// Mainline method for creating an ICL file. This writes out
        /// a single File Header Record (01) and then
        /// one or more "Cash Letters" (records 10-90) in the WriteCashLetter()
        /// method and finally a File Control Record (99).
        /// 
        /// This method segregates each Cash Letter by the iPayment workgroup.
        /// This translates to the "Branch" field in record 26, field 7.
        /// It is NOT clear that this segregation by branch is necessary.
        /// 
        /// </summary>
        /// <returns></returns>
        private bool ProcessICLBatch()
        {
            m_Diagnostic.TraceMe2("START ProcessICLBatch()", "", false);
            m_dtFileCreatedTime = DateTime.Now;
            BinaryWriter iclFile = null;
            clearCountersForSingleFile();
            try
            {
                // Clean out old file in the unlikely event it is found
                if (File.Exists(m_miscInfo.sBatchPathFile))
                {
                    File.Delete(m_miscInfo.sBatchPathFile);
                }

                // It is important to write the file as binary not characters so that the 
                // big-endian binary prefixes are correct.
                //m_ICLFile = File.Create(m_miscInfo.sBatchPathFile);
                iclFile = new BinaryWriter(File.Open(m_miscInfo.sBatchPathFile, FileMode.Create));


                // Reset total count and amount
                m_iTotalCLCntInFile = 0;
                m_iTotalRecCntInFile = 0;
                m_iTotalChkCntInFile = 0;
                m_dTotalAmtInFile = 0;
                String sTemp = "";

                // Bug 22678: Make encoding configurable
                Encoding encoding;
                if (m_miscInfo.encodeEBCDIC)
                {
                    encoding = Encoding.GetEncoding("IBM037");     // Bug 17234: Encode the bytes as EBCDIC
                }
                else
                {
                    encoding = Encoding.Default;                  // ASCII
                }

                // File Header (01) - it is always matched record 99
                sTemp = FormatFileHeader();
                // prefixed length - a four-byte unsigned integer 
                if (!m_miscInfo.bTurnOffPrefixLength)
                {
                    int length = encoding.GetByteCount(sTemp);
                    byte[] bigEndianLength = convertToBigEndian(length);
                    iclFile.Write(bigEndianLength);
                }

                iclFile.Write(encoding.GetBytes(sTemp), 0, encoding.GetByteCount(sTemp));
                m_Diagnostic.TraceMe2("[**FileHeader(01)****************]", sTemp, true);


        // Bug 26684, 26552 Separate the checks into workgroup bin's
        Dictionary<string, List<CheckDetail>> workgroupBin = new Dictionary<string, List<CheckDetail>>();
        foreach (CheckDetail checkDetail in m_lstCheckDetail)
        {
          arrangeChecksByWorkgroup(workgroupBin, checkDetail);
        }

        // Bug 26684, 26552 Create a Cash Letter for each list of checks
                int iCashLetterCnt = 0;
        foreach (string workgroupID in workgroupBin.Keys)
                {
                    iCashLetterCnt++;
          List<CheckDetail> checkList = workgroupBin[workgroupID];
          if (!WriteCashLetter(checkList, workgroupID, iclFile, iCashLetterCnt, encoding))  // Bug 22678: passed in encoding
                        return false;
                }

                // File Control Record (99)
                sTemp = FormatFileControlRec();
                // prefixed length - a four-byte unsigned integer 
                if (!m_miscInfo.bTurnOffPrefixLength)
                {
                    int length = encoding.GetByteCount(sTemp);
                    byte[] bigEndianLength = convertToBigEndian(length);
                    iclFile.Write(bigEndianLength);
                }
                iclFile.Write(encoding.GetBytes(sTemp), 0, encoding.GetByteCount(sTemp));
                m_Diagnostic.TraceMe2("[**FileControlRec(99)************]", sTemp, true);
            }
            catch (Exception ex)
            {
                m_sErrorMsg = "ProcessICLBatch() Failed: " + ex.ToString();
                m_Diagnostic.TraceMe2("[TRACE]", m_sErrorMsg, false);
                return false;

            }
            finally
            {
                if (iclFile != null)
                    iclFile.Close();
            }

            m_Diagnostic.TraceMe2("[TRACE]", "END ProcessICLBatch()", false);
            return true;
        }

        /// <summary>
        /// Main method for writing an the Cash Letter part of the ICL.
    /// This, for some strange reason, corresponds with a single workgroup (not so strange anymore: see 26552).
        /// Note: this workgroup number gets written to the BOFD Deposit Branch
        /// field (Record 26, field 7) and if we get a workgroup number that is
    /// not 1, there could be some issues. (cannot find this field in ICL spec)
        /// 
        /// This writes the Cash Letter Record (10), fills in all Bundles,
        /// and finally the Cash Letter Control Record (90).  The calling
        /// method takes care of the 01 and 99 records.
        /// 
        /// The records in this Cash Letter are arranged by pairs of Bundles.
        /// Each pair of Bundles represents an iPayment deposit and there is on
        /// deposit per CORE file, so each pair of Bundles represents a file.
        /// The part of a Bundle pair is the Deposit Ticket.  This is a special,
        /// synthetic image that simulates a real deposit slip along with 
        /// a MICR.  The next bundle in the pair are the actual checks.
        /// </summary>
        /// <param name="iWorkgroup"></param>
        /// <param name="iclFile"></param>
        /// <param name="encoding">Need to inhert the encoding from the calling method so we can configure it</param>
        /// <returns></returns>
    private bool WriteCashLetter(List<CheckDetail> checkList,  string iWorkgroup, BinaryWriter iclFile, int iCashLetterSeqNbr, Encoding encoding)
        {
            m_Diagnostic.TraceMe2("[TRACE]", String.Format("Start WriteICLFile for workgroup:{0}", iWorkgroup), false);

      if (checkList.Count == 0)
            {
                m_Diagnostic.TraceMe2("[TRACE]", String.Format("No checks for workgroup{0}", iWorkgroup), false);
                return true; // this workgroup contains no check
            }
      // Bug 26684: Change to use the binned check list
      IEnumerable<CheckDetail> checksInGroup = checkList.AsEnumerable();

            m_iTotalCLCntInFile++;

            // look up checks by payfiles in this group and get file list in this group
            ILookup<Int64, CheckDetail> checksByFileNbr = checksInGroup.ToLookup(c => c.iFILENBR);
            List<Int64> lstCoreFile = checksByFileNbr.Select(g => g.Key).ToList();

            String sTemp = "";

            // Cash Letter Header (10) it is always matched record 90
            sTemp = FormatCashLetterHeader(iWorkgroup, iCashLetterSeqNbr);
            // prefixed length - a four-byte unsigned integer 
            if (!m_miscInfo.bTurnOffPrefixLength)
            {
                int length = encoding.GetByteCount(sTemp);
                byte[] bigEndianLength = convertToBigEndian(length);
                iclFile.Write(bigEndianLength);
            }
            iclFile.Write(encoding.GetBytes(sTemp), 0, encoding.GetByteCount(sTemp));
            m_Diagnostic.TraceMe2("[**CashLetterHeader(10)**********]", sTemp, true);

            // For each core file - the bundle record corresponds to a group of checks within a single core file
            int iBundleCntInCL = 0;
            int iItemCntInCL = 0;
            int iDepositInCL = 0;
            Decimal dTotalAmtInCL = Decimal.Zero;

            try
            {
                foreach (Int64 iFileNbr in lstCoreFile)
                {
                    if (checksByFileNbr.Contains(iFileNbr))
                    {
                        m_Diagnostic.TraceMe2("[TRACE]", String.Format("Workgroup({0}) and FileNbr({1})", iWorkgroup, iFileNbr), false); //HeidiTest

                        IEnumerable<CheckDetail> checksInFile = checksByFileNbr[iFileNbr];
                        List<CheckDetail> lstCheck = checksInFile.ToList<CheckDetail>();
                        DateTime dtPostDate = lstCheck[0].dtFILE_POST_DATE;

                        // Here I need make sure first item is deposit slip On-Us is "13"
                        // Please refer "OnSite ICL File Format JUNE-2011 b.doc Page 13"
                        bool bFoundDepositSlip = false;
                        //int idx = lstCheck.FindIndex(x => x.sON_US.EndsWith("/13"));
                        int idx = lstCheck.FindIndex(x => x.bIsDepositSlip == true);
                        if (idx != -1)
                        {
                            // Move the Deposit Ticket to the first item in the list.
                            // I should already be the first so this will be quick.
                            CheckDetail item = lstCheck[idx];
                            lstCheck.RemoveAt(idx);
                            lstCheck.Insert(0, item);
                            bFoundDepositSlip = true;
                        }

                        // found bank
                        string sBankName = m_miscInfo.sDestinationName; // bank name
                        sBankName = sBankName.Replace(" ", String.Empty).ToUpper();

                        // bundle and cash detail
                        int iSize = 0;
                        int iLoop = 0;
                        do
                        {
                            iLoop++;

                            // Fill Bundle Header (20) matched record 70 - includes 25
                            iBundleCntInCL++;
                            sTemp = FormatBundleHeader(iBundleCntInCL, dtPostDate, iFileNbr.ToString());
                            // prefixed length - a four-byte unsigned integer 
                            if (!m_miscInfo.bTurnOffPrefixLength)
                            {
                                int length = encoding.GetByteCount(sTemp);
                                byte[] bigEndianLength = convertToBigEndian(length);
                                iclFile.Write(bigEndianLength);
                            }
                            iclFile.Write(encoding.GetBytes(sTemp), 0, encoding.GetByteCount(sTemp));
                            m_Diagnostic.TraceMe2("[**BundleHeader(20)**************]", sTemp, true);

                            int iItemCntInBdl = 0;
                            Decimal dTotalAmtInBdl = Decimal.Zero;       // Bug 19713
                            int iDepositCntInBdl = 0;

                            if (m_miscInfo.iMaxCheckNumInBundle == -1)
                                m_miscInfo.iMaxCheckNumInBundle = 300;   // Suiteable default

                            int k = 0;
                            for (k = m_miscInfo.iMaxCheckNumInBundle * (iLoop - 1); k < Math.Min(lstCheck.Count, m_miscInfo.iMaxCheckNumInBundle * iLoop); k++)
                            {
                                CheckDetail check = lstCheck[k];

                                // Heidi for TTS 18994
                                if (k == 0) // first one is deposit slip
                                {
                                    if (m_miscInfo.includeCreditRecord)
                                    {
                                        if (bFoundDepositSlip)
                                        {
                                            sTemp = FormatCreditRec(check);
                                            // prefixed length - a four-byte unsigned integer 
                                            if (!m_miscInfo.bTurnOffPrefixLength)
                                            {
                                                int length = encoding.GetByteCount(sTemp);
                                                byte[] bigEndianLength = convertToBigEndian(length);
                                                iclFile.Write(bigEndianLength);
                                            }
                                            iclFile.Write(encoding.GetBytes(sTemp), 0, encoding.GetByteCount(sTemp));
                                            iDepositCntInBdl++;
                                            m_Diagnostic.TraceMe2("[**CreditRecord(61)*********]", sTemp, true);

                                            continue;
                                        }
                                    }
                                }

                                // Fill Check detail record (25) it will be two pair of records (50) and (52)
                                sTemp = FormatCheckDetailRec(check);
                                // prefixed length - a four-byte unsigned integer 
                                if (!m_miscInfo.bTurnOffPrefixLength)
                                {
                                    int length = encoding.GetByteCount(sTemp);
                                    byte[] bigEndianLength = convertToBigEndian(length);
                                    iclFile.Write(bigEndianLength);
                                }
                                iclFile.Write(encoding.GetBytes(sTemp), 0, encoding.GetByteCount(sTemp));
                                m_Diagnostic.TraceMe2("[**CheckDetailRecord(25)*********]", sTemp, true);

                                // Fill Check Detail Addendum A Record(26)
                                if (m_Cleveland || 
                                    sBankName.IndexOf("WELLSFARGO") != -1) // wells fargo
                                {
                                    m_Diagnostic.TraceMe2("[**Not writing CheckAddendumARec(26) for Cleveland *********]", sTemp, true);
                                }
                                else
                                {
                                    int iRecCnt = k % m_miscInfo.iMaxCheckNumInBundle + 1;
                                    sTemp = FormatCheckAddendumARec(check, 1); // always is 1 here
                                    // prefixed length - a four-byte unsigned integer 
                                    if (!m_miscInfo.bTurnOffPrefixLength)
                                    {
                                        int length = encoding.GetByteCount(sTemp);
                                        byte[] bigEndianLength = convertToBigEndian(length);
                                        iclFile.Write(bigEndianLength);
                                    }
                                    iclFile.Write(encoding.GetBytes(sTemp), 0, encoding.GetByteCount(sTemp));
                                    m_Diagnostic.TraceMe2("[**CheckAddendumARec(26)*********]", sTemp, true);
                                }

                                // Fill Image View Detail Record (50) and Image View Data Data (52)
                                //----------------------------------------------------------------------------
                                // Front image
                                sTemp = FormatImageViewDetailRec(check, true); // record (50)
                                // prefixed length - a four-byte unsigned integer 
                                if (!m_miscInfo.bTurnOffPrefixLength)
                                {
                                    int length = encoding.GetByteCount(sTemp);
                                    byte[] bigEndianLength = convertToBigEndian(length);
                                    iclFile.Write(bigEndianLength);
                                }
                                iclFile.Write(encoding.GetBytes(sTemp), 0, encoding.GetByteCount(sTemp));
                                m_Diagnostic.TraceMe2("[**FrontImageViewDetailRec(50)***]", sTemp, true);

                                sTemp = FormatImageViewDataRec(check, true);    // record (52)
                                // prefixed length - a four-byte unsigned integer 
                                if (!m_miscInfo.bTurnOffPrefixLength)
                                {
                                    int length = encoding.GetByteCount(sTemp) + check.iFrontImageLen;
                                    byte[] bigEndianLength = convertToBigEndian(length);
                                    iclFile.Write(bigEndianLength);
                                }
                                iclFile.Write(encoding.GetBytes(sTemp), 0, encoding.GetByteCount(sTemp));
                                m_Diagnostic.TraceMe2("[**FrontImageViewDataRec(52)*****]", sTemp, true);

                                // write image data
                                if (check.iFrontImageLen != 0)
                                {
                                    iclFile.Write(check.FrontImage, 0, check.iFrontImageLen);
                                    //sTemp = encoding.GetString(check.FrontImage);
                                    //m_Diagnostic.TraceMe("[  FrontImage                ]" + sTemp, false);
                                }
                                //----------------------------------------------------------------------------
                                //----------------------------------------------------------------------------
                                // Back Image
                                sTemp = FormatImageViewDetailRec(check, false); // record (50)
                                // prefixed length - a four-byte unsigned integer 
                                if (!m_miscInfo.bTurnOffPrefixLength)
                                {
                                    int length = encoding.GetByteCount(sTemp);
                                    byte[] bigEndianLength = convertToBigEndian(length);
                                    iclFile.Write(bigEndianLength);
                                }
                                iclFile.Write(encoding.GetBytes(sTemp), 0, encoding.GetByteCount(sTemp));
                                m_Diagnostic.TraceMe2("[**BackImageViewDetailRec(50)****]", sTemp, true);

                                sTemp = FormatImageViewDataRec(check, false);   // record (52)
                                if (!m_miscInfo.bTurnOffPrefixLength)
                                {
                                    int length = encoding.GetByteCount(sTemp) + check.iBackImageLen;
                                    byte[] bigEndianLength = convertToBigEndian(length);
                                    iclFile.Write(bigEndianLength);
                                }
                                iclFile.Write(encoding.GetBytes(sTemp), 0, encoding.GetByteCount(sTemp));
                                m_Diagnostic.TraceMe2("[**BackImageViewDataRec(52)******]", sTemp, true);

                                // write image data
                                if (check.iBackImageLen != 0)
                                {
                                    iclFile.Write(check.BackImage, 0, check.iBackImageLen);
                                    //sTemp = encoding.GetString(check.BackImage);
                                    //m_Diagnostic.TraceMe("[  BackImage                 ]" + sTemp, false);
                                }
                                //----------------------------------------------------------------------------

                                iItemCntInBdl++;
                                // Bug 19713: dTotalAmtInBdl += check.dITEM_AMOUNT;
                                dTotalAmtInBdl  = Decimal.Add(dTotalAmtInBdl, check.dITEM_AMOUNT);


                            }

                            iSize = k;

                            iItemCntInCL += iItemCntInBdl;
                            iDepositInCL += iDepositCntInBdl;
                            dTotalAmtInCL += dTotalAmtInBdl;

                            // Fill Bundle Control record (70)
                            sTemp = FormatBundleControlRec(iItemCntInBdl, iDepositCntInBdl,dTotalAmtInBdl);
                            // prefixed length - a four-byte unsigned integer 
                            if (!m_miscInfo.bTurnOffPrefixLength)
                            {
                                int length = encoding.GetByteCount(sTemp);
                                byte[] bigEndianLength = convertToBigEndian(length);
                                iclFile.Write(bigEndianLength);
                            }
                            iclFile.Write(encoding.GetBytes(sTemp), 0, encoding.GetByteCount(sTemp));
                            m_Diagnostic.TraceMe2("[**BundleControlRec(70)**********]", sTemp, true);

                        } while (iSize == m_miscInfo.iMaxCheckNumInBundle * iLoop);
                    }
                }


                // Cash Letter control Record (90)
                sTemp = FormatCashLetterControlRec(iBundleCntInCL, iItemCntInCL, iDepositInCL,dTotalAmtInCL);
                // prefixed length - a four-byte unsigned integer 
                if (!m_miscInfo.bTurnOffPrefixLength)
                {
                    int length = encoding.GetByteCount(sTemp);
                    byte[] bigEndianLength = convertToBigEndian(length);
                    iclFile.Write(bigEndianLength);
                }
                iclFile.Write(encoding.GetBytes(sTemp), 0, encoding.GetByteCount(sTemp));
                m_Diagnostic.TraceMe2("[**CashLetterControlRec(90)******]", sTemp, true);
            }
            catch (Exception ex)
            {
                m_sErrorMsg = String.Format("END WriteICLFile for workgroup{0} with ERR:{1}", iWorkgroup, ex.ToString());
                m_Diagnostic.TraceMe2("[TRACE]", m_sErrorMsg, false);
                return false;
            }

            // Calculate for file control Record
            m_iTotalChkCntInFile += iItemCntInCL;
            m_iTotalChkCntInFile += iDepositInCL; // Add deposit in record 61
            // Bug 19713: m_dTotalAmtInFile += dTotalAmtInCL;
            m_dTotalAmtInFile = Decimal.Add(m_dTotalAmtInFile, dTotalAmtInCL);

            m_Diagnostic.TraceMe2("[TRACE]", String.Format("END WriteICLFile for workgroup{0}", iWorkgroup), false);
            return true;
        }

        /// <summary>
        /// Format the File Header Record (01).
        /// </summary>
        /// <returns></returns>
        private string FormatFileHeader()
        {
            m_iTotalRecCntInFile++;

            StringBuilder sb = new StringBuilder();

            String sValue = "";
            int iLen = 0;
            String sFmtStr = "";
            //int lengthInPrefix = 0;

            // Length of File Header is 80
            // append inserted size
            // to hex number
            // do we need this in bb&t?

            /*if (!m_miscInfo.bTurnOffPrefixLength)
            {
                iLen = 80;
                lengthInPrefix = iLen;
                sFmtStr = String.Format("{0:X8}", iLen);
                String sTemp = MyFormatString(sFmtStr, iLen);
                String sHex = HexStringToAscii(sTemp);
                //m_Diagnostic.TraceMe("sHex = "+sHex);
                sb.Append(sHex);
            }*/

            // 1(Numeric)- Record Type fixed "01"
            sValue = "01";
            sb.Append(sValue);

            // 2(Numeric)- Standard Level fixed "03"
            sValue = "03";
            sb.Append(sValue);

            // 3(Numeric)- File Type Indicator - system interface other2 len:1
            sValue = m_miscInfo.sFileType; // "T" or "P"
            sb.Append(sValue);

            // 4(Numeric)- immediate destination routing number; len:9
            sValue = m_miscInfo.sDestinationRN;
            iLen = 9;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, Convert.ToInt32(sValue)));

            // 5(Numeric)- immediate origin routing number; len:9
            sValue = m_miscInfo.sOriginRN;
            iLen = 9;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, Convert.ToInt32(sValue)));

            // 6(Numeric)- File Create Date; len 8
            sValue = m_dtFileCreatedTime.ToString("yyyyMMdd");
            iLen = 8;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, Convert.ToInt32(sValue)));

            // 7(Numeric)- File Create Time; len 4
            sValue = m_dtFileCreatedTime.ToString("HHmm");
            iLen = 4;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, Convert.ToInt32(sValue)));


            // 8(Alpha)- Resend Indicator - "N"; len:1
            sValue = "N";
            sb.Append(sValue);

            // 9(Alpha)- immediate destination name len:18
            sValue = m_miscInfo.sDestinationName;
            iLen = 18;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 10(Alpha)- immediate origin name 
            sValue = m_miscInfo.sOriginName;
            iLen = 18;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 11(Alpha)- File ID Modifier Value that differentiate this from other file sent the same day? HeidiLater
            // len:1
            sValue = m_miscInfo.sFileIDModifier;
            iLen = 1;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 12(Alpha)- Country Code space filed Len: 2
            sValue = "US";    // Changed to US for Wells Fargo
            iLen = 2;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 13(Alpha)- User Field space filed Len: 4
            sValue = " ";
            iLen = 4;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 14(Blank)- Reserved space filled, Len:1
            sValue = " ";
            iLen = 1;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            //if (!m_miscInfo.bTurnOffPrefixLength && sb.Length != (lengthInPrefix + 4))
            //    m_Diagnostic.TraceMe("[**FormatFileHeader(01) Internal*********]Error: Prefix length: " + lengthInPrefix.ToString() + " Actual length: " + sb.Length.ToString());

            return sb.ToString();
        }
        /// <summary>
        /// Format the Cash Letter Header (10).
        /// </summary>
        /// <param name="iWorkGroupNbr"></param>
        /// <returns></returns>
    private string FormatCashLetterHeader(string sWorkGroupNbr, int iCashLetterSeqNbr)
        {
            m_iTotalRecCntInFile++;

            // Length of cash letter header is 80
            StringBuilder sb = new StringBuilder();

            String sValue = "";
            int iLen = 0;
            String sFmtStr = "";
            //int lengthInPrefix = 0;

            /*if (!m_miscInfo.bTurnOffPrefixLength)
            {
                // Length of CL Header is 80
                // append inserted size
                // to hex number
                // do we need this in bb&t?
                iLen = 80;
                lengthInPrefix = iLen;
                sFmtStr = String.Format("{0:X8}", iLen);
                String sTemp = MyFormatString(sFmtStr, iLen);
                String sHex = HexStringToAscii(sTemp);
                sb.Append(sHex);
            }*/

            // 1(Numeric)- Record Type Fixed Value Fixed Value "10"
            sValue = "10";
            sb.Append(sValue);

            // 2(Numeric)- collection type indicator; Fixed Value "01"
            sValue = "01";
            sb.Append(sValue);

            // 3(Numeric)- Destination routing number; Len:9
            sValue = m_miscInfo.sDestinationRN;
            iLen = 9;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, Convert.ToInt32(sValue)));

            // 4(Numeric)- ECE Institution Routing Number; Len:9
            sValue = m_miscInfo.sECEInstitutionRN;
            iLen = 9;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, Convert.ToInt32(sValue)));

            // 5(Numeric)- Cash Letter Business Date - Now?? len:8
            sValue = DateTime.Now.ToString("yyyyMMdd");
            iLen = 8;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, Convert.ToInt32(sValue)));

            // 6(Numeric)- File Create Date; len:8
            sValue = m_dtFileCreatedTime.ToString("yyyyMMdd");
            iLen = 8;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, Convert.ToInt32(sValue)));

            // 7(Numeric)- File Create Time
            sValue = m_dtFileCreatedTime.ToString("HHmm");
            iLen = 4;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, Convert.ToInt32(sValue)));

            // 8(Alpha)- Cash Letter Record Type Indicator; Fixed Value "I"
            sValue = "I";
            sb.Append(sValue);

            // 9(AlphaNumeric)- Cash Letter Document Type Indicator; Fixed Value "G"
            sValue = "G";
            sb.Append(sValue);

            // 10(AlphaNumeric)- Cash Letter ID - format "BBBHHMM"
            // based on Wells fargo it should use a cash letter ID of 2051nnnn.and nnnn is sequence
            // Always 2NNNnnnn, where NNN is a Bank-specific location number and nnnn is four digits starting with 0001 for the first type 10 record in a file and increasing by one for each subsequent type 10 record in the file, if any.
            // For files in the Federal Reserve Bank format, Wells Fargo does not validate this field. However, do not include any spaces in the field. Cash letter ID is always reported as 20310001 for FRB format files.
            // International customers using the Wells Fargo proprietary format, and not the Federal Reserve Bank format, should use a cash letter ID of 2051nnnn
            //sValue = String.Format("{0,0:D3}{1,0:D4}", iWorkGroupNbr, DateTime.Now.ToString("HHmm"));
            sValue = String.Format("2{0}{1,0:D4}", m_miscInfo.cashLetterIDBankLocation, iCashLetterSeqNbr);
            iLen = 8;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 11(NumericBlank)- Originator Contact Name
            sValue = m_miscInfo.sOriginatorContactName;
            iLen = 14;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 12(NumericBlank)- Originator Contact Number Len: 10
            sValue = m_miscInfo.sOriginatorContactNumber;
            iLen = 10;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 13(AlphaNumeric)- Fed Work Type - space filled Len: 1
            // WF - Recommend C, but D, E, H, and I are also acceptable
            sValue = "C";
            iLen = 1;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 14(AlphaNumericBlank)- User Field Len:2
            sValue = " ";
            iLen = 2;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 15(Blank)- Reserved len:1
            sValue = " ";
            iLen = 1;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            //if (!m_miscInfo.bTurnOffPrefixLength && sb.Length != (lengthInPrefix + 4))
            //    m_Diagnostic.TraceMe("[**FormatCashLetterHeader(10) Internal***]Error: Prefix length: " + lengthInPrefix.ToString() + " Actual length: " + sb.Length.ToString());

            return sb.ToString();
        }

        /// <summary>
        /// Format the Bundle Header Record (20).
        /// </summary>
        /// <param name="iBundleCnt"></param>
        /// <param name="ctPostDate"></param>
        /// <param name="sFileNbr"></param>
        /// <returns></returns>
        private string FormatBundleHeader(int iBundleCnt, DateTime ctPostDate, String sFileNbr)
        {
            m_iTotalRecCntInFile++;

            // Length of bundle header is 80
            StringBuilder sb = new StringBuilder();

            String sValue = "";
            int iLen = 0;
            String sFmtStr = "";
            //int lengthInPrefix = 0;

            /*if (!m_miscInfo.bTurnOffPrefixLength)
            {
                // Length of bundle Header is 80
                // append inserted size
                // to hex number
                // do we need this in bb&t?
                iLen = 80;
                lengthInPrefix = iLen;
                sFmtStr = String.Format("{0:X8}", iLen);
                String sTemp = MyFormatString(sFmtStr, iLen);
                String sHex = HexStringToAscii(sTemp);
                sb.Append(sHex);
                //m_Diagnostic.TraceMe("[**FormatBundleHeader(20) Internal*******]Hex Number Length=" + sb.Length.ToString());
            }*/

            // 1(Numeric)- Record Type
            sValue = "20";
            sb.Append(sValue);

            // 2(Numeric)- Collection Type Indicator; when using "01" X9Viewer generate error. I will use "00" instead
            // 00 also have problem set "01" back
            sValue = "01";
            sb.Append(sValue);

            // 3(Numeric)- Bundle Destination Routing Number 
            sValue = m_miscInfo.sDestinationRN;
            iLen = 9;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, Convert.ToInt32(sValue)));

            // 4(Numeric)- BB&T Client ID; Len:9
            //sValue = m_miscInfo.sClientID;          // Bug 17234: This field must be the ECE Institution Routing Number, not the Client ID
            sValue = m_miscInfo.sECEInstitutionRN;
            iLen = 9;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, Convert.ToInt32(sValue)));

            // 5(Numeric)- Bundle business Date - Payfile Posting date in formate YYYYMMDD
            sValue = ctPostDate.ToString("yyyyMMdd");
            iLen = 8;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, Convert.ToInt32(sValue)));


            // 6(Numeric)- Bundle creation date - payfile posting date in format YYYYMMDD
            sValue = ctPostDate.ToString("yyyyMMdd");
            iLen = 8;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, Convert.ToInt32(sValue)));

            // 7(AlphaNumeric)- Bundle ID - Payfile Number in the format YYJJJWWWSS
            sValue = formatFilenameYYYJJJSSSS(sFileNbr);
            iLen = 10;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 8(Numeric)- Bundle Sequence Number - right justified fill 0
            iLen = 4;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, iBundleCnt));

            // 9(AlphaNumeric)- Cycle Number - space filled, Len: 2
            // WF Recommend 01, although we accept 01 through 05. Cannot include spaces.
            sValue = "01";
            iLen = 2;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 10(Numeric)- Return location Routing Number, Len:9
            // It is Numeric field, but it is said space filled ?? - it should be space filled
            sValue = " ";
            iLen = 9;
            //sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            //sb.Append(MyFormatString(sFmtStr, 0));
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));


            // 11(AlphaNumeric)- User Field - space filled, len:5
            sValue = " ";
            iLen = 5;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 12(Blank)- Work Type ID & Client ID work Type Value
            // First 2 digits are "01" and denote the deposit work type
            // the next 8 digits are the client ID provided by BB&T     
            // the last 2 characters are blanks HeidiLater
            
            // Bug 24223: FT: Simplify update to field
            iLen = 12;
            sb.Append(m_miscInfo.sClientID.PadRight(iLen));

            //if (!m_miscInfo.bTurnOffPrefixLength && sb.Length != (lengthInPrefix + 4)  )
            //   m_Diagnostic.TraceMe("[**FormatBundleHeader(20) Internal*******]Error: prefix length: " + lengthInPrefix.ToString() + " actual length: " + sb.Length.ToString());

            return sb.ToString();
        }

    /// <summary>
    /// Take the filename in a format of YYYYJJJSSS or YYYYJJJSSSS
    /// and translate to YYJJJSSSS for the news larger sequence numbers
    /// so that it can fit in the 
    /// Rec 20 Bundle Header Field 7 field which is 10 characters long.
    /// Bug 25279
    /// </summary>
    /// <param name="sFileNbr"></param>
    /// <returns></returns>
    private string formatFilenameYYYJJJSSSS(string sFileNbr)
    {
      if (sFileNbr.Length < 3)
        return sFileNbr;

      string yyJJJSSSS = sFileNbr.Substring(2);
      return yyJJJSSSS.PadRight(10);    // Right pad, right?
    }

    /// <summary>
    /// Format Credit Record (61) for Wells Fargo
    /// The credit (type 61) record represents the deposit slip for posting deposit credit entries.  
    /// There must be at least one credit (type 61) record in a file.  
    /// It follows the first bundle header (type 20) record.  
    /// The credit (type 61) is then followed by one or more check detail (type 25) records and associated image records.  
    /// The total dollar value of credit records in a cash letter or file must always equal the dollar amount of the check detail records in that cash letter or file. 
    /// 
    /// </summary>
    /// <returns></returns>
    private string FormatCreditRec(CheckDetail chkDetail)
        {
            m_iTotalRecCntInFile++;

            // Length of check detail is 80
            StringBuilder sb = new StringBuilder();

            String sValue = "";
            int iLen = 0;
            String sFmtStr = "";

            // 1(Numeric)- Record Type; Len:2; fixed "61" 
            sValue = "61";
            sb.Append(sValue);

            // 2(N) - Amount of credit with leading 0s; len:12
            // Bug 25712 - Prevent Negative Checks from being including in the ICL Batch Update
            Decimal amount = (chkDetail.dITEM_AMOUNT < Decimal.Zero) ? Decimal.Zero : chkDetail.dITEM_AMOUNT;
            String sAmt = String.Format("{0,0:F2}", amount);
            sAmt = sAmt.Replace(".", String.Empty);
            int iAmt = Convert.ToInt32(sAmt);
            iLen = 12;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, iAmt));

            // 3(NBS) Credit Account Number(on us??) right justified len:17
            //sValue = chkDetail.sON_US;
            sValue = m_miscInfo.creditRecordAccountNumber;
            iLen = 17;
            sFmtStr = String.Format("{{0,{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 4(NB) - Process Control len:6 fixed value "  6586" always two spaces followed by 6586
            sValue = m_miscInfo.creditRecordProcessControl;
            iLen = 6;
            sFmtStr = String.Format("{{0,{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));
            

            // 5(NB) - Routing Number len:9 value: 500000377 - should be same as  ??
            //sValue = m_miscInfo.sDestinationRN;
            sValue = m_miscInfo.creditRecordRoutingNumber;
            iLen = 9;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 6(NB) - Serial Number(Auxiliary On-us) len:15
            // Bug 19711
            if (string.IsNullOrWhiteSpace(chkDetail.sCreditRecordSerialNumber))
            {
              sValue = Convert.ToString(chkDetail.iFILENBR);        // Bug 19711
            }
            else
            {
              sValue = chkDetail.sCreditRecordSerialNumber;      // Bug 19711
            }

              iLen = 15;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 7(NB) - Item sequence Number( same as field 8 in rec 25)
            sValue = chkDetail.sECE_SEQUENCE_NUMBER;
            iLen = 15;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 8(B) External Processing code iLen:1
            sValue = " ";
            iLen = 1;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 9(B) Type of Account Code iLen:1
            sValue = " ";
            iLen = 1;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 10(B) source of Work iLen:1 - Always 3
            sValue = "3";
            iLen = 1;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 11(B) Reserved iLen:1
            sValue = " ";
            iLen = 1;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            return sb.ToString();
        }

        /// <summary>
        /// Format the Check Detail Record (25)
        /// </summary>
        /// <param name="chkDetail"></param>
        /// <returns></returns>
        private string FormatCheckDetailRec(CheckDetail chkDetail)
        {
            m_iTotalRecCntInFile++;

            // Length of check detail is 80
            StringBuilder sb = new StringBuilder();

            String sValue = "";
            int iLen = 0;
            String sFmtStr = "";
            //int lengthInPrefix = 0;

            /*if (!m_miscInfo.bTurnOffPrefixLength)
            {
                // Length of Check Detail is 80
                // append inserted size
                // to hex number
                // do we need this in bb&t?
                iLen = 80;
                lengthInPrefix = iLen;
                sFmtStr = String.Format("{0:X8}", iLen);
                String sTemp = MyFormatString(sFmtStr, iLen);
                String sHex = HexStringToAscii(sTemp);
                sb.Append(sHex);
                //m_Diagnostic.TraceMe("[**FormatCheckDetailRecord(25) Internal**]Hex Number Length=" + sb.Length.ToString());
            }*/

            // 1(Numeric)- Record Type; Len:2; fixed "25" 
            sValue = "25";
            sb.Append(sValue);

            // 2(NBSMD)- Auxiliary on-us; Len:15 usually on business check, it is right justifed
            sValue = chkDetail.sAUXILIARY_ON_US.Trim();
            iLen = 15;
            sFmtStr = String.Format("{{0,{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));


            // 3(AlphaNumeric)- External Processing code; Len:1
            sValue = chkDetail.sEXT_PROCESSING_CODE;
            iLen = 1;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 4(NumericNBD)- Payor bank Routing number; Len:8
            // 4(N) in BB&T spec
            // Bug 19362
            /*
            if (string.IsNullOrEmpty(chkDetail.sROUTING_NUMBER))
              sValue = "000000000";   
            else if (chkDetail.sROUTING_NUMBER.Length <= 9)
              sValue = chkDetail.sROUTING_NUMBER;
            else
             * */
            // Bug 19362 If there is empty Routing number, the MICR reading probably failed.
            // Abort the batch update now

            if (string.IsNullOrEmpty(chkDetail.sROUTING_NUMBER))
            {
              // Bug 21327: Include the event and tender nbr in the error message
              string errMessage = string.Format(
                "Error: check routing number is null or empty.  Filenumber: {1}, Event Number: {2}, Tender Number: {3}, Amount: {4}",
                chkDetail.sROUTING_NUMBER,
                chkDetail.iFILENBR,
                chkDetail.iEventNbr,
                chkDetail.iTndrNbr, 
                chkDetail.dITEM_AMOUNT);
              throw new Exception(errMessage);
            }

            if (chkDetail.sROUTING_NUMBER.Length <= 9)
              sValue = chkDetail.sROUTING_NUMBER; 
            else
              sValue = chkDetail.sROUTING_NUMBER.Substring(0, 8);
            iLen = 8;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, Convert.ToInt64(sValue)));
            //sFmtStr = String.Format("{{0,-{0}}}", iLen);
            //sb.Append(MyFormatString(sFmtStr, sValue, iLen));


            // 5(NBSM)- Routing number check digit; 
            sValue = chkDetail.sCHECK_DIGIT;
            iLen = 1;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 6(NBSM)- OnUs should be stCheck->csON_US; len:20, right justified
            sValue = chkDetail.sON_US;
            sValue = sValue.Replace('o', m_miscInfo.onUsSymbol);    // Bug 24223: change the 'o' to something like '/'
            iLen = 20;
            sFmtStr = String.Format("{{0,{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 7(Numeric)- Item Amount with leading 0s; len:10 
            String sAmt = String.Format("{0,0:F2}", chkDetail.dITEM_AMOUNT);
            sAmt = sAmt.Replace(".", String.Empty);
            int iAmt = Convert.ToInt32(sAmt);
            iLen = 10;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, iAmt));

            // 8(NumericBlank)- ECE Institution Item sequence Number number - left justified space filled; len:15
            sValue = chkDetail.sECE_SEQUENCE_NUMBER;
            iLen = 15;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 9(AlphaNumeric)- Documentation type indicator - Fixed Value G; len:1
            sValue = "G";
            sb.Append(sValue);

            // 10(AlphaNumeric)- sRetAccptInd - space filled; len:1
            //sValue = chkDetail.sReturnAcceptanceIndicator;   
            sValue = " ";
            sb.Append(sValue);                     

            // 11(Numeric)- MICR Valid Indicator - space filled; len:1
            sValue = " ";
            sb.Append(sValue);
            //iLen = 1;
            //sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            //sb.Append(MyFormatString(sFmtStr, 0));

            // 12(Alpha)- BOFD indicator "U"-  len:1 
            if (m_miscInfo.sBOFD_IND == "")
                sValue = "U";
            else
                sValue = m_miscInfo.sBOFD_IND;
            iLen = 1;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 13(Numeric)- Check Detail Record Addendum count; len:2 "00" - wf have no addendum for each check
            iLen = 2;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            //sb.Append(MyFormatString(sFmtStr, chkDetail.iADDENDUM_COUNT));
            sb.Append(MyFormatString(sFmtStr, 0));

            // 14(Numeric)- Correct Indicator space filled Len 1 
            sValue = " ";
            iLen = 1;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));
            //sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            //sb.Append(MyFormatString(sFmtStr, 0));

            // 15(AlphaNumeric)- archive type indicator - space filled total len:1
            sValue = chkDetail.sArchiveTypeIndicator;       // Bug 17864
            sb.Append(sValue.Substring(0, 1));              // Bug 17864

            //if (!m_miscInfo.bTurnOffPrefixLength && sb.Length != (lengthInPrefix + 4))
            //    m_Diagnostic.TraceMe("[**FormatCheckDetailRecord(25) Internal**]Error: prefix length: " + lengthInPrefix.ToString() + " actual length: " + sb.Length.ToString());

            return sb.ToString();
        }

        /// <summary>
        /// Format the Check Addendum Record (26).
        /// </summary>
        /// <param name="chkDetail"></param>
        /// <param name="iRecNbr"></param>
        /// <returns></returns>
        private string FormatCheckAddendumARec(CheckDetail chkDetail, int iRecNbr)
        {
            m_iTotalRecCntInFile++;

            // Length of check addendumA is 80

            StringBuilder sb = new StringBuilder();

            String sValue = "";
            int iLen = 0;
            String sFmtStr = "";
            //int lengthInPrefix = 0;

            /*if (!m_miscInfo.bTurnOffPrefixLength)
            {
                // Length of Check Addendum is 80
                // append inserted size
                // to hex number
                // do we need this in bb&t?
                iLen = 80;
                lengthInPrefix = iLen;
                sFmtStr = String.Format("{0:X8}", iLen);
                String sTemp = MyFormatString(sFmtStr, iLen);
                String sHex = HexStringToAscii(sTemp);
                sb.Append(sHex);
            }*/

            // 1(Numeric)- Record Type - Fixed Value "26"; len:2
            sValue = "26";
            sb.Append(sValue);

            // 2(Numeric)- check detail addendum A Record Number; len:1 *** size is 1 in BB&T Spec 
            iLen = 1;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, iRecNbr));


            // 3(Numeric)- Returns Routing Number; Len:9
            sValue = m_miscInfo.sReturnLocationRN;
            iLen = 9;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, Convert.ToInt32(sValue)));

            // 4(Numeric)- BOFD Business Date format "YYYYMMDD"; len:8
            sValue = DateTime.Now.ToString("yyyyMMdd");
            iLen = 8;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, Convert.ToInt32(sValue)));


            // 5(NumericBlank)- BOFD Item Sequence Number, is it same as ECE Sequence number?; len:15
            sValue = chkDetail.sECE_SEQUENCE_NUMBER;
            iLen = 15;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 6(AlphaNumeric)- Deposit Account Number at BOFD, it is same as ECE sequence number?; len:18
            //sValue = m_miscInfo.sECEInstitutionRN;
            sValue = m_miscInfo.sDestinationRN;
            iLen = 18;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 7(AlphaNumeric)- Deposit Branch, Workgroup number?; len 5
      // The ICL standards are unclear on this
      sValue = chkDetail.workgroupID;   // Bug 26684 More accurate than the numeric equivilent

            iLen = 5;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 8(AlphaNumeric)- Payee Name; len:15; size is 15 in BB&T Spec
            sValue = chkDetail.sPAYEE_NAME;
            iLen = 15;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 9(AlphaNumeric)- Truncation Indicator; len:1
            sValue = "Y";
            sb.Append(sValue);

            // 10(AlphaNumeric)- BOFD Conversion Indicator
            sValue = m_miscInfo.sBOFD_Conversion_IND;
            if (sValue.Length == 0)
                sValue = "0";
            iLen = 1;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 11(Numeric)- BOFD Correction Indicator Len: 1
            sValue = " ";
            iLen = 1;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));
            //sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            //sb.Append(MyFormatString(sFmtStr,0));

            // 12(AlphaNumeric)- User Field Len:1
            sValue = " ";
            iLen = 1;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 13(AlphaNumeric)- Reserved len:3 size is 3 in BB&T Spec
            sValue = " ";
            iLen = 3;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            //if (!m_miscInfo.bTurnOffPrefixLength && sb.Length != (lengthInPrefix + 4))
            //    m_Diagnostic.TraceMe("[**FormatCheckAddendumARec(26) Internal**]Error: prefix length: " + lengthInPrefix.ToString() + " actual length: " + sb.Length.ToString());

            return sb.ToString();

        }

        /// <summary>
        /// Format the Image View Detail Record (50).
        /// </summary>
        /// <param name="chkDetail"></param>
        /// <param name="bFront"></param>
        /// <returns></returns>
        private string FormatImageViewDetailRec(CheckDetail chkDetail, bool bFront)
        {
            m_iTotalRecCntInFile++;

            // Length of Image View Detail is 80

            StringBuilder sb = new StringBuilder();

            String sValue = "";
            int iLen = 0;
            String sFmtStr = "";
            //int lengthInPrefix = 0;

            /*if (!m_miscInfo.bTurnOffPrefixLength)
            {
                // Length of Image View Detail is 80
                // append inserted size
                // to hex number
                // do we need this in bb&t?
                iLen = 80;
                lengthInPrefix = iLen;
                sFmtStr = String.Format("{0:X8}", iLen);
                String sTemp = MyFormatString(sFmtStr, iLen);
                
                String sHex = HexStringToAscii(sTemp);
                sb.Append(sHex);
            }*/


            // 1(Numeric)- Record Type fixed "50"
            sValue = "50";
            sb.Append(sValue);

            // 2(Numeric)- Image Indicator fixed "1"
            sValue = "1";
            sb.Append(sValue);

            // 3(Numeric)- Image Creator routing number - is it alwaysssame as destination rn like BB&T (??); len:9
            // Bug 22678
            sValue = m_miscInfo.sImageCreatorRN;
            iLen = 9;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, Convert.ToInt32(sValue)));

            // 4(Numeric)- Image Creator Date; len:8
            sValue = m_dtFileCreatedTime.ToString("yyyyMMdd");
            iLen = 8;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, Convert.ToInt32(sValue)));

            // 5(Numeric)- Image View Format Indicator; len:2
            sValue = "00";
            sb.Append(sValue);

            // 6(Numeric)- Image View Compression Algorithm Indentifer; fixed "00", len:2
            sValue = "00";
            sb.Append(sValue);

            // 7(Numeric)- Image View Data Size; len:7
            iLen = 7;
            long iImageSize = bFront ? chkDetail.iFrontImageLen : chkDetail.iBackImageLen;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, (int)iImageSize));

            // 8(Numeric)- Image View Side Indicator; len:1
            sValue = bFront ? "0" : "1";
            sb.Append(sValue);

            // 9(Numeric)- View Descriptor; fixed "00"
            sValue = "00";
            sb.Append(sValue);

            // 10(Numeric)- Digital Signature Indicator, fixed "0"
            sValue = "0";
            sb.Append(sValue);

            // 11 - 14(Numeric) space filled; total len:21
            sValue = " ";
            iLen = 21;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));
            //sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            //sb.Append(MyFormatString(sFmtStr, 0));

            // 15 (Numberic) Image re-create indicator -Recommend 0 (zero), although we can also accept 1 for files in Federal Reserve Bank format.
            sValue = "0";
            iLen = 1;
            sb.Append(sValue);

            // 16(AlphaNumeric) - User Field Len:8
            sValue = " ";
            iLen = 8;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 17(Blank)- Reserved len:15
            sValue = " ";
            iLen = 15;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));


            //if (!m_miscInfo.bTurnOffPrefixLength && sb.Length != (lengthInPrefix + 4))
            //   m_Diagnostic.TraceMe("[**FormatImageViewDetailRec(50) Internal*]Error: prefix length: " + lengthInPrefix.ToString() + " actual length: " + sb.Length.ToString());

            return sb.ToString();

        }

        /// <summary>
        /// Format the Image View Data Record (52).
        /// This contains the image TIFF in the last field.
        /// This record is variable size.
        /// </summary>
        /// <param name="chkDetail"></param>
        /// <param name="bFront"></param>
        /// <returns></returns>
        private string FormatImageViewDataRec(CheckDetail chkDetail, bool bFront)
        {
            m_iTotalRecCntInFile++;

            // Length of Image View Data is 117 + size of tiff data

            StringBuilder sb = new StringBuilder();

            String sValue = "";
            int iLen = 0;
            String sFmtStr = "";
            //int lengthInPrefix = 0;

            /*if (!m_miscInfo.bTurnOffPrefixLength)
            {
                // Length of image view data is 117+image size
                // append inserted size
                // to hex number
                // do we need this in bb&t?
                iLen = 117 + (bFront ? chkDetail.iFrontImageLen : chkDetail.iBackImageLen);
                //iLen = 9382;// Heidi Test
                lengthInPrefix = iLen;     // Save for dianostic at the end
                sFmtStr = String.Format("{0:X8}", iLen);
                String sTemp = MyFormatString(sFmtStr, iLen);
                String sHex = HexStringToAscii(sTemp);
                sb.Append(sHex);
                // Heidi test
                //int imageLen = chkDetail.FrontImage.Length;
                //m_Diagnostic.TraceMe("image length = " + imageLen.ToString());
                //m_Diagnostic.TraceMe(sTemp);
                //m_Diagnostic.TraceMe(sHex);
                
                //m_Diagnostic.TraceMe("[**FrontImageViewDataRec(52) Internal****]Hex Number Length=" + sb.Length.ToString());
            }*/

            // 1(Numeric)- Record Type fixed value "52"
            sValue = "52";
            sb.Append(sValue);

            // 2(Numeric)- Image Creator Routing Number - is it always same as destination rn like bb&T (??); len:9 
            //sValue = m_miscInfo.sDestinationRN;
            sValue = m_miscInfo.sECEInstitutionRN;
            iLen = 9;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, Convert.ToInt32(sValue)));

            // 3 - Bundle business date - payfile post date; len:8
            sValue = chkDetail.dtFILE_POST_DATE.ToString("yyyyMMdd");
            iLen = 8;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, Convert.ToInt32(sValue)));

            // 4(AlphaNumeric)- Cycle number - space filled; len:2
            // 4(AlphaNumberic) Cycle number - WF Recommend 01, although we accept 01 through 05. Cannot include spaces.
            sValue = "01";
            iLen = 2;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 5(NumericBlank)- ECE Institution item sequence number;len:15
            sValue = chkDetail.sECE_SEQUENCE_NUMBER;
            iLen = 15;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 6 - 8(AlphaNumeric)- Security - space filled; total len:48
            sValue = " ";
            iLen = 48;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 9(Numeric)- clipping orighen fixed "0"
            sValue = "0";
            sb.Append(sValue);

            // 10 - 13(Numeric)- Clipping Origin - space filled; total len:16
            sValue = " ";
            iLen = 16;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));
            //sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            //sb.Append(MyFormatString(sFmtStr, 0));

            // 14(Numeric)- length of image reference key; fixed "0000"; iLen:4
            iLen = 4;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, 0));

            // 15 - does not exist

            // 16(Numeric) - length of digital signature; fixed value "00000"
            iLen = 5;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, 0));

            // 17 - does not exist

            // 18(Numeric)- length of image; len:7
            long iImageSize = bFront ? chkDetail.iFrontImageLen : chkDetail.iBackImageLen;
            iLen = 7;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, (int)iImageSize));

            /*if (!m_miscInfo.bTurnOffPrefixLength)
            {
                int imageLen =  bFront ? chkDetail.iFrontImageLen : chkDetail.iBackImageLen;

                if ((sb.Length + imageLen) != (lengthInPrefix + 4))
                    m_Diagnostic.TraceMe("[**FrontImageViewDataRec(52) Internal****]Error: prefix length: " + lengthInPrefix.ToString() + " actual length: " + sb.Length.ToString());
          
            }*/
            return sb.ToString();

        }

        /// <summary>
        /// Format the Bundle Control Record (70).
        /// </summary>
        /// <param name="iItemCnt"></param>
        /// <param name="dItemTotal"></param>
        /// <returns></returns>
        private string FormatBundleControlRec(int iItemCnt, int iDepositCntInBdl,Decimal dItemTotal)
        {
            m_iTotalRecCntInFile++;

            // Length of Bundle Control rec is 80

            StringBuilder sb = new StringBuilder();

            String sValue = "";
            int iLen = 0;
            String sFmtStr = "";
            //int lengthInPrefix = 0;

            /*if (!m_miscInfo.bTurnOffPrefixLength)
            {
                // Length of bundle control is 80
                // append inserted size
                // to hex number
                // do we need this in bb&t?
                iLen = 80;
                lengthInPrefix = iLen;

                sFmtStr = String.Format("{0:X8}", iLen);
                String sTemp = MyFormatString(sFmtStr, iLen);
                String sHex = HexStringToAscii(sTemp);
                sb.Append(sHex);
            }*/


            // 1(Numeric)- Record Type
            sValue = "70";
            sb.Append(sValue);

            // 2(Numeric) - Items in Bundle len:4
            iLen = 4;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, iItemCnt+iDepositCntInBdl));

            // 3(Numeric)- Bundle Debit Total Amount len:12
            String sAmt = String.Format("{0,0:F2}", dItemTotal);
            sAmt = sAmt.Replace(".", String.Empty);
            int iAmt = Convert.ToInt32(sAmt);
            iLen = 12;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, iAmt));

            // 4(Numeric)- MICR Valid Debit Total Amount, it is same as 3
            // X9Viewer indicator this field should be 0 (??) // Heidi Later
            iLen = 12;
            sb.Append("".PadRight(iLen));   // Bug 24223: Changed to Space filled


      // 5(Numeric)- Bundle Image View Count
      iLen = 5;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, iItemCnt * 2));

            // 6(Numeric) - User Field Len:20
            sValue = " ";
            iLen = 20;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));
            //sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            //sb.Append(MyFormatString(sFmtStr, 0));

            // 7(Blank) - Reserved len:25
            sValue = " ";
            iLen = 25;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));


            // if (!m_miscInfo.bTurnOffPrefixLength && sb.Length != (lengthInPrefix + 4))
            //     m_Diagnostic.TraceMe("[**FormatBundleControlRec(70) Internal***]Error: prefix length: " + lengthInPrefix.ToString() + " actual length: " + sb.Length.ToString());

            return sb.ToString();

        }

        /// <summary>
        /// Format the Cash Letter Control Record (90).
        /// </summary>
        /// <param name="iBundleCnt"></param>
        /// <param name="iItemCnt"></param>
        /// <param name="dTotalAmt"></param>
        /// <returns></returns>
        private string FormatCashLetterControlRec(int iBundleCnt, int iItemCnt, int iDepositCnt, Decimal dTotalAmt)
        {
            m_iTotalRecCntInFile++;

            // Length of Cash Letter Control is 80

            StringBuilder sb = new StringBuilder();

            String sValue = "";
            int iLen = 0;
            String sFmtStr = "";
            //int lengthInPrefix = 0;

            /*if (!m_miscInfo.bTurnOffPrefixLength)
            {
                // Length of cash letter control is 80
                // append inserted size
                // to hex number
                // do we need this in bb&t?
                iLen = 80;
                lengthInPrefix = iLen;
                sFmtStr = String.Format("{0:X8}", iLen);
                String sTemp = MyFormatString(sFmtStr, iLen);
                String sHex = HexStringToAscii(sTemp);
                sb.Append(sHex);
            }*/


            // 1(Numeric)- Record Type fixed value
            sValue = "90";
            sb.Append(sValue);

            // 2(Numeric)- Bundle Count Len:6
            iLen = 6;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, iBundleCnt));

            // 3(Numeric)- Cash Letter Item Count len:8
            iLen = 8;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, iItemCnt+iDepositCnt));

            // 4(Numeric)- Cash Letter total Debit Amount len:14
            String sAmt = String.Format("{0,0:F2}", dTotalAmt);
            sAmt = sAmt.Replace(".", String.Empty);
            int iAmt = Convert.ToInt32(sAmt);
            iLen = 14;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, iAmt));

            // 5(Numeric)- Cash Letter View Count len:9
            iLen = 9;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, iItemCnt * 2));

            // 6(Alpha)- ECE Institution Name Len:18
            sValue = " ";
            iLen = 18;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));

            // 7(Numeric) - Settlement Date Len:8
            sValue = " ";
            iLen = 8;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));
            //sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            //sb.Append(MyFormatString(sFmtStr, 0));

            // 8(Numeric)- Reserved Len:15
            sValue = " ";
            iLen = 15;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));
            //sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            //sb.Append(MyFormatString(sFmtStr, 0));


            //if (!m_miscInfo.bTurnOffPrefixLength && sb.Length != (lengthInPrefix + 4))
            //    m_Diagnostic.TraceMe("[**FmtCashLetterControlRec(90) Internal**]Error: prefix length: " + lengthInPrefix.ToString() + " actual length: " + sb.Length.ToString());

            return sb.ToString();
        }

        /// <summary>
        /// Format the File Control Record (99).
        /// </summary>
        /// <returns></returns>
        private string FormatFileControlRec()
        {
            m_iTotalRecCntInFile++;

            // Length of File control is 80

            StringBuilder sb = new StringBuilder();

            String sValue = "";
            int iLen = 0;
            //int lengthInPrefix = 0;
            String sFmtStr = "";

            /*if (!m_miscInfo.bTurnOffPrefixLength)
            {
                // Length of file control is 80
                // append inserted size
                // to hex number
                // do we need this in bb&t?
                iLen = 80;
                lengthInPrefix = iLen;
                sFmtStr = String.Format("{0:X8}", iLen);
                String sTemp = MyFormatString(sFmtStr, iLen);
                String sHex = HexStringToAscii(sTemp);
                sb.Append(sHex);
            }*/


            // 1(Numeric)- Record Type
            sValue = "99";
            sb.Append(sValue);


            // 2(Numeric)- total number of Cash Letters in the file Count Len:6
            iLen = 6;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, m_iTotalCLCntInFile));

            // 3(Numeric)- Total Record item count len:8;
            // Total record count of all record types in the file including this record
            iLen = 8;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, m_iTotalRecCntInFile));

            // 4(Numeric)- Total Item Count Len: 8
            // Notes: should include deposit in record 61
            iLen = 8;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, m_iTotalChkCntInFile));

            // 5(Numeric) - File total Debit Amount Len: 16
            String sAmt = String.Format("{0,0:F2}", m_dTotalAmtInFile);
            sAmt = sAmt.Replace(".", String.Empty);
            int iAmt = Convert.ToInt32(sAmt);
            iLen = 16;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, iAmt));

            // 6(AlphaNumeric)- Immediate Origin Contact Name - space filled Len:14
            sValue = m_miscInfo.sOriginatorContactName;
            iLen = 14;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));


            // 7(Numeric)- Immediate Origin Contact Phone Number Len: 10 
            sValue = m_miscInfo.sOriginatorContactNumber;
            sValue = sValue.Replace("-", "");
            sValue = sValue.Replace("(", "");
            sValue = sValue.Replace(")", "");
            sValue = sValue.Replace(".", "");
            iLen = 10;
            sFmtStr = String.Format("{{0,0:D{0}}}", iLen);
            if (sValue == "")
                sb.Append(MyFormatString(sFmtStr, 0L));
            else
            {
                try
                {
                    sb.Append(MyFormatString(sFmtStr, Convert.ToInt64(sValue)));
                }
                catch (Exception ex)
                {
                    m_sErrorMsg = "ProcessICLBatch() warning: the Contact phone number configuration is non-numeric: " + ex.ToString();
                    sb.Append(MyFormatString(sFmtStr, 0L));
                }
            }

            // 8(Blank) - Reserved - space filled Len:16
            sValue = " ";
            iLen = 16;
            sFmtStr = String.Format("{{0,-{0}}}", iLen);
            sb.Append(MyFormatString(sFmtStr, sValue, iLen));


            //if (!m_miscInfo.bTurnOffPrefixLength && sb.Length != (lengthInPrefix + 4))
            //    m_Diagnostic.TraceMe("[**FormatFileControlRec(99) Internal*****]Error: prefix length: " + lengthInPrefix.ToString() + " actual length: " + sb.Length.ToString());

            return sb.ToString();
        }


        private string MyFormatString(String sFmtStr, String sValue, int iLen)
        {
            return String.Format(sFmtStr, sValue.Length > iLen ? sValue.Substring(0, iLen) : sValue);
        }
        private string MyFormatString(String sFmtStr, Int64 iValue)
        {
            return String.Format(sFmtStr, iValue);
        }

        /// <summary>
        /// Take a standard C# little-endian integer and 
        /// reverse the bytes to create big-endian (Motorola ordering) version 
        /// on the number in a byte array.
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        private byte[] convertToBigEndian(int length)
        {
            Byte[] bytes = BitConverter.GetBytes(length);
            // Bug 22678: Make endian toggle-able
            if (!m_miscInfo.useBigEndian)
            {
                return bytes;
            }
            // End bug 22678
            Array.Reverse(bytes);
            return bytes;
        }

        /// <summary>
        /// Change Hex String to 4-byte binary number representing the record length.
        /// I believe that we should use Little Endian for this.
        /// we will use binarywriter instead , this function is not used.
        /// </summary>
        /// <param name="hexString"></param>
        /// <returns></returns>
        [Obsolete("Not needed for BinaryWriter", true)]
        private string HexStringToAscii(string hexString)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i <= hexString.Length - 2; i += 2) // NULNULNULP    (Big endian)
            //for (int i = hexString.Length-2; i >= 0; i -= 2) // PNULNULNUL    (Little endian)
            {
                sb.Append(Convert.ToString(Convert.ToChar(Int32.Parse(hexString.Substring(i, 2), System.Globalization.NumberStyles.HexNumber))));
                //string hs = hexString.Substring(i, 2);
                //sb.Append(Convert.ToChar(Convert.ToUInt64(hs, 16)));
                //sb.Append(Convert.ToString(Convert.ToChar(Int64.Parse(hs, System.Globalization.NumberStyles.AllowHexSpecifier|System.Globalization.NumberStyles.HexNumber))));

            }
            string sValue = sb.ToString();
            return sValue;

        }

        /// <summary>
        /// Attempt to write out Big- or Little-endian number as binary inside a character string.
        /// </summary>
        /// <param name="hexString"></param>
        /// <returns></returns>
        [Obsolete("Not needed for BinaryWriter", true)]
        private string HexStringToAscii_1(string hexString)
        {
            byte[] tmp;
            int j = 0;
            tmp = new byte[(hexString.Length) / 2];
            //for (int i = 0; i <= hexString.Length - 2; i += 2)
            for (int i = hexString.Length - 2; i >= 0; i -= 2) // PNULNULNUL    (Little endian)
            {
                tmp[j] = (byte)Convert.ToChar(Int32.Parse(hexString.Substring(i, 2), System.Globalization.NumberStyles.HexNumber));

                j++;
            }
            return Encoding.GetEncoding(1252).GetString(tmp);
        }

    /// <summary>
    /// Simply separate the checks by workgroups.
    /// Bug 26684 and 26552
    /// </summary>
    /// <param name="workgroupBin"></param>
    /// <param name="checkDetail"></param>
    private void arrangeChecksByWorkgroup(Dictionary<string, List<CheckDetail>> workgroupBin, CheckDetail checkDetail)
    {
      string workgroupID = checkDetail.workgroupID;

      List<CheckDetail> checks;
      if (workgroupBin.TryGetValue(workgroupID, out checks))
      {
        checks.Add(checkDetail);
    }
      else
      {
        checks = new List<CheckDetail>();
        checks.Add(checkDetail);
        workgroupBin[workgroupID] = checks;
}
    }


  }
}
