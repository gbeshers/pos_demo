using CASL_engine;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.X509;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using TranSuiteServices;
using System.Security;

namespace baseline
{
  // Bug 17505 MJO - VeriFone Mx915 support
  // Bug 19153 MJO - Changed client IP to MAC address everywhere
  // Bug 24451 MJO - Modified to make device type configurable for Point

  public static class VeriFonePoint// Bug 27153 DH - Made it static
  {
    private static readonly int PORT = 5015;
    private static readonly int SECONDARYPORT = 5016;
    private static ConcurrentDictionary<string, DeviceEntry> Devices = PopulateDevices();

    private static ConcurrentDictionary<string, DeviceEntry> PopulateDevices()
    {
      // Bug 18177 MJO - Handle and log exceptions
      ConcurrentDictionary<string, DeviceEntry> devices = new ConcurrentDictionary<string, DeviceEntry>();

      try
      {
        GenericObject database = c_CASL.c_object("TranSuite_DB.Config.db_connection_info") as GenericObject;
        Hashtable sqlArgs = new Hashtable();

        string sql = "SELECT * FROM TG_DEVICE_ID WHERE DEVICE_TYPE LIKE '%POINT'";

        string error_message = "";
        IDBVectorReader reader = db_reader.Make(database, sql, CommandType.Text, 0,
          sqlArgs, 0, 0, null, ref error_message);

        GenericObject records = new GenericObject();
        reader.ReadIntoRecordset(ref records, 0, 0);
        reader.CloseReader();

        for (int i = 0; i < records.getLength(); i++)
        {
          DeviceEntry entry = new DeviceEntry((GenericObject)records.get(i));
          devices[entry.MACAddress] = entry;
        }
      }
      catch (Exception e)
      {
        devices.Clear();
        Logger.cs_log_trace("An exception occurred when querying the TG_DEVICE_ID table", e);
      }

      return devices;
    }

    public static object Cancel(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string MACAddress = (string)geo_args.get("mac_address");
      bool log = (bool)geo_args.get("log");

      DeviceEntry entry = Devices[MACAddress];

      XDocument request = new XDocument();
      using (var writer = request.CreateWriter())
      {

        writer.WriteStartDocument();
        writer.WriteStartElement("TRANSACTION");
        writer.WriteElementString("FUNCTION_TYPE", "SECONDARYPORT");
        writer.WriteElementString("COMMAND", "CANCEL");
        // Bug 23845 MJO - COUNTER, MAC, and MAC_LABEL not needed for Cancel command
        writer.WriteEndElement();
        writer.WriteEndDocument();
      }

      if (log)
        Log(request, entry.serialNbr); // Bug 19502 MJO - Log serial nbr

      return new GenericObject("xml", request.ToString(SaveOptions.DisableFormatting), "port", SECONDARYPORT);
    }

    public static object UnregisterAll(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string serialNbr = (string)geo_args.get("serial_nbr");

      XDocument request = new XDocument();
      using (var writer = request.CreateWriter())
      {

        writer.WriteStartDocument();
        writer.WriteStartElement("TRANSACTION");
        writer.WriteElementString("FUNCTION_TYPE", "SECURITY");
        writer.WriteElementString("COMMAND", "UNREGISTERALL");
        writer.WriteEndElement();
        writer.WriteEndDocument();
      }

      return new GenericObject("xml", request.ToString(SaveOptions.DisableFormatting), "port", PORT);
    }

    public static object Add(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string serialNbr = (string)geo_args.get("serial_nbr");
      string MACAddress = (string)geo_args.get("mac_address");
      string deviceIP = (string)geo_args.get("device_ip");
      string clientID = (string)geo_args.get("client_id");
      string deviceType = (string)geo_args.get("device_type");
      string locationID = geo_args.get<string>("location_id");  // Bug IPAY-876 MJO - Added Location ID
      bool p2peFlag = geo_args.get<bool>("p2pe_flag"); // Bug IPAY-914 MJO - Added P2PE Flag

      if (Devices.ContainsKey(MACAddress))
      {
        // Bug 23345 MJO - Show this to the user
        GenericObject error = LogAndReturnError(null, null, "A device is already registered with this MAC address.");
        error.set("user_message", "A device is already registered with this MAC address.");
        return error;
      }

      // Bug 23345 MJO - Also check for duplicate device IP and serial number
      foreach (DeviceEntry entr in Devices.Values)
      {
        if (serialNbr == entr.serialNbr)
        {
          // Bug 23345 MJO - Show this to the user
          GenericObject error = LogAndReturnError(null, null, "A device is already registered with this serial number.");
          error.set("user_message", "A device is already registered with this serial number.");
          return error;
        }
        else if (deviceIP == entr.deviceIP)
        {
          // Bug 23345 MJO - Show this to the user
          GenericObject error = LogAndReturnError(null, null, "A device is already registered with this device IP address.");
          error.set("user_message", "A device is already registered with this device IP address.");
          return error;
        }
      }
      
      // Bug IPAY-876 MJO - Added Location ID
      // Bug IPAY-914 MJO - Added P2PE Flag
      DeviceEntry entry = new DeviceEntry(serialNbr, MACAddress, deviceIP, null, null, 0, clientID, deviceType, locationID, p2peFlag);

      string err = entry.insertIntoDB();

      if (err == null)
      {
        Devices[MACAddress] = entry;
      }
      else
      {
        return LogAndReturnError(null, null, "Error adding device to TG_DEVICE_ID table: " + err);
      }

      return true;
    }

    public static Object Edit(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string serialNbr = (string)geo_args.get("serial_nbr");
      string MACAddress = (string)geo_args.get("mac_address");
      string deviceIP = (string)geo_args.get("device_ip");
      string clientID = (string)geo_args.get("client_id");
      string deviceType = (string)geo_args.get("device_type");
      string locationID = geo_args.get<string>("location_id"); // Bug IPAY-876 MJO - Added Location ID
      bool p2peFlag = geo_args.get<bool>("p2pe_flag"); // Bug IPAY-914 MJO - Added P2PE Flag

      bool deviceUpdated = false;

      foreach (DeviceEntry device in Devices.Values)
      {
        if (device.serialNbr == serialNbr)
        {
          string oldMACAddress = device.MACAddress;

          device.serialNbr = serialNbr;
          device.MACAddress = MACAddress;
          device.deviceIP = deviceIP;
          device.clientID = clientID;
          device.deviceType = deviceType;
          device.locationID = locationID; // Bug IPAY-876 MJO - Added Location ID
          device.p2peFlag = p2peFlag; // Bug IPAY-914 MJO - Added P2PE Flag

          string err = device.updateDB();

          if (err != null)
          {
            Logger.cs_log("Error updating device in TG_DEVICE_ID: " + err);
          }
          else
          {
            deviceUpdated = true;

            if (oldMACAddress != MACAddress)
            {
              // Bug 20351 MJO - Updated to use .NET ConcurrentDictionary
              DeviceEntry outEntry = null;
              Devices.TryRemove(oldMACAddress, out outEntry);
              Devices[MACAddress] = device;
            }
          }

          break;
        }
      }

      return deviceUpdated;
    }

    public static object Delete(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string MACAddress = (string)geo_args.get("mac_address");
      XDocument request = new XDocument();

      bool deleted = false;

      foreach (DeviceEntry device in Devices.Values)
      {
        if (device.MACAddress == MACAddress)
        {
          string err = device.deleteFromDB();

          if (err != "")
          {
            return LogAndReturnError(null, null, "Error deleting device from TG_DEVICE_ID: " + err);
          }
          else
          {
            // Bug 20351 MJO - Updated to use .NET ConcurrentDictionary
            DeviceEntry outEntry = null;
            Devices.TryRemove(device.MACAddress, out outEntry);
            deleted = true;
          }

          break;
        }
      }

      return deleted;
    }

    public static object PairNewDevice(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string MACAddress = (string)geo_args.get("mac_address");
      string entryCode = (string)geo_args.get("entry_code");

      if (!Devices.ContainsKey(MACAddress))
        return LogAndReturnError(null, null, "Unable to pair Point device. The device must be registered in VeriFone Device Management before pairing.");

      DeviceEntry entry = Devices[MACAddress];

      if (entry.mac != null)
        return LogAndReturnError(null, null, "Unable to pair Point device. A device is already paired with this IP address.");

      // generate an RSA 2048 Public/Private KeyPair

      RSACryptoServiceProvider cryptoProvider = new RSACryptoServiceProvider(2048);

      // get encoded public key and convert to base 64
      // If the certificate is loaded through the X509 certificate object,
      // it is possible to get the der encoded bytes without Bouncy Castle
      var publicKey = DotNetUtilities.GetRsaPublicKey(cryptoProvider);
      var publicKeyInfo = SubjectPublicKeyInfoFactory.CreateSubjectPublicKeyInfo(publicKey);
      var publicEncodedBytes = publicKeyInfo.ToAsn1Object().GetDerEncoded();
      var publicEncodedString = Convert.ToBase64String(publicEncodedBytes);

      // build request
      XDocument request = new XDocument();
      using (XmlWriter writer = request.CreateWriter())
      {
        writer.WriteStartDocument();
        writer.WriteStartElement("TRANSACTION");
        writer.WriteElementString("FUNCTION_TYPE", "SECURITY");
        writer.WriteElementString("COMMAND", "REGISTER");
        writer.WriteElementString("ENTRY_CODE", entryCode);
        writer.WriteElementString("KEY", publicEncodedString);
        writer.WriteEndElement();
        writer.WriteEndDocument();
      }

      return new GenericObject("xml", request.ToString(SaveOptions.DisableFormatting), "port", PORT, "crypto_provider", cryptoProvider, "device_ip", entry.deviceIP);
    }

    public static object PairNewDeviceResponse(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string xml = (string)geo_args.get("xml");
      string MACAddress = (string)geo_args.get("mac_address");
      string deviceIP = (string)geo_args.get("device_ip");
      RSACryptoServiceProvider cryptoProvider = (RSACryptoServiceProvider)geo_args.get("crypto_provider");

      DeviceEntry entry = Devices[MACAddress];

      XDocument responseXml = XDocument.Parse(xml);

      // validate that the RESULT_CODE came back a SUCCESS
      if ("-1" != responseXml.Element("RESPONSE").Element("RESULT_CODE").Value)
      {
        string message = "Unable to register: unsucessful message response";
        if (responseXml.Element("RESPONSE").Element("RESPONSE_TEXT") != null)
          message = responseXml.Element("RESPONSE").Element("RESPONSE_TEXT").Value;

        // Bug 18058 MJO - Mask response
        return LogAndReturnError(null, MaskResponseXML(responseXml).ToString(), message);
      }

      // validate that the MAC_LABEL was returned
      string macLabel = responseXml.Element("RESPONSE").Element("MAC_LABEL").Value;
      if (string.IsNullOrEmpty(macLabel))
      {
        // Bug 18058 MJO - Mask response
        return LogAndReturnError(null, MaskResponseXML(responseXml).ToString(), "Unable to register: MAC_LABEL not returned in response");
      }

      // validate that the MAC_KEY was returned
      var encryptedMacKey = responseXml.Element("RESPONSE").Element("MAC_KEY").Value;
      if (string.IsNullOrEmpty(macLabel))
      {
        // Bug 18058 MJO - Mask response
        return LogAndReturnError(null, MaskResponseXML(responseXml).ToString(), "Unable to register: MAC_KEY not returned in response");
      }

      // base 64 decode MAC_KEY
      byte[] macKeyBase64Decoded = Convert.FromBase64String(encryptedMacKey);

      // decode key
      byte[] macKey = cryptoProvider.Decrypt(macKeyBase64Decoded, false);

      entry.counter = 1;
      entry.mac = macKey;
      entry.macLabel = macLabel;

      string err = entry.updateDB();

      if (err != null)
      {
        return LogAndReturnError(null, null, "Error adding device to TG_DEVICE_ID table: " + err);
      }
      return entry.MACAddress;
    }

    public static object ProcessCard(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string MACAddress = (string)geo_args.get("mac_address");
      GenericObject tender = (GenericObject)geo_args.get("tender");
      GenericObject systemInterface = (GenericObject)geo_args.get("system_interface");
      bool log = (bool)geo_args.get("log");
      GenericObject geoHCAmts = geo_args.get("healthcare_amounts") as GenericObject;
      double fsaAmount = Convert.ToDouble(geoHCAmts.has("HEALTHCARE") ? geoHCAmts.get("HEALTHCARE") : geo_args.get("fsa_amount"));
      string paymentType = (string)geo_args.get("payment_type");
      bool manualEntry = (bool)geo_args.get("manual_entry"); // Bug 21109 MJO
      bool promptSignature = (bool)geo_args.get("prompt_signature"); // Bug 27368 MJO

      DeviceEntry entry = Devices[MACAddress];

      XDocument request = new XDocument();

      string invoiceNbr = ((int)tender.get("_serial_id")).ToString();
      if (invoiceNbr.Length > 6)
        invoiceNbr = invoiceNbr.Substring(invoiceNbr.Length - 6);

      using (var writer = request.CreateWriter())
      {
        string counter = entry.getAndIncrementCounter().ToString();

        if (counter == "-1")
        {
          return LogAndReturnError(null, null, "Error updating counter in TG_DEVICE_ID");
        }

        // Bug 18047 MJO - If the total is negative, process this as a CREDIT and ignore FSA
        double total = Convert.ToDouble(tender.get("total"));

        writer.WriteStartDocument();
        writer.WriteStartElement("TRANSACTION");
        writer.WriteElementString("FUNCTION_TYPE", "PAYMENT");

        if (total >= 0.00)
          writer.WriteElementString("COMMAND", "CAPTURE");
        else
          writer.WriteElementString("COMMAND", "CREDIT");

        if (paymentType != "ANY" && fsaAmount == 0.00)
          writer.WriteElementString("PAYMENT_TYPE", paymentType);

        writer.WriteElementString("TRANS_AMOUNT", Math.Abs(total).ToString("F2"));

        // Bug 25434 UMN DMS and FSA magic modified from pci.cs. TODO: refactor!
        // Bug 23500 MJO - If a partial payment, limit FSA amount to that
        if (fsaAmount > total)
          fsaAmount = total;

        if (fsaAmount != 0.00)
          writer.WriteElementString("AMOUNT_HEALTHCARE", string.Format("{0:F2}", Math.Abs(fsaAmount)));

        object theObj;
        if (geoHCAmts.has("PRESCRIPTION"))
        {
          theObj = geoHCAmts.get("PRESCRIPTION");
          decimal dTotalPrescription = theObj is decimal ? (decimal)theObj : 0.00M;
          writer.WriteElementString("AMOUNT_PRESCRIPTION", string.Format("{0:F2}", Math.Abs(dTotalPrescription)));
        }
        if (geoHCAmts.has("VISION"))
        {
          theObj = geoHCAmts.get("VISION");
          decimal dTotalVision = theObj is decimal ? (decimal)theObj : 0.00M;
          writer.WriteElementString("AMOUNT_VISION", string.Format("{0:F2}", Math.Abs(dTotalVision)));
        }
        if (geoHCAmts.has("CLINIC"))
        {
          theObj = geoHCAmts.get("CLINIC");
          decimal dTotalClinic = theObj is decimal ? (decimal)theObj : 0.00M;
          writer.WriteElementString("AMOUNT_CLINIC", string.Format("{0:F2}", Math.Abs(dTotalClinic)));
        }
        if (geoHCAmts.has("DENTAL"))
        {
          theObj = geoHCAmts.get("DENTAL");
          decimal dTotalDental = theObj is decimal ? (decimal)theObj : 0.00M;
          writer.WriteElementString("AMOUNT_DENTAL", string.Format("{0:F2}", Math.Abs(dTotalDental)));
        }

        // Bug 21109 MJO - Manual entry support
        if (manualEntry)
          writer.WriteElementString("MANUAL_ENTRY", "TRUE");
          
        // Bug IPAY-1422 MJO - Always capture the signature after so we can show the total amount
        writer.WriteElementString("SIGNATURE_CAPTURE", "FALSE");

        // Bug 27368 MJO - Gateway manual entry override
        // Bug IPAY-1422 MJO - This is done in CASL now as signature capture is always separate
        // writer.WriteElementString("SIGNATURE_CAPTURE", promptSignature ? "TRUE" : "FALSE");

        writer.WriteElementString("MAC_LABEL", entry.macLabel);
        writer.WriteElementString("COUNTER", counter);
        writer.WriteElementString("MAC", PrintMacAsBase64(entry.mac, counter));
        writer.WriteEndElement();
        writer.WriteEndDocument();
      }

      if (log)
        Log(request, entry.serialNbr); // Bug 19502 MJO - Log serial nbr

      return new GenericObject("xml", request.ToString(SaveOptions.DisableFormatting), "port", PORT);
    }

    public static object ProcessCardResponse(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string xml = (string)geo_args.get("xml");
      GenericObject tender = (GenericObject)geo_args.get("tender");
      GenericObject systemInterface = (GenericObject)geo_args.get("system_interface");
      bool log = (bool)geo_args.get("log");
      string serialNbr = (string)geo_args.get("serial_nbr"); // Bug 19502 MJO - Log serial nbr
      string receiptNbr = geo_args.get("receipt_nbr") as string; // Bug 21595 MJO
      bool fullEMVReceipt = (bool)geo_args.get("full_emv_receipt", true);
      bool isLastTran = geo_args.get<bool>("is_last_tran", false); // Bug IPAY-1383 MJO - Last transaction support

      XDocument responseXml = XDocument.Parse(xml);

      string sessionID = misc.get_masked_session_id(); // Bug 20435 UMN PCI-DSS
      
      if (log)
        // Bug 18058 MJO - Mask credit card info
        Log(MaskResponseXML(responseXml), serialNbr); // Bug 19502 MJO - Log serial nbr

      // Bug 20284 MJO - Store receipt from Point response in case we need to print it
      GenericObject receiptText = MakeReceiptGEO(responseXml);

      if (receiptText != null)
        tender.set("point_response_receipt", receiptText);

      // Bug 18047 MJO - CREDIT responses don't have RESPONSE_TEXT
      // Bug 19108 MJO - Changed cancel message as secondary port messages are now ignored
      if (responseXml.Element("RESPONSE").Element("RESPONSE_TEXT") != null && responseXml.Element("RESPONSE").Element("RESPONSE_TEXT").Value == "Cancelled by POS")
      {
        // Bug 22010 MJO - Don't show giant list of EMV tags on approval
        SetEMVText(tender, responseXml, true);

        // Bug 17877 MJO - Mask response XML
        // Bug 24878 MJO - Set status to POSTEDFAILURE
        misc.CASL_call("_subject", c_CASL.execute("System_interface_tracking"), "a_method", "update", "args", new GenericObject("a_core_item", tender,
        "status", c_CASL.execute("System_interface_tracking.PostedFailure"), "response", MaskResponseXML(responseXml).ToString(), "system", systemInterface));

        return "CANCELLED";
      }

      string result = isLastTran ? "CAPTURED" : responseXml.Element("RESPONSE").Element("RESULT").Value;
      string resultCode = responseXml.Element("RESPONSE").Element(isLastTran ? "STATUS_CODE" : "RESULT_CODE") == null ? null : responseXml.Element("RESPONSE").Element(isLastTran ? "STATUS_CODE" : "RESULT_CODE").Value;

      // Bug 24644 MJO - Check to see if result code exists
      if (resultCode != null && "4" != resultCode && "6" != resultCode)
      {
      	// Bug IPAY-1383 MJO - Try doing the last transaction request if there's a comm failure (unless this is already one, then don't loop)
        if (!isLastTran && result == "COMM ERROR")
          return LogAndReturnError(null, MaskResponseXML(responseXml).ToString(), "Communications Error");

        // Bug 22010 MJO - Don't show giant list of EMV tags on approval
        SetEMVText(tender, responseXml, true);

        // Bug 17877 MJO - Mask response XML
        // Bug 24878 MJO - Set status to POSTEDFAILURE
        misc.CASL_call("_subject", c_CASL.execute("System_interface_tracking"), "a_method", "update", "args", new GenericObject("a_core_item", tender,
        "status", c_CASL.execute("System_interface_tracking.PostedFailure"), "response", "Point :" + MaskResponseXML(responseXml).ToString(), "system", systemInterface));

        if (resultCode == "59001")
        {
          // Bug 18058 MJO - Mask response
          return LogAndReturnError(null, MaskResponseXML(responseXml).ToString(), "Transaction cancelled by customer");
        }

        // Bug 21595 MJO - Store the response for CoreDirect
        // Bug 25434 MJO - Kiosk mode doesn't have an event number yet so just store it in my
        if (CASLInterpreter.get_my().get("ui_mode", "full").ToString() == "kiosk")
          CASLInterpreter.get_my().set("point_response", responseXml.ToString());
        else
          PointCache.CachePointResponse(receiptNbr, responseXml.ToString());

        string message = "Transaction unsuccessful";
        // Bug 19063 MJO - Replace "Card Data Not Valid" message with "Card Declined"
        if (responseXml.Element("RESPONSE").Element("RESPONSE_TEXT") != null)
        {
          if (responseXml.Element("RESPONSE").Element("RESPONSE_TEXT").Value.Equals("Card Data Not Valid"))
            message = "Card Declined";
          else
            message = responseXml.Element("RESPONSE").Element("RESPONSE_TEXT").Value;
        }

        // Bug 18058 MJO - Mask response
        return LogAndReturnError(null, MaskResponseXML(responseXml).ToString(), message);
      }
      string terminationStatus = isLastTran ? "SUCCESS" : responseXml.Element("RESPONSE").Element("TERMINATION_STATUS").Value;

      if ((result == "CAPTURED" || result.Contains("APPROVED")) && resultCode == "4" && terminationStatus == "SUCCESS")
      {
        // Bug 22010 MJO - Don't show giant list of EMV tags on approval
        // Bug 21574 MJO - Configurable full/minimal EMV fields on receipt
        SetEMVText(tender, responseXml, fullEMVReceipt);

        // Bug 25434 MJO - Kiosk mode doesn't have an event number yet so just store it in my
        if (CASLInterpreter.get_my().get("ui_mode", "full").ToString() == "kiosk")
          CASLInterpreter.get_my().set("point_response", responseXml.ToString());
        else
          PointCache.CachePointResponse(receiptNbr, responseXml.ToString());

        // IPAY-13 DJD: Use PAYware CTROUTD for follow-on transactions
        string ctroutd = string.Format("C{0}", responseXml.Element("RESPONSE").Element("CTROUTD").Value);
        string paymentType = responseXml.Element("RESPONSE").Element("PAYMENT_TYPE").Value;
        string creditCardNbr = responseXml.Element("RESPONSE").Element("ACCT_NUM").Value;
        // Bug 18047 MJO - CREDIT responses may not have AUTH_CODE, so just use the CTROUTD
        // IPAY-13 DJD: Use PAYware CTROUTD for follow-on transactions
        string authCode = responseXml.Element("RESPONSE").Element("AUTH_CODE") == null ? ctroutd : responseXml.Element("RESPONSE").Element("AUTH_CODE").Value;
        string paymentMedia = responseXml.Element("RESPONSE").Element("PAYMENT_MEDIA").Value;
        string image = null;

        tender.set("auth_str", ctroutd); // IPAY-13 DJD: Use PAYware CTROUTD for follow-on transactions
        tender.set("auth_nbr", authCode); // Bug 18121 MJO - Corrected field name
        tender.set("credit_card_nbr", creditCardNbr);
        tender.set("payment_type", paymentType.ToLower());

        // IPAY-767 MJO - Moved here so the entry method is always set
        // Bug 15502 UMN entry_method was not being saved, so move before the return
        string entryMethod = responseXml.Element("RESPONSE").Element("CARD_ENTRY_MODE") != null ? responseXml.Element("RESPONSE").Element("CARD_ENTRY_MODE").Value : "UNKNOWN";

        // Bug IPAY-538 MJO - Support descriptive entry mode text
        tender.set("entry_method", DescriptiveToDefaultEntryMode(entryMethod));

        if (responseXml.Element("RESPONSE").Element("CARDHOLDER") != null)
        {
          string cardholder = responseXml.Element("RESPONSE").Element("CARDHOLDER").Value;
          string[] cardholderSplit = cardholder.Split('/');
          
          if (cardholderSplit.Length > 1)
          {
            tender.set(
              "payer_name", $"{cardholderSplit[1]} {cardholderSplit[0]}",
              "payer_lname", cardholderSplit[0],
              "payer_fname", cardholderSplit[1]
            );
          }
          else if (cardholderSplit.Length == 1)
          {
            tender.set("payer_name", cardholder);
          }
        }

        // Bug 25482 MJO - Store expiration date in case we're doing a token fee request
        // Bug 26104 MJO - Make sure they exist
        if (responseXml.Element("RESPONSE").Element("CARD_EXP_MONTH") != null)
        {
          string expMonth = responseXml.Element("RESPONSE").Element("CARD_EXP_MONTH").Value;
          tender.set("exp_month", expMonth);
        }

        if (responseXml.Element("RESPONSE").Element("CARD_EXP_YEAR") != null)
        {
          string expYear = responseXml.Element("RESPONSE").Element("CARD_EXP_YEAR").Value;
          tender.set("exp_year", expYear);
        }

        // Bug 24068 MJO - Record whether PIN was captured
        if (responseXml.Element("RESPONSE").Element("EMV_CVM") != null && responseXml.Element("RESPONSE").Element("EMV_CVM").Value == "PIN")
        {
          tender.set("pin_captured", "Yes");
        }

        // Bug 23797 UMN store the card brand
        if (responseXml.Element("RESPONSE").Element("PAYMENT_MEDIA") != null)
        {
          string card_brand = tender.get("card_brand", paymentMedia) as string;
          tender.set("card_brand", card_brand);
        }

        misc.CASL_call("_subject", c_CASL.execute("System_interface_tracking"), "a_method", "update_account_nbr_point", "args", new GenericObject("a_core_item", tender,
        "system", systemInterface));

        // Bug 17877 MJO - Mask sensitive data before storing in tracking table
        // Bug 18058 MJO - Moved masking code into function
        XDocument maskedResponseXml = MaskResponseXML(responseXml);

        // IPAY-13 DJD: Use PAYware CTROUTD for follow-on transactions
        misc.CASL_call("_subject", c_CASL.execute("System_interface_tracking"), "a_method", "update", "args", new GenericObject("a_core_item", tender,
        "status", c_CASL.execute("System_interface_tracking.PostedSuccess"), "reference", ctroutd, "response", maskedResponseXml.ToString(), "system", systemInterface));

        if (responseXml.Element("RESPONSE").Element("SIGNATUREDATA") != null)
          image = responseXml.Element("RESPONSE").Element("SIGNATUREDATA").Value;

        // Bug 18047 MJO - If the total is negative, negate the amount coming back from Point
        object total = tender.get("total");
        double totalNegifier = Convert.ToDouble(total) >= 0.00 ? 1.0 : -1.0;

        if (responseXml.Element("RESPONSE").Element("FSA_AMOUNT") != null)
        {
          tender.set("_fsa", true);

          if (Convert.ToDouble(responseXml.Element("RESPONSE").Element("FSA_AMOUNT").Value) != Convert.ToDouble(tender.get("total")))
            tender.set("_fsa_notify", true);

          tender.set("total", Convert.ToDouble(responseXml.Element("RESPONSE").Element("FSA_AMOUNT").Value));

          misc.CASL_call("_subject", c_CASL.execute("System_interface_tracking"), "a_method", "update_amount", "args", new GenericObject("a_core_item", tender,
          "system", systemInterface));
        }
        // Bug 18047 MJO - Negate amounts if necessary
        else if (responseXml.Element("RESPONSE").Element("APPROVED_AMOUNT") != null && Convert.ToDouble(responseXml.Element("RESPONSE").Element("APPROVED_AMOUNT").Value) * totalNegifier != Convert.ToDouble(tender.get("total")))
        {
          tender.set("_partial_approval", Convert.ToDouble(responseXml.Element("RESPONSE").Element("APPROVED_AMOUNT").Value) * totalNegifier);
          
          tender.set("total", tender.get("_partial_approval"));

          if (responseXml.Element("RESPONSE").Element("DIFF_AMOUNT_DUE") != null)
          {
            tender.set("_partial_approval_diff", Convert.ToDouble(responseXml.Element("RESPONSE").Element("DIFF_AMOUNT_DUE").Value) * totalNegifier);
          }

          misc.CASL_call("_subject", c_CASL.execute("System_interface_tracking"), "a_method", "update_amount", "args", new GenericObject("a_core_item", tender,
          "system", systemInterface));

          tender.set("total", total);
        }

        // Bug 25482 MJO - Create secure string class containing the token
        if (responseXml.Element("RESPONSE").Element("CARD_TOKEN") != null)
        {
          string token = responseXml.Element("RESPONSE").Element("CARD_TOKEN").Value;
          SecureString ss = new SecureString();
          foreach (char c in token)
          {
            ss.AppendChar(c);
          }

          pci.SecureStringClass ssc = new pci.SecureStringClass("POINT_TOKEN", ss);
          GenericObject geo = new GenericObject();
          geo.insert(ssc);

          tender.set("credit_card_nbr_secure_class", geo);
        }

        return image;
      }
      // Bug 23096 MJO - Block offline approvals
      else if (result == "APPROVED/STORED")
      {
        return LogAndReturnError(null, MaskResponseXML(responseXml).ToString(), "OFFLINE");
      }
      else
      {
        // Bug 22010 MJO - Don't show giant list of EMV tags on approval
        SetEMVText(tender, responseXml, true);

        // Bug 21595 MJO - Store the response for CoreDirect
        // Bug 25434 MJO - Kiosk mode doesn't have an event number yet so just store it in my
        if (CASLInterpreter.get_my().get("ui_mode", "full").ToString() == "kiosk")
          CASLInterpreter.get_my().set("point_response", responseXml.ToString());
        else
          PointCache.CachePointResponse(receiptNbr, responseXml.ToString());

        // Bug 24644 MJO - Check to see if ctroutd exists
        // IPAY-13 DJD: Use PAYware CTROUTD for follow-on transactions
        string ctroutd = responseXml.Element("RESPONSE").Element("CTROUTD") == null ? null : responseXml.Element("RESPONSE").Element("CTROUTD").Value;
        tender.set("declined", true);

        // Bug 17877 MJO - Mask response XML
        if (ctroutd == null) // IPAY-13 DJD: Use PAYware CTROUTD for follow-on transactions
          misc.CASL_call("_subject", c_CASL.c_object("System_interface_tracking"), "a_method", "update", "args", new GenericObject("a_core_item", tender,
            "status", c_CASL.execute("System_interface_tracking.PostedFailure"), "response", MaskResponseXML(responseXml).ToString(), "system", systemInterface));
        else
          misc.CASL_call("_subject", c_CASL.c_object("System_interface_tracking"), "a_method", "update", "args", new GenericObject("a_core_item", tender,
            "status", c_CASL.execute("System_interface_tracking.PostedFailure"), "reference", ctroutd, "response", MaskResponseXML(responseXml).ToString(), "system", systemInterface));

        // Bug 18841 MJO - Updated default message
        string message = "Card Declined";
        if (responseXml.Element("RESPONSE").Element("RESPONSE_TEXT") != null)
          message = responseXml.Element("RESPONSE").Element("RESPONSE_TEXT").Value;

        // Bug 18058 MJO - Mask response
        return LogAndReturnError(null, MaskResponseXML(responseXml).ToString(), message);
      }
    }

    // Bug 18058 MJO - Mask sensitive fields
    // Bug 24562/24552 NK MX915 (Point): Payment gets approved but freezes in ipayment
    public static XDocument MaskResponseXML(XDocument responseXml)
    {
      XDocument maskedResponseXml = new XDocument(responseXml);
      XElement responseElement = maskedResponseXml.Element("RESPONSE");

      if (responseElement.Element("ACCT_NUM") != null)
      {
        string creditCardNbr = responseElement.Element("ACCT_NUM").Value;
        responseElement.Element("ACCT_NUM").SetValue(creditCardNbr.Substring(creditCardNbr.Length - 5).PadLeft(creditCardNbr.Length, '*'));
      }
      
      // Bug 24552 MJO - Make sure the exp elements exist before trying to set them
      if (responseElement.Element("ACCT_NUM") != null && responseElement.Element("CARD_EXP_MONTH") != null)
        responseElement.Element("CARD_EXP_MONTH").SetValue("**");
      if (responseElement.Element("ACCT_NUM") != null && responseElement.Element("CARD_EXP_YEAR") != null)
        responseElement.Element("CARD_EXP_YEAR").SetValue("**");

      // Bug 25482 MJO - Token support
      if (responseElement.Element("CARD_TOKEN") != null)
      {
        string cardToken = responseElement.Element("CARD_TOKEN").Value;
        responseElement.Element("CARD_TOKEN").SetValue(cardToken.Substring(cardToken.Length - 4).PadLeft(cardToken.Length, '*'));
      }

      return maskedResponseXml;
    }

    public static object StartSession(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string MACAddress = (string)geo_args.get("mac_address");
      GenericObject tender = (GenericObject)geo_args.get("tender");
      GenericObject systemInterface = (GenericObject)geo_args.get("system_interface");
      string merchantKey = (string)geo_args.get("merchant_key"); // Bug 17878 MJO - Store merchant key in tracking table
      bool log = (bool)geo_args.get("log");
      double total = Convert.ToDouble(tender.get("total")); // Bug 18889 MJO - Get total

      tender.set("credit_card_nbr", "XXXX");

      DeviceEntry entry = Devices[MACAddress];

      try
      {
        // Bug 17878 MJO - Store merchant key in tracking table
        // Bug 18889 MJO - Negative transactions should be recorded as CREDIT
        misc.CASL_call("_subject", c_CASL.execute("System_interface_tracking"), "a_method", "start_request", "args", new GenericObject("a_core_item", tender,
              "request_xml", "POINT Request", "action", total > 0.00 ? "SALE" : "CREDIT", "system", systemInterface, "merchant_key", merchantKey));
      }
      catch (CASL_error e)
      {
        return LogAndReturnError((string)e.casl_object.get("message"), null, "Failed to write to TG_SYSTEM_INTERFACE_TRACKING");
      }

      tender.set("credit_card_nbr", "");

      string invoiceNbr = ((int)tender.get("_serial_id")).ToString();
      if (invoiceNbr.Length > 6)
        invoiceNbr = invoiceNbr.Substring(invoiceNbr.Length - 6);

      XDocument request = new XDocument();
      using (var writer = request.CreateWriter())
      {
        string counter = entry.getAndIncrementCounter().ToString();

        if (counter == "-1")
        {
          return LogAndReturnError(null, null, "Error updating counter in TG_DEVICE_ID");
        }

        writer.WriteStartDocument();
        writer.WriteStartElement("TRANSACTION");
        writer.WriteElementString("FUNCTION_TYPE", "SESSION");
        writer.WriteElementString("COMMAND", "START");
        writer.WriteElementString("INVOICE", invoiceNbr);
        writer.WriteElementString("MAC_LABEL", entry.macLabel);
        writer.WriteElementString("COUNTER", counter);
        writer.WriteElementString("MAC", PrintMacAsBase64(entry.mac, counter));
        writer.WriteEndElement();
        writer.WriteEndDocument();
      }

      if (log)
        Log(request, entry.serialNbr); // Bug 19502 MJO - Log serial nbr

      return new GenericObject("xml", request.ToString(SaveOptions.DisableFormatting), "port", PORT);
    }

    public static object StartSessionResponse(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string xml = (string)geo_args.get("xml");
      GenericObject tender = (GenericObject)geo_args.get("tender");
      GenericObject systemInterface = (GenericObject)geo_args.get("system_interface");
      bool log = (bool)geo_args.get("log");
      string serialNbr = (string)geo_args.get("serial_nbr"); // Bug 19502 MJO - Log serial nbr

      XDocument responseXml = XDocument.Parse(xml);

      if (log)
        Log(responseXml, serialNbr); // Bug 19502 MJO - Log serial nbr

      if ("-1" != responseXml.Element("RESPONSE").Element("RESULT_CODE").Value)
      {
        string message = "Unable to start device session";
        if (responseXml.Element("RESPONSE").Element("RESPONSE_TEXT") != null)
          message = responseXml.Element("RESPONSE").Element("RESPONSE_TEXT").Value;

        misc.CASL_call("_subject", c_CASL.execute("System_interface_tracking"), "a_method", "update", "args", new GenericObject("a_core_item", tender,
        "status", c_CASL.execute("System_interface_tracking.DeviceError"), "system", systemInterface));

        // Bug 18058 MJO - Mask response
        return LogAndReturnError(null, MaskResponseXML(responseXml).ToString(), message);
      }

      return true;
    }

    public static object EndSession(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string MACAddress = (string)geo_args.get("mac_address");
      bool log = (bool)geo_args.get("log");

      DeviceEntry entry = Devices[MACAddress];

      XDocument request = new XDocument();
      using (var writer = request.CreateWriter())
      {
        string counter = entry.getAndIncrementCounter().ToString();

        if (counter == "-1")
        {
          return LogAndReturnError(null, null, "Error updating counter in TG_DEVICE_ID");
        }

        writer.WriteStartDocument();
        writer.WriteStartElement("TRANSACTION");
        writer.WriteElementString("FUNCTION_TYPE", "SESSION");
        writer.WriteElementString("COMMAND", "FINISH");
        writer.WriteElementString("MAC_LABEL", entry.macLabel);
        writer.WriteElementString("COUNTER", counter);
        writer.WriteElementString("MAC", PrintMacAsBase64(entry.mac, counter));
        writer.WriteEndElement();
        writer.WriteEndDocument();
      }

      if (log)
        Log(request, entry.serialNbr); // Bug 19502 MJO - Log serial nbr

      return new GenericObject("xml", request.ToString(SaveOptions.DisableFormatting), "port", PORT);
    }

    public static object EndSessionResponse(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string xml = (string)geo_args.get("xml");
      bool log = (bool)geo_args.get("log");
      string serialNbr = (string)geo_args.get("serial_nbr"); // Bug 19502 MJO - Log serial nbr

      XDocument responseXml = XDocument.Parse(xml);

      if (log)
        Log(responseXml, serialNbr); // Bug 19502 MJO - Log serial nbr

      if ("-1" != responseXml.Element("RESPONSE").Element("RESULT_CODE").Value)
      {
        string message = "Unable to end device session";
        if (responseXml.Element("RESPONSE").Element("RESPONSE_TEXT") != null)
          message = responseXml.Element("RESPONSE").Element("RESPONSE_TEXT").Value;

        // Bug 18058 MJO - Mask response
        return LogAndReturnError(null, MaskResponseXML(responseXml).ToString(), message);
      }

      return true;
    }

    public static object ProcessVoid(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string MACAddress = (string)geo_args.get("mac_address");
      GenericObject tender = (GenericObject)geo_args.get("tender");
      GenericObject systemInterface = (GenericObject)geo_args.get("system_interface");
      bool log = (bool)geo_args.get("log");

      DeviceEntry entry = Devices[MACAddress];

      try
      {
        misc.CASL_call("_subject", c_CASL.execute("System_interface_tracking"), "a_method", "start_request", "args", new GenericObject("a_core_item", tender,
              "request_xml", "POINT Request", "action", "CREDIT", "system", systemInterface, "merchant_key", MACAddress));
      }
      catch (CASL_error e)
      {
        return LogAndReturnError((string)e.casl_object.get("message"), null, "Failed to write to TG_SYSTEM_INTERFACE_TRACKING");
      }

      string invoiceNbr = ((int)tender.get("_serial_id")).ToString();
      if (invoiceNbr.Length > 6)
        invoiceNbr = invoiceNbr.Substring(invoiceNbr.Length - 6);

      XDocument request = new XDocument();

      using (var writer = request.CreateWriter())
      {
        string counter = entry.getAndIncrementCounter().ToString();

        if (counter == "-1")
        {
          return LogAndReturnError(null, null, "Error updating counter in TG_DEVICE_ID");
        }

        writer.WriteStartDocument();
        writer.WriteStartElement("TRANSACTION");
        writer.WriteElementString("FUNCTION_TYPE", "PAYMENT");
        writer.WriteElementString("COMMAND", "VOID");
        writer.WriteElementString("PAYMENT_TYPE", ((string)tender.get("payment_type")).ToUpper());
        writer.WriteElementString("CTROUTD", ((string)tender.get("auth_str")).TrimStart('C')); // IPAY-13 DJD: Use PAYware CTROUTD for follow-on transactions
        writer.WriteElementString("MAC_LABEL", entry.macLabel);
        writer.WriteElementString("COUNTER", counter);
        writer.WriteElementString("MAC", PrintMacAsBase64(entry.mac, counter));
        writer.WriteEndElement();
        writer.WriteEndDocument();
      }

      if (log)
        Log(request, entry.serialNbr); // Bug 19502 MJO - Log serial nbr

      return new GenericObject("xml", request.ToString(SaveOptions.DisableFormatting), "port", PORT);
    }

    public static object ProcessVoidResponse(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string xml = (string)geo_args.get("xml");
      GenericObject tender = (GenericObject)geo_args.get("tender");
      GenericObject systemInterface = (GenericObject)geo_args.get("system_interface");
      bool log = (bool)geo_args.get("log");
      string serialNbr = (string)geo_args.get("serial_nbr"); // Bug 19502 MJO - Log serial nbr

      XDocument responseXml = XDocument.Parse(xml);

      if (log)
        Log(responseXml, serialNbr); // Bug 19502 MJO - Log serial nbr

      if (responseXml.Element("RESPONSE").Element("RESULT_CODE").Value != "7" && responseXml.Element("RESPONSE").Element("RESULT_CODE").Value != "6")
      {
        string message = "Unable to void transaction.";
        if (responseXml.Element("RESPONSE").Element("RESPONSE_TEXT") != null)
          message = responseXml.Element("RESPONSE").Element("RESPONSE_TEXT").Value;

        // Bug 18058 MJO - Mask response
        return LogAndReturnError(null, MaskResponseXML(responseXml).ToString(), message);
      }

      string result = responseXml.Element("RESPONSE").Element("RESULT").Value;
      string terminationStatus = responseXml.Element("RESPONSE").Element("TERMINATION_STATUS").Value;

      if (result == "VOIDED" && terminationStatus == "SUCCESS")
      {
        // IPAY-13 DJD: Use PAYware CTROUTD for follow-on transactions
        string ctroutd = string.Format("C{0}", responseXml.Element("RESPONSE").Element("CTROUTD").Value);
        string authCode = responseXml.Element("RESPONSE").Element("AUTH_CODE").Value;

        tender.set("auth_str", ctroutd);
        tender.set("auth_nbr", authCode); // Bug 18121 MJO - Corrected field name
        tender.set("response_xml", responseXml.ToString());

        // Bug 17877 MJO - Mask response XML
        // IPAY-13 DJD: Use PAYware CTROUTD for follow-on transactions
        misc.CASL_call("_subject", c_CASL.execute("System_interface_tracking"), "a_method", "update", "args", new GenericObject("a_core_item", tender,
        "status", c_CASL.execute("System_interface_tracking.PostedSuccess"), "reference", ctroutd, "response", MaskResponseXML(responseXml).ToString(), "system", systemInterface));
      }
      else
      {
        string ctroutd = responseXml.Element("RESPONSE").Element("CTROUTD").Value;
        tender.set("declined", true);

        // Bug 17877 MJO - Mask response XML
        misc.CASL_call("_subject", c_CASL.c_object("System_interface_tracking"), "a_method", "update", "args", new GenericObject("a_core_item", tender,
        "status", c_CASL.execute("System_interface_tracking.PostedFailure"), "reference", ctroutd, "response", "Point:" + MaskResponseXML(responseXml).ToString(), "system", systemInterface));

        // Bug 18058 MJO - Mask response
        return LogAndReturnError(null, MaskResponseXML(responseXml).ToString(), "Void unsuccessful");
      }

      return true;
    }

    public static object ProcessRefund(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string MACAddress = (string)geo_args.get("mac_address");
      GenericObject tender = (GenericObject)geo_args.get("tender");
      GenericObject systemInterface = (GenericObject)geo_args.get("system_interface");
      bool log = (bool)geo_args.get("log");
      
      DeviceEntry entry = Devices[MACAddress];

      try
      {
        misc.CASL_call("_subject", c_CASL.execute("System_interface_tracking"), "a_method", "start_request", "args", new GenericObject("a_core_item", tender,
              "request_xml", "POINT Request", "action", "CREDIT", "system", systemInterface, "merchant_key", MACAddress));
      }
      catch (CASL_error e)
      {
        return LogAndReturnError((string)e.casl_object.get("message"), null, "Failed to write to TG_SYSTEM_INTERFACE_TRACKING");
      }

      string invoiceNbr = ((int)tender.get("_serial_id")).ToString();
      if (invoiceNbr.Length > 6)
        invoiceNbr = invoiceNbr.Substring(invoiceNbr.Length - 6);

      XDocument request = new XDocument();
      
      using (var writer = request.CreateWriter())
      {
        string counter = entry.getAndIncrementCounter().ToString();

        if (counter == "-1")
        {
          return LogAndReturnError(null, null, "Error updating counter in TG_DEVICE_ID");
        }

        writer.WriteStartDocument();
        writer.WriteStartElement("TRANSACTION");
        writer.WriteElementString("FUNCTION_TYPE", "PAYMENT");
        writer.WriteElementString("COMMAND", "CREDIT");
        writer.WriteElementString("PAYMENT_TYPE", ((string)tender.get("payment_type")).ToUpper());
        writer.WriteElementString("TRANS_AMOUNT", (Convert.ToDouble(tender.get("total")) * -1.0).ToString("F2"));
        writer.WriteElementString("CTROUTD", ((string)tender.get("auth_str")).TrimStart('C')); // IPAY-13 DJD: Use PAYware CTROUTD for follow-on transactions
        writer.WriteElementString("MAC_LABEL", entry.macLabel);
        writer.WriteElementString("COUNTER", counter);
        writer.WriteElementString("MAC", PrintMacAsBase64(entry.mac, counter));
        writer.WriteEndElement();
        writer.WriteEndDocument();
      }

      if (log)
        Log(request, entry.serialNbr); // Bug 19502 MJO - Log serial nbr

      return new GenericObject("xml", request.ToString(SaveOptions.DisableFormatting), "port", PORT);
    }

    public static object ProcessRefundResponse(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string xml = (string)geo_args.get("xml");
      GenericObject tender = (GenericObject)geo_args.get("tender");
      GenericObject systemInterface = (GenericObject)geo_args.get("system_interface");
      bool log = (bool)geo_args.get("log");
      string serialNbr = (string)geo_args.get("serial_nbr"); // Bug 19502 MJO - Log serial nbr

      XDocument responseXml = XDocument.Parse(xml);

      if (log)
        Log(responseXml, serialNbr); // Bug 19502 MJO - Log serial nbr

      if (responseXml.Element("RESPONSE").Element("RESULT_CODE").Value != "4" && responseXml.Element("RESPONSE").Element("RESULT_CODE").Value != "6")
      {
        string message = "Unable to refund transaction";
        if (responseXml.Element("RESPONSE").Element("RESPONSE_TEXT") != null)
          message = responseXml.Element("RESPONSE").Element("RESPONSE_TEXT").Value;

        // Bug 18058 MJO - Mask response
        return LogAndReturnError(null, MaskResponseXML(responseXml).ToString(), message);
      }

      string result = responseXml.Element("RESPONSE").Element("RESULT").Value;
      string terminationStatus = responseXml.Element("RESPONSE").Element("TERMINATION_STATUS").Value;

      if (result == "CAPTURED" && terminationStatus == "SUCCESS")
      {
        // IPAY-13 DJD: Use PAYware CTROUTD for follow-on transactions
        string ctroutd = string.Format("C{0}", responseXml.Element("RESPONSE").Element("CTROUTD").Value);
        string creditCardNbr = responseXml.Element("RESPONSE").Element("ACCT_NUM") == null ? null : responseXml.Element("RESPONSE").Element("ACCT_NUM").Value;
        string authCode = responseXml.Element("RESPONSE").Element("AUTH_CODE") == null ? null : responseXml.Element("RESPONSE").Element("AUTH_CODE").Value;

        tender.set("auth_str", ctroutd);

        if (authCode != null)
          tender.set("auth_nbr", authCode); // Bug 18121 MJO - Corrected field name

        if (creditCardNbr != null)
          tender.set("credit_card_nbr", creditCardNbr);

        // Bug 17877 MJO - Mask response XML
        // IPAY-13 DJD: Use PAYware CTROUTD for follow-on transactions
        misc.CASL_call("_subject", c_CASL.execute("System_interface_tracking"), "a_method", "update", "args", new GenericObject("a_core_item", tender,
        "status", c_CASL.execute("System_interface_tracking.PostedSuccess"), "reference", ctroutd, "response", MaskResponseXML(responseXml).ToString(), "system", systemInterface));
      }
      else
      {
        string ctroutd = responseXml.Element("RESPONSE").Element("CTROUTD").Value;
        tender.set("declined", true);

        // Bug 17877 MJO - Mask response XML
        misc.CASL_call("_subject", c_CASL.c_object("System_interface_tracking"), "a_method", "update", "args", new GenericObject("a_core_item", tender,
        "status", c_CASL.execute("System_interface_tracking.PostedFailure"), "reference", ctroutd, "response", MaskResponseXML(responseXml).ToString(), "system", systemInterface));

        string message = "Unable to refund transaction";
        if (responseXml.Element("RESPONSE").Element("RESPONSE_TEXT") != null)
          message = responseXml.Element("RESPONSE").Element("RESPONSE_TEXT").Value;

        // Bug 18058 MJO - Mask response
        return LogAndReturnError(null, MaskResponseXML(responseXml).ToString(), message);
      }

      return true;
    }

    public static object ProcessSignatureCapture(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string MACAddress = (string)geo_args.get("mac_address");
      string displayText = (string)geo_args.get("signature_display");
      GenericObject dataObj = (GenericObject)geo_args.get("data_obj");
      bool log = (bool)geo_args.get("log");

      DeviceEntry entry = Devices[MACAddress];

      XDocument request = new XDocument();
      using (var writer = request.CreateWriter())
      {
        string counter = entry.getAndIncrementCounter().ToString();

        if (counter == "-1")
        {
          return LogAndReturnError(null, null, "Error updating counter in TG_DEVICE_ID");
        }

        writer.WriteStartDocument();
        writer.WriteStartElement("TRANSACTION");
        writer.WriteElementString("FUNCTION_TYPE", "DEVICE");
        writer.WriteElementString("COMMAND", "SIGNATURE");
        writer.WriteElementString("DISPLAY_TEXT", displayText);
        writer.WriteElementString("MAC_LABEL", entry.macLabel);
        writer.WriteElementString("COUNTER", counter);
        writer.WriteElementString("MAC", PrintMacAsBase64(entry.mac, counter));
        writer.WriteEndElement();
        writer.WriteEndDocument();
      }

      if (log)
        Log(request, entry.serialNbr); // Bug 19502 MJO - Log serial nbr

      return new GenericObject("xml", request.ToString(SaveOptions.DisableFormatting), "port", PORT);
    }

    public static object ProcessSignatureCaptureResponse(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string xml = (string)geo_args.get("xml");
      bool log = (bool)geo_args.get("log");
      string serialNbr = (string)geo_args.get("serial_nbr"); // Bug 19502 MJO - Log serial nbr
      string receiptNbr = geo_args.get("receipt_nbr") as string;

      XDocument responseXml = XDocument.Parse(xml);

      if (log)
        Log(responseXml, serialNbr); // Bug 19502 MJO - Log serial nbr

      // Bug 19108 MJO - Fixed element name, check for both user and POS messages, and changed cancel message as secondary port messages are now ignored
      if (responseXml.Element("RESPONSE").Element("RESPONSE_TEXT").Value == "Cancelled by POS" || responseXml.Element("RESPONSE").Element("RESPONSE_TEXT").Value == "CANCELED by Customer")
        return null;

      if ("-1" != responseXml.Element("RESPONSE").Element("RESULT_CODE").Value)
      {
        string message = "Unable to capture signature";
        if (responseXml.Element("RESPONSE").Element("RESPONSE_TEXT") != null)
          message = responseXml.Element("RESPONSE").Element("RESPONSE_TEXT").Value;

        // Bug 18058 MJO - Mask response
        return LogAndReturnError(null, MaskResponseXML(responseXml).ToString(), message);
      }

      // Bug 22592 MJO - If this is a card processing retry, replace the signature data in the XML
      string signatureData = responseXml.Element("RESPONSE").Element("SIGNATUREDATA").Value;

      // Bug 25434 MJO - Not supported in kiosk mode
      if (CASLInterpreter.get_my().get("ui_mode", "full").ToString() != "kiosk")
      {
        string captureResponse = PointCache.GetPointResponse(receiptNbr);

        if (captureResponse != null)
        {
          XDocument captureXML = XDocument.Parse(captureResponse);

          IEnumerable<XElement> sigElement = captureXML.Descendants("SIGNATUREDATA");

          foreach (XElement data in sigElement)
            data.Value = signatureData;

          PointCache.CachePointResponse(receiptNbr, captureXML.ToString());
        }
      }

      return signatureData;
    }

    public static object ProcessAcceptDecline(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string MACAddress = (string)geo_args.get("mac_address");
      string displayText = (string)geo_args.get("accept_decline_display");
      GenericObject dataObj = (GenericObject)geo_args.get("data_obj");
      bool log = (bool)geo_args.get("log");

      DeviceEntry entry = Devices[MACAddress];

      string[] displayLines = displayText.Split('\n');

      XDocument request = new XDocument();
      using (var writer = request.CreateWriter())
      {
        string counter = entry.getAndIncrementCounter().ToString();

        if (counter == "-1")
        {
          return LogAndReturnError(null, null, "Error updating counter in TG_DEVICE_ID");
        }

        writer.WriteStartDocument();
        writer.WriteStartElement("TRANSACTION");
        writer.WriteElementString("FUNCTION_TYPE", "DEVICE");
        writer.WriteElementString("COMMAND", "CUST_BUTTON");

        for (int i = 0; i < displayLines.Length && i < 5; i++)
          writer.WriteElementString("DISPLAY_TEXT" + (i+1).ToString(), (displayLines[i].Length > 45 ? displayLines[i].Substring(0, 45) : displayLines[i]));
        
        writer.WriteElementString("BUTTON_LABEL1", "Accept");
        writer.WriteElementString("BUTTON_LABEL2", "Decline");
        writer.WriteElementString("MAC_LABEL", entry.macLabel);
        writer.WriteElementString("COUNTER", counter);
        writer.WriteElementString("MAC", PrintMacAsBase64(entry.mac, counter));
        writer.WriteEndElement();
        writer.WriteEndDocument();
      }

      if (log)
        Log(request, entry.serialNbr); // Bug 19502 MJO - Log serial nbr

      return new GenericObject("xml", request.ToString(SaveOptions.DisableFormatting), "port", PORT);
    }

    public static object ProcessAcceptDeclineResponse(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string xml = (string)geo_args.get("xml");
      bool log = (bool)geo_args.get("log");
      string serialNbr = (string)geo_args.get("serial_nbr"); // Bug 19502 MJO - Log serial nbr

      XDocument responseXml = XDocument.Parse(xml);

      if (log)
        Log(responseXml, serialNbr); // Bug 19502 MJO - Log serial nbr

      // Bug 17898 MJO - Error out on cancel
      // Bug 19108 MJO - Changed cancel message as secondary port messages are now ignored
      if ("-1" != responseXml.Element("RESPONSE").Element("RESULT_CODE").Value || responseXml.Element("RESPONSE").Element("RESPONSE_TEXT").Value == "Cancelled by POS")
      {
        string message = "Unable to process Accept/Decline";
        if (responseXml.Element("RESPONSE").Element("RESPONSE_TEXT") != null)
          message = responseXml.Element("RESPONSE").Element("RESPONSE_TEXT").Value;

        // Bug 18058 MJO - Mask response
        return LogAndReturnError(null, MaskResponseXML(responseXml).ToString(), message);
      }

      string result = responseXml.Element("RESPONSE").Element("CUST_BUTTON_DATA").Value;

      return result == "1";
    }

    public static object ProcessYesNo(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string MACAddress = (string)geo_args.get("mac_address");
      string displayText = (string)geo_args.get("yes_no_display");
      GenericObject dataObj = (GenericObject)geo_args.get("data_obj");
      bool log = (bool)geo_args.get("log");

      DeviceEntry entry = Devices[MACAddress];

      XDocument request = new XDocument();
      using (var writer = request.CreateWriter())
      {
        string counter = entry.getAndIncrementCounter().ToString();

        if (counter == "-1")
        {
          return LogAndReturnError(null, null, "Error updating counter in TG_DEVICE_ID");
        }

        writer.WriteStartDocument();
        writer.WriteStartElement("TRANSACTION");
        writer.WriteElementString("FUNCTION_TYPE", "DEVICE");
        writer.WriteElementString("COMMAND", "CUST_QUESTION");
        writer.WriteElementString("DISPLAY_BULKDATA", displayText);
        writer.WriteElementString("MAC_LABEL", entry.macLabel);
        writer.WriteElementString("COUNTER", counter);
        writer.WriteElementString("MAC", PrintMacAsBase64(entry.mac, counter));
        writer.WriteEndElement();
        writer.WriteEndDocument();
      }

      if (log)
        Log(request, entry.serialNbr); // Bug 19502 MJO - Log serial nbr

      return new GenericObject("xml", request.ToString(SaveOptions.DisableFormatting), "port", PORT);
    }

    public static object ProcessYesNoResponse(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string xml = (string)geo_args.get("xml");
      bool log = (bool)geo_args.get("log");
      string serialNbr = (string)geo_args.get("serial_nbr"); // Bug 19502 MJO - Log serial nbr

      XDocument responseXml = XDocument.Parse(xml);

      if (log)
        Log(responseXml, serialNbr); // Bug 19502 MJO - Log serial nbr

      // Bug 17898 MJO - Error out on cancel
      // Bug 19108 MJO - Changed cancel message as secondary port messages are now ignored
      if ("-1" != responseXml.Element("RESPONSE").Element("RESULT_CODE").Value || responseXml.Element("RESPONSE").Element("RESPONSE_TEXT").Value == "Cancelled by POS")
      {
        string message = "Unable to process Yes/No";
        if (responseXml.Element("RESPONSE").Element("RESPONSE_TEXT") != null)
          message = responseXml.Element("RESPONSE").Element("RESPONSE_TEXT").Value;

        // Bug 18058 MJO - Mask response
        return LogAndReturnError(null, MaskResponseXML(responseXml).ToString(), message);
      }

      string result = responseXml.Element("RESPONSE").Element("CUST_QUESTION_DATA").Value;

      return result == "YES";
    }

    // Bug 23096 MJO - Remove all offline transactions from device to prevent them from being posted
    public static object ProcessRemoveOffline(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string MACAddress = (string)geo_args.get("mac_address");
      bool log = (bool)geo_args.get("log");

      DeviceEntry entry = Devices[MACAddress];

      XDocument request = new XDocument();
      using (var writer = request.CreateWriter())
      {
        string counter = entry.getAndIncrementCounter().ToString();

        if (counter == "-1")
        {
          return LogAndReturnError(null, null, "Error updating counter in TG_DEVICE_ID");
        }

        writer.WriteStartDocument();
        writer.WriteStartElement("TRANSACTION");
        writer.WriteElementString("FUNCTION_TYPE", "SAF");
        writer.WriteElementString("COMMAND", "REMOVE");
        writer.WriteElementString("MAC_LABEL", entry.macLabel);
        writer.WriteElementString("COUNTER", counter);
        writer.WriteElementString("MAC", PrintMacAsBase64(entry.mac, counter));
        writer.WriteEndElement();
        writer.WriteEndDocument();
      }

      if (log)
        Log(request, entry.serialNbr); // Bug 19502 MJO - Log serial nbr

      return new GenericObject("xml", request.ToString(SaveOptions.DisableFormatting), "port", PORT);
    }

    // Bug 24482 MJO - e355 barcode support
    public static object ProcessBarcodeConfig(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string MACAddress = (string)geo_args.get("mac_address");
      bool log = (bool)geo_args.get("log");

      DeviceEntry entry = Devices[MACAddress];

      XDocument request = new XDocument();
      using (var writer = request.CreateWriter())
      {
        string counter = entry.getAndIncrementCounter().ToString();

        if (counter == "-1")
        {
          return LogAndReturnError(null, null, "Error updating counter in TG_DEVICE_ID");
        }

        writer.WriteStartDocument();
        writer.WriteStartElement("TRANSACTION");
        writer.WriteElementString("FUNCTION_TYPE", "BARCODE");
        writer.WriteElementString("COMMAND", "BCCONFIG");
        writer.WriteElementString("BCCFG_TRIGMODE", "2");
        writer.WriteElementString("BCCFG_SESTIMEOUT", "30000");
        writer.WriteElementString("MAC_LABEL", entry.macLabel);
        writer.WriteElementString("COUNTER", counter);
        writer.WriteElementString("MAC", PrintMacAsBase64(entry.mac, counter));
        writer.WriteEndElement();
        writer.WriteEndDocument();
      }

      if (log)
        Log(request, entry.serialNbr); // Bug 19502 MJO - Log serial nbr

      return new GenericObject("xml", request.ToString(SaveOptions.DisableFormatting), "port", PORT);
    }

    // Bug 24482 MJO - e355 barcode support
    public static object ProcessBarcodeConfigResponse(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      string xml = (string)geo_args.get("xml");
      bool log = (bool)geo_args.get("log");
      string serialNbr = (string)geo_args.get("serial_nbr"); // Bug 19502 MJO - Log serial nbr

      XDocument responseXml = XDocument.Parse(xml);

      if (log)
        Log(responseXml, serialNbr); // Bug 19502 MJO - Log serial nbr

      return null;
    }

    // Bug 24482 MJO - e355 barcode support
    public static object ProcessBarcode(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string MACAddress = (string)geo_args.get("mac_address");
      bool log = (bool)geo_args.get("log");

      DeviceEntry entry = Devices[MACAddress];

      XDocument request = new XDocument();
      using (var writer = request.CreateWriter())
      {
        string counter = entry.getAndIncrementCounter().ToString();

        if (counter == "-1")
        {
          return LogAndReturnError(null, null, "Error updating counter in TG_DEVICE_ID");
        }

        writer.WriteStartDocument();
        writer.WriteStartElement("TRANSACTION");
        writer.WriteElementString("FUNCTION_TYPE", "BARCODE");
        writer.WriteElementString("COMMAND", "SCAN");
        writer.WriteElementString("BCSCAN_TIMEOUT", "10000");
        writer.WriteElementString("BCSCAN_ENCODE", "2");
        writer.WriteElementString("MAC_LABEL", entry.macLabel);
        writer.WriteElementString("COUNTER", counter);
        writer.WriteElementString("MAC", PrintMacAsBase64(entry.mac, counter));
        writer.WriteEndElement();
        writer.WriteEndDocument();
      }

      if (log)
        Log(request, entry.serialNbr); // Bug 19502 MJO - Log serial nbr

      return new GenericObject("xml", request.ToString(SaveOptions.DisableFormatting), "port", PORT);
    }
    
    // Bug 24482 MJO - e355 barcode support
    public static object ProcessBarcodeResponse(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      string xml = (string)geo_args.get("xml");
      bool log = (bool)geo_args.get("log");
      string serialNbr = (string)geo_args.get("serial_nbr"); // Bug 19502 MJO - Log serial nbr

      XDocument responseXml = XDocument.Parse(xml);

      if (log)
        Log(responseXml, serialNbr); // Bug 19502 MJO - Log serial nbr

      if (responseXml.Element("RESPONSE").Element("RESULT_CODE").Value == "-1" && responseXml.Element("RESPONSE").Element("BARCODE_DATA") != null)
      {
        return responseXml.Element("RESPONSE").Element("BARCODE_DATA").Value;
      }
      else
      {
        return CASL_error.create_str(responseXml.Element("RESPONSE").Element("RESPONSE_TEXT").Value);
      }
    }

    public static object fCASL_ProcessLastTransaction(CASL_Frame frame)
    {
      GenericObject geo_args = frame.args;
      string MACAddress = (string)geo_args.get("mac_address");
      bool log = (bool)geo_args.get("log");

      return ProcessLastTransaction(MACAddress, log);
    }

    // Bug IPAY-1382 MJO - Last transaction request
    public static GenericObject ProcessLastTransaction(string MACAddress, bool log)
    {
      DeviceEntry entry = Devices[MACAddress];

      XDocument request = new XDocument();
      using (var writer = request.CreateWriter())
      {
        string counter = entry.getAndIncrementCounter().ToString();

        if (counter == "-1")
        {
          return LogAndReturnError(null, null, "Error updating counter in TG_DEVICE_ID");
        }

        writer.WriteStartDocument();
        writer.WriteStartElement("TRANSACTION");
        writer.WriteElementString("FUNCTION_TYPE", "REPORT");
        writer.WriteElementString("COMMAND", "LAST_TRAN");
        writer.WriteElementString("MAC_LABEL", entry.macLabel);
        writer.WriteElementString("COUNTER", counter);
        writer.WriteElementString("MAC", PrintMacAsBase64(entry.mac, counter));
        writer.WriteEndElement();
        writer.WriteEndDocument();
      }

      if (log)
        Log(request, entry.serialNbr); // Bug 19502 MJO - Log serial nbr

      return new GenericObject("xml", request.ToString(SaveOptions.DisableFormatting), "port", PORT);
    }

    public static object fCASL_ProcessLastTransactionResponse(CASL_Frame frame)
    {
      GenericObject geo_args = frame.args;
      string xml = (string)geo_args.get("xml");
      bool log = (bool)geo_args.get("log");
      string serialNbr = (string)geo_args.get("serial_nbr");

      return ProcessLastTransactionResponse(xml, log, serialNbr);
    }

    // Bug IPAY-1382 MJO - Last transaction response (return same fields as ProcessCardResponse where available)
    // Bug IPAY-1384 MJO - Added support (add'l fields) for recovering from orphaned charge STARTED records
    public static object ProcessLastTransactionResponse(string xml, bool log, string serialNbr)
    {
      XDocument doc = XDocument.Parse(xml);

      if (log)
        Log(doc, serialNbr);

      GenericObject ret = new GenericObject();

      bool approved = doc.Element("RESPONSE").Element("STATUS_CODE").Value == "4";
      string ctroutd = string.Format("C{0}", doc.Element("RESPONSE").Element("CTROUTD").Value);
      string paymentType = doc.Element("RESPONSE").Element("PAYMENT_TYPE").Value;
      string creditCardNbr = doc.Element("RESPONSE").Element("ACCT_NUM").Value;
      string authCode = doc.Element("RESPONSE").Element("AUTH_CODE") == null ? ctroutd : doc.Element("RESPONSE").Element("AUTH_CODE").Value;
      string paymentMedia = doc.Element("RESPONSE").Element("PAYMENT_MEDIA").Value;
      decimal amount = Decimal.Parse(doc.Element("RESPONSE").Element("AUTH_AMOUNT").Value);
      string invoice = doc.Element("RESPONSE").Element("INVOICE").Value;

      ret.set(
        "auth_str", ctroutd,
        "auth_nbr", authCode,
        "credit_card_nbr", creditCardNbr,
        "payment_type", paymentType.ToLower(),
        "amount", amount,
        "entry_method", "Unknown",
        "serial_id", invoice,
        "approved", approved
      );

      if (doc.Element("RESPONSE").Element("CARDHOLDER") != null)
      {
        string cardholder = doc.Element("RESPONSE").Element("CARDHOLDER").Value;
        string[] cardholderSplit = cardholder.Split('/');

        if (cardholderSplit.Length > 1)
        {
          ret.set(
            "payer_name", $"{cardholderSplit[1]} {cardholderSplit[0]}",
            "payer_lname", cardholderSplit[0],
            "payer_fname", cardholderSplit[1]
          );
        }
        else if (cardholderSplit.Length == 1)
        {
          ret.set("payer_name", cardholder);
        }
      }

      // Bug 23797 UMN store the card brand
      if (doc.Element("RESPONSE").Element("PAYMENT_MEDIA") != null)
      {
        string card_brand = ret.get("card_brand", paymentMedia) as string;
        ret.set("card_brand", card_brand);
      }

      return ret;
    }

    public static object IsDevicePaired(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string MACAddress = (string)geo_args.get("mac_address");

      // Bug 23199 UMN handle case where passed mac address has a trailing comma, like what the peripheral service reports
      MACAddress = MACAddress.TrimEnd(',');

      if (Devices.ContainsKey(MACAddress))
      {
        DeviceEntry entry = Devices[MACAddress];
        if (entry.mac == null)
          return "PAIR";
        return true;
      }

      return false;
    }

    // Bug 19153 MJO - Check to see if a MAC address in a comma-separated list matches with a registered one
    public static object GetMACFromList(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string MACAddresses = (string)geo_args.get("mac_addresses");

      foreach (string MACAddress in MACAddresses.Split(','))
      {
        if (Devices.ContainsKey(MACAddress))
        {
          return MACAddress;
        }
      }

      return false;
    }

    public static string GetClientID(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string MACAddress = (string)geo_args.get("mac_address");

      if (Devices.ContainsKey(MACAddress))
      {
        return Devices[MACAddress].clientID;
      }

      return null;
    }

    public static string GetDeviceIP(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string MACAddress = (string)geo_args.get("mac_address");

      if (Devices.ContainsKey(MACAddress))
      {
        return Devices[MACAddress].deviceIP;
      }

      return null;
    }

    public static string GetDeviceType(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string MACAddress = (string)geo_args.get("mac_address");

      if (Devices.ContainsKey(MACAddress))
      {
        return Devices[MACAddress].deviceType;
      }

      return null;
    }

    // Bug 19502 MJO - Return serial nbr so it can be used for logging later
    public static string GetSerialNbr(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string MACAddress = (string)geo_args.get("mac_address");

      if (Devices.ContainsKey(MACAddress))
      {
        return Devices[MACAddress].serialNbr;
      }

      return null;
    }

    public static object Unpair(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string MACAddress = (string)geo_args.get("mac_address");

      if (!Devices.ContainsKey(MACAddress))
      {
        return LogAndReturnError(null, null, "Could not locate device entry.");
      }

      DeviceEntry entry = Devices[MACAddress];

      entry.mac = null;
      entry.macLabel = "";

      string err = entry.updateDB();

      if (err != null)
        return LogAndReturnError(null, null, err);

      return true;
    }

    private static readonly XmlWriterSettings WRITER_SETTINGS = new XmlWriterSettings()
    {
      OmitXmlDeclaration = true,
      Encoding = Encoding.UTF8,
      IndentChars = "\t",
      Indent = true
    };

    private static readonly XmlReaderSettings READER_SETTINGS = new XmlReaderSettings()
    {
      CloseInput = false,
      IgnoreWhitespace = true,
      ConformanceLevel = ConformanceLevel.Fragment
    };

    /// <summary>
    /// Calculates the MAC of the COUNTER and base 64 encodes it as a String
    /// </summary>
    private static string PrintMacAsBase64(byte[] macKey, string counter)
    {
      // convert counter to bytes
      byte[] counterBytes = Encoding.UTF8.GetBytes(counter);

      // import AES 128 MAC_KEY
      HMACSHA256 hmac = new HMACSHA256(macKey);

      byte[] macBytes = hmac.ComputeHash(counterBytes);

      // perform hmac function and print as base 64
      return Convert.ToBase64String(macBytes);
    }

    private static GenericObject LogAndReturnError(string request, string response, string message)
    {
      // Bug 24644 MJO - Log in normal log so stack trace isn't printed
      Logger.LogInfo(String.Format("VeriFone Point system interface error occurred: {0}{1}{2}",
        message, request == null ? "" : Environment.NewLine + request, response == null ? "" : Environment.NewLine + response), "POINT");
      return new GenObj_Error(message);
    }

    // Bug 19502 MJO - Added serial nbr logging
    private static void Log(XDocument xml, string serialNbr)
    {
      XDocument newXML = new XDocument(xml);

      string reqOrResp = (newXML.Root.Name == "RESPONSE") ? "Point Response" : "Point Request";

      foreach (XElement element in newXML.Root.Elements().ToList())
      {
        if (element.Name == "MAC_LABEL" || element.Name == "MAC" || element.Name == "SIGNATUREDATA")
          element.Remove();
        
        // Bug 27368 MJO - Show that the signature data is there
        if (element.Name == "SIGNATUREDATA")
          element.Value = "...";
      }

      Logger.LogInfo(String.Format("{0}={1} Serial Nbr={2}", reqOrResp, newXML.ToString(), serialNbr), "");
    }
    // Bug 20284 MJO - EMV fields that don't start with EMV_TAG
    private static List<string> EMVTags = new List<string>(new string[] {
      "TAC_DEFAULT",
      "TAC_DENIAL",
      "TAC_ONLINE",
      "MERCHID",
      "TERMID",
      "ISSUER_SCRIPT_RESULTS",
      "REF_NUMBER",
      "VALIDATION_CODE",
      "VISA_IDENTIFIER"
    });

    // Bug 23345 MJO - Get all the unique fields so they can be checked for duplicates for new entries
    public static GenericObject GetDeviceFields(params object[] args)
    {
      GenericObject serialNbrs = new GenericObject();
      GenericObject deviceIPs = new GenericObject();
      GenericObject MACAddresses = new GenericObject();

      foreach (DeviceEntry entry in Devices.Values)
      {
        serialNbrs.insert("'" + entry.serialNbr + "'");
        deviceIPs.insert("'" + entry.deviceIP + "'");
        MACAddresses.insert("'" + entry.MACAddress + "'");
      }

      return new GenericObject(
        "serial_nbrs", serialNbrs,
        "device_ips", deviceIPs,
        "mac_addresses", MACAddresses
      );
    }

    // Bug 20284 MJO - These fields go at the top of the list
    private static List<string> EMVExcludeTags = new List<string>(new string[] {
      "EMV_CVM",
      "EMV_TAG_9F41",
      "EMV_TAG_9F06",
      "EMV_TAG_95",
      "EMV_TAG_9F10",
      "EMV_TAG_9B",
      "EMV_TAG_8A",
    });

    // Bug IPAY-521 MJO - Retrieve the entry method from the XML response
    public static object fCASL_GetEntryMethod (CASL_Frame frame)
    {
      GenericObject geoArgs = misc.convert_args(frame.args);
      return GetEntryMethod(geoArgs.get<string>("response_xml"));
    }

    private static object GetEntryMethod (string responseXml)
    {
      XDocument response = XDocument.Parse(responseXml);

      XElement element = response.Element("RESPONSE").Element("CARD_ENTRY_MODE");

      if (element == null)
        return CASL_error.create_str("Error: Could not find Point CARD_ENTRY_MODE in response XML").casl_object;

      return DescriptiveToDefaultEntryMode(element.Value);
    }

    // Bug IPAY-538 MJO - Support descriptive entry mode text
    private static string DescriptiveToDefaultEntryMode (string entryMode)
    {
      if (entryMode == null)
        return null;
      else if (entryMode.Contains("Contactless"))
        return "Contactless";
      else if (entryMode.Contains("Chip Read"))
        return "Chip Read";
      else if (entryMode.Contains("Manual"))
        return "Keyed";
      // IPAY-767 MJO - FSwipe was falling through here
      else if (entryMode.Contains("Fallback") || entryMode.Contains("FSwipe"))
        return "FSwipe";
      else if (entryMode.Contains("Swipe"))
        return "Swiped";
      else
        return entryMode;
    }

    // Bug 20284 MJO - Add EMV text to tender if present in response
    // Bug 22010 MJO - Don't show giant list of EMV tags on approval
    private static void SetEMVText(GenericObject tender, XDocument responseXml, bool addExtraFields)
    {
      // EMV testing
      //responseXml = XDocument.Parse("<RESPONSE>  <RESPONSE_TEXT>APPROVAL - 000 </RESPONSE_TEXT>  <RESULT>CAPTURED</RESULT>  <RESULT_CODE>4</RESULT_CODE>  <TERMINATION_STATUS>SUCCESS</TERMINATION_STATUS>  <COUNTER>305</COUNTER>  <TRANS_SEQ_NUM>1</TRANS_SEQ_NUM>  <INTRN_SEQ_NUM>6795294</INTRN_SEQ_NUM>  <AUTH_CODE>446605</AUTH_CODE>  <TROUTD>6795294</TROUTD>  <CTROUTD>431</CTROUTD>  <LPTOKEN>244085</LPTOKEN>  <PAYMENT_TYPE>CREDIT</PAYMENT_TYPE>  <BANK_USERDATA>MC</BANK_USERDATA>  <MERCHID>372627218889</MERCHID>  <TERMID>1525702</TERMID>  <BATCH_TRACE_ID>e4334bbb-3fe9-4c81-89a4-06bebdb38bf6</BATCH_TRACE_ID>  <TRANS_DATE>2018.04.23</TRANS_DATE>  <TRANS_TIME>08:35:24</TRANS_TIME>  <AUTHNWID>03</AUTHNWID>  <REFERENCE>000000000001</REFERENCE>  <AUTHNWNAME>MASTERCARD</AUTHNWNAME>  <APPROVED_AMOUNT>60.00</APPROVED_AMOUNT>  <TRANS_AMOUNT>60.00</TRANS_AMOUNT>  <PAYMENT_MEDIA>MC</PAYMENT_MEDIA>  <ACCT_NUM>************4111</ACCT_NUM>  <CARDHOLDER>UAT USA/Test Card 06      </CARDHOLDER>  <CARD_EXP_MONTH>**</CARD_EXP_MONTH>  <CARD_EXP_YEAR>**</CARD_EXP_YEAR>  <CARD_ENTRY_MODE>Chip Read</CARD_ENTRY_MODE>  <CARD_ABBRV>MC</CARD_ABBRV>  <EMV_TAG_4F>A0000000041010</EMV_TAG_4F>  <EMV_TAG_50>MASTERCARD</EMV_TAG_50>  <EMV_TAG_5F2A>0840</EMV_TAG_5F2A>  <EMV_TAG_5F34>01</EMV_TAG_5F34>  <EMV_TAG_82>1800</EMV_TAG_82>  <EMV_TAG_8A>00</EMV_TAG_8A>  <EMV_TAG_95>8000008000</EMV_TAG_95>  <EMV_TAG_9A>180423</EMV_TAG_9A>  <EMV_TAG_9B>6800</EMV_TAG_9B>  <EMV_TAG_9C>00</EMV_TAG_9C>  <EMV_TAG_9F02>60.00</EMV_TAG_9F02>  <EMV_TAG_9F03>0.00</EMV_TAG_9F03>  <EMV_TAG_9F07>FF00</EMV_TAG_9F07>  <EMV_TAG_9F0D>B0509C8000</EMV_TAG_9F0D>  <EMV_TAG_9F0E>0000000000</EMV_TAG_9F0E>  <EMV_TAG_9F0F>B0709C9800</EMV_TAG_9F0F>  <EMV_TAG_9F10>0110601003220000000000000000000000FF</EMV_TAG_9F10>  <EMV_TAG_9F12>MasterCard</EMV_TAG_9F12>  <EMV_TAG_9F1A>0840</EMV_TAG_9F1A>  <EMV_TAG_9F26>DF8D09EFFF9EC77C</EMV_TAG_9F26>  <EMV_TAG_9F27>40</EMV_TAG_9F27>  <EMV_TAG_9F34>5E0300</EMV_TAG_9F34>  <EMV_TAG_9F36>00A3</EMV_TAG_9F36>  <EMV_TAG_9F37>0C4CDCDC</EMV_TAG_9F37>  <EMV_MODE>ISSUER</EMV_MODE>  <EMV_CVM>SIGNATURE</EMV_CVM>  <EMV_CHIP_INDICATOR>CONTACT</EMV_CHIP_INDICATOR>  <TAC_DEFAULT>FE50BC2000</TAC_DEFAULT>  <TAC_DENIAL>0000000000</TAC_DENIAL>  <TAC_ONLINE>FE50BCF800</TAC_ONLINE>  <EMV_TAG_84>A0000000041010</EMV_TAG_84>  <EMV_TAG_9F21>083402</EMV_TAG_9F21>  <EMV_TAG_9F08>0002</EMV_TAG_9F08>  <EMV_TAG_9F09>0002</EMV_TAG_9F09>  <EMV_TAG_9F33>E0F8C8</EMV_TAG_9F33>  <EMV_TAG_9F35>22</EMV_TAG_9F35>  <EMV_TAG_8E>000000000000000042015E031F03</EMV_TAG_8E>  <MIME_TYPE>image/bmp</MIME_TYPE>  <INVOICE>840</INVOICE>  <RECEIPT_DATA>    <RECEIPT>      <TEXTLINE></TEXTLINE>      <TEXTLINE></TEXTLINE>      <TEXTLINE>              MMIS CHILDRENS HEALTH               </TEXTLINE>      <TEXTLINE>                  Childrens Test                  </TEXTLINE>      <TEXTLINE>             1935 MEDICAL DISTRICT DR             </TEXTLINE>      <TEXTLINE>                 DALLAS, TX 75235                 </TEXTLINE>      <TEXTLINE></TEXTLINE>      <TEXTLINE></TEXTLINE>      <TEXTLINE>Client ID:17211400010001                          </TEXTLINE>      <TEXTLINE>Merchant ID:372627218889                          </TEXTLINE>      <TEXTLINE>Terminal ID:1525702                               </TEXTLINE>      <TEXTLINE></TEXTLINE>      <TEXTLINE></TEXTLINE>      <TEXTLINE>04/23/18                                  08:35:34</TEXTLINE>      <TEXTLINE></TEXTLINE>      <TEXTLINE></TEXTLINE>      <TEXTLINE>                       SALE                       </TEXTLINE>      <TEXTLINE>                   Invoice:840                    </TEXTLINE>      <TEXTLINE></TEXTLINE>      <TEXTLINE></TEXTLINE>      <TEXTLINE>Account:MC ************4111                       </TEXTLINE>      <TEXTLINE>Payment Type:CREDIT                               </TEXTLINE>      <TEXTLINE>Cardholder:UAT USA/Test Card 06                   </TEXTLINE>      <TEXTLINE>Application Name:MasterCard                       </TEXTLINE>      <TEXTLINE>Application PAN:************4111                  </TEXTLINE>      <TEXTLINE>Transaction Total:USD$ 60.00                      </TEXTLINE>      <TEXTLINE>Card Entry Mode:Chip Read                         </TEXTLINE>      <TEXTLINE>Mode:Issuer                                       </TEXTLINE>      <TEXTLINE>Auth Network ID:03                                </TEXTLINE>      <TEXTLINE>Auth Network Name:MASTERCARD                      </TEXTLINE>      <TEXTLINE>Result:CAPTURED                                   </TEXTLINE>      <TEXTLINE>Authorization Code:446605                         </TEXTLINE>      <TEXTLINE>CTroutd:431                                       </TEXTLINE>      <TEXTLINE>Approved Amount:USD$ 60.00                        </TEXTLINE>      <TEXTLINE></TEXTLINE>      <TEXTLINE></TEXTLINE>      <TEXTLINE></TEXTLINE>      <TEXTLINE>Subtotal:                               USD$ 60.00</TEXTLINE>      <TEXTLINE></TEXTLINE>      <TEXTLINE>Total:                                  USD$ 60.00</TEXTLINE>      <TEXTLINE></TEXTLINE>      <TEXTLINE></TEXTLINE>      <TEXTLINE></TEXTLINE>      <TEXTLINE>                Signature Captured                </TEXTLINE>      <TEXTLINE></TEXTLINE>      <TEXTLINE>                SEQUENCE:00000036                 </TEXTLINE>      <TEXTLINE>                AID:A0000000041010                </TEXTLINE>      <TEXTLINE>                  TVR:8000008000                  </TEXTLINE>      <TEXTLINE>     IAD:0110601003220000000000000000000000FF     </TEXTLINE>      <TEXTLINE>                     TSI:6800                     </TEXTLINE>      <TEXTLINE>                      ARC:00                      </TEXTLINE>      <TEXTLINE></TEXTLINE>      <TEXTLINE></TEXTLINE>      <TEXTLINE>Tag 4F: AID A0000000041010                        </TEXTLINE>      <TEXTLINE>Tag 50: Application Label MASTERCARD              </TEXTLINE>      <TEXTLINE>Tag 8A: Application Response Code 00              </TEXTLINE>      <TEXTLINE>Tag 95: Terminal Verifications Results 8000008000 </TEXTLINE>      <TEXTLINE>Tag 9B: Transaction Status Info 6800              </TEXTLINE>      <TEXTLINE>Tag 9F10: Issuer Application Data (IAD) 0110601003</TEXTLINE>      <TEXTLINE>220000000000000000000000FF                        </TEXTLINE>      <TEXTLINE>Tag 9F39: POS Entry Mode 05                       </TEXTLINE>      <TEXTLINE>Tag 9F41: Transaction Seq Counter 00000036        </TEXTLINE>      <TEXTLINE>Tag 9F1E: IFD 3631393736303837                    </TEXTLINE>      <TEXTLINE>Tag 9F33: Terminal Capabilities E0F8C8            </TEXTLINE>      <TEXTLINE>Tag 9F21: Transaction Time 083402                 </TEXTLINE>      <TEXTLINE>Tag 9F06: Terminal AID A0000000041010             </TEXTLINE>      <TEXTLINE>Tag 84: Dedicated file name A0000000041010        </TEXTLINE>      <TEXTLINE>Tag 9F08: ICC App version num 0002                </TEXTLINE>      <TEXTLINE>Tag 9F09: Terminal App version num 0002           </TEXTLINE>      <TEXTLINE>Tag 9F35: Terminal Type 22                        </TEXTLINE>      <TEXTLINE></TEXTLINE>      <TEXTLINE></TEXTLINE>      <TEXTLINE></TEXTLINE>      <TEXTLINE></TEXTLINE>      <TEXTLINE>                THANKS, COME AGAIN                </TEXTLINE>      <TEXTLINE>                    Courtesy:                     </TEXTLINE>      <TEXTLINE>                     VeriFone                     </TEXTLINE>    </RECEIPT>  </RECEIPT_DATA></RESPONSE>");

      XElement responseElement = responseXml.Element("RESPONSE");

      if (responseElement.Document.Root.Elements().Where(x => x.Name.LocalName.Contains("EMV_TAG")).Count() == 0)
        return;

      string header = GetResponseXMLValue(responseElement, "EMV_CVM");

      switch (header)
      {
        case "SIGNATURE": header = "Signature Captured"; break;
        case "PIN": header = "Verified by PIN"; break;
        case "NONE": header = "NO CVM"; break;
      }

      string sequence = GetResponseXMLValue(responseElement, "EMV_TAG_9F41");
      string aid = GetResponseXMLValue(responseElement, "EMV_TAG_4F");
      string tvr = GetResponseXMLValue(responseElement, "EMV_TAG_95");
      string iad = GetResponseXMLValue(responseElement, "EMV_TAG_9F10");
      string tsi = GetResponseXMLValue(responseElement, "EMV_TAG_9B");
      string arc = GetResponseXMLValue(responseElement, "EMV_TAG_8A");

      List<string> output = new List<string>();

      output.Add(header);
      output.Add("");

      if (sequence != null)
        output.Add("SEQUENCE: " + sequence);

      output.Add("AID: " + aid);
      output.Add("TVR: " + tvr);
      output.Add("IAD: " + iad);
      output.Add("TSI: " + tsi);
      output.Add("ARC: " + arc);
      output.Add("");

      if (addExtraFields)
      {
      foreach (XElement element in responseElement.Elements().Where(item => item.Name.LocalName.StartsWith("EMV_TAG")))
      {
        if (!EMVExcludeTags.Contains(element.Name.LocalName))
          output.Add("Tag " + element.Name.LocalName.Split('_')[2] + ": " + element.Value);
      }

      foreach (string name in EMVTags)
      {
        string value = GetResponseXMLValue(responseElement, "name");

        if (value != null)
          output.Add(new CultureInfo("en-US").TextInfo.ToTitleCase(name.Replace('_', ' ')) + ": " + value);
      }

      output.Add("");
      }

      tender.set("emv_text", StringCompressor.CompressString(String.Join(Environment.NewLine, output)));
    }

    private static string GetResponseXMLValue(XElement responseElement, string name)
    {
      XElement element = responseElement.Element(name);
      return element == null ? null : element.Value;
    }

    // Bug 20284 MJO - Make GEO vector from Point response receipt text
    private static GenericObject MakeReceiptGEO(XDocument responseXml)
    {
      XElement receiptDataElement = responseXml.Element("RESPONSE").Element("RECEIPT_DATA");

      if (receiptDataElement == null)
        return null;

      GenericObject receiptGEO = new GenericObject();

      if (receiptDataElement.Element("RECEIPT").Elements("TEXTLINE").Count() == 0)
        return null;

      //bool skipRows = false;

      foreach (XElement textline in receiptDataElement.Element("RECEIPT").Elements("TEXTLINE"))
      {
        /*if (textline.Value.TrimStart().StartsWith("Tag"))
        {
          skipRows = true;
          receiptGEO.Remove(receiptGEO.getLength() - 1);
          receiptGEO.Remove(receiptGEO.getLength() - 1);
        }
        else if (skipRows && textline.Value.Length == 0)
          skipRows = false;

        if (!skipRows)*/
          receiptGEO.insert(textline.Value.TrimEnd());
      }

      return receiptGEO;
    }

    // Bug IPAY-876 MJO - Added Location ID field/column
    // Bug IPAY-914 MJO - Added P2PE Flag field/column
    private class DeviceEntry
    {
      public string serialNbr;
      public string MACAddress;
      public string deviceIP;
      public byte[] mac;
      public string macLabel;
      public int counter;
      public string clientID;
      public string deviceType; // Bug 24451 MJO - Device type now configurable
      public string locationID { get; set; }
      public bool p2peFlag { get; set; }

      public DeviceEntry(string serialNbr, string MACAddress, string deviceIP, byte[] mac, string macLabel, int counter, string clientID, string deviceType, string locationID, bool p2peFlag)
      {
        this.serialNbr = serialNbr;
        this.MACAddress = MACAddress;
        this.deviceIP = deviceIP;
        this.mac = mac;
        this.macLabel = macLabel;
        this.counter = counter;
        this.clientID = clientID;
        this.deviceType = deviceType;
        this.locationID = locationID;
        this.p2peFlag = p2peFlag;
      }

      public DeviceEntry(GenericObject dbEntry)
      {
        serialNbr = (string)dbEntry.get("SERIAL_NBR");
        string[] args = ((string)dbEntry.get("EXTERNAL_ID")).Split('|');
        counter = Convert.ToInt32(args[0]);
        macLabel = args[1] == "" ? null : args[1];
        mac = args[2] == "" ? null : Convert.FromBase64String(args[2]);
        MACAddress = args[3];
        deviceIP = args[4];
        clientID = args[5];
        deviceType = (string)dbEntry.get("DEVICE_TYPE");
        locationID = dbEntry.get<string>("LOCATION_ID", "");
        p2peFlag = dbEntry.get<bool>("P2PE_FLAG", false);

        /*if (deviceType == "POINT")
        {
          deviceType = "MX915POINT";
          updateDB();
        }*/
      }

      public int getAndIncrementCounter()
      {
        ++counter;

        string error_message = updateDB();

        if (error_message != null)
          return -1;

        return counter - 1;
      }

      public string insertIntoDB()
      {
        GenericObject database = c_CASL.c_object("TranSuite_DB.Config.db_connection_info") as GenericObject;
        Hashtable sqlArgs = new Hashtable();

        string sql = "INSERT INTO TG_DEVICE_ID (DEVICE_TYPE, SERIAL_NBR, EXTERNAL_ID, LOCATION_ID, P2PE_FLAG) VALUES (@device_type, @serial_nbr, @external_id, @location_id, @p2pe_flag)";
        sqlArgs["serial_nbr"] = serialNbr;
        sqlArgs["external_id"] = counter.ToString() + "|" + (macLabel == null ? "" : macLabel) + "|" + (mac == null ? "" : Convert.ToBase64String(mac)) + "|" + MACAddress + "|" + deviceIP + "|" + clientID;
        sqlArgs["device_type"] = deviceType;
        sqlArgs["location_id"] = locationID;
        sqlArgs["p2pe_flag"] = p2peFlag;

        string error_message = null;
        IDBVectorReader reader = db_reader.Make(database, sql, CommandType.Text, 0,
          sqlArgs, 0, 0, null, ref error_message);

        reader.ExecuteNonQuery(ref error_message);
        reader.CloseReader();

        return error_message;
      }

      public string updateDB()
      {
        GenericObject database = c_CASL.c_object("TranSuite_DB.Config.db_connection_info") as GenericObject;
        Hashtable sqlArgs = new Hashtable();

        string sql = "UPDATE TG_DEVICE_ID SET EXTERNAL_ID=@external_id, DEVICE_TYPE=@device_type, LOCATION_ID = @location_id, P2PE_FLAG = @p2pe_flag WHERE SERIAL_NBR=@serial_nbr";
        sqlArgs["serial_nbr"] = serialNbr;
        sqlArgs["external_id"] = counter.ToString() + "|" + (macLabel == null ? "" : macLabel) + "|" + (mac == null ? "" : Convert.ToBase64String(mac)) + "|" + MACAddress + "|" + deviceIP + "|" + clientID;
        sqlArgs["device_type"] = deviceType;
        sqlArgs["location_id"] = locationID;
        sqlArgs["p2pe_flag"] = p2peFlag;

        string error_message = null;
        IDBVectorReader reader = db_reader.Make(database, sql, CommandType.Text, 0,
          sqlArgs, 0, 0, null, ref error_message);

        reader.ExecuteNonQuery(ref error_message);
        reader.CloseReader();

        return error_message;
      }

      //Bug 24976 NK Change DELETE Query
      public string deleteFromDB()
      {
        GenericObject database = c_CASL.c_object("TranSuite_DB.Config.db_connection_info") as GenericObject;
        Hashtable sqlArgs = new Hashtable();

        string sql = "DELETE FROM TG_DEVICE_ID WHERE DEVICE_TYPE LIKE '%POINT' AND SERIAL_NBR=@serial_nbr";
        sqlArgs["serial_nbr"] = serialNbr;

        string error_message = "";
        IDBVectorReader reader = db_reader.Make(database, sql, CommandType.Text, 0,
          sqlArgs, 0, 0, null, ref error_message);

        reader.ExecuteNonQuery(ref error_message);
        reader.CloseReader();

        return error_message;
      }
    }
  }

  // Bug 20284 MJO - Compress EMV receipt text so it easily fits in the custom field table
  // Code based on https://stackoverflow.com/questions/7343465/compression-decompression-string-with-c-sharp
  static class StringCompressor// Bug 27153 DH - Made it static
  {

    public static string CASL_CompressString(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      string str = (string)geo_args.get("str");

      return CompressString(str);
    }
    
    /// <summary>
    /// Compresses the string.
    /// </summary>
    /// <param name="text">The text.</param>
    /// <returns></returns>
    public static string CompressString(string text)
    {
      // Bug 21952 UMN if text is null return original
      if (String.IsNullOrEmpty(text))
        return text;

      byte[] buffer = Encoding.UTF8.GetBytes(text);
      var memoryStream = new MemoryStream();
      using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Compress, true))
      {
        gZipStream.Write(buffer, 0, buffer.Length);
      }

      memoryStream.Position = 0;

      var compressedData = new byte[memoryStream.Length];
      memoryStream.Read(compressedData, 0, compressedData.Length);

      var gZipBuffer = new byte[compressedData.Length + 4];
      Buffer.BlockCopy(compressedData, 0, gZipBuffer, 4, compressedData.Length);
      Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gZipBuffer, 0, 4);
      return Convert.ToBase64String(gZipBuffer);
    }

    public static string CASL_DecompressString(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      string str = (string)geo_args.get("str");

      return DecompressString(str);
    }

    /// <summary>
    /// Decompresses the string.
    /// </summary>
    /// <param name="compressedText">The compressed text.</param>
    /// <returns></returns>
    public static string DecompressString(string compressedText)
    {
      // Bug 21952 UMN if not really base64String, then someone has likely called this on a string.
      // To wit: GEO.<decompress_string> "foo" </decompress_string> or DecompressString("foo");
      // Slight risk that we are not catching some other type of error.
      if (String.IsNullOrEmpty(compressedText))
        return compressedText;

      try
      {
        byte[] gZipBuffer = Convert.FromBase64String(compressedText);
        using (var memoryStream = new MemoryStream())
        {
          int dataLength = BitConverter.ToInt32(gZipBuffer, 0);
          memoryStream.Write(gZipBuffer, 4, gZipBuffer.Length - 4);

          var buffer = new byte[dataLength];

          memoryStream.Position = 0;
          using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Decompress))
          {
            gZipStream.Read(buffer, 0, buffer.Length);
          }
          return Encoding.UTF8.GetString(buffer);
        }
      }
      catch (Exception)// Bug 27153 DH - Clean up warnings when building ipayment
      {
        return compressedText;
      }
    }
  }
}
