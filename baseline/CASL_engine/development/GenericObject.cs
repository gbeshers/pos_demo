using System;
using System.Collections;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace CASL_engine
{
 /// <summary>
 /// COM Interface to GenericObject.
 /// Interface Added by Daniel Hofman - 
 /// Need this in order to pass as COM obj to and from DLLs or ActiveX controls.
 /// </summary>
 [Guid("DBE0E8C4-1C61-41f3-B6A4-4E2F353D3D05")]
 public interface IGenericObjectInterface
 {     
  object insert(Object value);
  object set(Object key, Object value);
  Object get(object key);
  bool has(Object key);
  Object remove(Object key);
  void removeAll();
  Object[] getObjectArr_StringKeys();   
  int getIntKeyLength();
 }
    
  

 /// <summary>
 /// Represents a collection of ordered key-and-value pairs. 
 /// Remarks: 
 /// GenericObject uses a private hashtable member to store unorderd key-and-value pairs. 
 /// As the element of key-and-value pair is added to a GenericObject, the pair is saved 
 /// in hashtable member and order of element is managed by GenericObject itself. 
 /// 
 /// In each element of key-and-value pair, A couple different kinds of Keys are used for 
 /// special purpose or the key management.
 /// o All System Keys start with character of "_". Reserved System Key include "_parent" 
 /// that indicates CASL type of a GenericObject instance. You can add your own system key 
 /// for any special purpose.
 /// o Integer key can be managed inside GenericObject if the user passes null as key. 
 /// Inside-managed Integer keys are zero-based consecutive 32-bit signed integer. public 
 /// member intKeyLength indicate the length of these zero-based consecutive integer keys. 
 /// o String keys is the common key that usually managed by the user. you can use those to 
 /// present C# class members or methods. The method of getStringKeys() will return an 
 /// ArrayList of string keys which are commonly used in foreach loop to get the individual 
 /// keyed value and also determine the length of string keys by ArrayList::count method.
 /// o complex key can be used. The methods on the any C# typed object, Object::Equals() 
 /// and Object::GetHashCode(), need override to get value based on this complex key. 
 /// GenericObject itself already override two methods, so it serves will for this purpose.
 /// 
 /// In each element of key-and-pair, Values can be null or any type of C# object including 
 /// GenericObject.
 /// </summary>
 /// 
 [Serializable]// Added by Daniel Hofman - Need this in order to pass as COM obj to and from DLLs or ActiveX controls on IIS.
 public class GenericObject : Hashtable, IGenericObjectInterface 
 {
  //private members
  public ArrayList  vectors=new ArrayList();

  /// <summary>
  /// 
  /// </summary>
  /// <param name="info"></param>
  /// <param name="context"></param>
  public GenericObject(SerializationInfo info, StreamingContext context) : base(info, context)
  {
   // Required only by IIS.

   // Added by Daniel Hofman
   // Need this in order to pass as COM obj to and from DLLs or ActiveX controls on IIS.
   // Deserialiszation constructor for hash table is protected so I 
   // need to expose it to IIS in order to deserialize this object.
  }

  public GenericObject()
  {
  }
  public GenericObject(params Object[] args)
  {
   bool bIsKey = true; 
   Object objLastKey = null; // handle only string keys
   bool bIntKey = false; 
   foreach (Object arg in args) // alternative key and value
   {
    // Handle Keys
    if ( bIsKey )
    {
     if ( arg == null ) // implicit vector key
       bIntKey= true; 
     else if ( arg is int && arg.Equals(vectors.Count))// explicit vector key
       bIntKey= true;  
     else  // string key and integer key
     {
      objLastKey = arg;
      bIntKey=false;
     }
    }
     // Handle Value
    else 
    {
     if( bIntKey ) 
     {
      vectors.Add( arg); //vector key 
      // SHIFT : move hash interger key to vector key
      while(Contains(vectors.Count))
      {
       int next_vector_key = vectors.Count;
       object next_vector_value = this[next_vector_key];
       Remove(next_vector_key);
       vectors.Add(next_vector_value);
      }
      // END SHIFT 
     }
     else set(objLastKey,arg); // string key and interger key TOCHANGE TO THIS[KEY]
    }
    bIsKey = !bIsKey;
   }
  }



  /// <summary>
  /// lookup only for STRING KEY (deny lookup for vector keys,interger keys, complex object keys)
  /// </summary>
  /// <param name="key"></param>
  /// <param name="lookup"> lookup=false</param>
  /// <returns></returns>
  public bool has(Object key, bool lookup)
  {
   if(key is int && ((int)key)<vectors.Count)
    return true;
   else if( Contains(key)) return true;
   else if( lookup && key is string)// TODO lookup for string key
   {
    GenericObject lookup_object = this;
    while (lookup_object.Contains("_parent"))
    {
     lookup_object = this["_parent"] as GenericObject;
     if(lookup_object == null)
      return false;
     else if( lookup_object.Contains(key))
      return true;
    } 
    return false;
//    GenericObject parent = get("_parent",null,false) as GenericObject;
//    if( parent == null)		
//     return false;
//    else
//     return parent.has(key,true);
   }
   else
    return false;
  }
  /// <summary>
  /// Determine whether the GenericObject contains a specific key
  /// </summary>
  /// <param name="key"></param>
  /// <returns></returns>
  public bool has(Object key)
  {
   if(key is int && ((int)key)<vectors.Count)
    return true;
   return Contains(key);
  }

  /// <summary>
  /// Get the value associated with the specified key.  
  /// If the key is not found, return the opt parameter return_object_for_fail
  /// </summary>
  /// <param name="key"></param>
  /// <param name="return_object_for_fail"></param>
  /// <returns></returns>
  public Object get(Object key, Object if_missing, bool lookup)
  {
   if(key is int && ((int)key)<vectors.Count)
    return vectors[(int)key];
   else if(Contains(key))
    return this[key];
   else if (lookup)
   {
    //						if(key.Equals("GEO"))
    //							return c_CASL.c_object("GEO");
    GenericObject parent = get("_parent",null,false) as GenericObject;
    if( parent== null)
    {
     return misc.handle_if_missing(if_missing,key,this);
    }

    //-------------------------------------------------------
    // special look up for GEO: look up my first then GEO itself
    //-------------------------------------------------------
    if( parent == c_CASL.GEO )
    {
     GenericObject my = (GenericObject)CASLInterpreter.get_my();
     Object return_obj = null;
     if ( my !=null )
      return_obj = my.get(key, null);
     if(return_obj!=null) 
      return return_obj;
     else
      return c_CASL.GEO.get(key,if_missing,false); // ?SHUANG: look at root GEO without lookup
    }
    else
     return parent.get(key,if_missing,true);
   }
   else
   {
    return misc.handle_if_missing(if_missing, key, this);
   }
  }	

  // keep this function for backward compatibility
  public Object get(Object key, Object if_missing)
  {
   if(key is int && ((int)key)<vectors.Count)
    return vectors[(int)key];
   else if ( !Contains(key) )
    return misc.handle_if_missing(if_missing, key, this);
   else
    return this[key];
  }	
  /// <summary>
  /// Get the value associated with the specified key. 
  /// </summary>
  /// <param name="key"></param>
  /// <returns></returns>
  public Object get(Object key)
  {
   if(key is int && ((int)key)<vectors.Count)
    return vectors[(int)key];
   else if ( !Contains(key) )
    return misc.handle_if_missing(c_CASL.c_error, key, this); // change inline
   else
    return this[key];
  }	

		
  /// <summary>
  /// Set the value associated with the specified key. 
  /// Add new key if key is not exist.
  /// </summary>
  /// <param name="key"></param>
  /// <param name="objValue"></param>
  public object set(Object key, Object value)
  {
   if(key is int &&  ((int)key)<=vectors.Count)
   {
    if( ((int)key)== this.vectors.Count)
     this.vectors.Add(value);
    else 
     vectors[(int)key]=value;

    // SHIFT : move hash interger key to vector key
     while(Contains(vectors.Count))
     {
      int next_vector_key = vectors.Count;
      object next_vector_value = this[next_vector_key];
      Remove(next_vector_key);
      vectors.Add(next_vector_value);
     }
    // END SHIFT 
   }
   else 
    this[key] = value;
   return this;
  }

  /// <summary>
  /// Set the multiple key_and_value pair. 
  /// Add new key if the key is not exist.
  /// </summary>
  /// <param name="key_and_value_pair_array">Specify the Array of keys and values alternatively</param>
  public object set(params object[] key_and_value_pair_array)
  {
   if( key_and_value_pair_array.Length%2 !=0 )
    throw new CASL_error("title","Syntax Error", "message","'set' requires key-value pairs, but odd number of arguments was found.");
   bool is_key = true;
   object key = null;
   foreach( object key_or_value in key_and_value_pair_array)
   {
    if(is_key)
    {
     key = key_or_value;
    }
    else
    {
     if(key is int &&  ((int)key)<=vectors.Count)
     {
      if( ((int)key)== this.vectors.Count)
       this.vectors.Add(key_or_value);
      else 
       vectors[(int)key]=key_or_value;
      // handle prossible SHIFT : move hash interger key to vector key
      while(Contains(vectors.Count))
      {
       int next_vector_key = vectors.Count;
       object next_vector_value = this[next_vector_key];
       Remove(next_vector_key);
       vectors.Add(next_vector_value);
      }
      // end of handle prossible SHIFT : move hash interger key to vector key
     }
     else 
      this[key] = key_or_value;
    }
    is_key=!is_key;
   }
   return this;
  }

  /// <summary>
  /// Add value to the next zero-based consecutive integer key. 
  /// The integer key length is based on intKeyLength.
  /// </summary>
  /// <param name="value"></param>
  /// <returns></returns>
  public object insert(Object value)
  {
    vectors.Add(value);
   // SHIFT : move hash interger key to vector key
   while(Contains(vectors.Count))
   {
    int next_vector_key = vectors.Count;
    object next_vector_value = this[next_vector_key];
    Remove(next_vector_key);
    vectors.Add(next_vector_value);
   }
   // SHIFT : move hash interger key to vector key
   return this;
  }			

  /// <summary>
  /// 
  /// </summary>
  /// <param name="key"></param>
  /// <param name="shift"></param>
  /// <param name="if_missing"></param>
  /// <returns></returns>
  public Object remove(Object key, bool shift, object if_missing)
  {
   if(key is int && ((int)key) < vectors.Count  )
   {
    int ikey = (int) key;
    object R =  vectors[ikey];
    if(shift) // SHIFT AUTOMATIC IN ARRAYLIST
     vectors.RemoveAt(ikey);
    else // UNSHIFT, MOVE VECTOR KEY-VALUE TO INTERGER KEY-VALUE
    {
     ArrayList interger_objects = vectors.GetRange(ikey+1,Count-ikey-1);
     vectors.RemoveRange(ikey,vectors.Count-ikey-1);
     int interger_key = ikey+1;
     foreach(object interger_object in interger_objects)
     {
      this[interger_key] = interger_object;
      interger_key++; 
     }
    }
    return R;
   }
   else if( Contains(key))
   {
    object R = this[key];
    base.Remove(key);
    return R;
   }
   else
     return misc.handle_if_missing(if_missing, key, this);  
  }

  /// <summary>
  /// Removes the element of key-and-value pair with integer key of intKeyLength-1, 
  /// and returns removed value
  /// </summary>
  /// <returns></returns>
  public Object remove()
  {
   int last_index = vectors.Count-1;
   object last_value = vectors[last_index];
   vectors.RemoveAt(last_index);
   return last_value;
  }														 
  /// <summary>
  /// Removes the element of key-and-value pair with the specified key, and returns removed value
  /// </summary>
  /// <param name="key"> shift=true if_missing=error</param>
  /// <returns></returns>
  public Object remove(Object key)
  {
   if(key is int && ((int)key)<vectors.Count)
   {
    Object R = vectors[(int)key];
    vectors.RemoveAt((int)key); // AUTOMATICALLY SHIFT
    return R;
   }
   else if (Contains(key))
   {
    Object R = this[key];
    Remove(key);
    return R;
   }
   else
    return misc.handle_if_missing(c_CASL.c_error,key,this);
  }

  /// <summary>
  /// 
  /// </summary>
  /// <param name="key">shift=true </param>
  /// <param name="if_missing"></param>
  /// <returns></returns>
  public Object remove(Object key, object if_missing)
  {
   return remove(key, true,if_missing);
  }

  /// <summary>
  /// Removes all elements of key-and-value pair
  /// </summary>
  public void removeAll()
  {
   vectors.Clear();
   Clear();
  }
	
  /// <summary>
  /// relative to the subject, go down a path of keys where the keys are integers or strings.
  /// slip path and convert path_part if it's integer
  /// GEO.x.0.y == GEO.<get_path> "x.0.y" </get_path>
  /// path=req lookup=false=bool if_missing=error
  /// </summary>
  /// <returns></returns>
  public Object get_path(object path, bool lookup, object if_missing) 	
  {
   string [] path_parts=null;
   try 
   {
    path_parts= (path as string).Split('.');
   } 
   catch (Exception e) 
   {
    object x = e;
    return (false);
   }
   Object path_it = this;
   foreach(string path_part_string in path_parts )
   {
    object path_part= path_part_string;
    char[] char_parts = path_part_string.ToCharArray();
    if(ConciseXMLCharSet2.IS_DIGIT(char_parts[0]))
     path_part = Convert.ChangeType(path_part_string,typeof(int));
    path_it = (path_it as GenericObject).get(path_part,if_missing, lookup);
    if (path_it!=null && false.Equals(path_it))
     return false;
   }
   return path_it;
  }
  //				public Object get_path(object path, bool lookup, object if_missing) 		
  //			{
  //				//convert string to path or symbol in order to prepare lookup
  //				if (path is String)
  //				{
  //					path = Tool.string_to_path_or_symbol(path.ToString());
  //				}
  //				GenericObject path_or_symbol = path as GenericObject;
  //				object a_value = this;
  //				if ( Tool.is_symbol(path_or_symbol) )
  //				{
  //					a_value = (a_value as GenericObject).get(path_or_symbol.get("name"),if_missing,lookup);//get_w_lookup(path_or_symbol,null);
  //				}
  //				else
  //				{
  //					for(int i=0; i< path_or_symbol.getLength(); i++)
  //					{
  //						object a_symbol_or_key = path_or_symbol.get(i);
  //
  //						if(  a_value is GenericObject && Tool.is_symbol(a_symbol_or_key))
  //							a_value = (a_value as GenericObject).get(Tool.symbol_to_string(a_symbol_or_key as GenericObject),if_missing,lookup);//get_w_lookup(a_symbol_or_key,null);
  //						else if( a_value is GenericObject)
  //							a_value = (a_value as GenericObject).get(a_symbol_or_key,if_missing,lookup);//get_w_lookup(a_symbol_or_key,null);
  //						else if( a_value != null) // handle primitive type lookup
  //							a_value = c_CASL.GEO.get(a_symbol_or_key,if_missing,lookup);//get_w_lookup(a_symbol_or_key,null);
  //						else 
  //							return misc.handle_if_missing(if_missing,path,this);
  //					}
  //				}
  //				if (a_value is GenericObject && a_value.Equals(this)) // no found
  //					return misc.handle_if_missing(if_missing,path,this);
  //				return a_value;
  //			}

  public Object get_path(object path, bool lookup) 		
  {
   return get_path(path,lookup,c_CASL.c_error);
  }

  public Object get_path(object path) 		
  {
   return get_path(path,false,c_CASL.c_error);
  }

  /// <summary>
  /// Get an ArrayList containing all the keys 
  /// - integer key, string key, system key, and complex keys.
  /// NOTE: 
  /// The order of the keys in the ICollection is unspecified in hashtable class,
  /// but it is the same order as the associated values in the ICollection returned by the Values method.
  /// </summary>
  /// <returns></returns>
  public ArrayList getKeys()
  {
   //				ArrayList al_keys = new ArrayList(Keys);
   //				IComparer myComparer = new GEO_comparer();
   //				al_keys.Sort(myComparer);
   //				return al_keys;
   ArrayList all_keys = new ArrayList(Keys);
  
   for(int i=0; i<vectors.Count; i++)
    all_keys.Add(i);
   return all_keys;
  }
  public ArrayList get_normalized_keys()
  {
   ArrayList all_keys = new ArrayList(Keys);
   ArrayList system_keys = new ArrayList();
   ArrayList no_system_no_vector_keys = new ArrayList();
   ArrayList keys = new ArrayList();
   foreach( object key in all_keys)
   {
    if( key is string && key.ToString().StartsWith("_"))
     system_keys.Add(key);
    else if ( key is int && (int)key < vectors.Count)
     continue;
    else
     keys.Add(key);
   }
   // sort string key 
   keys.Sort(new GEO_comparer());
   keys.InsertRange(0,system_keys);
   //				system_keys.CopyTo(keys,0);
   for(int i=0; i< vectors.Count;i++)
    keys.Add(i);
   return keys;
  }
	
  /// <summary>
  /// Get an ArrayList of string keys, TOASK: SHOULD THIS RE
  /// </summary>
  /// <returns></returns>
  public ArrayList getStringKeys()
  {

   ArrayList alStringKeys = new ArrayList();
   foreach (Object key in Keys )
   {
    if ( key is String && !(key.ToString().StartsWith("_") ) && (key.ToString().IndexOf("_f_")<0 ))
     //if ( key is String && !(key.ToString().StartsWith("_")) )
    {
     alStringKeys.Add(key);
    }
   }
   return alStringKeys;
  }				
							 
	
  public ArrayList getObjectKeys()
  {
   ArrayList alObjectKeys = new ArrayList();
   foreach (Object key in Keys )
   {
    if ( key is GenericObject )
    {
     alObjectKeys.Add( key );
    }
   }
   return alObjectKeys;
  }
      
  /// <summary>
  /// Get an ArrayList of system keys 
  /// </summary>
  /// <returns></returns>
  public ArrayList getSystemKeys()
  {
   ArrayList alKeys = new ArrayList();
   foreach (Object key in Keys )
   {
    if ( key is String && (key.ToString().StartsWith("_")) )
    {
     alKeys.Add(key);
    }
   }
   return alKeys;
  }			
		
  /// <summary>
  /// Get an ArrayList containing all the Value
  /// </summary>
  /// <returns></returns>
  public ArrayList getValues()
  {
   ArrayList all_values = new ArrayList(Values);
   all_values.SetRange(all_values.Count,vectors); //?
   return all_values;
   //			ArrayList alValues = new ArrayList();
   //			foreach ( Object key in Keys )
   //			{
   //				alValues.Add(this[key]);
   //			}
   //			return alValues;
  }

  /// <summary>
  /// NOT USED, USE COPY INSTEAD. ALWAYS RETURN NULL. Get a shallow clone of current GenericObject instance 
  /// </summary>
  /// <returns></returns>
  public GenericObject clone()
  {
   return base.Clone() as GenericObject;
  }

  /// <summary>
  /// use generic clone function in object
  /// this is not safe for dynamic change hashtable
  /// </summary>
  /// <returns></returns>
  public GenericObject fast_clone()
  {
   return (GenericObject)this.MemberwiseClone();
  }

  /// <summary>
  /// selective shallow copy 
  /// include and exclude can not be option at the same time. 
  /// 
  /// If include is not opt, then loop through include keys
  ///   if( object.has include key and (exclude is opt or not exclude.key_of key) )
  ///     copy each field
  /// else (include is  opt), then loop through all keys
  ///   if( not exclude.key_of key )
  ///     copy each field
  /// </summary>
  /// <param name="include"></param>
  /// <param name="exclude"></param>
  /// <returns></returns>
  public GenericObject copy(GenericObject include, GenericObject exclude)
  {
   GenericObject a_copy = (GenericObject)c_CASL.c_GEO();

   if (!c_CASL.c_opt.Equals(include))  // include=vector exclude=any
   {
    for(int i=0; i<include.getLength(); i++)
    {
     object key = include.get(i);
     if( has(key) && (c_CASL.c_opt.Equals(exclude) || false.Equals(exclude.key_of(key))))
      a_copy.set(key, get(key)); // included and not in exclude
    }
   }
   else  
   {
    IDictionaryEnumerator myEnumerator = Keys.GetEnumerator() as IDictionaryEnumerator;
    while ( myEnumerator.MoveNext() )
    {
     if( false.Equals(exclude.key_of(myEnumerator.Key)))
      a_copy.set(myEnumerator.Key, myEnumerator.Value);
    }
   }
   return a_copy;				
  }

	  
  public static GenericObject deepCopy(params Object[] arg_pairs)
  {
   GenericObject args = (GenericObject)misc.convert_args(arg_pairs);
   GenericObject geoToBeCopied = args.get("_subject", c_CASL.c_undefined) as GenericObject;
   GenericObject geoAlreadyCopied = (GenericObject)c_CASL.c_GEO();
   object oTopPath = geoToBeCopied.to_path();
   //		  return geoToBeCopied.deepCopyAux(geoAlreadyCopied, oTopPath, true);
   return geoToBeCopied.deepCopyAux(geoAlreadyCopied, oTopPath.ToString(), true);
  }

	
  public GenericObject deepCopyAux(GenericObject geoAlreadyCopied, String sTopPath, bool bIsTop)
  {
   //If the value to be copied has already been copied (by value), return the 
   //reference to it
   if( geoAlreadyCopied.has(this) )
    return geoAlreadyCopied.get(this) as GenericObject;

   bool bExternalObj = false;
   GenericObject a_copy = (GenericObject)c_CASL.c_GEO();
   IDictionaryEnumerator myEnumerator = Keys.GetEnumerator() as IDictionaryEnumerator;
   geoAlreadyCopied.set(this, a_copy);
   //Loop through all key/value pairs in the Generic Object
   while ( myEnumerator.MoveNext() )
   {
    //At the top level ONLY, do not copy the container
    //(Two people can't sit in the same chair...)
    if( ! "_container".Equals(myEnumerator.Key) || !bIsTop )
    {
     //If the Value to be copied is a Generic Object, determine if it's a 
     //reference to an external object (compare it's path with the top level path)
     if( myEnumerator.Value is GenericObject )
     {
      String sThisPath = "";
      sThisPath = (myEnumerator.Value as GenericObject).to_path().ToString();
      bExternalObj = !sThisPath.StartsWith(sTopPath);
     }
     //If the Value to be copied is not a generic object at all __OR__
     //the Value to be copied is an External generic object, 
     //simply copy the value (copy by reference, basically)
     if( !(myEnumerator.Value is GenericObject) || bExternalObj )
      a_copy.set(myEnumerator.Key, myEnumerator.Value);
      //If the Value to be copied is an Internal generic object, call deepCopy
      //recursively so that all internal values are copied by value, not reference
     else
      a_copy.set(myEnumerator.Key, (myEnumerator.Value as GenericObject).deepCopyAux(geoAlreadyCopied, sTopPath, false));
    }
   }
   return a_copy;
  }

		
  public GenericObject make_reverse()
  {
   ArrayList keys = getKeys();
   GenericObject reverse_obj = new GenericObject();
   foreach( object key in keys )
   {
    object value = this[key];
    if( key is String && ((string)key).StartsWith("_"))
     reverse_obj.set(key, value);
    else
     reverse_obj.set(value, key);
   }
   return reverse_obj;
  }

  /// <summary>
  /// Get the value of the last integer key
  /// </summary>
  /// <returns></returns>
  public object top_of()
  {
   if( vectors.Count == 0) 
    throw new CASL_error("message", "length is 0");
   else
    return vectors[vectors.Count];
  }	 
		
			
  public int getLength()
  {
   return vectors.Count;
  }
 /// <summary>
 /// TOREMOVE
 /// </summary>
 /// <returns></returns>
  public int getIntKeyLength()
  {
   return getLength();
  }

  //----------------------------------------
  // Format Menthods - XML, URI, HTML
  //---------------------------------------
  /// <summary>
  /// Returns a query String with conciseXML encoding that represents the current GenericObject
  /// e.g. ?key_1=value_1&key_2=value_2
  /// reference formatting issue:
  /// check to_path && contained_in
  /// check circular reference
  /// </summary>
  /// <returns></returns>
  /*
  public String to_query_string()
  {
   return misc.CASL_to_query_string("_subject",this);
   }
   */
  public String to_xml()
  {
   return misc.cs_GenericObject_to_xml(this);
  }

  /// <summary>
  /// Convert GenericObject to a html string
  /// </summary>
  /// <returns></returns>
  public String to_htm_large()
  {
   return to_htm_large("");
  }

  /// <summary>
  /// optinal param container is used to create relative path when it is avaiable.
  /// </summary>
  /// <param name="container"></param>
  /// <returns></returns>
  public String to_htm_large(String path)
  {
   String sReturn = "";
   if ( path.Length== 0 )
    path = this.toPath();
   //-------------------------------------------------------
   // Handle null
   if ( this == null)
   {
    return sReturn;
   }
   String sType = this.get("_parent", "GenericObject").ToString();
   sReturn += String.Format("<TR><TD colspan=2>A {0}</TD></TR>",sType);
   ArrayList ordKeys = this.get_normalized_keys();
   foreach ( Object objKey in ordKeys )
   {
    String sKey = objKey.ToString();
    if(sKey.StartsWith("_"))
     continue;
    Object objValue = this[objKey];
    String sValue = "";
    if ( objValue == null )
     sValue = "";
    else if ( objValue.GetType() ==  typeof(GenericObject) )
     sValue = ((GenericObject)objValue).to_htm_large(path + "." + sKey);
    else
     sValue = objValue.ToString();
    sReturn += String.Format("<TR ><TD><span id='{0}.key'>{1}</span></TD><TD><span id='.value'>{2}</span></TD></TR>", path, sKey ,sValue);
   }
   sReturn = String.Format("<Span id='{0}'><Table border='1' width='100%'>{1}</Table></Span>", path, sReturn);
   return sReturn;
  }
  /// <summary>
  /// optinal param container is used to create relative path when it is avaiable.
  /// </summary>
  /// <param name="container"></param>
  /// <returns></returns>
  public String to_htm_editable(String path)
  {
   String sReturn = "";
   if ( path.Length== 0 )
    path = this.toPath();
   //-------------------------------------------------------
   // Handle null
   if ( this == null)
   {
    return sReturn;
   }
   String sType = this.get("_parent", "GenericObject").ToString();
   sReturn += String.Format("<TR><TD colspan=2><H1>A {0}</TD></TR>",sType);
   ArrayList ordKeys = this.getStringKeys();
   foreach ( Object objKey in ordKeys )
   {
    String sKey = objKey.ToString();
    Object objValue = this[objKey];
    String sValue = "";
    if ( objValue == null )
     sValue = "";
    else if ( objValue.GetType() ==  typeof(GenericObject) )
     sValue = ((GenericObject)objValue).to_htm_large(path + "." + sKey);
    else
     sValue = objValue.ToString();
    sReturn += String.Format("<TR ><TD>{0}</TD><TD><INPUT type='text' id='{0}' name='{0}' value='{1}' ></TD></TR>", sKey ,sValue);
   }
   return sReturn;
  }

  /// <summary>
  /// Returns an URI String with conciseURI encoding that represents the current GenericObject
  /// </summary>
  /// <returns></returns>
  public String toUri()
  {
   String sReturn = "";

   //-------------------------------------------------------
   // Handle null
   if ( this == null)
   {
    return sReturn;
   }

   String sStartXMLTag = "(", sEndXMLTag = ")";
   ArrayList ordKeys = this.getKeys();
   sReturn += sStartXMLTag;
   int iIndex = 0;
   foreach ( Object objKey in ordKeys )
   {
    String sKey = "", sValue = "";
    if ( objKey.GetType().IsPrimitive )
     sKey = "";
    else
     sKey = objKey.ToString() + "=";
    Object objValue = this[objKey];
    if ( objValue == null )
     sValue = "";
    else if ( objValue.GetType() ==  typeof(GenericObject) )
     sValue = ((GenericObject)objValue).toUri();
    else
     sValue = objValue.ToString();

    if ( iIndex > 0)
     sReturn += CASLParser.CONCISEURI_FIELD_CONNECTOR + sKey + sValue;
    else 
     sReturn += sKey + sValue;
    iIndex++;
   }
   sReturn += "\n" + sEndXMLTag;
   return sReturn;
  }

  //----------------------------------------
  // Path related Methods
  //---------------------------------------

  /// <summary>
  /// Use relection to get path of a c# typed instance, path of an instance is like "transaction.of.123"
  /// </summary>
  /// <returns></returns>
  public String toPath()
  {
   String sPrimaryValue = "";
   Object objPrimaryValue = getPrimaryValue();
   if ( objPrimaryValue == null)
    sPrimaryValue = "";
   else
    sPrimaryValue = objPrimaryValue.ToString();
   String sPath = String.Format("{0}.of.{1}", this.get("_parent", "GenericObject"), sPrimaryValue);
   return sPath;
  }

  /// <summary>
  /// new to_path functions: 
  /// return <concat> [_container.<to_path/>] _name</concat> if path is avaiable
  /// return null if path is not avaiable (no _name or _container)
  /// return "" if _subject is GEO
  /// </summary>
  /// <returns></returns>
  public object to_path()
  {
   return misc.CASL_to_path("_subject",this, "root",c_CASL.GEO);
  }


  /// <summary>
  /// Use reflection to get primary value from primary_key
  /// </summary>
  /// <returns></returns>
  public Object getPrimaryValue()
  {
   return null;
   //			Object objPrimaryValue = null;
   //			if ( this.GetType() == typeof(GenericObject) )
   //				objPrimaryValue = this.get(_primary_key);
   //			else if (this._primary_key != null && this._primary_key.Length !=0)
   //				CASLParser.CallMember(this, this._primary_key, ref objPrimaryValue);
   //			return objPrimaryValue;
  }
															 
  //----------------------------------------
  // Convenient Menthods
  //---------------------------------------
  // Add getStringValues() ; getStringKeys()
  //----------------------------------------
  /// <summary>
  /// Get an ArrayList of values associated with the specified "_parent" text - type
  /// </summary>
  /// <param name="type"></param>
  /// <returns></returns>
  public ArrayList getTypedValues(String type)
  {
   ArrayList alValues = new ArrayList();		
   foreach( Object objValue in getValues())
   {
    if ( objValue is GenericObject )
    {
     GenericObject thValue = (GenericObject)objValue;
     if( type.Equals(((String)thValue.get("_parent"))) )
     {
      alValues.Add(thValue);
     }
    }
   }
   return alValues;
  }

  /// <summary>
  /// Flat special field object to the multiple meta fields in the CASL file definition
  /// e.g. 
  /// field: outer_key=<field> key='inner_key' value='default_value' req=true </field>
  /// meta_field: outer_key='default_value' outer_key_f_req=true
  /// </summary>
  /// <returns></returns>
  public bool flat_field()
  {
   ArrayList keys = this.getKeys().Clone() as ArrayList;		
   foreach( Object key in keys)
   {
    object obj_value = get(key);
    if ( obj_value is GenericObject)
    {
     GenericObject gobj_value = obj_value as GenericObject;
     //flat field by using meta key 
     if ( gobj_value.get("_parent", "").ToString() == "field" )
     {
      Object outer_key = key;
      Object inner_key = "";
      GenericObject the_container = this;
      GenericObject the_field = gobj_value;
      if(!the_field.has("key"))
      {
       return false;
      }
      inner_key = the_field.get("key");
      if ( inner_key.ToString() != outer_key.ToString())
       return false;
      the_container.set(inner_key, the_field.get("value"));
      ArrayList field_keys = the_field.getStringKeys();
      foreach ( Object field_key in field_keys )
      {
       if(field_key.ToString()!="key" && field_key.ToString()!="value")
       {
        the_container.set(inner_key+"_f_"+field_key, the_field.get(field_key));
       }
      }
     }
      // flat field for nested GenericObject
     else 
      gobj_value.flat_field();
    }
   }
   return true;
  }

  /// <summary>
  /// Get the field object from the flatted meta fields in the CASL file definition
  /// </summary>
  /// <param name="key"></param>
  /// <returns></returns>
  public GenericObject get_field(Object key)
  {
   if( !has(key) )
    return null;
			
   GenericObject the_field = new GenericObject("_parent","field", "key",key, "value", get(key));
   ArrayList keys = getStringKeys();
   foreach( Object any_key in keys)
   {
    String s_any_key = any_key.ToString();
    if( s_any_key.IndexOf("" + key +"_f_") >=0)
    {
     String inner_key = s_any_key.Replace("" + key +"_f_", "");
     the_field.set(inner_key, get(any_key));
    }
   }
   return the_field;
  }


  /// <summary>
  /// Get the value which has the specified key_and_value pair
  /// </summary>
  /// <param name="key_in_value"></param>
  /// <param name="value_in_value"></param>
  /// <returns></returns>
  public ArrayList get_with_value(String key_in_value, Object value_in_value)
  {
   ArrayList alValues = new ArrayList();		
   foreach( Object objValue in getValues())
   {
    if ( objValue is GenericObject )
    {
     GenericObject thValue = (GenericObject)objValue;
     if( (thValue.get(key_in_value)).Equals(value_in_value) )
     {
      alValues.Add(thValue);
     }
    }
   }
   return alValues;
  }     

  /// <summary>
  /// Get the value which has the specified key
  /// </summary>
  /// <param name="key_in_value"></param>
  /// <returns></returns>
  public ArrayList get_with_key(String key_in_value)
  {
   ArrayList alValues = new ArrayList();		
   foreach( Object objValue in getValues())
   {
    if ( objValue is GenericObject )
    {
     GenericObject thValue = (GenericObject)objValue;
     if( ( thValue.has(key_in_value)) )
     {
      alValues.Add(thValue);
     }
    }
   }
   return alValues;
  }     
	    
  /// <summary>
  /// Get an Object Array which contains alternative string key and its value
  /// </summary>
  /// <returns></returns>
  public Object[] getObjectArr()
  {
   ArrayList KeysArr = getStringKeys();
   int iCounter = 0;
   object[] args = null; // Create parameter list.
   if(KeysArr.Count > 0)
   {
    args = new Object[KeysArr.Count*2];
    foreach ( Object key in KeysArr )
    {
     args[iCounter] = key;
     Object objValue = get(key);
     if(objValue!= null)
      args[iCounter+1] = objValue;
     else
      args[iCounter+1] = null;
     iCounter += 2;
    }
   }  
   return args;
  }

  /// <summary>
  /// Get an Object Array which contains alternative any key and its value
  /// </summary>
  /// <returns></returns>
  public Object[] getObjectArr_All()
  {
   ArrayList KeysArr = this.get_normalized_keys();
   int iCounter = 0;
   object[] args = null; // Create parameter list.
   if(KeysArr.Count > 0)
   {
    args = new Object[KeysArr.Count*2];
    foreach ( Object key in KeysArr )
    {
     args[iCounter] = key;
     Object objValue = get(key);
     if(objValue!= null)
      args[iCounter+1] = objValue;
     else
      args[iCounter+1] = null;
     iCounter += 2;
    }
   }  
   return args;
  }
	    
  /// <summary>
  /// Get an Object Array which contains all string keys
  /// </summary>
  /// <returns></returns>
  public Object[] getObjectArr_StringKeys()
  {
   return getStringKeys().ToArray();
  }
  /// <summary>
  /// Get an Object Array which contains all keys
  /// </summary>
  /// <returns></returns>
  public Object[] getObjectArr_AllKeys()
  {

   return getKeys().ToArray();
  }

  /// <summary>
  /// reverse the interger_key_and_value pairs
  /// </summary>
  public void reverse()
  {
   GenericObject go_reverse = new GenericObject();
   for(int i=0; i< this.getLength(); i++)
   {
    int new_i = getLength() -i-1;				
    go_reverse.set(new_i,get(i));
   }
   this.set(go_reverse.getObjectArr_All());
   return;
  }
	   
  /// <summary>
  /// Test if the current instance is the Error GenericObject
  /// <Error> message='error' </Error>
  /// </summary>
  /// <returns></returns>
  public bool isError()
  {
   if( "error".Equals(get("_parent","").ToString().ToLower()) )
   {
    return true;
   }
   return false;
  }
  /// <summary>
  /// If the GenericObject contains the value in a field, return the key for that field, else return false.
  /// </summary>
  /// <param name="value"></param>
  /// <returns></returns>
  public Object key_of(Object the_value)
  {
   IDictionaryEnumerator myEnumerator = Keys.GetEnumerator() as IDictionaryEnumerator;
   while ( myEnumerator.MoveNext() )
   {
    object key = myEnumerator.Key;
    object value = myEnumerator.Value;
    if( key is string && ((string)key).StartsWith("__")) // ignore special system key such as __length, __parsed_field_order
     continue;
    if( the_value == null && value == null)
     return key;
    else if( the_value is GenericObject )
    {
     if(the_value== myEnumerator.Value) // compare reference for none-primitive value
      return key;
    }
    else if( the_value.Equals(myEnumerator.Value)) // handle primitive value
     return myEnumerator.Key;
   }
   return false;
  }
			
  public override int GetHashCode()
  {
   string s_hash_code = toPath(); //use path
   if( s_hash_code == null || s_hash_code.Length==0)
    s_hash_code = CASLParser.encode_conciseXML(this,true); // use conciseXML
   int hash_code = s_hash_code.GetHashCode();
   return hash_code;
  }

  public bool DeepEquals(Object obj)
  {
   if( base.Equals(obj)) // compare reference first
    return true;
   //			else if( !(obj.GetType() == typeof(GenericObject))) // compare C# type
   //							return base.Equals(obj);
   //			return this._baseHashTable.Equals((obj as GenericObject)._baseHashTable);

   if( obj ==null || !(obj.GetType() == typeof(GenericObject))) // compare C# type
    return base.Equals(obj);
   GenericObject c_obj = obj as GenericObject;
   ArrayList my_keys = getKeys();
   ArrayList c_keys = c_obj.getKeys();
   if ( my_keys.Count != c_keys.Count ) // compare key length
    return false;
   for(int index =0; index< my_keys.Count; index++)
   {
    //-------------------------------------- 
    // compare all key and value in hashtable without the consideration of orderkeys
    //-------------------------------------- 
    object my_key = my_keys[index];
    //				object c_key = c_keys[index];
    //				if((my_key is string && my_key.ToString().StartsWith("__"))|| (c_key is string && c_key.ToString().StartsWith("__")))
    //					continue;
    //				if( !my_key.Equals(c_key) )
    //					return false;
    object my_value = get(my_key);

    if( !c_obj.has(my_key) ) return false;
    object c_value = c_obj.get(my_key);
    if ( my_value == null || c_value == null )
    {
     if( my_value != c_value)
      return false;
    }
    else if( (my_value is GenericObject) && (c_value is GenericObject) )
    {
     if(! (my_value as GenericObject).DeepEquals(c_value))
      return false;
    }
    else if( !my_value.Equals(c_value) ) // prone to recursive call 
     return false;
    //				object my_key = my_keys[index];
    //				object c_key = c_keys[index];
    ////				if((my_key is string && my_key.ToString().StartsWith("__"))|| (c_key is string && c_key.ToString().StartsWith("__")))
    ////					continue;
    //				if( !my_key.Equals(c_key) )
    //					return false;
    //				object my_value = get(my_key);
    //				object c_value = c_obj.get(c_key);
    //				if ( my_value == null || c_value == null )
    //				{
    //					if( my_value != c_value)
    //						return false;
    //				}
    //				else if( !my_value.Equals(c_value) )
    //					return false;
   }
   return true;
  }
  public object get_type(object key)
  {
   return get(misc.field_key(key,"type"),false,true);
  }

  // definiton of contained check _container and _name
  public bool is_contained()
  {
   if( !(has("_container")) || !(has("_name")) ) // use part_name_key instead of _name
    return false;
   if( (get("_container") as GenericObject).get( get("_name")).Equals(this) )
    return true;
   return false;
  }
  public bool is_contained_in(GenericObject a_container)
  {
   if ( is_contained() && get("_container").Equals(a_container) )
    return true;
   return false;
  }
 }
}
