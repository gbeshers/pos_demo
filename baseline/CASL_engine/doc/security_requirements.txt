Security:

Tests:
o Create invalid URLs to verify that the system doesn't break or let you in

o Turn off JavaScript validation, and enter data directly

o use .html extension to 
o try removing extension 
o try accessing root


TODO:
o If bad URI, what should it do?
  - Very likely that someone is trying to break into the system
  - Should log it explicitly?
  - Should we tell the user to stop?
  - What page should it show?
       Error explaining why it failed? (too much information)
       Kick them out an go back to Portal home?

o machine and network security settings
  - IIS only handles limited file extensions:  .gif, .jpg, .css, .js, .htm    MUST have an extension!
  
o CASL server filtering:
  - path can only contain specific alphanumeric chars and _, /,  no double-slash
  
o declaring public fields.  Used when server is calculating requested_object.  Can only go down public fields.

o 

uri path filter
validatoin for process call
SQL injection
encrption







https://localhost/site/foo.htm
  relatively link: "/bar.htm"  ==>  https://localhost/site/bar.htm
  need to put absolute URI if not using the page's as root.
  "/site_logical/foo.htm"
  
  <A href="http://"/>
  
  login (coming from authentication)
  How sensitive is their SID?  send encrypted in URI query.
  
  <ssn> "234234234324" </ssn>   a_ssn=req=ssn
  flag -> this form needs to be encrypted.  FORM action needs
    to use HTTPS.  
  When my/1.htm => 
  easy: everything http or everything is https.
  mixed: what things need https.  
  IMG-src, A-href, FORM-action, SCRIPT.src, ...
    <uri> "/" site_logical </uri>
  hard: some users using HTTPS, some HTTP.  Based on department.
  FORM -> process method.  How to know if process method submit
   needs to be encrypted?  Showing data that should be encrypted.
   field's type is a type that needs encryption. or field value
   is a class that needs encryption.  
   If there is any data that needs to be encrypted, the whole
      process/form is encrypted.
   Could be at the process level or status of process level.
   process.make -> Tender.credit_card -- multiple pages.
   Good enough to put encrypted flag into class. 
      Calculate the encrypted flag by looking at all the field type
        needs encryption.
        
Type.<defclass _name='ssn'> encrypt=true </defclass>

process.<defclass _name='registration'>
  a_ssn=req=ssn
</defclass>

business.<defmethod _name='init_class'>
 .<set_encrypt/>
</defmethod>

process.<defmethod _name='init_class'>
 .<set_encrypt/>
</defmethod>

GEO.<defmethod _name='set_encrypt'>
 ._field_order.<for_each> 
   <if> .<get_type> value </get_type>.<get> "encrypt" </get>
           <do>
            .<set> _encrypt = true </set>
            <return/>
           </do>
   </if>
 </for_each>

<test>
 process.registration._encrypt
 true
</test> 

Server: server root.
 server serves out a server root object.
 root=GEO
 root=<GEO my=my process/>
 
Only go down the object path, not system field.

handle_request  =>  look at path (before the query)
 not process anything without .htm   to_htm_top
 no special characters.  ignore non-printable ascii characters.
 
SQL: generated and protected from any special characters.

Validation for any calls.  process_call needs to validate
  the method it calls.  
  all arguments must be typed or the default type is
     alphanumeric50
  all keys are alphanumeric50
 
Database vs. server: only the Web server can access the
  database.  Login-user can have security settings.  Read/write on
  specific tables.
  
Registry, the whole computer security.
  
Authentication (token)

Verify that IIS can only deliver static data: .gif, .jpg, .css, .js
  can't access any other page or extension or folder
  Certain folders can be http: images
  .htm or gif or jpg.
  No access to folders:
    http://localhost/site_template/casl_ide/
  Main top site works:
    http://localhost/site_template

IIS User: anybody who comes to page, can write to database,
  Want to control/configure the IIS Anonymous user settings.
  
Encryption of CASL files.
  Protection of code/IP
     <encrypted key="a23423">29389f8as9d8fasdfoasidf</encrypted>
  Protection of sensitive passwords, etc.  
     
Encryption of elements of Config CASL files.
  Config file has encrypted parts
  <Config>
    database_password=<encrypted public_key="234234">as2938498fsadf</>
    
  </Config>
  Database username and password and encryption key.

Generate encrypted values.  /site/make_encrypted_value.htm

OR Config generation program write the file.  
  
  Credit card information

o communication channel: https for encrypting data
    doesn't encrypt images or style sheets. include or image
    
o networking - machine lockdown

o port 80 access:
   o only file extensions allowed are setup in IIS (.gif, .jpg)

OneStep communication with T4 hub:
 use HTTPS
 similar (or exactly) like a server root.
 only allowed to call a specific number of methods.
 needs to call CASL method, not C# method.


External communication with T4:
 private network or HTTPS



===================

Security:
categories
everything has granting ability
most should give rights, not to take something away
hightlight additions/take away
copy and modify it.
  No immediate benefit to dynamic updated from source.

permission/right/access/attribute
  type: enable or disable
  kind: read-only or modify or execute

Ex: void a transaction (before it is tendered.)
   permission to void a tender
   permission to void completed transaction
   MAY: permission to void different tender types
   
Ex: see everyone that is a cashier.
    see what groups that a person belongs to.

Ex: How an OS represents user groups

Access Control List:

Cashiering options

HIPPA - privacy
  

viewing of data

auditing can help reduce need for tight security

USER:
 o userid/login-name: use same as their standard
     unique?  Cashiering not unique userid, benefit.
     admin is picking it.  
 o displayid => might be employee number, ...
     display on receipts
     defaults to?: custom: usernumber, initials, 
     does it need to be unique? 
 o first name
 o last name
 o usernumber (private, primary_id)
 o other custom fields
    Ex: different systems have different IDs
    "acme.org/some_system/userid"
 o integration with external authentication ??
   o can login through another system.
   o can accept an authentication token from another
       system.  Option to not do password management.
 o log into system without central authentication sys
 o profiles/rights/security_access
 o we provide standard 'profiles' to use it
 o customers can create custom profiles.
 
 Places to display user-like info:
 o reports
 o receipts
 o see login
 o auditing
 
 
 REQ: don't show usernumber, can change userid
 REQ: for auditing, never change
 REQ: never delete users, always need to keep reference for historical reporting
 REQ: want a code to refer to a user (organization id)
      up to them for what to show
 Ex: having that displayid on receipts.
 

AUDITING: ---------
o knowing who did what across whole system
    research on objects.
o watch performance implications


NEXT STEP:
o organize this list
o proposal naming and structure
o get feedback from George, Jeff, ..
