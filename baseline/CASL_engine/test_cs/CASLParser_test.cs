using System;
using System.Windows.Forms;
using System.Collections;
using System.Data;
using System.Xml;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Soap;
using CASL_engine;


namespace T4_test
{
	using NUnit.Framework;

	[TestFixture]
	public class CASLParser_test
	{
		[SetUp]
		public void Init()
		{
		}
		[Test]
		public void CASL_object()
		{
//			GenericObject my_casl = CASLParser.decode_conciseXML(CASLParser.s_CASL,"");
		}
		[Test]
		public void n00_string_parent()
		{
			// test function
			String text = "<GEO>_parent='ROOT'</GEO>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject(
				"_parent","ROOT");
			Assert.AreEqual(E,R);
		}
		
		[Test]
		public void n01_primitive_and_symbol_value()
		{
			// test function
			String text = "<s> 'abc' 5 6  x</s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject(
					"_parent",Tool.string_to_path_or_symbol("s"), 
					null, "abc", 
					null, 5, 
					null, 6, 
				null, Tool.string_to_path_or_symbol("x"));
			Assert.AreEqual(E,R);
		}
		[Test]
		public void n01_decimal_value()
		{
			// test function
			String text = "<s>  x=5.9 </s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
//			GenericObject E = new GenericObject();
//			E.set("_parent", Tool.string_to_path_or_symbol("s"));
//			E.set("x", 5.9m);

			GenericObject E = new GenericObject(
				"_parent",Tool.string_to_path_or_symbol("s"), 
				"x", 5.9m);
			Assert.AreEqual(E,R);
		}
		[Test]
		public void n02_string_key_and_primitive_value_pair()
		{
			// test function
			String text = "<s> x=5 6 </s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject(
				"_parent",Tool.string_to_path_or_symbol("s"), 
				"x", 5, null, 6);
			Assert.AreEqual(E,R);
		}
		[Test]
		public void n03_interger_key()
		{
			//-------------------------------------------------------
			// test function
			String text = "<s> 56 = 7 </s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject("_parent",Tool.string_to_path_or_symbol("s"),  
				56, 7);
			Assert.AreEqual(E, R);// compare conciseXML presentation of object
		}
		[Test]
		public void n04_string_only_path_value_pair()
		{
			// test function
			String text = "<s> x=foo.bar 6</s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject(
				"_parent",Tool.string_to_path_or_symbol("s"), 
				"x", c_CASL.c_sy_n_pa("foo.bar"), null, 6);
			Assert.AreEqual(E,R);
		}
		[Test]
		public void n05_complex_value()
		{
			// test function
			String text = "<s> <x></x> 6</s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject(
				"_parent",c_CASL.c_sy_n_pa("s"), 
				null, new GenericObject("_parent",c_CASL.c_sy_n_pa("x")), null, 6);
			Assert.AreEqual(E,R);
		}
		[Test]
		public void n06_complex_key()
		{
			String text = "<s> <x></x> = <m> </m></s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject(
				"_parent",Tool.string_to_path_or_symbol("s"), 
				new GenericObject("_parent", Tool.string_to_path_or_symbol("x")), 
				new GenericObject("_parent", Tool.string_to_path_or_symbol("m")));
			Assert.AreEqual(E, R);
		}
		[Test]
		public void n07_multi_complex_value()
		{
			String text = "<s><x></x><y></y></s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject("_parent",Tool.string_to_path_or_symbol("s"),  
				null, new GenericObject("_parent", Tool.string_to_path_or_symbol("x")), 
				null, new GenericObject("_parent", Tool.string_to_path_or_symbol("y")));
			Assert.AreEqual(E, R);
		}
		[Test]
		public void n08_complex_value_with_nested_complex_value()
		{
			String text = "<s><x><v></v></x></s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject("_parent",Tool.string_to_path_or_symbol("s"),  
				null, new GenericObject("_parent", Tool.string_to_path_or_symbol("x"), 
				null, new GenericObject("_parent", Tool.string_to_path_or_symbol("v"))));
			Assert.AreEqual(E, R);
		}
		[Test]
		public void n09_complex_value_with_nested_complex_key_and_value()
		{
			String text = "<s><x><y></y>=<z></z></x></s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject("_parent",Tool.string_to_path_or_symbol("s"),  
				null, new GenericObject("_parent", Tool.string_to_path_or_symbol("x"), 
				new GenericObject("_parent", Tool.string_to_path_or_symbol("y")), new GenericObject("_parent", Tool.string_to_path_or_symbol("z"))));
			Assert.AreEqual(E, R);
		}
		[Test]
		public void n10_complex_value_with_multi_nested_complex_value()
		{
			String text = "<s><x><y></y><m><n></n>=5</m></x></s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject("_parent",Tool.string_to_path_or_symbol("s"),  
				null, new GenericObject("_parent", Tool.string_to_path_or_symbol("x"), 
					null, new GenericObject("_parent", Tool.string_to_path_or_symbol("y")), 
					null, new GenericObject("_parent", Tool.string_to_path_or_symbol("m"),
						new GenericObject("_parent", Tool.string_to_path_or_symbol("n")), 5)
						)
					
				);
			Assert.AreEqual(E, R);
		}
		[Test]
		public void n11_complex_value_with_nested_complex_key()
		{
			//String text = "<s> 5 6=10 8=<x></x> <y><x> 1=true</x></y>=<z><m></m>=<n></n> 10=8 </z></s>";
			String text = "<s> 5 6=10.2 8=<x></x> <y><x> 1=true</x></y>=<z><m></m>=<n></n> 10=8.8 </z></s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");			
			GenericObject E = new GenericObject("_parent",Tool.string_to_path_or_symbol("s"), null,5, 6,10.2m, 
				8, new GenericObject("_parent", Tool.string_to_path_or_symbol("x")), 
				new GenericObject("_parent", Tool.string_to_path_or_symbol("y"), null,new GenericObject("_parent",Tool.string_to_path_or_symbol("x"), 1,true)), 
				new GenericObject("_parent",Tool.string_to_path_or_symbol("z"),
				new GenericObject("_parent",Tool.string_to_path_or_symbol("m")),new GenericObject("_parent",Tool.string_to_path_or_symbol("n")), 
				10, 8.8m));
			Assert.AreEqual(E, R);
		}
		/// <summary>
		/// parser v3 <field/> is not processed in parser , 
		/// move the implementation to the interperter.
		/// comments out all the test cases related to it.
		/// </summary>
		//[Test]
		public void n12_field_value_with_nested_value()
		{
			String text = "<s> foo=<field> key= 'foo' value=<x>stuff</x> bar=<y>5</y></field></s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject("_parent",c_CASL.c_sy_n_pa("s"), 
				"foo", new GenericObject("_parent", Tool.string_to_path_or_symbol("x"), 
					null, c_CASL.c_sy_n_pa("stuff")), 
				"foo_f_bar", new GenericObject("_parent", c_CASL.c_sy_n_pa("y"), null, 5));
			Assert.AreEqual(E, R);
		}
		//[Test]
		public void n13_single_field_with_key_and_value()
		{
			String text = "<s> id=<field> key='id' value='stu_pay' </field> </s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject("_parent",c_CASL.c_sy_n_pa("s"), 
				"id", "stu_pay");
			Assert.AreEqual(E, R);
		}
		//[Test]
		public void n14_multi_fields_with_key_and_value()
		{
			String text = "<s> id=<field> key='id' value='stu_pay' </field> sid=<field> key='sid' value='none' </field> </s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject("_parent",c_CASL.c_sy_n_pa("s"), 
				"id", "stu_pay", 
				"sid", "none");
			Assert.AreEqual(E, R);
		}
		//[Test]
		public void n15_field_with_field_object_and_meta_field_name()
		{
			String text = "<s>y=<field>key='y' value='none' type='string'</field></s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject("_parent",c_CASL.c_sy_n_pa("s"), 
				"y", "none",
				"y_f_type","string");
			Assert.AreEqual(E, R);
		}
		//[Test]
		public void n16_field_with_field_object_and_int_key()
		{
			String text = "<s>y=<field>key='y' value='none' 0='string'</field></s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject("_parent",c_CASL.c_sy_n_pa("s"), 
				"y", "none",
				"y_f_type","string");
			Assert.AreEqual(E, R);
		}
		[Test]
		public void n17_symbol_string_path()
		{
			String text = "<s> foo.bar a</s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject("_parent",c_CASL.c_sy_n_pa("s"), 
				null, new GenericObject("_parent", "path", 
					null, c_CASL.c_sy_n_pa("foo"), null, "bar"),
				null, c_CASL.c_sy_n_pa("a")
				);
			Assert.AreEqual(E, R);
		}
		[Test]
		public void n18_symbol_object_path()
		{
			String text = "<s> foo.<bar></bar> </s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			//-------------------------------------------------------
			// create expected result object
			GenericObject E = new GenericObject("_parent",c_CASL.c_sy_n_pa("s"), 
				null, new GenericObject("_parent", "path", 
				null, c_CASL.c_sy_n_pa("foo"), null, c_CASL.c_call("bar"))
				);
			Assert.AreEqual(E, R);
		}
		[Test]
		public void n19_symbol_string_object_path()
		{
			String text = "<s> foo.bar.<bar></bar> </s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject("_parent",c_CASL.c_sy_n_pa("s"), 
				null, new GenericObject("_parent", "path", 
					null, c_CASL.c_sy_n_pa("foo"), 
					null, "bar",
					null, c_CASL.c_call("bar"))
				);
			Assert.AreEqual(E, R);
		}
		[Test]
		public void n20_object_string_path()
		{
			String text = "<s> <bar></bar>.foo </s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject("_parent",c_CASL.c_sy_n_pa("s"), 
				null, new GenericObject("_parent", "path", 
				null, c_CASL.c_call("bar"),
				null, "foo")
				);
			Assert.AreEqual(E, R);
		}		
		[Test]
		public void n21_symbol_object_string_path()
		{
			String text = "<s> foo.<bar/>.foo </s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject("_parent",c_CASL.c_sy_n_pa("s"), 
				null, new GenericObject("_parent", "path", 
				null, c_CASL.c_sy_n_pa("foo"), 
				null, c_CASL.c_call("bar"),
				null, "foo")
				);
			Assert.AreEqual(E, R);
		}		
		[Test]
		public void n22_ws_in_path()
		{
			String text = @"<s> foo. <bar/>.
				bar
			</s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject("_parent",c_CASL.c_sy_n_pa("s"), 
				null, new GenericObject("_parent", "path", 
				null, c_CASL.c_sy_n_pa("foo"), 
				null, c_CASL.c_call("bar"),
				null, "bar")
				);
			Assert.AreEqual(E, R);
		}		
		[Test]
		public void n23_subject_path()
		{
			String text = @"<s> .foo</s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject("_parent",c_CASL.c_sy_n_pa("s"), 
				null, new GenericObject("_parent", "path", 
				null, c_CASL.c_sy_n_pa("_subject"), 
				null, "foo")
				);
			Assert.AreEqual(E, R);
		}		
		[Test]
		public void n24_empty_object_path()
		{
			String text = @"<s/>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject("_parent",c_CASL.c_sy_n_pa("s"));
			Assert.AreEqual(E, R);
		}		
		[Test]
		public void n25_symbol_int_object_path()
		{
			String text = @"<s> foo.0.<bar/> </s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject("_parent",c_CASL.c_sy_n_pa("s"),
				null, new GenericObject("_parent","path",
					null, c_CASL.c_sy_n_pa("foo"),
					null,0, null, c_CASL.c_call("bar"))
				);
			Assert.AreEqual(E, R);
		}		
		[Test]
		public void n26_object_int_string_not_empty_object_path()
		{
			String text = @"<s> <bar/>.0.bar.<foo> x=5</foo> </s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject("_parent",c_CASL.c_sy_n_pa("s"),
				null, new GenericObject("_parent","path",
					null, c_CASL.c_call("bar"),
					null, 0,
					null, "bar",
					null, new GenericObject(
						"_parent", c_CASL.c_sy_n_pa("foo"),
						"x", 5))
				);
			Assert.AreEqual(E, R);
		}						
		[Test]
		public void n27_object_not_empty_object_string_path()
		{
			String text = @"<s> <bar/>.<foo> x=5</foo>.bar </s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject("_parent",c_CASL.c_sy_n_pa("s"),
				null, new GenericObject("_parent","path",
				null, c_CASL.c_call("bar"),
				null, new GenericObject(
					"_parent", c_CASL.c_sy_n_pa("foo"),
					"x", 5),				
				null, "bar")
				);
			Assert.AreEqual(E, R);
		}								
		[Test]
		public void n28_symbol_object_path_key_and_symbol_object_path_value()
		{
			String text = @"<s> foo.0.<bar/>=foo.0.<bar/></s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject("_parent",c_CASL.c_sy_n_pa("s"),
				new GenericObject("_parent","path",
				null, c_CASL.c_sy_n_pa("foo"),
				null,0, null, c_CASL.c_call("bar")),new GenericObject("_parent","path",
				null, c_CASL.c_sy_n_pa("foo"),
				null,0, null, c_CASL.c_call("bar"))
				);
			Assert.AreEqual(E, R);
		}		
		//[Test] // required file in the disk
		public void xml_parser_9()
		{
			String result = "";
			try
			{
				GenericObject obj1 = CASLParser.decode_conciseXML("",@"D:\DOTNETSPACE\docs\CASL_v2.xml") as GenericObject;
				// issue Note: C# doesn't support late binding, The developer need to cast object to its actually type all the time. or use reflection to call the method. For clarificaion, C# support polymophism with dynamic binding.
				GenericObject obj2 = ((GenericObject)((GenericObject)obj1.get("Biz")).get("Dept_base")).get_field("users");
				result = CASLParser.encode_conciseXML(obj1, false);
			}
			catch (Exception e)
			{
				throw e;
			}
			//-------------------------------------------------------
			// create expected result object
			GenericObject expect_object = new GenericObject("_parent","field",  
				"key", "users", 
				"value", Tool.string_to_path_or_symbol("[active_value]"), 
				"type", new GenericObject("_parent", "vector_of", 
				null, Tool.string_to_path_or_symbol("Biz.User_base") 
				));
			String expected_result = CASLParser.encode_conciseXML(expect_object, true);
		}

		//--------------------------------------------------------------------------
		// this section try to test multiple meta fields by equal sign, comment testing out for the future enhancement
		//[Test]
		public void n14_multiple_equal_sign_with_string_string()
		{
			String text = "<s> sid='none'='string' </s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject("_parent",c_CASL.c_sy_n_pa("s"), 
				"sid","none",
				"sid_f_type","string"
				);
			Assert.AreEqual(E, R);
		}		
		//[Test]
		public void n30_field_without_field_object()
		{
			String text = "<s>y='none'='string' x='none'='string'</s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject("_parent",c_CASL.c_sy_n_pa("s"), 
				"y", "none",
				"y_f_type","string",
				"x", "none",
				"x_f_type","string");
			Assert.AreEqual(E, R);
		}
		/// <summary>
		/// expected to failed for 3 eaual sign 
		/// handle it on equal event-> tks==kk || tks==uk
		/// </summary>
		//[Test]
		public void n16_three_equal_sign_with_object_symbol_int()
		{
			String text = "<s> y=<x/>=abc=3 </s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject("_parent",c_CASL.c_sy_n_pa("s"), 
				"y", new GenericObject("_parent",c_CASL.c_sy_n_pa("x")),
				"y_f_type",c_CASL.c_sy_n_pa("abc"),
				"y_f_0",3);
			Assert.AreEqual(E, R);
		}
		//[Test]
		public void n14_multiple_equal_sign_with_string_number()
		{
			String text = "<s> y='z'=3 sid='none'='string' </s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject("_parent",c_CASL.c_sy_n_pa("s"), 
				"y", "z", 
				"y_f_type", 3,
				"sid","none",
				"sid_f_type","string"
				);
			Assert.AreEqual(E, R);
		}
		//[Test]
		public void n15_multiple_equal_sign_with_object_symbol()
		{
			String text = "<s> y=<x>x=1</x>=abc </s>";
			GenericObject R = CASLParser.decode_conciseXML(text,"");
			GenericObject E = new GenericObject("_parent",c_CASL.c_sy_n_pa("s"), 
				"y", new GenericObject("_parent",c_CASL.c_sy_n_pa("x"),"x",1),
				"y_f_type",c_CASL.c_sy_n_pa("abc"));
			Assert.AreEqual(E, R);
		}
		//--------------------------------------------------------------------------
	}			
}