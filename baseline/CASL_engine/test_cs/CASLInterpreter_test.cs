using System;
using System.Windows.Forms;
using System.Collections;
using System.Data;
using System.Xml;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Soap;
using CASL_engine;


namespace T4_test
{
	using NUnit.Framework;

	[TestFixture]
	public class CASLInterpreter_test
	{
		[SetUp]
		public void Init()
		{
		}
		[Test]//<casl>foo=<method>_name='foo' x=required _impl=<do> true </do></method></casl>
		public void n01_primitive_expr_call()
		{
			String s_expr = "<foo> x=10 </foo>"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object obj_return = CASLInterpreter.execute_expr(expr,c_CASL.c_GEO());
			Assert.AreEqual(obj_return, true);
		}		
		//bar=<method>_name='bar' x=required _impl=<do> x </do></method>
		[Test]
		public void n01_symbol_expr_call()
		{
			String s_expr = "<bar> x=20 </bar>";
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,"");
			object obj_return = CASLInterpreter.execute_expr(expr,c_CASL.c_GEO());
			Assert.AreEqual(obj_return, 20);
		}		
		//tar=<method>_name='tar' x=required _impl=<do> <bar> x=40 </bar> </do></method>
		[Test]
		public void n02_call_expr_call()
		{
			String s_expr = "<tar> x=30 </tar>";
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,"");
			object obj_return = CASLInterpreter.execute_expr(expr,c_CASL.c_GEO());
			Assert.AreEqual(obj_return, 40);
		}		
		//				sar=<method>_name='sar' x=required _impl=<do> <bar> x=40</bar> x</do></method>
		[Test]
		public void n03_call_expr_call_2()
		{
			String s_expr = "<sar> x=30 </sar>";
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,"");
			object obj_return = CASLInterpreter.execute_expr(expr,c_CASL.c_GEO());
			Assert.AreEqual(obj_return, 30);
		}			
		//				kar=<method>_name='kar' x=required _impl=<do> <bar> x=<bar>x=50</bar></bar></do></method>
		//				bar=<method>_name='bar' x=required _impl=<do> x </do></method>
		[Test]
		public void n04_nested_call_expr_call()
		{
			String s_expr = "<kar> x=30 </kar>";
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,"");
			object obj_return = CASLInterpreter.execute_expr(expr,c_CASL.c_GEO());
			Assert.AreEqual(obj_return, 50);
		}				
		//				gar=<method>_name='gar' x=required _impl=<do> <set>y=1</set> </do></method>
		[Test]
		public void n05_single_set()
		{
			String s_exprs = "<do><set> x=30 </set> x</do>";
			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
			object obj_return = CASLInterpreter.execute_exprs(exprs,c_CASL.c_GEO());
			Assert.AreEqual(obj_return, 30);
		}		
		[Test]
		public void n05_multiple_set()
		{
			String s_exprs = "<do><set> x=10 x=30</set> x</do>";
			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
			object obj_return = CASLInterpreter.execute_exprs(exprs,c_CASL.c_GEO());
			Assert.AreEqual(obj_return, 30);
		}		
		[Test]
		public void n06_if_expr_call()
		{
			String s_exprs = "<do> <set> x=20</set> <if> true <set> x=10</set> </if> x</do>";
			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
			object obj_return = CASLInterpreter.execute_exprs(exprs,c_CASL.c_GEO());
			Assert.AreEqual(obj_return, 10);
		}
		[Test]
		public void n06_if_2_expr_call()
		{
			String s_exprs = "<do> <if> true <set> x=10</set> else <set> x=20 </set> </if> x</do>";
			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
			object obj_return = CASLInterpreter.execute_exprs(exprs,c_CASL.c_GEO());
			Assert.AreEqual(obj_return, 10);
		}
		[Test]
		public void n06_if_3_expr_call()
		{
			String s_exprs = "<do> <if> false <set> x=10</set> else <set> x=20 </set> </if> x</do>";
			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
			object obj_return = CASLInterpreter.execute_exprs(exprs,c_CASL.c_GEO());
			Assert.AreEqual(obj_return, 20);
		}
		[Test]
		public void n06_is_expr_call()
		{
			String s_exprs = "<do> <set> x=20</set> <is> x 20</is> </do>";
			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
			object obj_return = CASLInterpreter.execute_exprs(exprs,c_CASL.c_GEO());
			Assert.AreEqual(obj_return, true);
		}			
		[Test]
		public void n06_is_not_expr_call()
		{
			String s_exprs = "<do> <set> x=20</set> <is_not> x 20</is_not> </do>";
			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
			object obj_return = CASLInterpreter.execute_exprs(exprs,c_CASL.c_GEO());
			Assert.AreEqual(obj_return, false);
		}			
		[Test]
		public void n07_call_constructor_test()
		{
			//create a instance
			String s_exprs = 
				@"<do>
				<Student_Payment>
				id='333'
				sid='555'
				year=2002
				amount=20
				added_on='2002/01/01'
				</Student_Payment>
				it
				</do>";
			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
			object obj_return = CASLInterpreter.execute_exprs(exprs,c_CASL.c_GEO());
			string s_result = @"<Student_Payment>
				id='333'
				sid='555'
				year=2002
				amount=20
				added_on='2002/01/01'
				</Student_Payment>";
			GenericObject obj_result = CASLParser.decode_conciseXML(s_result,"");
			obj_result.set("_parent",c_CASL.Path(Tool.string_to_path_or_symbol("Student_Payment")) as GenericObject);
			object b_equal = obj_return.Equals(obj_result);
			Assert.AreEqual(obj_return, obj_result);
		}
		[Test]
		public void n08_field_look_up_test()
		{
			//create a instance
			//<cs.CASLInterpreter.CASL_handle_event> _subject=it event='post' </cs.CASLInterpreter.CASL_handle_event>
			String s_exprs = 
				@"<do>
				<Student_Payment>
				id='333'
				sid='555'
				year=2002
				amount=20
				added_on='2002/01/01'
				</Student_Payment>
				<Student_Payment.get_status> _subject=it </Student_Payment.get_status>
				</do>";
			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
			object obj_return = CASLInterpreter.execute_exprs(exprs,c_CASL.c_GEO());
			Assert.AreEqual(obj_return, "active");
		}	
		[Test]
		public void n20_execute_expr_2()
		{
			String s_method = 
				@"<do>
				GEO.<defmethod>
				_name='do_one_2'
				x=required
					<do_two_2></do_two_2>
					'bcd'
				</defmethod>
		</do>
				";
			object do_one_method = CASLParser.decode_conciseXML(s_method,"");
			CASLInterpreter.execute_exprs(do_one_method as GenericObject,c_CASL.c_GEO());
			s_method = 
				@"<do>
				GEO.<defmethod>
				_name='do_two_2'
					<prompt> prompt_obj=<method></method> question='why?' </prompt>
					'abc'
				</defmethod>
		</do>
				";
			object do_two_method = CASLParser.decode_conciseXML(s_method,"");
			do_two_method = CASLInterpreter.execute_exprs(do_two_method as GenericObject,c_CASL.c_GEO());
			Object call = CASLParser.decode_conciseXML(@"<do_one_2>x=10</do_one_2>","");
			//call = CASLInterpreter.execute_expr(call,c_CASL.c_GEO());
			Object R = CASLInterpreter.execute_expr(call as GenericObject,c_CASL.c_GEO());
			Assert.AreEqual("bcd",R);
		}
		//to test eval_args()
//		[Test]
//		public void n23_eval_args()
//		{
//			Object a_args = CASLParser.decode_conciseXML("<GEO> _parent=GEO y=x</GEO>","");
//			Object a_env = CASLParser.decode_conciseXML("<GEO>x=5</GEO>","");
//			Object R = CASLInterpreter.eval_args(a_args as GenericObject, a_env as GenericObject);
//			Object R = CASLInterpreter.eval_args(a_args as GenericObject, a_env as GenericObject);
//			Object E = CASLParser.decode_conciseXML("<GEO> _parent=GEO y=5</GEO>","");
//			(E as GenericObject).set("_parent", CASLParser.CASL);
//			Assert.AreEqual(E,R);
//		}
		[Test]
		public void n24_foreach()
		{
			Object a_exprs = CASLParser.decode_conciseXML("<do> <GEO> 'abc' 'bcd'</GEO> it.<for_each> include='vector_key' <H1> key value </H1> </for_each>  it.<to_htm/> </do>","");
			Object R = CASLInterpreter.execute_exprs(a_exprs as GenericObject,c_CASL.c_GEO());
			Assert.AreEqual("<H1>1bcd</H1>",R);
			a_exprs = CASLParser.decode_conciseXML(
@"<do>
<for_each> include='vector_key' returns='all' _subject=<GEO> 'abc' 'bcd'</GEO>
 <H1> key value </H1>
 </for_each> 
 it.<to_htm/>
</do>","");
			R = CASLInterpreter.execute_exprs(a_exprs as GenericObject,c_CASL.c_local_env());
			Assert.AreEqual("<H1>0abc</H1><H1>1bcd</H1>",R);
		}
		//totest: execute_path
		// my_payment.<get_status/> -> 'active'
		// Student_Payment.year -> '2004'
		// Student_Payment.H1._name -> 'H1'
		// Student_Payment.<H1.to_htm/> Student_Payment.<<do Student_Payment.H1.to_htm/>/>
		// === Student_Payment.<to_htm/>  if to_htm was only defined on geo
		[Test]
		public void n25_execute_path_with_call()
		{
			Object a_exprs = CASLParser.decode_conciseXML("<do> a_payment.<get_status/> </do>","");
			Object R = CASLInterpreter.execute_exprs((a_exprs as GenericObject),c_CASL.c_local_env());
			Assert.AreEqual("active",R);
		}
		//totest: execute_path
		[Test]
		public void n25_execute_path_without_call()
		{
			Object a_exprs = CASLParser.decode_conciseXML("<do>Student_Payment.year</do>","");
			Object R = CASLInterpreter.execute_path((a_exprs as GenericObject).get(0) as GenericObject,c_CASL.c_GEO());
			Assert.AreEqual("2004",R);
		}
		[Test]
		public void n25_execute_path_without_call_with_lookup()		{
			Object a_exprs = CASLParser.decode_conciseXML("<do>Student_Payment.H1._name</do>","");
			Object R = CASLInterpreter.execute_path((a_exprs as GenericObject).get(0) as GenericObject,c_CASL.c_GEO());
			Assert.AreEqual("H1",R);
		}
		[Test]
		public void n26_subject_call()
		{
			String s_exprs = 
				@"<do>
				<Student_Payment>
				id='333'
				sid='555'
				year=2002
				amount=20
				added_on='2002/01/01'
				</Student_Payment>
				it.<get_status/>
				</do>";
			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
			object obj_return = CASLInterpreter.execute_exprs(exprs,c_CASL.c_GEO());
			Assert.AreEqual(obj_return, "active");
		}	
		// test is_a
		// <H1/> is_a hypertext
		// <H1/> is GEO
		[Test]
		public void n27_is_a()
		{
			Object obj= CASLParser.CASL.get("hypertext");
			obj= CASLParser.CASL.get_w_lookup("H1");
			String s_exprs = 
				@"<H1>
				'content'
				</H1>
				";
			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
			object obj_return = CASLInterpreter.execute_expr(exprs,c_CASL.c_GEO());
			bool R = misc.CASL_is_a(new GenericObject("_subject",obj_return,"type",CASLParser.CASL.get("hypertext")));
			Assert.AreEqual(true, R);
			R = misc.CASL_is_a(new GenericObject("_subject",obj_return,"type",CASLParser.CASL));
			Assert.AreEqual(true, R);
			R = misc.CASL_is_a(new GenericObject("_subject",obj_return,"type",CASLParser.CASL.get("Student_Payment")));
			Assert.AreEqual(false, R);
		}	
		//test CASL_defclass()
		/// 1. GEO.<defclass> _name='sample_class' id=123</defclass>
		/// 2. GEO.<defclass> _name='sample_class' id=123 <defclass> _name='inner_class' id=123</defclass></defclass>

		[Test]
		public void n28_CASL_defclass_direct()
		{
			misc.CASL_defclass(new GenericObject("_outer_env", c_CASL.c_GEO(), "_subject",CASLParser.CASL,"_name","sample_class"));
		}

		[Test]
		public void n29_CASL_defclass_expr()
		{
			//String s_exprs =@"<do> GEO.<defclass> _name='sample_class' id=123</defclass></do>";
			String s_exprs =@"<do> GEO.<defclass> _name='sample_class' <field> key='id' value=123 </field></defclass></do>";
			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
			CASLInterpreter.execute_exprs(exprs,c_CASL.c_GEO());
			GenericObject R = CASLParser.CASL.get("sample_class") as GenericObject;
			GenericObject E = new GenericObject("_parent",CASLParser.CASL, "_container",CASLParser.CASL,"_name", "sample_class","id",123);
			Assert.AreEqual(E, R);
			s_exprs = 
				@"<do> GEO.<defclass> _name='sample_class' <field> key='id' value=123 </field> <defclass> _name='inner_class' id=123</defclass></defclass></do>
				";
			exprs = CASLParser.decode_conciseXML(s_exprs,"");
			CASLInterpreter.execute_exprs(exprs,c_CASL.c_GEO());
			R = CASLParser.CASL.get("sample_class") as GenericObject;
			E = new GenericObject("_parent",CASLParser.CASL, "_container",CASLParser.CASL,"_name", "sample_class","id",123, "inner_class", new GenericObject("_parent",CASLParser.CASL.get("sample_class"), "_container",CASLParser.CASL.get("sample_class"),"_name", "inner_class","id",123));
			Assert.AreEqual(E, R);
		}
		[Test]
		public void n36_CASL_defmethod_direct()
		{
			String s_exprs = 
				@"<do> 				GEO.<set> post_sample=<defmethod> 
						_precond = <defmethod> 
								           <is> .posted_on null </is> 
						    	       </defmethod>
								true
						</defmethod>
</set>
</do>
				";
			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
			CASLInterpreter.execute_exprs(exprs,c_CASL.c_GEO());
			GenericObject R = CASLParser.CASL.get("post_sample") as GenericObject;
			GenericObject E = new GenericObject("_parent",CASLParser.CASL.get("method"), "_precond",CASLParser.CASL.getAtPath("post_sample._precond"),0,true);
			Assert.AreEqual(E, R);
		}

		[Test]
		public void n31_CASL_defmethod_expr()
		{
			//String s_exprs = @"<do> GEO.<defmethod> _name='sample_method' id=123 true </defmethod></do>";
			String s_exprs = @"<do> GEO.<defmethod> _name='sample_method' <field> key='id' value=123 </field> true </defmethod></do>";
			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
			CASLInterpreter.execute_exprs(exprs,c_CASL.c_GEO());
			GenericObject R = CASLParser.CASL.get("sample_method") as GenericObject;
			GenericObject E = new GenericObject("_parent",CASLParser.CASL.get("method"), "_container",CASLParser.CASL,"_name", "sample_method","id",123,0,true);
			Assert.AreEqual(E, R);
			
			s_exprs = 
				@"<do> GEO.<defmethod> _name='if_sample' _do_not_execute_vector_args=true _impl=cs.CASLInterpreter.CASL_if</defmethod></do>
				";
			exprs = CASLParser.decode_conciseXML(s_exprs,"");
			CASLInterpreter.execute_exprs(exprs,c_CASL.c_GEO());
			R = CASLParser.CASL.get("if_sample") as GenericObject;
		}
		[Test]
		public void n32_CASL_defstatus()
		{
			String s_exprs = 
				@"<do> GEO.<defstatus> _name='sample_status' </defstatus></do>
				";
			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
			CASLInterpreter.execute_exprs(exprs,c_CASL.c_GEO());
			GenericObject R = CASLParser.CASL.get("sample_status") as GenericObject;
			GenericObject E = new GenericObject("_parent",CASLParser.CASL.get("status"), "_container",CASLParser.CASL,"_name", "sample_status");
			Assert.AreEqual(E, R);
		}
		[Test]
		public void n32_CASL_defstatus_expr()
		{
			string s_exprs = 
				@"<do> GEO.<defstatus> _name='sample_status' id=123 <defmethod> _name='inner_method' id=123</defmethod></defstatus></do>
				";
			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
			CASLInterpreter.execute_exprs(exprs,c_CASL.c_GEO());
			GenericObject R = CASLParser.CASL.get("sample_status") as GenericObject;
			GenericObject E = new GenericObject("_parent",CASLParser.CASL.get("status"), "_container",CASLParser.CASL,"_name", "sample_status","id",123, "inner_method", new GenericObject("_parent",CASLParser.CASL.get("method"), "_container",CASLParser.CASL.get("sample_status"),"_name", "inner_method","id",123));
			Assert.AreEqual(E, R);
		}
		//		[Test]
		//		public void n32_CASL_defstatus_expr_2()
		//		{
		//			string s_exprs = 
		//				@"<do> foo.<defstatus> 
		//		_name='active' 
		//		<defmethod> 
		//		_name='post'
		//		<defmethod> 
		//		_name='precond'
		//		_impl=<do> <is> .posted_on null </is> </do>
		//		</defmethod>
		//		<defmethod> 
		//		_name='implementation'
		//		_impl=
		//		<do>
		//		.<set> posted_on='2004/05/01' </set>
		//		_subject
		//			</do>
		//		</defmethod>
		//		ending_status='completed'
		//		</defmethod>
		//		</defstatus></do>
		//				";
		//			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
		//			CASLInterpreter.execute_exprs(exprs,c_CASL.c_GEO());
		//		}
		[Test]
		public void n32_CASL_defprocess()
		{
			String s_exprs = 
				@"<do> GEO.<defprocess> _name='sample_proces' true</defprocess></do>
				";
			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
			CASLInterpreter.execute_exprs(exprs,c_CASL.c_GEO());
			GenericObject R = CASLParser.CASL.get("sample_proces") as GenericObject;
			GenericObject E = new GenericObject("_parent",CASLParser.CASL.get("process"), "_container",CASLParser.CASL,"_name", "sample_proces",null,true);
			Assert.AreEqual(E, R);
		}
		[Test]
		public void n33_CASL_local()
		{
			String s_exprs = 
				@"<do> <set> x='shuang'</set> _local.x </do>
				";
			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
			GenericObject exprs_env = c_CASL.c_GEO();
			exprs_env.set("_local",exprs_env);
			Object R = CASLInterpreter.execute_exprs(exprs,exprs_env);
			Assert.AreEqual("shuang", R);
		}
		[Test]
		public void n34_CASL_inspect()
		{
			String s_exprs = 
				@"<do> <set> x='shuang' _name='my_name' 0=123 _impl=124</set> _local.<inspect/> </do>
				";
			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
			Object R = CASLInterpreter.execute_exprs(exprs,c_CASL.c_local_env());
		}
		[Test]
		public void n35_CASL_passed_all_args_by_local_in_casl_method()
		{
			String s_exprs = 
				@"<do> 
GEO.<defmethod> _name='xar' x=required _local.<inspect/> x</defmethod> 
<set> x=45 _name='my_name' 0=123 </set> 
<xar> _args=_local </xar> </do>
				";
			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
			Object R = CASLInterpreter.execute_exprs(exprs,c_CASL.c_local_env());
			Assert.AreEqual(45, R);
		}
		[Test]
		public void n35_CASL_passed_all_args_by_local_in_init()
		{
			String s_exprs = 
				@"<do>
<set> id='333'
				sid='555'
				year=2002
				amount=20
				added_on='2002/01/01'
</set> 
				<Student_Payment>
				_args=_local.<inspect/>
				</Student_Payment>
				<cs.CASL_engine.misc.CASL_handle_event> _subject=it event='post' </cs.CASL_engine.misc.CASL_handle_event>
				it.posted_on
				</do>";
		GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
		object obj_return = CASLInterpreter.execute_exprs(exprs,c_CASL.c_local_env());
		Assert.AreEqual(obj_return, "2004/05/01");
	}
		[Test]
		public void n37_do()
		{
			String s_expr = 
				@"<do><foo> x=10 </foo> <bar> x=20 </bar> </do>
				";
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,"");
			Object R = CASLInterpreter.execute_expr(expr,c_CASL.c_local_env());
			Assert.AreEqual(20, R);
		}
		[Test]
		public void n38_field_to_uri()
		{
			String s_expr = 
				@"<do> <field> container=Biz.Transaction key='Parking' </field>.<to_uri/>  </do>
				";
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,"");
			Object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_local_env());
			Assert.AreEqual("Biz/Transaction/Parking.htm", R);
		}
//		[Test]
//		public void n39_execute_cs_method_flex()
//		{
//			GenericObject g_env = c_CASL.c_GEO();
//			g_env.set("_subject",c_CASL.c_object("Biz.CoreEvent"));
//			Object R = CASLInterpreter.execute_cs_method(c_CASL.c_sy_n_pa("cs.CASL_engine.misc.CASL_to_path"),g_env);
//			Assert.AreEqual("Biz.CoreEvent", R);
//		}
//		[Test]
//		public void n40_execute_cs_method_fixed()
//		{
//			GenericObject g_env = c_CASL.c_GEO();
//			g_env.insert("a");
//			g_env.insert("b");
//			Object R = CASLInterpreter.execute_cs_method(c_CASL.c_sy_n_pa("cs.CASL_engine.misc.field_key"),g_env);
//			Assert.AreEqual("a_f_b", R);
//		}
//		[Test]
//		public void n33_set_parent()
//		{
//			String s_exprs = 
//				@"<do> GEO.<set_parent> 'ROOT'</set_parent> </do>
//				";
//			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
//			CASLInterpreter.execute_exprs(exprs,c_CASL.c_GEO());
//			Object R = CASLParser.CASL.get("_parent") ;
//			Assert.AreEqual("ROOT", R);
//		}
//		[Test]
//		public void n21_execute_cs_method()
//		{
//			Object R = CASLInterpreter.execute_cs_method(new GenericObject("_parent","cs.CASLInterpreter.to_htm","_subject",10));
//			Assert.AreEqual("10", R);
//		}

	}			
}