using System;
using System.Windows.Forms;
using System.Collections;
using System.Data;
using System.Xml;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Soap;
using CASL_engine;


namespace T4_test
{
	using NUnit.Framework;

	[TestFixture]
	public class speed_test
	{
		[SetUp]
		public void Init()
		{
		}
		[Test]
		public void n09_cs_native_primative_speed_test()
		{
			object obj_return = false;
			GenericObject go= new GenericObject();
			for(int i=0;i<1; i++) //real test for 10000000
			{
				obj_return = misc.CASL_speed_test(go);
			}
			Assert.AreEqual(obj_return, true);
		}
		[Test]
		public void n10_cs_primative_speed_test()
		{
			String s_exprs = 
				@"
				<GO>
				</GO>
				";
			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
			String s_method = 
				@"<method>
					_name='abc_casl'
					_impl=cs.misc.CASL_speed_test
				  </method>
				";
			GenericObject a_method = CASLParser.decode_conciseXML(s_method,"");

			object obj_return = false;
			for(int i=0;i<1; i++)// real test for 100000
			{
				//obj_return = CASLInterpreter.execute_casl_method(a_method,exprs);
			}
			Assert.AreEqual(obj_return, true);
		}
		[Test]
		public void n11_casl_primative_speed_test()
		{
			String s_exprs = 
				@"
				<GO>
				</GO>
				";
			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
			String s_method = 
				@"<method>
					_name='abc_casl'
					_impl=<do> true</do>
				  </method>
				";
			GenericObject a_method = CASLParser.decode_conciseXML(s_method,"");
			object obj_return = false;
			for(int i=0;i<1; i++)// real test for 100000
			{
				//obj_return = CASLInterpreter.execute_casl_method(a_method,exprs);
			}
			Assert.AreEqual(obj_return, true);
		}
		[Test]
		public void n12_cs_native_if_speed_test()
		{
			object obj_return = false;
			GenericObject go= new GenericObject();
			for(int i=0;i<1; i++)// real test for 10000000
			{
				obj_return = misc.CASL_native_if(go);
			}
			Assert.AreEqual(obj_return, 100);
		}
		[Test]
		public void n13_cs_if_speed_test()
		{
			String s_exprs = 
				@"
				<GO>
				</GO>
				";
			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
			String s_method = 
				@"<method>
					_name='if_casl'
					_impl=cs.misc.CASL_native_if	
				  </method>
				";
			GenericObject a_method = CASLParser.decode_conciseXML(s_method,"");
			object obj_return = false;
			for(int i=0;i<1; i++)// real test for 100000
			{
				//obj_return = CASLInterpreter.execute_casl_method(a_method,exprs);
				//obj_return = CASLInterpreter.execute_cs_method(exprs);
			}
			Assert.AreEqual(obj_return, 100);
		}
		[Test]
		public void n14_casl_if_speed_test()
		{
			String s_method = 
				@"<do> GEO.<defmethod>
					_name='if_casl'
						<if> false 
							10
							false
							20
							false
							30
							false
							40
							false
							50				
							false
							60
							false
							70
							false
							80
							false
							90
							else
							100
						</if>
				  </defmethod>
</do>
				";
			GenericObject a_method = CASLParser.decode_conciseXML(s_method,"");
			CASLInterpreter.execute_exprs(a_method,c_CASL.c_GEO());
			object obj_return = false;
			for(int i=0;i<1; i++)// real test for 100000
			{
				//obj_return = CASLInterpreter.execute_casl_method(CASLParser.CASL.get("if_casl") as GenericObject,c_CASL.c_GEO());
			}
			Assert.AreEqual(obj_return, 100);
		}
		[Test]
		public void n17_execute_process_1()
		{
			Assert.AreEqual(CASLInterpreter.execute_process(c_CASL.c_sy_n_pa("do_one"),null,null),"abc");
		}
		[Test]
		public void n18_continue_process_1()
		{
			String s_method = 
				@"
				<GEO>
				_name='do_one'
				_parent=process
				_container=GEO
				_impl=<do>
					<do_two></do_two>
					'bcd'
					</do>
				</GEO>
				";
			object do_one_method = CASLParser.decode_conciseXML(s_method,"");
			s_method = 
				@"
				<GEO>
				_name='do_two'
				_parent=process
				_container=GEO
				_impl=<do>
					<prompt> prompt_obj=<method></method> question='why?' </prompt>
					'abc'
					</do>
				</GEO>
				";
			object do_two_method = CASLParser.decode_conciseXML(s_method,"");
			CASLParser.CASL.set("do_one", do_one_method);
			CASLParser.CASL.set("do_two",do_two_method);
			Object R = CASLInterpreter.execute_process(do_one_method as GenericObject,null,null);
			R = CASLInterpreter.process_continue(new GenericObject("_subject",R));
			Assert.AreEqual("bcd",R);
			CASLParser.CASL.remove("do_one");
			CASLParser.CASL.remove("do_two");
		}
	}
}
