using System;
using System.Windows.Forms;
using System.Collections;
using System.Data;
using System.Xml;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Soap;
using CASL_engine;


namespace T4_test
{
	using NUnit.Framework;

	[TestFixture]
	public class Other_test
	{
		[SetUp]
		public void Init()
		{
		}
		/// convert uri to object 
		/// <GEO> call_name =required result_render_call_name=required _args=optional</GEO>
		[Test] 
		public void n01_uri_to_obj_test()
		{	
			string s_uri = "http://localhost/T4_server/foo.htm?x=10";
			GenericObject uri_obj = misc.uri_to_obj(s_uri);
			string result = misc.CASL_server(new GenericObject("uri",s_uri)).ToString();
		}
		[Test] 
		public void n02_uri_to_obj_test_2()
		{	
			string s_uri = "http://localhost/T4_server/bar.htm?x=20";
			string result = misc.CASL_server(new GenericObject("uri",s_uri)).ToString();

		}
		[Test] // without _subject arg
		public void n03_uri_to_obj_test_3()
		{	
			string s_uri = "http://localhost/T4_server/my/a_payment/post.htm?x=20";
			GenericObject R = misc.uri_to_obj(s_uri) ;
			//GenericObject R = c_CASL.c_GEO();
			//GenericObject args = new GenericObject("x", 20, "_subject", "my.a_payment");
			//R.set("call_name","my.a_payment.post","result_render_call_name","to_htm","_args",args);
			GenericObject E = CASLParser.decode_conciseXML("<GEO>call_name ='my.a_payment.post' result_render_call_name='to_htm' _args=<GEO>x='20' _subject='my.a_payment'</GEO></GEO>","");
			(E.get("_args") as GenericObject).remove("_parent");
			Assert.AreEqual(E,R);
		}
		[Test] // hypertext_to_htm
		public void n04_hypertext_to_htm()
		{
			GenericObject args = CASLParser.decode_conciseXML(@"<GEO> _subject=<H1>'HELLO' 'WORLD'</H1></GEO>","");
			String R = misc.CASL_hypertext_to_htm(args).ToString();
			String E = "<H1> HELLO WORLD</H1>";
			Assert.AreEqual(E,R);        
		}
		[Test] // hypertext_to_htm
		public void n05_to_class_type()
		{
			string name_space = "System";
			string class_name = "Object";
			String class_type_name = String.Format("{0}.{1}" , name_space, class_name);
			Type class_type = Type.GetType(class_type_name);
		}
//		[Test] // hypertext_to_htm
//		public void n06_search()
//		{
//			Search my_search = Search.Search_("abc") as Search;
//			Search my_search_2 = Search.Search_("abc_2")  as Search;
//			Console.Out.WriteLine(my_search.user_id+"  "+my_search_2.user_id);
//		}
		[Test] // hypertext_to_htm
		public void n07_temp()
		{ 
			string abc = System.Reflection.Assembly.GetExecutingAssembly().CodeBase ;
			abc = abc.Substring(0,abc.IndexOf("/bin/"));

			MessageBox.Show(abc);

//			DirectoryInfo dir = new DirectoryInfo(Directory.GetCurrentDirectory());
//			MessageBox.Show(dir.Parent.Parent.FullName);
		}
	}
}