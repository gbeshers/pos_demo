using System;
using System.Windows.Forms;
using System.Collections;
using System.Data;
using System.Xml;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Soap;
using CASL_engine;


namespace T4_test
{
	using NUnit.Framework;

	/// <summary>
	/// test cases include: 
	/// geo.<to_xml/>, hypertext.<to_htm/>, geo.<htm_large/>, 
	/// geo.<htm_medium/>, geo.<to_htm_top/>
	/// </summary>
	[TestFixture]
	public class formatter_test
	{
		[SetUp]
		public void Init()
		{
		}
		/// <summary>
		/// hypertext_to_htm() need to handle nested hypertext object 
		/// </summary>
		[Test]
		public void n01_nested_hypertext_to_htm()
		{
			String s_expr = "<do> <TABLE> <TR> 'Student_Payment'</TR> </TABLE> it.<to_htm/></do>"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_local_env());
			Console.Out.WriteLine(R);
			s_expr = "<do> <INPUT> type='button' value='finish_buy'</INPUT> it.<to_htm/> </do>"; //how to get path from call
			expr = CASLParser.decode_conciseXML(s_expr,""); 
			R = CASLInterpreter.execute_exprs(expr,c_CASL.c_local_env());
			Console.Out.WriteLine(R);
			
		}		
		/// <summary>
		/// hypertext_to_htm() need to handle multiply hypertext objects in vector
		/// </summary>
		[Test]
		public void n02_hypertexts_in_vector_to_htm()
		{
			object a_exprs = CASLParser.decode_conciseXML(
				@"<do>
<for_each> include='vector_key' returns='all' _subject=<GEO> 'abc' 'bcd'</GEO>
 <H1> key value </H1>
 </for_each> 
 it.<to_htm/>
</do>","");
			object R = CASLInterpreter.execute_exprs(a_exprs as GenericObject,c_CASL.c_local_env());
			Assert.AreEqual("<H1>0abc</H1><H1>1bcd</H1>",R);
			Console.Out.WriteLine(R);
		}		

		/// <summary>
		/// testing default GEO.<htm_small/>
		/// </summary>
		[Test]
		public void n03_default_htm_small()
		{
			String s_expr = "<do> Student_Payment.<htm_small/>.<to_htm/> </do>"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_local_env());
			Assert.AreEqual("Student_Payment", R);
			Console.Out.WriteLine(R);
		}	

		/// <summary>
		/// testing default GEO.<htm_medium/>
		/// </summary>
		[Test]
		public void n04_htm_medium()
		{
			String s_expr = "<do> Student_Payment.<htm_medium/>.<to_htm/> </do>"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_local_env());
			Console.Out.WriteLine(R);
		}		
	
		/// <summary>
		/// testing Student_Payment.<htm_large/>
		/// </summary>
		[Test]
		public void n05_htm_large()
		{
			String s_expr = "<do> Student_Payment.<htm_large/>.<to_htm/> </do>"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_local_env());
			Console.Out.WriteLine(R);
		}	

		/// <summary>
		/// testing GEO.<htm_large/>
		/// </summary>
	[Test]
		public void n06_cs_htm_large()
		{
			object hypertext = misc.CASL_htm_large(new GenericObject("_subject",CASLParser.CASL.get("Student_Payment")));
			object R = c_CASL.call("to_htm","_subject",hypertext);
		Console.Out.WriteLine(R);
		}			
		[Test]
		public void n07_to_htm_top()
		{
			String s_expr = "<do> Student_Payment.<to_htm_top/> </do>"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_local_env());
			Console.Out.WriteLine(R);
		}		
		[Test]
		public void n08_to_htm()
		{
			String s_expr = "<do> Student_Payment.<to_htm/> </do>"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_local_env());
			Console.Out.WriteLine(R);
		}		
		[Test]
		public void n09_opc_to_htm_top()
		{
			String s_expr = "<do> <!--my_app.<finish_buy/>--> my_app.<buying_htm_large/>.<to_htm/>  </do>"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_local_env());
			Console.Out.WriteLine(R);
		}		

		[Test]
		public void n10_opc_htm_wrap()
		{
			String s_expr = "<do> my_app.<htm_wrap/> it.<to_htm/> </do>"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_local_env());
			Console.Out.WriteLine(R);
		}	
		[Test]
		public void n12_my_app_a_dept_tender_types_htm_large()
		{
			String s_expr = "<do>   my_app.a_dept.tender_types.<htm_large/>.<to_htm/> </do>"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_local_env());
			Console.Out.WriteLine(R);
		}	
		[Test]
		public void n14_my_Biz_Tender_htm_rows()
		{
			String s_expr = "<do>   Biz.Tender.Check.<htm_rows_readonly/>.<to_htm/> </do>"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_local_env());
			Console.Out.WriteLine(R);
		}	

//		[Test]
//		public void n11_table_with_vector_to_htm()
//		{
//			String s_expr = 
//				@"<do> <TABLE>
//												<TR> <TD> colspan='4' 'transition types'</TD> </TR>
//												my_app.a_dept.transaction_types.<for_each> include='vector_key' returns='all'
//																value.<htm_row/>
//											</for_each> 
//												</TABLE>
//
//									it.<to_htm/>
//							</do>"; //how to get path from call
//			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
//			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_local_env());
//			Console.Out.WriteLine(R);
//		}

//		<SPAN>					
//		.a_dept.tender_types.<for_each>	include='vector_key' return='all'
//		<DIV> value.description </DIV>
//		</for_each></SPAN>
		[Test]
		public void n13_mix_hypertext_vector()
		{
			String s_expr = 
				@"<do> 					
					my_app.a_dept.tender_types.<for_each>	include='vector_key' return='all'
					<DIV> value.description </DIV>
					</for_each>
									it.<to_htm/>
							</do>"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_local_env());
			Console.Out.WriteLine(R);
		}
		//<IMG> src='../webfolder/logo.jpg'</IMG>
		[Test]
		public void n15_uri_formatting()
		{
			String s_expr = 
				@" 					
					<IMG> 'webfolderlogo.jpg'</IMG>
"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_local_env());
			Console.Out.WriteLine(R);
		}
		[Test]
		public void n15_coreEvent_completed_htm_large()
		{
			String s_expr = 
				@"<do> 	
							my_app.a_coreEvent.<post/>
							my_app.a_coreEvent.<htm_large/>	
							it.<to_htm/>
							</do>
"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_local_env());
			Console.Out.WriteLine(R);
		}
		[Test]
		public void n16_my_app_to_xml_top()
		{
			String s_expr = 
				@"<do> 	
							my_app.<to_xml/>
							</do>
"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_local_env());
			Console.Out.WriteLine(R);
		}
	}
}