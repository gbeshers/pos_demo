using System;
using System.Windows.Forms;
using System.Collections;
using System.Data;
using System.Xml;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Soap;
using CASL_engine;


namespace T4_test
{
	using NUnit.Framework;

	[TestFixture]
	public class Tool_test
	{
		[SetUp]
		public void Init()
		{
		}

		public void n01_symbol_or_path_to_string()
		{	
			String s_path = "<casl> path=cs.misc.CASL_to_htm </casl>";
			GenericObject a_path = CASLParser.decode_conciseXML(s_path,"") ;
			Object R = Tool.symbol_or_path_to_string(a_path.get("path") as GenericObject);
			string E = "cs.misc.CASL_to_htm";
			Assert.AreEqual(E, R);
		}
		[Test]
		public void n01_symbol_or_path_to_string_2()
		{	
			String s_path = "<casl> path=cs </casl>";
			GenericObject a_path = CASLParser.decode_conciseXML(s_path,"") ;
			Object R = Tool.symbol_or_path_to_string(a_path.get("path") as GenericObject);
			string E = "cs";
			Assert.AreEqual(E, R);
		}
		[Test]
		public void n02_string_to_path_or_symbol()
		{
			Object R = Tool.string_to_path_or_symbol("cs.CASLInterpreter.to_htm");
			String s_path = "<casl> path=cs.CASLInterpreter.to_htm </casl>";
			GenericObject a_path = CASLParser.decode_conciseXML(s_path,"") ;
			object E = a_path.get("path");
			Assert.AreEqual(E, R);
		}
		[Test] 
		public void n02_string_to_path_or_symbol_2()
		{
			Object R = Tool.string_to_path_or_symbol("cs");
			String s_symbol = "<casl> symbol=cs </casl>";
			GenericObject a_symbol = CASLParser.decode_conciseXML(s_symbol,"") ;
			object E = a_symbol.get("symbol");
			Assert.AreEqual(E, R);
		}
		[Test] 
		public void n03_is_symbol()
		{
			String s_symbol = "<casl> symbol=cs no_symbol='cs' </casl>";
			GenericObject a_symbol = CASLParser.decode_conciseXML(s_symbol,"").get("symbol") as GenericObject ;
			Object R = Tool.is_symbol(a_symbol);
			object E = true;
			Assert.AreEqual(E, R);
			R = Tool.is_symbol("cs");
			E = false;
			Assert.AreEqual(E, R);
		}
		[Test] 
		public void n04_is_path()
		{
			String s_container = "<casl> a_symbol=cs a_path=cs.path a_string='cs' </casl>";
			GenericObject a_container = CASLParser.decode_conciseXML(s_container,"") ;
			Object R = Tool.is_path(a_container.get("a_path"));
			object E = true;
			Assert.AreEqual(E, R);
			R = Tool.is_path(a_container.get("a_symbol"));
			E = false;
			Assert.AreEqual(E, R);
			R = Tool.is_path(a_container.get("a_string"));
			E = false;
			Assert.AreEqual(E, R);
		}
		//to_test:  is_call_for, is_process_call
		[Test] 
		public void n05_is_call_for()
		{
			String s_container = "<casl> a_call=<call_name></call_name> a_path=call_name.path a_string='call_name' </casl>";
			GenericObject a_container = CASLParser.decode_conciseXML(s_container,"") ;
			Object R = Tool.is_call_for(a_container.get("a_call"),"call_name");
			object E = true;
			Assert.AreEqual(E, R);
			R = Tool.is_call_for(a_container.get("a_symbol"),"cs");
			E = false;
			Assert.AreEqual(E, R);
			R = Tool.is_call_for(a_container.get("a_path"),"cs");
			E = false;
			Assert.AreEqual(E, R);
		}
		[Test] 
		public void n06_is_process_call()
		{
			String s_container = "<GEO> a_call='abc' a_process_call=<process></process> a_path=<method></method> a_string='cs' </GEO>";
			GenericObject a_container = CASLParser.decode_conciseXML(s_container,"") ;
			Object R = Tool.is_process_call(a_container.get("a_process_call"));
			object E = true;
			Assert.AreEqual(E, R);
			R = Tool.is_process_call(a_container.get("a_call"));
			E = false;
			Assert.AreEqual(E, R);
			R = Tool.is_process_call(a_container.get("a_path"));
			E = false;
			Assert.AreEqual(E, R);
		}
//		[Test]
//			/// true - <if></if> ;<is></is>; <is_not></is_not>
//			/// false - <fi></fi>; <do_if></do_if> ; <if.is_not></if.is_not>
//		public void n07_is_system_call()
//		{
//			String s_container = "<casl> a_if=<if></if> a_is=<is></is> a_fi=<fi></fi> a_if_is_not=<if.is_not></if.is_not> </casl>";
//			GenericObject a_container = CASLParser.decode_conciseXML(s_container,"") ;
//			Object R = Tool.is_system_call(a_container.get("a_if"));
//			object E = true;
//			Assert.AreEqual(E, R);
//			R = Tool.is_system_call(a_container.get("a_is"));
//			E = false;
//			Assert.AreEqual(E, R);
//			R = Tool.is_system_call(a_container.get("a_fi"));
//			E = false;
//			Assert.AreEqual(E, R);
//			R = Tool.is_system_call(a_container.get("a_if_is_not"));
//			E = false;
//			Assert.AreEqual(E, R);
//		}
		[Test]
		public void n08_is_symbol_for()
		{
			String s_container = "<casl> a_if=if a_is=is a_fi=<fi></fi> a_if_is_not=<if.is_not></if.is_not> </casl>";
			GenericObject a_container = CASLParser.decode_conciseXML(s_container,"") ;
			Object R = Tool.is_symbol_for(a_container.get("a_if"),"if");
			object E = true;
			Assert.AreEqual(E, R);
			R = Tool.is_symbol_for(a_container.get("a_is"),"if");
			E = false;
			Assert.AreEqual(E, R);
		}
		//TODO: is_constructor_call; is_method_call
		[Test]
		public void n09_is_constructor_call()
		{
			string s_method = 
				@"
				<GEO>
				_parent=GEO
				_name='A_Class'
				_container=GEO
				_class_name = 'A_Construct'
				</GEO>
				";
			object a_obj = CASLParser.decode_conciseXML(s_method,"");
			CASLParser.CASL.set("A_Class", a_obj);
			s_method = 
				@"
				<GEO>
				_parent=method
				_name = 'A_Method'
				_impl=<do>10</do>
				</GEO>
				";
			a_obj = CASLParser.decode_conciseXML(s_method,"");
			CASLParser.CASL.set("A_Method", a_obj);
			
			String s_expr_container = @"<casl> 
				a_constructor=<A_Class>x=10</A_Class> 
				not_constructor=<A_ClassX>x=10</A_ClassX> 
				a_method=<A_Method>x=10</A_Method>  
				a_if=<if></if> 
			</casl>";
			GenericObject a_expr_container = CASLParser.decode_conciseXML(s_expr_container,"") ;
			Object R = Tool.is_constructor_call(a_expr_container.get("a_constructor"));
			object E = true;
			Assert.AreEqual(E, R);
			R = Tool.is_constructor_call(a_expr_container.get("a_method"));
			E = false;
			Assert.AreEqual(E, R);
			R = Tool.is_constructor_call(a_expr_container.get("not_constructor"));
			E = false;
			Assert.AreEqual(E, R);
			R = Tool.is_constructor_call(a_expr_container.get("a_if"));
			E = false;
			Assert.AreEqual(E, R);
			CASLParser.CASL.remove("A_Class");
			CASLParser.CASL.remove("A_Method");
		}
		//TODO: is_constructor_call; is_method_call
		[Test]
		public void n10_is_exprs()
		{
			String s_expr_container = @"<casl> 
				exprs=<exprs>x=10</exprs> 
				not_exprs=<no_exprs>x=10</no_exprs> 
			</casl>";
			GenericObject a_expr_container = CASLParser.decode_conciseXML(s_expr_container,"") ;
			object R = Tool.is_exprs(a_expr_container.get("exprs"));
			object E = true;
			Assert.AreEqual(E, R);
			R = Tool.is_exprs(a_expr_container.get("not_exprs"));
			E = false;
			Assert.AreEqual(E, R);
		}
		// is_type_for
		[Test]
		public void n11_is_type_for()
		{
			String s_expr_container = @"<GEO> 
				prompt=<prompt>x=10</prompt> 
				not_exprs=<method>x=10</method> 
			</GEO>";
			GenericObject a_expr_container = CASLParser.decode_conciseXML(s_expr_container,"") ;
			a_expr_container = CASLInterpreter.execute_expr(a_expr_container, c_CASL.c_GEO()) as GenericObject;
			object R = Tool.is_type_for(a_expr_container.get("prompt"), "prompt");
			object E = true;
			Assert.AreEqual(E, R);
			R = Tool.is_type_for(a_expr_container.get("not_exprs"), "prompt");
			E = false;
			Assert.AreEqual(E, R);
		}
	}
}