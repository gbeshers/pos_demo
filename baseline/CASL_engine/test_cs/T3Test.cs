using System;
using System.Windows.Forms;
using System.Collections;
using System.Data;
using System.Xml;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Soap;
using CBT_Library;


namespace test
{
	using NUnit.Framework;

	[TestFixture]
	public class T3Test
	{
		[SetUp]
		public void Init()
		{
		}

		[Test]
		public void xml_parser_1()
		{
			//-------------------------------------------------------
			// test function
			String text = "<s> <x></x> = <m> </m></s>";
			GenericObject obj1 = CASLParser.decode_conciseXML(text,"");
			String result = CASLParser.encode_conciseXML(obj1, false);

			//-------------------------------------------------------
			// create expected result object
			GenericObject expected_obj = new GenericObject("_parent","s",  
				new GenericObject("_parent", "x"), 
				new GenericObject("_parent", "m"));
			String expected_result = CASLParser.encode_conciseXML(expected_obj, true);
			Assert.AreEqual(result, expected_result);
			
		}
		[Test]
		public void xml_parser_2()
		{
			//-------------------------------------------------------
			// test function
			String text = "<s><x></x><y></y></s>";
			GenericObject obj1 = CASLParser.decode_conciseXML(text,"");
			String result = CASLParser.encode_conciseXML(obj1, false);
			//-------------------------------------------------------
			// create expected result object
			GenericObject expected_obj = new GenericObject("_parent","s",  
				null, new GenericObject("_parent", "x"), 
				null, new GenericObject("_parent", "y"));
			String expected_result = CASLParser.encode_conciseXML(expected_obj, true);
			Assert.AreEqual(result, expected_result);
		}
		[Test]
		public void xml_parser_3()
		{
			//-------------------------------------------------------
			// test function
			String text = "<s> 56 = 7 </s>";
			GenericObject obj1 = CASLParser.decode_conciseXML(text,"");
			String result = CASLParser.encode_conciseXML(obj1, false);
			//-------------------------------------------------------
			// create expected result object
			GenericObject expected_obj = new GenericObject("_parent","s",  
				56, 7);
			String expected_result = CASLParser.encode_conciseXML(expected_obj, true);
			Assert.AreEqual(result, expected_result);
		}
		[Test]
		public void xml_parser_4()
		{
			String text = "<s><x><v></v></x></s>";
			GenericObject obj1 = CASLParser.decode_conciseXML(text,"");
			String result = CASLParser.encode_conciseXML(obj1, false);
			//-------------------------------------------------------
			// create expected result object
			GenericObject expected_obj = new GenericObject("_parent","s",  
				null, new GenericObject("_parent", "x", 
				null, new GenericObject("_parent", "v")));
			String expected_result = CASLParser.encode_conciseXML(expected_obj, true);
			Assert.AreEqual(result, expected_result);
		}
		[Test]
		public void xml_parser_5()
		{
			String text = "<s><x><y></y>=<z></z></x></s>";
			GenericObject obj1 = CASLParser.decode_conciseXML(text,"");
			String result = CASLParser.encode_conciseXML(obj1, false);
			//-------------------------------------------------------
			// create expected result object
			GenericObject expected_obj = new GenericObject("_parent","s",  
				null, new GenericObject("_parent", "x", 
				new GenericObject("_parent", "y"), new GenericObject("_parent", "z")));
			String expected_result = CASLParser.encode_conciseXML(expected_obj, true);
			Assert.AreEqual(result, expected_result);
		}
		[Test]
		public void xml_parser_6()
		{
			String text = "<s><x><y></y><m><n></n>=5</m></x></s>";
			GenericObject obj1 = CASLParser.decode_conciseXML(text,"");
			String result = CASLParser.encode_conciseXML(obj1, false);
			//-------------------------------------------------------
			// create expected result object
			GenericObject expected_obj = new GenericObject("_parent","s",  
				null, new GenericObject("_parent", "x", 
				null, new GenericObject("_parent", "y"), 
				null, new GenericObject("_parent", "m",
				new GenericObject("_parent", "n"), 5)));
			String expected_result = CASLParser.encode_conciseXML(expected_obj, true);
			Assert.AreEqual(result, expected_result);
		}
		[Test]
		public void xml_parser_7()
		{
			String text = "<s> 56=10.2 8=<x></x> <y></y>=<z><m></m>=<n></n> 10=8.8 </z></s>";
			GenericObject obj1 = CASLParser.decode_conciseXML(text,"");
			String result = CASLParser.encode_conciseXML(obj1, true);
			
			//-------------------------------------------------------
			// create expected result object
			GenericObject gobj = new GenericObject("_parent","s", 56,10.2m, 
				8, new GenericObject("_parent", "x"), 
				new GenericObject("_parent", "y"), 
				new GenericObject("_parent","z",
				new GenericObject("_parent","m"),new GenericObject("_parent","n"), 
				10, 8.8m));
			String expected_result = CASLParser.encode_conciseXML(gobj, true);
			Assert.AreEqual(result, expected_result);
		}
		[Test]
		public void xml_parser_8()
		{
			String text = "<s> foo=<field> key= 'foo' value=<x>stuff</x> bar=<y>5</y></field></s>";
			GenericObject obj1 = CASLParser.decode_conciseXML(text,"");
			String result = CASLParser.encode_conciseXML(obj1, false);
			//-------------------------------------------------------
			// create expected result object
			GenericObject gobj = new GenericObject("_parent","s", 
				"foo", new GenericObject("_parent", "x", 
				null, new GenericObject("_parent","symbol", "name","stuff")), 
				"foo_f_bar", new GenericObject("_parent", "y", null, 5));
			String expected_result = CASLParser.encode_conciseXML(gobj, true);
			Assert.AreEqual(result, expected_result);
		}
		[Test]
		public void xml_parser_9()
		{
			String result = "";
			try
			{
				for(int i=0; i<100; i++)
				{
					GenericObject obj1 = CASLParser.decode_conciseXML("",@"D:\DOTNETSPACE\docs\CASL_v2.xml") as GenericObject;
					// issue Note: C# doesn't support late binding, The developer need to cast object to its actually type all the time. or use reflection to call the method. For clarificaion, C# support polymophism with dynamic binding.
					GenericObject obj2 = ((GenericObject)((GenericObject)obj1.get("Biz")).get("Dept_base")).get_field("users");
					result = CASLParser.encode_conciseXML(obj1, false);
				}
			}
			catch (Exception e)
			{
				throw e;
			}
			//-------------------------------------------------------
			// create expected result object
			GenericObject expect_object = new GenericObject("_parent","field",  
				"key", "users", 
				"value", new GenericObject("_parent","symbol", "name", "[active_value]"), 
				"type", new GenericObject("_parent", "vector_of", 
				null, new GenericObject("_parent","path", 
				null, new GenericObject("_parent","symbol", "name", "Biz"),
				null, "User_base")));
			String expected_result = CASLParser.encode_conciseXML(expect_object, true);
//			Assert.AreEqual(result, expected_result);
		}
		[Test]
		public void xml_parser_10()
		{
			string text = "type<Type.t>0 = 1</Type.t>";
			GenericObject obj1 = CASLParser.decode_conciseXML(text,"");
			String result = CASLParser.encode_conciseXML(obj1, false);
			//-------------------------------------------------------
			// create expected result object
			GenericObject expect_object = new GenericObject("_parent", "Type.Vector_of", null, new GenericObject("_parent","path"));
			String expected_result = CASLParser.encode_conciseXML(expect_object, true);
//			Assert.AreEqual(result, expected_result);
		}
	}			
}