using System;
using System.Windows.Forms;
using System.Collections;
using System.Data;
using System.Xml;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Soap;
using CASL_engine;


namespace T4_test
{
	using NUnit.Framework;

	[TestFixture]
	public class casl_ide_test
	{
		[SetUp]
		public void Init()
		{
		}
		/// <summary>
		/// geo.<remove/>: remove last element of integer key.
		/// geo.<remove> key="a_key"</remove>: remove element with a key.
		/// </summary>
		[Test]
		public void n01_to_htm_top()
		{
			String s_expr = "<do> casl_ide.<htm_large/> it.<to_htm/> </do>"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			Object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_local_env());
			Console.Out.WriteLine(R);
		}		
	}
}