using System;
using System.Windows.Forms;
using System.Collections;
using System.Data;
using System.Xml;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Soap;
using CASL_engine;


namespace T4_test
{
	using NUnit.Framework;

	[TestFixture]
	public class c_CASL_test
	{
		[SetUp]
		public void Init()
		{
		}

		[Test]
		public void n01_c_sybmol()
		{	
			GenericObject a_container = CASLParser.decode_conciseXML("<GEO> abc=abc </GEO>","") ;
			Object R = c_CASL.c_symbol("abc");
			Object E = a_container.get("abc");
			Assert.AreEqual(E, R);
		}
		[Test]
		public void n02_c_path()
		{	
			GenericObject a_container = CASLParser.decode_conciseXML("<GEO> abc=cs.CASLParser </GEO>","") ;
			Object R = c_CASL.c_path("cs", "CASLParser");
			Object E = a_container.get("abc");
			Assert.AreEqual(E, R);
		}
		[Test]
		public void n03_call()
		{	
			GenericObject args = CASLParser.decode_conciseXML("<GEO> x=10 </GEO>","") ;
			Object R = c_CASL.call("foo", "x",10);
			Object E = true;
			Assert.AreEqual(E, R);
			R = c_CASL.call("Student_Payment.get_status", "_subject",CASLParser.CASL.getAtPath("session.my.a_payment"));
			E = "active";
			Assert.AreEqual(E, R);
		}
	}
}