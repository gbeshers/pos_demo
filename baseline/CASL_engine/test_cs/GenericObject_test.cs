using System;
using System.Windows.Forms;
using System.Collections;
using System.Data;
using System.Xml;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Soap;
using CASL_engine;


namespace T4_test
{
	using NUnit.Framework;

	[TestFixture]
	public class GenericObject_test
	{
		[SetUp]
		public void Init()
		{

		}

		[Test]
		public void n00_constructor()
		{
			GenericObject R = new GenericObject("_parent","user","it",null);
			GenericObject E = new GenericObject();
			E.set("_parent", "user");
			E.set("it",null);
			Assert.AreEqual(E, R);		
		}
		[Test]
		public void n01_set()
		{

			GenericObject R = new GenericObject();
			R.set(new GenericObject("x",124,"y","y_value","_parent", null, 0,"re").getObjectArr());
			GenericObject E = new GenericObject("x",124,"y","y_value");
			Assert.AreEqual(E, R);		
		}
		[Test]
		public void n02_equal_w_null()
		{
			GenericObject R = new GenericObject("_parent","user","it",null);
			GenericObject E = new GenericObject("_parent","user","it",null);
			Assert.AreEqual(E, R);		
		}
		[Test]
		public void n03_equal_with_complex_key()
		{
			GenericObject R = new GenericObject("_parent","user",new GenericObject("x",1),"it");
			GenericObject E = new GenericObject("_parent","user",new GenericObject("x",1),"it");
			Assert.AreEqual(E, R);		
		}
		[Test]
		public void n04_equal_with_nested_value()
		{
			GenericObject R = new GenericObject("_parent","user","it",new GenericObject("x",1));
			GenericObject E = new GenericObject("_parent","user","it",new GenericObject("x",1));
			Assert.AreEqual(E, R);		
		}
		[Test]
		public void n05_equal_casl_clone()
		{
			GenericObject R = CASLParser.CASL.clone();
			GenericObject E = CASLParser.CASL;
			R.Equals(E);
			Assert.AreEqual(E, R);		
		}
		[Test]
		public void n06_complex_key_out_test()
		{
			GenericObject G = new GenericObject();
			GenericObject O = new GenericObject("first","shuang", "chirdren",new GenericObject("last", "xiao"));
			GenericObject O2 = new GenericObject("first","shuang", "chirdren",new GenericObject("last", "xiao"));
			String a_value = "VALUE";
			G.set(O, a_value);
			bool b_equal= O.Equals(O2);
			Object in_value = G.get(O2);
			Object c_value = G.get(O);
			Assert.AreEqual(in_value, c_value);
		}	
		[Test]
		public void n07_getAtPath_method_test()
		{
			GenericObject O= new GenericObject("GenObj", new GenericObject("nested", "nested value"));
			String abc = O.getAtPath("GenObj.nested").ToString();
			Assert.AreEqual(abc, "nested value");
		}		
		[Test]
		public void n08_reverse_method_test()
		{
			GenericObject O= new GenericObject("_parent","abc", null,"a",null,"b",null,"c");
			O.reverse();
			GenericObject O2= new GenericObject("_parent","abc", null,"c",null,"b",null,"a");
			Assert.AreEqual(O, O2);
		}	
		[Test]
		public void n09_isError_method_test()
		{
			GenericObject O= new GenericObject("_parent","Error", "message","error_message");
			bool is_error = O.isError();
			Assert.AreEqual(is_error, true);
			GenericObject O2= new GenericObject("_parent","non_error", "message","error_message");
			is_error = O2.isError();
			Assert.AreEqual(is_error, false);
		}	

		[Test]
		public void n10_set_speed_test()
		{
			GenericObject O= new GenericObject();
			for(int i=0;i<1;i++)// real test 10000
			{
				O.set("f"+ i, i);
			}
		}	
		[Test]
		public void n11_getObjectArr_StringKeys_method_test()
		{
			GenericObject O= new GenericObject("_parent","Transaction", "id","real_estate","amount",10.50);
			GenericObject R = new GenericObject(O.getObjectArr_StringKeys());
			Object[] obj_ar = new Object[2];
			obj_ar[0]="id";
			obj_ar[1]="amount";
			GenericObject E = new GenericObject(obj_ar);
			Assert.AreEqual(E, R);
		}	


		// get_w_lookuop: lookup for class herirachy
		// true: 
		// <GEO>A_Class=<class>_parent=GEO _class_name='A_Class' a_lookup_field='a_lookup_field_value'</A_Class><GEO> 
		// a_instance = <A_Class>a_instance_field='a_instance_field_value'</A_Class>
		// a_instance.<get_w_lookup>"a_instance_field"</>; a_instance.get_w_lookup("a_lookup_field");
		// a_instance.<get_w_lookup><symbol> name="a_lookup_field"<symbol></>;
		// a_instance.<get_w_lookup>"A_Class"</>
		// false: 
		// a_instance.<get_w_lookup>"a_field"</>
		// TODO: test implicit my enviroment lookup
		// TODO: ask Mike about test cases.
		[Test]
		public void n12_get_w_lookup()
		{
			string a_xml_obj = 
				@"
				<A_Class>
				_parent=GEO
				_class_name = 'A_Class'
				a_lookup_field='a_lookup_field_value'
				</A_Class>
				";
			object a_obj = CASLParser.decode_conciseXML(a_xml_obj,"");
			CASLParser.CASL.set("A_Class", a_obj);
			string a_xml_instance = 
				@"
				<A_Class>
				a_instance_field='a_instance_field_value'
				</A_Class>
				";
			GenericObject a_instance = CASLParser.decode_conciseXML(a_xml_instance,"");
			Object R = a_instance.get_w_lookup("a_instance_field");
			Object E = "a_instance_field_value";
			Assert.AreEqual(E, R); // get field without lookup by string
			R = a_instance.get_w_lookup("a_lookup_field");
			E = "a_lookup_field_value";
			Assert.AreEqual(E, R); // get field with one_level lookup by string
			R = a_instance.get_w_lookup(Tool.string_to_path_or_symbol("a_lookup_field"));
			E = "a_lookup_field_value";
			Assert.AreEqual(E, R); // get field with one_level lookup  by symbol
			R = a_instance.get_w_lookup("A_Class");
			E = a_obj;
			Assert.AreEqual(E, R); // get field in root - GEO
			R = a_instance.get_w_lookup("a_field");
			E = null;
			Assert.AreEqual(E, R); // no field found
			CASLParser.CASL.remove("A_Class");
		}	
		// get field at path, if any path_part not found, lookup its _parent, 
		// TODO: if GEO is object to lookup, look my first
		// TODO: security checking (only lookup the last one?)
		// get_at_path_w_lookup: lookup for class herirachy, test parameters
		// true: 
		// <GEO>A_Class=<class>_parent=GEO _class_name='A_Class' B_Class=<class> _parent='A_Class' _class_name='B_Class' B_Class_field='B_Class_field_value' </class> A_Class_field='A_Class_field_value'</class><GEO> 
		// A_Class_instance = <A_Class>A_Class_instance_field='A_Class_instance_field_value'</A_Class>
		// A_Class_instance.<get_at_path_w_lookup>"B_Class.B_Class_field"</> ->  'B_Class_field_value'
		// A_Class_instance.<get_at_path_w_lookup><path> B_Class "B_Class_field"</></path> ->  'B_Class_field_value'
		// A_Class_instance.<get_at_path_w_lookup>"A_Class.B_Class.B_Class_field"</> -> 'B_Class_field_value'
		// false: 
		// A_Class_instance.<get_at_path_w_lookup>"B_Class_field"</> -> null;
		[Test]
		public void n13_get_at_path_w_lookup()
		{
			string a_xml_obj = 
				@"
				<class>
				_parent=GEO
				_class_name = 'A_Class'
				B_Class=
					<class> _parent='A_Class'
						_class_name='B_Class' 
						B_Class_field='B_Class_field_value' 
					</class> 
				A_Class_field='A_Class_field_value'
				</class>
				";
			object a_obj = CASLParser.decode_conciseXML(a_xml_obj,"");
			CASLParser.CASL.set("A_Class", a_obj);
			string a_xml_instance = 
				@"
				<A_Class>
				A_Class_instance_field='A_Class_instance_field_value'
				</A_Class>
				";
			GenericObject A_Class_instance = CASLParser.decode_conciseXML(a_xml_instance,"");
			Object R = A_Class_instance.get_at_path_w_lookup("B_Class.B_Class_field");
			Object E = "B_Class_field_value";
			Assert.AreEqual(E, R); // get field without lookup by string
			R = A_Class_instance.get_at_path_w_lookup(Tool.string_to_path_or_symbol("B_Class.B_Class_field"));
			E = "B_Class_field_value";
			Assert.AreEqual(E, R); // get field with one_level lookup by string
			R = A_Class_instance.get_at_path_w_lookup("A_Class.B_Class.B_Class_field");
			E = "B_Class_field_value";
			Assert.AreEqual(E, R); // get field with one_level lookup  by symbol
			R = A_Class_instance.get_at_path_w_lookup("B_Class_field");
			E = null;
			Assert.AreEqual(E, R); // no field found		}
			CASLParser.CASL.remove("A_Class");
		}
		[Test]
		public void n14_remove()
		{
			GenericObject E = new GenericObject("_parent","abc","key_1","value_1");
			E.remove("_parent");
			GenericObject R = new GenericObject("key_1","value_1");
			Assert.AreEqual(E, R); 
		}
		//object E = CASLParser.decode_conciseXML("<GEO>call_name ='my.a_payment.post' result_render_call_name='to_htm' _args=<GEO>x=20 _subject='my.a_payment'</GEO></GEO>","");
		[Test]
		public void n15_remove_2()
		{
			GenericObject R = CASLParser.decode_conciseXML("<GEO>call_name ='my.a_payment.post' result_render_call_name='to_htm' _args=<GEO>x=20 _subject='my.a_payment'</GEO></GEO>","");
			(R.get("_args") as GenericObject).remove("_parent");
			GenericObject E = c_CASL.c_GEO();
			GenericObject args = new GenericObject("x", 20, "_subject", "my.a_payment");
			E.set("call_name","my.a_payment.post","result_render_call_name","to_htm","_args",args);
			Assert.AreEqual(E, R); 
		}
		//TODO: is_contained is_contained_in
		[Test]
		public void n16_is_contained()
		{
			GenericObject container = CASLParser.decode_conciseXML(
				@"<class> _parent=GEO _name='biz' 
					trans=<class> _parent=biz _name='trans' _container=biz id=123</class>
					trans2=<GEO> _name='trans' id=123</GEO>
				 </class>","");
			

			(container.get("trans") as GenericObject).set("_parent", container);
			(container.get("trans") as GenericObject).set("_container", container);
			bool R = (container.get("trans") as GenericObject).is_contained();
			bool E = true;
			Assert.AreEqual(E, R); 
			R = (container.get("trans2") as GenericObject).is_contained();
			E = false;
			Assert.AreEqual(E, R); 
		}

		/// <summary>
		/// to_xml format object without circular reference("circular_ref") & casl defined reference (to_string)
		/// </summary>
		[Test]
		public void n17_to_xml()
		{
			String s_expr = 
				@"<do> <Student_Payment> id='333' sid='555' year=2002 amount=20 added_on='2002/01/01'</Student_Payment></do>";
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,"");
			object obj_return = CASLInterpreter.execute_exprs(expr,c_CASL.c_GEO());
			string R = (obj_return as GenericObject).to_xml();
			Console.Out.WriteLine(R);
			R = (CASLParser.CASL.getAtPath("Student_Payment.get_status") as GenericObject).to_xml();
			Console.Out.WriteLine(R);
			R = CASLParser.CASL.to_xml(); //TODO: solve overstack error
			Console.Out.WriteLine(R);
			R = (CASLParser.CASL.getAtPath("bar") as GenericObject).to_xml();
			Console.Out.WriteLine(R);
			R = (CASLParser.CASL.getAtPath("session.my.my_app") as GenericObject).to_xml();
			Console.Out.WriteLine(R);

		}

		/// <summary>
		/// to_path: 
		/// </summary>
		[Test]
		public void n18_to_path()
		{
			String s_expr = 
				@"<Student_Payment>
				id='333'
				sid='555'
				year=2002
				amount=20
				added_on='2002/01/01'
				</Student_Payment>";
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,"");
			object obj_return = CASLInterpreter.execute_expr(expr,c_CASL.c_GEO());
			object R = (obj_return as GenericObject).to_path();
			Assert.AreEqual(null, R); 

			R = CASLParser.CASL.to_path();
			Assert.AreEqual("", R); 
			R = (CASLParser.CASL.getAtPath("Student_Payment") as GenericObject).to_path();
			Assert.AreEqual("Student_Payment", R); 
			R = (CASLParser.CASL.getAtPath("Student_Payment.active") as GenericObject).to_path();
			Assert.AreEqual("Student_Payment.active", R); 
		}
	}
}