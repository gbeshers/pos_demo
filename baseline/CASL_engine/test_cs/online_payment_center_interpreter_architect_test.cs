using System;
using System.Windows.Forms;
using System.Collections;
using System.Data;
using System.Xml;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Soap;
using CASL_engine;

namespace T4_test
{
	using NUnit.Framework;

	[TestFixture]
	public class online_payment_center_test
	{
		[SetUp]
		public void Init()
		{
		}
		[Test]
		public void n01_transaction_post_test()
		{
			//create a instance
			String s_exprs = 
				@"<do>
				<Student_Payment>
				id='333'
				sid='555'
				year=2002
				amount=20
				added_on='2002/01/01'
				</Student_Payment>
				it
				</do>";
			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
			object obj_return = CASLInterpreter.execute_exprs(exprs,new GenericObject("_parent",Tool.string_to_path_or_symbol("GEO")));
			string s_result = @"<Student_Payment>
				id='333'
				sid='555'
				year=2002
				amount=20
				added_on='2002/01/01'
				</Student_Payment>";
			GenericObject obj_result = CASLParser.decode_conciseXML(s_result,"");			
			obj_result.set("_parent",c_CASL.Path(Tool.string_to_path_or_symbol("Student_Payment")) as GenericObject);
			Assert.AreEqual(obj_return, obj_result);
		}		
		[Test]
		public void n02_transaction_get_status_test()
		{
			String s_exprs = 
				@"<do>
				<Student_Payment>
				id='333'
				sid='555'
				year=2002
				amount=20
				added_on='2002/01/01'
				</Student_Payment>
				<Student_Payment.get_status> _subject=it </Student_Payment.get_status>
				</do>";
			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
			object obj_return = CASLInterpreter.execute_exprs(exprs,c_CASL.c_GEO());
			Assert.AreEqual(obj_return, "active");
		}	

		[Test]
		public void n03_transaction_handle_event_test()
		{
			String s_exprs = 
				@"<do>
				<Student_Payment>
				id='333'
				sid='555'
				year=2002
				amount=20
				added_on='2002/01/01'
				</Student_Payment>
				<cs.misc.CASL_handle_event> _subject=it event='post' </cs.misc.CASL_handle_event>
				it.posted_on
				<!--<casl.Student_Payment.get_status> _subject=it </casl.Student_Payment.get_status>-->
				</do>";
			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
			object obj_return = CASLInterpreter.execute_exprs(exprs,new GenericObject("_parent",Tool.string_to_path_or_symbol("GEO")));
			Assert.AreEqual(obj_return, "2004/05/01");
		}	
		[Test]
		public void n04_transaction_handle_event_test()
		{
			String s_exprs = 
				@"<do>
				<Student_Payment>
				id='333'
				sid='555'
				year=2002
				amount=20
				added_on='2002/01/01'
				</Student_Payment>
				<cs.misc.CASL_handle_event> _subject=it event='post' </cs.misc.CASL_handle_event>
				<Student_Payment.get_status> _subject=it </Student_Payment.get_status>
				</do>";
			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
			object obj_return = CASLInterpreter.execute_exprs(exprs,c_CASL.c_GEO());
			Assert.AreEqual(obj_return, "completed");
		}	
		[Test]
		public void n05_transaction_post_test()
		{
			String s_exprs = 
				@"<do>
				<Student_Payment>
				id='333'
				sid='555'
				year=2002
				amount=20
				added_on='2002/01/01'
				</Student_Payment>
				it.<handle_event> event='post' </handle_event>
				it.posted_on
				</do>";
			GenericObject exprs = CASLParser.decode_conciseXML(s_exprs,"");
			object obj_return = CASLInterpreter.execute_exprs(exprs,c_CASL.c_GEO());
			Assert.AreEqual(obj_return, "2004/05/01");
		}	
		[Test]
		public void n06_opc_a_coreEvent_htm_large_readonly()
		{
			String s_expr = "<do> my_app.a_coreEvent.<post/> my_app.a_coreEvent.<htm_large_readonly/>.<to_htm/> </do>"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_local_env());
			Console.Out.WriteLine(R);
		}		 
		[Test]
		public void n07_opc_htm_large_readonly()
		{
			String s_expr = "<do> my_app.<finish_buy/> my_app.<pay/> my_app.<htm_large_readonly/>.<to_htm/> </do>"; //how to get path from call
			//String s_expr = "<do> my_app.<get_status/> </do>"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_local_env());
			Console.Out.WriteLine(R);
		}		 

		[Test]
		public void n08_opc_a_coreEvent_remove_transaction()
		{
			String s_expr = "<do> my_app.a_coreEvent.<remove_transaction/>.<to_htm_top/> <!--my_app.a_coreEvent.<htm_large/>.<to_htm/>--> </do>"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_local_env());
			Console.Out.WriteLine(R);
		}		 
		[Test]
		public void n09_opc_add_transaction()
		{
			String s_expr = @"<do>	my_app.<add_transaction> ttid='Excise' amount=9 qty=2 </add_transaction> my_app.a_coreEvent.<inspect/>  </do>"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_local_env());
			Console.Out.WriteLine(R);
		}		 
		[Test]
		public void n10_opc_add_transaction_htm_row()
		{
			String s_expr = @"<do>	my_app.add_transaction.<htm_row> form_subject=my_app form_data=Biz.Transaction.Parking </htm_row>.<to_htm/>  </do>"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_local_env());
			Console.Out.WriteLine(R);
		}		 
		[Test]
		public void n10_opc_add_transaction_htm_button_js()
		{
			String s_expr = @"<do>	my_app.add_transaction.<htm_button_js> form_subject=my_app form_data=Biz.Transaction.Parking </htm_button_js>.<to_htm/> my_app.<to_path/> </do>"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_local_env());
			Console.Out.WriteLine(R);
		}		 

	}
}