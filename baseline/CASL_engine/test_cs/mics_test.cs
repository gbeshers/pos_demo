using System;
using System.Windows.Forms;
using System.Collections;
using System.Data;
using System.Xml;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Soap;
using CASL_engine;


namespace T4_test
{
	using NUnit.Framework;

	[TestFixture]
	public class mics_test
	{
		[SetUp]
		public void Init()
		{
		}
		/// <summary>
		/// geo.<remove/>: remove last element of integer key.
		/// geo.<remove> key="a_key"</remove>: remove element with a key.
		/// </summary>
		[Test]
		public void n01_remove()
		{
			String s_expr = "<do> <GEO> x=10 10 </GEO> <set> a=it</set> a.<remove/> a</do>"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_GEO());
			GenericObject E = c_CASL.c_instance("GEO");
			E.set("x",10);
			Assert.AreEqual(E, R);
		}		
		[Test]
		public void n02_remove_with_key()
		{
			String s_expr = "<do> <GEO> x=10 10 </GEO> <set> a=it</set> a.<remove> key='x' </remove> a</do>"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_GEO());
			GenericObject E = c_CASL.c_instance("GEO");
			E.insert(10);
			Assert.AreEqual(E, R);
		}		
		/// <summary>
		/// geo.<insert> "a_value "</insert>/>: insert a element
		/// </summary>
		[Test]
		public void n03_insert()
		{
			String s_expr = "<do> <GEO> x=10 10 </GEO> <set> a=it</set> a.<insert> 20 </insert> a</do>"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_GEO());
			GenericObject E = c_CASL.c_instance("GEO");
			E.set("x",10);
			E.insert(10);
			E.insert(20);
			Assert.AreEqual(E, R);
		}		

		[Test]
		public void n04_get()
		{
			String s_expr = "<do> Biz.Transaction.<get> key='Parking' </get> </do>"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_GEO());
			Object E = c_CASL.c_object("Biz.Transaction.Parking");
			Assert.AreEqual(E, R);
		}		
		[Test]
		public void n05_to_path()
		{
			String s_expr = "<do> my_app.<to_path/> </do>"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_GEO());
			Object E = "my_app";
			Assert.AreEqual(E, R);
		}		
		[Test]
		public void n06_to_uri_for_class()
		{
			String s_expr = "<do> Biz.Transaction.Parking.<to_uri/> </do>"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_GEO());
			Object E = "Biz/Transaction/Parking.htm";
			Assert.AreEqual(E, R);
		}		
		[Test]
		public void n07_to_uri_for_instance()
		{
			String s_expr = "<do> my_app.<to_uri/> </do>"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_GEO());
			Object E = "my_app.htm";
			Assert.AreEqual(E, R);
		}
		// CASL_to_query_string
		// CASL_server
		/*
		[Test]
		public void n07_to_query_string()
		{
			String s_expr = "<do> a_payment.<to_query_string/> </do>"; //how to get path from call
			GenericObject expr = CASLParser.decode_conciseXML(s_expr,""); 
			object R = CASLInterpreter.execute_exprs(expr,c_CASL.c_GEO());
			Object E = "?id=333&sid=555&year=2002&amount=15&added_on=2002/01/01";
			Assert.AreEqual(E, R);
			Console.Out.WriteLine(R);
		}
*/
	}
}