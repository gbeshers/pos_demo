<?xml version="1.0" encoding="utf-8" ?> 
<do>
<!-- one_of -->
<test>
 <one_of> "a" "b" "c" </one_of>.<is_type_for> "a" </is_type_for>
 true
</test>

<test> name="one_of with OPTION"
<one_of>
 "US"
 <OPTION> value="CA" "Canada" </OPTION>
</one_of>.<is_type_for> "CA" </is_type_for>
true
</test>

<test>
 <one_of> "a" "b" "c" </one_of>.<is_type_for> "z" </is_type_for>
 false
</test>

<!-- end one_of -->

<!-- vector_of -->
<test>
 <vector_of> String </vector_of>.<is_type_for> <vector> "aa" "bbb" </vector> </is_type_for>
 true
</test>

<test>
 <vector_of> String </vector_of>.<is_type_for> <vector> "cc" 5 </vector> </is_type_for>
 false
</test>

<test>
 <vector_of> String  min_length=2 </vector_of>.
   <is_type_for> <vector> "cc" "dd" </vector> </is_type_for>
 true
</test>

<test>
 <try>
 <vector_of> String  min_length=2 </vector_of>.
   <is_type_for> <vector> "cc" </vector> </is_type_for>
 <if_error> the_error.message </if_error>
 </try>
 "The minimium length is 2 and length is 1"
</test>

<test>
 <vector_of> String  max_length=2 </vector_of>.
   <is_type_for> <vector> "cc" "dd" </vector> </is_type_for>
 true
</test>

<test>
 <try>
 <vector_of> String  max_length=2 </vector_of>.
   <is_type_for> <vector> "cc""df" "fds"</vector> </is_type_for>
 <if_error> the_error.message </if_error>
 </try>
 "The maximum length is 2 and length is 3"
</test>

<test>
 <vector_of> String Number </vector_of>.
   <is_type_for> <vector> "cc" 5 2 3 </vector> </is_type_for>
 true
</test>

<test>
 <vector_of> String Number </vector_of>.
   <is_type_for> <vector> "cc" 5 2 "ss" </vector> </is_type_for>
 false
</test>


<!-- end vector_of -->

<!-- Decimal -->
<test> name="Decimal.is_type_for with decimal"
Decimal.<is_type_for> 3.3 </is_type_for>
true
</test>

<test> name="Decimal.is_type_for with integer"
Decimal.<is_type_for> 3 </is_type_for>
false
</test>

<!-- Type.text -->
<test> name="type.text"
 <text> 3 10 </text>.<is_type_for> "asdfas" </is_type_for>
 true
</test>

<test> name="type.text"
 <text> 3 10 </text>.<is_type_for> "as" </is_type_for>
 false
</test>

<test> name="type.numeric"
 <numeric> 3 </numeric>.
    <is_type_for> "332" </is_type_for>
 true
</test>

<test> name="type.numeric"
 <numeric> 2 </numeric>.
    <is_type_for> "332" </is_type_for>
 false
</test>

<test> name="type.numeric"
 <numeric> 5 </numeric>.
    <is_type_for> "332" </is_type_for>
 true
</test>

<test> name="type.alphanumeric"
 <alphanumeric> 5 </alphanumeric>.
    <is_type_for> "aa33" </is_type_for>
 true
</test>

<test> name="type.alphanumeric"
 <alphanumeric> 5 </alphanumeric>.
    <is_type_for> "ad332" </is_type_for>
 true
</test>

<test> name="type.alphanumeric"
 <alphanumeric> 5 </alphanumeric>.
    <is_type_for> "2dfa223" </is_type_for>
 false
</test>

<test> name="type.alphanumeric"
 <alphanumeric> 5 </alphanumeric>.
    <is_type_for> "aa33" </is_type_for>
 true
</test>

<test> name="type.alphanumeric"
 <alphanumeric> 5 </alphanumeric>.
    <is_type_for> "ad332" </is_type_for>
 true
</test>

<test> name="type.alpha"
 <alpha> 4 </alpha>.
    <is_type_for> "stuff" </is_type_for>
 false
</test>

<test> name="type.alpha"
 <alpha> 4 </alpha>.
    <is_type_for> "stuf" </is_type_for>
 true
</test>

<test> name="type.zipcode"
 Type.zipcode.<is_type_for> "12345" </is_type_for>
 true
</test>

<test> name="type.zipcode"
 Type.zipcode.<is_type_for> "123" </is_type_for>
 false
</test>

<test> name="type.zipcode"
 Type.zipcode.<is_type_for> "1234567" </is_type_for>
 false
</test>

<test> name="type.zipcode"
 Type.zipcode.<is_type_for> "123456789" </is_type_for>
 true
</test>

<test> name="type.zipcode_5"
 Type.zipcode_5.<is_type_for> "12345" </is_type_for>
 true
</test>

<test> name="type.zipcode_5"
 Type.zipcode_5.<is_type_for> "1234" </is_type_for>
 false
</test>

<test> name="type.zipcode_5"
 Type.zipcode_5.<is_type_for> "123456789" </is_type_for>
 false
</test>

<test> name="type.zipcode_9"
 Type.zipcode_9.<is_type_for> "12345" </is_type_for>
 false
</test>

<test> name="type.zipcode_9"
 Type.zipcode_9.<is_type_for> "1234" </is_type_for>
 false
</test>

<test> name="type.zipcode_9"
 Type.zipcode_9.<is_type_for> "123456789" </is_type_for>
 true
</test>

<test> name="type.phone_number"
 Type.Type.phone_number.<is_type_for> "1234" </is_type_for>
 false
</test>

<test> name="type.phone_number"
 Type.phone_number.<is_type_for> "3334441111" </is_type_for>
 true
</test>

<test> name="Type.range_of.max"
 Type.<range_of> max=10 </range_of>.<is_type_for> 5 </is_type_for>
 true
</test>

<test> name="Type.range_of.max"
 Type.<range_of> max=10 </range_of>.<is_type_for> 10 </is_type_for>
 false
</test>

<test> name="Type.range_of.include_max"
 Type.<range_of> max=10 include_max=true </range_of>.<is_type_for> 10 </is_type_for>
 true
</test>

<test> name="Type.range_of.min"
 Type.<range_of> min=5 </range_of>.<is_type_for> 5 </is_type_for>
 true
</test>

<test> name="Type.range_of.min"
 Type.<range_of> min=5 </range_of>.<is_type_for> 3 </is_type_for>
 false
</test>

<test> name="Type.range_of.max negative"
 Type.<range_of> max=5 </range_of>.<is_type_for> -3 </is_type_for>
 false
</test>

<test> name="Type.range_of.max"
 Type.<range_of> max=5 </range_of>.<is_type_for> 0.0 </is_type_for>
 true
</test>

<test> name="Type.range_of.max"
 Type.<range_of> max=5 </range_of>.<is_type_for> 0 </is_type_for>
 true
</test>

<test> name="less_or_equal"
 5.0.<less_or_equal> 5 </less_or_equal>
 false
</test>

<test> name="less_or_equal"
 5.0.<less_or_equal> 5 </less_or_equal>
 false
</test>

<test> name="less_or_equal"
 5.0.<less_or_equal> 5.0 </less_or_equal>
 false
</test>

<test>  name="is_a"
 5.9.<is_a> Integer </is_a>
 false
</test>

<test> name="is_a"
 5.9.<is_a> Decimal </is_a>
 true
</test>


<test> name="Type.one_of"
 Type.<one_of> "a" "b" </one_of>.<is_type_for> "a" </is_type_for>
 true
</test>

<test> name="Type.one_of"
 Type.<one_of> "a" "b" </one_of>.<is_type_for> "c" </is_type_for>
 false
</test>

<test> name="Type.one_of non-primitive"
 Type.<one_of> <GEO> x=10 </GEO> <GEO> x=20 </GEO> </one_of>.<is_type_for> <GEO> x=20 </GEO> </is_type_for>
 true
</test>

<test> name="Type.one_of non-primitive"
 Type.<one_of> <GEO> x=10 </GEO> <GEO> x=20 </GEO> </one_of>.<is_type_for> 6 </is_type_for>
 false
</test>

<test> name="Type.one_of non-primitive"
 Type.<one_of> 1 2 6 7 8 9 11 14 15 16 18 32 34 39 50  </one_of>.<is_type_for> 6 </is_type_for>
 true
</test>

<test> name="Type.one_of types"
 Type.<one_of> Integer String  </one_of>.<is_type_for> Integer </is_type_for>
 true
</test>

<test> name="Type.one_of types"
 Type.<one_of> Integer String  </one_of>.<is_type_for> 6 </is_type_for>
 false
</test>




<test> name="Type.one_of_type"
 Type.<one_of_type> Integer String </one_of_type>.<is_type_for> "a" </is_type_for>
 true
</test>

<test> name="Type.one_of_type"
 Type.<one_of_type> Integer String </one_of_type>.<is_type_for> 5.5 </is_type_for>
 false
</test>

<test> name="Type.one_of_type nested type"
 Type.<one_of_type> <one_of> "a" "B" </one_of> Integer </one_of_type>.<is_type_for> "B" </is_type_for>
 true
</test>



<test> name="Number.zero" 
 Number.zero.<is_type_for> 0 </is_type_for>
 true
</test>

<test> name="Number.zero" 
 Number.zero.<is_type_for> 1 </is_type_for>
 false
</test>

<test> name="Number.zero" 
 Number.zero.<is_type_for> 0.0 </is_type_for>
 true
</test>

<test> name="positive" 
  Number.positive.<is_type_for> 0 </is_type_for>
  false
</test>
<test> name="positive" 
 Number.positive.<is_type_for> 1 </is_type_for> true
</test>
<test> name="positive" 
 Number.positive.<is_type_for> -1 </is_type_for> false
</test>

<test> name="negative" 
 Number.negative.<is_type_for> 0 </is_type_for> false
</test>
<test> name="negative" 
 Number.negative.<is_type_for> 1 </is_type_for> false
</test>
<test> name="negative" 
 Number.negative.<is_type_for> -1 </is_type_for> true
</test>

<test> name="Type.one_of_type" 
 Type.<one_of_type> Number.negative Number.zero </one_of_type>.
   <is_type_for> 0 </is_type_for>
 true
</test>

<test> name="Type.all_of_type" 
 Type.<all_of_type> <range_of> 0 10 </range_of> <range_of> 5 16 </range_of>  </all_of_type>.
   <is_type_for> 7 </is_type_for>
 true
</test>

<test> name="Type.all_of_type" 
 Type.<all_of_type> <range_of> 0 10 </range_of> <range_of> 5 16 </range_of>  </all_of_type>.
   <is_type_for> 12 </is_type_for>
 false
</test>

<!-- should work, but not tested in suite 

<test>
 Tender.Credit_card.Online.VISA.cc_ck_number_f_type.
   <is_type_for> "6111111111111111" </is_type_for>
 false
</test>


<test> name="validate_contract"

  Tender.Credit_card.Online.VISA.<validate_contract> 
  <GEO>
   total=10
   payer_fname="Mi"
   payer_lname="Pl"
   credit_card_nbr="4111111111111111"
   ccv="333"
   exp_year=10
   exp_month=10
   address="82"
   city="Boston"
   state="MA"
   zip="02481"
  </GEO> 
 </validate_contract>
 true
</test>

<test> name="validate_contract bad credit_card_nbr"
 Tender.Credit_card.Online.VISA.<validate_contract> 
  <GEO>
   total=10
   payer_fname="Mi"
   payer_lname="Pl"
   credit_card_nbr="6111111111111111"
   ccv="333"
   exp_year=10
   exp_month=10
   address="82"
   city="Boston"
   state="MA"
   zip="02481"
  </GEO> 
 </validate_contract>
 false
</test>

<test> name="validate_contract with text passes"
  GEO.<defclass _name="val_class"> x=req=<text> 1 5 Type.numeric </text>
  </defclass>
  val_class.<validate_contract> args=<GEO> x="444" </GEO> </validate_contract>
  true
</test>



<test> name="validate_contract with text fails"
  GEO.<defclass _name="val_class"> x=req=<text> 2 5 Type.numeric </text>
  </defclass>
  val_class.<validate_contract> args=<GEO> x="4" </GEO> </validate_contract>
  false
</test>

<test> name="validate_contract with text"
  GEO.<defclass _name="val_class"> x=req=<text> 2 5 Type.numeric </text>
  </defclass>
  val_class.<validate_contract> args=<GEO> x="aaa" </GEO> </validate_contract>
  false
</test>

<test>
 <text> 2 5 Type.numeric </text>.<is_type_for> "aaa" </is_type_for>
 false
</test>

<test>
 <text> 2 5 Type.numeric </text>.<is_type_for> "555" </is_type_for>
 true
</test>


<test> name="upper_alphanumeric"
 Type.upper_alphanumeric.<is_type_for> "AAA" </is_type_for>
 true
</test>

<test> name="upper_alphanumeric"
 Type.upper_alphanumeric.<is_type_for> "aaBB" </is_type_for>
 false
</test>



-->

<test> name="Type.range_of.is_type_for string"
 Type.<range_of> "A" "Z" </range_of>.<is_type_for> "C" </is_type_for>
 true
</test>

<test> name="Type.range_of.is_type_for string"
 Type.<range_of> "C" "Z" </range_of>.<is_type_for> "A" </is_type_for>
 false
</test>

<test> name="Type.range_of.is_type_for string"
 Type.<range_of> "C" "D" </range_of>.<is_type_for> "D" </is_type_for>
 false
</test>

<test> name="Type.range_of.is_type_for string"
 Type.<range_of> "C" "D" </range_of>.<is_type_for> "C" </is_type_for>
 true
</test>

<test> name="Type.range_of.is_type_for string"
 Type.<range_of> "C" "D" include_max=true </range_of>.<is_type_for> "D" </is_type_for>
 true
</test>

<test> name="Type.range_of.is_type_for string"
 Type.<range_of> "AC" "ACD" </range_of>.<is_type_for> "ACA" </is_type_for>
 true
</test>

<test> name="Type.range_of.from string"
 Type.<range_of> "A" "Z" </range_of>.<from> "C" </from>
 "C"
</test>

<test> name="Type.range_of.from string"
 Type.<range_of> "000" "999" </range_of>.<from> 8 </from>
 "008"
</test>

<test> name="Type.range_of.from string"
 Type.<range_of> "000" "999" pad_char=" "</range_of>.<from> 8 </from>
 "  8"
</test>

<test> name="Type.range_of.from string"
 Type.<range_of> "000" "999" </range_of>.<from> 8888 </from>
 "8888"
</test>

<test> name="Type.one_of.make_widget2" 
Type.one_of.<ui_config>
  key_path="xx"
  a_key="yy"
  a_value=Security_profile.of.setup
  a_type=<one_of> Security_profile.of.setup </one_of>
  obj=GEO
</ui_config>.<to_htm/>
<![CDATA[<SELECT name="xx.yy"><OPTION selected="true" value='Business.Security_profile.of.setup'>setup - Setup Profile</OPTION></SELECT>]]>
</test>

<test> name="Type.one_of.make_widget2" 
Type.one_of.<ui_config>
  key_path="xx"
  a_key="yy"
  a_value=Security_item.of."122"
  a_type=<one_of> Security_item.of."122" </one_of>
  obj=GEO
</ui_config>.<to_htm/>
<![CDATA[<SELECT name="xx.yy"><OPTION selected="true" value='Business.Security_item.of."122"'>122 - Can Void Entire Current Event</OPTION></SELECT>]]>
</test>

<test> name="Type.one_of.make_widget2" 
Type.<ui_config>
  key_path="xx"
  a_key="yy"
  a_value=Security_profile.of.setup
  a_type=Security_profile
  obj=GEO
  owner=Business_app
</ui_config>.<to_htm/>
<![CDATA[<SELECT name="xx.yy"><OPTION selected="true" value='Business.Security_profile.of.setup'>setup - Setup Profile</OPTION></SELECT>]]>
</test>

</do>
