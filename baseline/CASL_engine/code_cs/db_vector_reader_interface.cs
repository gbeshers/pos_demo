using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace CASL_engine
{
	/// <summary>
	/// Interface for db_vector_reader methods
	/// </summary>
	public interface IDBVectorReader
	{
    void AddCommand(string command_text, CommandType command_type, Hashtable sql_args);
    int ExecuteNonQuery(ref string error_message);
    bool EstablishTransaction(ref string error_message);
    bool ExecuteTransaction(ref string strError, ref int iRowCount);
    bool ExecuteTransactionCommands(ref string strError, ref int iRowCount, /*added by DH for bug# 6218*/ bool bCloseReader);
    bool RollbackTransaction(ref string error_message);
    bool CommitTransaction(ref string error_message);

    // Runs a query and reads the results into the recordset.
    void ReadIntoRecordset(ref GenericObject record_set, int start, int insert_size);
    
    // Close the reader connection
    void CloseReader();
	}
}
