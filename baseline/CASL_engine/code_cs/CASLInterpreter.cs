/* -*- mode: c; -*- */
// #define DEBUG
// #define DEBUG_CASL_Frame
// #define DEBUG_GenObj_Code
// #define DEBUG_promote_to_GEO
// #define PERFORMANCE_GRAPH
// #define DEBUG_GenObj_Lock


using System;
using System.Reflection;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Runtime.CompilerServices;
using System.Xml;
using System.Text;
using System.Windows.Forms;
using System.Web;
using System.Runtime.InteropServices;

using System.Diagnostics;
using System.Threading;

namespace CASL_engine
{
  //***********************************************************************
  // CASL evaluation functions
  //***********************************************************************

  // The EE_Steps are the steps to evaluate a method C# or CASL.
  // After set_args the handling of C# methods differs from CASL
  // methods.  Sometimes the caller instructs us to skip to a
  // specific step because the previous steps have already been done.
  //
  // However the first and last steps are done by every method call.

  public enum EE_Steps {
    standard,     // Normal execute_expr()
    evaluate_args,
    set_args,
    execute_method,
    call_CS_delegate,
    handle_constructors,
    setup_CASL_method,
    execute_expr_list,
    execute_if,
    post_execute,
    base_frame,
    casl_call_frame,
    execute_file_frame,
    undefined
  };

  //
  // The CASL_Frame implements each method call for the per thread
  // CASL_Stack, plus a few odd cases:
  //     base_frame is always the bottom frame of the stack and is
  //         the only frame with 'calling_frame == null'.
  //     execute_expr_list is used for cases where there is an
  //         implicit '<do> </do>'; see CASL_execute_document() or
  //         CASL_load().
  //     execute_if is used by CASL_if to reduce the C# stack size.
  //     standard handles primitives, symbols, and paths.
  // Other EE_Steps are all involved in evaluating C# or CASL
  // methods.

  public class CASL_Frame {
#if DEBUG_CASL_Frame
    public static int CASL_Frame_count = 0;
    public int frame_id;
#endif
    
    // Currently set with push, but will be a parameter to execute_expr()
    // so that CASL_stack on threads works correctly.
    public CASL_Frame calling_frame;

    // The expression to be evaluated and the environment to do it in.
    // See coments in execute_expr() to understand the procedure.
    public object exp;
    public GenericObject outer_env;

    public EE_Steps next_step;
    public GenericObject call_expr = null;
    public GenericObject a_method = null;
    public Object subject = null;
    public GenericObject args = null;
    public GenericObject inner_env = null;

    // This is passed to L_execute_expr_list and L_execute_if
    public ArrayList vectors = null;
    public int offset = 0;

    // This is used to handle expression lists that implement a
    // method: if true then we grab "value" from the returned
    // GenericObject.  See L_execute_expr_list.
    public bool exprs_top_level = false;

    //
    // If a C# method is called directly from execute_expr() and
    // the next call to execute_expr() is at the end of the method, e.g.,
    // return execute_expr(f_next);
    // Then we can just return the CASL_Frame f_next and execute_expr()
    // evaluate it as the next step.  Currently used by execute_path().
    //
    public bool tail_return_safe = false;

    //
    // Disabled by capture_casl_stacktrace() so that CASL_Frames
    // that are on the CASL_stack when the CASL_error object is
    // created are not recycled prematurely.
    //
    public bool ok_to_freelist = true;

    public string method_name = null;
    public code_location loc = code_location.no_location;
    public code_location current_loc = code_location.no_location;

#if PERFORMANCE_GRAPH
    public Perf_Method perf_method_data = null;
#endif

#if DEBUG_CASL_Frame
    string cs_stack = null;

    public void setStackTrace()
    {
      string st = Environment.StackTrace;
      string [] cr_lf = new string[] {Environment.NewLine};
      string [] stk = st.Split(cr_lf, StringSplitOptions.None);

      int size = stk.Length - 3; // 3 skipped frames
      if (size > 2)
        size = 2;

      cs_stack = "";

      // Note: stk[0] is System.Environment.GetStackTrace() so we skip it.
      //       stk[1] is System.Environment.get_StackTrace() so we skip it.
      //       stk[2] is CASL_Frame.setStackTrace() so we skip it.
      //       stk[3] is CASL_Frame.init() so we skip it.
      //       stk[4] is CASL_Frame.Alloc_Frame() so we skip it.
      for (int i = 0; i < size; i++) {
        cs_stack += stk[i+5];
        if (stk[i+5].IndexOf("handle_request") >= 0)
          break;
      }
      return;
    }
#endif

    public CASL_Frame()
    {
#if DEBUG_CASL_Frame
      frame_id = CASL_Frame_count++;
#endif
    }

    //
    // We insist that the outer_env be a valid GenericObject
    // because was true in >99.99% of the cases and it saves
    // millions (litterally) of calls to
    //     (CASL_)convert_primitive_to_casl_object()
    // Use c_CASL,c_local_env() for the special cases when
    // there is no environment.
    //
    public void init(EE_Steps st, object a, GenericObject e)
    {
      next_step = st;

      exp = a;
      outer_env = e;

      if (e == null || e == c_CASL.c_undefined || (! e.has("_parent"))) {
        Logger.cs_log("CASL_Frame.init: bad outer_env!! ", Logger.cs_flag.error);
#if DEBUG
        Debugger.Break();
#endif
      }

      call_expr = null;
      a_method = null;
      subject = null;
      args = null;

      exprs_top_level = false;
      tail_return_safe = false;
      ok_to_freelist = true;
      
      // For the moment leave method_name active but may want to
      // put it inside the #if DEBUG before shipping.
      method_name = null;

      if (a is GenObj_Code c) {
        loc = c.loc;
      } else {
        loc = code_location.no_location;
      }
      current_loc = code_location.no_location;
      vectors = null;
      offset = 0;

      if (a is GenObj_Method) {
        method_name = (a as GenObj_Method).full_name;
      }
#if PERFORMANCE_GRAPH
      perf_method_data = null;
#endif

#if DEBUG_CASL_Frame
      setStackTrace();
#endif
    }

    public void clean()
    {
      method_name = null;
      loc = code_location.no_location;
      current_loc = code_location.no_location;
      vectors = null;
      inner_env = null;
      args = null;
      subject = null;
      a_method = null;
      call_expr = null;
      outer_env = null;
    }

    private static string name_to_string(object n)
    {
      if (n is string ns)
        return ns;
      if (n is int)
        return ((int) n).ToString();
      return "* Bogus *";
    }
    
    //
    // WARNING, this gets called from stacktrace which is called from
    // Logger.cs_log(), Flt_Rec.log(), etc.  This means that if we
    // call *anythng* where there is a possibility of something not
    // working and getting logged an infinite recursion is possible.
    //
    //    GenericObject.to_path() and misc.ToPath() are verboten.
    //

    public static string show_value(object val)
    {
      if (val == null)
        return "null";
      if (val is GenericObject geo_val)
        return geo_val.get_path_to_me;
      return val.ToString();
    }
 
    public override string ToString()
    {
      string s = "";
#if DEBUG_CASL_Frame
      s += frame_id.ToString() + ": ";
#endif
      switch (next_step) {
      case EE_Steps.standard:
        s += " st ";
        break;
      case EE_Steps.evaluate_args:
        s += " ea ";
        break;
      case EE_Steps.set_args:
        s += " sa ";
        break;
      case EE_Steps.execute_method:
        s += " em ";
        break;
      case EE_Steps.call_CS_delegate:
        s += " CS ";
        break;
      case EE_Steps.handle_constructors:
        s += " hc ";
        break;
      case EE_Steps.setup_CASL_method:
        s += " CASL ";
        break;
      case EE_Steps.execute_expr_list:
        s += " el ";
        break;
      case EE_Steps.execute_if:
        s += " if ";
        break;
      case EE_Steps.post_execute:
        s += " pe ";
        break;
      case EE_Steps.base_frame:
        s += " bf ";
        break;
      case EE_Steps.execute_file_frame:
        s += " eff ";
        break;
     case EE_Steps.undefined:
        s += " un ";
        break;
      }
      if (current_loc.isValid()) {
        if ((current_loc.file_index == 0) || (! loc.isValid()))
          s += current_loc.ToString();
        else {
          s += loc.ToString();
          s += " @ ";
          if (loc.file_index != current_loc.file_index)
            s += current_loc.ToString();
          else
            s += '#' + current_loc.line_num + "," + current_loc.line_pos;
        }
      } else
        s += loc.ToString();
      s += ",  ";

      switch (next_step) {
      case EE_Steps.standard:
      case EE_Steps.evaluate_args:
      case EE_Steps.set_args:
      case EE_Steps.execute_method:
      case EE_Steps.call_CS_delegate:
      case EE_Steps.handle_constructors:
      case EE_Steps.setup_CASL_method:
      case EE_Steps.post_execute:

          bool need_comma = false;

          if (args != null) {
            object subject = args.get("_subject", null);

            if (subject != null) {
            s += show_value(subject);
            }
          }

        if (method_name == null && a_method != null)
          if (a_method is GenObj_Method)
            method_name = (a_method as GenObj_Method).full_name;
        if (method_name != null)
          s += " <" + method_name + "> ";

        if (args != null) {
          // Show position (vector) args
          if (! a_method.has("_do_not_execute_vector_args")) {
            int len = args.getLength();
            for (int i = 0; i < len; i++) {
              s += show_value(args.vectors[i]);
              if (i < len-1)
                s += ", ";
              else
                need_comma = true;
            }
          } else {
            s += " vector args are unevaluated ";
          }

          if (! a_method.has("_do_not_execute_string_args")) {
            foreach (object st in args.getStringKeys()) {
              if (st is string) {
                string str = st as string;
                if (str == "_subject")
                  continue;
                if (need_comma)
                  s += ", ";
                need_comma = true;
                s += str + "=>";
                if (str.Contains("password") ||
                    str.Contains("credit")   ||
                    str.Contains("bank")     ||
                    str.Contains("cvv")      ||
                    str.Contains("exp_")     ||
                    str.Contains("login")    ||
                    str.Contains("logon")    ||
                    str.Contains("track"))
                  {
                    s += " * masked *";
                  }
                else
                  s += show_value(args.get(str, null));
#if DEBUG
              } else {
                Debugger.Break();
#endif
              }
            }
          } else {
            s += " string args are unevaluated ";
          }
        }
        break;

      case EE_Steps.execute_expr_list:
        s += "  processing expression list at expression " + offset.ToString();
        break;

      case EE_Steps.execute_if:
        s += "  processing condition, action pair #" + (offset/2).ToString();
        break;
      case EE_Steps.base_frame:
        s += "  CASL_base_frame";
        break;
      case EE_Steps.execute_file_frame:
        s += "  Execute_file_frame";
        break;
      case EE_Steps.undefined:
#if DEBUG
        Debugger.Break();
#endif
        s += "CASL_Frame **UNDEFINED**";
        break;
      }
#if DEBUG_CASL_Frame
      s += "  " + cs_stack;
#endif
      return s;
    }
  }

  // There is one CASL_Stack for each thread which is running the
  // CASL Interpreter.
  public class CASL_Stack
  {
    //
    // Explicit initialization of casl_stack is *required* for every
    // thread, i.e., ' = new CASL_Frame(EE_Steps.base_frame,...) will
    // only be run *once* for the main thread!!.
    //
    [ThreadStatic] public static CASL_Frame stack_base;
    [ThreadStatic] public static CASL_Frame stack_top;
    [ThreadStatic] public static CASL_Frame unused_frames;

    public static CASL_Frame Alloc_Frame(EE_Steps st, object a, GenericObject e)
    {
      CASL_Frame ret = unused_frames;
      
      if (ret == null)
        ret = new CASL_Frame();
      else
        unused_frames = ret.calling_frame;
      
      ret.calling_frame = CASL_Stack.stack_top;
      CASL_Stack.stack_top = ret;

      ret.init(st, a, e);

      return ret;
    }

    static private GenObj_Method base_frame_method = null;

    public static void Initialization()
    {
      if (base_frame_method == null) {
        GenericObject.Initialization();
        base_frame_method = new GenObj_Method(GO_kind.base_method);
        base_frame_method.method_name = "*base_frame";
        base_frame_method.full_name = base_frame_method.method_name;
        CASLInterpreter.dbg_meth_dict_add(base_frame_method.method_name,
                                          base_frame_method);
      }

      stack_base = new CASL_Frame();
      stack_base.calling_frame = null;
      stack_base.init(EE_Steps.base_frame, null, c_CASL.c_local_env());
#if PERFORMANCE_GRAPH
      stack_base.perf_method_data =
        CASLInterpreter.call_graph.get_per_method_data(
            base_frame_method.method_idx);
      stack_base.perf_method_data.m_call_count++;
#endif
      stack_top = stack_base;

      // Maybe can reuse frames across threads.
      unused_frames = null;
    }


    public static void Release_Frames()
    {
      stack_base = null;
      stack_top = null;
      unused_frames = null;
    }

#if DEBUG
    //
    // Checking stack integrity.  NOTE: this is a running
    // stack and does not work for a captured stack.
    //
    public static void verify(CASL_Frame frame)
    {
      // if frame is null we are just looking at the stack.
      bool found = frame == null;

      if (stack_base == null)
        Debugger.Break();
      CASL_Frame f = stack_top;
      while (f != stack_base) {
        if (f == null)
          Debugger.Break();
        if (f == frame)
          found = true;
        f = f.calling_frame;
      }
      if (!found)
        Debugger.Break();
    }
#endif    

    static public void pop_to(CASL_Frame frame)
    {
      while (stack_top != null) {
        if (stack_top == frame)
          return;
        if (stack_top == stack_base)
          break;
        CASL_Frame tmp = stack_top;
        stack_top = stack_top.calling_frame;
        if (tmp.current_loc.isValid())
          stack_top.current_loc = tmp.current_loc;
        else if (tmp.loc.isValid())
          stack_top.current_loc = tmp.loc;
        if (tmp.ok_to_freelist) {
          tmp.calling_frame = unused_frames;
          unused_frames = tmp;
          tmp.clean();
        }
      }
#if DEBUG_CASL_Frame
      Logger.cs_log("disconnected stack frame", Logger.cs_flag.error);
      Debugger.Break();
#endif
    }

#if DEBUG
    // This is significantly easier to use from the immediate window
    // in VisualStudio.
    static public string[] stacktrace(
        int max_cnt = 0,
        CASL_Frame top_frame = null)
    {
      if (top_frame == null) {
        top_frame = CASL_Stack.stack_top;
      }

      CASL_Frame frame = top_frame;
      if (max_cnt == 0) {
        while (frame != null &&
               frame.next_step != EE_Steps.base_frame) {
          frame = frame.calling_frame;
          max_cnt++;
        }
      }

      string [] fs = new string[max_cnt];
      frame = top_frame;
      int cnt = 0;
      while (frame != null &&
             frame.next_step != EE_Steps.base_frame) {
        fs[cnt++] = frame.ToString();
        if (cnt == max_cnt)
          break;
        frame = frame.calling_frame;
      }
      return fs;
    }
#endif

    static public StringBuilder stacktrace_StrBld(
        StringBuilder sb = null,
        int max_cnt = 0,
        CASL_Frame top_frame = null)
    {
      if (sb == null)
        sb = new StringBuilder();
      CASL_Frame frame = (top_frame == null) ? stack_top : top_frame;
      int cnt = 0;
      while (frame != null &&
             frame.next_step != EE_Steps.base_frame) {
        sb.AppendLine(frame.ToString());
        cnt++;
        if (cnt == max_cnt)
          break;
        frame = frame.calling_frame;
      }
      return sb;
    }

    static public CASL_Frame capture_casl_stacktrace()
    {
      CASL_Frame f = stack_top;
      while (f != null) {
        f.ok_to_freelist = false;
        if (f.next_step == EE_Steps.base_frame)
          break;
        f = f.calling_frame;
      }
      return stack_top;
    }
  }


#if PERFORMANCE_GRAPH
  //
  // The performance / call_graph data.  A few things to note:
  //
  //   - call_count = return_count + frames_discarded_by_throws
  //         frames_discarded_by_throws include the throwing method
  //         frame and all the stack frames up to, but not including,
  //         the one whose method catches the exception.  Always think
  //         C# throw because all CASL_try's are C# try-catch.
  //
  //   - total_ticks are recorded on return; divide by return_count to
  //         get as accurate an average as Windows timers support.
  //
  //   - the processing times for frames methods interrupted by a throw
  //     (possibly thrown by a called method but not caught in this
  //     method) are included in the time for method with the C# catch
  //     or the nearest enclosing method on the *CASL* stack.
  //
  // I realize that min/max/total (avg) can be misleading but it will give
  // a clear indication of where large amounts of time are going.
  //
  // Also, the tool needs to look for leaf and "near-leaf" methods, i.e.,
  // the ones most easily translated to C#.
  //
  // Finally, note the the methods which are never called.  Are they needed?

  //
  // There is one Perf_Method for every "method" that has a GenObj_Method
  // entry: GenObj_CS_Methods, GenObj_CASL_Methods, plus a few special cases,
  // e.g., the base frame method.
  //
  // The m_* stats are for all calls to the method, i.e., they are the
  // totals for that method.  The dictionary of calls notionally are
  // all the calls from the specified method to some other method.
  //
  // The calls dictionary key is a long which encodes the *callee*
  // method_id and the call_site information.  Note that the caller
  // is always the method which corresponds to the Perf_Method that
  // the calls dictionary belongs to.
  //
  // The reason for the callee to be part of the key is that call site
  // information is sometimes missing or sometimes deliberately conflated,
  // e.g., execute_string() we usually don't really gain useful information
  // by recording the position in the string---although that is an option.
  //
  // The calls record the total stats from a particular location.
  //
  // Notionally if you take all the Perf_Calls for a given callee_method_id
  // the sum of the stats should be close to the m_* stats with the
  // difference being interpreter overhead for those calls.
  //
  public class Perf_Method {
    public int method_id = -1;

    public ulong m_call_count = 0;
    public ulong m_return_count = 0;
    public ulong m_total_time = 0;
    public ulong m_min_time = Int64.MaxValue;
    public ulong m_max_time = 0;

    public Dictionary<long, Perf_Call> calls =
      new Dictionary<long, Perf_Call>();

    public Perf_Method(int m_id)
    {
      if (m_id < 0 || m_id >= CASLInterpreter.MethodCount)
        Debugger.Break();
      method_id = m_id;
    }
      
    public void update(ulong delta)
    {
      lock (calls) {
        m_return_count++;
        m_total_time += delta;
        if (delta < m_min_time)
          m_min_time = delta;
        if (delta > m_max_time)
          m_max_time = delta;
      }
    }

    public Perf_Call get_per_call_site(
                                       int casl_caller_idx,
                                       int callee_idx,
                                       code_location cl)
    {
      long call_site = (long) 0;
      if (cl.line_pos < 0 || cl.line_pos >= 1024) {
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          Flt_Rec.log("get_per_call_site",
                    "invalid column " + cl.line_pos.ToString());
        cl.line_pos %= 1024;
      }
      if (cl.line_num < 0 || cl.line_num >= 16384) {
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          Flt_Rec.log("get_per_call_site",
                      "invalid line " + cl.line_num.ToString());
        cl.line_num %= 16384;
      }

      if (cl.file_index == 0 && cl.source_or_filename != null) {
        //
        // 500 is the size of the FileIndex so this way File and
        // string positions don't conflict.
        //
        call_site = (long) cl.source_or_filename.GetHashCode();
        call_site %= 536870911;    // Yes 536870911 is prime :)
        call_site += 500;
      } else
        call_site = (long) cl.file_index;
      call_site <<= 24;
      call_site |= ((long) cl.line_num << 10) | (long) cl.line_pos;

      if (callee_idx < 0 || callee_idx >= 16384) {
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          Flt_Rec.log("get_per_call_site",
                      "invalid callee index " + callee_idx.ToString());
      }
      call_site <<= 14;
      call_site |= (long) callee_idx;

      Perf_Call pc = null;
      lock (calls) {
        if (calls.ContainsKey(call_site)) {
          pc = calls[call_site];
        } else {
          pc = new Perf_Call();
          pc.caller_method_id = method_id;
          pc.casl_caller_method_id = casl_caller_idx;
          pc.callee_method_id = callee_idx;
          pc.loc = cl;
          calls[call_site] = pc;
        }
      }
      return pc;
    }
  };

  public class Perf_Call {
    public int caller_method_id = -1;
    public int casl_caller_method_id = -1;

    public code_location loc;

    public int callee_method_id = -1;

    public ulong call_count = 0;
    public ulong return_count = 0;
    public ulong total_time = 0;
    public ulong min_time = Int64.MaxValue;
    public ulong max_time = 0;

    public void update(ulong delta)
    {
      lock (this) {
        call_count++;  // At the moment don't worry about exceptions. GMB.
        return_count++;
        total_time += delta;
        if (min_time > delta)
          min_time = delta;
        if (max_time < delta)
          max_time = delta;
      }
    }
    public static bool file_order(Perf_Call pc1, Perf_Call pc2)
    {
      if (pc1.loc.line_num < pc2.loc.line_num)
        return true;
      if (pc1.loc.line_num > pc2.loc.line_num)
        return false;
      return pc1.loc.line_pos < pc2.loc.line_pos;
    }
  };
  //
  // Get the perf_raw object for each call site inside the
  // calling method -- there is a perf_raw for every call site.
  //
  public class CallGraph {
    private Perf_Method[] method_stats =
      new Perf_Method[10000];

    public Perf_Method get_per_method_data(int method_idx)
    {
      Perf_Method pm;
      lock (method_stats) {
        pm = method_stats[method_idx];
        if (pm == null) {
          pm = new Perf_Method(method_idx);
          method_stats[method_idx] = pm;
        }
      }

      return pm;
    }

    public int length()
    {
      return method_stats.Length;
    }
  }
#endif
  

  /// <summary>
  /// Summary description for CASLInterpreter.
  /// </summary>
  public class CASLInterpreter
  {
    public static int step_mode=0;
    public static string no_subject="_no_subject_";
#if DEBUG_GenObj_Lock
    public static bool load_complete = false;
#endif

    public static ThreadLocal<GenericObject> no_session_my = null; // Bug 24902 MJO - Custom "my" object for when there is none present in the session

    // Believe this is best moved outside Interpreter.
    public class CASL_File {
      public string         file_name = "";
      public int            file_index = 0;
      public int            file_last_line = -1;
      public BitArray       per_line_breakpoints = null;
    };
    static public Dictionary<string, CASL_File> FileDictionary =
        new Dictionary<string, CASL_File>();
    static public CASL_File[] FileIndex = new CASL_File[500];
    static public int FileCount = 0;
    // 0= continue 
    // 1= step_into


    static public Dictionary<string, GenObj_Class> ClassDictionary =
      new Dictionary<string, GenObj_Class>();
    

#if PERFORMANCE_GRAPH
    static public Dictionary<string, GenObj_Method> MethodDictionary =
      new Dictionary<string, GenObj_Method>();
    static int MaxMethodIndex = 20000;
    static public GenObj_Method[] MethodIndex =
      new GenObj_Method[MaxMethodIndex];
#endif

    static bool FileIdx_initialized = false;
    public static int getFileIdx(string name)
    {
      CASL_File file = null;

      lock (FileDictionary) {
        if (! FileIdx_initialized) {
          // This is the default file name to avoid array bounds errors
          // when the file is not known, e.g., execute_string().
          file = new CASL_File();
          file.file_name = "undefined file";
          file.file_index = 0;
          FileIndex[0] = file;
          FileDictionary.Add(FileIndex[0].file_name, FileIndex[0]);
          file = new CASL_File();
          file.file_name = "builtin";
          file.file_index = 1;
          FileIndex[1] = file;
          FileCount = 2;
          FileDictionary.Add(FileIndex[1].file_name, FileIndex[1]);
          code_location.builtin_location = new code_location(null, 1, 0, 0);
          FileIdx_initialized = true;
        }

        string hdr = misc.CASL_get_code_base();
        int st = name.IndexOf(hdr);
        // Trim the code_base path plus the '/'
        if (st >= 0)
          name = name.Substring(st + hdr.Length + 1);

        if (! FileDictionary.ContainsKey(name)) {
          file = new CASL_File();
          file.file_name = name;
          file.file_index = FileCount++;

          int len = FileIndex.Length;
          if (file.file_index >= len) {
            len += (len / 10) + 1;
            if (len < FileCount)
              len = FileCount + len;
            Array.Resize(ref FileIndex, len);
          }
          FileIndex[file.file_index] = file;
          FileDictionary.Add(name, file);
        } else {
          file = FileDictionary[name];
        }
      }

      return file.file_index;
    }

    public static bool dbg_class_dict_add(string name, GenObj_Class c1)
    {
      bool ret = false;
      lock (ClassDictionary) {
        if (ClassDictionary.ContainsKey(name)) {
          GenObj_Class c2 = ClassDictionary[name];
          if (c1 != c2) {
            if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
              Flt_Rec.log("dbg_class_dict_add",
                          "Already in ClassDictionary " + name +
                          Environment.NewLine +
                          "    " +  c1.ToString() + Environment.NewLine +
                          "    " +  c2.ToString() + Environment.NewLine);
          }
        } else {
          ClassDictionary.Add(name, c1);
          ret = true;
        }
      }
      return ret;
    }

    // Consider second Dictionary with <method_name, GenObj_Method>, but
    // unclear CASL equivalent.
    public static int MethodCount = 0;
    public static bool dbg_meth_dict_add(string name, GenObj_Method gom)
    {
      bool ret = true;
#if PERFORMANCE_GRAPH
      if (MethodCount >= MaxMethodIndex - 1)
        return ret;

      if (!gom.loc.isValid()) {
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          Flt_Rec.log("dbg_meth_dict_add", "gom.loc invalid " + name, true, true);
      }
        
      lock (MethodDictionary) {
        try {
          if (gom.method_idx != -1) {
            if (MethodIndex[gom.method_idx] == gom &&
                MethodDictionary.ContainsKey(name) &&
                MethodDictionary[name] == gom) {
#if DEBUG_tmp
              // These are minor cleanup work.  GMB
              if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
                Flt_Rec.log("dbg_meth_dict_add", "redo of " + name);
#endif
              ret = false;
            } else {
#if DEBUG_tmp
              if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
                Flt_Rec.log(
                  "dbg_meth_dict_add", name + " already has method_idx = " +
                  gom.method_idx.ToString() + "    " + gom.ToString() +
                  Environment.NewLine + "MethodIndex has " +
                    ((MethodIndex[gom.method_idx] == null) ? "null"
                     : MethodIndex[gom.method_idx].show_short()),
                  true, true);
#endif
            }
          }
          if (ret) {
            gom.method_idx = MethodCount++;
            MethodIndex[gom.method_idx] = gom;

            //
            // Do to issues with the class container hierarchy we can
            // get a duplicate name, but everything except debugger stuff
            // still works including performance statistics.
            //
            if (MethodDictionary.ContainsKey(name)) {
              if (Flt_Rec.go(Flt_Rec.where_t.always_flag)) {
                StringBuilder msg = new StringBuilder();
                msg.AppendLine("Already in MethodDictionary " + name);
                msg.AppendLine("MethodDictionary has " +
                               MethodDictionary[name].show_short());
                msg.AppendLine("    " + gom.ToString());
                Flt_Rec.log("dbg_meth_dict_add", msg.ToString(), true, true);
              }
            } else {
              MethodDictionary.Add(name, gom);
              ret = true;
            }
          }
        } catch (Exception e) {
          Logger.LogError("CASLInterpreter.dbg_meth_dict_add: " +
                          "Exception on " + name + Environment.NewLine,
                          e.ToString() +
                          CASL_Stack.stacktrace_StrBld().ToString()); 
        }
      }
#endif
      return ret;
    }


#if PERFORMANCE_GRAPH
    public static CallGraph call_graph = new CallGraph();
#endif

    /// <summary>
    /// evaluate expression that includes primative, string, symbol, path, 
    /// call, system call
    /// </summary>
    /// <param name="a_expr"></param>
    /// <param name="local_env"></param>
    /// <returns></returns>
    // Called by: many places in CASLInterpreter.cs and misc.cs.
    //            3 in c_CASL.cs, CASL_server.cs - set_nested_parent


    //
    // Main routine of the CASLInterpreter.  Much of the heavily nested
    // code has been pulled into this routine to avoid problems with
    // stackoverflow.
    //

    // The frame passed in should always be the top of the CASL_Stack, we
    // verify this in debug mode.
    public static object execute_expr (CASL_Frame frame)
    {
      // The object to be returned and a flag indicating that ret has been
      // set --- null is a legitimate return value so flag is needed.
      object ret = null;
      int this_cnt = 0;

      object a_method = null;  // Here for debugging purposes.
      GenericObject go_expr = null;
      
#if DEBUG_CASL_Frame
      if (CASL_Stack.stack_top != frame) {
        Logger.cs_log("Frame passed to execute_expr must be top of CASL_Stack",
                      Logger.cs_flag.error);
      }
#endif

      // At the end of this routine we need to pop the CASL_Stack which is
      // accomblished by simply assigning the frame passed in to
      // CASL_Stack.stack_top. 
      CASL_Frame orig_frame = frame;


#region Flight Recorder Call
      if (Flt_Rec.go(Flt_Rec.where_t.execute_expr_flag))
        Flt_Rec.log("execute_expr", Flt_Rec.show_object(frame.exp));
#endregion
      
      //
      // This loop implements "tail recursion" in a handful of cases.  The
      // CASL_Stack still reflects the CASL semantics, but we reduce the
      // number of C# frames on the stack.
      //
      while (true) {
        c_CASL.master_step_count++;
#if DEBUG_CASL_Frame
        CASL_Stack.verify(frame);
#endif
        if (frame.next_step == EE_Steps.standard) {
          if (Flt_Rec.go(Flt_Rec.where_t.execute_expr_flag))
            Flt_Rec.log("execute_expr " + this_cnt.ToString(),
                        Flt_Rec.show_object(frame.exp));

          //
          // Handle primitives (true, false, a number(decimal, integer),
          // string, null).
          //
          // May want to expand this to any non GenObj_Code.
          //

          if( frame.exp == null || frame.exp.GetType().IsValueType ||
              frame.exp.GetType()== typeof(System.String)) {
            ret = frame.exp;
            break;  // ret is never a CASL_Frame
          }

          //
          // Eventually we want the this to always be GenObj_Code with
          // useful code location information, but there are a few corner
          // cases left to fix.  GMB.
          //
          if (frame.exp is GenericObject) {
            go_expr = frame.exp as GenericObject;
            if (go_expr is GenObj_Code) {
              GenObj_Code go_code = go_expr as GenObj_Code;
              if (go_code.loc.isValid())
                frame.current_loc = go_code.loc;
            }
#if DEBUG_GenObj_Code
            else {
              Logger.cs_log("execute_expr() got GenericObject but was looking for GenObj_Code " +
                            go_expr.ToString(), Logger.cs_flag.trace);
            }
#endif

            if (go_expr.kind == GO_kind.symbol) {
              string symbol_name = Tool.symbol_to_string(
                  (GenObj_Symbol) go_expr);

              Object symbol_var = frame.outer_env.get(symbol_name,
                                                  c_CASL.c_undefined, true);

              if( symbol_var != null && c_CASL.c_undefined.Equals(symbol_var)) {
                string s_message = string.Format("Variable {0} not found" +
                                                 " in environment. ", misc.to_eng(go_expr));
                Logger.cs_log("execute_expr(): Variable " + symbol_name + " not found in environment",
                              Logger.cs_flag.error);
                throw CASL_error.create("message", s_message,
                                     "subject", frame.outer_env,
                                     "key", misc.to_eng(go_expr));
              }
              ret = symbol_var;
              break;  // ret is never a CASL_Frame
            }

            if ( go_expr.kind == GO_kind.path ) {
              if( Tool.is_cs_path(go_expr) ) { // if path is call_name, then return methodInfo
                ret = eval_cs_member(go_expr as GenericObject); // TODO: implement eval_cs_member
              } else {
                ret = execute_path(frame, go_expr as GenObj_Path, frame.outer_env);
              }
              if (ret is CASL_Frame)
                goto have_CASL_Frame;
              break;
            }
            
            object call_name = go_expr.get("_parent", false);
            if(!Tool.is_symbol_or_path(call_name)) {
              ret = go_expr; // return already evaluated object
              break;
            }

            CASL_Frame f1 = CASL_Stack.Alloc_Frame(EE_Steps.standard,
                                                   call_name, frame.outer_env);
            f1.method_name = Tool.symbol_or_path_to_string(
                call_name as GenericObject);

            a_method = CASLInterpreter.execute_expr(f1);

            if( a_method == null ) {
#if DEBUG_GenObj
              Logger.cs_log("execute_expr(): Method " + f1.method_name + " not found in environment",
                   Logger.cs_flag.error);
#endif
              string s_message = string.Format("execute_expr#1: Method {0} not found in {1}",
                                               misc.to_eng(call_name),
                                               misc.to_eng(frame.outer_env));
              throw CASL_error.create("message", s_message, "subject", frame.outer_env,
                                   "key", call_name);
            }
#if DEBUG_GenObj_Code
            if (! (a_method is GenObj_Method))
              Debugger.Break();
#endif
            
            frame.call_expr = go_expr;
            frame.a_method = a_method as GenericObject;
            frame.subject = no_subject;
#if DEBUG
            frame.method_name = Tool.symbol_or_path_to_string(
                                      call_name as GenericObject);
#endif
#if GENERAL_CS_OBJECTS
          } else {
            Debugger.Break();
#endif
          }
        }


        //
        // At this point we are calling a method or an expression list from
        // a string or file.
        //
        // The steps below capture the evaluation process for CASL methods
        // either those implemented in C# or those which execute CASL code.
        //
        // The reason for the steps is that the CASL Interpreter often short
        // circuits the evaluation process when it knows that it already has
        // a method, e.g., from execute_path(), CASL_call(), and CASL_apply().
        //

#if PERFORMANCE_GRAPH
        ulong ticks_start = (ulong) c_CASL.master_stopwatch.Elapsed.TotalMilliseconds * 1000000;
#endif
        //
        // Basically, execute_method() and execute_method_aux() calls go
        // to L_execute_method().  We follow the same code path for C#
        // and CASL calls until we must separate.
        //
        // The execute_exprs() calls go to L_execute_expr_list.
        //

        switch (frame.next_step) {
        case EE_Steps.evaluate_args:
          goto L_evaluate_args;
        case EE_Steps.set_args:
          goto L_set_args;
        case EE_Steps.execute_method:
          goto L_execute_method;
        case EE_Steps.handle_constructors:
          goto L_handle_constructors;
        case EE_Steps.execute_expr_list:
          goto L_execute_expr_list;
        case EE_Steps.execute_if:
          goto L_execute_if;
        default: // standard
          break;
        }

      L_evaluate_args:
        frame.args = evaluate_args_from_call_expr(frame, frame.call_expr, frame.a_method, frame.outer_env);
      L_set_args:
        set_args(frame, frame.args, frame.a_method, frame.subject, frame.outer_env); // remove in C# case ? -- change to fallback condition

        //
        // There are 3 cases:
        //   We are calling a C# delegate -> L_call_CS_delegate
        //   We are calling a CASL method -> L_execute_CASL_method
        //   We are initializing an instance -> L_handle_constructors
        //     NOTE: a_method is *NOT* necessarily a class!!
        //
        // frame.a_method is the method being called.
        // frame.args is the argument list.
      L_execute_method:
        if (frame.a_method is GenObj_CS_Method)
          goto L_call_CS_delegate;
#if Overwrite_Method_Fix
        if (frame.a_method is GenObj_CASL Method)
#else
          if (Tool.is_casl_method(frame.a_method))
#endif    
            goto L_setup_CASL_method;

        //
        // We are initializing an instance -- perhaps from another instance.
        // We search up the _parent chain for the init method which may now
        // be a C# method to support wrappers.
        //
      L_handle_constructors:
        {
#if DEBUG_GenObj_Class
          if ( ! (frame.a_method is GenObj_Class)) {
            Logger.cs_log("L_handle_constructors not GenObj_Class!! " +
                          frame.a_method.ToString(), Logger.cs_flag.trace);
          }
#endif
          GenObj_Method g_init_method = frame.a_method.get("init",null,true)
            as GenObj_Method;
          if( g_init_method == null ) {
            GenericObject g_init_method_1 = frame.a_method.get("init",null,true) as GenericObject;
            if (g_init_method_1 != null)
              Logger.cs_log("execute_expr(): L_handle_constructors: g_init_method" +
                            " is not a GenObj_Method " + g_init_method_1.ToString(),
                            Logger.cs_flag.error);

            string s_message = string.Format("init method not found in {0}.",
                                             misc.to_eng(frame.a_method));
            throw CASL_error.create("message", s_message,
                                 "subject", frame.a_method,
                                 "key", "init");
          }
          // create init method inner_env
          GenericObject new_object = frame.args; 
          new_object.set("_parent",frame.a_method);
          GenericObject inner_env = c_CASL.c_GEO();
          inner_env.set("_local",inner_env);

          inner_env.set("_subject", new_object);
          // set _subject for init method, will change to _subject_of_call
          if(frame.subject == null )
            {
              inner_env.set("_subject_of_call", frame.subject);
            }
          else if( !frame.subject.Equals(no_subject))
            {
              inner_env.set("_subject_of_call", frame.subject);
            }
          if (g_init_method.has("_do_not_execute_vector_args"))
            {                           // control flow method can be in casl or in c#(it is true at the moment)
              inner_env.set("_outer_env", frame.outer_env);
            }
          frame.a_method = g_init_method;
          frame.vectors = g_init_method.vectors;
          frame.args = inner_env;
          if (frame.a_method is GenObj_CS_Method)
            goto L_call_CS_delegate;
        }

        //
        // At this point we have a CASL method, period.
        //

      L_setup_CASL_method:
#if PERFORMANCE_GRAPH
          ticks_start = (ulong) c_CASL.master_stopwatch.Elapsed.TotalMilliseconds * 1000000;

          // Increments call_count
          if (frame.a_method is GenObj_CASL_Method) {
            GenObj_Method gom = frame.a_method as GenObj_Method;
#if FALSE
            if (! gom.loc.isValid()) {
              if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
                Flt_Rec.log("L_setup_CASL_method gom.loc is invalid ",
                    gom.full_name,
                    true, true);
            }
#endif
            frame.perf_method_data =
              call_graph.get_per_method_data(gom.method_idx);
            lock (frame.perf_method_data) {
              frame.perf_method_data.m_call_count++;
            }
          }
#endif

        frame.inner_env = frame.args;
        frame.inner_env.set("_local", frame.inner_env);
        frame.inner_env.set("_this_method", frame.a_method);

        frame.exprs_top_level = true;
        // Fall through to L_execute_expr_list
        // ret = execute_exprs(frame, frame.a_method, frame.inner_env,true);
        frame.vectors = frame.a_method.vectors;

      L_execute_expr_list:
        bool is_not_init = (frame.a_method != null) &&
          ! "init".Equals(frame.a_method.get("_name", ""));
        int i = 0;
        ArrayList ex_l = frame.vectors;
#if DEBUG
        if (frame.a_method.vectors != frame.vectors)
          Debugger.Break();
#endif
        int L = ex_l.Count;
        while (i < L) {
#if DEBUG
          frame.offset = i;
          if (frame.a_method.get(i) != ex_l[i])
            Debugger.Break();
#endif
          ret = CASLInterpreter.execute_expr(
                    CASL_Stack.Alloc_Frame(EE_Steps.standard,
                                           ex_l[i],
                                           frame.inner_env));
          CASL_Stack.pop_to(frame);

          if ( is_not_init &&
               ret != null &&
               ret is GenericObject) {
            GenericObject geo_ret = ret as GenericObject;
            if (geo_ret.get("_parent", null) == c_CASL.c_return) {
              if (frame.exprs_top_level)
                ret = (ret as GenericObject).get("value", null);
              i = L;
            }
          }
          i++;
        }
        goto L_post_execute;

      L_execute_if:
        ret = null;
        ArrayList cond_pairs = frame.vectors;
        int M = cond_pairs.Count;
        for (int j = 0; j < M; j += 2) {
#if DEBUG
          frame.offset = j;
          if (cond_pairs[j] is GenObj_Code)
            frame.current_loc = (cond_pairs[j] as GenObj_Code).loc;
#endif
          Object b_condition = execute_expr(
              CASL_Stack.Alloc_Frame(EE_Steps.standard, cond_pairs[j],
                                     frame.inner_env));
          if (CASL_Stack.stack_top != frame)
            if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
              Flt_Rec.log("L_execute_if ", "if condition stack_top_issue");
          if (b_condition == null || ! false.Equals(b_condition)) {
#if DEBUG
            if (cond_pairs[j+1] is GenObj_Code)
              frame.current_loc = (cond_pairs[j+1] as GenObj_Code).loc;
#endif
            ret = execute_expr(
                CASL_Stack.Alloc_Frame(EE_Steps.standard, cond_pairs[j+1],
                                       frame.inner_env));
            if (CASL_Stack.stack_top != frame)
              if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
                Flt_Rec.log("L_execute_if ", "then condition stack_top_issue");
            break;
          }
        }
        goto L_post_execute;

      L_call_CS_delegate:
        GenObj_CS_Method m_cs = frame.a_method as GenObj_CS_Method;

#if PERFORMANCE_GRAPH
        ticks_start = (ulong) c_CASL.master_stopwatch.Elapsed.TotalMilliseconds * 1000000;

        // Increments the method_data.call_count
        frame.perf_method_data =
          call_graph.get_per_method_data(m_cs.method_idx);
        lock (frame.perf_method_data) {
          frame.perf_method_data.m_call_count++;
        }
#endif

#if DEBUG_outer_env
        if (frame.args.has("_outer_env") &&
            (frame.outer_env != frame.args.get("_outer_env"))) {
          Logger.cs_log("### outer_env's are not equal", Logger.cs_flag.error);
          Debugger.Break();
        }
#endif

        frame.tail_return_safe = true;
        if (m_cs.method_delegate_frame != null) {
          ret = m_cs.method_delegate_frame(frame);
        } else if (m_cs.method_delegate != null) {
          ret = m_cs.method_delegate(frame.args);
        }
        else {
          Logger.cs_log("CS Delegate does not exist: " + m_cs.full_name +
                        " \"" + m_cs.cs_name + "\"",
                        Logger.cs_flag.error);
#if DEBUG
          Debugger.Break();
#endif
        }
        goto L_post_execute;

      L_post_execute:
#if PERFORMANCE_GRAPH        
        ulong ticks_end = (ulong) c_CASL.master_stopwatch.Elapsed.TotalMilliseconds * 1000000;
        if (frame.next_step != EE_Steps.execute_expr_list &&
            frame.a_method is GenObj_Method) {
          ulong delta = ticks_end - ticks_start;
          frame.perf_method_data.update(delta);
          GenObj_Method gom = frame.a_method as GenObj_Method;

#if FALSE
            if (! gom.loc.isValid()) {
              if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
                Flt_Rec.log("L_post_execute gom.loc is invalid ",
                    gom.full_name,
                    true, true);
            }
#endif

          // Find caller which may be a C# method, e,g,. fCASL_do,
          // fCASL_if, etc.
          CASL_Frame caller_fs = frame.calling_frame;
          while (caller_fs.perf_method_data == null)
            caller_fs = caller_fs.calling_frame;

          // Find the casl caller which will often be the same, but
          // does help to distinguish where time is being used.
          CASL_Frame casl_caller_fs = caller_fs;
          int casl_method_idx = 0;
          while (casl_caller_fs != null) {
            if ((casl_caller_fs.a_method != null) &&
                (casl_caller_fs.a_method is GenObj_CASL_Method)) {
              casl_method_idx =
                (casl_caller_fs.a_method as GenObj_Method).method_idx;
              break;
            }
            casl_caller_fs = casl_caller_fs.calling_frame;
          }
          Perf_Call pc = caller_fs.perf_method_data.get_per_call_site(
              casl_method_idx,
              gom.method_idx,
              frame.current_loc);
          
          pc.update(delta);
        }
#endif
        ;


         // Thrown exceptions disrupt simple decrement.
         if (Flt_Rec.go(Flt_Rec.where_t.execute_expr_flag))
           Flt_Rec.log("<exit> execute_expr " + this_cnt.ToString(),
                       Flt_Rec.show_object(ret));

        if (! (ret is CASL_Frame))
          break;
   have_CASL_Frame:
        frame = ret as CASL_Frame;
        ret = null;
      }

#region Flight Recorder Call
      // Thrown exceptions disrupt simple decrement.
      if (Flt_Rec.go(Flt_Rec.where_t.execute_expr_flag))
        Flt_Rec.log("<exit> execute_expr", Flt_Rec.show_object(ret));
#endregion

      //
      // POP the CASL_Stack of all frames pushed onto the stack by
      // recursive calls and continuation frames.  This releases the
      // inner stack frames to the free list (or garbage collection).
      //
      // NOTE: the frame passed as an argument is also popped!!
      //
      CASL_Stack.pop_to(orig_frame.calling_frame);
      return ret;
    }


    //
    // set_args is always called immediately after
    // GenericObject args = evaluate_args_from_call_expr(
    //       call_expr, a_method as GenericObject, outer_env);
    //

    public static void set_args(
        CASL_Frame frame,
        GenericObject args, object a_method,
        Object subject, GenericObject outer_env)
    {
      if( a_method is GenObj_Method
// #if ToBeRemoved
          || (a_method is GenericObject && 
              (a_method as GenericObject).get("_parent", false) ==
              c_CASL.c_method)
// #endif
          )
      {
        // explicitly set subject
        if(subject == null ) 
          args.set("_subject",subject);
        else if( !subject.Equals(no_subject) )//&& !args.has("_subject")
          args.set("_subject",subject);
        // set args by position based on the contract
        set_args_from_contract(args, a_method as GenericObject);
        // set default value in the method contract
        set_default_value_in_contract(frame, args, a_method as GenericObject);
        //validate args
        misc.validate_contract_against("_subject",a_method, "args",args);
        if ((a_method as GenericObject).has("_do_not_execute_vector_args")
          || (a_method as GenericObject).has("_do_not_execute_string_args")
          || (a_method as GenericObject).has("_pass_outer_env")
          )				// control flow method can be in casl or in c#(it is true at the moment)
          args.set("_outer_env",outer_env);
      }
      else //if ( Tool.is_casl_class(a_method) )
      {
        // set args by position based on the contract
        set_args_from_contract(args, a_method as GenericObject);
      }
      //			else 
      //			{
      //				string s_message = string.Format("Invalid call name: {0}.",misc.to_eng(a_method));
      //				throw CASL_error.create("message",s_message, "code",265);
      //			}
      return;
    }


    public static bool is_none_control_flow_method(GenericObject a_method)
    {
      if (a_method is GenObj_Method &&
          (!a_method.has("_do_not_execute_vector_args")))
        return true;
// #if Remove when certain of methods
      if (a_method.get("_parent", null) == c_CASL.c_method &&
          (!a_method.has("_do_not_execute_vector_args")))
        return true;
// #endif
      return false;
    }



    public static void set_default_value_in_contract(
        CASL_Frame frame, GenericObject args, GenericObject contract)
    {
      GenericObject FO = (contract as GenericObject).get("_field_order", null) as GenericObject;
      if( FO != null )
      {
        for( int i=0; i<FO.getLength() ; i++)
        {
          object key = FO.get(i);
          if( !(args.has(key)) || (args.has(key) && args.get(key)!= null && c_CASL.c_opt.Equals(args.get(key))))
          {
            if( key is String && (key as String).StartsWith("_"))		continue;
            // shallow copy if type of param is GenericObject
            object arg_expr = (contract as GenericObject).get(key); // parameter's expression value in the contract   need lookup=true
            if (arg_expr != null && c_CASL.c_req.Equals(arg_expr))
              continue;
            CASL_Frame f1 = CASL_Stack.Alloc_Frame(EE_Steps.standard, arg_expr, args);
#if DEBUG
            f1.method_name = frame.method_name;
#endif
            args.set(key, execute_expr(f1));      // only copy if param not required.
          }
        }
      }
    }

    public static void set_args_from_contract(GenericObject args, GenericObject contract)
    {
      // set args by position based on contract
      GenericObject FO = (contract as GenericObject).get("_field_order", null) as GenericObject;
      if( FO != null )
      {
        if( (contract as GenericObject).has("_other_unkeyed") ) //ALLOW_UNKEYED_ARGS
        {
          for( int i=0; i<FO.getLength() ; i++)
          {
            object key = FO.get(i);
            object contract_value = (contract as GenericObject).get(key, c_CASL.c_error,true);
            // TOCHECK: what to do if contract_value is null
            if( !args.has(key) && contract_value!= null && (c_CASL.c_req.Equals(contract_value)|| c_CASL.c_req.Equals(contract_value))) //IS_REQ_AND_NOT_PRESENT
            {
              args.set(key, args.remove(i));
            }
          }
        }
        else //NOT_ALLOW_UNKEYED_ARGS 
        {
          int offset=0;
          for( int i=0; i<FO.getLength() ; i++)
          {
            object key = FO.get(i);
            //	object contract_value = (contract as GenericObject).get(key,c_CASL.c_error,true);
            int j = i - offset;
            if( args.has(j) ) // HAS_POSITION_VALUE, set args_name by position
            {
              args.set(key, args.remove(j) );
              offset++;
            }
          }
        }
      }
    }

    public static GenericObject evaluate_args_from_call_expr(
        CASL_Frame frame, GenericObject call_expr, GenericObject outer_env)
    {
      GenericObject args= c_CASL.c_local_env();
      add_filtered_args(frame, call_expr, args, outer_env);//handle special _args
      // evaluate args based on source code order - __parsed_field_order in object
      GenericObject parsed_field_order = call_expr.get("__parsed_field_order") as GenericObject;
      CASL_Frame f1 = null;
      for(int i=0; i< parsed_field_order.getLength();i++)
      {
        object key = parsed_field_order.get(i);
        if("_parent".Equals(key) || "_args".Equals(key)) continue;
        object arg_expr = call_expr.get(key);
        f1 = CASL_Stack.Alloc_Frame(EE_Steps.standard, arg_expr,outer_env);
#if DEBUG
        f1.method_name = frame.method_name;
#endif
        object arg_value = execute_expr(f1);
        args.set(key,arg_value);  // bind local variable to the arg value
      }
      // evaluate int args
      for( int i=0; i< call_expr.getLength(); i++)
      {
        object arg_expr =  call_expr.get(i);
        f1 = CASL_Stack.Alloc_Frame(EE_Steps.standard, arg_expr, outer_env); 
#if DEBUG
        f1.method_name = frame.method_name;
#endif
        object arg_value = execute_expr(f1);
        args.set(i,arg_value);  // bind local variable to the arg value
      }
      return args;
    }

    
    public static GenericObject evaluate_args_from_call_expr(
        CASL_Frame frame, GenericObject call_expr, GenericObject contract, GenericObject outer_env)
    {
      if( contract == null) {
#if CS_OBJECTS
        Debugger.Break();
#endif
        return evaluate_args_from_call_expr(frame, call_expr,outer_env);
      }
      GenericObject args= c_CASL.c_GEO();
      add_filtered_args(frame, call_expr, args, outer_env);//handle special _args
      // evaluate args based on source code order - __parsed_field_order in object
      GenericObject parsed_field_order = call_expr.get("__parsed_field_order") as GenericObject;
      CASL_Frame f1 = null;
      for(int i=0; i< parsed_field_order.getLength();i++)
      {
        object key = parsed_field_order.get(i);
        if("_parent".Equals(key) || "_args".Equals(key)) continue;
        //if(key is string && (key as string).StartsWith("_")) continue;
        object value = call_expr.get(key);
        object arg_expr = value;
        object arg_value;
        if( key is int && contract.has("_do_not_execute_vector_args"))
          arg_value = value;
        else if( key is string && contract.has("_do_not_execute_string_args"))
          arg_value = value;
        else if ( "__parsed_field_order".Equals(key) && !(contract as GenericObject).has("_def"))
          continue;
        else {    // recursively execute the arg expression to return a value
          f1 = CASL_Stack.Alloc_Frame(EE_Steps.standard, arg_expr,outer_env); 
#if DEBUG
          f1.method_name = frame.method_name;
#endif
          arg_value= execute_expr(f1);
        }
        args.set(key,arg_value);  // bind local variable to the arg value
      }
      // evaluate int args
      for( int i=0; i< call_expr.getLength(); i++)
      {
        object value = call_expr.get(i);
        object arg_expr = value;
        object arg_value;
        if( contract.has("_do_not_execute_vector_args"))
          arg_value = value;
        else {    // recursively execute the arg expression to return a value
          f1 = CASL_Stack.Alloc_Frame(EE_Steps.standard, arg_expr, outer_env);
#if DEBUG
          f1.method_name = frame.method_name;
#endif
          arg_value= execute_expr(f1);
        }
        args.set(i,arg_value);  // bind local variable to the arg value
      }
      // handle __parsed_field_order and {__length}
      if( contract.has("_def"))
        args.set("__parsed_field_order",call_expr.get("__parsed_field_order"));  // bind local variable to the arg value
      if( contract.has("_save_expr"))
        args.set("__expr",call_expr);  // bind local variable to the arg value
      return args;
    }
    //
    public static void add_filtered_args(
        CASL_Frame frame, GenericObject call_expr, GenericObject inner_env, GenericObject outer_env)
    {
      if ( (call_expr as GenericObject).has("_args") )
      {
        GenericObject args = CASLInterpreter.execute_expr(
            CASL_Stack.Alloc_Frame(EE_Steps.standard,
                           (call_expr as GenericObject).get("_args"),outer_env))
                as GenericObject;
   
        if(args != null) 
        {
          ArrayList keys = args.getKeys();
          foreach(object key in keys)
          {
            if("_outer_env".Equals(key)||"_local".Equals(key)||"it".Equals(key)||"_parent".Equals(key))   // MP: can remove "it" check
              continue;
            inner_env.set(key, args.get(key)); // no need to execute value
          }
        }
        else
        {
          //TODO: throw error
        }
        // DO NOT REMOVE ANY THING FROM ORIGINAL EXPRESSION
        //(call_expr as GenericObject).remove("_args");
      }
    }
    public static object execute_path (
        CASL_Frame frame, GenObj_Path path_expr, GenericObject local_env)
    {
      Object PP = null; //path part
      Object path_it = null;

      for(int i=0; i<path_expr.getLength(); i++)
      {
        PP = path_expr.get(i);
        if(PP is GenObj_Symbol PP_sym) // IS_SYMBOL 
        {
          if( i!= 0 )
          {
            string s_message = string.Format("Symbol found within path {2}",misc.to_eng(PP), misc.to_eng(path_it), misc.to_eng(path_expr));
            throw CASL_error.create("message",s_message, "key",PP,  "a_expr",path_expr);
          }
          else
          {
            // lookup 

            object temp_path_it = local_env.get(
                   Tool.symbol_to_string(PP as GenObj_Symbol),
                   c_CASL.c_undefined, true);
            if( temp_path_it != null && c_CASL.c_undefined.Equals(temp_path_it))
            {
#if DEBUG_GenObj
              Logger.cs_log("execute_path #1 " + PP_sym.name + " not found in " +
                            Tool.path_to_string(path_expr), Logger.cs_flag.error);
#endif
              // Bug 27090 NAM: log file and line of CASL code causing the error 
              string s_message = string.Format("{0} not found in {1} within path {2} CASL_source {3}",
                                                misc.to_eng(PP),
                                                misc.to_eng(path_it), 
                                                misc.to_eng(path_expr), 
                                                path_expr.loc.ToString() != null ? path_expr.loc.ToString() : string.Empty);
              throw CASL_error.create("message",s_message, "subject",path_it,
                                   "key",PP,  "a_expr",path_expr);
            }
            path_it = temp_path_it;
          }
        }
        else if(PP is string || PP is int) // IS_STRING_INT
        {
          // set first path part as string or number
          if(i==0) // NOT_LOOK_UP_FOR_FIRST_INTERGER_STRING
          {
            path_it = PP; 
          }
          else // LOOK_UP_FOR_STRING_INTEGER
          {
            GenericObject env = misc.convert_primitive_to_casl_object(path_it);
            
            object temp_path_it = env.get(PP, c_CASL.c_undefined, true);
            if (temp_path_it != null && c_CASL.c_undefined.Equals(temp_path_it))
            {

              if ((path_it is GenericObject) &&
                   ((path_it as GenericObject).has("_if_missing_method")))
              {
                GenericObject a_method = (path_it as GenericObject).get("_if_missing_method") as GenericObject;
                return misc.CASL_call("a_method", a_method, "args", new GenericObject("_subject", path_it, "key", PP));
              }
#if DEBUG_GenObj
              Logger.cs_log("execute_path #2 " + PP.ToString() + " not found in " +
                            Tool.path_to_string(path_expr), Logger.cs_flag.error);
#endif              
              // Bug 27090 NAM: log file and line of CASL code causing the error 
              string s_message = string.Format("{0} not found in {1} within path {2} CASL_source {3}",
                                               misc.to_eng(PP),
                                               misc.ToPath(path_it, c_CASL.GEO),
                                               misc.to_eng(path_expr),
                                               path_expr.loc.ToString() != null ? path_expr.loc.ToString() : string.Empty);
              throw CASL_error.create("message",s_message, "subject",path_it,
                                   "key",PP,  "a_expr",path_expr);
            }
            path_it = temp_path_it;
          }
        }
        else if(PP is decimal || PP is bool || PP is byte[] || PP == null ) // IS_PRIMITIVE_VALUE
        {
          path_it= PP;
        }
        else // IS_CASL_EXPR
        {
          object call_name = (PP as GenericObject).get("_parent", null);
          object a_method = null;
          GenericObject g_call_name = call_name as GenericObject;
          if(g_call_name.kind == GO_kind.symbol)
          {
            if( i== 0 ) {
              a_method = local_env.get(
                  Tool.symbol_to_string(g_call_name as GenObj_Symbol), null, true);
            } else {
              GenericObject env = misc.convert_primitive_to_casl_object(path_it);
              a_method = env.get(
                  Tool.symbol_to_string(g_call_name as GenObj_Symbol),
                                 null, true);
            }
          }
          else if( call_name is GenObj_Path)
          {
            path_it = misc.convert_primitive_to_casl_object(path_it);
            a_method = execute_path(frame, call_name as GenObj_Path,path_it as GenericObject);
            if (a_method is CASL_Frame)
              a_method = execute_expr(a_method as CASL_Frame);
          }
          else
          {
            string s_message = string.Format("invalid call_name. ");
            throw CASL_error.create("message",s_message);
          }
          //---------------------------------------
          // handle call_name as path e.g. foo.<bar.kar/>, 
          // handle call_name as symbol e.g. 5.<bar/> except 5.<bar.kar/>
          // TODO: 
          // ?handle call_name as first element <foo.bar/>.<kar/>
          //---------------------------------------
          if( a_method == null ) // throw error if a_method is not found
          {
            string s_message = string.Format("execute path#3: Method {0} not found in {1}",misc.to_eng(call_name),misc.to_eng(path_it));
            throw CASL_error.create("message",s_message, "subject",path_it, "key",call_name);
          }
          // just move try/catch from execute_cs_method
          // BUG: if local_env is used as default subject, do not use it.
          CASL_Frame f = CASL_Stack.Alloc_Frame(
                             EE_Steps.evaluate_args, a_method, local_env);
#if DEBUG
          if (! (PP is GenObj_Code)) {
            Logger.cs_log("execute_expr got GenericObject but was looking" +
                          " for GenObj_Code " + PP.ToString(),
                          Logger.cs_flag.trace);
          } else
            f.current_loc = (PP as GenObj_Code).loc;
#endif

          f.call_expr = PP as GenericObject;
          f.a_method = a_method as GenericObject;
          f.subject = no_subject;
          f.outer_env = local_env;
#if DEBUG
          f.method_name = Tool.symbol_or_path_to_string(g_call_name);
#endif
          if ( i > 0)
            f.subject = path_it; 
            
          //
          // IMPORTANT: critical to avoiding StackOverflow issues.
          //
          // When the last element of a path is a method call we
          // wrap the call up into a CASL_Frame (a CASL continuation)
          // and return it to the calling execute_expr() for evaluation.
          //
          // This is a "tail recursion optimization", it eliminates
          // nested C# execute_path() frames which reduces the depth
          // of the C# runtime stack.
          //

          if (i == path_expr.getLength() - 1)
            return f;
          
          path_it = execute_expr(f);
        }
      }
      return path_it;
    }

    // Called by: 
    public static GenericObject get_my ()
    {
      if(System.Web.HttpContext.Current!=null && System.Web.HttpContext.Current.Session != null)
        return System.Web.HttpContext.Current.Session["my"] as GenericObject;
      else if (CASLInterpreter.no_session_my != null) // Bug 24902 MJO - If set, use this "my" object
        return CASLInterpreter.no_session_my.Value;
      else  // called from ActiveX or stand-alone test
        return c_CASL.GEO.get("my",null) as GenericObject;
    }

    /// <summary>
    /// evalue zero or more expression in order, 
    /// and each inside expression share the one environment belong to expressions.
    /// </summary>
    /// <param name="a_exprs"></param>
    /// <param name="exprs_env"></param>
    /// <returns></returns>
    // Called by: multiple places in misc.cs and CASLInterpreter.cs

    public static object execute_exprs (
	// CASL_Frame frame,
	GenericObject a_exprs, GenericObject env, bool top_level)
    {
      object it = null;
      int i = 0;
      int L=a_exprs.getLength();
      CASL_Frame frame = CASL_Stack.stack_top;

      while( i < L )
      {
        it = CASLInterpreter.execute_expr(
            CASL_Stack.Alloc_Frame(EE_Steps.standard, a_exprs.get(i), env));
        CASL_Stack.pop_to(frame);

        if ( ! "init".Equals(a_exprs.get("_name","")) && it != null 
          && it is GenericObject 
          && (it as GenericObject).get("_parent",null) == c_CASL.c_return ) 
        {
          if (top_level)
            return ((it as GenericObject).get("value",null));
          else
            return it;
        }
        i++;
      }
      return it;
    }
    /// <summary>
    /// call static member in C# class
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    // Called by: ??? 
    public static object eval_cs_member (GenericObject a_path)  // MP OPT
    {
      return get_cs_method_from_call_name(a_path);

      //			// get path from casl method to include C# class path(namespace) and instances
      //			String s_path = call_env.get("_parent").ToString();
      //			String s_container_path = s_path.Substring(s_path.IndexOf(".")+1,s_path.LastIndexOf(".")-3);
      //			String s_key = s_path.Substring(s_path.LastIndexOf(".")+1);
      //			// call the C# method by C# class and C# instances
      //			CodeRuntime run_time = new CodeRuntime();
      //			String s_type = String.Format("CASL_engine.{0}, CASL_engine" , s_container_path);
      //			Type class_type = Type.GetType(s_type);
      //			MethodInfo a_method = class_type.GetMethod(s_key);
      //			ParameterInfo[] param_info = a_method.GetParameters();
      //			if (param_info.GetLength(0) == 1)
      //			{
      //				// call funtion with one param GenericObject - GenericObject args
      //				obj_return = CASLParser.CallMember(class_type, s_key, call_env);
      //			}   
      //return obj_return;
    }

    // call_name is path: cs.namespace.[multi namespace].classname.methodname
    //, such as cs.CASL_engine.CASLInterpreter.CASL_if
    // Called by: eval_cs_member, execute_cs_method
    public static MethodInfo get_cs_method_from_call_name (Object call_name)  // MP OPT
    {
      // TODO: Handle namespace and instance method
      if( !Tool.is_symbol_or_path(call_name) )
      {
        string s_message = string.Format("Tagname (or callname) must be a name or a path, but is {0}",misc.to_eng(call_name));
        throw CASL_error.create("message",s_message);
      }
      // get class type from 2nd part of path
      GenericObject g_call_name = call_name as GenericObject;
      int count = g_call_name.getLength();
      if(count < 4) 
      {
        return null;
      }
      //-------------------------------------------------
      // get method name as last one
      //-------------------------------------------------
      string class_name = g_call_name.get(count-2).ToString();
      string method_name = g_call_name.get(count-1).ToString();
      string name_space = "";
      for(int i=1; i<= count-3 ; i++)
      {
        if (i==1)
          name_space = g_call_name.get(i).ToString();
        else
          name_space += "." + g_call_name.get(i).ToString();
      }
      
      Type class_type = misc.get_type(name_space, class_name);

      // Either couldn't load the assembly or something similar
      if ( class_type == null )
      {
        string s_message = string.Format("C# class {0} not found.",misc.to_eng(call_name));
        throw CASL_error.create("message",s_message);
      }

      MethodInfo Method = class_type.GetMethod(method_name);		
      if( Method == null )
      {
        string s_message = string.Format("get_cs_method_from_call_name: " +
           "C# method {0} not found in {1}.",misc.to_eng(call_name),
           class_type.ToString());
        throw CASL_error.create("message",s_message, "subject",class_type.ToString(), "key", call_name.ToString());
      }
          
      return Method;
			
      //			string class_name = (call_name as GenericObject).get(1).ToString();
      //			String class_type_name = String.Format("CASL_engine.{0}, CASL_engine" , class_name);
      //			Type class_type = Type.GetType(class_type_name);
      //			// get method from class type from 3rd part of path
      //			string method_name = (call_name as GenericObject).get(2).ToString();
      //			MethodInfo Method = class_type.GetMethod(method_name);		
      //			return Method;
    }



    //***********************************************************************
    // CASL Interpreter Helper functions
    //***********************************************************************
    // Bug 20435 UMN PCI-DSS Removed unused to_path_with_sessionid

    public static Object fCASL_get_args(CASL_Frame frame)
    {
      GenericObject args = frame.args;
      GenericObject outer_env = args.get("outer_env") as GenericObject;
      if (outer_env == c_CASL.c_opt)
        outer_env = frame.outer_env;
      GenericObject contract = args.get("contract") as GenericObject;
      if (contract == c_CASL.c_opt)
        contract = outer_env.get("_this_method") as GenericObject;
             // frame.calling_frame.a_method??
      bool include_opt = (bool) args.get("include_opt", false);

      GenericObject R = null;
      GenericObject field_order = contract.get("_field_order", null)
        as GenericObject;
#if DEBUG_get_args
      if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
        Flt_Rec.log("get_args: before R assignment: ", include_opt.ToString() +
                    field_order.ToString());
#endif
      if (include_opt)
        R = misc.Copy(outer_env, R, field_order, null);
      else {
        R = new GenericObject();
        foreach (object k in field_order.vectors) {
          GenericObject v = outer_env.get(k, c_CASL.c_opt)
            as GenericObject;
          if (v != c_CASL.c_opt)
            R.set(k, v);
        }
      }

#if DEBUG_get_args
      if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
        Flt_Rec.log("get_args: after _field_order: ", R.ToString());
#endif

      if (contract.has("_other_unkeyed")) {
        for (int i = 0; i < outer_env.getLength(); i++)
          R.set(i, outer_env.get(i));
      }

#if DEBUG_get_args
      if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
        Flt_Rec.log("get_args: after _other_unkeyed: ", R.ToString());
#endif

      if (contract.has("_other_keyed")) {
        HashSet<object> hs = new HashSet<object>();
        foreach (object x in field_order.vectors)
          hs.Add(x);
        foreach (object key in outer_env.getDictKeys()) {
          if ("_parent".Equals(key))
            continue;

          if ("_outer_env".Equals(key))
            continue;
          if ("_local".Equals(key))
            continue;
          if ("_this_method".Equals(key))
            continue;
          if ("it".Equals(key))
            continue;
          if (hs.Contains(key))
            continue;
          R.set(key, outer_env.get(key));
        }
      }

      if (outer_env.has("_subject"))
        R.set("_subject", outer_env.get("_subject"));

#if DEBUG_get_args
      if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
        Flt_Rec.log("get_args: the result: ", R.ToString());
#endif
      return R;
    }


    // TTS#26349 GMB
    //
    // Call this when we are done loading the interpreter and (sadly) config.
    //
    public static object fCASL_validate_interpreter_load(CASL_Frame frame)
    {
#if DEBUG_GenObj_Lock
      load_complete = true;
      HashSet<GenericObject> seen_GenObjs = new HashSet<GenericObject>();

      foreach (GenObj_Class cl in ClassDictionary.Values) {
        GenericObject.Lock_GenObj_Code(seen_GenObjs, cl);
      }
#if PERFORMANCE_GRAPH
      foreach (GenObj_Method md in MethodDictionary.Values) {
        // Recursive setting of locked_as
        md.locked_as = Lock_Kind.code;
      }
#endif

      // Check methods for classes
      // Check class hierarchy
#endif
      return null;
    }

        }

        public class CASL_error: ApplicationException
  {
    public GenericObject casl_object = null; // casl error object

#if DEBUG
    // When this is set to true we break just before completing
    // building a CASL_Error.  Useful because the C# stack is intact.
    // Capturing the throw (turn on Common Language Runtime Exceptions)
    // sometimes comes to late for local variables.

    public static bool catch_CASL_Error = false;

#endif

    private CASL_error(string message, Exception inner, GenericObject co)
      : base(message, inner)
    {
      casl_object = co;

      if (c_CASL.get_show_verbose_error() &&
          (!casl_object.has("casl_stack")))
        casl_object.set("casl_stack",
                    CASL_Stack.stacktrace_StrBld(null, 7).ToString());

#if DEBUG
      if (c_CASL.get_debug() && ! casl_object.has("cs_stack_string"))
        casl_object.set("cs_stack_string",  Environment.StackTrace);
#endif

      if (Flt_Rec.go(Flt_Rec.where_t.error_flag))
        Flt_Rec.log("CASL_error", casl_object.ToString()); 

#if DEBUG
      if (catch_CASL_Error)
        Debugger.Break();
#endif
    }
    
    public static CASL_error create_str(string message)
    {
      // Bug #13574 Mike O - Initialize casl_object so CASL can deal with this
      GenObj_Error casl_object = new GenObj_Error(message);
      return new CASL_error(message, null, casl_object);
    }

    public static CASL_error create_obj(string message, GenericObject casl_obj)
    {
      Exception inner = null;
      if (casl_obj.has("inner"))
        inner = casl_obj.get("inner") as Exception;

      if (casl_obj.has("message")) {
#if DEBUG
        if ( ! message.Equals(casl_obj.get("message")))
          Logger.cs_log("Message parameter and casl_obj.get(\"message\") not equal");
#endif
      } else {
        casl_obj.set("message", message);
      }
      return new CASL_error(message, inner, casl_obj);
    }

    /// <summary>
    /// eg. throw CASL_error.create("message",s_message, "subject",path_it, "key",call_name);
    /// </summary>
    /// <param name="args"></param>
    public static CASL_error create(params object[] args)
    {
      GenericObject casl_object = null;
      if (args.Length == 1 && args[0] is GenericObject) {
        Logger.cs_log("CASL_error.create() single paramter!! use create_obj()",
                        Logger.cs_flag.error);
        casl_object = args[0] as GenericObject;
      } else {
        casl_object = new GenObj_Error();
        casl_object.set(args);
      }

      casl_object.set("_parent",c_CASL.c_object("error"), "throw",false);

      string message = casl_object.get("message", "") as String;
      Exception inner = casl_object.get("inner", null) as Exception;
      CASL_Frame frame = CASL_Stack.stack_top;
      string this_method = "no_method";
      // frame == null only if there is no session yet.  GMB.
      if (frame != null && frame.a_method != null) {
        if (frame.a_method is GenObj_Method)
          this_method = (frame.a_method as GenObj_Method).full_name;
        else
          this_method = misc.ToPath(frame.a_method, c_CASL.GEO).ToString();
      }

      string this_cont = "no_container";
      if (frame.args != null)
        this_cont = frame.args.get("_container", this_cont).ToString();

      code_location cl = code_location.no_location;
      CASL_Frame fr = frame;
      while (fr != null && (! cl.isValid())) {
        cl = fr.current_loc;
        if (! cl.isValid())
          cl = fr.loc;
        fr = fr.calling_frame;
      }

      message = String.Format("{0} near {1} in {2} - {3}.", message,
                              cl.ToString(),
                              this_method, this_cont);


      if ( !casl_object.has("title"))
        casl_object.set("title","Runtime error");

#if DEBUG
      if (catch_CASL_Error)
        Debugger.Break();
#endif

      return new CASL_error(message, inner, casl_object);
    }

    public static CASL_error create_inner(string message, Exception inner)
    {
      // Bug #13574 Mike O - Initialize casl_object so CASL can deal with this
      GenObj_Error casl_object = new GenObj_Error(message);

      casl_object.set("inner", inner);

      return new CASL_error(message, inner, casl_object);
    }
  }
}

