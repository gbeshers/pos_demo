using System;
using System.Reflection;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Xml;
using System.Text;
using System.Windows.Forms;

namespace CASL_engine
{
  /// <summary>
  /// Summary description for CASHParser.
  /// </summary>
  public class CASLParser
  {

    //---------------------------------------------------
    // Use by encoder and decoder
    public const String GENERICNAME = "GenericObject";

    //---------------------------------------------------
    // ConciseXML Encoding Character Definition
    public const String CONCISEXML_FIELD_CONNECTOR = "\n";
    public const String CONCISEXML_STRING_CHAR = "\"";
    public const String CONCISEXML_INT = "int";
    public const String CONCISEXML_STRING = "string";
    //---------------------------------------------------

    //---------------------------------------------------
    // ConciseURI Encoding Character Definition
    public const String CONCISEURI_FIELD_CONNECTOR = "&";
    public const String CONCISEURI_STRING_CHAR = "";
    public const String EQUAL = "=";
    //---------------------------------------------------

    /// <summary>
    /// Initializes a new instance of the CASLParser class
    /// </summary>
    public CASLParser()
    {

    }



    /// <summary>
    /// convert xml with conciseXML encoding to a generic object
    /// </summary>
    /// <param name="conciseXML"></param>
    /// <param name="xmlFileName"></param>
    /// <returns> 
    /// o If conciseXML is not contain xml. It returns <GenericObject> _parent="string" _content=conciseXML</GenericObject>
    /// o The field elements in the return GenericObject will be flatten into metafield. 
    /// o If error occurs, throw the Exception either from xmlparser or conciseXML encoding error.
    /// o If Return null if both params are ""/null.
    /// </returns>
    public static GenericObject decode_conciseXML(String conciseXML, String xmlFileName)
    {
      GenericObject objReturn;
      CASLParser_ConciseXML P = new CASLParser_ConciseXML();
      try
      {
        objReturn = P.Run(conciseXML, xmlFileName) as GenericObject;
      }
      catch (XmlException xe)
      {
        string the_message = "XML syntax error: " + xe.Message + " in file " + xmlFileName;
        throw CASL_error.create("title", "Syntax Error", "code", 102, "message", the_message, "line_nbr", xe.LineNumber, "line_position", xe.LinePosition, "file_name", xmlFileName);
      }
      return objReturn;
    }

    /// <summary>
    /// convert xml with conciseXML encoding to a generic object
    /// </summary>
    /// <param name="conciseXML"></param>
    /// <param name="xmlFileName"></param>
    /// <returns> 
    /// o If conciseXML is not contain xml. It returns <GenericObject> _parent="string" _content=conciseXML</GenericObject>
    /// o The field elements in the return GenericObject will be flatten into metafield. 
    /// o If error occurs, throw the Exception either from xmlparser or conciseXML encoding error.
    /// o If Return null if both params are ""/null.
    /// </returns>
    public static GenericObject decode_dataXML(String dataXML, String xmlFileName)
    {
      GenericObject objReturn;
      CASLParser_DataXML P = new CASLParser_DataXML();
      try
      {
        objReturn = P.Run(dataXML, xmlFileName) as GenericObject;
      }
      catch (XmlException xe)
      {
        string the_message = "Not valid XML -" + xe.Message;
        throw CASL_error.create("title", "Syntax Error", "code", 102, "message", the_message, "line_nbr", xe.LineNumber, "line_position", xe.LinePosition, "file_name", xmlFileName);
      }
      return objReturn;
    }

    public static String encode_conciseXML(Object objTarget, bool bGeneric)
    {
      String sReturn = "";

      //-------------------------------------------------------
      // Handle null
      if (objTarget == null)
      {
        return "null";
      }

      Type myType = objTarget.GetType();
      //-------------------------------------------------------
      // Handle Keyed_Value Generic Instance like Hashtable or GenericObject
      if (myType == typeof(GenericObject))
      {
        String sStartXMLTag = "<GenericObject>", sEndXMLTag = "</GenericObject>";
        GenericObject objThing = objTarget as GenericObject;
        ArrayList ordKeys = objThing.getKeys();
        sReturn += sStartXMLTag;
        int iIndex = 0;
        foreach (Object objKey in ordKeys)
        {
          String sKey = objKey.ToString() + "=";
          Object objValue = objThing.get(objKey);
          String sValue = encode_conciseXML(objValue, false);
          if (iIndex > 0)
            sReturn += CONCISEXML_FIELD_CONNECTOR + sKey + sValue;
          else
            sReturn += sKey + sValue;
          iIndex++;
        }
        sReturn += "\n" + sEndXMLTag;
      }
      ///////////////////////////////////////////
      // Handle SYSTEM-CONTAINER-TYPE Object
      else if (myType == typeof(System.Collections.Hashtable))
      {
        String sHashStartXMLTag = "", sHashEndXMLTag = "";
        CreateXMLTag(myType.Name, ref sHashStartXMLTag, ref sHashEndXMLTag, bGeneric);
        IDictionaryEnumerator myEnumerator = ((Hashtable)objTarget).GetEnumerator();
        sReturn += sHashStartXMLTag;
        int iHashIndex = 0;
        Object objHashValue;
        String sHashKey;
        while (myEnumerator.MoveNext())
        {
          sHashKey = (String)myEnumerator.Key;
          objHashValue = myEnumerator.Value;
          if (bGeneric || iHashIndex > 1)
            sReturn += CASLParser.CONCISEXML_FIELD_CONNECTOR + sHashKey + EQUAL + encode_conciseXML(objHashValue, bGeneric);
          else
            sReturn += sHashKey + EQUAL + encode_conciseXML(objHashValue, bGeneric);
          iHashIndex++;
        }
        sReturn += "\n" + sHashEndXMLTag;
      }
      else if (myType == typeof(System.Collections.ArrayList))
      {
        String sArrayStartXMLTag = "", sArrayEndXMLTag = "";
        CreateXMLTag(myType.Name, ref sArrayStartXMLTag, ref sArrayEndXMLTag, bGeneric);
        sReturn += sArrayStartXMLTag;
        int iKey = 0;
        foreach (Object obj in (ArrayList)objTarget)
        {
          if (bGeneric || iKey > 0)
            sReturn += CASLParser.CONCISEXML_FIELD_CONNECTOR;
          if (obj.GetType().IsPrimitive || obj.GetType() == typeof(System.String))
          {
            sReturn += String.Format("{0}={1}", iKey, obj.ToString());
          }
          else
          {
            String sNestedObjectInArray = encode_conciseXML(obj, bGeneric);
            sReturn += String.Format("{0}={1}", iKey, sNestedObjectInArray);
          }
          iKey++;
        }
        sReturn += sArrayEndXMLTag;
      }
      //-------------------------------------------------------
      // Handle Primative Instance like String, Int, or Double
      else if (myType.IsPrimitive || myType == typeof(System.Decimal) || myType == typeof(System.String))
      {
        sReturn += String.Format("{0}", objTarget.ToString());
      }
      ///////////////////////////////////////////
      // Handle CORE-TYPE Object
      else
      {
        //-----------------------------------------------------
        // Write Instance Info
        String sStartXMLTag = "", sEndXMLTag = "";
        CreateXMLTag(myType.Name, ref sStartXMLTag, ref sEndXMLTag, bGeneric);
        sReturn += sStartXMLTag;

        // Get the type and fields of FieldInfoClass.
        FieldInfo[] myFieldInfo = myType.GetFields(BindingFlags.Instance
          | BindingFlags.Public);
        // Display the field information of FieldInfoClass.
        int iIndex = 1;
        foreach (FieldInfo fi in myFieldInfo)
        {
          //-----------------------------------------------------
          // Loop through the fields
          Object objField = null;
          if (!CallMember(objTarget, fi.Name, ref objField))
            return "false";

          if (bGeneric || iIndex > 1)
            sReturn += CASLParser.CONCISEXML_FIELD_CONNECTOR + fi.Name + EQUAL + encode_conciseXML(objField, bGeneric);
          else
          {
            sReturn += fi.Name + EQUAL + encode_conciseXML(objField, bGeneric);
          }
          iIndex++;
        }
        sReturn += sEndXMLTag;
      }
      return sReturn;
    }
    /// <summary>
    /// Goal: 
    /// Feature: decode typed c# object to GenericObject
    /// </summary>
    /// <param name="objTarget"></param>
    /// <returns></returns>
    public static GenericObject decode_TypedObject(Object objTarget)
    {
      GenericObject objReturn = null;

      //-------------------------------------------------------
      // Handle null
      if (objTarget == null)
      {
        return objReturn;
      }

      Type myType = objTarget.GetType();

      if (myType.IsPrimitive || myType == typeof(System.String))
      {
        return null;
      }
      else if (myType == typeof(System.Collections.ArrayList))
      {
        ArrayList alTarget = objTarget as ArrayList;
        objReturn = new GenericObject("_parent", myType.Name);
        foreach (Object objValue in alTarget)
        {
          Object objInsertValue = objValue;
          if ((!objValue.GetType().IsPrimitive) && (!(objValue.GetType() == typeof(System.String))) && (!(objValue is GenericObject)))
            objInsertValue = decode_TypedObject(objValue);
          objReturn.insert(objInsertValue);
        }
      }
      else if (myType == typeof(System.Collections.Hashtable))
      {
        Hashtable aTarget = objTarget as Hashtable;
        objReturn = new GenericObject("_parent", myType.Name);
        IDictionaryEnumerator myEnumerator = ((Hashtable)objTarget).GetEnumerator();
        while (myEnumerator.MoveNext())
        {
          Object objInsertValue = myEnumerator.Value;
          if ((!objInsertValue.GetType().IsPrimitive) && (!(objInsertValue.GetType() == typeof(System.String))) && (!(objInsertValue is GenericObject)))
            objInsertValue = decode_TypedObject(objInsertValue);
          objReturn.set(myEnumerator.Key, objInsertValue);
        }
      }
      //-------------------------------------------------------
      // Handle typed C# object
      else if (myType != typeof(GenericObject))
      {
        objReturn = new GenericObject("_parent", myType.Name);
        FieldInfo[] myFieldInfo = myType.GetFields(BindingFlags.NonPublic | BindingFlags.Instance
          | BindingFlags.Public);
        foreach (FieldInfo fi in myFieldInfo)
        {
          //-----------------------------------------------------
          // Loop through the fields
          Object objField = null;
          if (!CallMember(objTarget, fi.Name, ref objField))
            return null;
          if ((!objField.GetType().IsPrimitive) && (!(objField.GetType() == typeof(System.String))) && (!(objField is GenericObject)))
            objField = decode_TypedObject(objField);
          setValueForAllTypes(objReturn, fi.Name, objField);
        }
      }
      else
        objReturn = objTarget as GenericObject;
      return objReturn;
    }
    /// <summary>
    /// Goal: TypedObject encoder 
    /// Feature: encode GenericObject to typed C# Object
    /// </summary>
    /// <param name="objTarget"></param>
    /// <returns></returns>
    public static Object encode_TypedObject(GenericObject objTarget)
    {
      Object objReturn = null;

      //-------------------------------------------------------
      // Handle null
      if (objTarget == null)
      {
        return objReturn;
      }

      Type myType = objTarget.GetType();
      //-------------------------------------------------------
      // Handle Keyed_Value Generic Instance like Hashtable or GenericObject
      if (myType == typeof(GenericObject))
      {
        String sType = objTarget.get("_parent") as string;
        createInstanceForAllTypes(sType, ref objReturn);
        if (sType == "ArrayList")
        {
          for (int i = 0; i < objTarget.getLength(); i++)
          {
            Object objValue = objTarget.get(i);
            if (objValue is GenericObject)
            {
              objValue = encode_TypedObject(objValue as GenericObject);
            }
            setValueForAllTypes(objReturn, "", objValue);
          }
        }
        else
        {
          ArrayList ordKeys = objTarget.getStringKeys();
          foreach (Object objKey in ordKeys)
          {
            Object objValue = objTarget.get(objKey);
            if (objValue is GenericObject)
            {
              objValue = encode_TypedObject(objValue as GenericObject);
            }
            setValueForAllTypes(objReturn, objKey, objValue);
          }
        }
      }
      else
        objReturn = objTarget;
      return objReturn;
    }
    /// <summary>
    /// Goal: ConciseURI encoder 
    /// Feature: encode a C# Object to Concise URI String
    /// C# instance including system_typed object like Hashtable, 
    /// other CORE_typed object
    /// </summary>
    /// <param name="objTarget"></param>
    /// <returns></returns>
    public static String encode_conciseURI(Object objTarget, bool bGeneric)
    {
      String sReturn = "";

      //-------------------------------------------------------
      // Handle null
      if (objTarget == null)
      {
        return sReturn;
      }

      Type myType = objTarget.GetType();
      //-------------------------------------------------------
      // Handle Keyed_Value Generic Instance like Hashtable or GenericObject
      if (myType == typeof(GenericObject))
      {
        sReturn = ((GenericObject)objTarget).toUri();
      }
      ///////////////////////////////////////////
      // Handle SYSTEM-CONTAINER-TYPE Object
      else if (myType == typeof(System.Collections.Hashtable))
      {
        String sHashStartURITag = "", sHashEndURITag = "";
        CreateURITag(myType.Name, ref sHashStartURITag, ref sHashEndURITag, bGeneric);
        IDictionaryEnumerator myEnumerator = ((Hashtable)objTarget).GetEnumerator();
        sReturn += sHashStartURITag;
        int iHashIndex = 0;
        Object objHashValue;
        String sHashKey;
        // Lo
        while (myEnumerator.MoveNext())
        {
          sHashKey = (String)myEnumerator.Key;
          objHashValue = myEnumerator.Value;
          if (bGeneric || iHashIndex > 1)
            sReturn += CASLParser.CONCISEURI_FIELD_CONNECTOR + sHashKey + "=" + encode_conciseURI(objHashValue, bGeneric);
          else
            sReturn += sHashKey + "=" + encode_conciseURI(objHashValue, bGeneric);
          iHashIndex++;
        }
        sReturn += "\n" + sHashEndURITag;
      }
      else if (myType == typeof(System.Collections.ArrayList))
      {
        String sArrayStartURITag = "", sArrayEndURITag = "";
        CreateURITag("array", ref sArrayStartURITag, ref sArrayEndURITag, bGeneric);
        sReturn += sArrayStartURITag;
        int iKey = 0;
        foreach (Object obj in (ArrayList)objTarget)
        {
          if (bGeneric || iKey > 0)
            sReturn += CASLParser.CONCISEURI_FIELD_CONNECTOR;
          if (obj.GetType().IsPrimitive || obj.GetType() == typeof(System.String))
          {
            sReturn += String.Format("{0}", obj.ToString());
          }
          else
          {
            String sNestedObjectInArray = encode_conciseURI(obj, bGeneric);
            sReturn += String.Format("{0}", sNestedObjectInArray);
          }
          iKey++;
        }
        sReturn += sArrayEndURITag;
      }
      //-------------------------------------------------------
      // Handle Primative Instance like String, Int, or Double
      else if (myType.IsPrimitive || myType == typeof(System.String))
      {
        sReturn += String.Format("{0}", objTarget.ToString());
      }
      ///////////////////////////////////////////
      // Handle CORE-TYPE Object
      else
      {
        //-----------------------------------------------------
        // Write Instance Info
        String sStartURITag = "", sEndURITag = "";
        CreateURITag(myType.Name, ref sStartURITag, ref sEndURITag, bGeneric);
        sReturn += sStartURITag;

        // Get the type and fields of FieldInfoClass.
        FieldInfo[] myFieldInfo = myType.GetFields(BindingFlags.Instance
          | BindingFlags.Public);
        // Display the field information of FieldInfoClass.
        int iIndex = 1;
        foreach (FieldInfo fi in myFieldInfo)
        {
          //-----------------------------------------------------
          // Loop through the fields
          Object objField = null;
          if (!CallMember(objTarget, fi.Name, ref objField))
            return "false";

          if (bGeneric || iIndex > 1)
            sReturn += CASLParser.CONCISEURI_FIELD_CONNECTOR + fi.Name + "=" + encode_conciseURI(objField, bGeneric);
          else
          {
            sReturn += fi.Name + "=" + encode_conciseURI(objField, bGeneric);
          }
          iIndex++;
        }
        sReturn += sEndURITag;
      }
      return sReturn;
    }

    /// <summary>
    /// Goal: ConciseURI decoder 
    /// Feature: decode ConciseURI String to a C# Object
    /// </summary>
    /// <param name="conciseURI"></param>
    /// <param name="bGeneric"></param> // not support bGeneric=false at the moment
    /// <returns></returns>
    public static Object decode_conciseURI(String conciseURI, bool bGeneric)
    {
      CASLParser_ConciseURI cp_uri = new CASLParser_ConciseURI(conciseURI);
      cp_uri.Run();
      return cp_uri.nested_objects[0];
    }
    /// <summary>
    /// Feature: Create XML Tag
    /// </summary>
    /// <param name="sTagName"></param>
    /// <param name="sStartTag"></param>
    /// <param name="sEndTag"></param>
    public static void CreateXMLTag(String sTagName, ref String sStartTag, ref String sEndTag, bool bGeneric)
    {
      if (bGeneric)
      {
        sStartTag = String.Format("<{0}>_parent={1}", CASLParser.GENERICNAME, sTagName);
        sEndTag = String.Format("</{0}>", CASLParser.GENERICNAME);
      }
      else
      {
        sStartTag = String.Format("<{0}>", sTagName);
        sEndTag = String.Format("</{0}>", sTagName);
      }
    }
    /// <summary>
    /// Feature: Create XML Tag
    /// </summary>
    /// <param name="sTagName"></param>
    /// <param name="sStartTag"></param>
    /// <param name="sEndTag"></param>
    public static void CreateURITag(String sTagName, ref String sStartTag, ref String sEndTag, bool bGeneric)
    {
      if (bGeneric)
        sStartTag = String.Format("({0}~_parent={1}", CASLParser.GENERICNAME, sTagName);
      else if (sTagName == "array")
        sStartTag = String.Format("(");
      else if (sTagName.Length == 0)
        sStartTag = String.Format("(");
      else
        sStartTag = String.Format("({0}~", sTagName);
      sEndTag = String.Format(")");
    }

    /// <summary>
    /// Set key_value pair for any typed an untyped instance
    /// </summary>
    /// <param name="objClassInstance"></param>
    /// <param name="objKey"></param>
    /// <param name="objValue"></param>
    public static void setValueForAllTypes(Object objClassInstance, Object objKey, Object objValue)
    {
      ///////////////////////////////////////////
      // Handle SYSTEM-CONTAINER-TYPE Object
      if (objClassInstance is ArrayList)
      {
        ArrayList objAL = objClassInstance as ArrayList;
        objAL.Add(objValue);
      }
      else if (objClassInstance is Hashtable)
      {
        Hashtable objHt = objClassInstance as Hashtable;
        objHt.Add(objKey, objValue);
      }
      ///////////////////////////////////////////
      // Handle CORE-TYPE Object
      else if (!(objClassInstance is GenericObject))
      {
        // Cast the value to the Field type 
        Type classType = objClassInstance.GetType();
        String FieldName = ((String)objKey).Trim();
        Type fieldType = (classType.GetField(FieldName)).FieldType;
        // Bug #12055 Mike O - Use misc.Parse to log errors
        if (fieldType == typeof(System.Double) && (objValue.GetType() != typeof(System.Double)))
          objValue = misc.Parse<Double>((String)objValue);
        else if (fieldType == typeof(System.Int32) && (objValue.GetType() != typeof(System.Int32)))
          objValue = misc.Parse<Int32>((String)objValue);
        else if (fieldType == typeof(System.Boolean) && (objValue.GetType() != typeof(System.Boolean)))
        {
          if ("true".Equals(objValue.ToString().ToLower()))
            objValue = true;
          else
            objValue = false;
        }
        SetMember(objClassInstance, ((String)objKey).Trim(), objValue);
      }
      ///////////////////////////////////////////
      // Handle GENERIC-TYPE Object
      else
      {
        GenericObject TH = objClassInstance as GenericObject;
        TH.set(objKey, objValue);
      }
      return;
    }

    /// <summary>
    /// Initializes a new instance of any typed an untyped class
    /// </summary>
    /// <param name="sClassName"></param>
    /// <param name="objClassInstance"></param>
    /// <returns></returns>
    public static bool createInstanceForAllTypes(String sClassName, ref Object objClassInstance)
    {
      String classType = "";
      String sTemp = String.Format("CASL_engine.{0}, CASL_engine", sClassName);
      Type aType = Type.GetType(sTemp);
      if (!(aType == null))
        classType = aType.Name;
      ///////////////////////////////////////////
      // Handle SYSTEM-CONTAINER-TYPE Object
      if (sClassName == "ArrayList")
      {
        objClassInstance = new ArrayList();
      }
      else if (sClassName == "Hashtable")
      {
        objClassInstance = new Hashtable();
      }
      ///////////////////////////////////////////
      // Handle CORE-TYPE Object
      else if (classType != "GenericObject" && aType != null)
      {
        CreateInstance(aType, ref objClassInstance);
      }
      ///////////////////////////////////////////
      // Handle GENERIC-TYPE Object
      else
      {
        objClassInstance = new GenericObject();
        setValueForAllTypes(objClassInstance, "_parent", sClassName);
      }
      return true;
    }

    /// <summary>
    /// Goal: Use it to be access CASL hashtable easily
    /// Feature: Get Array of specific Element by its name from a hashtable
    /// </summary>
    /// <param name="hs"></param>
    /// <param name="sElementType"></param>
    /// <param name="alHashtable"></param>
    public static void GetCASLElements(Hashtable hs, String sElementType, ref ArrayList alHashtable)
    {
      //ArrayList alHashtable = new ArrayList();
      Hashtable htTemp;
      IDictionaryEnumerator aEnumerator = hs.GetEnumerator();
      while (aEnumerator.MoveNext())
      {
        if (aEnumerator.Value is Hashtable)
        {
          htTemp = (Hashtable)aEnumerator.Value;
          if (((String)htTemp["_parent"]).Equals(sElementType))
          {
            alHashtable.Add(htTemp);
          }
        }
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="hs"></param>
    /// <param name="sElementType"></param>
    /// <returns></returns>
    public static bool CheckElementType(Hashtable hs, String sElementType)
    {
      return ((String)hs["_parent"]).Equals(sElementType);
    }

    //**********************************************************************
    // Reflection Runtime Function
    //**********************************************************************

    /// <summary>
    /// 
    /// </summary>
    /// <param name="objClassInstance"></param>
    /// <param name="strFieldName"></param>
    /// <param name="objReturn"></param>
    /// <returns></returns>
    public static bool CallMember(Object objClassInstance, string strFieldName, ref Object objReturn)
    {
      // Just a helper method to call functions
      Type type = objClassInstance.GetType();
      objReturn = type.InvokeMember(strFieldName,
        BindingFlags.DeclaredOnly |
        BindingFlags.Public |
        BindingFlags.NonPublic |
        BindingFlags.Instance |
        BindingFlags.GetField,
        null,
        objClassInstance,
        null);
      return true;
    }

    public static bool SetMember(Object objClassInstance, string strFieldName, Object objValue)
    {
      Type type = objClassInstance.GetType();
      type.InvokeMember(strFieldName,
        BindingFlags.DeclaredOnly |
        BindingFlags.Public | BindingFlags.NonPublic |
        BindingFlags.Instance | BindingFlags.SetField,
        null,
        objClassInstance,
        new Object[] { objValue });
      return true;
    }
    public static bool CreateInstance(Type classType, ref Object objReturn)
    {
      // Just a helper method to create an instance of class
      //Type type = Type.GetType(sClassName);    
      objReturn = classType.InvokeMember(null,
        BindingFlags.DeclaredOnly |
        BindingFlags.Public | BindingFlags.NonPublic |
        BindingFlags.Instance | BindingFlags.CreateInstance,
        null,
        null,
        null);
      return true;
    }

    //		//**********************************************************************
    //		// Sample code Functions
    //		//**********************************************************************
    //
    //		/// <summary>
    //		/// A sample code to Use the Generic Object named "GenericObject" 
    //		/// </summary>
    //		private void UseThing ()
    //		{
    //			//-------------------------------------------------------------
    //			// Use Constructor
    //            GenericObject aThing = new GenericObject();
    //			aThing = new GenericObject("a","5"); 
    //			aThing = new GenericObject("a","stuff");
    //			aThing = new GenericObject("a",5, "b","foo"); 
    //			aThing = new GenericObject("a",new GenericObject("x",5)); 
    //			aThing = new GenericObject(null,new GenericObject("x",5), null, null);
    //			aThing = new GenericObject(null,new GenericObject(null,"x", null,2, null,new GenericObject())); 
    //			aThing = new GenericObject("_parent","boat",  "color","red",  "owner",new GenericObject("_parent","person", null,"Bob")); 
    //
    //			//-------------------------------------------------------------
    //			//Get Value
    //			aThing.get("a");
    //		}
    //		/// <summary>
    //		/// A sample code to Use CASL Hashtable 
    //		/// </summary>
    //		private void UseCASLHashtable ()
    //		{
    //			Hashtable htCASL = new Hashtable();
    //			String CASLFileName = "D:\\DOTNETSPACE\\docs\\CASL_v1.xml";
    //			XmlTextReader aXmlTextReader;
    //			try 
    //			{
    //				aXmlTextReader = new XmlTextReader( CASLFileName);
    //				aXmlTextReader.WhitespaceHandling = WhitespaceHandling.None;
    //			}
    //			catch(Exception e)
    //			{
    //				MessageBox.Show( "Exception Occured: " +e.Message, "Exception Occured",
    //					MessageBoxButtons.YesNo, MessageBoxIcon.Question );
    //				return;
    //			}
    //
    //			//-------------------------------------------------------------
    //			//Parse CASL XML file to a nested Hashtable 
    //			//-------------------------------------------------------------
    //			CASLParser aCASLParser = new CASLParser();;
    //			if ( aXmlTextReader.Read() )
    //			{	
    //				//The first note contains a standard description (is not dynamically created from the XML document).
    //				htCASL.Add("_parent", aXmlTextReader.Name);
    //			}
    //			///////////////////////////////////////////////////////////////
    //			//Get Class-Level information
    //			///////////////////////////////////////////////////////////////
    //			//Get Defining Class
    //
    //			Hashtable htClass = (Hashtable)htCASL["Transaction"];
    //			//Get Class Name
    //			String sClassName = (String)htClass["name"];
    //			Console.WriteLine("-Class(Transaction): name={0}-\r\n", sClassName );
    //
    //			///////////////////////////////////////////////////////////////
    //			//Get Class's Field-Level information
    //			///////////////////////////////////////////////////////////////
    //			//Get Defining Fields of a Class in Array
    //			ArrayList alFields = new ArrayList();
    //			CASLParser.GetCASLElements(htClass, "field", ref alFields);
    //			foreach (Object aField in alFields)
    //			{
    //				Hashtable htField = (Hashtable)aField;
    //				//Get Field Name adn Type
    //				String sFieldName = (String)htField["name"];
    //				String sFieldType = (String)htField["type"];
    //				Console.WriteLine("-Field: name={0};type={1}-", sFieldName , sFieldType);
    //			}
    //			//Get individual Field's name and type by name
    //			Hashtable htOneField = (Hashtable)htClass["ID"];
    //			String sOneFieldName = (String)htOneField["name"];
    //			String sOneFieldType = (String)htOneField["type"];
    //			Console.WriteLine("-Field(ID): name={0};type={1}-", sOneFieldName , sOneFieldType);
    //
    //			///////////////////////////////////////////////////////////////
    //			//Get Class's Status-Level information
    //			///////////////////////////////////////////////////////////////
    //			//Get Defining Statuses of a Class in Array
    //			ArrayList alStatuses = new ArrayList();
    //			CASLParser.GetCASLElements(htClass, "status", ref alStatuses);
    //			//ArrayList alStatuses = GetCASLClassStatuses(htClass, "status");
    //			foreach (Hashtable htStatus in alStatuses) 
    //			{
    //				//Get Status Name
    //				String sSTName = (String)htStatus["name"];
    //				Console.WriteLine("-Status: name={0}-", sSTName );
    //				///////////////////////////////////////////////////////////////
    //				//Get Status's Transition-Level information
    //				///////////////////////////////////////////////////////////////
    //				//Get Defining Transitions of a Status in Array
    //				ArrayList alTransitions = new ArrayList();
    //				CASLParser.GetCASLElements(htStatus, "transition", ref alTransitions);
    //				foreach (Hashtable htTransition in alTransitions) 
    //				{
    //					//Get Transition name, action, precond, ending_status
    //					String sTSName = (String)htTransition["name"];
    //					String sTSAction = (String)htTransition["action"];
    //					String sTSPrecond = (String)htTransition["precond"];
    //					String sTSEnding_status = (String)htTransition["ending_status"];
    //					Console.WriteLine("-Transition: name={0},action={1},precond={2},ending_status={3}-", sTSName , sTSAction, sTSPrecond, sTSEnding_status );
    //				}
    //			}
    //			//Get individual Status and inside transition by name
    //			Hashtable htOneStatus = (Hashtable)htClass["created"];
    //			String sOneSTName = (String)htOneStatus["name"];
    //			Hashtable htOneTransition = (Hashtable)htOneStatus["void"];
    //			String sOneTSName = (String)htOneTransition["name"];
    //			String sOneTSAction = (String)htOneTransition["action"];
    //			String sOneTSPrecond = (String)htOneTransition["precond"];
    //			String sOneTSEnding_status = (String)htOneTransition["ending_status"];
    //			Console.WriteLine("-Status(created): name={0}-", sOneSTName );
    //			Console.WriteLine("-Transition(create): name={0},action={1},precond={2},ending_status={3}-", sOneTSName , sOneTSAction, sOneTSPrecond, sOneTSEnding_status );
    //		}
    //
  }
}


