﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Diagnostics;
using CASL_engine;
using NLog;

//
// Calls to Logger.cs_log() now fall into five categories:
//   - info: note worthy but normal (default)
//   - warn: something isn't right but recovery is straight forward
//   - error: something is wrong
//   - trace: usually used for something that we want to become
//            a warning, but there are known issues still to be
//            resolved; usually #if .. #endif out during production.
//   - debug: related to a problem that we want to eliminate, i.e.,
//            will become an error; should be #if .. #endif out
//            during production
//
// If show_verbose is set then C# and CASL stacktraces are added
// to the log message, except for the info setting.
//
// The warnings have 2 frames each, just enough to have a clear
// idea of the context of the message.
// The errors have 10 frames each, intended to give reasonably
// complete understanding of where the error occured.
//
// If both show_verbose and debug are set then full stacktraces
// for both C# and CASL are provided.
//
// Sadly, there seems to be no "canned" way to get local variable
// information for the C# stack.
//
///////////////////////
//
// The Flt_Rec mechanism is *not* intended for production use.
// It writes a separate log file for each run without regard to
// change in dates, etc.
//
// What it currently provides is some control over when and
// what gets logged; my intention is extend this to specific
// methods and start, stop intervals.
//
// Currently, each call should look like
//
//      if (Flt_Rec.go(Flt_Rec.where_t.**some_flag**))
//        Flt_Rec.log(**current function string**
//                    **message string**);
//
// The flags are:
//   error_flag - associated with CASL_error construction
//   methods_flag - tracing all method calls
//   load_file_flag - tracing the loading of *.casl or *.casle files
//   execute_expr_flag - every step of interpreter (well close to that)
//   always_flag - if the Flt_Rec is active always log this.
//
// NOTE: Each entry of the Flt_Rec provides a "current cycle" number,
// which measures CPU cycles for the current thread; it does not
// reflect wall-clock time but gives a very accurate understanding
// of how much CPU work is being done by the code.  It is the same
// mechanism used in when gather call_graph statistics.
//
// NOTE: At the moment the Flt_Rec is primarily focused on supporting
// performance improvements to the CASL Interpreter and tracking down
// why Config (a.k.a., <thaw_with_path>) takes so *_!#@{%&( long.
//


// Whole file is under Bug 19856 UMN Reimplement Logging using NLog

namespace CASL_engine
{
  public class Logger
  {
    private static NLog.Logger logger = NLog.LogManager.GetLogger("ipayment_log");
    private static NLog.Logger cs_logger = NLog.LogManager.GetLogger("ipayment_cs_log");

    /// <summary>
    /// Log content to log. This is the generic interface, but developers should use the more
    /// efficient LogInfo, LogWarn, LogTrace and LogError functions from C# when they can.
    /// </summary>
    /// <param name="arg_pairs"></param>
    public static Object CASL_Log(params Object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      String logMessage = args.get("log_content", "") as String;
      String action = args.get("action", "") as String;
      String logType = args.get("log_type", "") as String;
      return Log(logMessage, action, logType);
    }

    public static Object Log(String logMessage, String action, String logType)
    {
      String msg = FormatLogString(logMessage, action);
      String actionLC = action.ToLower();

      switch (logType)
      {
        case "err":
          logger.Error(msg);
          break;

        case "warn":
          logger.Warn(msg);
          break;

        case "info":
          logger.Info(msg);
          break;

        case "trace":
          logger.Trace(msg);
          break;

        default:
          // Don't worry, Contains is really fast
          if (actionLC.Contains("error") || logMessage.Contains("error="))
          {
            logger.Error(msg);
          }
          else if (actionLC.Contains("warning"))
          {
            // Note we will want to use the FormatProvider or the LogMessageGenerator, but for now get it working
            logger.Warn(msg);
          }
          else
          {
            // Note we will want to use the FormatProvider or the LogMessageGenerator, but for now get it working
            logger.Info(msg);
          }
          break;
      }
      return null;
    }

    /// <summary>
    /// Helper method to format the string to log in a standard way.
    /// </summary>
    /// <param name="logMessage">The Message to be logged.</param>
    /// <param name="action">An optional action that represents what the user was doing</param>
    /// <returns></returns>
    private static string FormatLogString(String logMessage, String action)
    {
      GenericObject my = CASLInterpreter.get_my();

      // Bug 20435 UMN PCI-DSS
      String sessionID = (my == null) ? "" : my.get("MaskedSessionID", "") as String;

      String userId = GetUserId();
      String ipAddress = misc.get_ip_address();

      if (!String.IsNullOrWhiteSpace(action)) action = String.Format("ACTION='{0}' ", action);

      //need add double quote if log from cs, be consistent with casl call 
      if (logMessage.IndexOf('"') < 0)
        logMessage = String.Format("\"{0}\"", logMessage);
      // Note we will want to use the FormatProvider or the LogMessageGenerator, but for now get it working
      return String.Format("IpAddress='{0}' SessionID='{1}' UserID='{2}' {3} {4}", ipAddress, sessionID, userId, action, logMessage);
    }

    static readonly Regex myPat = new Regex(@"\/my\/([0-9]+)\/", RegexOptions.Compiled | RegexOptions.Singleline);

    /// <summary>
    /// Get the user ID from the top app (top app will be my.0, my.1 etc, and will be available from the raw_url
    /// in most (but not all cases.
    /// </summary>
    /// <returns></returns>
    public static String GetUserId()
    {
      // Simplify
      String userId = "";

      GenericObject my = CASLInterpreter.get_my();
      if (my == null || System.Web.HttpContext.Current == null) return userId;

      MatchCollection matches = myPat.Matches(System.Web.HttpContext.Current.Request.RawUrl);
      if (matches.Count != 1) return userId;

      int myId = -1;
      if (!int.TryParse(matches[0].Groups[1].Value, out myId)) return userId;

      GenericObject top_app = (my == null) ? null : my.get(myId, null) as GenericObject;
      if (top_app == null) return userId;

      GenericObject a_user = top_app.get("a_user", null) as GenericObject;
      if (a_user == null) return userId;
      if (a_user.has("id")) return a_user.get("id") as String;
      else return a_user.ToString();
    }

    /// <summary>
    /// Log informational content to log
    /// </summary>
    /// <param name="logMessage">The Message to be logged.</param>
    /// <param name="action">An optional action that represents what the user was doing</param>
    public static void LogInfo(String logMessage, String action)
    {
      String msg = FormatLogString(logMessage, action);
      logger.Info(msg);
    }

    /// <summary>
    /// Log warning content to log
    /// </summary>
    /// <param name="logMessage">The Message to be logged.</param>
    /// <param name="action">An optional action that represents what the user was doing</param>
    public static void LogWarn(String logMessage, String action)
    {
      String msg = FormatLogString(logMessage, action);
      logger.Warn(msg);
    }

    /// <summary>
    /// Log error content to log
    /// </summary>
    /// <param name="logMessage">The Message to be logged.</param>
    /// <param name="action">An optional action that represents what the user was doing</param>
    public static void LogError(String logMessage, String action)
    {
      String msg = FormatLogString(logMessage, action);
      logger.Error(msg);
    }

    /// <summary>
    /// Log trace content to log
    /// </summary>
    /// <param name="logMessage">The Message to be logged.</param>
    /// <param name="action">An optional action that represents what the user was doing</param>
    public static void LogTrace(String logMessage, String action)
    {
      String msg = FormatLogString(logMessage, action);
      logger.Trace(msg);
    }

    /// <summary>
    /// Log debug content to log
    /// </summary>
    /// <param name="logMessage">The Message to be logged.</param>
    /// <param name="action">An optional action that represents what the user was doing</param>
    public static void LogDebug(String logMessage, String action)
    {
      String msg = FormatLogString(logMessage, action);
      logger.Debug(msg);
    }

    public enum cs_flag
    {
      info,
      warn,
      error,
      trace,
      debug
    }

    public static string[] new_line = new string[] { Environment.NewLine };

    static string reduceFrames(int cnt)
    {
      string st = Environment.StackTrace;
      string[] stk = st.Split(new_line, StringSplitOptions.None);

      int top = stk.Length - 3; // Start with the Logger.cs_log() call.
      if (top < cnt)
        cnt = top;
      st = "";
      for (int i = 3; i < cnt; i++)
      {
        st += stk[i] + Environment.NewLine;
      }
      return st;
    }

    /// <summary>
    /// Rudimentary logging to Cs log. Assume everything in there is trace level stuff
    /// </summary>
    /// <param name="logMessage"></param>
    public static void cs_log(String logMessage, cs_flag f = cs_flag.info)
    {
      if (!c_CASL.Add_cs_Log_Entry)
        return;
      string casl_stack = null;

      logMessage = FormatLogString(logMessage, "");  // Bug 24955 UMN

      // Do not dump the CASL Stack without show_verbose set to true.
      if (c_CASL.get_show_verbose_error())
      {
        switch (f)
        {
          case cs_flag.info:
            break;
          case cs_flag.warn:
            logMessage += reduceFrames(3);
            // logMessage += Environment.NewLine;
            casl_stack = CASL_Stack.stacktrace_StrBld(null, 5).ToString();
            break;
          case cs_flag.error:
            logMessage += reduceFrames(10);
            // logMessage += Environment.NewLine;
            casl_stack = CASL_Stack.stacktrace_StrBld(null, 10).ToString();
            break;
          case cs_flag.trace:
            if (c_CASL.debug)
            {
              logMessage += reduceFrames(10);
              // logMessage += Environment.NewLine;
              casl_stack = CASL_Stack.stacktrace_StrBld(null, 10).ToString();
            }
            break;
          case cs_flag.debug:
            if (c_CASL.debug)
            {
              logMessage += Environment.StackTrace;
              // logMessage += Environment.NewLine;
              casl_stack = CASL_Stack.stacktrace_StrBld(null, 20).ToString();
            }
            break;
        }
      }

      if (casl_stack != null) {
        string[] delim = new string[1] {Environment.NewLine};
        var cs_arr = casl_stack.Split(delim, 30, StringSplitOptions.None);
        casl_stack = "    Associated CASL Stack" + Environment.NewLine;
        foreach (String s in cs_arr)
          casl_stack += "    " + s + Environment.NewLine;
      }

      switch (f)
      {
        case cs_flag.info:
          cs_logger.Info(logMessage);
          if (casl_stack != null)
            cs_logger.Info(casl_stack);
          break;
        case cs_flag.warn:
          cs_logger.Warn(logMessage);
          if (casl_stack != null)
            cs_logger.Warn(casl_stack);
          break;
        case cs_flag.error:
          cs_logger.Error(logMessage);
          if (casl_stack != null)
            cs_logger.Error(casl_stack);
          break;
        case cs_flag.trace:
          cs_logger.Trace(logMessage);
          if (casl_stack != null)
            cs_logger.Trace(casl_stack);
          break;
        case cs_flag.debug:
          cs_logger.Debug(logMessage);
          if (casl_stack != null)
            cs_logger.Debug(casl_stack);
          break;
      }
    }

    /// <summary>
    /// CS logs an exception traceback
    /// UMN added for Bug 26489
    /// </summary>
    /// <param name="logMessage">A message to prefix the exception traceback</param>
    /// <param name="e">The exception</param>
    public static void cs_log_trace(String logMessage, Exception e)
    {
      cs_logger.Error($"{logMessage}: {e.ToMessageAndCompleteStacktrace()}");
    }

    public static Object cs_log_aux(params Object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      String logMessage = args.get("log_content", "") as String;

      cs_log(logMessage, cs_flag.trace);

      return null;
    }
    
    public static Object cs_log_genobj(params Object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject logObject = args.get("log_content", "") as GenericObject;

      if (c_CASL.Add_cs_Log_Entry)
        cs_logger.Trace(logObject.ToString());

      return null;
    }
  }

  [Flags]
  public enum GO_Actions {
    none = 0x0,
    create = 0x1,
    set = 0x2,
    has = 0x4,
    get = 0x8,
    remove = 0x10,
    parent_set = 0x20
  };

  // Flight recorder
  //Bug 12081 NJ-unused variable
  public class Flt_Rec
  {
    public enum when_t
    {
      never,               // Don't initialize flight recorder.
      programatic,         // Initialize, but activated via CASL or Visual Studio
      after_init,          // Setup CASL Interpreter then log config load.
      after_config,        // Wait until config is loaded and then start recording
      always               // Guess :).
    }
    [Flags]
    public enum where_t
    {
      error_flag = 0x1,
      methods_flag = 0x2,
      load_file_flag = 0x4,
      execute_expr_flag = 0x8,
      always_flag = 0x10
    };

    [Flags]
    public enum option_t
    {
      datetime = 0x1,
      stopwatch = 0x2,
      step_count = 0x4,
      step_delta = 0x8
    }

#pragma warning disable 414
    // UMN to enable flight recorder for debugging stackoverflows, set to true and recompile
    public static when_t when = when_t.never;
    public static bool active = false;
    public static int fl_where = 0;
    public static option_t fl_option = option_t.datetime;
#pragma warning restore 414
    //end 12081 NJ
    //private static int depth = 0; Bug# 27153 DH - Clean up warnings when building ipayment
    private static string name = "";
    private static StreamWriter file;
    public static object fl_lock = new object();

    public static Dictionary<long, GO_Actions> objects_of_interest =
       new Dictionary<long, GO_Actions>();
    public static Dictionary<string, GO_Actions> names_of_interest =
       new Dictionary<string, GO_Actions>();
    // public static Dictionary<RegExp, GO_Actions> paths_of_interest =
    //   new Dictionary<RegExp, GO_Actions>();
    public static Dictionary<string, GO_Actions> keys_of_interest =
       new Dictionary<string, GO_Actions>();
    public static Dictionary<string, GO_Actions> classes_of_interest =
       new Dictionary<string, GO_Actions>();
    public static HashSet<string> methods_of_interest = new HashSet<string>();

    protected static char[] vertbar = new char[] { '|' };
    public static GO_Actions default_actions = GO_Actions.create
      | GO_Actions.set | GO_Actions.has | GO_Actions.get | GO_Actions.remove;
    public static GO_Actions parse_GO_Actions(string s)
    {
      s = s.Trim();
      if (s.Length == 0)
        return default_actions;

      GO_Actions ret = GO_Actions.none;
      string[] acts = s.Split(vertbar, StringSplitOptions.None);
      foreach (string act in acts) {
        string a = act.Trim();
        if (a == "create") {
          ret |= GO_Actions.create;
          continue;        
        }
        if (a == "set") {
          ret |= GO_Actions.set;
          continue;        
        }
        if (a == "has") {
          ret |= GO_Actions.has;
          continue;        
        }
        if (a == "get") {
          ret |= GO_Actions.get;
          continue;        
        }
        if (a == "remove") {
          ret |= GO_Actions.remove;
          continue;        
        }
        if (a == "parent_set") {
          ret |= GO_Actions.parent_set;
          continue;        
        }
      }
      return ret;
    }

    protected static char[] spacetab = new char [] { ' ', '\t' };
    public static string skipToSpace(string s)
    {
      int ix = s.IndexOfAny(spacetab);
      return s.Substring(ix + 1);
    }

    // Flight recorder is slowly being converted into a tracing mechanism
    // which can be used stand alone or inside Visual Studio.
    public static void initialize(string flt_when, string flt_where, string flt_option)
    {

      switch (flt_when.Trim())
      {
        case "never":
          return;      // Don't complete initialization
        case "programatic":
          Flt_Rec.when = Flt_Rec.when_t.programatic;
          break;
        case "after_init":
          Flt_Rec.when = Flt_Rec.when_t.after_init;
          break;
        case "after_config":
          Flt_Rec.when = Flt_Rec.when_t.after_config;
          break;
        case "always":
          Flt_Rec.when = Flt_Rec.when_t.always;
          active = true;
          break;
      }

      fl_where = 0x1;
      char[] vertbar = new char[] { '|' };
      string[] flags = flt_where.Split(vertbar, StringSplitOptions.RemoveEmptyEntries);
      foreach (string flag in flags)
      {
        switch (flag.Trim())
        {
          case "error":
            fl_where |= (int)Flt_Rec.where_t.error_flag;
            break;
          case "methods":
            fl_where |= (int)Flt_Rec.where_t.methods_flag;
            break;
          case "load_file":
            fl_where |= (int)Flt_Rec.where_t.load_file_flag;
            break;
          case "execute_expr":
            fl_where |= (int)Flt_Rec.where_t.execute_expr_flag;
            break;
          case "nothing":
            fl_where = 0;
            break;
        }
      }

      
      string[] options = flt_option.Split(vertbar, StringSplitOptions.RemoveEmptyEntries);
      foreach (string opt in options)
      {
        switch (opt.Trim())
        {
        case "~datetime":
          fl_option &= ~Flt_Rec.option_t.datetime;
          break;
        case "datetime":
          fl_option |= Flt_Rec.option_t.datetime;
          break;
        case "~stopwatch":
          fl_option &= ~Flt_Rec.option_t.stopwatch;
          break;
        case "stopwatch":
          fl_option |= Flt_Rec.option_t.stopwatch;
          break;
        case "~step_count":
          fl_option &= ~Flt_Rec.option_t.step_count;
          break;
        case "step_count":
          fl_option |= Flt_Rec.option_t.step_count;
          break;
        case "~step_delta":
          fl_option |= Flt_Rec.option_t.step_delta;
          break;
        case "step_delta":
          fl_option |= Flt_Rec.option_t.step_delta;
          break;
        }
      }

      string base_folder = misc.CASL_get_code_base();

      string log_folder = base_folder + "/log/";

      string timestamp = System.DateTime.Now.ToString("yyyy_MM_dd_HH-mm-ss");
      name = log_folder + "flight_recorder." + timestamp + ".txt";
      file = new System.IO.StreamWriter(name, false,
                                        System.Text.Encoding.UTF8, 2000);
      if (file != null && active)
      {
        file.Write("OPENED ON: ");
        file.WriteLine(timestamp);
        file.Flush();
      }

      string trace_file = base_folder + "/trace_ctl";
      if (File.Exists(trace_file)) {
        file.WriteLine("processing " + trace_file);
        var lines = File.ReadLines(trace_file);
        foreach (var line in lines) {
          string s1 = line.TrimStart();
          string s2 = null;
          if (s1.Length == 0)
            continue;
          if (s1[0] == '#')
            continue;

          if (s1.StartsWith("object")) {
            s2 = s1.Substring(6).TrimStart();
            
            Regex obj_reg = new Regex(@"(\d+)\s*(\S.*)?$", RegexOptions.Compiled);
            Match obj_match = obj_reg.Match(s2);

            if (obj_match.Success) {
              Group gnum = obj_match.Groups[1];
              if (Int64.TryParse(gnum.Value, out long geo_num)) {
                string ss2 = obj_match.Groups[2].Value;
                GO_Actions o_act = parse_GO_Actions(ss2);
                objects_of_interest.Add(geo_num, o_act);
              } else {
                file.WriteLine("TryParse failed on " + gnum.Value);
              }
            } else {
              file.WriteLine("Unrecognized : " + line);
            }
            continue;
          }

          if (s1.StartsWith("name")) {
            s2 = s1.Substring(4).TrimStart();
            Regex name_reg = new Regex(@"(\a+)\s+(\S.*)?$", RegexOptions.Compiled);
            Match name_match = name_reg.Match(s2);
            if (name_match.Success) {
              string name = name_match.Groups[1].Value;
              GO_Actions name_actions =
                parse_GO_Actions(name_match.Groups[2].Value);
              names_of_interest.Add(name, name_actions);
            } else {
              names_of_interest.Add(s2, default_actions);
            }
            continue;
          }

          if (s1.StartsWith("key")) {
            s2 = s1.Substring(3).TrimStart();
            Regex key_reg = new Regex(@"(\a+)\s+(\S.*)?$", RegexOptions.Compiled);
            Match key_match = key_reg.Match(s2);
            if (key_match.Success) {
              string key = key_match.Groups[1].Value;
              GO_Actions key_actions =
                parse_GO_Actions(key_match.Groups[2].Value);
              keys_of_interest.Add(key, key_actions);
            } else {
              keys_of_interest.Add(s2, default_actions);
            }
            continue;
          }

          if (s1.StartsWith("class")) {
            s2 = s1.Substring(5).TrimStart();
            Regex class_reg = new Regex(@"(\S+)\s+(\S.*)?$",
                                        RegexOptions.Compiled);
            Match class_match = class_reg.Match(s2);
            if (class_match.Success) {
              string class_name = class_match.Groups[1].Value;
              GO_Actions class_actions =
                parse_GO_Actions(class_match.Groups[2].Value);
              classes_of_interest.Add(class_name, class_actions);
            } else {
              classes_of_interest.Add(s2, default_actions);
            }
            continue;
          }

          if (s1.StartsWith("method")) {
            s2 = s1.Substring(6).TrimStart();
            methods_of_interest.Add(s1);
            continue;
          }
        }
      }
    }

    public static bool go(where_t wh)
    {
      if (!active)
        return false;

      if (wh != where_t.always_flag)
      {
        if (0 == (fl_where & (int)wh))
          return false;
      }
      return true;
    }

    private static ulong last_step_count = 0;

    public static void log(
        string method,
        string log,
        bool dump_casl_stack = false,
        bool dump_cs_stack = false)
    {
      StringBuilder sb = new StringBuilder();

      GenericObject my = CASLInterpreter.get_my();
      String sessionID = (my == null) ? "" : my.get("MaskedSessionID", "") as String;

      String userId = Logger.GetUserId();
      // String ipAddress = misc.get_ip_address();

      string line = "";
      if (fl_option.HasFlag(option_t.datetime))
        line += DateTime.Now.ToString("HH:mm:ss-FFFFF") + ": ";
      if (fl_option.HasFlag(option_t.stopwatch)) {
        double delta = c_CASL.master_stopwatch.Elapsed.TotalMilliseconds;
        line += ((ulong) (delta * 10000)).ToString() + ": ";
      }
      if (fl_option.HasFlag(option_t.step_count))
        line += c_CASL.master_step_count.ToString() + ": ";
      if (fl_option.HasFlag(option_t.step_delta)) {
        ulong delta = c_CASL.master_step_count - last_step_count;
        last_step_count = c_CASL.master_step_count;  
        line += delta.ToString() + ": ";
      }

      sb.AppendLine(
          string.Format("{0} SessionID='{1}' UserId='{2}' {3}: {4}",
                        line,
                        sessionID, userId,
                        method, log));
      if (dump_casl_stack)
        CASL_Stack.stacktrace_StrBld(sb);
      if (dump_cs_stack)
        sb.AppendLine(Environment.StackTrace);

      lock (fl_lock)
      {
        file.WriteLine(sb.ToString());
        file.Flush();
      }  // should add a final file.Close() when system exits.
    }

    public static String show_object(object obj)
    {
      if (obj == null)
        return "<null>";
      return obj.ToString();
    }

    public static object fCASL_flight_recorder(CASL_Frame frame)
    {
      GenericObject geo_args = frame.args;
      string meth_where = "no location found";

      CASL_Frame f = frame;
      code_location loc = code_location.no_location;
      bool name_found = false;
      while (f != null)
      {
        if ((!name_found) && (f.method_name != null))
        {
          meth_where = f.method_name;
          name_found = true;
        }
        if (!loc.isValid())
        {
          if (f.current_loc.isValid())
            loc = f.current_loc;
          else if (f.loc.isValid())
            loc = f.loc;
        }
        if (name_found && loc.isValid())
          break;
        f = f.calling_frame;
      }
      meth_where += " " + loc.ToString();

      string s = "";
      int l = geo_args.getLength();
      for (int i = 0; i < l; i++) {
        Object obj = geo_args.get(i);
        if (obj is GenericObject geo) {
          string ptm = geo.get_path_to_me;
          if (! ptm.StartsWith("["))
            s += ptm;
          else
            s += geo.show_short();
        } else
          s += obj.ToString();
        if (i < (l - 1))
          s += "  ";
      }

      if (go(where_t.always_flag))
        log(meth_where, s);
      return null;
    }
  }

  public class DumpFile
  {
    public static TextWriter create_writer(string file_name)
    {
      string log_folder = misc.CASL_get_code_base() + "/log/";

      string timestamp = System.DateTime.Now.ToString("yyyy_MM_dd_HH-mm-ss");
      string name = log_folder + file_name + "." + timestamp + ".txt";

      return File.CreateText(name);
    }
  }
}
