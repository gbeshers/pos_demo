// #define DEBUG

using System;
using System.Collections;
using System.Text;
using System.Xml;
using System.Reflection;
using System.IO;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;

// Temporary. GMB.
using System.Diagnostics;


namespace CASL_engine
{
  /// <summary>
  /// Summary description for CASLParser_ConciseXML.
  /// </summary>
  public class CASLParser_ConciseXML
  {
    private char current_event;
    //		private char next_event;
    private string current_stream ="";
    private int current_index =0;
    private string xml_str=null;  // for debugging, keep original string around

    private string current_event_args = "";
    private int status;
    // Consistent with FileDictionary and FileIndex initialization.
    private string current_filename = "undefined file";
    private int current_file_idx = 0;
    private string current_source_string = null;
    private XmlTextReader _aXmlTextReader;
    private GenericObject path=null;

    private GenericObject CS = new GenericObject(); // Container Stack 
    private GenericObject KS = new GenericObject(); // Key Stack 
    private StringBuilder TOKEN;

    public const String UNKNOWN_KEY = "__UK";
    public const String PATH_PART = "__PP";

    private bool pre_ws=false;
		
    private GenericObject symbols = new GenericObject("null", null, "true", true, "false",false, UNKNOWN_KEY, UNKNOWN_KEY, "END","END");

    public CASLParser_ConciseXML( )
    {

    }

    /*
    **
    ** public void run()
    ***/

    // <param name="a_xml">a_xml=""=string</param>
    // <param name="file_name"> file_name=""=string</param>
    // <returns></returns>
    public virtual object Run(String a_xml, String file_name )
    {		
      current_filename = "undefined file";
      current_file_idx = 0;
      current_source_string = null;
      Object return_obj=null;
      try
      {
        if( file_name == null || file_name.Trim().Length == 0)
        {
          if( a_xml== null )
            return null;
          if( a_xml.Trim().Length == 0 ) 
            return null;
          if( !a_xml.Trim().StartsWith("<") )
            a_xml = "<do>" + a_xml + "</do>";
        }
        //-----------------------------------------
        // Use XMLReader(C# Parser) to parse XML
        if ( _aXmlTextReader == null )
        {
          //Read from file directly
          if(file_name!=null && file_name.Length >0 )
          {
            _aXmlTextReader = new XmlTextReader(file_name);
            _aXmlTextReader.WhitespaceHandling = WhitespaceHandling.None;
            _aXmlTextReader.Normalization = true;
            current_filename = file_name;
            current_file_idx = CASLInterpreter.getFileIdx(file_name);
            current_source_string = null;
          }
            //Read from Xml String Parameter
          else 
          {
            xml_str=a_xml;
            StringReader SR = new StringReader(a_xml);
            _aXmlTextReader = new XmlTextReader(SR);
            _aXmlTextReader.WhitespaceHandling = WhitespaceHandling.None;
            _aXmlTextReader.Normalization = true;

            current_file_idx = 0;
            current_source_string = a_xml;
          }
        }

        //-----------------------------------------------------
        // fire event for all occations
        status = ConciseXMLStates2.START_FIELD;
        CS.insert(new GenericObject());
        KS.insert("END");
        while (_aXmlTextReader.Read() == true)
        {
          if ( _aXmlTextReader.NodeType == XmlNodeType.CDATA)
          {
            current_event = '"';
            process_event();
            TOKEN.Append(_aXmlTextReader.Value.ToString());
            current_event = '"';
            process_event();
          }
          else if (_aXmlTextReader.NodeType == XmlNodeType.Text) 
          {
            String sTextValue = _aXmlTextReader.Value;
            current_stream = sTextValue;

            for (int i = 0; i < sTextValue.Length; i++)
            {
              current_event = sTextValue[i];
              current_index = i;
              process_event();
            }
            current_stream="";

          }
          else if (_aXmlTextReader.NodeType == XmlNodeType.Element) 
          {
            String sElementName = _aXmlTextReader.Name;
            current_event = '<';
            current_event_args = sElementName;
            process_event();
            current_event_args = "";
            // set _parent and other attribute 
            //string sTextValue = " _parent="+sElementName +  sAttribute + " "; // handle element name as path/symbol
            string sTextValue = " _parent="+sElementName  + " "; // handle element name as path/symbol
            for (int i = 0; i < sTextValue.Length; i++)
            {
              current_event = sTextValue[i];
              process_event();
            }
          }
          if (_aXmlTextReader.NodeType == XmlNodeType.EndElement || (_aXmlTextReader.NodeType == XmlNodeType.Element && _aXmlTextReader.IsEmptyElement)  )
          {
            current_event = '>';
            process_event();
          }
        }
#if DEBUG
        if (current_file_idx > 0) {
        CASLInterpreter.FileIndex[current_file_idx].file_last_line =
          _aXmlTextReader.LinePosition;
        }
#endif
        _aXmlTextReader.Close();
        char eos = (char)8;
        current_event = eos;
        process_event();
        return_obj= (TCS() as GenericObject).get(0);
      }
      catch(CASL_error ce)
      {
        _aXmlTextReader.Close();
        //Logger.cs_log(ce.ToMessageAndCompleteStacktrace()); // should not be logged here; during loading, we do throw a lot of exeption when parsing.   
        throw ce;
      }
      catch(Exception)// Bug# 27153 DH - Clean up warnings when building ipayment
      {
        _aXmlTextReader.Close();	
        //Logger.cs_log(e.ToMessageAndCompleteStacktrace()); // should not be logged here; during loading, we do throw a lot of exeption when parsing.   
        throw;				

      }
      return return_obj;
    }
    private object get_next_char()
    {
      if( current_index+1 < current_stream.Length)
        return current_stream[current_index+1];
      else
        return null;
    }
    private void check_double_dots()
    {
      // check double dots throw error
      if( ConciseXMLCharSet2.IS_DOT(current_event) && ConciseXMLCharSet2.IS_DOT(get_next_char()) )
      {
        throw CASL_error.create("title","Syntax Error", "code",109, 
          "message","Not ConciseXML Encoding. Found two dots (..) in " + current_filename + ", line nbr: " + _aXmlTextReader.LineNumber, 
          "filename", current_filename, "line_number",_aXmlTextReader.LineNumber, "line_position",_aXmlTextReader.LinePosition);
      }
    }
    private bool in_path()
    {
      return TCS() is GenObj_Path;
    }
    private bool has_key()
    {
      return (KS.getLength() > CS.getLength());
    }
    private bool is_even_stack()
    {
      return (KS.getLength() == CS.getLength());
    }

    private void add_code_info (GenObj_Code obj) 
    {
      obj.loc.source_or_filename = current_source_string;
      obj.loc.file_index = current_file_idx;
      obj.loc.line_num = (short) _aXmlTextReader.LineNumber;
      obj.loc.line_pos = (short) _aXmlTextReader.LinePosition;

//#if DEBUG -- not yet.
      obj.set("__code_info", current_filename + "#" +
          _aXmlTextReader.LineNumber + "," + _aXmlTextReader.LinePosition);
//#endif
    }

    private GenObj_Code new_object ()
    {
      GenObj_Code obj = new GenObj_Code(GO_kind.casl_code, code_location.no_location);
      add_code_info(obj);

      if (_aXmlTextReader.HasAttributes)
      {
        for (int i = 0; i < _aXmlTextReader.AttributeCount; i++)
        {
          _aXmlTextReader.MoveToAttribute(i);
          obj.set( _aXmlTextReader.Name, _aXmlTextReader.Value);
          if( ! obj.has("__parsed_field_order") )
            obj.set("__parsed_field_order", c_CASL.c_GEO());
          (obj.get("__parsed_field_order") as GenericObject).insert(_aXmlTextReader.Name);
        }
        _aXmlTextReader.MoveToElement(); //Moves the reader back to the element node.
      }

      return obj;
    }
    private void append_token()
    {
      TOKEN.Append(current_event);
    }
    private void reset_token()
    {
      TOKEN = new StringBuilder("");
    }		
    private object TKS()
    {
      return KS.get(KS.getLength()-1);
    }
    private object TCS()
    {
      return CS.get(CS.getLength()-1);
    }
    private Object number_end_action ()
    {
            // Bug #12055 Mike O - Use misc.Parse to log errors
      // Bug 8030 MJO - This may not actually be an int, so don't log a failure
      return misc.Parse<Int32>(TOKEN.ToString(), false);
    }	
    // never used
    private Object decimal_end_action()
    {
      // Bug #12055 Mike O - Use misc.Parse to log errors
      return misc.Parse<Decimal>(TOKEN.ToString());
    }	

    private Object string_end_action()
    {
      return TOKEN.ToString();
    }	

    private void path_string_dot_action()
    {
      path.insert(TOKEN.ToString());
    }	

    private void path_integer_dot_action()
    {
      // Bug #12055 Mike O - Use misc.Parse to log errors
      int i_token = misc.Parse<Int32>(TOKEN.ToString()); 
      path.insert(i_token);
    }	

    private Object path_string_end_action()
    {
      path.insert(TOKEN.ToString());
      return path;
    }	
    private Object path_integer_end_action()
    {
      // Bug #12055 Mike O - Use misc.Parse to log errors
      int i_token = misc.Parse<Int32>(TOKEN.ToString());
      path.insert(i_token);
      return path;
    }	

    private Object symbol_end_action()
    {
      if (symbols.has(TOKEN.ToString()))
        return symbols.get(TOKEN.ToString());
      GenObj_Symbol symbol = new GenObj_Symbol(TOKEN.ToString());
      return symbol;
    }	
    private void tcs_set_key_and_value ()
    {
      if( !is_even_stack() )  // equiv to has_key
      {
        // Bug #8510 Mike O - Show no details when show_verbose_error is false
        if(c_CASL.get_show_verbose_error())
        throw CASL_error.create("title","Syntax Error", "code",107, 
          "message","Not ConciseXML Encoding.  Mismatched key and value in " + current_filename + ", line nbr: " + _aXmlTextReader.LineNumber, 
          "filename", current_filename, "line_number",_aXmlTextReader.LineNumber, "line_position",_aXmlTextReader.LinePosition);
        else
          throw CASL_error.create("title", "Error", "code", 107, "message", "");
      }
      Object V = CS.remove();
      Object K = KS.remove();
      if( K is GenObj_Path)
        K = Tool.path_to_string(K as GenObj_Path);
      GenericObject tcs=(GenericObject)TCS();
      if( ! "_parent".Equals(K) && tcs.has(K) )
      {
        string s_message = string.Format("Duplicate key '{0}' found in {1} at {2}:{3}", K, current_filename, _aXmlTextReader.LineNumber, _aXmlTextReader.LinePosition);
        throw CASL_error.create("title","Syntax Error", "code",103, "message",s_message, "filename", current_filename, "line_number",_aXmlTextReader.LineNumber, "line_position",_aXmlTextReader.LinePosition);
      }
			 
      if( ! tcs.has("__parsed_field_order") )
        tcs.set("__parsed_field_order", c_CASL.c_GEO());
      (tcs.get("__parsed_field_order") as GenericObject).insert(K);
      tcs.set(K,V);		
    }

    private void tcs_set_unkey_and_value()
    {
      if( !is_even_stack() )  // equiv to has_key
      {
        throw CASL_error.create("title","Syntax Error", "code",112, 
          "message","Not ConciseXML Encoding. Mismatched key-value in " + current_filename + ", line nbr: " + _aXmlTextReader.LineNumber, 
          "filename", current_filename, "line_number",_aXmlTextReader.LineNumber, "line_position",_aXmlTextReader.LinePosition);
      }
      KS.remove();
      Object V = CS.remove();
      ((GenericObject)TCS()).insert(V);
    }

    private void cs_push_new_object_ks_push_uk()
    {
      GenericObject it= new_object();
      CS.insert(it);
      KS.insert(UNKNOWN_KEY);
    }
    private void cs_push_new_object_ks_push_pp()
    {
      GenericObject it= new_object();
      CS.insert(it);
      KS.insert(PATH_PART);
    }
    private void cs_push_new_object_ks_push_end()
    {
      GenericObject it= new_object();
      CS.insert(it);
      KS.insert("END");
    }
    private void cs_push_new_object()
    {
      GenericObject it= new_object();
      CS.insert(it);
    }
    private void move_tcs_to_tks()
    {
      Object K= CS.remove();

      // Convert symbol key to string key
      if ( K is GenObj_Symbol)
      { 
        K = (K as GenObj_Symbol).get("name").ToString();
      }
      KS.remove();
      KS.insert(K);
    }	
    private void  process_event()
    {
      switch (status)
      {
        case ConciseXMLStates2.START: 
        {
          if( ConciseXMLCharSet2.IS_START_ELEMENT(current_event) ) 
          {
            cs_push_new_object_ks_push_end();
            status = ConciseXMLStates2.START_FIELD;
          }
          break;
        }
        case ConciseXMLStates2.START_FIELD: 
        {
          if( ConciseXMLCharSet2.IS_START_ELEMENT(current_event) ) 
          {
            if(in_path())
            {
              cs_push_new_object_ks_push_pp();
            }
            else if (has_key())
            {
              cs_push_new_object();
            }
            else 
            {
              cs_push_new_object_ks_push_uk();
            }
          }
          else if (ConciseXMLCharSet2.IS_END_ELEMENT(current_event))
          {
            if ( ( TKS() is string) && TKS().ToString() == UNKNOWN_KEY ) 
            {
              tcs_set_unkey_and_value();
              if( (TKS() is string) && TKS().ToString() == PATH_PART)
                tcs_set_unkey_and_value();
              status = ConciseXMLStates2.EAT_WS;
              pre_ws = false;
            }
            else if ( ( TKS() is string) && TKS().ToString() == PATH_PART )
            {
              tcs_set_unkey_and_value();
              status = ConciseXMLStates2.EAT_WS;
              pre_ws = false;
            }						
            else if ( (TKS()is string) && TKS().ToString() == "END" )
            {
              status = ConciseXMLStates2.END;
            }						
            else
            {
              tcs_set_key_and_value();
              if( (TKS() is string) && TKS().ToString() == PATH_PART)
                tcs_set_unkey_and_value();
              status = ConciseXMLStates2.EAT_WS;
              pre_ws = false;
            }
          } // end a IS_END_ELEMENT
          else if (ConciseXMLCharSet2.IS_EQUAL(current_event)) //IS_EQUAL
          {
            pre_ws = true;
            if ( ( TKS() is string) && TKS().ToString() == PATH_PART )
            {
              tcs_set_unkey_and_value();
              if( (TKS() is string) && TKS().ToString() == UNKNOWN_KEY)
                move_tcs_to_tks();
              else // KK  - handle 3rd "type" position key=value=type
              {
                Object K = TKS();
                Object FK = misc.field_key(K,"type"); // FK = field_key , foo_f_type
                tcs_set_key_and_value();
                KS.insert(FK);
              }
            }
            else // IS NOT PP
            {
              if( (TKS() is string) && TKS().ToString() == UNKNOWN_KEY)
                move_tcs_to_tks();
              else // KK  - handle 3rd "type" position key=value=type
              {
                Object K = TKS();
                Object FK = misc.field_key(K,"type"); // FK = field_key , foo_f_type
                tcs_set_key_and_value();
                KS.insert(FK);
              }
            }
          }
          else if (ConciseXMLCharSet2.IS_WS(current_event))
          {
            pre_ws = true;
          }					
          else if (ConciseXMLCharSet2.IS_EOS(current_event))
          {
            // check it is the bottom one, otherwise throw error
            status = ConciseXMLStates2.END;
          }					
          else if (ConciseXMLCharSet2.IS_DOT(current_event))
          {
            if( in_path())
            {
              //							if( TOKEN.Length == 0)
              //							{}
              //							else
              //							{
              //								(TCS() as GenericObject).insert(TOKEN.ToString());
              //								reset_token();
              //							}
            }
            else
            {
              if( pre_ws )
              {
                if(!has_key())
                  KS.insert(UNKNOWN_KEY);

                GenObj_Path a_path_expr = new GenObj_Path();
                GenObj_Symbol subject = new GenObj_Symbol("_subject");
                a_path_expr.set(0, subject);
                CS.insert(a_path_expr);
                add_code_info(a_path_expr);
              }
              else
              {
                object PP= CS.remove();
                GenObj_Path a_path_expr = new GenObj_Path();
                a_path_expr.set(0, PP);
                CS.insert(a_path_expr);
                add_code_info(a_path_expr);
              }
            }

          }
          else if (ConciseXMLCharSet2.IS_ANY_CHAR(current_event))
          {
            reset_token();
            if( ConciseXMLCharSet2.IS_DIGIT(current_event) || '-'.Equals(current_event) ) 
            {
              append_token();
              status = ConciseXMLStates2.NUMBER;
            }
            else if (ConciseXMLCharSet2.IS_DOUBLEQUOTE(current_event) ) 
            {
              status = ConciseXMLStates2.DOUBLE_QUOTED_STRING;
            }
            else if (ConciseXMLCharSet2.IS_SINGLEQUOTE(current_event) ) 
            {
              status = ConciseXMLStates2.SINGLE_QUOTED_STRING;
            }
            else if (ConciseXMLCharSet2.IS_TOKEN_CHAR(current_event) ) 
            {
              append_token();
              status = ConciseXMLStates2.SYMBOL_OR_STRING;
            }
            else
            {
              throw CASL_error.create("title","Syntax Error", "code",104, 
                "message","Not ConciseXML Encoding.  Expecting a key or value in " + current_filename + ", line nbr: " + _aXmlTextReader.LineNumber, 
                "filename", current_filename, "line_number",_aXmlTextReader.LineNumber, "line_position",_aXmlTextReader.LinePosition);
            }
          }
          else
          {
            throw CASL_error.create("title","Syntax Error", "code",104, 
              "message","Not ConciseXML Encoding in " + current_filename + ", line nbr: " + _aXmlTextReader.LineNumber, 
              "filename", current_filename, "line_number",_aXmlTextReader.LineNumber, "line_position",_aXmlTextReader.LinePosition);
          }
          break;
        }
          ////////////////////////////////////////////////////////
          // NUMBER
          ////////////////////////////////////////////////////////
        case ConciseXMLStates2.NUMBER: 
        {
          if( ConciseXMLCharSet2.IS_DIGIT(current_event) ) // TODO: add -
          {
            append_token();
          }
          else if ( ConciseXMLCharSet2.IS_DOT(current_event))  
          {
            check_double_dots();
            if( in_path())
            {
              // [32]
              if( TCS() is GenericObject && (TCS() as GenericObject).getLength() ==1 && (TCS() as GenericObject).get(0) is int)// IS_DECIMAL
              { 
                // [30]
                (TCS() as GenericObject).insert(TOKEN.ToString());
                GenericObject V= CS.remove() as GenericObject;
                string s_v = Tool.symbol_or_path_to_string(V);
                decimal d_v = (decimal)Convert.ChangeType(s_v, typeof(decimal));
                CS.insert(d_v);
                status = ConciseXMLStates2.START_FIELD;
                process_event();
              }
              else // IS_NOT_DECIMAL
              {
                // [31]
                (TCS() as GenericObject).insert(number_end_action());
                status = ConciseXMLStates2.START_FIELD;
              }
            }
            else // NOT_IN_PATH
            {
              // [33]
              object convert_it = number_end_action();
              // if peek is a digit, process decimal 
              if( ConciseXMLCharSet2.IS_DIGIT(get_next_char())) // IS DECIMAL 
              {
                // [34]
                append_token();
                status = ConciseXMLStates2.DECIMAL;
              }
              else
              {
                //process it and finish it
                if(has_key())
                {
                  CS.insert(convert_it);
                  reset_token();//clear it
                }
                else
                {
                  KS.insert(UNKNOWN_KEY);
                  CS.insert(convert_it);
                  reset_token();//clear it
                }
                status = ConciseXMLStates2.EAT_WS;
                pre_ws = false;

                process_event();// rethrow
              }
            }
          }
          else if ( ConciseXMLCharSet2.IS_EV(current_event)) 
          {
            if(in_path())
            {
              if( TCS() is GenericObject && (TCS() as GenericObject).getLength() ==1 && (TCS() as GenericObject).get(0) is int) // IS_DECIMAL
              {
                //								(TCS() as GenericObject).insert(number_end_action());
                (TCS() as GenericObject).insert(TOKEN.ToString());
                GenericObject V= CS.remove() as GenericObject;
                string s_v = Tool.symbol_or_path_to_string(V);
                decimal d_v = (decimal)Convert.ChangeType(s_v, typeof(decimal));
                CS.insert(d_v);
                status = ConciseXMLStates2.EAT_WS;
                pre_ws = false;
                process_event();
              }
              else // IS_NOT_DECIMAL
              {
                (TCS() as GenericObject).insert(number_end_action());
                reset_token();
                status = ConciseXMLStates2.EAT_WS;
                pre_ws = false;
                process_event();
              }
            }
            else // NOT_IN_PATH
            {
              if(has_key())
              {
                CS.insert(number_end_action());
                reset_token();
                status = ConciseXMLStates2.EAT_WS;
                pre_ws = false;
                process_event();
              }
              else
              {
                KS.insert(UNKNOWN_KEY);
                CS.insert(number_end_action());
                reset_token();
                status = ConciseXMLStates2.EAT_WS;
                pre_ws = false;
                process_event();
              }
            }
          }
          else
          {
            throw CASL_error.create("title","Syntax Error", "code",106, 
              "message","Not ConciseXML Encoding in " + current_filename + ", line nbr: " + _aXmlTextReader.LineNumber, 
              "filename", current_filename, "line_number",_aXmlTextReader.LineNumber, "line_position",_aXmlTextReader.LinePosition);
          }
          break;
        }

          ////////////////////////////////////////////////////////
          // DECIMAL 
          ////////////////////////////////////////////////////////
        case ConciseXMLStates2.DECIMAL:  //[10]
        {
          if( ConciseXMLCharSet2.IS_DIGIT(current_event) )
          {
            append_token();
          }
          else if( ConciseXMLCharSet2.IS_EV(current_event) || ConciseXMLCharSet2.IS_DOT(current_event)) 
          {
            if(has_key())
            {
              CS.insert(decimal_end_action());
              reset_token();
              status = ConciseXMLStates2.EAT_WS;
              pre_ws = false;
              process_event();
            }
            else
            {
              KS.insert(UNKNOWN_KEY);
              CS.insert(decimal_end_action());
              reset_token();
              status = ConciseXMLStates2.EAT_WS;
              pre_ws = false;
              process_event();
            }
          }
          else
          { 
            throw CASL_error.create("title","Syntax Error", "code",111, 
              "message","Not ConciseXML Encoding when reading a decimal in " + current_filename + ", line nbr: " + _aXmlTextReader.LineNumber, 
              "filename", current_filename, "line_number",_aXmlTextReader.LineNumber, "line_position",_aXmlTextReader.LinePosition);
          }
          break;
        }

          ////////////////////////////////////////////////////////
          // DOUBLE_QUOTED_STRING
          ////////////////////////////////////////////////////////
        case ConciseXMLStates2.DOUBLE_QUOTED_STRING: 
        {
          if ( ConciseXMLCharSet2.IS_DOUBLEQUOTE(current_event) )
          {
            if(in_path())
            {
              (TCS() as GenericObject).insert(string_end_action());// two args
              reset_token();
              status = ConciseXMLStates2.EAT_WS;
              pre_ws = false;
            }
            else
            {
              if(has_key())
              {
                CS.insert(string_end_action());
                reset_token();
                status = ConciseXMLStates2.EAT_WS;
                pre_ws = false;
              }
              else
              {
                KS.insert(UNKNOWN_KEY);
                CS.insert(string_end_action());
                reset_token();
                status = ConciseXMLStates2.EAT_WS;
                pre_ws = false;
              }
            }					
          }
            // CHECK EOS in the middle of string
          else if(	ConciseXMLCharSet2.IS_EOS(current_event))
          {
            throw CASL_error.create("title","Syntax Error", "code",112, 
              "message","Not ConciseXML Encoding. Missing double quote in " + current_filename + ", line nbr: " + _aXmlTextReader.LineNumber, 
              "filename", current_filename, "line_number",_aXmlTextReader.LineNumber, "line_position",_aXmlTextReader.LinePosition);
          }
          else
          {
            append_token();
          }
          break;
        }
          ////////////////////////////////////////////////////////
          // SINGLE_QUOTED_STRING
          ////////////////////////////////////////////////////////
        case ConciseXMLStates2.SINGLE_QUOTED_STRING: 
        {
          if (ConciseXMLCharSet2.IS_SINGLEQUOTE(current_event) )
          {
            if(in_path())
            {
              (TCS() as GenericObject).insert(string_end_action());
              //							(TCS() as GenericObject).set(string_end_action());
              reset_token();
              status = ConciseXMLStates2.EAT_WS;
              pre_ws = false;
            }
            else
            {
              if(has_key())
              {
                CS.insert(string_end_action());
                reset_token();
                status = ConciseXMLStates2.EAT_WS;
                pre_ws = false;
              }
              else
              {
                KS.insert(UNKNOWN_KEY);
                CS.insert(string_end_action());
                reset_token();
                status = ConciseXMLStates2.EAT_WS;
                pre_ws = false;
              }
            }						
          }
          else if(	ConciseXMLCharSet2.IS_EOS(current_event))
          {
            throw CASL_error.create("title","Syntax Error", "code",165, 
              "message","Not ConciseXML Encoding. Missing double quote in " + current_filename + ", line nbr: " + _aXmlTextReader.LineNumber, 
              "filename", current_filename, "line_number",_aXmlTextReader.LineNumber, "line_position",_aXmlTextReader.LinePosition);
          }
          else
          {
            append_token();
          }
          break;
        }
          ////////////////////////////////////////////////////////
          // SYMBOL_OR_STRING
          ////////////////////////////////////////////////////////
        case ConciseXMLStates2.SYMBOL_OR_STRING: 
        {
          if(ConciseXMLCharSet2.IS_TOKEN_CHAR(current_event) )
          {
            append_token();
          }
          else if (ConciseXMLCharSet2.IS_EV(current_event) || ConciseXMLCharSet2.IS_DOT(current_event) )
          {
            check_double_dots();
            if(in_path())
            {
              (TCS() as GenericObject).insert(string_end_action());
              reset_token();
              status = ConciseXMLStates2.EAT_WS;
              pre_ws = false;
              process_event();
            }
            else
            {
              if(has_key())
              {
                CS.insert(symbol_end_action());
                reset_token();
                status = ConciseXMLStates2.EAT_WS;
                pre_ws = false;
                process_event();
              }
              else
              {
                KS.insert(UNKNOWN_KEY);
                CS.insert(symbol_end_action());
                reset_token();
                status = ConciseXMLStates2.EAT_WS;
                pre_ws = false;
                process_event();
              }
            }						
          }
          else
          {
            string str_message="";
            if (current_filename.Equals("no_filename"))
              str_message="Not ConciseXML Encoding: " + xml_str;
            else
              str_message="Not ConciseXML Encoding in " + current_filename;
            // Bug #8510 Mike O - Show no details when show_verbose_error is false
            if (c_CASL.get_show_verbose_error())
            throw CASL_error.create("title","Syntax Error", "code",107, 
              "message", str_message + " line #: " + _aXmlTextReader.LineNumber, 
              "filename", current_filename, "line_number",_aXmlTextReader.LineNumber, "line_position",_aXmlTextReader.LinePosition);
            else
              throw CASL_error.create("title", "Error", "code", 107, "message", "");
          }
          break;
        }
          ////////////////////////////////////////////////////////
          // EAT_WS
          ////////////////////////////////////////////////////////
        case ConciseXMLStates2.EAT_WS: 
        {
          if (ConciseXMLCharSet2.IS_WS(current_event)) 
          {
            pre_ws=true;
          }
          else if ( ConciseXMLCharSet2.IS_EQUAL(current_event)|| ConciseXMLCharSet2.IS_END_ELEMENT(current_event))  
          {
            status = ConciseXMLStates2.START_FIELD;
            process_event();
          }
          else if (ConciseXMLCharSet2.IS_DOT(current_event))
          {
            check_double_dots();
            if( pre_ws )
            {
              if ( (TKS() is string) && TKS().ToString() == UNKNOWN_KEY)
              {
                tcs_set_unkey_and_value();
                status = ConciseXMLStates2.START_FIELD;
                process_event();
              }
              else // KK
              {
                tcs_set_key_and_value();
                status = ConciseXMLStates2.START_FIELD;
                process_event();
              }
            }
            else // IS NOT PRE_WS
            {
              status = ConciseXMLStates2.START_FIELD;
              process_event();
            }
          }
          else if ( ConciseXMLCharSet2.IS_EOS(current_event) || ConciseXMLCharSet2.IS_ANY_CHAR(current_event) || ConciseXMLCharSet2.IS_START_ELEMENT(current_event))  
          {
            if ( (TKS() is string) && TKS().ToString() == UNKNOWN_KEY)
            {
              tcs_set_unkey_and_value();
              status = ConciseXMLStates2.START_FIELD;
              process_event();
            }
            else
            {
              tcs_set_key_and_value();
              status = ConciseXMLStates2.START_FIELD;
              process_event();
            }
          }
          else
          {
            throw CASL_error.create("title","Syntax Error", "code",108, 
              "message","Not ConciseXML Encoding after whitespace in " + current_filename + ", line nbr: " + _aXmlTextReader.LineNumber, 
              "filename", current_filename, "line_number",_aXmlTextReader.LineNumber, "line_position",_aXmlTextReader.LinePosition);
          }
          break;
        }		
        default: 
        {
          throw CASL_error.create("title","Syntax Error", "code",109, 
            "message","Not ConciseXML Encoding in " + current_filename + ", line nbr: " + _aXmlTextReader.LineNumber, 
            "filename", current_filename, "line_number",_aXmlTextReader.LineNumber, "line_position",_aXmlTextReader.LinePosition);
        }
      }
    }
  }

  class ConciseXMLStates2
  {
    public const int START = 1; 
    public const int START_FIELD = 2; 
    public const int NUMBER = 3;
    public const int DOUBLE_QUOTED_STRING = 5;
    public const int SINGLE_QUOTED_STRING = 6;
    public const int SYMBOL_OR_STRING = 7;
    public const int EAT_WS = 8;
    public const int END = 9;
    public const int DECIMAL = 10;
  }
  class ConciseXMLCharSet2
  {
    public const String LOWERCASE = "abcdefghijklmnopqrstuvwxyz";
    public const String UPPERCASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public const String DIGIT = "0123456789";
    public const String TOKEN_CHAR = LOWERCASE + UPPERCASE + DIGIT + "-_[]";// todo: define the token char
    public const String	DOT = ".";
    public const String	DOUBLEQUOTE = "\"";
    public const String	SINGLEQUOTE = "'";
    public const String ANY_CHAR = TOKEN_CHAR+DOUBLEQUOTE+SINGLEQUOTE+DOT;

    public const String WS = "\r\t\n ";
    public const String	EQUAL = "=";
    public const String START_ELEMENT = "<";
    public const String END_ELEMENT = ">";
    public const char EOS = (char)8; //backspace EOS=end of stream

    public const String EV = START_ELEMENT+ END_ELEMENT + EQUAL +WS;

    public static bool IS_EOS(char new_char)
    {
      if( EOS== new_char )
        return true;
      return false;
    }
    public static bool IS_ANY_CHAR(char new_char)
    {
      if( ConciseXMLCharSet2.ANY_CHAR.IndexOf(new_char)>=0 )
        return true;
      return false;
    }
    public static bool IS_LOWERCASE(char new_char)
    {
      if( ConciseXMLCharSet2.LOWERCASE.IndexOf(new_char)>=0 )
        return true;
      return false;
    }
    public static bool IS_UPPERCASE(char new_char)
    {
      if( ConciseXMLCharSet2.UPPERCASE.IndexOf(new_char)>=0 )
        return true;
      return false;
    }
    //  public static bool IS_DIGIT(char new_char)
    //  {
    //   if( ConciseXMLCharSet2.DIGIT.IndexOf(new_char)>=0 )
    //    return true;
    //   return false;
    //  }
    //
    public static bool IS_DIGIT(object new_char)
    {
      if(new_char==null)
        return false;
      else if (ConciseXMLCharSet2.DIGIT.IndexOf((char)new_char)>=0 )
        return true;
      else
        return false;
    }
    public static bool IS_TOKEN_CHAR(char new_char)
    {
      if( ConciseXMLCharSet2.TOKEN_CHAR.IndexOf(new_char)>=0 )
        return true;
      return false;
    }
    public static bool IS_WS(char new_char)
    {
      if( ConciseXMLCharSet2.WS.IndexOf(new_char)>=0 )
        return true;
      return false;
    }
    public static bool IS_EQUAL (char new_char)
    {
      if( ConciseXMLCharSet2.EQUAL.IndexOf(new_char)>=0 )
        return true;
      return false;
    }
    public static bool IS_DOT (object new_char)
    {
      if(new_char==null)
        return false;
      else if ( ConciseXMLCharSet2.DOT.IndexOf((char)new_char)>=0 )
        return true;
      else
        return false;
    }
    public static bool IS_DOUBLEQUOTE (char new_char)
    {
      if( ConciseXMLCharSet2.DOUBLEQUOTE.IndexOf(new_char)>=0 )
        return true;
      return false;
    }	
    public static bool IS_SINGLEQUOTE (char new_char)
    {
      if( ConciseXMLCharSet2.SINGLEQUOTE.IndexOf(new_char)>=0 )
        return true;
      return false;
    }	
    public static bool IS_START_ELEMENT (char new_char)
    {
      if( ConciseXMLCharSet2.START_ELEMENT.IndexOf(new_char)>=0 )
        return true;
      return false;
    }	
    public static bool IS_END_ELEMENT (char new_char)
    {
      if( ConciseXMLCharSet2.END_ELEMENT.IndexOf(new_char)>=0 )
        return true;
      return false;
    }	
    public static bool IS_EV (char new_char)
    {
      if( ConciseXMLCharSet2.EV.IndexOf(new_char)>=0 )
        return true;
      return false;
    }	
  }
}
