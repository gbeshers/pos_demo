/***************************************************
 File Created by Daniel Hofman 03/05/2004
 
 This file is no longer used (MP 2007)
 
 The "CASLCodeGenerator.cs" file has one namespace and 3 classes:
 NameSpace:	CASL_engine
 Class 1:	CodeGenerator
 Class 2:	CodeCompiler
 Class 3:	CodeRuntime

 CodeGenerator:
 This class is used to generate class object and components 
 like methods, fields, properties structures etc. as well as 
 the C# file to house all these elements.

 CodeCompiler:
 This class has the ability to compile multiple files into 
 an assembly. Normally you would create a single class with 
 all the methods, fields etc. per C# file using CodeGenerator 
 class and then use the CodeCompiler class to compile those 
 files into one MSIL assembly file.

 CodeRuntime:
 This class is used to call the compiled code. 
   For example:
    Use the CodeGenerator to create a class and some methods 
    then use CodeCompiler class to compile the code and 
    finally use CodeRuntime class to invoke methods on just 
    created and compiled class passing the class instance returned 
    by CodeCompiler class.
*/


using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using Microsoft.CSharp;
using System.IO;
using System.Text;
using System.Reflection;
using System.Collections;
using System.Windows.Forms;

//12081 NJ-disable compiler warnings
#pragma warning disable 618
namespace CASL_engine
{
  public class CodeGenerator
  {
    public CodeGenerator() { }


    //-----------------------------------------
    // Used for the mirror class functionality.
    private bool m_bUseMirrorClass = false;
    private string m_strTargetClassName = "";
    private string m_strMirrorClassName = "";// m_strTargetClassName + _Mirror
    //-----------------------------------------

    private string m_strCSFileTarget = "";
    private CodeNamespace m_TargetNameSpace = null;
    private CodeTypeDeclaration m_TargetClass;
    private CodeConstructor m_Constructor = null;

    public void Set_CSFileTarget(string strFileName)
    {
      m_strCSFileTarget = strFileName;
    }

    public string Get_CSFileTarget()
    {
      return m_strCSFileTarget;
    }

    public bool CreateNameSpace(string strNameSpaceName)
    {
      // Namespace and includes
      if (m_TargetNameSpace == null)
      {
        m_TargetNameSpace = new CodeNamespace(strNameSpaceName);
        if (m_TargetNameSpace == null)
          return false;
      }

      // This is a minimumm. Caller should add any special refs.
      AddUsingReference("System");
      AddUsingReference("System.Drawing");
      AddUsingReference("System.Windows.Forms");
      AddUsingReference("System.Collections");
      AddUsingReference("System.ComponentModel");

      return true;
    }

    public void AddUsingReference(string strUsing)
    {
      m_TargetNameSpace.Imports.Add(new CodeNamespaceImport(strUsing));
    }

    public bool CreateStruct(string strStructName, Object[,] Members)
    {
      CodeTypeDeclaration TargetStruct = new CodeTypeDeclaration(strStructName);
      if (TargetStruct == null)
        return false;

      m_TargetNameSpace.Types.Add(TargetStruct);
      TargetStruct.IsStruct = true;

      //----------------------------------------------------------------
      // Now add some members.
      // Array should contain:
      // [0]= Type of memeber.
      // [1]= Name of memeber.

      if (Members != null)
      {
        int row = 0;
        int col = 0;
        int iLen = Members.Length / 2;
        for (int i = 0; i < iLen; i++)
        {
          Type DataType = (Type)Members[row, col];
          string strName = (string)Members[row, col + 1];
          col = 0;
          row++;

          CodeMemberField field = new CodeMemberField(DataType, strName);
          TargetStruct.Members.Add(field);
        }
      }

      return true;
    }

    public bool CreateClass(string strClassName, string strBaseClassName, string strBaseClassNameSpace, bool bUseMirror)
    {
      // Create class. Optionally derive from another class.
      // If the base class is in another namespace, pass 
      // the name space of that class too.

      m_TargetClass = new CodeTypeDeclaration(strClassName);
      if (m_TargetClass == null)
        return false;

      m_TargetNameSpace.Types.Add(m_TargetClass);
      m_TargetClass.IsClass = true;

      //-----------------------------------------------------
      // Derive class from another.
      // Will not create base class if name of base class is not
      // provided.
      if (strBaseClassName.Length > 0)
      {
        // If we are creating base class, make sure to add the 
        // namespace if it is not in the same one.
        if (strBaseClassNameSpace.Length > 0)
          AddUsingReference(strBaseClassNameSpace);// future use...

        m_TargetClass.Attributes = MemberAttributes.Public;
        m_TargetClass.Attributes = MemberAttributes.Private;
        // Set our base class.
        m_TargetClass.BaseTypes.Add(strBaseClassName);
      }
      //-----------------------------------------------------

      //-----------------------------------------------------
      // Also setup the caller class so we can access some members.
      CreateLiteralField(System.CodeDom.MemberAttributes.Public, "Object", "m_CallerObj", "null");

      // Add Hashtable to hold all custom control objects. This will let me expand them later.
      CreateLiteralField(System.CodeDom.MemberAttributes.Public, "Hashtable", "m_AllCustControls", "new Hashtable()");
      //-----------------------------------------------------

      //-----------------------------------------------------
      // Also setup the mirror class if needed.
      // We need the flag in the code generator also, this will determine if I should 
      // add the stubs for the mirror class to class object and most methods.
      m_bUseMirrorClass = bUseMirror;
      if (m_bUseMirrorClass == true)
      {
        m_strTargetClassName = strClassName;
        m_strMirrorClassName = m_strTargetClassName + "_Mirror";

        CreateBoolDatatypeField(System.CodeDom.MemberAttributes.Public, "m_bUseMirrorClass", bUseMirror);
        // This will be initialized from class constructor.
        CreateLiteralField(System.CodeDom.MemberAttributes.Public, m_strMirrorClassName, "m_" + m_strMirrorClassName, "null");
      }
      //-----------------------------------------------------

      return false;
    }

    public bool CreateClassConstructor(Object[] BodyObject)
    {
      m_Constructor = new CodeConstructor();
      if (m_Constructor == null)
        return false;

      m_Constructor.Attributes = MemberAttributes.Public;
      m_TargetClass.Members.Add(m_Constructor);

      // Add body to the constructor.
      if (BodyObject != null)
      {
        for (int j = 0; j < BodyObject.Length; j++)
        {
          string strBodyLine;
          strBodyLine = (string)BodyObject[j];
          m_Constructor.Statements.Add(new CodeSnippetStatement(strBodyLine));
        }
      }

      // Add initialization to the for the mirror class.
      if (m_bUseMirrorClass == true)
      {
        string str = string.Format("{0} = new {1}(this);", "m_" + m_strMirrorClassName, m_strMirrorClassName);
        m_Constructor.Statements.Add(new CodeSnippetStatement(str));
      }

      return true;
    }

    public bool AddConstructorStatements(Object[] BodyObject)
    {
      if (BodyObject != null && m_Constructor != null)
      {
        for (int j = 0; j < BodyObject.Length; j++)
        {
          string strBodyLine;
          strBodyLine = (string)BodyObject[j];
          m_Constructor.Statements.Add(new CodeSnippetStatement(strBodyLine));
        }
      }
      else
        return false;

      return true;
    }

    public void AddDisposeMchanism()
    {
      // This is provided only for Forms.

      CodeMemberMethod Method1 = new CodeMemberMethod();
      Method1.Name = "Dispose";
      Method1.ReturnType = new CodeTypeReference(typeof(void));
      Method1.Attributes = MemberAttributes.Override |
        MemberAttributes.Family;
      Method1.Parameters.Add(new CodeParameterDeclarationExpression(typeof(bool), "disposing"));
      Method1.Statements.Add(new CodeSnippetStatement("if( disposing )"));
      Method1.Statements.Add(new CodeSnippetStatement("{"));
      Method1.Statements.Add(new CodeSnippetStatement("if (components != null)"));
      Method1.Statements.Add(new CodeSnippetStatement("{"));
      Method1.Statements.Add(new CodeSnippetStatement("components.Dispose();"));
      Method1.Statements.Add(new CodeSnippetStatement("}"));
      Method1.Statements.Add(new CodeSnippetStatement("}"));
      Method1.Statements.Add(new CodeSnippetStatement("base.Dispose( disposing );"));
      m_TargetClass.Members.Add(Method1);
    }

    public bool CreateMainEntryPointMethod()
    {
      // Main
      CodeEntryPointMethod Start = new CodeEntryPointMethod();
      if (Start == null)
        return false;

      m_TargetClass.Members.Add(Start);

      return true;
    }

    public bool CreateStringDatatypeField(MemberAttributes Atributes, string strFieldName, string InitVal)
    {
      // Create and initialize to InitVal.
      CodeMemberField field = new CodeMemberField("System.String", strFieldName);
      if (field == null)
        return false;

      field.Attributes = Atributes;
      m_TargetClass.Members.Add(field);
      field.InitExpression = new CodePrimitiveExpression(InitVal);
      return true;
    }

    public bool CreateBoolDatatypeField(MemberAttributes Atributes, string strFieldName, bool bInitVal)
    {
      // Create and initialize to InitVal.
      CodeMemberField field = new CodeMemberField(typeof(bool), strFieldName);
      if (field == null)
        return false;

      field.Attributes = Atributes;
      m_TargetClass.Members.Add(field);
      field.InitExpression = new CodePrimitiveExpression(bInitVal);
      return true;
    }
    public bool CreateIntDatatypeField(MemberAttributes Atributes, string strFieldName, int InitVal)
    {
      // Create and initialize to InitVal.
      CodeMemberField field = new CodeMemberField("System.Int32", strFieldName);
      if (field == null)
        return false;

      field.Attributes = Atributes;
      m_TargetClass.Members.Add(field);
      field.InitExpression = new CodePrimitiveExpression(InitVal);
      return true;
    }

    public bool CreateLongDatatypeField(MemberAttributes Atributes, string strFieldName, long InitVal)
    {
      // Create and initialize to InitVal.
      CodeMemberField field = new CodeMemberField(typeof(long), strFieldName);
      if (field == null)
        return false;

      field.Attributes = Atributes;
      m_TargetClass.Members.Add(field);
      field.InitExpression = new CodePrimitiveExpression(InitVal);
      return true;
    }

    public bool CreateDoubleDatatypeField(MemberAttributes Atributes, string strFieldName, double InitVal)
    {
      // Create and initialize to InitVal.
      CodeMemberField field = new CodeMemberField(typeof(double), strFieldName);
      if (field == null)
        return false;

      field.Attributes = Atributes;
      m_TargetClass.Members.Add(field);
      field.InitExpression = new CodePrimitiveExpression(InitVal);
      return true;
    }

    public bool CreateDateTimeDatatypeField(MemberAttributes Atributes, string strFieldName, string InitVal)
    {
      // Create and initialize to InitVal.
      CodeMemberField field = new CodeMemberField("System.DateTime", strFieldName);
      if (field == null)
        return false;

      field.Attributes = Atributes;
      m_TargetClass.Members.Add(field);
      string str;
      str = "new DateTime(";
      str += InitVal;
      str += ")";
      field.InitExpression = new CodeSnippetExpression(str);
      return true;
    }

    public bool CreateCustomDatatypeField(MemberAttributes Atributes, Type Custom_Type, string strFieldName, string InitVal)
    {
      // Create and initialize to InitVal.
      // Create special custom type field. This will be good for structs or other objects.
      // You must initialize with the apropriate data!!!

      CodeMemberField field = new CodeMemberField(Custom_Type, strFieldName);
      field.Attributes = Atributes;
      if (field == null)
        return false;

      m_TargetClass.Members.Add(field);
      field.InitExpression = new CodeSnippetExpression(InitVal);
      return true;
    }

    public bool CreateLiteralField(MemberAttributes Atributes, string strCodeDeclaration, string strCodeInstanceName, string strInitializationWord)
    {
      CodeMemberField field = new CodeMemberField(strCodeDeclaration, strCodeInstanceName);
      field.Attributes = Atributes;
      if (field == null)
        return false;

      m_TargetClass.Members.Add(field);

      if (strInitializationWord.Length > 0)
        field.InitExpression = new CodeSnippetExpression(strInitializationWord);

      return true;
    }

    public bool CreateMethod(MemberAttributes Atributes, string strMethodName, object[,] ArgList, object[] BodyObject, Type ReturnType, bool bUsingMirror, bool bUseParams)
    {
      CodeMemberMethod Method1 = new CodeMemberMethod();
      Method1.Name = strMethodName;
      Method1.ReturnType = new CodeTypeReference(ReturnType);
      Method1.Attributes = Atributes;

      if (ArgList != null)// There could be no arguments for this method.
      {
        int row = 0;
        int col = 0;
        int iLen = ArgList.Length / 2;
        for (int i = 0; i < iLen; i++)
        {
          Type dType = (Type)ArgList[row, col];
          string strVal = (string)ArgList[row, col + 1];
          col = 0;
          row++;
          Method1.Parameters.Add(new CodeParameterDeclarationExpression(dType, strVal));
        }
      }
      else // Use params array as argument list instead here.
      {
        if (bUseParams == true)
          Method1.Parameters.Add(new CodeParameterDeclarationExpression("params object[]", "args"));
      }

      if (bUsingMirror == false)
      {
        // Insert callers method body and do not use mirror stub.
        if (BodyObject != null)
        {
          for (int j = 0; j < BodyObject.Length; j++)
          {
            string strBodyLine;
            strBodyLine = (string)BodyObject[j];
            Method1.Statements.Add(new CodeSnippetStatement(strBodyLine));
          }
        }
      }
      else if (bUsingMirror == true)
      {
        // Insert mirror stub.
        string strArgs = "";
        if (ArgList != null)
        {
          int row = 0;
          int col = 0;
          int iLen = ArgList.Length / 2;
          for (int i = 0; i < iLen; i++)
          {
            if (i > 0)
              strArgs += ",";

            string strVal = (string)ArgList[row, col + 1];
            col = 0;
            row++;
            strArgs += strVal;
          }
        }
        else
          if (bUseParams == true)
          strArgs = "args";// Using params here.

        string strStubBody = "";
        if (ReturnType != typeof(void))
          strStubBody = string.Format("return {0}.{1}({2});", "m_" + m_strMirrorClassName, strMethodName, strArgs);
        else
          strStubBody = string.Format("{0}.{1}({2});", "m_" + m_strMirrorClassName, strMethodName, strArgs);

        Method1.Statements.Add(new CodeSnippetStatement(strStubBody));
      }

      m_TargetClass.Members.Add(Method1);

      return false;
    }

    public bool CreateOutputFile()
    {
      TextWriter tw = new StreamWriter(new FileStream(Get_CSFileTarget(), FileMode.Create));
      if (tw == null)
        return false;

      // Code generator and code provider.
      ICodeGenerator codeGenerator = new CSharpCodeProvider().CreateGenerator();
      CSharpCodeProvider cdp = new CSharpCodeProvider();
      codeGenerator = cdp.CreateGenerator();
      codeGenerator.GenerateCodeFromNamespace(m_TargetNameSpace, tw, null);
      tw.Close();

      return true;
    }

    private bool SetControlLocation(string strName, int ixPos, int iyPos)
    {
      string strCode = "";
      strCode = string.Format("this.{0}.Location = new System.Drawing.Point({1}, {2});", strName, ixPos, iyPos);
      m_Constructor.Statements.Add(new CodeSnippetStatement(strCode));

      return true;
    }

    private bool AddControlEventHandler(string strName, string strEventType, Object[] EventHandlerBody)
    {
      string strCode = "";

      // Handle Click Event
      strCode = string.Format("this.{0}.{1} += new System.EventHandler(this.{2}_{3});", strName, strEventType, strName, strEventType);

      m_Constructor.Statements.Add(new CodeSnippetStatement(strCode));

      //------------------------------------------------------------------
      // Add the event handler function to this class.
      Object[,] args_list = new Object[2, 2];
      args_list[0, 0] = typeof(Object);
      args_list[0, 1] = "sender";
      args_list[1, 0] = typeof(System.EventArgs);
      args_list[1, 1] = "e";

      string strMethodName = strName;
      strMethodName += "_" + strEventType;
      CreateMethod(System.CodeDom.MemberAttributes.Private,
        strMethodName,
        args_list,
        EventHandlerBody,
        typeof(void),
        false,
        false);
      //------------------------------------------------------------------

      return true;
    }

    public bool AddButtonControl(string strName, string strBtnCaption, int ixPos, int iyPos, int iWidth, int iHeight, int iTabOrder, Object[] EventHandlerBody)
    {
      CreateControlDeclaration(System.CodeDom.MemberAttributes.Private, "System.Windows.Forms.Button", strName);
      CreateControlInstance("System.Windows.Forms.Button()", strName);
      SetControlName(strName);
      SetControlLocation(strName, ixPos, iyPos);
      SetControlSize(strName, iWidth, iHeight);
      SetControlText(strName, strBtnCaption);
      SetControlTabStop(strName, iTabOrder == 0 ? "false" : "true");
      SetControlTabIndex(strName, iTabOrder);
      AddControlEventHandler(strName, "Click", EventHandlerBody);

      //------------------------------------------------------------------
      // Add the control to form
      AddControlToConstructor(strName);
      //------------------------------------------------------------------

      //------------------------------------------------------------------
      // Add the control to hashtable of controls.
      AddControlToHashTable(strName);
      //------------------------------------------------------------------

      return true;
    }

    private bool SetControlName(string strName)
    {
      string strCode = "";
      strCode = string.Format("this.{0}.Name = \"{0}\";", strName);
      m_Constructor.Statements.Add(new CodeSnippetStatement(strCode));

      return true;
    }

    private bool SetControlSize(string strName, int iWidth, int iHeight)
    {
      string strCode = "";
      strCode = string.Format("this.{0}.Size = new System.Drawing.Size({1}, {2});", strName, iWidth, iHeight);
      m_Constructor.Statements.Add(new CodeSnippetStatement(strCode));

      return true;
    }

    private bool SetControlText(string strName, string strDefaultText)
    {
      string strCode = "";
      strCode = string.Format("this.{0}.Text = \"{1}\";", strName, strDefaultText);
      m_Constructor.Statements.Add(new CodeSnippetStatement(strCode));

      return true;
    }

    private bool SetControlMultiLine(string strName, string strMultiLine)
    {
      string strCode = "";
      strCode = string.Format("this.{0}.Multiline = {1};", strName, strMultiLine);
      m_Constructor.Statements.Add(new CodeSnippetStatement(strCode));

      return true;
    }

    private bool SetControlMaxLength(string strName, int iMaxLen)
    {
      string strCode = "";
      strCode = string.Format("this.{0}.MaxLength = {1};", strName, iMaxLen);
      m_Constructor.Statements.Add(new CodeSnippetStatement(strCode));

      return true;
    }

    private bool SetControlPasswordChar(string strName, char c)
    {
      // Set the password character if desired.
      string strCode = "";
      if (c != ' ')
      {
        strCode = string.Format("this.{0}.PasswordChar = \'{1}\';", strName, c);
        m_Constructor.Statements.Add(new CodeSnippetStatement(strCode));
      }
      return true;
    }

    public bool AddTextControl(string strName, string strDefaultText, string strLabelText, int ixPos, int iyPos, int iWidth, int iHeight, int iTabOrder, bool bMultiLine, int iMaxLen, bool bPasswordChars)
    {
      CreateControlDeclaration(System.CodeDom.MemberAttributes.Private, "System.Windows.Forms.TextBox", strName);
      CreateControlInstance("System.Windows.Forms.TextBox()", strName);
      SetControlName(strName);
      SetControlLocation(strName, ixPos, iyPos);
      SetControlSize(strName, iWidth, iHeight);
      SetControlText(strName, strDefaultText);
      SetControlTabStop(strName, iTabOrder == 0 ? "false" : "true");
      SetControlTabIndex(strName, iTabOrder);
      SetControlMultiLine(strName, bMultiLine == true ? "true" : "false");
      SetControlMaxLength(strName, iMaxLen);
      SetControlPasswordChar(strName, bPasswordChars == true ? '*' : ' ');

      //------------------------------------------------------------------
      // Add the control to form
      AddControlToConstructor(strName);
      //------------------------------------------------------------------

      //------------------------------------------------------------------
      // Add the control to hashtable of controls.
      AddControlToHashTable(strName);
      //------------------------------------------------------------------

      //========================================================
      // Add label for the control if label text is provided.
      // Also pass the name that will indicate which control it belongs to.
      // We also have to offset the iyPos to show above the parent control.
      if (strLabelText.Length > 0)
        AddLabelControl(strName, strLabelText, ixPos - (iWidth + 5), iyPos, iWidth, iHeight, 0);
      //========================================================

      return true;
    }

    private bool CreateControlDeclaration(MemberAttributes Atributes, string strCtlType, string CtlName)
    {
      CreateLiteralField(Atributes, strCtlType, CtlName, "");

      return true;
    }

    public bool AddComboControl(string strName, object[] Items, string strDefaultText, string strLabelText, int ixPos, int iyPos, int iWidth, int iHeight, int iTabOrder, bool bSorted, Object[] EventHandlerBody)
    {
      CreateControlDeclaration(System.CodeDom.MemberAttributes.Private, "System.Windows.Forms.ComboBox", strName);
      CreateControlInstance("System.Windows.Forms.ComboBox()", strName);
      SetControlName(strName);
      SetControlLocation(strName, ixPos, iyPos);
      SetControlSize(strName, iWidth, iHeight);
      SetControlText(strName, strDefaultText);
      SetControlTabIndex(strName, iTabOrder);

      string strCode = "";
      strCode = string.Format("this.{0}.AllowDrop = {1};", strName, "true");
      m_Constructor.Statements.Add(new CodeSnippetStatement(strCode));

      // Add list items.
      if (Items != null)
      {
        string strItems = "{";
        for (int i = 0; i < Items.Length; i++)
        {
          if (i != 0)
            strItems += ",";

          strItems += "\"";
          strItems += Items[i].ToString();
          strItems += "\"";
        }
        strItems += "}";

        strCode = string.Format("this.{0}.Items.AddRange( new object[]{1});", strName, strItems);
        m_Constructor.Statements.Add(new CodeSnippetStatement(strCode));
      }

      strCode = string.Format("this.{0}.Sorted = {1};", strName, bSorted == true ? "true" : "false");
      m_Constructor.Statements.Add(new CodeSnippetStatement(strCode));

      AddControlEventHandler(strName, "SelectedIndexChanged", EventHandlerBody);

      //------------------------------------------------------------------
      // Add the control to form
      AddControlToConstructor(strName);
      //------------------------------------------------------------------

      //------------------------------------------------------------------
      // Add the control to hashtable of controls.
      AddControlToHashTable(strName);
      //------------------------------------------------------------------

      //========================================================
      // Add label for the control if label text is provided.
      // Also pass the name that will indicate which control it belongs to.
      // We also have to offset the iyPos to show above the parent control.
      if (strLabelText.Length > 0)
        AddLabelControl(strName, strLabelText, ixPos - (iWidth + 5), iyPos, iWidth, iHeight, 0);
      //========================================================

      return true;
    }

    private bool CreateControlInstance(string CtlType, string CtlName)
    {
      // This is used for creating new controls.

      string strCode;
      strCode = string.Format("this.{0} = new {1};", CtlName, CtlType);
      m_Constructor.Statements.Add(new CodeSnippetStatement(strCode));

      return true;
    }

    private bool AddControlToConstructor(string strCtlName)
    {
      // Use this method to add just created controls to the constructor.

      string strCode = "";
      strCode = string.Format("this.Controls.Add(this.{0});", strCtlName);
      m_Constructor.Statements.Add(new CodeSnippetStatement(strCode));

      return true;
    }

    private bool AddControlToHashTable(string strName)
    {
      // Use this method to add just created controls to the control hashtable.

      string strCode = "";
      strCode = string.Format("this.m_AllCustControls.Add(\"{0}\",{1});", strName, strName);
      m_Constructor.Statements.Add(new CodeSnippetStatement(strCode));

      return true;
    }

    private bool SetControlTabStop(string strName, string strTabStop)
    {
      string strCode = "";
      strCode = string.Format("this.{0}.TabStop = {1};", strName, strTabStop);
      m_Constructor.Statements.Add(new CodeSnippetStatement(strCode));

      return true;
    }

    private bool SetControlTabIndex(string strName, int iTabOrder)
    {
      string strCode = "";
      strCode = string.Format("this.{0}.TabIndex = {1};", strName, iTabOrder);
      m_Constructor.Statements.Add(new CodeSnippetStatement(strCode));

      return true;
    }

    private bool SetControlDefaultFont(string strName)
    {
      string strCode = "";
      strCode = string.Format("this.{0}.Font = {1};", strName, "new System.Drawing.Font(\"Microsoft Sans Serif\", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));");
      m_Constructor.Statements.Add(new CodeSnippetStatement(strCode));

      return true;
    }

    private bool SetControlTextAllignment(string strName, string strTextAllign)
    {
      string strCode = "";
      strCode = string.Format("this.{0}.TextAlign = {1};", strName, "System.Drawing.ContentAlignment.MiddleRight");
      m_Constructor.Statements.Add(new CodeSnippetStatement(strCode));

      return true;
    }

    public bool AddLabelControl(string strName, string strLabelText, int ixPos, int iyPos, int iWidth, int iHeight, int iTabOrder)
    {
      // Create label name for this control;
      string strLabel = "Label_" + strName;
      CreateControlDeclaration(System.CodeDom.MemberAttributes.Private, "System.Windows.Forms.Label", strLabel);
      CreateControlInstance("System.Windows.Forms.Label()", strLabel);
      SetControlName(strLabel);
      SetControlLocation(strLabel, ixPos, iyPos);
      SetControlSize(strLabel, iWidth, iHeight);
      SetControlText(strLabel, strLabelText);
      SetControlTabStop(strLabel, "false");
      SetControlTabIndex(strLabel, 0);
      SetControlDefaultFont(strLabel);
      SetControlTextAllignment(strLabel, "System.Drawing.ContentAlignment.MiddleRight");
      AddControlToConstructor(strLabel);
      AddControlToHashTable(strLabel);

      return true;
    }

    public void SetFormSize(int iWidth, int iHeight)
    {
      // This is only for Window/WEB FORM classess.

      // Set the form size. That is if this is a form class. 
      string strSize = string.Format("this.ClientSize = new System.Drawing.Size({0}, {1});", iWidth, iHeight);
      Object[] FormSize = new Object[]{
                                        "this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);",
                                        strSize};
      AddConstructorStatements(FormSize);
    }
  }

  public class CodeCompiler
  {
    public CodeCompiler() { }

    //---------------------------------------------------------------
    // Using for compiling files and returning refs to compiled code.
    private Assembly m_ReferenceToCompiledCode = null;
    private CompilerResults m_CompilerResult = null;

    //  This class will add the refs below. This is a minimum. Caller of
    //  CompileCSFile() must call AddComplireReferences() with any special
    //  reference libraries.
    //  AddComplireReferences("system.dll");
    //  AddComplireReferences("System.Drawing.dll");
    //  AddComplireReferences("System.Windows.Forms.dll");
    private CompilerParameters m_CompilerParameters = null;
    //---------------------------------------------------------------

    public bool AddCompilerReferences(string strReference)
    {
      // Create new if we have to.
      if (m_CompilerParameters == null)
      {
        m_CompilerParameters = new CompilerParameters();
        if (m_CompilerParameters == null)
          return false;

        m_CompilerParameters.GenerateExecutable = true;
      }

      m_CompilerParameters.ReferencedAssemblies.Add(strReference);

      return true;
    }

    public Assembly GetReferenceToCompiledCode()
    {
      return m_ReferenceToCompiledCode;
    }

    public bool CompileCSFile(string[] files)
    {
      // Code compiler and provider
      ICodeCompiler CodeComp = new CSharpCodeProvider().CreateCompiler();
      if (CodeComp == null)
        return false;

      //--------------------------------------------------------
      // This is a minimumm. Caller should add any special refs.
      AddCompilerReferences("system.dll");
      AddCompilerReferences("System.Drawing.dll");
      AddCompilerReferences("System.Windows.Forms.dll");
      //--------------------------------------------------------

      // Run the compiler.
      m_CompilerResult = CodeComp.CompileAssemblyFromFileBatch(m_CompilerParameters, files);

      if (m_CompilerResult.Errors.HasErrors)
      {
        // Just return failure if a problem here.
        StringBuilder error = new StringBuilder();
        error.Append("Error Compiling Expression: ");
        foreach (CompilerError err in m_CompilerResult.Errors)
        {
          error.AppendFormat("{0}\n", err.ErrorText);
        }
        throw new Exception("Error Compiling Expression: " + error.ToString());
      }

      if (m_CompilerResult.Errors.HasErrors)
        return false;

      m_ReferenceToCompiledCode = m_CompilerResult.CompiledAssembly;

      return true;
    }
  }

  public class CodeRuntime
  {
    public CodeRuntime() { }

    public object CallStaticMethod(Type type, string strMethodName, params Object[] args)
    {
      object ReturnObj = null;
      MethodInfo Method = type.GetMethod(strMethodName,
                    BindingFlags.Static |
                    BindingFlags.Public |
                    BindingFlags.NonPublic);
      try
      {
        ReturnObj = Method.Invoke(null, args);
      }
      catch (Exception e)
      {
        // HX: Bug 17849
        Logger.cs_log(e.ToMessageAndCompleteStacktrace());
        ReturnObj = null;
      }
      return ReturnObj;
    }

    public object CallNonStaticMethod(Object objClassInstance, string strMethodName, params object[] args)
    {
      object ReturnObj = null;
      BindingFlags flags = 0;
      flags = BindingFlags.DeclaredOnly |
        BindingFlags.Public |
        BindingFlags.NonPublic |
        BindingFlags.Instance |
        BindingFlags.InvokeMethod;

      // Just a helper method to call functions
      try
      {
        Type type = objClassInstance.GetType();
        ReturnObj = type.InvokeMember(strMethodName,
                                       flags,
                                       null,
                                       objClassInstance,
                                       args);
      }
      catch (Exception e)
      {
        if (e.InnerException.Message.IndexOf("prompt") >= 0)
          throw;
        else
          ReturnObj = null;
      }
      return ReturnObj;
    }

    public object CallConstructorMethod(Type type, object[] args)
    {
      // This method will create an instance of a type and 
      // call its constructor with matching arguments.
      object ReturnObj = null;
      BindingFlags flags = 0;
      flags = BindingFlags.CreateInstance;
      try
      {
        ReturnObj = type.InvokeMember("",
                                       flags,
                                       null,
                                       null,
                                       args);
      }
      catch (Exception e)
      {
        // HX: Bug 17849
        Logger.cs_log(e.ToMessageAndCompleteStacktrace());
        ReturnObj = null;
      }
      return ReturnObj;
    }

    public object SetMemberVariableValue(object InstObj, object[] args)
    {
      // Set member variable value by passing name of the variable 
      // and the value as (KEY , VALUE) pair.
      object ReturnObj = null;
      Type t = InstObj.GetType();
      for (int j = 0; j < args.Length; j += 2)
      {
        try
        {
          ReturnObj = t.InvokeMember(args[j].ToString(),
                        BindingFlags.DeclaredOnly |
                        BindingFlags.Public |
                        BindingFlags.NonPublic |
                        BindingFlags.Instance |
                        BindingFlags.SetField,
                        null,
                        InstObj,
                        new Object[] { args[j + 1].ToString() });
        }
        catch (Exception e)
        {
          // HX: Bug 17849
          Logger.cs_log(e.ToMessageAndCompleteStacktrace());
          ReturnObj = null;
        }
      }

      return ReturnObj;
    }
  }



}
#pragma warning restore 618
