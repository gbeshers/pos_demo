/* -*- mode: c; -*- */
//#define DEBUG
//#define DEBUG_GenObj_Class
//#define DEBUG_defmethod
//#define DICT_STATS
// #define PERFORMANCE_GRAPH
// #define DEBUG_GenObj_Lock

using System;
using System.Reflection;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Compilation;
using System.Net;  //.HttpWebRequest;  //System. HttpWebRequest
using System.Threading;
using System.Globalization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web.Script.Serialization;
using System.Security.Cryptography.X509Certificates;
using System.Linq;
using System.Xml.Linq; // Bug 12098 - iPayment Baseline: Addition of Formulas [DJD]
using System.Diagnostics;
using Newtonsoft.Json.Linq;


namespace CASL_engine
{
  /// <summary>
  /// Summary description for CASLInterpreter.
  /// </summary>
  public class misc
  {

    //***********************************************************************
    // CASL evaluation functions
    //***********************************************************************

    //***********************************************************************
    // Bug# 27153 DH - Clean up warnings when building ipayment
    // CASL misc system functions 
    // content: 
    //    statement control flow (set, if, for)
    //    arithmetic operation (plus, munis, multi, divi, modulo, increment, decrement)
    //    comparison operation (is,is_not,greater_than,less_than,greater_equal_to,less_equal_to)
    //    logic operation (and,or,not, is_type_for,is_a)
    //    string operation(concat, substring)
    // note: 
    //		system function take the statement control over. So it is its responsible to 
    //		evaluate args inside of statement block. 
    //***********************************************************************

    private static char[] DIGITS = "0123456789".ToCharArray();
    private static GEO_comparer comparer = new GEO_comparer();


    //CASL_async variables
    private object thread_expression = null;
    private GenericObject thread_local_env = null;

    private Thread a_syncThread = null;


    public static GenObj_Symbol fCASL_new_GenObj_Symbol(CASL_Frame frame)
    {
      GenericObject geo_args = frame.args;
      GenericObject subject = geo_args.get("_subject", null) as GenericObject;
      Object name = subject.get("name", null);

      if (name == null)
      {
        Logger.cs_log("fCASL_new_GenObj_Symbol name is null",
                      Logger.cs_flag.error);
#if DEBUG_break
        Debugger.Break();
#endif
      }
#if code_is_not_needed
      if (name.GetType() == typeof(int)) {
        Logger.cs_log("fCASL_new_GenObj_Symbol name is int (" +
                      ((int) name).ToString() + ")", Logger.cs_flag.error);
        return new GenObj_Symbol((int) name);

      }
#endif
      if (name is string)
        return new GenObj_Symbol((string)name);
      Logger.cs_log("fCASL_new_GenObj_Symbol name is not valid (" +
                    Flt_Rec.show_object(name) + ")", Logger.cs_flag.error);
#if DEBUG_break
      Debugger.Break();
#endif
      return new GenObj_Symbol("*bogus*");
    }

    public static GenObj_Path fCASL_new_GenObj_Path(CASL_Frame frame)
    {
      GenericObject geo_args = frame.args;
      GenericObject subject = geo_args.get("_subject", null) as GenericObject;
      Object args = subject.get("args", null);

      if (args == null)
      {
        Logger.cs_log("fCASL_new_GenObj_Path args == null (" +
                      subject.show_short() + ")",
                      Logger.cs_flag.error);
#if DEBUG_break
        Debugger.Break();
#endif
      }

      return c_CASL.c_path(args);
    }

    /// <summary>
    /// CASL set system function. usage: <set> symbol=expr </set>
    /// can not set _parent, _subject, _outer_env
    /// </summary>
    /// <param name="args">_subject=_outer_env</param>
    /// <returns></returns>
    public static object CASL_set(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      object subject = geo_args.get("_subject", null);// opt _subject
      object set_subject = subject;
      if (subject == null)
        set_subject = geo_args.get("_outer_env");
      if (set_subject == null || !(set_subject is GenericObject))
        throw CASL_error.create("message", "subject must be a GEO to set", "method", c_CASL.c_object("set"), "subject", to_eng(subject));
      // should loop through __parsed_field_order rather than getKeys.  <set> 3 </set> should error (or be ignored?) -->
      ArrayList keys = geo_args.getKeys();
      foreach (object key in keys)
      {
        //exclude special system keys
        if (("_outer_env".Equals(key)
          || "_parent".Equals(key)
          || "_subject".Equals(key)
          || "_local".Equals(key)
          || (key is string && ((string)key).StartsWith("__"))))
          continue;
        (set_subject as GenericObject).set(key, geo_args.get(key));
      }
      if (subject == null)
        return subject;
      else
        return set_subject;
    }
    //		public static object CASL_set(params object[] args)
    //		{
    //			GenericObject geo_args = misc.convert_args(args);
    //			object subject = geo_args.get("_subject",null);// opt _subject
    //			if(subject == null)
    //				subject = geo_args.get("_outer_env");
    //			if(subject == null || !(subject is GenericObject))
    //				return null; //TODO: error
    //
    //			ArrayList keys = geo_args.getKeys(); //07/14 changed from string key to all key
    //			foreach( object key in keys)
    //			{
    //				if( (key.Equals("_outer_env")||key.Equals("_parent")||key.Equals("_subject")||key.Equals("_local")|| (key is string && ((string)key).StartsWith("__"))))
    //					continue;
    //				//				if(key is int)
    //				//					(subject as GenericObject).set(key, CASLInterpreter.execute_expr(geo_args.get(key),geo_args.get("_outer_env") as GenericObject)); // execute int geo_args which is not execute in advance
    //				//				else
    //				(subject as GenericObject).set(key, geo_args.get(key));
    //			}
    //			if(geo_args.has("_subject"))
    //				return subject;
    //			else
    //				return null;
    //		}
    /// <summary>
    /// CASL_set_value Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args"> _subject=req=GenericObject key=req=object value=req=object</param>
    /// <returns></returns>
    public static object CASL_set_value(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      GenericObject subject = geo_args.get("_subject") as GenericObject;
      object key = geo_args.get("key");
      object value = geo_args.get("value");
      subject.set(key, value);
      return subject;
    }

    /// <summary>
    /// CASL set system function. usage: <set> symbol=expr </set>
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    // Bug IPAY-876 MJO - Fixed so it works when called from CASL
    public static object CASL_set_parent(CASL_Frame frame)
    {
      GenericObject geo_args = misc.convert_args(frame.args);
      GenericObject local_env = frame.outer_env;
      ArrayList keys = geo_args.getStringKeys();
      string symbol_name = "_parent";
      object set_value;

      if (geo_args.has("_subject"))
      {
        GenericObject subject = geo_args.get("_subject") as GenericObject;
        if (Tool.is_symbol_or_path(subject))
        {
          subject = CASLInterpreter.execute_expr(
              CASL_Stack.Alloc_Frame(EE_Steps.standard,
                             subject, local_env)) as GenericObject;
        }

        set_value = CASLInterpreter.execute_expr(
            CASL_Stack.Alloc_Frame(EE_Steps.standard, geo_args.get(0), subject));
        subject.set(symbol_name, set_value);
      }
      else
      {
        set_value = CASLInterpreter.execute_expr(
            CASL_Stack.Alloc_Frame(EE_Steps.standard, geo_args.get(0), local_env));

        local_env.set(symbol_name, set_value);
      }
      return null;
    }

    public static object fCASL_if(CASL_Frame frame)
    {
      GenericObject geo_args = frame.args;

      Debug.Assert(frame.outer_env == geo_args.get("_outer_env"));


      if (geo_args.getLength() % 2 != 0)
      {
        string s_message = string.Format(
            "'if' is unbalanced, must have condition-action pairs." +
            " Did you forget a 'do' around multiple expression?");
        throw CASL_error.create("message", s_message,
                             "method", c_CASL.c_object("if"),
                             "content", geo_args);
      }
      CASL_Frame f1 = CASL_Stack.Alloc_Frame(EE_Steps.execute_if, frame.args,
                                             frame.outer_env);
      f1.vectors = geo_args.vectors;
      f1.inner_env = frame.outer_env;
      f1.exprs_top_level = false;

      return f1;
    }

    public static object CASL_generate_integer_range(params object[] args)
    {
      GenericObject geo_args = convert_args(args);
      int start_at = (int)geo_args.get("start_at");
      int length = (int)geo_args.get("length");
      GenericObject IV = c_CASL.c_instance("vector");//IV=interger vector 
      for (int i = start_at; i < start_at + length; i++)
      {
        IV.insert(i);
      }
      return IV;
    }

    public static object CASL_integer_for_each(params object[] args)
    {
      GenericObject geo_args = convert_args(args);

      Object obj_return = null;
      // TODO: check if there is local_env. if not, create one
      GenericObject local_env = geo_args.get("_outer_env") as GenericObject;
      int max_loops = (int)geo_args.get("_subject");
      Object returns = geo_args.get("returns", "last");
      int step = (int)geo_args.get("step");//Added step for for_each Bug#5144 PCI ANU
      bool b_return_all = false;
      GenericObject return_all_obj = null;
      Object it = null;


      if ("all".Equals(returns))
      {
        b_return_all = true;
        return_all_obj = c_CASL.c_make("vector") as GenericObject;
      }
      int loop_count = 0;
      while (loop_count < max_loops)
      {
        local_env.set("key", loop_count);
        //local_env.set("value", loop_count);

        int i = 0;
        while (i < geo_args.getLength())
        {
          it = CASLInterpreter.execute_expr(
              CASL_Stack.Alloc_Frame(EE_Steps.standard, geo_args.get(i), local_env));

          if (it is GenericObject)
          {
            GenericObject iter_result1 = it as GenericObject;
            if (iter_result1.get("_parent", null) == c_CASL.c_return)
              return iter_result1;
            else if (iter_result1.get("_parent", null) == c_CASL.c_break)
              return iter_result1.get("value", null);
          }
          i++;
        }
        // end 
        if (b_return_all)
          return_all_obj.insert(it);
        loop_count += step;//Added step for for_each Bug#5144 PCI ANU
      }
      if (b_return_all)
        obj_return = return_all_obj;
      else
        obj_return = it;
      return obj_return;
    }

    /// <summary>
    /// CASL_for_each Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args"> include='vector_key'=['vector_key_string_key_all'] returns='all'</param>
    /// <returns></returns>
    public static object CASL_for_each(params object[] args)
    {
      GenericObject geo_args = convert_args(args);

      Object obj_return = null;
      GenericObject subject = geo_args.get("_subject") as GenericObject;

      // Null subject yields null return
      if (subject == null) { return obj_return; }

      // TODO: check if there is local_env. if not, create one
      GenericObject local_env = geo_args.get("_outer_env") as GenericObject;
      object include = geo_args.get("include", "vector_key");
      /* Includes:
        * vector_key - contiguous vector keys starting with 0
        * integer_key - vector keys plus discontiguous integer keys
        * object_key - object keys
        * string_key - string keys excluding system and meta keys
        * system_key - system keys (keys starting with "_" that don't include "_f_")
        * meta_key - meta keys (keys that include "_f_")
        * all - all of the above
        */
      Object exclude = geo_args.get("exclude", c_CASL.c_GEO());
      Object returns = geo_args.get("returns", "last");
      bool lookup = (bool)geo_args.get("lookup", false);
      object if_missing = geo_args.get("if_missing", c_CASL.c_error);

      ArrayList keys = null;

      if (include is string)
      {
        ArrayList excludeList = null;
        if (exclude is GenericObject) { excludeList = (exclude as GenericObject).vectors; }
        //Determine which keys to get
        keys = subject.GetKeys(include as string, excludeList);
      }
      else if (include is GenericObject)
      {
        keys = null;
        ArrayList excludeList = null;
        if (exclude is GenericObject) { excludeList = (exclude as GenericObject).vectors; }
        if (excludeList == null)
        {
          keys = new ArrayList((include as GenericObject).vectors);
        }
        else
        {
          keys = new ArrayList();
          foreach (object key in (include as GenericObject).vectors)
          {
            if (!excludeList.Contains(key))
            {
              keys.Add(key);
            }
          }
        }
      }
      // ASSERT: keys holds a ArrayList of all the keys to look for values
      bool b_return_all = ("all".Equals(returns));
      GenericObject return_all_obj = null;
      if (b_return_all) { return_all_obj = c_CASL.c_make("vector") as GenericObject; }

      if (keys.Count == 0) return null;

      object it = null;
      bool hasKey, hasValue;
      object preKey = null, preValue = null;

      hasKey = local_env.has("key");
      if (hasKey) { preKey = local_env.get("key", null, false); }
      hasValue = local_env.has("value");
      if (hasValue) { preValue = local_env.get("value", null, false); }

      foreach (object key in keys)
      {
        local_env.set("key", key);

        object temp = subject.get(key, if_missing, lookup);

        if (temp != null && "skip".Equals(temp)) continue; // Skip on "skip" -- This could be a problem if a value is "skip" but we're ignoring it for now
        local_env.set("value", temp);

        int i = 0;
        // Execute each line in the for_each
        while (i < geo_args.getLength())
        {
          it = CASLInterpreter.execute_expr(
              CASL_Stack.Alloc_Frame(EE_Steps.standard, geo_args.get(i), local_env));

          if (it is GenericObject)
          {
            GenericObject iter_result1 = it as GenericObject;
            if (iter_result1.get("_parent", null) == c_CASL.c_return)
              return iter_result1;
            else if (iter_result1.get("_parent", null) == c_CASL.c_break)
              return iter_result1.get("value", null);
          }

          i++;
        }
        // end 
        if (b_return_all) { return_all_obj.insert(it); }
      }

      // Reset original key and value
      if (hasKey) { local_env.set("key", preKey); }
      else { local_env.remove("key", null); }
      if (hasValue) { local_env.set("value", preValue); }
      else { local_env.remove("value", null); }

      if (b_return_all)
      {
        obj_return = return_all_obj;
      }
      else
      {
        obj_return = it;
      }
      return obj_return;
    }
    /// <summary>
    /// fCASL_do Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>

    // WARNING: fCASL_do() should never be called except as a delegate
    // from execute_expr().
    public static object fCASL_do(CASL_Frame frame)
    {
      GenericObject geo_args = frame.args;

      CASL_Frame f1 = CASL_Stack.Alloc_Frame(EE_Steps.execute_expr_list,
                 frame.exp, frame.outer_env);
      Debug.Assert(frame.outer_env == geo_args.get("_outer_env"));
      f1.a_method = geo_args;
      f1.vectors = geo_args.vectors;
      f1.inner_env = frame.outer_env;
      f1.exprs_top_level = false;

      return f1;
      //      return CASLInterpreter.execute_exprs(geo_args,
      //          geo_args.get("_outer_env") as GenericObject, false);
    }

    public static object CASL_lock(params object[] args)
    {
      GenericObject geo_args = convert_args(args);
      object lock_object = geo_args.remove("on");
      object to_return = null;

      lock (lock_object)
      {
        to_return = CASLInterpreter.execute_exprs( // frame,
            geo_args,
            geo_args.get("_outer_env") as GenericObject, false);
      }
      return to_return;
    }

    /// <summary>
    /// Will return true if the objects are the same object.
    /// (The objects have the same reference)
    /// Two primitives of the same value will always return true. 
    /// 
    /// See also: CASL_equal
    /// </summary>
    /// <param name="args">_subject=req=object 0=req=object</param>
    /// <returns></returns>
    public static object CASL_is(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      Object subject = geo_args.get("_subject", CASLInterpreter.no_subject);
      Object target = geo_args.get(0);
      return Equals(subject, target);
    }

    /// <summary>
    /// Will return true if the objects are the same object
    /// or they have the same fields and values. (one level of comparison)
    /// 
    /// See also: CASL_is
    /// </summary>
    /// <param name="args">_subject=req=object 0=req=object</param>
    /// <returns></returns>
    public static object CASL_equal(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      Object subject = geo_args.get("_subject");
      Object target = geo_args.get(0);

      if (subject == null)
        return subject == target;
      else if (subject is GenericObject)
        return (subject as GenericObject).DeepEquals(target);
      else
        return subject.Equals(target);
    }


    /// <summary>
    /// Will return true if the objects are numbers
    /// AND they have the same value REGARDLESS OF TYPE 
    /// This is different from CASL_equal, which will consider the
    /// integer 123 NOT equal to the decimal 123.0
    /// 
    /// See also: CASL_equal
    /// </summary>
    /// <param name="args">_subject=req=object 0=req=object</param>
    /// <returns></returns>
    public static object CASL_equal_number(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      Object subject = geo_args.get("_subject");
      Object target = geo_args.get(0);

      double dSubject = 0, dTarget = -1;

      if (!double.TryParse(subject.ToString(), out dSubject)) { return false; }
      if (!double.TryParse(target.ToString(), out dTarget)) { return false; }

      if (dSubject == dTarget) { return true; }

      return false;
    }

    /// <summary>
    /// Opposite of 'is'
    /// 
    /// See also: CASL_is
    /// </summary>
    /// <param name="args">_subject=req=object 0=req</param>
    /// <returns></returns>
    public static object CASL_is_not(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      Object subject = geo_args.get("_subject");
      Object target = geo_args.get(0);
      if (subject != null && Tool.is_casl_primitive(subject)) // use equals for primitive instead '==' because for c# '==': different objects, same boxed value, return False
      {
        return !(subject.Equals(target));
      }
      else
        return subject != target;
    }

    /// <summary>
    /// If subject is false, return true, else return false
    /// 
    /// See also: CASL_is_not
    /// </summary>
    /// <param name="args">_subject=req=object</param>
    /// <returns></returns>
    public static object CASL_not(params object[] args)
    {
      GenericObject geo_args = convert_args(args);
      Object subject = geo_args.get("_subject");

      if (subject != null && false.Equals(subject))
        return true;
      else
        return false;
    }
    public static object CASL_and(params object[] args)
    {
      GenericObject geo_args = convert_args(args);
      GenericObject local_env = geo_args.get("_outer_env") as GenericObject;

      object R = true;

      for (int i = 0; i < geo_args.getLength(); i++)
      {
        R = CASLInterpreter.execute_expr(
            CASL_Stack.Alloc_Frame(EE_Steps.standard, geo_args.get(i), local_env));

        if (false.Equals(R))
          return false;
      }
      return R;
    }

    public static object CASL_or(params object[] args)
    {
      GenericObject geo_args = convert_args(args);
      GenericObject local_env = geo_args.get("_outer_env") as GenericObject;


      object R = false;
      for (int i = 0; i < geo_args.getLength(); i++)
      {
        R = CASLInterpreter.execute_expr(
            CASL_Stack.Alloc_Frame(EE_Steps.standard, geo_args.get(i), local_env));

        if (!false.Equals(R))
          return R;
      }
      return false;
    }
    /// <summary>
    /// if _subject is genericObject, lookup parent _name 
    /// _subject.<is_a> type="type_name"</is_a>
    /// e.g. <student_payment/>.<is_a> type=transaction</is_a>
    /// <student_payment/>.<is_a> type=GEO </is_a>
    /// </summary>
    /// <param name="args">_subject=req=GEO type=req=GEO lookup=true=Boolean same=false=Boolean</param>
    /// <returns></returns>
    public static bool base_is_a(Object subject, Object type)
    {

      if (type == null)
      {
        string s_message = string.Format("Invalid Argument, type is null");
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          Flt_Rec.log("misc.base_is_a", s_message);
        throw CASL_error.create("message", s_message, "subject", subject);
        //throw CASL_error.create(s_message);
      }
      object parent = null;
      //TODO: handle primitive type 
      if (subject == null) return false;
      if (subject is ValueType || subject is String || subject is byte[])
      {
        if (subject is Boolean && type.Equals(c_CASL.c_Boolean)) //c_CASL.c_Boolean
          return true;
        else if ((subject is int || subject is Decimal || subject is Double) && type.Equals(c_CASL.c_Number))//
          return true;
        else if (subject is int && type.Equals(c_CASL.c_Integer))//
          return true;
        else if ((subject is Decimal || subject is Double) && type.Equals(c_CASL.c_Decimal))//c_CASL.c_Decimal
          return true;
        else if ((subject is String && type.Equals(c_CASL.c_String)))//c_CASL.c_String
          return true;
        else if ((subject is byte[] && type.Equals(c_CASL.c_Bytes)))//c_CASL.c_Bytes
          return true;
        else
          return false;
      }
      else if (subject is GenericObject && (subject as GenericObject).has("_parent"))
        parent = (subject as GenericObject).get("_parent");


      if (parent == null || !(parent is GenericObject)) // parent is invalid
      {
        return false;
      }

      if (parent.Equals(type))
      {
        return true;
      }
      else
      {
        return base_is_a(parent, type);
      }
    }

    public static object fCASL_is_a_impl(CASL_Frame frame)
    {
      GenericObject geo_args = frame.args;
      Object subject = geo_args.get("_subject");
      Object type = geo_args.get("type");

      if (type == null)
      {
        string s_message = string.Format("Invalid Argument, type not found");
        throw CASL_error.create("message", s_message, "subject", geo_args,
                             "key", "type");
      }
      return base_is_a(subject, type);
    }


    public static object fCASL_declare_class(CASL_Frame frame)
    {
      GenericObject geo_args = frame.args;
      object obj_name = geo_args.get("obj_key", null);
      object obj_container = geo_args.get("container", null);
      object obj_promote = geo_args.get("promote_to_geo", null);

      if (!(obj_name is string))
      {
        Logger.cs_log("misc.cs: declare_class, obj_key is not a string " +
                      obj_name.ToString(), Logger.cs_flag.error);
        throw CASL_error.create("message", "<declare_class> obj_key must be a string");
      }
      string name = obj_name as string;

#if DEBUG_GenObj_Lock
      if (CASLInterpreter.load_complete)
        Logger.cs_log("misc.cs declare_class: **LATE** declare_class of " +
                      name, Logger.cs_flag.error);
#endif

      if (obj_container == null)
      {
        Logger.cs_log("misc.cs: declare_class, container is null",
                      Logger.cs_flag.error);
#if DEBUG_break
        Debugger.Break();
#endif
        throw CASL_error.create("message", "<declare_class> container is null");
      }
      if (!(obj_container is GenObj_Class))
      {
        Logger.cs_log("misc.cs: declare_class, container is not a GenObj_Class " +
                      obj_name.ToString(), Logger.cs_flag.error);
        throw CASL_error.create("message", "<declare_class> GenObj_Class required");
      }

      GenObj_Class container = obj_container as GenObj_Class;

      if (container == null)
      {
        Logger.cs_log("fCASL_declare_class(): for " + name +
                      " container is not a GenObj_Class" +
                      container.ToString(), Logger.cs_flag.error);
#if DEBUG_break
        Debugger.Break();
#endif
        throw CASL_error.create("message", "declare_class has undefined subject");
      }


      bool promote_to_geo = false;
      if (obj_promote is bool)
        promote_to_geo = (bool)obj_promote;
      else
        throw CASL_error.create("message", "declare_class paramter" +
                             " promote_to_geo must be a boolean");


      //
      // Verify that if we have promote_to_geo we have the same
      // object in both GEO and the primary container.
      //
      object cont_entry = container.get(name, null);

      if (promote_to_geo)
      {
        object geo_entry = c_CASL.GEO.get(name, null);
        if (geo_entry != cont_entry)
        {
          string geo_entry_str = "null";
          if (geo_entry != null)
            geo_entry_str = geo_entry.ToString();

          string cont_entry_str = "null";
          if (cont_entry != null)
            cont_entry_str = cont_entry.ToString();

          if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          {
            Flt_Rec.log("misc.cs: declare_class: ",
              "mis-matched entries " + geo_entry_str + "  " + cont_entry_str);
          }
#if DEBUG_GenObj_Class
          Debugger.Break();
#endif
        }
      }

      // We allow multiple <declare_class> calls for the same class.
      if (cont_entry != null)
      {
        if (cont_entry is GenObj_Class)
          return cont_entry;
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
        {
          Flt_Rec.log("misc.cs: declare_class: ",
            "overwriting " + name + " in container " +
            container.class_name);
        }
        throw CASL_error.create("message", "declare_class overwriting " +
                             "declaration " + name + " in container " +
                             container.class_name);
      }

      GenObj_Class new_class = new GenObj_Class();

      container.set(name, new_class);
      new_class.set("_parent", container, "_container", container, "_name", name);
      new_class.class_name = name;
      new_class.container = container;
      new_class.promote_to_GEO = promote_to_geo;

      if (frame.exp is GenObj_Code)
      {
        GenObj_Code fc = frame.exp as GenObj_Code;
        new_class.decl_loc = fc.loc;
      }

      if (promote_to_geo)
      {
        if (!c_CASL.GEO.has(name))
          c_CASL.GEO.set(name, new_class);
#if DEBUG_GenObj_Class
        else
          Logger.cs_log("declare_class: conflict promoting " + name +
                        " to GEO", Logger.cs_flag.warn);
#endif
      }
      return new_class;
    }


    /// <summary>
    /// overview:
    /// defclass is special set to define a class
    /// It can take a subject(arg._subject); or it can be used in nested defxxx object(outter_env._new_env); or default container is my.
    /// 1. _subject become _parent and _container
    /// if no _subject, use my
    /// create new_class with _parent=_subject/my and _container=_subject/my
    /// 2. _subject.<set> args._name=new_class</set>
    /// 3. evaluate the string keys under _subject/my
    /// 3. create new local_env with _new_class
    /// 4. evaluate the vector keys under new local_env
    /// test cases: 
    /// 1. GEO.<defclass> _name='sample_class' id=123</defclass>
    /// 2. GEO.<defclass> _name='sample_class' id=123 <defclass> _name='inner_class' id=123</defclass></defclass>
    /// fCASL_defclass("key1",value, "key2",value2)
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static object fCASL_defclass(CASL_Frame frame)
    {
      GenericObject geo_args = frame.args;
      GenericObject local_env = geo_args.get("_outer_env") as GenericObject;
      Object obj_name = geo_args.get("_name", null);

      if (obj_name == null)
        return false; //throw CASL_error.create("message","_name is req" ,"code",223);

      if (!(obj_name is string))
      {
        Logger.cs_log("misc.cs: defclass, _name is not a string " +
                      obj_name.ToString(), Logger.cs_flag.error);
#if DEBUG_break
        Debugger.Break();
#endif
      }
      string name = obj_name as string;

#if DEBUG_GenObj_Lock
      if (CASLInterpreter.load_complete)
        Logger.cs_log("misc.cs defclass: **LATE** defclass of " + name,
                      Logger.cs_flag.error);
#endif

      GenericObject container_and_parent = null;
      // container_or_parent is subject if avaiable or my if not.
      if (local_env.has("_new_class"))
      {
        container_and_parent = local_env.get("_new_class") as GenericObject;
      }
      else if (geo_args.has("_subject"))
      {
        container_and_parent = geo_args.get("_subject") as GenericObject;
      }
      else
      {
        throw CASL_error.create("message", "defclass must have a subject.  It could be 'my' or 'GEO'");
        //container_and_parent =  CASLInterpreter.get_my();
        //if (container_and_parent == null) throw CASL_error.create("message","Use GEO.{defclass} " + name + " because you don't have 'my' available during the system load " ,"code",268); 
      }
      if (Tool.is_symbol_or_path(container_and_parent))
      {
        CASL_Frame f1 = CASL_Stack.Alloc_Frame(EE_Steps.standard,
                                               container_and_parent, local_env);

#if DEBUG
        f1.method_name = "CASL_defclass";
#endif
        container_and_parent = CASLInterpreter.execute_expr(f1) as GenericObject;
      }
      if (container_and_parent == null) {
        Logger.cs_log("misc.cs: defclass: container_and_parent is null" +
                      " GenObj_Class " + name, Logger.cs_flag.error);
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          Flt_Rec.log("misc.cs: CASL_defclass: container_and_parent is null",
                      name);
      } else if (!(container_and_parent is GenObj_Class)) {
        Logger.cs_log("misc.cs: defclass: container_and_parent non" +
                      " GenObj_Class " + name, Logger.cs_flag.error);
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          Flt_Rec.log("misc.cs: CASL_defclass: ",
              container_and_parent.get("_name", "container with no name??") +
              " is not a GenObj_Class");
      }

      // set _parent, _container, ._name for 
      GenericObject new_class = null;
      // Unevaluated
      if (container_and_parent != null && container_and_parent.has(name))
      {
        new_class = container_and_parent.get(name) as GenericObject;
        if (new_class is GenObj_Class)
        {
          GenObj_Class nc = new_class as GenObj_Class;
          // nc.class_name = name;
          // nc.container = container_and_parent as GenObj_Class;
        }
#if DEBUG_GenObj_Class
        else {
          Logger.cs_log("misc.cs: defclass: class reuse non GenObj_Class " +
                        name + "  " + new_class.ToString(),
                        Logger.cs_flag.error);
          Debugger.Break();
        }
#endif
        new_class.removeAll();
        new_class.set("_parent", container_and_parent,
                "_container", container_and_parent,
                "_name", name);
      }
      else
      {
        if (container_and_parent is GenObj_Class)
          new_class = new GenObj_Class(name,
              container_and_parent as GenObj_Class);
        else
        {
          // REMOVE when Class hierarchy is cleaned up.
          new_class = new GenObj_Class();
          new_class.set("_parent", container_and_parent,
                "_container", container_and_parent,
                "_name", name);
        }
      }

      container_and_parent.set(name, new_class);
      if (new_class is GenObj_Class n_class &&
          container_and_parent is GenObj_Class c_and_p)
      {
        n_class.class_name = name;
        n_class.container = c_and_p;
        // FIXME dbg_name
        CASLInterpreter.dbg_class_dict_add(
            (container_and_parent as GenObj_Class).class_name + "." + name,
            (new_class as GenObj_Class));
      } else
        Logger.cs_log("fCASL_defclass: GenObj_Class hierarchy corrupted on " +
                      name +
          " is not a GenObj_Code", Logger.cs_flag.error);

      // save _code_info in the class
      if (frame.call_expr is GenObj_Code)
      {
        if (new_class is GenObj_Code)
        {
          GenObj_Code fe = frame.call_expr as GenObj_Code;
          GenObj_Code nc = new_class as GenObj_Code;
          nc.loc = fe.loc;
#if DEBUG
          // To be removed after testing of above.
          new_class.set("_code_info",
                        frame.call_expr.get("__code_info"));
#endif
        } else
          Logger.cs_log("fCASL_defclass: new_class " + name +
            " is not a GenObj_Class", Logger.cs_flag.warn);
      } else
        Logger.cs_log("fCASL_defclass: call_expr for class " + name +
          " is not a GenObj_Code", Logger.cs_flag.warn);


      // handle multiple string keys set
      GenericObject parsed_field_order = geo_args.get("__parsed_field_order") as GenericObject;
      GenericObject field_order = c_CASL.c_GEO();
      // set field order 
      new_class.set("_field_order", field_order);

      for (int i = 0; i < parsed_field_order.getLength(); i++)
      {
        object key = parsed_field_order.get(i);
        if (key is Int32 || "_parent".Equals(key) || "_container".Equals(key) || "_name".Equals(key))
          continue;
        new_class.set(key, geo_args.get(key));
        // do not include system keys or meta keys into the field order
        if (key is String &&
          (!
          (((string)key).StartsWith("_") || ((string)key).IndexOf("_f_") >= 0)
          )
          )
          field_order.insert(key); // _field_order only store string key not system key
      }

      //create new local env with _new_class
      GenericObject new_local_env = c_CASL.c_GEO();
      new_local_env.set("_new_class", new_class);

      // evaluate vector key expressions
      for (int i = 0; i < geo_args.getLength(); i++)
      {
        object value = geo_args.get(i);

        //------------------------------------------------------------
        // process field in defclass
        if (value is GenericObject && Tool.is_symbol_for((value as GenericObject).get("_parent"), "field"))
        {
          GenericObject F = value as GenericObject; // F - field object to be flatted
          new_class.set(F.get("key"), F.get("value"));
          ArrayList string_keys = F.getStringKeys();
          foreach (Object key in string_keys)
          {
            if (key.ToString() != "key" && key.ToString() != "value")
            {
              new_class.set(field_key(F.get("key"), key), F.get(key));
            }
          }
        }
        //------------------------------------------------------------
        else
        {
          CASLInterpreter.execute_expr(
              CASL_Stack.Alloc_Frame(EE_Steps.standard, geo_args.get(i), new_local_env));
        }
      }

#if DEBUG_GenObj
            if (new_class is GenObj_Class geo_cl && geo_cl.full_name() != null &&
          Flt_Rec.classes_of_interest.ContainsKey(geo_cl.full_name()))
        geo_cl.tracking = Flt_Rec.classes_of_interest[geo_cl.full_name()];
#endif
      return new_class;
    }

    public static object CASL_inspect(params object[] args)
    {
      GenericObject geo_args = convert_args(args);
      object subject = geo_args.get("_subject");
      object st_inspect = CASL_to_xml("_subject", geo_args);
      System.Console.Out.WriteLine(st_inspect);
      return subject;
    }

    public static void CreateDelegate(GenObj_CS_Method method)
    {
      string s = null;
      MethodInfo mi = method.method_info;
      if (!mi.IsStatic)
      {
        Logger.cs_log("misc.cs: Can't handle non-static _impl methods" +
                      method.cs_name, Logger.cs_flag.error);
        throw CASL_error.create("message", method.cs_name + " must be static method");
      }

      Type ty = mi.GetType();
      Type ret_ty = mi.ReturnType;
      ParameterInfo[] param_info = mi.GetParameters();

//      if (typeof(object).IsAssignableFrom(ret_ty))
      if (typeof(object) == ret_ty ||
          typeof(string) == ret_ty ||
          typeof(GenericObject) == ret_ty ||
          typeof(byte[]) == ret_ty ||
          typeof(db_vector_reader_sql) == ret_ty ||
          ret_ty.IsSubclassOf(typeof(GenericObject)) )
      {
        if (param_info.GetLength(0) == 1)
        {
          if (param_info[0].ParameterType == typeof(CASL_Frame))
          {
            try
            {
              method.method_delegate_frame = (ObjectFrameDelegate)
                Delegate.CreateDelegate(typeof(ObjectFrameDelegate), mi);
              return;
            }
            catch (Exception e)
            {
              s = "Error CreateDelegate: C# Method '" + method.cs_name +
                         "'  CASL_Frame case failed";
              // HX: Bug 17849
              Logger.cs_log(s + e.ToMessageAndCompleteStacktrace(),
                  Logger.cs_flag.error);
              throw CASL_error.create("message", s);
            }
          }
          if (param_info[0].ParameterType == typeof(Object[]))
          {
            try
            {
              method.method_delegate = (ObjectParamsDelegate)
                Delegate.CreateDelegate(typeof(ObjectParamsDelegate), mi);
              return;
            }
            catch (Exception e)
            {
              s = "Error CreateDelegate: C# Method '" + method.cs_name +
                         "'  variable params case failed";
              // HX: Bug 17849
              Logger.cs_log(s + e.ToMessageAndCompleteStacktrace(),
                  Logger.cs_flag.error);
              throw CASL_error.create("message", s);
            }
          }
        }
        s = "Error CreateDelegate: C# Method '" + method.cs_name +
          "'  must be converted to take either CASL_Frame or variable params" +
          " arguements";
        // HX: Bug 17849
        Logger.cs_log(s, Logger.cs_flag.error);
        throw CASL_error.create("message", s);
      }
      s = "C# Method, " + method.cs_name + ", return type " +
         ret_ty.Name + " is not assignable to object";
      // HX: Bug 17849
      Logger.cs_log(s, Logger.cs_flag.error);
      throw CASL_error.create("message", s);
    }

    //
    // Given a container, which sould always be a GenObj_Class, calculate
    // the path through the CASL class hierarchy.
    //
    // NOTE: _preconditon is an issue here.
    //

    public static string get_class_path(GenericObject go_container)
    // public static string get_class_path(GenObj_Class go_container)
    {
      string dbg_name = "";
      while (go_container != c_CASL.GEO)
      {
        if (!(go_container is GenObj_Class))
        {
#if DEBUG_GenObj_Class
          Logger.cs_log("misc.cs: get_class_path() container is not a " +
                        "GenObj_Class " + go_container.ToString(),
                        Logger.cs_flag.debug);
#endif
        }

        string t = go_container.get("_name", null) as string;
        if (t == null)
        {
#if DEBUG_GenObj_Class
          Logger.cs_log("misc.cs: get_class_path() container has no _name " +
                        go_container.ToString(),
                        Logger.cs_flag.debug);
#if DEBUG_break
          Debugger.Break();
#endif
#endif
          break;
        }
        if (dbg_name == "")
          dbg_name = t;
        else
          dbg_name = t + "." + dbg_name;

        GenericObject chk = go_container.get("_container", null) as GenericObject;
        if (chk == null)
        {
          // Get a better dbg_name for Boolean.ui -- but revisit as part
          // of promote_to_GEO fix.
          chk = go_container.get("_parent", null) as GenericObject;
          if (chk == null)
            break;
        }

#if DEBUG_GenObj_Class
        if (go_container is GenObj_Class &&
            (go_container as GenObj_Class).container != chk) {
          Logger.cs_log("misc.cs: get_class_path() container != _container " +
                        go_container.ToString(),
                        Logger.cs_flag.debug);
        }
#endif

#if DEBUG_GenObj_Class
        Object check = chk.get(t, null);
        if (check != go_container) {
          Logger.cs_log("misc.cs:get_class_path() consistency check failed" +
                        chk.get("_name", "*bogus*") +
                        "[" + t + "] != " + go_container.ToString(),
                        Logger.cs_flag.debug);
      }
#endif
        go_container = chk;
      }
      return dbg_name;
    }

    public static GenObj_CS_Method defmethod_impl(
        string name, GenericObject container, bool pass_outer_env,
        object other_keyed, object other_unkeyed, string method_name)
    {
      GenObj_CS_Method new_method = new GenObj_CS_Method();
      new_method.set("_name", name);
      new_method.set("_container", container);
      new_method.set("_parent", c_CASL.GEO.get("method"));
      new_method.set("_pass_outer_env", pass_outer_env);
      new_method.set("_other_keyed", other_keyed);
      new_method.set("_other_unkeyed", other_unkeyed);

      if (method_name.IndexOf("cs.") < 0)
      {
        Logger.LogError("Fix cs. leader of " + method_name, "defmethod_impl 2");
        method_name = "cs." + method_name;
      }
      new_method.cs_name = method_name;
      new_method.method_name = name;

      new_method.method_info = CASLInterpreter.get_cs_method_from_call_name(
      Tool.string_to_path_or_symbol(method_name));

      new_method.set("_impl", new_method.method_info);
      // new_method.method_index=method_index;
      container.set(name, new_method);

      // May need a full path?  GMB
      string dbg_name = "";
      if (container != null)
        dbg_name = get_class_path(container);
      if (dbg_name != "")
        dbg_name += ".";
      dbg_name += name;

      new_method.full_name = dbg_name;

#if DEBUG_GenObj
      if (Flt_Rec.methods_of_interest.Contains(dbg_name))
        new_method.tracking = true;
#endif

      new_method.loc = code_location.builtin_location;
      CASLInterpreter.dbg_meth_dict_add(dbg_name, new_method);

      CreateDelegate(new_method);
      return new_method;
    }

    public static GenObj_CS_Method defmethod_impl(
        string name, GenericObject container, GenericObject args,
        string method_name)
    {
      GenObj_CS_Method new_method = new GenObj_CS_Method();
      new_method.set("_name", name);
      new_method.set("_container", container);
      new_method.set("_parent", c_CASL.GEO.get("method"));

      if (method_name.IndexOf("cs.") < 0)
      {
        Logger.LogError("Fix cs. leader of " + method_name, "defmethod_impl 2");
        method_name = "cs." + method_name;
      }

      new_method.cs_name = method_name;
      new_method.method_name = name;

      new_method.method_info = CASLInterpreter.get_cs_method_from_call_name(
          Tool.string_to_path_or_symbol(method_name));
      new_method.set("_impl", new_method.method_info);
      ArrayList string_keys = args.getKeys();
      foreach (string key in string_keys)
        new_method.set(key, args.get(key));

      container.set(name, new_method);

      CreateDelegate(new_method);

      // May need a full path?  GMB
      string dbg_name = "";
      if (container != null)
        dbg_name = get_class_path(container);
      if (dbg_name != "")
        dbg_name += ".";
      dbg_name += name;
      new_method.method_name = name;
      new_method.full_name = dbg_name;

#if DEBUG_GenObj
      if (Flt_Rec.methods_of_interest.Contains(dbg_name))
        new_method.tracking = true;
#endif

      new_method.loc = code_location.builtin_location;
      CASLInterpreter.dbg_meth_dict_add(dbg_name, new_method);

      return new_method;
    }


    /// <summary>
    /// overview:
    /// defmethod is special set to define a method
    /// It can take a subject(arg._subject); or it can be used in nested defxxx object(outter_env._new_env); or default container is my.
    /// 1. create new_method object with _parent=method, _container as _new_class/my
    /// 2. _container.<set> args._name=new_method _impl=args._impl</set>
    /// 3. evaluate the string keys under _subject/my
    /// 3. create new local_env with _subject
    /// 4. evaluate the vector keys under new local_env
    /// test cases: 
    /// 1. GEO.<defclass> _name='sample_class' id=123</defclass>
    /// 2. GEO.<defclass> _name='sample_class' id=123 <defclass> _name='inner_class' id=123</defclass></defclass>
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static object fCASL_defmethod(CASL_Frame frame)
    {
      GenericObject geo_args = frame.args;
      GenericObject local_env = geo_args.get("_outer_env") as GenericObject;
      Object method_name_obj = geo_args.get("_name");

#if DEBUG_GenObj_Lock
      if (CASLInterpreter.load_complete)
        Logger.cs_log("misc.cs defmethod: **LATE** defmethod of " +
                      method_name.ToString() + " at "
                      + CASL_Stack.stacktrace_StrBld().ToString());
#endif

      if (is_primitive(method_name_obj).Equals(false))
      {
        method_name_obj = CASLInterpreter.execute_expr(
            CASL_Stack.Alloc_Frame(EE_Steps.standard, method_name_obj, local_env));
      }

      if (! (method_name_obj is string)) {
        Logger.cs_log("fCASL_defmethod: method_name is not a string " +
                                        method_name_obj.ToString());
        string message1 = "method_name is not a string";
        GenObj_Error err = new GenObj_Error(message1);
        err.set("subject", method_name_obj);
        throw CASL_error.create_obj(message1, err);
      }
      string method_name = method_name_obj as string;


      GenericObject container = null;
      // container_or_parent is subject if avaiable or my if not.
      if (local_env.has("_new_class"))
      {
        container = local_env.get("_new_class") as GenericObject;
        if (container == null || (!(container is GenObj_Class))) {
          Logger.cs_log("fCASL_defmethod: _new_class is not GenObj_Class " +
                        local_env.get("_new_class").ToString(),
                        Logger.cs_flag.debug);
#if DEBUG_break
          Debugger.Break();
#endif
        }
      }
      else if (geo_args.has("_subject"))
      {
        object obj_cont = geo_args.get("_subject");
        container = obj_cont as GenericObject;

        if (container == null || (!(container is GenObj_Class))) {
          if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
            Flt_Rec.log("fCASL_defmethod",
                "container " + (obj_cont as GenericObject).show_short() +
                " is not GenObj_Class for method = " +
                method_name);
          // Debugger.Break();
        }
        geo_args.remove("_subject", null);
      } else { // No Subject
        // Methods without a subject have no container and need to be
        // assigned to a local variable for use.
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          Flt_Rec.log("fCASL_defmethod",
              "no container for method = " + method_name);
        //Debugger.Break();
      }

      //else
      //{
      //throw	new	CASL_error("defmethod and defclass must have a subject.  It could be 'my' or 'GEO'");
      // container =  CASLInterpreter.get_my();
      //}
      GenericObject parent = c_CASL.GEO.get("method") as GenericObject;

      // set _parent, _container, ._name for 
      GenObj_Method new_method = null;

      // Unevaluated
      if (container != null && container.has(method_name) &&
           base_is_a(container.get(method_name), c_CASL.c_method))
      {
        new_method = container.get(method_name) as GenObj_Method;

#if DEBUG_defmethod_copies
        if (method_name is string) {
          string s = method_name as string;
          string [] bad_methods = new string[]{"logout", "post_authentication",
                     "htm_small", "ui_config", "_precondition"};
          foreach (string t in bad_methods) {
            if (s == t)
              Logger.cs_log("misc.cs: defmethod geo_args " +
                            geo_args.ToString(), Logger.cs_flag.debug);
          }
        }
#endif

#if DEBUG
        if (new_method == null)
        {
          GenericObject bad_method = container.get(method_name) as GenericObject;
          Logger.cs_log("misc.cs: Hit method reuse non GenObj_Method " +
                        method_name + "  " + bad_method.show_short(),
                        Logger.cs_flag.debug);

          if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
            Flt_Rec.log("misc.cs: def_method() ",
                        "HACK: Redefining Method " + method_name +
                        Environment.NewLine + "new_method = " +
                        bad_method.show_short() + Environment.NewLine +
                        "geo_args = " + geo_args.ToString(), true, true);
          if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
            Flt_Rec.log("         ", Environment.StackTrace);
        }
#endif
        new_method.removeAll();
      }
      else if (geo_args.has("_impl"))
      {
        new_method = new GenObj_CS_Method();
      }
      else
      {
        new_method = new GenObj_CASL_Method();
      }
      new_method.set("_parent", parent);

      if (geo_args.has("__expr") && c_CASL.get_debug())
      {
        new_method.set("_code_info",
                (geo_args.get("__expr") as GenericObject).get("__code_info"));
      }

      new_method.set("_name", method_name);
      if (new_method is GenObj_Method)
        (new_method as GenObj_Method).method_name = method_name as string;


      // REMOVE: debugging code.
      if (c_CASL.get_debug())
      {
        c_CASL.GEO.set("where", method_name);
      }

      if (geo_args.has("_args"))
      {
        GenericObject _args = CASLInterpreter.execute_expr(
            CASL_Stack.Alloc_Frame(EE_Steps.standard, geo_args.get("_args"), local_env)) as GenericObject;

        _args.remove("_local", null);
        _args.remove("_parent", null);
        //_args.remove("it",null);
        _args.remove("_subject", null);
        new_method.set(_args.getObjectArr_All());
        geo_args.remove("_args", null);
      }
      geo_args.remove("_outer_env", null);
      geo_args.remove("_local", null);

      // handle multiple string keys set
      GenericObject parsed_field_order = geo_args.get("__parsed_field_order", new GenericObject()) as GenericObject;
      GenericObject field_order = c_CASL.c_GEO();
      for (int i = 0; i < parsed_field_order.getLength(); i++)
      {
        object key = parsed_field_order.get(i);
        if (key is Int32 || "_parent".Equals(key) || "_container".Equals(key) || "_name".Equals(key))
          continue;
        object value = geo_args.get(key);
        // DANGER/SLOW: DeepEquals should not be called from C# interpreter.  Really it should directly check for whether value is a req or opt symbol
        if (key.ToString().IndexOf("_f_") >= 0 || (value != null && (c_CASL.c_req_symbol.DeepEquals(value) || c_CASL.c_opt_symbol.DeepEquals(value)))) // EVALUATED META FIELD
        {
          object eval_value = CASLInterpreter.execute_expr(
              CASL_Stack.Alloc_Frame(EE_Steps.standard, value, c_CASL.c_GEO()));// use immutable and empty evn

          new_method.set(key, eval_value);
        }
        else // IS_NOT_META_FIELD
          new_method.set(key, geo_args.get(key));
        // do not include system keys or meta keys into the field order
        if (key is String &&
          (!
          (((string)key).StartsWith("_") || ((string)key).IndexOf("_f_") >= 0)
          )
          )
          field_order.insert(key); // _field_order only store string key not system key
      }

      // set field order based on string keys
      new_method.set("_field_order", field_order);
      //create new local env with _new_object
      GenericObject new_local_env = c_CASL.c_GEO();

      // set vector key expressions (body of the method)
      for (int i = 0; i < geo_args.getLength(); i++)
      {
        object value = geo_args.get(i);

        //------------------------------------------------------------
        // process field in defmethod
        if (value is GenericObject && Tool.is_symbol_for((value as GenericObject).get("_parent"), "field"))
        {
          GenericObject F = value as GenericObject; // F - field object to be flatted
          new_method.set(F.get("key"), F.get("value"));
          ArrayList string_keys = F.getStringKeys();
          foreach (Object key in string_keys)
          {
            if (key.ToString() != "key" && key.ToString() != "value")
            {
              new_method.set(field_key(F.get("key"), key), F.get(key));
            }
          }
        }
        //------------------------------------------------------------
        else
          new_method.insert(geo_args.get(i));
      }

      if (container != null)
      {
        new_method.set("_container", container);
        new_method.container = container as GenObj_Class;
        if (container.has(method_name) && c_CASL.GEO.has("echo_redefine"))
          // <echo> "Method being overridden in container" method=method_name container=container </echo>
          misc.base_casl_call("echo_redefine",
                         CASLInterpreter.no_subject,
                         new GenericObject("_parent", c_CASL.GEO,
                              "existing_method", container.get(method_name),
                              "new_method", new_method)
            );
        container.set(method_name, new_method);
      }

      // ----------------- SPEEDUP ------------- //
      if (new_method.has("_impl"))
      {
        GenObj_CS_Method gom = new_method as GenObj_CS_Method;
        object cs_method_impl = gom.get("_impl", null);
        string cs_method_name = null;
        if (cs_method_impl is GenericObject)
        {
          if (cs_method_impl is GenObj_Symbol)
            cs_method_name = Tool.symbol_to_string(
                                       cs_method_impl as GenObj_Symbol);
          else if (cs_method_impl is GenObj_Path)
            cs_method_name = Tool.path_to_string(
                                       cs_method_impl as GenObj_Path);
          else
            Logger.cs_log("cs_method_impl is not a symbol or path " +
                          cs_method_impl.ToString(),
                          Logger.cs_flag.error);
        }
        else if (cs_method_impl is string)
        {
          cs_method_name = cs_method_impl as string;
        }
        else {
          Logger.cs_log("fCASL_defmethod: " + method_name + " bad _impl" +
                        Flt_Rec.show_object(cs_method_impl),
                        Logger.cs_flag.error);
#if DEBUG_break
          Debugger.Break();
#endif
        }

        // string cs_method_name = gom.get("_impl", null) as string;
        if (cs_method_name == null)
        {
          Logger.LogError("Method " + method_name + "  C# name " +
                          cs_method_name, " not found");
          throw CASL_error.create_str("Method " + method_name + "  C# name " +
                               cs_method_name + " not found");
        }
        try
        {
          if (cs_method_name.IndexOf("cs.") < 0)
          {
            Logger.LogError("Fix cs. leader of " + cs_method_name, "defmethod_impl 2");
            cs_method_name = "cs." + cs_method_name;
          }
          gom.cs_name = cs_method_name;
          gom.method_info = CASLInterpreter.get_cs_method_from_call_name(
                            Tool.string_to_path_or_symbol(cs_method_name));
          gom.set("_impl", gom.method_info);

          CreateDelegate(gom);

        }
        catch(Exception e)
        {
          StringBuilder message_builder = new StringBuilder();
          message_builder.Append("Method ");

          //impl_method
          message_builder.Append(gom.get("_name"));
          message_builder.Append(" (");

          GenericObject impl = gom.get("_impl") as GenericObject;
          if (impl is GenObj_Path)
          {
            message_builder.Append(impl.show_short());
          }
          else
          {
            message_builder.Append(impl.get(0)); message_builder.Append(".");
            message_builder.Append(impl.get(1)); message_builder.Append(".");
            message_builder.Append(impl.get(2)); message_builder.Append(".");
            message_builder.Append(impl.get(3));
          }
          message_builder.Append(") does not exist");
          Logger.cs_log("misc.cs: CASL_defmethod: " + message_builder.ToString(),
                                  Logger.cs_flag.error);
          // HX: Bug 17849
          Logger.cs_log("Error in misc.cs: CASL_defmethod: " +
              e.ToMessageAndCompleteStacktrace(), Logger.cs_flag.error);
        }
      }
      new_method.set("_path", new_method.to_path());

      // May need a full path?  GMB
      string dbg_name = "";
      if (String.Equals(method_name.ToString(), "_precondition", StringComparison.InvariantCulture))//Bug# 27153 DH - Clean up warnings when building ipayment
      {
        if (!((container is GenObj_Class) ||
                Tool.is_casl_class(container)))
        {
          Logger.cs_log("misc.cs: defmethod: _precondition() " +
                        container.show_short(), Logger.cs_flag.trace);
        }
      }
      if (container != null)
        dbg_name = get_class_path(container);

      if (dbg_name != "")
        dbg_name += ".";
      dbg_name += method_name;
      new_method.full_name = dbg_name;

#if DEBUG_GenObj
      if (Flt_Rec.methods_of_interest.Contains(dbg_name))
        new_method.tracking = true;
#endif

      if (frame.current_loc.isValid())
        new_method.loc = frame.loc;
      else if (frame.loc.isValid())
        new_method.loc = frame.loc;
      else if (frame.exp is GenObj_Code e && e.loc.isValid())
        new_method.loc = e.loc;

      if (! new_method.loc.isValid()) {
        new_method.loc = get_first_location(geo_args);
        if (! new_method.loc.isValid()) {
          Logger.cs_log("fCASL_defmethod: " + dbg_name +
            " code_locaton is not being set", Logger.cs_flag.error);
        }
      }

      if (new_method.full_name == null) {
        Logger.cs_log("misc.cs: defmethod: full_name not set " +
                      container.show_short(), Logger.cs_flag.error);
        Debugger.Break();
      }

      CASLInterpreter.dbg_meth_dict_add(dbg_name, new_method);
#if probably_delete
      if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
        Flt_Rec.log("misc.CASLdefmethod #1 ",
                    "new_method.full_name = " + new_method.full_name,
                    true, true);
#endif
      return new_method;
    }


    //
    // Recursively scan the code for a method looking for
    // a valid code_location.
    //

    static code_location get_first_location(GenericObject geo)
    {
      for (int i = 0; i < geo.getLength(); i++) {
        object o = geo.vectors[i];
        if (o is GenObj_Code code && code.loc.isValid())
          return code.loc;
        if (o is GenericObject geo2) {
          code_location loc = get_first_location(geo2);
          if (loc.isValid())
            return loc;
        }
      }
      return code_location.no_location;
    }

#if dead_code
    public static object CASL_defprocess(params object[] args)
    {
      GenericObject geo_args = convert_args(args);
      GenericObject local_env = geo_args.get("_outer_env") as GenericObject;
      Object name = geo_args.get("_name", null);

      if (name == null) return false;
      else name = CASLInterpreter.execute_expr(
          CASL_Stack.Alloc_Frame(EE_Steps.standard, name, local_env));

      GenericObject container = null;
      // container_or_parent is subject if avaiable or my if not.
      if (local_env.has("_new_object"))
      {
        container = local_env.get("_new_object") as GenericObject;
      }
      else if (geo_args.has("_subject"))
      {
        container = geo_args.get("_subject") as GenericObject;
      }
      else
      {
        container = CASLInterpreter.get_my();
      }
      GenericObject parent = c_CASL.GEO.get("process") as GenericObject;
      if (Tool.is_symbol_or_path(container))
        container = CASLInterpreter.execute_expr(
            CASL_Stack.Alloc_Frame(EE_Steps.standard, container, local_env)) as GenericObject;

      // set _parent, _container, ._name for 
      GenericObject new_method = null;
      if (geo_args.has("_impl"))
        new_method = new GenObj_CS_Method();
      else
        new_method = new GenObj_CASL_Method();
      new_method.set("_parent", parent, "_container", container, "_name", name);

      // FIXME: Init methods need same processing as other methods.
      if (geo_args.has("_args"))
      {
        GenericObject _args = CASLInterpreter.execute_expr(
            CASL_Stack.Alloc_Frame(EE_Steps.standard, geo_args.get("_args"), local_env)) as GenericObject;

        _args.remove("_local", null);
        _args.remove("_parent", null);
        //_args.remove("it",null);
        _args.remove("_subject", null);
        new_method.set(_args.getObjectArr_All());
      }
      container.set(name, new_method);
      if (geo_args.has("_impl") && !(new_method is GenObj_CS_Method))
      {
        Logger.cs_log("fCASL_defmethod method as \"_impl\" but new_method" +
                      " is not a GenObj_CS_Method.", Logger.cs_flag.error);
#if DEBUG_break
        Debugger.Break();
#endif
      }
      if (new_method is GenObj_CS_Method)
      {
        GenObj_CS_Method gom = new_method as GenObj_CS_Method;

        object a = geo_args.get("_impl");
        if (!(a is MethodInfo))
        {
          if (a is GenObj_Symbol)
          {
            gom.cs_name = Tool.symbol_to_string(a as GenObj_Symbol);
            a = CASLInterpreter.get_cs_method_from_call_name(a);
          }
          else if (a is GenObj_Path)
          {
            gom.cs_name = Tool.path_to_string(a as GenObj_Path);
            a = CASLInterpreter.get_cs_method_from_call_name(a);
          }
          else
            Logger.cs_log("misc.cs: CASL_defprocess: WARNING: uncertain _impl="
                                + a.ToString(), Logger.cs_flag.error);
        }
        else
        {
          Logger.cs_log("misc.cs: CASL_defprocess: WARNING: MethodInfo _impl="
                        + a.ToString(), Logger.cs_flag.error);
        }
        if (a is MethodInfo)
          gom.method_info = a as MethodInfo;
        new_method.set("_impl", a);
      }
      //if(key.Equals("_outer_env")) continue;

      // handle multiple string keys set
      ArrayList keys = geo_args.getStringKeys();
      foreach (object key in keys)
      {
        new_method.set(key, geo_args.get(key));
      }
      //create new local env with _new_object
      GenericObject new_local_env = c_CASL.c_GEO();
      new_local_env.set("_new_object", new_method);

      // evaluate vector key expressions
      for (int i = 0; i < geo_args.getLength(); i++)
      {
        new_method.set(i, geo_args.get(i));
      }
      return new_method;
    }
#endif


#if dead_code
    public static object CASL_defstatus(params object[] args)
    {
      GenericObject geo_args = convert_args(args);
      GenericObject local_env = geo_args.get("_outer_env") as GenericObject;
      Object name = geo_args.get("_name", null);

      if (name == null) return false;
      else name = CASLInterpreter.execute_expr(
          CASL_Stack.Alloc_Frame(EE_Steps.standard, name, local_env));

      GenericObject container = null;
      // container_or_parent is subject if avaiable or my if not.
      if (local_env.has("_new_class"))
      {
        container = local_env.get("_new_class") as GenericObject;
      }
      else if (geo_args.has("_subject"))
      {
        container = geo_args.get("_subject") as GenericObject;
      }
      else
      {
        container = CASLInterpreter.get_my();
      }
      GenericObject parent = c_CASL.GEO.get("status") as GenericObject;

      if (Tool.is_symbol_or_path(container))
        container = CASLInterpreter.execute_expr(
            CASL_Stack.Alloc_Frame(EE_Steps.standard, container, local_env)) as GenericObject;

      // set _parent, _container, ._name for 
      GenericObject new_method = new GenericObject();
      new_method.set("_parent", parent, "_container", container, "_name", name);
      container.set(name, new_method);
      if (geo_args.has("_impl"))
      {
        new_method.set("_impl", CASLInterpreter.execute_expr(
                           CASL_Stack.Alloc_Frame(EE_Steps.standard, geo_args.get("_impl"), local_env)));
      }


      // handle multiple string keys set
      ArrayList keys = geo_args.getStringKeys();
      foreach (object key in keys)
      {
        if ("_outer_env".Equals(key)) continue;
        string symbol_name = key.ToString();
        object set_value;
        set_value = CASLInterpreter.execute_expr(
            CASL_Stack.Alloc_Frame(EE_Steps.standard, geo_args.get(key), container));

        new_method.set(symbol_name, set_value);
      }
      //create new local env with _new_class
      GenericObject new_local_env = c_CASL.c_GEO();
      new_local_env.set("_new_class", new_method);

      // evaluate vector key expressions
      for (int i = 0; i < geo_args.getLength(); i++)
      {
        CASLInterpreter.execute_expr(
            CASL_Stack.Alloc_Frame(EE_Steps.standard, geo_args.get(i), new_local_env));
      }
      return new_method;
    }
#endif

    /// <summary>
    /// CASL misc functions Bug# 27153 DH - Clean up warnings when building ipayment
    /// content: 
    ///		Those functions will be called from CASL expression
    ///	note:
    ///	    The implementation of those functions utilize the in-memory 
    ///	    CASL Object which includes casl class/object definition.
    ///	    o get object definition by  Use getWithLookUp().
    ///	    o call casl method by execute_casl_method(). 
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static object CASL_speed_test(params object[] args)
    {
      return true;
    }
    public static object CASL_native_if(params object[] args)
    {
      bool x = false;
      if (x) return 10;
      else if (x) return 20;
      else if (x) return 30;
      else if (x) return 40;
      else if (x) return 50;
      else if (x) return 60;
      else if (x) return 70;
      else if (x) return 80;
      else if (x) return 90;
      else return 100;
    }
    /// <summary>
    /// CASL_handle_event Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args"><args>_subject=req event=req _other_args</args></param>
    /// <returns></returns>
    public static object CASL_handle_event(params object[] args)
    {
      GenericObject geo_args = convert_args(args);
      object ret_object = null;
      GenericObject instance = geo_args.get("_subject") as GenericObject;
      string event_name = geo_args.get("event").ToString();
      // call casl get_status with _subject
      GenericObject get_status_method = instance.get("get_status", null, true) as GenericObject;//get_w_lookup("get_status",null) as GenericObject;// CS=current status
      Object CS = misc.base_casl_call(
              get_status_method, instance,
              new GenericObject("_parent", c_CASL.GEO,
                                 "_subject", instance));

      GenericObject a_status_def = instance.get(CS, null, true) as GenericObject;//get_w_lookup( CS,null) as GenericObject;
      if (!a_status_def.has(event_name)) return false; // no tranisition found for current status
      GenericObject a_transition = a_status_def.get(event_name) as GenericObject;

      ///////////////////////////////////////////////////////////////
      // call precond
      GenericObject precond_method = a_transition.get("_precond") as GenericObject;
      // call casl precond with _subject and event
      Object precond_result = misc.base_casl_call(precond_method, instance,
                  new GenericObject("_subject", instance,
                                    "event", event_name,
                                    "_parent", c_CASL.GEO));

      if (false.Equals(precond_result)) return false;//or return error

      ///////////////////////////////////////////////////////////////
      // call implementation
      //			GenericObject impl_method = a_transition.get("implementation") as GenericObject;;
      // call casl implementation with _subject and event
      Object impl_result = misc.base_casl_call(a_transition, instance,
                    new GenericObject("_subject", instance,
                                      "event", event_name,
                                      "_parent", c_CASL.GEO));

      ///////////////////////////////////////////////////////////////
      // check ending_status to match with beginning_status
      Object ending_status = a_transition.get("_ending_status");
      // call casl implementation with _subject and event
      CS = misc.base_casl_call(get_status_method,
                               instance,
                               new GenericObject("_subject", instance,
                                                 "_parent", c_CASL.GEO));
      if (!ending_status.Equals(CS)) return false;

      ret_object = impl_result;
      return ret_object;
    }

    /// <summary>
    /// convert uri to expressions 
    /// 1. Handle
    /// CoreEvent.of.123.handle_event.htm?id=student_payment&qty=1&amount=10.00
    /// -> 
    /// <do>
    /// <CoreEvent.handle_event> _subject=my_opc.coreEvent event=post id=student_payment qty=1 amount=10.00 </CoreEvent.handle_event>
    /// <_subject.to_htm>_subject=it </to_htm>
    /// </do>
    /// foo.htm?x=10
    /// -> <do><foo>x=10</foo> <cs.CASLInterpreter.to_html> _subject=it </to_html></do>
    /// 2. StudentPayment.htm
    /// -> 
    /// TODO: redirect uri to root
    /// <do>
    /// <StudentPayment.to_htm></StudentPayment.to_htm>
    /// </do>
    /// </summary>
    public static object CASL_server(params object[] args)
    {
      GenericObject geo_args = convert_args(args);
      string uri = geo_args.get("uri").ToString();
      GenericObject uri_obj = uri_to_obj(uri);
      GenericObject exprs = new GenericObject("_parent", Tool.string_to_path_or_symbol("do"));

      if (uri_obj.has("_args"))
      {
        GenericObject a_call_with_args = new GenericObject();
        a_call_with_args.set((uri_obj.get("_args") as GenericObject).getObjectArr_All());
        a_call_with_args.set("_parent", Tool.string_to_path_or_symbol(uri_obj.get("call_name").ToString()));
        exprs.insert(a_call_with_args);
      }
      else
      {
        exprs.insert(Tool.string_to_path_or_symbol(uri_obj.get("call_name").ToString()));
      }
      string s_render_call = string.Format("<it.{0}>_subject=it</it.{0}>", uri_obj.get("result_render_call_name"));
      GenericObject render_call = CASLParser.decode_conciseXML(s_render_call, "");
      exprs.insert(render_call);

      object result = CASLInterpreter.execute_exprs(exprs,
         c_CASL.c_local_env(), false);
      return result;
    }
    public static object CASL_to_htm(params object[] args)
    {
      GenericObject geo_args = convert_args(args);
      object subject = geo_args.get("_subject");
      //string result = CASLParser.encode_conciseXML(subject, true);
      string result = "";
      if (subject is GenericObject)
        result = (subject as GenericObject).to_xml();
      else
        result = CASLParser.encode_conciseXML(subject, true);
      return result;
    }

    /// <summary>
    /// format object to uri as path of object filtered with uri format
    /// if has extension is fase, return uri without extension (htm, xml)
    /// </summary>
    /// 
    /// <param name="args">
    ///  _subject=req 
    ///  extension="htm"=<one_of> string false </one_of> 
    ///  base_path="/corebt/"
    ///  has_extension=true  // OLD
    ///  </param>
    /// <returns> return null if path is not found </returns>
    public static object CASL_to_uri(params object[] args)
    {
      GenericObject geo_args = convert_args(args);
      object subject = geo_args.get("_subject");
      object has_extension = geo_args.get("has_extension", true);
      object base_path = geo_args.get("base_path", "/corebt/");
      object extension = geo_args.get("extension", "htm");

      if (false.Equals(has_extension))
        extension = false;

      // TODO: implementation is similiar to path.
      object to_path_result = misc.base_casl_call("to_path", subject,
                                                  c_CASL.c_local_env());

      if (to_path_result == null || !(to_path_result is string)) return null; // throw error;
      string path = to_path_result.ToString();
      string uri = base_path + path.Replace(".", "/");
      if (!false.Equals(extension))
        uri += "." + extension;
      return uri;
    }

    public static object CASLToPath(params object[] args)
    {
      GenericObject geoArgs = convert_args(args);
      object subject = geoArgs.get("_subject");
      GenericObject root = geoArgs.get("root", c_CASL.GEO) as GenericObject;

      if (subject == null || !(subject is GenericObject)) { return false; }
      return ToPath(subject, root);
    }

    public static object ToPath(object subject, GenericObject root)
    {
      if (subject == null || !(subject is GenericObject)) { return false; }

      GenericObject geoSubject = (GenericObject)subject;


      if (geoSubject == c_CASL.GEO) // If we're looking for GEO's path
      {
        return "GEO";
      }
      else if (geoSubject == CASLInterpreter.get_my()) // my's path is always "my"
      {
        return "my";
      }

      return ToPathAux(geoSubject, root);
    }

    public static object ToPathAux(object subject, GenericObject root)
    {
      if (subject == null || !(subject is GenericObject)) { return false; }

      GenericObject geoSubject = (GenericObject)subject;

      if (geoSubject == root) { return ""; }

      object partNameKey = false;
      if (geoSubject.has("_name")) { partNameKey = "_name"; }
      else if (geoSubject.has("_part_name_key", true))
      {
        partNameKey = geoSubject.get("_part_name_key", false, true);
      }

      if (geoSubject == c_CASL.GEO)
      {
        if (root == null)
        {
          return "GEO";
        }
        else
        {
          return false;
        }
      }

      object key = false;
      string stringKey = "";
      // Setting up part name key info
      if (geoSubject.has(partNameKey))
      {
        key = geoSubject.get(partNameKey, false, true);
        if (key is string) { stringKey = format_string_key_or_path_part(key as string); }
        else { stringKey = key.ToString(); }
      }
      else { return false; }

      object CP = false; // container path
      if (geoSubject.has("_container"))
      {
        CP = ToPathAux(geoSubject.get("_container") as GenericObject, root);
        if (false.Equals(CP)) { return false; }
        else if (CP.Equals("")) { return stringKey; }
        // Average Path depth hovers around 2, so there's no point changing this to a StringBuilder
        else { return CP + "." + stringKey; }
      }
      else
      {
        object parent = geoSubject.get("_parent", false);
        if (parent is GenericObject && (parent as GenericObject).has(key))
        {
          CP = ToPathAux(parent, root);
          if (false.Equals(CP)) { return false; }
          else if ("".Equals(CP)) { return stringKey; }
          // Average Path depth hovers around 2, so there's no point changing this to a StringBuilder
          else { return CP + "." + stringKey; }
        }
      }

      return false;
    }

    ////////////////////////////////////////////////////////////////////////////
    // HTM Formatting Bug# 27153 DH - Clean up warnings when building ipayment
    ////////////////////////////////////////////////////////////////////////////
    /// <summary>
    /// define default GEO.<htm_large/> in c#, return hypertext object for any geo object
    /// <TABLE> 
    /// <TR><TH>colspan='2' .<htm_small/> <TH><TR>
    /// .<for_each returns='all' > 
    ///   <TR>  
    ///   <TD> key </TD>
    ///   <TD>value.<htm_medium/></TD>
    ///   </TR> 
    /// </for_each>
    ///				</TABLE>
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static Object CASL_htm_large(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object subject = g_args.get("_subject");

      Object hypertext = "";
      if (is_primitive(subject))
        return primitive_to_htm(subject);
      else if (is_cs_object(subject))
        return cs_object_to_htm(subject);
      else // GEO
      {
        GenericObject table = c_CASL.c_make("TABLE") as GenericObject;
        table.set("border", 1);
        GenericObject tr = c_CASL.c_make("TR") as GenericObject;
        GenericObject th = c_CASL.c_make("TH") as GenericObject;
        th.set("colspan", "2");
        th.insert(base_casl_call("htm_title", subject, c_CASL.c_local_env()));
        tr.insert(th);
        table.insert(tr);
        GenericObject g_subject = subject as GenericObject;
        object doc = "";
        if (g_subject.has("_doc"))
        {
          Object doc_subject = g_subject.get("_doc");
          doc = misc.base_casl_call("to_htm", doc_subject,
               new GenericObject("_subject", doc_subject,
                                 "extension", false,
                                 "_parent", c_CASL.GEO));
        }
        //-------------------------------------------------------
        // get content conditioning based on value.to_path() and value.is_contained_in(this)
        ArrayList keys = (subject as GenericObject).get_normalized_keys();
        foreach (object key in keys)
        {
          object value_htm_medium = null;

          //KEY
          string s_key = "";
          if (!(key is string)) s_key = "" + key; else s_key = key.ToString();
          if (s_key.StartsWith("__") || "_doc".Equals(s_key)) continue; // except special system keys and _doc

          //VALUE
          object value = (subject as GenericObject).get(key);
          if (is_primitive(value))
            value_htm_medium = primitive_to_htm(value);
          else if (is_cs_object(value))
            value_htm_medium = cs_object_to_htm(value);
          else if (Tool.is_symbol_or_path(value) || Tool.is_call("_subject", value)) // IS_EXPRESSION
            value_htm_medium = CASL_to_source_code("_subject", value);
          else // GEO
          {

            if (value.Equals(subject) && (value as GenericObject).to_path().Equals(false)) //  IS_CIRCULAR_REF AND NO_PATH
              value_htm_medium = "circular_ref"; //   
            else
            {
              object P = (value as GenericObject).to_path(); // P-> path
              if (!false.Equals(P)) //HAS_PATH
              {
                value_htm_medium = misc.base_casl_call("htm_medium", value,
                    new GenericObject("_subject", value,
                                      "_parent", c_CASL.GEO));
                //MP: causing a bug when "extension" was arg.
                // , "args",new GenericObject("_subject",value, "extension",false));			
              }
              else
              {
                if ("_container".Equals(s_key)) value_htm_medium = "circular_ref"; // except _container
                else value_htm_medium = misc.CASL_call("htm", value, new GenericObject("_subject", value));		//NO_PATH
              }
            }
          }

          GenericObject tr_key_value = c_CASL.c_make("TR") as GenericObject;
          GenericObject td_key = c_CASL.c_make("TD") as GenericObject;
          GenericObject td_value = c_CASL.c_make("TD") as GenericObject;
          td_key.insert("" + key);
          td_value.insert(value_htm_medium);
          tr_key_value.insert(td_key);
          tr_key_value.insert(td_value);
          table.insert(tr_key_value);
        }
        GenericObject div = c_CASL.c_make("DIV") as GenericObject;
        div.insert(doc);
        div.insert(table);
        hypertext = div;
        // hypertext = table;
      }
      return hypertext;
    }

    /// <summary>
    /// convert hypertext object to string with corresponding htm tag
    /// handle nested hypertext object 
    /// handle embeded vector which wraps multiple hypertext objects
    /// </summary>
    /// <param name="args"> _subject=req=hypertext </param>
    /// <returns></returns>
#if REMOVE_ME
    public static string CASLHypertextToHtm_old(params object[] argPairs)
    {
			GenericObject args = convert_args(argPairs);
			GenericObject subject = args.get("_subject") as GenericObject;
			return HypertextToHtm(subject);
		}
#endif
    public static string fCASL_HypertextToHtm(CASL_Frame frame)
    {
      GenericObject subject = frame.args.get("_subject") as GenericObject;
      return HypertextToHtm(subject);
    }

    public static string HypertextToHtm(GenericObject subject)
    {
      List<string> list = new List<string>();
      HypertextToHtmArray(subject, ref list);
      return String.Join("", list.ToArray());
    }
    public static void HypertextToHtmArray(GenericObject subject, ref List<string> list)
    {
      // The tag will always be a string
      string tag = subject.get("_name", null, true) as string;
      // Begin opening tag
      list.Add(string.Format("<{0}", tag));
#region Add attributes
      ArrayList keys = subject.getStringKeys();
      object value;
      foreach (string key in keys)
      {
        value = subject.get(key);
        if (value is GenericObject)
        {
          if (value != c_CASL.c_opt)
          {
            list.Add(String.Format(" {0}={1}", key,
                               smart_quote(base_casl_call("to_htm_attr", value,
                                                   c_CASL.c_local_env()) as string)));
          }
        }
        else
        {
          list.Add(String.Format(" {0}={1}", key, smart_quote(to_htm(value))));
        }
      }
#endregion
      // Close tag
#region Close tag
      // Bug #10205 Mike O - Close all tags that should not have end tags, to prevent Safari errors
      if ("BR".Equals(tag) ||
                "INPUT".Equals(tag) ||
                "IMG".Equals(tag) ||
                "LINK".Equals(tag) ||
                "FRAME".Equals(tag) ||
                "META".Equals(tag)
                )
      {
        list.Add("/>");
      }
      else
      {
        list.Add(">");
#region Content
        int length = subject.getLength();
        for (int i = 0; i < length; i++)
        {
          value = subject.get(i);
          if (is_primitive(value)) { list.Add(primitive_to_htm(value)); }
          else if (is_cs_object(value)) { list.Add(cs_object_to_htm(value)); }
          else if (IsHypertextGEO(value)) { HypertextToHtmArray(value as GenericObject, ref list); }
          else { list.Add(misc.base_casl_call("to_htm", value, c_CASL.c_local_env()) as string); }
        }
#endregion
        list.Add(String.Format("</{0}>", tag));
      }
#endregion
    }
    public static bool IsHypertextGEO(object value)
    {
      return (value is GenericObject) &&
        (misc.base_is_a(value, c_CASL.c_object("hypertext")));
    }

    public static string CASL_string_encode_html(params object[] args)
    {
      GenericObject geo_args = convert_args(args);
      string subject = geo_args.get("_subject") as string;
      return string_encode_html(subject);
    }

    public static string string_encode_html(string a_string)
    {
      return a_string.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;");
    }

    // Bug 17031 UMN added these so can get sql_verify to work properly with entities
    public static string CASL_string_decode_html(params object[] args)
    {
      GenericObject geo_args = convert_args(args);
      string subject = geo_args.get("_subject") as string;
      return string_decode_html(subject);
    }

    public static string string_decode_html(string a_string)
    {
      return a_string.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"");
    }

    public static object CASL_hypertext_to_memo_expr(params object[] args)
    {
      GenericObject geo_args = convert_args(args);
      object subject = geo_args.get("_subject");

      GenericObject exclude = geo_args.get("exclude") as GenericObject;
      GenericObject memo_env = geo_args.get("memo_env") as GenericObject;

      GenericObject result_vector = new GenericObject("_parent", c_CASL.GEO.get("vector"));

      // ATTRIBUTE 
      // attribute of hypertext can only be primitive
      object tag = (subject as GenericObject).get("_name", null, true);//get_w_lookup("_name",null); //Get tag name

      ArrayList keys = (subject as GenericObject).getStringKeys();
      string attribute = string.Format("<{0}", tag);
      foreach (object key in keys)
      {
        object value = (subject as GenericObject).get(key);
        if (value is GenericObject)
        { // for speed, only call casl when not a primitive
          if (base_is_a(value, c_CASL.c_expr_symbol) &&
            ((bool)exclude.key_of((value as GenericObject).get("name") as string)))
          {
            if (attribute != "") result_vector.insert(attribute);
            result_vector.insert(value);
            attribute = "";
          }
          else if (!value.Equals(c_CASL.c_opt))
          {
            attribute += " " + key + "=" + smart_quote(
                          base_casl_call("to_htm_attr", value, c_CASL.c_local_env()) as String);
          }
        }
        else
          attribute += " " + key + "=" + smart_quote(to_htm(value));
      }

      if ("BR".Equals(tag) || "INPUT".Equals(tag))
      {
        result_vector.insert(attribute + "/>");
        return base_casl_call("to_join_expr", result_vector, c_CASL.c_local_env());
        //No need to calculate content if there's no content!
      }
      else
      {
        result_vector.insert(attribute + ">");
      }

      // CONTENT 
      String content = "";
      int length = (subject as GenericObject).getLength();
      for (int i = 0; i < length; i++)
      {
        Object value = (subject as GenericObject).get(i);
        string s_value = "";

        if (is_primitive(value)) // Optimization to prevent additional call into CASL for primitive type 
          s_value = primitive_to_htm(value);
        if (is_cs_object(value))
          s_value = cs_object_to_htm(value);
        if (value is GenericObject)
        {
          if (base_is_a(value, c_CASL.GEO.get("hypertext")) ||
            base_is_a(value, c_CASL.GEO.get("vector")))
          {
            if (content != "") result_vector.insert(content);


            GenericObject inner_expr = base_casl_call("to_memo_expr", value,
                       new GenericObject("exclude", exclude,
                                         "memo_env", memo_env,
                                         "_parent", c_CASL.GEO)) as GenericObject;
            int ie_length = inner_expr.getLength();
            for (int j = 0; j < ie_length; j++)
            {
              result_vector.insert(inner_expr.get(j));
            }
            content = "";
          }
          if (base_is_a(value, c_CASL.c_expr_symbol) &&
            !(exclude.key_of((value as GenericObject).get("name") as String).Equals(false)))
          {
            if (content != "") result_vector.insert(content);
            result_vector.insert(value);
            content = "";
          }
        }
        else
          s_value = misc.base_casl_call("to_htm", value, c_CASL.c_local_env()) as String;
        content += s_value;
      }
      if (content != "") result_vector.insert(content);
      result_vector.insert(string.Format("</{0}>", tag));

      return base_casl_call("to_join_expr", result_vector, c_CASL.c_local_env());
    }


    /// <summary>
    /// basic formating function used in cs algorighm such as CASL_sort. 
    /// </summary>
    /// <param name="subject"></param>
    /// <returns></returns>
    public static Object CS_htm_small(object subject)
    {
      if (subject == null)
      {
        return "null";
      }
      else if (subject is string)
      {
        return subject;
      }
      else if (subject.GetType().IsValueType)// handle primitive type
      {
        if (true.Equals(subject)) return "true";
        else if (false.Equals(subject)) return "false";
        else return subject;
      }
      else if (subject is GenericObject && (subject as GenericObject).has("_parent"))
      {
        GenericObject geo_subject = subject as GenericObject;
        if (geo_subject.has("_name"))
          return geo_subject.get("_name");
        else
        {
          object _part_name_key = geo_subject.get("_part_name_key", c_CASL.c_error, true);
          if (geo_subject.has(_part_name_key))
            return geo_subject.get(_part_name_key);
          else
            return geo_subject.get("_name", c_CASL.c_error, true);
        }
      }
      else
        return subject.ToString();
    }

    /// <summary>
    /// Formatting function for sorting
    /// </summary>
    /// <param name="subject"></param>
    /// <returns></returns>
    public static Object CS_htm_sort(object subject)
    {
      // Handle primitives
      if (subject == null || subject.GetType().IsValueType || subject is string)
      {
        if (subject == null) return "null";
        else if (true.Equals(subject)) return "true";
        else if (false.Equals(subject)) return "false";
        else if (subject is string) return subject;
        else return subject.ToString();
      } // Handle GEOs
      else if (subject is GenericObject && (subject as GenericObject).has("_parent"))
      {
        GenericObject geo_subject = subject as GenericObject;
        if (geo_subject.has("_sort_name")) // Look at _sort_name first
          return geo_subject.get("_sort_name");
        else if (geo_subject.has("_name")) // Then just _name
          return geo_subject.get("_name");
        else
        { // Then _part_name_key
          object _part_name_key = geo_subject.get("_part_name_key", c_CASL.c_error, true);
          if (geo_subject.has(_part_name_key)) // _part_name_key
            return geo_subject.get(_part_name_key);
          else if (geo_subject.has("_sort_name", true)) // _sort_name, looking up into parents
            return geo_subject.get("_sort_name", c_CASL.c_error, true);
          else // Finally _name, looking up into parents
            return geo_subject.get("_name", c_CASL.c_error, true);
        }
      }
      else
        return subject.ToString();
    }

    /// <summary>
    /// format casl class with category
    ///     data fields
    ///     subclass (including status class)
    ///     methods
    /// </summary>
    /// <param name="casl_class"></param>
    /// <returns></returns>
    // Called by: misc.CASL_to_htm_large
    public static Object casl_class_to_htm(Object subject)
    {
      GenericObject g_subject = subject as GenericObject;
      object P = g_subject.to_path();
      StringBuilder R = new StringBuilder();
      R.Append("<LINK href='");
      R.Append(c_CASL.GEO.get("baseline_static"));
      R.Append("/ide/include/main.css' rel='stylesheet' type='text/css'></LINK>");
      if (g_subject.has("_doc"))
      {
        Object doc_subject = g_subject.get("_doc");
        string doc = (string)base_casl_call("to_htm", doc_subject,
                                            new GenericObject("_subject", doc_subject,
                                                              "_parent", c_CASL.GEO));
        R.Append("<DIV>" + doc + "</DIV>");
      }
      R.Append("<TABLE class='object class' ui='casl_class_to_htm'><TR><TH class='class' colspan=2>");
      if (P.Equals(false)) {
        long n1 = g_subject.get_gen_obj_number();
        if (n1 >= 0)
          R.Append("[" + n1.ToString() + "] ");
        R.Append("a " + ((subject as GenericObject).get("_parent") as GenericObject).to_path());
      } else {
        R.Append(P);
        long n2 = g_subject.get_gen_obj_number();
        if (n2 >= 0)
          R.Append("[" + n2.ToString() + "] ");
      }
      R.Append("</TH></TR>");
      //-------------------------------------------------------
      // get content conditioning based on value.to_path() and value.is_contained_in(this)
      // normalized_key sincludes system key, vector_key, string_key
      ArrayList keys = (subject as GenericObject).get_normalized_keys();
      // seperate data_keys subclasses_keys and methods_key
      ArrayList data_keys = new ArrayList();
      ArrayList subclasses_keys = new ArrayList();
      ArrayList methods_keys = new ArrayList();
      foreach (object key in keys)
      {
        object value = (subject as GenericObject).get(key);
        if (key.Equals("_parent") || key.Equals("_container"))// _parent and _container as data
          data_keys.Add(key);
        else if (Tool.is_casl_class(value) && (value as GenericObject).get("_container", null) == subject) // IS_SUB_CLASS
          subclasses_keys.Add(key);
        else if (Tool.is_casl_method(value)) // IS_CASL_METHOD
          methods_keys.Add(key);
        else // IS_DATA
          data_keys.Add(key);
      }
      // APPEND DATA FIELD FRIST
      foreach (object key in data_keys)
      {
        object value_htm_medium = null;
        //string s_key = ""; 
        string s_key = (string)CASL_to_htm_large("_subject", key);
        //if( !(key is string) ) s_key = ""+ key;
        //else s_key = key.ToString();
        if (s_key.StartsWith("__") || "_doc".Equals(s_key)) continue; //except special system keys
        object value = (subject as GenericObject).get(key);
        if (is_primitive(value)) value_htm_medium = primitive_to_htm(value);
        else if (is_cs_object(value)) value_htm_medium = cs_object_to_htm(value);
        else if (Tool.is_symbol_or_path(value) || Tool.is_call("_subject", value))
          value_htm_medium = CASL_to_source_code("_subject", value);
        else // GEO
        {
          if (value.Equals(subject) && false.Equals((value as GenericObject).to_path()))
            value_htm_medium = "circular_ref"; //   
          else
          {
            object PT = (value as GenericObject).to_path(); // P= path
            if (!false.Equals(PT)) // HAS_PATH
            {
              object a_uri = misc.base_casl_call("to_uri", value,
                             new GenericObject("_subject", value,
                                               "extension", false,
                                               "_parent", c_CASL.GEO));
              value_htm_medium = "<A href=\"" + a_uri + ".inspect\">" + PT + "</A>";
            }
            else  // NO_PATH
            {
              if ("_container".Equals(s_key)) value_htm_medium = "circular_ref"; // except _parent
              else value_htm_medium = CASL_to_htm_large("_subject", value);
            }
          }
        }
        //string key_type = typeof(key.GetType).ToString();  //typeof(key.GetType).ToString();  // MP temp;
        //string value_type = typeof(value.GetType).ToString();
        String value_type = "";
        if (value == null)
        {
          value_type = "System.null";
          value_htm_medium = "null";
        }
        else
          value_type = value.GetType().ToString();
        value_type = value_type.Substring(value_type.LastIndexOf(".") + 1);
        String key_type = key.GetType().ToString();
        key_type = key_type.Substring(key_type.LastIndexOf(".") + 1);

        //	if (key is Int32)			R.Append("<TR><TD class='key int'>");
        String sys_class = "";
        if (s_key.StartsWith("_")) sys_class = " class='system'";
        else if (s_key.IndexOf("_f_") != -1) // a meta-field
          sys_class = " class='meta_field'";
        //     R.Append(string.Format("<TR{0}><TD><SPAN class='key {1}'>{2}</SPAN></TD><TD class='value {3}'>{4}</TD></TR>",sys_class,key_type,s_key,value_type,value_htm_medium));
        R.Append(string.Format("<TR{0}><TD class='key {1}'>{2}</TD><TD class='value {3}'>{4}</TD></TR>", sys_class, key_type, s_key, value_type, value_htm_medium));
      }
      // APPEND SUBCLASSES 
      // Subclasses Header
      R.Append(string.Format("<TR><TH colspan=2 class='class'>Subclasses</TH></TR>"));
      foreach (object key in subclasses_keys)
      {
        object value_htm_medium = null;
        //string s_key = ""; 
        string s_key = (string)CASL_to_htm_large("_subject", key);
        object value = (subject as GenericObject).get(key);
        object PT = (value as GenericObject).to_path(); // P= path
        if (!false.Equals(PT)) // HAS_PATH
        {
          object a_uri = misc.base_casl_call("to_uri", value,
                     new GenericObject("_subject", value,
                                       "extension", false,
                                       "_parent", c_CASL.GEO));
          value_htm_medium = "<A href=\"" + a_uri + ".inspect\">" + PT + "</A>";
        }
        else  // NO_PATH
        {
          if ("_container".Equals(s_key)) value_htm_medium = "circular_ref"; // except _parent
          else value_htm_medium = CASL_to_htm_large("_subject", value);
        }
        // value_type, key_type
        String value_type = "";
        value_type = value.GetType().ToString();
        value_type = value_type.Substring(value_type.LastIndexOf(".") + 1);
        String key_type = key.GetType().ToString();
        key_type = key_type.Substring(key_type.LastIndexOf(".") + 1);

        //	if (key is Int32)			R.Append("<TR><TD class='key int'>");
        String sys_class = "";
        if (s_key.StartsWith("_")) sys_class = " class='system'";
        else if (s_key.IndexOf("_f_") != -1) // a meta-field
          sys_class = " class='meta_field'";
        //     R.Append(string.Format("<TR{0}><TD><SPAN class='key {1}'>{2}</SPAN></TD><TD class='value {3}'>{4}</TD></TR>",sys_class,key_type,s_key,value_type,value_htm_medium));
        R.Append(string.Format("<TR{0}><TD class='key {1} class'>{2}</TD><TD class='value {3}'>{4}</TD></TR>", sys_class, key_type, s_key, value_type, value_htm_medium));
      }
      // APPEND method keys
      // Methods Header
      R.Append(string.Format("<TR><TH colspan=2 class='method'>Methods</TH></TR>"));
      foreach (object key in methods_keys)
      {
        object value_htm_medium = null;
        //string s_key = ""; 
        string s_key = (string)CASL_to_htm_large("_subject", key);
        object value = (subject as GenericObject).get(key);
        object PT = (value as GenericObject).to_path(); // P= path
        if (!false.Equals(PT)) // HAS_PATH
        {
          object a_uri = misc.base_casl_call("to_uri", value,
                            new GenericObject("_subject", value,
                                              "extension", false,
                                              "_parent", c_CASL.GEO));
          value_htm_medium = "<A href=\"" + a_uri + ".inspect\">" + PT + "</A>";
        }
        else  // NO_PATH
        {
          if ("_container".Equals(s_key)) value_htm_medium = "circular_ref"; // except _parent
          else value_htm_medium = CASL_to_htm_large("_subject", value);
        }
        // value_type, key_type
        String value_type = "";
        value_type = value.GetType().ToString();
        value_type = value_type.Substring(value_type.LastIndexOf(".") + 1);
        String key_type = key.GetType().ToString();
        key_type = key_type.Substring(key_type.LastIndexOf(".") + 1);

        //	if (key is Int32)			R.Append("<TR><TD class='key int'>");
        String sys_class = "";
        if (s_key.StartsWith("_")) sys_class = " class='system'";
        else if (s_key.IndexOf("_f_") != -1) // a meta-field
          sys_class = " class='meta_field'";
        //     R.Append(string.Format("<TR{0}><TD><SPAN class='key {1}'>{2}</SPAN></TD><TD class='value {3}'>{4}</TD></TR>",sys_class,key_type,s_key,value_type,value_htm_medium));
        R.Append(string.Format("<TR{0}><TD class='key {1} method'>{2}</TD><TD class='value {3}'>{4}</TD></TR>", sys_class, key_type, s_key, value_type, value_htm_medium));
      }
      R.Append("</TABLE>");
      return R.ToString();
    }


    public static Object CASL_to_htm_medium(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object subject = g_args.get("_subject");
      Object value = g_args.get("value");

      return to_htm_medium(subject, value, 4);
    }
    // Called by: CASL_to_htm_large
    public static Object to_htm_medium(object subject, object value, int depth)  // MP TODO: take a key and container to make a true path to the object, rather than trusting that the object knows its correct path.  Watch out for non-primitive keys, though.
    {
      object value_htm_medium = null;

      if (value == null)
      {
        value_htm_medium = "null";
      }
      else if (is_primitive(value))
      {
        value_htm_medium = primitive_to_htm(value);
      }
      else if (is_cs_object(value))
      {
        value_htm_medium = cs_object_to_htm(value);
      }
      else if (Tool.is_symbol_or_path(value) || Tool.is_call("_subject", value))
      {
        value_htm_medium = CASL_to_source_code("_subject", value);
      }
      else // GEO
      {
        object PT = (value as GenericObject).to_path(); // P= path

        if (value.Equals(subject) && false.Equals(PT))
          value_htm_medium = "circular_ref";
        else
        {
          if (!false.Equals(PT)) // HAS_PATH
          {
            object a_uri = misc.base_casl_call(
                "to_uri", value,
                new GenericObject("_subject", value, "extension", false,
                                  "_parent", c_CASL.GEO));
            value_htm_medium = "<A href=\"" + a_uri + ".inspect\">" + PT + "</A>";
          }
          else  // NO_PATH - CONTAINER CHECK (circularity) DONE OUTSIDE THIS FUNCTION
          {
            value_htm_medium = CASL_to_htm_large("_subject", value, "depth", depth); // MP: Need to pass in a list of objects already shown or a depth to limit any circularity.
          }
        }
      }

      return value_htm_medium;
    }
    /// <summary>
    /// CASL_to_htm_large Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args"> _subject=req </param>
    /// <returns></returns>
    // called by: to__top and to__inspect.
    public static Object CASL_to_htm_large(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object subject = g_args.get("_subject");
      int depth = (int)g_args.get("depth", 4);

      StringBuilder R = new StringBuilder();

      Object hypertext = "";
      if (is_primitive(subject)) return primitive_to_htm(subject);
      else if (is_cs_object(subject)) return cs_object_to_htm(subject);
      else if (Tool.is_casl_class(subject))
        return casl_class_to_htm(subject);

      else // GEO
      {
        GenericObject g_subject = subject as GenericObject;
        object P = g_subject.to_path();
        R.Append("<LINK href='");
        R.Append(c_CASL.GEO.get("baseline_static"));
        R.Append("/ide/include/main.css' rel='stylesheet' type='text/css'></LINK>");
        if (g_subject.has("_doc"))
        {
          Object doc_subject = g_subject.get("_doc");
          string doc = (string)misc.base_casl_call("to_htm", doc_subject,
                                      new GenericObject("_subject", doc_subject,
                                              "_parent", c_CASL.GEO));
          R.Append("<DIV>" + doc + "</DIV>");
        }
        if (Tool.is_casl_method(subject))
          R.Append("<TABLE class='object method' ui='CASL_to_htm_large' ><TR><TH class='method' colspan=2>");
        else
          R.Append("<TABLE class='object' ui='CASL_to_htm_large' ><TR><TH colspan=2>");
        if (P.Equals(false))
        {
          GenericObject parent = ((subject as GenericObject).get("_parent") as GenericObject);
          R.Append("a " + parent.to_path());
        }
        else
          R.Append(P);
        R.Append("</TH></TR>");
        //-------------------------------------------------------
        // get content conditioning based on value.to_path() and value.is_contained_in(this)
        // normalized_key sincludes system key, vector_key, string_key
        ArrayList keys = (subject as GenericObject).get_normalized_keys();
        foreach (object key in keys)
        {
          object key_htm_medium = null;
          object value_htm_medium = null;

          string s_key = "";
          bool is_container = false;
          if (key is string)
          {
            s_key = (string)key;
            if (s_key.StartsWith("__") || "_doc".Equals(s_key)) continue; //except special system keys
            is_container = s_key.Equals("_container") || s_key.Equals("_local");
          }
          key_htm_medium = to_htm_medium(subject, key, 2);

          object value = (subject as GenericObject).get(key);

          if (is_container && (value as GenericObject).to_path().Equals(false))
          {
            value_htm_medium = "{circular reference}";
          }
          else if (value is String)
          {
            value_htm_medium = string_encode_html(value as String).Replace("\n", "<BR/>");
          }
          else
          {
            if (depth == 0)
              value_htm_medium = "Maximum depth reached";
            else
              value_htm_medium = to_htm_medium(subject, value, depth - 1);
          }

          String key_type = key.GetType().ToString();
          String value_type;

          if (value == null)
          {
            value_type = "System.null";
          }
          else
          {
            value_type = value.GetType().ToString();
          }

          value_type = value_type.Substring(value_type.LastIndexOf(".") + 1);
          key_type = key_type.Substring(key_type.LastIndexOf(".") + 1);

          String sys_class = "";
          if (key is string)
          {
            if (s_key.StartsWith("_"))
            {
              sys_class = " class='system'";
            }
            else if (s_key.IndexOf("_f_") != -1) // a meta-field
            {
              sys_class = " class='meta_field'";
            }
          }

          R.Append(
            string.Format(
            "<TR{0}><TD class='key {1}'>{2}</TD><TD class='value {3}'>{4}</TD></TR>",
            sys_class, key_type, key_htm_medium, value_type, value_htm_medium
            )
            );
        }
        R.Append("</TABLE>");
      }
      return R.ToString();
    }

    //Bug# 27153 DH - Clean up warnings when building ipayment
    ////////////////////////////////////////////////////////////////////////////
    // END OF HTM Formatting 
    ////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////
    // XML FORMATTING FUNCTIONS
    /////////////////////////////////////////////////////////////////////////

    /// <summary>
    /// dispatch formmatter
    /// </summary>
    /// <param name="args">_subject=req</param>
    /// <returns></returns>
    // Called by: GEO.to_xml_large which is called by GEO.to_xml
    /* merged MP
    public static string CASL_to_xml_large (params Object[] args)
      {
        GenericObject g_args = convert_args(args);
        Object subject = g_args.get("_subject", c_CASL.c_error);
        bool exclude_sk = (bool) g_args.get("exclude_sk", false);

        if( Tool.is_casl_class(subject)) //CLASS
          //return CASL_to_xml_large_class("_subject",subject);
          return CASL_call("a_method","to_xml_large_class" ,"_subject",subject,
            "args", new GenericObject("exclude_sk", exclude_sk) ) as string;// dispatch for instance
        else // INSTANCE
          return CASL_call("a_method","to_xml_large_inst" ,"_subject",subject,
            "args", new GenericObject("exclude_sk", exclude_sk) ) as string;// dispatch for instance
      }

      // Called by: CASL_to_xml_large
      public static string CASL_to_xml_large_inst (params Object[] args)
      {
        return CASL_to_xml_large_class(args);
      }
    */
    /// <summary>
    /// to_xml: format object to xml string except all system start with "_" "__" and keys inside _exclude_to_xml
    /// 
    /// Returns a XML String with conciseXML encoding that represents the current GenericObject
    /// reference formatting issue:
    /// check to_path && contained_in
    /// check circular reference
    /// handle all type including c# type(to_string()), primitive ('true', 'false', '1',etc..), GGE
    /// 
    /// For performance reasons, the _subject is checked for null when it could
    /// be defined on the Null_Class. 
    /// </summary>
    /// <param name="args">_subject=req</param>
    /// <returns></returns>
    /// 
    // Called by: misc.CASL_to_xml_large and GEO.to_xml_large which is called by GEO.to_xml
    public static string CASL_to_xml_large(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object subject = g_args.get("_subject", c_CASL.c_opt);
      bool exclude_sk = (bool)g_args.get("exclude_sk", false);
      int depth = (int)g_args.get("depth", 29);  // MP errors if default is c_CASL.c_error

      if (subject == c_CASL.c_opt)
      {
        // OPT can also be passed as subject
        if (subject != g_args.get("_subject", null))
        {
          string s_message = string.Format("_subject not found when calling" +
                                           " CASL_to_xml_large");
          throw CASL_error.create_str(s_message);
        }
      }

      string s_xml = "";
      //-------------------------------------------------
      // handle primitive type and c# object(method,class) xml presentation
      //-------------------------------------------------
      if (subject == null)
        return "null";
      if (is_primitive(subject))
        return primitive_to_xml(subject);
      else if (is_cs_object(subject))
        return cs_object_to_xml(subject, exclude_sk);

      GenericObject g_subject = subject as GenericObject;
      //-------------------------------------------------
      // show source code for path and symbol: path->path1.path2
      // symbol-> symbol_name
      //-------------------------------------------------

      if (g_subject.kind == GO_kind.symbol)
        return Tool.symbol_to_string(g_subject as GenObj_Symbol);
      else if (g_subject.kind == GO_kind.path)
      {
        for (int i = 0; i < g_subject.getLength(); i++)
        {
          object path_part = g_subject.get(i);
          string path_part_xml = null;
          if (path_part is String)
            path_part_xml = format_string_key_or_path_part(path_part as String);
          else
            path_part_xml = CASL_to_xml("_subject", path_part,
                                        "exclude_sk", exclude_sk);
          if (i == 0)
            s_xml += path_part_xml;
          else
            s_xml += "." + path_part_xml;
        }
        return s_xml;
      }

      //-------------------------------------------------
      // handle object (instance and class)
      // set tag name by parent.to_path() else use GEO
      //-------------------------------------------------
      object parent = g_subject.get("_parent", null);
      string tag_xml = null;
      if (parent == null)
        tag_xml = "GEO";  // NO_PARENT -> GEO
      else if (parent is GenObj_Symbol)
        tag_xml = Tool.symbol_to_string(parent as GenObj_Symbol);
      else if (parent is GenObj_Path)
        tag_xml = Tool.path_to_string(parent as GenObj_Path);
      else if (parent is GenericObject)
      {
        GenericObject g_parent = parent as GenericObject;

        object P = g_parent.to_path();

        if (false.Equals(P))
          tag_xml = "GEO";
        else if ("".Equals(P))
          tag_xml = "GEO"; // ROOT
        else
          tag_xml = P as string;
      }
      else  // parent is primitive or C# object
        tag_xml = "" + parent;

      string content_xml = "";

      // handle invalid xml tag name by move _parent to content
      // <foo."2"/>  CHANGED TO: <GEO> _parent=foo."2" </GEO>
      if (tag_xml.IndexOf("\"") >= 0)
      {
        content_xml = " _parent=" + tag_xml + " ";
        tag_xml = "GEO";
      }

      // CONTENT
      content_xml = content_xml + CASL_to_xml_large_content("_subject", g_subject, "exclude_sk", exclude_sk, "depth", depth);

      if (content_xml == "")
        s_xml = string.Format("<{0}/>", tag_xml);
      else
        s_xml = string.Format("<{0}>{1}</{0}>", tag_xml, content_xml);
      return s_xml;
    }


    /// <summary>
    /// convert to xml content
    /// </summary>
    /// <param name="args"> _subject, include, exclude, exclude_sk (sk=system keys)</param>
    /// <returns></returns>
    public static string CASL_to_xml_large_content(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object include = g_args.get("include", c_CASL.c_opt);
      Object exclude = g_args.get("exclude", c_CASL.c_opt);
      bool exclude_sk = (bool)g_args.get("exclude_sk", false);
      Object subject = g_args.get("_subject");
      int depth = (int)g_args.get("depth", 29);//c_CASL.c_error);

      //-------------------------------------------------------
      // set content xml 
      // prevent circular reference based on value.to_path() and value.is_contained_in(this)
      //-------------------------------------------------------
      GenericObject g_subject = subject as GenericObject;
      ArrayList keys = g_subject.get_normalized_keys();
      GenericObject exclude_keys = null;
      if (!c_CASL.c_opt.Equals(exclude))
      {
        exclude_keys = exclude as GenericObject;
        exclude_keys = exclude_keys.make_reverse();
      }
      //TODO: include
      string content_xml = "";
      foreach (object key in keys)
      {
        //-------------------------------------------------------
        // set key xml 
        //-------------------------------------------------------
        string key_xml = "";
        if (!(key is string)) key_xml = "" + key;
        else key_xml = key.ToString();

        if ("_parent".Equals(key_xml) || "_container".Equals(key_xml) || key_xml.StartsWith("__")) continue; // except all system keys

        if (exclude_sk && key_xml.StartsWith("_") && !key_xml.Equals("_subject")) continue; // except all system keys
        if (exclude_keys != null && exclude_keys.has(key)) continue;// handle _exclude_in_xml
        //-------------------------------------------------------
        // set value xml 
        //-------------------------------------------------------
        string value_xml = "";
        object value = g_subject.get(key);
        if (is_primitive(value))
          value_xml = primitive_to_xml(value);
        else if (is_cs_object(value))
          value_xml = cs_object_to_xml(value, exclude_sk);
        else //GEO
        {
          GenericObject g_value = value as GenericObject;
          if (value.Equals(subject)) //CIRCULAR REF
            continue; // skip circular_ref key_and_value_pair
          else if (depth <= 0)
          {
            /*
            Logger.cs_log("misc.cs: format_error_too_deep. Throwing error. key: " + key_xml + " value: " + content_xml);
            throw CASL_error.create_str("format_error_too_deep. content_xml: " + content_xml);
            value_xml="format_error_too_deep";
						*/
            value_xml = "max depth found";
          }
          else
            value_xml = to_xml(g_subject, value as GenericObject, exclude_sk, depth - 1);
        }

        // assume value_xml and key_xml
        if ((key is Int32) && ((Int32)key < g_subject.getLength()))  //VECTOR_KEY
          content_xml += "\n" + value_xml;
        else
          content_xml += "\n" + format_string_key_or_path_part(key_xml) + "=" + value_xml;
      }
      return content_xml;
    }

    public static string CASL_to_js_key(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      // Bug #7354 Mike O - Restrict to string keys
      object key = args.get("_subject", null);
      if (!(key is string))
      {
        string message = "to_js_key only supports string keys";
        GenericObject error = new GenericObject();
        error.set("_subject", key);
        error.set("message", message);
        CASL_error_throw(error.getObjectArr());
        return message;
      }
      // End Bug #7354
      string a_string = args.get("_subject", "") as string;
      bool has_weird_chars_in_path = false;
      char[] chars = a_string.ToCharArray();
      foreach (char a_char in chars)
      {
        if (!(Char.IsLetterOrDigit(a_char) || a_char.Equals('_')))
        {
          has_weird_chars_in_path = true;
          break;
        }
      }
      if (a_string.Length > 0 && (has_weird_chars_in_path || Char.IsDigit(a_string, 0)))
        return CASL_to_xml("_subject", a_string);  // quote the path part
      else
        return $"\"{a_string}\"";
      // end copied
    }

    public static string CASL_format_string_key_or_path_part(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      string a_string = args.get("_subject", "") as string;
      return format_string_key_or_path_part(a_string);
    }

    // Called by: ToPath, CASL_to_xml_large_class, CASL_to_xml_large_content, CASL_to_xmli_large 
    public static string format_string_key_or_path_part(string a_string)
    {
      bool has_weird_chars_in_path = false;
      char[] chars = a_string.ToCharArray();
      int n = 0;
      while (n < chars.Length)
      {
        char a_char = chars[n];
        if (!(Char.IsLetterOrDigit(a_char) || a_char.Equals('-') || a_char.Equals('_')))
        {
          has_weird_chars_in_path = true;
          break;
        }
        n++;
      }
      if (a_string.Length > 0 && (has_weird_chars_in_path || Char.IsDigit(a_string, 0)))
        return CASL_to_xml("_subject", a_string);  // quote the path part
      else
        return a_string;
      // end copied
    }

    /// <summary>
    /// cs convienience method , called by CASL_to_xml_class
    /// </summary>
    /// <param name="container"></param>
    /// <param name="subject"></param>
    /// <returns></returns>
    // Called by: CASL_to_xml
    public static string to_xml(GenericObject container, GenericObject obj)
    {
      return to_xml(container, obj, false, 4);
    }

    // Called by: ???
    public static string to_xml(GenericObject container, GenericObject obj, bool exclude_sk, int depth)
    {
      Object P = ToPath(obj, c_CASL.GEO);
      if (!(false.Equals(P)) && !obj.is_contained_in(container)) // HAVE_PATH AND NOT_CONTAINED -- classes and instances
        return P as string;
      else // NO_PATH  or HAVE_PATH AND CONTAINED 
        return CASL_to_xml_large("_subject", obj, "exclude_sk", exclude_sk, "depth", depth);
    }

    /// <summary>
    /// CASL_to_xml Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static string CASL_to_xml(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object subject = g_args.get("_subject", c_CASL.c_opt);
      bool exclude_sk = (bool)g_args.get("exclude_sk", false);
      Object P = ToPath(subject, c_CASL.GEO);
      if (!false.Equals(P)) // HAVE_PATH
        return P as string;
      else
      {  // NO_PATH
        GenericObject l_env = c_CASL.c_GEO();
        l_env.set("exclude_sk", exclude_sk);
        return base_casl_call("to_xml_large", subject, l_env) as string;
      }
    }


    /// <summary>
    /// non-customized format for XML , no casl dispath at all 
    /// raw xml includes all system key except "__" and 
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static string CASL_to_xmli_large(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object subject = g_args.get("_subject", c_CASL.c_opt);

      if (subject == c_CASL.c_opt)
      {
        if (subject != g_args.get("_subject", null))	//	OPT	can	also	be	passed	as	subject
        {
          string s_message = string.Format("_subject	not	found	when	calling	CASL_to_xmli_large");
          throw CASL_error.create_str(s_message);
        }
      }

      string s_xml = "";
      //-------------------------------------------------
      // handle primitive type and c# object(method,class) xml presentation
      //-------------------------------------------------
      if (subject == null)
        return "null";
      if (is_primitive(subject))
        return primitive_to_xml(subject);
      else if (is_cs_object(subject))
        return cs_object_to_xml(subject);

      GenericObject g_subject = subject as GenericObject;
      //-------------------------------------------------
      // show source code for path and symbol: path->path1.path2 symbol-> symbol_name
      //-------------------------------------------------
      if (g_subject.kind == GO_kind.symbol)
        return Tool.symbol_to_string(g_subject as GenObj_Symbol);
      else if (g_subject.kind == GO_kind.path)
      {
        for (int i = 0; i < g_subject.getLength(); i++)
        {
          string path_part_xml = CASL_to_xmli_large("_subject", g_subject.get(i));
          path_part_xml = path_part_xml.Replace("'", "");
          path_part_xml = path_part_xml.Replace("\"", "");
          if (i == 0)
            s_xml += path_part_xml;
          else
            s_xml += "." + path_part_xml;
        }
        return s_xml;
      }

      //-------------------------------------------------
      // handle object (instance and class)
      // set tag name by parent.to_path() else use GEO
      //-------------------------------------------------
      object parent = g_subject.get("_parent", null);
      string tag_xml = null;
      if (parent == null) tag_xml = "GEO";  // NO_PARENT -> GEO 
      else if (parent is GenericObject) // GEO
      {
        GenericObject g_parent = parent as GenericObject;
        //-------------------------------------------------
        // show source code in casl:  call-> <call_name/>
        //-------------------------------------------------
        if (Tool.is_symbol_or_path(g_parent))
          tag_xml = CASL_to_xmli_large("_subject", g_parent);
        else
        {
          object P = g_parent.to_path();
          if (false.Equals(P)) tag_xml = "GEO";
          else if ("".Equals(P)) tag_xml = "GEO"; // ROOT
          else tag_xml = P as string;
        }
      }
      else  // parent is primitive or C# object
        tag_xml = "" + parent;

      string content_xml = "";
      // handle invalid xml tag name by move _parent to content
      // <foo."2"/>  CHANGED TO: <GEO> _parent=foo."2" </GEO>
      if (tag_xml.IndexOf("\"") >= 0)
      {
        content_xml = " _parent=" + tag_xml + " ";
        tag_xml = "GEO";
      }
      //-------------------------------------------------------
      // set content xml 
      // prevent circular reference based on value.to_path() and value.is_contained_in(this)
      //-------------------------------------------------------
      ArrayList keys = g_subject.get_normalized_keys();
      foreach (object key in keys)
      {
        //-------------------------------------------------------
        // set key xml 
        //-------------------------------------------------------
        string key_xml = "";
        if (!(key is string)) key_xml = "" + key;
        else key_xml = key.ToString();
        if ("_parent".Equals(key_xml) || key_xml.StartsWith("__")) continue; // except _parent and special system keys

        //-------------------------------------------------------
        // set value xml 
        //-------------------------------------------------------
        string value_xml = "";
        object value = g_subject.get(key);
        if (value is GenericObject)
        {
          GenericObject g_value = value as GenericObject;
          if (g_value.Equals(g_subject))
            continue; // skip circular_ref key_and_value_pair
          else
          {
            Object P = ToPath(g_value, c_CASL.GEO);
            if (!false.Equals(P))  // have path
            {
              if (g_value.is_contained_in(g_subject)) // contained =true
                value_xml = CASL_to_xmli_large("_subject", g_value);
              else // not contained
                value_xml = P.ToString();
            }
            else // no path
            {
              if ("_container".Equals(key)) value_xml = "circular_ref"; // except _parent
              else value_xml = CASL_to_xmli_large("_subject", g_value);
            }
          }
        }
        else
          value_xml = CASL_to_xmli_large("_subject", value).ToString();
        if ((key is Int32) && ((Int32)key < g_subject.getLength()))
          content_xml += "\n" + value_xml;
        else
          content_xml += "\n" + format_string_key_or_path_part(key_xml as String) + "=" + value_xml + " ";
      }
      if (content_xml == "")
        s_xml = string.Format("<{0}/>", tag_xml);
      else
        s_xml = string.Format("<{0}>{1}</{0}>", tag_xml, content_xml);
      return s_xml;
    }

    /// <summary>
    /// fancy concatenation, typical used by casl for customization 
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static string CASL_format_xml(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object tag_name = g_args.get("tag_name", "GEO");
      Object attributes = g_args.get("attributes", "");
      Object content = g_args.get("content", "");

      string R = string.Format("<{0} {1}>{2}</{0}>", tag_name, attributes, content);
      return R;
    }

    // Bug# 27153 DH - Clean up warnings when building ipayment
    /////////////////////////////////////////////////////////////////////////
    // END OF XML FORMATTING FUNCTIONS
    /////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////
    // OTHER Formatting 
    ////////////////////////////////////////////////////////////////////////////
    /// <summary>
    /// english presentation of an object, use as error message 
    /// includes primitive, cs object, expression, GEO
    /// </summary>
    /// <param name="subject"></param>
    /// <returns></returns>
    public static string to_eng(object subject)
    {
      string it = "";
      if (is_primitive(subject))
        return primitive_to_xml(subject);

      object P = ToPath(subject, c_CASL.GEO);
      if (!false.Equals(P)) //HAVE_PATH
        it = P as string;
      else if (Tool.is_call("_subject", subject) || Tool.is_symbol_or_path(subject))
      {
        // The try block is only used if there is an error during
        // CASL initialization before all the CASL classes with their
        // respective to_xml() methods are loaded.  This try block
        // ignores the lack of an appropriate to_xml() or to_xml_large()
        // method and reports the original error.
        try
        {
          it = misc.CASL_to_xml("_subject", subject);
        }
        catch (Exception e)
        {
          it = subject.ToString();
          // HX: Bug 17849
          Logger.cs_log(e.ToMessageAndCompleteStacktrace());
        }
        it = it.Replace("<", "{");
        it = it.Replace(">", "}");
      }
      else
        it = "an object.";
      return it;
    }

    // Bug 17061 UMN PCI DSS Mask some info so safe to have show_verbose on in prod sites
    public static string to_eng_masked(string key, object value)
    {
      // I could use a regex here, but may be harder for others to maintain as it changes
      // Note: this is very crude, and therefore will have false positives that are masked
      if (key.Contains("password") ||
          key.Contains("credit") ||
          key.Contains("bank") ||
          key.Contains("cvv") ||
          key.Contains("exp_") ||
          key.Contains("login") ||
          key.Contains("logon") ||
          key.Contains("track"))
      {
        return "** masked **";
      }
      return to_eng(value);
    }

    // Bug# 27153 DH - Clean up warnings when building ipayment
    ////////////////////////////////////////////////////////////////////////////
    // END OF Formatting 
    ////////////////////////////////////////////////////////////////////////////

    public static string prompt_htm_large_instance(params object[] args)
    {
      GenericObject geo_args = convert_args(args);
      object subject = geo_args.get("_subject");

      string result = CASLParser.encode_conciseXML(subject, true);
      return result;
    }

    /// <summary>
    /// convert uri to object 
    /// <GEO> call_name =req result_render_call_name=req _args=opt</GEO>
    /// </summary>
    /// <param name="uri"> e.g. "CoreEvent/addtransaction.htm?id=student_payment&qty=1&amount=10.00"</param>
    /// <returns></returns>
    public static GenericObject uri_to_obj(string uri)
    {
      GenericObject expr_for_uri = c_CASL.c_GEO();

      //----------------------------------------------
      // Find "?" in uri to seperate query string and uri path
      //--------------------------------------------------
      int index = uri.IndexOf("%3f");
      if (index < 0) index = uri.Length;

      string uri_path = uri.Substring(0, index);
      uri_path = HttpUtility.UrlDecode(uri_path);

      string query_string = "";
      if (index == uri.Length)
        query_string = "";
      else
        query_string = uri.Substring(index + 3);

      //			string[] uri_parts= uri.Split('?');// '?'
      //			string uri_path = uri_parts[0];
      string[] uri_path_parts = uri_path.Split('.');
      string call = uri_path_parts[0];
      call = call.Substring(call.IndexOf("corebt"));
      call = call.Replace("corebt/", "");
      call = call.Replace("/", ".");
      Object subject = "";
      if (call.IndexOf(".") >= 0)
        subject = call.Substring(0, call.LastIndexOf("."));

      expr_for_uri.set("call_name", call);
      string result_render_call = "to_" + uri_path_parts[1] + "_top";
      expr_for_uri.set("result_render_call_name", result_render_call);
      if (uri.IndexOf("%3f") >= 0)
      {
        string args = query_string;
        //			string args = uri_parts[1];
        GenericObject obj_args = CASLParser.decode_conciseURI(args, true) as GenericObject;
        if (!"".Equals(subject) && !obj_args.has("_subject")) // add subject if not explicit subject in args
        {
          subject = Tool.string_to_path_or_symbol(subject.ToString());
          obj_args.set("_subject", subject);
        }
        expr_for_uri.set("_args", obj_args);
      }
      return expr_for_uri;
    }

    public static Object CASLJoin(params object[] argPairs)
    {
      GenericObject args = convert_args(argPairs);
      object subject = args.get("_subject", null);
      object formatter = args.get("formatter", null);
      object separator = args.get("separator", String.Empty);
      bool skipEmpty = (bool)args.get("skip_empty", true); // Bug #14079 Mike O - Added skip_empty argument

      ArrayList argList = null; // Capacity of argList is based on what the subject actually is.
      if (subject != null && subject != c_CASL.c_opt && !subject.Equals(String.Empty))
      {
        if (subject is string)
        {
          argList = new ArrayList(1 + args.getLength());
          argList.Add(subject);
        }
        else if (subject is GenericObject)
        {
          GenericObject subjectGeo = (GenericObject)subject;
          argList = new ArrayList(subjectGeo.getLength() + args.getLength());
          argList.AddRange(subjectGeo.vectors);
        }
      }
      else
      {
        argList = new ArrayList(args.getLength());
      }
      argList.AddRange(args.vectors);
      // Bug #14079 Mike O - Added skip_empty argument
      return Join(argList.ToArray(), formatter, separator, skipEmpty);
    }

    // Bug #14079 Mike O - Added skipEmpty argument
    public static string Join(object[] args, object formatter, object separator, bool skipEmpty)
    {
      List<string> stringList = new List<string>();

      string separatorString = null;
      if ((separator is string) || (formatter == null)) { separatorString = separator.ToString(); }
      else
      {
        separatorString = base_casl_call(formatter, separator,
                  c_CASL.c_local_env()).ToString();
      }

      foreach (object arg in args)
      {
        // Add the item if it is a non-opt, non-null, non-empty string
        // Bug #14079 Mike O - Allow empty string if skipEmpty is false
        if (arg != null && arg != c_CASL.c_opt && (!skipEmpty || !arg.Equals(String.Empty)))
        {
          if (formatter == null) stringList.Add(to_htm(arg));
          else stringList.Add((base_casl_call(formatter, arg,
                        c_CASL.c_local_env())).ToString());
        }
      }
      return String.Join(separatorString, stringList.ToArray());
    }

    public static string returnBinary(params object[] args_pairs)
    {
      GenericObject args = convert_args(args_pairs);
      int intValue = (int)args.get("a_value");
      return Convert.ToString(intValue, 2);
    }

    // Bug# 27153 DH - Clean up warnings when building ipayment
    // Returns index of first difference between _subject
    // and a_value strings, -1 if no differences
    public static Object c_string_diff(params object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);

      char[] a = args.get("_subject", "").ToString().ToCharArray();
      char[] b = args.get("a_value", "").ToString().ToCharArray();

      int min = Math.Min(a.Length, b.Length);

      int result = -1;

      for (int x = 0; x < min; x++)
      {
        if (!a[x].Equals(b[x])) { return x; }
      }

      return result;
    }

    public static object CASLConcat(params object[] argPairs)
    {
      GenericObject args = convert_args(argPairs);
      int length = args.getLength();
      object[] cArgs = new object[length + 1];
      cArgs[0] = args.get("_subject", null);
      args.vectors.CopyTo(cArgs, 1);
      /*
        for(int i = 0; i < length; i++) {
          cArgs[i + 1] = args.get(i);
        }*/
      return Concat(cArgs);
    }
    public static string Concat(object[] args)
    {
      string[] strings = new string[args.Length];
      object o;
      for (int i = 0; i < args.Length; i++)
      {
        o = args[i];
        if (o == null) { strings[i] = String.Empty; }
        else if (o is string || o.GetType().IsValueType)
        {
          if (o.Equals(true)) { strings[i] = "true"; }
          else if (o.Equals(false)) { strings[i] = "false"; }
          else { strings[i] = o.ToString(); }
        }
        else { strings[i] = base_casl_call("to_htm", o, c_CASL.c_local_env()) as string; }
      }
      return string.Join("", strings);
    }

    public static object CASL_Number_htm_medium(params object[] args)
    {
      GenericObject geo_args = convert_args(args);
      object subject = geo_args.get("_subject");
      return number_to_htm(subject);
    }


    // Bug 17062 UMN Removed CASL_set_cookie, since the code is old and wrong with new PCI requirements. If needed
    // retrieve CASL_set_cookie from SVN and make it PCI-complaint (See 17062 and related bugs). I have not done so
    // because CASL_set_cookie is not used in our code.

    // Bug 17062 UMN Removed CASL_get_cookie, since the code is old and wrong with new PCI requirements. If needed
    // retrieve CASL_get_cookie from SVN and make it PCI-complaint (See 17062 and related bugs). I have not done so
    // because CASL_get_cookie is not used in our code.

    /// <summary>
    /// UNUSED in CASL, USED in cs formatting functions 
    /// convert object to string 
    /// 1. primary type 
    /// 2. GenericObject
    /// </summary>
    /// <param name="args">_subject=req</param>
    /// <returns></returns>
    public static string CASL_to_string(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object subject = g_args.get("_subject");
      string result = "";
      if (subject == null || subject.GetType().IsValueType || subject is string)// handle primitive type
      {
        if (subject == null) result = "null";
        else if (true.Equals(subject)) result = "true";
        else if (false.Equals(subject)) result = "false";
        else if (subject is int || subject is Decimal || subject is Double) // IS_NUMBER
          result = CASL_Number_htm_medium("_subject", subject) as string;
        else result = "" + subject;
      }
      else if (subject is GenericObject)
      {
        if (c_CASL.c_opt.Equals(subject)) result = "";
        else if (c_CASL.c_req.Equals(subject)) result = "";
        else
        {
          Object P = (subject as GenericObject).to_path(); // P-> path
          if (!false.Equals(P))
          {
            if ("".Equals(P)) result = "GEO";
            else result = P.ToString();
          }
          else
            result = "GEO";
        }
      }
      else
        result = subject.ToString();
      return result;
    }

    /// <summary>
    /// convert_args translates an array of arguments into a new GenericObject.
    /// If there is a single entry into the list and that entry is a valid
    /// GenericObject that object is returned.  Otherwise, if there is an even
    /// number of entries in the array the array is used as an argument to the
    /// GenericObject constructor.
    /// </summary>
    /// <param name="args">The array of arguments to make into a normal GenericObject</param>
    /// <returns>The resulting GenericObject based on the arguments</returns>
    public static GenericObject convert_args(params Object[] args)
    {
      // One single argument of GenericObject type is returned
      if (args.Length == 1 && args[0] is GenericObject)
      {
        return args[0] as GenericObject;
      }
      return new GenericObject(args);
    }

    /// <summary>
    /// e.g. geo.<remove/>; geo.<remove> "a_key"</remove>
    /// </summary>
    /// <param name="args"> _subject=req=GEO key=opt=object shift=true=bool if_missing=error</param>
    /// <returns></returns>
    public static object CASL_remove(params object[] args)
    {
      GenericObject geo_args = convert_args(args);
      GenericObject subject = geo_args.get("_subject") as GenericObject;
      object key = geo_args.get("key", c_CASL.c_opt);
      bool shift = (bool)geo_args.get("shift", true);
      object if_missing = geo_args.get("if_missing", c_CASL.c_error);

      if (key == c_CASL.c_opt)
        return subject.remove();
      else
        return subject.remove(key, shift, if_missing);
    }
    public static object CASL_remove_all(params Object[] args)
    {
      GenericObject geo_args = convert_args(args);
      GenericObject subject = geo_args.get("_subject") as GenericObject;
      if (subject == null)
      {
        string s_message = string.Format("Invalid Argument, subject not found");
        throw CASL_error.create("message", s_message, "subject", geo_args, "key", "subject");
      }
      subject.removeAll();
      return null;
    }

    /// <summary>
    /// e.g. <to_path_or_symbol> 'a_string' </to_path_or_symbol>
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static object CASL_to_path_or_symbol(params object[] args)
    {
      GenericObject geo_args = convert_args(args);
      object s_path = geo_args.get(0);
      return Tool.string_to_path_or_symbol(s_path.ToString());
    }

    public static object CASL_get_path(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      GenericObject subject = (GenericObject)g_args.get("_subject", c_CASL.GEO) as GenericObject;
      object path = g_args.get("path");
      bool lookup = (bool)g_args.get("lookup", false);
      object if_missing = g_args.get("if_missing", false);

        // IPAY-325: GMB maintain existing semantics that GenObj_Symbol, GenObj_Path
        // return false.

      if (path is string path_str)
        return subject.get_path(path_str, lookup, if_missing);
      return false;
    }
    //		public static object CASL_get_path_2(params Object[] args)
    //		{
    //			GenericObject g_args = convert_args(args);
    //			GenericObject _subject = (GenericObject)g_args.get("_subject", c_CASL.c_undefined);
    //			string path = g_args.get("path") as string;
    //			bool lookup= (bool)g_args.get("lookup",false);
    //			object if_missing= g_args.get("if_missing",false);
    //			return _subject.get_path_2(path,lookup,if_missing);
    //		}
    /// <summary>
    /// CHANGE: In 'get', parameter 'default' was renamed to 'if_missing'
    /// </summary>
    /// <param name="args">_subject=req=object lookup=false=boolean if_missing=false=object 0=req=object</param>
    /// <returns> object </returns>
    public static object fCASL_get(CASL_Frame frame)
    {
      GenericObject g_args = frame.args;
      object _subject = g_args.get("_subject", c_CASL.c_undefined);
      bool lookup = (bool)g_args.get("lookup", false);
      object if_missing = g_args.get("if_missing", false);
      object key = g_args.get(0, null);

      return base_get(_subject, key, lookup, if_missing);
    }


    public static object base_get(
        object _subject,
        object key,
        bool lookup,
        object if_missing)
    {
      //------------------------------------
      // TODO: implemente validation
      //------------------------------------
      if (_subject != null && c_CASL.c_undefined.Equals(_subject))
      {
        return misc.handle_if_missing(if_missing, key, _subject as GenericObject);
      }

      //------------------------------------
      // convert c# primitive object to casl object(String, Number, Boolean, Null_Class)
      //------------------------------------
      GenericObject g_subject = convert_primitive_to_casl_object(_subject);
      return g_subject.get(key, if_missing, lookup);
    }

    /// <summary>
    /// CASL_get_type Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args"> _subject=req key=req</param>
    /// <returns></returns>
    public static object CASL_get_type(params object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object _subject = g_args.get("_subject", c_CASL.c_undefined);
      Object key = g_args.get("key", c_CASL.c_undefined);
      Boolean config = (bool)g_args.get("config", false); // Bug 15220 MJO - Added special config type
      if (_subject is GenericObject)
      {
        // Bug 15220 MJO - Added special config type
        return (_subject as GenericObject).get_type(key, config);
      }
      else
        return false;
    }
    /// <summary>
    /// CASL_insert Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args">_subject=req=GEO, 0=req=object</param>
    /// <returns></returns>
    public static object CASL_insert(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      GenericObject subject = g_args.get("_subject") as GenericObject;
      object at_key = g_args.get("at_key");
      int arg_length = g_args.getLength();
      object value = null; // g_args.get(0);
      // algorithm: for_each 0 1 2 | 3 4 5
      // loop from at_key to end, set each to index + length
      // loop from at_key to end and set each to g_args
      if (at_key == c_CASL.c_opt)
      {
        for (int i = 0; i < arg_length; i++)
        {
          value = g_args.get(i);
          subject.insert(value);
        }
      }
      else
      {// shift values to the right
        int at_key2 = (int)at_key;
        for (int i = subject.getLength() - 1; i >= at_key2; i--)
        {
          subject.set(i + arg_length, subject.get(i));
        }
        // add new values
        int L = g_args.getLength();
        for (int i = 0; i < L; i++)
        {
          subject.set(i + at_key2, g_args.get(i));
        }
      }

      return subject;
    }


    /// <summary>
    /// IDE_execute_and_format Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args"> 
    /// source=""=string 
    /// output_format="opt"=<one_of> "opt" "xml" "htm" "html" "txt"</one_of>
    /// </param>
    /// <returns></returns>
    public static object IDE_execute_and_format(params object[] args)
    {
      GenericObject geo_args = convert_args(args);
      object source = geo_args.get("source", "");
      object output_format = geo_args.get("output_format", "");
      GenericObject my = CASLInterpreter.get_my();
      if (!my.has("ide_local_env"))
      {
        my.set("ide_local_env", c_CASL.c_local_env());
      }
      GenericObject ide_local_env = my.get("ide_local_env") as GenericObject;
      CASLInterpreter.step_mode = 0; // reset the stepping mode for IDE.

      // It is a mistake to use g_env here so provide an ide specific
      // environment that won't effect the rest of ipayment.
      Object R = CASL_execute_and_format("source", source,
          "output_format", output_format,
          "local_env", ide_local_env);

      GenericObject a_response = c_CASL.c_make("response") as GenericObject;
      a_response.set("extension", output_format);
      a_response.set("result", R);
      (c_CASL.GEO.get("casl_ide") as GenericObject).set("last_result", R);
      return a_response;
    }
    /// <summary>
    /// CASL_execute_and_format Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args"> source="" output_format="" local_env=<GEO/> </param>
    /// <returns></returns>
    public static object CASL_execute_and_format(params object[] args)
    {
      GenericObject geo_args = convert_args(args);
      object source = geo_args.get("source", "");
      object output_format = geo_args.get("output_format", "");  // will call to_opt_top
      GenericObject local_env = (GenericObject)geo_args.get("local_env", c_CASL.c_local_env());


      //source = HttpUtility.UrlDecode(source.ToString()); 
      String s_expr = string.Format("<do> {0} </do>", source); //how to get path from call
      object R = null;
      GenericObject expr = null;
      CASL_Frame orig_frame = CASL_Stack.stack_top;
      try
      {
        expr = CASLParser.decode_conciseXML(s_expr, "");
      }
      catch (XmlException e)  // Syntax Error
      {
        R = c_CASL.c_make("error", "message", e.Message, "throw", false);
        misc.error_add_stack_trace(R as GenericObject, e.StackTrace);
        // HX: Bug 17849
        Logger.cs_log(e.ToMessageAndCompleteStacktrace() +
                      Environment.NewLine +
                      CASL_Stack.stacktrace_StrBld(null, 3).ToString());
      }
      try
      {
        CASL_Frame f1 = CASL_Stack.Alloc_Frame(EE_Steps.standard,
                   expr, local_env);
        f1.loc.source_or_filename = s_expr;
        f1.loc.file_index = 0;
        f1.loc.line_num = 0;
        f1.loc.line_pos = 0;
        R = CASLInterpreter.execute_expr(f1);
      }
      catch (CASL_error ce)  // Runtime Error
      {
        R = ce.casl_object;
        misc.error_add_stack_trace(R as GenericObject, ce.StackTrace);
        // HX: Bug 17849
        Logger.cs_log(ce.ToMessageAndCompleteStacktrace() +
                      Environment.NewLine +
                      CASL_Stack.stacktrace_StrBld(null, 3).ToString());
      }
      CASL_Stack.pop_to(orig_frame);
      return R;
    }
    //Takes a casl expression and a enviroment
    // will then run that exrpesstion in a seperate thread
    public static object CASL_async(params object[] args)
    {
      GenericObject geo_args = convert_args(args);

      misc a_misc = null;
      a_misc = new misc();

      a_misc.thread_expression = geo_args.get("a_expression", "");
      a_misc.thread_local_env = geo_args.get("_outer_env", "") as GenericObject;
      a_misc.a_syncThread = new System.Threading.Thread(
        new System.Threading.ThreadStart(a_misc.async_thread));

      a_misc.a_syncThread.IsBackground = true;
      a_misc.a_syncThread.Start();


      return "";
    }

    public void async_thread()
    {
      try
      {
        CASL_Stack.Initialization();
        object R = CASLInterpreter.execute_expr(
            CASL_Stack.Alloc_Frame(EE_Steps.standard, thread_expression, thread_local_env));
      }
      catch (Exception e)
      {
        // HX: Bug 17849
        Logger.cs_log_trace("Error async_thread", e);
        // TODO: add logging here so that failures can be reported
        //12081 NJ-Obsolete Code.Use sleep instead.
        //a_syncThread.Suspend();
        Thread.Sleep(1000);
        //end 12081 NJ
      }
      a_syncThread.Abort();
      return;
    }


    public static string field_key(object outer_key, object inner_key)
    {
      string field_key = outer_key.ToString() + "_f_" + inner_key.ToString();
      return field_key;
    }

    public static void error_add_stack_trace(
        GenericObject a_error,
        string a_stack_trace)
    {
      GenericObject the_stack_trace = a_error.get("cs_stack", c_CASL.c_GEO()) as GenericObject;
      if (a_stack_trace != null && c_CASL.show_verbose_error)//BUG#8952
      {
        string[] new_stack_trace = a_stack_trace.Split('\n');
        foreach (string a_trace in new_stack_trace)
          the_stack_trace.insert(a_trace);
      }
      a_error.set("cs_stack", the_stack_trace);
      if (c_CASL.show_verbose_error && (! a_error.has("casl_stack")))
        a_error.set("casl_stack",
                    CASL_Stack.stacktrace_StrBld(null, 5).ToString());
    }

    public static object fCASL_casl_stacktrace(CASL_Frame frame)
    {
      return CASL_Stack.stacktrace_StrBld(null, 7).ToString();
    }

    //
    //    This is currently used only by error.<htm_inst> which
    // I believe is just used by the CASL_ide but am not sure.
    //
    public static object fCASL_convert_error_stacks(CASL_Frame frame)
    {
      GenericObject casl_stack = new GenObj_Stack();
      GenericObject error_obj = frame.args.get("error_obj", null) as
                    GenericObject;
      if (error_obj == null)
      {
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          Flt_Rec.log("fCASL_convert_casl_stack", "no error_obj");
#if DEBUG_break
        Debugger.Break();
#endif
        return casl_stack;
      }

#if Dangerous_Code
      // This is dangerous because it has pointers into the CASL stack.
      // 
      CASL_Frame f = error_obj.get("top_CASL_Frame", null) as CASL_Frame;
      if (f == null)
      {
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          Flt_Rec.log("fCASL_convert_casl_stack",
                      "no casl_frame defined on error_obj");
        return casl_stack;
      }

      bool has_last_frame = error_obj.has("very_last_frame");

      while (f.next_step != EE_Steps.base_frame)
      {
        GenericObject g_fr = new GenObj_Frame();
        if (f.exp != null)
          g_fr.set("a_expr", f.exp);
        else
          g_fr.set("a_expr", "");
        if (f.a_method != null)
          g_fr.set("a_method", f.a_method);
        else
          g_fr.set("a_method", "");
        if (f.exp is GenObj_Code)
        {
          GenObj_Code c = f.exp as GenObj_Code;
          g_fr.set("__code_info", c.loc.ToString());
        }
        g_fr.set("local_env", f.outer_env);
        if (!has_last_frame)
        {
          has_last_frame = true;
          error_obj.set("very_last_frame", g_fr);
        }
        casl_stack.insert(g_fr);
        f = f.calling_frame;
      }
      error_obj.set("casl_stack", casl_stack);
#endif

      //For the moment go with the one generated by the exceptions.
      //error_add_stack_trace(error_obj,
      //  error_obj.get("cs_stack_string", null) as string);

      return error_obj;
    }

    /// <summary>
    /// CASL_starts_with Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args">_subject=req=string 0=req=string</param>
    /// <returns></returns>
    public static Object CASL_starts_with(params object[] args)
    {
      GenericObject geo_args = convert_args(args);
      Object subject = geo_args.get("_subject");
      Object value = geo_args.get(0);

      if (subject is string && value is string)
      {
        return (subject as string).StartsWith(value.ToString());
      }
      return false;
    }

    /// <summary>
    /// CASL_ends_with Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args">_subject=req=string 0=req=string</param>
    /// <returns></returns>
    public static object CASL_ends_with(params object[] args)
    {
      GenericObject geo_args = convert_args(args);
      Object subject = geo_args.get("_subject");
      Object value = geo_args.get(0);

      if (subject is string && value is string)
      {
        return (subject as string).EndsWith(value.ToString());
      }
      return false;
    }

    /// <summary>
    /// convert_primitive_to_casl_object Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args">_subject=req=c#_primitive </param>
    /// <returns></returns>
    public static GenericObject convert_primitive_to_casl_object(
        Object subject)
    {
      if (subject == null)
        return c_CASL.c_object("Null_Class") as GenericObject;
      else if (subject is string)
        return c_CASL.c_String;
      //			else if (subject is Int32)
      //				return c_CASL.c_Integer;
      //			else if ( subject is Decimal || subject is Double)
      //				return c_CASL.c_Decimal;
      else if (subject is Int32 || subject is Decimal || subject is Double)
        return c_CASL.c_Number;
      else if (subject is Boolean)
        return c_CASL.c_Boolean;
      else if (subject is byte[])
        return c_CASL.c_Bytes;
      else if (subject is GenericObject)
      {
        GenericObject g_subject = subject as GenericObject;
        if (!c_CASL.GEO.Equals(g_subject) && !g_subject.has("_parent"))
          g_subject.set("_parent", c_CASL.GEO);
        return g_subject;
      }
      else //if (subject is c# object) {
        return c_CASL.c_object("CS_Class") as GenericObject;
    }

    /// <summary>
    /// CASL_replace Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args">_subject=req=string 0=req=string 1=req=string</param>
    /// <returns></returns>
    public static object CASL_replace(params object[] args)
    {
      GenericObject geo_args = convert_args(args);
      Object subject = geo_args.get("_subject");
      Object old_value = geo_args.get(0);
      Object new_value = geo_args.get(1);

      if (subject is string && old_value is string && new_value is string)
      {
        return (subject as string).Replace(old_value.ToString(), new_value.ToString());
      }
      return subject;
    }

    /// <summary>
    /// CASL_string_length Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args"> _subject=req=String</param>
    /// <returns></returns>
    public static object CASL_string_length(params object[] args)
    {
      GenericObject geo_args = convert_args(args);
      string subject = geo_args.get("_subject").ToString();
      return subject.Length;
    }

    /// <summary>
    /// constructor for <test/> 
    /// execute all interger key in order and compare it with last value
    /// set success=true if equal or set success=false if not equal
    /// </summary>
    /// <param name="args">_new_object=req=GenericObject </param>
    /// <returns></returns>
    public static object CASL_test_init(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object new_object = g_args.get("_subject"); // _new_object
      if (new_object == null || !(new_object is GenericObject) || (new_object as GenericObject).getLength() <= 0)
      {
        string s_message = string.Format("invalid arg: {0}", misc.to_eng(g_args));
        throw CASL_error.create("message", s_message);
      }
      GenericObject g_new_object = new_object as GenericObject;

      int i = 0;
      GenericObject exprs_env = c_CASL.c_local_env();
      object pre_it = null;
      Object it = null;
      string error_message = "";

      try
      {
        while (i < g_new_object.getLength())
        {
          it = CASLInterpreter.execute_expr(
              CASL_Stack.Alloc_Frame(EE_Steps.standard, g_new_object.get(i), exprs_env));
          if (i == g_new_object.getLength() - 1)
          {
            break;
          }
          else
          {
            //exprs_env.set("it", it);
            pre_it = it;
          }
          i++;
        }
      }
      catch (Exception e)
      {
        // HX: Bug 17849
        Logger.cs_log_trace("Error CASL_test_init", e);
        error_message = "test has error:" + e.Message;
        g_new_object.set("error_message", error_message);
        pre_it = "error";
      }
      //pre_it = exprs_env.get("it");
      g_new_object.set("result", pre_it);
      g_new_object.set("expected_result", it);

      bool b_sucess = false;
      if (it == null && pre_it == null)
        b_sucess = true;
      else if (it != null && pre_it != null)
        if (it is GenericObject) // compare it 
          b_sucess = (it as GenericObject).DeepEquals(pre_it);
        else
          b_sucess = it.Equals(pre_it);
      else
        b_sucess = false;

      //	if( (it== null && pre_it == null) ||(it!= null && pre_it!=null && it.Equals(pre_it)))
      //g_new_object.set("success", true);
      if (b_sucess == true)
        g_new_object.set("success", true);
      else
      {
        g_new_object.set("success", false);
        GenericObject failures = c_CASL.c_object("test.failures") as GenericObject;
        if (failures != null)
        {
          failures.insert(g_new_object);
        }
      }
      GenericObject test = c_CASL.c_object("test") as GenericObject;
      test.set("test_count", (int)c_CASL.GEO.get_path("test.test_count", false, 0) + 1);//.c_object("test.test_count")+1 );
      return g_new_object;
    }

    /// <summary>
    /// CASL_to_source_code Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args">_subject=req=GenericObject</param>
    /// <returns></returns>
    public static object CASL_to_source_code(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object subject = g_args.get("_subject");
      if (subject == null || !(subject is GenericObject))
      {
        string s_message = string.Format("Invalid argument {0}.", misc.to_eng(subject));
        throw CASL_error.create("message", s_message);
      }
      string s_source_code = CASL_to_xml("_subject", subject);
      return HttpUtility.HtmlEncode(s_source_code);
    }

    public static object CASL_length(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object subject = g_args.get("_subject");
      if (subject == null || !(subject is GenericObject))
      {
        string s_message = string.Format("Invalid argument {0}.", misc.to_eng(g_args));
        throw CASL_error.create("message", s_message);
      }
      return (subject as GenericObject).getLength();
    }

    /// <summary>
    /// CASL_last Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args"> _subject=require=GenericObject</param>
    /// <returns></returns>
    public static object CASL_last(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      GenericObject subject = g_args.get("_subject", null) as GenericObject;
      if (subject == null)
      {
        string s_message = string.Format("Invalid argument {0}.", misc.to_eng(g_args));
        throw CASL_error.create("message", s_message);
      }
      if (subject.getLength() == 0)
      {
        return false;
        //string s_message = string.Format(" subject {0} does not have any vector keys .",misc.to_eng(subject));
        //throw CASL_error.create("message",s_message);
      }
      return subject.get(subject.getLength() - 1);
    }
    /// <summary>
    /// CASL_execute Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args">_subject=req=string </param>
    /// <returns></returns>
    public static object CASL_execute(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object subject = g_args.get("_subject");
      GenericObject local_env = g_args.get("local_env", c_CASL.c_GEO()) as GenericObject;
      if (subject == null || !(subject is string))
      {
        string s_message = string.Format("Invalid argument {0}.", misc.to_eng(g_args));
        throw CASL_error.create("message", s_message);
      }

      subject = HttpUtility.HtmlDecode(subject.ToString());
      subject = ((string)subject).Trim(' ', '\0');

      String string_to_execute = string.Format("<do>{0}</do>", subject); //how to get path from call
      object result = CASL_execute_string("_subject", string_to_execute, "local_env", local_env);
      return result;
    }

    public static object execute_string(
        StreamWriter error_file,
        string string_to_execute)
    {
      GenericObject expr = CASLParser.decode_conciseXML(string_to_execute, "");

      CASL_Frame f1 =
        CASL_Stack.Alloc_Frame(EE_Steps.standard, expr, c_CASL.c_GEO());
      f1.loc.source_or_filename = string_to_execute;
      f1.loc.file_index = 0;
      f1.loc.line_num = 0;
      f1.loc.line_pos = 0;

      object obj_return = null;
      try {
        obj_return = CASLInterpreter.execute_expr(f1);
      } catch (CASL_error ce) {
        error_file.WriteLine("Error trying to evaluate CASL " +
          ce.casl_object.get("message") as string);
        error_file.WriteLine("    " + string_to_execute);
      } catch (Exception e) {
        error_file.WriteLine("Error trying to evaluate CASL " +
          e.ToMessageAndCompleteStacktrace());
        error_file.WriteLine("    " + string_to_execute);
      }
      return obj_return;
    }

    // Assumes one top-level tag/element.  does not wrap into a <do> ... </do>   Should work with XML header.
    public static object CASL_execute_string(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      String string_to_execute = g_args.get("_subject") as String;
      GenericObject local_env = g_args.get("local_env", c_CASL.c_GEO()) as GenericObject;
      GenericObject expr = CASLParser.decode_conciseXML(string_to_execute, "");

      CASL_Frame orig_frame = CASL_Stack.stack_top;

      CASL_Frame f1 = CASL_Stack.Alloc_Frame(EE_Steps.standard, expr, local_env);
      f1.loc.source_or_filename = string_to_execute;
      f1.loc.file_index = 0;
      f1.loc.line_num = 0;
      f1.loc.line_pos = 0;

      object obj_return = CASLInterpreter.execute_expr(f1);
      CASL_Stack.pop_to(orig_frame);   
      return obj_return;
    }

    public static object CASL_execute_expr(params object[] arg_list)
    {
      GenericObject args = convert_args(arg_list);

      GenericObject subject = args.get("_subject") as GenericObject;
      GenericObject local_env = args.get("local_env") as GenericObject;

      CASL_Frame orig_frame = CASL_Stack.stack_top;

      object ret = CASLInterpreter.execute_expr(
          CASL_Stack.Alloc_Frame(EE_Steps.standard, subject, local_env));
      CASL_Stack.pop_to(orig_frame);   
      return ret;
    }

    /// <summary>
    /// CASL_parse Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args">_subject=req=string </param>
    /// <returns></returns>
    public static object CASL_parse(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object subject = g_args.get("_subject");
      if (subject == null || !(subject is string))
      {
        string s_message = string.Format("Invalid argument {0}.", misc.to_eng(g_args));
        throw CASL_error.create("message", s_message);
      }
      String subject_str = subject.ToString(); // HttpUtility.HtmlDecode(subject.ToString());
      if (subject_str.StartsWith("<?"))  // remove XML header
        subject_str = subject_str.Substring(subject_str.IndexOf("?>") + 2);
      String s_expr = "<do>" + subject_str + "</do>"; //how to get path from call
      // Occasionally useful if there is a reproducable situation where cruft
      // is being submitted.  GMB
      //
      // if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
      //   Flt_Rec.log("misc.CASL_parse", s_expr);


      //			try
      //			{
      GenericObject result = CASLParser.decode_conciseXML(s_expr, "");
      return result.get(0);
      //			}
      //			catch(XmlException e)
      //			{
      //				return string.Format(" <H1> Error(: </H1><PRE> {0}</PRE>",e.ToString());
      //			}
      //			catch(CASL_error e)
      //			{
      //				return string.Format(" <H1> Error: </H1><PRE> {0}</PRE>",e.ToString());
      //			}

    }


    /// <summary>
    /// CASL_parse_xml_data Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args">_subject=req=string </param>
    /// <returns></returns>
    public static object CASL_parse_xml_data(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object subject = g_args.get("_subject");
      if (subject == null || !(subject is string))
      {
        string s_message = string.Format("Invalid argument {0}.", misc.to_eng(g_args));
        throw CASL_error.create("message", s_message);
      }
      String subject_str = subject.ToString(); // HttpUtility.HtmlDecode(subject.ToString());  WHY??
      if (subject_str.StartsWith("<?"))  // remove XML header
        subject_str = subject_str.Substring(subject_str.IndexOf("?>") + 2);

      String s_expr = "<do>" + subject_str + "</do>"; //how to get path from call
      GenericObject result = CASLParser.decode_dataXML(s_expr, "");
      return result.get(0);
    }

    /// <summary>
    /// call casl method 
    /// if call_name is string, set a_method by call_name, this is usually used by c# call or wait for look up a_method in subject
    ///    if subject is avaiable, lookup call_name in subject
    ///    else lookup call_name in GEO
    /// call a_method with all args
    /// e.g. 
    ///   + call with call_name string and _subject args-> lookup and call
    /// 			misc.CASL_call("call_name","xxxx", "args",new GenericObject("_subject", xxx));
    ///   + call with a_method and _subject args -> no lookup, call directly
    /// 			misc.CASL_call("a_method",xxxx, "args",new GenericObject("_subject", xxx));
    /// </summary>
    /// <param name="args">
    /// a_method=req   
    /// _subject=no_subject
    /// args=<GEO/>
    /// _subject old argument
    /// </param>
    /// <returns></returns>
    public static object fCASL_call(CASL_Frame frame)
    {
      GenericObject g_args = frame.args;
      Object a_method = g_args.get("a_method");
      GenericObject call_args = g_args.get("args", c_CASL.c_local_env()) as GenericObject;

      // FIXME: Eventually call_args should all have _parent set.
      // However, many CASL_call() should be eliminated and
      // this facilitates the main performance improvement.  GMB.
      if (!call_args.has("_parent"))
      {
        Logger.cs_log("fCASL_call: call_args had no _parent",
                      Logger.cs_flag.warn);
        call_args.set("_parent", c_CASL.GEO);
      }

      object subject = g_args.get("subject", CASLInterpreter.no_subject);
      if (CASLInterpreter.no_subject.Equals(subject))
      {
        subject = g_args.get("_subject", CASLInterpreter.no_subject);
      }
      if (subject != null && !CASLInterpreter.no_subject.Equals(subject) && call_args.has("_subject"))
        subject = call_args.get("_subject");
      return base_casl_call(a_method, subject, call_args,
                            frame.tail_return_safe);
    }

    public static object CASL_call(params Object[] arg_pairs)
    {
      GenericObject g_args = misc.convert_args(arg_pairs);
      Object a_method = g_args.get("a_method");
      GenericObject call_args = g_args.get("args", c_CASL.c_local_env()) as GenericObject;

      // FIXME: Eventually call_args should all have _parent set.
      // However, many CASL_call() should be eliminated and
      // this facilitates the main performance improvement.  GMB.
      if (!call_args.has("_parent"))
        call_args.set("_parent", c_CASL.GEO);


      object subject = g_args.get("subject", CASLInterpreter.no_subject);
      if (CASLInterpreter.no_subject.Equals(subject))
      {
        subject = g_args.get("_subject", CASLInterpreter.no_subject);
      }
      if (subject != null && !CASLInterpreter.no_subject.Equals(subject) && call_args.has("_subject"))
        subject = call_args.get("_subject");
      Object ret = base_casl_call(a_method, subject, call_args, true);
      if (ret is CASL_Frame)
        ret = CASLInterpreter.execute_expr(ret as CASL_Frame);
      return ret;
    }

    public static object base_casl_call(
        Object a_method,
        object subject,
        GenericObject call_args,
        bool tail_return_safe = false)
    {
      if (!call_args.has("_parent"))
      {
        call_args.set("_parent", c_CASL.GEO);
#if DEBUG
        Logger.cs_log("base_casl_call where call_args has no _parent",
                      Logger.cs_flag.debug);
#endif
      }

      //-------------------------------------------
      // get a_method from call_name 
      if (a_method is string str_method)
      {
        if (subject != null && !CASLInterpreter.no_subject.Equals(subject))
        {
          if (subject == c_CASL.c_undefined)
          {
            a_method = misc.handle_if_missing(c_CASL.c_error, a_method,
                                              subject as GenericObject);
          }
          else
          {
            GenericObject g_subject = null;
            if (!(subject is GenericObject))
            {
              // if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
              //   Flt_Rec.log("misc.CASL_call",
              //   "Expected GenericObject but got " + subject.ToString());
              g_subject = convert_primitive_to_casl_object(subject);
            }
            else
            {
              g_subject = subject as GenericObject;
            }
            a_method = g_subject.get(str_method, c_CASL.c_error, true);
          }
          // a_method = misc.CASL_get("_subject",subject, "lookup",true,
          // 0,a_method, "if_missing",c_CASL.c_error);
        }
        else
          a_method = c_CASL.GEO.get_path(str_method, false);
      }
      GenericObject go_method = a_method as GenericObject;

      // call _precondition
      /*
        * _precondition is a method defined on a method
        * GEO.<defmethod _name='foo'/>
        * GEO.foo.<defmethod _name='_precondition'/>
        * 
        * The _precondition method is called with the same arguments (as shallow
        * copies) of the main method. (subject.<foo> bar=baz </foo> calls
        * foo.<_precondition> _subject=subject bar=baz </_precondition>
        * 
        * Thus, _precondition methods should have the same or a more permissive
        * contract than their main method.
        * 
        * The return value of a precondition is Boolean {true} if the 
        * precondition succeeds and the object to be returned to the program if
        * the precondition fails.
        * 
        * Thus, most precondition CASL methods should fail with top_app.
        * 
        * */
      object RV = null;
      bool is_execute_method = true;
      CASL_Frame orig_frame = CASL_Stack.stack_top;

      if ((go_method).has("_precondition"))
      {
        string msg = "misc.cs: base_casl_call: _precondition called ";
        if (go_method is GenObj_Method)
          msg += (go_method as GenObj_Method).full_name;
        else
          msg += go_method.get("_name", "totally bogus");
        // Logger.cs_log(msg, Logger.cs_flag.info);
        CASL_Frame frame = CASL_Stack.Alloc_Frame(EE_Steps.execute_method, null,
                                                  c_CASL.c_local_env());
        frame.a_method = go_method.get("_precondition") as GenericObject;
        frame.args = call_args.copy(c_CASL.c_opt, c_CASL.c_opt);

        frame.subject = subject;
#if DEBUG
        frame.method_name = go_method.get("_name", null) as string;
#endif

        CASLInterpreter.set_args(frame, frame.args, go_method, frame.subject, frame.outer_env);

        Object R_precondition = CASLInterpreter.execute_expr(frame);

        if (!(R_precondition.Equals(true)))
        {
          RV = R_precondition;
          is_execute_method = false;
        }
      }
      if (is_execute_method)
      {
        GenericObject outer_env = c_CASL.c_local_env();

        CASL_Frame f1 = CASL_Stack.Alloc_Frame(EE_Steps.execute_method, null, outer_env);
        f1.a_method = go_method;
        f1.subject = subject;
        f1.args = call_args;
#if DEBUG
        f1.method_name = f1.a_method.get("_name", null) as string;
#endif

        CASLInterpreter.set_args(f1, f1.args, f1.a_method, f1.subject, f1.outer_env);

        RV = CASLInterpreter.execute_expr(f1);
      }
      CASL_Stack.pop_to(orig_frame);

      return RV;
    }

    /// <summary>
    /// CASL_apply Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args"> a_method=req local_env=req</param>
    /// <returns></returns>
    public static object CASL_apply(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      GenericObject a_method = geo_args.get("a_method") as GenericObject;
      GenericObject local_env = geo_args.get("local_env") as GenericObject;

      // Bug 26202 UMN adding null check
      if (a_method == null)
        return null;

      CASL_Frame frame = CASL_Stack.Alloc_Frame(EE_Steps.execute_method, null, local_env);
      frame.a_method = a_method;
      frame.args = local_env;
#if DEBUG
      frame.method_name = a_method.get("_name", null) as string;
#endif

      CASLInterpreter.set_default_value_in_contract(frame, local_env, a_method);
      string method_path = (string)a_method.get("_name");
      if (Flt_Rec.go(Flt_Rec.where_t.methods_flag))
        Flt_Rec.log("misc.CASL_apply -> execute_method_aux ",
                    method_path + " " + Flt_Rec.show_object(args));

      object ret = CASLInterpreter.execute_expr(frame);

      if (Flt_Rec.go(Flt_Rec.where_t.methods_flag))
        Flt_Rec.log("  <<  misc.CASL_apply ",
                    Flt_Rec.show_object(ret));

      return ret;
    }
    /// <summary>
    /// CASL_try Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args"> _args=vector_key</param>
    /// <returns></returns>
    public static object CASL_try(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      int i = 0;
      //GenericObject exprs_env = c_CASL.c_local_env();
      GenericObject exprs_env = g_args.get("_outer_env") as GenericObject;

      // NOTE: the frame created to pass to execute_expr() is popped in
      // the catch-statements.
      CASL_Frame orig_frame = CASL_Stack.stack_top;

      //-----------------------------------------------------
      // set error expression
      //-----------------------------------------------------
      Object error_expr = null;
      if (Tool.is_call_for(g_args.get(g_args.getLength() - 1), "if_error"))
      {
        error_expr = g_args.get(g_args.getLength() - 1);
      }
      Object it = null;
      while (i < g_args.getLength())
      {
        try
        {
          if (i == g_args.getLength() - 1 && Tool.is_call_for(g_args.get(i), "if_error"))
            break;
          it = CASLInterpreter.execute_expr(
              CASL_Stack.Alloc_Frame(EE_Steps.standard, g_args.get(i), exprs_env));

          //BUG4036 S.X. handle return expr in try 
          if (it is GenericObject
            && (it as GenericObject).get("_parent", null) == c_CASL.c_return)
            return it;
          // END OF BUG4036 
          //exprs_env.set("it", it);
        }
        catch (CASL_error ce)
        {
          // Important to pop stack so inner frames can be garbage collected.
          CASL_Stack.pop_to(orig_frame);

          //GenericObject the_error = c_CASL.c_make("error", "casl""cs_stack",ce.StackTrace, "message",ce.Message);
          exprs_env.set("the_error", ce.casl_object);
          //                    try
          //                    {
          Object if_error = CASLInterpreter.execute_expr(
              CASL_Stack.Alloc_Frame(EE_Steps.standard, error_expr, exprs_env));

          return if_error;
        }
        catch (Exception e)
        {
          //string expr = string.Format("<error> message='{0}' throw=false </error>",e.Message);
          //Object the_error = CASL_execute("_subject", expr);
          //GenericObject the_error = c_CASL.c_make("error", "message",e.Message, "throw",false)  as GenericObject;
          //BUG#11322 get inner exception if available 
          Exception ie = e.InnerException; // ie=inner exception
          string exception_message = e.Message;
          GenericObject the_error = null;
          if (ie == null)
            the_error = c_CASL.c_make("error", "message", exception_message, "throw", false) as GenericObject;
          else
          {
            exception_message = ie.Message;
            if (ie is CASL_error)
              the_error = (ie as CASL_error).casl_object;
            else
              the_error = c_CASL.c_make("error", "message", exception_message, "throw", false) as GenericObject;
          }
          //end BUG#11322
          misc.error_add_stack_trace(the_error, e.StackTrace);
          CASL_Stack.pop_to(orig_frame);

          exprs_env.set("the_error", the_error);
          Object if_error = CASLInterpreter.execute_expr(
              CASL_Stack.Alloc_Frame(EE_Steps.standard, error_expr, exprs_env));

          // HX: Bug 17849
          Logger.cs_log("Error CASL_try: " +
                        e.ToMessageAndCompleteStacktrace() +
                        Environment.NewLine +
                        CASL_Stack.stacktrace_StrBld(
                            null, 3).ToString(),
                        Logger.cs_flag.debug);
          return if_error;
        }
        i++;
      }
      return it; // exprs_env.get("it");
    }

    /// <summary>
    /// CASL_error_throw Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args"> _subject=req=error message=opt=string</param>
    /// <returns></returns>
    public static object CASL_error_throw(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      GenericObject subject = g_args.get("_subject") as GenericObject;
      throw CASL_error.create(
        "title", subject.get("title", c_CASL.c_error, true),
        "message", subject.get("message", c_CASL.c_error, true),
        "user_action", subject.get("user_action", c_CASL.c_error, true),
        "code", subject.get("code", c_CASL.c_error, true)
        );
    }
    /// <summary>
    /// CASL_primitive_is_type_for Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args"> _subject=req=GEO a_geo=req=object</param>
    /// <returns></returns>
    public static object CASL_primitive_is_type_for(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      GenericObject a_geo = args.get("a_geo") as GenericObject;
      return is_primitive(a_geo);
    }

    // TTS#24619 Implement DeepDiff() for developer QA purposes.  GMB
    //
    // This "my regular expression".<regex/> returns a C# Regex object.
    // Primarily useful for creating patterns for DeepDiff.
    //
    public static object fCASL_regex(CASL_Frame frame)
    {
      string regex_str = frame.args.get("_subject", null) as string;
      return new Regex(regex_str);
    }

    //
    // This is part of the implementation of the CASL test harness.
    // It "takes" via the "to_be_validated", a string and tests a number
    // of patterns as a sequence.  I found it easier to put in a sequence
    // of simple patterns rather than a complicated pattern.
    //
    // Improvements:
    //   - Consider changing <test> to just use _subject.
    //   - Advance test_string so that multiple regex must be ordered.
    //   - Allow advancing test_string to be overriden
    //
    public static object fCASL_regex_check(CASL_Frame frame)
    {
      string test_string = frame.outer_env.get("to_be_validated", null) as string;
      bool res = true;
      string result = "";
      for (int i = 0; i < frame.args.getLength(); i++) {
        string reg_str = frame.args.get(i) as string;
        Regex regex = new Regex(reg_str);
        bool m = regex.IsMatch(test_string);
        if (! m) {
          res = false;
          result += reg_str + "  ";
        }
      }
      if (res)
        return (object) true;
      return result;
    }

    // Bug 17031 UMN add method to validate field against a regex
    public static Object CASL_validate_regex(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      string val = args.get("value") as String;
      string regex = args.get("regex") as String;
      Regex rgx = new Regex(regex);
      return rgx.IsMatch(val);
    }

    // Bug 18948 UMN implement CAPTCHA support
    public static Object CASL_validate_captcha(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      string captchaResp = args.get("value") as String;
      string secretKey = (c_CASL.c_object("Business.Business_center_settings.data") as GenericObject).get("cbc_captcha_secret_key", "").ToString();
      string remoteIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

      if (String.IsNullOrEmpty(secretKey))
      {
        Logger.cs_log("misc.cs: cbc_captcha_secret_key not found in" +
                      " Business_center_settings Config", Logger.cs_flag.warn);
        return false;
      }

      try
      {
        using (var client = new WebClient())
        {
          string URL = String.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}&remoteIp={2}",
            secretKey, captchaResp, remoteIP);

          string response = client.DownloadString(URL);
          RecaptchaResponse success = new JavaScriptSerializer().Deserialize<RecaptchaResponse>(response);
          Logger.cs_log("misc.cs: CASL_validate_captcha " +
                        String.Format("Captcha with IP {0} returned {1}",
                                      remoteIP, success.Success),
                        Logger.cs_flag.warn);
          Logger.LogInfo(String.Format("Captcha for IP {0} returned {1}", remoteIP, success.Success), "Captcha Success");
          return success.Success;
        }
      }
      catch (Exception e)
      {
        Logger.LogError(String.Format("misc.CASL_validate_captcha failed: {0}", e.Message), "Validate Captcha");
        // HX: Bug 17849
        Logger.cs_log_trace("misc.CASL_validate_captcha failed", e);
        return false;
      }
    }

    private class RecaptchaResponse
    {
      public bool Success { get; set; }
    }
    // END Bug 18948 UMN implement CAPTCHA support

    /// <summary>
    /// CASL_set_subject Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args">_subject=req=GenericObject 0=req</param>
    /// <returns></returns>
    public static object CASL_set_subject(params Object[] args)
    {
      //--------convert args and validate args-----------------------------
      GenericObject geo_args = convert_args(args);
      object subject = geo_args.get("_subject");
      object subject_value = geo_args.get(0);

      // manually validate args
      if (subject == null || !(subject is GenericObject) || subject_value == null)
      {
        string s_message = string.Format("Invalid argument in set");
        throw CASL_error.create("message", s_message);
      }
      //--------end to convert args and validate args-----------------------------
      (subject as GenericObject).set("_subject", subject_value);
      return null;
    }


    /// <summary>
    /// TODO: refactoring - move this method to CASLInterpreter
    /// </summary>
    /// <param name="args">_subject=req=GEO args=req=GenericObject</param>
    /// <returns></returns>
    public static object validate_contract_against(params Object[] args)
    {

      GenericObject _geo_args = convert_args(args);
      GenericObject a_contract = _geo_args.get("_subject") as GenericObject;
      GenericObject geo_args = _geo_args.get("args") as GenericObject;

      ///////////////////////////////////////////////////////////
      /// BEGIN AUTHORIZATION CHECKING
      /// check if 
      /// method or class need checking authorization
      /// 1) check has _check_authorization method(casl configable), if existed, call it. 
      /// 2) or check if the method or the class has _security_items field (casl configable)
      ///     if existed, check those security_items with standard way
      ///     each security items get checked, any one failed, return error object
      /// 3) if unauthorized return error object, 
      ///    need to handle in app, using exception can be expensive? 
      /// Note: 
      ///   1) For optimization, implement the standard _check_authorization in cs. 
      ///       and create wrapper in casl, so the method is casl configurable. 
      ///////////////////////////////////////////////////////////
      if (a_contract.has("_check_authorization") || a_contract.has("_security_items"))
      {
        GenericObject auth_args = geo_args.copy(c_CASL.c_opt, c_CASL.c_opt) as GenericObject;
        auth_args.set("a_contract", a_contract);

        object is_authorized = true;
        if (a_contract.has("_check_authorization"))
        {
          GenericObject auth_method = a_contract.get("_check_authorization") as GenericObject; // no lookup means that we not support inheritant _check_authorization for efficiency
          is_authorized = (bool)misc.base_casl_call(auth_method,
              CASLInterpreter.no_subject, auth_args);
        }
        else if (a_contract.has("_security_items"))
        {
          is_authorized = misc.base_casl_call(
              c_CASL.c_object("Security_item._check_authorization_aux"),
              CASLInterpreter.no_subject, auth_args);
        }
        if (misc.base_is_a(is_authorized, c_CASL.c_error))// UNANTHORIZED 
        {
          GenericObject unauthorized_error = is_authorized as GenericObject;
          String error_message = unauthorized_error.get("message", "", true) + "";
          String error_code = unauthorized_error.get("code", "", true) + "";
          String error_title = unauthorized_error.get("title", "", true) + "";
          throw CASL_error.create("message", error_message,
            "title", error_title,
            "code", error_code,
            "contract", a_contract,
            "args", auth_args);

          //CASL_unauthorized_error
        }
      }
      else if (a_contract.has("_security_items"))
      {
        GenericObject _security_items = a_contract.get("_security_items") as GenericObject;
        GenericObject auth_args = geo_args.copy(c_CASL.c_opt, c_CASL.c_opt) as GenericObject;
        auth_args.set("a_contract", a_contract, "_parent", c_CASL.GEO);
        object is_authorized = (bool)misc.base_casl_call(
            c_CASL.c_object("Security_item._check_authorization_aux"),
            CASLInterpreter.no_subject,
            auth_args);
        if (is_authorized.Equals(false))
          throw CASL_error.create("message", "not authorized to perform this actiion.", "title", "UNAUTHORIZED", "code", "UNAUTHORIZED", "contract", a_contract, "args", auth_args);
      }

      /// END OF AUTHORIZATION CHECKING
      ///////////////////////////////////////////////////////////


      // loop through contract and if args has req fields
      GenericObject FO = a_contract.get("_field_order", c_CASL.c_GEO()) as GenericObject; // field_order
      for (int i = 0; i < FO.getLength(); i++)
      {
        Object key = FO.get(i);
        if (key.ToString().StartsWith("_")) continue;  // system params done other ways
        object contract_value = a_contract.get(key, null);
        //Make sure it exists if req
        if (contract_value != null && c_CASL.c_req.Equals(contract_value))	//Put to string?  Or use Object itself?
        {
          if (!(geo_args.has(key)))
          {
            string s_message =
                string.Format("Argument {0} is required in contract {1}.",
                misc.to_eng(key), misc.to_eng(a_contract));
            Logger.cs_log(s_message, Logger.cs_flag.error);
            throw CASL_error.create("message", s_message, "title", "Runtime error",
                "code", 201, "contract", a_contract, "key", key,
                "args", geo_args);
          }
        }
      }
      //Validate the type for all arguments against the contract
      // NOTE: Use IsTypeOf instead - need to write that function
      // The value in the ars must be matched against the meta-field
      // in the contract.  (The args likely won't have a meta-field
      // for type, and even if it did, we need to check the value
      // passed not the type expected.)
      object type_checking = c_CASL.GEO.get("type_checking", "off"); //<!-- 'off' 'warning' error -->
      bool public_access = a_contract.get("_access", false).Equals("public");
      if (!"off".Equals(type_checking) || public_access) //TYPE_CHECKING for all public methods
      {
        bool warning = ("warning".Equals(type_checking) || public_access);

        for (int i = 0; i < FO.getLength(); i++)
        {
          Object key = FO.get(i);
          if (key.ToString().StartsWith("_")) continue;  // system params done other ways
          object value = geo_args.get(key);
          String type_field_name = field_key(key, "type");
          if (a_contract.has(type_field_name))
          {
            object type = a_contract.get(type_field_name);
            GenericObject call_args = c_CASL.c_GEO();
            call_args.insert(value);
            bool is_valid_type = (
              (value != null && (value.Equals(c_CASL.c_req) || value.Equals(c_CASL.c_opt))) ||
              (bool)misc.base_casl_call("is_type_for", type, call_args)
              );// dispatch is_type_for in casl call
            if (!is_valid_type)
            {
              // Bug #8395 Mike O - Only log if show_verbose_error is true, cleaned up code
              // Bug 17061 UMN PCI-DSS
              string keyString = to_eng(key);
              string s_message = string.Format("Argument {0} was passed {3}, but type is {1} in contract {2}.",
                                               keyString, misc.to_eng(type), misc.to_eng(a_contract),
                                               to_eng_masked(keyString, value));
              if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
                Flt_Rec.log("misc.cs: validate_contract_against", s_message);

              if (!warning)
                throw CASL_error.create("message", s_message, "title", "Type-checking error", "code", 202, "contract", a_contract, "key", key, "type", type, "args", geo_args);
              // End Bug #8395
            }
          }
          // Bug #8395 Mike O - Only log if show_verbose_error is true
          // Bug #11761 Mike O - Don't show this message any more (it's ok to not specify a type)
        }
      }
      // Verify that any args passed in are in the contract
      // (except for when "_other_keyed" is present which means that all
      // keyed args are valid.
      if (!a_contract.has("_other_keyed"))
      {
        //Loop through all keys in goArgs
        ArrayList argsKeyList = new ArrayList();
        argsKeyList = geo_args.getStringKeys();
        argsKeyList.AddRange(geo_args.getIntegerKeys());
        foreach (Object key in argsKeyList)
        {
          if (key is Int32)
          {
            if (!a_contract.has("_other_unkeyed"))
            {
              string s_message = string.Format("Unkeyed Argument {0} not allowed in contract {1}.", misc.to_eng(key), misc.to_eng(a_contract));
              throw CASL_error.create("message", s_message, "contract", a_contract, "key", key, "args", geo_args);
            }
          }
          else if (!a_contract.has(key) && !"_subject".Equals(key)) // except _subject, can not set _subject
          {
            string s_message = string.Format("Argument {0} not in contract {1}.", misc.to_eng(key), misc.to_eng(a_contract));
            throw CASL_error.create("message", s_message, "contract", a_contract, "key", key, "args", geo_args);
          }
        }
      }
      return true;
    }

    /// <summary>
    /// CASL_plus Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args">_subject=opt=Number any_unkeyed_arguments=req=Number</param>
    /// <returns></returns>
    public static object CASL_plus(params Object[] args)
    {
      //--------convert args and validate args-----------------------------
      GenericObject geo_args = convert_args(args);
      object subject = geo_args.get("_subject", 0);

      //			string s_contract = @"<GEO> <field> key='_subject' value=opt type=Number</field> 
      //<field> key='any_unkeyed_arguments' value=req type=Number</field> </GEO>";
      //			GenericObject contract = new GenericObject("_subject",CASLInterpreter.OPT, 0, CASLInterpreter.REQ);
      // manually validate args
      //--------end to convert args and validate args-----------------------------
      decimal result = 0;
      bool all_int = true;
      if (subject is ValueType)
      {
        if (subject.GetType() == typeof(decimal)) all_int = false;
        result = (decimal)Convert.ChangeType(subject, typeof(decimal));
      }
      for (int i = 0; i < geo_args.getLength(); i++)
      {
        object value = geo_args.get(i);
        if (value is ValueType)
        {
          if (value.GetType() == typeof(decimal)) all_int = false;
          result = result + (decimal)Convert.ChangeType(value, typeof(decimal)); ;
        }
      }
      if (all_int) return (int)Convert.ChangeType(result, typeof(int));

      return result;
    }

    public static object CASL_minus(params Object[] args)
    {
      //--------convert args and validate args-----------------------------
      GenericObject geo_args = convert_args(args);
      object subject = geo_args.get("_subject", 0);
      object arg = geo_args.get("arg");
      decimal subject_ = (decimal)Convert.ChangeType(subject, typeof(decimal));
      //			decimal arg1_ = (decimal)Convert.ChangeType(arg1, typeof(decimal));
      //			return subject_ - arg1_;

      decimal result = 0;
      bool all_int = true;
      if (subject is ValueType)
      {
        if (subject.GetType() == typeof(decimal)) all_int = false;
        result = (decimal)Convert.ChangeType(subject, typeof(decimal));
      }
      if (arg is ValueType)
      {
        if (arg.GetType() == typeof(decimal)) all_int = false;
        result = result - (decimal)Convert.ChangeType(arg, typeof(decimal)); ;
      }
      if (all_int) return (int)Convert.ChangeType(result, typeof(int));
      return result;
    }

    public static object CASL_times(params Object[] args)
    {
      //--------convert args and validate args-----------------------------
      GenericObject geo_args = convert_args(args);
      object subject = geo_args.get("_subject");
      object arg1 = geo_args.get(0);
      decimal subject_ = (decimal)Convert.ChangeType(subject, typeof(decimal));
      decimal arg1_ = (decimal)Convert.ChangeType(arg1, typeof(decimal));
      return subject_ * arg1_;
    }

    public static object CASL_divide(params Object[] args)
    {
      //--------convert args and validate args-----------------------------
      GenericObject geo_args = convert_args(args);
      object subject = geo_args.get("_subject");
      object arg1 = geo_args.get(0);
      decimal subject_ = (decimal)Convert.ChangeType(subject, typeof(decimal));
      decimal arg1_ = (decimal)Convert.ChangeType(arg1, typeof(decimal));
      return subject_ / arg1_;
    }
    public static object CASL_remainder(params Object[] args)
    {
      //--------convert args and validate args-----------------------------
      GenericObject geo_args = convert_args(args);
      object subject = geo_args.get("_subject");
      object arg1 = geo_args.get(0);
      if (subject is int && arg1 is int)
        return (int)subject % (int)arg1;
      else
      {
        decimal subject_ = (decimal)Convert.ChangeType(subject, typeof(decimal));
        decimal arg1_ = (decimal)Convert.ChangeType(arg1, typeof(decimal));
        return subject_ % arg1_;
      }
    }
    public static object CASL_more(params Object[] args)
    {
      //--------convert args and validate args-----------------------------
      GenericObject geo_args = convert_args(args);
      object subject = geo_args.get("_subject");
      object arg1 = geo_args.get(0);
      decimal subject_ = (decimal)Convert.ChangeType(subject, typeof(decimal));
      decimal arg1_ = (decimal)Convert.ChangeType(arg1, typeof(decimal));
      return subject_ > arg1_;
    }

    public static object CASL_less(params Object[] args)
    {
      //--------convert args and validate args-----------------------------
      GenericObject geo_args = convert_args(args);
      object subject = geo_args.get("_subject");
      object arg1 = geo_args.get(0);
      decimal subject_ = (decimal)Convert.ChangeType(subject, typeof(decimal));
      decimal arg1_ = (decimal)Convert.ChangeType(arg1, typeof(decimal));
      return subject_ < arg1_;
    }

    static Random aRand = new Random();
    public static object CASL_random(params object[] args)
    {
      GenericObject geo_args = convert_args(args);
      int iMax = (int)geo_args.get("_subject", 1);
      return aRand.Next(iMax);
    }

    /// <summary>
    /// CASL_has Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args"> _subject=req 0=req </param>
    /// <returns></returns>
    public static object CASL_has(params Object[] args)
    {
      //--------convert args and validate args-----------------------------
      GenericObject geo_args = convert_args(args);
      object subject = geo_args.get("_subject", null);
      if (subject == null)
      {
        subject = geo_args.get("_outer_env", null);
      }
      object key = geo_args.get("key");
      bool lookup = (bool)geo_args.get("lookup", false);

      // manually validate args
      if (subject == null || !(subject is GenericObject))
      {
        string s_message = string.Format("subject is required in 'CASL_has'.");
        throw CASL_error.create("message", s_message);
      }

      if (false.Equals((subject as GenericObject).has(key, lookup)))
        return false;
      else
        return true;
    }

    /// <summary>
    /// CASL_subvector Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args">_subject=req=GEO 
    /// start=0=<range_of> 0 _subject.inkeylength </range_of> 
    /// end=_subject.inkeylength-1=<range_of> start inkeylength</range_of>
    /// </param>
    /// <returns></returns>
    public static object CASL_subvector(params object[] args)
    {
      //--------------------------------------------------
      GenericObject geo_args = misc.convert_args(args);
      GenericObject subject = geo_args.get("_subject") as GenericObject;
      if (subject == null)
      {
        string s_message = string.Format("subject is required.");
        throw CASL_error.create("message", s_message);
      }
      int length = subject.getLength();
      int start = (int)(geo_args.get("start", 0));
      int end = (int)(geo_args.get("end", length));
      if (start < 0)
      {
        start = 0;
      }
      if (end < start)
      {
        string s_message = string.Format("end must be more than start, but end is {0} and start is {1}", misc.to_eng(end), misc.to_eng(start));
        throw CASL_error.create("message", s_message);
      }
      if (end > length)
        end = length;

      //			int start = (int) Convert.ChangeType( geo_args.get("start",0) ,typeof(int) );
      //			int end = (int) Convert.ChangeType(geo_args.get("end",subject.getLength()),typeof(int) );
      //   
      //   //----------------------------------------------------------------------------------------
      //   // Daniel changed this.
      //   if(end > subject.getLength())
      //     end = subject.getLength();
      //     
      //   if( end < start )
      //			{
      //				string s_message = string.Format("Start must be greater than or equal to end.");
      //				throw CASL_error.create("message",s_message);
      //			}
      //----------------------------------------------------------------------------------------

      /*
                        //--------------------------------------------------
                        //TODO: use generic flex_args validation function
                        if( end < start || end > subject.getLength() )
                        {
                            string s_message = string.Format("Argument {0} not valid.",misc.to_eng(geo_args));
                            throw CASL_error.create("message",s_message);
                        }
                */
      //--------------------------------------------------
      GenericObject subvector = c_CASL.c_make("vector") as GenericObject;
      for (int i = start; i < end; i++)
      {
        subvector.insert(subject.get(i));
      }
      return subvector;
    }



    public static object CASL_string_pad(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      string subject = geo_args.get("_subject") as string;

      string padding = "";
      int pad_length = (int)(geo_args.get("length"));
      string align = geo_args.get("align", "right") as string;
      string pad_char = geo_args.get("a_char", " ") as string;

      for (int i = subject.Length; i < pad_length; i++)
        padding += pad_char;

      if (align == "left")
        return subject + padding; // left-align
      else
        return padding + subject; // right-align
    }

    public static object CASL_string_subvector(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      string subject = geo_args.get("_subject") as string;
      if (subject == null)
      {
        string s_message = string.Format("subject is required.");
        throw CASL_error.create("message", s_message);
      }
      int start = (int)(geo_args.get("start", 0));
      int end = (int)(geo_args.get("end", subject.Length));
      if (start < 0)
      {
        start = 0;
      }
      if (end < start)
      {
        string s_message = string.Format("end must be more than start, but end is {0} and start is {1}", misc.to_eng(end), misc.to_eng(start));
        throw CASL_error.create("message", s_message);
      }

      if (end >= subject.Length)
        return subject.Substring(start);
      else
        return subject.Substring(start, end - start);
    }


    /// <summary>
    /// get key based on provided value on GenericObject
    /// </summary>
    /// <param name="args"> _subject=req=GenericObject value=req=object</param>
    /// <returns></returns>
    public static object CASL_key_of(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      object subject = geo_args.get("_subject");

      if (subject is GenericObject)
      {
        GenericObject subject_geo = (GenericObject)subject;
        object the_value = geo_args.get("value");
        return subject_geo.key_of(the_value);
      }

      return false;
    }

    public static object CASL_all_keys(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      GenericObject subject = geo_args.get("_subject") as GenericObject;

      return arraylist_to_GenericObject(subject.getKeys());
    }

    public static object CASL_system_keys(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      GenericObject subject = geo_args.get("_subject") as GenericObject;

      return arraylist_to_GenericObject(subject.getSystemKeys());
    }
    public static object CASL_string_keys(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      GenericObject subject = geo_args.get("_subject") as GenericObject;
      return arraylist_to_GenericObject(subject.getStringKeys());
    }

    public static object fCASL_path_to_vector(CASL_Frame frame)
    {
      GenericObject subject = frame.args.get("_subject") as GenericObject;
      GenericObject target = frame.args.get("new_object", null) as GenericObject;

      if (frame.subject != subject) {
        Debugger.Break();
      }

      if (target == null)
        target = new GenericObject("_parent", c_CASL.c_object("vector"));

      foreach (object key in subject.getKeys()) {
        if (! "_parent".Equals(key)) {
          target.set(key, subject.get(key));
        }
      }
        
      return target;
    }

#region Copy
    //FIXME
    public static object CASLCopy(params object[] args)
    {
      GenericObject geoArgs = misc.convert_args(args);
      GenericObject subject = geoArgs.get("_subject") as GenericObject;
      GenericObject target = geoArgs.get("new_object", null) as GenericObject;
      object include = geoArgs.get("include", null);
      GenericObject exclude = geoArgs.get("exclude", null) as GenericObject;

      return Copy(subject, target, include, exclude);
    }

    public static GenericObject Copy(
        GenericObject subject,
        GenericObject target,
        object include,
        GenericObject exclude)
    {
      if (target == null)
      {
        target = GenericObject.GenObj_Allocate(subject);
#if DEBUG
      }
      else
      {
        Type target_type = target.GetType();
        Type subject_type = subject.GetType();
        if ((target_type != subject_type) &&
            (!target.GetType().IsSubclassOf(subject.GetType())))
        {
          string str =
              "misc.cs:Copy() the provided target has kind " +
              target.kind.ToString() + " but subject is " +
              subject.kind.ToString();
          Logger.cs_log(str, Logger.cs_flag.warn);
        }
#endif
      }
      ArrayList includeList = null;
      ArrayList excludeList = null;
      if (exclude != null)
      {
        excludeList = exclude.vectors;
      }

      if (include == null || !(include is GenericObject || include is string))
      {
        includeList = subject.GetKeys("all", excludeList);
      }
      else if (include is GenericObject)
      {
        includeList = new ArrayList((include as GenericObject).vectors);
        if (excludeList != null)
        {
          foreach (object o in excludeList)
          {
            if (includeList.Contains(o))
            {
              includeList.Remove(o);
            }
          }
        }
      }
      else if (include is string)
      {
        includeList = subject.GetKeys(include as string, excludeList);
      }


      foreach (object key in includeList)
      {
        if (excludeList != null && excludeList.Contains(key) ||
            !(subject.has(key)))
        {
          continue;
        }
        target.set(key, subject.get(key));
      }
      return target;
    }
#endregion

    public static object CASL_make_reverse(params object[] args)  // NOT USED -- see GEO.copy in CASL
    {
      GenericObject geo_args = misc.convert_args(args);
      GenericObject subject = geo_args.get("_subject") as GenericObject;
      return subject.make_reverse();
    }
    /// <summary>
    /// get index(position interger) for a substring in a string.
    /// string is treated as a vector of charaters. 
    /// if from_end is true, return last index in the string. 
    /// </summary>
    /// <param name="args">_subject=req=String 0=req=string from_end=false=boolean</param>
    /// <returns></returns>
    public static object CASL_string_key_of(params object[] args)
    {
      GenericObject geo_args = convert_args(args);
      String subject = geo_args.get("_subject") as string;
      String value = geo_args.get("a_string") as string;
      bool from_end = (bool)geo_args.get("from_end", false);
      int start_at = (int)geo_args.get("start_at", 0);
      if (subject == null || value == null)
      {
        string s_message = string.Format("Argument {0} not valid.", misc.to_eng(geo_args));
        throw CASL_error.create("message", s_message);
      }
      int R;
      if (false.Equals(from_end))
        R = subject.IndexOf(value, start_at);
      else
        R = subject.LastIndexOf(value);
      if (R < 0)
        return false;
      return R;
    }

    public static object CASL_object_keys(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      GenericObject subject = geo_args.get("_subject") as GenericObject;

      return arraylist_to_GenericObject(subject.getObjectKeys());
    }
    /// <summary>
    /// not useful
    /// </summary>
    /// <param name="a_array"></param>
    /// <returns></returns>
    public static GenericObject arraylist_to_GenericObject(ArrayList a_array)
    {
      GenericObject R = c_CASL.c_GEO();
      foreach (object key in a_array)
      {
        R.insert(key);
      }
      return R;
    }

    public static object CASL_vector_keys(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      GenericObject subject = geo_args.get("_subject") as GenericObject;

      GenericObject R = c_CASL.c_GEO();
      for (int i = 0; i < subject.getLength(); i++)
      {
        R.insert(i);
      }
      return R;
    }

    public static object CASL_regular_keys(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      GenericObject subject = geo_args.get("_subject") as GenericObject;
      return RegularKeys(subject);
    }
    public static object RegularKeys(GenericObject subject)
    {
      GenericObject R = c_CASL.c_GEO();
      R.vectors.AddRange(subject.getRegularKeys());
      return R;
    }

    // should be deleted because execute_string handles this
    public static object CASL_execute_document(params Object[] args)
    {
      // take filename, get content of file as string, execute string and
      GenericObject geo_args = convert_args(args);
      string subject = geo_args.get("_subject") as string;

      if (subject.StartsWith("<?"))  // remove XML header
        subject = subject.Substring(subject.IndexOf("?>") + 2);
      GenericObject local_env = geo_args.get("local_env", c_CASL.c_local_env()) as GenericObject;
      GenericObject exprs = CASLParser.decode_conciseXML(subject, "");
      // return CASLInterpreter.execute_exprs(frame, exprs, local_env, false);
      return CASLInterpreter.execute_exprs(exprs, local_env, false);
    }

    //
    //  GEO.<defmethod _name='test_write'>
    //        a_uri=req secret_key=opt extension="e"
    //        secret_key="Ye1/fHWFsvZRhxFj9AjONIzeen85v+fO"
    //        _impl=cs.CASL_engine.misc.CASL_execute_file_aux_write_encrypted
    //  </defmethod>
    //  <test_write> "T:/T4/pos_demo/load.casl" </test_write>
    //

    // Bug #9335 Mike O - Changed to public
    // Called by: CASL_execute_file and EncryptUpgradeFiles
    public static void execute_file_aux_write_encrypted(string a_uri, string secret_key, string extension)
    {
      if (secret_key.Length == 0)
        throw CASL_error.create_str("The secret key has not been assigned.");
      string file_content_str = misc.file_get_content("a_uri", a_uri); // 
      System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();

      //
      // Hack MP.  When decrypting, the first 8 bytes are corrupted and
      // is related to either the initialization vector or how stream-based
      // decryption works.  Tried a number of alternatives, but just decided
      // that adding some starting space was the best workaround.
      //
      byte[] file_content_bytes = encoding.GetBytes("              " +
          file_content_str);
      //byte[] file_content_bytes=misc.file_get_content_bytes("a_uri",subject);
      // get content as string, not bytes, then convert to bytes.

      GenericObject encrypted_obj = Crypto.symmetric_encrypt(
          "data", file_content_bytes,
          "secret_key", secret_key,
          "is_ascii_string", false);// Bug# 11809 Added is_ascii_string parameter DH 10/12/2011
      file_set_content("a_uri", a_uri + extension, "content", encrypted_obj.get("data"));
    }

    /* DEBUGGING:
    public static GenericObject CASL_execute_file_aux_read_encrypted (params Object[] args) 
    {    
      GenericObject geo_args = convert_args(args);
      string a_uri = geo_args.get("a_uri") as string;     
      string secret_key = geo_args.get("secret_key", c_CASL.c_opt) as string;     
      string extension = geo_args.get("extension") as string;     
      return execute_file_aux_read_encrypted(a_uri, secret_key, extension);
    }
    GEO.<defmethod _name='test_read'> a_uri=req secret_key=opt extension="e"
           _impl=cs.CASL_engine.misc.CASL_execute_file_aux_read_encrypted
        </defmethod>
    <test_read> "T:/T4/pos_demo/load.casl" </test_read>
    <test_read> "T:/T4/pos_demo/load_ide.casl" </test_read>
    */

    //
    // Only called by:
    //   - execute_file_aux when use_encrypted is true and the encrypted
    //     file exists.
    //   - ExecuteUpgradeFiles() searches for the '*.casle' files.
    //
    private static GenericObject execute_file_aux_read_encrypted(string a_uri)
    {
      string secret_key = Crypto.get_secret_key();

      byte[] content_bytes = misc.file_get_content_bytes("a_uri", a_uri);
      byte[] decrypted_bytes = Crypto.symmetric_decrypt("data", content_bytes, "secret_key", secret_key);

      System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
      string source_str = enc.GetString(decrypted_bytes);
      int start_of_do = source_str.IndexOf("<do>");
      if (start_of_do > 0)
        source_str = source_str.Substring(start_of_do);  // Sometimes there are head-of-file characters like: o;?

      try
      {
        return (CASLParser.decode_conciseXML(source_str, ""));  // this gets file.  decode_conciseXML is bad name.
      }
      catch (Exception e)
      {
        throw CASL_error.create_str("Loading decrypted file fails on: " + a_uri + ". " + e.Message);
      }
    }

    /// <summary>
    /// MSD-131 DH - Create a new GUID for the merchant ID - ACH Verification Service
    /// </summary>
    /// <returns></returns>
    public static string NewGUID(params Object[] args)
    {
      return Guid.NewGuid().ToString().ToUpper();
    }

    //
    // This is now the only place where a decision to use .casl vs. .casle files is
    // made---so at the worst we will have uniform confusion :-/.
    //
    // Logging: my intention is to flag all the anomalies which does mean a few
    // more potential log entries; specifically <site_load> of a non-existent
    // file is noted as LogInfo().
    //
    private static Object execute_file_aux(string a_uri, bool OK_file_missing,
                                                  string action)
    {
      // global flag that says to load from encrypted file (casle)
      // if available
      bool use_encrypted = (bool)c_CASL.GEO.get("use_encrypted", false);
    
      string encrypt_exten = "e";
      bool casl_exists = File.Exists(a_uri);
      bool encrypted_exists = File.Exists(a_uri + "e");

      if (casl_exists && encrypted_exists) {
        DateTime casl_dt = File.GetLastWriteTime(a_uri);
        DateTime encrypt_dt = File.GetLastWriteTime(a_uri + "e");

        if (use_encrypted && (casl_dt > encrypt_dt))
          Logger.LogWarn(a_uri + " has newer .casl than the .casle file!!",
          action);
        else
          Logger.LogInfo(a_uri + " has both .casl and .casle files: using " +
                       (use_encrypted ? "encrypted" : "unencrypted"), action);
      } else if (! (casl_exists || encrypted_exists)) {
        if (OK_file_missing) {
          Logger.LogInfo(a_uri + " not found: skipping", action);
          return null;
        }
        Logger.LogError(a_uri + " can't find either .casl or .casle files",
                        action);
        throw CASL_error.create_str("Loading " + a_uri + " failed: file not found");

      } else if ( (!use_encrypted) && (!casl_exists) ) {
        if (OK_file_missing) {
          Logger.LogWarn(a_uri + " use_encrypted is false, but only " +
                         "encrypted file found: skipping", action);
          return null;
        }
        Logger.LogError(a_uri + " use_encrypted is false, but only " +
                         "encrypted file found: aborting!!", action);
        throw CASL_error.create_str("Loading " + a_uri + " failed: file not found");
      }

      if (Flt_Rec.go(Flt_Rec.where_t.load_file_flag)) {
        string msg = null;
        if (use_encrypted) {
          if (encrypted_exists)
            msg = " -- using encrypted";
          else
            msg = " -- encrypted requested, but only casl available";
        } else {
          if (encrypted_exists)
            msg = " -- ignoring encrypted";
          else  // casl_exist because otherwise we throw an error earlier.
            msg = " -- using .casl";
        }
        Flt_Rec.log(action + ": ", a_uri + msg);
      }

      GenericObject exprs = null;
      if (use_encrypted && encrypted_exists) {
        exprs = execute_file_aux_read_encrypted(a_uri + encrypt_exten);
      }
      else
        exprs = CASLParser.decode_conciseXML("", a_uri);

      // BUG#14868 do not use local_env which cause name conflict when
      // loading casl file.
      // geo_args.get("local_env", c_CASL.c_local_env()) as GenericObject;
      GenericObject local_env = c_CASL.c_local_env();
      Object result;
      if (a_uri.IndexOf("/frozen/") != -1)
      {
        result = CASLInterpreter.execute_expr(
            CASL_Stack.Alloc_Frame(EE_Steps.standard, exprs, local_env));
      }
      else
      {
        result = CASLInterpreter.execute_exprs(exprs, local_env, false);
      }

      if (Flt_Rec.go(Flt_Rec.where_t.load_file_flag))
        Flt_Rec.log(" << " + action + ": ", a_uri);

      return result;
    }
    
    /// <summary>
    ///   if my.encrypt_file is true, then encrypts files
    /// </summary>
    /// <param name="args">_subject=req=string</param>
    /// <returns></returns>
    public static object fCASL_execute_file(CASL_Frame frame)
    {
      // take filename, get content of file as string, execute string and
      GenericObject geo_args = frame.args;
      string subject = geo_args.get("_subject") as string;
      frame.loc.source_or_filename = subject;
      frame.loc.file_index = CASLInterpreter.getFileIdx(subject);
      frame.loc.line_num = 0;
      frame.loc.line_pos = 0;

      object it = execute_file_aux(subject, false, "fCASL_execute_file");
      return it;
    }

    /// <summary> CASL_sort sorts a GenericObject using first the _sort_name
    /// field followed by the _name field. </summary>
    /// <param name="args"> _subject=req </param> The vector to sort
    /// <returns> returns=req=vector </returns>
    public static object CASL_sort(params Object[] args)
    {
      GenericObject geo_args = convert_args(args);
      GenericObject subject = geo_args.get("_subject") as GenericObject;
      ArrayList al = new ArrayList();
      al.AddRange(subject.vectors);
      al.Sort(comparer);

      return arraylist_to_GenericObject(al).set("_parent", c_CASL.c_object("vector"));
    }

    public static object CASL_reverse(params Object[] args)
    {
      GenericObject geo_args = convert_args(args);
      GenericObject subject = geo_args.get("_subject") as GenericObject;
      GenericObject R = null;
      if (subject != null)
      {
        R = subject.copy(c_CASL.c_opt, c_CASL.c_opt);
        R.reverse();
      }
      return R;
    }

    /// <summary>
    /// if folder_path is opt, then use default.
    /// From outer environment, get "local_folder"
    /// if name is a folder, then try to execute "load.casl" within the folder,
    /// if name + ".casl" is a file, then try to execute the file.
    /// folder is string of a path of folder which contain resource by name
    /// </summary>
    /// <param name="args">  name=req folder="opt"=string  </param>
    /// <returns></returns>

    // Called by: GEO.load
    public static object CASL_load(params Object[] args)
    {
      // take filename, get content of file as string, execute string and
      GenericObject geo_args = convert_args(args);
      string name = geo_args.get("name") as string;
      object folder = geo_args.get("folder", c_CASL.c_opt);
      object obj2 = geo_args.get("OK_if_missing", false);
      bool OK_file_missing = (obj2 is bool) && (bool) obj2;

      string folder_string = "";
      if (c_CASL.c_opt.Equals(folder))
      {
        folder_string = CASL_get_code_base(); // code_casl only append to code base 
      }
      else
        folder_string = (string)folder;

      string module_folder = folder_string + @"/" + name;
      string module_load_file = module_folder + @"/code_casl/load.casl";

      object R = execute_file_aux(module_load_file, OK_file_missing, "CASL_load");
      return R;
    }

    public static object fCASL_c_exists(CASL_Frame frame)
    {
      // take filename, get content of file as string, execute string and
      GenericObject args = frame.args;
      GenericObject file = args.get("_subject") as GenericObject;
      string a_uri = file.get("a_uri") as String;
#if DEBUG
      if (Flt_Rec.go(Flt_Rec.where_t.load_file_flag))
        Flt_Rec.log("fCASL_c_exists ", a_uri);
#endif
      if (!misc.CASL_uri_is_file_uri("_subject", a_uri)) return true;
      return File.Exists(a_uri);
    }

    // Please add examples of what this is doing.  http://localhost/pos_demo => pos_demo
    public static string CASL_get_code_base()
    {
      if (c_CASL.GEO != null && c_CASL.GEO.has("code_base"))
      {
        return c_CASL.GEO.get("code_base") as string;
      }
      else if (c_CASL.code_base != null && c_CASL.code_base != "")
      {
        return c_CASL.code_base;
      }
      // return different thing based on whether from a Web server or local
      string folder_string = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
      folder_string = folder_string.Substring(0, folder_string.LastIndexOf("/"));
      folder_string = folder_string.Substring(0, folder_string.LastIndexOf("/"));
      folder_string = folder_string.Replace("file:///", "");
      return folder_string;
    }

    public static object CASL_get_code_base_impl(params object[] args)
    {
      return CASL_get_code_base();
    }


    /// <summary>
    /// sets:
    ///   GEO.site_physical      Ex: "C:/T4/site"   58 uses
    ///     MP: used for loading casl files
    ///     MP: reviewed all 58 users.  Any non-.casl uses were turned into:
    ///     Added: site_physical_log for where the log files are written.
    ///     Added: site_physical_write for top folder for where files are written
    ///     Added: site_physical_batch for where batch files are written.
    ///   GEO.site_static       Ex: "/site"    85 uses   (renamed some from site_logical)
    ///     URLs to static images like /site/pos_custom/images/foo.png
    ///     "/activity_log/" </join> 
    ///     
    ///   GEO.site_logical
    ///     MP: should be "/site"   used for dynamic calls like /site/my/0/post.htm?
    ///     
    ///   GEO.baseline_physical  Ex: "C:/T4/baseline" or "C:/T4/site/baseline"(EMBEDDED)    52 uses 
    ///     MP reviewed all 52 uses.  All were used for loading casl files except for 2:
    ///     MP: found example of writting js file in Business_f_service.casl
    ///       Need to dynamically write a to_js_top method on an object that will return a static string.
    ///        <file> <join> baseline_physical "/business/include/validation.js"</join> </file>.<set_content>
    ///       	<file> <join> baseline_physical "/business/include/autocomplete_array.js"</join> </file>.<set_content>
    ///   GEO.baseline_static   Ex: "baseline" or "site/baseline" (EMBEDDED)   127 uses  
    ///      (to be renamed to baseline_static)
    ///     MP: should have a leading "/", like "/baseline"  used for URLs to static images
    ///     MP: looked at all 127 uses and all were pointing to static files (images, css, js)
    /// </summary>
    /// <returns></returns>
    /// 

    public static string get_site_name()
    {
      string site_physical = CASL_get_code_base();
      return site_physical.Substring(site_physical.LastIndexOf("/") + 1);
    }

    public static Object get_site_name_impl(params object[] args)
    {
      return get_site_name();
    }



    /*  public static bool set_baseline_and_site()
      {
        //Obsoletized by my.referrer_host
        //CL: site_physical_ext
        //string site_physical_ext = "http://localhost";
        //c_CASL.GEO.set("site_physical_ext", site_physical_ext);

        // site_physical stores file system path to get to internal load files
        string site_physical = CASL_get_code_base();  
        c_CASL.GEO.set("site_physical", site_physical);

        string site_logical = site_physical.Substring(site_physical.LastIndexOf("/")+1);
        c_CASL.GEO.set("site_logical", site_logical);

        string baseline_physical_embeded = site_physical + "/baseline";
        if( Directory.Exists(baseline_physical_embeded) ) // EMBEDED BASELINE
        {
        string baseline_physical = baseline_physical_embeded;
        c_CASL.GEO.set("baseline_physical", baseline_physical);
				
        string baseline_static = site_logical + "/baseline";
        c_CASL.GEO.set("baseline_static", baseline_static);
        }
        else // ASSUME PARALLEL BASELINE
        {
        string baseline_physical = site_physical.Replace(site_logical,"")+"baseline";
        c_CASL.GEO.set("baseline_physical", baseline_physical);
				
        string baseline_static = "baseline";
        c_CASL.GEO.set("baseline_static", baseline_static);
        }

        return true;

      }
    */
    /// <summary>
    /// handling single quote and double quote, 
    /// use double quote if no double quote in string, 
    /// use single quote if no single quote in string,
    /// or use double quote and encoding the double quote in string
    /// Return the result string
    /// </summary>
    /// <param name="a_string"></param>
    /// <returns></returns>
    public static string smart_quote(string a_string)
    {
      string xml_quote = "\"";
      if (a_string.IndexOf('"') < 0)
        xml_quote = "\"";
      else if (a_string.IndexOf('\'') < 0)
        xml_quote = "'";
      else
      {
        xml_quote = "\"";
        a_string = a_string.Replace("\"", "&quot;");
      }
      return string.Format("{0}{1}{0}", xml_quote, a_string);
    }

    public static object cs_array_to_GEO(Object[] a_array)
    {
      GenericObject R = c_CASL.c_GEO();
      for (int i = 0; i < a_array.Length; i++)
      {
        R.insert(a_array[i]);
      }
      return R;
    }

    public static string get_YYJJJ()
    {
      DateTime dtNow = DateTime.Now;
      return String.Format("{0:00}{1:000}", dtNow.ToString("yy"), dtNow.DayOfYear);
    }

    public static string get_YYYYJJJ(params object[] args)
    {
      DateTime dtNow = DateTime.Now;
      return String.Format("{0:0000}{1:000}", dtNow.Year, dtNow.DayOfYear);
    }

    public static object handle_if_missing(object if_missing, object key, GenericObject subject)
    {
      if (if_missing == c_CASL.c_error)
      {
        // does object have an _if_missing_method?  if so, then call it.
        if ((subject != null) && (subject.has("_if_missing_method")))
        {
          GenericObject a_method = subject.get("_if_missing_method") as GenericObject;
          GenericObject l_env = c_CASL.c_GEO();
          l_env.set("_subject", subject, "key", key);
          return base_casl_call(a_method, subject, l_env);
        }
        else
        {
          string message = String.Format("{0} not found", misc.to_eng(key));
          throw CASL_error.create("code", 220, "message", message, "key", key, "subject", subject);
        }
      }
      else
        return if_missing;
    }

    ////////////////////////////////////////////////////////////////////////////
    // Bug# 27153 DH - Clean up warnings when building ipayment
    //  cs convenient function 
    ////////////////////////////////////////////////////////////////////////////

    public static bool is_primitive(object obj)
    {
      if (obj == null || obj is string)
        return true;
      else if (obj is GenericObject)
        return false;
      else if (obj is int || obj is bool || obj is decimal || obj is double || obj is long || obj is char)
        return true;
      else
        return false;
    }


    public static bool is_primitive5(object obj)
    { // MP: Should also add req and opt as primitives.
      //     <or>
      //       a_geo.<is> null </is>
      //       a_geo.<is_a> type=Decimal </is_a>
      //       a_geo.<is_a> type=Integer </is_a>
      //       a_geo.<is_a> type=String </is_a>
      //       a_geo.<is_a> type=Boolean </is_a>
      //  </or>
      if (obj == null || obj is int || obj is long || obj is decimal || obj is double ||
        obj is string || obj is char || obj is bool)   // obj is Int16
        return true;
      else
        return false;
    }

    /// <summary>
    /// cs object include
    /// + none casl primitive type
    /// + none GEO (GenericObject with _parent)
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static bool is_cs_object(object obj)
    {
      if (is_primitive(obj))
        return false;
      else if ((obj is GenericObject && (obj as GenericObject).has("_parent")) || (obj == c_CASL.GEO))
        return false;
      else
        return true;
    }

    // replace to_string
    public static string to_htm(object obj)
    {
      string R = "";
      if (is_primitive(obj)) return primitive_to_htm(obj);
      else if (is_cs_object(obj)) return cs_object_to_htm(obj);
      else // GEO
      {
        if (c_CASL.c_opt.Equals(obj)) R = "";
        else if (c_CASL.c_req.Equals(obj)) R = "";
        else if (obj is GenericObject geo)
        {
          object P = geo.to_path(); // P-> path
          if (!false.Equals(P))  // HAS_PATH
          {
            if ("".Equals(P)) R = "GEO"; // ROOT
            else R = P as string;
            long n = geo.get_gen_obj_number();
            if (n >= 0)
              R += " [" + n.ToString() + "]";
          }
          else //NO_PATH, call to_htm
            R = base_casl_call("to_htm", obj, c_CASL.c_local_env()) as String;
        }
      }
      return R;
    }

    /// <summary>
    /// primitive_to_htm Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static string primitive_to_htm(object obj)
    {
      string R = null;
      if (obj == null) R = "";
      else if (obj is int || obj is long || obj is decimal || obj is double) // IS_NUMBER
        R = number_to_htm(obj) as string;
      else if (true.Equals(obj)) R = "true";
      else if (false.Equals(obj)) R = "false";
      else if (obj is string) R = (string)obj;
      else if (obj is char) R = "" + obj;
      else R = "" + obj;
      //else throw CASL_error.create("code",230, "message","parameter is not primitive type");
      return R;
    }

    public static string primitive_to_xml(object obj)
    {
      string R = null;
      if (obj == null) R = "null";
      else if (true.Equals(obj)) R = "true";
      else if (false.Equals(obj)) R = "false";
      else if (obj is int || obj is long || obj is decimal || obj is double) // IS_NUMBER
        R = number_to_htm(obj) as string;
      else if (obj is string)
      {
        string obj_str = (obj as String);
        // <![CDATA[ and ]]> will cause CDATA nesting incompatibilities
        bool has_nested_cdata = obj_str.IndexOf("<![CDATA[") != -1 || obj_str.IndexOf("]]>") != -1;
        // don't use CDATA if nested CDATA in the string
        // HACK: This heuristic breaks messaging so I'm disabling it
        //bug 12081 NJ- disable warnings for unreachable code
#pragma warning disable 429
        if (false && /* Disabling HACK */ (obj_str.StartsWith("<") || obj_str.StartsWith("&")) && !has_nested_cdata)
        {
          // Assumes that you are printing lots of XML, and don't want to see a lot of escape characters
          R = "<![CDATA[" + obj + "]]>";
        }
        else
        {
          R = smart_quote(obj_str);
          R = R.Replace("&", "&amp;");

          // Bugzilla Bug 6787 - Validation errors clear screen contents (related fix)
          // DJD: The previous replace statement may have inadvertently changed special characters
          // codes already in string (eg, "&quot;" to "&amp;quot;") - need to be changed back.
          R = R.Replace("&amp;quot;", "&quot;");
          R = R.Replace("&amp;lt;", "&lt;");
          R = R.Replace("&amp;gt;", "&gt;");

          R = R.Replace("<", "&lt;");
          //if (has_nested_cdata) //Isn't it good practice to do this anyway?
          R = R.Replace(">", "&gt;");
        }
#pragma warning restore 429
      }
      else if (obj is char) R = "" + obj;
      //else R = "" + obj;
      else throw CASL_error.create("code", 231, "message", "parameter is not primitive type");
      return R;
      //			if( !( subject is GenericObject ))
      //			{
      //				// string.<to_xml/> : add single quote and encode "<".
      //				if(subject is String) 
      //				{
      //					//					s_xml = "'"+subject+"'";
      //					string s_subject = subject.ToString();
      //					s_xml = smart_quote(s_subject);
      //					//------------------------------------------------
      //					// xml encoding for special character: 
      //					// &amp; & ampersand  (replace first)
      //					// &lt; < less than 
      //					// &gt; > greater than
      //					// &apos; ' apostrophe 
      //					// &quot; " quotation mark 
      //					s_xml = s_xml.Replace("&","&amp;");
      //					s_xml = s_xml.Replace("<","&lt;");
      //				}
      //					// Number.<to_xml/> : convert type
      //				else if(subject is Int32||subject is Decimal) s_xml = Convert.ChangeType(subject,typeof(string)).ToString();
      //					// Boolean.<to_xml/> : use lower case true and false
      //				else if (subject.Equals(true)) s_xml = "true";
      //				else if (subject.Equals(false)) s_xml = "false";
      //					// C#_Method.<to_xml/> : use casl path (cs.misc.CASL_to_xml)
      //				else if (subject is MethodInfo)
      //				{
      //					MethodInfo m_subject = subject as MethodInfo;
      //					string class_name = m_subject.ReflectedType.ToString();
      //					string method_name = m_subject.Name;
      //					s_xml = string.Format("cs.{0}.{1}",class_name,method_name);
      //				}
      //				else s_xml = "" + subject; 
      //				return s_xml;
    }

    public static object number_to_htm(object obj)
    {
      string R = null;
      if (obj is int || obj is long) R = obj.ToString();
      else if (obj is decimal) R = ((decimal)obj).ToString("0.00######"); // TODO: Bug #6307 -- Here's where it's hard coded to 2 digits
      else if (obj is double) R = ((double)obj).ToString("0.00######");
      else return MakeCASLError("Number format error", "232", String.Format("Parameter is not number type: {0}", obj));
      return R;
    }


    public static String format_dollar_amount(params Object[] args)
    {
      GenericObject geo_args = convert_args(args);
      object subject = geo_args.get("_subject");
      String sRV = "";

      // Bug# 15435 DH - Better error recovery is needed in formatting dollar amounts
      try
      {
        int iNegFormat = (int)geo_args.get("negative_format", 2);
        int iPosFormat = (int)geo_args.get("positive_format", 0);
        string sSeparator = (string)geo_args.get("group_separator", ",");
        string sCurSymbol = (string)geo_args.get("currency_symbol", "$");
        string sCurDecSeparator = (string)geo_args.get("decimal_separator", ".");
        int iDecLength = (int)geo_args.get("decimal_length", 2);
        /*
                    NOTE: Legitimate patterns are as follows:
                      Positive (indicator  result)
                      0 $n 
                      1 n$ 
                      2 $ n 
                      3 n $ 

                      Negative (indicator  result)
                      0 ($n) 
                      1 -$n 
                      2 $-n 
                      3 $n- 
                      4 (n$) 
                      5 -n$ 
                      6 n-$ 
                      7 n$- 
                      8 -n $ 
                      9 -$ n 
                      10 n $- 
                      11 $ n- 
                      12 $ -n 
                      13 n- $ 
                      14 ($ n) 
                      15 (n $) 
                  */

        //Convert into Decimal
        Decimal dAmount = 0;
        if (subject is int)
          dAmount = new Decimal((int)subject);
        else if (subject is long)
          dAmount = new Decimal((long)subject);
        else if (subject is float)
          dAmount = new Decimal((float)subject);
        else if (subject is double)
          dAmount = new Decimal((double)subject);
        else if (subject is decimal)
          dAmount = (decimal)subject;
        else if (subject is string)
          // Bug #12055 Mike O - Use misc.Parse to log errors
          dAmount = misc.Parse<Decimal>(subject.ToString());
        else
        {
          //If it's not a recognized type, just return the string representation
          //String sType = (subject.GetType()).ToString();
          return subject.ToString();
        }

        // Gets a NumberFormatInfo associated with the en-US culture.
        NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
        nfi.CurrencyNegativePattern = iNegFormat;
        nfi.CurrencyPositivePattern = iPosFormat;
        nfi.CurrencyGroupSeparator = sSeparator;
        nfi.CurrencySymbol = sCurSymbol;
        nfi.CurrencyDecimalSeparator = sCurDecSeparator;
        nfi.CurrencyDecimalDigits = iDecLength;
        //nfi.CurrencyGroupSizes = ? This is an int array Research before adding
        sRV = String.Format("{0:C}", dAmount);

        //sRV = dAmount.ToString("C", nfi);
      }
      catch (Exception ex)
      {
        // Bug# 15435 DH - Better error recovery is needed in formatting dollar amounts
        Logger.cs_log("format_dollar_amount error: " + ex.Message, Logger.cs_flag.error);
        return subject.ToString();
      }

      return sRV;
    }

    /// <summary>
    /// customized to_htm for cs objects including
    /// DateTime, GenericObject(without parent) etc. 
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static string cs_object_to_htm(object obj)
    {
      if (obj is GenericObject)
        return (obj as GenericObject).to_htm_large("");
      else if (obj is byte[])
      {
        string byteString = Convert.ToBase64String(obj as byte[]);
        if (byteString.Length > 10)
        {
          byteString = byteString.Substring(0, 10) + "...";
        }
        return byteString;
      }
      else
        return "" + obj;
    }

    public static string cs_object_to_xml(object obj)
    {
      return cs_object_to_xml(obj, false);
    }

    public static string cs_object_to_xml(object obj, bool exclude_sk)
    {
      if (obj is MethodInfo)
      {
        MethodInfo m_obj = obj as MethodInfo;
        string class_name = m_obj.ReflectedType.ToString();
        string method_name = m_obj.Name;
        return string.Format("cs.{0}.{1}", class_name, method_name);
      }
      else if (obj is GenericObject)
        return cs_GenericObject_to_xml(obj as GenericObject, exclude_sk);
      else if (obj is DateTime)
        return "\"" + obj + "\""; // Ex: a CS date will return "1/23/3004 5:40pm"
      else if (obj is byte[])
        return "<Bytes>\n\"" + Convert.ToBase64String(obj as byte[]) + "\"</Bytes>";
      else if (obj is ValueType)
        return "" + obj;
      else
        return "\"cs." + obj.GetType() + ": " + obj + "\"";
    }

    public static string cs_GenericObject_to_xml(GenericObject obj)
    {
      return cs_GenericObject_to_xml(obj, false);
    }

    public static string cs_GenericObject_to_xml(GenericObject obj, bool exclude_sk)
    {
      string s_xml = "";

      //-------------------------------------------------
      // handle object (instance and class)
      // set tag name by parent.to_path() else use GEO
      //-------------------------------------------------
      string tag_xml = "GenericObject"; //GEO
      string content_xml = "";
      // CONTENT
      content_xml = content_xml + CASL_to_xml_large_content("_subject", obj, "exclude_sk", exclude_sk, "depth", 3);

      if (content_xml == "")
        s_xml = string.Format("<{0}/>", tag_xml);
      else
        s_xml = string.Format("<{0}>{1}</{0}>", tag_xml, content_xml);
      return s_xml;
    }

    /// <summary>
    /// Bug 16638 SX
    /// type: braces,brackets,parentheses
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static object split_with_pairs(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      String sMessage = g_args.get("message", "") as String;
      String pairstype = g_args.get("type", "braces") as String;
      String[] splits = null;

      // Regex.Matches(sMessage, @"\[.*?\]").Cast<Match>().Select(m => m.Value).ToArray();
      if (pairstype == "braces")
        splits = Regex.Matches(sMessage, @"\{.*?\}").Cast<Match>().Select(m => m.Value).ToArray();
      else if (pairstype == "brackets")
        splits = Regex.Matches(sMessage, @"\[.*?\]").Cast<Match>().Select(m => m.Value).ToArray();
      else if (pairstype == "parentheses")
        splits = Regex.Matches(sMessage, @"\(.*?\)").Cast<Match>().Select(m => m.Value).ToArray();
      GenericObject splitsGEO = c_CASL.c_GEO();
      if (splits != null)
      {
        for (int i = 0; i < splits.Length; i++)
        {
          splitsGEO.insert(splits[i]);
        }
      }
      return splitsGEO;
    }

    public static object split(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object value = g_args.get("_subject");
      Object devider = g_args.get("separator");

      string separator = (string)(devider);
      string MainString = (string)(value);

      string[] splits = MainString.Split(new Char[] { separator[0] });
      object obj_return = (object)(splits);
      GenericObject splitsGEO = c_CASL.c_GEO();
      for (int i = 0; i < splits.Length; i++)
      {
        splitsGEO.insert(splits[i]);
      }

      return splitsGEO;
    }

    public static object string_split(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object value = g_args.get("_subject");
      Object devider = g_args.get("separator");

      string separator = (string)(devider);
      string MainString = (string)(value);

      string[] splits = MainString.Split(new Char[] { separator[0] });
      object obj_return = (object)(splits);
      GenericObject splitsGEO = c_CASL.c_GEO();
      foreach (string strItem in Regex.Split(MainString, separator))
      {
        if (strItem.Length != 0)
        {
          splitsGEO.insert(strItem);
        }
      }
      return splitsGEO;
    }
    public static string first_letter(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object value = g_args.get("_subject");
      Object devider = g_args.get("separator");

      string separator = (string)(devider);
      string MainString = (string)(value);
      string s_return = "";

      string[] splits = MainString.Split(new Char[] { separator[0] });
      object obj_return = (object)(splits);

      for (int i = 0; i < splits.Length; i++)
      {
        s_return += "<SPAN class='fl'>";
        s_return += splits[i][0];
        s_return += "</SPAN>";
        for (int r = 1; r < splits[i].Length; r++)
        {
          s_return += splits[i][r];
        }
        s_return += " ";
      }

      return s_return;
    }


    ////////////////////////////////DURATION-TIMESPAN//////////////////////
    /// <summary>
    /// day_difference Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args">
    /// ticks=opt=long
    /// days=0=int
    /// hours=0=int
    /// minutes=0=int
    /// seconds=0=int
    /// milliseconds=0=int
    /// </param>
    /// <returns></returns>
    ///Bug 9506 (Grouphealth)/Bug 9560 SN
    public static Object day_difference(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      Object date1 = args.get("date1");
      Object date2 = args.get("date2");
      DateTime date3 = Convert.ToDateTime(date1);
      DateTime date4 = Convert.ToDateTime(date2);
      TimeSpan ts = date3.Subtract(date4);
      int days = ts.Days;
      return days;
    }
    //end bug 9506
    public static Object make_duration(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      int days = (int)args.get("days", 0);
      int hours = (int)args.get("hours", 0);
      int minutes = (int)args.get("minutes", 0);
      int seconds = (int)args.get("seconds", 0);
      int milliseconds = (int)args.get("milliseconds", 0);

      TimeSpan result = new TimeSpan(0);
      result = new TimeSpan(days, hours, minutes, seconds, milliseconds);
      return result;
    }

    /// <summary>
    /// get_totaldays Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static Object get_totaldays(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      TimeSpan ts = (TimeSpan)((GenericObject)args.get("_subject")).get("__native_obj");
      return ts.TotalDays;
    }
    public static Object get_totalhours(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      TimeSpan ts = (TimeSpan)((GenericObject)args.get("_subject")).get("__native_obj");
      return ts.TotalHours;
    }
    public static Object get_totalminutes(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      TimeSpan ts = (TimeSpan)((GenericObject)args.get("_subject")).get("__native_obj");
      return ts.TotalMinutes;
    }
    public static Object get_totalseconds(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      TimeSpan ts = (TimeSpan)((GenericObject)args.get("_subject")).get("__native_obj");
      return ts.TotalSeconds;
    }
    public static Object get_totalmilliseconds(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      TimeSpan ts = (TimeSpan)((GenericObject)args.get("_subject")).get("__native_obj");
      return ts.TotalSeconds;
    }
    ////////////////////////////////DATES//////////////////////
    /// <summary>
    /// Takes care of dates, years, month,days, and time
    /// </summary>
    /// <returns>ints and objects</returns>

    public static Object make_date(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object date2 = g_args.get("base");
      long returns = 0;

      string date = (string)date2;
      if (date != "")
      {
        // Bug #12055 Mike O - Use misc.Parse to log errors
        DateTime dt = misc.Parse<DateTime>(date);
        returns = dt.Ticks;
      }
      return returns;
    }

    public static Object date_more(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object first = g_args.get("first");
      Object second = g_args.get("second");

      if ((long)first >= (long)second)
        return true;
      else
        return false;
    }
    
    public static Object date_less(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object first = g_args.get("first");
      Object second = g_args.get("second");

      if ((long)first <= (long)second)
        return true;
      else
        return false;
    }

    public static Object get_year(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object ticks = g_args.get("ticks");
      long tick = (long)ticks;
      DateTime dt = new DateTime(tick);

      return dt.Year;
    }
    
    public static Object get_month(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object ticks = g_args.get("ticks");
      long tick = (long)ticks;
      DateTime dt = new DateTime(tick);
      return dt.Month;
    }
    
    public static Object get_day(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object ticks = g_args.get("ticks");
      long tick = (long)ticks;
      DateTime dt = new DateTime(tick);
      return dt.Day;
    }

    // BUG#4601 
    public static Object get_days_in_month(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object ticks = g_args.get("ticks");
      long tick = (long)ticks;
      DateTime dt = new DateTime(tick);
      int yr = dt.Year;
      int mo = dt.Month;
      return System.DateTime.DaysInMonth(yr, mo);
    }
    // END OF BUG#4601 

    public static Object yyyymmdd(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object ticks = g_args.get("ticks");
      long tick = (long)ticks;
      DateTime dt = new DateTime(tick);

      return dt.ToString("yyyyMMdd");
    }

    public static Object DDMMYYYY(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object ticks = g_args.get("ticks");
      long tick = (long)ticks;
      DateTime dt = new DateTime(tick);

      return dt.ToString("dd-MMM-yyyy");
    }

    public static Object MMDDYYHHMM(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object ticks = g_args.get("ticks");
      long tick = (long)ticks;
      DateTime dt = new DateTime(tick);
      return dt.ToString("MMddyyHHmm");
    }

    public static Object get_formatted(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      Object ticks = g_args.get("ticks");
      string sFormat = g_args.get("format") as string;
      long tick = (long)ticks;
      DateTime dt = new DateTime(tick);

      return dt.ToString(sFormat, DateTimeFormatInfo.InvariantInfo);
    }

    public static Object db_date(params Object[] args)
    {

      GenericObject g_args = convert_args(args);
      Object ticks = g_args.get("ticks");
      long tick = (long)ticks;
      DateTime dt = new DateTime(tick);
      return dt.ToString("yyyy-MM-dd HH:mm:ss");
    }

    // Bug 26346 UMN added for allocation support
    public static Object add_days(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      GenericObject subject = g_args.get("_subject") as GenericObject;
      long ticks = (long)g_args.get("ticks");
      int days = (int)g_args.get("days");
      DateTime dt = new DateTime(ticks).AddDays(days);
      subject.set("base", dt.ToString("d"), "__native_obj", dt.Ticks);
      return subject;
    }

    // Bug 26346 UMN added for allocation support
    public static Object add_months(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      GenericObject subject = g_args.get("_subject") as GenericObject;
      long ticks = (long)g_args.get("ticks");
      int months = (int)g_args.get("months");
      DateTime dt = new DateTime(ticks).AddMonths(months);
      subject.set("base", dt.ToString("d"), "__native_obj", dt.Ticks);
      return subject;
    }

    // Bug 26346 UMN added for allocation support
    public static Object add_years(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      GenericObject subject = g_args.get("_subject") as GenericObject;
      long ticks = (long)g_args.get("ticks");
      int years = (int)g_args.get("years");
      DateTime dt = new DateTime(ticks).AddYears(years);
      subject.set("base", dt.ToString("d"), "__native_obj", dt.Ticks);
      return subject;
    }

    /*public static object get_time()
          {  
            DateTime dt = DateTime.Now;
            TimeSpan time = dt.TimeOfDay;
            return time;
          }
          public static object get_date()
          {  
            DateTime dt = DateTime.Now;
            DateTime date = dt.Date;
            return date;
          }
          */

    /////////////////////////////////////////////////////////////////////
    public static Object make_date2(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      Object base_ = args.get("base", c_CASL.c_opt);
      Object year = args.get("year", c_CASL.c_opt);
      Object month = args.get("month", c_CASL.c_opt);
      Object day = args.get("day", c_CASL.c_opt);

      DateTime result;
      if (base_ is DateTime)
        result = (DateTime)base_;
      else if (!c_CASL.c_opt.Equals(base_))
      {
        string s_base = base_ as String;
        if (s_base.Length == 0)
        {
          result = new DateTime();
        }
        else if (s_base.Length <= 10)
        {
          string[] date_parts = (base_ as String).Split('/');
          if (date_parts[2].Length <= 2) date_parts[2] = "20" + date_parts[2];

          int i_year = int.Parse(date_parts[2]),
            i_month = int.Parse(date_parts[0]),
            i_day = int.Parse(date_parts[1]);


          while (i_month > 12)
          {
            i_month -= 12;
            i_year++;
          }
          while (i_month <= 0)
          {
            i_month += 12;
            i_year--;
          }

          if (i_day > DateTime.DaysInMonth(i_year, i_month))
          {
            while (i_day > DateTime.DaysInMonth(i_year, i_month))
            {
              i_day -= DateTime.DaysInMonth(i_year, i_month);
              i_month++;
              while (i_month > 12)
              {
                i_month -= 12;
                i_year++;
              }
            }
          }
          else if (i_day <= 0)
          {
            while (i_day <= 0)
            {
              i_month--;
              while (i_month <= 0)
              {
                i_month += 12;
                i_year--;
              }
              i_day += DateTime.DaysInMonth(i_year, i_month);
            }
          }
          result = new DateTime(i_year, i_month, i_day);
        }
        else
        {
          // Bug #12055 Mike O - Use misc.Parse to log errors
          result = misc.Parse<DateTime>(s_base);
        }
      }
      else if (year != c_CASL.c_opt || month != c_CASL.c_opt || day != c_CASL.c_opt)
        result = new DateTime((int)year, (int)month, (int)day);
      else
        throw CASL_error.create("message", "Cannot create date, ", "code", 253);
      return result;
    }
    /// <summary>
    /// Datetime_plus Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="arg_pairs">_subject=req a_duration=req </param>
    /// <returns></returns>
    public static object Datetime_plus(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      TimeSpan a_duration = (TimeSpan)((GenericObject)args.get("a_duration")).get("__native_obj");
      DateTime a_datetime = (DateTime)((GenericObject)args.get("_subject")).get("__native_obj");

      DateTime result_datetime = a_datetime.Add(a_duration);
      object result = c_CASL.c_make("Datetime", "__native_obj", result_datetime);
      return result;
    }
    /// <summary>
    /// Datetime_minus Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="arg_pairs">_subject=req a_duration=req</param>
    /// <returns></returns>
    public static object Datetime_minus(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      TimeSpan a_duration = (TimeSpan)((GenericObject)args.get("a_duration")).get("__native_obj");
      DateTime a_datetime = (DateTime)((GenericObject)args.get("_subject")).get("__native_obj");

      DateTime result_datetime = a_datetime.Subtract(a_duration);
      object result = c_CASL.c_make("Datetime", "__native_obj", result_datetime);
      return result;
    }

    public static Object date_more2(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      DateTime first_datetime = (DateTime)(((GenericObject)args.get("first")).get("__native_obj"));
      DateTime second_datetime = (DateTime)(((GenericObject)args.get("second")).get("__native_obj"));
      if ((long)first_datetime.Ticks > (long)second_datetime.Ticks) // Was more or equal
        return true;
      else
        return false;
    }

    /// <summary>
    /// date_less2 Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args">first=req=GEO.Datetime second=req=Datetime </param>
    /// <returns></returns>
    public static Object date_less2(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      DateTime first_datetime = (DateTime)(((GenericObject)args.get("first")).get("__native_obj"));
      DateTime second_datetime = (DateTime)(((GenericObject)args.get("second")).get("__native_obj"));

      if ((long)first_datetime.Ticks < (long)second_datetime.Ticks) // Was less or equal
        return true;
      else
        return false;
    }

    public static Object get_year2(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      DateTime dt = (DateTime)(((GenericObject)args.get("_subject")).get("__native_obj"));

      return dt.Year;
    }
    /// <summary>
    /// get_month2 Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="arg_pairs">_subject=req=GEO.Datetime </param>
    /// <returns></returns>
    public static Object get_month2(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      DateTime dt = (DateTime)(((GenericObject)args.get("_subject")).get("__native_obj"));

      return dt.Month;
    }
    public static Object get_day2(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      DateTime dt = (DateTime)(((GenericObject)args.get("_subject")).get("__native_obj"));
      return dt.Day;
    }

    public static Object get_hour2(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      DateTime dt = (DateTime)(((GenericObject)args.get("_subject")).get("__native_obj"));
      return dt.Hour;
    }

    public static Object get_minute2(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      DateTime dt = (DateTime)(((GenericObject)args.get("_subject")).get("__native_obj"));
      return dt.Minute;
    }

    public static Object get_second2(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      DateTime dt = (DateTime)(((GenericObject)args.get("_subject")).get("__native_obj"));
      return dt.Second;
    }

    public static Object date_current(params Object[] args)
    {
      DateTime dt = DateTime.Now;
      return dt.ToString("G"); // BUG#14670  General date/long time with seconds 
    }

    public static Object get_formatted2(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      DateTime dt = (DateTime)(((GenericObject)args.get("_subject")).get("__native_obj"));
      string sFormat = args.get("format") as string;

      return dt.ToString(sFormat, DateTimeFormatInfo.InvariantInfo);
    }
    ///////////////////////////////////////////////////////
    // TOREMOVE: use get_formmated instead 
    public static string yyyymmdd2(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      DateTime dt = (DateTime)(((GenericObject)args.get("_subject")).get("__native_obj"));

      return dt.ToString("yyyyMMdd");
    }

    public static string DDMMYYYY2(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      DateTime dt = (DateTime)(((GenericObject)args.get("_subject")).get("__native_obj"));

      return dt.ToString("dd-MMM-yyyy");
    }

    public static string MMDDYYHHMM2(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      DateTime dt = (DateTime)(((GenericObject)args.get("_subject")).get("__native_obj"));
      return dt.ToString("MMddyyHHmm");
    }

    public static string db_date2(params Object[] arg_pairs)
    {

      GenericObject args = convert_args(arg_pairs);
      DateTime dt = (DateTime)(((GenericObject)args.get("_subject")).get("__native_obj"));

      return dt.ToString("yyyy-MM-dd HH:mm:ss");
    }

    // BEGIN Bug 24112 DJD: Time Zone for Receipts Configurable at Workgroup Level
    const string BUSINESS_STD_TIME_ZONE = "Standard Business Time Zone";
    const string IPAYMENT_TIME_FORMAT = "MM/dd/yyyy h:mm:ss tt";

    /// <summary>
    /// Bug 24112 DJD: Time Zone for Receipts Configurable at Workgroup Level
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static Object get_time_for_zone(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      GenericObject returnGeo = new GenericObject();

      try
      {
        string zone = GetStringFromArgs(args, "zone");
        // The time zone config value will be a string so if it is null then it is the old (and unused) time_zone configured value
        if (zone == null)
        {
          zone = "";
        }
        string dateTime = GetStringFromArgs(args, "date_time");
        if (dateTime == null) // This MAY be a DateTime object (printing duplicate receipt)
        {
          DateTime? dtArg = GetDateTimeFromArgs(args, "date_time");
          dateTime = dtArg.HasValue ? ((DateTime)dtArg).ToString(IPAYMENT_TIME_FORMAT) : "";
        }

        string dtReturn = "";
        string abbrevReturn = "";

        DateTime dt = DateTime.Now;
        if (!string.IsNullOrWhiteSpace(dateTime))
        {
          if (!DateTime.TryParse(dateTime, out dt))
          {
            dt = DateTime.Now;
          }
        }

        if ((string.IsNullOrWhiteSpace(zone) || zone.Equals(BUSINESS_STD_TIME_ZONE)) && !string.IsNullOrWhiteSpace(dateTime))
        {
          dtReturn = dateTime;
        }
        else
        {
          DateTime? dt_converted = ConvertToTimeZone(dt, zone);
          if (dt_converted.HasValue)
          {
            dtReturn = ((DateTime)dt_converted).ToString(IPAYMENT_TIME_FORMAT);
            abbrevReturn = GetTimeZoneAbbreviation(zone);
          }
        }

        returnGeo.Add("date_time", dtReturn);
        returnGeo.Add("abbrev", abbrevReturn);
      }
      catch (Exception e)
      {
        // Log the error
        string errorDetail = String.Format("Get Time for Zone Exception:{0}{1}", Environment.NewLine, e.ToMessageAndCompleteStacktrace());
        Logger.cs_log(errorDetail, Logger.cs_flag.error);
        returnGeo.Clear();
      }

      return returnGeo;
    }

    private static DateTime? GetDateTimeFromArgs(GenericObject args, string key)
    {
      DateTime? value = null;
      object obj = args.get(key, "");

      if (!c_CASL.c_opt.Equals(obj))
      {
        if (obj.GetType() == typeof(System.DateTime))
        {
          value = (DateTime)obj;
        }
      }
      return value;
    }

    private static string GetStringFromArgs(GenericObject args, string key)
    {
      string value = "";
      object obj = args.get(key, "");

      if (!c_CASL.c_opt.Equals(obj))
      {
        if (obj.GetType() == typeof(System.String))
        {
          value = (string)obj;
          if (string.IsNullOrWhiteSpace(value))
          {
            value = "";
          }
        }
        else
        {
          value = null;
        }
      }
      return value;
    }

    private static string GetTimeZoneAbbreviation(string zone)
    {
      string abbrev = "";
      switch (zone)
      {
        case "Standard Business Time Zone":
          abbrev = "";
          break;
        case "Hawaiian Standard Time":
          abbrev = "HST";
          break;
        case "Alaskan Standard Time":
          abbrev = "AKST";
          break;
        case "Pacific Standard Time":
          abbrev = "PST";
          break;
        case "Mountain Standard Time":
          abbrev = "MST";
          break;
        case "US Mountain Standard Time":
          abbrev = "US MST";
          break;
        case "Central Standard Time":
          abbrev = "CST";
          break;
        case "Eastern Standard Time":
          abbrev = "EST";
          break;
        case "US Eastern Standard Time":
          abbrev = "US EST";
          break;
        case "Atlantic Standard Time":
          abbrev = "AST";
          break;
        case "SA Western Standard Time":
          abbrev = "SA WST";
          break;
        case "Newfoundland Standard Time":
          abbrev = "NST";
          break;
        case "Canada Central Standard Time":
          abbrev = "CA CST";
          break;
      }
      return abbrev;
    }

    private static DateTime? ConvertToTimeZone(DateTime sourceTime, string destinationTimeZone)
    {
      DateTime? convertedTime = null;
      try
      {
        if (string.IsNullOrWhiteSpace(destinationTimeZone) ||
            destinationTimeZone == BUSINESS_STD_TIME_ZONE)
        {
          convertedTime = sourceTime;
#if DEBUG
          Logger.cs_log(string.Format("No time zone conversion needed for the {0} Time zone.",
                                      BUSINESS_STD_TIME_ZONE),
                        Logger.cs_flag.debug);
#endif
        }
        else
        {
          TimeZoneInfo destinationZone = TimeZoneInfo.FindSystemTimeZoneById(destinationTimeZone);
          convertedTime = TimeZoneInfo.ConvertTime(sourceTime, TimeZoneInfo.Local, destinationZone);
#if DEBUG
          Logger.cs_log(string.Format("Local time {0} converted to Time zone {1}: {2}."
            , sourceTime.ToString(IPAYMENT_TIME_FORMAT)
            , destinationTimeZone
            , ((DateTime)convertedTime).ToString(IPAYMENT_TIME_FORMAT)),
            Logger.cs_flag.debug);
#endif
        }
      }
      catch (TimeZoneNotFoundException)
      {
        LogEntry(string.Format("The registry does not define the {0} Time zone.", destinationTimeZone));
        convertedTime = null;
      }
      catch (InvalidTimeZoneException)
      {
        LogEntry(string.Format("Registry data on the {0} Time zone has been corrupted.", destinationTimeZone));
        convertedTime = null;
      }
      return convertedTime;
    }
    // END Bug 24112 DJD

    /*public static object get_time()
          {  
              DateTime dt = DateTime.Now;
              TimeSpan time = dt.TimeOfDay;
              return time;
          }
          public static object get_date()
          {  
              DateTime dt = DateTime.Now;
              DateTime date = dt.Date;
              return date;
          }
          */
    ///////////////////////////////////////////////////////
    // not yet used.  Needs to have window impersonate method to login as another user
    public static void execute_os_command(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      String a_file = (string)g_args.get("file");
      object os_args = g_args.get("args");
      if (os_args.Equals(c_CASL.c_opt))
        System.Diagnostics.Process.Start(a_file);
      else
        System.Diagnostics.Process.Start(a_file, (string)os_args);
    }

    public static bool CASL_uri_is_file_uri(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      String uri = (string)g_args.get("_subject");
      return new Uri(uri).IsFile;
    }

    public static Object CASL_uri_is_file_uri_impl(params Object[] args)
    {
      return CASL_uri_is_file_uri(args);
    }

    public static string file_get_content(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      String uri = (string)g_args.get("a_uri");
      String returns;

      TextReader tr = null;

      // Bug 7121 Mike O: Set certificate validation policy to our custom settings
      //Bug 12081 NJ-disable compiler warning
#pragma warning disable 618
      ServicePointManager.CertificatePolicy = new CertificatePolicy(g_args);
#pragma warning restore 618

      // Bug #5867 Mike O (6/9/09): Resolved inability to access ExternalCallsHandlerService.asmx through SSL
      if (uri.StartsWith("http://") || uri.StartsWith("https://"))
      {
        try
        {
          WebClient wc = new WebClient();
          tr = new StreamReader(new WebClient().OpenRead(uri));
        }
        catch (Exception e)
        {
          //TODO: Figure out exactly why this causes problems and fix it instead of just catching it
          //Logger.cs_log("misc.cs: file_get_content Caught an error in file_get_content: " + e.Message);
          // HX: Bug 17849
          Logger.cs_log("misc.cs: file_get_content Caught an error in " +
               "file_get_content: " + e.ToMessageAndCompleteStacktrace(),
               Logger.cs_flag.warn);
          // TODO: Determine how to catch this certificate error and prevent the user from logging in
          //          if(e.Message.IndexOf("trust") >= 0)
          //          {
          //            CASL_call("error","code","WS-102","title","Failed to make Web Service Request", "message",e.Message, "throw",false);
          //          }
          //          else
          //          {
          Logger.cs_log("misc.cs: file_get_content Discarding the error and" +
              "returning a blank message instead.",
              Logger.cs_flag.warn);
          //          }
          return "";
        }
      }
      else
      {
        tr = new StreamReader(uri);
      }

      returns = tr.ReadToEnd();
      tr.Close();
      return returns;
    }

    // Called by: CASL_execute_file
    public static byte[] file_get_content_bytes(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      String uri = (string)g_args.get("a_uri");
      object len = g_args.get("length", c_CASL.c_opt);
      int int_len = 0;

      BinaryReader br = new BinaryReader(new StreamReader(uri).BaseStream);
      if (c_CASL.c_opt == len)
        int_len = (int)br.BaseStream.Length;
      else
        int_len = (int)len;

      byte[] returns;

      returns = br.ReadBytes(int_len);
      br.Close();
      return returns;
    }

    public static string file_get_last_line(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      String uri = (string)g_args.get("a_uri");
      String returns = "";

      TextReader tr = new StreamReader(uri);
      string strLine = tr.ReadLine();

      while (strLine != null)
      {
        returns = strLine;
        strLine = tr.ReadLine();
      }


      tr.Close();
      return returns;
    }

    public static string copy_file(params Object[] args)
    {

      GenericObject g_args = convert_args(args);
      String to_uri = (string)g_args.get("to_uri");
      String from_uri = (string)g_args.get("from_uri");

      FileInfo fi = new FileInfo(from_uri);
      FileInfo fi2 = new FileInfo(to_uri);
      DirectoryInfo dir = new DirectoryInfo(fi2.DirectoryName);
      try
      {
        dir.Create();
      }
      catch (Exception e)
      {
        string message = e.Message;
        // HX: Bug 17849
        Logger.cs_log_trace("misc.cs", e);
      }
      try
      {
        fi.CopyTo(to_uri, true);
      }
      catch (Exception e)
      {
        string error_mess = e.Message;
        // HX: Bug 17849
        Logger.cs_log_trace("misc.cs", e);
      }

      return "Success";
    }


    public static string file_set_content(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      String uri = (string)g_args.get("a_uri");
      object content = g_args.get("content");

      FileInfo fi = new FileInfo(uri);
      DirectoryInfo dir = new DirectoryInfo(fi.DirectoryName);
      try
      {
        dir.Create();
      }
      catch (Exception e)
      {
        string message = e.Message;
        System.Console.WriteLine(message);
        // HX: Bug 17849
        Logger.cs_log_trace("Error in directory create", e);
      }
      try
      {
        if (content is String)
        {
          StreamWriter sw = new StreamWriter(uri);
          sw.Write((string)content);
          sw.Close();
        }
        else if (content is byte[])
        {
          FileStream fs = File.Create(uri);
          BinaryWriter bw = new BinaryWriter(fs);  // how to get uri as a file stream?
          bw.Write((byte[])content);
          bw.Close();
        }
      }
      catch (Exception e)
      {
        string message = e.Message;
        System.Console.WriteLine(message);

        // HX: Bug 17849
        Logger.cs_log_trace("Error in file create", e);
      }
      return "";
    }

    // Bug 11989 Mike O - Holds one mutex for each file that has been written to
    // Bug 19856 UMN - I left this in place because the CASL file class uses it, and we use file in a few places
    // like images and flex. But it's an open question if we should revert to the pre-11989 code?
    private static Dictionary<string, Mutex> file_insert_mutexes = new Dictionary<string, Mutex>();

    /// <summary>
    /// file_insert Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args"> </param>
    /// <returns></returns>
    public static string file_insert(params Object[] args)
    {
      GenericObject g_args = convert_args(args);
      string uri = (string)g_args.get("a_uri");
      object content = g_args.get("content");
      string text = null;

      if (content is GenericObject)
      {
        GenericObject geoContent = (GenericObject)content;
        text = geoContent.to_xml();
      }
      else
      {
        text = content.ToString();
      }

      FileInfo fi = new FileInfo(uri);
      DirectoryInfo dir = new DirectoryInfo(fi.DirectoryName);
      try
      {
        dir.Create();
      }
      catch (Exception e)
      {
        string message = e.Message;
        System.Console.WriteLine(message);

        // HX: Bug 17849
        Logger.cs_log_trace("Error in file_insert", e);
      }
      // Bug #11989 Mike O - Get/create the mutex for this file, then get in line to write to said file
      Mutex mutex = null;
      try
      {
        if (!file_insert_mutexes.TryGetValue(uri, out mutex))
        {
          mutex = new Mutex();
          file_insert_mutexes[uri] = mutex;
        }

        mutex.WaitOne();

        TextWriter tw = new StreamWriter(uri, true);
        tw.Write(text);
        tw.Close();
      }
      catch (Exception e)
      {
        string message = e.Message;
        System.Console.WriteLine(message);

        // HX: Bug 17849
        Logger.cs_log_trace("Error in file_insert", e);
      }
      finally
      {
        mutex.ReleaseMutex();
      }
      // End Bug #11989 Mike O
      return "";
    }
    public static Object Decimal_round(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      object obj = args.get("obj");
      object digit = args.get("digit");
      string method = (string)args.get("method", "Round"); // Bug 19205 MJO - Allow rounding up or truncating

      Decimal R = 0;
      int d = 0;
      Decimal result = 0;

      try
      {
        if (digit == null)
          d = 0;
        else if (digit is String)
          digit = CASL_execute("_subject", digit);
        d = (int)Convert.ChangeType(digit, typeof(int));

        if (obj == null)
          obj = 0;
        else if (obj is String)
          obj = CASL_execute("_subject", obj);
        R = (Decimal)Convert.ChangeType(obj, typeof(Decimal));

        // Bug 18636 UMN - Remove faulty CommonRound functions
        // Bug 19205 MJO - Added support for forced rounding up and truncating
        if (method.Equals("Round"))
          result = decimal.Round(R, d, MidpointRounding.AwayFromZero);
        else if (method.Equals("Round Up"))
        {
          decimal multiplier = Convert.ToDecimal(Math.Pow(10, d));
          result = Math.Ceiling(R * multiplier) / multiplier;
        }
        else
          result = Math.Truncate(R * 100) / 100;
      }
      catch (Exception ex)
      {
        string message = ex.Message;
        throw CASL_error.create("message", "can not round " + obj + " to " + digit + " digits.", "code", 250);
      }
      return result;
    }
    /// <summary>
    /// BUG20544 CANADIAN ROUNDING 
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static object CASL_Rounding(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      string sRoundingType = args.get("rounding_type") as string;

      Decimal dRoundingTotal;
      decimal.TryParse("" + args.get("rounding_total"), out dRoundingTotal);
      if (sRoundingType != "Canadian") return dRoundingTotal;
      Decimal dRoundedValue = Math.Round(dRoundingTotal * 20) / 20;
      return dRoundedValue;
    }
    /// <summary>
    /// BUG20671 
    ///  converting_amount=req
    ///  FOREIGN_CURRENCY_EXCHANGE_RATE=1.00
    ///  direction="localtoforeign"=<one_of> "localtoforeign" "foreigntolocal" </one_of>
    ///  exchange_difference_limit = 0.01  <!-- define exchange diffference because of always rounding down 2 decimal points from foreign to local currency-->
    ///  max_amount=opt
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static object CASL_Convert_Currency(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      Decimal dRate;
      decimal.TryParse("" + args.get("FOREIGN_CURRENCY_EXCHANGE_RATE"), out dRate);

      Decimal dConvertingTotal;
      decimal.TryParse("" + args.get("converting_amount"), out dConvertingTotal);

      Decimal dMaxAmount = dConvertingTotal;
      if (args.has("max_amount") && args.get("max_amount") != c_CASL.c_opt)
        decimal.TryParse("" + args.get("max_amount"), out dMaxAmount);

      Decimal dExchangeDifferenceLimit;
      decimal.TryParse("" + args.get("exchange_difference_limit"), out dExchangeDifferenceLimit);

      String sDirection = args.get("direction") as string;

      Decimal dConvertedTotal = dConvertingTotal;
      if (sDirection == "localtoforeign")
      {
        dConvertedTotal = Math.Round(dConvertingTotal / dRate, 2);// normal rounding with 2 decimal point   
      }
      else if (sDirection == "foreigntolocal")
      {
        dConvertedTotal = Math.Round(dConvertingTotal * dRate, 2); // normal rounding with 2 decimal point  
      }
      if (Math.Abs(dMaxAmount - dConvertedTotal) <= dExchangeDifferenceLimit)
      {
        dConvertedTotal = dMaxAmount;
      }

      return dConvertedTotal;
    }

#if not_yet
    public static object fCASL_Integer_from(CASL_Frame frame)
    {
      // Bug #14029 Mike O - Set the default to null, so we know
      // whether or not this was provided
      object obj = frame.args.get("obj", null);
      if (obj == null)
        return obj;

      // Bug 12440 NJ-if conversion fails, return this value.
      // If a value is not provided,set to original value      
      object if_fail = frame.args.get("if_fail", c_CASL.c_opt);
      return Integer_from(obj, if_fail);
    }

    public static object Integer_from(
       object obj,
       object if_fail)
    {
      object R = 0;
      try
      {
        if (obj is String)
        {
          R = int.Parse(obj as string, NumberStyles.Any);
        }
        else
        {
          R = (int)Convert.ChangeType(obj, typeof(int));
        }
      }
      catch (Exception ex)
      {
        //Bug 12440 NJ-Possible scenarios to check before giving up-empty string and decimal value
        int result;
        bool isReParsed = false;
        string str = obj as string;
        if (str.IndexOf('.') > -1 && !str.Equals(""))
        {
          obj = str.Substring(0, str.IndexOf('.'));
          isReParsed = int.TryParse(obj as string, out result);
          R = result;
        }
        if (!isReParsed)
        {
          //end bug 12440
          string message = ex.Message;

          // Bug #14029 Mike O - Only log when if_fail isn't provided
          if (if_fail == c_CASL.c_opt)
          {
            Logger.LogError(message, "Integer_from (250)");

            // HX: Bug 17849
            Logger.cs_log("Error in integer from: \"" + str + "\"" +
                Environment.NewLine + ex.ToMessageAndCompleteStacktrace(),
                Logger.cs_flag.error);
            R = obj;
          }
          else
          {
            R = if_fail; //Bug 12440 NJ-return this value
          }
        }

      }
      //Bug 12440 - added per Review
      if (R.Equals(c_CASL.c_opt))
      {
        R = obj;
      }
      //end bug 12440
      return R;
    }
#endif


    public static object Integer_from(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      object obj = args.get("obj");
      // Bug #14029 Mike O - Set the default to null, so we know whether or not this was provided
      object if_fail = args.get("if_fail", c_CASL.c_opt);//Bug 12440 NJ-if conversion fails, return this value.If a value is not provided,set to original value      
      if (obj == null) return obj;//(int)0;
      object R = 0;
      try
      {
        if (obj is String)
        {
          R = int.Parse(obj as string, NumberStyles.Any);
        }
        else
        {
          R = (int)Convert.ChangeType(obj, typeof(int));
        }
      }
      catch (Exception ex)
      {
        //Bug 12440 NJ-Possible scenarios to check before giving up-empty string and decimal value
        int result;
        bool isReParsed = false;
        string str = obj as string;
        if (str.IndexOf('.') > -1 && !str.Equals(""))
        {
          obj = str.Substring(0, str.IndexOf('.'));
          isReParsed = int.TryParse(obj as string, out result);
          R = result;
        }
        if (!isReParsed)
        {
          //end bug 12440
          string message = ex.Message;

          // Bug #14029 Mike O - Only log when if_fail isn't provided
          if (if_fail == c_CASL.c_opt)
          {
            Logger.LogError(message, "Integer_from (250)");

            // HX: Bug 17849
            Logger.cs_log("Error in integer from: \"" + str + "\"" +
                Environment.NewLine + ex.ToMessageAndCompleteStacktrace(),
                Logger.cs_flag.error);
            R = obj;
          }
          else
          {
            R = if_fail; //Bug 12440 NJ-return this value
          }
        }

      }
      //Bug 12440 - added per Review
      if (R.Equals(c_CASL.c_opt))
      {
        R = obj;
      }
      //end bug 12440
      return R;
    }

    public static object Decimal_from(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      object obj = args.get("obj");
      // Bug #14029 Mike O - Set the default to null, so we know whether or not this was provided
      object if_fail = args.get("if_fail", c_CASL.c_opt);//Bug 12440 NJ-if conversion fails, return this value.If a value is not provided,set to original value
      if (obj == null) return obj;//0.0M;
      object R = 0.0M;
      try
      {
        if (obj is String)
        {
          string str = obj as string;
          if (!str.Equals(""))//Bug 12440 NJ-do not parse empty string
          {
            R = Decimal.Parse(obj as string, NumberStyles.Any);
          }
          else
          {
            R = if_fail; //Bug 12440 - return empty string itself
          }
        }
        else
        {
          R = (decimal)Convert.ChangeType(obj, typeof(decimal));
        }
      }
      catch (Exception ex)
      {
        string message = ex.Message;

        // Bug #14029 Mike O - Only log when if_fail isn't provided
        if (if_fail == c_CASL.c_opt)
        {
          Logger.LogError(message, "Decimal_from (250)");
          // HX: Bug 17849
          Logger.cs_log("Error in decimal from: \"" + obj.ToString() + "\"" +
              Environment.NewLine + ex.ToMessageAndCompleteStacktrace(),
              Logger.cs_flag.error);
          R = obj;
        }
        else
        {
          R = if_fail; //Bug 12440 NJ-return this value
        }
      }
      //Bug 12440 - added per Review
      if (R.Equals(c_CASL.c_opt))
      {
        R = obj;
      }
      //end bug 12440
      return R;
    }

    public static String c_trim(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      string a_string = args.get("_subject") as String;
      return a_string.Trim();
    }
    public static String c_trim_start(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      string a_string = args.get("_subject") as String;
      return a_string.TrimStart();
    }
    public static String c_trim_end(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      string a_string = args.get("_subject") as String;
      return a_string.TrimEnd();
    }

    public static Object c_string_more(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      string subject = args.get("_subject") as String;
      string a_value = args.get("a_value") as String;
      bool result = subject.CompareTo(a_value) > 0;
      return result;
    }

    public static Object c_string_less(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      string subject = args.get("_subject") as String;
      string a_value = args.get("a_value") as String;
      bool result = subject.CompareTo(a_value) < 0;
      return result;
    }

    public static GenericObject c_uri_builder(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      GenericObject a_uri = args.get("_subject") as GenericObject;
      if (a_uri.has("uri_string"))
      {
        string uri_string = a_uri.get("uri_string") as String;
        UriBuilder uri_inst = new UriBuilder(uri_string);
        a_uri.set("port", uri_inst.Port);
        a_uri.set("query", uri_inst.Query);
        a_uri.set("scheme", uri_inst.Scheme);
        a_uri.set("host", uri_inst.Host);
        a_uri.set("path", uri_inst.Path);
      }
      else
      {
        a_uri.set("uri_string",
          a_uri.get("scheme", "", true) + "://" +
          a_uri.get("host", "", true) + ":" +
          a_uri.get("port", "", true) +
          a_uri.get("path", "", true) +
          a_uri.get("query", "", true)
          );
      }
      return a_uri;
    }

    //BUG 6207 PCI BLL: moved SendHttpRequest from here to pci.cs

    /*  Function will return a GEO of file(s).
              directory = the directory you are searching
              file_name = the name of the file, can also accept wildcard(*)
              extension_pattern = the extension of the file, can also accept wildcard(*)  */

    public static GenericObject findFile(params object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      string strFolder = args.get("directory") as string;
      string strFileName = args.get("file_name") as string;
      string strExtensionPattern = args.get("extension_pattern") as string;

      DirectoryInfo dirFolder = new DirectoryInfo(strFolder);

      FileInfo[] fiFiles = dirFolder.GetFiles(strFileName + "." + strExtensionPattern);
      GenericObject geoFiles = new GenericObject();

      for (int i = 0; i < fiFiles.Length; i++)
      {
        geoFiles.insert(fiFiles[i].Name);
      }

      return geoFiles;
    }
    // created for new Resource2 -- to be integrated with SendHttpRequest
    /// <summary>
    /// SendHttpRequest2 Bug# 27153 DH - Clean up warnings when building ipayment
    /// </summary>
    /// <param name="args">
    /// a_request=req=Request
    /// sample request:
    /// <Request>
    /// a_uri=<URI> uri_string="" </URI>
    /// body=""
    /// </Request>
    /// </param>
    /// <returns></returns>
    public static object SendHttpRequest2(params Object[] args)
    {
      GenericObject geo_args = convert_args(args);
      GenericObject a_resource = geo_args.get("_subject") as GenericObject;
      int timeout = (int)geo_args.get("timeout", 10000);

      GenericObject a_response = c_CASL.c_make("Response") as GenericObject;// Response object
      GenericObject a_request = a_resource.get("a_request") as GenericObject;
      string user_agent = (string)a_request.get("user_agent", c_CASL.c_error, true);
      string http_method = ((string)a_request.get("http_method", c_CASL.c_error, true)).ToUpper();
      string content_type = (string)a_request.get("content_type", c_CASL.c_error, true);
      string body = (string)a_request.get("body", c_CASL.c_error, true);

      GenericObject a_uri = a_request.get("a_uri") as GenericObject;
      string uri_string = a_uri.get("uri_string") as String;
      string scheme = a_uri.get("scheme", c_CASL.c_error, true) as String;

      HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri_string);

      string PostingData = body;

      request.Method = http_method; // Method;    
      request.UserAgent = user_agent; // UserAgent;
      request.Timeout = timeout;
      request.AllowAutoRedirect = false;

      if (http_method == "POST")
      {
        request.ContentType = content_type; // ContentType;
        request.ContentLength = PostingData.Length;

        Stream writeStream = request.GetRequestStream();
        UTF8Encoding encoding = new UTF8Encoding();// NOT SURE IF THIS IS NEEDED
        byte[] bytes = encoding.GetBytes(PostingData);
        writeStream.Write(bytes, 0, bytes.Length);
        writeStream.Close();
        // log("method","make_request", "url",strPath, "body",PostingData);
      }

      HttpWebResponse response = (HttpWebResponse)request.GetResponse();
      Stream responseStream = response.GetResponseStream();
      StreamReader readStream = new StreamReader(responseStream, Encoding.UTF8);
      string strResponse = readStream.ReadToEnd();
      responseStream.Close();

      a_response.set("body", strResponse);
      a_response.set("content_type", response.ContentType.ToString());

      HttpContext.Current.ApplicationInstance.CompleteRequest(); // Bug 16461

      a_resource.set("a_response", a_response);
      return a_resource;
    }



    public static string casl_to_lowercase(params object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      string subject = args.get("_subject") as string;
      return subject.ToLower();
    }
    public static string casl_to_uppercase(params object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      string subject = args.get("_subject") as string;
      return subject.ToUpper();
    }
    // not used
    public static string get_windows_username(params object[] arg_pairs)
    {
      //LogonUser
      return (string)System.Security.Principal.WindowsIdentity.GetCurrent().Name;
    }

    // Bug 23231 MJO - Add string to HashSet (only used for post_methods right now)
    public static string HashSetStringAdd(params object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      HashSet<string> hs = args.get("hashset") as HashSet<string>;
      string value = args.get("value") as string;

      hs.Add(value);

      return null;
    }

    ///////////////////////////////////////////////////////////////
    // remote call for web service
    ///////////////////////////////////////////////////////////////

    // Bug 7121 Mike O: Set certificate validation policy to our custom settings
    // helper enum and class to support custom verification of certificates
    private enum CertificateProblem : uint
    {
      CertVALID = 0,
      CertEXPIRED = 0x800B0101,
      CertVALIDITYPERIODNESTING = 0x800B0102,
      CertROLE = 0x800B0103,
      CertPATHLENCONST = 0x800B0104,
      CertCRITICAL = 0x800B0105,
      CertPURPOSE = 0x800B0106,
      CertISSUERCHAINING = 0x800B0107,
      CertMALFORMED = 0x800B0108,
      CertUNTRUSTEDROOT = 0x800B0109,
      CertCHAINING = 0x800B010A,
      CertREVOKED = 0x800B010C,
      CertUNTRUSTEDTESTROOT = 0x800B010D,
      CertREVOCATION_FAILURE = 0x800B010E,
      CertCN_NO_MATCH = 0x800B010F,
      CertWRONG_USAGE = 0x800B0110,
      CertUNTRUSTEDCA = 0x800B0112
    }

    // Class to validate certificates based upon given policy
    private class CertificatePolicy : ICertificatePolicy
    {

      private bool will_accept_untrusted_ca = false;
      private bool will_accept_invalid_date = false;
      private bool will_accept_invalid_name = false;

      public CertificatePolicy(GenericObject args)
      {
        will_accept_untrusted_ca = (bool)args.get("will_accept_untrusted_ca", false);
        will_accept_invalid_date = (bool)args.get("will_accept_invalid_date", false);
        will_accept_invalid_name = (bool)args.get("will_accept_invalid_name", false);
      }

      public bool CheckValidationResult(ServicePoint sp, X509Certificate cert,
        WebRequest request, int problem)
      {
        switch ((uint)problem)
        {
          case (uint)CertificateProblem.CertVALID:
            return true;
          case (uint)CertificateProblem.CertEXPIRED:
            if (will_accept_invalid_date)
            {
              return true;
            }
            break;
          case (uint)CertificateProblem.CertUNTRUSTEDCA:
            if (will_accept_untrusted_ca)
            {
              return true;
            }
            break;
          case (uint)CertificateProblem.CertUNTRUSTEDROOT:
            if (will_accept_untrusted_ca)
            {
              return true;
            }
            break;
          case (uint)CertificateProblem.CertCN_NO_MATCH:
            if (will_accept_invalid_name)
            {
              return true;
            }
            break;
        }
        Logger.cs_log("misc.cs: Rejected certificate provided by " +
                      request.RequestUri + ": " +
                      GetProblemMessage((CertificateProblem)problem),
                      Logger.cs_flag.warn);
        return false;
      }

      private String GetProblemMessage(CertificateProblem Problem)
      {
        String ProblemMessage = "";
        CertificateProblem problemList = new CertificateProblem();
        String ProblemCodeName = Enum.GetName(problemList.GetType(), Problem);
        if (ProblemCodeName != null)
          ProblemMessage = ProblemMessage + "-Certificateproblem:" +
            ProblemCodeName;
        else
          ProblemMessage = "Unknown Certificate Problem";
        return ProblemMessage;
      }
    }
    // END Bug 7121 Mike O
    // BUG20017 remove ExternalCallHandlers.asmx from ipayment 

    public static Object clear_and_set_session_impl(params Object[] args)
    {
      clear_and_set_session();
      return null;
    }

    // Called by: GEO.reload and Page_Load in handle_request_2.cs
    public static void clear_and_set_session()
    {
#if DEBUG
      CASL_Stack.verify(null);
#endif
      if (System.Web.HttpContext.Current != null) // should always be true (Use an assert?)
      {
        // Bug 17513 MJO - Store DOUBLESUBMIT value before clearing the session
        GenericObject my = CASLInterpreter.get_my();
        string doublesubmit = my == null ? "" : (string)my.get("__DOUBLESUBMIT__", "");

        System.Web.HttpContext.Current.Session.Clear();

        // Bug 17048 UMN PCI-DSS set session timeout to configured value
        // Bug 24623 Encryption: null reference
        GenericObject B_data = c_CASL.c_object("Business.Misc.data") as GenericObject;
        int timeout = 14;
        if (B_data != null)
          timeout = (int)B_data.get("session_timeout_minutes", 14);

        System.Web.HttpContext.Current.Session.Timeout = timeout + 1;//BUG19824 Delay server side timeout 1 min in order for cbc to redirect to exit url 

        GenericObject session = c_CASL.create_session_my() as GenericObject;

        // Bug 17513 MJO - Restore the DOUBLESUBMIT value into the new session
        session.set("__DOUBLESUBMIT__", doublesubmit);

        System.Web.HttpContext.Current.Session.Add("my", session);

        // Guaranteed a session id at entry
        String session_id = System.Web.HttpContext.Current.Session.SessionID;

        session.set("SessionID", session_id); // BUG#5091 log with session id. SX 07/07/2008

        // Bug 20435 UMN PCI-DSS
        System.Web.SessionState.SessionIDManager manager = new System.Web.SessionState.SessionIDManager();
        session.set("MaskedSessionID", manager.CreateSessionID(System.Web.HttpContext.Current));
      }
    }
    //Bug 8341/8890
    public static String get_ip_address(params object[] arg)
    {
      String ip_address = "";
      if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null)
      {// BUG#7986 Need check .NET CRASH if called from worker thread
        ip_address = HttpContext.Current.Request.UserHostAddress;
      }
      return ip_address;
    }

    /// <summary>
    /// This is a variant of get_ip_address() that uses the ServerVariables and checks HTTP_X_FORWARDED_FOR.
    /// Using the HTTP_X_FORWARDED_FOR may allow us to look behind a proxy to get the real IP.
    /// See: http://stackoverflow.com/questions/735350/how-to-get-a-users-client-ip-address-in-asp-net
    /// Bug 16663
    /// </summary>
    /// <returns></returns>
    public static String get_ip_address_vars(params object[] arg)
    {
      try
      {
        if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Request == null)
          return "";

        string ipList = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipList))
        {
          string forwardedIp = ipList.Split(',')[0];
          return forwardedIp;
        }

        string remoteAddr = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        return remoteAddr;
      }
      catch
      {
        return "";
      }
    }


    // end bug 8341/8890
    //BUG#5532 
    public static object set_LOADING_STATUS(params object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      string LOADING_STATUS = args.get("LOADING_STATUS") as string;
      if (System.Web.HttpContext.Current != null)
        HttpContext.Current.Application["LOADING_STATUS"] = LOADING_STATUS;
      else
        return false;
      return true;
    }
    //BUG#5532 
    public static object get_LOADING_STATUS(params object[] arg_pairs)
    {
      if (System.Web.HttpContext.Current != null)
        return HttpContext.Current.Application["LOADING_STATUS"] as string;
      return "";
    }

    //    public static object session_set_my (params object[] arg_pairs ) 
    //    { 
    //      GenericObject args = convert_args(arg_pairs);
    //      GenericObject session=args.get("session") as GenericObject; 
    //      if (System.Web.HttpContext.Current != null) 
    //      {
    //        System.Web.HttpContext.Current.Session.Clear();
    //        System.Web.HttpContext.Current.Session.Add("my",session);
    //      } 
    //      else 
    //      {
    //        c_CASL.GEO.set("my",session);
    //        session.set("_container",c_CASL.GEO);
    //        session.set("_name","my");
    //      }
    //      return null;
    //    }

    /// <summary>
    ///  IMPORTANT: CASL CODE STEPPER BREAKPOINT 
    ///  bp is a way to set a breakpoint in CASL.
    ///  Requires VS.NET IDE to be in Debug mode, and a
    ///  breakpoint set in the method below.
    ///  1. Break anywhere in CASL (inside CASL IDE, inside any CASL method 
    ///     from running application)
    ///     <bp/>              {BP/}
    ///  2. Break anywhere in CASL with subject
    ///     foo.<bp/>           foo.{BP/}
    ///  
    ///  Once it reached the following break point, Check the two variables 
    ///  inside debugging local window of VS.NET IDE.
    ///  1. local_env - see all variable in CASL local environment at the CASL breakpoint
    ///  2. subject - if subject is not available, show "no subject", otherwise show 
    ///  subject information
    /// </summary>
    /// <param name="arg_pairs"> _subject=opt outer_env=req </param>
    /// <returns></returns>
    public static object CASL_breakpoint(params Object[] arg_pairs)
    {
      GenericObject args = convert_args(arg_pairs);
      object expr_value = args.get("_subject", "no bp_subject");
      string code_info = args.get("code_info") as string;
      string source = args.get("source") as string;
      if (CASLInterpreter.step_mode == 9)
      {
        return expr_value;
      }
      int step_mode = (int)args.get("step_mode", 0);
      GenericObject local_env = args.get("outer_env") as GenericObject;
      object _subject = local_env.get("_subject", "no _subject");
      object this_method = "no method";
      if (local_env.has("_this_method"))
        this_method = (local_env.get("_this_method") as GenericObject).to_path();
      string code_to_execute = "";
      string echo = "";












      // To use breakpoints:  set a breakpoint on the line below
      // You can add .<BP/> into the CASL code
      // When this breakpoint is reached, select and drag these lines into the Watch variables:
      /*
      code_info
      source
      ((System.Collections.Hashtable)(local_env))
      this_method
      _subject
      expr_value
      echo

      ((System.Collections.Hashtable)((((System.Collections.Hashtable)(local_env)))))["x"]
      ((System.Collections.Hashtable)((((System.Collections.Hashtable)(local_env)))))["z"]
      */

      object result = null;  // ALWAYS SET A BREAKPOINT <BP/> HERE.

      //  "source" has the source code for that expression
      // expr_value holds the value of the current expression
      //   "code_to_execute" type in a CASL expression in this variable
      //         to excute CASL code at break point, 
      //   "result" holds the result
      //   "step_mode" to dyanamically change step mode (FUTURE)
      //   "echo" list the CASL expressions that you want to echo to the console.   _subject Number 10  equivalent to: <echo> _subject Number 10 </echo>












      if (!"".Equals(code_to_execute))
      {
        result = misc.CASL_execute_and_format("source", code_to_execute, "output_format", "xml", "local_env", local_env);
        result = result + "";
      }
      if (!"".Equals(echo))
      {
        misc.CASL_execute_and_format("source", "<echo>" + echo + "</echo>", "output_format", "xml", "local_env", local_env);
      }
      CASLInterpreter.step_mode = step_mode;
      return expr_value;
    }


    ////////////////////////////////////////////////////////////////////////////
    //Bug# 27153 DH - Clean up warnings when building ipayment
    //  end of cs convenient function 
    ////////////////////////////////////////////////////////////////////////////

    //------------------------  template ------------------------------------
    //		public static object CASL_xxx(params Object[] args)
    //		{
    //			//--------convert args and validate args-----------------------------
    //			GenericObject geo_args = convert_args(args);
    //			object subject = geo_args.get("_subject");
    //			object subject_value = geo_args.get(0);
    //
    //			GenericObject contract = new GenericObject("_subject",CASLInterpreter.REQ, 0, CASLInterpreter.REQ);
    //			// manually validate args
    //			if(subject == null || !(subject is GenericObject) || subject_value == null)
    //			{
    //				string s_message = string.Format("CASL Runtime Error(invalid argument): in CASL_xxx");
    //				throw CASL_error.create_str(s_message);
    //			}
    //			//--------end to convert args and validate args-----------------------------
    //			(subject as GenericObject).set("_subject",subject_value);
    //			return null;
    //		}
    //------------------------  template ------------------------------------

    public static string PauseSeconds(params object[] args)
    {
      // Added by Daniel for testing Database Connections.
      // CL: Changed argument names to match functionality
      GenericObject geo_args = misc.convert_args(args);
      int iMilliSecondsToPause = (int)geo_args.get("milliseconds", 1000);

      Thread.Sleep(iMilliSecondsToPause);

      return "";
    }

    // ----------------- CL TIME START --------------- //
    public static string CASL_now(params object[] args)  // seconds appear: 2007/03/12 15:07:06.243
    {
      GenericObject geo_args = misc.convert_args(args);
      string format = (string)geo_args.get("format", "yyyy/MM/dd HH:mm:ss.fff");

      return System.DateTime.Now.ToString(format);
    }

    public static Object date_current_ticks(params object[] args)
    {
      return System.DateTime.Now.Ticks.ToString();
    }
    // ----------------- CL TIME END --------------- //

    public static string CASL_reverse_string(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      string subject = (string)geo_args.get("_subject", "");

      string toReturn = "";

      for (int i = 0; i < subject.Length; i++)
      {
        toReturn = subject[i] + toReturn;
      }
      return toReturn;
    }
    // ----------------- CL LOG START --------------- //
    // Use only for development and debugging

    // Bug 19865 UMN Logger.cs_log is now Logger.cs_log.

    //GH Testing ANU 
    public static Object Check_Log_Entry(params object[] arg)
    {
      bool log = (bool)c_CASL.GEO.get("Add_Log_Entry", true, false);
      return log;
    }

    //GH Testing ANU 
    public static Object Check_disable_echo(params object[] arg)
    {
      bool disableecho = (bool)c_CASL.GEO.get("disable_echo", true, false);
      return disableecho;
    }

    public static string CASL_subtract_long(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      return "" +
        (long.Parse((string)geo_args.get("_subject", 0))
        - long.Parse((string)geo_args.get(0, 0)));
    }
    public static bool debug_mode()
    {
      return (c_CASL.GEO != null) && ((bool)c_CASL.GEO.get("debug", false));
    }
    public static void CASL_debug_mode(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      string mode = (string)geo_args.get("mode", "disable");
      if (mode == "enable")
      {
        c_CASL.GEO.set("debug", true);
      }
      else
      {
        c_CASL.GEO.set("debug", false);
      }
      return;
    }
    public static string expr_to_string(object a_expr)
    {
      string to_return = "";
      if (a_expr == null || a_expr.GetType().IsValueType || a_expr.GetType() == typeof(System.String))
      {
        to_return = "value: " + a_expr.ToString();
      }
      else if (a_expr is GenObj_Symbol)
      {
        to_return = "symbol: " + (string)(a_expr as GenericObject).get("name", "no_name");
      }
      else if (a_expr is GenObj_Path)
      {
        if (Tool.is_cs_path(a_expr)) // if path is call_name, then return methodInfo
        {
          to_return = "***cs_path***";
        }
        else
        {
          to_return = "path: " + expr_to_string((a_expr as GenericObject).get(0));
          for (int i = 1; i < ((int)(a_expr as GenericObject).getLength()); i++)
          {
            to_return = to_return + "." + expr_to_string((a_expr as GenericObject).get(i));
          }
        }
      }
      else // is either call or a regular object to return
      {
        to_return = "call: " + (string)((a_expr as GenericObject).get("_parent") as GenericObject).get("name", "no_name");
      }
      return to_return;
    }

    public static GenericObject CASL_deserialize(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      string serializedString = geo_args.get("value") as string;

      byte[] buffer = Convert.FromBase64String(serializedString);

      MemoryStream ostream = new MemoryStream(buffer);

      BinaryFormatter bf = new BinaryFormatter();

      return bf.Deserialize(ostream) as GenericObject;
    }

    /**
            * This is a useless test function -- It'll only work if you embed core_logo.png in
            * the CASL_engine project in an images folder.  I'd rather not delete it, since if
            * I accidentally delete valuable data, this may just survive to show what I did in 
            * the first place.
          public static GenericObject CASL_get_core_logo(params object[] args) 
          {
              System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
              Stream resourceStream = assembly.GetManifestResourceStream(assembly.GetName().Name + ".images.core_logo.png");
              BinaryReader imageReader = new BinaryReader( resourceStream );
        
              long length = imageReader.BaseStream.Length;

              byte[] result = imageReader.ReadBytes((int) length);
              imageReader.Close();

              GenericObject a_response = c_CASL.c_make("response") as GenericObject;

              a_response.set("content_type", "image");
              a_response.set("body_bytes", result);
              a_response.set("result", result);

              return a_response;
          }
          */

    public static byte[] CASL_bytes_from_string(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      string a_value = geo_args.get("a_value", c_CASL.c_req) as String;

      return Convert.FromBase64String(a_value);
    }
    public static byte[] CASL_bytes_from_string_2(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      string a_value = geo_args.get("a_value", c_CASL.c_req) as String;
      return System.Text.Encoding.Default.GetBytes(a_value);
    }

    public static string CASL_string_from_bytes(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      byte[] a_value = geo_args.get("a_value", c_CASL.c_req) as byte[];
      return System.Text.Encoding.Default.GetString(a_value);
    }
    
    public static Object CASL_bytes_length(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      byte[] a_subject = geo_args.get("_subject", c_CASL.c_req) as byte[];

      return a_subject.Length;
    }
    public static byte[] CASL_bytes_copy(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      byte[] a_subject = geo_args.get("_subject", c_CASL.c_req) as byte[];

      return a_subject.Clone() as byte[];
    }
    public static string CASL_Bytes_htm_medium(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      byte[] a_subject = geo_args.get("_subject", c_CASL.c_req) as byte[];

      return Convert.ToBase64String(a_subject);
    }

    public static string CASL_Bytes_as_string(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      byte[] a_subject = geo_args.get("_subject", c_CASL.c_req) as byte[];

      //System.Text.Encoding.Default;
      System.Text.UTF8Encoding enc = new System.Text.UTF8Encoding();
      // System.Text.ASCIIEncoding enc = new System.Text.Encoding   .ASCIIEncoding();
      return enc.GetString(a_subject);
    }

    public static string StringToBase64String(string s)
    {
      return Convert.ToBase64String(System.Text.Encoding.Default.GetBytes(s));
    }
    public static string CASL_string_to_base64string(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      string a_subject = geo_args.get("_subject", "") as string;
      return StringToBase64String(a_subject);
    }
    public static string CASL_base64string_to_string(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      string a_subject = geo_args.get("_subject", "") as string;
      return System.Text.Encoding.Default.GetString(Convert.FromBase64String(a_subject));
    }

    // Bug 17128 UMN Add new functions to safely base64 encode a URL.
    // Taken from https://tools.ietf.org/html/draft-ietf-jose-json-web-signature-08#appendix-C

    // Bug 17128 UMN Implements <to_base64string>
    public static string CASL_string_to_base64stringURL(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      string a_subject = geo_args.get("_subject", "") as string;
      return Base64UrlEncode(System.Text.Encoding.Default.GetBytes(a_subject));
    }

    /// <summary>
    /// Because Base64 includes some characters (+, / and =) that are an issue in URLs, use this
    /// Bug 17128 UMN Add new functions to safely base64 encode a URL.
    /// </summary>
    /// <param name="arg"></param>
    /// <returns></returns>
    public static string Base64UrlEncode(byte[] arg)
    {
      string s = Convert.ToBase64String(arg); // Regular base64 encoder
      s = s.Split('=')[0]; // Remove any trailing '='s
      s = s.Replace('+', '-'); // 62nd char of encoding
      s = s.Replace('/', '_'); // 63rd char of encoding
      return s;
    }

    // Bug 17128 UMN Implements <from_base64string>
    public static string CASL_base64stringURL_to_string(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      string a_subject = geo_args.get("_subject", "") as string;
      return System.Text.Encoding.Default.GetString(Base64UrlDecode(a_subject));
    }

    /// <summary>
    /// Inverse of Base64UrlEncode.
    /// Bug 17128 UMN Add new functions to safely base64 encode a URL.
    /// </summary>
    /// <param name="arg"></param>
    /// <returns></returns>
    public static byte[] Base64UrlDecode(string arg)
    {
      string s = arg;
      s = s.Replace('-', '+'); // 62nd char of encoding
      s = s.Replace('_', '/'); // 63rd char of encoding
      switch (s.Length % 4) // Pad with trailing '='s
      {
        case 0: break; // No pad chars in this case
        case 2: s += "=="; break; // Two pad chars
        case 3: s += "="; break; // One pad char
        default:
          throw new System.Exception(
   "Illegal base64url string!");
      }
      return Convert.FromBase64String(s); // Standard base64 decoder
    }


    public static object CASL_Image_to_png(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      byte[] content = (geo_args.get("_subject") as GenericObject).get("content") as byte[];

      System.Drawing.Bitmap output_image = new System.Drawing.Bitmap(
        new System.IO.MemoryStream(content));

      MemoryStream result_stream = new MemoryStream();
      output_image.Save(result_stream, System.Drawing.Imaging.ImageFormat.Png);
      byte[] result = result_stream.ToArray();
      result_stream.Close();

      GenericObject a_image = c_CASL.c_make("Image") as GenericObject;

      a_image.set("content_type", "image/png");
      a_image.set("content", result);

      return a_image;
    }

    public static object CASL_Image_convert(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      GenericObject subject = geo_args.get("_subject") as GenericObject;
      byte[] content = subject.get("content") as byte[];
      string format = geo_args.get("content_type") as string;

      GenericObject a_image = subject.copy(c_CASL.c_opt,
        new GenericObject(0, "content", 1, "content_type"));

      System.Drawing.Bitmap output_image = new System.Drawing.Bitmap(
        new System.IO.MemoryStream(content));

      System.Drawing.Imaging.ImageFormat imageFormat = null;
      string content_type = "";
      switch (format)
      {
        case "image/jpeg":
        case "image/jpg":
        case "jpeg":
        case "jpg":
          imageFormat = System.Drawing.Imaging.ImageFormat.Jpeg;
          content_type = "image/jpeg";
          break;
        case "image/tiff":
        case "image/tif":
        case "tiff":
        case "tif":
          imageFormat = System.Drawing.Imaging.ImageFormat.Tiff;
          content_type = "image/tiff";
          break;
        case "image/bmp":
        case "bmp":
          imageFormat = System.Drawing.Imaging.ImageFormat.Bmp;
          content_type = "image/bmp";
          break;
        case "image/png":
        case "png":
        default:
          imageFormat = System.Drawing.Imaging.ImageFormat.Png;
          content_type = "image/png";
          break;
      }

      MemoryStream result_stream = new MemoryStream();
      output_image.Save(result_stream, imageFormat);
      byte[] result = result_stream.ToArray();
      result_stream.Close();

      a_image.set("content_type", content_type);
      a_image.set("content", result);

      return a_image;
    }
    // BUG16884 save non image file into folder and return file name based on content type
    public static object CASL_Store_non_image_file(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      GenericObject subject = geo_args.get("_subject") as GenericObject;
      string filefolder = geo_args.get("file_folder", "download_files") as string;
      byte[] content = subject.get("content") as byte[];
      string format = subject.get("content_type") as string;

      string fileextention = "";
      CASL_engine.CASL_server.uploadfileformatwhitelist.TryGetValue(format, out fileextention);//Bug16884 use file upload whitelist to get file extention

      string filename = String.Format("{0}.{1}", Guid.NewGuid().ToString(), fileextention);
      string filewholename = String.Format("{0}\\{1}\\{2}", c_CASL.code_base, filefolder, filename);

      File.WriteAllBytes(filewholename, content);
      return filename;
    }
        
    public static object CASL_Image_mask_image_micr(params Object[] args)// Bug# 11071 DH - Add security based MICR masking on document images after scan.
    {
      GenericObject geo_args = misc.convert_args(args);
      GenericObject subject = geo_args.get("_subject") as GenericObject;

      try
      {        
        byte[] content = subject.get("content") as byte[];

        System.Drawing.Bitmap img1 = new System.Drawing.Bitmap(new System.IO.MemoryStream(content));
        System.Drawing.Bitmap img = new System.Drawing.Bitmap(img1.Width, img1.Height);// Resolve the Graphics issues with the bad index errors and draw on top of a new image instead.

        int iHeight = img.Height;
        int iWidth = img.Width;

        using (System.Drawing.Graphics gfx = System.Drawing.Graphics.FromImage(img))// Must be from a new image.
        using (System.Drawing.SolidBrush brush = new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(2, 58, 101)))
        {
          gfx.DrawImage(img1, 0, 0, img1.Width, img1.Height);
          gfx.FillRectangle(brush, 0, iHeight-110, iWidth, 100);

          using (var stream = new MemoryStream())
          {
            img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);// iPayment will flash and take a while to process anything else than PNG or JPEG
            subject.set("content", stream.ToArray());
          }
        }
      }
      catch(Exception)
      {
        return subject;// No masking
      }
    
      return subject;
    }

    public static object CASL_ConvertOneStepImage(params Object[] args)
    {
      // Bug# 26417 DH

      GenericObject geo_args = misc.convert_args(args);
      GenericObject subject = geo_args.get("_subject") as GenericObject;

      try
      {
        byte[] content = subject.get("content") as byte[];

        System.Drawing.Bitmap img = new System.Drawing.Bitmap(new System.IO.MemoryStream(content));
        using (var stream = new MemoryStream())
        {
          img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
          subject.set("content", stream.ToArray());
        }
      }
      catch (Exception)
      {
        return subject;
      }

      return subject;
    }

    public static object CASL_Image_get_native_dimensions(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      GenericObject subject = geo_args.get("_subject") as GenericObject;
      byte[] content = subject.get("content") as byte[];

      System.Drawing.Bitmap output_image = new System.Drawing.Bitmap(
        new System.IO.MemoryStream(content));

      return new GenericObject(
        "_parent", c_CASL.GEO,
        "height", output_image.Height,
        "width", output_image.Width);
    }


    static public string BaseConvert(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      string subject = geo_args.get("_subject") as string;
      string digits = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ+-";

      long dividend = long.Parse(subject);

      string result = "";

      for (int i = 7; i >= 0; i--)
      {
        long divisor = (long)Math.Pow(64, i);

        long remainder = 0;
        long quotient = Math.DivRem(dividend, divisor, out remainder);
        //long remainder = dividend % divisor;

        result += digits[(int)quotient];
        //result += digits[int.Parse("" + ((long)(dividend / divisor)) )];

        dividend = remainder;
      }


      return result;
    }
    // ----------------- CL TIME END --------------- //


    /// <summary>
    /// Open file 'log.txt', if it exists, and write entry passed in by caller
    /// Purpose: Keep track of casl files loaded and order of execution
    /// </summary>
    /// <param name="sEntry"></param>
    static public void LogEntry(string sEntry)
    {
      try
      {
        //retrieve TEMP folder path & file
        string sLogFilePath = System.Environment.GetEnvironmentVariable("TEMP");
        sLogFilePath += "\\log.txt";

        //if log file exists log the entry; otherwise do nothing
        FileInfo fi = new FileInfo(sLogFilePath);
        if (fi.Exists)
        {
          using (StreamWriter w = File.AppendText(sLogFilePath))
          {
            //write entry
            w.WriteLine("{0} \t {1}", DateTime.Now, sEntry);
            // Close the writer and underlying file.
            w.Close();
          }
        }
      }
      catch (Exception e)
      {
        e.ToString();
      }
    }

    public static GenericObject convert_parent_from_string_to_object_with_nested(GenericObject a_GEO)
    {
      // set nested Genericobject _parent as string 
      ArrayList all_keys = a_GEO.getKeys();
      if (a_GEO.has("_parent"))
      {
        string parent_string = a_GEO.get("_parent", "GEO") as string;
        if (parent_string != null)
          a_GEO.set("_parent", c_CASL.c_object(parent_string));
      }

      foreach (object key in all_keys)
      {
        if (key.Equals("_parent")) continue; // SKIP _parent
        object value = a_GEO.get(key);
        if (value is GenericObject && ((GenericObject)value).has("_parent"))
          convert_parent_from_string_to_object_with_nested((GenericObject)value);
      }

      return a_GEO;
    }

    /// <summary>
    /// convert deserialized geo 
    ///   handle 
    ///     GEO path "GEO", "GEO.X"
    ///     my (ingore session)
    ///     _self path
    ///  TODO: handle my.x path
    /// </summary>
    /// <param name="serialized_geo"></param>
    /// <param name="mapping"></param>
    /// <param name="self_path"></param>
    /// <returns></returns>
    public static GenericObject convert_deserialized_geo(GenericObject serialized_geo, GenericObject mapping, string self_path)
    {
      if (self_path.Equals("_self")) // handle top_level "_self" to mapping
        mapping.set(self_path, serialized_geo); // mapping key is path, value is ref

      ArrayList all_keys = serialized_geo.get_normalized_keys(); // order is important for recursive serialization
      foreach (object key in all_keys)
      {
        // REMOVE SPECIAL KEY if(key.Equals("_parent") || key.Equals("_container") ) continue; // SKIP  recursive key including _parent, _container
        object value = serialized_geo.get(key);
        if (value is string && value.Equals("GEO"))
          serialized_geo.set(key, c_CASL.GEO);
        else if (value is string && ((string)value).StartsWith("GEO."))
          serialized_geo.set(key, c_CASL.c_object((string)value));
        else if (value is string && (value.Equals("my") || ((string)value).StartsWith("my.")))
          serialized_geo.set(key, value);
        else if (value is string && (value.Equals("_self") || ((string)value).StartsWith("_self.")))
          serialized_geo.set(key, mapping.get((string)value));
        else if (value is GenericObject)
        {
          string nested_path = self_path + "." + key;
          mapping.set(nested_path, value);
          convert_deserialized_geo((GenericObject)value, mapping, nested_path);
        }
        //else SKIP CS OBJECT VALUE
      }
      return serialized_geo;
    }

    /// <summary>
    /// convert nested GEO so 
    /// handle big system object like GEO, Transaction 
    ///   replace GEO path object to "GEO.X", GEO to "GEO"
    /// handle multiple references and recursive ref
    ///   use mapping before serialization and after deserialization, 
    ///   use self path in mapping "_self.x" to replace multiple references and recursive reference
    ///   
    /// inverse operation is: convert_deserialized_geo
    /// TODO:   
    ///   handle cs object
    ///   handle session object, in most of time, don't need pass session object. 
    ///    pass object only necessary.  probably not whole application like pos 
    /// </summary>
    /// <param name="geo_to_serialize"></param>
    /// <param name="mapping"></param>
    /// <param name="self_path"></param>
    /// <returns></returns>
    // Called by:  ipayment_remote_call, CASL_convert_geo_to_serialize  (was called by: WebQuery, Client_QueryOverWEB, Client_UpdateDBOverWEB, ipayment_handle_remote_call, UpdateDBOverWEB)
    public static GenericObject convert_geo_to_serialize(GenericObject geo_to_serialize, GenericObject mapping, string self_path)
    {
      GenericObject R = new GenericObject("_parent", "GEO");
      if (self_path.Equals("_self")) // handle top_level "_self" to mapping
        mapping.set(geo_to_serialize, self_path);
      ArrayList all_keys = geo_to_serialize.get_normalized_keys(); // order is important for recursive serialization
      foreach (object key in all_keys)
      {
        // REMOVE SPECIAL KEY if(key.Equals("_parent") || key.Equals("_container") ) continue; // SKIP  recursive key including _parent, _container
        object value = geo_to_serialize.get(key);
        if (Tool.is_casl_primitive(value) || value is Array) // HANDLE PRIMITIVE VALUE
          R.set(key, value);
        else if (value is GenericObject)
        {
          GenericObject geo_value = value as GenericObject;
          if (mapping.has(value)) // Has an internal path
          {
            String internal_path = mapping.get(value) as string;
            R.set(key, internal_path);
          }
          else if (has_GEO_path((GenericObject)geo_value))  // HANDLE EXTERNAL PATH (e.g. _parent, _container)
          {
            string parent_string = (string)misc.ToPath(geo_value, null);
            R.set(key, parent_string);
          }
          else if (Tool.is_casl_primitive(key)) // Does not previously exist and is not an object key
          {
            string nested_self_path = self_path + "." + key;
            mapping.set(value, nested_self_path);
            GenericObject nested_geo = convert_geo_to_serialize(geo_value, mapping, nested_self_path);
            R.set(key, nested_geo);
          }
          //else SKIP CS OBJECT VALUE
        }
      }
      return R;
    }

    public static string CASL_get_session_id(params object[] args)
    {
      return get_session_id();
    }

    public static string get_session_id()
    {
      String session_id;
      if (CASLInterpreter.get_my() != null && CASLInterpreter.get_my().has("SessionID"))
        session_id = CASLInterpreter.get_my()["SessionID"] as string;
      else if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
        session_id = "no_session_id";
      else
        session_id = System.Web.HttpContext.Current.Session.SessionID;
      return session_id;
    }

    // Bug 20435 UMN PCI-DSS
    public static string CASL_get_masked_session_id(params object[] args)
    {
      return get_masked_session_id();
    }

    public static string get_masked_session_id()
    {
      String session_id;
      if (CASLInterpreter.get_my() != null && CASLInterpreter.get_my().has("MaskedSessionID"))
        session_id = CASLInterpreter.get_my()["MaskedSessionID"] as string;
      else
        session_id = "no_session_id";
      return session_id;
    }


    // Called by: (is _impl for) GEO.convert_geo_to_serialize
    public static object CASL_convert_geo_to_serialize(params object[] args)  // GEO.<defmethod _name='convert_geo_to_serialize'>
    {
      GenericObject geo_args = misc.convert_args(args);
      GenericObject a_geo = geo_args.get("a_geo") as GenericObject;// opt _subject
      GenericObject a_converted_geo = convert_geo_to_serialize(a_geo, new GenericObject(), "_self");
      GenericObject converted_back_geo = convert_deserialized_geo(a_converted_geo, new GenericObject(), "_self");
      return converted_back_geo;
    }

    /// <summary>
    /// efficient function to test if object has GEO path, 
    /// details: check container tree for GEO
    /// TO TEST
    /// </summary>
    /// <param name="a_GEO"></param>
    /// <returns></returns>
    public static bool has_GEO_path(GenericObject a_GEO)
    {
      GenericObject object_with_container = a_GEO;
      if (a_GEO == null) return false;
      else if (a_GEO.Equals(c_CASL.GEO)) return true; // HANDLE GEO 
      GenericObject parent = a_GEO.get("_parent", null) as GenericObject;
      string a_GEO_name = a_GEO.get("_name", null) as string;
      if (parent != null && a_GEO_name != null && parent == c_CASL.GEO && parent.has(a_GEO_name)) return true;  // HANDLE DIRECT key under GEO e.g. GEO.X

      GenericObject my = CASLInterpreter.get_my();
      //if( parent!=null && parent == c_CASL.c_object("My")) return true; // HANDLE my path 
      if (a_GEO.Equals(my)) return true; // HANDLE my path 
      while (object_with_container.has("_container"))
      {
        GenericObject container = object_with_container.get("_container") as GenericObject;
        GenericObject container_parent = container.get("_parent", false) as GenericObject;
        if (container == null) return false;
        else if (container.Equals(c_CASL.GEO)) return true;  // HANDLE GEO.x path
                                                             //else if( container_parent!=null && container_parent.Equals(c_CASL.c_object("My"))) return false; // HANDLE my path 
        else if (container.Equals(my)) return false;  // HANDLE my.x path
        object_with_container = container;
      }

      return false;
    }

    /// <summary>
    /// Prepared a GenericObject to be serialized that is used by web service 
    /// Remove recursive reference before serialization
    ///    + make shallow copy in each nested level, return deep copy genericObject
    ///    + handle recursive key like "_parent" and "_container" 
    ///           set _parent as string path, remove _container key_value_pair
    /// Because of optimization consideration, replacing old casl function transform_parent_and_container_to_string_path with cs implementation
    /// </summary>
    /// <param name="a_GEO"></param>
    /// <returns></returns>
    public static GenericObject convert_object_to_serialize_by_removing_recursive_ref(GenericObject a_GEO)
    {
      GenericObject R = new GenericObject("_parent", c_CASL.GEO);
      // set nested Genericobject _parent as string 
      if (c_CASL.c_error.Equals(a_GEO.get("_parent", "")))
      {
        R = new GenericObject("_parent", "GEO.error", "code", a_GEO.get("code", "UNKNOWN", true), "message", a_GEO.get("message", "UNKNOWN", true), "throw", false);
        return R;
      }

      ArrayList all_keys = a_GEO.getKeys();
      foreach (object key in all_keys)
      {
        if (key.Equals("_parent") || key.Equals("_container")) continue; // SKIP  recursive key including _parent, _container
        object value = a_GEO.get(key);
        if (value is GenericObject &&
          (((GenericObject)value).has("_parent") ||
          ((GenericObject)value).has("_container"))) // HANDLE GENERICOBJECT VALUE WITH RECURSIVE KEY _PARENT OR _CONTAINER
          R.set(key, convert_object_to_serialize_by_removing_recursive_ref((GenericObject)value));
        else if (Tool.is_casl_primitive(value)) // HANDLE PRIMITIVE VALUE
          R.set(key, value);
        else if (value is GenericObject) // HANDLE GENERICOBJECT VALUE WITHOUT RECURSIVE KEYS
          R.set(key, value);
        //else SKIP CS OBJECT VALUE
      }
      if (a_GEO.has("_parent")) // ASSUMING PARENT ALWAYS IS GENERICOBJECT AND HAS PATH
      {
        GenericObject parent_object = a_GEO.get("_parent") as GenericObject;
        string parent_string = (string)misc.ToPath(parent_object, c_CASL.GEO);
        R.set("_parent", parent_string);
      }
      return R;
    }
    /*
      * GetType returns the type given the name_space and class_name.
      * First attempts generic simple find.
      * Then iterates through loaded assemblies and attempts to find there.
      * Then attempts to load the appropriate assembly and find in there.
      */
    // Called by: get_cs_method_from_call_name (which is in a critical part of code), but might be not called after Cody's stuff.
    public static Type get_type(string name_space, string class_name)
    {
      // proper type name
      String qualified_name = String.Format("{0}.{1}, {0}", name_space, class_name);

      // naive GetType call
      Type class_type = Type.GetType(qualified_name);
      if (class_type != null)  // if this class in memory, return it
      {
        return class_type;
      }

      string type_name = String.Format("{0}.{1}", name_space, class_name);

      string loaded_assembly_name;
      var loaded_assemblies = BuildManager.GetReferencedAssemblies().Cast<Assembly>();// BUG#13402 replace unreliable GetAssemblies method Thread.GetDomain().GetAssemblies();
      foreach (Assembly loaded_assembly in loaded_assemblies)
      {
        loaded_assembly_name = loaded_assembly.GetName().Name;

        if (loaded_assembly_name == name_space) // found a plausible name_space
        {
          class_type = loaded_assembly.GetType(type_name);
          if (class_type != null)  // found the type
          {
            return class_type;
          }
        }
      }

      // Try loading the right assembly and looking in there!
      //bug 12081 NJ-disable compiler warning
#pragma warning disable 618
      Assembly new_assembly = Assembly.LoadWithPartialName(name_space);
#pragma warning restore 618
      if (new_assembly != null)
      {
        class_type = new_assembly.GetType(type_name);
      }

      return class_type;
    }

    /// <summary>
    /// Added for bug #5470 - Sort users lists. But can be used to sort any GEO string keys list D.H 05-12-2008
    /// Takes a GEO with unsorted_list parameter.
    /// </summary>
    /// <param name="args"></param>
    /// <returns>Sorted GEO</returns>
    static public GenericObject SortList(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      GenericObject geoListToSort = (GenericObject)geo_args.get("unsorted_list");

      ArrayList pArrList = geoListToSort.getStringKeys();
      pArrList.Sort();

      GenericObject pSortedGeo = new GenericObject();
      for (int i = 0; i < pArrList.Count; i++)
        pSortedGeo.insert(pArrList[i]);

      return pSortedGeo;
    }

    // BUG# 7484 D.H.
    public static string ConvertByteArrayToBase64String(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      object arr = geo_args.get("data");
      string strReturnVal = "";

      if (arr is byte[])
        strReturnVal = Convert.ToBase64String((byte[])arr);

      return strReturnVal;
    }

    public static string ConvertFileToBase64(params object[] args)
    {
      // Bug# 18309 DH ? Add the ability to configure the receipt header image.
      GenericObject geo_args = misc.convert_args(args);

      try
      {
        string strPathAndFileName = geo_args.get("path_and_file") as string;
        if (strPathAndFileName == "")
        {
          return "";
        }
        else
        {
          object[] arr = new object[2];

          arr[0] = "data";
          arr[1] = File.ReadAllBytes(strPathAndFileName);
          return ConvertByteArrayToBase64String(arr);
        }
      }
      catch (Exception e)
      {
        // HX: Bug 17849
        Logger.cs_log("Error in ConvertFileToBase64: " +
            Environment.NewLine + e.ToMessageAndCompleteStacktrace(),
            Logger.cs_flag.debug);
        return e.Message;
      }

    }

    public static string ConvertToBase16ToASCII(params Object[] args)
    {
      // BUG# 8223 DH - Function added for HTML Editor

      GenericObject geo_args = misc.convert_args(args);
      string str = (string)geo_args.get("str");
      string Data1 = "";
      string sData = "";

      // Don't process if "0x" indicatior is not there.
      if (str.Length > 2)
      {
        if (str.Substring(0, 2) == "0x")
        {
          str = str.Substring(2);
          while (str.Length > 0)
          {
            Data1 = System.Convert.ToChar(System.Convert.ToUInt32(str.Substring(0, 2), 16)).ToString();
            sData = sData + Data1;
            str = str.Substring(2, str.Length - 2);
          }
        }
        else
          return str;// Just return original so we don't disturb anything.
      }
      else
        return str;// Just return original so we don't disturb anything.

      return sData;
    }

    public static string GetExecutableFormula(params Object[] arg_pairs)
    {
      // Bug 12098 - iPayment Baseline: Addition of Formulas [DJD]

      GenericObject geoARGS = misc.convert_args(arg_pairs);
      GenericObject geoTranInfo = (GenericObject)geoARGS.get("tran_data");
      string strFormula = (string)ConvertToBase16ToASCII(arg_pairs);

      return GetExecutableFormula_aux(strFormula, geoTranInfo);
    }

    public static string GetExecutableFormula_aux(string strConfigFormula, GenericObject geoTranInfo)
    {
      try
      {
        // Bug 12098 - iPayment Baseline: Addition of Formulas [DJD]
        string strErrorMsg = "";
        string strInvalid = "";
        string strExeFormula = strConfigFormula;

        // Validate methods in Formula
        strInvalid = ValidateFormulaMethods_aux(ref strConfigFormula);
        if (strInvalid.Length > 0)
        {
          strErrorMsg = string.Format("An invalid or unrecognized method was used in a formula: <{0}>.", strInvalid);
          // Bug 24703 DJD: Log error and return empty string [throwing CASL error seems to cause problems]
          Logger.LogError(strErrorMsg, "Error Executing Formula");
          return "";
        }

        // Get the list of Custom Field IDs used in formula
        List<string> FieldIDList = new List<string>();
        GetSubstrings(strConfigFormula, '[', ']', ref FieldIDList);

        // Replace each Custom Field ID in formula with the transaction value.
        foreach (string strID in FieldIDList)
        {
          // Ignore the configuration test value if there is one 
          // (e.g., [CUSTOM_FIELD_ID,"YES"] or [FEE,5.25])
          string strCustomFieldID = strID;
          int idx = strCustomFieldID.IndexOf(",");
          if (idx != -1) { strCustomFieldID = strCustomFieldID.Substring(0, idx); }
          strCustomFieldID.Trim();

          if (geoTranInfo.has(strCustomFieldID))
          {
            Object val = geoTranInfo.get(strCustomFieldID);

            if ((val != null) && (val.ToString() != ""))
            {
              Type val_type = val.GetType();
              bool IsString = (val_type == typeof(System.String));
              string oldVal = '[' + strID + ']';
              string newVal = val.ToString();
              if (IsString) { newVal = '"' + newVal + '"'; }
              strExeFormula = strExeFormula.Replace(oldVal, newVal);
            }
            else // No value set yet
            {
              // Cannot evaluate expression
              return "";
            }
          }
          else // Bug 24703 DJD: Log error and return empty string [throwing CASL error seems to cause problems]
          {
            string strTranDesc = "";
            if (geoTranInfo.has("description"))
              strTranDesc = geoTranInfo.get("description").ToString();
            strErrorMsg = string.Format("Custom Field ID ({0}) used in formula is not configured with this transaction ({1}).", strID, strTranDesc);
            Logger.LogError(strErrorMsg, "Error Executing Formula");
            return "";
          }
        }
        return strExeFormula;
      }
      catch (Exception ex)
      {
        // Bug 24703 DJD: [Added Try-Catch] Allocation Formulas: Referencing a Tag That Doesn't Exist Causes Application Slowness for ALL Users
        Logger.LogError(ex.ToMessageAndCompleteStacktrace(), "Error Executing Formula");
        return "";
      }
    }

    public static string ExecuteFormulaTestValues(string strConfigFormula)
    {
      // Bug 12098 - iPayment Baseline: Addition of Formulas [DJD]
      // NOTE: Formula sent should ALREADY be validated.

      string strResult = "";
      string strExeFormula = strConfigFormula;

      // Get the list of Custom Field IDs & Test values configured in formula
      List<string> FieldIDList = new List<string>();
      GetSubstrings(strConfigFormula, '[', ']', ref FieldIDList);

      string strTestValue = "";
      string strMissingTestVal = "";
      int CustFieldCount = 0, TestValCount = 0;

      // Replace each Custom Field ID in formula with the test value.
      foreach (string strID in FieldIDList)
      {
        // Get the configuration test value if there is one 
        // (e.g., [CUSTOM_FIELD_ID,"YES"] or [FEE,5.25])
        CustFieldCount++;
        strTestValue = "";
        int idx = strID.IndexOf(",");
        if (idx != -1) // Test value configured
        {
          TestValCount++;
          strTestValue = strID.Substring(idx);
          strTestValue = strTestValue.TrimStart(',');
          strTestValue = strTestValue.Trim();
          string oldVal = '[' + strID + ']';
          if (strTestValue.Length != 0) // Valid Test value
          { strExeFormula = strExeFormula.Replace(oldVal, strTestValue); }
          else
          { strMissingTestVal = oldVal; }
        }
      }

      if (strMissingTestVal.Length > 0)
      {
        strResult = string.Format("TEST VALUES ERROR: The following test value is missing in the formula: {0}", strMissingTestVal);
      }
      else
      {
        if (CustFieldCount == TestValCount) // Try to execute formula
        {
          object R;
          try
          {
            R = ExecuteCASLExpression(ref strExeFormula);
            strResult = "TEST VALUES RESULT: " + R.ToString();
          }
          catch (Exception e)
          {
            strResult = "TEST VALUES ERROR: " + e.Message;
            // HX: Bug 17849
            Logger.cs_log_trace("TEST VALUES ERROR", e);
          }
        }
        else
        {
          if (TestValCount > 0)
          { strResult = string.Format("TEST VALUES ERROR: All custom fields must have a test value configured:\n\nCustom Field Count:{0}\nTest Value Count:{1}", CustFieldCount, TestValCount); }
        }
      }

      return strResult;
    }

    private static object ExecuteCASLExpression(ref string exp)
    {
      // Bug 12098 - iPayment Baseline: Addition of Formulas [DJD]
      String s_exprs = "<do>" + exp + "</do>";
      GenericObject exprs = CASLParser.decode_conciseXML(s_exprs, "");

      // object obj_return = CASLInterpreter.execute_exprs(frame, exprs, c_CASL.c_local_env(), false);
      object obj_return = CASLInterpreter.execute_exprs(exprs, c_CASL.c_local_env(), false);
      return obj_return;
    }

    // Bug 12098 - iPayment Baseline: Addition of Formulas [DJD]
    // This static, read-only list contains approved CASL methods for use in formulas.
    private static readonly List<string> FormulaValidMethodList = new List<string>
    {
        // String Methods
        "concat",
        "join",
        "compare",
        "contains",
        "copy",
        "diff",
        "ends_with",
        "from",
        "has",
        "highlight",
        "intersection",
        "length",
        "less",
        "less_or_equal",
        "more",
        "more_or_equal",
        "pad",
        "replace",
        "replace_braces",
        "split",
        "split_into_parts",
        "starts_with",
        "strip",
        "subvector",
        "to_label",
        "to_lowercase",
        "to_path_part",
        "to_title_case",
        "to_uppercase",
        "trim",
        "trim_end",
        "trim_start",
        "to_string",
        
        // Number Methods
        "from",
        "is_type_for",
        "round",
        "absolute_value",
        "copy",
        "divide",
        "for_each",
        "from",
        "has",
        "is_around_zero",
        "is_odd",
        "length",
        "less",
        "less_or_equal",
        "minus",
        "more",
        "more_or_equal",
        "pad",
        "plus",
        "random",
        "remainder",
        "reverse_amount",
        "times",
        "to_money",
        
        // Date Methods
        "current",
        "day_difference",
        "DD-MMM-YYYY",
        "from",
        "get_day",
        "get_days_in_month",
        "get_formatted",
        "get_month",
        "get_year",
        "init",
        "less",
        "m_d_y",
        "MMDDYYHHMM",
        "more",
        "yyyymmdd",
        "add_days",     // Bug 26346 UMN
        "add_months",   // Bug 26346 UMN
        "add_years",    // Bug 26346 UMN

        // Condition, Logic & Common Methods
        "if",
        "do",
        "is",
        "is_not",
        "equal",
        "equal_number",
        "not",
        "and",
        "or",
    };

    public static string ValidateFormulaMethods_aux(ref string strFormula)
    {
      // Bug 12098 - iPayment Baseline: Addition of Formulas [DJD]

      // This method checks the formula for approved CASL methods - returns empty string
      // if ALL methods used in formula are recognized; string with first invalid method
      // found otherwise.

      //String s_expr = "<do> casl_ide.<htm_large/> it.<to_htm/> </do>"; //how to get path from call
      //GenericObject expr = CASLParser.decode_conciseXML(s_expr, "");
      //Object R = CASLInterpreter.execute_exprs(expr, c_CASL.c_local_env(), false);

      string strInvalidMethod = "";

      // Get the list of methods used in formula
      List<string> MethodList = new List<string>();
      if (GetSubstrings(strFormula, '<', '>', ref MethodList))
      {
        for (int i = 0; i < MethodList.Count; i++)
        {
          if (!FormulaValidMethodList.Contains(MethodList[i]))
          {
            strInvalidMethod = MethodList[i];
            break;
          }
        }
      }

      return strInvalidMethod;
    }

    public static string ValidateFormulaXMLParse(ref string strFormula)
    {
      // Bug 12098 - iPayment Baseline: Addition of Formulas [DJD]

      // This method checks that the formula can be parsed as XML (i.e., matching tags).
      // Returns empty string if parses OK; string with the error message otherwise.

      // Add <do></do> tags to formula because XML is not valid unless it has a tag at the
      // "Root Level"
      string strFormulaToParse = "<do>" + strFormula + "</do>";
      string strRetErr = "";

      try
      {
        XDocument doc = XDocument.Parse(strFormulaToParse);
      }
      catch (System.Xml.XmlException e)
      {
        strRetErr = e.Message;

        // HX: Bug 17849
        Logger.cs_log_trace("Error in ValidateFormulaXMLParse", e);
      }

      return strRetErr;
    }

    private static bool GetSubstrings(string str, char BeginCh, char EndCh, ref List<string> SubstrList)
    {
      // Bug 12098 - iPayment Baseline: Addition of Formulas [DJD]
      char ch;
      int i = 0, l = 0;
      string strSubStr = "";
      bool getsubstr = false, error = false;

      l = str.Length;
      while (i < l)
      {
        ch = str[i];
        if (ch == BeginCh)
        {
          getsubstr = true;
        }
        else if (ch == EndCh)
        {
          getsubstr = false;
          strSubStr = strSubStr.Replace("/", "");
          strSubStr = strSubStr.Trim();
          if (SubstrList.IndexOf(strSubStr) == -1)
          {
            SubstrList.Add(strSubStr);
          }
          strSubStr = "";
        }
        else if (getsubstr)
        {
          strSubStr += ch;
        }
        i++;
      }
      return error ? false : true;
    }


    public static string CASLTicksToDateTime(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      string tick_string = geo_args.get("ticks", "0") as string;
      DateTime date_time = new DateTime(long.Parse(tick_string));
      return date_time.ToString();
    }

    // Bug 8030 MJO - Added logging override
    public static T Parse<T>(object input)
    {
      return Parse<T>(input, true);
    }

    // Bug #12055 Mike O - Use reflection to parse a string into any supported object T
    // Note: This does NOT work when converting a char to hex, as it's converted to a string first
    public static T Parse<T>(object input, bool logError)
    {
      if (input == null)
        return default(T);

      MethodInfo methodInfo = typeof(T).GetMethod(
        "TryParse",
        new[] { typeof(string), typeof(T).MakeByRefType() }
      );

      var inputParameters = new object[] { input.GetType() == typeof(string) ? input : input.ToString(), default(T) };
      if (!((bool)methodInfo.Invoke(null, inputParameters)))
      {
        var stackInfo = new StringBuilder();
        var stackTrace = new StackTrace(true);
        String indent = "";
        int counter = 0;
        foreach (var r in stackTrace.GetFrames())
        {
          // Skip the setErr() method
          if (counter++ == 0)
            continue;
          // Enough is enough
          if (counter > 10)
            break;
          stackInfo.
              Append(indent).
              Append(String.Format("Filename: {0} Method: {1} Line: {2} Column: {3}",
              r.GetFileName(), r.GetMethod(), r.GetFileLineNumber(),
              r.GetFileColumnNumber())).
              Append("\n\n");
          indent = indent + "  ";
        }
        string err = String.Format("Failed to parse {0} out of the following string: {1}. STACKTRACE: {2}", typeof(T).FullName, input.ToString(), stackInfo.ToString());

        // Bug 8030 MJO - Added logging override
        if (logError)
          Logger.LogError(err, "Parse<T>");

        throw new FormatException("Failed while attempting to parse a string into a new type.");
      }

      return (T)inputParameters[1];
    }


    public static string CASL_cs_log(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      string message = (string)geo_args.get("message");
      Logger.cs_log("CASL_cs_log" + message);
      return message;
    }

    public static GenericObject MakeCASLError(string title, string code, string message)
    {
      return CASL_error.create("title", title, "code", code, "message", message).casl_object;
    }

    // Bug 22013 DJD - Brand-specific card tenders allow processing of other brands' cards [Add method with "user_message"]
    public static GenericObject MakeCASLError(string title, string code, string message, bool user_message)
    {
      return CASL_error.create("title", title, "code", code, "message", message, "user_message", user_message).casl_object;
    }

    // Bug #9335 Mike O - Execute all .casle files in the upgrade directory
    internal static void ExecuteUpgradeFiles()
    {
      DirectoryInfo upgradeDir = null;

      try
      {
        upgradeDir = new DirectoryInfo(misc.CASL_get_code_base() + "/upgrade");
      }
      catch (Exception /*e*/) //12081 NJ-unused variable. If there is no upgrade directory, don't bother
      {
        return;
      }

      // Get all of the unprocessed CASLE files
      FileInfo[] files = upgradeDir.GetFiles("*.casle");

      foreach (FileInfo file in files)
      {
        try
        {
          // Execute the file
          GenericObject local_env = c_CASL.c_local_env();
          GenericObject expr = execute_file_aux_read_encrypted(file.FullName);

          object R = CASLInterpreter.execute_expr(
              CASL_Stack.Alloc_Frame(EE_Steps.standard, expr, local_env));

          // Throw the error if one is returned, so it can be handled properly
          if (R is CASL_error)
            throw ((CASL_error)R);

          // Rename the file so it won't be executed twice
          file.MoveTo(file.FullName + "." + DateTime.Now.ToString("MM-dd-yy") + ".SUCCESS");
        }
        catch (Exception e) // Mark file as failed, then let the outer error handling deal with the error
        {
          String failName = file.FullName + "." + DateTime.Now.ToString("MM-dd-yy") + ".FAILURE";
          file.MoveTo(failName);

          if (e is CASL_error)
          {
            ((CASL_error)e).casl_object.set("message", "Failed to execute " + file.FullName + ": " + ((CASL_error)e).casl_object.get("message"));
            throw;
          }
          else
            throw new Exception("Failed to execute " + file.FullName + ": " + e.Message);
        }
      }
    }

#region Escape and unescape a string
    public static object CASL_unescape(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      object subject = geo_args.get("_subject");
      if (subject is string)
      {
        string subject_string = (string)subject;
        return HttpUtility.UrlDecode(subject_string);
      }
      else
      {
        return false;
      }
    }
    public static object CASL_escape(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      object subject = geo_args.get("_subject");
      if (subject is string)
      {
        string subject_string = (string)subject;
        return HttpUtility.UrlEncode(subject_string);
      }
      else
      {
        return false;
      }
    }
#endregion

#region Common Rounding
    // Bug 18636 UMN - Remove faulty CommonRound functions
#endregion

#region Return Hash Code
    public static int CASLGetHashCode(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      GenericObject subject = geo_args.get("_subject") as GenericObject;
      return subject.GetHashCode();
    }
#endregion
#region Force Garbage Collector
    public static object CASLForceGarbageCollector(params object[] args)
    {
      GC.Collect();
      GC.WaitForPendingFinalizers();
      return true;
    }
#endregion
#region Test Method
    public static string CASLTestMethod(params object[] args)
    {
      return "wxyz";
    }
#endregion
#region Sorting
    /// <summary>
    /// Sorts the given subject based on the expression list.  Assumes we can
    /// use a stable sort algorithm.
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static object CASLSortOn(params object[] args)
    {
      GenericObject geoArgs = convert_args(args);
      GenericObject subject = geoArgs.get("_subject") as GenericObject;
      GenericObject expressions = geoArgs.get("expressions") as GenericObject;

      return SortOn(subject, expressions);
    }
    public static GenericObject SortOn(
                    GenericObject subject,
                    GenericObject expressions)
    {
      // The unsorted list of items
      List<SortItem> items = new List<SortItem>();
      SortItem item = null;
      GenericObject geo = null;
      object expressionValue = null;
      GenericObject environment = c_CASL.c_local_env();
      foreach (object o in subject.vectors)
      {
        geo = o as GenericObject;
        item = new SortItem(geo);
        foreach (object e in expressions.vectors)
        {
          environment["_subject"] = geo;

          expressionValue = CASLInterpreter.execute_expr(
CASL_Stack.Alloc_Frame(EE_Steps.standard, e, environment));

          if ((expressionValue is string) || (expressionValue is int) || (expressionValue is decimal))
          {
            item.sortValues.Add(expressionValue);
          }
          else
          {
            item.sortValues.Add(null);
          }
        }
        items.Add(item);
      }
      // All items are added
      items.Sort();
      GenericObject sortedList = c_CASL.c_make("vector") as GenericObject;
      int i = 0;
      foreach (SortItem o in items)
      {
        subject.vectors[i] = o.obj;
        i++;
      }
      return subject;
    }
#endregion
#region Randomize a GEO
    public static object CASLShuffle(params object[] args)
    {
      GenericObject geoArgs = convert_args(args);
      GenericObject subject = geoArgs.get("_subject") as GenericObject;
      return Shuffle(subject);
    }
    public static object Shuffle(GenericObject subject)
    {
      ArrayList list = subject.vectors;
      Random random = new Random();
      object t;
      for (int i = list.Count - 1; i > 0; i--)
      {
        int j = random.Next(i + 1);
        t = list[i];
        list[i] = list[j];
        list[j] = t;
      }
      return subject;
    }
#endregion
#region Test class methods
    public static int CASLTimeIt(params object[] args)
    {
      GenericObject geoArgs = convert_args(args);

      int count = (int)geoArgs.get("count");
      GenericObject expression = geoArgs.get("expression") as GenericObject;
      GenericObject environment = geoArgs.get("environment") as GenericObject;

      System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
      watch.Start();
      for (int i = 0; i < count; i++)
      {
        CASLInterpreter.execute_expr(
            CASL_Stack.Alloc_Frame(EE_Steps.standard, expression, environment));
      }
      watch.Stop();
      if (watch.ElapsedMilliseconds <= int.MaxValue)
      {
        return (int)watch.ElapsedMilliseconds;
      }
      else
      {
        return int.MaxValue;
      }
    }
#endregion

    //
    // Initialize a C# stopwatch and return it to the CASL caller.
    // Takes one unkeyed argument which is the message for the log file,
    // if omitted (or not a string) then nothing is logged.  IPAY-85 GMB.
    //
    public static Object fCASL_stopwatch_start(CASL_Frame frame)
    {
      Stopwatch t = new Stopwatch();
      Object o = frame.args.get(0, null);
      if (o is String s) {
        string time_report = " is " + t.Elapsed.ToString() +
          "  (" + t.ElapsedTicks.ToString() + " ticks)";
#if DEBUG
        Logger.cs_log("Stopwatch starting for " + s + time_report);
#endif
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          Flt_Rec.log("fCASL_stopwatch_start ", s + time_report);
      }
      t.Start();
      return t;
    }

    //
    // At a specific point note the lapsed time.  The first argument, 0,
    // is the Stopwatch object; if that is "opt" then just return.
    // Otherwise 1 should be a string---it is included in the log message.
    // IPAY-85 GMB.
    //
    public static object fCASL_stopwatch_log_checkpoint(CASL_Frame frame)
    {
      object o = frame.args.get(0, c_CASL.c_opt);
      if (o == c_CASL.c_opt)
        return c_CASL.GEO;
      if (o is Stopwatch t) {
        string time_report = " is " + t.Elapsed.ToString() +
          "  (" + t.ElapsedTicks.ToString() + " ticks)";
        o = frame.args.get(1, "");
        if (o is String s) {
#if DEBUG
          Logger.cs_log("Stopwatch elapsed time for " + s + time_report);
#endif
          if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
            Flt_Rec.log("fCASL_stopwatch_log_checkpoint ", s + time_report);
        } else {
#if DEBUG
          Logger.cs_log("Stopwatch elapsed ticks (bad arg 1) is " +
                        time_report);
#endif
          if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
            Flt_Rec.log("fCASL_stopwatch_log_checkpoint ", "(bad arg 1)" + time_report);
        }
      } else {
#if DEBUG
        Logger.cs_log("stopwatch_log_checkpoint first argument not a stopwatch");
#endif
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          Flt_Rec.log("fCASL_stopwatch_log_checkpoint ",
                      "first argument is not a stopwatch");
      }
      return c_CASL.GEO;
    }

    //
    // Stop the stopwatch and log the total time.  Same call
    // pattern as for log_checkpoint.  IPAY-85 GMB.
    //
    public static object fCASL_stopwatch_end(CASL_Frame frame)
    {
      object o = frame.args.get(0, c_CASL.c_opt);
      if (o == c_CASL.c_opt)
        return c_CASL.GEO;
      if (o is Stopwatch t) {
        t.Stop();
        string time_report = " is " + t.Elapsed.ToString() +
          "  (" + t.ElapsedTicks.ToString() + " ticks)";
        o = frame.args.get(0, "");
        if (o is String s) {
#if DEBUG
          Logger.cs_log("Stopwatch total elapsed time for " + s + time_report);
#endif
          if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
            Flt_Rec.log("fCASL_stopwatch_end ", s + time_report);
        } else {
#if DEBUG
          Logger.cs_log("Stopwatch total elapsed ticks (bad arg 0) is " +
                        time_report);
#endif
          if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
            Flt_Rec.log("fCASL_stopwatch_log_checkpoint ", "(bad arg 0)" + time_report);
        }
      } else {
#if DEBUG
        Logger.cs_log("end_stopwatch first argument is not a stopwatch");
#endif
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          Flt_Rec.log("fCASL_stopwatch_log_checkpoint ",
                      "first argument is not a stopwatch");
      }
      return c_CASL.GEO;
    }

#region Make Part and Set Part
    public static GenericObject CASLMakePart(params object[] args)
    {
      GenericObject geoArgs = convert_args(args);
      GenericObject subject = geoArgs.get("_subject") as GenericObject;
      GenericObject target = geoArgs.get("in") as GenericObject;
      object name = geoArgs.get("name");

      return MakePart(subject, target, name);
    }
    public static GenericObject MakePart(GenericObject subject, GenericObject target, object name)
    {
      // UMN added this in hopes Shuang can diagnose why this is happening.
      // The error is: Error: MakePart target is null for subject error and name opt.
      if (target == null)
      {
        string subjType = (string)((GenericObject)subject.get("_parent")).get("_name");
        string err = string.Format("MakePart target is null for subject {0} and name {1}.",
          subjType, ((GenericObject)name).get("_name", "noname").ToString());
        Logger.cs_log("misc.cs: MakePart" + err, Logger.cs_flag.error);
        Logger.LogError(err, "Make Part");
        return subject;
      }
      lock (target)
      {
        object partName = subject.get("_part_name_key", false, true);
        object newName = null;
        if (name != c_CASL.c_opt)
        {
          newName = name;
        }
        else if (subject.has(partName))
        {
          newName = subject.get(partName);
        }
        else
        {
          newName = target.getLength();
        }
        subject.set("_container", target);
        subject.set(partName, newName);
        target.set(newName, subject);
      }
      return subject;
    }
    public static GenericObject CASLSetPart(params object[] args)
    {
      GenericObject geoArgs = convert_args(args);
      GenericObject subject = geoArgs.get("_subject") as GenericObject;
      return SetPart(subject, geoArgs);
    }
    public static GenericObject SetPart(GenericObject subject, GenericObject args)
    {
      foreach (object o in args.vectors)
      {
        MakePart((GenericObject)o, subject, c_CASL.c_opt);
      }
      foreach (string s in args.getStringKeys())
      {
        GenericObject geo = args.get(s) as GenericObject;
        MakePart(geo, subject, s);
      }
      // Bug #14396 Mike O - Don't forget about non-vectored integer keys
      foreach (int i in args.getIntegerKeys())
      {
        GenericObject geo = args.get(i) as GenericObject;
        if (i >= args.vectors.Count)
          MakePart(geo, subject, i);
      }
      return subject;
    }
#endregion
#region Copy Down Data
    public static object CASLCopyDownData(params object[] args)
    {
      GenericObject geoArgs = convert_args(args);
      GenericObject subject = geoArgs.get("_subject") as GenericObject;
      GenericObject source = geoArgs.get("source") as GenericObject;

      return CopyDownData(subject, source);
    }
    public static object CopyDownData(GenericObject subject, GenericObject source)
    {
      GenericObject parent = null;
      if (source == c_CASL.c_opt)
      {
        parent = subject.get("_parent") as GenericObject;
      }
      else
      {
        parent = source;
      }

      GenericObject fieldOrder = parent.get("_field_order", false, true) as GenericObject;
      foreach (object key in fieldOrder.vectors)
      {
        if (!(parent.has(key))) { continue; }
        object value = parent.get(key);
        if (!(subject.has(key)) && value != c_CASL.c_opt && value != c_CASL.c_req)
        {
          object path = ToPath(subject, c_CASL.GEO);
          if ((value is GenericObject) && (path.Equals(false)))
          {
            subject.set(key, Copy(value as GenericObject, null, null, null));
          }
          else
          {
            subject.set(key, value);
          }
        }
      }

      CopyDownMultiples(subject);
      return subject;
    }
    public static object CASLCopyDownMultiples(params object[] args)
    {
      GenericObject geoArgs = convert_args(args);
      GenericObject subject = geoArgs.get("_subject") as GenericObject;
      return CopyDownMultiples(subject);
    }
    public static object CopyDownMultiples(GenericObject subject)
    {
      GenericObject parent = subject.get("_parent") as GenericObject;
      GenericObject fieldOrder = null;
      if (parent.has("_db_field_order"))
      {
        fieldOrder = parent.get("_db_field_order") as GenericObject;
      }
      else if (parent.has("_field_order"))
      {
        fieldOrder = parent.get("_field_order") as GenericObject;
      }

      object lastObject = null;

      if (fieldOrder != null)
      {
        foreach (object key in fieldOrder.vectors)
        {
          object value = subject.get(key, false);
          object type = GetType(parent, key, false, false);
          if ((type == c_CASL.c_object("multiple_of")) || (type == c_CASL.c_object("multiple")) || base_is_a(type, c_CASL.c_object("multiple_of")))
          {
            if (value.Equals(false) || ((value is GenericObject) && ((GenericObject)value).get("_parent") == c_CASL.GEO))
            {
              GenericObject newGeo = null;
              if (base_is_a(type, c_CASL.c_object("multiple_of.ordered")))
              {
                newGeo = c_CASL.c_make("multiple.ordered") as GenericObject;
              }
              else
              {
                newGeo = c_CASL.c_make("multiple") as GenericObject;
              }
              lastObject = MakePart(newGeo, subject, key);
            }
            else if (base_is_a(value, c_CASL.c_object("multiple")))
            {
              // value is a multiple
              GenericObject geoValue = (GenericObject)value;
              if (!geoValue.has("_container")) { MakePart(geoValue, subject, key); }
              GenericObject multipleObjects = geoValue.get("_objects") as GenericObject;
              GenericObject callArgs = c_CASL.c_GEO();
              foreach (object o in multipleObjects.vectors)
              {
                callArgs.insert(o);
              }
              lastObject = base_casl_call(
                                                             c_CASL.c_object("multiple.insert") as GenericObject,
                                                             geoValue,
                                                             callArgs);
              // TODO: Make CASLMultipleInsert/MultipleInsert
            }
            else
            {
              // Bug 17061 UMN PCI-DSS
              string keyString = to_eng(key);
              lastObject = MakeCASLError("Type Mismatch Error",
                "CS-100", "Type mismatch. Expected a multiple in field " + keyString + " in object " + to_eng_masked(keyString, value));
            }
          }
        }
      }
      return lastObject;
    }
#endregion
#region GetType
    public static object GetType(GenericObject subject, object key, object if_missing, bool config)
    {
      object tKey;
      if (key is int) { tKey = "_other_unkeyed"; }
      else { tKey = key; }

      if (!config)
      {
        bool hasCFType;
        string fKey = field_key(tKey, "custom_field");
        hasCFType = subject.has(fKey, true);
        if (hasCFType)
        {
          GenericObject CFType = subject.get(fKey, false, true) as GenericObject;
          object CType = GetCustomType(CFType);
          if (!CType.Equals(false)) { return CType; }
        }
        return subject.get(field_key(tKey, "type"), if_missing, true);
      }
      else
      {
        object configType = subject.get(field_key(tKey, "type_config"), false, true);
        if (!(configType.Equals(false)))
        {
          return configType;
        }
        else
        {
          return GetType(subject, tKey, if_missing, false);
        }
      }
    }
    public static object SetType(GenericObject subject, object key, object type)
    {
      if (subject == null || key == null || type == null)
      {
        return MakeCASLError("SetType Error", "200",
          String.Format("Error setting type ({0}) for key ({1}) on subject ({2})", type, key, subject));
      }
      subject.set(field_key(key, "type"), type);
      return subject;
    }
    public static object CASLGetCustomType(params object[] args)
    {
      GenericObject geoArgs = convert_args(args);
      GenericObject subject = geoArgs.get("_subject") as GenericObject;
      return GetCustomType(subject);
    }
    public static object GetCustomType(GenericObject subject)
    {
      object valueList = subject.get("value_list", false, true);
      object type = subject.get("type", c_CASL.c_error, true);
      if (base_is_a(valueList, c_CASL.c_object("Field_value_list")))
      {
        return valueList;
      }
      else if ((c_CASL.c_object("Type.text.chars_f_type") as GenericObject).vectors.Contains(type))
      {
        GenericObject newType = c_CASL.c_make("Type.text") as GenericObject;
        int minSize = (int)subject.get("min_length", 0);
        int maxSize = (int)subject.get("max_length", 0);
        newType.set("chars", type, "min_size", minSize, "max_size", maxSize);
        return newType;
      }
      else
      {
        return subject.get("type", c_CASL.c_error, true);
      }
    }
#endregion
#region GetLabel/GetLabel2
    public static string GetLabel(GenericObject subject, object key)
    {
      string label = subject.get(misc.field_key(key, "label"), null, true) as string;
      if (label == null)
      {
        label = ToLabel(key);
      }
      return label;
    }
    public static string ToLabel(object key)
    {
      if (key == null)
      {
        return "";
      }
      else if (key is string)
      {
        return ToTitleCase((key as string).Replace('_', ' '));
      }
      else if (key.GetType().IsValueType)
      {
        return key.ToString();
      }
      else
      {
        GenericObject l_env = c_CASL.c_GEO();
        l_env.set("_subject", key);
        return base_casl_call("htm_small",
                               CASLInterpreter.no_subject,
                               l_env).ToString();
      }
    }
    public static string ToTitleCase(string phrase)
    {
      string[] words = phrase.Split(' ');
      GenericObject stopWords = c_CASL.c_object("String.to_title_case.stop_words") as GenericObject;
      char[] characters = phrase.ToCharArray();
      int w = 0;
      characters[0] = char.ToUpper(characters[0]); // First letter is always uppercase
      for (int i = 0; i < characters.Length - 1; i++)
      {
        if (characters[i].Equals(' '))
        {
          w++; // Move to the next word
          if (!stopWords.get(words[w], false).Equals(true)) // Uppercase if not in stopWords
          {
            characters[i + 1] = char.ToUpper(characters[i + 1]);
          }
        }
      }

      return new string(characters);
    }
#endregion
#region GetCSSClass
    public static string CASLGetCSSClass(params object[] argPairs)
    {
      GenericObject args = convert_args(argPairs);
      GenericObject subject = args.get("a_obj") as GenericObject;
      object key = args.get("key");
      return GetCSSClass(subject, key);
    }
    public static string GetCSSClass(GenericObject subject, object key)
    {
      object[] args = { key, " ", (GetType(subject, key, c_CASL.c_String, false) as GenericObject).get("_name", false, true) };
      return Concat(args);
    }
#endregion
#region Convert vector values
    public static object CASLConvertValueInVectorUsingType(params object[] argPairs)
    {
      GenericObject args = convert_args(argPairs);
      GenericObject valueClass = args.get("value_class") as GenericObject;
      GenericObject vector = args.get("a_vector") as GenericObject;
      return ConvertValue(valueClass, vector);
    }
    public static object ConvertValue(GenericObject valueClass, GenericObject vector)
    {
      GenericObject shortClass = c_CASL.c_GEO();
      ArrayList stringKeys = valueClass.getStringKeys();
      object value, type;
      foreach (string key in stringKeys)
      {
        value = valueClass.get(key);
        type = GetType(valueClass, key, false, false);
        if (!type.Equals(false) && (type.Equals(c_CASL.c_Integer) || type.Equals(c_CASL.c_Decimal)))
        {
          shortClass.set(key, value);
          SetType(shortClass, key, type);
        }
      }

      stringKeys = shortClass.getStringKeys();
      GenericObject vectorValue;
      for (int i = 0; i < vector.getLength(); i++)
      {
        vectorValue = vector.get(i) as GenericObject;

        foreach (string key in stringKeys)
        {
          type = GetType(shortClass, key, false, false);
          GenericObject l_env = c_CASL.c_GEO();
          l_env.set(0, vectorValue.get(key));
          vectorValue.set(key,
            base_casl_call("from",
                type,
                l_env));
        }
      }
      return vector;
    }
#endregion
#region Expressions
    public static GenericObject MakeExpression(string caslString)
    {
      return CASLParser.decode_conciseXML(caslString, "");
    }
#endregion

    //////////////////////////////////////////////////////////////////
    //
    //    CASL Debugging and Performance
    //

    public static object fCASL_debug_test_throw(CASL_Frame frame)
    {
      throw CASL_error.create("message", "this is a test error");
    }

#if PERFORMANCE_GRAPH
    public struct CandidateMethod {
      public Perf_Method pm;
      public int cs_call_cnt;
      public int casl_call_cnt;
      public HashSet<String> casl_dependencies;
    }
    
    public static int CM_cmp(CandidateMethod cm1, CandidateMethod cm2)
    {
      long c1 = (long) (cm1.cs_call_cnt * 10000);
      long d = (long) (cm1.cs_call_cnt + cm1.casl_call_cnt);
      if (d == 0)
        c1 = 10000000;
      else
        c1 /= d;
      long c2 = (long) (cm2.cs_call_cnt * 10000);
      d = (long) (cm2.cs_call_cnt + cm2.casl_call_cnt);
      if (d == 0)
        c2 = 10000000;
      else
        c2 /= d;
      long r = c1 - c2;
      if (r == 0)
        r = (long) cm1.pm.m_total_time - (long) cm2.pm.m_total_time;
      return (r == 0) ? 0 : ((r < 0) ? -1 : 1);
    }
#endif

    //
    // Useful only when
    //    #define PERFORMANCE_GRAPH is set in CASLInterpreter.cs and here, or
    //    #define DICT_STATS is set in GenericObject.cs and here.
    // Produces the log/stats_file* which has
    //    Rudimentary call graph information with call counts, total cycles,
    //      minimum cycles, and maximum cycles.  No relative times yet because
    //      certain constructs like <if>, <do>, and <foreach> need special
    //      handling.
    //    Methods that are never called (and so have no regression testing)
    //    Methods called by C# methods which in a few cases implies they
    //      should be translated to C#.
    //    Methods which are "leaf" methods---no recorded calls to other CASL
    //      methods and are therefore good candidates for converting to C#
    //    Methods with high call counts and high total cycle counts which
    //      would offer the most performance improvement.
    //
    // Adding relative method times and more sophisticated handling of
    // if and, most importantly, loops, would be very helpful.
    //
    // The dictionary statistics (DICT_STATS) count the usages of each
    // key used in the GenericObject method dictionary.  At the moment
    // this just provides information about which keys offer the biggest
    // improvement for being included as members in GenericObject and
    // derived classes.
    //
    // A potential future use would be to note which keys are associated
    // with which CASL classes.
    //
    public static object fCASL_callgraph_dump_stats(CASL_Frame frame)
    {
      object message = frame.args.get("message", null);
      string stats_file_name = misc.CASL_get_code_base() + "/log/";
      string timestamp = System.DateTime.Now.ToString("yyyy_MM_dd_HH-mm-ss");
      stats_file_name += "stats_file." + timestamp + ".txt";

      StreamWriter stats_file = new System.IO.StreamWriter(stats_file_name,
          false, System.Text.Encoding.UTF8, 2000);

      if (message is string sm_x)
        stats_file.WriteLine(sm_x);
      stats_file.WriteLine(timestamp);
      stats_file.WriteLine("");


#if PERFORMANCE_GRAPH
      GenObj_Method[] MethodIndex = CASLInterpreter.MethodIndex;

      int i;
      int len = CASLInterpreter.call_graph.length();
      List<Perf_Call>[] caller_method = new List<Perf_Call>[len];
      for (i = 0; i < len; i++)
        caller_method[i] = new List<Perf_Call>();
      int total_called_casl_methods = 0;

      for (i = 0; i < CASLInterpreter.MethodCount; i++) {
        Perf_Method caller =
            CASLInterpreter.call_graph.get_per_method_data(i);

        if (caller.m_call_count > 0 &&
            MethodIndex[caller.method_id] is GenObj_CASL_Method)
          total_called_casl_methods++;
        
        if (caller != null) {
          foreach (KeyValuePair<long, Perf_Call> c in caller.calls) {
            caller_method[c.Value.callee_method_id].Add(c.Value);
          }
#if DEBUG_GOOD // OK at least for loading multiples
          GenObj_Method gom1 = CASLInterpreter.MethodIndex[i];
          if (caller.m_call_count != (ulong) gom1.call_count ||
              caller.m_return_count != (ulong) gom1.return_count ||
              caller.m_total_time != (ulong) gom1.total_ticks) {
            Debugger.Break();
          }
#endif
        }
      }

      stats_file.WriteLine("");
      stats_file.WriteLine("");
      stats_file.WriteLine("");

      for (i = 0; i < CASLInterpreter.MethodCount; i++) {
        GenObj_Method gom = MethodIndex[i];
        Perf_Method caller =
            CASLInterpreter.call_graph.get_per_method_data(i);

        // We gather a list of the methods that are never called later in
        // the Uncalled array; this reduces the clutter in the call graph
        // list.
        if (caller.m_call_count == 0)
          continue;

        stats_file.WriteLine("");
        string s = "@@ " + gom.full_name;
        if (gom is GenObj_CS_Method)
          s += "   C# method is " + (gom as GenObj_CS_Method).cs_name;
        else
          s += "   declared at: " + gom.loc.ToString();
        stats_file.WriteLine(s);

        s = "        calls=" + caller.m_call_count.ToString() + "/" +
          caller.m_return_count.ToString();

        if (caller.m_total_time > 0) {
          s +=  "     cycles: " +
          caller.m_min_time.ToString() + " min  " +
          caller.m_max_time.ToString() + " max  " +
            caller.m_total_time.ToString() + " tot  ";
          if (caller.m_return_count > 0) {
            s += (caller.m_total_time / caller.m_return_count).ToString()
              + " avg";
          }
        }
        stats_file.WriteLine(s);

        List<Perf_Call> callers = caller_method[i];
        callers.Sort( (c1, c2)=> (c1.caller_method_id - c2.caller_method_id));
        foreach (Perf_Call pc in callers) {
          s = "    <-  " + MethodIndex[pc.caller_method_id].full_name;
          s += " {" + MethodIndex[pc.casl_caller_method_id].full_name + "}";
          s += " " + pc.loc.ToString();
          s += "    calls=" + pc.call_count.ToString() + "/" +
            pc.return_count.ToString();
          if (pc.total_time > 0) {
            s +=  "     ticks: " +
              pc.min_time.ToString() + " min  " +
              pc.max_time.ToString() + " max  " +
              pc.total_time.ToString() + " tot  ";
            if (pc.return_count > 0) {
              s += ((double) pc.total_time / (double) pc.return_count).ToString()
                + "avg";
            }
          }
          stats_file.WriteLine(s);        }
          
        List<KeyValuePair<long, Perf_Call>> callees =
          caller.calls.ToList();
        callees.Sort( (p1, p2)=>
                      (p1.Value.callee_method_id - p2.Value.callee_method_id));
        
        ulong total_cycles = caller.m_total_time;
        foreach (KeyValuePair<long, Perf_Call> kpc in callees) {
          stats_file.WriteLine("          " + kpc.Key.ToString());
          s = "    ->  " + MethodIndex[kpc.Value.callee_method_id].full_name;
          s += " {" + MethodIndex[kpc.Value.casl_caller_method_id].full_name + "}";
          s += kpc.Value.loc.ToString();
          s += "    calls=" + kpc.Value.call_count.ToString() + "/" +
            kpc.Value.return_count.ToString();
          if (kpc.Value.total_time > 0) {
            s +=  "     ticks: " +
              kpc.Value.min_time.ToString() + " min  " +
              kpc.Value.max_time.ToString() + " max  " +
              kpc.Value.total_time.ToString() + " tot  ";
            if (kpc.Value.return_count > 0) {
              s += ((double) kpc.Value.total_time /
                    (double) kpc.Value.return_count).ToString()
                + " avg  ";
            }
            if (total_cycles > 0) {
              s += (((double) kpc.Value.total_time * 100) /
                    (double) total_cycles).ToString() + "%";
            }
          }
          stats_file.WriteLine(s);
          stats_file.WriteLine("");
        }
      }

      //
      // Notionally, collect Methods which are candidates for rewriting into C#.
      // However, we also look at calls from C# to CASL as these are also
      // potentially significant wins.
      //
      stats_file.WriteLine("");
      stats_file.WriteLine("");
      stats_file.WriteLine("C# Methods that call CASL Methods");
      CandidateMethod[] meth_arr =
        new CandidateMethod[total_called_casl_methods];
      int called_methods = 0;

      GenObj_Method[] Uncalled = new GenObj_Method[
                 CASLInterpreter.MethodCount - total_called_casl_methods + 1];
      int uncalled_cnt = 0;

      for (i = 0; i < CASLInterpreter.MethodCount; i++) {
        Perf_Method pm =
                    CASLInterpreter.call_graph.get_per_method_data(i);
        GenObj_Method gom = MethodIndex[pm.method_id];

        if (pm.m_call_count > 0) {
          if (gom is GenObj_CS_Method) {
            int cs_call_cnt = 0;
            int casl_call_cnt = 0;
            HashSet<String> casl_depend = new HashSet<String>();
            foreach (KeyValuePair<long, Perf_Call> kpc in pm.calls) {
              int called_method_ix = kpc.Value.callee_method_id;
              if (MethodIndex[called_method_ix] is GenObj_CS_Method)
                cs_call_cnt++;
              else {
                casl_call_cnt++;
                casl_depend.Add(MethodIndex[called_method_ix].full_name);
              }
            }
            if (casl_call_cnt > 0) {
              string s2 = "    ";
              stats_file.WriteLine(gom.full_name);
              foreach (string s in casl_depend) {
                s2 += s + " ";
                if (s2.Length > 80)
                  stats_file.WriteLine(s2);
                s2 = "    ";
              }
              if (s2.Length > 4)
                stats_file.WriteLine(s2);
            }
          } else if (gom is GenObj_CASL_Method) {
            meth_arr[called_methods].pm = pm;
            meth_arr[called_methods].cs_call_cnt = 0;
            meth_arr[called_methods].casl_call_cnt = 0;
            meth_arr[called_methods].casl_dependencies = new HashSet<String>();
          
            foreach (KeyValuePair<long, Perf_Call> kpc in pm.calls) {
              int called_method_ix = kpc.Value.callee_method_id;
              if (MethodIndex[called_method_ix] is GenObj_CS_Method)
                meth_arr[called_methods].cs_call_cnt++;
              else {
                meth_arr[called_methods].casl_call_cnt++;
                meth_arr[called_methods].casl_dependencies.Add(
                            MethodIndex[called_method_ix].full_name);
              }
            }
            called_methods++;
          } // else GO_kind.base_method
        } else {
          Uncalled[uncalled_cnt++] = gom;
        }
      }

      if (called_methods != total_called_casl_methods)
        Debugger.Break();

      stats_file.WriteLine("");
      stats_file.WriteLine("");
      stats_file.WriteLine("CASL Methods with highest call count");
      Array.Sort<CandidateMethod>(meth_arr,
                 (m1, m2) => m2.pm.m_call_count.CompareTo(m1.pm.m_call_count));
      int cnt = 100;
      if (cnt >= called_methods)
        cnt = called_methods;
      for (i = 0; i < cnt; i++) {
        Perf_Method pm = meth_arr[i].pm;
        stats_file.WriteLine(
            String.Format("{0,50}  {1,9}  {2,13}",
                          MethodIndex[pm.method_id].full_name,
                          pm.m_call_count.ToString(),
                          pm.m_total_time.ToString()));
      }
      
      stats_file.WriteLine("");
      stats_file.WriteLine("");
      stats_file.WriteLine("CASL Methods with highest total cycles");
      Array.Sort(meth_arr,
                 (m1, m2) => m2.pm.m_total_time.CompareTo(m1.pm.m_total_time));
      for (i = 0; i < cnt; i++) {
        Perf_Method pm = meth_arr[i].pm;
        stats_file.WriteLine(
            String.Format("{0,50}  {1,9}  {2,13}",
                          MethodIndex[pm.method_id].full_name,
                          pm.m_call_count.ToString(),
                          pm.m_total_time.ToString()));
      }

      stats_file.WriteLine("");
      stats_file.WriteLine("");
      stats_file.WriteLine("CASL Methods with fewest CASL dependencies");
      Array.Sort(meth_arr, CM_cmp);
      cnt = 200;
      if (cnt >= called_methods)
        cnt = called_methods;
      for (i = 0; i < cnt; i++) {
        Perf_Method pm = meth_arr[i].pm;
        stats_file.WriteLine(
            String.Format("{0,-50}  {1,3}/{2,3}  {3,9}  {4,13}",
                          MethodIndex[pm.method_id].full_name,
                          meth_arr[i].cs_call_cnt,
                          meth_arr[i].casl_call_cnt,
                          pm.m_call_count.ToString(),
                          pm.m_total_time.ToString()));

        string s2 = "      ";
        foreach (string s in meth_arr[i].casl_dependencies) {
          s2 += s + " ";
          if (s2.Length > 80) {
            stats_file.WriteLine(s2);
            s2 = "      ";
          }
        }
        if (s2.Length > 6)
          stats_file.WriteLine(s2);
      }

      stats_file.WriteLine("");
      stats_file.WriteLine("");
      stats_file.WriteLine("Methods never called");
      for (i = 0; i < uncalled_cnt; i++) {
        GenObj_Method gom = Uncalled[i];
        stats_file.WriteLine(gom.full_name + "  " +
                             gom.loc.ToString());
      }
#endif



#if DICT_STATS
      long total_has = 0;
      long total_get = 0;
      long total_set = 0;
      long total_rem = 0;
      
      stats_file.WriteLine("");
      stats_file.WriteLine("");
      stats_file.WriteLine(String.Format("{0,36} {1,15} {2,15} {3,15} {4,15}",
                                         "key", "has", "get", "set", "remove"));
      foreach (KeyValuePair<Object, GenericObject.dict_stats> kvp in
          GenericObject.gen_obj_key_stats) {
        if (kvp.Value.has_cnt + kvp.Value.get_cnt + kvp.Value.set_cnt > 1000) {
          if (kvp.Key is GenericObject k_geo) {
            long kn = k_geo.get_gen_obj_number();
            if (kn >= 0)
              stats_file.Write(String.Format("GenObj #{0,28}", kn.ToString()));
            else
              stats_file.Write("GenObj ....");
        else
            stats_file.Write(
                String.Format("{0,36}", kvp.Key.ToString()));
          stats_file.WriteLine(" {0,15} {1,15} {2,15} {3,15}",
                             kvp.Value.has_cnt,
                             kvp.Value.get_cnt,
                             kvp.Value.set_cnt,
                             kvp.Value.rem_cnt
                             );
        }
        total_has += kvp.Value.has_cnt;
        total_get += kvp.Value.get_cnt;
        total_set += kvp.Value.set_cnt;
        total_rem += kvp.Value.rem_cnt;
      }

      stats_file.WriteLine("");
      stats_file.WriteLine("");
      stats_file.WriteLine("total_has = " + total_has.ToString());
      stats_file.WriteLine("total_get = " + total_get.ToString());
      stats_file.WriteLine("total_set = " + total_set.ToString());
      stats_file.WriteLine("total_rem = " + total_rem.ToString());
      long tt = total_has + total_get + total_set + total_rem;
      stats_file.WriteLine("");
      stats_file.WriteLine("Total Dictionary Operations = " + tt.ToString());
#endif

      stats_file.WriteLine("");
      stats_file.WriteLine("");
#if DEBUG_CASL_Frame
      stats_file.WriteLine("Total Frame count " +
                                  CASL_Frame.frame_cnt.ToString());
#endif
      long total_genobj = GenericObject.get_total_genobj_allocated();
      if (total_genobj >= 0)
        stats_file.WriteLine("Total GenericObject count " +
                               total_genobj.ToString());

      stats_file.WriteLine("");
      stats_file.Close();

      return null;
    }

    public static object fCASL_callgraph_reset_stats(CASL_Frame frame)
    {
      if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
        Flt_Rec.log("fCASL_callgraph_reset_stats", "");
#if PERFORMANCE_GRAPH
      //
      // Zero out the call graph information so we get capture the behavior
      // within a snapshot of time --- often the start-end of a single method.
      //
      for (int i = 0; i < CASLInterpreter.MethodCount; i++) {
        Perf_Method cr1 =
            CASLInterpreter.call_graph.get_per_method_data(i);
        cr1.m_call_count = 0;
        cr1.m_return_count = 0;
        cr1.m_total_time = 0;
        cr1.m_min_time = Int64.MaxValue;
        cr1.m_max_time = 0;
        cr1.calls = new Dictionary<long, Perf_Call>();
      }
#endif
#if DICT_STATS
      foreach (dict_stats ds in GenericObject.gen_obj_key_stats.Values) {
        ds.get_cnt = 0;
        ds.has_cnt = 0;
        ds.set_cnt = 0;
        ds.rem_cnt = 0;
      }
#endif
      return null;
    }
    
    public static object CASL_is_null_or_empty_string(params Object[] args)
    {
      GenericObject geo_args = convert_args(args);
      string str = (geo_args.get("_subject", null) ?? "") as string;

      return String.IsNullOrEmpty(str);
    }

    public static object CASL_is_null_or_whitespace_string(params Object[] args)
    {
      GenericObject geo_args = convert_args(args);
      string str = (geo_args.get("_subject", null) ?? "") as string;

      return String.IsNullOrWhiteSpace(str);
    }

    public static string cleanUpDetailsForActLog(string details)
    {
      if (string.IsNullOrEmpty(details))
        return details;
      return details.Replace(':', ' ').Replace(',', '|');
    }

    /// <summary>
    /// Format the stack trace for the "detail" value in the activity log.
    /// Escape any commas or colons.
    /// Bug 23712 and Bug 17849 
    /// </summary>
    /// <param name="exception"></param>
    /// <returns></returns>
    public static string FormatStacktraceForActivityLog(Exception exception)
    {
      Exception e = exception;
      StringBuilder s = new StringBuilder();
      int counter = 1;
      while (e != null)
      {
        // HX:  Bug 17849 to protect again null message, it is possible user provide null message
        string e_message = e.Message != null ? e.Message : "";
        if (counter > 1)
          s.Append(',');
        s.Append("Error Message ").Append(counter).Append(':').Append(e_message.Replace(',', '|').Replace(':', ';'));
        s.Append(',');
        s.Append("Exception Type ").Append(counter).Append(':').Append(e.GetType().FullName.Replace(',', '|').Replace(':', ';'));
        s.Append(',');
        s.Append("Stack Trace ").Append(counter).Append(':');
        if (e.StackTrace != null)
          s.Append(e.StackTrace.Replace(',', '|').Replace(':', ';').Replace(" at ", Environment.NewLine + "at "));
        e = e.InnerException;
        counter++;
      }
      return s.ToString().Trim();
    }

    /// <summary>
    /// Attempt to get the id of the logged in user.
    /// Bug 25125
    /// </summary>
    /// <returns></returns>
    public static string getLoginName()
    {
      string login_user = "";
      try
      {
        GenericObject my = CASLInterpreter.get_my();
        if (my != null && CASLInterpreter.get_my().has("login_user"))
        {
          login_user = CASLInterpreter.get_my()["login_user"] as string;
        }
      }
      catch (Exception e)
      {
        Logger.cs_log("misc.getLoginName(): cannot read the login_name: " + e.Message, Logger.cs_flag.trace);
      }
      return login_user;
    }

    /// <summary>
    /// Attempt to get the application name ('Cashier', 'Portal', etc.)
    /// Bug 25125
    /// </summary>
    /// <returns></returns>
    public static string getApplicationName()
    {
      string application_name = "";
      try
      {
        GenericObject my = CASLInterpreter.get_my();
        if (my != null && CASLInterpreter.get_my().has("application_name"))
        {
          application_name = CASLInterpreter.get_my()["application_name"] as string;
        }
      }
      catch (Exception e)
      {
        Logger.cs_log("misc.getApplicationName(): cannot read the application_name: " + e.Message, Logger.cs_flag.trace);
      }
      return application_name;
    }

    public static string NewLinesToHTML(params Object[] args)
    {
      // Bug# 26417 DH - OneStep Historical Search Module
      
      GenericObject geo_args = misc.convert_args(args);
      string s = geo_args.get("data_string", c_CASL.c_req) as String;
      string strTemp = "";

      strTemp = s.Replace("\n", "<BR/>");
      strTemp = strTemp.Replace("\r", "<BR/>");

      return strTemp;
    }

    public static string MaskConnStrPassword(params Object[] args)
    {
      // Bug# 21505 DH - Mask the db connection string password for displaying it in the CASL_IDE.

      GenericObject geo_args = convert_args(args);
      String strConnectionString = "";

      try
      {
        strConnectionString = geo_args.get("pass", "") as string;
        int iPosStart = strConnectionString.IndexOf("Password=");
        int iEndPos = strConnectionString.IndexOf(";", iPosStart);

        string strPassVal = strConnectionString.Substring(iPosStart + 9);
        strPassVal = strPassVal.Substring(0, strPassVal.IndexOf(";"));
        
        // Encrypt the password so developers can still retrieve it.
        string strPasswordBase64 = misc.StringToBase64String(strPassVal);
        GenericObject bytes = Crypto.symmetric_encrypt("data", strPasswordBase64, "is_ascii_string", false);
        byte[] data = (byte[])bytes.get("data");
        strPassVal = Convert.ToBase64String(data);

        string s = strConnectionString.Substring(0, iPosStart);
        s += "Password=" + strPassVal + strConnectionString.Substring(iEndPos);
        strConnectionString = s;
      }
      catch(Exception ex)
      {
        strConnectionString = ex.Message;
      }

      return strConnectionString;
    }

  }

#region Sorting objects
  class SortItem : IComparable<SortItem>
  {
    public object obj;
    public ArrayList sortValues; // sortValues are either strings or numbers.
    public SortItem(object _obj)
    {
      obj = _obj;
      sortValues = new ArrayList(); // Values are numbers, null or strings
    }
    public override string ToString()
    {
      StringBuilder sb = new StringBuilder(obj + ", [", 2 * sortValues.Count + 5);
      if (sortValues.Count > 0)
      {
        if (sortValues[0] == null) sb.Append("(null)");
        else if (sortValues[0] is string) sb.Append("s" + sortValues[0]);
        else sb.Append(sortValues[0]);
      }
      for (int i = 1; i < sortValues.Count; i++)
      {
        sb.Append(", ");
        if (sortValues[i] == null) sb.Append("(null)");
        else if (sortValues[i] is string) sb.Append("s" + sortValues[i]);
        else sb.Append(sortValues[i]);
      }
      sb.Append("]");

      return sb.ToString();
    }
#region IComparable<SortItem> Members
    public int CompareTo(SortItem that)
    {
      if (this != null && that == null) { return 1; }  // Always win against nulls when non-null
      else if (this == null && that == null) { return 0; } // Tie if both are null
      else if (this == null && that != null) { return -1; } // Always lose when null and that isn't

      int comparison = 0;

      if (sortValues.Count == 0) // If we're empty
      {
        if (that.sortValues.Count == 0) { return 0; } // Equivalent if both empty
        else { return -1; } // Lose if we're empty and that is not
      }

      for (int i = 0; i < sortValues.Count; i++)
      {
        if (that.sortValues.Count <= i) { return 1; } // obj doesn't have an i-th entry

        object x = this.sortValues[i]; // Get objects
        object y = that.sortValues[i];

        if (x == null)
        {
          if (y == null) { comparison = 0; } // Both null is a tie, go to next entry (tie-break)
          else { return -1; } // Null loses to non-null
        }
        else if (x is string)
        {
          if (y is string)
          {
            comparison = string.Compare(x as string, y as string);
            if (comparison != 0) // Equivalence yields a tie -- go to tie-breaker
            {
              return comparison;
            }
          }
          else { return 1; } // String beats number or null
        }
        else if (y is string) { return -1; } // String beats number
                                             // Bug #12055 Mike O - Use misc.Parse to log errors
        else if (misc.Parse<Double>(x) > misc.Parse<Double>(y)) { return 1; } // Win by comparison
        else if (misc.Parse<Double>(y) > misc.Parse<Double>(x)) { return -1; } // Lose by comparison
      }
      return 0;
    }
#endregion
  }
#endregion
  public class GEO_comparer : IComparer
  {
    private static CaseInsensitiveComparer internalComparer = new CaseInsensitiveComparer();

    // Calls CaseInsensitiveComparer.Compare with the parameters reversed.
    int IComparer.Compare(Object x, Object y)
    {
      /*	If x, y are of different types or not value types then compare
			 * using the string representations */
      if (x.GetType() != y.GetType() || !x.GetType().IsValueType)
      {
        x = misc.CS_htm_sort(x); // Use the path as the
        y = misc.CS_htm_sort(y); // string equivalent

        if (x.GetType().IsValueType) { x = x.ToString(); } // Call ToString
        if (y.GetType().IsValueType) { y = y.ToString(); } // if necessary
      }
      if (x.GetType() != y.GetType())
        return 0; // do not compare different types
                  // Compare with the static comparision object
      return (internalComparer.Compare(x, y));
    }
  }
}
