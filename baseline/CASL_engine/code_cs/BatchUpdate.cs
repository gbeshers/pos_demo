//*************************************************************************
// THIS WHOLE FILE IS A NEW IMPLEMENTATION OF BATCH UPDATE FOR Bug 7564
// ------------------------------------------------------------------------
// DJD NOTE: Numerous code modifications and additions (far too many to 
// indicate each line with the bug number) have been made to this file for
// Bug 15464 - Batch Update Overhaul.
//*************************************************************************

/*
==================================================
      THIS IS A SAMPLE THAT WILL BE REMOVE WHEN CONFIG SUPPORTS THIS. YOU CAN RUN THIS IN THE IDE
      Make sure to choose a good file to run:

 *    CONFIGURATION:
 *    
 *      COMMUNICATION BETWEEN PRODUCT AND PROJECT LEVEL CLASSES:
 *          NOTE:
 *            PLEASE LOOK AT "PROJECT LEVEL SYSTEM INTERFACE IMPLEMENTATION REQUIREMENTS" SECTION BELOW
 *            ALSO PLEASE LOOK AT THE SAMPLE CLASS I CREATED FOR TESTING PRODUCT IMPLEMENTATION
 *            "ProjectLevelImplementationOfSystemInterface_SAMPLE"
 *          
 * 
 *      SYSTEM INTERFACES:
 *          CONFIGURE THE SAMPLE SYSTEM INTERFACE (sample_interface) FOR TESTING. THIS IS THE ONE
 *          TESTED BY Q/A IN PRODUCT LEVEL.
 *          THE SYSTEM INTERFACE CASL CLASS CONFIGURATION IS ACCESSED BY: 
 *             (PARAM:pConfigSystemInterface) -->> pConfigSystemInterface.get("path").ToString();
 *          
 *    
 * 
 *      SAMPLE MODE:
 *          IF SAMPLE SYSTEM INTERFACE IS CONFIGURED, SAMPLE MODE IS TURNED ON BY DEFAULT.
 *          m_bTestMode = TRUE
 *          SOME FUNCTIONALITY IS DISABLED IN SAMPLE MODE. READ BELOW TO SEE WHICH FUNCTIONS ARE EFFECTED.
 *      
 * 
 *      CONNECTIONS:
 *          USE OutputConnection CLASS TO MANAGE YOUR CONNECTIONS. THIS SHOULD SIMPLIFY AND CENTRALIZE
 *          YOUR CLOSING AND CLEANING UP CONNECTIONS PROCESS. IT WILL BE EASIER FOR ANYONE ELSE WORKING
 *          WITH YOUR CODE TO CATCH ANY OPEN CONNECTION PROBLEMS. THIS APPLYS TO FILE, DATABASE OR ANY 
 *          OTHER TYPE OF CONNECTION LIKE FTP ETC...
 *          
 * 
 *      PROJECT CODE ERRORS:
 *          USE "BatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError" FUNCTION TO ADD 
 *          ANY ERRORS GENERATED IN YOUR PROJECT IMPLEMENTATION. THESE WILL SHOW UP ON THE 
 *          BATCH UPDATE SUMMARY REPORT. USE (PARAM: object pProductLevelMainClass) TO CALL IT.
 *          IMPORTANT:
 *              THE COLUMN (TG_UPDATEFILE_DATA.COMMENTS) IS ONLY 250 CHARACTERS LONG
 *              ALL ERRORS WILL BE TRUNCATED TO THAT LENGTH. SO PLEASE KEEP ERRORS SHORT. 
 *              PREFERABLY ONLY ERROR CODES THAT CAN BE TRACED AT A LATER TIME. THIS WILL BE VERY 
 *              IMPORTANT IF YOU HAVE MULTIPLE ERRORS FOR DIFFERENT SYSTEM INTERFACES FOR THE SAME FILE. 
 *                  
 * 
 *      PROJECT LEVEL SYSTEM INTERFACE IMPLEMENTATION REQUIREMENTS:
 *      
 *          1.  "ProductLevel_SETUP" (MODIFY EXISTING ONLY)
 *                  NOTE: THIS IS A CRITICAL FUNCTION THAT ALLOWS PRODUCT LEVEL CODE COMMUNICATION WITH 
 *                  PROJECT LEVEL SYSTEM INTERFACE IMPLEMENTATION CLASSES. PLEASE LOOK AT THE SAMPLE.
 *                  THE BASIC IDEA IS THAT YOU ADD YOUR CLASS REFERENCE TO PRODUCT LEVEL HASHTABLE AND THAT
 *                  IN TURN IS USED WITH REFLECTION TO LOCATE AND INVOKE YOUR PROJECT LEVEL FUNCTIONS.
 *                  PROJECT LEVEL FUNCTIONS ARE PASSED A REFERENCE TO THE PRODUCT LEVEL CLASS 
 *                  FOR BACKWARD COMMUNICATION (PARAM: object pProductLevelMainClass)
 *          
 * 
 *          1.  "Project_CoreFile_BEGIN" FUNCTION. (NOT CALLED IN SAMPLE MODE)
 *                  CALLED BY "BatchUpdate_ProductLevel.ProductLevel_BeginCoreFile"
 *
 * 
 *          1.  "Project_ProcessCoreEvent"
 *                  CALLED BY "BatchUpdate_ProductLevel.ProductLevel_DispatchEvent"
 *            
 * 
 *          1.  "Project_ProcessDeposits" FUNCTION. (NOT CALLED IN SAMPLE MODE)
 *                  CALLED BY "BatchUpdate_ProductLevel.ProductLevel_DispatchDeposits"
 *          
 * 
 *          1.  "Project_RollbackCoreFile" FUNCTION. (ONLY CALLED FOR "CORE FILE" PROCESSING MODE)
 *                  CALLED BY "BatchUpdate_ProductLevel.ProductLevel_BeginCoreFile" or
 *                            "BatchUpdate_ProductLevel.ProductLevel_DispatchEvent" or 
 *                            "BatchUpdate_ProductLevel.ProductLevel_DispatchDeposits" or
 *                            "BatchUpdate_ProductLevel.ProductLevel_EndCoreFile"
 *                            if an error occurs (returns false) while processing a CORE file (Events or Deposits).
 *
 * 
 *          1.  "Project_CoreFile_END" FUNCTION. (NOT CALLED IN SAMPLE MODE)
 *                  CALLED BY "BatchUpdate_ProductLevel.ProductLevel_EndCoreFile"
 *
 * 
 *          1.  "Project_WriteTrailer"
 *                  CALLED BY "BatchUpdate_ProductLevel.ProductLevel_EndingBatchUpdate_WriteTrailers"
 * 
 * 
 *          1.  "Project_SendFiles"
 *                  CALLED BY "BatchUpdate_ProductLevel.ProductLevel_EndingBatchUpdate_SendFiles"
 * 
 * 
 *          1.  "Project_Rollback"
 *                  NOTE: THIS FUNCTION IS CALLED IN CRITICAL SITUATIONS WHEN BATCH UPDATE STATUS CANNOT BE SET 
 *                  CALLED BY "BatchUpdate_ProductLevel.ProductLevel_PrjRollback"
 *                  
 *                  
 *          1.  "Project_Cleanup"
 *                  NOTE: THIS IS GENERAL CLEANUP FUNCTION AND IS ALWAYS CALLED AT END OF BATCH UPDATE. 
 *                        IT SHOULD BE USED FOR CLOSING CONNECTIONS AND REMOVING OBJECTS.
 *                        PLEASE NULL ANY ALLOCATED OBJECT SO THAT THE GARBAGE COLLECTOR DEALLOCATES THEM
 *                        SOONER THAN LATER. THIS IS IMPORTANT WITH LARGE FILES AND LARGE MEMORY USAGE.
 *                        OTHERS MAYBE RUNNING REPORTS OR PROCESSING TRANSACTIONS IN CASHIERING AND WILL BE 
 *                        EFFECTED BY ANY MEMORY OVERUSE.
 *                  CALLED BY "BatchUpdate_ProductLevel.ProductLevel_PrjCleanup"
 *                
 *       

      YOU CAN USE CASL TO INVOKE THIS BATCH UPDATE WITH THE CODE BELOW:
      =================================================================
      Batch_update.<defmethod _name='run_batch_update'>
      corefiles=<v> "2009289001" </v>
      system_interface_list=<v> 'sample_interface' </v>
      db_data=TranSuite_DB.Data.db_connection_info 
      user_name="daniel"
      comment=""

      _impl=cs.CASL_engine.BatchUpdate_ProductLevel.RunBatchUpdate
      </defmethod>

      Batch_update.<run_batch_update/>
      =================================================================
 */

using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using CASL_engine;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Web.Compilation;
using System.Text; // Bug 14333
using System.Security;
using System.Runtime.InteropServices;

namespace CASL_engine
{
  // Daniel wrote these classes. Please do not change without talking to me first.
  // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

  /// <summary>
  /// Adding enum type for status recorded in the DB. There may need to be new statuses added
  /// for the BU processes in the future (i.e., COMPLETE_RECONCILED, INCOMPLETE_NOT_SENT)
  /// 
  ///  WORKING           : Set prior to the Batch Update job beginning and changes to a status below when the job finishes.
  ///  EXCLUDED_COMPLETE : Set when a Batch Update process is already completed (and "Force" is NOT selected) and will be skipped during processing.
  ///  EXCLUDED_WORKING  : Set when a Batch Update process is in a "WORKING" state (and "Force" is NOT selected) and will be skipped during processing.
  ///  INCOMPLETE        : Set when an error occurs during processing and the System/Core File is not completed.
  ///  COMPLETE          : Set after processing finishes and the System/Core File completed successfully.
  /// 
  /// IMPORTANT: A System/Core File record should NEVER be in a WORKING status after a BU job finishes. This indicates
  /// that processing was somehow interrupted and the true status is unknown (COMPLETE, INCOMPLETE, or Partially 
  /// COMPLETE).  If a BU process has this status when a job is started then it will NOT be processed (it will be excluded).
  /// A user will need to "force" the BU if re-processing is desired.
  /// </summary>
  public enum eStatusDB
  {
    WORKING,
    EXCLUDED_COMPLETE,
    EXCLUDED_WORKING,
    INCOMPLETE,
    COMPLETE,
  };

  /// <summary>
  /// The "Processing Mode" will determine if a Batch Update process should continue when an error is 
  /// encountered in a CORE file:
  ///   "SYSTEM" Mode    : An error will terminate the individual batch process.
  ///   "CORE_FILE" Mode : An error will "skip" a CORE file with an error and continue processing the
  ///                      other selected CORE files.
  /// IMPORTANT: The mode will default to SYSTEM if "Processing Mode" property is not found.
  /// IMPORTANT: The implementation of the BU process MUST explicitly support "CORE File" mode.
  /// </summary>
  public enum eProcessingMode
  {
    SYSTEM,
    CORE_FILE,
  };

  /// <summary>
  /// In addition to returning false (indicating an error) from the "Project_ProcessCoreEvent" or 
  /// "Project_ProcessDeposits" methods, the "Project implementation" for a BU Process may
  /// explicitly instruct the BU product to perform one of two "non-default" actions:
  ///   TERMINATE_SYSTEM_INTERFACE : Skips (terminates) the System Interface.
  ///   TERMINATE_ENTIRE_BATCH_RUN : Terminates the entire Batch Update Run. (i.e., ALL selected batch processes)
  ///   DEFAULT_ACTION_FOR_MODE    : Action is default - based on Processing Mode (see below).
  ///   
  /// The default "Corrective Action" will depend on the processing mode (see "eProcessingMode" defined above)
  /// 
  /// For example, while running in "CORE_FILE" Mode, the implementation for a Batch process can (optionally)
  /// terminate the system interface by setting TERMINATE_SYSTEM_INTERFACE for the return action.
  /// </summary>
  public enum eReturnAction
  {
    TERMINATE_SYSTEM_INTERFACE,
    TERMINATE_ENTIRE_BATCH_RUN,
    DEFAULT_FOR_MODE,
  };

  /// <summary>
  /// While WORKING these are the possible actions for the selected BU Processes:
  ///   CONTINUE : Everything is fine so continue processing this SI.
  ///   TERMINATE: SI has encountered an error while processing and is now terminated (i.e., for ALL selected CORE files).
  ///   SKIP     : SI is Complete for ALL selected Core files (and the BU is NOT forced).
  /// </summary>
  public enum eWorking
  {
    CONTINUE,
    TERMINATE,
    SKIP,
  };

  /// <summary>
  /// The ProcessState struct holds:
  /// The initial System Interface status (the start of a BU job) from the DB (COMPLETE or not for a Core file)
  /// The new System Interface status (the end of a BU job) from the processing results
  /// (Also the resulting System Interface state after the BU job has finished.)
  /// The CoreFile/System Interface Transaction Count and Amount (if SI is associated with the Transaction)
  /// The CoreFile/System Interface Tender Count and Amount (if SI is associated with the Tender)
  /// The CoreFile/System Interface "Project" Count and Amount ("Project_CoreFile_END" optional method)
  /// </summary>
  public struct ProcessStateForCoreFile
  {
    public bool bCompleteStatusInDB;   // True if this system interface for this CORE file has been marked COMPLETE in the DB; false otherwise.
    public bool bCompletePostBatchJob; // True if this system interface for this CORE file has been marked COMPLETE in the DB or is newly complete after the Batch Job completed; false otherwise.
    public bool bWorkingStatusInDB;    // True if this system interface for this CORE file has been marked WORKING (and NOT COMPLETE) in the DB; false otherwise.
    public bool bSkipCoreFile_Error;   // True if this CORE file processing should be skipped due to an error when processing; false otherwise. (CORE file processing mode)

    public long lTransactionCount;  // Count calculated from product from CORE file transactions associated with SI
    public double dTransactionAmt;    // Amount calculated from product from CORE file transactions associated with SI
    public long lTenderCount;       // Count calculated from product from CORE file tenders associated with SI
    public double dTenderAmt;         // Amount calculated from product from CORE file tenders associated with SI 
    public long? lProjectCount;      // Count returned from project  ("Project_CoreFile_END" opt method) [overrides product value if NOT null]
    public double? dProjectAmt;        // Amount returned from project ("Project_CoreFile_END" opt method) [overrides product value if NOT null]

    // Constructor:
    public ProcessStateForCoreFile(bool bCompleteDB)
    {
      this.bCompleteStatusInDB = bCompleteDB;
      this.bCompletePostBatchJob = bCompleteDB;
      this.bWorkingStatusInDB = false;
      this.bSkipCoreFile_Error = false;
      this.lTransactionCount = 0;
      this.dTransactionAmt = 0.00;
      this.lTenderCount = 0;
      this.dTenderAmt = 0.00;
      this.lProjectCount = null;
      this.dProjectAmt = null;
    }

    // Constructor:
    public ProcessStateForCoreFile(eStatusDB DBStatus)
    {
      this.bCompleteStatusInDB = (DBStatus == eStatusDB.COMPLETE);
      this.bCompletePostBatchJob = this.bCompleteStatusInDB;
      this.bWorkingStatusInDB = (DBStatus == eStatusDB.WORKING);
      this.bSkipCoreFile_Error = false;
      this.lTransactionCount = 0;
      this.dTransactionAmt = 0.00;
      this.lTenderCount = 0;
      this.dTenderAmt = 0.00;
      this.lProjectCount = null;
      this.dProjectAmt = null;
    }
  }

  /// <summary>
  /// The CoreFileData struct is primarily used for updating the TG_UPDATEFILE_DATA at end of the BU job.
  /// pSysIntState Dictionary: Lists System Interface names and the DB COMPLETE Status/Result for CORE File:
  /// Key   (string)               : System Interface Name (ID)
  /// Value (bCompleteStatusInDB   : True = System Interface is COMPLETE (in database: BU Job start) for CORE file. False = NOT COMPLETE.
  ///       (bCompletePostBatchJob : True = System Interface is COMPLETE (at BU Job completion) for CORE file. False = NOT COMPLETE.
  ///       (bWorkingStatusInDB    : True = System Interface is WORKING (in database: BU Job start) for CORE file. False = NOT WORKING.
  ///       (bSkipCoreFile_Error   : True = CORE file processing should be skipped due to an error when processing; False otherwise. (CORE file processing mode)
  ///       (lTransactionCount     : Count calculated from product from CORE file transactions associated with SI
  ///       (dTransactionAmt       : Amount calculated from product from CORE file transactions associated with SI
  ///       (lTenderCount          : Count calculated from product from CORE file tenders associated with SI
  ///       (dTenderAmt            : Amount calculated from product from CORE file tenders associated with SI 
  ///       (lProjectCount         : Count returned from project  ("Project_CoreFile_END" opt method) [overrides product value if NOT null]
  ///       (dProjectAmt           : Amount returned from project ("Project_CoreFile_END" opt method) [overrides product value if NOT null]
  /// </summary>
  public struct CoreFileData
  {
    public string strUpdateID;      // Batch Update ID
    public string strFileNumber;    // CORE file number
    public string strFileSequence;  // CORE file sequence number
    public Dictionary<string, ProcessStateForCoreFile> pSysIntState; // Batch processing state of each selected SI for this CORE file

    // Constructor:
    public CoreFileData(string strUpdateID, string strFileNumber, string strFileSequence, Dictionary<string, ProcessStateForCoreFile> pSystemInterfaceState)
    {
      this.strUpdateID = strUpdateID;
      this.strFileNumber = strFileNumber;
      this.strFileSequence = strFileSequence;
      this.pSysIntState = pSystemInterfaceState;
    }
  }

  /// <summary>
  /// Daniel wrote these classes. Please do not change without talking to me first. ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!
  /// </summary>
  public struct ErrorData // Bug 8224 DH
  {
    public string strFileName;
    public string strSystemInterfaceID;
    public string strErrorMessage;

    // Constructor:
    public ErrorData(string sFile, string sSysInt, string sErrMsg)
    {
      this.strFileName = sFile;
      this.strSystemInterfaceID = sSysInt;
      this.strErrorMessage = sErrMsg;
    }
  }

  /// <summary>
  /// Daniel wrote this class. Please do not change without talking to me first. ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!
  /// Use this class for establishing project level system interface scope class output files, database, FTP or other types of connections.
  /// Please only use a single connection per each instance of this class.
  /// </summary>
  public class OutputConnection
  {
    public TextWriter m_pOutputFile = null;
    public SqlConnection m_pSqlConnection = null;

    public bool CloseConnection(ref string strError)
    {
      // Close any connection that was established.
      // Also returns false and an error string in case of an error while closing connection.
      // You can then determine if it's something to worry about.
      try
      {
        if (m_pOutputFile != null)
          m_pOutputFile.Close();


        if (m_pSqlConnection != null)
          m_pSqlConnection.Close();

      }
      catch (Exception ex)
      {
        // HX: Bug 17849
        strError = ex.Message;
        Logger.LogError(ex.ToMessageAndCompleteStacktrace(), "Batch Update");
        Logger.cs_log( string.Format("Batch Update Close connection failed:{0}.", ex.ToMessageAndCompleteStacktrace()));
        return false;
      }

      return true;
    }
  }

  /// <summary>
  /// Definitions of constants used in Batch Update processing.
  /// </summary>
  public static class BU_Const
  {
    public const string sSYS_INT_SCOPE = "SYS_INTERFACE_SCOPE_ONLY";
  }

  /// <summary>
  /// Daniel wrote this class. Please do not change without talking to me first. ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!
  /// </summary>
  public class BatchUpdate_ProductLevel
  {
    private static int m_BatchUpdateRunCount = 0; // Used to determine (and prevent) when users attempt to run more than one BU at the same time.
    public bool m_bRunBatchUpdateStarted = false;
    public bool m_bRunBatchUpdateCompletedSuccessfully = false;
    public GenericObject m_pSelectedSysInterfaces = null;
    public GenericObject m_pSelectedCoreFiles = null;
    public GenericObject m_geoBatchUpdateApp = null;
    public Hashtable m_pSysInterfaceProjectImplementations = null;
    public ArrayList m_arrSysIntsThatProcessFullFiles = null; // Bug 12136 DH - PB needs the whole file.
    public Hashtable m_pAllTransactionTypesWithSysInterfaces = null;
    public Hashtable m_pAllTenderTypesWithSysInterfaces = null; // Bug 14210
    public string m_strUserName = "";
    public string m_strUpdateID = "";
    public string m_strBatchJobComments = "";
    public bool m_bTestMode = false;
    public string m_strEmailReceiver = "";    // Configurable comma delimited email notification group.
    public bool m_bForceBatchJob = false; // Indicates the user has selected to "Force" running previously completed BU processes. 
    public bool m_bAutoBatchJob = false; // Indicates the Batch Update run is automated (e.g., from scheduled task). 
    public ArrayList m_pSysInterfaceErrorsList = null;
    public DateTime m_dtStartDateTime;
    public decimal m_totalAmount = Decimal.Zero;    // Bug 16806 
    public long m_totalCount = 0;                   // Bug 16806 

    /// <summary>
    /// This Dictionary is needed so that each Batch Update process is treated separately; it will allow one
    /// Batch Update process (i.e. System Interface) to continue and complete even if another fails to complete.
    /// Key (string)   : Selected system interface ID
    /// Value (Working): TERMINATE = Indicates that process (SI) is to be terminated due to an error or other issue.
    ///                  SKIP      = Indicates that process (SI) is to be skipped because it has been completed
    ///                              previously for ALL Core files selected. 
    ///                  CONTINUE  = Processing normally (not terminated or skipped).
    /// </summary>
    public Dictionary<string, eWorking> m_SysIntProcessAction = null;

    /// <summary>
    /// This Dictionary is needed so that a completed process (System Interface) is not processed again and so
    /// that CORE files with ALL the Batch Update System Interfaces COMPLETE are set to UPDATED.
    /// It will also maintain the "CORE file skip" state for any BU processes that are not running at the SYSTEM level.
    /// Key (string): CORE File [formatted CoreFile = strFileNbr + strFileSequenceNbr]
    /// Value (Dictionary<string, ProcessState>): Lists System Interface names and correponding Core File Data ("CoreFileData" defined above)
    /// </summary>
    public Dictionary<string, CoreFileData> m_CoreFileSysInterfaceState = null;

    public BatchUpdate_ProductLevel(params Object[] arg)
    {
      System.Threading.Interlocked.Increment(ref m_BatchUpdateRunCount);
    }

    public BatchUpdate_ProductLevel()
    {
      System.Threading.Interlocked.Increment(ref m_BatchUpdateRunCount);
    }

    // Bug 10394 UMN removed empty destructor

    // Database types
    int m_iCurrentDatabase_SqlServer = 2;
    int m_iCurrentDatabase_MsAccess_ODBC = 3;
    int m_iDBType = 0;

    GenericObject m_pDBInfo = null; // Passed in from the caller of RunBatchUpdate().

    /// <summary>
    /// Records a WORKING status entry in TG_UPDATEFILE_DATA for ALL selected Core File/System Interface combinations.
    /// </summary>
    /// <param name="strDBConnectionString"></param>
    /// <param name="strError"></param>
    /// <returns></returns>
    public bool ProductLevel_SetBatchUpdateFileStatus_Start(string strDBConnectionString, ref string strError)
    {
      const string sPREVIOUSLY_COMPLETE_COMMENT = @"The selected system was previously marked COMPLETE.";
      const string sPREVIOUSLY_WORKING_COMMENT = @"The selected system was previously marked WORKING.";
      StringBuilder sb = new StringBuilder();

      // Iterate over each selected Core File:
      foreach (KeyValuePair<string, CoreFileData> fd in m_CoreFileSysInterfaceState)
      {
        // Iterate over each selected System Interface and use the corresponding file data:
        for (int iSI_Index = 0; iSI_Index < m_pSelectedSysInterfaces.getLength(); iSI_Index++)
        {
          string sSysInt = (string)m_pSelectedSysInterfaces.get(iSI_Index);
          string comment = "";
          if (!m_bForceBatchJob)
          {
            comment = fd.Value.pSysIntState[sSysInt].bCompleteStatusInDB ? sPREVIOUSLY_COMPLETE_COMMENT :
                      (fd.Value.pSysIntState[sSysInt].bWorkingStatusInDB ? sPREVIOUSLY_WORKING_COMMENT : "");
          }
          if (!string.IsNullOrEmpty(comment))
          {
            ProductLevel_AddSystemInterfaceError(fd.Key, sSysInt, comment);
          }
          sb.AppendLine(
              string.Format("INSERT INTO TG_UPDATEFILE_DATA(UPDATE_ID,DEPFILENBR,DEPFILESEQ,INTERFACE_ID,STATUS,COMMENTS,UPDATE_COUNT,UPDATE_TOTAL) VALUES('{0}','{1}','{2}','{3}','{4}','{5}',{6},{7});"
              , fd.Value.strUpdateID
              , fd.Value.strFileNumber
              , fd.Value.strFileSequence
              , sSysInt
              , eStatusDB.WORKING.ToString()
              , comment.Replace("'", "''")
              , 0
              , 0)
              );
        }
      }
      string strSQL = sb.ToString();

      return ConnectToDatabaseAndUpdateStatus(strDBConnectionString, ref strError, strSQL);
    }

    /// <summary>
    /// Records a COMPLETE, INCOMPLETE, EXCLUDED_COMPLETE, or EXCLUDED_WORKING status entry in TG_UPDATEFILE_DATA
    /// for ALL selected Core File and selected System Interface combinations.  In addition, any error messages 
    /// (or other relevant information) are recorded in the Comments field.  Also, modifies the "bCompletePostBatchJob"
    /// status for those CORE file/System Interface combinations that are newly complete.
    /// Sets the "m_bRunBatchUpdateCompletedSuccessfully" boolean value.
    /// </summary>
    /// <param name="strDBConnectionString"></param>
    /// <param name="strError"></param>
    /// <returns></returns>
    public bool ProductLevel_SetBatchUpdateFileStatus_Finish(string strDBConnectionString, ref string strError)
    {
      const string sFORCE_PREV_COMPLETE_COMMENT = @"Re-processed (Force): The selected system was previously marked COMPLETE.";
      const string sFORCE_PREV_WORKING_COMMENT = @"Re-processed (Force): The selected system was previously marked WORKING.";
      const string sDEFAULT_TERMINATE_COMMENT = @"Process terminated due to an error.";
      const int COMMENTS_MAX_LENGTH = 250; // COMMENTS field maximum size is 250 characters.
      StringBuilder sb = new StringBuilder();
      bool bReturnValue = false;

      try
      {
        // Set "m_bRunBatchUpdateCompletedSuccessfully" to true initially
        m_bRunBatchUpdateCompletedSuccessfully = true;

        // Iterate over each selected Core File:
        foreach (KeyValuePair<string, CoreFileData> fd in m_CoreFileSysInterfaceState)
        {
          // Iterate over each SELECTED System Interface and use the corresponding file data:
          for (int iSysIntCount = 0; iSysIntCount < m_pSelectedSysInterfaces.getLength(); iSysIntCount++)
          {
            string sysIntName = (string)m_pSelectedSysInterfaces.get(iSysIntCount);
            ProcessStateForCoreFile procState = fd.Value.pSysIntState[sysIntName];
            string coreFileName = fd.Key;
            string comment = "";
            string defaultComment = "";

            // Set status to COMPLETE to initialize
            eStatusDB statusDB = eStatusDB.COMPLETE;

            switch (m_SysIntProcessAction[sysIntName])
            {
              case eWorking.CONTINUE:
                if (procState.bSkipCoreFile_Error)
                {
                  // Set status to INCOMPLETE if CORE file was skipped due to an error
                  statusDB = eStatusDB.INCOMPLETE;
                  defaultComment = sDEFAULT_TERMINATE_COMMENT;
                }
                else
                {
                  if (procState.bCompleteStatusInDB)
                  {
                    if (m_bForceBatchJob) // Add the "Force Process" comment for COMPLETE
                    {
                      ProductLevel_AddSystemInterfaceError(coreFileName, sysIntName, sFORCE_PREV_COMPLETE_COMMENT);
                    }
                    else // EXCLUDED_COMPLETE: SI was NOT forced and already COMPLETE for this CORE file (i.e., should have been skipped) 
                    {
                      statusDB = eStatusDB.EXCLUDED_COMPLETE;
                    }
                  }
                  else if (procState.bWorkingStatusInDB)
                  {
                    if (m_bForceBatchJob) // Add the "Force Process" comment for WORKING
                    {
                      ProductLevel_AddSystemInterfaceError(coreFileName, sysIntName, sFORCE_PREV_WORKING_COMMENT);
                    }
                    else // EXCLUDED_WORKING: SI was NOT forced and had a WORKING state for this CORE file (i.e., should have been skipped)
                    {
                      statusDB = eStatusDB.EXCLUDED_WORKING;
                    }
                  }
                }
                break;

              case eWorking.TERMINATE:
                if (!m_bForceBatchJob && procState.bCompleteStatusInDB)
                {   // EXCLUDED_COMPLETE: SI was NOT forced with a COMPLETE state for this CORE file (i.e., should have been skipped)
                  statusDB = eStatusDB.EXCLUDED_COMPLETE;
                }
                else if (!m_bForceBatchJob && procState.bWorkingStatusInDB)
                {   // EXCLUDED_WORKING: SI was NOT forced with a WORKING state for this CORE file (i.e., should have been skipped)
                  statusDB = eStatusDB.EXCLUDED_WORKING;
                }
                else
                {   // Set status to INCOMPLETE if terminated
                  statusDB = eStatusDB.INCOMPLETE;
                  defaultComment = sDEFAULT_TERMINATE_COMMENT;
                }
                break;

              case eWorking.SKIP:
                // Set to EXCLUDED(_COMPLETE or _WORKING) if all selected Core Files were already COMPLETE or WORKING for SI (and BU was NOT forced)
                statusDB = procState.bCompleteStatusInDB ? eStatusDB.EXCLUDED_COMPLETE : eStatusDB.EXCLUDED_WORKING;
                break;
            }

            if (statusDB != eStatusDB.COMPLETE && statusDB != eStatusDB.EXCLUDED_COMPLETE)
            {
              // Set "m_bRunBatchUpdateCompletedSuccessfully" to false if there were any non-COMPLETE
              // (or EXCLUDED_COMPLETE) system/core file results
              m_bRunBatchUpdateCompletedSuccessfully = false;
              // Bug 25383 [24854] DJD: Add Logging to Batch Update Processing for "Non-Complete" System/Core File Items 
              string errMsg = string.Format("{0} SYSTEM [{1}] CORE FILE [{2}] STATUS [{3}]"
                , @"NON-COMPLETE System-Core File item found:"
                , sysIntName
                , coreFileName
                , statusDB.ToString());
              LogBatchUpdateError(errMsg);
            }

            // Modify the "bCompletePostBatchJob" status for those CORE file/System Interface combinations that are newly complete.
            if (!procState.bCompletePostBatchJob && statusDB == eStatusDB.COMPLETE)
            {
              ProcessStateForCoreFile ps = m_CoreFileSysInterfaceState[coreFileName].pSysIntState[sysIntName];
              ps.bCompletePostBatchJob = true;
              m_CoreFileSysInterfaceState[coreFileName].pSysIntState[sysIntName] = ps;
            }

            // Get any error messages for the COMMENT field.
            for (int i = 0; i < m_pSysInterfaceErrorsList.Count; i++)
            {
              ErrorData d = (ErrorData)m_pSysInterfaceErrorsList[i];
              if (d.strSystemInterfaceID.Equals(sysIntName))
              {
                if (d.strFileName.Equals(BU_Const.sSYS_INT_SCOPE) || d.strFileName.Equals(coreFileName))
                {
                  comment += string.Format("{0} [{1}]"
                      , string.IsNullOrEmpty(comment) ? string.Format("{0}:", d.strSystemInterfaceID) : ""
                      , d.strErrorMessage);
                }
              }
            }

            // If there is no specific error message then use the default error message.
            if (!string.IsNullOrEmpty(defaultComment) && string.IsNullOrEmpty(comment))
            {
              comment += (sysIntName + ": [" + defaultComment.Trim() + "]");
            }

            // Bug 9015 - Trim comments even if data is lost; Project code should not exceed the table column width of 250.
            comment = comment.Length > COMMENTS_MAX_LENGTH ? comment.Substring(0, COMMENTS_MAX_LENGTH) : comment;

            long lUpdateCount = 0;
            double dUpdateTotal = 0.00;
            if (statusDB == eStatusDB.COMPLETE)
            {
              // For the count and amount the precedence is: 1) Project values, 2) Tender values, 3) Transaction Values
              if (procState.lProjectCount.HasValue && procState.dProjectAmt.HasValue)
              {
                lUpdateCount = (long)procState.lProjectCount;
                dUpdateTotal = (double)procState.dProjectAmt;
              }
              else
              {
                lUpdateCount = procState.lTenderCount > 0 ? procState.lTenderCount : procState.lTransactionCount;
                dUpdateTotal = procState.lTenderCount > 0 ? procState.dTenderAmt : procState.dTransactionAmt;
              }
            }

            sb.AppendLine(
                string.Format("UPDATE TG_UPDATEFILE_DATA SET STATUS='{0}',COMMENTS='{1}',UPDATE_COUNT={2}, UPDATE_TOTAL={3} WHERE UPDATE_ID='{4}' AND INTERFACE_ID='{5}' AND DEPFILENBR={6} AND DEPFILESEQ={7};",
                statusDB.ToString(),
                comment.Replace("'", "''"),
                lUpdateCount,
                dUpdateTotal,
                fd.Value.strUpdateID,
                sysIntName,
                ProductLevel_atoi(fd.Value.strFileNumber),
                ProductLevel_atoi(fd.Value.strFileSequence))
                );

            m_totalAmount = Decimal.Add(m_totalAmount, Convert.ToDecimal(dUpdateTotal));
            m_totalCount = m_totalCount + lUpdateCount;

          }
        }
        string strSQL = sb.ToString();
        bReturnValue = ConnectToDatabaseAndUpdateStatus(strDBConnectionString, ref strError, strSQL);
      }
      catch (Exception ex)
      {
        // Set "m_bRunBatchUpdateCompletedSuccessfully" to false if error
        m_bRunBatchUpdateCompletedSuccessfully = false;

        // Log the error
        string errMsg = string.Format("{0}", @"An Error Has Been Encountered While Writing to TG_UPDATEFILE_DATA Table.");
        LogBatchUpdateError(ex, errMsg);
        return false;
      }
      return bReturnValue;
    }

    /// <summary>
    /// Updates the TG_DEPFILE_DATA table with the User ID and Update Date to any selected CORE file that meets the
    /// following condition: ALL Batch Update processes are COMPLETE for the CORE file.
    /// MUST check the status for ALL Batch Update system interfaces (i.e., NOT just the SIs currently selected)
    /// and if they are ALL COMPLETE then mark the selected CORE file updated.
    /// IMPORTANT PRE-CONDITION: The "ProductLevel_SetCoreFilesAsUpdated" method MUST be called after calling the
    /// "ProductLevel_SetBatchUpdateFileStatus_Finish" method because it sets the bCompletePostBatchJob value.
    /// </summary>
    /// <param name="strDBConnectionString"></param>
    /// <param name="strError"></param>
    /// <returns></returns>
    public bool ProductLevel_SetCoreFilesAsUpdated(string strDBConnectionString, ref string strError)
    {
      // This is for the SQL statements:            
      StringBuilder sb = new StringBuilder();

      // Iterate over each selected Core File:
      foreach (KeyValuePair<string, CoreFileData> fileData in m_CoreFileSysInterfaceState)
      {
        // Iterate over each BU System Interface and check the post batch job "Complete" status for the CORE file:
        bool bAllBatchSystemInterfacesAreCOMPLETE = true;
        foreach (KeyValuePair<string, ProcessStateForCoreFile> systemState in fileData.Value.pSysIntState)
        {
          if (!systemState.Value.bCompletePostBatchJob)
          {
            bAllBatchSystemInterfacesAreCOMPLETE = false;
            break;
          }
        }
        if (bAllBatchSystemInterfacesAreCOMPLETE) // Mark the CORE file Updated:
        {
          string strUpdateDateTime = ProductLevel_FormatDateForDB(m_dtStartDateTime);
          sb.AppendLine(
              string.Format("UPDATE TG_DEPFILE_DATA SET UPDATE_USERID='{0}',UPDATEDT='{1}' WHERE DEPFILENBR={2} AND DEPFILESEQ={3}; "
              , m_strUserName.Replace("'", "''")
              , strUpdateDateTime
              , ProductLevel_atoi(fileData.Value.strFileNumber)
              , ProductLevel_atoi(fileData.Value.strFileSequence)
              ));
        }
      }
      string strSQL = sb.ToString();
      return ConnectToDatabaseAndUpdateStatus(strDBConnectionString, ref strError, strSQL);
    }

    /// <summary>
    /// Inserts a new record into the TG_UPDATE_DATA table for the completed BU job with the Update ID, Start Time,
    /// End Time, User ID, and Comments.
    /// </summary>
    /// <param name="strDBConnectionString"></param>
    /// <param name="strError"></param>
    /// <returns></returns>
    public bool ProductLevel_SetBatchUpdateJobStatus(string strDBConnectionString, ref string strError)
    {
      string sResult = m_bRunBatchUpdateCompletedSuccessfully ? @"Run was Successful" : @"Run was NOT Wholly Successful";
      m_strBatchJobComments = string.Format("{0}{1}{2}"
      , m_strBatchJobComments
      , string.IsNullOrEmpty(m_strBatchJobComments) ? "" : " | "
      , sResult
      );
      const int nCOMMENTS_MAX_SIZE = 250;
      if (m_strBatchJobComments.Length > nCOMMENTS_MAX_SIZE)
      {
        m_strBatchJobComments = m_strBatchJobComments.Substring(0, nCOMMENTS_MAX_SIZE); // Field maximum size is 250 characters.
      }
      string strStartDateTime = ProductLevel_FormatDateForDB(m_dtStartDateTime); // This must be formatted for particular RDBMS.
      string strEndDateTime = ProductLevel_FormatDateForDB(System.DateTime.Now);

      string strSQL = @"INSERT INTO TG_UPDATE_DATA (UPDATE_ID,START_TIME,END_TIME,USERID,COMMENTS) VALUES";
      bool retVal = false;
      if (m_iDBType == m_iCurrentDatabase_SqlServer)
      {
        strSQL += @"(@updateId,@startTime,@endTime,@userId,@comments)";
        retVal = ConnectToSqlSvrDBAndExecuteNonQuery(strDBConnectionString, ref strError, strSQL
            , new SqlParameter("@updateId", m_strUpdateID)
            , new SqlParameter("@startTime", strStartDateTime)
            , new SqlParameter("@endTime", strEndDateTime)
            , new SqlParameter("@userId", m_strUserName)
            , new SqlParameter("@comments", m_strBatchJobComments)
            );
      }
      return retVal;
    }

    /// <summary>
    /// When a Batch Update job is finished there are three tables in the DB that need to have records updated or inserted:
    /// 
    /// 1) TG_UPDATEFILE_DATA [Method: ProductLevel_SetBatchUpdateFileStatus_Finish]
    ///    Update all the selected CORE File/System Interface combinations with the following: Status, Comments, Update Count, and Update Total.
    ///    
    /// 2) TG_DEPFILE_DATA    [Method: ProductLevel_SetCoreFilesAsUpdated]
    ///    Update the User ID and Update Date to any selected CORE file that meets the following condition: ALL Batch Update processes are COMPLETE for the CORE file.
    /// 
    /// 3) TG_UPDATE_DATA     [Method: ProductLevel_SetBatchUpdateJobStatus]
    ///    Insert a record for the completed BU job with the Update ID, Start Time, End Time, User ID, and Comments.
    /// 
    /// This method returns true if writing to the DB is successful; false otherwise and strError contains the error message.
    /// </summary>
    /// <param name="strDBConnectionString"></param>
    /// <param name="strError"></param>
    /// <returns></returns>
    public bool ProductLevel_WriteToDB_BatchUpdateJobFinished(string strDBConnectionString, ref string strError)
    {
      StringBuilder sbTableFail = new StringBuilder();
      char delimiter = ',';
      bool bWriteToDBSuccess = true;

      // Write to the TG_UPDATEFILE_DATA table:
      if (!ProductLevel_SetBatchUpdateFileStatus_Finish(strDBConnectionString, ref strError))
      {
        bWriteToDBSuccess = false;
        sbTableFail.Append(string.Format("{0}{1}", "TG_UPDATEFILE_DATA", delimiter));
      }

      // Write to the TG_DEPFILE_DATA table:
      if (!ProductLevel_SetCoreFilesAsUpdated(strDBConnectionString, ref strError))
      {
        bWriteToDBSuccess = false;
        sbTableFail.Append(string.Format("{0}{1}", "TG_DEPFILE_DATA", delimiter));
      }

      // Write to the TG_UPDATE_DATA table:
      if (!ProductLevel_SetBatchUpdateJobStatus(strDBConnectionString, ref strError))
      {
        bWriteToDBSuccess = false;
        sbTableFail.Append(string.Format("{0}{1}", "TG_UPDATE_DATA", delimiter));
      }

      // NOTE: The detailed DB error(s) will be in the cs log file.
      if (!bWriteToDBSuccess)
      {
        strError = "After finishing Batch Update job, failed to write to table(s): " + sbTableFail.ToString().TrimEnd(delimiter);
      }

      return bWriteToDBSuccess;
    }

    public string ProductLevel_FormatDateForDB(DateTime dtDateTime)
    {
      // Daniel wrote this function. Please do not change without talking to me first. ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      string strDBFormatedDate = "";
      string strDate = dtDateTime.ToString("MM/dd/yyyy HH:mm:ss");

      if (m_iDBType == m_iCurrentDatabase_MsAccess_ODBC || m_iDBType == m_iCurrentDatabase_SqlServer)
        strDBFormatedDate = strDate;

      return strDBFormatedDate;
    }

    /// <summary>
    /// This will stop running Batch Update without the BU report displaying. [NOTE: This method
    /// is called when BU has an unexpected (i.e., uncaught during normal processing) error.]  
    /// An error message will be displayed to the user, logged, and BU process exited.
    /// </summary>
    /// <param name="errMsg"></param>
    private static void StopBatchUpdateJobWithNoReport(string errMsg, BatchUpdate_ProductLevel pBatchUpdate_ProductLevel)
    {
      // Log the error:
      pBatchUpdate_ProductLevel.LogBatchUpdateError(errMsg);

      const string sEND_BU_ERROR_PREFIX = "end_bu_error:";
      string endBuErr = string.Format("{0}{1}", sEND_BU_ERROR_PREFIX, errMsg);
      // The sEND_BU_ERROR_PREFIX indicates to js function GetProgressBarUpdate() to close the 
      // progress bar window and exit active BU processing window.
      c_CASL.GEO.set("BatchUpdateActivityProgress", endBuErr);
    }

    /// <summary>
    /// Executes a Batch Update job for the selected System Interfaces and Core Files.
    /// DJD added this "try-catch" RunBatchUpdate method so that any unhandled exceptions are caught and logged.
    /// An instance (_this) of the BatchUpdate_ProductLevel class is created from within this static method
    /// (RunBatchUpdate) of the same class.
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static object RunBatchUpdate(params Object[] args)
    {
      System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
      watch.Start();
      string errMsg = "";
      bool bTerminationByUnexpectedError = false;
      GenericObject pArgs = misc.convert_args(args);
      BatchUpdate_ProductLevel _this = new BatchUpdate_ProductLevel(); // Create instance (_this) of the BatchUpdate_ProductLevel class
      try
      {
        // Initialize values for various member variables [Some are needed for Activity Log].
        if (!_this.InitializeMemberVariables(pArgs)) { return false; }

        // Write BU Start to Activity Log
        _this.WriteToActivityLog("Batch Update Run START", "", null, true
            , "BU Run Type", (_this.m_bAutoBatchJob ? "Automated" : "Started Manually")
            , "Force Selected", (_this.m_bForceBatchJob ? "Yes" : "No")
            );

        // Check if there is already an instance of "BatchUpdate_ProductLevel" - this indicates that 
        // BU is already running.  Do NOT allow user to proceed and run another BU job.
        if (m_BatchUpdateRunCount > 1)
        {
          errMsg = string.Format("{0}{1}"
              , @"There is already a Batch Update job running.  "
              , @"Batch Update processing may not be started by more than one iPayment user at the same time.");

          // Log the error
          _this.LogBatchUpdateError(errMsg);

          // Write error to Activity Log
          _this.WriteToActivityLog("Batch Update Error", "BU job already running", "Error", errMsg);

          // Future enhancement: Need to find a way to display this info to the user.
          return false;
        }
        else
        {
          _this.m_bRunBatchUpdateStarted = true;
          return RunBatchUpdate_aux(ref pArgs, ref _this);
        }
      }
      catch (Exception ex)
      {
        // Log the error and display message to user
        _this.m_bRunBatchUpdateCompletedSuccessfully = false;
        errMsg = string.Format("{0}", @"An Unexpected Error Has Been Encountered During Batch Update Processing.");
        _this.LogBatchUpdateError(ex, errMsg);
        _this.WriteToActivityLog("Batch Update Error", "Unexpected Error", ex); // Write error to Activity Log
        errMsg = string.Format("{0}{1}", errMsg, @"  Please have an administrator check the log for details.");
        StopBatchUpdateJobWithNoReport(errMsg, _this);
        bTerminationByUnexpectedError = true;
        return false;
      }
      finally
      {
        //-------------------------------------------------------------
        // Write BU Finish to Activity Log
        watch.Stop();
        TimeSpan ts = watch.Elapsed; // Get the elapsed time as a TimeSpan value.
        string elapsedTimeKey = "Elapsed Time [hh-mm-ss.ss]";
        string elapsedTimeValue = String.Format("{0:00}-{1:00}-{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10); // Format the TimeSpan value. 
        if (_this.m_bRunBatchUpdateStarted)
        {
          string sSummary = _this.m_bRunBatchUpdateCompletedSuccessfully ? "Successful" : "NOT Wholly Successful";
          _this.WriteToActivityLog("Batch Update Run FINISH", sSummary, elapsedTimeKey, elapsedTimeValue,
            // Bug 16806 
            "Total amount", String.Format("{0:C}", _this.m_totalAmount),
            "Total count", _this.m_totalCount.ToString());
        }
        else
        {
          _this.WriteToActivityLog("Batch Update Run FINISH", "NOT Successful", elapsedTimeKey, elapsedTimeValue, "Error", "Batch Update Run was NOT started.");
        }
        //-------------------------------------------------------------

        if (_this.m_bRunBatchUpdateStarted)
        {
          //-------------------------------------------------------------
          // Set the "CASL flag" for a the success status of the run.
          if (_this.m_geoBatchUpdateApp != null)
          {
            _this.m_geoBatchUpdateApp.set("UPDATE_SUCCESSFUL", _this.m_bRunBatchUpdateCompletedSuccessfully);
          }
          //-------------------------------------------------------------

          //-------------------------------------------------------------
          // Let the project level close all connections at this point.
          _this.ProductLevel_PrjCleanup(ref bTerminationByUnexpectedError);
          //-------------------------------------------------------------

          //-------------------------------------------------------------
          // Allow project level to ROLL BACK if error(s) occurred.
          if (bTerminationByUnexpectedError)
          {
            // Unexpected error - rollback ALL system interfaces
            _this.ProductLevel_PrjRollback(ref bTerminationByUnexpectedError);
          }
          if (!_this.m_bRunBatchUpdateCompletedSuccessfully &&
              _this.m_pSelectedSysInterfaces != null &&
              _this.m_SysIntProcessAction != null)
          {
            int nSysIntCount = _this.m_pSelectedSysInterfaces.getLength();
            for (int i = 0; i < nSysIntCount; i++)
            {
              string sSysInt = _this.m_pSelectedSysInterfaces.get(i).ToString();
              if (_this.m_SysIntProcessAction.ContainsKey(sSysInt) &&
                  _this.m_SysIntProcessAction[sSysInt] == eWorking.TERMINATE) // BU process was terminated
              {
                bool bTermination = true;
                _this.ProductLevel_PrjRollbackSystem(sSysInt, ref bTermination);
              }
            }
          }
          //-------------------------------------------------------------

          //-------------------------------------------------------------
          // Progress bar and report update
          string sProgress = (string)c_CASL.GEO.get("BatchUpdateActivityProgress", "");
          if (!sProgress.StartsWith("end"))
          {
            c_CASL.GEO.set("BatchUpdateActivityProgress", "end");
          }
          //-------------------------------------------------------------
        }

        //-------------------------------------------------------------
        // Set instance (_this) of the BatchUpdate_ProductLevel to null
        // & decrement "BatchUpdate_ProductLevel" count (increments in constructor)
        _this = null;
        System.Threading.Interlocked.Decrement(ref m_BatchUpdateRunCount);
        //-------------------------------------------------------------
      }
    }

    /// <summary>
    /// Executes a Batch Update job for the selected System Interfaces and Core Files.
    /// -----------------------------------------------------------------------------
    /// Daniel wrote this function. Please do not change without talking to me first. ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!
    /// </summary>
    /// <param name="pArgs">Batch Update Parameters sent from CASL</param>
    /// <param name="_this">An instance of the BatchUpdate_ProductLevel class</param>
    /// <returns></returns>
    public static bool RunBatchUpdate_aux(ref GenericObject pArgs, ref BatchUpdate_ProductLevel _this)
    {
      string strError = "";
      string strDBConnectionString = (string)_this.m_pDBInfo.get("db_connection_string", "");
      bool bTerminateOrSkipAllSIs = false; // Indicates that ALL selected SIs (processes) are to be terminated or skipped

      // TEST UNCAUGHT ERROR HERE:
      //int test001 = 100; int test002 = 0; int test003 = test001 / test002;

      //---------------------------------------------------------------------------------------------------
      // Bug 9233 DH - Make sure other modules can see if batch update is running.
      if (c_CASL.GEO.has("BatchUpdateActivityProgress"))
      {
        c_CASL.GEO.remove("BatchUpdateActivityProgress");
      }
      //---------------------------------------------------------------------------------------------------

      //---------------------------------------------------------------------------------------------------
      // Set Update ID and core_file_ids to BU GEO (accessible in CASL)
      _this.m_geoBatchUpdateApp.set("UPDATE_ID", _this.m_strUpdateID); // SX 01/25/08
      if (!_this.m_geoBatchUpdateApp.has("core_file_ids"))
      {
        _this.m_geoBatchUpdateApp.set("core_file_ids", _this.m_pSelectedCoreFiles);
      }
      //---------------------------------------------------------------------------------------------------

      //---------------------------------------------------------------------------------------------------
      // Collect the transaction types (& taxes), and tender types (Bug 14210) that have system interfaces and
      // add them to the hashtables:
      _this.GetAllTransactionTypeSystemInterfaces();
      _this.GetAllTenderTypeSystemInterfaces();
      //---------------------------------------------------------------------------------------------------

      //---------------------------------------------------------------------------------------------------
      // Initialize the "m_CoreFileSysIntState" member variable.
      if (!_this.InitializeCoreFileSysIntState()) { return false; }
      //---------------------------------------------------------------------------------------------------

      //---------------------------------------------------------------------------------------------------
      // Initialize the "m_SysIntProcessAction" member variable.
      if (!_this.InitializeSysIntProcessAction()) { return false; }
      //---------------------------------------------------------------------------------------------------

      //---------------------------------------------------------------------------------------------------
      // Initialize the "m_pSysInterfaceProjectImplementations" member variable.
      if (!_this.InitializeSysIntProjectImplementations()) { return false; }
      //---------------------------------------------------------------------------------------------------

      //---------------------------------------------------------------------------------------------------
      // The following are used to update the progress bar:
      long lCurrentTotalEvent = 0;
      int iCntOfCoreFiles = _this.m_pSelectedCoreFiles.getIntKeyLength();
      string strAllEventCount = "";
      if (!_this.CountEventsForSelectedCoreFiles(ref iCntOfCoreFiles, ref strAllEventCount)) { return false; }
      //---------------------------------------------------------------------------------------------------

      //---------------------------------------------------------------------------------------------------
      // Set WORKING values to the TG_UPDATEFILE_DATA. The final SI status (e.g., COMPLETE) will be set later.
      if (!_this.ProductLevel_SetBatchUpdateFileStatus_Start(strDBConnectionString, ref strError)) { return false; }
      //---------------------------------------------------------------------------------------------------

      //---------------------------------------------------------------------------------------------------
      // Loop over all the selected CORE files and collect the info for one event at a time.
      for (int j = 0; j < iCntOfCoreFiles; j++)
      {
        //---------------------------------------------------------------------------------------------------
        // Check if ALL Batch Update System Interface processes are NOT to continue (terminated or skipped)
        bTerminateOrSkipAllSIs = _this.TerminateOrSkipAllSIs();
        if (bTerminateOrSkipAllSIs)
        {
          break;
        }
        //---------------------------------------------------------------------------------------------------

        //---------------------------------------------------------------------------------------------------
        // The following variables are used for CORE file (and event) processing:
        string strFileNbr = _this.ProductLevel_GetFileNumber((string)_this.m_pSelectedCoreFiles.get(j));
        string strFileSequenceNbr = _this.ProductLevel_GetFileSequence((string)_this.m_pSelectedCoreFiles.get(j));
        string strCurrentFile = string.Format("{0}{1}", strFileNbr, strFileSequenceNbr);
        long lFileNbr = ProductLevel_atoi(strFileNbr);
        long lFileSeqNbr = ProductLevel_atoi(strFileSequenceNbr);
        GenericObject pFileGEO = null; // Bug 8182 DH
        //---------------------------------------------------------------------------------------------------

        //---------------------------------------------------------------------------------------------------
        // Bug 8182 DH - Retrieve the file level info
        // IPAY-398: FT: Parameterize
        string strSqlDepFileData = "SELECT * FROM TG_DEPFILE_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq";
        GenericObject sql_args = new GenericObject();
        sql_args.Add("depfilenbr", (int) lFileNbr);
        sql_args.Add("depfileseq", (int) lFileSeqNbr);
        pFileGEO = (GenericObject)_this.ProductLevel_GetRecords(_this.m_pDBInfo, 1, strSqlDepFileData, _this.m_iDBType, sql_args);
        //---------------------------------------------------------------------------------------------------

        //---------------------------------------------------------------------------------------------------
        // Bug 12136 DH - Find which system interfaces need to process the whole file.
        _this.PopulateArrayForSysIntsThatProcessFullFiles(lFileNbr, lFileSeqNbr);
        //---------------------------------------------------------------------------------------------------

        //---------------------------------------------------------------------------------------------------
        // Notify each System that batch processing for the CORE file is about to begin (i.e., all CORE file 
        // events and deposits to be dispatched).
        if (!_this.ProductLevel_BeginCoreFile(strCurrentFile, pFileGEO))
        {
          // NOTE: False returned means an error occurred [corrective actions (Skip Sys Int or CORE file) already taken].
        }
        //---------------------------------------------------------------------------------------------------

        //---------------------------------------------------------------------------------------------------
        // Get the count for all events in the CORE file and then dispatch events for System Interface processing
        // IPAY-398: FT: Parameterize
        // FT: do we need a mutex here to protext the MAX?
        string strSqlEvent = "SELECT MAX(EVENTNBR) AS EVENT_COUNT FROM TG_PAYEVENT_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND STATUS != '1' AND STATUS != '3' AND STATUS != '4'";
        sql_args = new GenericObject();
        sql_args.Add("depfilenbr", (int)lFileNbr);
        sql_args.Add("depfileseq", (int)lFileSeqNbr);
        GenericObject pPayEventCount = (GenericObject)_this.ProductLevel_GetRecords(_this.m_pDBInfo, 1, strSqlEvent, _this.m_iDBType, sql_args);
        if (pPayEventCount != null)
        {
          _this.ProcessEventsInCoreFile(ref lCurrentTotalEvent, iCntOfCoreFiles, strAllEventCount, j, strCurrentFile, lFileNbr, lFileSeqNbr, ref pFileGEO, pPayEventCount);
        }
        //---------------------------------------------------------------------------------------------------

        //---------------------------------------------------------------------------------------------------
        // Dispatch deposits for this CORE file
        if (_this.m_bTestMode == false)
        {
          if (!_this.ProductLevel_DispatchDeposits(strFileNbr, strFileSequenceNbr, strCurrentFile, pFileGEO/*Bug 8332 DH*/))
          {
            // Currently always returns TRUE
          }
        }
        //---------------------------------------------------------------------------------------------------

        //---------------------------------------------------------------------------------------------------
        // Notify each System that batch processing for the CORE file is complete (i.e., all events/deposits
        // dispatched).  System implementation MAY return count/amount totals for the CORE file.
        if (!_this.ProductLevel_EndCoreFile(strCurrentFile, pFileGEO))
        {
          // NOTE: False returned means an error occurred [corrective actions (Skip Sys Int or CORE file) already taken].
        }
        //---------------------------------------------------------------------------------------------------
      }

      //---------------------------------------------------------------------------------------------------
      // Need to UPDATE SYS INT status (possibly to TERMINATED) because all Core Files may have had errors
      _this.UpdateSysIntProcessAction();
      bTerminateOrSkipAllSIs = _this.TerminateOrSkipAllSIs();
      //---------------------------------------------------------------------------------------------------

      //---------------------------------------------------------------------------------------------------
      // ProductLevel_EndingBatchUpdate_WriteTrailers and ProductLevel_EndingBatchUpdate_SendFiles:
      // End of all events in all files for all system interfaces. Perform the last project actions which is to close all
      // connections and write any potential trailer records, send files, etc.
      // Removed referenced "bEarlyTerminationRequestedByProject" argument from "ProductLevel_EndingBatchUpdate_WriteTrailers"
      // method.  The termination flags are now maintained at the System Interface level (i.e., there is no longer a single
      // terminate action for ALL System Interfaces (BU Processes) that are selected).
      if (!bTerminateOrSkipAllSIs)
      {
        _this.ProductLevel_EndingBatchUpdate_WriteTrailers();
        _this.ProductLevel_EndingBatchUpdate_SendFiles();
      }
      //---------------------------------------------------------------------------------------------------

      //---------------------------------------------------------------------------------------------------
      // When a Batch Update job is finished there are three tables in the DB that need to have records updated or inserted:
      if (!_this.ProductLevel_WriteToDB_BatchUpdateJobFinished(strDBConnectionString, ref strError))
      {
        _this.m_bRunBatchUpdateCompletedSuccessfully = false;
        try
        {
          string sEmailReceiver = _this.m_strEmailReceiver.Trim();
          if (string.IsNullOrEmpty(sEmailReceiver))
          {
            sEmailReceiver = "DEFAULT";
            _this.LogBatchUpdateError("Configuration Error: There is no value for the Batchupdate Email Receiver (General Info - Misc).");
          }
          GenericObject geoErr = new GenericObject("_parent", "GEO.error", "code", "BU0010", "message", strError);
          emails.CASL_send_email_notification(
            "module", "iPayment Batch Update Module",
            "action", "Running Batch Update",
            "email_subject", "ERROR Writing to Database When Batch Update Finished",
            "email_receiver", sEmailReceiver,
            "error", geoErr);
        }
        catch (Exception ex)
        {
          _this.LogBatchUpdateError(ex, string.Format("Exception sending email to {0} when BU finished.", _this.m_strEmailReceiver));
        }
      }
      //---------------------------------------------------------------------------------------------------

      return true;
    }

    /// <summary>
    /// This method will set the values for various member variables.  Most values are retrieved from
    /// the arguments (pArgs) sent from the CASL method.
    /// </summary>
    /// <param name="pArgs"></param>
    private bool InitializeMemberVariables(GenericObject pArgs)
    {
      try
      {
        m_dtStartDateTime = DateTime.Now;
        m_strUserName = (string)pArgs.get("user_name", "");
        m_pDBInfo = (GenericObject)pArgs.get("db_data");
        m_iDBType = ProductLevel_GetDBConnectionType(m_pDBInfo);
        m_strBatchJobComments = ((string)pArgs.get("comment", "")).Trim();
        m_bAutoBatchJob = (m_strBatchJobComments == "AUTOMATED");
        // Forces the processing of System/Core File items already completed
        object objForce = pArgs.get("force_batch_job", false);
        m_bForceBatchJob = objForce.GetType() == typeof(System.String) ? (bool)(((string)objForce) == "true") : (bool)objForce;
        m_strEmailReceiver = (c_CASL.c_object("Business.Misc.data") as GenericObject).get("batchupdate_email_receiver", "").ToString();
        m_pSelectedCoreFiles = (GenericObject)pArgs.get("corefiles");
        m_pSelectedSysInterfaces = (GenericObject)pArgs.get("system_interface_list");
        m_geoBatchUpdateApp = (GenericObject)pArgs.get("_subject"); //SX 01/25/08 use to get UPDATE_ID and generate the system interface report. 
        m_SysIntProcessAction = new Dictionary<string, eWorking>();
        m_CoreFileSysInterfaceState = new Dictionary<string, CoreFileData>();
        m_pSysInterfaceErrorsList = new ArrayList(); // Bug 8224 DH
        m_arrSysIntsThatProcessFullFiles = new ArrayList(); // Bug 12136 DH - PB needs the whole file.
        m_strUpdateID = ProductLevel_CreateUpdateID(); // Generate the Update ID for this BU Job
      }
      catch (Exception ex)
      {
        // Log detailed error in cs log file
        LogBatchUpdateError(ex, "Error occurred while populating various member variables");
        return false;
      }
      return true;
    }

    /// <summary>
    /// This method will query the DB (TG_UPDATEFILE_DATA table) and populate the member variable
    /// "m_CoreFileSysIntState" with the CORE file data and its corresponding list of system
    /// interfaces that have a COMPLETE or WORKING status.
    /// NOTE: Each Core File will have a list of ALL BU SIs (not just those that are selected for this BU Run).
    /// </summary>
    /// <returns>true if successful; false if error</returns>
    private bool InitializeCoreFileSysIntState()
    {
      const string SQL_STR = "SELECT DISTINCT INTERFACE_ID AS SI_ID_STATUS FROM TG_UPDATEFILE_DATA WHERE (DEPFILENBR=@depfilenbr) AND (DEPFILESEQ=@depfileseq) AND (STATUS=@status)";
      try
      {
        // Get the list of ALL Batch Update system interfaces (i.e., not just those currently selected)
        List<string> SysIntNameList = null;
        GetAllBatchUpdateSysIntNames(ref SysIntNameList);

        int iCountOfCoreFiles = m_pSelectedCoreFiles.getIntKeyLength();
        for (int j = 0; j < iCountOfCoreFiles; j++)
        {
          string strFileNbr = ProductLevel_GetFileNumber((string)m_pSelectedCoreFiles.get(j));
          string strFileSeqNbr = ProductLevel_GetFileSequence((string)m_pSelectedCoreFiles.get(j));
          long lFileNbr = ProductLevel_atoi(strFileNbr);
          long lFileSeqNbr = ProductLevel_atoi(strFileSeqNbr);
          Dictionary<string, ProcessStateForCoreFile> SysIntList_State_Dictionary = new Dictionary<string, ProcessStateForCoreFile>();

          // Get the COMPLETE SIs from the DB for this CORE file:
          // IPAY-398: FT: Parameterize batch update queries
          GenericObject sql_args = new GenericObject();
          sql_args.Add("depfilenbr", (int)lFileNbr);
          sql_args.Add("depfileseq", (int)lFileSeqNbr);
          sql_args.Add("status", eStatusDB.COMPLETE.ToString());
          GenericObject pCompleteSysInts = (GenericObject)ProductLevel_GetRecords(m_pDBInfo, 1, SQL_STR, m_iDBType, sql_args);
          if (pCompleteSysInts != null)
          {
            int lCnt = pCompleteSysInts.getLength();
            for (int lSysInt = 0; lSysInt < lCnt; lSysInt++)
            {
              GenericObject aSysInt = (GenericObject)pCompleteSysInts.get(lSysInt, null);
              if (aSysInt != null)
              {
                string strSysInt = (string)aSysInt.get("SI_ID_STATUS");
                SysIntList_State_Dictionary.Add(strSysInt, new ProcessStateForCoreFile(eStatusDB.COMPLETE));
              }
            }
          }

          // Get any WORKING SIs from the DB  for this CORE file (that are NOT COMPLETE):
          // IPAY-398: FT: Parameterize batch update queries
          sql_args = new GenericObject();
          sql_args.Add("depfilenbr", (int)lFileNbr);
          sql_args.Add("depfileseq", (int)lFileSeqNbr);
          sql_args.Add("status", eStatusDB.WORKING.ToString());
          GenericObject pWorkingSysInts = (GenericObject)ProductLevel_GetRecords(m_pDBInfo, 1, SQL_STR, m_iDBType, sql_args);
          if (pWorkingSysInts != null)
          {
            int lCnt = pWorkingSysInts.getLength();
            for (int lSysInt = 0; lSysInt < lCnt; lSysInt++)
            {
              GenericObject aSysInt = (GenericObject)pWorkingSysInts.get(lSysInt, null);
              if (aSysInt != null)
              {
                string strSysInt = (string)aSysInt.get("SI_ID_STATUS");
                if (!SysIntList_State_Dictionary.ContainsKey(strSysInt))
                {
                  SysIntList_State_Dictionary.Add(strSysInt, new ProcessStateForCoreFile(eStatusDB.WORKING));
                }
              }
            }
          }

          // Add the BU SI's that are NOT COMPLETE (or WORKING):
          foreach (string strSysInt in SysIntNameList)
          {
            if (!SysIntList_State_Dictionary.ContainsKey(strSysInt))
            {
              SysIntList_State_Dictionary.Add(strSysInt, new ProcessStateForCoreFile(false));
            }
          }

          // Add the CORE file data and System Interface states to m_CoreFileSysIntState:
          CoreFileData FData = new CoreFileData(m_strUpdateID, strFileNbr, strFileSeqNbr, SysIntList_State_Dictionary);
          m_CoreFileSysInterfaceState.Add((strFileNbr + strFileSeqNbr), FData);
        }
      }
      catch (Exception ex)
      {
        // Log detailed error in cs log file
        LogBatchUpdateError(ex, "Error occurred while populating the member variable m_CoreFileSysIntState");
        return false;
      }
      return true;
    }

    /// <summary>
    /// Initialize all BU Processes (SIs) Action values:
    ///   Set to Working.SKIP if SI is Complete or Working for all selected Core files (and the BU is NOT forced)
    ///   Set to Working.CONTINUE otherwise.
    /// NOTE: This method is called PRIOR to processing CORE files.
    /// </summary>
    /// <param name="_this"></param>
    /// <returns>true if successful; false if error</returns>
    private bool InitializeSysIntProcessAction()
    {
      try
      {
        for (int i = 0; i < m_pSelectedSysInterfaces.getLength(); i++)
        {
          eWorking eAction = eWorking.CONTINUE;
          string sysIntName = (string)m_pSelectedSysInterfaces.get(i);
          if (!m_bForceBatchJob && ProcessCompleteOrWorkingForAllSelectedCoreFiles(sysIntName))
          {
            eAction = eWorking.SKIP;
          }
          m_SysIntProcessAction.Add(sysIntName, eAction);
        }
      }
      catch (Exception ex)
      {
        // Log detailed error in cs log file
        LogBatchUpdateError(ex, "Error occurred while populating the member variable m_SysIntProcessAction");
        return false;
      }
      return true;
    }

    /// <summary>
    /// This method will loop though all events in a CORE file collecting data (transactions, tenders, over/shorts,
    /// allocations, etc...) and dispatch the event data to the SI instances.  It will also update the BU progress
    /// activity display.
    /// </summary>
    /// <param name="lCurrentTotalEvent"></param>
    /// <param name="iCntOfCoreFiles"></param>
    /// <param name="strAllEventCount"></param>
    /// <param name="iCoreFileIndex"></param>
    /// <param name="strCurrentFile"></param>
    /// <param name="lFileNbr"></param>
    /// <param name="lFileSeqNbr"></param>
    /// <param name="pFileGEO"></param>
    /// <param name="fData"></param>
    /// <param name="pPayEventCount"></param>
    private void ProcessEventsInCoreFile(ref long lCurrentTotalEvent, int iCntOfCoreFiles, string strAllEventCount, int iCoreFileIndex, string strCurrentFile, long lFileNbr, long lFileSeqNbr, ref GenericObject pFileGEO, /*ref FileData fData,*/ GenericObject pPayEventCount)
    {
      string strCurrentTotalEvent = "";
      long lEventCount = 0;
      GenericObject pEventGEO = null;
      GenericObject pTempGEO = null;
      bool bOverShortDataEvent = false; // Bug 8332 DH

      pTempGEO = (GenericObject)pPayEventCount.get(0, null);
      if (pTempGEO != null)
      {
        lEventCount = pTempGEO.has("EVENT_COUNT") ? ProductLevel_atoi(pTempGEO.get("EVENT_COUNT").ToString()) : 0;
      }

      for (long lCurrentEventID = 1; lCurrentEventID < lEventCount + 1; lCurrentEventID++)
      {
        lCurrentTotalEvent++;
        strCurrentTotalEvent = lCurrentTotalEvent.ToString();

        pEventGEO = null;
        // Bug 13275 QG Get Event info
        // IPAY-398: FT: Parameterize batch update queries
        string strSqlPayEventData = @"
          SELECT 
            EVENTNBR, DEPFILENBR, DEPFILESEQ, USERID, STATUS, MOD_DT, SOURCE_TYPE, SOURCE_DATE, CREATION_DT 
          FROM TG_PAYEVENT_DATA 
            WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND EVENTNBR=@eventnbr";
        GenericObject sql_args = new GenericObject();
        sql_args.Add("depfilenbr", (int)lFileNbr);
        sql_args.Add("depfileseq", (int)lFileSeqNbr);
        sql_args.Add("eventnbr", (int)lCurrentEventID);
        pEventGEO = (GenericObject)ProductLevel_GetRecords(m_pDBInfo, 1, strSqlPayEventData, m_iDBType, sql_args);
        pEventGEO = pEventGEO.get(0, null) as GenericObject;
        // END OF Bug13275             
        bOverShortDataEvent = false; // Bug 8332 DH

        UpdateProgressBar(iCntOfCoreFiles, strAllEventCount, strCurrentTotalEvent, (iCoreFileIndex + 1), lEventCount, lCurrentEventID);

        // Get transactions for the event
        // IPAY-398: FT: Parameterize batch update queries
        string strSqlEventTran = "SELECT * FROM TG_TRAN_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND EVENTNBR=@eventnbr AND VOIDDT IS NULL";
        sql_args = new GenericObject();
        sql_args.Add("depfilenbr", (int)lFileNbr);
        sql_args.Add("depfileseq", (int)lFileSeqNbr);
        sql_args.Add("eventnbr", (int)lCurrentEventID);
        GenericObject pEventTrans = (GenericObject)ProductLevel_GetRecords(m_pDBInfo, 1, strSqlEventTran, m_iDBType, sql_args);
        if (pEventTrans != null)
        {
          //----------------------------------------------------------------------
          // Add the allocations & custom fields for each transaction
          int lCnt = pEventTrans.getLength();
          for (int lCurrentTran = 0; lCurrentTran < lCnt; lCurrentTran++)
          {
            if (pEventTrans.has(lCurrentTran))
            {
              GenericObject aTran = (GenericObject)pEventTrans.get(lCurrentTran);
              int iTranNbr = (int)aTran.get("TRANNBR");

              UpdateProgressBar(iCntOfCoreFiles, strAllEventCount, strCurrentTotalEvent, (iCoreFileIndex + 1), lEventCount, lCurrentEventID);

              if (m_bTestMode == false)
              {
                //-------------------------
                // Allocations
                string strSqlTranAllocations = "SELECT * FROM TG_ITEM_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND EVENTNBR=@eventnbr AND TRANNBR=@trannbr";
                // IPAY-398: FT: Parameterize batch update queries
                sql_args = new GenericObject();
                sql_args.Add("depfilenbr", (int)lFileNbr);
                sql_args.Add("depfileseq", (int)lFileSeqNbr);
                sql_args.Add("eventnbr", (int)lCurrentEventID);
                sql_args.Add("trannbr", iTranNbr);
                GenericObject pTranAllocations = (GenericObject)ProductLevel_GetRecords(m_pDBInfo, 1, strSqlTranAllocations, m_iDBType, sql_args);
                pTempGEO = null;
                if (pTranAllocations != null)
                {
                  pTempGEO = (GenericObject)pEventTrans.get(lCurrentTran);// Current tran
                  pTempGEO.Add("TG_ITEM_DATA", pTranAllocations);
                }
                //-------------------------

                //-------------------------
                // Custom fields
                // IPAY-398: FT: Parameterize batch update queries
                string strSqlTranCustomFields = "SELECT * FROM GR_CUST_FIELD_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfilenbr AND EVENTNBR=@eventnbr AND TRANNBR=@trannbr";
                sql_args = new GenericObject();
                sql_args.Add("depfilenbr", (int)lFileNbr);
                sql_args.Add("depfileseq", (int)lFileSeqNbr);
                sql_args.Add("eventnbr", (int)lCurrentEventID);
                sql_args.Add("trannbr", iTranNbr);
                GenericObject pTranCustomFields = (GenericObject)ProductLevel_GetRecords(m_pDBInfo, 1, strSqlTranCustomFields, m_iDBType, sql_args);
                if (pTranCustomFields != null)
                {
                  if (pTempGEO == null)// Current tran maybe already allocated
                    pTempGEO = (GenericObject)pEventTrans.get(lCurrentTran);
                  pTempGEO.Add("GR_CUST_FIELD_DATA", pTranCustomFields);
                }
                //-------------------------

                // Now replace it if we have allocations or custom fields
                if (pTempGEO != null)
                  pEventTrans[lCurrentTran] = pTempGEO;
              }
            }
          }
          //----------------------------------------------------------------------
          pEventGEO.Add("TRANSACTIONS", pEventTrans);
        }

        //------------------------------------------------------------------------
        // Get tenders for the event
        if (m_bTestMode == false)
        {
          // IPAY-398: Parameterize with the correct type (int rather than long)
          string strSqlEventTenders = "SELECT * FROM TG_TENDER_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND EVENTNBR=@eventnbr AND VOIDDT IS NULL";
          sql_args = new GenericObject();
          sql_args.Add("depfilenbr", (int) lFileNbr);
          sql_args.Add("depfileseq", (int) lFileSeqNbr);
          sql_args.Add("eventnbr", (int) lCurrentEventID);
          GenericObject pEventTenders = (GenericObject)ProductLevel_GetRecords(m_pDBInfo, 1, strSqlEventTenders, m_iDBType, sql_args);
          if (pEventTenders != null)
          {
            int lCnt = pEventTenders.getLength();
            for (int lCurrentTender = 0; lCurrentTender < lCnt; lCurrentTender++)
            {
              GenericObject aTender = (GenericObject)pEventTenders.get(lCurrentTender);

              int iTenderNbr = (int)aTender.get("TNDRNBR");

              //------------------------------------------------------
              // Bug 8332 DH
              if (aTender.get("TNDRID").ToString() == "SYSOVERSHORT")
                bOverShortDataEvent = true;
              //------------------------------------------------------

              UpdateProgressBar(iCntOfCoreFiles, strAllEventCount, strCurrentTotalEvent, (iCoreFileIndex + 1), lEventCount, lCurrentEventID);

              //--------------------------------------
              // Add all tender custom fields
              // IPAY-398: Parameterize with the correct type (int rather than long)
              string strSqlTenderCustomFields = "SELECT * FROM CUST_FIELD_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND EVENTNBR=@eventnbr AND POSTNBR=@postnbr";
              sql_args = new GenericObject();
              sql_args.Add("depfilenbr", (int) lFileNbr);
              sql_args.Add("depfileseq", (int) lFileSeqNbr);
              sql_args.Add("eventnbr", (int) lCurrentEventID);
              sql_args["postnbr"] = iTenderNbr;
              GenericObject pTenderCustomFields = (GenericObject)ProductLevel_GetRecords(m_pDBInfo, 1, strSqlTenderCustomFields, m_iDBType, sql_args);

              if (pTenderCustomFields != null)
              {
                pTempGEO = (GenericObject)pEventTenders.get(lCurrentTender);
                pTempGEO.Add("CUST_FIELD_DATA", pTenderCustomFields);
                pEventTenders[lCurrentTender] = pTempGEO;
              }
              //------------------------------------------------------------------------
            }
            pEventGEO.Add("TENDERS", pEventTenders);
          }
        }

        //***************************************************************************
        // Send all event data for processing. System interfaces are in a global GEO.
        if (ProductLevel_DispatchEvent(ref pFileGEO/*Bug 8182 DH*/, ref pEventGEO, strCurrentFile) == false)
        {   // NOTE: False returned will now mean an error occurred - NOT an early "termination"
          break; // Stop processing the rest of the events in this CORE file.
        }
        //***************************************************************************

        //***************************************************************************
        // Bug 8332 DH
        // If this is an over/short event then add it to pFileGEO so it can be accessible
        // from ProductLevel_DispatchDeposits. There should be only one over/short event.
        // NOTE: For some reason I encountered file with multiple over/short events so I
        // had to add "_EVENTNBR_" and event nbr to separate them.
        if (bOverShortDataEvent)
        {
          pFileGEO.Add("SYSOVERSHORT" + "_EVENTNBR_" + lCurrentEventID.ToString(), pEventGEO);
        }
        bOverShortDataEvent = false;
        //***************************************************************************
      }
    }

    /// <summary>
    /// Check if ALL Batch Update processes are NOT to continue (terminated or skipped).
    /// </summary>
    /// <param name="_this"></param>
    /// <returns>True if no system has "CONTINUE" action; false otherwise</returns>
    private bool TerminateOrSkipAllSIs()
    {
      bool bTerminateOrSkipAllSIs = true;
      foreach (KeyValuePair<string, eWorking> entry in m_SysIntProcessAction)
      {
        if (entry.Value == eWorking.CONTINUE)
        {
          bTerminateOrSkipAllSIs = false;
          break;
        }
      }
      return bTerminateOrSkipAllSIs;
    }

    /// <summary>
    /// This method checks to see if a BU process (i.e., System Interface) has a COMPLETE or WORKING status
    /// for ALL the selected CORE files.  Returns true if all are COMPLETE or WORKING; false otherwise.
    /// </summary>
    /// <param name="SysIntName"></param>
    /// <returns></returns>
    private bool ProcessCompleteOrWorkingForAllSelectedCoreFiles(string SysIntName)
    {
      bool bAllCompleteOrWorking = true;
      int iCountOfCoreFiles = m_pSelectedCoreFiles.getIntKeyLength();
      for (int j = 0; j < iCountOfCoreFiles; j++)
      {
        string strFileNbr = ProductLevel_GetFileNumber((string)m_pSelectedCoreFiles.get(j));
        string strFileSequenceNbr = ProductLevel_GetFileSequence((string)m_pSelectedCoreFiles.get(j));
        if (!m_CoreFileSysInterfaceState[strFileNbr + strFileSequenceNbr].pSysIntState[SysIntName].bCompleteStatusInDB)
        {
          if (!m_CoreFileSysInterfaceState[strFileNbr + strFileSequenceNbr].pSysIntState[SysIntName].bWorkingStatusInDB)
          {
            bAllCompleteOrWorking = false;
            break;
          }
        }
      }
      return bAllCompleteOrWorking;
    }

    /// <summary>
    /// Need to update the system interface process action (possibly to TERMINATE) because all selected Core
    /// Files may have had an error and are now in a "skipped" state.
    /// NOTE: This method is called PRIOR to calling ProductLevel_EndingBatchUpdate_WriteTrailers; after the 
    /// selected CORE files (events & deposits) are finished processing.
    /// </summary>
    private void UpdateSysIntProcessAction()
    {
      for (int i = 0; i < m_pSelectedSysInterfaces.getLength(); i++)
      {
        bool bHasCoreFileError = false;
        bool bChangeActionToTerminate = false;
        string sSysInt = (string)m_pSelectedSysInterfaces.get(i);
        if (m_SysIntProcessAction[sSysInt] == eWorking.CONTINUE)
        {
          bChangeActionToTerminate = true;
          int iCountOfCoreFiles = m_pSelectedCoreFiles.getIntKeyLength();
          for (int j = 0; j < iCountOfCoreFiles; j++)
          {
            string sNbr = ProductLevel_GetFileNumber((string)m_pSelectedCoreFiles.get(j));
            string sSeq = ProductLevel_GetFileSequence((string)m_pSelectedCoreFiles.get(j));
            string sCoreFile = string.Format("{0}{1}", sNbr, sSeq);
            // Check if ALL the selected CORE files were skipped (due to error, completion, or working state)
            if (SkipProcessing(sSysInt, sCoreFile))
            {
              if (m_CoreFileSysInterfaceState[sCoreFile].pSysIntState[sSysInt].bSkipCoreFile_Error)
              {
                bHasCoreFileError = true;
              }
            }
            else
            {
              bChangeActionToTerminate = false;
              break;
            }
          }
          if (bChangeActionToTerminate && bHasCoreFileError)
          {
            // All CORE files were skipped (one or more due to Errors during processing) so set the SI to TERMINATE
            m_SysIntProcessAction[sSysInt] = eWorking.TERMINATE;
          }
        }
      }
    }

    /// <summary>
    /// Initialize all BU Project Implementations [Instances of Derived Classes (Abstract BU Base Class)]:
    /// Add pointers to the BU system interfaces so they can be called for: Processing events, Writing
    /// trailers, and other communication between this class and the project level system interfaces
    /// implementations.
    /// Bug 12126 Mike O - Rewrote to use the abstract class, and so projects wouldn't have to modify it
    /// Hashtable: 
    ///   key:   "system interface name"
    ///   value: Project level system interface class implementation pointer (abstract class)
    /// </summary>
    /// <param name="_this"></param>
    /// <returns>true if successful; false if error</returns>
    private bool InitializeSysIntProjectImplementations()
    {
      try
      {
        if (m_pSysInterfaceProjectImplementations == null)
        {
          m_pSysInterfaceProjectImplementations = new Hashtable();
        }

        // Bug 12126 Mike O - Use type as key instead of configured name, and put the GEO in as the value
        // Map types of system interfaces to actual configured system interface names
        Dictionary<string, List<string>> caslSysIntTypeToNames = new Dictionary<string, List<string>>();
        for (int i = 0; i < m_pSelectedSysInterfaces.getLength(); i++)
        {
          string systemInterfaceName = (string)m_pSelectedSysInterfaces.get(i);
          GenericObject sysIntGEO = (GenericObject)c_CASL.execute("System_interface.of." + systemInterfaceName);
          m_pSysInterfaceProjectImplementations.Add(systemInterfaceName, sysIntGEO); // Add user selected system interfaces

          string sysIntType = (string)((GenericObject)sysIntGEO.get("_parent")).get("_name");
          if (caslSysIntTypeToNames.ContainsKey(sysIntType))
          {
            caslSysIntTypeToNames[sysIntType].Add(systemInterfaceName);
          }
          else
          {
            List<string> newList = new List<string>();
            newList.Add(systemInterfaceName);
            caslSysIntTypeToNames[sysIntType] = newList;
          }
        }

        var type = typeof(AbstractBatchUpdate);
        var types = BuildManager.GetReferencedAssemblies().Cast<Assembly>().ToList()
        // Bug 25649 MJO - Ignore exceptions
        .SelectMany(new Func<Assembly,IEnumerable<Type>>(s => {
          try
          {
            return s.GetTypes();
          }
          catch (Exception)
          {
            return new List<Type>();
          }
        }))
        .Where(p => type.IsAssignableFrom(p) && p != type);
        foreach (Type t in types)
        {
          AbstractBatchUpdate tempInstance = (AbstractBatchUpdate)Activator.CreateInstance(t);
          string caslSysIntTypeName = tempInstance.GetCASLTypeName();
          List<string> caslSysIntNamesForType = null;
          if (caslSysIntTypeToNames.TryGetValue(caslSysIntTypeName, out caslSysIntNamesForType))
          {
            foreach (string caslSysIntName in caslSysIntNamesForType)
            {
              AbstractBatchUpdate instance =
                  tempInstance.GetInstance((GenericObject)m_pSysInterfaceProjectImplementations[caslSysIntName]);
              m_pSysInterfaceProjectImplementations[caslSysIntName] = instance;
            }
          }
        }
      }
      catch (Exception ex)
      {
        // Log detailed error in cs log file
        LogBatchUpdateError(ex, "Error occurred while populating the member variable m_pSysInterfaceProjectImplementations");
        return false;
      }
      return true;
    }

    /// <summary>
    /// Count all events for all files for the progress bar. This is very slow but
    /// we need it. Hopefully SQL Server will cash the SQL statements and speed it
    /// up later on in this function, so we can get some of the performance loss regained.
    /// </summary>
    /// <param name="iCntOfCoreFiles"></param>
    /// <returns></returns>
    private bool CountEventsForSelectedCoreFiles(ref int iCntOfCoreFiles, ref string strAllEventCount)
    {
      long lTotalEventCount = 0;
      try
      {
        for (int j = 0; j < iCntOfCoreFiles; j++)
        {
          string strFileNbr = ProductLevel_GetFileNumber((string)m_pSelectedCoreFiles.get(j));
          string strFileSequenceNbr = ProductLevel_GetFileSequence((string)m_pSelectedCoreFiles.get(j));
          long lFileNbr = ProductLevel_atoi(strFileNbr);
          long lFileSeqNbr = ProductLevel_atoi(strFileSequenceNbr);

          // IPAY-398: FT: Parameterize 
          // FT: No mutex?  Probably too expensive
          string strSqlTotalEvents = "SELECT MAX(EVENTNBR) AS EVENT_COUNT FROM TG_PAYEVENT_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=depfileseq AND STATUS != '1' AND STATUS != '3' AND STATUS != '4'";
          GenericObject sql_args = new GenericObject();
          sql_args.Add("depfilenbr", (int)lFileNbr);
          sql_args.Add("depfileseq", (int)lFileSeqNbr);
          GenericObject pPayEventCount = (GenericObject)ProductLevel_GetRecords(m_pDBInfo, 1, strSqlTotalEvents, m_iDBType, sql_args);
          if (pPayEventCount != null)
          {
            GenericObject pTempGEO = (GenericObject)pPayEventCount.get(0);
            if (pTempGEO.has("EVENT_COUNT"))
              lTotalEventCount += ProductLevel_atoi(pTempGEO.get("EVENT_COUNT").ToString());
          }
        }
        strAllEventCount = lTotalEventCount.ToString();

      }
      catch (Exception ex)
      {
        LogBatchUpdateError(ex); // Log detailed error in cs log file
        return false;
      }
      return true;
    }

    /// <summary>
    /// The GetAllBatchUpdateSysIntNames method will populate the SysIntNameList with the SI
    /// names of ALL batch update processes.  Currently, batch update processes are identified
    /// by the CASL method 'update_core_file' defined in the System Interface.
    /// </summary>
    /// <param name="SysIntNameList"></param>
    private static void GetAllBatchUpdateSysIntNames(ref List<string> SysIntNameList)
    {
      if (SysIntNameList == null)
      {
        SysIntNameList = new List<string>();
      }
      SysIntNameList.Clear();
      GenericObject geoAllSysInt = (GenericObject)c_CASL.c_object("System_interface.of");
      ArrayList listSysInt = geoAllSysInt.getStringKeys();
      foreach (string si in listSysInt)
      {
        GenericObject geoSysInt = (GenericObject)geoAllSysInt.get(si, null);
        if (geoSysInt != null && geoSysInt.has("_parent"))
        {
          GenericObject geoSIParent = (GenericObject)geoSysInt.get("_parent");
          if (geoSIParent.has("update_core_file"))
          {
            SysIntNameList.Add(si);
          }
        }
      }
    }

    /// <summary>
    /// This will update the contents of the Progress Bar.
    /// </summary>
    /// <param name="iTotalCountOfCoreFiles"></param>
    /// <param name="strAllEventCount"></param>
    /// <param name="strCurrentTotalEvent"></param>
    /// <param name="iCountOfProcessingCoreFile"></param>
    /// <param name="lEventCount"></param>
    /// <param name="lCurrentEventID"></param>
    private static void UpdateProgressBar(int iTotalCountOfCoreFiles, string strAllEventCount, string strCurrentTotalEvent, int iCountOfProcessingCoreFile, long lEventCount, long lCurrentEventID)
    {
      // Update progress bar.
      string strProgressData = string.Format("File Count <font color=\"#347C17\"><b>{0}</b></font> of <b>{1}</b> <br/>Event Count <font color=\"#347C17\"><b>{2}</b></font> of <b>{3}</b><br/><br/>TOTAL EVENTS <font color=\"#347C17\"><b>{4}</b></font> of <b>{5}</b>",
      iCountOfProcessingCoreFile.ToString(),
      iTotalCountOfCoreFiles.ToString(),
      lCurrentEventID.ToString(),
      lEventCount.ToString(),
      strCurrentTotalEvent,
      strAllEventCount);
      c_CASL.GEO.set("BatchUpdateActivityProgress", " " + strProgressData);
    }

    /// <summary>
    /// Bug 12136 - Populate an array (m_arrSystemInterfacesThatProcessFullFiles) with the system interfaces
    /// that need to process the whole file (for a specific CORE file).
    /// </summary>
    /// <param name="lFileNbr">CORE file number</param>
    /// <param name="lFileSeqNbr">CORE file sequence number</param>
    private void PopulateArrayForSysIntsThatProcessFullFiles(long lFileNbr, long lFileSeqNbr)
    {
      m_arrSysIntsThatProcessFullFiles.Clear();
      // IPAY-398: FT: Parameterize batch update queries
      string strSqlAllTrans = "SELECT DISTINCT TTID FROM TG_TRAN_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND VOIDDT IS NULL";
      GenericObject sql_args = new GenericObject();
      sql_args.Add("depfilenbr", (int)lFileNbr);
      sql_args.Add("depfileseq", (int)lFileSeqNbr);
      GenericObject pTTIDs = (GenericObject)ProductLevel_GetRecords(m_pDBInfo, 1, strSqlAllTrans, m_iDBType, sql_args);
      if (pTTIDs != null)
      {
        int iCnt = pTTIDs.getLength();
        for (int lPos = 0; lPos < iCnt; lPos++)
        {
          GenericObject aTTID = (GenericObject)pTTIDs.get(lPos);
          string strTTID = aTTID.get("TTID").ToString();
          if (m_pAllTransactionTypesWithSysInterfaces.Contains(strTTID))
          {
            GenericObject pInterfaceList = (GenericObject)m_pAllTransactionTypesWithSysInterfaces[strTTID];
            int iInterfaceCnt = pInterfaceList == null ? 0 : pInterfaceList.getLength();
            for (int x = 0; x < iInterfaceCnt; x++)
            {
              GenericObject pCurrentSysInterface = (GenericObject)pInterfaceList.get(x);
              if (!m_arrSysIntsThatProcessFullFiles.Contains(pCurrentSysInterface))
              {
                if ((bool)(pCurrentSysInterface.get("process_everything_in_file", false)))
                {
                  m_arrSysIntsThatProcessFullFiles.Add(pCurrentSysInterface);
                }
              }
            }
          }
        }
        pTTIDs = null;
      }
    }

    /// <summary>
    /// Connects to the SqlServer database and updates or inserts
    /// records based on parameterized SQL statement argument.
    /// Returns true if successful; false otherwise (DB error).
    /// </summary>
    /// <param name="sDBConn"></param>
    /// <param name="sError"></param>
    /// <param name="sSQL"></param>
    /// <param name="paramList"></param>
    /// <returns></returns>
    private bool ConnectToSqlSvrDBAndExecuteNonQuery(string sDBConn, ref string sError, string sSQL, params SqlParameter[] paramList)
    {
      // If there is no SQL statement received then just return true
      if (string.IsNullOrEmpty(sSQL.Trim()))
      {
        return true;
      }

      // Sql Server
      SqlConnection pSqlConn = null;
      SqlTransaction pSqlTran = null;
      SqlCommand pSqlCmd = null;

      //----------------------------------------------
      // Try to connect for status update.
      try
      {
        pSqlCmd = new SqlCommand();
        pSqlConn = new SqlConnection(sDBConn);
        pSqlConn.Open();
        pSqlCmd.Connection = pSqlConn;
        pSqlCmd.CommandText = sSQL;

        for (int k = 0; k < paramList.Length; k++)
        {
          pSqlCmd.Parameters.Add(paramList[k]);
        }

        pSqlTran = pSqlConn.BeginTransaction(IsolationLevel.ReadCommitted);
        pSqlCmd.Transaction = pSqlTran;
        pSqlCmd.ExecuteNonQuery();
        pSqlTran.Commit();
      }
      catch (Exception ex)
      {
        LogBatchUpdateError(ex); // Log detailed error in cs log file
        sError = ex.Message;
        if (pSqlTran != null)
        {
          pSqlTran.Rollback(); // Return to previous state.
        }
        return false;
      }
      finally // Cleanup: Added "finally" block for the close connection calls.
      {
        if (pSqlConn != null)
        {
          pSqlConn.Close();
        }
      }
      //----------------------------------------------
      return true;
    }

    /// <summary>
    /// Connects to the database and updates (or inserts) records based on SQL statement argument.
    /// Returns true if successful; false otherwise (DB error).
    /// </summary>
    /// <param name="strDBConnectionString"></param>
    /// <param name="strError"></param>
    /// <param name="strSQL"></param>
    /// <returns></returns>
    private bool ConnectToDatabaseAndUpdateStatus(string strDBConnectionString, ref string strError, string strSQL)
    {
      // If there is no SQL statement received then just return true
      if (string.IsNullOrEmpty(strSQL.Trim()))
      {
        return true;
      }

      // Sql Server
      SqlConnection pSqlConn = null;
      SqlTransaction pSqlTran = null;
      SqlCommand pSqlCmd = null;

      // Try to connect for status update.
      //----------------------------------------------
      try
      {
        if (m_iDBType == m_iCurrentDatabase_SqlServer)
        {
          pSqlCmd = new SqlCommand();
          pSqlConn = new SqlConnection(strDBConnectionString);
          pSqlConn.Open();

          pSqlCmd.Connection = pSqlConn;
          pSqlCmd.CommandText = strSQL;
          pSqlTran = pSqlConn.BeginTransaction(IsolationLevel.ReadCommitted);
          pSqlCmd.Transaction = pSqlTran;
          pSqlCmd.ExecuteNonQuery();
          pSqlTran.Commit();
        }
      }
      catch (Exception ex)
      {
        LogBatchUpdateError(ex); // Log detailed error in cs log file
        strError = ex.Message;
        Logger.LogError(strError, "Batch Update");

        if (m_iDBType == m_iCurrentDatabase_SqlServer)
        {
          if (pSqlTran != null)
          {
            pSqlTran.Rollback();// Return to previous state.
          }
        }
        return false;
      }
      finally
      {
        // Cleanup: Added "finally" block for the close connection calls.
        if (pSqlConn != null)
        {
          pSqlConn.Close();
        }
      }
      //----------------------------------------------

      return true;
    }

    /// <summary>
    /// Bug 8224 DH
    /// Daniel wrote this function. Please do not change without talking to me first. ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!
    /// </summary>
    /// <param name="strFileName"></param>
    /// <param name="strSystemInterfaceID"></param>
    /// <param name="strErrorMessage"></param>
    public void ProductLevel_AddSystemInterfaceError(string strFileName/*Will hold SYS_INTERFACE_SCOPE_ONLY for system interface scope only*/, string strSystemInterfaceID/*REQ*/, string strErrorMessage)
    {
      for (int i = 0; i < m_pSysInterfaceErrorsList.Count; i++)
      {
        ErrorData d = (ErrorData)m_pSysInterfaceErrorsList[i];
        if (d.strFileName == strFileName && d.strSystemInterfaceID == strSystemInterfaceID)
        {
          d.strErrorMessage += "\n" + strErrorMessage;
          m_pSysInterfaceErrorsList[i] = d;
          return;
        }
      }

      // Else add new error entry.
      ErrorData data = new ErrorData(strFileName, strSystemInterfaceID, strErrorMessage);
      m_pSysInterfaceErrorsList.Add(data);

      return;
    }

    /// <summary>
    /// Daniel wrote this function. Please do not change without talking to me first.  ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!
    /// Retrieve all deposits for this file and pass it to each system interface implementation class.
    /// </summary>
    /// <param name="strFileNbr"></param>
    /// <param name="strFileSequenceNbr"></param>
    /// <param name="strCurrentFile"></param>
    /// <param name="pCurrentFile"></param>
    /// <returns></returns>
    public bool ProductLevel_DispatchDeposits(string strFileNbr, string strFileSequenceNbr, string strCurrentFile, GenericObject pCurrentFile/*Bug 8332 DH*/)
    {
      //----------------------------------------------------------------------------------------------------------------
      // Hold all deposit and deposit tender records
      string sCoreFile = strFileNbr + strFileSequenceNbr;
      GenericObject pAllDeposits = new GenericObject();
      pAllDeposits.Add("DEPFILENBR", strFileNbr);
      pAllDeposits.Add("DEPFILESEQ", strFileSequenceNbr);
      pAllDeposits.Add("FULL_FILE_NAME", strCurrentFile);

      // IPAY-398: FT: Parameterize
      string strSql = "SELECT * FROM TG_DEPOSIT_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND VOIDDT IS NULL";
      GenericObject sql_args = new GenericObject();
      sql_args.Add("depfilenbr", ProductLevel_atoi(strFileNbr));
      sql_args.Add("depfileseq", ProductLevel_atoi(strFileSequenceNbr));
      GenericObject pDeposits = (GenericObject)ProductLevel_GetRecords(m_pDBInfo, 1, strSql, m_iDBType, sql_args);
      if (pDeposits != null)
      {
        int i = pDeposits.getIntKeyLength();
        for (int j = 0; j < i; j++)
        {
          GenericObject pDeposit = (GenericObject)pDeposits.get(j);
          string strDepositID = pDeposit.get("DEPOSITID").ToString();
          int iDepositNbr = (int)pDeposit.get("DEPOSITNBR");

          // IPAY-398: FT: Parameterize
          strSql = "SELECT * FROM TG_DEPOSITTNDR_DATA WHERE DEPOSITID=@depositid AND DEPOSITNBR=@depositnbr ORDER BY DEPOSITNBR";
          sql_args = new GenericObject();
          sql_args.Add("depositid", strDepositID);
          sql_args.Add("depositnbr", iDepositNbr);
          GenericObject pDepositTenders = (GenericObject)ProductLevel_GetRecords(m_pDBInfo, 1, strSql, m_iDBType, sql_args);
          if (pDepositTenders != null)
          {
            int x = pDepositTenders.getIntKeyLength();
            GenericObject pTenders = new GenericObject();

            for (int n = 0; n < x; n++)
              pTenders.insert((GenericObject)pDepositTenders.get(n));// in sequence from query

            pDeposit.Add("DEPOSIT_TENDERS", pTenders);
          }
          pAllDeposits.insert(pDeposit);
        }
      }
      //----------------------------------------------------------------------------------------------------------------------

      //----------------------------------------------------------------------------------------------------------------------
      // Call each implemented system interface and pass the deposits to it. The "Project_ProcessDeposits" must 
      // be implemented but may not do anything with the data. This is project specific.
      int iCntOfInterfaces = m_pSelectedSysInterfaces.getIntKeyLength();
      // Bug 12126 Mike O - Call using abstract class
      for (int i = 0; i < iCntOfInterfaces; i++)
      {
        string strSystemInterface = m_pSelectedSysInterfaces.get(i).ToString();
        AbstractBatchUpdate pTarget = (AbstractBatchUpdate)m_pSysInterfaceProjectImplementations[strSystemInterface];
        GenericObject pCurrentSystemInterface = (GenericObject)((GenericObject)c_CASL.c_object("System_interface.of")).get(strSystemInterface);
        string strSysInterfaceName = pCurrentSystemInterface.get("_name").ToString();

        if (SkipProcessing(strSysInterfaceName, strCurrentFile))
        {
          continue;
        }

        if (pTarget != null)// Ignore if project level not implemented
        {
          try
          {
            eReturnAction RetAction = eReturnAction.DEFAULT_FOR_MODE; // Set referenced variable to default
            bool bReturnVal = pTarget.Project_ProcessDeposits(pAllDeposits, pCurrentSystemInterface, this, strSystemInterface, pCurrentFile, ref RetAction);
            if (!bReturnVal)
            {
              //m_SysIntProcessAction[strSysInterfaceName] = Working.TERMINATE; // Terminate individual BU process
              TerminateProcessOrSkipCoreFile(sCoreFile, pCurrentFile, strSystemInterface, pCurrentSystemInterface, pTarget, RetAction);
              continue;
            }
          }
          catch (Exception ex)
          {
            // Log the error [NOTE: Project level implementation SHOULD make sure to catch all the errors in that code.]
            LogBatchUpdateError(ex, string.Format("System Interface = {0} File = {1}", strSysInterfaceName, strCurrentFile));
            //m_SysIntProcessAction[strSysInterfaceName] = Working.TERMINATE; // Terminate individual BU process
            TerminateProcessOrSkipCoreFile(sCoreFile, pCurrentFile, strSystemInterface, pCurrentSystemInterface, pTarget);
            continue;
          }
        }
      }
      return true;
    }

    /// <summary>
    /// Calls each implemented system interface with the "Project_CoreFile_BEGIN" function. The "Project_CoreFile_BEGIN" 
    /// method is virtual and may (or may NOT) be implemented by the project instance.
    /// </summary>
    /// <param name="sCoreFile"></param>
    /// <param name="geoCoreFile"></param>
    /// <returns></returns>
    public bool ProductLevel_BeginCoreFile(string sCoreFile, GenericObject geoCoreFile)
    {
      int iCntOfInterfaces = m_pSelectedSysInterfaces.getIntKeyLength();
      for (int i = 0; i < iCntOfInterfaces; i++)
      {
        string sSystemInterface = m_pSelectedSysInterfaces.get(i).ToString();
        AbstractBatchUpdate projectImplementation = (AbstractBatchUpdate)m_pSysInterfaceProjectImplementations[sSystemInterface];
        GenericObject geoSystemInterface = (GenericObject)((GenericObject)c_CASL.c_object("System_interface.of")).get(sSystemInterface);

        if (SkipProcessing(sSystemInterface, sCoreFile))
        {
          continue;
        }

        if (projectImplementation != null) // Ignore if project level not implemented
        {
          try
          {
            eReturnAction RetAction = eReturnAction.DEFAULT_FOR_MODE; // Set referenced variable to default
            bool bReturnVal = projectImplementation.Project_CoreFile_BEGIN(sSystemInterface, geoSystemInterface, this, geoCoreFile, sCoreFile, ref RetAction);
            if (!bReturnVal)
            {
              TerminateProcessOrSkipCoreFile(sCoreFile, geoCoreFile, sSystemInterface, geoSystemInterface, projectImplementation, RetAction);
              continue;
            }
          }
          catch (Exception ex)
          {
            // Log the error [NOTE: Project level implementation SHOULD make sure to catch all the errors in that code.]
            LogBatchUpdateError(ex, string.Format("System Interface = {0} File = {1}", sSystemInterface, sCoreFile));
            TerminateProcessOrSkipCoreFile(sCoreFile, geoCoreFile, sSystemInterface, geoSystemInterface, projectImplementation);
            continue;
          }
        }
      }
      return true;
    }

    /// <summary>
    /// Calls each implemented system interface with the "Project_CoreFile_END" function. The "Project_CoreFile_END" 
    /// method is virtual and may (or may NOT) be implemented by the project instance.
    /// </summary>
    /// <param name="sCoreFile"></param>
    /// <param name="geoCoreFile"></param>
    /// <returns></returns>
    public bool ProductLevel_EndCoreFile(string sCoreFile, GenericObject geoCoreFile)
    {
      int iCntOfInterfaces = m_pSelectedSysInterfaces.getIntKeyLength();
      for (int i = 0; i < iCntOfInterfaces; i++)
      {
        string sSystemInterface = m_pSelectedSysInterfaces.get(i).ToString();
        AbstractBatchUpdate projectImplementation = (AbstractBatchUpdate)m_pSysInterfaceProjectImplementations[sSystemInterface];
        GenericObject geoSystemInterface = (GenericObject)((GenericObject)c_CASL.c_object("System_interface.of")).get(sSystemInterface);

        if (SkipProcessing(sSystemInterface, sCoreFile))
        {
          continue;
        }

        if (projectImplementation != null) // Ignore if project level not implemented
        {
          try
          {
            long? lProjectCount = null;
            double? dProjectAmt = null;
            eReturnAction RetAction = eReturnAction.DEFAULT_FOR_MODE; // Set referenced variable to default
            bool bReturnVal = projectImplementation.Project_CoreFile_END(sSystemInterface, geoSystemInterface, this, geoCoreFile, sCoreFile, ref lProjectCount, ref dProjectAmt, ref RetAction);
            if (bReturnVal)
            {
              if (lProjectCount.HasValue && dProjectAmt.HasValue)
              {
                SetProjectCountAndAmount(sCoreFile, sSystemInterface, lProjectCount, dProjectAmt);
              }
            }
            else
            {
              TerminateProcessOrSkipCoreFile(sCoreFile, geoCoreFile, sSystemInterface, geoSystemInterface, projectImplementation, RetAction);
              continue;
            }
          }
          catch (Exception ex)
          {
            // Log the error [NOTE: Project level implementation SHOULD make sure to catch all the errors in that code.]
            LogBatchUpdateError(ex, string.Format("System Interface = {0} File = {1}", sSystemInterface, sCoreFile));
            TerminateProcessOrSkipCoreFile(sCoreFile, geoCoreFile, sSystemInterface, geoSystemInterface, projectImplementation);
            continue;
          }
        }
      }
      return true;
    }

    /// <summary>
    /// Skip any terminated or skipped [Action != CONTINUE] BU process (system interface) OR
    /// Skip any "CORE File Processing Mode" BU process that has encountered an error OR
    /// Skip any BU process that is already COMPLETE for the CORE file AND ForceProcess is NOT selected OR
    /// Skip any BU process that is in a WORKING state for the CORE file AND ForceProcess is NOT selected 
    /// </summary>
    /// <param name="sSystemInterface"></param>
    /// <param name="sCoreFile"></param>
    /// <returns></returns>
    private bool SkipProcessing(string sSystemInterface, string sCoreFile)
    {
      bool bSkipSystemCoreFile =
      (
      (m_SysIntProcessAction[sSystemInterface] != eWorking.CONTINUE) ||
      (m_CoreFileSysInterfaceState[sCoreFile].pSysIntState[sSystemInterface].bSkipCoreFile_Error) ||
      (!m_bForceBatchJob && m_CoreFileSysInterfaceState[sCoreFile].pSysIntState[sSystemInterface].bCompleteStatusInDB) ||
      (!m_bForceBatchJob && m_CoreFileSysInterfaceState[sCoreFile].pSysIntState[sSystemInterface].bWorkingStatusInDB)
      );
      return bSkipSystemCoreFile;
    }

    public bool ProductLevel_FindSystemInterface(string strSysInterface, string strTranSysInterfaces)
    {
      // Daniel wrote this function. Please do not change without talking to me first. ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      string strInt = "\"" + strSysInterface + "\"";

      if (-1 != strTranSysInterfaces.IndexOf(strInt))
        return true;

      return false;
    }

    public string ProductLevel_GetFileNumber(string strFileName)
    {
      // Daniel wrote this function. Please do not change without talking to me first. ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      return strFileName.Substring(0, 7);
    }

    public string ProductLevel_GetFileSequence(string strFileName)
    {
      // Daniel wrote this function. Please do not change without talking to me first. ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      return "" + ProductLevel_atoi(strFileName.Substring(7, strFileName.Length - 7));
    }

    public static long ProductLevel_atoi(string s)
    {
      // Daniel wrote this function. Please do not change without talking to me first. ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      return (int)Convert.ChangeType(s, typeof(System.Int32));
    }

        /// <summary>
        /// Changed ProductLevel_GetRecords to fetch records with parameterized query.
        /// IPAY-398
        /// </summary>
        /// <param name="db"></param>
        /// <param name="iFlag"></param>
        /// <param name="strSQL"></param>
        /// <param name="iDBType"></param>
        /// <param name="sql_args">GenericObject, not hash</param>
        /// <returns></returns>
        public GenericObject ProductLevel_GetRecords(GenericObject db, int iFlag, string strSQL, int iDBType, GenericObject sql_args)
        {
            // Daniel wrote this function. Please do not change without talking to me first. ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

            GenericObject geoDBReturnData = null;
            GenericObject gTmp = null;

            if (iDBType == m_iCurrentDatabase_SqlServer)
            {
                db_vector_reader_sql pReader = (db_vector_reader_sql)db_vector_reader_sql.CASL_make("db_connection", db, "sql_statement", strSQL, "sql_args", sql_args);   // IPAY-398: FT: Added sql_args
                geoDBReturnData = new GenericObject();
                geoDBReturnData = (GenericObject)db_vector_reader_sql.CASL_insert_data("reader", pReader, "_subject", geoDBReturnData, "start", 0, "insert_size", 0);
                if (geoDBReturnData.has(0))
                {
                    if (iFlag == 0)
                        gTmp = (GenericObject)geoDBReturnData.get(0);
                    else
                        gTmp = (GenericObject)geoDBReturnData;
                }
                pReader = null;
            }

            return gTmp;
        }

        private int ProductLevel_GetDBConnectionType(GenericObject DBConnInfo)
    {
      // Daniel wrote this function. Please do not change without talking to me first. ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      GenericObject DBInfo = (GenericObject)DBConnInfo;
      string strConnctionClass = (string)(DBInfo.get("_parent") as GenericObject).get("_name", "", true);

      if (strConnctionClass == "Sql_server")
        return m_iCurrentDatabase_SqlServer;
      else if (strConnctionClass == "Access")
        return m_iCurrentDatabase_MsAccess_ODBC;

      return -1;// Undefined
    }

    /// <summary>
    /// Daniel wrote this function. Please do not change without talking to me first. ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!
    /// Create unique ID the same as the old BU code format but add milliseconds for better separation.
    /// I had problems running two BUs in the same minute.
    /// </summary>
    /// <returns></returns>
    public string ProductLevel_CreateUpdateID()
    {
      DateTime dtNow = DateTime.Now;
      string strID = string.Format("{0}{1}{2}{3}{4}{5}", dtNow.Month, dtNow.Day, dtNow.Year, dtNow.Hour, dtNow.Minute, dtNow.Millisecond);
      return strID;
    }

    private bool ProductLevel_IsOneOfUISelectedSysInterfaces(string strSysInterfaceName)
    {
      // Daniel wrote this function. Please do not change without talking to me first. ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!
      int iCntOfInterfaces = m_pSelectedSysInterfaces.getIntKeyLength();
      for (int j = 0; j < iCntOfInterfaces; j++)
      {
        string strSystemInterface = m_pSelectedSysInterfaces.get(j).ToString();
        if (strSystemInterface == strSysInterfaceName)
          return true;
      }
      return false;
    }

    public eProcessingMode ProductLevel_GetProcessingMode(string strSysInterfaceName)
    {
      eProcessingMode returnMode = eProcessingMode.SYSTEM;
      GenericObject sysIntGEO = (GenericObject)c_CASL.execute("System_interface.of." + strSysInterfaceName);
      string sMode = (string)(sysIntGEO.get("processing_mode", "SYSTEM"));
      if (sMode != "SYSTEM")
      {
        returnMode = eProcessingMode.CORE_FILE;
      }

      return returnMode;
    }

    /// <summary>
    /// Daniel wrote this function. Please do not change without talking to me first.  ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!
    /// ------------------------------------------------------------------------------------------------------------------------------
    /// This method will call the overridden "Project_ProcessCoreEvent" for each Batch Update instance (Project Level System 
    /// Interface implementation) if the event contains a transaction or tender associated with a selected system interface.
    /// NOTE: Project Level implementations for system interface classes will be done in C#. Do NOT call into CASL at all 
    /// unless there is something that does not require looping, transformations, or much logic at all.
    /// </summary>
    /// <param name="pFileGEO"></param>
    /// <param name="pEventGEO"></param>
    /// <param name="strCurrentFile"></param>
    /// <returns></returns>
    public bool ProductLevel_DispatchEvent(ref GenericObject pFileGEO /*Bug 8182 DH*/, ref GenericObject pEventGEO, string strCurrentFile)
    {
      if (pEventGEO == null)
      {
        return true;
      }

      //-----------------------------------------------------------------------
      // Bug 12136 DH - Process all transactions for certain system interfaces
      // Get transactions and tenders for this event
      int iEventTranCount = 0, lEventTenderCount = 0; // Bug 14210-tender count in event
      GenericObject pEventGEOTrans = null, pEventTenders = null;
      if (pEventGEO.has("TRANSACTIONS") || pEventGEO.has("TENDERS"))
      {
        pEventGEOTrans = (GenericObject)pEventGEO.get("TRANSACTIONS", c_CASL.c_GEO()); // Bug 14852 - added empty GEO for if_missing
        iEventTranCount = (int)pEventGEOTrans.getLength();

        pEventTenders = (GenericObject)pEventGEO.get("TENDERS", c_CASL.c_GEO()); // Bug 14852 - added empty GEO for if_missing
        lEventTenderCount = pEventTenders != null ? (int)pEventTenders.getLength() : 0;
      }
      else
      {
        return true;// Nothing to process
      }
      //-----------------------------------------------------------------------

      //-----------------------------------------------------------------------
      // Process all transactions for these system interfaces if they need it.
      if (m_arrSysIntsThatProcessFullFiles.Count > 0)
      {
        ArrayList arrListOfInterfacesProcessedForThisEvnt1 = new ArrayList();

        // Go through all system interfaces that require all transactions
        for (int iInterfaceCnt = 0; iInterfaceCnt < m_arrSysIntsThatProcessFullFiles.Count; iInterfaceCnt++)
        {
          GenericObject pCurrentSysInterface = (GenericObject)m_arrSysIntsThatProcessFullFiles[iInterfaceCnt];
          string strSysInterfaceName = pCurrentSysInterface.get("_name").ToString();

          // Skip any system interfaces we didn't intend to process
          if (!ProductLevel_IsOneOfUISelectedSysInterfaces(strSysInterfaceName))
          {
            continue;
          }

          if (SkipProcessing(strSysInterfaceName, strCurrentFile))
          {
            continue;
          }

          // Process all transactions for this system interfcae
          AbstractBatchUpdate pTarget = (AbstractBatchUpdate)m_pSysInterfaceProjectImplementations[strSysInterfaceName];
          if (pTarget != null)// Ignore if project level not implemented
          {
            int iTranCount = (int)pEventGEOTrans.getLength();
            for (int i = 0; i < iTranCount; i++)
            {
              try
              {
                GenericObject pTran = (GenericObject)pEventGEOTrans.get(i);

                // Bug 12053 DH - Make sure not to process the same system interface twice.
                if (!arrListOfInterfacesProcessedForThisEvnt1.Contains(strSysInterfaceName))
                  arrListOfInterfacesProcessedForThisEvnt1.Add(strSysInterfaceName);
                else
                  continue;

                string strError = "";
                if (m_bTestMode == false)
                {
                  // Because everything is going through here, some transactions will not have associated tender with them. This will show on the report as an error.
                  // So please handle it accordingly in the project code.
                  if (pEventGEO.has("TENDERS"))
                  {
                    bool bResolvedTenderToTrans = ProductLevel_ResolveTenderToTrans(pEventGEOTrans/*Bug 12136 DH*/, ref pTran, (GenericObject)pEventGEO.get("TENDERS"), strSysInterfaceName, ref strError);
                    if (!bResolvedTenderToTrans)// false is a problem
                    {
                      // Bug 12053 DH - No need to report on this since some transaction will fail resolving tenders.
                      // Not all transactions processed for Bug 12136 will have a tender associated with them.
                    }
                  }
                }

                eReturnAction RetAction = eReturnAction.DEFAULT_FOR_MODE; // Set referenced variable to default
                if (pTarget.Project_ProcessCoreEvent(strSysInterfaceName, pEventGEO, pCurrentSysInterface, pFileGEO, this, strCurrentFile, ref RetAction))
                {
                  IncrementCountAndAmountForEvent(strCurrentFile, pCurrentSysInterface, pEventGEO);
                }
                else
                {
                  TerminateProcessOrSkipCoreFile(strCurrentFile, pFileGEO, strSysInterfaceName, pCurrentSysInterface, pTarget, RetAction);
                  continue;
                }
              }
              catch (Exception ex)
              {
                // Log the error [NOTE: Project level implementation SHOULD make sure to catch all the errors in that code.]
                LogBatchUpdateError(ex, string.Format("System Interface [Processes Full Files] = {0}", strSysInterfaceName));
                TerminateProcessOrSkipCoreFile(strCurrentFile, pFileGEO, strSysInterfaceName, pCurrentSysInterface, pTarget); // Terminate individual BU process
                continue;
              }
            }
          }
        }
      }
      //-----------------------------------------------------------------------

      ArrayList arrListOfInterfacesProcessedForThisEvnt = new ArrayList();

      //-----------------------------------------------------------------------
      // ********** Transactions **********
      for (int i = 0; i < iEventTranCount; i++)
      {
        System.Text.StringBuilder strTransactionRecord = new System.Text.StringBuilder();

        GenericObject pTran = (GenericObject)pEventGEOTrans.get(i);
        if (pTran != null)
        {
          string strTranTTID = pTran.get("TTID").ToString();

          // We've collected all these at the begining of RunBatchUpdate
          if (!m_pAllTransactionTypesWithSysInterfaces.Contains(strTranTTID))
            continue;

          // List of system interfaces configured on this transaction type.
          GenericObject pInterfaceList = (GenericObject)m_pAllTransactionTypesWithSysInterfaces[strTranTTID];
          int iCnt = pInterfaceList == null ? 0 : pInterfaceList.getLength();
          for (int x = 0; x < iCnt; x++)
          {
            GenericObject pCurrentSysInterface = (GenericObject)pInterfaceList.get(x);
            string strSysInterfaceName = pCurrentSysInterface.get("name").ToString();// name is the ID. Go figure...

            // Bug 12136 DH - Process all transactions for certain system interfaces
            // If this 'strSysInterface' system interface requires all transactions then it was already processed above. Skip it.
            if (m_arrSysIntsThatProcessFullFiles.Contains(pCurrentSysInterface))
              continue;

            // Make sure not to process the same system interface twice.
            if (!arrListOfInterfacesProcessedForThisEvnt.Contains(pCurrentSysInterface))
              arrListOfInterfacesProcessedForThisEvnt.Add(pCurrentSysInterface);
            else
              continue;

            // Check to see if this System Interface was chosen by user.
            if (ProductLevel_SystemInterfaceChosenInUI(strSysInterfaceName))
            {
              if (SkipProcessing(strSysInterfaceName, strCurrentFile))
              {
                continue;
              }

              // Bug 12126 Mike O - Call using abstract class
              AbstractBatchUpdate pTarget = null;
              try
              {
                pTarget = (AbstractBatchUpdate)m_pSysInterfaceProjectImplementations[strSysInterfaceName];
                if (pTarget != null)// Ignore if project level not implemented
                {
                  // Lets get the list of tenders for this transaction
                  string strError = "";
                  if (m_bTestMode == false)
                  {
                    // Bug 8946 & 8947 DH - TG_TTA_MAP_DATA is no longer updated and should not be used here. Please see Bug 8952.
                    // I am leaving the code in case we need to resume its use in the future.
                    if (pEventGEO.has("TRANSACTIONS") && pEventGEO.has("TENDERS"))//ANN added for 12117 case of no tenders- zero dollar receipt
                    {
                      GenericObject pEventTrans = (GenericObject)pEventGEO.get("TRANSACTIONS");
                      bool bResolvedTenderToTrans = ProductLevel_ResolveTenderToTrans(pEventTrans, ref pTran, (GenericObject)pEventGEO.get("TENDERS"), strSysInterfaceName, ref strError);
                      if (!bResolvedTenderToTrans) // false is a problem
                      {
                        // Will have to at least write it to the log.
                        ProductLevel_AddSystemInterfaceError(strCurrentFile, strSysInterfaceName, strError);// Bug 8224 DH
                        Logger.LogError("Unable to establish relationship TTAMAP: " + strError, "Batch Update");
                      }
                    }
                  }

                  eReturnAction RetAction = eReturnAction.DEFAULT_FOR_MODE; // Set referenced variable to default
                  if (pTarget.Project_ProcessCoreEvent(strSysInterfaceName, pEventGEO, pCurrentSysInterface, pFileGEO, this, strCurrentFile, ref RetAction))
                  {
                    IncrementCountAndAmountForEvent(strCurrentFile, pCurrentSysInterface, pEventGEO);
                  }
                  else
                  {
                    TerminateProcessOrSkipCoreFile(strCurrentFile, pFileGEO, strSysInterfaceName, pCurrentSysInterface, pTarget, RetAction);
                    continue;
                  }
                }
              }
              catch (Exception ex)
              {
                // Log the error [NOTE: Project level implementation SHOULD make sure to catch all the errors in that code.]
                LogBatchUpdateError(ex, string.Format("System Interface = {0}", strSysInterfaceName));
                TerminateProcessOrSkipCoreFile(strCurrentFile, pFileGEO, strSysInterfaceName, pCurrentSysInterface, pTarget);
                continue;
              }
            }
          }
        }
      }
      //-----------------------------------------------------------------------

      //-----------------------------------------------------------------------
      // ********** Tenders **********
      // Bug 14210 - This section is for batch updates that are only assigned to tenders
      // Bug 21267 - Batch Update Failure When Assigned to Tender [Fixed issue: The tender loop was INSIDE the transaction loop]
      for (int j = 0; j < lEventTenderCount; j++)
      {
        GenericObject pTender = (GenericObject)pEventTenders.get(j);
        if (pTender != null)
        {
          string strTenderID = pTender.get("TNDRID").ToString();

          // We've collected all these at the begining of RunBatchUpdate
          if (!m_pAllTenderTypesWithSysInterfaces.Contains(strTenderID))
            continue;

          // List of system interfaces configured on this tender type.
          GenericObject pInterfaceList = (GenericObject)m_pAllTenderTypesWithSysInterfaces[strTenderID];
          int iCnt = pInterfaceList == null ? 0 : pInterfaceList.getLength();
          for (int x = 0; x < iCnt; x++)
          {
            GenericObject pCurrentSysInterface = (GenericObject)pInterfaceList.get(x);
            string strSysInterfaceName = pCurrentSysInterface.get("name").ToString(); // name is the ID. Go figure...

            // Bug 12136 DH - Process all transactions for certain system interfaces
            // If this 'strSysInterface' system interface requires all transactions then it was already processed above. Skip it.
            if (m_arrSysIntsThatProcessFullFiles.Contains(pCurrentSysInterface))
              continue;

            // Make sure not to process the same system interface twice.
            if (!arrListOfInterfacesProcessedForThisEvnt.Contains(pCurrentSysInterface))
              arrListOfInterfacesProcessedForThisEvnt.Add(pCurrentSysInterface);
            else
              continue;

            // Check to see if this System Interface was chosen by user.
            if (ProductLevel_SystemInterfaceChosenInUI(strSysInterfaceName))
            {
              if (SkipProcessing(strSysInterfaceName, strCurrentFile))
              {
                continue;
              }

              // Bug 12126 Mike O - Call using abstract class
              AbstractBatchUpdate pTarget = null;
              try
              {
                pTarget = (AbstractBatchUpdate)m_pSysInterfaceProjectImplementations[strSysInterfaceName];
                if (pTarget != null)// Ignore if project level not implemented
                {
                  eReturnAction RetAction = eReturnAction.DEFAULT_FOR_MODE; // Set referenced variable to default
                  if (pTarget.Project_ProcessCoreEvent(strSysInterfaceName, pEventGEO, pCurrentSysInterface, pFileGEO, this, strCurrentFile, ref RetAction))
                  {
                    IncrementCountAndAmountForEvent(strCurrentFile, pCurrentSysInterface, pEventGEO);
                  }
                  else
                  {
                    TerminateProcessOrSkipCoreFile(strCurrentFile, pFileGEO, strSysInterfaceName, pCurrentSysInterface, pTarget, RetAction);
                    continue;
                  }
                }
              }
              catch (Exception ex)
              {
                // Log the error [NOTE: Project level implementation SHOULD make sure to catch all the errors in that code.]
                LogBatchUpdateError(ex, string.Format("System Interface = {0}", strSysInterfaceName));
                TerminateProcessOrSkipCoreFile(strCurrentFile, pFileGEO, strSysInterfaceName, pCurrentSysInterface, pTarget);
                continue;
              }
            }
          }
        }
      }
      // end Bug 14210
      //-----------------------------------------------------------------------

      return true;
    }

    /// <summary>
    /// Increments the count and amount for tenders or transactions associated with a System Interface in an CORE event.
    /// </summary>
    /// <param name="sCoreFile"></param>
    /// <param name="geoSysInterface"></param>
    /// <param name="geoEvent"></param>
    private void IncrementCountAndAmountForEvent(string sCoreFile, GenericObject geoSysInterface, GenericObject geoEvent)
    {
      string sSysInterface = geoSysInterface.get("_name").ToString();

      //-----------------------------------------------------------------------
      // Tender Event Items: Count and Amount
      if (m_pAllTenderTypesWithSysInterfaces.Count > 0)
      {
        GenericObject geoTenders = (GenericObject)geoEvent.get("TENDERS", null);
        int nTenderCount = geoTenders == null ? 0 : (int)geoTenders.getLength();
        for (int i = 0; i < nTenderCount; i++)
        {
          // Get the Tender Count & Amount if the transaction is associated with System Interface:
          GenericObject geoTender = (GenericObject)geoTenders.get(i);
          string strTenderID = geoTender.get("TNDRID").ToString();
          GenericObject pInterfaceList = (GenericObject)m_pAllTenderTypesWithSysInterfaces[strTenderID]; // List of system interfaces configured on this tender type.
          if (pInterfaceList != null && pInterfaceList.vectors.Contains(geoSysInterface))
          {
            double dTenderAmt = (double)geoTender.get("AMOUNT", 0.00);
            if (dTenderAmt != 0.00) // Bug 20601 (20598): Incorrect Reversal Amounts in the System Interface Updates Report
            {
              IncrementCountAndAmount(sCoreFile, sSysInterface, dTenderAmt, true);
            }
          }
        }
      }
      //-----------------------------------------------------------------------

      //-----------------------------------------------------------------------
      // Transaction Event Items: Count and Amount
      if (m_pAllTransactionTypesWithSysInterfaces.Count > 0)
      {
        bool bProcessEverything = (bool)geoSysInterface.get("process_everything_in_file", false); // Bug 20601 - Check for "process_everything_in_file"
        GenericObject geoTransactions = (GenericObject)geoEvent.get("TRANSACTIONS", null);
        int nTransactionCount = geoTransactions == null ? 0 : (int)geoTransactions.getLength();
        for (int i = 0; i < nTransactionCount; i++)
        {
          // Get the Transaction Count & Amount if the transaction is associated with System Interface:
          GenericObject geoTran = (GenericObject)geoTransactions.get(i);
          string strTranTTID = geoTran.get("TTID").ToString();
          GenericObject pInterfaceList = (GenericObject)m_pAllTransactionTypesWithSysInterfaces[strTranTTID]; // List of system interfaces configured on this transaction type.
          if ((pInterfaceList != null && pInterfaceList.vectors.Contains(geoSysInterface)) || bProcessEverything) // Bug 20601 - Check for "process_everything_in_file"
          {
            double dTranAmt = (double)geoTran.get("TRANAMT", 0.00);
            if (dTranAmt != 0.00) // Bug 20601 (20598): Incorrect Reversal Amounts in the System Interface Updates Report
            {
              IncrementCountAndAmount(sCoreFile, sSysInterface, dTranAmt);
            }
          }
        }
      }
      //-----------------------------------------------------------------------
    }

    private void IncrementCountAndAmount(string sCoreFile, string sSysInterface, double dAmount)
    {
      IncrementCountAndAmount(sCoreFile, sSysInterface, dAmount, false);
    }

    private void IncrementCountAndAmount(string sCoreFile, string sSysInterface, double dAmount, bool bTender)
    {
      ProcessStateForCoreFile ps = m_CoreFileSysInterfaceState[sCoreFile].pSysIntState[sSysInterface];
      if (bTender)
      {
        ps.dTenderAmt += dAmount;
        ps.lTenderCount += 1;
      }
      else
      {
        ps.dTransactionAmt += dAmount;
        ps.lTransactionCount += 1;
      }
      m_CoreFileSysInterfaceState[sCoreFile].pSysIntState[sSysInterface] = ps;
    }

    private void SetProjectCountAndAmount(string sCoreFile, string sSysInterface, long? lCount, double? dAmount)
    {
      ProcessStateForCoreFile ps = m_CoreFileSysInterfaceState[sCoreFile].pSysIntState[sSysInterface];
      ps.lProjectCount = lCount;
      ps.dProjectAmt = dAmount;
      m_CoreFileSysInterfaceState[sCoreFile].pSysIntState[sSysInterface] = ps;
    }

    /// <summary>
    /// Overloaded "TerminateProcessOrSkipCoreFile" method: Excludes the ref "eReturnAction" parameter.
    /// </summary>
    /// <param name="sCoreFile"></param>
    /// <param name="pCoreFile"></param>
    /// <param name="sSysInt"></param>
    /// <param name="pSysInt"></param>
    /// <param name="pBU"></param>
    private void TerminateProcessOrSkipCoreFile(string sCoreFile, GenericObject pCoreFile, string sSysInt, GenericObject pSysInt, AbstractBatchUpdate pBU)
    {
      eReturnAction RetAction = eReturnAction.DEFAULT_FOR_MODE; // Set referenced variable to default
      TerminateProcessOrSkipCoreFile(sCoreFile, pCoreFile, sSysInt, pSysInt, pBU, RetAction);
      return;
    }

    /// <summary>
    /// The "TerminateProcessOrSkipCoreFile" method will set either the "m_SysIntProcessAction" action to Working.TERMINATE
    /// or the CORE file's "m_CoreFileSystemInterfaceState" to skipped (bOmitCoreFile_Error = true) based on the configured
    /// Processing Mode (SYSTEM or CORE_FILE) for the System Interface.
    /// </summary>
    /// <param name="sCoreFile"></param>
    /// <param name="pCoreFile"></param>
    /// <param name="sSysInt"></param>
    /// <param name="pSysInt"></param>
    /// <param name="pBU"></param>
    /// <param name="ReturnAction"></param>
    private void TerminateProcessOrSkipCoreFile(string sCoreFile, GenericObject pCoreFile, string sSysInt, GenericObject pSysInt, AbstractBatchUpdate pBU, eReturnAction ReturnAction)
    {
      string ErrMsg = "";

      // Get the configured Processing Mode for the System Interface
      eProcessingMode procMode = string.IsNullOrEmpty(sCoreFile) ? eProcessingMode.SYSTEM : ProductLevel_GetProcessingMode(sSysInt);

      if (ReturnAction == eReturnAction.DEFAULT_FOR_MODE)
      {
        switch (procMode)
        {
          // Terminate processing the BU Process (System Interface)
          case eProcessingMode.SYSTEM:
            m_SysIntProcessAction[sSysInt] = eWorking.TERMINATE;
            break;

          // Terminate processing the BU Process (System Interface) ONLY for this CORE file (i.e., skip the CORE file during processing)
          case eProcessingMode.CORE_FILE:
            {
              ProcessStateForCoreFile ps = m_CoreFileSysInterfaceState[sCoreFile].pSysIntState[sSysInt];
              ps.bSkipCoreFile_Error = true;
              m_CoreFileSysInterfaceState[sCoreFile].pSysIntState[sSysInt] = ps;
            }
            break;
        }
      }
      else // Project implementation specified a non-default action:
      {
        switch (ReturnAction)
        {
          // Terminate processing the BU Process (System Interface)
          case eReturnAction.TERMINATE_SYSTEM_INTERFACE:
            m_SysIntProcessAction[sSysInt] = eWorking.TERMINATE;
            if (procMode == eProcessingMode.CORE_FILE)
            {
              ErrMsg = string.Format("Due to an error, the following System Interface requested to be terminated: {0}.", sSysInt);
            }
            break;

          // Terminate ALL BU Processes (System Interfaces)
          case eReturnAction.TERMINATE_ENTIRE_BATCH_RUN:
            {
              // Project level System Interface implementation has the ability to terminate the whole thing.
              ErrMsg = string.Format("Due to an error, ALL SYSTEMS were terminated by a System Interface ({0}) request.", sSysInt);
              m_strBatchJobComments = string.Format("{0}{1}{2}"
                  , m_strBatchJobComments
                  , string.IsNullOrEmpty(m_strBatchJobComments) ? "" : " | "
                  , ErrMsg
                  );

              for (int i = 0; i < m_pSelectedSysInterfaces.getLength(); i++)
              {
                string sSI = (string)m_pSelectedSysInterfaces.get(i);
                m_SysIntProcessAction[sSI] = eWorking.TERMINATE;
              }
            }
            break;
        }
      }

      // If processing mode is "CORE_FILE" then call Project_RollbackCoreFile
      if ((ReturnAction == eReturnAction.DEFAULT_FOR_MODE) && (procMode == eProcessingMode.CORE_FILE))
      {
        if (!pBU.Project_RollbackCoreFile(sSysInt, pSysInt, this, pCoreFile, sCoreFile))
        {
          // If derived BU class fails to rollback the Core file data then terminate the system interface
          m_SysIntProcessAction[sSysInt] = eWorking.TERMINATE; // Terminate individual BU process
        }
      }

      // Log any error message
      if (!string.IsNullOrEmpty(ErrMsg))
      {
        LogBatchUpdateError(ErrMsg);
      }
    }

    public bool ProductLevel_ResolveTenderToTrans(GenericObject pEventTrans, ref GenericObject pFirstTran, GenericObject pEventTenders, string strSysInterface, ref string strError)
    {
      // Daniel wrote this function. Please do not change without talking to me first. ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!
      // ANN modified this code for Bug 10089 to loop through all trans in the event and
      //get the glacct and amount for the TTA table and the tender desc and tender id from the TENDER table
      //According to my testing it looks like this whole method should be moved to another place before calling ProductLevel_DispatchEvent
      long iEventNbr = (int)pFirstTran.get("EVENTNBR");
      long lFileNbr = (int)pFirstTran.get("DEPFILENBR");
      long lFileSeqNbr = (int)pFirstTran.get("DEPFILESEQ");

      try
      {
        //--------------------------------------------------
        // I have the transaction and tenders records for this event. I still need to query the TG_TTA_MAPPING data.
        // IPAY-398: FT: Parameterize
        string strSql = "SELECT * FROM TG_TTA_MAP_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND EVENTNBR=@eventnbr";
        GenericObject sql_args = new GenericObject();
        sql_args.Add("depfilenbr", (int)lFileNbr);
        sql_args.Add("depfileseq", (int)lFileSeqNbr);
        sql_args.Add("eventnbr", iEventNbr);
        GenericObject pTTA = (GenericObject)ProductLevel_GetRecords(m_pDBInfo, 1, strSql, m_iDBType, sql_args);
        if (pTTA == null)
        {
          // This should never happen here.
          //12146 suppress this message for Parkland per Ali strError = string.Format("No transaction-tender association found for: Event:{0} PayFile:{1} Seq:{2}", iEventNbr, lFileNbr, lFileSeqNbr);
          strError = "";
          return false;
        }
        //--------------------------------------------------
        int kCnt = pEventTrans.getLength();
        for (int k = 0; k < kCnt; k++)
        {
          GenericObject pTranTenders = new GenericObject();// ann holds all tenders for this transaction
          GenericObject pTran = new GenericObject();
          pTran = (GenericObject)pEventTrans.get(k);
          //Ann note:Check if this trans has TTA_TENDERS becuase it was already processed by another system int
          //Indicates that this whole method should be called in another place probably before calling ProductLevel_DispatchEvent
          if (pTran.has("TTA_TENDERS"))
            continue;
          double dTranAmt = (double)pTran.get("TRANAMT");

          int iCnt = pTTA.getLength();
          for (int i = 0; i < iCnt; i++)
          {

            GenericObject pTTAItem = (GenericObject)pTTA.get(i);
            if ((pTTAItem.has("CREDITITEMNBR")) && (pTTAItem.has("DEBITITEMNBR")))
            {
              if ((pTTAItem.get("CREDITITEMNBR").ToString().Trim() != "") && (pTTAItem.get("DEBITITEMNBR").ToString().Trim() != ""))
                continue; //with current TTA MAP table data this is the only way I know to identify if this record offsets itself and should not map to a transaction
            }
            if ((!pTTAItem.has("CREDITITEMNBR")) && (!pTTAItem.has("DEBITITEMNBR")))
              continue; // Bug 10785 with current TTA MAP table data this is the only way I know to identify if this record is CHANGE and does not map to a transaction
            int iTranNbr = (int)pTran.get("TRANNBR");

            //Get the TRANNBR and TENDERNBR from the TTA record these are stored in DEBITNBR CREDITNBR fields
            int iTTATranNbr = 0;
            int iTTATndrNbr = 0;
            if (pTTAItem.has("CREDITITEMNBR"))
            {
              iTTATranNbr = (int)pTTAItem.get("CREDITNBR");
              iTTATndrNbr = (int)pTTAItem.get("DEBITNBR");
            }
            if (pTTAItem.has("DEBITITEMNBR"))
            {
              iTTATranNbr = (int)pTTAItem.get("DEBITNBR");
              iTTATndrNbr = (int)pTTAItem.get("CREDITNBR");
            }
            /* if (dTranAmt >= 0)
             {
                 if (!pTTAItem.has("CREDITITEMNBR"))
                     continue;//this is not one of the tta records for this trans
                 iTTATranNbr = (int)pTTAItem.get("CREDITNBR");
             }
             else if (dTranAmt < 0)
             {
                 if (!pTTAItem.has("DEBITITEMNBR"))
                     continue;//this is not one of the tta records for this trans
       
                 iTTATranNbr = (int)pTTAItem.get("DEBITNBR");
             }*/


            if (iTTATranNbr == iTranNbr)
            {
              string TTAgl_nbr = pTTAItem.get("GLACCTNBR", "").ToString();
              double TTAAmount = (double)pTTAItem.get("AMOUNT");
              int jCnt = pEventTenders.getLength();
              for (int j = 0; j < jCnt; j++)
              {
                GenericObject pTndrItem = new GenericObject();
                pTndrItem = (GenericObject)pEventTenders.get(j);
                int iTndrNbr = (int)pTndrItem.get("TNDRNBR");

                //int iTTATndrNbr = 0;
                /*  if (dTranAmt >= 0)
                  {
                      iTTATndrNbr = (int)pTTAItem.get("DEBITNBR");
                  }
                  else if (dTranAmt < 0)
                  {
                      iTTATndrNbr = (int)pTTAItem.get("CREDITNBR");
                  }*/

                if (iTndrNbr == iTTATndrNbr)
                {
                  GenericObject pTranTenderItem = new GenericObject();

                  pTranTenderItem.set("GLACCTNBR", TTAgl_nbr);
                  pTranTenderItem.set("AMOUNT", pTTAItem.get("AMOUNT"));
                  pTranTenderItem.set("TNDRDESC", pTndrItem.get("TNDRDESC"));
                  pTranTenderItem.set("TNDRID", pTndrItem.get("TNDRID"));
                  if (pTTAItem.has("DEBITITEMNBR")) //10793 to send the sign of the tender
                    pTranTenderItem.set("CREDIT", true);//negative tender
                  else
                    pTranTenderItem.set("CREDIT", false);

                  pTranTenders.insert(pTranTenderItem);
                }
              }
            }
          }
          // Add all tenders to the transaction
          pTran.Add("TTA_TENDERS", pTranTenders);
          //  pTranTenders.vectors.Clear();
        }// end pEventTrans
      }
      catch (Exception ex)
      {
        // Log the error
        LogBatchUpdateError(ex, string.Format("FileNbr = {0} SeqNbr = {1} EventNbr = {2}", lFileNbr, lFileSeqNbr, iEventNbr));
        return false;
      }
      return true;
    }

    public bool ProductLevel_SystemInterfaceChosenInUI(string strTransactionSysInterface)
    {
      // Daniel wrote this function. Please do not change without talking to me first. ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!
      // Check if user selected this system interface to run.

      int iSysInterfaceCount = m_pSelectedSysInterfaces.getLength();
      for (int i = 0; i < iSysInterfaceCount; i++)
      {
        string strUISysInterface = m_pSelectedSysInterfaces.get(i).ToString();
        if (strUISysInterface == strTransactionSysInterface)
          return true;
      }

      return false;
    }

    public void ProductLevel_EndingBatchUpdate_WriteTrailers()
    {
      // Daniel wrote this function. Please do not change without talking to me first. ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!
      // End of all events in all files for all system interfaces. Call the last project thing which is to close all connections
      // and write any potential trailer records etc.  You have a chance here to rollback any records in case it fails. 

      int iSysInterfaceCount = m_pSelectedSysInterfaces.getLength();
      for (int i = 0; i < iSysInterfaceCount; i++)
      {
        string strUISysInterface = m_pSelectedSysInterfaces.get(i).ToString();

        // The "Project_WriteTrailer" method will NOT be called for ALL SIs. If any SI that has been terminated needs
        // to close connections or remove objects then it should be implemented in the "Project_Cleanup" method.
        // Skip any terminated or skipped [Action != CONTINUE] BU processes (system interfaces)
        if (m_SysIntProcessAction[strUISysInterface] != eWorking.CONTINUE)
        {
          continue;
        }

        // Bug 12126 Mike O - Call using abstract class
        try
        {
          // PROJECT LEVEL WILL HAVE TO IMPLEMENT THESE SYSTEM INTERFACE CLASSES
          // THIS MUST BE DONE IN C#. DO NOT CALL INTO CASL AT ALL UNLESS YOU NEED SOMETHING THAT DOES NOT REQUIRE
          // LOOPING AND TRANSFORMATIONS AND MUCH LOGIC AT ALL.
          // I AM PASSING OBJECTS BELOW TO SAVE ON THE ITERATIONS, SINCE I ALREADY HAVE THE RELEVANT
          // TRANSACTION, ALLOCATION FOR THAT TRANSACTION, AND THE CUSTOM FIELD.
          AbstractBatchUpdate pTarget = (AbstractBatchUpdate)m_pSysInterfaceProjectImplementations[strUISysInterface];
          if (pTarget != null)// Ignore if project level not implemented
          {
            // Project code could set the referenced TerminationRequest boolean to true and return true from WriteTrailer method.
            bool bTerminationRequest = false;
            bool bReturnVal = pTarget.Project_WriteTrailer(this, strUISysInterface, ref bTerminationRequest);
            if (!bReturnVal || bTerminationRequest)
            {
              m_SysIntProcessAction[strUISysInterface] = eWorking.TERMINATE; // Terminate individual BU process
              continue;
            }
          }
        }
        catch (Exception ex)
        {
          // Log the error [NOTE: Project level implementation SHOULD make sure to catch all the errors in that code.]
          LogBatchUpdateError(ex, string.Format("System Interface = {0}", strUISysInterface));
          m_SysIntProcessAction[strUISysInterface] = eWorking.TERMINATE; // Terminate individual BU process
          continue;
        }
      }
      return;
    }

    public void ProductLevel_EndingBatchUpdate_SendFiles()
    {
      int iSysInterfaceCount = m_pSelectedSysInterfaces.getLength();
      for (int i = 0; i < iSysInterfaceCount; i++)
      {
        string strUISysInterface = m_pSelectedSysInterfaces.get(i).ToString();
        if (m_SysIntProcessAction[strUISysInterface] != eWorking.CONTINUE)
        {
          continue;
        }

        try
        {
          AbstractBatchUpdate pTarget = (AbstractBatchUpdate)m_pSysInterfaceProjectImplementations[strUISysInterface];
          if (pTarget != null)// Ignore if project level not implemented
          {
            // Project code could set the referenced TerminationRequest boolean to true and return true from WriteTrailer method.
            bool bReturnVal = pTarget.Project_SendFiles(this, strUISysInterface);
            if (!bReturnVal)
            {
              m_SysIntProcessAction[strUISysInterface] = eWorking.TERMINATE; // Terminate individual BU process
              continue;
            }
          }
        }
        catch (Exception ex)
        {
          // Log the error [NOTE: Project level implementation SHOULD make sure to catch all the errors in that code.]
          LogBatchUpdateError(ex, string.Format("System Interface = {0}", strUISysInterface));
          m_SysIntProcessAction[strUISysInterface] = eWorking.TERMINATE; // Terminate individual BU process
          continue;
        }
      }
      return;
    }

    public void WriteToActivityLog(string sAction, string sSummary, params string[] detailsList)
    {
      WriteToActivityLog(sAction, sSummary, null, false, detailsList);
    }

    public void WriteToActivityLog(string sAction, string sSummary, Exception ex, params string[] detailsList)
    {
      WriteToActivityLog(sAction, sSummary, ex, false, detailsList);
    }

    /// <summary>
    /// Records an informational entry (or error) to the Activity Log.
    /// </summary>
    /// <param name="sAction"></param>
    /// <param name="sSummary"></param>
    /// <param name="ex"></param>
    /// <param name="bAddFilesAndSystems"></param>
    /// <param name="detailsList"></param>
    public void WriteToActivityLog(string sAction, string sSummary, Exception ex, bool bAddFilesAndSystems, params string[] detailsList)
    {
      string sDetails = "";

      // Add the Update ID
      if (!string.IsNullOrEmpty(m_strUpdateID))
      {
        AddToActLogDetails("Update ID", m_strUpdateID, ref sDetails);
      }

      if (bAddFilesAndSystems)
      {
        // Get pipe delimited list of selected CORE files
        string sCoreFiles = "";
        for (int i = 0; i < m_pSelectedCoreFiles.getIntKeyLength(); i++)
        {
          // Bug 22957: Simplified formatting so that the sequence number does not lose its leading zeros
          sCoreFiles = string.Format("{0}{1}"
              , string.IsNullOrEmpty(sCoreFiles) ? "" : string.Format("{0}|", sCoreFiles)
              , m_pSelectedCoreFiles.get(i));
        }
        if (!string.IsNullOrEmpty(sCoreFiles))
        {
          AddToActLogDetails("CORE Files", sCoreFiles, ref sDetails);
        }

        // Get pipe delimited list of selected system interfaces
        string sSysInts = "";
        for (int j = 0; j < m_pSelectedSysInterfaces.getLength(); j++)
        {
          sSysInts = string.Format("{0}{1}"
              , string.IsNullOrEmpty(sSysInts) ? "" : string.Format("{0}|", sSysInts)
              , (string)m_pSelectedSysInterfaces.get(j));
        }
        if (!string.IsNullOrEmpty(sSysInts))
        {
          AddToActLogDetails("System Interfaces", sSysInts, ref sDetails);
        }
      }

      // Add the error details
      if (ex != null)
      {
        AddToActLogDetails("Error", ex.ToMessageAndCompleteStacktrace().TrimEnd('\n'), ref sDetails);
      }

      // Add any additional detail records (key-value pairs)
      if (detailsList.Length % 2 == 0)
      {
        for (int k = 0; k < detailsList.Length; k++)
        {
          if (k % 2 == 0)
          {
            AddToActLogDetails(detailsList[k], detailsList[k + 1], ref sDetails);
          }
        }
      }
      else
      {
        AddToActLogDetails("Act Log Write Error", "Details MUST be sent as key-value pairs.", ref sDetails);
      }

      // Add the Action
      if (string.IsNullOrEmpty(sAction))
      {
        sAction = "Batch Update Run";
      }

      // Write to the Activity Log:
      try
      {
        misc.CASL_call("a_method", "actlog", "args",
          new GenericObject(
              "app", "Portal"
              // Bug 20435 UMN PCI-DSS don't pass session
              , "user", m_strUserName
              , "action", sAction
              , "summary", sSummary
              , "detail", sDetails
              )
              );
      }
      catch (Exception e)
      {
        LogBatchUpdateError(e, string.Format("Exception writing Action ({0}) to Activity Log.", sAction));
      }
    }

    private void AddToActLogDetails(string sKey, string sValue, ref string sDetails)
    {
      // Replace any commas with pipes in Value:
      sValue = sValue.Replace(',', '|');

      sDetails = string.Format("{0}{1}:{2}"
      , string.IsNullOrEmpty(sDetails) ? "" : string.Format("{0},", sDetails)
      , sKey
      , sValue);
    }

    public void LogBatchUpdateError(Exception ex)
    {
      LogBatchUpdateError(ex, null, false);
    }

    public void LogBatchUpdateError(string strErrorDescription)
    {
      LogBatchUpdateError(null, strErrorDescription, false);
    }

    public void LogBatchUpdateError(Exception ex, string strErrorDescription)
    {
      LogBatchUpdateError(ex, strErrorDescription, false);
    }

    /// <summary>
    /// Records an error to the cs log flat file and activity log.
    /// </summary>
    /// <param name="ex"></param>
    /// <param name="strErrorDescription"></param>
    /// <param name="bProjectLevel"></param>
    public void LogBatchUpdateError(Exception ex, string strErrorDescription, bool bProjectLevel)
    {
      string strErrorInfo = "";
      string strExceptionInfo = ex == null ? "" : string.Format("{0}{1}", ex.ToMessageAndCompleteStacktrace(), Environment.NewLine);
      if (!string.IsNullOrEmpty(strErrorDescription))
      {
        strErrorInfo = ex == null ? string.Format("{0}{1}", strErrorDescription, Environment.NewLine) :
                                    string.Format("Error Info    : {0}{1}", strErrorDescription, Environment.NewLine);
      }
      string sLevelType =
      string.Format("=============================== {0} Level: BATCH UPDATE ERROR ==============================="
      , bProjectLevel ? "Project" : "Product");
      Logger.cs_log(string.Format("{0}{1}{0}{2}{3}{4}{0}"
      , Environment.NewLine                                                                                 // {0}
      , sLevelType                                                                                          // {1}
      , strErrorInfo                                                                                        // {2}
      , strExceptionInfo                                                                                    // {3}
      , "=================================================================================================" // {4}
      ));

      // Write error to the Activity Log
      string sAction = string.Format("Batch Update Error - {0} Level", bProjectLevel ? "Project" : "Product");
      WriteToActivityLog(sAction, strErrorDescription, ex);
    }

    /// <summary>
    /// Daniel wrote this function. Please do not change without talking to me first. ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!
    /// Let the project know that there was a problem and we need to rollback. It will be up to the project to decide what to do.
    /// NOTE: This calls Project_Rollback on ALL selected system interfaces.
    /// </summary>
    /// <param name="bEarlyTerminationRequestedByProject"></param>
    public void ProductLevel_PrjRollback(ref bool bEarlyTerminationRequestedByProject)
    {
      int iSysInterfaceCount = m_pSelectedSysInterfaces.getLength();
      for (int i = 0; i < iSysInterfaceCount; i++)
      {
        string strUISysInterface = m_pSelectedSysInterfaces.get(i).ToString();
        // Bug 12126 Mike O - Call using abstract class
        try
        {
          if (m_pSysInterfaceProjectImplementations != null)// Skip if m_pSysInterfaceProjectImplementations is null
          {
            AbstractBatchUpdate pTarget = (AbstractBatchUpdate)m_pSysInterfaceProjectImplementations[strUISysInterface];
            if (pTarget != null)// Ignore if project level not implemented
            {
              pTarget.Project_Rollback(this, bEarlyTerminationRequestedByProject);
            }
          }
        }
        catch (Exception ex)
        {
          // Log the error [NOTE: Project level implementation SHOULD make sure to catch all the errors in that code.]
          LogBatchUpdateError(ex, string.Format("System Interface = {0}", strUISysInterface));
          continue;
        }
      }
    }

    /// <summary>
    /// NOTE: This calls Project_Rollback on a single system interface.
    /// </summary>
    /// <param name="sSysInterface"></param>
    /// <param name="bEarlyTerminationRequestedByProject"></param>
    public void ProductLevel_PrjRollbackSystem(string sSysInterface, ref bool bEarlyTerminationRequestedByProject)
    {
      try
      {
        if (m_pSysInterfaceProjectImplementations != null)// Skip if m_pSysInterfaceProjectImplementations is null
        {
          AbstractBatchUpdate pTarget = (AbstractBatchUpdate)m_pSysInterfaceProjectImplementations[sSysInterface];
          if (pTarget != null)// Ignore if project level not implemented
          {
            pTarget.Project_Rollback(this, bEarlyTerminationRequestedByProject);
          }
        }
      }
      catch (Exception ex)
      {
        // Log the error [NOTE: Project level implementation SHOULD make sure to catch all the errors in that code.]
        LogBatchUpdateError(ex, string.Format("System Interface = {0}", sSysInterface));
      }
    }

    /// <summary>
    /// Daniel wrote this function. Please do not change without talking to me first. ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!
    /// Let the project know it's time to close connections and cleanup.
    /// </summary>
    /// <param name="bEarlyTerminationRequestedByProject"></param>
    public void ProductLevel_PrjCleanup(ref bool bEarlyTerminationRequestedByProject)
    {
      if (m_pSelectedSysInterfaces == null || m_pSysInterfaceProjectImplementations == null)
      {
        return;
      }

      int iSysInterfaceCount = m_pSelectedSysInterfaces.getLength();
      for (int i = 0; i < iSysInterfaceCount; i++)
      {
        string strUISysInterface = m_pSelectedSysInterfaces.get(i).ToString();
        // Bug 12126 Mike O - Call using abstract class
        try
        {
          AbstractBatchUpdate pTarget = (AbstractBatchUpdate)m_pSysInterfaceProjectImplementations[strUISysInterface];
          if (pTarget != null)// Ignore if project level not implemented
          {
            pTarget.Project_Cleanup(bEarlyTerminationRequestedByProject, this);
          }
        }
        catch (Exception ex)
        {
          // Log the error [NOTE: Project level implementation SHOULD make sure to catch all the errors in that code.]
          LogBatchUpdateError(ex, string.Format("System Interface = {0}", strUISysInterface));
          continue;
        }
      }
    }

    /// <summary>
    /// Collect all the possible transaction types and if they have system interfaces then add them to the 
    /// hashtable. This way I don't have to do this in CASL for each transaction later. This should be a
    /// nice performance gain and I can stay out of CASL as well.
    /// </summary>
    public void GetAllTransactionTypeSystemInterfaces()
    {
      if (m_pAllTransactionTypesWithSysInterfaces == null)
      {
        m_pAllTransactionTypesWithSysInterfaces = new Hashtable();
      }
      GenericObject pAllTrans = (GenericObject)((GenericObject)c_CASL.c_object("Business.Transaction.of"));
      foreach (string s in pAllTrans.getStringKeys())
      {
        GenericObject pTran = (GenericObject)pAllTrans.get(s);
        if (pTran.has("update_core_file_system_interface_list"))
        {
          m_pAllTransactionTypesWithSysInterfaces.Add(pTran.get("id"), pTran.get("update_core_file_system_interface_list"));
        }
      }

      // Now, collect the taxes (that are transactions) with system interfaces and add them to the hashtable.
      // NOTE: A tax transaction can NOT have the same ID as a regular transaction even though the config app
      // currently allows this.
      GenericObject pAllTax = (GenericObject)((GenericObject)c_CASL.c_object("Business.Transaction.Tax.of"));
      foreach (string s in pAllTax.getStringKeys())
      {
        GenericObject pTax = (GenericObject)pAllTax.get(s);
        if (pTax.has("update_core_file_system_interface_list"))
        {
          object objTaxID = pTax.get("id");
          if (m_pAllTransactionTypesWithSysInterfaces.ContainsKey(objTaxID))
          {
            // DJD TODO: This is a config problem (tax and trans with SAME id): Do not add, log the error and send notification.
          }
          else
          {
            m_pAllTransactionTypesWithSysInterfaces.Add(objTaxID, pTax.get("update_core_file_system_interface_list"));
          }
        }
      }
    }

    // Bug 14210-Collect all the tenders and their system interfaces
    public void GetAllTenderTypeSystemInterfaces()
    {
      if (m_pAllTenderTypesWithSysInterfaces == null)
      {
        m_pAllTenderTypesWithSysInterfaces = new Hashtable();
      }

      GenericObject pAllTenders = (GenericObject)((GenericObject)c_CASL.c_object("Business.Tender.of"));
      foreach (string s in pAllTenders.getStringKeys())
      {
        GenericObject pTender = (GenericObject)pAllTenders.get(s);
        // BEGIN Bug 20601 [17495] - PB Flat File Batch Update: Incorrect Transaction Total on Report
        string tenderID = pTender.get("id") as string;
        GenericObject geoBatchSIs = new GenericObject("_parent", c_CASL.GEO);
        if (pTender.has("system_interfaces"))
        {
          GenericObject geoSystemInterfaces = pTender.get("system_interfaces") as GenericObject;
          if (geoSystemInterfaces.has("_objects"))
          {
            foreach (GenericObject si in ((GenericObject)geoSystemInterfaces["_objects"]).vectors)
            {
              string siName = si.get("name", "") as string;
              if (m_pSelectedSysInterfaces.vectors.Contains(siName))
              {
                geoBatchSIs.insert(si);
              }
            }
          }
        }
        if (geoBatchSIs.getLength() > 0)
        {
          m_pAllTenderTypesWithSysInterfaces.Add(tenderID, geoBatchSIs);
        }
        // END Bug 20601 [17495] - PB Flat File Batch Update: Incorrect Transaction Total on Report
      }
    }
    //end Bug 14210

    //
    // I am assuming that if m_BatchUpdateRunCount > 0 we have an outstanding
    // request and CASLInterpreter.reload should not be allowed to run.  GMB.
    //
    public static bool ResetForCASLReload()
    {
      return m_BatchUpdateRunCount == 0;
    }
  }

  /// <summary>
    /// Class for Custom Extension Methods
    /// </summary>
    public static class CustomExtensions
    {
      /// <summary>
      /// Provides an exception extension method that is useful for logging complete error information.
      /// Use it like this:
      /// using CASL_engine; // If not in CASL_engine namespace
      /// try { Some code that may throw an exception }
      /// catch(Exception ex)
      /// {
            ///     misc.cs_log(ex.ToMessageAndCompleteStacktrace());
      /// }
      /// </summary>
      /// <param name="exception"></param>
      /// <returns></returns>
      public static string ToMessageAndCompleteStacktrace(this Exception exception)
      {
        Exception e = exception;
        StringBuilder s = new StringBuilder();
        while (e != null)
        {
        s.AppendLine(string.Format("Error Message : {0})", e.Message!=null?e.Message:""));// HX:  Bug 17849 to protect aganist null message, it is possible user provide null message
        s.AppendLine(string.Format("Exception Type: {0})", e.GetType().FullName));
        s.AppendLine(string.Format("Stack Trace   :"));
          s.AppendLine(e.StackTrace);
          s.AppendLine();
          e = e.InnerException;
        }

#if FutureLogging  // This can cause NLog to reset iPayment so leave
                   // inactive, but we really need to capture this
                   // information.
                   
        // TTS#26349 Capture CASL Stack to assist debugging.  GMB
        s.AppendLine();
        CASL_Stack.stacktrace_StrBld(s);
#endif
        return (s.ToString().Trim());
      }

    /// <summary>
    /// Bug 25177 - DJD: Adding a Truncate() method on string.
    /// </summary>
    /// <param name="value"></param>
    /// <param name="maxLength"></param>
    /// <returns></returns>
    public static string Truncate(this string value, int maxLength)
    {
      if (string.IsNullOrEmpty(value)) return value;
      return value.Length <= maxLength ? value : value.Substring(0, maxLength);
    }

    /// <summary>
    /// Provides an exception extension method that is useful for logging complete error information without a traceback.
    /// Use it like this:
    /// using CASL_engine; // If not in CASL_engine namespace
    /// try { Some code that may throw an exception }
    /// catch(Exception ex)
    /// {
    ///     misc.cs_log(ex.ToMessage());
    /// }
    /// </summary>
    /// <param name="exception"></param>
    /// <returns></returns>
    public static string ToMessage(this Exception exception)
    {
      Exception e = exception;
      StringBuilder s = new StringBuilder();
      while (e != null)
      {
        s.AppendLine("Error Message : " + e.Message);
        s.AppendLine("Exception Type: " + e.GetType().FullName);
        e = e.InnerException;
      }
      return (s.ToString().Trim());
    }

    // TTS#26349 Deleted FormatStacktraceForActivityLog(...).  GMB.
    // Use the version in misc.cs instead.

    // Bug 18129 DJD
    public static string ConvertToUnsecureString(this SecureString secureStr)
        {
            if (secureStr == null)
                throw new ArgumentNullException("secureStr");

            IntPtr unmanagedString = IntPtr.Zero;
            try
            {
                unmanagedString = Marshal.SecureStringToGlobalAllocUnicode(secureStr);
                return Marshal.PtrToStringUni(unmanagedString);
  }
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(unmanagedString);
            }
        }
    }

  // Bug 12126 Mike O - Changed to extend AbstractBatchUpdate, added GetCASLTypeName, fixed Project_ProcessCoreEvent to handle the whole event during one call
  public class ProjectLevelImplementationOfSystemInterface_SAMPLE : AbstractBatchUpdate
  {
    // Daniel wrote this class. Please do not change without talking to me first.
    // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

    public OutputConnection m_pOutputConnection = null;

    public override string GetCASLTypeName()
    {
      return "sample";
    }

    //****************************************************************
    // Required function - MUST BE IMPLEMENTED IN PROJECT LEVEL CLASS
    public override bool Project_ProcessCoreEvent(string strSystemInterface, GenericObject pEventGEO, GenericObject pConfigSystemInterface/*Access to the Config System Interface*/, GenericObject pFileGEO/*Bug 8182 DH*/, object pProductLevelMainClass/*Bug 8224 DH*/, string strCurrentFile/*Bug 8224 DH*/, ref eReturnAction ReturnAction/*Bug 15464*/)
    {
      // Daniel wrote this function. Please do not change without talking to me first. ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      //-------------------------------------------------------------------------------------------------------------------------------
      // Bug 8224 DH
      // Any errors generated in this function that you want to display on the batch update end report add here.
      // You should not need reflection to get back to the main class, just use the full path to it.
      CASL_engine.BatchUpdate_ProductLevel pBatchUpdate_ProductLevel = (BatchUpdate_ProductLevel)pProductLevelMainClass;
      //-------------------------------------------------------------------------------------------------------------------------------


      // THIS IS A SAMPLE PROJECT LEVEL SYSTEM INTERFACE IMPLEMENTATION
      try
      {

        if (m_pOutputConnection == null)
        {
          // Access to the Config System Interface
          string strFilePath = pConfigSystemInterface.get("path").ToString();

          m_pOutputConnection = new OutputConnection();
          m_pOutputConnection.m_pOutputFile = new StreamWriter(strFilePath + "\\sample_system_interface.txt");
        }
      }
      catch (Exception e)
      {
        m_pOutputConnection = null;
        string strErr = string.Format("Invalid System Interface file path. ERROR: {0}", e.Message);
        if (strErr.Length >= 240)
          strErr = strErr.Substring(0, 240);

        pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError(strCurrentFile, strSystemInterface, strErr);
        return false; // Bug 10153 DH -  I am removing this because it confuses Rebecca. This is only a sample interface but that is all QA tests and they get funny results here. 
      }

      //Transactions
      try
      {
        for (int eventInd = 0; eventInd < ((GenericObject)pEventGEO.get("TRANSACTIONS")).getLength(); eventInd++)
        {
          GenericObject pTran = (GenericObject)((GenericObject)pEventGEO.get("TRANSACTIONS")).get(eventInd);
          if (pTran != null)
          {
            //******************************************************************************
            //*********************Transaction level ONLY!**********************************
            //******************************************************************************
            // Transaction level ONLY!
            // Simpler output here for testing performance relative 
            // to the last batch Update implementation.
            System.Text.StringBuilder strTransactionRecord = new System.Text.StringBuilder();
            strTransactionRecord.Append("*TRANSACTION:*");
            strTransactionRecord.Append(pTran.get("DEPFILENBR").ToString());
            strTransactionRecord.Append(pTran.get("DEPFILESEQ").ToString());
            strTransactionRecord.Append(pTran.get("EVENTNBR").ToString());
            strTransactionRecord.Append(pTran.get("TRANNBR").ToString());
            strTransactionRecord.Append(pTran.get("TTDESC").ToString());
            strTransactionRecord.Append(pTran.get("TRANAMT").ToString());
            m_pOutputConnection.m_pOutputFile.WriteLine(strTransactionRecord.ToString());
            // Bug 12304 Mike O - Clear out Builder after output
            strTransactionRecord = new System.Text.StringBuilder();
            //continue;// Remove this if you want to run all available data
            //******************************************************************************
            //******************************************************************************
            //******************************************************************************

            //------------------------------------------------------
            // Transaction level

            // Now for each tender insert a record into the file
            // Bug 12126 Mike O - Allow this to work even if there are no tenders
            GenericObject pTTATenders = (GenericObject)pTran.get("TTA_TENDERS", new GenericObject());
            int iTTATenderCnt = (int)pTTATenders.getLength();
            for (int i = 0; i < iTTATenderCnt; i++)
            {
              GenericObject pTndr = (GenericObject)pTTATenders.get(i);

              strTransactionRecord.Append("*TRANSACTION:*");
              strTransactionRecord.Append(pTran.get("DEPFILENBR").ToString());
              strTransactionRecord.Append(pTran.get("DEPFILESEQ").ToString());
              strTransactionRecord.Append(pTran.get("EVENTNBR").ToString());
              strTransactionRecord.Append(pTran.get("TRANNBR").ToString());
              strTransactionRecord.Append(pTran.get("TTDESC").ToString());
              strTransactionRecord.Append(pTran.get("TRANAMT").ToString());

              strTransactionRecord.Append("-" + pTndr.get("TNDRDESC").ToString() + "**");
            }
            m_pOutputConnection.m_pOutputFile.WriteLine(strTransactionRecord.ToString());
          }

          //------------------------------------------------------------------------------------------------------
          // Write out all allocations for the current transaction
          if (pTran.has("TG_ITEM_DATA"))
          {
            GenericObject pAllocations = (GenericObject)pTran.get("TG_ITEM_DATA");
            int lAllocationCount = (int)pAllocations.getLength();
            for (int j = 0; j < lAllocationCount; j++)
            {
              System.Text.StringBuilder strAllocationRecord = new System.Text.StringBuilder();
              strAllocationRecord.Append("*ALLOCATION:*");

              GenericObject pAllocation = (GenericObject)pAllocations.get(j);

              strAllocationRecord.Append(pAllocation.get("ITEMDESC").ToString());
              strAllocationRecord.Append(pAllocation.get("QTY").ToString());
              strAllocationRecord.Append(pAllocation.get("TOTAL").ToString() + "**");

              m_pOutputConnection.m_pOutputFile.WriteLine(strAllocationRecord.ToString());
            }
          }
          //------------------------------------------------------------------------------------------------------

          //------------------------------------------------------------------------------------------------------
          // Write out all the custom fields for the current transaction.
          if (pTran.has("GR_CUST_FIELD_DATA"))
          {
            GenericObject pCustomFields = (GenericObject)pTran.get("GR_CUST_FIELD_DATA");
            if (pCustomFields != null)
            {
              int lCustomFieldsCount = (int)pCustomFields.getLength();
              for (int n = 0; n < lCustomFieldsCount; n++)
              {
                System.Text.StringBuilder strCustomFieldRecord = new System.Text.StringBuilder();
                strCustomFieldRecord.Append("*CUSTOM FIELD:*"); // Bug 12304 Mike O - Typo fix

                GenericObject pCustomField = (GenericObject)pCustomFields.get(n);

                if (pCustomField.has("CUSTLABEL"))
                  strCustomFieldRecord.Append(pCustomField.get("CUSTLABEL").ToString());

                if (pCustomField.has("CUSTVALUE"))
                  strCustomFieldRecord.Append(pCustomField.get("CUSTVALUE").ToString());

                strCustomFieldRecord.Append(pCustomField.get("TTID").ToString() + "**");

                m_pOutputConnection.m_pOutputFile.WriteLine(strCustomFieldRecord.ToString());
              }
            }
          }
        }
        //------------------------------------------------------------------------------------------------------
      }
      catch (Exception e)// Bug 8224 DH
      {
        string strErr = e.Message;
        if (strErr.Length >= 240)
          strErr = strErr.Substring(0, 240);
        pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError(strCurrentFile, strSystemInterface, strErr);
        return true; // This is just a sample, don't want to interrupt anything.
      }

      return true;
    }

    public override bool Project_ProcessDeposits(GenericObject pFileDeposits, GenericObject pConfigSystemInterface, object pProductLevelMainClass/*Bug 8224 DH*/, string strSysInterface/*Bug 8224 DH*/, GenericObject pCurrentFile/*Bug 8332*/, ref eReturnAction ReturnAction)
    {
      // Daniel wrote this function. Please do not change without talking to me first. ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      //-------------------------------------------------------------------------------------------------------------------------------
      // Bug 8332 DH
      // NOTE: OVERSHORT EVENT IS UNDER "SYSOVERSHORT_EVENTNBR_n" IN pCurrentFile.
      //-------------------------------------------------------------------------------------------------------------------------------

      //-------------------------------------------------------------------------------------------------------------------------------
      // Bug 8224 DH
      // Any errors generated in this fucntion that you want to display on the batch update end report add here.
      // You should not need reflection to get back to the main class, just use the full path to it.
      CASL_engine.BatchUpdate_ProductLevel pBatchUpdate_ProductLevel = (BatchUpdate_ProductLevel)pProductLevelMainClass;
      //-------------------------------------------------------------------------------------------------------------------------------

      // THIS IS A SAMPLE PROJECT LEVEL SYSTEM INTERFACE IMPLEMENTATION
      if (m_pOutputConnection == null)
      {
        // Access to the Config System Interface
        string strFilePath = pConfigSystemInterface.get("path").ToString();

        m_pOutputConnection = new OutputConnection();
        m_pOutputConnection.m_pOutputFile = new StreamWriter(strFilePath + "\\sample_system_interface.txt");
      }

      System.Text.StringBuilder strStr = new System.Text.StringBuilder();
      m_pOutputConnection.m_pOutputFile.WriteLine("**********DEPOSITS**********");
      if (pFileDeposits != null)
      {
        string strFileName = pFileDeposits.get("FULL_FILE_NAME").ToString();
        m_pOutputConnection.m_pOutputFile.WriteLine("FILE: " + strFileName);

        int iDepositCount = (int)pFileDeposits.getIntKeyLength();
        for (int i = 0; i < iDepositCount; i++)
        {
          try
          {
            GenericObject pDeposit = (GenericObject)pFileDeposits.get(i);
            string strDepositID = pDeposit.get("DEPOSITID").ToString();
            string strDepositNbr = pDeposit.get("DEPOSITNBR").ToString();
            GenericObject pTenders = (GenericObject)pDeposit.get("DEPOSIT_TENDERS");
            int iTendersCount = (int)pTenders.getIntKeyLength();
            for (int j = 0; j < iTendersCount; j++)
            {
              GenericObject pTender = (GenericObject)pTenders.get(j);
              string strTenderDesc = pTender.get("TNDRDESC").ToString();
              // Bug 12055 Mike O - Use misc.Parse to log errors
              string strTenderAmt = string.Format("${0:#.00}", misc.Parse<Double>(pTender.get("AMOUNT").ToString()));
              m_pOutputConnection.m_pOutputFile.WriteLine(strDepositID + "-" + strDepositNbr + "-" + strTenderAmt + "-" + strTenderDesc);
            }
          }
          catch (Exception e)
          {
            // Bug 8224 DH
            // Column is only 250 characters.
            string strErr = e.Message;
            if (strErr.Length >= 240)
              strErr = strErr.Substring(0, 240);

            pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError(strFileName, strSysInterface, strErr);
          }
        }
      }
      m_pOutputConnection.m_pOutputFile.WriteLine("********END DEPOSITS********");

      return true;
    }

    public override bool Project_Cleanup(bool bEarlyTerminationRequestedByProject, object pProductLevelMainClass/*Bug 8224 DH*/)
    {
      // Daniel wrote this class. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      // Use this function to close any open files and connections.

      // Lets close the text file I was writing to.
      if (m_pOutputConnection != null)
      {
        string strError = "";
        m_pOutputConnection.CloseConnection(ref strError);
      }

      Logger.LogInfo("", "END Project_Cleanup");
      return true;
    }

    public override void Project_Rollback(object pProductLevelMainClass/*Bug 8224 DH*/, bool bEarlyTerminationRequestedByProject)
    {
      // Daniel wrote this class. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      // This function is used in case the TG_UPDATE_DATA cannot be updated and system interface needs to rollback any changes.
      Logger.LogInfo("", "END Project_Rollback");
    }

    public override bool Project_WriteTrailer(object pProductLevelMainClass/*Bug 8224 DH*/, string strSysInterface, ref bool bEarlyTerminationRequestedByProject)
    {
      // DISABLE AFTER TESTING FUNCTIONALITY SO THIS ERROR DOES NOT SHOW UP IN THE REPORT
      return true;

      // Bug# Bug 27153 DH - Clean up warnings when building ipayment

      // Daniel wrote this class. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      // Do not close your connections in this function. 
      // There will be another call at end to give you chance to rollback any issues and close all connections.

      //-------------------------------------------------------------------------------------------------------------------------------
      // Bug 8224 DH
      // Any errors generated in this fucntion that you want to display on the batch update end report add here.
      // You should not need reflection to get back to the main class, just use the full path to it.
      // BatchUpdate_ProductLevel pBatchUpdate_ProductLevel = (BatchUpdate_ProductLevel)pProductLevelMainClass;
      // If you do not have a filename then this error will show on a report for system interfaces not individual files.
      // pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError(/*File name here if available*/BU_Const.sSYS_INT_SCOPE, strSysInterface, "test trailer error");
      //-------------------------------------------------------------------------------------------------------------------------------


      //-------------------------------------------
      // Write trailers;
      //if (m_pOutputConnection != null && m_pOutputConnection.m_pOutputFile != null)
      //{
      // Write the trailer for this system interface. If there is a problem, you will have to decide how to proceed
      // you can return true and the process will continue writing out rest of the trailers or you can return false and stop everything.
      // make sure you close any connections and cleanup any temp files before you return false and quit.
      // m_pOutputConnection.m_pOutputFile.WriteLine("******************TRAILER RECORDS HERE******************");
      //m_pOutputConnection.m_pOutputFile.WriteLine("*************END OF SAMPLE SYSTEM INTERFACE*************");

      // SAMPLE: return false to terminate the WHOLE batch update process
      //}
      //-------------------------------------------			

      //Logger.LogInfo("", "END Project_WriteTrailer");
      //return true;
    }
    //****************************************************************
  }
}
