using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace CASL_engine
{
  /// <summary>
  /// Summary description for Crypto.
  /// </summary>
  public class Crypto
  {
    private static String secret_key="Ye1/fHWFsvZRhxFj9AjONIzeen85v+fO";//This line is changed programmatically.  MP: we did consider trying to hide this secret_key, but a hacker would just look for our CS calls to Security.Cryptography
    static readonly object lock_obj = new object();// Bug# 14264 DH - Group Health hash problem fix.

    // Bug #11495 Mike O - Always return a new CryptoServiceProvider (because MS lies when they say they're thread-safe)
    // Bug #10955 Mike O - Speed up hash calculation
    private static class CryptoProviders
    {
      public static MD5CryptoServiceProvider MD5_CSP // Can only be instantiated if FIPS compliance is disabled
      {
        get
        {
            try
            {
            return new MD5CryptoServiceProvider();
            }
            catch (InvalidOperationException /*e*/) //12081 NJ-unused variable
            {
              string hash_algorithm = (c_CASL.get_app_settings().Get("hash_algorithm") == null ? "" :
                (c_CASL.get_app_settings().Get("hash_algorithm")));
              if (hash_algorithm.Equals("MD5"))
              {
                Logger.cs_log("ERROR: The MD5 hash algorithm is not supported on this platform. Please correct the hash_algorithm value in the web.config.");
                throw CASL_error.create_str("Unsupported hash algorithm specified");
              }
              return null;
            }
          }
      }

      public static SHA1CryptoServiceProvider SHA1_CSP
      {
        get
        {
          return new SHA1CryptoServiceProvider();
        }
      }

      public static SHA256CryptoServiceProvider SHA256_CSP // Cannot be instantiated in Windows XP
      {
        get
        {
            try
            {
            return new SHA256CryptoServiceProvider();
            }
            catch (InvalidOperationException /*e*/)//12018 Nj-unused variable
            {
              string hash_algorithm = (c_CASL.get_app_settings().Get("hash_algorithm") == null ? "" :
                (c_CASL.get_app_settings().Get("hash_algorithm")));
              if (hash_algorithm.Equals("SHA256"))
              {
                Logger.cs_log("ERROR: The SHA256 hash algorithm is not supported on this platform. Please correct the hash_algorithm value in the web.config.");
                throw CASL_error.create_str("Unsupported hash algorithm specified");
              }
              return null;
            }
          }
        }
      }

    public static HashAlgorithm DefaultHashAlgorithm
    {
      get
      {
        // Bug #10955 Mike O - Switched from test_mode to hash_algorithm
        // Bug #8856 Mike O - Use new hash algorithm; the choice of new algorithm depends upon the "hash_algorithm" flag
        string hash_algorithm = (c_CASL.get_app_settings().Get("hash_algorithm") == null ? "" :
          (c_CASL.get_app_settings().Get("hash_algorithm")));
        
        switch (hash_algorithm)
        {
          case "MD5":
            return CryptoProviders.MD5_CSP;
          case "SHA256":
            return CryptoProviders.SHA256_CSP;
          case "SHA1":
            return CryptoProviders.SHA1_CSP;
          default:
            Logger.cs_log("ERROR: No default hash algorithm has been specified. Please configure the hash_algorithm value in the web.config.");
            throw CASL_error.create_str("No hash algorithm specified");
        }
      }
    }
    // End Bug #10955 Mike O

    public static String get_secret_key() 
    {
      return secret_key;
    }

    // 5% faster to just call ComputeHash than to duplicate effort
    // and assignments like it was being done
    public static String CASLComputeHash(params object[] arg_list)
    {
      GenericObject args=misc.convert_args(arg_list);
      string salt=(string)args.get("salt","");
      string data=(string)args.get("data");
      return ComputeHash(salt,data);
    }
    // 18% faster than the old method, 15% faster than CASLComputeHash above
    // Bug #10955 Mike O - Moved checksum calculation into its own function
    public static String ComputeHash(string salt, string data)
    {
      return ComputeHash(salt, data, DefaultHashAlgorithm);
    }

    // Bug #10955 Mike O - Pulled into its own function
    public static String ComputeHash(string salt, string data, HashAlgorithm alg)
    {
      lock (lock_obj)// Bug# 14264 DH - Group Health hash problem fix.
      {
		byte[] bytes=System.Text.UnicodeEncoding.UTF8.GetBytes(data+salt);
      
		  byte[] hashed_bytes;
		  hashed_bytes = alg.ComputeHash(bytes);
		  return Convert.ToBase64String(hashed_bytes);
	  }
    }

    // Returns the HashAlgorithm object that's associated with the hash in question.
    // Returns null if no valid hash algorithm could be found
    public static HashAlgorithm HashAlgorithmUsed(string hash)
    {
      if ((hash.Length == 24 || hash.Length == 32) && (c_CASL.get_app_settings().Get("allow_MD5") == null ? false : Boolean.Parse(c_CASL.get_app_settings().Get("allow_MD5"))))
        return CryptoProviders.MD5_CSP;
      else if ((hash.Length == 28 || hash.Length == 40) && (c_CASL.get_app_settings().Get("allow_SHA1") == null ? false : Boolean.Parse(c_CASL.get_app_settings().Get("allow_SHA1"))))
        return CryptoProviders.SHA1_CSP;
      else if ((hash.Length == 44 || hash.Length == 60) && (c_CASL.get_app_settings().Get("allow_SHA256") == null ? false : Boolean.Parse(c_CASL.get_app_settings().Get("allow_SHA256"))))
        return CryptoProviders.SHA256_CSP;
      else
        return null;
    }
    
    private static byte[] get_secret_key_as_bytes (object secret_key) 
    {
      byte[] secret_key_bytes=null;
      if (secret_key==c_CASL.c_opt || secret_key==null) { secret_key=Crypto.get_secret_key(); }

      if (secret_key is int) 
      {
        SymmetricAlgorithm crypto_svc=new TripleDESCryptoServiceProvider();
        crypto_svc.KeySize=(int)secret_key;
        crypto_svc.GenerateKey();
        secret_key_bytes=crypto_svc.Key;
      }
      else if (secret_key is string)  // assume base64 string
        secret_key_bytes=Convert.FromBase64String((string)secret_key);  // needs base64 decoding.
      else if (secret_key is byte[])
        secret_key_bytes=(byte[])secret_key;
      else
        throw CASL_error.create_str("not a valid secret_key format");
      return secret_key_bytes;
    } 

    public static GenericObject symmetric_encrypt(params object[] arg_list) 
    {
      GenericObject args = misc.convert_args(arg_list);

      object data = args.get("data");
      
      // Bug# 11809 make sure the value was passed in. DH 10/12/2011.
      bool IsASCIIString = false;
      if(args.has("is_ascii_string"))
        IsASCIIString = (bool)args.get("is_ascii_string", false); // BUG 9831 DH

      // Bug #12801 Mike O - Use provided secret key, if available
      object secret_key = args.get("secret_key", null);

      byte[] data_bytes = null;

      if (data is String)
      {
          if (IsASCIIString)// BUG 9831 & 10904 DH
              data = Convert.ToBase64String(System.Text.UnicodeEncoding.UTF8.GetBytes((string)data));
          
          data_bytes = Convert.FromBase64String((string)data);
      }
      else
          data_bytes = (byte[])data;
    
      byte[] tripleDesIV = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 };
      // Bug #12801 Mike O - Use provided secret key, if available
      byte[] secret_key_bytes = get_secret_key_as_bytes(secret_key);
      TripleDESCryptoServiceProvider pProvider = new TripleDESCryptoServiceProvider();
      ICryptoTransform pCryptoTransform = pProvider.CreateEncryptor(secret_key_bytes, tripleDesIV);
      MemoryStream encryptedStream = new MemoryStream();
      CryptoStream cryptStream = new CryptoStream(encryptedStream, pCryptoTransform, CryptoStreamMode.Write);
      cryptStream.Write(data_bytes, 0, data_bytes.Length);
      cryptStream.FlushFinalBlock();
      encryptedStream.Position = 0;
      Byte[] bResult = new Byte[encryptedStream.Length];
      encryptedStream.Read(bResult, 0, encryptedStream.ToArray().Length);
      cryptStream.Close();
      GenericObject encrypted = new GenericObject("_parent", c_CASL.c_object("encrypted"), "data", encryptedStream.ToArray(), "secret_key", Convert.ToBase64String(secret_key_bytes));
      return encrypted;
    }

    public static string generate_secret_key(params object[] arg_list) 
    {
      GenericObject args = misc.convert_args(arg_list);
      int size = (int)args.get("size",1024);

      SymmetricAlgorithm crypto_svc=new TripleDESCryptoServiceProvider();
      crypto_svc.GenerateKey();
      return Convert.ToBase64String(crypto_svc.Key);
    }

    public static byte[] symmetric_decrypt(params object[] arg_list)
    {
      GenericObject args = misc.convert_args(arg_list);
      object data = args.get("data");
      // Bug #12801 Mike O - Use provided secret key, if available
      object secret_key = args.get("secret_key", null);
      byte[] keyArray = get_secret_key_as_bytes(secret_key);

      byte[] arrDataBytes = null;
      if (data is string)  // assume base64 string
      {
        if(data.ToString().Length == 0)
          arrDataBytes = Convert.FromBase64String("KauYRbzhEak=");
        else
          arrDataBytes = Convert.FromBase64String((string)data);  // needs base64 decoding.
      }
      else if (data is byte[])
        arrDataBytes = (byte[])data;
      else
        throw CASL_error.create_str("not a valid data format");

      byte[] tripleDesIV = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 };

      TripleDESCryptoServiceProvider pProvider = new TripleDESCryptoServiceProvider();
      ICryptoTransform pTranform = pProvider.CreateDecryptor(keyArray, tripleDesIV);
      MemoryStream decryptedStream = new MemoryStream();
      CryptoStream cryptStream = new CryptoStream(decryptedStream, pTranform, CryptoStreamMode.Write);
      cryptStream.Write(arrDataBytes, 0, arrDataBytes.Length);
      cryptStream.FlushFinalBlock();
      decryptedStream.Position = 0;
      Byte[] result = new Byte[decryptedStream.Length];
      decryptedStream.Read(result, 0, decryptedStream.ToArray().Length);
      decryptedStream.Close();
      return result;
    }

    /// <summary>
    /// Decrypt URL safely, since base64 is not sufficient because it has +, / and = chars 
    /// which are problematic in a URL.
    /// Bug 17045 UMN 
    /// </summary>
    /// <param name="arg_list"></param>
    /// <returns></returns>
    public static String symmetric_decrypt_url(params object[] arg_list)
    {
      GenericObject args = misc.convert_args(arg_list);
      string data = args.get("_subject") as string;

      byte[] keyArray = Convert.FromBase64String((string)secret_key);

      byte[] arrDataBytes = misc.Base64UrlDecode(data);

      byte[] tripleDesIV = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 };

      TripleDESCryptoServiceProvider pProvider = new TripleDESCryptoServiceProvider();
      ICryptoTransform pTranform = pProvider.CreateDecryptor(keyArray, tripleDesIV);
      MemoryStream decryptedStream = new MemoryStream();
      CryptoStream cryptStream = new CryptoStream(decryptedStream, pTranform, CryptoStreamMode.Write);
      cryptStream.Write(arrDataBytes, 0, arrDataBytes.Length);
      cryptStream.FlushFinalBlock();
      decryptedStream.Position = 0;
      Byte[] result = new Byte[decryptedStream.Length];
      decryptedStream.Read(result, 0, decryptedStream.ToArray().Length);
      decryptedStream.Close();
      return System.Text.Encoding.Default.GetString(result);
  }

    /// <summary>
    /// Encrypt URL safely, since base64 is not sufficient because it has +, / and = chars 
    /// which are problematic in a URL.
    /// Bug 17045 UMN 
    /// Cut-down version of original. Only supports String.
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public static string symmetric_encrypt_url(params object[] arg_list)
    {
      GenericObject args = misc.convert_args(arg_list);
      string data = args.get("_subject") as string;
      byte[] data_bytes = System.Text.UnicodeEncoding.UTF8.GetBytes(data);

      byte[] tripleDesIV = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 };

      // Bug #12801 Mike O - Use provided secret key, if available
      byte[] secret_key_bytes = Convert.FromBase64String((string)secret_key);
      using (TripleDESCryptoServiceProvider pProvider = new TripleDESCryptoServiceProvider())
      {
        ICryptoTransform pCryptoTransform = pProvider.CreateEncryptor(secret_key_bytes, tripleDesIV);
        using (MemoryStream encryptedStream = new MemoryStream())
        {
          using (CryptoStream cryptStream = new CryptoStream(encryptedStream, pCryptoTransform, CryptoStreamMode.Write))
          {
            cryptStream.Write(data_bytes, 0, data_bytes.Length);
            cryptStream.FlushFinalBlock();
            encryptedStream.Position = 0;
            Byte[] bResult = new Byte[encryptedStream.Length];
            encryptedStream.Read(bResult, 0, encryptedStream.ToArray().Length);
          }
          return misc.Base64UrlEncode(encryptedStream.ToArray());
        }
      }
    }
    public static string fCASL_encrypt_source_files(CASL_Frame frame)
    {
      GenericObject geo_args = frame.args;
      string hdr = misc.CASL_get_code_base();
      for (int i = 0; i < CASLInterpreter.FileCount; i++) {
        CASLInterpreter.CASL_File file = CASLInterpreter.FileIndex[i];
        if (file.file_name.IndexOf(".casl") > 0) {
          string fn = hdr + "/" + file.file_name;
          if (file.file_name != "config.casl") {
            misc.execute_file_aux_write_encrypted(fn, get_secret_key(), "e");
          } else {
            if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
              Flt_Rec.log("fCASL_encrypt_source_files",
                          "skipping " + fn);
          }
        }
      }
      //
      // TODO: Reset globals and reload.  GMB
      //
      return "Encrypted files (*.casle) have been created.";
    }

    //Bug 24786 - SM This adds back the functionality that was in IPaymentSupportFunctions.cs for encrypting casl files in the upgrade folder.
    public static string fCASL_encrypt_upgrade_files(CASL_Frame frame)
    {
        GenericObject geo_args = frame.args;
        string hdr = misc.CASL_get_code_base();

        DirectoryInfo upgradeDir = null;

            try
            {
                upgradeDir = new DirectoryInfo(misc.CASL_get_code_base() + "/upgrade");
            }
            catch (Exception e) // If there is no upgrade directory, don't bother just display message.
            {
                return e + "  " + "Check if upgrade directory exists!";
            }

            // Get all of the unprocessed CASL files
            FileInfo[] files = upgradeDir.GetFiles("*.casl");

            foreach (FileInfo file in files)
            {
                misc.execute_file_aux_write_encrypted(file.FullName, Crypto.get_secret_key(), "e");
            }
            //
            // TODO: Reset globals and reload.  GMB  SM - Dont htink this was ever finished.
            //
            return "Encrypted upgrade files (*.casle) have been created.";
    }


    }
}
