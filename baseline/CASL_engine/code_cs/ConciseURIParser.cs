// ConciseURIParser.java
// 21 JAN 2004, Merrick J. Stemen
// 22 JAN 2004, Merrick J. Stemen
// 04 FEB 2004, Merrick J. Stemen
// 06 FEB 2004, Merrick J. Stemen

// Parses ConciseURI syntax using a state machine
// based on Water source code by Mike Plusch,
// Clearmethods, Inc.
using System;

namespace JAVA
{
	public class ConciseURIParser 
	{
	
		private char current_event;
		//private int unkey;
		private int status;
		private System.String a_uri;
		private System.Text.StringBuilder field;
		private System.String key;
	
		//private System.Collections.ArrayList current_object_Renamed_Field;
		public System.Collections.ArrayList nested_objects;
		private System.Collections.ArrayList nested_objects_keys;
	
		public ConciseURIParser(System.String a_uri)
		{
			status = ConciseURIStates.STARTING_FIELD;
			this.a_uri = a_uri;
			//unkey = 0;
			nested_objects = new System.Collections.ArrayList(10);
			nested_objects.Add(new GenericObject());
			nested_objects_keys = new System.Collections.ArrayList(10);
			nested_objects_keys.Add(null);
		}
	
		private GenericObject current_object()
		{
			return (GenericObject) SupportClass.VectorLastElement(nested_objects);
		}	
	
		private void  start_keyed_object()
		{
			nested_objects.Add(new GenericObject());
			nested_objects_keys.Add(field.ToString());
			status = ConciseURIStates.STARTING_FIELD;
		}
	
		private void  end_object_name()
		{
			current_object().Name = field.ToString();
		}
	
		private void  start_unkeyed_object()
		{
			nested_objects.Add(new GenericObject());
			nested_objects_keys.Add(null);
		}
	
		private void  end_unkeyed_value()
		{
			current_object().addUnkeyedValue(field.ToString());
		}
	
		private void  end_keyed_value()
		{
			current_object().addKeyedValue(key, field);
		}
	
		private void  end_keyed_null_value()
		{
			current_object().addKeyedValue(key, null);
		}
	
		private void  end_unkeyed_null_value()
		{
			current_object().addUnkeyedValue(null);
		}
	
		private void  end_object_and_keyed_value()
		{
			end_keyed_value();
			end_object();
		}
	
		private void  end_object_and_keyed_null_value()
		{
			end_keyed_null_value();
			end_object();
		}
	
		private void  end_object_and_unkeyed_null_value()
		{
			end_unkeyed_null_value();
			end_object();
		}
	
		private void  end_object_and_unkeyed_value()
		{
			end_unkeyed_value();
			end_object();
		}
	
		private void  end_object()
		{
			System.Object temp_object;
			temp_object = nested_objects[nested_objects.LastIndexOf(SupportClass.VectorLastElement(nested_objects))];
			nested_objects.RemoveAt(nested_objects.LastIndexOf(SupportClass.VectorLastElement(nested_objects)));
			System.Object R = temp_object;
			// This object needs to be removed from the vector;
			System.Object temp_object2;
			temp_object2 = nested_objects_keys[nested_objects_keys.LastIndexOf(SupportClass.VectorLastElement(nested_objects_keys))];
			nested_objects_keys.RemoveAt(nested_objects_keys.LastIndexOf(SupportClass.VectorLastElement(nested_objects_keys)));
			System.Object K = temp_object2;
			// This object needs to be removed from the vector;
			if (K == null)
			{
				current_object().addUnkeyedValue(R);
			}
			else
			{
				current_object().addKeyedValue(K, R);
			}
		}
	
		private void  error_occurred()
		{
			//System.out.println("Calling error_occurred()\n");
		}
	
		private void  start_field()
		{
			field = new System.Text.StringBuilder(current_event + "");
			//System.out.println("Calling start_field()\n" + field.toString() );
		}
	
		private void  start_value()
		{
			field = new System.Text.StringBuilder(current_event + "");
			//System.out.println("Calling start_value()\n" + field.toString() );
		}
	
		private void  end_key()
		{
			key = field.ToString();
			//System.out.println("Calling end_key()\n" + key );
		}
	
		private void  add_to_field()
		{
			field.Append(current_event);
			//System.out.println("Calling add_to_field()\n" + field.toString() );
		}
	
		/*
		**
		** public void run()
		***/
		public virtual void  Run()
		{
		
			for (int i = 0; i < a_uri.Length; i++)
			{
				current_event = a_uri[i];
				process_event();
			}
		
			//System.out.println( "Reached end of input." );
		
			if (status != ConciseURIStates.STARTING_FIELD)
			{
				//System.out.println( "Status at end of input is " + status + "." );
				current_event = '&';
				process_event();
			}
		
			if (nested_objects.Count != 1)
			{
				System.Console.Out.WriteLine("Number of nested_objects on exit = " + nested_objects.Count);
			}
			else
			{
				//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Object.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
				System.Console.Out.WriteLine("Object = " + nested_objects[0].ToString());
			}
		
			return ;
		}
	
		private void  process_event()
		{
			switch (status)
			{
			
				case ConciseURIStates.STARTING_FIELD: 
				{
					switch (current_event)
					{
					
						case '~': 
							error_occurred();
							status = ConciseURIStates.ERROR_STATE;
							break;
					
						case '&': 
							end_unkeyed_null_value();
							break;
					
						case '(': 
							start_unkeyed_object();
							break;
					
						case ')': 
							end_object();
							break;
					
						case '=': 
							error_occurred();
							status = ConciseURIStates.ERROR_STATE;
							break;
					
						default: 
							start_field();
							status = ConciseURIStates.ADDING_TO_FIELD;
							break;
					
					}
					break;
				}
			
				case ConciseURIStates.ADDING_TO_FIELD: 
				{
					switch (current_event)
					{
					
						case '~': 
							end_object_name();
							status = ConciseURIStates.STARTING_FIELD;
							break;
					
						case '&': 
							end_unkeyed_value();
							status = ConciseURIStates.STARTING_FIELD;
							break;
					
						case '(': 
							error_occurred();
							status = ConciseURIStates.ERROR_STATE;
							break;
					
						case ')': 
							end_object_and_unkeyed_value();
							status = ConciseURIStates.STARTING_FIELD;
							break;
					
						case '=': 
							end_key();
							status = ConciseURIStates.STARTING_VALUE;
							break;
					
						default: 
							add_to_field();
							break;
					
					}
					break;
				}
			
				case ConciseURIStates.STARTING_VALUE: 
				{
					switch (current_event)
					{
					
						case '~': 
							error_occurred();
							status = ConciseURIStates.ERROR_STATE;
							break;
					
						case '&': 
							end_keyed_null_value();
							status = ConciseURIStates.STARTING_FIELD;
							break;
					
						case '(': 
							start_keyed_object();
							status = ConciseURIStates.STARTING_FIELD;
							break;
					
						case ')': 
							error_occurred();
							status = ConciseURIStates.ERROR_STATE;
							break;
					
						case '=': 
							error_occurred();
							status = ConciseURIStates.ERROR_STATE;
							break;
					
						default: 
							start_value();
							status = ConciseURIStates.ADDING_TO_VALUE;
							break;
					
					}
					break;
				}
			
				case ConciseURIStates.ADDING_TO_VALUE: 
				{
					switch (current_event)
					{
					
						case '~': 
							error_occurred();
							status = ConciseURIStates.ERROR_STATE;
							break;
					
						case '&': 
							end_keyed_value();
							status = ConciseURIStates.STARTING_FIELD;
							break;
					
						case '(': 
							error_occurred();
							status = ConciseURIStates.ERROR_STATE;
							break;
					
						case ')': 
							end_object_and_keyed_value();
							status = ConciseURIStates.STARTING_FIELD;
							break;
					
						case '=': 
							error_occurred();
							status = ConciseURIStates.ERROR_STATE;
							break;
					
						default: 
							add_to_field();
							break;
					
					}
					break;
				}
			
				case ConciseURIStates.ERROR_STATE: 
				{
					add_to_field();
					break;
				}
			
				default: 
				{
					System.Console.Out.WriteLine("Error: Invalid machine state (" + status + ").");
				}
					break;
			
			}
		}
	}

	class ConciseURIStates
	{
		public const int STARTING_FIELD = 1;
		public const int ADDING_TO_FIELD = 2;
		public const int STARTING_VALUE = 3;
		public const int ADDING_TO_VALUE = 4;
		public const int ERROR_STATE = 5;
	}

	class GenericObject
	{
		virtual public System.String Name
		{
			set
			{
				id = value;
			}
		
		}
		private System.String id;
		private System.Collections.ArrayList fields;
		private System.Collections.ArrayList values;
		//private int unkey;
	
		internal GenericObject()
		{
			id = "GenericObject";
			fields = new System.Collections.ArrayList(10);
			values = new System.Collections.ArrayList(10);
		}
	
		public virtual void  addUnkeyedValue(System.Object a_thing)
		{
			fields.Add(null);
		
			values.Add(a_thing);
		}
	
		public virtual void  addKeyedValue(System.Object a_key, System.Object a_thing)
		{
			fields.Add(a_key);
			values.Add(a_thing);
		}
	
		//	public override System.String ToString()
		//	{
		//		unkey = 0;
		//		System.Object key;
		//		//System.out.println( "GenericObject.toString() called." );
		//		System.Text.StringBuilder sb = new System.Text.StringBuilder("<" + id + " ");
		//		//System.out.println( 1 );
		//		//System.out.println( "fields.size()=" + fields.size() + 
		//		//                    ";values.size()=" + values.size());
		//		
		//		//System.Collections.ArrayList efields = fields;
		//		//System.Collections.ArrayList evalues = values;
		//		//UPGRADE_TODO: Method 'java.util.Enumeration.hasMoreElements' was converted to 'System.Collections.IEnumerator.MoveNext' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
		//		foreach ( Object obj in fields)
		//		{
		//			//UPGRADE_TODO: Method 'java.util.Enumeration.nextElement' was converted to 'System.Collections.IEnumerator.Current' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
		//			key = efields.Current;
		//			if (key == null)
		//				key = System.Convert.ToString(unkey++);
		//			//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Object.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
		//			//UPGRADE_TODO: Method 'java.util.Enumeration.nextElement' was converted to 'System.Collections.IEnumerator.Current' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
		//			efields.MoveNext();
		//			System.String s = key + "=" + evalues.Current + " ";
		//			//         System.out.println( s );
		//			sb = sb.Append(s);
		//		}
		//		//      System.out.println( 2 );
		//		sb = sb.Append("/>");
		//		//      System.out.println( 3 );
		//		
		//		return sb.ToString();
		//	}
	}
	/// <summary>
	/// Contains conversion support elements such as classes, interfaces and static methods.
	/// </summary>
	public class SupportClass
	{
		/// <summary>
		/// Returns the last element of an ArrayList instance.
		/// </summary>
		/// <param name="arrayList">The ArrayList instance</param>
		/// <returns>The last element of the ArrayList</returns>  
		public static System.Object VectorLastElement(System.Collections.ArrayList arrayList)
		{
			return arrayList[arrayList.Count - 1];
		}

		/// <summary>
		/// Returns the last element of a Stack instance.
		/// </summary>
		/// <param name="stack">The Stack instance</param>
		/// <returns>The last element of the Stack</returns>
		public static System.Object VectorLastElement(System.Collections.Stack stack)
		{
			return stack.ToArray()[0];
		}


	}
}