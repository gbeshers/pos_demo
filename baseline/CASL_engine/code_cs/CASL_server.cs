// File created by Daniel Hofman on 04/05/2004

using System;
using System.Web;
using System.Reflection;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Xml;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Collections.Concurrent;
using System.Collections.Generic;

/*
  ERROR CODES FOR CASL_SERVER ARE IN THE  SV-NNN .... 
  PLEASE ALWAYS USE A SPECIFIC ERROR CODE FOR EACH ERROR.
  
    SV-200 Runtime Error 
    SV-210 Decode URI Error
    SV-211 Decode URI Error
    SV-212 Security Permission Error 
    SV-213 Decode URI Error 
    SV-300 Formatting Error (Generic)
    SV-301 Formatting Error (Exception)
    SV-302 Formatting error (TargetInvocationException)
    SV-303 Formatting Error
    SV-304 Server Runtime Error (Exception)
    SV-305 Server Runtime Error (Exception with InnerException)
    SV-322 Server invalid request
    SV-323 Session Expired... 
 
*/
namespace CASL_engine
{

  /*
   * CASL_server.init_server()
   * CASL_server.process_request
   * CASL_server.decode_uri
   *   
   * CASL_server.execute_code
   * CASL_server.format
   * 
   * 
   */
  public class CASL_server
  {

        public static readonly IDictionary<string, string> uploadfileformatwhitelist =
    new ConcurrentDictionary<string, string>(
        new List<KeyValuePair<string, string>>
        {
            new KeyValuePair<string, string>("text/plain", "txt"),
            new KeyValuePair<string, string>("application/rtf", "rtf"),
            new KeyValuePair<string, string>("application/pdf", "pdf"),
            new KeyValuePair<string, string>("application/msword", "doc"),
            new KeyValuePair<string, string>("application/vnd.openxmlformats-officedocument.wordprocessingml.document", "docx"),
            new KeyValuePair<string, string>("application/vnd.ms-excel", "xls"),
            new KeyValuePair<string, string>("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "xlsx"),
            new KeyValuePair<string, string>("application/vnd.ms-powerpoint", "ppt"),
            new KeyValuePair<string, string>("application/vnd.openxmlformats-officedocument.presentationml.presentation", "pptx"),
            new KeyValuePair<string, string>("application/x-zip-compressed", "zip"),
            new KeyValuePair<string, string>("image/gif", "gif"),
            new KeyValuePair<string, string>("image/jpeg", "jpeg"),
            new KeyValuePair<string, string>("image/bmp", "bmp"),
            new KeyValuePair<string, string>("image/tiff", "tif"),
            new KeyValuePair<string, string>("image/png", "png")
      });
     
        /// <summary>
        /// init_server take raw_uri and return server object with a_request and a_response 
        /// set right a_request extension="", path_string="", query_string="" by uri
        /// </summary>
        /// <param name="args"> a_uri=req=string </param>
        /// <returns> 
        /// <GEO> a_request=<GEO> extension="" path_string="" query_string="" </GEO> 
        ///							a_response=<GEO> </GEO>
        /// </GEO>
        /// </returns>
        // Called by: process_request
        public static Object init_server(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      string a_uri = (string)geo_args.get("a_uri", "");
      string http_method = (string)geo_args.get("http_method", "GET");
      string content = (string)geo_args.get("content", null);
      string content_type = (string)geo_args.get("content_type", null);
      string browser = (string)geo_args.get("browser", "");

      //GenericObject S = c_CASL.c_make("server") ;
      GenericObject S = new GenericObject("_parent", c_CASL.GEO.get("server"));
      GenericObject a_request = new GenericObject("_parent", c_CASL.GEO.get("request"));
      GenericObject a_response = new GenericObject("_parent", c_CASL.GEO.get("response"));

      //----------------------------------------------
      // parse/set raw uri to path string, query_string, body_bytes, extension
      //--------------------------------------------------
      string uri_path = "";
      string query_string = null;
      string path_string = "";
      string extension = "";

      string[] UA = a_uri.Split('?'); // UA =uri array (include undecoded path_string, extension and query_string
      uri_path = UA[0];
      string[] PA = uri_path.Split('.');
      string path_no_ext = PA[0];
      if (PA.Length > 1) extension = PA[1];
      if (UA.Length > 1) query_string = UA[1];
      //string site_logical = c_CASL.GEO.get("site_logical") as string;
      //trim off logical folder  path_no_ext="/pos_demo/casl_ide" => "casl_ide"
      int protocol_index = path_no_ext.IndexOf("://");
      if (protocol_index > -1)  //  http://localhost/pos_demo ==>  /pos_demo
        path_no_ext = path_no_ext.Substring(path_no_ext.IndexOf("/", protocol_index + 3)); // strip path up until the first path part

      path_string = path_no_ext.Substring(path_no_ext.IndexOf("/", 1) + 1);  // skip first slash and find second slash.
      //path_string = path_string.Replace(site_logical+"/","");

      S.set("path_no_ext", path_no_ext);

      a_request.set("path_string", path_string);
      a_request.set("query_string", query_string);
      a_request.set("extension", extension);
      a_request.set("uri_string", a_uri);
      //System.Web.HttpRequest a_http_request = HttpContext.Current.Request;
      a_request.set("http_method", http_method);
      bool has_post = false;
      if (http_method == "POST")
      {
        has_post = true;
        a_request.set("body", content);
        a_request.set("content_type", content_type);
      }
      bool has_query = a_request.get("query_string") != null;
      //a_request.set("native_object", a_http_request);  // Who uses this??

      if (!has_query && !has_post) // use as redirect
        S.set("no_post_no_query_path_no_ext", path_no_ext);

      GenericObject my = CASLInterpreter.get_my();

      S.set("a_request", a_request);
      S.set("a_response", a_response);
      if (my != null)
      {
        my.set("current_request", S);
        my.set("browser", browser);
      }

      return S;
    }

    // called by: process_request_and_return_html in casl_client.cs
    public static string process_request_and_return_html(params Object[] args) // content_type, content  CASL_server.process_request_and_return_html
    {
      
      GenericObject a_server = CASL_server.process_request(args) as GenericObject; // return a server

      GenericObject a_response = a_server.get("a_response") as GenericObject;
      //string status_code = a_response.get("status_code", "200").ToString();

      if (a_response.has("location"))
      { // follow redirect
        //c_CASL.alert("following_redirect:" + a_response.get("location"));
        a_server = CASL_server.process_request(
         "a_uri", a_response.get("location") as string, "http_method", "GET", "content_type", null, "content", null) as GenericObject; // return a server
        a_response = a_server.get("a_response") as GenericObject;
      }

      string html_to_display = a_response.get("body_bytes", null) as string; // could return bytes.  
      /*if (html_to_display==null) 
        html_to_display=a_response.get("result", "no result") as string;
      */
      if (a_response.get("status_code", "200").Equals("204"))
        return "";  // Don't change page.
      else
        return html_to_display;
    }
    
    /// Assumes: args has string fields: a_uri is required, http_method defaults to "GET", content defaults to null, content_type defaults to null.
    // called by: process_request_and_return_html for client-side and Page_Load in handle_request_2.cs
    public static object process_request(params Object[] args)
    {
      // HX: Bug17849 Note: it already logged inner exception and stacktrace

      // args in process_request are directly passed into init_server.
      GenericObject a_server = null;
      Object RS = null;
      CASL_Frame orig_frame = CASL_Stack.stack_top;

      try
      {
        // TODO: take a http request object and convert it to our a_server.a_request
        a_server = init_server(args) as GenericObject;
        a_server = decode_uri(0, a_server) as GenericObject;

        // Bug #11869 Mike O - If the session isn't valid, return a session invalid message instead of the desired action
        string appURI = null;
        bool sessionValid = validate_session((GenericObject)a_server.get("a_request", null), out appURI);
        if (!sessionValid)
        {
          return process_request("a_uri", appURI);
        }

        a_server = execute_code(0, a_server);
      }
      catch (CASL_error ce)
      {
        RS = ce.casl_object;
        misc.error_add_stack_trace(RS as GenericObject, ce.StackTrace);
        (a_server.get("a_response") as GenericObject).set("result", RS);
      }
      catch (Exception e)
      {
        GenericObject the_error = null;
        Exception ie = e.InnerException; // ie=inner exception
        // normal 
        if (ie == null)
        {
          the_error = c_CASL.c_make("error", "title", "Server Runtime Error", "code", "SV-304", "message", e.Message, "throw", false) as GenericObject;
          misc.error_add_stack_trace(the_error, e.StackTrace);
          //Logger.cs_log(e.Message + Environment.NewLine + e.StackTrace);
        }
        else if (ie is CASL_error)
        {
          the_error = (ie as CASL_error).casl_object;
        }
        else // ie is a regular nested exception, like Bad image(preh DLL) leading to TargetInvocation exception
        {
          the_error = c_CASL.c_make("error", "title", "Server Runtime Error", "code", "SV-305", "message", ie.Message, "throw", false) as GenericObject;
          misc.error_add_stack_trace(the_error, ie.StackTrace);      
          
        }
        // HX: 17849
        Logger.cs_log(e.ToMessageAndCompleteStacktrace() +
                      Environment.NewLine +
                      CASL_Stack.stacktrace_StrBld(null, 3).ToString());
        
        // set the error as the response to be sent to the client
        (a_server.get("a_response") as GenericObject).set("result", the_error);
      }
      CASL_Stack.pop_to(orig_frame);
      try
      {
        a_server = format(0, a_server) as GenericObject;
      }
      catch (TargetInvocationException te) // BUG#13858 
      {
        /*string the_error_message = te.Message;
        Exception ie = te.InnerException; // ie=inner exception
        Exception ex = te;
        if (ie != null)
        {
          the_error_message = ie.Message;
          ex = ie;
        }*/

        // HX: bug 17849, found first innerexception's error message
        string the_error_message = "";
        Exception ex = te;
        while(ex != null)
        {
          if(ex.Message != null)
            the_error_message = ex.Message;
          ex = te.InnerException;
        }

        GenericObject the_error = c_CASL.c_make("error", "title", "Formatting error", "code", "SV-302", "message", the_error_message, "throw", false) as GenericObject;
       
        misc.error_add_stack_trace(the_error, te.StackTrace);
        (a_server.get("a_response") as GenericObject).set("result", the_error);
        (a_server.get("a_request") as GenericObject).set("extension", "htm");
        a_server = format(0, a_server) as GenericObject;

        // HX: 17849
        Logger.cs_log(te.ToMessageAndCompleteStacktrace() +
                      Environment.NewLine +
                      CASL_Stack.stacktrace_StrBld(null, 3).ToString());
      }
      catch (Exception e)
      {
        /*string the_error_message = e.Message;
        Exception ie = e.InnerException; // BUG#13858  ie=inner exception
        Exception ex = e;
        if (ie != null)// BUG#13858  error include both exception error and inner exception error
        {
          if (ie.Message != null) // Bug 23712: Protect against null inner exception message
          {
          the_error_message = ie.Message;
          ex = ie;
        }
        }*/

        // HX: bug 17849, found first innerexception's error message
        string the_error_message = "";
        Exception ex = e;
        while (ex != null)
        {
          if (ex.Message != null)
            the_error_message = ex.Message;
          ex = e.InnerException;
        }

        // Bug 26202 UMN first arg mus be the class you are making!
        GenericObject the_error = c_CASL.c_make("error", "title", "Formatting error", "code", "SV-301", "message", the_error_message, "throw", false) as GenericObject;
        misc.error_add_stack_trace(the_error, e.StackTrace);
        (a_server.get("a_response") as GenericObject).set("result", the_error);
        (a_server.get("a_request") as GenericObject).set("extension", "htm");
        a_server = format(0, a_server) as GenericObject;

        // HX: 17849
        Logger.cs_log(e.ToMessageAndCompleteStacktrace() +
                      Environment.NewLine +
                      CASL_Stack.stacktrace_StrBld(null, 3).ToString());
      }

      /* if result is an error, the log to a file */
      object result = a_server.get_path("a_response.result", false, false);
      if (misc.base_is_a(result, c_CASL.c_error))
      {
        GenericObject the_error = result as GenericObject;
        String logMessage = $"error={the_error.get("title", "Runtime Error", true)} " +
          $"error_code={the_error.get("code", "SV-200", true)} " +
          $"message={the_error.get("message", "No message")}";
        Logger.Log(logMessage, "process_request", "err");
      }
      CASL_Stack.pop_to(orig_frame);
      return a_server;
    }
  
  // Bug #11869 Mike O - Is the current request in a valid session? Return the URI to call in the out argument if not
  private static bool validate_session(GenericObject a_request, out string appURI)
  {
    appURI = null;

      if (a_request == null)
        return true;

      string path = (string)a_request.get("path_string", "");

      if (path.Contains("display_session_expired"))
        return true;

      string[] splitPath = path.Split('/');

      int appId = 0;
      if (splitPath.Length < 3 || !splitPath[0].Equals("my") || !Int32.TryParse(splitPath[1], out appId))
        return true;

      GenericObject my = CASLInterpreter.get_my();
      GenericObject app = my.get(appId, null) as GenericObject;

      if (app == null)
        return true;

      // If this is an Authentication app, look one level deeper
      if (((app.get("_parent") as GenericObject).get("_parent") as GenericObject).get("_name").Equals("Authentication"))
      {
        if (app.getLength() == 0)
          return true;

        app = app.get(app.getLength() - 1) as GenericObject;
      }

      string appName = (app.get("_parent") as GenericObject).get("_name") as string;

      bool result = (bool)misc.CASL_call("subject", my, "a_method", "check_active_session", "args", new GenericObject("app_name", appName, "id", appId));

      string siteLogical = (string)c_CASL.GEO.get("site_logical");
      appURI = siteLogical + "/display_session_expired.htm?id=" + appId.ToString();
      return result;
    }

    /// <summary>
    /// set_request_object()
    /// set_args()
    /// </summary>
    /// <param name="args"> 0=req=Server</param>
    /// <returns></returns>
    // Called by: process_request
    public static object decode_uri(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      GenericObject a_server = geo_args.get(0) as GenericObject;
      //--------------------------------------------------
      // TODO: check error for a_response.error
      if ((a_server.get("a_response") as GenericObject).has("result"))
      {
        return a_server;
      }

      //--------------------------------------------------
      set_request_object(0, a_server);
      if ((a_server.get("a_response") as GenericObject).has("result"))
        return a_server;

      // SET REFERRER HOST
      GenericObject my = CASLInterpreter.get_my();
      if ((HttpContext.Current != null) && HttpContext.Current.Request.UrlReferrer != null)
      {
        string referrer_host = HttpContext.Current.Request.UrlReferrer.Host;
        if (my != null) //Bug#7989 To remove error log on the initial Load.
          my.set("referrer_host", referrer_host);
      }
      else  // Bug 15184 UMN - set referrer_host even if HttpContext.Current.Request.UrlReferrer is null
      {
        if (my != null) my.set("referrer_host", "NoReferrer");
      }

      // Bug 17505 MJO - Store client's IP address for VeriFone Mx915 support
      string ipAddress = HttpContext.Current.Request.UserHostAddress;

      if (ipAddress == "::1")
        ipAddress = "127.0.0.1";

      if (my != null) my.set("ip_address", ipAddress);

      compare_referrer_and_last_uri(a_server);

      if ((a_server.get("a_response") as GenericObject).has("result"))
        return a_server;

      //--------------------------------------------------
      // set args by query_string
      set_args(0, a_server);
      return a_server;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="args">0=req=Server</param>
    /// <returns></returns>
    // Called by: process_request ONLY.
    public static GenericObject execute_code(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      GenericObject a_server = geo_args.get(0) as GenericObject;
      GenericObject a_request = a_server.get("a_request") as GenericObject;
      GenericObject a_response = a_server.get("a_response") as GenericObject;
      CASL_Frame orig_frame = CASL_Stack.stack_top;
      //--------------------------------------------------
      // TODO: check error for a_response.error, if error return 
      if ((a_server.get("a_response") as GenericObject).has("result"))
        return a_server;

      //--------------------------------------------------
      // handle _no_call 
      GenericObject request_args = a_request.get("args") as GenericObject;

      // Bug #12959 Mike O - Call the method, even if there are no arguments
      string caslPath = ((String)a_request.get("path_string")).Replace('/', '.');
      object target = c_CASL.c_object(caslPath);
      if (request_args == null && ((String)a_request.get("uri_string")).Contains(".htm") &&
           target != null && target is GenericObject &&
           !(target as GenericObject).has("htm_inst") &&
           (target as GenericObject).get("_parent").Equals(c_CASL.c_object("method")))
        request_args = new GenericObject();

      bool perform_call = ((request_args != null) && (!(request_args as GenericObject).has("_no_call")));
      object R = null; // R=result

      //--------------------------------------------------
      // execute the requested_object w/wo args
      if (perform_call)
      {
        GenericObject requested_object = a_request.get("requested_object") as GenericObject;
        //--------------------------------------------------
        // TODO: handle _set and remove _set from a_request.args
        //if ( !request_args.has("_subject") && a_request.has("subject"))
        //{
        //	 request_args.set("_subject", a_request.get("subject"));
        //}
        if (requested_object.has("the_server_request"))    // any method that has a param named the_server_request 
        {
          request_args.set("the_server_request", a_request);
        }
        /* TTS 13613: Removing this excessive logging here */
        try
        {
          // validate args req and type before calling casl method
          if (a_request.has("subject"))
            R = misc.CASL_call("a_method", requested_object, "args", request_args, "subject", a_request.get("subject"));
          else
            R = misc.CASL_call("a_method", requested_object, "args", request_args);
        }
        //    catch(CASL_unauthorized_error ce) // HANDLE UNAUTHORIZED EXCEPTION
        //    {
        //     String message = ce.casl_object.get("message") as string;
        //      // R = misc.CASL_call("a_method","Security_item.handle_unauthorized_error", "args",new GenericObject("the_error",ce.casl_object,"requested_object",requested_object));
        //    }
        catch (CASL_error ce)
        {
          R = ce.casl_object;
          String code = ((GenericObject)R).get("code", "", true) + "";
          if (code.StartsWith("UA"))// HANDLE UNAUTHORIZED EXCEPTION
          {
            R = misc.CASL_call("a_method", "Security_item.handle_unauthorized_error", "args", new GenericObject("the_error", ce.casl_object, "requested_object", a_request.get("subject")));
          }
          else
          {
            misc.CASL_call("a_method", "make_part", "args", new GenericObject("_subject", R));
            misc.error_add_stack_trace(R as GenericObject, ce.StackTrace);
          }
        }
        catch(Exception e) // HX: Bug 17849
        {
          Logger.cs_log(e.ToMessageAndCompleteStacktrace() +
                        Environment.NewLine +
                        CASL_Stack.stacktrace_StrBld(null, 3).ToString());
        }
        CASL_Stack.pop_to(orig_frame);
      }
      else
        R = a_request.get("requested_object");

      ////////////////////////////////////////////////////
      // Redirect
      ////////////////////////////////////////////////////
      //--------------------------------------------------
      // convert result to uri, if uri exists then use redirect to change http
      // else return it
      object uri = misc.CASL_call("a_method", c_CASL.c_object("to_uri"), "args", new GenericObject("_subject", R, "extension", false));
      //object uri = c_CASL.call("to_uri", "_subject",R);
      if (perform_call && !false.Equals(uri))
      {
        string history = "";
        if ((R as GenericObject).has("h"))  /* redirect to history path */
        {
          int L = ((R as GenericObject).get("h") as GenericObject).getLength();
          if (L > 0) history = "/h/" + (L - 1);
        }
        R = misc.CASL_call("a_method", "hypertext.redirect_http", "args", new GenericObject("a_uri", uri + history + "." + a_request.get("extension")));
      }
      CASL_Stack.pop_to(orig_frame);
      //--------------------------------------------------
      // set a_response.result 
      a_response.set("result", R);

      ////////////////////////////////////////////////////
      // Unwrap the nested response object(promoting)
      ////////////////////////////////////////////////////
      if (misc.base_is_a(R, c_CASL.c_object("response")))
      {
        a_server.set("a_response", R);
      }
      return a_server;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="args"> 0=req=GEO </param>
    /// <returns> a</returns>
    public static object format(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      GenericObject a_server = geo_args.get(0) as GenericObject;
      GenericObject a_request = a_server.get("a_request") as GenericObject;
      GenericObject a_response = a_server.get("a_response") as GenericObject;
      object result = a_response.get("result", c_CASL.c_GEO()); // R=result
      CASL_Frame orig_frame = CASL_Stack.stack_top;

      string extension = a_request.get("extension", "") as string;
      if (a_response.has("extension"))
        extension = a_response.get("extension", "") as string; // get extension from manual response. 

      //-------------------------------------------------------------------------
      // get formatter 
      object formatter = get_format_top("obj", result, "name", extension, "a_request", a_request);
      // handle missing formatter issue, throw error
      if (false.Equals(formatter))
      {
        a_request.set("extension", "htm");
        GenericObject a_error_obj = new GenericObject("_parent", c_CASL.c_object("error"), "throw", false);
        // Bug#8455 ANU PA-DSS - Cross-Site Scripting issue  
        String str_uri = a_request.get("uri_string", "No Request String").ToString();

        // Bug 15400 [Bug 15404] UMN PCI-DSS:Sensitive info being written to the log upon error 321
        Uri uri = new Uri(str_uri);
        str_uri = uri.GetLeftPart(UriPartial.Path);
    
        // Bug #8349 Mike O - Don't show error details when show_verbose_error is turned off
        if ((bool)c_CASL.GEO.get("show_verbose_error", false))
          {
            a_error_obj.set("message", "For request (" + HttpContext.Current.Server.HtmlEncode(str_uri) + ") extension " + extension + " is not supported.", "code", 321, "title", "Server invalid request");
            // Bug#8455 ANU PA-DSS - Cross-Site Scripting issue end 
          }
        else
          {
            // Bug #8499 Mike O - Return 404 so a completely generic error is displayed
            a_response.set("status_code", 404);
          }

        result = a_error_obj;
        formatter = get_format_top("obj", result, "name", "htm", "a_request", a_request);
        a_response.set("result", result);
      }
      //---------------------------------------------
      //	calls formatter on response.result to set a_reponse's body_bytes 
      //	if a_response has "body_bytes", get formatter from extension get_format_top()
      if (a_response.has("result") && !(a_response.has("body_bytes")))
      {
        object BS = "";
        try
        {
          BS = misc.CASL_call("a_method", formatter, "args", new GenericObject("_subject", result)); // BS=Body string
        }
        catch (CASL_error ce)
        {
          if (result is GenericObject && c_CASL.c_error.Equals((result as GenericObject).get("_parent", "")))
          {
            // MP: should write to activity log if there is an error here.
            BS = "Error in CASL_server.format (1) Request: " + a_request.get("uri_string") + "<br/>" + misc.CASL_to_htm_large("_subject", (ce as CASL_error).casl_object);
          }
          else
          {
            GenericObject a_error_obj = ce.casl_object;
            a_error_obj.set("title", "Formatting Error", "code", "SV-300");
            misc.error_add_stack_trace(a_error_obj, ce.StackTrace);
            (a_server.get("a_response") as GenericObject).set("result", a_error_obj);
            a_server = format(0, a_server) as GenericObject;
            CASL_Stack.pop_to(orig_frame);
            return a_server;
          }
        }
        catch (Exception e)
        {
          Exception ie = e.InnerException; // ie=inner exception
          // normal 
          if (ie is CASL_error)
          {
            if (result is GenericObject && c_CASL.c_error.Equals((result as GenericObject).get("_parent", "")))
              BS = "Error in CASL_server.format (2) Request: " + a_request.get("uri_string") + "<br/>" + misc.CASL_to_htm_large("_subject", (ie as CASL_error).casl_object);
            else
            {
              GenericObject a_error_obj = (ie as CASL_error).casl_object;
              a_error_obj.set("title", "Formatting Error", "code", "SV-303");
              misc.error_add_stack_trace(a_error_obj, ie.StackTrace);
              (a_server.get("a_response") as GenericObject).set("result", a_error_obj);
              a_server = format(0, a_server) as GenericObject;
              CASL_Stack.pop_to(orig_frame);
              return a_server;
            }
          }
          else
          {
            throw;
          }
        }

        // if to_*_top formatter returns a 'response' object, use that response for the server's response.
        GenericObject BS_g = BS as GenericObject;
        if ((BS_g != null) &&
            (BS_g.get("_parent", null) == c_CASL.c_object("response")) &&
            BS_g.has("body_bytes")
            )
        {
          a_server.set("a_response", BS_g);
          a_response = BS_g;
        }
        else
        {
          //---------------------------------------------
          //	to set a_response.body_byte
          a_response.set("body_bytes", BS);
        }
      }
      //---------------------------------------------
      //	to set a_reponse.content_type
      //	if formatter has "content_type",  set content_type=formatter.content_type
      //	else check body string to set content type
      if (!(a_response.has("content_type")))
      {
        object content_type = (formatter as GenericObject).get("content_type", "text/html");
        a_response.set("content_type", content_type);
      }
      CASL_Stack.pop_to(orig_frame);
      return a_server;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="args"> obj=req=object name=""=string a_request=request</param>
    /// <returns></returns>
    // Called by: CASL_server.format (cs) and Page_Load in handle_request_2.cs,  
    public static object get_format_top(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      object obj = geo_args.get("obj");
      object name = geo_args.get("name", "");

      string FN = "to_" + name + "_top";
      if (geo_args.has("a_request"))
      {
        object a_request = geo_args.get("a_request");
        string PS = (a_request as GenericObject).get("path_string", "") as string;
        if ((PS.StartsWith("casl_ide") && !PS.StartsWith("casl_ide/execute_and_format")) ||
         PS.StartsWith("console"))
        {
          if (c_CASL.GEO.has("casl_ide"))
            obj = c_CASL.GEO.get("casl_ide");
        }
      }
      return misc.base_get(obj, FN, true, false);
    }

    /// <summary>
    /// set a_request.request_object by a_request.path_string
    /// </summary>
    /// <param name="args"> 0=req=Server</param>
    /// <returns> a_server with set a_request.request_object</returns>
    public static object set_request_object(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      GenericObject a_server = geo_args.get(0) as GenericObject;
      GenericObject a_request = a_server.get("a_request") as GenericObject;

      string PS = a_request.get("path_string", "").ToString();

      //-------------------------------------------------------
      // set PO = path objects
      GenericObject RO = c_CASL.c_GEO() as GenericObject; // root is GEO
      // RO = requested object
      //GenericObject RO = c_CASL.c_object("GEO") as GenericObject; // root is GEO
      GenericObject PO = new GenericObject(0, RO);   // PO = current path object
      ArrayList PA = new ArrayList(PS.Split('/'));  // PA = path array
      if (PS.StartsWith("/")) PS = PS.Substring(1);  // PS = path string
      if ("".Equals(PS))
        a_request.set("requested_object", RO);
      else
      {
        object S = null; // S = object at each level as you decend the path
        object key = null;
        object V = null;             // V = value to insert into
        //Object wrap= false;
        int count = PA.Count;
        foreach (object PP in PA)    // PP = path part
        {
          int index = PA.IndexOf(PP);
          //MessageBox.Show("hello");
          key = int_or_string_from(0, PP);
          S = PO.get(PO.getLength() - 1); // S=decends path
          ////////////////////////////////////////////////////////////
          /// GET VALUE AS REQUREST OBJECT PATH PART
          if (key is Int32)  // KEY_IS_INT
          {
            // lookup as integer key
            V = misc.base_get(S, key, true, c_CASL.c_undefined);
            if (c_CASL.c_undefined.Equals(V)) // KEY_IS_STRING
            {
              key = PP; // reset key as string
              V = misc.base_get(S, PP, true, c_CASL.c_undefined);
            }
          }
          else  // KEY_IS_STRING
          {
            V = misc.base_get(S, key, true, c_CASL.c_undefined);

            // Bug 17917 MJO - If the lookup failed and the string is quoted, try removing the quotes
            if (c_CASL.c_undefined.Equals(V) && key is String && ((string)key).StartsWith("'") && ((string)key).EndsWith("'"))
              V = misc.base_get(S, ((string)key).Substring(1, ((string)key).Length - 2), true, c_CASL.c_undefined);
          }

          if (c_CASL.c_undefined.Equals(V)) // NO_VALUE
          {
            GenericObject a_error_obj = new GenericObject("_parent", c_CASL.c_object("error"), "throw", false);
            //======= Handle BOOKMARK Issue BUG4352 ============
            if (CASLInterpreter.get_my().Equals(S)) //Bookmark my/1.htm
            {
              a_error_obj.set("title", "Session Expired...", "code", "SV-323", "message", "");
            }
            //======= Handle BOOKMARK Issue ============
            else
            {
              string message = string.Format("{0} not found in {1}", misc.to_eng(key), misc.to_eng(S));
              // Bug #8349 Mike O - Don't show error details when show_verbose_error is turned off
              if ((bool)c_CASL.GEO.get("show_verbose_error", false))
              {
                a_error_obj.set("title", "Server invalid request", "code", "SV-322", "message", message, "key", key, "subject", S);
              }
              else
              {
                // Bug #8499 Mike O - Return 404 so a completely generic error is displayed
                (a_server.get("a_response") as GenericObject).set("status_code", 404);
              }
            }
            (a_server.get("a_response") as GenericObject).set("result", a_error_obj);

            return null;
          }
          PO.insert(V);
          ////////////////////////////////////////////////////////////

          ///////////////////////////////////////////////
          /// SET MY.WRAP
          ///  V is the object down request path , lookup on V htm_wrap, if it not false, 
          /*
           if( false.Equals(wrap) && 
           V is GenericObject && 
           (V as GenericObject).has("htm_wrap",true) && !(V as GenericObject).has("master_app",true) && 
           !c_CASL.GEO.get("casl_ide",false,false).Equals((V as GenericObject)) &&
           a_request.get("extension","").Equals("htm")
           )
           wrap = V;// set first one has htm_wrap
          */
          ////////////////////////////////////////////////////////////

          ////////////////////////////////////////////////////////////
          /// CHECK SECURITY for the last concheck index and count to 
          if (index == count - 1)// IS_LAST_URI_PATH_PART 
          {
            bool check_security = (bool)c_CASL.GEO.get("check_security", false, false);
            string access = null;
            if (count == 1) // URI_HAS_SINGLE_PATH_PART
            {
              // check container (my,GEO, etc. ) and its field access 
              object single_object = PO.get(PO.getLength() - 1);
              if (check_security && single_object is GenericObject)
              {
                GenericObject container = ((GenericObject)single_object).get("_container", null) as GenericObject;
                if (container != null)
                {
                  access = container.get(key + "_f_access", false, true) as string; // CHECK xx_f_access with LOOKUP
                  if (access == null && key is int)// IS_VECTOR_KEY , Check _other_unkeyed_f_access 
                    access = container.get("_other_unkeyed_f_access", false, true) as string;
                }
              }
            }
            else if (index == count - 1) // URI_HAS_MULTI_PATH_PART
            {
              object subject = PO.get(PO.getLength() - 2);
              if (check_security && subject is GenericObject)
              {
                access = ((GenericObject)subject).get(key + "_f_access", false, true) as string;
                if (access == null && key is int)// IS_VECTOR_KEY , Check _other_unkeyed_f_access 
                  access = ((GenericObject)subject).get("_other_unkeyed_f_access", false, true) as string;
              }
              a_request.set("subject", subject); //SET server.a_request.subject 
            }
            // Bug #7252 Mike O - Added conditions to allow for CASL IDE to override security when inspecting objects
            // Bug #7605 Mike O - Only allow inspection when the CASL IDE is enabled...
            // Bug 26202 MJO - Or if it's coming from the Config report
            if (check_security && !(("call_method".Equals(key) || access == "public") ||
              ((c_CASL.GEO.has("casl_ide", false) || CASLInterpreter.get_my().Contains("report_config")) && a_request.get("extension", false, true).ToString().Equals("inspect"))))
            {
              bool use_client = c_CASL.GEO.get("use_client", false, false).Equals(true);
              if (!use_client)
              {
                //
                // TTS#???? GMB.  Can't 
                GenericObject my_spe = CASLInterpreter.get_my();
                string additional_log = "";
                if (my_spe != null)
                  additional_log = "  login_user=" + my_spe.get("login_user", "no_user");
                if ((HttpContext.Current != null) &&
                    HttpContext.Current.Request.UrlReferrer != null) {
                  additional_log += "  ip=" + HttpContext.Current.Request.UserHostAddress;
                  additional_log += "  referrer_host=" +
                    HttpContext.Current.Request.UrlReferrer.Host;
                }
                Logger.cs_log("security: no security permissions for path: " + PS +
                              additional_log, Logger.cs_flag.error);
                Logger.cs_log("    security: dumping a_uri " +
                              a_server.get("a_uri", "no uri??").ToString());
              }
              throw CASL_error.create("title", "Security Permission Error", "code", "SV-212", "message", "Invalid URI, No security permission for " + PS);
            }
          }
          ////////////////////////////////////////////////////////////
        }

        /*if( !false.Equals(wrap))
        {
         GenericObject my= CASLInterpreter.get_my();
         my.set("wrap",wrap);
         System.Diagnostics.Debug.WriteLine(string.Format("Wrap in server is {0}",misc.to_eng(wrap)));
        }
        */
      }
      object request_object = PO.get(PO.getLength() - 1);
      a_request.set("requested_object", request_object);//SET server.a_request.requested_object 

      return null;
    }

        /// <summary>
        /// query_string_to_object()
        /// </summary>
        /// <param name="args">0=req=Server </param>
        /// <returns> no return </returns>
        // Called by: decode_uri 
        public static object set_args(params Object[] args)
        {
            GenericObject geo_args = misc.convert_args(args);
            GenericObject a_server = geo_args.get(0) as GenericObject;
            GenericObject a_request = a_server.get("a_request") as GenericObject;
            string content_type = (string)a_request.get("content_type", null);

            if (((string)(a_request.get("http_method"))).CompareTo("POST") != -1 &&  // POST method with text/xml content
                 content_type.StartsWith("text/xml")
               )
            {
                misc.CASL_call("a_method", "process_http", "args",
                 new GenericObject("a_request", a_request)
                );  // need to use safe execution.  (no method calls)
                return null;
            }
            // else url-form-encoded
            object QS = a_request.get("query_string", null);

            if (QS == null)
            {
                a_request.set("args", null);
                return null;
            }
            else if ("".Equals(QS))
            {
                a_request.set("args", new GenericObject());
                return null;
            }

            //----------------------------------------------------
            // handle/decode args by checking content type
            // content-type(xml, soap, xmlrpc, uriencoding), method(post/get) -> decode_uri , decode_xml
            object value = query_string_to_object("query_string", QS.ToString(), "requested_object", a_request.get("requested_object"));

            a_request.set("args", value);
            if ((value as GenericObject).has("_subject"))
                a_request.set("subject", (value as GenericObject).remove("_subject"));

            // BUG16884 add files to args if available
            if (content_type != null && content_type.StartsWith("multipart/form-data"))
            /* Ideally, I want to decode the multi-part mime, and add args with name and value.
               Files would be Binary objects with a client_uri, content_type, data, etc.
               I see _multipartContentElements gives me that information, but it isn't public and
               I can't get to it.
               As a workaround, assume that any file uploads will be added as Binary CASL objects
               to the "files" parameter/argument.  No other arguments except "file" are allowed for the method.

               Alternatively, we could handle everything ourselves.  Get the raw content, parse as multipart mime,
               and extract the content.
               MP searched for example of this, and found:
               http://commons.apache.org/fileupload/
               It was hard to find.  Used Google search: "multipart/form-data" parser open-source
               I couldn't connect to the SVN server to look at the files, and it looks quite large.
            */
            {
                GenericObject file_list = c_CASL.c_GEO();
                // GenericObject form_data = new GenericObject("files", file_list);  // HACK: see above
                // a_request.set("args", form_data);
                System.Web.HttpFileCollection raw_files = System.Web.HttpContext.Current.Request.Files;
                for (System.Int32 i = 0; i < raw_files.Count; i++)
                {
                    System.Web.HttpPostedFile a_raw_file = raw_files[i];
                    GenericObject a_casl_file = c_CASL.c_make("Binary") as GenericObject;

                    byte[] content_as_bytes = new byte[a_raw_file.InputStream.Length];
                    a_raw_file.InputStream.Read(content_as_bytes, 0, (int)a_raw_file.InputStream.Length);
                    a_casl_file.set("bytes", content_as_bytes);

                    a_casl_file.set("client_uri", a_raw_file.FileName);
                    string sFileConetentType = a_raw_file.ContentType;
                    a_casl_file.set("content_type", sFileConetentType);
                    if(a_raw_file.FileName!="")//Bug 24822 NK- File Upload: not selecting a file for an optional field results in log error
                    { 
                      if (uploadfileformatwhitelist.ContainsKey(sFileConetentType))//use white list ot limit file type to upload
                          file_list.insert(a_casl_file);
                      else
                      {
                          string sLogMessage = String.Format("File Content Type({0}) is not valid.", sFileConetentType);
                          Logger.LogWarn(sLogMessage, "Upload file"); 
                        }
                    }
            }
                if (file_list.getLength() > 0)
                    (value as GenericObject).set("files", file_list);
            }

            return null;
        }

    /// <summary>
    /// loop with query_sting split by &
    ///   set_nested_uri(key, value)
    /// </summary>
    /// <param name="args"> 
    /// query_string=req=string
    /// requested_object=opt=object </param>
    /// <returns> </returns>
    // Called by: decode_uri
    public static object query_string_to_object(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      string QS = geo_args.get("query_string").ToString();//	QS=qeury String
      object requested_object = geo_args.get("requested_object", c_CASL.c_opt);//	QS=qeury String

      GenericObject QO = new GenericObject(); //	QO=qeury object
      ArrayList QA = new ArrayList(QS.Split('&')); // QA=query string array list
      foreach (object QP in QA)// QP=query string array list part
      {
        ArrayList S = new ArrayList(QP.ToString().Split('='));
        object K = null;
        object V = null;
        if (S.Count == 1)
        {
          K = "unkeyed_arg";
          V = S[0];
        }
        else
        {
          K = S[0];
          V = S[1];
        }

        if ("".Equals(V))  // has no value
        {
          if (!K.Equals("_args")) // special case to handle IIS workaround where _args=null
          {
            // Check class/method contract, if found and not req, pass opt to call method
            object param_value = (requested_object as GenericObject).get(K, c_CASL.c_undefined);
            if (!c_CASL.c_undefined.Equals(param_value) && !param_value.Equals(c_CASL.c_req))
            {
              set_nested_uri("root", QO, "path", K, "new_value", "opt", "a_method", requested_object);
            }
            else if ((K as string).IndexOf(".") != -1)
            {
              set_nested_uri("root", QO, "path", K, "new_value", V, "a_method", requested_object);
            }
          }
        }
        else
        {
          set_nested_uri("root", QO, "path", K, "new_value", V, "a_method", requested_object);
        }
      }
      return QO;
    }

    /// <summary>
    /// convert_to_key_from
    /// 
    /// </summary>
    /// <param name="args"> 
    /// root=req=GEO 
    /// path=req=string 
    /// new_value=req=GEO
    /// a_method=opt
    /// </param>
    /// <returns></returns>
    // Called by: query_string_to_object
    public static object set_nested_uri(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      object root = geo_args.get("root");//	root is the low-level object which may be nested in args. 
      object path = geo_args.get("path");//	
      object new_value = geo_args.get("new_value");//	
      GenericObject a_method = geo_args.get("a_method", c_CASL.c_opt) as GenericObject;

      //----------------------------------------
      // handle dot notation "." as nested object 
      //----------------------------------------
      object the_key = path;

      if (path is String)
      {
        string s_key = path.ToString();
        if (s_key.IndexOf(".") >= 0) // IS_KEY_PATH
        {
          string[] keys = s_key.Split('.');
          the_key = keys[keys.Length - 1]; // the last part of key path
          for (int i = 0; i < keys.Length - 1; i++)
          {
            object K = convert_to_key_from(0, keys[i]);
            if (!(root as GenericObject).has(K))
              (root as GenericObject).set(K, c_CASL.c_GEO());
            root = (root as GenericObject).get(K);
          }
        }
      }
      the_key = convert_to_key_from(0, the_key);

      //////////////////////////////////////////////////////////
      /// process value
      /// 1. validate value based on request object contract - req, type
      ///       validate on first part of key path, which is the args 
      ///        on request object 
      /// 2. validate value based on nested object in args - root
      ///       validate the last part of key path based on cascading contract 
      ///       of request object
      object the_value = null;
      object a_type = a_method.get_type(the_key); // get type from request_object
      if (c_CASL.c_opt.Equals(a_method))
        the_value = convert_to_value_from("the_string_value", new_value);
      else
      {
        //				if( !a_type.Equals(false) )
        the_value = convert_to_value_from("the_string_value", new_value, "type", a_type);
        //				else
        //					the_value = convert_to_value_from(0,new_value);

        //				object a_type = c_CASL.c_undefined;
        //				if( the_key.Equals("source") )
        //					a_type = c_CASL.c_object("String"); // TODO: replace with metakey in contract. 
        //				//object a_type = (a_method as GenericObject).get(misc.field_key(the_key,"type"),c_CASL.c_undefined);
        //				if( ! a_type.Equals(c_CASL.c_undefined) )
        //					the_value = convert_to_value_from(0,new_value, "type",a_type);
        //				else
        //					the_value = convert_to_value_from(0,new_value);
      }
      // <if> <or> a_type.<is> vector </is> a_type.<is_a> vector_of </is_a> </or>
      //          root.<set_value> the_key <v/> </set_value>     make sure the field exists for holding multiple values
      if (!a_type.Equals(false))
      {
        if ((a_type as GenericObject).get("_name", "").Equals("vector"))
        {
          if (!(root as GenericObject).has(the_key))
          {
            (root as GenericObject).set(the_key, c_CASL.c_make("vector"));
          }

        }
        else if ((a_type as GenericObject).has("_parent"))
        {
          if (((a_type as GenericObject).get("_parent", false) as GenericObject).get("_name", "").Equals("vector_of"))
          {
            if (!(root as GenericObject).has(the_key))
            {
              (root as GenericObject).set(the_key, c_CASL.c_make("vector"));
            }
          }
        }
      }

      set_or_insert_value("obj", root, "the_key", the_key, "new_value", the_value);
      return null;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="args">obj=req=GEO the_key=req new_value=req</param>
    /// <returns></returns>
    public static object set_or_insert_value(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      object obj = geo_args.get("obj");//	
      object the_key = geo_args.get("the_key");//	
      object new_value = geo_args.get("new_value");//	

      if (the_key.Equals("unkeyed_arg"))
        (obj as GenericObject).insert(new_value);
      //----------------------------------------
      // handle "_args"
      //----------------------------------------
      else if (the_key.Equals("_args"))
      {
        ArrayList keys = (new_value as GenericObject).getKeys();
        foreach (object key in keys)
        {
          (obj as GenericObject).set(key, (new_value as GenericObject).get(key));
        }
      }
      else
      {
        //----------------------------------------
        // to set multiple keys as vector value
        //----------------------------------------
        GenericObject geo_obj = obj as GenericObject;
        if (geo_obj.has(the_key) && !the_key.Equals("_parent"))
        {
          object current_value = geo_obj.get(the_key);
          // set second true to override false got 
          if (current_value.Equals(false) && new_value.Equals(true))
          {
            geo_obj.set(the_key, new_value);
          }
          else
          {
            GenericObject this_value = geo_obj.get(the_key, false, false) as GenericObject;
            if (
             !misc.base_is_a(this_value, c_CASL.c_object("vector")))
            {
              GenericObject vector = c_CASL.c_make("vector") as GenericObject;
              vector.insert(geo_obj.get(the_key));
              geo_obj.set(the_key, vector);
            }
            (geo_obj.get(the_key) as GenericObject).insert(new_value);
          }
        }
        else
          geo_obj.set(the_key, new_value);
      }
      return null;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="args"> 0=req </param>
    /// <returns></returns>
    // Called by: set_nested_uri
    public static object convert_to_key_from(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      object S = geo_args.get(0);
      return primitive_from(0, HttpUtility.HtmlAttributeEncode(uri_decode("_subject", S) as string)); // Bug 16644 UMN prevent XSS attack
    }

    // Called by: set_nested_uri
    public static object int_or_string_from(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      object SO = geo_args.get(0);

      string S = SO.ToString();
      if (S.Length > 0 && (S[0] == '-' || Char.IsDigit(S[0]))) // Number 
      {
        //					return (int)Convert.ChangeType(S,typeof(int));
        int temp = 0;
        try
        {
          // Bug #12055 Mike O - Use misc.Parse to log errors
          temp = misc.Parse<Int32>(S);
        }
        catch (Exception e)
        {
          // HX: Bug 17849
          Logger.cs_log(e.ToMessageAndCompleteStacktrace());
          return S;
        }
        return temp;
      }
      return S;
    }

    // Called by: convert_to_value_from
    public static object from_string_to_type(string subject, object a_type)
    {
      if (a_type.Equals(false))
        //return subject;
        return primitive_from(0, subject);
      string S = subject.ToString();
      if (a_type.Equals(c_CASL.c_Decimal) || (a_type.Equals(c_CASL.c_Number) && S.IndexOf(".") >= 0))//IS_DECIMAL
      {
        try
        {
          return Decimal.Parse(S, System.Globalization.NumberStyles.Any);
        }
        catch (Exception e)
        {
          string message = e.Message;
          throw CASL_error.create("title", "Decode URI Error", "message", message, "code", "SV-210");
        }
      }
      else if (a_type.Equals(c_CASL.c_Integer) || (a_type.Equals(c_CASL.c_Number)))// IS_INTEGER
      {
        try
        {
          return Int32.Parse(S, System.Globalization.NumberStyles.Any);
        }
        catch (Exception e)
        {
          throw CASL_error.create("title", "Decode URI Error", "message", e.Message, "code", "SV-211");
        }
      }
      else if ((a_type.Equals(c_CASL.c_Boolean) && S.Equals("true")))// IS_TRUE 
        return true;
      else if ((a_type.Equals(c_CASL.c_Boolean) && S.Equals("false")))// IS_FALSE
        return false;
      else if ((a_type.Equals(c_CASL.c_String)))
        return S;
      else if (S.Equals("null"))// IS_NULL
        return null;
      else if ("my".Equals(S))
        return CASLInterpreter.get_my();
      else if ("GEO.my".Equals(S))
        return CASLInterpreter.get_my();
      else if ((a_type is GenericObject))
      {
        try
        {
          if (((GenericObject)a_type).has("from", true)) // Use the from method for the type to translate from a string
          {
            return misc.CASL_call("a_method", "from", "_subject", a_type, "args", new GenericObject("a_value", S));
          }
          else if (a_type.Equals(c_CASL.get_GEO()) && S.IndexOf("<") != -1) // Contains a call, should be a construction
          {
            return execute_safe("_subject", S);
          }
          else if (a_type is GenericObject) // Should just be a path
          {
            return misc.CASL_execute("_subject", S);
          }
        }
        catch (Exception e)
        {
          // HX: Bug 17849
          string the_error_message = "";
          Exception ex = e;
          while (ex != null)
          {
            if (ex.Message != null)
              the_error_message = ex.Message;
            ex = e.InnerException;
          }

          string m = String.Format("Decode URI Error: {0}({1})", the_error_message, "SV-213");
          Logger.LogError(m, "CASL_engine from_string_to_type");
          Logger.cs_log(e.ToMessageAndCompleteStacktrace() +
                        Environment.NewLine +
                        CASL_Stack.stacktrace_StrBld(null, 3).ToString()); // HX: Bug 17849
          return S;
        }
      }
      return S;
    }

    /// <summary>
    /// convert a string to c# primitive type
    /// if string start with digit and has decimal point, return converted decimal
    /// if string start with digit, return converted integer
    /// if string is "true" or "false", return bolean true or false
    /// if string is "null", return null
    /// else return string itself
    /// </summary>
    /// <param name="args"> 0=req=string </param>
    /// <returns></returns>
    public static object primitive_from(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      object SO = geo_args.get(0);

      string S = SO.ToString();
      if (S.Equals(""))
      {
        return S;
      }
      else if ("-.0123456789".IndexOf(S[0]) >= 0 && (S.Length < 10)) // Number 
      {
        if (S.IndexOf(".") >= 0)// decimal
        {
          //					return (decimal)Convert.ChangeType(S,typeof(decimal));
          decimal temp = 0;
          try
          {
            // Bug #12055 Mike O - Use misc.Parse to log errors
            temp = misc.Parse<Decimal>(S);
          }
          catch (Exception e)
          {
            // HX: Bug 17849
            Logger.cs_log(e.ToMessageAndCompleteStacktrace());
            return S;
          }
          return temp;
        }
        else // interger
        {
          //					return (int)Convert.ChangeType(S,typeof(int));
          int temp = 0;
          try
          {
            // Bug #12055 Mike O - Use misc.Parse to log errors
            temp = misc.Parse<Int32>(S);
          }
          catch (Exception e)
          {
            // HX: Bug 17849
            Logger.cs_log(e.ToMessageAndCompleteStacktrace());
            return S;
          }
          return temp;
        }
      }
      else if (S.Equals("true"))// boolean true 
        return true;
      else if (S.Equals("false"))// boolean false
        return false;
      else if (S.Equals("null"))// boolean false
        return null;
      else // string
        return S;
    }
    /// <summary>
    /// </summary>
    /// <param name="args">the_string_value=req=string type=opt=GEO</param>
    /// <returns></returns>
    /// When no type is given:
    /// "1" -> "1" (Current behavior)
    /// ".1" -> ".1" (Current behavior)
    /// "1.1" -> 1.1 (Current behavior)
    /// "1." -> "1." (Current behavior)
    /// ".foo" -> ".foo" (Current behavior)
    /// "foo.bar" -> foo.bar | "foo.bar" if foo.bar fails (Current behavior)
    /// "foo.'b ar'" -> foo.'b ar' | "foo.'b ar'" if foo.'b ar' fails (Current behavior)
    /// "my" -> "my" (Current behavior)
    /// "GEO" -> "GEO" (Current behavior)
    /// "foo" -> "foo" (Current behavior)
    /// "Hi, I am a programmer. My name is Cody." -> "Hi, I am a programmer. My name is Cody." without executing (Current behavior)
    /// "Hi, I am a programmer. What is your name?" -> "Hi, I am a programmer. What is your name?" due to execute failure (Current behavior)
    /// "foo." -> "foo." (Current behavior)
    // Called by: set_nested_uri
    public static object convert_to_value_from(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      object SO = geo_args.get("the_string_value"); // SO= string object
      object type = geo_args.get("type", false); // SO= string object
      //object type = geo_args.get("type",c_CASL.c_opt); // SO= string object

      string S = SO.ToString(); // S= string
      S = uri_decode("_subject", S).ToString();

      // Bug #11366, 11474 Mike O - Use a regex to remove the contents from all quoted strings before checking to see if it should be executed
      Regex removeStrings = new Regex("(\".*\"|'.*')");
      string sEval = removeStrings.Replace(S, "\"\"");

      if (S.Equals("opt"))
        return c_CASL.c_opt;
      else if (type.Equals(c_CASL.GEO.get("String"))) // TYPE_IS_STRING
      {
        // Bug 19256 DJD: White-list single and double quotes
        // Bug 20235 UMN and & for name search.
        return HttpUtility.HtmlAttributeEncode(S)    // Bug 16644 UMN prevent XSS attack
          .Replace("&quot;", "\"")
          .Replace("&#39;", "'")
          .Replace("&amp;", "&");
      }
      else if (!type.Equals(false))
        return from_string_to_type(S, type);
      //NEEDED for argument GEOs such as vector_of types in Config -- changed vector_of types
      /*
      else if (S.StartsWith("<") && S.EndsWith(">"))
      return execute_safe("_subject",S);
      */
      /*     
       * Current requirements:
       * Attempt to execute if:
       * There are no angle brackets
       * It does not start or end with a "."
       * It contains at least one "."
       * This will not execute "GEO" "my" or "Business"
       */
      // Bug #11366 Mike O - Replaced S with sEval
      else if (sEval.IndexOf(".") != -1 && sEval.IndexOf("<") == -1 && !sEval.StartsWith(".") && !sEval.EndsWith(".") && sEval.IndexOf(".") != -1)
      {
        // execute path
        try
        {
          return misc.CASL_execute("_subject", S);
        }
        catch (Exception)
        {
          // HX: Bug 17849
          //Logger.cs_log(e.ToMessageAndCompleteStacktrace()); // do not log them, it uses for normal flow. 
          return HttpUtility.HtmlAttributeEncode(S);  // Bug 16644 UMN prevent XSS attack
        }
      }
      else
      {
        // Bug 19256 DJD: White-list single and double quotes
        // Bug 20235 UMN and & for name search.
        return HttpUtility.HtmlAttributeEncode(S)    // Bug 16644 UMN prevent XSS attack
          .Replace("&quot;", "\"")
          .Replace("&#39;", "'")
          .Replace("&amp;", "&");
      }
    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="args">_subject=req=string</param>
    /// <returns></returns>
    // Called by: convert_to_value_from
    public static object execute_safe(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      object S = geo_args.get("_subject");
      GenericObject obj = CASLParser.decode_conciseXML(S.ToString(), "");

      //----------------------------------------
      // set parent recursively instead execute it.
      //----------------------------------------
      set_nested_parent(obj);
      return obj;
    }

    public static object set_nested_parent(GenericObject geo_obj)
    {
      ArrayList keys = geo_obj.getKeys();
      if (geo_obj.has("_parent"))
      {
        object parent = geo_obj.get("_parent");
        if (Tool.is_symbol_or_path(parent))
        {
          parent = CASLInterpreter.execute_expr(
            CASL_Stack.Alloc_Frame(EE_Steps.standard, parent, c_CASL.c_local_env()));

          geo_obj.set("_parent", parent);
        }
      }
      foreach (object key in keys)
      {
        if (key.Equals("_parent")) continue;
        object value = geo_obj.get(key);
        if (value is GenericObject)
          set_nested_parent(value as GenericObject);
      }
      return null;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="args"> _subject=req=String</param>
    /// <returns> string </returns>
    public static object uri_decode(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      object obj = geo_args.get("_subject");
      return HttpUtility.UrlDecode(obj.ToString());
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="args"> _subject=req=String</param>
    /// <returns> string </returns>
    public static object uri_encode(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      object obj = geo_args.get("_subject");
      return HttpUtility.UrlEncode(obj.ToString());
    }

    // Bug #23386 MJO
    /// <summary>
    /// Encode HTML characters
    /// </summary>
    /// <param name="args"> _subject=req=String</param>
    /// <returns> string </returns>
    public static object htm_encode(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      object obj = geo_args.get("_subject");
      return HttpUtility.HtmlEncode(obj.ToString());
    }

    // called after object has been found.  Don't let the same page submit a request.
    public static void compare_referrer_and_last_uri(GenericObject a_server)
    {

      GenericObject a_request = a_server.get("a_request") as GenericObject;
      GenericObject a_response = a_server.get("a_response") as GenericObject;
      GenericObject my = CASLInterpreter.get_my();
      if (HttpContext.Current == null || my == null) return;  //Bug#7989 To remove error log on the initial Load.// EXIT if running locally

      if (HttpContext.Current.Request.HttpMethod.Equals("POST") &&
       HttpContext.Current.Request.ContentType.StartsWith("text/xml")
       )
      {
        return;
      }

      string last_non_query_uri = my.get("last_non_query_uri", "no_last_uri") as String;

      string referrer_uri = "no_referrer";
      if (HttpContext.Current.Request.UrlReferrer != null)
      {
        my.set("referrer_uri", HttpContext.Current.Request.UrlReferrer.ToString());
        referrer_uri = HttpContext.Current.Request.UrlReferrer.LocalPath;

        int dotpos = referrer_uri.LastIndexOf(".");
        if (dotpos != -1)  // strip off ending extension and query string for comparison
        {
          referrer_uri = referrer_uri.Substring(0, dotpos);
        }
      }
      String path_string = ((GenericObject)a_server.get("a_request")).get("path_string", "") as string;
      //if ( (Boolean)c_CASL.GEO.get_path("Business.config.handle_browser_back_button",false,false) == false )  return;

      ///////////////////////////////
      ///  Do not handle back button crtieria
      ///  request exention is not htm
      ///  or the application is not casl_ide - request path is not start with "casl_ide" or "console"
      ///  or The applicaiton turn on handle_browser_back_button(equal to true)
      ///////////////////////////////
      string PS = (a_request as GenericObject).get("path_string") as string;
      bool b_do_not_handle_back_button = (!a_request.get("extension", "").Equals("htm") || PS.StartsWith("casl_ide") || PS.StartsWith("console") /*|| (Boolean)my.get_path("wrap.handle_browser_back_button",true,false) == false TEMP */ );
      if (b_do_not_handle_back_button)
        //  if ( !a_request.get("extension","").Equals("htm") || PS.StartsWith("casl_ide") || PS.StartsWith("console") || (Boolean)my.get_path("wrap.handle_browser_back_button",true,false) == false )
        return;
      if (
       last_non_query_uri != "no_last_uri" &&
       referrer_uri != "no_referrer" &&
       !referrer_uri.StartsWith(last_non_query_uri)
       ) // if the referrer is not the last static page,
      //  meaning that the user submitted from an old page,
      //   then show the last static page sent to the user.
      {
        my.set("last_non_query_uri", "no_last_uri");
        // HANDLE BACK BUTTON , this feature works with application with history/h fields
        //    if( !(path_string.EndsWith("/start")) )  // redirect to last known page
        //    {
        //     //				   string redirect_message = "<DIV style='margin:60px'><H1>" + 
        //     //					   c_CASL.GEO.get("old_request_msg_major") +  //"Request was ignored, please try again." +
        //     //					   "</H1><H4>" +
        //     //					   c_CASL.GEO.get("old_request_msg_minor") + // "Can not submit from old pages...please wait for current page." +
        //     //					   "</H4></DIV>";
        //     // show an error message to the user
        //     /* need to get the top app from the requested_object
        //      */
        //     GenericObject redirect_response = c_CASL.c_instance("response",
        //      "location",last_non_query_uri+ ".htm",
        //      "status_code", 303, 
        //      "result", "request redirected",
        //      "body_bytes", "request redirected"
        //      ); 
        //     my.set("last_non_query_uri","no_last_uri");
        //     a_server.set("a_response", redirect_response); //.set("result", redirect_response);
        //    }
        //
        //				//string temps = (a_server.get("a_request") as GenericObject).get("query_string") as string;
        //				bool has_query = (a_server.get("a_request") as GenericObject).get("query_string") != null;
        //				bool has_post = HttpContext.Current.Request.HttpMethod == "POST";
        //				if ( !has_query && !has_post ) //&& referrer_uri != "no_referrer") //  It is a 'static' return page like: /site/0/0.htm and not a submission like /site/0/continue.htm?
        //				{
        //					my.set("last_non_query_uri", a_server.get("no_post_no_query_path_no_ext","no_last_uri"));
        //					//my.set("last_non_query_uri", a_server.get("path_no_ext"));
        //				}		 
      }
      else
      {
        //string temps = (a_server.get("a_request") as GenericObject).get("query_string") as string;
        bool has_query = (a_server.get("a_request") as GenericObject).get("query_string") != null;
        bool has_post = HttpContext.Current.Request.HttpMethod == "POST";
        if (!has_query && !has_post) //&& referrer_uri != "no_referrer") //  It is a 'static' return page like: /site/0/0.htm and not a submission like /site/0/continue.htm?
        {
          my.set("last_non_query_uri", a_server.get("no_post_no_query_path_no_ext", "no_last_uri"));
          //my.set("last_non_query_uri", a_server.get("path_no_ext"));
        }
      }
    }
  }
}
