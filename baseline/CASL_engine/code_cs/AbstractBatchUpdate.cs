﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace CASL_engine
{

    // Bug #12126 Mike O - Made the interface between BatchUpdate.cs and individual system interfaces explicit
    abstract public class AbstractBatchUpdate
    {
        /// <summary>
        ///   GetInstance should be used to create new instances
        /// </summary>
        protected AbstractBatchUpdate()
        {
        }

        /// <summary>
        ///  Override this if you want to do something other than just instantiate this class
        /// </summary>
        /// <param name="sysInt"> The configured CASL system interface </param>
        /// <returns></returns>
        public virtual AbstractBatchUpdate GetInstance(GenericObject sysInt)
        {
            return (AbstractBatchUpdate)Activator.CreateInstance(this.GetType());
        }

        /// <summary>
        ///   Returns the class name of this system interface in CASL, so they can be matched up
        /// </summary>
        /// <returns>The class name of this system interface in CASL</returns>
        abstract public string GetCASLTypeName();

        /// <summary>
        ///   Use this function to close any open files and connections.
        /// </summary>
        /// <param name="bEarlyTerminationRequestedByProject"> Set to true to halt the batch update process early </param>
        /// <param name="pProductLevelMainClass"> The BatchUpdate_ProductLevel class </param>
        /// <returns>Whether or not the cleanup was successful</returns>
        abstract public bool Project_Cleanup(bool bEarlyTerminationRequestedByProject, object pProductLevelMainClass/*BUG# 8224 DH*/);

        /// <summary>
        ///   Added this method "Project_CoreFile_BEGIN" [Bug 15464 DJD - Batch Update Overhaul]
        ///   Optional (i.e., virtual) method used to inform the project implementation that CORE file processing is beginning
        ///   (i.e., the file events will be dispatched shortly).
        /// </summary>
        /// <param name="strSystemInterface"> The system interface name </param>
        /// <param name="pConfigSystemInterface"> The configured GenericObject for this sytem interface </param>
        /// <param name="pProductLevelMainClass"> The BatchUpdate_ProductLevel class </param>
        /// <param name="pCurrentFile"> The GenericObject of the CORE file to be rolled back </param>
        /// <param name="strCoreFile">  A string representation of CORE file to be rolled back </param>
        /// <param name="ReturnAction"> Referenced variable to set (when returning false) to terminate the SI or entire batch job </param>
        /// <returns>Whether or not CORE file initial processing was successful</returns>
        virtual public bool Project_CoreFile_BEGIN(string strSystemInterface, GenericObject pConfigSystemInterface, object pProductLevelMainClass, GenericObject pCurrentFile, string strCoreFile, ref eReturnAction ReturnAction)
        {
            return true;
        }

        /// <summary>
        ///   Each transaction/tender in a given event that uses this system interface is passed to this function to be processed.
        /// </summary>
        /// <param name="strSystemInterface"> The system interface name </param>
        /// <param name="pEventGEO"> A GenericObject which may contain TRANSACTION and TENDER GenericObjects that are vectors of the same </param>
        /// <param name="pConfigSystemInterface"> The configured GenericObject for this sytem interface </param>
        /// <param name="pFileGEO"> The GenericObject of the current file </param>
        /// <param name="pProductLevelMainClass"> The BatchUpdate_ProductLevel class </param>
        /// <param name="strCurrentFile"> A string representation of the current file </param>
        /// <param name="ReturnAction"> Referenced variable to set (when returning false) to terminate the SI or entire batch job </param>
        /// <returns>Whether or not processing was successful</returns>
        abstract public bool Project_ProcessCoreEvent(string strSystemInterface, GenericObject pEventGEO, GenericObject pConfigSystemInterface/*Access to the Config System Interface*/, GenericObject pFileGEO/*BUG# 8182 DH*/, object pProductLevelMainClass/*BUG# 8224 DH*/, string strCurrentFile/*BUG# 8224 DH*/, ref eReturnAction ReturnAction/*Bug 15464*/);

        /// <summary>
        ///   Each deposit in a given CORE file is passed to this function to be processed.
        ///   Added parameter "ReturnAction" [Bug 15464 DJD - Batch Update Overhaul]
        /// </summary>
        /// <param name="pFileDeposits"> A GenericObject vector of deposits </param>
        /// <param name="pConfigSystemInterface"> The configured GenericObject for this sytem interface </param>
        /// <param name="pProductLevelMainClass"> The BatchUpdate_ProductLevel class </param>
        /// <param name="strSysInterface"> The system interface name </param>
        /// <param name="pCurrentFile"> The GenericObject of the current file </param>
        /// <param name="ReturnAction"> Referenced variable to set (when returning false) to terminate the SI or entire batch job </param>
        /// <returns>Whether or not processing was successful</returns>
        abstract public bool Project_ProcessDeposits(GenericObject pFileDeposits, GenericObject pConfigSystemInterface, object pProductLevelMainClass/*BUG# 8224 DH*/, string strSysInterface/*BUG# 8224 DH*/, GenericObject pCurrentFile/*BUG# 8332*/, ref eReturnAction ReturnAction/*Bug 15464*/);

        /// <summary>
        ///   Added this method "Project_RollbackCoreFile" [Bug 15464 DJD - Batch Update Overhaul]
        ///   Optional (i.e., virtual) method used to roll back CORE file data if an error occurs while processing events or deposits.
        ///   NOTE: This method will ONLY be called for BU SIs that implement (and configure) "CORE FILE PROCESSING MODE".
        /// </summary>
        /// <param name="strSystemInterface"> The system interface name </param>
        /// <param name="pConfigSystemInterface"> The configured GenericObject for this sytem interface </param>
        /// <param name="pProductLevelMainClass"> The BatchUpdate_ProductLevel class </param>
        /// <param name="pCurrentFile"> The GenericObject of the CORE file to be rolled back </param>
        /// <param name="strCoreFile">  A string representation of CORE file to be rolled back </param>
        /// <returns>Whether or not CORE file rollback was successful</returns>
        virtual public bool Project_RollbackCoreFile(string strSystemInterface, GenericObject pConfigSystemInterface, object pProductLevelMainClass, GenericObject pCurrentFile, string strCoreFile)
        {
            return true;
        }

        /// <summary>
        ///   Added this method "Project_CoreFile_END" [Bug 15464 DJD - Batch Update Overhaul]
        ///   Optional (i.e., virtual) method used to inform the project implementation that CORE file processing is ending
        ///   (i.e., all the file events/deposits are dispatched).  It also allows the project implementation to return a 
        ///   record count and amount for the CORE file/System processed (these values will override any "product" counts 
        ///   and amounts).
        /// </summary>
        /// <param name="strSystemInterface"> The system interface name </param>
        /// <param name="pConfigSystemInterface"> The configured GenericObject for this sytem interface </param>
        /// <param name="pProductLevelMainClass"> The BatchUpdate_ProductLevel class </param>
        /// <param name="pCurrentFile"> The GenericObject of the CORE file to be rolled back </param>
        /// <param name="strCoreFile">  A string representation of CORE file to be rolled back </param>
        /// <param name="lItemCount"> If it has value, count of trans/tenders/GLs etc that are associated with system interface </param>
        /// <param name="dItemAmount"> If it has value, amount of trans/tenders/GLs etc that are associated with system interface </param>
        /// <param name="ReturnAction"> Referenced variable to set (when returning false) to terminate the SI or entire batch job </param>
        /// <returns>Whether or not CORE file final processing was successful</returns>
        virtual public bool Project_CoreFile_END(string strSystemInterface, GenericObject pConfigSystemInterface, object pProductLevelMainClass, GenericObject pCurrentFile, string strCoreFile, ref long? lItemCount, ref double? dItemAmount, ref eReturnAction ReturnAction)
        {
            return true;
        }

        /// <summary>
        ///   This function is used in case the TG_UPDATE_DATA cannot be updated and system interface needs to rollback any changes.
        /// </summary>
        /// <param name="pProductLevelMainClass"> The BatchUpdate_ProductLevel class </param>
        /// <param name="bEarlyTerminationRequestedByProject"> Set to true to halt the batch update process early </param>
        abstract public void Project_Rollback(object pProductLevelMainClass/*BUG# 8224 DH*/, bool bEarlyTerminationRequestedByProject);

        /// <summary>
        ///   Writes trailers for a system interface
        /// </summary>
        /// <param name="pProductLevelMainClass"> The BatchUpdate_ProductLevel class </param>
        /// <param name="strSysInterface"> The system interface name </param>
        /// <param name="bEarlyTerminationRequestedByProject"> Set to true to halt the batch update process early </param>
        /// <returns>Whether or not processing was successful</returns>
        abstract public bool Project_WriteTrailer(object pProductLevelMainClass/*BUG# 8224 DH*/, string strSysInterface, ref bool bEarlyTerminationRequestedByProject);

        /// <summary>
        ///   Added this method "Project_SendFiles" [Bug 15464 DJD - Batch Update Overhaul]
        ///   Optional (i.e., virtual) method to transfer file(s) to destination server(s).
        /// </summary>
        /// <param name="pProductLevelMainClass"> The BatchUpdate_ProductLevel class </param>
        /// <param name="strSysInterface"> The system interface name </param>
        /// <returns></returns>
        virtual public bool Project_SendFiles(object pProductLevelMainClass, string strSysInterface)
        {
            return true;
        }
    }
}
