using System;
using System.Reflection;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Xml;
using System.Text;
using System.Windows.Forms;
using System.Web;
using System.Net;
using System.Net.Mail; // Bug 7427 ANU 
using System.Text.RegularExpressions;// Bug 25327 SM
using System.Globalization;// Bug 25327 SM

namespace CASL_engine
{
  public class emails
  {
    // Bug 22627 UMN add support for Amazon SES email
    // No need to make port configurable, as best practice is to use 587, especially with Amazon
    const int SMTP_PORT = 587;

    /// <summary>
    /// This message is used by iPayment to send an email message.
    /// </summary>
    /// <param name="args">
    ///   to                = req
    ///   from              = req
    ///   password          = opt
    ///   subject           = req
    ///   body              = opt
    ///   host_smtp_server  = req
    ///   port              = req     // Bug 26910
    /// </param>
    /// <returns></returns>
    public static object CASL_email(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      // Bug# 25967 DH - We will be setting the email subject in the error
      string strEmailSubject = (string)geo_args.get("subject", "Email subject not set");

      //GenericObject account= geo_args.get("account") as GenericObject;

      // Bug 7427 ANU 

      MailMessage Message = null; // Bug 22001 DJD - Added dispose of MailMessage
                                  //------------------------ Set up the necessary components-------------
                                  // Bug #12990 Mike O - Moved the try block to cover everything, as adding bad e-mail addresses can throw exceptions
      try
      { // try send email
        // Bug 11617 NJ: check if the To address exists, else throw an error
        Message = new MailMessage(); // Bug 22001 DJD - Added dispose of MailMessage
        string toAddresses = (string)geo_args.get("to");
        string errorMessage = "";
        if (toAddresses.Equals(""))
        {
          errorMessage = "Administrator's email address is missing from config.casl." + "Email subject: " + strEmailSubject;// Bug# 25967 DH - We will be setting the email subject in the error
        }
        else
        {
          //HXIAO Bug 16546 and Bug 22199
          foreach (var address in toAddresses.Split(new[] { ";", ",", " " }, StringSplitOptions.RemoveEmptyEntries))
          {
            Message.To.Add(address);
            //Logger.cs_log("address= " + address); //hxiao test
          }
          //HXIAO remove this line
          //Message.To.Add(toAddresses); //(string)geo_args.get("to"));
          // End Bug 16546
        }
        // end bug 11617 NJ
        //Message.From = (string)account.get("from_email");
        Message.Subject = (string)geo_args.get("subject");
        // Bug 12998 
        if (HttpContext.Current != null)   // Add a check on HttpContext.Current
        {
          System.Web.HttpRequest current_request = HttpContext.Current.Request;
          string server_ip = current_request.ServerVariables["SERVER_NAME"] as string; //e.g. 172.16.1.83
          string strContent = (string)geo_args.get("body");
          string strSiteStatic = c_CASL.c_object("GEO.site_static") as string;
          string strInvalidImageSrc = string.Format("=\"{0}/", strSiteStatic);
          string protocol = "http";
          if (current_request.IsSecureConnection) // Bug 13039 
            protocol = "https";
          string strValidImageSrc = string.Format("=\"{0}://{1}{2}/", protocol, server_ip, strSiteStatic);
          strContent = strContent.Replace(strInvalidImageSrc, strValidImageSrc);
          Message.Body = strContent;
        }
        else
        {
          Message.Body = (string)geo_args.get("body");
        }
        // end of Bug 12998

        // Bug 11617 NJ: check if the From address exists, else throw an error
        string fromAddresses = (string)geo_args.get("from");
        if (fromAddresses.Equals(""))
        {
          errorMessage += " Sender's email address is missing from config.casl";
        }
        else
        {
          MailAddress fromAddress = new MailAddress((string)geo_args.get("from"));
          Message.From = fromAddress;
        }
        if (!errorMessage.Equals(""))
        {
          Logger.LogError(errorMessage, "SEND EMAIL FAILED");
          throw CASL_error.create("code", "Runtime error(200)", "message", errorMessage);
        }
        // end bug 11617 NJ
        //Bug 26901 NAM: support password and configurable port
        string provider = (string)geo_args.get("provider", "CORE");
        string smtp_host = (string)geo_args.get("host_smtp_server", "mail.corebt.com"); // host smtp server
        string password = (string)geo_args.get("password", "");
        int smtp_port = SMTP_PORT;
        string sPort = geo_args.get("port", "") as string;
        if (!string.IsNullOrEmpty(sPort))
          smtp_port = System.Convert.ToInt32(sPort);
        SmtpClient smtpClient = new SmtpClient(smtp_host, smtp_port);
        {
          // Bug 22627 UMN add support for Amazon SES email
          AddCredentials(provider, smtp_host, fromAddresses, password, smtpClient);                //Bug 26910 NAM:
          Message.IsBodyHtml = true;

          smtpClient.Send(Message);
          string log_message = "Sent email to (" + Message.To + ") from (" + Message.From + ") with subject (" + Message.Subject + ") by using SmtpServer (" + smtpClient.Host + ")";
          Logger.LogInfo(log_message, "SEND EMAIL SUCCESSFUL");
        }
      }
      catch (HttpCompileException ehttp)
      {
        Logger.LogError(ehttp.Message, "SEND EMAIL FAILED");
        return ehttp.Message;
      }
      catch (Exception e)
      {
        Logger.LogError(e.Message, "SEND EMAIL FAILED");
        // bug 17849
        Logger.cs_log_trace("Error sending email", e);
        return e.Message;
      }
      finally
      {
        // Bug 22001 DJD - Added dispose of MailMessage
        if (Message != null)
        {
          Message.Dispose();
        }
      }

      return "";
    }

    //Bug 25327 SM Verifies if an email is valid and returns the results.
    public static bool IsValidEmail(string email)
    {
      if (string.IsNullOrWhiteSpace(email))
        return false;

      try
      {
        email = Regex.Replace(email, @"(@)(.+)$", DomainMapper, RegexOptions.None, TimeSpan.FromMilliseconds(200));

        string DomainMapper(Match match)
        {
          var idn = new IdnMapping();
          var domainName = idn.GetAscii(match.Groups[2].Value);
          return match.Groups[1].Value + domainName;
        }
      }
      catch (RegexMatchTimeoutException)
      {
        return false;
      }
      catch (ArgumentException)
      {
        return false;
      }

      try
      {
        return Regex.IsMatch(email,
            @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z!#\$%'^&\*+\-~/_]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z!#\$%'^&\*+\-~/_])@))" +
            @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",      
            RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
      }
      catch (RegexMatchTimeoutException)
      {
        return false;
      }
    }

    //Bug 25327 SM - This method splits the email string passed in, verifies each address, places each address in an object array index.
    //One index each for bad addresses and good addresses.  The object array is passed back to the calling method.
    public static object fCASL_Validate_Emails(CASL_Frame frame)
    {
      GenericObject args_emails = frame.args;
      string emailString = args_emails.get("emailString").ToString();
      string[] emails = emailString.Split(new[] { ";", ",", " " }, StringSplitOptions.RemoveEmptyEntries);
      String finalValidEmails = "";
      String finalInvalidEmails = "";
      GenericObject args_EmailsFinal = new GenericObject();

      foreach (var emailAddress in emails)
      {
        if (!IsValidEmail(emailAddress))
        {
          finalInvalidEmails = finalInvalidEmails + emailAddress + ",";
        }
        else
        {
          finalValidEmails = finalValidEmails + emailAddress + ",";
        }

      }

      finalValidEmails = finalValidEmails.TrimEnd(',');
      finalInvalidEmails = finalInvalidEmails.TrimEnd(',');
      args_EmailsFinal[1] = finalInvalidEmails;
      args_EmailsFinal[0] = finalValidEmails;

      return args_EmailsFinal;
    }

    //25336 SM - Added new method 'fCASL_SplitEmails' to support fix for 25336. This does a proper split no matter what delimiter is used.
    public static Object fCASL_SplitEmails(CASL_Frame frame)
    {
      GenericObject args_emails = frame.args;
      string emailString = args_emails.get("emailString").ToString();
      string[] emails = emailString.Split(new[] { ";", ",", " " }, StringSplitOptions.RemoveEmptyEntries);
      GenericObject args_EmailsFinal = new GenericObject();

      for (int i = 0; i < emails.Length; i++)
      {
        args_EmailsFinal.Add(i, emails[i]);
      }

      return args_EmailsFinal;
    }

    /// <summary>
    /// Add user/password credentials if service is Amazon.
    /// </summary>
    /// <param name="host"></param>
    /// <param name="smtpClient"></param>
    public static void AddCredentials(string provider, string host, string sender, string password, SmtpClient smtpClient)   //Bug 26910 NAM:
    {
      if (host.Contains("amazonaws"))
      {
        string smtp_username = (string)c_CASL.GEO.get("smtp_user", "6L9enIsE4lvX3Nzv68aHHXZ7qym8XQkI");
        string smtp_password = (string)c_CASL.GEO.get("smtp_password", "bNpPyTUtz+Om2z9RnVIp8ygMNFcQRRlCwRFP3cprFSiAik1pZIzZKz4qxEgmVU5R");
        string secret_key = Crypto.get_secret_key();
        smtp_username = System.Text.Encoding.Default.GetString(Crypto.symmetric_decrypt("data", Convert.FromBase64String(smtp_username), "secret_key", secret_key));
        smtp_password = System.Text.Encoding.Default.GetString(Crypto.symmetric_decrypt("data", Convert.FromBase64String(smtp_password), "secret_key", secret_key));

        smtpClient.Credentials = new NetworkCredential(smtp_username, smtp_password);
        smtpClient.EnableSsl = true;
      }
      else if (!string.IsNullOrEmpty(sender) && !string.IsNullOrEmpty(password))      ///Bug 26910 NAM: if Department level email, use deparment sender and department password
      {
        smtpClient.Credentials = new NetworkCredential(sender, password);
        smtpClient.EnableSsl = true;
      }
      else if (string.IsNullOrEmpty(password) && provider.ToUpper().Equals("OTHER"))    //Bug 26910 NAM: get password from web.config
      {
        string smtp_password = (string)c_CASL.GEO.get("smtp_password_other", "");
        if (!string.IsNullOrEmpty(smtp_password))
        {
          string secret_key = Crypto.get_secret_key();
          smtp_password = System.Text.Encoding.Default.GetString(Crypto.symmetric_decrypt("data", Convert.FromBase64String(smtp_password), "secret_key", secret_key));
          if (!string.IsNullOrEmpty(sender) && !string.IsNullOrEmpty(smtp_password))
          {
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = new NetworkCredential(sender, smtp_password, "corebt.com");
            smtpClient.EnableSsl = true;
          }
        }
      }
    }

    /// <summary>
    /// Send email notification when you may have a loading failure, or don't have a department to get
    /// the smtp_host. This method will use the info configured in config.casl.
    /// This function is called by: ABAValidation.SendEmailNotification, BatchUpdate.RunBatchUpdate_aux,
    /// and handle_request_2.cs.
    /// 
    /// Bug 5630 Add email notification for error e.g. loading failure , require minimum dependency for casl. SX 06/10/08
    /// </summary>
    /// <param name="args">
    ///    email_subject opt
    ///    module opt
    ///    action opt
    ///    error opt
    ///    email_content opt
    /// </param>
    /// <returns>Empty string on success, or an error message.</returns>
    public static object CASL_send_email_notification(params object[] args)
    {
      try
      {
        if (c_CASL.GEO == null)
        {
          return "";// no email address available
        }
        GenericObject geo_args = misc.convert_args(args);
        string site_name = c_CASL.GEO.get("site_name", "UNKNOWN SITE NAME") as string;
        string server_ip = "";
        string browser_ip = "";
        string protocol = "http";
        if (HttpContext.Current != null)   // Add a check on HttpContext.Current Bug 13039
        {
          System.Web.HttpRequest current_request = HttpContext.Current.Request;
          server_ip = current_request.ServerVariables["SERVER_NAME"] as string; //e.g. 172.16.1.83
          browser_ip = current_request.UserHostName; // e.g. 127.0.0.1 
          if (current_request.IsSecureConnection) // Bug 13039 
            protocol = "https";
        }

        string session_id = misc.get_masked_session_id(); // Bug 20435 UMN PCI-DSS

        // Bug 7590 add new param  email_receiver (Bug 15464 DJD - Batch Update Overhaul)
        // Bug 22424 SX c# default need to be same as CASL default.
        string email_to = geo_args.get("email_receiver", "DEFAULT") as String;

        /////////////////////////////////////////////
        ///   
        ///   email server: GEO.email_server
        ///   email from: GEO.ipayment_sender_email
        ///   email to : GEO.email_for_error
        ///   email subject: "[site_name]- Server:[Server IP] Browser: [Error Occured]/[Passed-in subject] 
        ///   email body: 
        ///          HEADER : GEO.error_email_header
        ///          
        ///          Module : [iPayment Module Name]
        ///          Action :  [action name] 
        ///          Error Message : [error message] ([error code])
        ///          When:  [current datetime]
        ///          Browser IP/Session ID: [Browser IP]/[session id]
        ///          [any extra info]
        ///          
        ///          FOOTER : GEO.error_email_footer
        ///          
        /////////////////////////////////////////////
        string email_provider = c_CASL.GEO.get("smtp_provider", "CORE") as string;        //Bug 26910 NAM:
        string email_server = c_CASL.GEO.get("smtp_host", "mail.corebt.com") as string;
        int smtp_port = SMTP_PORT;
        string sPort = geo_args.get("port", "") as string;
        if (!string.IsNullOrEmpty(sPort))
          smtp_port = System.Convert.ToInt32(sPort);
        string email_from = c_CASL.GEO.get("ipayment_sender_email", "") as string;
        if (email_to == "DEFAULT") // Bug 7590 (Bug 15464 DJD - Batch Update Overhaul)
          email_to = c_CASL.GEO.get("email_for_error", "") as string;

        // Bug 11617 NJ-check if the to address doesn't exists, log it. 
        string errorMessage = "";
        if (email_to.Equals(""))
        {
          errorMessage = "Administrator's email address is missing from config.casl.";
        }
        if (!errorMessage.Equals(""))
        {
          Logger.LogError(errorMessage, "SEND EMAIL FAILED");
        }
        //end bug 11617 NJ
        if (email_server == "" || email_from == "" || email_to == "")
          return "";
        // EMAIL SUBJECT
        string subject_message = geo_args.get("email_subject", "Error Occurred") as String;
        string email_subject = string.Format("{0}/{1} {2}"
                                             , site_name
                                             , server_ip
                                             , subject_message); // geo_args.get("email_subject");
        // EMAIL BODY
        string email_body = "";
        // HEADER
        string email_header = c_CASL.GEO.get("error_email_header", "") as string;
        // MODULE
        string email_content_module = geo_args.get("module", "iPayment Enterprise") as String;
        email_body = email_body + string.Format("<TR> <TD> {0} </TD> <TD> {1}</TD></TR>", "Module", email_content_module);
        // ACTION
        string email_content_action = geo_args.get("action", "General") as String;
        email_body = email_body + string.Format("<TR> <TD> {0} </TD> <TD> {1}</TD></TR>", "Action", email_content_action);
        // ERROR
        object casl_error_obj = geo_args.get("error", null);
        if ((casl_error_obj is GenericObject) && !casl_error_obj.Equals(c_CASL.c_opt))
        {
          string error_message = string.Format("{0}({1})", (casl_error_obj as GenericObject).get("message", ""), (casl_error_obj as GenericObject).get("code", "", true));
          email_body = email_body + string.Format("<TR> <TD> {0} </TD> <TD> {1}</TD></TR>", "Error", error_message);
        }
        // DATETIME
        string current_datetime = DateTime.Now.ToString("G");
        email_body = email_body + string.Format("<TR> <TD> {0} </TD> <TD> {1}</TD></TR>", "When", current_datetime);
        // Browser IP/Session ID
        email_body = email_body + string.Format("<TR> <TD> {0} </TD> <TD> {1}/{2}</TD></TR>", "Browser IP/Session ID", browser_ip, session_id);
        // PASS IN INFO
        GenericObject email_content_list = geo_args.get("email_content", new GenericObject()) as GenericObject;
        ArrayList ordKeys = email_content_list.get_normalized_keys();
        foreach (Object objKey in ordKeys)
        {
          String sKey = objKey as string;
          if (sKey != null && !sKey.StartsWith("_")) // Bug 7569 exclude system keys and vector keys (Bug 15464 DJD - Batch Update Overhaul)
          {
            String sValue = email_content_list.get(objKey, "").ToString();
            // Bug 25777 MJO - Use static HtmlEncode as we can't rely on there being an HttpContext.Current
            email_body = email_body + string.Format("<TR> <TD> {0} </TD> <TD> {1}</TD></TR>", HttpUtility.HtmlEncode(sKey), HttpUtility.HtmlEncode(sValue));
          }
        }
        // Bug 7569 includes htm in vector keys like html report
        foreach (Object objContent in email_content_list.vectors)
        {
          string strContent = objContent as String;
          if (server_ip != "")// Add a check on HttpContext.Current Bug 13039
          {
            string strSiteStatic = c_CASL.c_object("GEO.site_static") as string;
            string strInvalidImageSrc = string.Format("=\"{0}/", strSiteStatic);
            string strValidImageSrc = string.Format("=\"{0}://{1}{2}/", protocol, server_ip, strSiteStatic);
            strContent = strContent.Replace(strInvalidImageSrc, strValidImageSrc);
          }
          email_body = email_body + "<BR/>" + strContent;
        }
        // END Bug 7569
        // FOOTER
        string email_footer = c_CASL.GEO.get("error_email_footer", "") as string;
        email_body = string.Format("<HTML><DIV>{0}</DIV> <BR/> <BR/><TABLE border='1'> {1} </TABLE> <BR/> <BR/> <DIV>{2}</DIV></HTML>", email_header, email_body, email_footer);

        //------------------------ Set up the necessary components-------------
        // Bug 7427 ANU 

        // Try to send email
        string host = email_server;
        SmtpClient smtpClient = new SmtpClient(host, smtp_port);         // Bug 26910
        MailMessage Message = new MailMessage(); //new mailmessage
        MailAddress fromAddress = new MailAddress(email_from);
        Message.From = fromAddress;
        //Message.To.Add(email_to); 
        Message.Subject = HttpContext.Current == null ? email_subject : HttpContext.Current.Server.HtmlEncode(email_subject); // Bug 15464 DJD - Batch Update Overhaul
        Message.Body = email_body;
        Message.IsBodyHtml = true;

        //HXIAO Bug 16546 and Bug 22199
        foreach (var address in email_to.Split(new[] { ";", ",", " " }, StringSplitOptions.RemoveEmptyEntries))
        {
          Message.To.Add(address);
          //Logger.cs_log("address= " + address); //hxiao test
        }

        // Bug 22627 UMN add support for Amazon SES email
        AddCredentials(email_provider, host, email_from, "", smtpClient);     //Bug 26910 NAM:

        smtpClient.Send(Message);
        string log_message = "Sent email to (" + Message.To + ") from (" + Message.From + ") with subject (" + Message.Subject + ") by using SmtpServer (" + smtpClient.Host + ")";
        Logger.LogInfo(log_message, "SEND EMAIL SUCCESSFUL");
      }
      // Bug 7427 ANU end
      catch (HttpCompileException ehttp)
      {
        /*try
        {
          String errMsg = ehttp.InnerException + ehttp.Message;
          Logger.LogError(errMsg, "SEND EMAIL FAILED");
        }
        catch (Exception) //12081 NJ-removed unused variable
        {
          return ehttp.Message;
        }*/
        // bug 17849
        Logger.LogError(ehttp.ToMessageAndCompleteStacktrace(), "SEND EMAIL FAILED");
        Logger.cs_log_trace("Error sending email", ehttp);
        return ehttp.Message;
      }
      catch (Exception e)
      {
        /*try
        {
          String errMsg = e.InnerException + e.Message;
          Logger.LogError(errMsg, "SEND EMAIL FAILED");
        }
        catch (Exception)//12081 NJ-removed unused variable
        {
          return e.Message;
        }*/

        // bug 17849
        Logger.LogError(e.ToMessageAndCompleteStacktrace(), "SEND EMAIL FAILED");
        Logger.cs_log_trace("Error sending email", e);
        return e.Message;
      }
      return "";
    }

    // BEGIN Bug 20562 EOM Add Multi-factor authentication (MFA) 
    /// <summary>
    /// Send the special email login message when using MFA from Config.
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static object CASL_email_login(params object[] args)
    {
      // needs user email, 
      GenericObject geo_args = misc.convert_args(args);

      using (MailMessage Message = new MailMessage())
      {
        //------------------------ Set up the necessary components -------------

        //  Moved the try block to cover everything, as adding bad e-mail addresses can throw exceptions
        try
        {
          string toAddresses = (string)geo_args.get("to");
          string errorMessage = "";
          if (toAddresses.Equals(""))
          {
            errorMessage = "User's email address is missing from config.casl.";
          }
          else
          {
            Message.To.Add(toAddresses);
          }

          Message.Subject = "iPayment Authentication Change Notice";

          // Set the params for link.
          string sParams = String.Format("user_id={0}&user_pwd={1}&change_pwd={2}&user_key={3}&change_pin={4}&reset_time={5}",
          (string)geo_args.get("user_id"), HttpUtility.UrlEncode((string)geo_args.get("user_pwd")),
          (string)geo_args.get("change_pwd"), HttpUtility.UrlEncode((string)geo_args.get("mfa_user_key")),
          (string)geo_args.get("change_pin"), DateTime.Now.ToString());

          // Encrypt the params
          string paramsBase64 = misc.StringToBase64String(sParams);
          GenericObject bytes = Crypto.symmetric_encrypt("data", paramsBase64, "is_ascii_string", false);
          byte[] data = (byte[])bytes.get("data");
          sParams = HttpUtility.UrlEncode(Convert.ToBase64String(data));

          // Extract the URL
          string myPath = HttpContext.Current.Request.Url.ToString().Substring(0, HttpContext.Current.Request.Url.ToString().LastIndexOf('/'));
          // Need to trim off any objects after root path
          myPath = myPath.Substring(0, myPath.LastIndexOf(HttpContext.Current.Request.ApplicationPath) + HttpContext.Current.Request.ApplicationPath.Length);

          // Note might need to strip off more from myPath if it also includes the app, since the app we want is Config
          string startURL = String.Format("{0}/Config/start.htm?par={1}", myPath, sParams);

          // Make the email body.
          StringBuilder bodyStringBuilder = new StringBuilder();
          bodyStringBuilder.Append("Hello " + geo_args.get("user_name").ToString() + ", <BR><BR>");

          // Customize the body message.
          string sWhat = "";
          if ((string)geo_args.get("change_pwd") == "true")
          {
            sWhat = "password";
            if ((string)geo_args.get("change_pin") == "true") sWhat = sWhat + " and PIN";
          }
          else sWhat = "PIN";

          bodyStringBuilder.Append("Your " + sWhat + " has been changed.  Please click on the link below to obtain the new " + sWhat + " information.<BR><BR>");
          bodyStringBuilder.Append(startURL);
          Message.Body = bodyStringBuilder.ToString();

          string fromAddresses = (string)geo_args.get("from");
          if (fromAddresses.Equals(""))
          {
            errorMessage += " Sender's email address is missing from config.casl";
          }
          else
          {
            MailAddress fromAddress = new MailAddress((string)geo_args.get("from"));
            Message.From = fromAddress;
          }
          if (!errorMessage.Equals(""))
          {
            Logger.LogError(errorMessage, "SEND EMAIL FAILED");
            throw CASL_error.create("code", "Runtime error(200)", "message", errorMessage);
          }

          // Prepare the attachment
          System.Net.Mail.Attachment attachment;
          string strFile = (string)c_CASL.GEO.get("site_physical") + "/doc/Google_Authenticator_Setup.doc";
          attachment = new System.Net.Mail.Attachment(strFile);
          attachment.Name = "Google_Authenticator_Setup.doc";

          // Add the attachment
          Message.Attachments.Add(attachment);

          //Bug 26901 NAM: support password and configurable port
          string smtp_host = (string)c_CASL.GEO.get("smtp_host", "mail.corebt.com");
          string smtp_provider = (string)c_CASL.GEO.get("smtp_provider", "CORE");
          string email_from = c_CASL.GEO.get("ipayment_sender_email", "") as string;
          int smtp_port = SMTP_PORT;
          string sPort = geo_args.get("port", "") as string;
          if (!string.IsNullOrEmpty(sPort))
            smtp_port = System.Convert.ToInt32(sPort);
          using (SmtpClient smtpClient = new SmtpClient(smtp_host, smtp_port))
          {
            Message.IsBodyHtml = true;
            AddCredentials(smtp_provider, smtp_host, email_from, "", smtpClient);           //Bug 26910 NAM:
            smtpClient.Send(Message);
            string log_message = "Sent email to (" + Message.To + ") from (" + Message.From + ") with subject (" + Message.Subject + ") by using SmtpServer (" + smtpClient.Host + ")";
            Logger.LogInfo(log_message, "SEND EMAIL SUCCESSFUL");
          }

        }
        catch (HttpCompileException ehttp)
        {
          //Logger.LogError(ehttp.Message, "SEND EMAIL FAILED");
          Logger.LogError(ehttp.ToMessageAndCompleteStacktrace(), "SEND EMAIL FAILED");
          Logger.cs_log_trace("Error sending email", ehttp);
          return ehttp.Message;
        }
        catch (Exception e)
        {
          //Logger.LogError(e.Message, "SEND EMAIL FAILED");
          Logger.LogError(e.ToMessageAndCompleteStacktrace(), "SEND EMAIL FAILED");
          Logger.cs_log_trace("Error sending email", e);
          return e.Message;
        }
      }

      return "";
    }
    // END Bug 20562 EOM Add Multi-factor authentication (MFA) 
  }
}
