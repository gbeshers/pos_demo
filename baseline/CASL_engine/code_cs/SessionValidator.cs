﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Collections;
using System.Collections.Concurrent;

// Bug #11869 Mike O - Used to keep track of user sessions

namespace CASL_engine
{
  public sealed class SessionValidator
  {
    private static readonly SessionValidator validator = new SessionValidator();

    private ConcurrentDictionary<string, string> dict = new ConcurrentDictionary<string, string>();

    private SessionValidator() {}

    // Get the singleton
    public static SessionValidator GetInstance()
    {
      return validator;
    }

    // Update the session id with the provided user and app strings
    public void updateSessionID(string user, string app, string session)
    {
      string key = user + "," + app;

      // Bug 20351 MJO - Updated to use .NET ConcurrentDictionary
      dict[key] = session;
    }

    // Bug 17410 MJO - Update all instances of a given session id to a new one
    public void updateSessionID(string oldSession, string newSession)
    {
      List<string> keys = new List<string>(dict.Keys);

      foreach (string key in keys)
      {
        if (dict[key] == oldSession)
          dict[key] = newSession;
      }
    }

    // Validate the session id for the provided user and app strings
    public bool validateSessionID(string user, string app, string session)
    {
      string key = user + "," + app;

      // Bug 20351 MJO - Updated to use .NET ConcurrentDictionary
      return dict.Keys.Contains(key) ? dict[key].Equals(session) : false;
    }
  }
}
