// UNUSED.  Now done by server's uri_decode

using System;
using System.Collections;
using System.Text;

namespace CASL_engine
{
	/// <summary>
	/// Summary description for CASLParser_ConciseURI.
	/// </summary>
	public class CASLParser_ConciseURI
	{
		private char current_event;
		//private int unkey;
		private int status;
		private String a_uri;
		private StringBuilder field;
		private String key;
	
		//private ArrayList current_object_Renamed_Field;
		public ArrayList nested_objects;
		private ArrayList nested_objects_keys;

		public CASLParser_ConciseURI(String a_uri)
		{
			status = ConciseURIStates.STARTING_FIELD;
			this.a_uri = a_uri;
			//unkey = 0;
			nested_objects = new ArrayList(10);
			nested_objects.Add(new GenericObject());
			nested_objects_keys = new ArrayList(10);
			nested_objects_keys.Add(null);
		}
	
		private GenericObject current_object()
		{
			return (GenericObject) SupportClass.VectorLastElement(nested_objects);
		}	
	
		private void  start_keyed_object()
		{
			nested_objects.Add(new GenericObject());
			nested_objects_keys.Add(field.ToString());
			status = ConciseURIStates.STARTING_FIELD;
		}
	
		private void  end_object_name()
		{
			current_object().set( "_parent" , field.ToString());
		}
	
		private void  start_unkeyed_object()
		{
			nested_objects.Add(new GenericObject());
			nested_objects_keys.Add(null);
		}
	
		private void  end_unkeyed_value()
		{
			current_object().insert(field.ToString());
		}
	
		private void  end_keyed_value()
		{
			current_object().set(key, field.ToString());
		}
	
		private void  end_keyed_null_value()
		{
			current_object().set(key, null);
		}
	
		private void  end_unkeyed_null_value()
		{
			current_object().insert(null);
		}
	
		private void  end_object_and_keyed_value()
		{
			end_keyed_value();
			end_object();
		}
	
		private void  end_object_and_keyed_null_value()
		{
			end_keyed_null_value();
			end_object();
		}
	
		private void  end_object_and_unkeyed_null_value()
		{
			end_unkeyed_null_value();
			end_object();
		}
	
		private void  end_object_and_unkeyed_value()
		{
			end_unkeyed_value();
			end_object();
		}
	
		private void  end_object()
		{
			System.Object temp_object;
			temp_object = nested_objects[nested_objects.LastIndexOf(SupportClass.VectorLastElement(nested_objects))];
			nested_objects.RemoveAt(nested_objects.LastIndexOf(SupportClass.VectorLastElement(nested_objects)));
			System.Object R = temp_object;
			// This object needs to be removed from the vector;
			System.Object temp_object2;
			temp_object2 = nested_objects_keys[nested_objects_keys.LastIndexOf(SupportClass.VectorLastElement(nested_objects_keys))];
			nested_objects_keys.RemoveAt(nested_objects_keys.LastIndexOf(SupportClass.VectorLastElement(nested_objects_keys)));
			System.Object K = temp_object2;
			// This object needs to be removed from the vector;
			if (K == null)
			{
				current_object().insert(R);
			}
			else
			{
				current_object().set(K, R);
			}
		}
	
		private void  error_occurred()
		{
			//System.out.println("Calling error_occurred()\n");
		}
	
		private void  start_field()
		{
			field = new System.Text.StringBuilder(current_event + "");
			//System.out.println("Calling start_field()\n" + field.toString() );
		}
	
		private void  start_value()
		{
			field = new System.Text.StringBuilder(current_event + "");
			//System.out.println("Calling start_value()\n" + field.toString() );
		}
	
		private void  end_key()
		{
			key = field.ToString();
			//System.out.println("Calling end_key()\n" + key );
		}
	
		private void  add_to_field()
		{
			field.Append(current_event);
			//System.out.println("Calling add_to_field()\n" + field.toString() );
		}
	
		/*
		**
		** public void run()
		***/
		public virtual void  Run()
		{
		
			for (int i = 0; i < a_uri.Length; i++)
			{
				current_event = a_uri[i];
				process_event();
			}
		
			//System.out.println( "Reached end of input." );
		
			if (status != ConciseURIStates.STARTING_FIELD)
			{
				//System.out.println( "Status at end of input is " + status + "." );
				current_event = '&';
				process_event();
			}
		
			if (nested_objects.Count != 1)
			{
				System.Console.Out.WriteLine("Number of nested_objects on exit = " + nested_objects.Count);
			}
			else
			{
				//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Object.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
				System.Console.Out.WriteLine("Object = " + nested_objects[0].ToString());
			}
		
			return ;
		}
	
		private void  process_event()
		{
			switch (status)
			{
			
				case ConciseURIStates.STARTING_FIELD: 
				{
					switch (current_event)
					{
					
						case '~': 
							error_occurred();
							status = ConciseURIStates.ERROR_STATE;
							break;
					
						case '&': 
							end_unkeyed_null_value();
							break;
					
						case '(': 
							start_unkeyed_object();
							break;
					
						case ')': 
							end_object();
							break;
					
						case '=': 
							error_occurred();
							status = ConciseURIStates.ERROR_STATE;
							break;
					
						default: 
							start_field();
							status = ConciseURIStates.ADDING_TO_FIELD;
							break;
					
					}
					break;
				}
			
				case ConciseURIStates.ADDING_TO_FIELD: 
				{
					switch (current_event)
					{
					
						case '~': 
							end_object_name();
							status = ConciseURIStates.STARTING_FIELD;
							break;
					
						case '&': 
							end_unkeyed_value();
							status = ConciseURIStates.STARTING_FIELD;
							break;
					
						case '(': 
							error_occurred();
							status = ConciseURIStates.ERROR_STATE;
							break;
					
						case ')': 
							end_object_and_unkeyed_value();
							status = ConciseURIStates.STARTING_FIELD;
							break;
					
						case '=': 
							end_key();
							status = ConciseURIStates.STARTING_VALUE;
							break;
					
						default: 
							add_to_field();
							break;
					
					}
					break;
				}
			
				case ConciseURIStates.STARTING_VALUE: 
				{
					switch (current_event)
					{
					
						case '~': 
							error_occurred();
							status = ConciseURIStates.ERROR_STATE;
							break;
					
						case '&': 
							end_keyed_null_value();
							status = ConciseURIStates.STARTING_FIELD;
							break;
					
						case '(': 
							start_keyed_object();
							status = ConciseURIStates.STARTING_FIELD;
							break;
					
						case ')': 
							error_occurred();
							status = ConciseURIStates.ERROR_STATE;
							break;
					
						case '=': 
							error_occurred();
							status = ConciseURIStates.ERROR_STATE;
							break;
					
						default: 
							start_value();
							status = ConciseURIStates.ADDING_TO_VALUE;
							break;
					
					}
					break;
				}
			
				case ConciseURIStates.ADDING_TO_VALUE: 
				{
					switch (current_event)
					{
					
						case '~': 
							error_occurred();
							status = ConciseURIStates.ERROR_STATE;
							break;
					
						case '&': 
							end_keyed_value();
							status = ConciseURIStates.STARTING_FIELD;
							break;
					
						case '(': 
							error_occurred();
							status = ConciseURIStates.ERROR_STATE;
							break;
					
						case ')': 
							end_object_and_keyed_value();
							status = ConciseURIStates.STARTING_FIELD;
							break;
					
						case '=': 
							error_occurred();
							status = ConciseURIStates.ERROR_STATE;
							break;
					
						default: 
							add_to_field();
							break;
					
					}
					break;
				}
			
				case ConciseURIStates.ERROR_STATE: 
				{
					string error_message = string.Format("The Expression may not be compatiable to conciseURI encoding. current_event={0}, field={1}",current_event,field.ToString());
					throw new Exception(error_message);
//					add_to_field();
//					break;
				}
			
				default: 
				{
					string error_message = string.Format("The Expression may not be compatiable to conciseURI encoding. current_event={0}, field={1}",current_event,field.ToString());
					throw new Exception(error_message);
//					System.Console.Out.WriteLine("Error: Invalid machine state (" + status + ").");
				}			
			}
		}
	}

	class ConciseURIStates
	{
		public const int STARTING_FIELD = 1;
		public const int ADDING_TO_FIELD = 2;
		public const int STARTING_VALUE = 3;
		public const int ADDING_TO_VALUE = 4;
		public const int ERROR_STATE = 5;
	}
	public class SupportClass
	{
		/// <summary>
		/// Returns the last element of an ArrayList instance.
		/// </summary>
		/// <param name="arrayList">The ArrayList instance</param>
		/// <returns>The last element of the ArrayList</returns>  
		public static System.Object VectorLastElement(System.Collections.ArrayList arrayList)
		{
			return arrayList[arrayList.Count - 1];
		}

		/// <summary>
		/// Returns the last element of a Stack instance.
		/// </summary>
		/// <param name="stack">The Stack instance</param>
		/// <returns>The last element of the Stack</returns>
		public static System.Object VectorLastElement(System.Collections.Stack stack)
		{
			return stack.ToArray()[0];
		}


	}
}
