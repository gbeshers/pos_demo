﻿using System;
using System.Collections;

namespace CASL_engine
{
  // Class created for BUG# 8103 by DH

  class DatabaseSchemaInfo
  {
    GenericObject m_pDatabaseConnection = null;
    private string m_strErrorMessage = "Table \"{0}\" is missing Primary Key column(s). Please contact CORE Business Technologies for assistance.";

    //Bug 10096 NJ      
    private DatabaseSchemaInfo()
    {
      string site_name = c_CASL.GEO.get("site_name", "UNKNOW SITE NAME") as string;
      System.Web.HttpRequest current_request = System.Web.HttpContext.Current.Request;
      string server_ip = current_request.ServerVariables["SERVER_NAME"] as string; //e.g. 172.16.1.83
      m_strErrorMessage = server_ip + "/" + site_name + ": " + m_strErrorMessage;
    }
    //End Bug 10096
    private void SendEmailNotificationToDBA(string strMsg)
    {
      misc.CASL_call("a_method", "email_error", "args", new GenericObject("subject", "DATABASE SCHEMA ERROR", "error_text", strMsg));
    }

    private void LogPKError(string strTableName)
    {
      string strMsg = string.Format(m_strErrorMessage, strTableName);

      // Notify the DBA about missing Primary Keys
      SendEmailNotificationToDBA(strMsg);

      // Also report it in our log file
      Logger.LogError(strMsg, "LogPKError");
    }

    private void LogPKColumns(string strTableName, GenericObject pColumns)
    {
      string strColumns = "";
      string strPKName = "";
      int iCnt = pColumns.getLength();

      for (int i = 0; i < iCnt; i++)
      {
        GenericObject pGeo = (GenericObject)pColumns.get(i);

        if (i == 0)
          strPKName = pGeo.get("IndexName").ToString();

        strColumns += pGeo.get("ColumnName").ToString();
        if (iCnt > 1 && i < iCnt - 1)
          strColumns += ",";

        pGeo = null;
      }

      String strMsg = String.Format("DB SCHEMA - PRIMARY KEY TABLE[{0}] INCLUDED COLUMNS[{1}] PK NAME[{2}]", strTableName, strColumns, strPKName);
      Logger.LogError(strMsg, "LogPKColumns");
    }

    public static bool LogDatabaseSchemaInfo(params Object[] args)
    {
      // This is the function called a single time per load of the Web App.
      // Please pass all the project tables to it.

      GenericObject pArgs = misc.convert_args(args);
      GenericObject pAllProjectTables = (GenericObject)pArgs.get("project_tables");

      // Bug #8648 Mike O - Don't let this prevent the application from running
      try
      {
        DatabaseSchemaInfo _this = new DatabaseSchemaInfo();
        _this.m_pDatabaseConnection = (GenericObject)pArgs.get("database");

        //---------------------------------------------------------------
        // Add the product level tables
        ArrayList arrTables = new ArrayList();
        arrTables.Add("CUST_FIELD_DATA");
        arrTables.Add("GR_CUST_FIELD_DATA");
        arrTables.Add("TG_ACTLOG_DATA");
        arrTables.Add("TG_DEPFILE_DATA");
        arrTables.Add("TG_DEPOSIT_DATA");
        arrTables.Add("TG_DEPOSITTNDR_DATA");
        // Bug 22611 UMN remove TG_FILESOURCE_DATA
        arrTables.Add("TG_GL_VALID_DATA");
        //arrTables.Add("TG_GROUP_DATA"); BUG# 10045 DH - Table was removed from design.
        arrTables.Add("TG_IMAGE_DATA");
        arrTables.Add("TG_ITEM_DATA");
        arrTables.Add("TG_PAYEVENT_DATA");
        arrTables.Add("TG_REVERSAL_EVENT_DATA");
        arrTables.Add("TG_REVERSAL_TRAN_DATA");
        arrTables.Add("TG_SUSPEVENT_DATA");
        arrTables.Add("TG_SUSPTRAN_DATA");
        arrTables.Add("TG_TENDER_DATA");
        arrTables.Add("TG_TRAN_DATA");
        arrTables.Add("TG_TTA_MAP_DATA");
        arrTables.Add("TG_UPDATE_DATA");
        arrTables.Add("TG_UPDATEFILE_DATA");

        //Add the project level tablses
        foreach (int index in pAllProjectTables.getIntegerKeys())
          arrTables.Add(pAllProjectTables.get(index).ToString());
        //---------------------------------------------------------------

        //---------------------------------------------------------------
        // ANALIZE PRIMARY KEYS
        foreach (string strTableName in arrTables)
          _this.ProcessPrimaryKeyColumns(strTableName);
        //---------------------------------------------------------------

        //---------------------------------------------------------------
        // ANALIZE NONCLUSTERED INDEXES
        foreach (string strTableName in arrTables)
          _this.ProcessNonClusteredIndexColumns(strTableName);
        //---------------------------------------------------------------
      }
      catch (Exception e) //12081 NJ-removed unused variable
      {
        // HX: Bug 17849
        Logger.cs_log_trace("LogDatabaseSchemaInfo Failed", e);
      }
      // End Bug #8648

      return true;
    }

    private void ProcessPrimaryKeyColumns(string strTableName)
    {
      // Retrieve all primary key columns

      GenericObject pSchema = new GenericObject();
      string strSQL = string.Format("SELECT i.name AS IndexName,COL_NAME(ic.OBJECT_ID,ic.column_id) AS ColumnName FROM sys.indexes AS i INNER JOIN sys.index_columns AS ic ON i.OBJECT_ID = ic.OBJECT_ID AND i.index_id = ic.index_id WHERE i.is_primary_key = 1 and OBJECT_NAME(ic.OBJECT_ID) = '{0}'", strTableName);

      db_vector_reader_sql pReader = (db_vector_reader_sql)db_vector_reader_sql.CASL_make("db_connection", m_pDatabaseConnection, "sql_statement", strSQL);
      pSchema = (GenericObject)db_vector_reader_sql.CASL_insert_data("reader", pReader, "_subject", pSchema, "start", 0, "insert_size", 0);

      // Log error and send email to DBA
      if (pSchema == null || pSchema.getLength() == 0)
        LogPKError(strTableName);
      else
        LogPKColumns(strTableName, pSchema);// Write all PK columns to log file.

      pReader = null;
    }

    private void ProcessNonClusteredIndexColumns(string strTableName)
    {
      // Retrieve all NONCLUSTERED index columns
      // If table or columns don't exist, just skip skip. NONECLUSTERED indexes are not required.

      GenericObject pSchema = new GenericObject();
      string strSQL = string.Format("SELECT i.name AS IndexName,COL_NAME(ic.OBJECT_ID,ic.column_id) AS ColumnName FROM sys.indexes AS i INNER JOIN sys.index_columns AS ic ON i.OBJECT_ID = ic.OBJECT_ID AND i.index_id = ic.index_id WHERE i.is_primary_key = 0 and OBJECT_NAME(ic.OBJECT_ID) = '{0}'", strTableName);

      db_vector_reader_sql pReader = (db_vector_reader_sql)db_vector_reader_sql.CASL_make("db_connection", m_pDatabaseConnection, "sql_statement", strSQL);
      pSchema = (GenericObject)db_vector_reader_sql.CASL_insert_data("reader", pReader, "_subject", pSchema, "start", 0, "insert_size", 0);

      if (pSchema == null || pSchema.getLength() == 0)
      {
        pReader = null;
        return;
      }

      LogNCColumns(strTableName, pSchema);// Write all NC columns to log file.

      pReader = null;
    }

    private void LogNCColumns(string strTableName, GenericObject pColumns)
    {
      string strColumns = "";
      string strNCName = "";
      int iCnt = pColumns.getLength();

      for (int i = 0; i < iCnt; i++)
      {
        GenericObject pGeo = (GenericObject)pColumns.get(i);

        if (i == 0)
          strNCName = pGeo.get("IndexName").ToString();

        strColumns += pGeo.get("ColumnName").ToString();
        if (iCnt > 1 && i < iCnt - 1)
          strColumns += ",";

        pGeo = null;
      }

      String strMsg = String.Format("DB SCHEMA - NUNCLUSTERED INDEX TABLE[{0}] INCLUDED COLUMNS[{1}] INDEX NAME[{2}]", strTableName, strColumns, strNCName);
      Logger.LogError(strMsg, "LogNCColumns");
    }
  }
}
