using System;
using System.Collections;
using System.Data;
/* Error Codes
 * DB-1 -- Could not make db_reader, type unknown.
 * DB-2 -- Could not execute db_reader, type unknown.
 * 
 */
namespace CASL_engine
{
	/// <summary>
	/// The db_reader class acts as the static method container for the different db_vector_readers.
	/// This marshals which reader to return.  All db_vector_readers should now implement IDBVectorReader.
	/// </summary>
	public class db_reader
	{
    #region Enumerations
    public enum DBType { SQLServer, Unknown };
    #endregion

    public static IDBVectorReader MakeTransactionReader(GenericObject db_connection,
      int query_timeout, int more_data_size, int more_data_timeout,
      ref string error_message)
    {
      IDBVectorReader reader=null;
      DBType dbType=GetDBType(db_connection);
      switch(dbType)
      {
        case DBType.SQLServer:
          reader=db_vector_reader_sql.make_transaction_reader(db_connection,
            query_timeout, more_data_size, more_data_timeout, ref error_message);
          break;
        default:
          error_message="Could not make db_reader.  Type unknown: " + dbType;
          break;
      }
      return reader;
    }
    // Construct a new IDBVectorReader -- Marshals based on db_connection type.
    public static IDBVectorReader Make(GenericObject db_connection, string sql_text, CommandType command_type,
      int query_timeout, Hashtable sql_args, int more_data_size, int more_data_timeout, GenericObject object_class,
      ref string error_message)
    {
      IDBVectorReader reader=null;
      DBType dbType=GetDBType(db_connection);
      switch(dbType)
      {
        case DBType.SQLServer:
          reader=db_vector_reader_sql.make(db_connection, sql_text, command_type,
            query_timeout, sql_args, more_data_size, more_data_timeout, object_class, ref error_message);
          break;
        default:
          error_message="Could not make db_reader.  Type unknown: " + dbType;
          break;
      }
      return reader;
    }


    // This may become public but currently is only used internally.
    private static DBType GetDBType(GenericObject db_connection)
    {
      string parent_name=db_connection.get("connection_type","",true) as string;
      switch(parent_name)
      {
        case "sql_server":
          return DBType.SQLServer;
        default:
          return DBType.Unknown;
      }
    }
	}
}
