using System;
using System.Data;
using System.Threading;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using CASL_engine;



//
// Utility methods for processing SQL Parameters
//
namespace CASL_engine
{
  /// <summary>
  /// Utility methods for processing SQL Parameters
  /// </summary>
  public class DatabaseParamUtils
  {
    /// <summary>
    /// Set the correct SQL type from a C# type.
    /// Could have used Parameters.ConvertTypeCodeToDbType() from System.Web.UI.WebControls
    /// but I was worried that it would treat strings as NVARCHAR
    /// Bug 25175
    /// </summary>
    /// <returns></returns>
    public static SqlDbType getSqlDbType(object arg_value, string parameterName)
    {
      if (arg_value == null)
        return System.Data.SqlDbType.VarChar;
      else if (arg_value is string)
        return System.Data.SqlDbType.VarChar;
      else if (arg_value is int)
        return System.Data.SqlDbType.Int;
      else if (arg_value is float)
        return System.Data.SqlDbType.Float;
      else if (arg_value is double)
        return System.Data.SqlDbType.Float;
      else if (arg_value is decimal)
        return System.Data.SqlDbType.Decimal;
      else if (arg_value is long)
        return System.Data.SqlDbType.BigInt;
      else if (arg_value is char)
        return System.Data.SqlDbType.VarChar;
      else if (arg_value is DateTime)
        return System.Data.SqlDbType.DateTime;
      else if (arg_value is byte[])
        return System.Data.SqlDbType.Image;
      else if (arg_value is Boolean) // Bug 15479 MJO - Added bit support
        return System.Data.SqlDbType.Bit;
      else
      {
        // Bug 25175 / 23557: Fixed error code and cleaned up
        string argValue = arg_value == null ? "null" : arg_value.ToString();
        string msg = "DatabaseParamUtils: getSqlDbType:  Unhandled object type in sql_args: value of '" + argValue
          + "' type: " + arg_value.GetType()
          + " for parameter: " + parameterName;
        Logger.cs_log(msg, Logger.cs_flag.error);

        misc.CASL_call("a_method", "actlog", "args",
          new GenericObject(
              "app", ""
              , "user", ""
              , "action", "Error"
              , "summary", "Database Parameter Error"
              , "detail", "Error:" + misc.cleanUpDetailsForActLog(msg) 
                + ",Stack:" + misc.cleanUpDetailsForActLog(Environment.StackTrace)  //misc.FormatStacktraceForActivityLog(e)
              )
              );


        throw CASL_error.create("message", msg, "code", "DB_SQL-90");
      }    
	  }
  }
}
