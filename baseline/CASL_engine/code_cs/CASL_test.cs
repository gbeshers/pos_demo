using System;
using System.Web;
using System.Reflection;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Xml;
using System.Text;
using System.Windows.Forms;

namespace CASL_engine
{

	public class CASL_test
	{
		public string a_instance_member = "intance_member_value_original";
		public static ArrayList a_array_list = new ArrayList();
		public static object[] a_object_array = new object[1];
		public static GenericObject a_GenericObject = new GenericObject("x",5, 0,"hi");
		public static Type a_class = typeof(CASL_test);
		public static object return_a_class()
		{
			return a_class;
		}
		public CASL_test()
		{
			a_instance_member = "intance_member_value_init";
		}
		public object test_instance_method_with_flex_args(params object[] args)
		{
			return args.Length;
		}
		public object test_instance_method_with_fix_args(int arg1, string arg2, object arg3)
		{
			return ""+arg1+arg2+arg3;
		}
		public static object test_class_method_with_flex_args(params object[] args)
		{
			return args.Length;
		}
		public static object test_class_method_with_fix_args(int arg1, string arg2, object arg3)
		{
			return ""+arg1+arg2+arg3;
		}
		public static object test_class_method_with_no_args()
		{
			return "no args";
		}
		public static object get_a_array_list()
		{
			return a_array_list;
		}
		public static object get_a_object_array()
		{
			return a_object_array;
		}
	}
}