/* -*- mode: c; -*- */
// #define DEBUG
// #define DEBUG_GenObj
// #define DEBUG_allocation_type
// #define DICT_STATS
// #define METHOD_STATS
// #define DEBUG_GenObj_Lock

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Runtime.CompilerServices;
using System.Reflection;  // Remove after delegate flip
using System.IO;
using System.Collections.Specialized;

using System.Diagnostics;

namespace CASL_engine
{
  /// <summary>
  /// COM Interface to GenericObject.
  /// Interface Added by Daniel Hofman - 
  /// Need this in order to pass as COM obj to and from DLLs or ActiveX controls.
  /// </summary>
  [Guid("DBE0E8C4-1C61-41f3-B6A4-4E2F353D3D05")]
  public interface IGenericObjectInterface
  {     
    GenericObject insert(Object value);
    GenericObject set(Object key, Object value);
    Object get(object key);
    T get<T>(object key);
    bool has(Object key);
    Object remove(Object key);
    void removeAll();
    Object[] getObjectArr_StringKeys();
    int getIntKeyLength();
  }

  /// <summary>
  /// Represents a collection of ordered key-and-value pairs. 
  /// Remarks: 
  /// GenericObject uses a private hashtable member to store unorderd key-and-value pairs. 
  /// As the element of key-and-value pair is added to a GenericObject, the pair is saved 
  /// in hashtable member and order of element is managed by GenericObject itself. 
  /// 
  /// In each element of key-and-value pair, A couple different kinds of Keys are used for 
  /// special purpose or the key management.
  /// o All System Keys start with character of "_". Reserved System Key include "_parent" 
  /// that indicates CASL type of a GenericObject instance. You can add your own system key 
  /// for any special purpose.
  /// o Integer key can be managed inside GenericObject if the user passes null as key. 
  /// Inside-managed Integer keys are zero-based consecutive 32-bit signed integer. public 
  /// member intKeyLength indicate the length of these zero-based consecutive integer keys. 
  /// o String keys is the common key that usually managed by the user. you can use those to 
  /// present C# class members or methods. The method of getStringKeys() will return an 
  /// ArrayList of string keys which are commonly used in foreach loop to get the individual 
  /// keyed value and also determine the length of string keys by ArrayList::count method.
  /// o complex key can be used. The methods on the any C# typed object, Object::Equals() 
  /// and Object::GetHashCode(), need override to get value based on this complex key. 
  /// GenericObject itself already override two methods, so it serves will for this purpose.
  /// 
  /// In each element of key-and-pair, Values can be null or any type of C# object including 
  /// GenericObject.
  /// </summary>
  ///

  // For the moment treat this with a LARGE grain of salt.
  public enum GO_kind {
    basic,
    symbol,
    path,
    cs_method,
    casl_method,
    base_method, // Special case: just the method associated with the base
                 // frame of the CASL_Stack.  It makes the PerfGraph code
                 // more uniform.  Should always be method_idx == 0.
#if FUTURE
    method_call,
#endif
    casl_class,
    casl_return, // Intended to speed up return handling.
    casl_code,

    casl_frame, // Legacy.  We convert CASL_Frame's to GenericObjects
                // for display in the ide.
    casl_stack, // Vector of casl_frame GenericObjects
    
    session,
    GEO,
    error
  };

  //
  // TTS#24619 GMB.  Implement DeepDiff() for developer Q/A purposes.
  //
  // The Match_Control describes the different levels of acceptable matching
  // that GenericObject.DeepDiff() can support.
  //
  // Each Diff_Rec is used to record a single difference, the Diff_kind records
  // the kind of difference found---except that eq_* and mt_success are actually
  // recording matches which is occasionally useful.
  
  public enum Match_Control {
    match_equal,        // True DeepDiff -- the default
    match_regex,        // All members must exist, but matching is sufficient,
                        // currently matching is only available for strings.
                        // **mt_indeterminate for other C# objects which are
                        // not identical.
    match_extras,       // Equality, but extra fields are not an issue
    match_extras_regex  // Everything in the pattern must match, extras OK.
    };

  public enum Diff_kind {
    eq_ident,        // identically same object
    eq_value,        // primitive type equals
    eq_geo,          // structurally equal GenericObjects
    mt_success,      // match succeeded
    mt_failed,       // match failed
    mt_loop_active,  // There is a loop in the GEO structures being compared
                     // and we have just noticed this, to avoid infinite
                     // recursion we note the loop but stop comparison.
                     // The equality of the two GEOs is correctly recorded
                     // on the first path where it is encountered.
                     // This could be improved on if we have loops in large
                     // GenericObjects.
    mt_indeterminate,// match of non-CASL objects of same C# type, e.g.,
                     // CASL_Frame
    df_null,         // left or right is null, but not both
    df_Csharp_type,  // objects have different C# types
    df_value,        // primitive objects of same type unequal value
    df_CASL_parent,  // GenericObjects have different CASL _parent (type)
    df_geo,          // GenericObject equal _parent, but different
    df_CS_object     // Non identical, non GenericObjects, e.g., CASL_Frame
                     // or Exception

  };

  public struct Diff_Rec {
    public GenObj_Path path;
    public Diff_kind difference;
    public object left_val;
    public object right_val;
    public Diff_Rec(GenObj_Path p, Diff_kind d, object l, object r)
    {
      path = p;
      difference = d;
      left_val = l;
      right_val = r;
    }
  };

#if DEBUG_GenObj_Lock
  public enum Lock_Kind {
    none,
    code,           // Object is considered to be code, usually is or
                    // derived from GenObj_Code.
    active_foreach  // Foreach is active using this object
  }
#endif

  //[DebuggerDisplay("{DebuggerDisplayGO,nq}")]
  [Serializable]// Added by Daniel Hofman - Need this in order to pass as COM obj to and from DLLs or ActiveX controls on IIS.
  public class GenericObject : IGenericObjectInterface, ISerializable 
  {
    //private members
#if FIX_ME
    [NonSerialized] private protected ArrayList vectors;
#else
    [NonSerialized] public ArrayList vectors;
#endif

#if DEBUG
    private protected Hashtable dict;
#else
    private protected HybridDictionary dict;
#endif

    public GO_kind kind = GO_kind.basic;

#if DEBUG_GenObj_Lock
    public Lock_Kind locked_as = Lock_Kind.none;
#endif

#if DEBUG_GenObj
    // Provided unique identifiers that can be seen in Visual Studio
    // *and* flight recorder logs.
    public static long num_GenObj_allocated = 0;
    public long gen_obj_number = -1;
#endif

    public static long get_total_genobj_allocated()
    {
#if DEBUG_GenObj
      return num_GenObj_allocated;
#else
      return (long) -1;
#endif
    }
    
    public long get_gen_obj_number()
    {
#if DEBUG_GenObj
      return gen_obj_number;
#else
      return (long) -1;
#endif
    }

#if DEBUG_allocation_type
    // Record where an object is created --- this is very useful
    // for debugging the interpreter but probably not much else.

    public const int num_stack_frames = 6;
    [NonSerialized] public string[] gen_obj_StackTrace = null;
    CASL_Frame casl_frame = null;

    public static string [] new_line = new string[] {Environment.NewLine};

    public static bool stacktrace_enabled = false;
    public void setStackTrace()
    {
      if (! stacktrace_enabled)
        return;
      string st = Environment.StackTrace;
      string [] stk = st.Split(new_line, StringSplitOptions.None);

      int size = stk.Length - 3; // 3 skipped frames
      if (size > num_stack_frames)
        size = num_stack_frames;
      gen_obj_StackTrace = new string[size];

      // Note: stk[0] is System.Environment.GetStackTrace() so we skip it.
      //       stk[1] is System.Environment.get_StackTrace() so we skip it.
      //       stk[2] is GenericObject.setStackTrace() so we skip it.
      for (int i = 0; i < size; i++) {
        gen_obj_StackTrace[i] = stk[i+3];
        if (stk[i+3].IndexOf("handle_request") >= 0)
          break;
      }
      // Disable recycling of CASL_Frame's on the current stack.
      casl_frame = CASL_Stack.capture_casl_stacktrace();
      return;
    }
#endif

#if DICT_STATS
    public class dict_stats {
      public long get_cnt = 0;
      public long has_cnt = 0;
      public long set_cnt = 0;
      public long rem_cnt = 0;
    };
    public static long gen_obj_key_identical = 0;
    public static long gen_obj_key_different = 0;
    public static Dictionary<Object, dict_stats> gen_obj_key_stats =
      new Dictionary<Object, dict_stats>();
#endif


    public void GetObjectData(SerializationInfo info, StreamingContext context) 
    { 
      //info.AddValue("vectors_arraylist", vectors); 
      //base.GetObjectData(info,context);
    } 


    public static GenericObject CreateGO_M(params Object[] args)
    {
      return new GenericObject(args);
    }

    //
    // This is really just for DEBUG, but we always define it even
    // if when running production it always returns null.  This avoids
    // having #define DEBUG_PATH in other source files.
    //
    public string get_path_to_me
    {
      get {
        string ptm = null;
#if DEBUG_GenObj
        ptm = "[" + safe_to_path() + ": [<" + gen_obj_number.ToString() + ">]]";
#else
        ptm = "[" + safe_to_path() + "]";
#endif
        return ptm;
      }
    }

    //
    // Single method to modify GenericObjects including the generic Constructor
    // (which had a long standing bug).  This way the migration of integer keys
    // to vectors from dict is systematically enforced in one place.
    //
    // Ulterior motive is to implement a "watch" capability on GenericObjects
    // based on 'this' and the key --- integrer and string keys.
    //
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void internal_set(object key, object value)
    {
#if DEBUG_GenObj_Lock
      if (locked_as != Lock_Kind.none) {
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          Flt_Rec.log("GenericObject.internal_set()",
                      " LOCK_ERROR " + locked_as.ToString() + "  " +
                      ShowKey(next_vector_key), true, true);
      }
#endif
#if DEBUG_GenObj
      if (Flt_Rec.objects_of_interest.ContainsKey(gen_obj_number) &&
          Flt_Rec.objects_of_interest[gen_obj_number].HasFlag(GO_Actions.set)) {
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          Flt_Rec.log("GenericObject.internal_set() " + get_path_to_me + "  ",
                      "key = " + CASL_Frame.show_value(key) +
                      "  value = " + CASL_Frame.show_value(value), true, true);
      }

      if (dict.Contains("_name") && dict["_name"] is string name &&
          Flt_Rec.names_of_interest.ContainsKey(name) &&
          Flt_Rec.names_of_interest[name].HasFlag(GO_Actions.set)) {
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          Flt_Rec.log("GenericObject.internal_set() " + get_path_to_me + "  ",
                      "key = " + CASL_Frame.show_value(key) +
                      "  value = " + CASL_Frame.show_value(value), true, true);
      }

      if (key is string skey) {
#if DEBUG_parent_class
        if (skey == "_parent") {
          if (value == null) {
            if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
              Flt_Rec.log("GenericObject.internal_set() " + get_path_to_me,
                          "  _parent being set to null", true, true);
          } else if (! (value is GenObj_Class)) {
            if (value is GenericObject geo) {
              if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
                Flt_Rec.log("GenericObject.internal_set() " + get_path_to_me + "  ",
                            "_parent should always be a GenObj_Class '" + 
                            CASL_Frame.show_value(geo) + "'", true, true);
            } else {
              if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
                Flt_Rec.log("GenericObject.internal_set() " + get_path_to_me + "  ",
                            "_parent being set to non GenericObject = " +
                            CASL_Frame.show_value(value), true, true);
            }
          }
        }
#endif
        if (Flt_Rec.keys_of_interest.ContainsKey(skey) &&
            Flt_Rec.keys_of_interest[skey].HasFlag(GO_Actions.set)) {
          if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
            Flt_Rec.log("GenericObject.internal_set() " + get_path_to_me + "  ",
                        "key = " + CASL_Frame.show_value(key) +
                        "  value = " + CASL_Frame.show_value(value), true, true);
        }
      }

      if (dict.Contains("_parent")) {
        object par = dict["_parent"];
        if (par == null) {
          Logger.cs_log("internal_set() _parent was set to null",
                        Logger.cs_flag.error);
          Debugger.Break();
        } else if (par is GenObj_Class parent &&
                   parent.tracking.HasFlag(GO_Actions.set)) {
          if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
            Flt_Rec.log("GenericObject.internal_set() parent=" +
                        parent.full_name() + "  " + get_path_to_me + "  ",
                        "key = " + CASL_Frame.show_value(key) +
                        "  value = " + CASL_Frame.show_value(value), true, true);
#if NOT_YET
          // Something similiar must be done for general support of classes
          // derived from GenericObject --- we are not there yet.
        } else if (par is GenericObject geo) {
          Logger.cs_log("internal_set() bad _parent for " + get_path_to_me +
                        "  " + geo.get_path_to_me);
          if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
            Flt_Rec.log("GenericObject.internal_set() bad _parent for " +
                        get_path_to_me, "  parent=" + geo.get_path_to_me, true, true);
          Debugger.Break();
#endif
        }
      }
#endif

      if (key is int) {
        int iKey = (int) key;
        if (0 <= iKey) {
          if (iKey < vectors.Count) {
            vectors[iKey] = value;
            goto Stats;
          }
          if (iKey == vectors.Count) {
            // Extending vectors case
            vectors.Add(value);
            while (dict.Contains(vectors.Count)) {
              int moving_key = vectors.Count;
              vectors.Add(dict[moving_key]);
              dict.Remove(moving_key);
            }
            goto Stats;
          }
        }
      }

      dict[key] = value;

Stats:
      ;
#if DICT_STATS
      if (gen_obj_key_stats.ContainsKey(key))
        gen_obj_key_stats[key].set_cnt++;
      else {
        dict_stats ds = new dict_stats();
        ds.set_cnt++;
        gen_obj_key_stats.Add(key, ds);
      }
#endif
    }


    private void internal_multiple_set(Object[] args, int len)
    {
      if( args.Length % 2 != 0 )
        throw CASL_error.create("title","Syntax Error",
           "message","'set' requires key-value pairs, but odd number of arguments was found.");

      bool is_key = true;
      Object key = null;
      foreach (Object arg in args) {
        if (is_key) {
          key = arg;
        } else
          internal_set(key, arg);
        is_key = !is_key;
      }
    }


    public GenericObject(params Object[] args)
    {
       // WARNING: The GenericObject get() code depends on dict[key] returning
       // null when dict.Contains(key) is false.  This is true for both Hashtable
       // and HybridDictionary but not for Dictionary<Key, Value>.  GMB.
#if DEBUG
      dict = new Hashtable();
#else
      dict = new HybridDictionary();
#endif
      vectors = new ArrayList();
#if DEBUG_GenObj
      gen_obj_number = num_GenObj_allocated++;
#endif
#if DEBUG_allocation_type
      setStackTrace();
#endif

#if DEBUG_GenObj
      if (Flt_Rec.objects_of_interest.ContainsKey(gen_obj_number) &&
          Flt_Rec.objects_of_interest[gen_obj_number].HasFlag(GO_Actions.create)) {
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          Flt_Rec.log("GenericObject created ", gen_obj_number.ToString(),
                      true, true);
      }
#endif

      if (args.Length % 2 != 0) {
        Logger.cs_log("GenericObject(...) called with odd number of arguments",
                      Logger.cs_flag.warn);
      }

      bool bIsKey = true; 
      Object objLastKey = null; // handle only string keys

      foreach (Object arg in args) // alternative key and value
      {
        // Handle Keys
        if ( bIsKey )
        {
          if ( arg == null ) { // implicit vector key
            Logger.cs_log("GenericObject(...) implicit vector key " +
                           args.Length.ToString(), Logger.cs_flag.error);
            objLastKey= vectors.Count; 
          } else  // explicit vector key & string key & integer key
            objLastKey = arg;
        }
          // Handle Value
        else 
          internal_set(objLastKey, arg);
        bIsKey = !bIsKey;
      }
#if DEBUG_GenObj
      if (dict.Contains("_name") && dict["_name"] is string name &&
          Flt_Rec.names_of_interest.ContainsKey(name) &&
          Flt_Rec.names_of_interest[name].HasFlag(GO_Actions.set)) {
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          Flt_Rec.log("GenericObject created ", get_path_to_me, true, true);
      }
#endif
    }



    /// <summary>
    /// lookup only for STRING KEY (deny lookup for vector keys,interger keys, complex object keys)
    /// </summary>
    /// <param name="key"></param>
    /// <param name="lookup"> lookup=false</param>
    /// <returns></returns>
    public bool has(Object key, bool lookup)
    {
#if DICT_STATS
      if (gen_obj_key_stats.ContainsKey(key))
        gen_obj_key_stats[key].has_cnt++;
      else {
        dict_stats ds = new dict_stats();
        ds.has_cnt++;
        gen_obj_key_stats.Add(key, ds);
      }
#endif

#if DEBUG_GenObj      
      if (key is string skey && Flt_Rec.keys_of_interest.ContainsKey(skey) &&
          Flt_Rec.keys_of_interest[skey].HasFlag(GO_Actions.has)) {
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          Flt_Rec.log("GenericObject.has() " + get_path_to_me + "  ",
                      "key = " + CASL_Frame.show_value(skey) +
                      "  result = " + dict.Contains(key).ToString(),
                      true, true);
      }

      if (Flt_Rec.objects_of_interest.ContainsKey(gen_obj_number) &&
          Flt_Rec.objects_of_interest[gen_obj_number].HasFlag(GO_Actions.has)) {
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          Flt_Rec.log("GenericObject.has() " + get_path_to_me + "  ",
                      "key = " + CASL_Frame.show_value(key) +
                      "  result " + dict.Contains(key).ToString(), true, true);
      }

      if (dict.Contains("_name") && dict["_name"] is string name &&
          Flt_Rec.names_of_interest.ContainsKey(name) &&
          Flt_Rec.names_of_interest[name].HasFlag(GO_Actions.has)) {
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          Flt_Rec.log("GenericObject.has() " + get_path_to_me + "  ",
                      "key = " + CASL_Frame.show_value(key) +
                      "  result = " + dict.Contains(key).ToString(),
                      true, true);
      }
#endif

      if(key is int && ((int)key)<vectors.Count && ((int)key)>=0)
        return true;
      else if( dict.Contains(key)) return true;
      if( lookup && key is string )//&& key is string)// TODO lookup for string key
      {
        GenericObject lookup_object = this;
        while (lookup_object.dict.Contains("_parent") )
        {
          lookup_object = lookup_object.dict["_parent"] as GenericObject;
          if(lookup_object == null)
            return false;
          else if( lookup_object.dict.Contains(key))
            return true;
        } 
        return false;
      }
      else
        return false;
    }
    /// <summary>
    /// Determine whether the GenericObject contains a specific key
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public bool has(Object key)
    {
      return has(key, false);
    }

    /// <summary>
    /// Get the value associated with the specified key.  
    /// If the key is not found, return the opt parameter return_object_for_fail
    /// </summary>
    /// <param name="key"></param>
    /// <param name="return_object_for_fail"></param>
    /// <returns></returns>
    public Object get(Object key, Object if_missing, bool lookup)
    {
#if DICT_STATS
      if (gen_obj_key_stats.ContainsKey(key))
        gen_obj_key_stats[key].get_cnt++;
      else {
        dict_stats ds = new dict_stats();
        ds.get_cnt++;
        gen_obj_key_stats.Add(key, ds);
      }
#endif
      object ret = null;

      if(key is int && ((int)key)<vectors.Count && ((int)key)>=0) {
        ret = vectors[(int)key];
        goto tracing;
      }
      if (dict.Contains(key)) {
        ret = dict[key];
        goto tracing;
        // optimization block for lookup
      }

      if( lookup && key is string )// lookup for string key
      {
        GenericObject lookup_object = this;
        while (lookup_object.dict.Contains("_parent") )
        {
          lookup_object = lookup_object.dict["_parent"] as GenericObject;
          if(lookup_object == null)
          {
            // optimization code, same as: return misc.handle_if_missing(if_missing,key,this);
            if( if_missing == c_CASL.c_error)
            {
              string message = string.Format("{0} not found", misc.to_eng(key));
              throw CASL_error.create("code",220, "message",
                                   message, "key",key, "subject",this);
            }
            else {
              ret = if_missing;
              goto tracing;
            }
            // end optimization code
          }
          else if(lookup_object ==c_CASL.GEO) //special look up for GEO: look up my first then GEO itself
          {
            //GenericObject my = (GenericObject)CASLInterpreter.get_my();
            //if ( my !=null && my.dict.Contains(key))
            //  return my[key];    // MP: Should not automatically look in 'my'.  Be explicit.
            if (c_CASL.GEO.dict.Contains(key)) { // c_CASL.GEO != null && 
              ret = c_CASL.GEO.dict[key];
              goto tracing;
            } else if ("my".Equals(key)) {
              ret = CASLInterpreter.get_my();
              goto tracing;
            } else
            {  
              // optimization code, same as: return misc.handle_if_missing(if_missing,key,this);
              if( if_missing == c_CASL.c_error)
              {
                string message = string.Format("{0} not found", misc.to_eng(key));
                throw CASL_error.create("code",220, "message", message, "key",key, "subject",this);
              }
              else {
                ret = if_missing;
                goto tracing;
              }
              // end optimization code
            }
          }
          else if( lookup_object.dict.Contains(key)) {
            ret = lookup_object.dict[key];
            goto tracing;
          }
        } 
        // optimization code, same as: return misc.handle_if_missing(if_missing,key,this);
        if( if_missing == c_CASL.c_error)
        {
          string message = string.Format("{0} not found", misc.to_eng(key));
          throw CASL_error.create("code",220, "message", message, "key",key, "subject",this);
        }
        else {
          ret = if_missing;
          goto tracing;
        }
        // end optimization code
      }
        // end of optimization block
      else
      { 
        // optimization code, same as: return misc.handle_if_missing(if_missing,key,this);
        if( if_missing == c_CASL.c_error)
        {
          string message = string.Format("{0} not found", misc.to_eng(key));
          throw CASL_error.create("code",220, "message", message, "key",key, "subject",this);
        }
        else {
          ret = if_missing;
          goto tracing;
        }
        // end optimization code      ret = if_missing;
      }

    tracing:
#if DEBUG_GenObj      
      if (Flt_Rec.objects_of_interest.ContainsKey(gen_obj_number) &&
          Flt_Rec.objects_of_interest[gen_obj_number].HasFlag(GO_Actions.get)) {
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          Flt_Rec.log("GenericObject.get() " + get_path_to_me + "  ",
                      "key = " + CASL_Frame.show_value(key) +
                      "  value = " + CASL_Frame.show_value(ret), true, true);
      }

      if (dict.Contains("_name") && dict["_name"] is string name &&
          Flt_Rec.names_of_interest.ContainsKey(name) &&
          Flt_Rec.names_of_interest[name].HasFlag(GO_Actions.get)) {
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          Flt_Rec.log("GenericObject.get() " + get_path_to_me + "  ",
                      "key = " + CASL_Frame.show_value(key) +
                      "  value = " + CASL_Frame.show_value(ret), true, true);
      }
      if (key is string skey && Flt_Rec.keys_of_interest.ContainsKey(skey) &&
          Flt_Rec.keys_of_interest[skey].HasFlag(GO_Actions.get)) {
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          Flt_Rec.log("GenericObject.get() " + get_path_to_me + "  ",
                      "key = " + CASL_Frame.show_value(key) +
                      "  value = " + CASL_Frame.show_value(ret), true, true);
      }
#endif
      return ret;
    }


    // Bug 24518 - Parameterization is nicer than casting
    public T get<T>(Object key, Object if_missing, bool lookup)
    {
      try
      {
        return (T)get(key, if_missing, lookup);
      }
      catch (InvalidCastException)
      {
        Object value = get(key, if_missing, lookup);

        Logger.cs_log("Error: Failed to cast '" + value.ToString() + "' of type " + value.GetType().FullName + " to type " + typeof(T).FullName);

        throw;
      }
    }

    // keep this function for backward compatibility
    public Object get(Object key, Object if_missing)
    {
#if DICT_STATS
      if (gen_obj_key_stats.ContainsKey(key))
        gen_obj_key_stats[key].get_cnt++;
      else {
        dict_stats ds = new dict_stats();
        ds.get_cnt++;
        gen_obj_key_stats.Add(key, ds);
      }
#endif

      if(key is int && ((int)key)<vectors.Count && ((int)key)>=0 )
        return vectors[(int)key];
      object R = dict[key];
      if( R != null)
        return R;
      else if (R==null && dict.Contains(key))
        return null;

        // optimization code, same as: return misc.handle_if_missing(if_missing,key,this);
      if (if_missing == c_CASL.c_error)
      {
        string message = string.Format("{0} not found", misc.to_eng(key));
        throw CASL_error.create("code",220, "message", message,
                             "key",key, "subject",this);
      }
      return if_missing;
      // end optimization code
    }	

    // Bug 24518 - Parameterization is nicer than casting
    public T get<T>(Object key, Object if_missing)
    {
      try
      {
        return (T)get(key, if_missing);
      }
      catch (InvalidCastException)
      {
        Object value = get(key, if_missing);

        Logger.cs_log("Error: Failed to cast '" + value.ToString() + "' of type " + value.GetType().FullName + " to type " + typeof(T).FullName);

        throw;
      }
    }

    /// <summary>
    /// Get the value associated with the specified key. 
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public Object get(Object key)
    {
#if DICT_STATS
      if (gen_obj_key_stats.ContainsKey(key))
        gen_obj_key_stats[key].get_cnt++;
      else {
        dict_stats ds = new dict_stats();
        ds.get_cnt++;
        gen_obj_key_stats.Add(key, ds);
      }
#endif

      if(key is int && ((int)key)<vectors.Count && ((int)key)>=0)
        return vectors[(int)key];
      object R = dict[key];
      if( R != null)
        return R;
      else if (R==null && dict.Contains(key))
        return null;

      // optimization code, same as: return misc.handle_if_missing(c_CASL.c_error, key, this);
      string message = string.Format("{0} not found", misc.to_eng(key));
      throw CASL_error.create("code",220, "message", message,
                           "key",key, "subject",this);
    }	

    // Bug 24518 - Parameterization is nicer than casting
    public T get<T>(Object key)
    {
      try
      {
        return (T)get(key);
      }
      catch(InvalidCastException)// Bug# 27153 DH
      {
        Object value = get(key);
		
        Logger.cs_log("Error: Failed to cast '" + value.ToString() + "' of type " + value.GetType().FullName + " to type " + typeof(T).FullName);

        throw;
      }
    }

    //
    // These are *ONLY* for debug code --- explicitly show_value() but
    // other code involving CASL_Stack tracing/dumps can use them also.
    //

    public object get_container()
    {
#if DEBUG
      if (dict.ContainsKey("_container"))
#else
      if (dict.Contains("_container"))
#endif
        return dict["_container"];
      return null;
    }
    
    public object get_name()
    {
#if DEBUG
      if (dict.ContainsKey("_name"))
#else
      if (dict.Contains("_name"))
#endif        
        return dict["_name"];
      return null;
    }
    /// <summary>
    /// Set the value associated with the specified key. 
    /// Add new key if key is not exist.
    /// </summary>
    /// <param name="key"></param>
    /// <param name="objValue"></param>
    // Bug 24518 - Always returns GenericObject
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public GenericObject set(Object key, Object value)
    {
       internal_set(key, value);
       return this;
    }

    //
    // Export portions of the dictionary interface in a way
    // that keeps our ability to enforce the GenericObject
    // abstractions.
    //

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Add(Object key, Object value)
    {
      internal_set(key, value);
    }

    public object this[Object key]
    {
      get
      {
        return this.get(key, null);
      }
      set
      {
        this.internal_set(key, value);
      }
    }

    public ArrayList Values
    {
      get
      {
        return new ArrayList(dict.Values);
      }
    }

    public int Count
    {
      get
      {
        return dict.Count;
      }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public bool Contains(Object key)
    {
      return this.has(key);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public object Remove(object key)
    {
      return this.remove(key);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Clear()
    {
#if DEBUG_GenObj_Lock
      if (locked_as != Lock_Kind.none) {
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          Flt_Rec.log("GenericObject.Clear()",
		      "LOCK_ERROR " +
		      locked_as.ToString(), true, true);
        }
#endif
      dict.Clear();
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public Hashtable getHashtable()
    {
#if DEBUG
      return dict.Clone() as Hashtable;
#else
      // Bug 23662: Carefully convert sql_args to Hashtable
      Hashtable ret = new Hashtable();

      foreach (DictionaryEntry entry in this)
        ret[entry.Key] = entry.Value;
      // End Bug 23662
      return ret;
#endif      
    }


    /// <summary>
    /// Set the multiple key_and_value pair. 
    /// Add new key if the key is not exist.
    /// </summary>
    /// <param name="key_and_value_pair_array">Specify the Array of keys and values alternatively</param>
    // Bug 24518 - Always returns GenericObject
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public GenericObject set(params object[] key_and_value_pair_array)
    {
      internal_multiple_set(key_and_value_pair_array,
                            key_and_value_pair_array.Length);
      return this;
    }

    /// <summary>
    /// Add value to the next zero-based consecutive integer key. 
    /// The integer key length is based on intKeyLength.
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    // Bug 24518 - Always returns GenericObject
    public GenericObject insert(Object value)
    {
      internal_set(vectors.Count, value);
      return this;
    }

    /// <summary>
    /// remove// Bug# 27153
    /// </summary>
    /// <param name="key"></param>
    /// <param name="shift"></param>
    /// <param name="if_missing"></param>
    /// <returns></returns>
    public Object remove(Object key, bool shift, object if_missing)
    {
#if DICT_STATS
      if (gen_obj_key_stats.ContainsKey(key))
        gen_obj_key_stats[key].rem_cnt++;
      else {
        dict_stats ds = new dict_stats();
        ds.rem_cnt++;
        gen_obj_key_stats.Add(key, ds);
      }
#endif

      if(key is int && ((int)key) < vectors.Count && ((int)key)>=0 )
      {
        int ikey = (int) key;
        object R =  vectors[ikey];
        if(shift || (int)key == vectors.Count-1) // SHIFT AUTOMATIC IN ARRAYLIST
        {
          // Need to shift all ids for vector fields.
          // For all objects above the removed object
          for(int i=ikey+1;i<vectors.Count;i++) 
          {
            //get the object
            object o=vectors[i];
            //update id if a geo
            if(o is GenericObject) 
            {
              //cast the object
              GenericObject geo=(GenericObject) o;
              //find its id key
              string name_key=(string)geo.get("_part_name_key",null,true);
              //update the id if the id exists, this object is contained and the id is the index
              if(name_key!=null && geo.is_contained() && geo.get(name_key).Equals(i)) 
              {
                geo.set(name_key, i-1);
              }
            }
          }

          vectors.RemoveAt(ikey);
        }
        else // NONSHIFT, MOVE VECTOR KEY-VALUE TO INTERGER KEY-VALUE
        {
          int interger_object_length = vectors.Count-ikey-1;
          ArrayList interger_objects = new ArrayList();
          interger_objects.InsertRange(0,vectors.GetRange(ikey+1,interger_object_length));
          vectors.RemoveRange(ikey,interger_object_length+1);
          int interger_key = ikey+1;
          foreach(object interger_object in interger_objects)
          {
            dict[interger_key] = interger_object;
            interger_key++; 
          }
        }
        return R;
      }
      else if( dict.Contains(key))
      {
        object R = dict[key];
        dict.Remove(key);
        return R;
      }
      else
      {
        // optimization code, same as: return misc.handle_if_missing(if_missing,key,this);
        if( if_missing == c_CASL.c_error)
        {
          string message = string.Format("{0} not found", misc.to_eng(key));
          throw CASL_error.create("code",220, "message", message, "key",key, "subject",this);
        }
        else
          return if_missing;
        // end optimization code
      }  
    }

    /// <summary>
    /// Removes the element of key-and-value pair with integer key of intKeyLength-1, 
    /// and returns removed value
    /// </summary>
    /// <returns></returns>
    public Object remove()
    {
      int last_index = vectors.Count-1;
      object last_value = vectors[last_index];
      vectors.RemoveAt(last_index);

#if DICT_STATS
      if (gen_obj_key_stats.ContainsKey(last_index))
        gen_obj_key_stats[last_index].rem_cnt++;
      else {
        dict_stats ds = new dict_stats();
        ds.rem_cnt++;
        gen_obj_key_stats.Add(last_index, ds);
      }
#endif

      return last_value;
    }														 
    /// <summary>
    /// Removes the element of key-and-value pair with the specified key, and returns removed value
    /// </summary>
    /// <param name="key"> shift=true if_missing=error</param>
    /// <returns></returns>
    public Object remove(Object key)
    {
#if DICT_STATS
      if (gen_obj_key_stats.ContainsKey(key))
        gen_obj_key_stats[key].rem_cnt++;
      else {
        dict_stats ds = new dict_stats();
        ds.rem_cnt++;
        gen_obj_key_stats.Add(key, ds);
      }
#endif

      if(key is int && ((int)key)<vectors.Count && ((int)key)>=0)
      {
        Object R = vectors[(int)key];
        vectors.RemoveAt((int)key); // AUTOMATICALLY SHIFT
        return R;
      }
      else if (dict.Contains(key))
      {
#if DEBUG_GenObj_Lock
        if (locked_as != Lock_Kind.none) {
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          Flt_Rec.log("GenericObject.remove(key)",
		      " LOCK_ERROR " + locked_as.ToString() + "  " +
		      Lock_ShowKey(key), true, true);
        }
#endif
        Object R = dict[key];
        dict.Remove(key);
        return R;
      }
      else
      {
        // optimization code, same as: return misc.handle_if_missing(c_CASL.c_error, key, this);
        string message = string.Format("{0} not found", misc.to_eng(key));
        throw CASL_error.create("code",220, "message", message, "key",key, "subject",this);
        // end optimization code
      }
    }

    /// <summary>
    /// remove Bug# 27153 DH
    /// </summary>
    /// <param name="key">shift=true </param>
    /// <param name="if_missing"></param>
    /// <returns></returns>
    public Object remove(Object key, object if_missing)
    {
#if DICT_STATS
      if (gen_obj_key_stats.ContainsKey(key))
        gen_obj_key_stats[key].rem_cnt++;
      else {
        dict_stats ds = new dict_stats();
        ds.rem_cnt++;
        gen_obj_key_stats.Add(key, ds);
      }
#endif

      return remove(key, true,if_missing);
    }

    /// <summary>
    /// Removes all elements of key-and-value pair
    /// </summary>
    public void removeAll()
    {
#if DEBUG_GenObj_Lock
      if (locked_as != Lock_Kind.none) {
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          Flt_Rec.log("GenericObject.removeAll()", " LOCK_ERROR " +
                      locked_as.ToString(), true, true);
        Debugger.Break();
      }
#endif
      vectors.Clear();
      dict.Clear();
    }

    static public void Initialization()
    {
#if DICT_STATS
      if (! gen_obj_key_stats.ContainsKey("_parent"))
        gen_obj_key_stats.Add("_parent", new dict_stats());
#endif
      return;
    }

    // Bug 20947 UMN need to check all digits in name (91001-1)
    static bool digitsOnly(string s)
    {
      int len = s.Length;
      for (int i = 0; i < len; ++i)
      {
        char c = s[i];
        if (c < '0' || c > '9')
          return false;
      }
      return true;
    }

    /// <summary>
    /// relative to the subject, go down a path of keys where the keys are integers or strings.
    /// slip path and convert path_part if it's integer
    /// GEO.x.0.y == GEO.<get_path> "x.0.y" </get_path>
    /// path=req lookup=false=bool if_missing=error
    /// </summary>
    /// <returns></returns>
    /// ONLY USED FROM C#.   USING CASL's get_path implementation because it works for: foo."100".bar
    public static object unknown = new object();
    public Object get_path(string path, bool lookup, object if_missing) 	
    {
      string [] path_parts=null;

      if (path == null)
      {
         Logger.cs_log("GenericObject.get_path null or empty path");
         return false;
      }

      // This was a "gotch" on GEO which has path_to_me == "".  GMB.
      if (path.Length == 0)
         return this;

      path_parts = path.Split('.');

      Object path_it = this;
      foreach (string path_part_string in path_parts )
      {
        object path_part = path_part_string;

        // Bug 20947 UMN need to check all digits in name (91001-1)
        if (path_part_string.Length > 0 && digitsOnly(path_part_string))
          path_part = Convert.ChangeType(path_part_string, typeof(int));

        if (ConciseXMLCharSet2.IS_DOUBLEQUOTE(path_part_string[0]) ||
            ConciseXMLCharSet2.IS_SINGLEQUOTE(path_part_string[0]))
          path_part = path_part_string.Substring(1, path_part_string.Length-2);

        //
        // IPAY-325.  There are two failing cases when if_missing should
        // be returned.
        //    1. We don't have a GenericObject so get() doesn't make sense.
        //    2. The path_part doesn't exist.  In this case we get back the unique
        //       "unknown" object which should never be a valid return from get().
        //

        if (path_it is GenericObject geo) {
          path_it = geo.get(path_part, unknown, lookup);
          if (path_it != null && unknown.Equals(path_it))
            return if_missing;
        } else
          return if_missing;
      }
      return path_it;
    }

    public Object get_path(string path, bool lookup) 		
    {
      return get_path(path,lookup,c_CASL.c_error);
    }

    public Object get_path(string path) 		
    {
      return get_path(path,false,c_CASL.c_error);
    }

    /// <summary>
    /// Get an ArrayList containing all the keys 
    /// - integer key, string key, system key, and complex keys.
    /// NOTE: 
    /// The order of the keys in the ICollection is unspecified in hashtable class,
    /// but it is the same order as the associated values in the ICollection returned by the Values method.
    /// </summary>
    /// <returns></returns>


    public ArrayList getDictKeys()
    {
      return new ArrayList(dict.Keys);
    }

    public ArrayList getKeys()
    {
      ArrayList all_keys = new ArrayList(dict.Keys);
  
      for(int i=0; i<vectors.Count; i++)
        all_keys.Add(i);
      return all_keys;
    }


    public ArrayList get_normalized_keys()
    {
      ArrayList all_keys = new ArrayList(dict.Keys);
      ArrayList system_keys = new ArrayList();
      ArrayList no_system_no_vector_keys = new ArrayList();
      ArrayList keys = new ArrayList();
      foreach( object key in all_keys)
      {
        if( key is string && key.ToString().StartsWith("_"))
          system_keys.Add(key);
        else if ( key is int && (int)key < vectors.Count && ((int)key)>=0)
          continue;
        else
          keys.Add(key);
      }
      // sort string key 
      GEO_comparer comparer = new GEO_comparer();
      system_keys.Sort(comparer);
      keys.Sort(comparer);
      keys.InsertRange(0,system_keys);
      for(int i=0; i< vectors.Count;i++)
        keys.Add(i);
      return keys;
    }
	
    /* vector_key - contiguous vector keys starting with 0
     * integer_key - vector keys plus discontiguous integer keys
     * object_key - object keys
     * string_key - string keys excluding system and meta keys
     * system_key - system keys (keys starting with "_" that don't include "_f_")
     * meta_key - meta keys (keys that include "_f_")
     * all - all of the above
     */

    public ArrayList getVectorKeys() 
    {
      int length=getLength();
			ArrayList alVectorKeys = new ArrayList(length);
      for( int i=0; i < length; i++ ) 
      {
        alVectorKeys.Add(i);
      }
      return alVectorKeys;
    }
	 
    public ArrayList getIntegerKeys()
    {
      ArrayList alIntegerKeys = new ArrayList();
      int length=getLength();
      for( int i=0; i < length; i++ ) 
      {
        alIntegerKeys.Add(i);
      }
      foreach (Object key in dict.Keys )
      {
        if ( key is int )
        {
          alIntegerKeys.Add(key);
        }
      }
      return alIntegerKeys;
    }
							 
    public ArrayList getObjectKeys()
    {
      ArrayList alObjectKeys = new ArrayList();
      foreach (Object key in dict.Keys )
      {
        if ( key is GenericObject )
        {
          alObjectKeys.Add( key );
        }
      }
      return alObjectKeys;
    }
      
    public ArrayList getStringKeys()
    {

      ArrayList alStringKeys = new ArrayList();
      foreach (Object key in dict.Keys )
      {
        if ( key is String && !( key.ToString().StartsWith("_") ) && ( key.ToString().IndexOf("_f_") < 0 ) )
        {
          alStringKeys.Add(key);
        }
      }
      return alStringKeys;
    }				

    public ArrayList getSystemKeys()
    {

      ArrayList alSystemKeys = new ArrayList();
      foreach (Object key in dict.Keys )
      {
        if ( key is String && ( key.ToString().StartsWith("_") ) && ( key.ToString().IndexOf("_f_") < 0 ) )
        {
          alSystemKeys.Add(key);
        }
      }
      return alSystemKeys;
    }				

    public ArrayList getMetaKeys()
    {

      ArrayList alMetaKeys = new ArrayList();
      foreach (Object key in dict.Keys )
      {
        if ( key is String &&  ( key.ToString().IndexOf("_f_") >= 0 ) )
        {
          alMetaKeys.Add(key);
        }
      }
      return alMetaKeys;
    }
    public ArrayList getAllKeys() 
    {
			ArrayList alKeys = new ArrayList(dict.Keys);
			alKeys.AddRange(getVectorKeys());
      return alKeys;
    }
    public ArrayList getRegularKeys()
    {
      ArrayList alKeys = getVectorKeys();
      foreach (object key in dict.Keys) {
        if (key is string && key.ToString().StartsWith("_"))
          continue;
        alKeys.Add(key);
      }
      return alKeys;
    }

    public ArrayList getValues()
    {
      ArrayList alValues = new ArrayList(dict.Values);
      // Bug 24902 MJO - Fixed index out of bounds exception
      alValues.AddRange(vectors);
      return alValues;
    }

    /// <summary>
    /// FINISHED 
    /// NOT USED, USE COPY INSTEAD. ALWAYS RETURN NULL. Get a shallow clone of current GenericObject instance 
    /// </summary>
    /// <returns></returns>
    /*public GenericObject clone()
    {
      GenericObject R = base.Clone() as GenericObject;
      R.vectors = this.vectors.Clone() as ArrayList ;

      return R;
    }*/

    /// <summary>
    /// FINISHED 
    /// TODO: CHANGE vectors.clone to copyto
    /// use generic clone function in object
    /// this is not safe for dynamic change hashtable
    /// </summary>
    /// <returns></returns>
    public GenericObject fast_clone()
    {
      GenericObject R = base.MemberwiseClone() as GenericObject;
      R.vectors= this.vectors.Clone() as ArrayList;

      return R;
      //return (GenericObject)this.MemberwiseClone();
    }

    /// <summary>
    /// ?? vector key alway copy
    /// selective shallow copy 
    /// include and exclude can not be option at the same time. 
    /// 
    /// If include is not opt, then loop through include keys
    ///   if( object.has include key and (exclude is opt or not exclude.key_of key) )
    ///     copy each field
    /// else (include is  opt), then loop through all keys
    ///   if( not exclude.key_of key )
    ///     copy each field
    /// </summary>
    /// <param name="include"></param>
    /// <param name="exclude"></param>
    /// <returns></returns>
    public GenericObject copy(GenericObject include, GenericObject exclude)
    {
      GenericObject a_copy = (GenericObject)c_CASL.c_GEO();
      a_copy.vectors = this.vectors.Clone() as ArrayList; // ?? TODO: always copy

      if (!c_CASL.c_opt.Equals(include))  // include=vector exclude=any
      {
        for(int i=0; i<include.getLength(); i++)
        {
          object key = include.get(i);
          if( has(key) && (c_CASL.c_opt.Equals(exclude) || false.Equals(exclude.key_of(key))))
            a_copy.set(key, get(key)); // included and not in exclude
        }
      }
      else  
      {
        foreach (object key in dict.Keys)
        {
          if( false.Equals(exclude.key_of(key)))
            a_copy.set(key, dict[key]);
        }
      }
      return a_copy;				
    }

    //
    // Given an object derived from GenericObject allocate a new object
    // with the same class.  The copy constructors are used to initialize
    // members, but the vector and dictionary information are handled by
    // the calling routine (because of include_list and exclude_list
    // processing.
    //
    // Bug# Bug 27153 DH - Clean up warnings when building ipayment
    public static GenericObject GenObj_Allocate(GenericObject subject)
    {
      switch (subject.kind) {
      case GO_kind.basic:
        return c_CASL.c_GEO();

      case GO_kind.symbol:

        return new GenObj_Symbol(subject as GenObj_Symbol);

      case GO_kind.path:
        return new GenObj_Path(subject as GenObj_Path);

      case GO_kind.cs_method:
        return new GenObj_CS_Method(subject as GenObj_CS_Method);

      case GO_kind.casl_method:
        return new GenObj_CASL_Method(subject as GenObj_CASL_Method);

#if Possible_Future
      case GO_kind.method_call:
        // This is not yet used; idea is that certain methods would get "hard coded",
        // e.g., <if>, <do>, <set>, <get>, <has> where we can probably get to the
        // point where C# overloading avoids the lookup.
        break;
#endif

      case GO_kind.casl_class:
        return new GenObj_Class(subject as GenObj_Class);

      case GO_kind.casl_return:
        return new GenObj_Class(subject as GenObj_Class);

      case GO_kind.casl_code:
        return new GenObj_Code(subject as GenObj_Code);

      case GO_kind.casl_frame:
        return new GenObj_Frame();

      case GO_kind.casl_stack:
        return new GenObj_Stack();

      case GO_kind.session:
        throw CASL_error.create_str("misc.cs:Copy(...) -- I don't believe we should copy" +
                             " the GenericObject associated with each session.");

      case GO_kind.GEO:
        throw CASL_error.create_str("misc.cs:Copy(...) -- we do NOT copy GEO.  Ever.");

      case GO_kind.error:
        return new GenObj_Error();
      }
      return null;
    }


    //
    // This is now GenObj hierarchy aware.  TTS#23757
    //

    public static GenericObject deepCopy(params Object[] arg_pairs)
    {
      GenericObject args = (GenericObject)misc.convert_args(arg_pairs);
      GenericObject geoToBeCopied = args.get("_subject", c_CASL.c_undefined) as GenericObject;

      Dictionary<GenericObject, GenericObject> geoAlreadyCopied =
        new Dictionary<GenericObject, GenericObject>();

      object oTopPath = geoToBeCopied.to_path();

      GenericObject res = geoToBeCopied.deepCopyAux(geoAlreadyCopied, oTopPath.ToString(), true);

      return res;
    }

    /// <summary>
    /// TODO: copy vectors values 
    /// </summary>
    /// <param name="geoAlreadyCopied"></param>
    /// <param name="sTopPath"></param>
    /// <param name="bIsTop"></param>
    /// <returns></returns>
    public GenericObject deepCopyAux(
        Dictionary<GenericObject, GenericObject> geoAlreadyCopied,
        String sTopPath,
        bool bIsTop)
    {
      //If the value to be copied has already been copied (by value), return the 
      //reference to it
      if( geoAlreadyCopied.ContainsKey(this) )
        return geoAlreadyCopied[this];

      GenericObject a_copy = GenericObject.GenObj_Allocate(this);

      geoAlreadyCopied.Add(this, a_copy);

      a_copy.vectors = this.vectors.Clone() as ArrayList; // TODO, copy one-level deep
      bool bExternalObj = false;
      // Bug 18933 MJO - Changed enumerator for HybridDictionary
      IDictionaryEnumerator myEnumerator =
          dict.GetEnumerator() as IDictionaryEnumerator;

      //Loop through all key/value pairs in the Generic Object
      while ( myEnumerator.MoveNext() )
      {
        // At the top level ONLY, do not copy the container
        // (Two people can't sit in the same chair...)
        if( "_container".Equals(myEnumerator.Key) && bIsTop )
          continue;

        // If the Value to be copied is a Generic Object, determine if it's a 
        // reference to an external object (compare it's path with the top level path)
        if( myEnumerator.Value is GenericObject ) {
          String sThisPath = "";
          sThisPath = (myEnumerator.Value as GenericObject).to_path().ToString();
          bExternalObj = !sThisPath.StartsWith(sTopPath);
        }

        // If the Value to be copied is not a generic object at all __OR__
        // the Value to be copied is an External generic object, 
        // simply copy the value (copy by reference, basically)

        if( !(myEnumerator.Value is GenericObject) || bExternalObj ) {
          a_copy.set(myEnumerator.Key, myEnumerator.Value);
        } else {
            //If the Value to be copied is an Internal generic object, call deepCopy
            //recursively so that all internal values are copied by value, not reference
          GenericObject v = myEnumerator.Value as GenericObject;
          GenericObject r = v.deepCopyAux(geoAlreadyCopied, sTopPath, false);
          a_copy.set(myEnumerator.Key, r);
        }
      }
      return a_copy;
    }

    /// <summary>
    /// FINISHED
    /// </summary>
    /// <returns></returns>
    public GenericObject make_reverse()
    {
      ArrayList keys = getKeys();
      GenericObject reverse_obj = new GenericObject();
      foreach( object key in keys )
      {
        object value = dict[key];
        if( key is String && ((string)key).StartsWith("_"))
          reverse_obj.set(key, value);
        else
          reverse_obj.set(value, key);
      }
      return reverse_obj;
    }

    /// <summary>
    /// Get the value of the last integer key
    /// </summary>
    /// <returns></returns>
    public object top_of()
    {
      if( vectors.Count == 0) 
        throw CASL_error.create("message", "length is 0");
      else
        return vectors[vectors.Count];
    }

    public IDictionaryEnumerator GetEnumerator()
    { return dict.GetEnumerator(); }

    public int getLength()
    {
      if (vectors == null) return 0;
      return vectors.Count;
    }
    /// <summary>
    /// TOREMOVE
    /// </summary>
    /// <returns></returns>
    public int getIntKeyLength()
    {
      return getLength();
    }

    //----------------------------------------
    // Format Menthods - XML, URI, HTML
    //---------------------------------------
    /// <summary>
    /// Returns a query String with conciseXML encoding that represents the current GenericObject
    /// e.g. ?key_1=value_1&key_2=value_2
    /// reference formatting issue:
    /// check to_path && contained_in
    /// check circular reference
    /// </summary>
    /// <returns></returns>

    public String to_xml()
    {
      return misc.cs_GenericObject_to_xml(this);
    }

    /// <summary>
    /// ?? USED 
    /// optinal param container is used to create relative path when it is avaiable.
    /// </summary>
    /// <param name="container"></param>
    /// <returns></returns>
    public String to_htm_large(String path)
    {
      String sReturn = "";
      if ( path.Length== 0 )
        path = this.to_path().ToString();  // to_path() returns either false
                                           // or a string.

      String sType = this.get("_parent", "GenericObject").ToString();
      sReturn += String.Format("<TR><TD colspan=2>A {0}</TD></TR>",sType);
      ArrayList ordKeys = this.get_normalized_keys();
      foreach ( Object objKey in ordKeys )
      {
        String sKey = objKey.ToString();
        if(sKey.StartsWith("_"))
          continue;
        Object objValue = dict[objKey];
        String sValue = "";
        if ( objValue == null )
          sValue = "";
        else if ( objValue.GetType() ==  typeof(GenericObject) )
          sValue = ((GenericObject)objValue).to_htm_large(path + "." + sKey);
        else
          sValue = objValue.ToString();
        sReturn += String.Format("<TR ><TD><span id='{0}.key'>{1}</span></TD><TD><span id='.value'>{2}</span></TD></TR>", path, sKey ,sValue);
      }
      sReturn = String.Format("<Span id='{0}'><Table border='1' width='100%'>{1}</Table></Span>", path, sReturn);
      return sReturn;
    }

    /// <summary>
    /// Returns an URI String with conciseURI encoding that represents the current GenericObject
    /// </summary>
    /// <returns></returns>
    public String toUri()
    {
      String sReturn = "";

      //-------------------------------------------------------
      // Handle null
      if ( this == null)
      {
        return sReturn;
      }

      String sStartXMLTag = "(", sEndXMLTag = ")";
      ArrayList ordKeys = this.getKeys();
      sReturn += sStartXMLTag;
      int iIndex = 0;
      foreach ( Object objKey in ordKeys )
      {
        String sKey = "", sValue = "";
        if ( objKey.GetType().IsPrimitive )
          sKey = "";
        else
          sKey = objKey.ToString() + "=";
        Object objValue = dict[objKey];
        if ( objValue == null )
          sValue = "";
        else if ( objValue.GetType() ==  typeof(GenericObject) )
          sValue = ((GenericObject)objValue).toUri();
        else
          sValue = objValue.ToString();

        if ( iIndex > 0)
          sReturn += CASLParser.CONCISEURI_FIELD_CONNECTOR + sKey + sValue;
        else 
          sReturn += sKey + sValue;
        iIndex++;
      }
      sReturn += Environment.NewLine + sEndXMLTag;
      return sReturn;
    }

    //----------------------------------------
    // Path related Methods
    //---------------------------------------


    /// <summary>
    /// new to_path functions: 
    /// return <concat> [_container.<to_path/>] _name</concat> if path is avaiable
    /// return null if path is not avaiable (no _name or _container)
    /// return "" if _subject is GEO
    /// </summary>
    /// <returns></returns>
    public object to_path()
    {
      return misc.ToPath(this, c_CASL.GEO);
    }



    //----------------------------------------
    // Convenient Menthods
    //---------------------------------------
    // Add getStringValues() ; getStringKeys()
    //----------------------------------------
    /// <summary>
    /// Get an ArrayList of values associated with the specified "_parent" text - type
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public ArrayList getTypedValues(String type)
    {
      ArrayList alValues = new ArrayList();		
      foreach( Object objValue in getValues())
      {
        if ( objValue is GenericObject )
        {
          GenericObject thValue = (GenericObject)objValue;
          if( type.Equals(((String)thValue.get("_parent"))) )
          {
            alValues.Add(thValue);
          }
        }
      }
      return alValues;
    }

    /// <summary>
    /// Flat special field object to the multiple meta fields in the CASL file definition
    /// e.g. 
    /// field: outer_key=<field> key='inner_key' value='default_value' req=true </field>
    /// meta_field: outer_key='default_value' outer_key_f_req=true
    /// </summary>
    /// <returns></returns>
    public bool flat_field()
    {
      ArrayList keys = this.getKeys().Clone() as ArrayList;		
      foreach( Object key in keys)
      {
        object obj_value = get(key);
        if ( obj_value is GenericObject)
        {
          GenericObject gobj_value = obj_value as GenericObject;
          //flat field by using meta key 
          if ( gobj_value.get("_parent", "").ToString() == "field" )
          {
            Object outer_key = key;
            Object inner_key = "";
            GenericObject the_container = this;
            GenericObject the_field = gobj_value;
            if(!the_field.has("key"))
            {
              return false;
            }
            inner_key = the_field.get("key");
            if ( inner_key.ToString() != outer_key.ToString())
              return false;
            the_container.set(inner_key, the_field.get("value"));
            ArrayList field_keys = the_field.getStringKeys();
            foreach ( Object field_key in field_keys )
            {
              if(field_key.ToString()!="key" && field_key.ToString()!="value")
              {
                the_container.set(inner_key+"_f_"+field_key, the_field.get(field_key));
              }
            }
          }
            // flat field for nested GenericObject
          else 
            gobj_value.flat_field();
        }
      }
      return true;
    }

    /// <summary>
    /// Get the field object from the flatted meta fields in the CASL file definition
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public GenericObject get_field(Object key)
    {
      if( !has(key) )
        return null;
			
      GenericObject the_field = new GenericObject("_parent","field", "key",key, "value", get(key));
      ArrayList keys = getStringKeys();
      foreach( Object any_key in keys)
      {
        String s_any_key = any_key.ToString();
        if( s_any_key.IndexOf("" + key +"_f_") >=0)
        {
          String inner_key = s_any_key.Replace("" + key +"_f_", "");
          the_field.set(inner_key, get(any_key));
        }
      }
      return the_field;
    }


    /// <summary>
    /// Get the value which has the specified key_and_value pair
    /// </summary>
    /// <param name="key_in_value"></param>
    /// <param name="value_in_value"></param>
    /// <returns></returns>
    public ArrayList get_with_value(String key_in_value, Object value_in_value)
    {
      ArrayList alValues = new ArrayList();		
      foreach( Object objValue in getValues())
      {
        if ( objValue is GenericObject )
        {
          GenericObject thValue = (GenericObject)objValue;
          if( (thValue.get(key_in_value)).Equals(value_in_value) )
          {
            alValues.Add(thValue);
          }
        }
      }
      return alValues;
    }     

    /// <summary>
    /// Get the value which has the specified key
    /// </summary>
    /// <param name="key_in_value"></param>
    /// <returns></returns>
    public ArrayList get_with_key(String key_in_value)
    {
      ArrayList alValues = new ArrayList();		
      foreach( Object objValue in getValues())
      {
        if ( objValue is GenericObject )
        {
          GenericObject thValue = (GenericObject)objValue;
          if( ( thValue.has(key_in_value)) )
          {
            alValues.Add(thValue);
          }
        }
      }
      return alValues;
    }     
	    
    /// <summary>
    /// Get an Object Array which contains alternative string key and its value
    /// </summary>
    /// <returns></returns>
    public Object[] getObjectArr()
    {
      ArrayList KeysArr = getStringKeys();
      int iCounter = 0;
      object[] args = null; // Create parameter list.
      if(KeysArr.Count > 0)
      {
        args = new Object[KeysArr.Count*2];
        foreach ( Object key in KeysArr )
        {
          args[iCounter] = key;
          Object objValue = get(key);
          if(objValue!= null)
            args[iCounter+1] = objValue;
          else
            args[iCounter+1] = null;
          iCounter += 2;
        }
      }  
      return args;
    }

    /// <summary>
    /// Get an Object Array which contains alternative any key and its value
    /// </summary>
    /// <returns></returns>
    public Object[] getObjectArr_All()
    {
      ArrayList KeysArr = this.get_normalized_keys();
      int iCounter = 0;
      object[] args = null; // Create parameter list.
      if(KeysArr.Count > 0)
      {
        args = new Object[KeysArr.Count*2];
        foreach ( Object key in KeysArr )
        {
          args[iCounter] = key;
          Object objValue = get(key);
          if(objValue!= null)
            args[iCounter+1] = objValue;
          else
            args[iCounter+1] = null;
          iCounter += 2;
        }

      }  
      return args;
    }
	    
    /// <summary>
    /// Get an Object Array which contains all string keys
    /// </summary>
    /// <returns></returns>
    public Object[] getObjectArr_StringKeys()
    {
      return getStringKeys().ToArray();
    }
    /// <summary>
    /// Get an Object Array which contains all keys
    /// </summary>
    /// <returns></returns>
    public Object[] getObjectArr_AllKeys()
    {
      ArrayList R = getKeys() ;
      R.SetRange(R.Count,vectors);


      return R.ToArray();  //getKeys().ToArray();
    }

    /// <summary>
    /// reverse the interger_key_and_value pairs
    /// </summary>
    public void reverse()
    {
      vectors.Reverse();
      //   GenericObject go_reverse = new GenericObject();
      //   for(int i=0; i< vectors.Count; i++)
      //   {
      //    int new_i = vectors.Count -i-1;				
      //    go_reverse.set(new_i,get(i));
      //   }
      //   this.set(go_reverse.getObjectArr_All());
      return;
    }
	   
    /// <summary>
    /// Test if the current instance is the Error GenericObject
    /// <Error> message='error' </Error>
    /// </summary>
    /// <returns></returns>
    public bool isError()
    {
      // Bug #13644 Mike O - This now works
      if( get("_parent", null) == c_CASL.c_error)
      {
        return true;
      }
      return false;
    }
    /// <summary>
    /// If the GenericObject contains the value in a field, return the key for that field, else return false.
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public Object key_of(Object the_value)
    {
      int vector_key = vectors.IndexOf(the_value);
      if(vector_key > -1 ) 
        return vector_key; 
      foreach (object key in dict.Keys)
      {
        object value = get(key);
        if( key is string && ((string)key).StartsWith("__")) // ignore special system key such as __length, __parsed_field_order
          continue;
        if( the_value == null && value == null)
          return key;
        else if( the_value is GenericObject )
        {
          if(the_value== value) // compare reference for none-primitive value
            return key;
        }
        else if( the_value.Equals(value)) // handle primitive value
          return key;
      }
      return false;
    }

    //
    // When a GenericObject is used as a key for a HashTable,
    // I believe always of another GenericObject, then Equals()
    // and GetHashCode() are used.  The RuntimeHelpers methods
    // implement object identity presumably using object addresses
    // under the covers.
    //
    public override bool Equals(object obj)
    {
      return RuntimeHelpers.Equals(this, obj);
    }

    public override int GetHashCode()
    {
      return RuntimeHelpers.GetHashCode(this);
    }

    //
    // DeepEquals recursively tests all (key, value) pairs between
    // two objects.  The routine now checks to see if the kind fields
    // are not identical which implies a type mismatch and failure.
    //
    // Also a dictionary is now used to avoid infinite recursion in
    // the case of a cycle of GenericObject references.
    //
    public static object fCASL_DeepEquals(CASL_Frame frame)
    {
      GenericObject args = frame.args;
      // GenericObject subject = frame.subject as GenericObject;
      GenericObject subject =
          args.get("_subject", c_CASL.c_undefined) as GenericObject;
      GenericObject geoToBeCompared =
          args.get(0, c_CASL.c_undefined) as GenericObject;

      return (object) subject.DeepEquals(geoToBeCompared);
    }
    
    public bool DeepEquals(Object obj)
    {
      if( this.Equals(obj)) // compare reference first
        return true;
      if (obj == null || !(obj is GenericObject)) // compare C# type
        return false;
      return DeepEqualsAux(new HashSet<GenericObject>(), obj as GenericObject);
    }

    public bool DeepEqualsAux(HashSet<GenericObject> geo_seen, GenericObject c_obj)
    {
      if (this.Equals(c_obj))
        return true;
      if (kind != c_obj.kind)
        return false;
      if (geo_seen.Contains(c_obj))
        return true;   // The equality of this object is being tested at
                       // a higher level in the recursion or has already
                       // been determined to be true (false causes an
                       // immediate exit).
      geo_seen.Add(c_obj);
      // Compare vector lengths
      if (vectors.Count != c_obj.vectors.Count)
        {
          return false;
        }

      ArrayList my_keys = getKeys();
      ArrayList c_keys = c_obj.getKeys();
      if ( my_keys.Count != c_keys.Count ) // compare key length
        return false;
      foreach(object my_key in my_keys)
        {
          //-------------------------------------- 
          // compare all key and value in hashtable without the consideration of orderkeys
          //-------------------------------------- 
          object my_value = get(my_key);

          if( !c_obj.has(my_key) ) return false;
          object c_value = c_obj.get(my_key);
          if (my_value == null || c_value == null) // If only one value is null we're not okay
            {
              if( my_value != c_value)
                return false;
            }
          else if( (my_value is GenericObject) && (c_value is GenericObject) )
            {
              GenericObject my_value_GO=(my_value as GenericObject);
              GenericObject c_value_GO=(c_value as GenericObject);
              if( my_value_GO.has("_container") )
                {
                  if (c_value_GO.has("_container"))
                    {
                      if (!my_value_GO.get("_container").Equals(c_value_GO.get("_container")))  // The containers are unequal -- this is a problem
                        {
                          return false;
                        }
                    }
                  else // If only one has a container they're obviously not equal
                    {
                      return false;
                    }
                }
              else if (!my_value_GO.DeepEqualsAux(geo_seen, c_value_GO)) // Recurse on two GEOs
                {
                  return false;
                }
            }
          else if( !my_value.Equals(c_value) ) // prone to recursive call 
            return false;
        }
      return true; // Everything passed okay
    }

    ////////////////////////////////////////////////////////////////////
    //
    // TTS# 24619: Implement DeepDiff() for developer QA purposes.   GMB
    // This is foundational for TTS#24906 Reactiving CASL test.
    //
    ////////////////////////////////////////////////////////////////////

    //
    // Look for actual differences in the result list.
    //
    // Flag for mt_indeterminate might be in order.
    //
    public static bool DeepDiff_success(List<Diff_Rec> diff)
    {
      foreach (Diff_Rec dr in diff) {
        switch (dr.difference) {
        case Diff_kind.eq_ident:
          break;
        case Diff_kind.eq_value:
          break;
        case Diff_kind.eq_geo:
          break;
        case Diff_kind.mt_success:
          break;
        case Diff_kind.mt_failed:
          return false;
        case Diff_kind.mt_loop_active:
          break;
        case Diff_kind.mt_indeterminate:
          return true;
        case Diff_kind.df_null:
          return false;
        case Diff_kind.df_Csharp_type:
          return false;
        case Diff_kind.df_value:
          return false;
        case Diff_kind.df_CASL_parent:
          return false;
        case Diff_kind.df_geo:
          return false;
        case Diff_kind.df_CS_object:
          return false;
        }
      }
      return true;
    }

    //
    // Build a string suitable for displaying in a log file (or debugger).
    // To get just differences pass 'false' to show_equal.
    //
    // NOTE: mt_indeterminate is always noted.
    //
    public static StringBuilder DeepDiff_to_SB(bool show_equal, List<Diff_Rec> diff)
    {
      StringBuilder sb = new StringBuilder();
      foreach (Diff_Rec dr in diff) {
        string d = null;
        switch (dr.difference) {
        case Diff_kind.eq_ident:
          if (show_equal)
            d = Tool.path_to_string(dr.path) + "    ref ident";
          break;
        case Diff_kind.eq_value:
          if (show_equal)
            d = Tool.path_to_string(dr.path) + "    equ value";
          break;
        case Diff_kind.eq_geo:
          if (show_equal)
            d = Tool.path_to_string(dr.path) + "    equ GenObj";
          break;
        case Diff_kind.mt_success:
          if (show_equal)
            d = Tool.path_to_string(dr.path) + "    match succeeded";
          break;
        case Diff_kind.mt_failed:
          d = Tool.path_to_string(dr.path) + "    match failed pattern=\"" +
            dr.left_val.ToString() + "\"   target=\"" + dr.right_val.ToString() + "\"";
          break;
        case Diff_kind.mt_loop_active:
          d = Tool.path_to_string(dr.path) + "    loop detected";
          break;
        case Diff_kind.mt_indeterminate:
          d = Tool.path_to_string(dr.path) + "    match indeterminate, objects have same C# type but are not identical=\"" +
            dr.left_val.ToString() + "\"   target=\"" + dr.right_val.ToString() + "\"";
          break;
        case Diff_kind.df_null:
          if (dr.left_val == null)
            d = Tool.path_to_string(dr.path) + "    left null";
          else
            d = Tool.path_to_string(dr.path) + "    right null";
          break;
        case Diff_kind.df_Csharp_type:
          {
            Type ty_left  = dr.left_val as Type;
            Type ty_right = dr.right_val as Type;
            d = Tool.path_to_string(dr.path) + "    C# types differ: " +
              ty_left.Name + " != " +  ty_right.Name;
          }
          break;
        case Diff_kind.df_value:
          d = Tool.path_to_string(dr.path) + "    " +
            dr.left_val.ToString() + " != " + dr.right_val.ToString();
          break;
        case Diff_kind.df_CASL_parent:
          d = Tool.path_to_string(dr.path) + "    _parent differ ";
          GenericObject lp = dr.left_val as GenericObject;
          GenericObject rp = dr.right_val as GenericObject;
          d += lp.to_path().ToString() +  " != " + rp.to_path().ToString();
          break;
        case Diff_kind.df_geo:
          d = Tool.path_to_string(dr.path) + "    GenericObjects unequal";
          break;
        }
        sb.AppendLine(d);
      }
      return sb;
    }

    //
    // This could stand improvement.
    //
    // Primarily useful for testing from the ide and getting a
    // detailed report in the flight recorder.
    //
    public static object fCASL_DeepDiff(CASL_Frame frame)
    {
      GenericObject args = frame.args;
      object pattern = args.get("_subject", null);
      object target = args.get(0, null);
      object diff_match = args.get("match", "match_equal");
      object obj_show_equals = args.get("show_equals", "only_top");

      Match_Control mc = Match_Control.match_equal;
      switch (diff_match as string) {
      case "match_regex":
        mc = Match_Control.match_regex;
        break;
      case "match_extras":
        mc = Match_Control.match_extras;
        break;
      case "match_extras_regex":
        mc = Match_Control.match_extras_regex;
        break;
      default:
        break;
      }

      string show_equals = null;
      if (obj_show_equals is string)
        show_equals = obj_show_equals as string;
      else
        return "show_equals is a string which must be one of never|top_only|always";

      List<Diff_Rec> diff = DeepDiff(mc, pattern, target);
      if (DeepDiff_success(diff)) {
        if (show_equals != "never") {
          if (diff.Count == 1 && diff[0].difference == Diff_kind.eq_ident)
            return "identical";
          return "equal";
        }
        return "";
      }
      StringBuilder sb = DeepDiff_to_SB(show_equals == "always", diff);

      GenericObject res = new GenericObject();
      String[] split_str = new String[1];
      split_str[0] = Environment.NewLine;
      foreach (string s in sb.ToString().Split(split_str,
                         StringSplitOptions.RemoveEmptyEntries)) {
        res.insert(s);
      }

      return res;
    }

    //
    // Interface routine to the recursive traversal.
    //

    public static List<Diff_Rec> DeepDiff(
        Match_Control mc, object pattern, object target)
    {
      bool allow_regex = (mc == Match_Control.match_regex) ||
        (mc == Match_Control.match_extras_regex);
      bool allow_extra_field = (mc == Match_Control.match_extras) ||
        (mc == Match_Control.match_extras_regex);
      return DeepDiffAux(allow_regex, allow_extra_field,
                         new Dictionary<GenericObject, GenericObject>(),
                         new List<Diff_Rec>(),
                         new GenObj_Path(),
                         pattern, target, true);
    }

    public static Diff_Rec dd_Equals<T>(GenObj_Path path, T l, T r)
    {
      if (l.Equals(r))
        return new Diff_Rec(path, Diff_kind.eq_value, l, r);
      else
        return new Diff_Rec(path, Diff_kind.df_value, l, r);
    }

    //
    //     The main routine of DeepDiff() returns a complete list
    // of the pairwise comparisons done; perhaps too much and a flag
    // should be added to limit the result.
    //
    // NOTE: the left object may actually be a "pattern" that the
    // right object is expected to match.  This is controlled by the
    // first two parameters.
    //
    // allow_regex: if left is a Regex and right is a string then
    //    use the Regex.Match() method --- lack of a match is flagged
    //    as a difference.
    //
    //        NOTE: if allow_regex is set and two C# objects have the
    //        same type but are not identical then match will succeed
    // allow_extra_fields: the right object having extra entries is
    //    not considered a difference.  Really the question being asked
    //    is are a few key fields correct.
    //
    //    NOTE: if both allow_regex and allow_extra_fields are false then
    //       DeepDiff() is in fact testing deep equality.
    //
    // ok_map: handles structural equality of GenericObjects.
    //
    // diff_list: list of results for fields already traversed in an
    //    earlier iteration of DeepDiff.
    //
    // left: object or pattern to be matched
    // right: object being tested.
    // recur: false => difference already seen but provide more
    //     information about differences at this level---don't search deeper.
    //
    public static List<Diff_Rec> DeepDiffAux(
         bool allow_regex,
         bool allow_extra_fields,
         Dictionary<GenericObject, GenericObject> ok_map,
         List<Diff_Rec> diff_list,
         GenObj_Path current_path,
         object left,
         object right,
         bool recur)
    {
      if (left == right) {
        diff_list.Add(
            new Diff_Rec(
                current_path, Diff_kind.eq_ident, left, right));
        return diff_list;
      }
      if (left == null || right == null) {
        if (left == null && allow_extra_fields) {
          diff_list.Add(
              new Diff_Rec(current_path, Diff_kind.mt_success, left, right));
          return diff_list;
        }
        diff_list.Add(
            new Diff_Rec(
                current_path, Diff_kind.df_null, left, right));
        return diff_list;
      }

      Type ty_left = left.GetType();
      Type ty_right = right.GetType();
      if (ty_left != ty_right) {
        if (left is Regex && right is string && allow_regex) {
          Match m = (left as Regex).Match(right as string);
          if (m.Success)
            diff_list.Add(
                new Diff_Rec(
                    current_path, Diff_kind.mt_success, ty_left, ty_right));
          else
            diff_list.Add(
                new Diff_Rec(
                    current_path, Diff_kind.mt_failed, ty_left, ty_right));
        } else
          diff_list.Add(
              new Diff_Rec(
                  current_path, Diff_kind.df_Csharp_type, ty_left, ty_right));
        return diff_list;
      }

      if (ty_left.IsValueType) {
        if (left is bool?) {
          diff_list.Add(
             dd_Equals<bool?>(
                 current_path, left as bool?, right as bool?));
          return diff_list;
        }
        if (left is char?) {
          diff_list.Add(
             dd_Equals<char?>(
                 current_path, left as char?, right as char?));
          return diff_list;
        }
        if (left is int?) {
          diff_list.Add(
             dd_Equals<int?>(
                 current_path, left as int?, right as int?));
          return diff_list;
        }
        if (left is long?) {
          diff_list.Add(
             dd_Equals<long?>(
                 current_path, left as long?, right as long?));
          return diff_list;
        }
        if (left is float?) {
          diff_list.Add(
             dd_Equals<float?>(
                 current_path, left as float?, right as float?));
          return diff_list;
        }
        if (left is double?) {
          diff_list.Add(
             dd_Equals<double?>(
                 current_path, left as double?, right as double?));
          return diff_list;
        }
        if (left is decimal?) {
          diff_list.Add(
             dd_Equals<decimal?>(
                 current_path, left as decimal?, right as decimal?));
          return diff_list;
        }
      }

      if (left is String) {
        diff_list.Add(
             dd_Equals<String>(
                 current_path, left as string, right as string));
        return diff_list;
      }

      if ( ! (left is GenericObject)) {
        if (allow_regex) {
          diff_list.Add(
            new Diff_Rec(current_path, Diff_kind.mt_indeterminate, left, right));
        } else {
          diff_list.Add(
            new Diff_Rec(current_path, Diff_kind.df_CS_object, left, right));
        }
        return diff_list;
      }

      //
      // OK, now for the structural equality of GenericObjects.
      // First, we handle the circular references via the ok_map.
      // _parent is handled as a special case as it often points to a
      //   deeper error than other keys.
      // Then we process the vectors[] first including mis-matched lengths.
      // Finally, all other keys.
      //

      GenericObject l_geo = left as GenericObject;
      GenericObject r_geo = right as GenericObject;

      if (ok_map.ContainsKey(l_geo)) {
        if (ok_map[l_geo] == r_geo) {
          diff_list.Add(
              new Diff_Rec(
                  current_path, Diff_kind.mt_loop_active, l_geo, r_geo));
          return diff_list;
        }
        diff_list.Add(
            new Diff_Rec(
                current_path, Diff_kind.df_geo, l_geo, r_geo));
        return diff_list;
      }
      ok_map[l_geo] = r_geo;

      GenericObject lp = l_geo.get("_parent", null) as GenericObject;
      GenericObject rp = r_geo.get("_parent", null) as GenericObject;
      if (lp != rp) {
        diff_list.Add(
            new Diff_Rec(
                current_path, Diff_kind.df_CASL_parent, lp, rp));
        return diff_list;
      }

      //
      // If we found a difference in the calling level, don't
      // continue as we are likely generating junk.
      //
      if (! recur)
        return diff_list;

      int l_len = l_geo.vectors.Count;
      int r_len = r_geo.vectors.Count;
      int i = 0;
      ArrayList l_keys = new ArrayList(l_geo.dict.Keys);
      HashSet<object> r_keys = new HashSet<object>();
      foreach (object r_k in r_geo.dict.Keys)
        r_keys.Add(r_k);

      if (! allow_extra_fields) {
        if ((l_len != r_len) || (l_keys.Count != r_keys.Count))
          recur = false;
      } else {
        if ((l_len > r_len) || (l_keys.Count > r_keys.Count))
          recur = false;
      }

      for (i = 0; (i < l_len) && (i < r_len); i++) {
        diff_list = DeepDiffAux(allow_regex, allow_extra_fields, ok_map, diff_list,
                                current_path.append(i),
                                l_geo.vectors[i], r_geo.vectors[i], recur);
      }

      // Record mis-matched lengths as null vs the object.
      for ( ; i < l_len; i++) {
        diff_list.Add(new Diff_Rec(current_path.append(i), Diff_kind.df_null,
                                   l_geo.vectors[i], null));
      }

      // if (! allow_extra_fields) {
        for ( ; i < r_len; i++) {
          diff_list.Add(new Diff_Rec(current_path.append(i), Diff_kind.df_null,
                                     null, r_geo.vectors[i]));
        }
      // }

      foreach (object key in l_keys) {
        if (key.Equals("_parent")) {
          r_keys.Remove(key);
          continue;
        }
        
        if (r_keys.Remove(key))
          diff_list = DeepDiffAux(allow_regex, allow_extra_fields, ok_map, diff_list,
                                  current_path.append(key),
                                  l_geo.dict[key], r_geo.dict[key], recur);
        else
          diff_list.Add(new Diff_Rec(current_path.append(key),
                                     Diff_kind.df_null,
                                     l_geo.dict[key], null));
      }
      if (! allow_extra_fields) {
        foreach (object key in r_keys) {
          diff_list.Add(new Diff_Rec(current_path.append(key), Diff_kind.df_null,
                                     null, r_geo.dict[key]));
        }
      }
      return diff_list;
    }

    // Bug 15220 MJO - Added special config type
    public object get_type(object key)
    {
      return get_type(key, false);
    }

    // Bug 15220 MJO - Added special config type
    // Bug 15397 MJO - If a text type, build a new type with min and max size, like the other function used to do
    public object get_type(object key, bool config)
    {
      if (config && has(misc.field_key(key, "type_config"), true))
      {
        GenericObject type = (GenericObject) get(misc.field_key(key, "type_config"), null, true);

        if (type != null && (c_CASL.c_object("Type.text.chars_f_type") as GenericObject).vectors.Contains(type))
        {
          GenericObject newType = c_CASL.c_make("Type.text") as GenericObject;
          int minSize = (int)get(misc.field_key(key, "min_length"), -1);
          int maxSize = (int)get(misc.field_key(key, "max_length"), -1);

          if (minSize != -1)
            newType.set("min_size", minSize);
          if (maxSize != -1)
            newType.set("max_size", maxSize);

          newType.set("chars", type);
          return newType;
        }
        else
        {
          return type;
        }
      }
      else
      {
        if (!has(misc.field_key(key, "type"), true))
          return false;

        GenericObject type = (GenericObject) get(misc.field_key(key, "type"), null, true);

        if (type != null && (c_CASL.c_object("Type.text.chars_f_type") as GenericObject).vectors.Contains(type))
        {
          GenericObject newType = c_CASL.c_make("Type.text") as GenericObject;

          // Bug 15397 MJO - Don't blindly try to cast
          int minSize = -1; 
          object minSizeObj = get(misc.field_key(key, "min_length"), -1);
          if (minSizeObj.GetType() == typeof(int))
            minSize = (int)minSizeObj;
          else if (minSizeObj.GetType() == typeof(string))
            Int32.TryParse((string)minSizeObj, out minSize);
          
          int maxSize = -1;
          object maxSizeObj = get(misc.field_key(key, "max_length"), -1);
          if (maxSizeObj.GetType() == typeof(int))
            maxSize = (int)maxSizeObj;
          else if (maxSizeObj.GetType() == typeof(string))
            Int32.TryParse((string)maxSizeObj, out maxSize);

          if (minSize != -1)
            newType.set("min_size", minSize);
          if (maxSize != -1)
            newType.set("max_size", maxSize);

          newType.set("chars", type);
          return newType;
        }
        else
        {
          return type;
        }
      }
    }

    // definiton of contained check _container and _name
    public bool is_contained()
    {
      if( !(has("_container")) || !(has("_name")) ) // use part_name_key instead of _name
        return false;
      if( (get("_container") as GenericObject).get( get("_name")).Equals(this) )
        return true;
      return false;
    }
    public bool is_contained_in(GenericObject a_container)
    {
      if ( is_contained() && get("_container").Equals(a_container) )
        return true;
      return false;
    }

		#region Get Keys
		public ArrayList GetKeys(string includeString, ArrayList excludeList)
		{
			ArrayList keys = new ArrayList();
			#region Determine which keys to get
			bool hasAll, hasInteger, hasVector, hasObject, hasString, hasSystem, hasMeta;
			hasAll = (includeString.IndexOf("all") >= 0);
			hasInteger = hasAll || (includeString.IndexOf("integer_key") >= 0);
			hasVector = hasAll || (includeString.IndexOf("vector_key") >= 0);
			hasObject = hasAll || (includeString.IndexOf("object_key") >= 0);
			hasString = hasAll || (includeString.IndexOf("string_key") >= 0);
			hasSystem = hasAll || (includeString.IndexOf("system_key") >= 0);
			hasMeta = hasAll || (includeString.IndexOf("meta_key") >= 0);
			#endregion
			#region integer_key
			if (hasInteger)
			{
				ArrayList integer_keys = getIntegerKeys();
				if (excludeList == null || excludeList.Count == 0)
				{
					keys.AddRange(integer_keys);
  }
				else
				{
					foreach (object key in integer_keys)
					{
						if (!excludeList.Contains(key))
							keys.Add(key);
					}
				}
			}
			#endregion
			#region vector_key
			if (!hasInteger && hasVector) // getIntegerKeys already gets vector keys
			{
				ArrayList vector_keys = getVectorKeys();
				if (excludeList == null || excludeList.Count == 0)
				{
					keys.AddRange(vector_keys);
				}
				else
				{
					foreach (object key in vector_keys)
					{
						if (!excludeList.Contains(key))
							keys.Add(key);
					}
				}
			}
			#endregion
			#region object_key
			if (hasObject)
			{
				ArrayList object_keys = getObjectKeys();
				if (excludeList == null || excludeList.Count == 0)
				{
					keys.AddRange(object_keys);
				}
				else
				{
					foreach (object key in object_keys)
					{
						if (!excludeList.Contains(key))
							keys.Add(key);
					}
				}
			}
			#endregion
			#region string_key
			if (hasString)
			{
				ArrayList string_keys = getStringKeys();
				if (excludeList == null || excludeList.Count == 0)
				{
					keys.AddRange(string_keys);
				}
				else
				{
					foreach (object key in string_keys)
					{
						if (!excludeList.Contains(key))
							keys.Add(key);
					}
				}
			}
			#endregion
			#region system_key
			if (hasSystem)
			{
				ArrayList system_keys = getSystemKeys();
				if (excludeList == null || excludeList.Count == 0)
				{
					keys.AddRange(system_keys);
				}
				else
				{
					foreach (object key in system_keys)
					{
						if (!excludeList.Contains(key))
							keys.Add(key);
					}
				}
			}
			#endregion
			#region meta_key
			if (hasMeta)
			{
				ArrayList meta_keys = getMetaKeys();
				if (excludeList == null || excludeList.Count == 0)
				{
					keys.AddRange(meta_keys);
				}
				else
				{
					foreach (object key in meta_keys)
					{
						if (!excludeList.Contains(key))
							keys.Add(key);
    }
  }

			}
			#endregion
			return keys;
		}
		#endregion

  

    public static string name_and_parent_to_string(GenericObject go)
    {
      string ret = go.safe_to_path() + " : ";
      object par_obj = go.get("_parent", null);
      if (par_obj is GenObj_Class par_ty)
        return ret + par_ty.full_name();
      if (par_obj is GenericObject geo)
        return ret + geo.safe_to_path();
      return ret + "*no parent*";
    }

#if dead_code
    private static string name_to_string(object n)
    {
      if (n is string ns)
        return ns;
      if (n is int)
        return ((int) n).ToString();
      return "* Bogus *";
    }
#endif

    public string safe_to_path()
    {
      string ret = "";
      if (this == c_CASL.GEO) {
        ret = "GEO";
      } else if (this == CASLInterpreter.get_my()) {
        ret = "my";
      } else if(has("_name")) {
        ret = Tool.caslize_path_part(get("_name"));
        GenericObject geo_cont = this;
        while (geo_cont.has("_container")) {
          object cont = geo_cont.get("_container", null);
          if (! (cont is GenericObject)) {
            Logger.cs_log("Garbage _container");
            ret = "*Garbage*." + ret;
          }
          geo_cont = cont as GenericObject;
          if (geo_cont == c_CASL.GEO)
            return ret;
          if (geo_cont == CASLInterpreter.get_my())
            return "my." + ret;
          object name = geo_cont.get("_name", null);
          if (name == null)
            return "*_name less container*." + ret;
          ret = Tool.caslize_path_part(name) + "." + ret;
        }
      } else
        ret = "No _name";
      return ret;
    }

    string my_path_to_string()
    {
      int L = getLength();
      string s = "";
      for (int i = 0; i < L; i++) {

        object a = get(i);
        if (a is GenObj_Symbol)
          s += Tool.symbol_to_string(a as GenObj_Symbol);
        else if (a is String)
          s += (a as String);
          else if (a is GenObj_Class)
            s += (a as GenObj_Class).class_name;
          else if (a is GenObj_Method)
            s += (a as GenObj_Method).method_name;
          else if (a is GenObj_Path) {
            s += "  Error: path as part of path" +
              (a as GenObj_Path).my_path_to_string();
          } else if (a is GenObj_Code)
            s += " GenObj_Code @" + (a as GenObj_Code).loc.ToString() + " ";
          else if (a is GenericObject) {
            GenericObject go = a as GenericObject;
            if (go.has("_name")) {
              s += " '" + go.get("_name").ToString() + "' ";
            } else {
              s += "' some generic object '";
            }
          } else
          s += "*" + a.ToString() + "*";
        if (i + 1 < L)
          s += ".";
      }
      return s;
    }


    // This is used indirectly by ToString() and is probably
    // a temporary implementation.  The replacement strategy
    // would use override methods on specific classes derived
    // from GenericObject.
    //
    // The reason this is needed is to get information but
    // stop the recursion of calling show_nested() or ToString()
    // for each field.
    
    public string show_short()
    {
       string s = null;
       string id = "";
#if DEBUG_GenObj
       id = " [<" + gen_obj_number.ToString() + ">] ";
#endif
        switch (kind) {
        case GO_kind.symbol:
         s = "GenObj_Symbol: " + id +
             Tool.symbol_to_string(this as GenObj_Symbol) + " ";
          break;
        case GO_kind.path:
         s = "GenObj_Path:" + id +
             this.my_path_to_string() + " ";
          break;
        case GO_kind.cs_method:
          if (! (this is GenObj_CS_Method))
            Debugger.Break();
          GenObj_CS_Method csm = this as GenObj_CS_Method;
          s = "GeoObj_CS_method: " + id + csm.method_name +
            "  '" + csm.cs_name + "' ";
          break;
        case GO_kind.casl_method:
          if (! (this is GenObj_CASL_Method))
            Debugger.Break();
          GenObj_CASL_Method caslm = this as GenObj_CASL_Method;
          s = "GenObj_CASL_method: " + id + caslm.method_name + " ";
          break;
        case GO_kind.GEO:
          s = "GenObj_Class: *GEO* the Big Kahuna :) ";
          break;
        case GO_kind.casl_class:
          if (! (this is GenObj_Class))
            Debugger.Break();
          s = "GenObj_class: " + id + (this as GenObj_Class).full_name() + " ";
          break;
        case GO_kind.casl_return:
          s = "GenObj_class: *return* " + id;
          break;
        case GO_kind.casl_frame:
          s = "GenObj_Frame: " + id;
          break;
        case GO_kind.casl_stack:
          s = "GenObj_Stack: " + id;
          break;
        case GO_kind.session:
          s = "GenObj session: " + id;
          break;
        case GO_kind.error:
          s = "GenObj_error: " + id;
          break;
        case GO_kind.basic:
          {
           if (Tool.is_casl_class(this))
              s = "ERROR casl_class: " + id +
                name_and_parent_to_string(this) + " ";
           else if (Tool.is_defclass(this))
              s = "ERROR defclass: " + id +
                this.get("_name", "<no name>") + " ";
           else if (Tool.is_cs_call(this))
              s = "ERROR cs_call: " + id +
                name_and_parent_to_string(this) + " ";
           else
             s = "<GenObj>: " + id + name_and_parent_to_string(this) + " ";
          }
          break;
        }
       return s;
    }

#if DEBUG_allocation_type
    public string show_gen_obj_stacktrace()
    {
      string s = Environment.NewLine;

      if (gen_obj_StackTrace == null) {
        s = "  stacktrace not recorded\n";
      } else {
        for (int i = 0; i < num_stack_frames; i++)
            s += "  " + gen_obj_StackTrace[i] + Environment.NewLine;
      }
      s += Environment.NewLine;
      return s;
    }
#endif

#if UNUSED
    public readonly struct K_V : IComparable<K_V>
    {

      public string key { get; }
      public string value { get; }
      public K_V(string k, string v)
      {
        key = k;
        value = v;
      }

      public override string ToString()
      {
        return key + " -> " + value;
      }

      public int CompareTo(K_V kv)
      {
        int ret = key.CompareTo(kv.key);
        if (ret == 0)
          ret = value.CompareTo(kv.value);
        return ret;
      }
    }
#endif
    
    //
    // A GenericObject, geo, is considered to be within the tree of another
    // GenericObject, root, if there exists an _container reference chain to root
    // from geo.  Here 'this' is 'root'.
    //
    public bool in_tree(GenericObject geo)
    {
      bool ret = false;
      while (geo.has("_container")) {
        geo = geo.get("_container") as GenericObject;
        if (geo == this)
          return true;
        if (geo == c_CASL.GEO)
          return false;
      }
      return ret;
    }

    public static void nested_show_object(
        TextWriter sb,
        Object o,
        HashSet<GenericObject> seen,
        GenericObject root,
        HashSet<String> exclude_keys,
        int indent,
        int depth)
    {
      if (o == null) {
        sb.Write(new String(' ', indent*3));
        sb.WriteLine("null");
      } else if (o is GenericObject geo) {
        if (geo.kind == GO_kind.basic || root.in_tree(geo))
          geo.show_nested(sb, seen, true, root, exclude_keys, indent+1, depth-1);
        else {
          sb.Write(new String(' ', indent*3));
          sb.WriteLine(geo.show_short());
        }
      } else {
        sb.Write(new String(' ', indent*3));
        sb.WriteLine(o.ToString());
      }
    }

    public void show_nested(
        TextWriter sb,
        HashSet<GenericObject> seen,
        bool do_not_indent_first_line,
        GenericObject root,
        HashSet<String> exclude_keys,
        int indent,
        int depth)
    {
      String indent_str =  new String(' ', indent*3+1);
      if (do_not_indent_first_line)
        sb.Write(" ");
      else
        sb.Write(new String(' ', indent*3));
      if (seen.Contains(this)) {
        sb.Write("Repeat ");
#if DEBUG_GenObj
        sb.WriteLine(get_path_to_me);
#else
        sb.WriteLine(show_short());
#endif
        return;
      }
      seen.Add(this);

      sb.WriteLine(show_short());

      if (depth <= 0)
        return;
      
      string s  = show_short();
      int l = getLength();
      ArrayList keys = get_normalized_keys();
      int [] intkeys = new int[keys.Count];
      int int_idx = 0;
      string [] stringkeys = new string[keys.Count];
      int str_idx = 0;
      string [] systemkeys = new string[keys.Count];
      int sys_idx = 0;
      bool [] boolkeys = new bool[2];
      int bool_idx = 0;
      GenericObject [] genobjkeys = new GenericObject[keys.Count];
      int genobj_idx = 0;
      Object [] objkeys = new Object[keys.Count];
      int obj_idx = 0;
      
      int i;

      for (i = 0; i < keys.Count; i++) {
        Object key = keys[i];
        if (key == null) {
          sb.Write(indent_str);
          sb.WriteLine("null key!! REALLY BAD IDEA");
          continue;
        }
        if (key is int) {
          intkeys[int_idx++] = (int) key;
          keys[i] = null;
          continue;
        }
        if (key is string sk) {
          if (exclude_keys.Contains(sk)) {
            keys[i] = null;
            continue;
          }
          if (sk == "_parent" || sk == "_container" || sk == "__code_info" ||
              sk == "_code_info") {
            systemkeys[sys_idx++] = sk;
            keys[i] = null;
            continue;
          }
          stringkeys[str_idx++] = sk;
          keys[i] = null;
          continue;
        }
        if (key is bool) {
          boolkeys[bool_idx++] = (bool) key;
          keys[i] = null;
          continue;
        }
        if (key is GenericObject geo_key) {
          genobjkeys[genobj_idx++] = geo_key;
          keys[i] = null;
          continue;
        }
        if (key is Object obj) {
          objkeys[obj_idx++] = obj;
          keys[i] = null;
          continue;
        }
      }

      Array.Sort(systemkeys, 0, sys_idx);
      for (i = 0; i < sys_idx; i++) {
        string key = systemkeys[i];
        object o = get(key, null);

        sb.Write(indent_str);
        sb.Write("\"" + key + "\" -> ");

        if (o == null)
          sb.WriteLine("null");
        else if (o is GenObj_Class go_c) {
          sb.WriteLine(go_c.full_name());
        } else if (o is GenericObject geo) {
          sb.WriteLine(geo.get_path_to_me);
        } else
          sb.WriteLine(o.ToString());
      }


      Array.Sort(intkeys, 0, int_idx);
      for (i = 0; i < int_idx; i++) {
        int j = intkeys[i];
        object o = null;
        sb.Write(indent_str);
        if (j < l) {
          sb.Write("[" + j.ToString() + "]=");
          o = vectors[j];
        } else {
          sb.Write(j.ToString() + " => ");
          o = dict[j];
        }
        nested_show_object(sb, o, seen, root, exclude_keys, indent, depth);
      }

      for (i = 0; i < bool_idx; i++) {
        bool b = boolkeys[i];
	object o = get(b, null);
        sb.Write(indent_str);
	if (b)
	  sb.Write(".true. => ");
	else
	  sb.Write(".false. => ");

        nested_show_object(sb, o, seen, root, exclude_keys, indent, depth);
      }

      Array.Sort(stringkeys, 0, str_idx);
      for (i = 0; i < str_idx; i++) {
        string key = stringkeys[i];
        object o = get(key, null);

        sb.Write(indent_str);
        sb.Write("\"" + key + "\" -> ");

        nested_show_object(sb, o, seen, root, exclude_keys, indent, depth);
      }

      SortedList<string, string> display_lines =
        new SortedList<string, string>();
      for (i = 0; i < genobj_idx; i++) {
        string geo_key = genobjkeys[i].get_path_to_me;
        string geo_val = CASL_Frame.show_value(dict[genobjkeys[i]]);
        display_lines.Add(geo_key, geo_val);
      }
      foreach (var kx1 in display_lines.Keys) {
        sb.Write(indent_str);
        sb.WriteLine("geo: " + kx1 + " -> " + display_lines[kx1]);
      }

      display_lines.Clear();
      for (i = 0; i < obj_idx; i++) {
        string obj_key = CASL_Frame.show_value(objkeys[i]);
        string obj_val = CASL_Frame.show_value(dict[objkeys[i]]);
        display_lines.Add(obj_key, obj_val);
      }
      foreach (var kx2 in display_lines.Keys) {
        sb.Write(indent_str);
        sb.WriteLine("obj: " + kx2 + " -> " + display_lines[kx2]);
      }

      sb.WriteLine();

#if DEBUG_allocation_type
      if (stacktrace_enabled) {
        sb.WriteLine(this.show_gen_obj_stacktrace());
        CASL_Stack.stacktrace_StrBld(sb, 0, casl_frame);
      }
#endif
      return;
    }

    public static object fCASL_debug_show_nested(CASL_Frame frame)
    {
      string file_name = frame.args.get("file_name", null) as string;
      if (file_name == null) 
        return "file_name is null or not a string";

      int depth = 4;
      if (frame.args.has("depth")) {
        object obj = frame.args.get("depth");
        if (obj is int d)
          depth = d;
      }

      if (! frame.args.has("_subject", false))
        return "_subject is not set";
      
      Object sub_obj = frame.args.get("_subject", null);
      if (sub_obj == null)
        return "_subject is null";

      GenericObject subject = sub_obj as GenericObject;

      if (subject == null) {
        using (TextWriter sb = DumpFile.create_writer(file_name))
          sb.WriteLine(sub_obj.ToString());
        return "_subject is not a GenericObject";
      }

      HashSet<string> exclude_keys = new HashSet<string>();
      GenericObject e_k = frame.args.get("exclude_keys", null, false) as GenericObject;
      if (e_k != null) {
        for (int i = 0; i < e_k.getLength(); i++) {
          string key = e_k.get(i) as string;
          if (key != null)
            exclude_keys.Add(key);
        }
      }

      HashSet<GenericObject> hs = new HashSet<GenericObject>();
      hs.Add(c_CASL.GEO);

      using (TextWriter sb = DumpFile.create_writer(file_name)) {
        subject.show_nested(sb, hs, false, subject, exclude_keys, 0, depth);
      }

      return "file written";
    }
  
    public override string ToString()
    {
      HashSet<GenericObject> hs = new HashSet<GenericObject>();
      hs.Add(c_CASL.GEO);
      HashSet<string> excl_keys = new HashSet<String>();

      string ret = null;
      using (var sb = new StringWriter()) {
        show_nested(sb, hs, false, this, excl_keys, 0, 2);
        ret = sb.ToString();
      }

      return ret;
    }

    // Bug 18933 MJO - Added for backwards compatibility
    public bool ContainsValue(object obj)
    {
      return this.getValues().Contains(obj);
    }

    // Comment out for now, but may revise when extending Visual Studio.  GMB.
    //    public string DebuggerDisplayGO {
    //      get { return show_short(); }
    //    }

    // Bug 24518 MJO - Simpler wrapper for CASL_call
    public object Call(string method_name)
    {
      return Call(method_name, new GenericObject());
    }

    // Bug 24518 MJO - Simpler wrapper for CASL_call
    public object Call(string method_name, GenericObject args)
    {
      return misc.CASL_call("subject", this, "a_method", method_name, "args", args);
    }

    // TTS#26349 GMB
    //
    // Recursively lock all GenericObjects which derive from GenObj_Code --- they
    // should be immutable after the interpreter is loaded, but we need to allow
    // for Config still.
    //
    public static void Lock_GenObj_Code(HashSet<GenericObject> seen_GenObjs, GenericObject geo)
    {
#if DEBUG_GenObj_Lock
      if (seen_GenObjs.Contains(geo))
        return;
      seen_GenObjs.Add(geo);
      if (geo is GenObj_Code)
        geo.locked_as = Lock_Kind.code;

      foreach (Object o in geo.dict.Values)
        if (o is GenericObject gx)
          Lock_GenObj_Code(seen_GenObjs, gx);
      for (int i = 0; i < geo.vectors.Count; i++)
        if (geo.vectors[i] is GenericObject gy)
          Lock_GenObj_Code(seen_GenObjs, gy);
      return;
#endif
    }

#if DEBUG_GenObj_Lock
    public static string Lock_ShowKey(object obj)
    {
      if (obj is GenericObject geo) {
	if (geo.has("name"))
	  return geo.get("name").ToString();
	if (geo.has("_name"))
	  return geo.get("_name").ToString();
	return "*GenObj*";
      }
      if (obj is string s)
	return s;
      if (obj is int)
	return ((int) obj).ToString();
      return "*Wierd Object";
    }
#endif
  }


  //
  // Base of the derived hierarchy used by the CASL Interpreter.
  // NOTE:
  //      Copy constructors don't actually copy vectors and dictionary
  //      information, see misc.Copy().
  //
  // I am in the process of converting the CASL code that might tickle
  // these issues to C#.
  //

  public struct code_location {
    public static code_location no_location =
      new code_location(null, 0, 0, 0);
    public static code_location builtin_location =
      new code_location(null, 1, 0, 0);
    public string source_or_filename;
    public int file_index;
    public int line_num;
    public int line_pos;
    public code_location(string s, int f, int l, int c)
    {
      source_or_filename = s;
      file_index = f;
      line_num = l;
      line_pos = c;
    }

    public static bool eq(code_location a, code_location b)
    {
      if (a.file_index != b.file_index)
        return false;
      if (a.line_num != b.line_num)
        return false;
      if (a.line_pos != b.line_pos)
        return false;
      if (a.source_or_filename != b.source_or_filename)
        return false;
      return true;
    }

    public static char[] newline_separator = new char[]{'\n'};

    public static string pos_string(string source, int line, int col)
    {
      string s = "";
      if (line > 0)
        line--;
      if (col > 0)
        col--;
      string [] rows = source.Split(newline_separator,
                                    StringSplitOptions.None);
      if (0 <= line && line < rows.Length) {
        if (line > 0)
          s += rows[line - 1];
        string ls = rows[line];
        int len = ls.Length;
        while (col > len) {
          s += ls;
          col -= len;
          line++;
          if (line >= rows.Length)
            break;
          ls = rows[line];
          len = ls.Length;
        }
        s += rows[line].Substring(0, col);
        s += " ^ ";
        s += rows[line].Substring(col);
        s += " ";
      }
      return s;
    }


    public override string ToString()
    {
      if (! c_CASL.get_show_verbose_error())
        return "";

      if (file_index > 2)
        return CASLInterpreter.FileIndex[file_index].file_name + "#" +
            line_num + "," + line_pos;

      if (source_or_filename == null) {
        if (file_index == 0)
          return "no code_location";
        if (file_index == 1)
          return "builtin";
#if DEBUG
        Debugger.Break();
#endif
      }
      return pos_string(source_or_filename, line_num, line_pos);
    }

    public bool isValid()
    {
      if (source_or_filename != null || file_index > 0)
        return true;
      return false;
    }
  };

  public class GenObj_Code : GenericObject {
    public code_location loc = code_location.no_location;
#if DEBUG
    public bool breakpoint = false;
#endif

    public GenObj_Code(GenObj_Code goc)
    {
      kind = goc.kind;
      loc = goc.loc;
#if DEBUG
      breakpoint = goc.breakpoint;
#endif
    }

    public GenObj_Code(GO_kind k,
                       code_location cl,
                       params Object[] args)
      : base(args)
      {
        kind = k;
        loc = cl;
      }
    
   
    public override string ToString()
    {
      HashSet<GenericObject> hs = new HashSet<GenericObject>();
      hs.Add(c_CASL.GEO);
      HashSet<string> excl_keys = new HashSet<String>();

      string ret = null;
      using (var sb = new StringWriter()) {
        show_nested(sb, hs, false, this, excl_keys, 0, 2);
        ret = sb.ToString();
      }

      return ret;
     }
  }


  public class GenObj_Path : GenObj_Code {
    public GenObj_Path(GenObj_Path gop)
      :  base(gop)
      {
        set("_parent",c_CASL.c_expr_path);
      }

    public GenObj_Path()
      : base(GO_kind.path, code_location.no_location)
      {
        set("_parent",c_CASL.c_expr_path);
      }

    public GenObj_Path append(object obj)
    {
      GenObj_Path p = new GenObj_Path(this);
      foreach (object o in vectors)
        p.vectors.Add(o);
      p.vectors.Add(obj);
      return p;
    }
   
    public override string ToString()
    {
      return base.ToString();
    }
  }

  public class GenObj_Symbol : GenObj_Code {
    public string name;
    public GenObj_Symbol(GenObj_Symbol gos)
      :  base(gos)
      {
        set("_parent", c_CASL.c_expr_symbol);
      }

    public GenObj_Symbol(string s)
      : base(GO_kind.symbol, code_location.no_location)
      {
        name = s;
        set("_parent", c_CASL.c_expr_symbol,
            "name", s);
      }

    public override string ToString()
    {
      return base.ToString();
    }
  }

  public class GenObj_Class : GenObj_Code {
    public code_location decl_loc = code_location.no_location;
    public string class_name = null;
    public GenObj_Class container = null;
    public bool promote_to_GEO = false;

#if DEBUG_GenObj
    // If go._parent == this then we consider logging to fligth_recorder
    public GO_Actions tracking = GO_Actions.none;
#endif

    public string full_name()
    {
      if (this == c_CASL.GEO)
        return "GEO";
      if (container == c_CASL.GEO)
        return class_name;
      string ret = class_name;
      GenObj_Class cont = this;
      while (cont.container != null) {
        cont = cont.container;
        if (cont == c_CASL.GEO)
          return ret;
        ret = cont.class_name + "." + ret;
      }
      return ret;
    }
    
    public GenObj_Class(GenObj_Class goc)
      : base(goc)
      {
        decl_loc = goc.decl_loc;
        class_name = goc.class_name;
        container = goc.container;
        promote_to_GEO = goc.promote_to_GEO;
      }

    public GenObj_Class()
      : base(GO_kind.casl_class, code_location.no_location)
      { }

    public GenObj_Class(string name, GenObj_Class par_cont)
      : base(GO_kind.casl_class, code_location.no_location)
      {
        class_name = name;
        container = par_cont;
        set("_name", name, "_parent", container, "_container", container);
      }


    //
    // Create c_CASL.GEO --- only called once fro handle_request_2.cs
    // before any actual request processing is done.
    //

    public static GenObj_Class create_GEO()
    {
      GenObj_Class geo = new GenObj_Class();
      geo.class_name = "GEO";
      geo.container = geo;   // Special case
      geo.loc = code_location.builtin_location;
      geo.dict["_name"] = geo.class_name;
      geo.dict["GEO"] = geo;

      return geo;
    }

    //
    // Create error as class, this must be done by hand and done immediately
    // after GEO has been created because it is used in GenericObject.get() as
    // the flag for "no such key" in GenericObjects.
    //

    public static GenObj_Class create_c_error()
    {
      GenObj_Class c_error = new GenObj_Class();
      c_error.class_name = "error";
      c_error.container = c_CASL.GEO;
      c_error.dict["_name"] = "error";
      c_error.dict["_parent"] = c_CASL.GEO;
      c_error.dict["_container"] = c_CASL.GEO;
      c_error.dict["message"] = "No message";
      c_error.dict["title"] = "Runtime error";
      c_error.dict["casl_stack"] = c_CASL.c_opt;
      c_error.dict["cs_stack"] = "";
      c_error.dict["code"] = 200;
      c_error.dict["throw"] = true;
      c_error.dict["_other_keyed"] = c_CASL.c_opt;
      c_error.dict["_field_order"] =
        new GenObj_Code(GO_kind.casl_code, code_location.builtin_location,
                        "_parent", c_CASL.GEO,
                        0,"message",
                        1,"title",
                        2,"code",
                        3,"throw",
                        4,"casl_stack",
                        5,"cs_stack");
      c_error.promote_to_GEO = false;
      c_error.loc = code_location.builtin_location;
      c_error.decl_loc = code_location.builtin_location;
      c_CASL.GEO.dict["error"] = c_error;
      return c_error;
    }

    public override string ToString()
    {
      return base.ToString();
    }
  }

  // NOTE: call_count .. max_ticks are going to be moved to a
  // method_index x method_index "array" so that we capture a
  // call graph.  GMB.

  public class GenObj_Method : GenObj_Code {
    public GenObj_Class container = null;
    public int method_idx = 0;
    public string method_name=null;
    public string full_name = null;
#if DEBUG_GenObj
    // If go._parent == this then we consider logging to fligth_recorder
    public bool tracking = false;
#endif

    //
    // Coming patch eliminates most (all?) of the method copies
    // but I'm not sure I have found all of them.  GMB.
    //
    public GenObj_Method(GenObj_Method gom)
      : base(gom)
    {
      method_idx = gom.method_idx;
      method_name = gom.method_name;
    }

    public GenObj_Method(GO_kind k)
      : base(k, code_location.no_location)
    { }

    public override string ToString()
    {
      HashSet<GenericObject> hs = new HashSet<GenericObject>();
      hs.Add(c_CASL.GEO);

      HashSet<string> excl_keys = new HashSet<String>();

      string ret = null;
      using (var sb = new StringWriter()) {
        show_nested(sb, hs, false, this, excl_keys, 0, 2);
        ret = sb.ToString();
      }

      return ret;
    }

  }

  public delegate object   ObjectParamsDelegate(params object[] args);
  public delegate object   ObjectFrameDelegate(CASL_Frame env);

  public class GenObj_CS_Method : GenObj_Method {
    public MethodInfo method_info = null;

    public ObjectParamsDelegate method_delegate = null;
    public ObjectFrameDelegate method_delegate_frame = null;
    public string cs_name = "";

    public GenObj_CS_Method(GenObj_CS_Method cs_meth)
      : base(cs_meth)
      {
        method_info = cs_meth.method_info;
        method_delegate = cs_meth.method_delegate;
        method_delegate_frame = cs_meth.method_delegate_frame;
        cs_name = cs_meth.cs_name;
      }

    public GenObj_CS_Method()
      : base(GO_kind.cs_method)
      {
      }

    public override string ToString()
    {
      HashSet<GenericObject> hs = new HashSet<GenericObject>();
      hs.Add(c_CASL.GEO);
      
      HashSet<string> excl_keys = new HashSet<string>();
      
      string ret = null;

      using (var sb = new StringWriter()) {
        show_nested(sb, hs, false, this, excl_keys, 0, 2);
        ret = sb.ToString();
      }

      return ret;
    }
  }

  public class GenObj_CASL_Method : GenObj_Method {

    public GenObj_CASL_Method(GenObj_CASL_Method casl_meth)
      : base(casl_meth)
      {
      }

    public GenObj_CASL_Method()
      : base(GO_kind.casl_method)
      {
      }

    public override string ToString()
    {
      HashSet<GenericObject> hs = new HashSet<GenericObject>();
      hs.Add(c_CASL.GEO);

      HashSet<string> excl_keys = new HashSet<string>();
      
      string ret = null;
      using (var sb = new StringWriter()) {
        show_nested(sb, hs, false, this, excl_keys, 0, 2);
        ret = sb.ToString();
      }

      return ret;
    }
  };

  public class GenObj_Error : GenericObject {
    public GenObj_Error()
      : base("_parent", c_CASL.c_error, "throw", false)
    {
      kind = GO_kind.error;
      
    }
    public GenObj_Error(string message)
      : base("_parent", c_CASL.c_error, "throw", false)
    {
      kind = GO_kind.error;
      set("message", message);
    }

    public static GenObj_Error fCASL_init(CASL_Frame frame)
    {
      GenericObject args = frame.args;
      string message = null;
      if (args.has("message")) {
        object o_msg = args.get("message");
        if ( !(o_msg is string)) {
          Logger.cs_log("GenObj_Error.init messages should be a string",
                        Logger.cs_flag.warn);
        } else {
          message = o_msg as string;
          if (message == "No message")
            Logger.cs_log("GenObj_Error.init *DUDE* the default message is"
                          + " not useful!!");
        }
        args.remove("message");
      } else {
        Logger.cs_log("GenObj_Error.init should always have a 'message'",
                      Logger.cs_flag.warn);
        message = "No message";
      }
      GenObj_Error casl_object = new GenObj_Error(message);
      casl_object.set(args);
      if ( (bool) args.get("throw", false))
        throw CASL_error.create_obj(message, casl_object);
      return casl_object;
    }
  };
  
  public class GenObj_Stack : GenericObject {
#if needed
    public GenObj_Stack(GenObj_Stack gos)
      : base(gos)
    { kind = GO_kind.casl_stack; }
#endif

    public GenObj_Stack()
    { kind = GO_kind.casl_stack; }
  }

  public class GenObj_Frame : GenericObject {
#if needed
    public GenObj_Frame(GenObj_Frame gof)
      : base(gof)
    { kind = GO_kind.casl_frame; }
#endif

    public GenObj_Frame()
    { kind = GO_kind.casl_frame; }
  }
}

