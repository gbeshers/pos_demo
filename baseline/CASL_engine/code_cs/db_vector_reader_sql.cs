/* 
 *  db_vector_reader_sql
 *  NOTE: This file is derived from db_request.cs. This file doesn't contain any user
 *  interface/view related fields. 
 *     
 *  DESCRIPTION:
 *  Database API.
 *  Given a specific SQL statement and a live database connection db_request will retrieve
 *  relational data from database and persist this data for the duration of this class
 *  object.
 *   1. Use state machine to mimic database retrieving behavior
 *      involved elements: one live Sql reader, one main thread, one worker thread
 *      0 - stopped : there is no worker thread to retrieve database records at the moment. 
 *                    it can be situation Sql reader hasn't start to read any record 
 *                    from database, or the working thread reaches more_data_size or 
 *                    more_data_timeout and stop retrieving. 
 *      1 - working : worker thread is retrieving database records. 
 *      2 - paused  : worker thread is paused to do the retrieving in order to copy data. 
 *                    there is the conflict between copying hashtable in main thread
 *                    and continue to insert data from working thread. 
 *      3 - done    : reader reach the end of retrieving and no more records to retrieve
 *      
 *   2. Threading information:
 *      There is a background worker thread that is created by calling 
 *      db_vector_reader_sql.retrieve() function. This thread is not terminated until the
 *      desired "more_data_size" is retrieved.
 * 
 *      calling CASL_insert_data() will conditionally call db_vector_reader_sql.retrieve()
 *      which creates another worker thread if one dosen't exist and attempt to retrieve 
 *      more records.
 * 
 *   3. SqlDataReader information:
 *      A new SqlDataReader object is created when the initial call to
 *      db_request.make_db_request() is executed. This SqlDataReader object will stay open
 *      until all rows are retrieved from database. While threads are created and destroyed
 *      with each group of records, reader object remains open and holding the possition
 *      within dataset so that the next thread can pick up where the last thread left off.
 *  
 * METHODS OF INTEREST:
 * 1. db_vector_reader_sql(params)/CONSTRUCTOR
 *   Do not call the constructor. 
 *   Instead call tatic db_vector_reader_sql.make(params)
 * 
 * 2. db_vector_reader_sql.make(params)
 *    Serves as a constructor and should only be called once.
 *    method returns an instance of db_vector_reader_sql as well as 
 *    passing passing parameters to the constructor. 
 *    Requirements: none. 
 *    Effects: 
 *        the method does not retrieve the first group of records. 
 *        call CASL_insert_data() with inserting's start index and size 
 *        to retrieve data
 *    Parameters: 
 *      "db_connection"       (required - connection string or object for SqlConnection)
 *      "sql_statement"       (required - const SQL statement) 
 *      "class"               (optional or may be null - CASL)
 *                              type or parent of individual retrieved record.
 *      "more_data_size"      (optional or 0(infinite/get all data) or 1 to 2,147,483,647
 *                              - nbr of rows to retrieve at a time.
 *                                Subsequent calls to CASL_insert_data() will retrieve
 *                                next group when needed. Worker thread will continue to
 *                                run in the background until group is retrieved. This has
 *                                nothing to do with display on screen. Only buffering of
 *                                data to be ready for display.)
 *      "more_data_timeout"  (optional) - milliseconds please
 *                                db_vector_reader_sql.retrieve(params) will wait until
 *                                group of records set by "NextGroupCount" is retrieved or
 *                                group_timeout expires. Although
 *                                db_request.make_db_request(params) returns, worker thread
 *                                will still attempt to finish retrieving the last group.)
 *      "query_timeout"      (optional) Command object timeout. TODO: need to add it. 
 * 
 * 3. db_vector_reader_sql.retrieve()
 *     create a worker thread and create an Sql reader if no Sql reader 
 *     avaible, otherwise pick up the previous Sql reader, then continue to 
 *     retrieve the next set of data from database. 
 *     
 * 4. CASL_insert_data(params). 
 *      Insert data into vector as subject, if insert size is greater than reader current
 *      data size, call retrieve() to get next group of data from database.
 *    Parameters: 
 *      "_subject"           (required)  - could be any GenericObject or CASL db_vector
 *      "reader"             (required)  - db_vector_reader_sql instance which is made by
 *                                         calling with live database connection. 
 *      "insert_size"        (required)  - the maximum size to insert
 *      "start"              (required)  - start index to insert
 * 
 * 5. CASL_may_have_more_data(params)
 *        Check if the db_vector_reader_sql has more static data than _subject
 *        or db_vector_reader_sql is not done retrieving. 
 *     Parameters: 
 *      "_subject"           (required)  - could be any GenericObject or CASL db_vector
 *      "reader"             (required)  - db_vector_reader_sql instance which is made by
 *                                         calling with live database connection. 
 * 6. db_exec_sql(). This is a static function for executing SQL
 *    statements directly on the database. Accepts a life database connection and 
 *    a SQL statement. If error executing SQL; CASL_error is thrown with the 
 *    original Sql message. 
 *    
 * 7. Close(). Optionally can be called at any point stopping the search and cleaning up 
 *    allocated memory. You can also let the garbage collector collect instead.
 *  
 * */
/*
  ERROR CODES FOR db_vector_reader_sql ARE IN THE FORMAT DB_SQL-1.... PLEASE ALWAYS USE A SPECIFIC 
  ERROR CODE FOR EACH ERROR.
  ALSO PLEASE UPDATE THIS SECTION WITH THE LARGEST ERROR YOU HAVE USED TO DATE. WHEN THE NEXT PERSON 
  CREATES ONE, THEY WILL BE ABLE TO ASSIGN ERRORS IN SEQUENCE.

  "DB_SQL-1" - CASL_error("message", "invalid connection", "DB_SQL-1");    
  "DB_SQL-2" - throw CASL_error.create("message", "Database connection failed.",
       "code", "DB_SQL-2");
  "DB_SQL-3" - throw CASL_error.create("message", GetErrMsg().ToString(),
       "code", "DB_SQL-3");
  "DB_SQL-4" - throw CASL_error.create("message", e.Message.ToString(),
       "code", "DB_SQL-4");
  "DB_SQL-5" - throw CASL_error.create("message", e.Message.ToString() + "<br>Connection string:<br>" + strTemp,
       "code", "DB_SQL-5");
  "DB_SQL-6" - "was encountered while attempting to roll back the transaction.",
       "code", "DB_SQL-6");
  "DB_SQL-7" - "was encountered while inserting the data. Neither record was written to database.",
       "code", "DB_SQL-7");
  "DB_SQL-8" - "GetRecords failure", "code", "DB_SQL-8");
  "DB_SQL-9" - "Error occured while opening database connection" (Bug 16433 [21921] DJD)

  DB_SQL-10 - *NEXT AVAILABLE*
*/

using System;
using System.Data;
using System.Threading;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Collections;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;

namespace CASL_engine
{
  /// <summary>
  /// db_vector_reader_sql is derived from db_request.
  /// it is used by CASL db_vector as build-in reader. 
  /// TODO: merge db_vector_reader_sql and db_request. 
  /// </summary>
  public class db_vector_reader_sql : IDBVectorReader
  {
    #region Variables
    // reader variables 
    string m_strConnectionClass = "";
    string m_strConnectionDataSource = "";
    private GenericObject m_Data = null;// Holds fetched records.
    private int m_more_data_size = 0; // m_lMaxGroupCount
    int m_more_data_timeout = 0;// milliseconds please!
    object m_class = null; // m_ClassObj

    // flags (exchange variables shared between worker thread and main thread ) 
    // and  status of worker thread 
    private object m_retrieval_lock = new object();
    // Bug #8025 Mike O - Rely on this instead of m_status when the main thread is waiting on the worker
    Semaphore m_waiting_for_lock = new Semaphore(0, 1);
    // TODO: Change these to boolean and enums
    private int m_status = 0; // 0 = not started, 1 = running, 2 = finished
    //private int m_pause_flag = 0;// 0=false, 1=true m_iPauseThread  /*12081 NJ-unused variable*/
    private int m_cancel_flag = 0; //m_cancel_retrieving

    // thread variables
    private ManualResetEvent m_SearchThreadTerminated_Event = null;// Set at end of thread worker loop
    private System.Threading.Thread m_SearchThread = null;// main search worker thread.
    private int m_iErrorState = 0;
    private object m_strErrorMSG = null;

    // Database specific variables 
    private SqlConnection m_SqlConn = null;
    private ArrayList sqlCommands = null;
    bool is_to_close_SqlConn = true; // if pass Sql connection, do not close it. 
    private SqlCommand m_Command = null;
    private SqlTransaction m_Transaction = null;
    private SqlDataReader m_dtReader = null;
    #endregion
    /// <summary>
    /// TODO: should make sure reader is closed and set to null
    /// db connection is closed.
    /// thread is dead
    /// </summary>
    ~db_vector_reader_sql()
    {
      if (m_SqlConn != null)
      {
        try
        {
          // Bug 16433 [21921] DJD - add close check and log error
          if ((is_to_close_SqlConn) && (m_SqlConn.State != ConnectionState.Closed))
          {
            m_SqlConn.Close(); // Try to close if possible.
          }
        }
        catch// Bug# 27153 DH - Clean up warnings when building ipayment
        {
          // HX: Bug 17849 maybe not needed
          // Logger.cs_log(e.ToMessageAndCompleteStacktrace());
        }
      }
    }

    private db_vector_reader_sql(GenericObject db_connection, int query_timeout, ref string error_message)
    {
      #region Set member fields
      m_strConnectionClass = (string)db_connection.get("_name", "", true);
      m_strConnectionDataSource = (string)db_connection.get("datasource_name", "");
      sqlCommands = new ArrayList();
      m_Data = new GenericObject();
      m_cancel_flag = 0;
      #endregion
      #region Establish new connection if necessary
      if (m_SqlConn == null)
      {
        string strConnectionString = (string)db_connection.get("db_connection_string", "");
        m_SqlConn = get_db_connection(strConnectionString, ref error_message);
        is_to_close_SqlConn = true;
      }
      #endregion
    }

    private db_vector_reader_sql(GenericObject db_connection, string sql_text, CommandType command_type, Hashtable sql_args, int query_timeout, ref string error_message)
    {
      #region Set member fields
      m_strConnectionClass = (string)db_connection.get("_name", "", true);
      m_strConnectionDataSource = (string)db_connection.get("datasource_name", "");
      m_Command = new SqlCommand();
      m_Data = new GenericObject();
      m_cancel_flag = 0;
      #endregion
      #region Establish new connection if necessary
      if (m_SqlConn == null)
      {
        string strConnectionString = (string)db_connection.get("db_connection_string", "");
        m_SqlConn = get_db_connection(strConnectionString, ref error_message);
        is_to_close_SqlConn = true;
      }
      #endregion
      #region Set command arguments
      if (sql_args != null)
      {
        // TODO: loop through string keys of sql_args, and for each, add as SQL parameter based on the type.
        object arg_value = null;
        SqlParameter parameter = null;
        foreach (string key in sql_args.Keys)
        {
          arg_value = sql_args[key];
          parameter = new SqlParameter();
          parameter.ParameterName = "@" + key;
          parameter.SqlDbType = DatabaseParamUtils.getSqlDbType(arg_value, parameter.ParameterName);  // Bug 25175
          parameter.Direction = ParameterDirection.Input;
          if (arg_value == null)
          {
            parameter.Value = DBNull.Value;
          }
          else
          {
            parameter.Value = arg_value;
          }
          m_Command.Parameters.Add(parameter);
        }
      }
      #endregion
      #region Set Command parameters
      m_Command.Connection = m_SqlConn;
      m_Command.CommandType = command_type;
      m_Command.CommandText = sql_text;
      m_Command.CommandTimeout = GetWebConfigQueryTimeout(query_timeout/*BUG# 8135 DH*/);
      #endregion
    }

    // BUG# 8135 Added Web.config setting for controlling query timeouts.
    public int GetWebConfigQueryTimeout(int iQueryTimeout)
    {
      // Lets only overide the INFINITE timeouts. Individual functions should still be able to set this as needed.
      // Bug #12055 Mike O - Use misc.Parse to log errors
      if (iQueryTimeout == 0)
        return misc.Parse<Int32>(c_CASL.GEO.get("sql_query_timeout").ToString());

      return iQueryTimeout;
    }
    // end BUG# 8135

    /// <summary>
    /// db_vector_reader_sql // Bug# 27153
    /// </summary>
    /// <param name="args">
    /// sql_statement="" 
    /// query_timeout=0 
    /// db_connection=""</param>
    public db_vector_reader_sql(params object[] args)
    {
      #region Set member fields and arguments
      GenericObject Args = misc.convert_args(args);
      string sql_statement = (string)Args.get("sql_statement", "");
      GenericObject sql_args = (GenericObject)Args.get("sql_args", c_CASL.c_opt);
      int query_timeout = (int)Args.get("query_timeout", 0);
      Object db_connection = Args.get("db_connection", "");

      GenericObject DBConnInfo = (GenericObject)db_connection;
      m_strConnectionClass = (string)DBConnInfo.get("_name", "", true); //(string)DBConnInfo.get("_name",c_CASL.c_error ,true);
      m_strConnectionDataSource = (string)DBConnInfo.get("datasource_name", ""); //(string)DBConnInfo.get("datasource_name",c_CASL.c_error, true);

      m_Command = new SqlCommand();
      m_Data = new GenericObject();
      m_cancel_flag = 0;// Let it run.
      #endregion      
      #region Establish new connection if one does not exist yet.
      if (m_SqlConn == null)
      {
        try
        {
          if (db_connection is GenericObject)
          {
            string strConnectionString = "";
            GenericObject DBInfo = (GenericObject)db_connection;
            strConnectionString = (string)DBInfo.get("db_connection_string", "");
            string error_message = "";
            m_SqlConn = get_db_connection(strConnectionString, ref error_message);
            if (m_SqlConn == null || error_message != "")
            {
              throw CASL_error.create("message", error_message, "code", "DB_SQL-5");
            }

            is_to_close_SqlConn = true;
          }
          else if (db_connection is SqlConnection)
          {
            m_SqlConn = (SqlConnection)db_connection;
            is_to_close_SqlConn = false;
          }
          else
          {
            throw CASL_error.create("message", "invalid connection", "code", "DB_SQL-1");
          }

        }
        catch (Exception e)   //
        {
          //BUG 16047,13832 - add innerexception for better debugging
          string msg = e.InnerException != null ? e.InnerException.Message : e.Message;
          if (e is CASL_error)
            msg = (e as CASL_error).casl_object.get("message", msg) as string;
          // Bug 25125
          misc.CASL_call("a_method", "actlog", "args",
            new GenericObject(
                "app", ""   // Let writeLogRecord() find this
                            // Bug 20435 UMN PCI-DSS don't pass session
                , "user", "" // Let writeLogRecord() find this
                , "action", "Error"
                , "summary", "Database Error"
                , "detail", "db_vector_reader_sql ctor() Database connection failed. Error:" + misc.cleanUpDetailsForActLog(msg)
                )
          );

          throw CASL_error.create("title", "Database Connection Failed",
                   "message", "Database connection failed." + msg,
                   "code", "DB_SQL-2");
        }
      }
      #endregion
      #region Set Command arguments
      if (!sql_args.Equals(c_CASL.c_opt))
      {
        if (sql_args.Contains("_command_type"))
          m_Command.CommandType = (System.Data.CommandType)sql_args["_command_type"];
        // TODO: loop through string keys of sql_args, and for each, add as SQL parameter based on the type.
        ArrayList string_keys = sql_args.getStringKeys();
        object V = null;
        foreach (string field_key in string_keys)
        {
          //pDBConn_Data.m_pOraCmd.Parameters.Add(new OracleParameter(":ImageBytes", System.Data.OracleClient.OracleType.LongRaw, byteDOC_IMAGE.Length, "DOC_IMAGE"));
          //pDBConn_Data.m_pOraCmd.Parameters[":ImageBytes"].Value = byteDOC_IMAGE;
          V = sql_args.get(field_key);
          SqlParameter param = new SqlParameter();
          param.ParameterName = "@" + field_key;
          param.SqlDbType = DatabaseParamUtils.getSqlDbType(V, param.ParameterName);    // Bug 25175
          param.Direction = ParameterDirection.Input;
          if (V == null)
          {
            param.Value = DBNull.Value;
          }
          else
          {
            param.Value = V;
          }
          m_Command.Parameters.Add(param);
        }
      }
      #endregion
      #region Set Command parameters
      m_Command.Connection = m_SqlConn;
      m_Command.CommandText = sql_statement;// SQL Statement
      m_Command.CommandTimeout = GetWebConfigQueryTimeout(query_timeout/*BUG# 8135 DH*/); // Second to allowe for creating command object.
      #endregion
    }

    public void Cancel()// Called by user of db_vector_reader_sql to pause search.
    {
      if (m_SearchThread != null)
      {
        if (m_SearchThread.IsAlive)
        {
          System.Threading.Interlocked.Exchange(ref m_cancel_flag, 1);
        }
      }
    }

    public void CancelOnServer()// Called by user of db_vector_reader_sql to cancel SQL execution at Server.
    {
      //*****************************************
      // Database specific.
      if (m_Command != null)
        m_Command.Cancel();// Cancel at Server.
      //*****************************************
    }

    /// <summary>
    /// create the instance of db_vector_reader_sql
    /// </summary>
    /// <param name="args"> 
    /// sql_statement="" 
    /// query_timeout=0 
    /// db_connection=""
    /// more_data_size=100 (0=infinite)
    /// more_data_timeout=30000
    /// class=GEO
    /// </param>
    /// <returns></returns>
/*
    public static db_vector_reader_sql make(params object [] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs); 
      int more_data_size = (int)args.get("more_data_size",0);
      int more_data_timeout = (int) args.get("more_data_timeout",0);
      object _class = args.get("class", c_CASL.GEO);

      db_vector_reader_sql SM = null;
      SM = new db_vector_reader_sql(arg_pairs);      
      SM.m_more_data_size = more_data_size;
      SM.m_more_data_timeout = more_data_timeout;
      SM.m_class = _class;
      SM.m_status = 0;
      return SM;
    }
*/

    public static db_vector_reader_sql CASL_make(params object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      int more_data_size = (int)args.get("more_data_size", 0);
      int more_data_timeout = (int)args.get("more_data_timeout", 0);
      object _class = args.get("class", c_CASL.GEO);

      db_vector_reader_sql SM = null;
      SM = new db_vector_reader_sql(arg_pairs);
      SM.m_more_data_size = more_data_size;
      SM.m_more_data_timeout = more_data_timeout;
      SM.m_class = _class;
      SM.m_status = 0;
      return SM;
    }

    public static db_vector_reader_sql make(GenericObject db_connection, string sql_text,
      CommandType command_type, int query_timeout, Hashtable sql_args, int more_data_size,
      int more_data_timeout, GenericObject object_class, ref string error_message)
    {
      db_vector_reader_sql new_reader = new db_vector_reader_sql(db_connection, sql_text, command_type,
        sql_args, query_timeout, ref error_message);
      if (new_reader != null)
      {
        new_reader.m_more_data_size = more_data_size;
        new_reader.m_more_data_timeout = more_data_timeout;
        if (object_class != null)
        {
          new_reader.m_class = object_class;
        }
        else
        {
          new_reader.m_class = c_CASL.GEO;
        }
        new_reader.m_status = 0;
      }
      return new_reader;
    }

    public static db_vector_reader_sql make_transaction_reader(GenericObject db_connection,
      int query_timeout, int more_data_size,
      int more_data_timeout, ref string error_message)
    {
      db_vector_reader_sql new_reader = new db_vector_reader_sql(db_connection, query_timeout, ref error_message);
      if (new_reader != null)
      {
        new_reader.m_more_data_size = more_data_size;
        new_reader.m_more_data_timeout = more_data_timeout;
        new_reader.m_class = c_CASL.GEO;
        new_reader.m_status = 0;
      }
      return new_reader;
    }

    /// <summary>
    /// create worker thread to retrieve data 
    /// </summary>
    public void retrieve()
    {
      // initialize retrieve for each time. 
      m_cancel_flag = 0;// Let it run.
      //      m_start_flag =false;

      //----------------------------------------------------------------
      // Initialize module.
      if (m_SearchThreadTerminated_Event != null)
        m_SearchThreadTerminated_Event.Close();

      m_SearchThreadTerminated_Event = new ManualResetEvent(false);

      m_SearchThread = new System.Threading.Thread(
        new System.Threading.ThreadStart(this.Work));

      m_SearchThread.IsBackground = true;

      // Bug #8025 Mike O - Prioritize DB threads over main threads
      m_SearchThread.Priority = ThreadPriority.AboveNormal;

      m_SearchThread.Start();

      // Bug #8025 Mike O - Wait in the semaphore until m_SearchThread has entered the lock
      m_waiting_for_lock.WaitOne();

      if (m_more_data_timeout > 0)
      {
        if (Monitor.TryEnter(m_retrieval_lock, m_more_data_timeout))
        {
          Monitor.Exit(m_retrieval_lock);
        }
      }
      else
      {
        lock (m_retrieval_lock) { }
      }

      // CHECK ERROR, if error, throw error
      if (m_iErrorState == 1)
      {
        CloseReader();        // TODO: move up to calling method...
        string strErrorMsg = GetErrMsg().ToString();// BUG# 8135 DH
                                                    // BUG# 7391
                                                    // do not show the full error message if verbose of off.
                                                    // Bug #7677 Mike O - Retrieve the correct show_verbose_error
                                                    // Bug #7998 Mike O - Default show_verbose_error to false
        bool error = (bool)c_CASL.GEO.get("show_verbose_error", false, false);
        if (error)
          throw CASL_error.create("message", GetErrMsg().ToString(), "code", "DB_SQL-3");

        // BUG# 8135 DH - There are many different errors that should be mentioned here but for now at least the query timeout must be noted.
        // Otherwize it maybe impossible to get to the buttom of errors of this type.
        if (strErrorMsg.IndexOf("Timeout expired") > 0)
          throw CASL_error.create("message", "Database query timeout expired. Please turn on verbose error messages to get more detail about this error.", "code", "DB_SQL-3");

        throw CASL_error.create("message", "", "code", "DB_SQL-3");
      }

      return;
    }

    private void Work()
    {
      //      object box_flag = m_is_finished; //
      //      // ? Share reference between worker thread and main thread
      //      System.Threading.Interlocked.Exchange(ref box_flag, false);

      lock (m_retrieval_lock)
      {
        try
        {
          m_status = 1; //Working

          // Bug #8025 Mike O - Let the main thread out of the semaphore now
          m_waiting_for_lock.Release();

          // No error yet.
          System.Threading.Interlocked.Exchange(ref m_iErrorState, 0);
          System.Threading.Interlocked.Exchange(ref m_strErrorMSG, null);

          //*****************************************
          // Database specific.
          // Initialize the reader only when first run.
          if (m_dtReader == null)
          {
            try
            {
              m_dtReader = m_Command.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
              //13832 - add innerexception for better debugging
              string innerException = e.InnerException != null ? e.InnerException.Message : "";
              string strErrorMsg = e.Message + innerException + ";SQL:" + m_Command.CommandText;
              Logger.LogError("DB ERROR(DB_SQL-10)" + strErrorMsg, "Db vector Work");

              object err;
              err = String.Format("Database class: {0}<br>Data Source:  {1}<br>Error Description: {2} <br>SQL Statement: {3}", m_strConnectionClass, m_strConnectionDataSource, e.Message, m_Command.CommandText);
              System.Threading.Interlocked.Exchange(ref m_strErrorMSG, err);
              System.Threading.Interlocked.Exchange(ref m_iErrorState, 1);
              m_SearchThreadTerminated_Event.Set();

              // HX: Bug 17849
              Logger.cs_log(e.ToMessageAndCompleteStacktrace());

              // Bug 25125
              misc.CASL_call("a_method", "actlog", "args",
                new GenericObject(
                    "app", misc.getApplicationName()
                                                // Bug 20435 UMN PCI-DSS don't pass session
                    , "user", misc.getLoginName()
                    , "action", "Error"
                    , "summary", "Database Error"
                    , "detail", "db_vector_reader_sql Work() ExecuteReader:" + misc.FormatStacktraceForActivityLog(e)
                    )
                    );


              return;
            }
          }
          //*****************************************

          //-----------------------------------------------------------------------------
          // BUG# 8135 DH Add try, catch block
          try
          {
            GetRecords();
          }
          catch (Exception e)
          {
            // BUG# 9070 DH - Originally added to GH to catch issues in SQL statements and prevent crashing.
            //13832 - add innerexception for better debugging
            string innerException = e.InnerException != null ? e.InnerException.Message : "";
            string strErrorMsg = e.Message + innerException + ";SQL:" + m_Command.CommandText;
            Logger.LogError("DB ERROR(DB_SQL-11)" + strErrorMsg, "Db vector Work");

            // Bug 25125
            misc.CASL_call("a_method", "actlog", "args",
              new GenericObject(
                  "app", ""   // Let writeLogRecord() find this
                  // Bug 20435 UMN PCI-DSS don't pass session
                  , "user", "" // Let writeLogRecord() find this
                  , "action", "Error"
                  , "summary", "Database Error"
                  , "detail", "db_vector_reader_sql Work() error (DB_SQL-11):" + misc.cleanUpDetailsForActLog(strErrorMsg)
                  )
                  );
            throw CASL_error.create("message", strErrorMsg, "code", "DB_SQL-3");


            //m_SearchThreadTerminated_Event.Set();
            //Close();

            //string strErrorMsg = GetErrMsg().ToString();
            //bool error = (bool)c_CASL.c_object("GEO.show_verbose_error");
            //if (error)
            //  throw CASL_error.create("message", strErrorMsg, "code", "DB_SQL-3");

            //if (strErrorMsg.IndexOf("Timeout expired") > 0)
            //  throw CASL_error.create("message", "Database query timeout expired. Please turn on verbose error messages to get more detail about this error.", "code", "DB_SQL-3");

          }
          //-----------------------------------------------------------------------------

          m_SearchThreadTerminated_Event.Set();
          Close();
        }
        catch (Exception e)
        {
          //13832 - add innerexception for better debugging 
          //string innerException = e.InnerException != null ? e.InnerException.Message : "";
          //string strErrorMsg = e.Message + innerException + ";SQL:" + m_Command.CommandText;
          //Logger.LogError("DB ERROR(DB_SQL-12)" + strErrorMsg, "Db vector Work");

          // HX: Bug 17849 commented out bug 13832
          Logger.cs_log_trace("DB Error (DB Vector Work)", e);
        }
        //      box_flag = m_is_finished;
        //      System.Threading.Interlocked.Exchange(ref box_flag, true);
      }
    }

    /// <summary>
    /// Called by the main search worker thread.
    /// Reader will stay open until we iterate to the end of result set.
    /// </summary>
    private void GetRecords()
    {
      GenericObject geoFieldOrder = new GenericObject();
      int more_data_length = 1;

      // Bug 15941:  Make sure reader is closed on abnormal exit
      try
      {
        //m_status = 1; // Working.

        //      object box_flag = m_is_finished;
        //      System.Threading.Interlocked.Exchange(ref box_flag, false);

        //************************************************************
        //**************ADD DATA TO GEO DATA OBJECT*******************
        lock (m_retrieval_lock)
        {
          if (m_dtReader != null && !m_dtReader.IsClosed)  // HAS OPEN READER
          {
            while (m_dtReader.Read()) // RETRIEVE DATA, ADD ROW TO m_Data
            {
              // Get columns.
              GenericObject GEO_Columns = new GenericObject("_parent", m_class);
              int FC = m_dtReader.FieldCount;
              for (int i = 0; i < m_dtReader.FieldCount; i++)
              {
                object obj_column_value = m_dtReader.GetValue(i);
                string strColName = m_dtReader.GetName(i);
                // Create column field order
                geoFieldOrder.set(i, strColName);
                // DO NOT SET GEO FIELD WHEN DATA VALUE IS NULL
                if (obj_column_value == System.DBNull.Value)
                  continue;

                GEO_Columns.set(strColName, ConvertTypeForCASL(obj_column_value));
              }
              //          if(m_Data == null)// Create the m_Data if this is the first time around.
              //            m_Data = new GenericObject();
              m_Data.insert(GEO_Columns);

              // RETURN OR CONTINUE
              // BREAK IF NOT INFINITE RETRIEVE AND MORE DATA SIZE IS REACHED ,0 means Infinite.
              if (m_more_data_size != 0 && (more_data_length >= m_more_data_size))
              {
                //            stop_retrieving();
                m_status = 2;
                return;
              }
              else if (m_cancel_flag == 1) // BREAK USER CANCEL TASK .
              {
                m_status = 2;
                return;
              }
              else
              {
                more_data_length++;
              }
            }// while
          }// if
           // SET NO MORE DATA FLAG. 
           //      m_no_more_data = true;
        }// lock
      }
      finally
      {
        // Bug 15941
        CloseReader();
      }
      m_status = 2;
    }

    //    public string get_status()
    //    {
    //      if( m_dtReader == null)
    //        return "done";
    //      else if (m_SearchThread == null)
    //        return "stopped";
    //      else if (m_is_paused)
    //        return "paused";
    //      else
    //        return "working";
    //    }
    public object ConvertTypeForCASL(object val)
    {
      //-------------------------------
      // TRY TO CONVERT ALL DATABASE DATA TYPES TO WORK WITH CASL CODE.
      // CASL ONLY SUPPORTS INT32

      // Ignore nulls and database nulls
      if (val == null || val == System.DBNull.Value)
        return val;

      object NewVal = val;

      // INT16 > INT32
      if (val.GetType().ToString() == "System.Int16")
        NewVal = (int)Convert.ChangeType(val, typeof(System.Int32));

      else

        // DECIMAL > DOUBLE > LONG > INT
        // Oracle uses decimal for all numeric dataypes.
        // Anything without a decimal must be changed to long.
        if (val.GetType().ToString() == "System.Decimal")
      {
        string s = string.Format("{0}", val);
        if (s.IndexOf(".", 0, s.Length) == -1)
          NewVal = Decimal.ToInt32((Decimal)val);
      }

      // CONVERSION TAKES SO LONG THAT I HAD TO SKEEP ALL OTHER DATATYPES.
      //     else
      //
      //      // DOUBLE > STRING
      //      if(val.GetType().ToString() == "System.Double")
      //        NewVal = (int)Convert.ChangeType(val, typeof(System.String));
      //
      //     else
      //       
      //      // DATE > STRING
      //      if(val.GetType().ToString() == "System.DateTime")
      //        NewVal = (int)Convert.ChangeType(val, typeof(System.String));
      //
      //      else  // everything else to string.
      //  
      //      if(val.GetType().ToString() == "System.Double")
      //        NewVal = (int)Convert.ChangeType(val, typeof(System.String));

      return NewVal;
    }

    public void CloseReader()
    {
      try
      {
        // Bug 16433 [21921] DJD - add close check and log error
        if ((m_dtReader != null) && (!m_dtReader.IsClosed))
        {
          m_dtReader.Close();
          m_dtReader = null;
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log(e.ToMessageAndCompleteStacktrace());
      }
      finally
      {
        // Bug 16433 [21921] DJD - Dispose SqlCommands in ArrayList:
        if ((sqlCommands != null) && (sqlCommands.Count > 0))
        {
          foreach (SqlCommand command in sqlCommands)
          {
            if (command != null)
            {
              command.Dispose();
            }
          }
        }

        // Bug 16433 [21921] DJD - Dispose SqlCommand
        if (m_Command != null)
        {
          m_Command.Dispose();
        }

        // Bug 16433 [21921] DJD - Dispose SqlTransaction:
        if (m_Transaction != null)
        {
          m_Transaction.Dispose();
        }

        // Bug 16433 [21921] DJD - Close SqlConnection:
        if ((m_SqlConn != null) && (m_SqlConn.State != ConnectionState.Closed))
        {
          m_SqlConn.Close();
          m_SqlConn = null;
        }
      }
    }
    public void close_reader() // finished_reader , ReaderFinished
    {
      // DEPRECATED, move to CloseReader
      CloseReader();
    }
    public bool IsErr(params Object[] args)
    {
      if (m_iErrorState == 1)
        return true;
      else
        return false;
    }

    public object GetErrMsg()
    {
      return m_strErrorMSG;
    }


    /// <summary>
    /// Adds a SqlCommand to the command queue
    /// </summary>
    /// <param name="new_command">the command to add</param>
    private void AddCommand(SqlCommand new_command)
    {
      sqlCommands.Add(new_command);
    }
    /// <summary>
    /// Adds a SqlCommand to the command queue
    /// </summary>
    /// <param name="command_text">Command text for the new command</param>
    /// <param name="command_type">Command type for the new command</param>
    /// <param name="sql_args">arguments for the new command</param>
    public void AddCommand(string command_text, CommandType command_type, Hashtable sql_args)
    {
      SqlCommand new_command = new SqlCommand();
      new_command.CommandText = command_text;
      new_command.CommandType = command_type;
      new_command.Connection = m_SqlConn;
      if (m_Transaction != null)
      {
        new_command.Transaction = m_Transaction;
      }
      #region Set command arguments
      if (sql_args != null)
      {
        // TODO: loop through string keys of sql_args, and for each, add as SQL parameter based on the type.
        object arg_value = null;
        SqlParameter parameter = null;
        foreach (string key in sql_args.Keys)
        {
          arg_value = sql_args[key];
          parameter = new SqlParameter();
          parameter.ParameterName = "@" + key;
          parameter.SqlDbType = DatabaseParamUtils.getSqlDbType(arg_value, parameter.ParameterName);  // Bug 25175
          parameter.Direction = ParameterDirection.Input;
          if (arg_value == null)
          {
            parameter.Value = DBNull.Value;
          }
          else
          {
            parameter.Value = arg_value;
          }
          new_command.Parameters.Add(parameter);
        }
      }
      #endregion
      sqlCommands.Add(new_command);
    }

    /// <summary>
    /// Executes a non-query SQL statement
    /// </summary>
    /// <returns>the number of rows affected</returns>
    public int ExecuteNonQuery(ref string error_message)
    {
      int result = 0;
      try
      {
        result = m_Command.ExecuteNonQuery();
      }
      catch (Exception e)
      {
        //13832 - add innerexception for better debugging
        //string innerException = e.InnerException != null ? e.InnerException.Message : "";
        //error_message = e.Message + innerException;
        // HX: Bug 17849 
        Logger.cs_log_trace("ExecuteNonQuery Failed", e);
        // Bug 25125
        misc.CASL_call("a_method", "actlog", "args",
          new GenericObject(
              "app", ""   // Let writeLogRecord() find this
                          // Bug 20435 UMN PCI-DSS don't pass session
              , "user", "" // Let writeLogRecord() find this
              , "action", "Error"
              , "summary", "Database Error"
              , "detail", "db_vector_reader_sql ExecuteNonReader(). Error:" + misc.FormatStacktraceForActivityLog(e)
              )
        );

      }
      finally
      {
        CloseReader();
      }
      return result;
    }
    /// <summary>
    /// Establishes a SQL transaction
    /// </summary>
    /// <param name="error_message">an output string of the error message</param>
    /// <returns>whether the transaction was established</returns>
    public bool EstablishTransaction(ref string error_message)
    {
      try
      {
        m_Transaction = m_SqlConn.BeginTransaction(IsolationLevel.ReadCommitted);
      }
      catch (Exception e)
      {
        //13832 - add innerexception for better debugging
        //string innerException = e.InnerException != null ? e.InnerException.Message : "";
        //error_message = "Error beginning transaction. " + e.Message + innerException;

        // HX: Bug 17849
        Logger.cs_log_trace("Error beginning transaction", e);
        // Bug 25125
        misc.CASL_call("a_method", "actlog", "args",
          new GenericObject(
              "app", ""   // Let writeLogRecord() find this
                          // Bug 20435 UMN PCI-DSS don't pass session
              , "user", "" // Let writeLogRecord() find this
              , "action", "Error"
              , "summary", "Database Error"
              , "detail", "db_vector_reader_sql EstablishTransaction(). Error:" + misc.FormatStacktraceForActivityLog(e)
              )
        );


        CloseReader();
        return false;
      }
      return true;
    }
    /// <summary>
    /// Executes a single transactional SQL statement
    /// </summary>
    /// <param name="strError">the error message if an error occurs</param>
    /// <param name="iRowCount">the number of rows affected</param>
    /// <returns>whether the execution succeeds</returns>
    public bool ExecuteTransaction(ref string strError, ref int iRowCount)
    {
      bool success = true;
      try
      {
        if (m_Transaction == null)
        {
          strError = "No transaction for this reader.";
          success = false;
        }
        else
        {
          iRowCount = ExecuteNonQuery(ref strError);
        }
      }
      catch (Exception e)
      {
        //13832 - add innerexception for better debugging
        //string innerException = e.InnerException != null ? e.InnerException.Message : "";
        //strError = "Error executing transaction." + e.Message + innerException;

        // HX: Bug 17849
        Logger.cs_log_trace("Error executing transaction", e);
        m_Transaction.Rollback();
        // Bug 25125
        misc.CASL_call("a_method", "actlog", "args",
          new GenericObject(
              "app", ""   // Let writeLogRecord() find this
                          // Bug 20435 UMN PCI-DSS don't pass session
              , "user", "" // Let writeLogRecord() find this
              , "action", "Error"
              , "summary", "Database Error"
              , "detail", "db_vector_reader_sql ExecuteTransaction(). Error:" + misc.FormatStacktraceForActivityLog(e)
              )
        );

        success = false;
      }
      finally
      {
        //CloseReader called in Commit/Rollback
        //CloseReader();
      }
      return success;
    }
    /// <summary>
    /// Executes all the commands associated with the SQL connection
    /// </summary>
    /// <param name="strError">the error message if an error occurs</param>
    /// <param name="iRowCount">the number of rows affected</param>
    /// <returns></returns>
    public bool ExecuteTransactionCommands(ref string strError, ref int iRowCount, bool bCloseReader)
    {
      bool success = true;
      try
      {
        if (m_Transaction == null)
        {
          strError = "No transaction for this reader. ";
          success = false;
        }
        else
        {
          foreach (SqlCommand command in sqlCommands)
          {
            command.Transaction = m_Transaction;
            iRowCount = command.ExecuteNonQuery();
          }
        }
      }
      catch (Exception e)
      {
        //13832 - add innerexception for better debugging
        //string innerException = e.InnerException != null ? e.InnerException.Message : "";
        //strError = "Error executing transactions. " + e.Message + innerException;

        // HX: Bug 17849
        Logger.cs_log_trace("Error executing transaction", e);
        m_Transaction.Rollback();
        // Bug 25125
        misc.CASL_call("a_method", "actlog", "args",
          new GenericObject(
              "app", ""   // Let writeLogRecord() find this
                          // Bug 20435 UMN PCI-DSS don't pass session
              , "user", "" // Let writeLogRecord() find this
              , "action", "Error"
              , "summary", "Database Error"
              , "detail", "db_vector_reader_sql ExecuteTransactionCommands(). Error:" + misc.FormatStacktraceForActivityLog(e)
              )
        );

        success = false;
      }
      finally
      {
        // Added by Daniel for bug 6218. Keep it open here.
        // CloseReader called in Commit/Rollback
        //if(bCloseReader)
        //   CloseReader();
      }
      return success;
    }
    /// <summary>
    /// Commits the SQL transaction
    /// </summary>
    /// <param name="error_message">the error message if an error occurs</param>
    /// <returns>whether the commit succeeds</returns>
    public bool CommitTransaction(ref string error_message)
    {
      bool success = true;
      try
      {
        m_Transaction.Commit();
      }
      catch (Exception e)
      {
        //13832 - add innerexception for better debugging
        //string innerException = e.InnerException != null ? e.InnerException.Message : "";
        //error_message = "Error committing transaction. " + e.Message + innerException;

        // HX: Bug 17849
        Logger.cs_log_trace("Error committing transaction", e);
        // Bug 25125
        misc.CASL_call("a_method", "actlog", "args",
          new GenericObject(
              "app", ""   // Let writeLogRecord() find this
                          // Bug 20435 UMN PCI-DSS don't pass session
              , "user", "" // Let writeLogRecord() find this
              , "action", "Error"
              , "summary", "Database Error"
              , "detail", "db_vector_reader_sql CommitTransaction(). Error:" + misc.FormatStacktraceForActivityLog(e)
              )
        );

        success = false;
      }
      finally
      {
        CloseReader();
      }
      return success;
    }

    public bool RollbackTransaction(ref string error_message)
    {
      bool success = true;
      try
      {
        m_Transaction.Rollback();
      }
      catch (Exception e)
      {
        //13832 - add innerexception for better debugging
        //string innerException = e.InnerException != null ? e.InnerException.Message : "";
        //error_message = "Error rolling back transaction. " + e.Message + innerException;
        // HX: Bug 17849
        Logger.cs_log_trace("Error rolling back transaction", e);
        // Bug 25125
        misc.CASL_call("a_method", "actlog", "args",
          new GenericObject(
              "app", ""   // Let writeLogRecord() find this
                          // Bug 20435 UMN PCI-DSS don't pass session
              , "user", "" // Let writeLogRecord() find this
              , "action", "Error"
              , "summary", "Database Error"
              , "detail", "db_vector_reader_sql RollbackTransaction(). Error:" + misc.FormatStacktraceForActivityLog(e)
              )
        );

        success = false;
      }
      finally
      {
        CloseReader();
      }
      return success;
    }


    //TODO: Move to db_reader, not in use
    /*
    public static int db_exec_sql(params Object[] args)
    {
      int lRows = 0;
      GenericObject geo_args = misc.convert_args(args);
      SqlConnection SqlConn = (SqlConnection)geo_args.get("a_connection", "");// OPEN Sql Connection.
      SqlCommand cmd = new SqlCommand();
      cmd.Connection = SqlConn;// reuse connection object.
      cmd.CommandText = (string)geo_args.get("sql_statement", "");// SQL Statement
      try
      {
        lRows = cmd.ExecuteNonQuery();
      }
      catch(Exception e)
      {   
        throw CASL_error.create("message", e.Message.ToString(), "code", "DB_SQL-4");
      }

      return lRows;
    }
    */

    /*
    public void pause_receiving() // ManuallyPauseThread
    {
      System.Threading.Interlocked.Exchange(ref m_pause_flag, 1);// Pause searching thread.
    }
    public void continue_receiving()// ManuallyRestartThread
    {
      System.Threading.Interlocked.Exchange(ref m_pause_flag, 0);// Resume search.
    }
    */

    public void wait_until_stopped()
    {
      lock (m_retrieval_lock) { }
    }

    //    public bool is_stopped()
    //    {
    //      return (m_is_paused || m_is_finished);
    //    }



    public int GetMax(int lStartVal, int lViewSize, int lTotalItems)
    {
      int j = 0;
      for (int i = 0; i < lViewSize; i++)
      {
        if (lStartVal + j < lTotalItems)
          j++;
        else
          break;
      }

      return lStartVal + j;
    }

    /// <summary>
    /// CASL_GetBufferedData
    /// </summary>
    /// <param name="args">reader=req </param>
    /// <returns></returns>
    //    public static GenericObject CASL_GetBufferedData(params Object[] args)
    //    {
    //      // Called from CASL code. The instance is passed as _subject.
    //      GenericObject geo_args= misc.convert_args(args);
    //      db_vector_reader_sql reader = geo_args.get("reader") as db_vector_reader_sql;
    //
    //      return reader.GetBufferedData(args);
    //    }

    /// <summary>
    /// CASL_insert_data
    /// </summary>
    /// <param name="arg_pairs"> 
    /// _subject=req=<one_of> GenericObject db_vector </one_of>
    /// reader=req=cs.CASL_engine.db_vector_reader_sql 
    ///    - db_vector_reader_sql instance which is maked by calling
    ///     with live database connection. 
    /// insert_size=req=Integer - the maximum size to insert , if 0, insert all data
    /// start=req=Integer  - start index to insert
    /// </param>
    /// <returns></returns>
    public static object CASL_insert_data(params object[] arg_pairs)
    {
      #region Set arguments
      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject subject = args.get("_subject") as GenericObject;
      db_vector_reader_sql a_db_vector_reader_sql = args.get("reader") as db_vector_reader_sql;
      int insert_size = (int)args.get("insert_size");
      int start = (int)args.get("start");
      #endregion
      #region Read
      a_db_vector_reader_sql.ReadIntoRecordset(ref subject, start, insert_size);
      #endregion
      return subject;
    }
    public void ReadIntoRecordset(ref GenericObject record_set, int start, int insert_size)
    {
      // Bug 15941. FT. Make sure the database connection is closed
      try
      {
        #region Initial read
        if (m_status == 0 && (insert_size == 0 ||
                             (start + insert_size) > m_Data.getLength())) // Not started
        {
          retrieve();
        }
        #endregion
        #region Continue read
        // Lock on retrieval for continued reading
        lock (m_retrieval_lock)
        {
          int end = Math.Min(start + insert_size, m_Data.getLength());
          if (insert_size == 0)
          {
            end = m_Data.getLength();
          }
          int record_set_length = record_set.vectors.Count;
          record_set.vectors.InsertRange(record_set_length,
            m_Data.vectors.GetRange(record_set_length,
            end - record_set_length));
        }
        #endregion
        // Finished
      }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseReader();
      }
    }

    public SqlConnection get_db_connection(string strConnect, ref string error_message)
    {
      // Create new database connection.
      SqlConnection SqlConn = null;
      try
      {
        //BUG19591 command out to avoid double append pool setting, move to Db_connection_info.Sql_server.make_db_connection_string, it will set it for all ipayment to use
        SqlConn = new SqlConnection(strConnect);
        SqlConn.Open();
        return SqlConn;
      }
      catch (Exception e)
      {
        string strTemp = strConnect.Substring(0, strConnect.IndexOf(";", 0, strConnect.Length));
        //13832 - add innerexception for better debugging
        //string innerException = e.InnerException != null ? e.InnerException.Message : "";
        //error_message = e.Message.ToString() + innerException + "<br>Connection string:<br>" + strTemp;

        // HX: Bug 17849
        Logger.cs_log_trace("Error database connection", e);
        // Bug 25125
        misc.CASL_call("a_method", "actlog", "args",
          new GenericObject(
              "app", ""   // Let writeLogRecord() find this
                          // Bug 20435 UMN PCI-DSS don't pass session
              , "user", "" // Let writeLogRecord() find this
              , "action", "Error"
              , "summary", "Database Error"
              , "detail", "db_vector_reader_sql get_db_connection(). Error:" + misc.FormatStacktraceForActivityLog(e)
              )
        );

      }
      return null;
    }

    /// <summary>
    /// Check if db_request has more static data than the partial vector 
    /// </summary>
    /// <param name="args"> 
    /// _subject=req=GenericObject
    /// reader=req=cs.CASL_engine.db_vector_reader_sql
    /// </param>
    /// <returns></returns>
    public static bool CASL_has_more_data(params object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject subject = args.get("_subject") as GenericObject;
      db_vector_reader_sql a_db_vector_reader_sql = args.get("reader") as db_vector_reader_sql;
      return (subject.getLength() < a_db_vector_reader_sql.m_Data.getLength());
    }

    /// <summary>
    /// Check if db_vector_reader_sql has more static data than the partial vector 
    /// or db_request is not done retrieving
    /// if return true, show next button 
    /// if return true, call insert_data to insert data
    /// </summary>
    /// <param name="args"> 
    /// _subject=req=GenericObject
    /// reader=null=cs.CASL_engine.db_vector_reader_sql
    /// </param>
    /// <returns></returns>
    public static object CASL_may_have_more_data(params object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject subject = args.get("_subject") as GenericObject;
      db_vector_reader_sql a_db_vector_reader_sql = args.get("reader") as db_vector_reader_sql;

      // RETURN FALSE WHEN NO READER AVAIBLE
      if (a_db_vector_reader_sql == null) return false;
      // return true if has more static data in reader
      // or reader is not done.
      return (CASL_has_more_data(args) || (a_db_vector_reader_sql.m_status != 2)); // Not finished
    }

    public byte[] ConvertImage_JPG(object objIMG)
    {
      // Take an image and convert it to JPG. This is needed for the CASL code for display on web pages.     
      byte[] b = (byte[])objIMG;
      System.IO.MemoryStream stream = new System.IO.MemoryStream(b, true);
      stream.Write(b, 0, b.Length);
      Bitmap bmp = new Bitmap(stream);

      //Select the image encoder
      ImageCodecInfo info = null;
      foreach (ImageCodecInfo ice in ImageCodecInfo.GetImageEncoders())
        if (ice.MimeType == "image/jpeg")
          info = ice;

      EncoderParameters ep = new EncoderParameters(1);
      System.Drawing.Imaging.Encoder enc = System.Drawing.Imaging.Encoder.Quality;
      ep.Param[0] = new EncoderParameter(enc, (long)EncoderValue.Flush);

      System.IO.MemoryStream stream2 = new System.IO.MemoryStream();
      bmp.Save(stream2, info, ep);
      //bmp.Save("c:\\0\\y.jpg", info, ep);

      byte[] byteJPG = new byte[b.Length];
      byteJPG = stream2.ToArray();
      stream.Close();

      return byteJPG;
    }

    public static void CloseReader(params object[] arg_pairs)
    {
      // Daniel added this in order to be able to create a new query with the same database connection but paging on.

      GenericObject args = misc.convert_args(arg_pairs);
      db_vector_reader_sql _this = args.get("_reader") as db_vector_reader_sql;

      try
      {
        _this.m_dtReader.Close();
      }
      catch (Exception e)
      {
        //Bug 13832-add innerexception for better handling
        //string innerException = e.InnerException != null ? e.InnerException.Message : "";
        //string s = e.Message + innerException;
        Logger.cs_log(e.ToMessageAndCompleteStacktrace()); // Bug 16433 [21921] log error and bug 17849
        // Bug 25125
        misc.CASL_call("a_method", "actlog", "args",
          new GenericObject(
              "app", ""   // Let writeLogRecord() find this
                          // Bug 20435 UMN PCI-DSS don't pass session
              , "user", "" // Let writeLogRecord() find this
              , "action", "Error"
              , "summary", "Database Error"
              , "detail", "db_vector_reader_sql CloseReader(). Error:" + misc.FormatStacktraceForActivityLog(e)
              )
        );

      }
    }

    /// <summary>
    /// RunSqlTransaction inserts a vector of sql statements into DB with rollback enabled.
    /// Takes the DSN and a vector of sql statements.
    /// Written by Axel Lekander
    /// </summary>
    /// <param name="args"></param>
    /// <returns>null if no rollback has been made else returns casl error</returns>
    public static object RunSqlTransaction(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      string myConnString = (string)geo_args.get("db_connection");
      GenericObject sql_statements = geo_args.get("sql_statement") as GenericObject;
      GenericObject sql_args = (GenericObject)geo_args.get("sql_args", c_CASL.c_opt);
      GenericObject geoError = null;

      // Bug 16433 [21921] DJD - add "using"s and logged detailed errors in cs log
      using (SqlConnection myConnection = new SqlConnection(myConnString))
      {
        try
        {
          myConnection.Open();
          using (SqlCommand myCommand = myConnection.CreateCommand())
          {
            //-----------------------------------------------------------------------
            if (!sql_args.Equals(c_CASL.c_opt))
            {
              // TODO: loop through string keys of sql_args, and for each, add as SQL parameter based on the type.
              ArrayList string_keys = sql_args.getStringKeys();
              object V = null;
              foreach (string field_key in string_keys)
              {
                V = sql_args.get(field_key);
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@" + field_key;
                param.SqlDbType = DatabaseParamUtils.getSqlDbType(V, param.ParameterName);  // Bug 25175
                param.Direction = ParameterDirection.Input;
                param.Value = V;
                myCommand.Parameters.Add(param);
              }
            }
            //-----------------------------------------------------------------------

            // Start a local transaction
            using (SqlTransaction myTrans = myConnection.BeginTransaction(IsolationLevel.ReadCommitted))
            {
              // Assign transaction object for a pending local transaction
              myCommand.Transaction = myTrans;
              int sql_statement_index = 0;
              try
              {
                for (int i = 0; i < sql_statements.getLength(); i++)
                {
                  myCommand.CommandText = (string)sql_statements.get(i);
                  myCommand.ExecuteNonQuery();
                  sql_statement_index++;  // used in error statement to know which sql statement errors
                }
                myTrans.Commit();
              }
              catch (Exception e)
              {
                string innerException = "";
                // bug 17849
                Logger.cs_log(e.ToMessageAndCompleteStacktrace());
                try
                {
                  myTrans.Rollback();
                }
                catch (SqlException ex)
                {
                  // bug 17849
                  Logger.cs_log(ex.ToMessageAndCompleteStacktrace());
                  if (myTrans.Connection != null)
                  {
                    //Bug 13832-add innerexception for better handling
                    innerException = e.InnerException != null ? e.InnerException.Message : "";
                    geoError = (GenericObject)c_CASL.c_make("error", "code", "DB_SQL-6", "message", ex.Message + innerException +
                    " Occured when attempting to roll back the transaction. SQL statement #" + sql_statement_index + ": " + myCommand.CommandText, "code", "DB_SQL-6", "code", "DB_SQL-7", "title", "error", "sql_statement", myCommand.CommandText, "throw", false);
                    return geoError;
                  }
                }
                //Bug 13832-add innerexception for better handling
                innerException = e.InnerException != null ? e.InnerException.Message : "";
                string msg = e.Message + innerException +
                " Occured while inserting data.  No records were written to the database. SQL statement #" + sql_statement_index + ": " + myCommand.CommandText;
                geoError = (GenericObject)c_CASL.c_make("error", "code", "DB_SQL-7", "message", msg, "code", "DB_SQL-7", "title", "error", "sql_statement", myCommand.CommandText, "throw", false);
                // Bug 25125
                misc.CASL_call("a_method", "actlog", "args",
                  new GenericObject(
                      "app", ""   // Let writeLogRecord() find this
                                  // Bug 20435 UMN PCI-DSS don't pass session
                      , "user", "" // Let writeLogRecord() find this
                      , "action", "Error"
                      , "summary", "Database Error"
                      , "detail", "db_vector_reader_sql RunSqlTransaction() error (DB_SQL-7). Error:" + misc.cleanUpDetailsForActLog(msg)
                      )
                ); 
                return geoError;
              }
            }
          }
        }
        catch (Exception ex)
        {
          // 17849
          Logger.cs_log(ex.ToMessageAndCompleteStacktrace());
          geoError = (GenericObject)c_CASL.c_make("error", "code", "DB_SQL-9", "message", ex.Message +
          " Occured while opening database connection.", "title", "error", "throw", false);
          // Bug 25125
          misc.CASL_call("a_method", "actlog", "args",
            new GenericObject(
                "app", ""   // Let writeLogRecord() find this
                            // Bug 20435 UMN PCI-DSS don't pass session
                , "user", "" // Let writeLogRecord() find this
                , "action", "Error"
                , "summary", "Database Error"
                , "detail", "db_vector_reader_sql RunSqlTransaction() error (DB_SQL-9). Error Occured while opening database connection:" + misc.FormatStacktraceForActivityLog(ex)
                )
          );


          return geoError;

        }
      }
      return null;
    }

    public object ExecuteTransaction(params object[] args)
    {
      return null;
    }

    public void Close()// NOT USED, Terminate search thread. 
    {
      //      System.Threading.Interlocked.Exchange(ref m_cancel_flag, 1);// Set to terminate

      if (m_SearchThread != null)
      {
        if (m_SearchThread.IsAlive)// Wait until dead.
        {
          m_SearchThreadTerminated_Event.WaitOne();
        }
      }

      m_SearchThreadTerminated_Event = null;
      m_SearchThread = null;

      //      //------------------------------------
      //      // Database specific.
      //      m_class = null;
      //      m_Command = null;
      //      if(m_dtReader != null)
      //      {
      //        if(!m_dtReader.IsClosed)
      //          m_dtReader.Close();
      //        m_dtReader = null;
      //      }
      //      //------------------------------------

      //      m_iClosingdb_request = 0;
    }
  }
}
