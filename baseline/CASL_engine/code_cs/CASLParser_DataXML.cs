using System;
using System.Collections;
using System.Text;
using System.Xml;
using System.Reflection;
using System.IO;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;


namespace CASL_engine
{
	/// <summary>
	/// Summary description for CASLParser_DataXML.
	/// </summary>
	public class CASLParser_DataXML
	{
		private char current_event;
		private int status;
		private XmlTextReader _aXmlTextReader;
//		private GenericObject path=null;

		private GenericObject CS = new GenericObject(); // Container Stack 
		private GenericObject KS = new GenericObject(); // Key Stack 
		private StringBuilder TOKEN;

		private GenericObject _field_order = new GenericObject(null,"key",null,"value",null,"type");
		
//		private GenericObject symbols = new GenericObject("null", null, "true", true, "false",false,"req","req","opt","opt", "UK", "UK", "END","END");
		private GenericObject symbols = new GenericObject("null", null, "true", true, "false",false, "UK", "UK", "END","END");

		public CASLParser_DataXML( )
		{

		}

    /*
		**
		** public void run()
		***/

    /// <summary>
    /// Run
    /// </summary>
    /// <param name="a_xml">a_xml=""=string</param>
    /// <param name="file_name"> file_name=""=string</param>
    /// <returns></returns>
    public virtual object Run(String a_xml, String file_name )
		{		
			Object return_obj=null;
			try
			{
				if( file_name == null || file_name.Trim().Length == 0)
				{
					if( a_xml== null )
						return null;
					if( a_xml.IndexOf("<") <0 || a_xml.Trim().Length == 0)
						return new GenericObject("_parent", "string", "_content", a_xml);
				}
				//-----------------------------------------
				// Use XMLReader(C# Parser) to parse XML
				if ( _aXmlTextReader == null )
				{
					//Read from file directly
					if(file_name!=null && file_name.Length >0 )
					{
						_aXmlTextReader = new XmlTextReader(file_name);
						_aXmlTextReader.WhitespaceHandling = WhitespaceHandling.None;
						_aXmlTextReader.Normalization = true;
					}
						//Read from Xml String Parameter
					else 
					{
						StringReader SR = new StringReader(a_xml);
						_aXmlTextReader = new XmlTextReader(SR);
						_aXmlTextReader.WhitespaceHandling = WhitespaceHandling.None;
						_aXmlTextReader.Normalization = true;
					}
				}

				//-----------------------------------------------------
				// fire event for all occations
				status = ConciseXMLStates2.START_FIELD;
				CS.insert(new GenericObject());
				KS.insert("END");
				while (_aXmlTextReader.Read() == true)
				{
					//If the nodeType is TEXT (i.e. some text value between the <> </> tags -
					//for example in "<cat>house</cat>", "house" would be TEXT), add the value
					//as a field to the GEO being created
					if (_aXmlTextReader.NodeType == XmlNodeType.Text) 
					{	
						String sTextValue = _aXmlTextReader.Value;
						//Process quotes in order to get the actual string (TOKEN)
						//to appear in the new GEO we're creating - processing those
						//"events" before and after putting the string into TOKEN does 
						//all kinds of stuff with initializing the temporary GEO's used 
						//to hold info, changing status, pushing and popping nodes, etc.
						current_event = '"';
						process_event();
						TOKEN.Append(sTextValue);
						current_event = '"';
						process_event();
					}
					//If the nodeType is ELEMENT (i.e. some text inside the <> tag -
					//for example, in "<cat>house<color>black</color></cat>", "color" 
					//would be an ELEMENT), add the value as a GEO to the main GEO being created
					else if (_aXmlTextReader.NodeType == XmlNodeType.Element) 
					{
						String sElementName = _aXmlTextReader.Name;
						current_event = '<';
						process_event();
						// set _parent and add it to GEO
						status = ConciseXMLStates2.SYMBOL_OR_STRING;
						reset_token();
						TOKEN.Append("_parent");
						current_event = '=';
						process_event();
						// set the element and add it to GEO
						status = ConciseXMLStates2.SYMBOL_OR_STRING;
						TOKEN.Append(sElementName);
						current_event = ' ';
						process_event();
					}
					if (_aXmlTextReader.NodeType == XmlNodeType.EndElement || (_aXmlTextReader.NodeType == XmlNodeType.Element && _aXmlTextReader.IsEmptyElement)  )
					{
						current_event = '>';
						process_event();
					}				
				}
				_aXmlTextReader.Close();
				char eos = (char)8;
				current_event = eos;
				process_event();
				return_obj= (TCS() as GenericObject).get(0);
			}
			catch(Exception)
			{
				_aXmlTextReader.Close();	
				throw;
			}
			return return_obj;
		}

		private bool in_path()
		{
			return TCS() is GenObj_Path;
		}
		private bool has_key()
		{
			return (KS.getLength() > CS.getLength());
		}
		private bool is_even_stack()
	 {
		return (KS.getLength() == CS.getLength());
		}
		private GenericObject new_object ()
		{
			GenericObject obj = new GenericObject();
			if (_aXmlTextReader.HasAttributes)
			{
				for (int i = 0; i < _aXmlTextReader.AttributeCount; i++)
				{
					_aXmlTextReader.MoveToAttribute(i);
					obj.set( _aXmlTextReader.Name, _aXmlTextReader.Value);
					if( ! obj.has("__parsed_field_order") )
						obj.set("__parsed_field_order", c_CASL.c_GEO());
					(obj.get("__parsed_field_order") as GenericObject).insert(_aXmlTextReader.Name);
				}
				_aXmlTextReader.MoveToElement(); //Moves the reader back to the element node.
			}

			return obj;
		}
		private void append_token()
		{
			TOKEN.Append(current_event);
		}
		private void reset_token()
		{
			TOKEN = new StringBuilder("");
		}		
		private object TKS()
		{
			return KS.get(KS.getLength()-1);
		}
		private object TCS()
		{
			return CS.get(CS.getLength()-1);
		}

		private Object string_end_action()
		{
			return TOKEN.ToString();
		}	

		private Object symbol_end_action()
		{
			if (symbols.has(TOKEN.ToString()))
				return symbols.get(TOKEN.ToString());
			GenObj_Symbol symbol = new GenObj_Symbol(TOKEN.ToString());
			return symbol;
		}	
		private void tcs_set_key_and_value()
		{
			if( !is_even_stack() )  // equiv to has_key
			{
				string s_message = string.Format("Not ConciseXML Encoding.");
				throw CASL_error.create("title","Syntax Error", "code",111, "message",s_message, "line_number",_aXmlTextReader.LineNumber, "line_position",_aXmlTextReader.LinePosition);
			}
			Object V = CS.remove();
			Object K = KS.remove();
			GenericObject tcs=(GenericObject)TCS();
			if( !"_parent".Equals(K) && tcs.has(K) )
			{
				string s_message = string.Format("Duplicate key '{0}' found ", K);
				throw CASL_error.create("title","Syntax Error", "code",103, "message",s_message, "line_number",_aXmlTextReader.LineNumber, "line_position",_aXmlTextReader.LinePosition);
			}
			 
			if( ! tcs.has("__parsed_field_order") )
				tcs.set("__parsed_field_order", c_CASL.c_GEO());
			(tcs.get("__parsed_field_order") as GenericObject).insert(K);
			tcs.set(K,V);		
		}

		private void tcs_set_unkey_and_value()
		{
			if( !is_even_stack() )  // equiv to has_key
			{
				string s_message = string.Format("Not ConciseXML Encoding.");
				throw CASL_error.create("title","Syntax Error", "code",112, "message",s_message, "line_number",_aXmlTextReader.LineNumber, "line_position",_aXmlTextReader.LinePosition);
			}
			KS.remove();
			Object V = CS.remove();
			((GenericObject)TCS()).insert(V);
		}

		private void cs_push_new_object_ks_push_uk()
		{
			GenericObject it= new_object();
			CS.insert(it);
			KS.insert("UK");
		}
		private void cs_push_new_object_ks_push_pp()
		{
			GenericObject it= new_object();
			CS.insert(it);
			KS.insert("PP");
		}
		private void cs_push_new_object_ks_push_end()
		{
			GenericObject it= new_object();
			CS.insert(it);
			KS.insert("END");
		}
		private void cs_push_new_object()
		{
			GenericObject it= new_object();
			CS.insert(it);
		}
		private void move_tcs_to_tks()
		{
			Object K= CS.remove();

			// Convert symbol key to string key
			if ( K is GenObj_Symbol)
                          K = (K as GenObj_Symbol).get("name").ToString();
			KS.remove();
			KS.insert(K);
		}	
		private void  process_event()
		{
			switch (status)
			{
				case ConciseXMLStates2.START: 
				{
					if( ConciseXMLCharSet2.IS_START_ELEMENT(current_event) ) 
					{
						cs_push_new_object_ks_push_end();
						status = ConciseXMLStates2.START_FIELD;
					}
					break;
				}
				case ConciseXMLStates2.START_FIELD: 
				{
					if( ConciseXMLCharSet2.IS_START_ELEMENT(current_event) ) 
					{
						if(in_path())
						{
							cs_push_new_object_ks_push_pp();
						}
						else if (has_key())
						{
							cs_push_new_object();
						}
						else 
						{
							cs_push_new_object_ks_push_uk();
						}
					}
					else if (ConciseXMLCharSet2.IS_END_ELEMENT(current_event))
					{
						if ( ( TKS() is string) && TKS().ToString() == "UK" ) 
						{
							tcs_set_unkey_and_value();
							if( (TKS() is string) && TKS().ToString() == "PP")
								tcs_set_unkey_and_value();
							status = ConciseXMLStates2.EAT_WS;
						}
						else if ( ( TKS() is string) && TKS().ToString() == "PP" )
						{
							tcs_set_unkey_and_value();
							status = ConciseXMLStates2.EAT_WS;
						}						
						else if ( (TKS()is string) && TKS().ToString() == "END" )
						{
							status = ConciseXMLStates2.END;
						}						
						else
						{
							tcs_set_key_and_value();
							if( (TKS() is string) && TKS().ToString() == "PP")
								tcs_set_unkey_and_value();
							status = ConciseXMLStates2.EAT_WS;
						}
					} // end a IS_END_ELEMENT
					else if (ConciseXMLCharSet2.IS_EQUAL(current_event)) //IS_EQUAL
					{
						if ( ( TKS() is string) && TKS().ToString() == "PP" )
						{
							tcs_set_unkey_and_value();
							if( (TKS() is string) && TKS().ToString() == "UK")
								move_tcs_to_tks();
							else // KK  - handle 3rd "type" position key=value=type
							{
								Object K = TKS();
								Object FK = misc.field_key(K,"type"); // FK = field_key , foo_f_type
								tcs_set_key_and_value();
								KS.insert(FK);
							}
						}
						else // IS NOT PP
						{
							if( (TKS() is string) && TKS().ToString() == "UK")
								move_tcs_to_tks();
							else // KK  - handle 3rd "type" position key=value=type
							{
								Object K = TKS();
								Object FK = misc.field_key(K,"type"); // FK = field_key , foo_f_type
								tcs_set_key_and_value();
								KS.insert(FK);
							}
						}
					}
					else if (ConciseXMLCharSet2.IS_EOS(current_event))
					{
						// check it is the bottom one, otherwise throw error
						status = ConciseXMLStates2.END;
					}					
					else if (ConciseXMLCharSet2.IS_ANY_CHAR(current_event))
					{
						reset_token();
						// Only using double quotes for data - single quotes behave as characters
						if (ConciseXMLCharSet2.IS_DOUBLEQUOTE(current_event)) 
						{
							status = ConciseXMLStates2.DOUBLE_QUOTED_STRING;
						}
						else if (ConciseXMLCharSet2.IS_TOKEN_CHAR(current_event) ||
								 ConciseXMLCharSet2.IS_DIGIT(current_event)  ||
								 ConciseXMLCharSet2.IS_SINGLEQUOTE(current_event)) 
						{
							append_token();
							status = ConciseXMLStates2.SYMBOL_OR_STRING;
						}
						else
						{
							string s_message = string.Format("Not ConciseXML Encoding.");
							throw CASL_error.create("title","Syntax Error",  "code",104, "message",s_message, "line_number",_aXmlTextReader.LineNumber, "line_position",_aXmlTextReader.LinePosition);
							//throw new XmlException("The Expression may not be compatiable to conciseXML encoding. ",null,_aXmlTextReader.LineNumber, _aXmlTextReader.LinePosition);
						}
					}
					else if (ConciseXMLCharSet2.IS_WS(current_event))
					{
						//do nothing
					}
					else
					{
						string s_message = string.Format("Not ConciseXML Encoding.");
						throw CASL_error.create("title","Syntax Error", "code",105, "message",s_message, "line_number",_aXmlTextReader.LineNumber, "line_position",_aXmlTextReader.LinePosition);
						//throw new XmlException("The Expression may not be compatiable to conciseXML encoding. ",null,_aXmlTextReader.LineNumber, _aXmlTextReader.LinePosition);
					}
					break;
				}
				////////////////////////////////////////////////////////
				// DOUBLE_QUOTED_STRING
				////////////////////////////////////////////////////////
				case ConciseXMLStates2.DOUBLE_QUOTED_STRING: 
				{
					if ( ConciseXMLCharSet2.IS_DOUBLEQUOTE(current_event) )
					{
						if(in_path())
						{
							(TCS() as GenericObject).insert(string_end_action());// two args
							reset_token();
							status = ConciseXMLStates2.EAT_WS;
						}
						else
						{
							if(has_key())
							{
								CS.insert(string_end_action());
								reset_token();
								status = ConciseXMLStates2.EAT_WS;
							}
							else
							{
								KS.insert("UK");
								CS.insert(string_end_action());
								reset_token();
								status = ConciseXMLStates2.EAT_WS;
							}
						}					
					}
					else
					{
						append_token();
					}
					break;
				}
				////////////////////////////////////////////////////////
				// SYMBOL_OR_STRING
				////////////////////////////////////////////////////////
				case ConciseXMLStates2.SYMBOL_OR_STRING: 
				{
					if(	ConciseXMLCharSet2.IS_TOKEN_CHAR(current_event) ) 
					{
						append_token();
					}
					else if (ConciseXMLCharSet2.IS_EV(current_event) )
					{
						if(in_path())
						{
							(TCS() as GenericObject).insert(string_end_action());
							reset_token();
							status = ConciseXMLStates2.EAT_WS;
							process_event();
						}
						else
						{
							if(has_key())
							{
								CS.insert(symbol_end_action());
								reset_token();
								status = ConciseXMLStates2.EAT_WS;
								process_event();
							}
							else
							{
								KS.insert("UK");
								CS.insert(symbol_end_action());
								reset_token();
								status = ConciseXMLStates2.EAT_WS;
								process_event();
							}
						}						
					}
					else
					{
						string s_message = string.Format("Not ConciseXML Encoding.");
            // Bug #8510 Mike O - Show no details when show_verbose_error is false
            if (c_CASL.get_show_verbose_error())
						throw CASL_error.create("title","Syntax Error", "code",107, "message",s_message, "line_number",_aXmlTextReader.LineNumber, "line_position",_aXmlTextReader.LinePosition);
            else
              throw CASL_error.create("title", "Error", "code", 107, "message", "");
					}
					break;
				}
				////////////////////////////////////////////////////////
				// EAT_WS
				////////////////////////////////////////////////////////
				case ConciseXMLStates2.EAT_WS: 
				{
					if (ConciseXMLCharSet2.IS_WS(current_event)) 
					{
						//do nothing
					}
					else if ( ConciseXMLCharSet2.IS_EQUAL(current_event)|| ConciseXMLCharSet2.IS_END_ELEMENT(current_event))  
					{
						status = ConciseXMLStates2.START_FIELD;
						process_event();
					}
					// Adding "IS_DOT" here is different from the ConciseXML parsing
					// Here there are no paths, so we don't care if there's a dot - 
					// it's just like any other character
					else if ( ConciseXMLCharSet2.IS_EOS(current_event) || 
							  ConciseXMLCharSet2.IS_ANY_CHAR(current_event) || 
							  ConciseXMLCharSet2.IS_START_ELEMENT(current_event) ||
							  ConciseXMLCharSet2.IS_DOT(current_event) )  
					{
					 if ( (TKS() is string) && TKS().ToString() == "UK")
						{
							tcs_set_unkey_and_value();
							status = ConciseXMLStates2.START_FIELD;
							process_event();
						}
						else
						{
							tcs_set_key_and_value();
							status = ConciseXMLStates2.START_FIELD;
							process_event();
						}
					}
					else
					{
						string s_message = string.Format("Not ConciseXML Encoding.");
						throw CASL_error.create("title","Syntax Error", "code",108, "message",s_message, "line_number",_aXmlTextReader.LineNumber, "line_position",_aXmlTextReader.LinePosition);
						//throw new XmlException("The Expression may not be compatiable to conciseXML encoding. ",null,_aXmlTextReader.LineNumber, _aXmlTextReader.LinePosition);
					}
					break;
				}		
				default: 
				{
					string s_message = string.Format("Not ConciseXML Encoding.");
					throw CASL_error.create("title","Syntax Error", "code",109, "message",s_message, "line_number",_aXmlTextReader.LineNumber, "line_position",_aXmlTextReader.LinePosition);
					//throw new XmlException("The Expression may not be compatiable to conciseXML encoding. ",null,_aXmlTextReader.LineNumber, _aXmlTextReader.LinePosition);
				}
			}
		}
	}
}
