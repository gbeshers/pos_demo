﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace CASL_engine
{
  class Reports
  {
    #region CSV formatting
    #region CSV formatting helper methods
    // Bug 13387 UMN
    /// <summary>
    /// Appends a single fieldname to the header row of the CSV output
    /// </summary>
    /// <param name="returnedCSV"></param>
    /// <param name="fieldNumber"></param>
    /// <param name="fieldLabels"></param>
    /// <param name="itemClass"></param>
    /// <param name="fieldOrdering"></param>
    public static void appendHeaderField(TextWriter returnedCSV, int fieldNumber,
      GenericObject fieldLabels, GenericObject itemClass, GenericObject fieldOrdering)
    {
      object fieldName = fieldOrdering.get(fieldNumber, "");
      String fieldHeader = fieldName.ToString().Trim();
      fieldHeader = Regex.Replace(fieldHeader, @"\,|\r\n?|\n", "");

      if (fieldLabels != null && fieldLabels.has(fieldName))
      {
        string fName = fieldLabels.get(fieldName, "").ToString().Trim();
        fName = Regex.Replace(fName, @"\,|\r\n?|\n", "");
        returnedCSV.Write((String.IsNullOrEmpty(fName)) ? fieldHeader : fName);
      }
      else if (itemClass != null)
      {
        string fName = misc.GetLabel(itemClass, fieldName).ToString().Trim();
        fName = Regex.Replace(fName, @"\,|\r\n?|\n", "");
        returnedCSV.Write((String.IsNullOrEmpty(fName)) ? fieldHeader : fName);
      }
      else
      {
        string fName = String.Format("Column {0}", fieldNumber);
        fName = Regex.Replace(fName, @"\,|\r\n?|\n", "");
        returnedCSV.Write((String.IsNullOrEmpty(fName)) ? fieldHeader : fName);
      }
    }

    /// <summary>
    /// Appends all the selected custom field labels or values to a header or a row
    /// </summary>
    /// <param name="returnedCSV"></param>
    /// <param name="customFields"></param>
    /// <param name="item"></param>
    /// <param name="fieldOrdering"></param>
    /// <param name="is_label"></param>
    public static void appendCustomFieldLabelsOrValues(TextWriter returnedCSV, ArrayList customFields,
      ArrayList otherCustomFields, GenericObject item, GenericObject fieldOrdering, bool is_label)
    {
      // remove any report standard fields that were in the fieldOrdering
      // because they have already been outputted to the CSV
      for (int i = 0; i < fieldOrdering.getLength(); i++)
      {
        string field = fieldOrdering.get(i).ToString();
        customFields.Remove(field);
      }

            // loop through the labels or the values of the customFields and output to CSV
            for (int i = 0; i < customFields.Count; i++)
            {
                writeLabelOrValue(returnedCSV, customFields[i], item, is_label);
            }

            // loop through the labels or the values of the otherCustomFields (used by tender) and output to CSV
            for (int i = 0; i < otherCustomFields.Count; i++)
            {
                writeLabelOrValue(returnedCSV, otherCustomFields[i], item, is_label);
            }

            // now handle subitems for Multiple Line items of GL MPT/UM bug 25045
            if (item.getLength() > 0)
            {
                for (int key = 0; key < item.getLength(); key++)
                {
                    GenericObject sItem = item.get(key) as GenericObject;

                    writeLabelOrValue(returnedCSV, "ITEMDESC", sItem, is_label);
                    if (sItem.has("ACCTID"))
                    {
                        writeLabelOrValue(returnedCSV, "ACCTID", sItem, is_label);
                    }
                    if (sItem.has("NAME"))
                    {
                        writeLabelOrValue(returnedCSV, "ACCTID", sItem, is_label);
                    }
                    writeLabelOrValue(returnedCSV, "GLACCTNBR", sItem, is_label);

                    string totalValue = null;
                    if (sItem.get("TOTAL", "").Equals(""))
                    {
                        totalValue = "";
                    }
                    else
                    {
                        totalValue = misc.format_dollar_amount("_subject", sItem.get("TOTAL", "0.0"));
                    }
                    if (is_label)
                    {
                        returnedCSV.Write(",");
                        returnedCSV.Write("TOTAL");
                    }
                    else
                    {
                        returnedCSV.Write(",");
                        returnedCSV.Write(totalValue);
                    }
                }
            }
        }

        //Refactoring UM/MPT
        private static void writeLabelOrValue(TextWriter returnedCSV, object field, GenericObject item, bool is_label)
        {
            string labelOrVal = item.get(field.ToString(), "") as string;
            if (is_label)
                labelOrVal = field.ToString();
            labelOrVal = Regex.Replace(labelOrVal, @"\,|\r\n?|\n", "");
            returnedCSV.Write(",");
            returnedCSV.Write(labelOrVal);
        }

        /// <summary>
        /// Appends the header row to the returned CSV.
        /// </summary>
        /// <param name="returnedCSV"></param>
        /// <param name="items"></param>
        /// <param name="fieldOrdering"></param>
        /// <param name="fieldLabels"></param>
        /// <param name="itemClass"></param>
        /// <param name="is_full"></param>
        public static void appendHeaderRow(TextWriter returnedCSV, GenericObject items, GenericObject fieldOrdering,
      GenericObject fieldLabels, GenericObject customFields, GenericObject otherCustomFields, GenericObject itemClass, bool is_full)
    {
      int i = 0;
      int len = fieldOrdering.getLength();

      // avoid trailing commas. Did it this way rather than a remove at the
      // end because of speed. The repeated code is ok-- loop unrolling optimization
      // techniques has shown that since the early days of computing...
      i = 0;
      if (fieldOrdering.getLength() > 0)
      {
        appendHeaderField(returnedCSV, i++, fieldLabels, itemClass, fieldOrdering);
      }
      while (i < fieldOrdering.getLength())
      {
        returnedCSV.Write(",");
        appendHeaderField(returnedCSV, i++, fieldLabels, itemClass, fieldOrdering);
      }

      // handle any custom fields in the reports
      if (items.getLength() > 0)
      {
        GenericObject item = items.get(0) as GenericObject;
        appendCustomFieldLabelsOrValues(returnedCSV, customFields.vectors, otherCustomFields.vectors, item, fieldOrdering, true);
      }
      returnedCSV.Write(Environment.NewLine);
    }

    /// <summary>
    /// Appends a single row to the CSV output
    /// </summary>
    /// <param name="returnedCSV"></param>
    /// <param name="item"></param>
    /// <param name="fieldOrdering"></param>
    /// <param name="is_full"></param>
    public static void appendRow(TextWriter returnedCSV, GenericObject item, GenericObject fieldOrdering,
      GenericObject customFields, GenericObject otherCustomFields, bool is_full)
    {
      int i = 0;
      object key;
      String field;
      int len = fieldOrdering.getLength();

      // avoid trailing commas. Did it this way rather than a remove at the
      // end because of speed. The repeated code is ok-- loop unrolling optimization
      // techniques has shown that since the early days of computing...
      if (len > 0)
      {
        key = fieldOrdering.get(i++);
        field = item.get(key, "").ToString();
        field = Regex.Replace(field, @"\,|\r\n?|\n", "");
        returnedCSV.Write(field);
      }
      while (i < len)
      {
        key = fieldOrdering.get(i++);
        returnedCSV.Write(",");
        field = item.get(key, "").ToString();
        field = Regex.Replace(field, @"\,|\r\n?|\n", "");
        returnedCSV.Write(field);
      }

      // handle any custom fields in the reports
      appendCustomFieldLabelsOrValues(returnedCSV, customFields.vectors, otherCustomFields.vectors, item, fieldOrdering, false);

      returnedCSV.Write(Environment.NewLine);
    }
    #endregion

    #region Export CSV Support
    /// <summary>
    /// Bug 13387 UMN One method that generates CSV for all standard summary and detail reports
    /// Will not handle management reports that are groups of other reports. Full reports will
    /// not display the custom fields (because they vary in number and kind per transaction or
    /// tender
    /// </summary>
    /// <param name="argPairs"></param>
    /// <returns></returns>
    public static object CASLGroupViewerApp_CSV_Top(params object[] argPairs)
    {
      StringWriter returnedCSV = new StringWriter();

      GenericObject args = misc.convert_args(argPairs);
      GenericObject groupViewerApp = args.get("_subject") as GenericObject;
      GenericObject group = groupViewerApp.get("group") as GenericObject;
      bool is_full = (bool)group.get("is_full", false, true);
      GenericObject fieldLabels = group.get("field_labels", null, true) as GenericObject;
      GenericObject itemClass = group.get("item_class", null, true) as GenericObject;
      GenericObject fieldOrdering, item;
      GenericObject items = group.get("items", null, true) as GenericObject;
      GenericObject customFields = group.get("custom_fields", null, true) as GenericObject;
      GenericObject otherCustomFields = group.get("other_custom_fields", null, true) as GenericObject;

      int i = 0;

      fieldOrdering = GetFieldOrdering(group);
      if (fieldOrdering == null)
      {
        return misc.MakeCASLError("CSV Generation Error", "200", "Could not find field ordering for the report.", true);
      }

      appendHeaderRow(returnedCSV, items, fieldOrdering, fieldLabels, customFields, otherCustomFields, itemClass, is_full);

      // loop over items
      for (i = 0; i < items.getLength(); i++)
      {
        item = items.get(i) as GenericObject;
        appendRow(returnedCSV, item, fieldOrdering, customFields, otherCustomFields, is_full);
      }
      return returnedCSV.ToString();
    }
    #endregion

    #region Email CSV Support

    // Bug 22627 UMN add support for Amazon SES email
    // No need to make port configurable, as best practice is to use 587, especially with Amazon
    const int SMTP_PORT = 587;

    /// <summary>
    /// Bug 13387 UMN One method that emails a CSV for all standard summary and detail reports
    /// Will not handle management reports that are groups of other reports. Full reports will
    /// not display the custom fields (because they vary in number and kind per transaction or
    /// tender. Email addresses are checked to be in the same domain as configured.
    /// </summary>
    /// <param name="argPairs"></param>
    /// <returns> true if emailed file, false or CASLError </returns>
    public static object CASLGroupViewerApp_CSV_Email(params object[] argPairs)
    {
      // First verify that the email addresses match the CSV domain restrictions
      // configured in General->Misc.
      GenericObject miscData = c_CASL.c_object("Business.Misc.data") as GenericObject;
      string csvDomainList = miscData.get("CSV_email_domain_list", "").ToString();
      GenericObject args = misc.convert_args(argPairs);
      string someRecipientFailure = "";
      string toAddresses = args.get("email_addresses").ToString();
      //Bug 26910 NAM:
      string smtpProvider = miscData.get("smtp_provider", "CORE").ToString();
      int smtp_port = SMTP_PORT;
      string sPort = miscData.get("port", "") as string;
      if (!string.IsNullOrEmpty(sPort))
        smtp_port = System.Convert.ToInt32(sPort);
      string smtpHost = miscData.get("smtp_host", "mail.corebt.com").ToString();
      string emailAddrErrs = "";

      toAddresses = FilterInvalidtoAddresses(csvDomainList, toAddresses, ref emailAddrErrs);

      // log any invalid email addresses
      if (!String.IsNullOrEmpty(emailAddrErrs))
      {
        // Bug 16572 SM - Changed error message as specified in the TTS.
        emailAddrErrs = String.Format("Email domain not found in approved domain check list:{0}", emailAddrErrs);
        Logger.cs_log("CSV Generation Error: " + emailAddrErrs);

        // Error out if invalid addresses leave no valid email addresses to send to
        if (String.IsNullOrEmpty(toAddresses))
        {
          return misc.MakeCASLError("CSV Generation Error", "200", emailAddrErrs, true);
        }
      }

      // Error out if no email addresses to send to
      if (String.IsNullOrEmpty(toAddresses))
      {
        return misc.MakeCASLError("CSV Generation Error", "200", "No valid email address for sending", true);
      }

      // get a unique temp file name with extension CSV
      string fileName = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + ".csv";
      TextWriter returnedCSV = TextWriter.Synchronized(File.AppendText(fileName));

      GenericObject groupViewerApp = args.get("_subject") as GenericObject;
      GenericObject group = groupViewerApp.get("group") as GenericObject;

      #region Output CSV
      // use try/finally so can close the TextWriter on an exception
      try
      {
        bool is_full = (bool)group.get("is_full", false, true);

        // get report parameters
        GenericObject fieldLabels = group.get("field_labels", null, true) as GenericObject;
        GenericObject itemClass = group.get("item_class", null, true) as GenericObject;
        GenericObject fieldOrdering, item;
        GenericObject items = group.get("items", null, true) as GenericObject;
        GenericObject customFields = group.get("custom_fields", null, true) as GenericObject;
        GenericObject otherCustomFields = group.get("other_custom_fields", null, true) as GenericObject;
        int i = 0;

        fieldOrdering = GetFieldOrdering(group);
        if (fieldOrdering == null)
        {
          return misc.MakeCASLError("CSV Generation Error", "200", "Could not find field ordering for the report.", true);
        }

        appendHeaderRow(returnedCSV, items, fieldOrdering, fieldLabels, customFields, otherCustomFields, itemClass, is_full);

        // loop over items
        for (i = 0; i < items.getLength(); i++)
        {
          item = items.get(i) as GenericObject;
          appendRow(returnedCSV, item, fieldOrdering, customFields, otherCustomFields, is_full);
        }

        // Close the TextWriter
        returnedCSV.Close();
        returnedCSV = null;
      }
      finally
      {
        // close the TextWriter if it hasn't already been closed
        if (returnedCSV != null)
          returnedCSV.Close();
      }
      #endregion

      #region Email CSV
      //Bug 26910 NAM: if sender email not in General Info/Misc then inherit from Config.casl
      string email_from = (string)miscData.get("ipayment_sender_email", "");
      if (string.IsNullOrEmpty(email_from))
        email_from = c_CASL.GEO.get("ipayment_sender_email", "") as string;

      // Bug 24036 UMN when using Amazon, the sender domain is ipayment.com.
      if (smtpHost.Contains("amazon"))
      {
        email_from = email_from.Replace("corebt.com", "ipayment.com");
      }

      string title = group.get("title", "", true) as string;
      string date = group.get("report_date", DateTime.Now.ToString("MMddyyHHmmss"), true) as string;

      SmtpClient smtpClient = new SmtpClient(smtpHost, smtp_port); // Bug 26910 NAM/Bug 22627 UMN add support for Amazon SES email

      // Bug 22627 UMN add support for Amazon SES email
      emails.AddCredentials(smtpProvider, smtpHost, email_from, "", smtpClient);    //Bug 26910 NAM:

      MailAddress from = new MailAddress(email_from);
      MailAddress to = new MailAddress("foo@bar.com");
      MailMessage message = new MailMessage(from, to);

      try
      {
        // Bug 22627 UMN add support for Amazon SES email
        emails.AddCredentials(smtpProvider, smtpHost, email_from, "", smtpClient);    //Bug 26910 NAM:

        // code around stupid MS bug-- it doesn't parse comma delimited email 
        // addresses even though doc says it should. Note: message.To is a MailAddressCollection.
        message.To.Clear(); // clear out the foo@bar.com
        message.To.Add(toAddresses);

        Attachment file = new Attachment(fileName, "text/csv");
        message.Subject = title + " Report " + date;
        message.Body = "Your requested CSV report from iPayment Enterprise is attached.";
        message.Attachments.Add(file);

        try
        {
          smtpClient.Send(message);
          string log_message = "Sent email to ("
            + message.To.ToString()
            + ") from ("
            + message.From.ToString()
            + ") with subject ("
            + message.Subject.ToString()
            + ")";
          Logger.LogInfo(log_message, "SEND EMAIL SUCCESSFUL");
        }
        catch (SmtpFailedRecipientException se)
        {
          someRecipientFailure = String.Format("Email to {0} failed due to: {1}",
            Regex.Replace(se.FailedRecipient, @"<|>", ""), se.Message);
          Logger.LogError(someRecipientFailure, "SEND EMAIL PARTIALLY SUCCESSFUL");
          // bug 17849
          Logger.cs_log_trace("Error sending email", se);
        }
      }
      catch (Exception e)
      {
        //Bug 26910 NAM: check if InnerException has any content first
        if (e.InnerException != null)
        {
          Exception ex = e.InnerException;
          string msg = "CSV Email Send Error (Exception thrown): " + ex.Message;
          Logger.cs_log(msg + Environment.NewLine + ex.StackTrace);
        }
        Logger.LogError(e.Message, "SEND EMAIL FAILED");

        // bug 17849
        Logger.cs_log_trace("Error sending email", e);
        return misc.MakeCASLError("CSV Email Send Error", "200", e.Message, true);

      }
      finally
      {
        try
        {
          // need to call Dispose on attachments before deleting the CSV file
          message.Attachments.Dispose();
          File.Delete(fileName);
          Logger.cs_log("SUCCESS: Deleted CSV attachment '" + fileName + "'.");
        }
        catch
        {
          Logger.cs_log("Error: Unable to delete CSV attachment '" + fileName + "'.");
        }
        message.Dispose();
      }
      #endregion

      // First must correct syntax errs or bad email domain sends b4 it will display
      // any recipient failures
      if (!String.IsNullOrEmpty(emailAddrErrs))
      {
        if (String.IsNullOrEmpty(someRecipientFailure))
          return misc.MakeCASLError("CSV Email Send Error", "200", emailAddrErrs, true);
        else
          return misc.MakeCASLError("CSV Email Send Error", "200", emailAddrErrs + ". Also: " + someRecipientFailure, true);
      }
      return true;
    }

    /// <summary>
    /// Takes a comma-delimited string of domains, and a comma-delimited string of email
    /// addresses and returns a string of email addresses with the invalid ones purged.
    /// </summary>
    /// <param name="domainList"></param>
    /// <param name="toAddresses"></param>
    /// <returns></returns>
    public static string FilterInvalidtoAddresses(string domainString, string emailAddressString, ref string emailAddrErrs)
    {
      string retAddresses = "";
      Regex re = new Regex(@"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$",
                RegexOptions.IgnoreCase);

      // Just return if user hasn't typed an email address. An error will be generated
      // by the caller
      if (String.IsNullOrEmpty(emailAddressString))
        return retAddresses;

      // hashes might be marginally quicker but for small lists their memory usage
      // is too much.
      // Fix for Bug 13387, Comment 12. Issue was that if domainString was "sw.org, corebt.com"
      // the trim and split resulted in: "sw.org", " corebt.com" and it wasn't matching the domains
      // in the email list. Use a little LINQ to fix that.
      List<String> domainList = domainString.Split(',').Select(d => d.Trim()).ToList();
      // Fix for Bug 25324 SM - Added suport for semi colon and space seperator for email addresses.   
      List<String> emailList = emailAddressString.Split(',', ' ', ';').ToList();
      // Don't error out if any addresses are invalid. Just remove and soldier on
      foreach (string emailAddr in emailList)
      {
        if (String.IsNullOrEmpty(emailAddr)) continue;

        // validate the form of the email address (not if it actually exists)
        if (!re.IsMatch(emailAddr))
        {
          emailAddrErrs += " " + emailAddr;
          continue;
        }

        string[] emailDomain = emailAddr.Trim().Split('@'); // 1st element should have the domain
        if (emailDomain.Count() > 1 && domainList.Contains(emailDomain[1]))
        {
          retAddresses += emailAddr + ',';
        }
        else
        {
          emailAddrErrs += " " + emailAddr;
        }
      }
      if (!String.IsNullOrEmpty(retAddresses))
        retAddresses = retAddresses.TrimEnd(',');

      return retAddresses;
    }
    #endregion
    #endregion

    #region HTM formatting
    public static object CASLGroupLevel_Htm_RowItem(params object[] argPairs)
    {
      GenericObject args = misc.convert_args(argPairs);
      GenericObject subject = args.get("_subject") as GenericObject;
      GenericObject item = args.get("item") as GenericObject;
      GenericObject group = args.get("a_group") as GenericObject;

      return GroupLevel_Htm_RowItem(subject, item, group);
    }

    public static object GroupLevel_Htm_RowItem(GenericObject subject, GenericObject item, GenericObject group)
    {
      #region Get Field Ordering
      GenericObject fieldOrdering = GetFieldOrdering(group);
      if (fieldOrdering == null)
      {
        return misc.MakeCASLError("Report Generation Error", "200", "Could not find field ordering for the report.", true);
      }
      #endregion

      bool groupFull = !(group.get("is_full", false, true).Equals(false));
      GenericObject groupItemClass = group.get("item_class", null, true) as GenericObject;
      GenericObject groupCustomFields = group.get("custom_fields", null, true) as GenericObject;
      GenericObject groupOtherCustomFields = group.get("other_custom_fields", null, true) as GenericObject;

      #region customPart
      GenericObject detailInfoRecords = c_CASL.c_GEO();
      foreach (string key in item.getStringKeys())
      {
        if ((groupFull && groupItemClass != null && !groupItemClass.has(key)) ||
          (groupCustomFields != null && groupCustomFields.vectors.Contains(key)) ||
                   (groupOtherCustomFields != null && groupOtherCustomFields.vectors.Contains(key)))//bug 9389 SN
        {
          detailInfoRecords.set(key, item.get(key));
        }
      }

      int numberPerRow = 3, numberOfRecords = 0;

      GenericObject detailInfoTable = c_CASL.c_make("TABLE") as GenericObject;
      GenericObject currentRow = c_CASL.c_make("TR") as GenericObject;
      GenericObject itemCustomFields = item.get("custom_fields", null, true) as GenericObject;

      if (item.has("custom_fields"))
      {
        if (numberOfRecords % numberPerRow == 0)
        {
          currentRow = c_CASL.c_make("TR") as GenericObject;
          detailInfoTable.insert(currentRow);
        }

        if (itemCustomFields != null)
        {
          for (int i = 0; i < itemCustomFields.getLength(); i++)
          {
            object value = itemCustomFields.get(i);
            string label = misc.GetLabel(item, value);
            int length = Math.Min(15, label.Length);
            string detailLabel = label.Substring(0, length) + ":";

            currentRow.insert(c_CASL.c_make("TD", "class", "report_detail_key", 0, detailLabel));
            currentRow.insert(c_CASL.c_make("TD", "class", "report_detail_value", 0, item.get(value)));

            if (i % 2 != 0)
            {
              currentRow.insert(c_CASL.c_make("TR"));
            }

            numberOfRecords++;
          }
        }
      }

      #endregion;

      //bug 9389 SN start
      #region otherCustomPart
      GenericObject itemOtherCustomFields = item.get("other_custom_fields", null, true) as GenericObject;
      if (item.has("other_custom_fields"))
      {
        if (numberOfRecords % numberPerRow == 0)
        {
          currentRow = c_CASL.c_make("TR") as GenericObject;
          detailInfoTable.insert(currentRow);
        }

        if (itemOtherCustomFields != null)
        {
          for (int i = 0; i < itemOtherCustomFields.getLength(); i++)
          {
            object value = itemOtherCustomFields.get(i);
            string label = misc.GetLabel(item, value);
            int length = Math.Min(15, label.Length);
            string detailLabel = label.Substring(0, length) + ":";

            currentRow.insert(c_CASL.c_make("TD", "class", "report_detail_key", 0, detailLabel));
            currentRow.insert(c_CASL.c_make("TD", "class", "report_detail_value", 0, item.get(value)));

            if (i % 2 != 0)
            {
              currentRow.insert(c_CASL.c_make("TR"));
            }

            numberOfRecords++;
          }
        }
      }

      numberOfRecords = 0;
      foreach (string key in detailInfoRecords.getStringKeys())
      {
        if (numberOfRecords % numberPerRow == 0)
        {
          currentRow = c_CASL.c_make("TR") as GenericObject;
          detailInfoTable.insert(currentRow);
        }

        string label = misc.GetLabel(item, key);
        int length = Math.Min(15, label.Length);
        string detailLabel = label.Substring(0, length) + ":";
        currentRow.insert(c_CASL.c_make("TD", "class", "report_detail_key", 0, detailLabel));
        currentRow.insert(c_CASL.c_make("TD", "class", "report_detail_value", 0, detailInfoRecords.get(key)));

        numberOfRecords++;
      }
      GenericObject customPart = detailInfoTable;
      //GenericObject othercustomPart = detailInfoTable;
      #endregion;
      //bug 9389 SN end
      #region SubItems
      string itemCSSClass = null;
      if (item.get("VOIDDT", "", false).Equals("")) { itemCSSClass = "item"; }
      else { itemCSSClass = "item void"; }

      GenericObject subItems = null;
      if (item.getLength() > 0)
      {
        subItems = c_CASL.c_make("vector") as GenericObject;
        for (int key = 0; key < item.getLength(); key++)
        {
          GenericObject sItem = item.get(key) as GenericObject;
          GenericObject row = c_CASL.c_make("TR") as GenericObject;
          subItems.insert(row);

          row.set("class", "item");

          if (sItem.has("NAME") && sItem.has("ACCTID")) { row.insert(c_CASL.c_make("TD", "colspan", 5)); }
          else { row.insert(c_CASL.c_make("TD", "colspan", 7)); } //bug 24688 MPT Multiple allocation template groups cause GL to be shown in the description change from 3 to 7 for colspan

          row.insert(c_CASL.c_make("TD",
  "class", misc.GetCSSClass(groupItemClass, "ITEMDESC"),
  0, sItem.get("ITEMDESC", "")));
          row.insert(c_CASL.c_make("TD"));

          if (sItem.has("ACCTID"))
          {
            row.insert(c_CASL.c_make("TD",
              "class", misc.GetCSSClass(groupItemClass, "ACCTID"),
              0, sItem.get("ACCTID", "xxxxx")));
          }
          if (sItem.has("NAME"))
          {
            row.insert(c_CASL.c_make("TD",
              "class", misc.GetCSSClass(groupItemClass, "NAME"),
              0, sItem.get("NAME", "")));
          }

          row.insert(c_CASL.c_make("TD",
            "class", misc.GetCSSClass(groupItemClass, "GLACCTNBR"),
            0, sItem.get("GLACCTNBR", "")));

          string totalValue = null;
          if (sItem.get("TOTAL", "").Equals("")) { totalValue = ""; }
          else { totalValue = misc.format_dollar_amount("_subject", sItem.get("TOTAL", "0.0")); }
          row.insert(c_CASL.c_make("TD",
            "class", misc.GetCSSClass(groupItemClass, "TOTAL"),
            0, totalValue));

          row.insert(c_CASL.c_make("TD"));
        }
      }
      #endregion

      #region Return Vector
      GenericObject returnVector = c_CASL.c_make("vector") as GenericObject;
      GenericObject returnRow = c_CASL.c_make("TR") as GenericObject;
      returnVector.insert(returnRow);
      returnRow.set("class", itemCSSClass);
      foreach (object key in fieldOrdering.vectors)
      {
        object value = item.get(key, false, true);
        string sValue = value.ToString();
        if (key.Equals("TOTAL") ||
          key.Equals("AMOUNT") ||
          key.Equals("amount") ||
          key.Equals("BANK_TOTAL") ||
          key.Equals("POST_TOTAL") ||
          key.Equals("ACC_TOTAL") || // Bug #10646 Mike O
          key.Equals("OVER_SHORT") ||
          key.Equals("trans_amount"))
        {
          if (value.Equals("")) { sValue = ""; }
          else { sValue = misc.format_dollar_amount("_subject", value); }
        }
        else if (key.Equals("BANKID"))
        {
          object bName = null;
          object systemInterface = c_CASL.c_object("System_interface.of.34");
          if (!systemInterface.Equals(false))
          {
            bName = misc.CASL_call("a_method", "get_bank_name",
                "args", new GenericObject("_subject", systemInterface, 0, value));
            if (!bName.Equals(false)) { sValue = String.Format("{0} ({1})", bName, value); }
          }
        }
        else if (key.Equals("OPEN_USERID"))
        {
          object uName = null;
          object systemInterface = c_CASL.c_object("System_interface.of.34");
          if (!systemInterface.Equals(false))
          {
            uName = misc.CASL_call("a_method", "get_user_name",
              "args", new GenericObject("_subject", systemInterface, 0, value));
            if (!uName.Equals(false)) { sValue = String.Format("{0} ({1})", uName, value); }
          }
        }

        returnRow.insert(c_CASL.c_make("TD",
          "class", misc.GetCSSClass(groupItemClass, key),
          0, sValue));
      }
      if (subItems != null) { returnVector.insert(subItems); }
      if (!customPart.Equals(""))
      {
        returnVector.insert(c_CASL.c_make("TR",
          "class", "item",
          0, c_CASL.c_make("TD",
            "colspan", fieldOrdering.getLength(),
            0, customPart)));
      }
      #endregion
      return returnVector;
    }

    public static object CASLGroupLevel_Htm_RowItemHeading(params object[] argPairs)
    {
      GenericObject args = misc.convert_args(argPairs);
      GenericObject group = args.get("a_group") as GenericObject;
      return GroupLevel_Htm_RowItemHeading(group);
    }

    public static object GroupLevel_Htm_RowItemHeading(GenericObject group)
    {
      GenericObject fieldOrdering = GetFieldOrdering(group);

      GenericObject row = c_CASL.c_make("TR", "class", "item") as GenericObject;
      GenericObject itemClass = group.get("item_class", null, true) as GenericObject;
      if (itemClass == null) return row;
      GenericObject fieldLabels = group.get("field_labels", null, true) as GenericObject;

      object key;
      GenericObject header;
      for (int i = 0; i < fieldOrdering.getLength(); i++)
      {
        key = fieldOrdering.get(i);
        header = c_CASL.c_make("TH", "class", key) as GenericObject;
        if (key.Equals("count"))
        {
          header.insert("&nbsp;");
        }
        else if (fieldLabels != null && fieldLabels.has(key))
        {
          header.insert(fieldLabels.get(key));
        }
        else
        {
          header.insert(misc.GetLabel(itemClass, key));
        }
        row.insert(header);
      }
      return row;
    }

    public static object CASLGroupLevel_Htm_GroupLevelHeading(params object[] argPairs)
    {
      GenericObject args = misc.convert_args(argPairs);
      GenericObject subject = args.get("_subject", null) as GenericObject;
      GenericObject group = args.get("a_group", null) as GenericObject;
      return GroupLevel_Htm_GroupLevelHeading(subject, group);
    }

    public static object GroupLevel_Htm_GroupLevelHeading(GenericObject subject, GenericObject group)
    {
      GenericObject fieldOrdering = GetFieldOrdering(group);

      GenericObject row = c_CASL.c_make("TR", "class", "group_level") as GenericObject;
      GenericObject itemClass = group.get("item_class", null, true) as GenericObject;
      if (itemClass == null) return row;
      GenericObject summary = subject.get("_summary", null, true) as GenericObject;
      if (summary == null) return row;
      GenericObject fieldLabels = group.get("field_labels", null, true) as GenericObject;
      if (fieldLabels == null) return row;

      object key;
      GenericObject header;
      for (int i = 0; i < fieldOrdering.getLength(); i++)
      {
        key = fieldOrdering.get(i);
        if (!itemClass.has(key)) continue;
        if (summary.has(key))
        {
          header = c_CASL.c_make("TH", "class", key) as GenericObject;
          header.insert("Total");
          header.insert(c_CASL.c_make("BR"));

          if (fieldLabels.has(key)) { header.insert(fieldLabels.get(key)); }
          else { header.insert(misc.GetLabel(itemClass, key)); }

          row.insert(header);
        }
        else
        {
          row.insert(c_CASL.c_make("TH"));
        }
      }
      return row;
    }

    public static object CASLGroupLevel_Htm_Inst(params object[] argPairs)
    {
      GenericObject args = misc.convert_args(argPairs);
      GenericObject subject = args.get("_subject", null) as GenericObject;
      GenericObject group = args.get("a_group", null) as GenericObject;
      return GroupLevel_Htm_Inst(subject, group);
    }

    public static object GroupLevel_Htm_Inst(GenericObject subject, GenericObject group)
    {
      GenericObject returnVector = c_CASL.c_make("vector") as GenericObject;

      returnVector.insert(GroupLevel_Htm_RowGroupLevel(subject, group));
      ArrayList keys = subject.getStringKeys();
      keys.Sort();
      GenericObject value;
      foreach (object key in keys)
      {
        value = subject.get(key) as GenericObject;
        returnVector.insert(GroupLevel_Htm_Inst(value, group));
      }
      if (subject.has("_items"))
      {
        returnVector.insert(GroupLevel_Htm_RowItemHeading(group));
        GenericObject items = subject.get("_items") as GenericObject;
        GenericObject expressions = c_CASL.c_GEO();
        GenericObject criteria = group.get("report_criteria", null, true) as GenericObject;
        GenericObject groupBy = criteria.get("group_by", null, true) as GenericObject;
        #region Sort _items
        // Bug #13772 Mike O - Handle possible null values in SOURCE_GROUP
        if (group.get("report_kind", null, true).Equals("summary"))
        {
          if (groupBy == c_CASL.c_object("Group_by.of.core_file"))
          {
            expressions.insert(misc.MakeExpression(".<get> \"file_nbr\"     if_missing = \"\" </get>"));
            expressions.insert(misc.MakeExpression("String.<from> .<get> \"SOURCE_GROUP\" if_missing = \"\" </get> </from>.<format_source_group_string/>"));
            expressions.insert(misc.MakeExpression(".<get> \"EVENTNBR\"     if_missing = \"\" </get>.<pad> a_char = \"0\" align = \"right\" length = 3</pad>"));
            expressions.insert(misc.MakeExpression(".<get> \"TRANNBR\"      if_missing = \"\" </get>.<pad> a_char = \"0\" align = \"right\" length = 3</pad>"));
            expressions.insert(misc.MakeExpression(".<get> \"GLACCTNBR\"    if_missing = \"\" </get>"));
            expressions.insert(misc.MakeExpression(".<get> \"TTDESC\"       if_missing = \"\" </get>"));
            expressions.insert(misc.MakeExpression(".<get> \"TNDRDESC\"     if_missing = \"\" </get>"));
            expressions.insert(misc.MakeExpression(".<get> \"OPEN_USERID\"  if_missing = \"\" </get>.<pad> a_char = \"0\" align = \"right\" length = 3</pad>"));
          }
          else if (groupBy == c_CASL.c_object("Group_by.of.transaction_type") ||
            groupBy == c_CASL.c_object("Group_by.of.core_file_transaction_type"))
          {
            expressions.insert(misc.MakeExpression(".<get> \"TTDESC\"       if_missing = \"\" </get>"));
            expressions.insert(misc.MakeExpression(".<get> \"file_nbr\"     if_missing = \"\" </get>"));
            expressions.insert(misc.MakeExpression("String.<from> .<get> \"SOURCE_GROUP\" if_missing = \"\" </get> </from>.<format_source_group_string/>"));
            expressions.insert(misc.MakeExpression(".<get> \"EVENTNBR\"     if_missing = \"\" </get>.<pad> a_char = \"0\" align = \"right\" length = 3</pad>"));
            expressions.insert(misc.MakeExpression(".<get> \"TRANNBR\"      if_missing = \"\" </get>.<pad> a_char = \"0\" align = \"right\" length = 3</pad>"));
            expressions.insert(misc.MakeExpression(".<get> \"OPEN_USERID\"  if_missing = \"\" </get>.<pad> a_char = \"0\" align = \"right\" length = 3</pad>"));
          }
          else if (groupBy == c_CASL.c_object("Group_by.of.financial_account") ||
            groupBy == c_CASL.c_object("Group_by.of.core_file_financial_account"))
          {
            expressions.insert(misc.MakeExpression(".<get> \"GLACCTNBR\"    if_missing = \"\" </get>"));
            expressions.insert(misc.MakeExpression(".<get> \"file_nbr\"     if_missing = \"\" </get>"));
            expressions.insert(misc.MakeExpression("String.<from> .<get> \"SOURCE_GROUP\" if_missing = \"\" </get> </from>.<format_source_group_string/>"));
            expressions.insert(misc.MakeExpression(".<get> \"EVENTNBR\"     if_missing = \"\" </get>.<pad> a_char = \"0\" align = \"right\" length = 3</pad>"));
            expressions.insert(misc.MakeExpression(".<get> \"TRANNBR\"      if_missing = \"\" </get>.<pad> a_char = \"0\" align = \"right\" length = 3</pad>"));
            expressions.insert(misc.MakeExpression(".<get> \"OPEN_USERID\"  if_missing = \"\" </get>.<pad> a_char = \"0\" align = \"right\" length = 3</pad>"));
          }
          else if (groupBy == c_CASL.c_object("Group_by.of.tender_type") ||
            groupBy == c_CASL.c_object("Group_by.of.core_file_tender_type") ||
            groupBy == c_CASL.c_object("Group_by.of.tender_type_core_file"))
          {
            expressions.insert(misc.MakeExpression(".<get> \"TNDRDESC\"     if_missing = \"\" </get>"));
            expressions.insert(misc.MakeExpression(".<get> \"file_nbr\"     if_missing = \"\" </get>"));
            expressions.insert(misc.MakeExpression("String.<from> .<get> \"SOURCE_GROUP\" if_missing = \"\" </get> </from>.<format_source_group_string/>"));
            expressions.insert(misc.MakeExpression(".<get> \"EVENTNBR\"     if_missing = \"\" </get>.<pad> a_char = \"0\" align = \"right\" length = 3</pad>"));
            expressions.insert(misc.MakeExpression(".<get> \"TRANNBR\"      if_missing = \"\" </get>.<pad> a_char = \"0\" align = \"right\" length = 3</pad>"));
            expressions.insert(misc.MakeExpression(".<get> \"OPEN_USERID\"  if_missing = \"\" </get>.<pad> a_char = \"0\" align = \"right\" length = 3</pad>"));
          }
          //bug 9256(Grouphealth) Bug 9422(Base) SN start
          else if (groupBy == c_CASL.c_object("Group_by.of.settlementkey_group"))
          {
            expressions.insert(misc.MakeExpression(".<get> \"DEPTID\"     if_missing = \"\" </get>"));
            expressions.insert(misc.MakeExpression(".<get> \"TNDRDESC\"     if_missing = \"\" </get>"));
            expressions.insert(misc.MakeExpression(".<get> \"file_nbr\"     if_missing = \"\" </get>"));
            expressions.insert(misc.MakeExpression("String.<from> .<get> \"SOURCE_GROUP\" if_missing = \"\" </get> </from>.<format_source_group_string/>"));
            expressions.insert(misc.MakeExpression(".<get> \"EVENTNBR\"     if_missing = \"\" </get>.<pad> a_char = \"0\" align = \"right\" length = 3</pad>"));
            expressions.insert(misc.MakeExpression(".<get> \"TRANNBR\"      if_missing = \"\" </get>.<pad> a_char = \"0\" align = \"right\" length = 3</pad>"));
            expressions.insert(misc.MakeExpression(".<get> \"OPEN_USERID\"  if_missing = \"\" </get>.<pad> a_char = \"0\" align = \"right\" length = 3</pad>"));
          }
          //end bug 9256/Bug 9422
          // Do nothing for user_transaction_type
          else if (groupBy != c_CASL.c_object("Group_by.of.user_transaction_type"))
          {
            expressions.insert(misc.MakeExpression(".<get> \"file_nbr\"     if_missing = \"\" </get>"));
            expressions.insert(misc.MakeExpression("String.<from> .<get> \"SOURCE_GROUP\" if_missing = \"\" </get> </from>.<format_source_group_string/>"));
            expressions.insert(misc.MakeExpression(".<get> \"EVENTNBR\"     if_missing = \"\" </get>.<pad> a_char = \"0\" align = \"right\" length = 3</pad>"));
            expressions.insert(misc.MakeExpression(".<get> \"TRANNBR\"      if_missing = \"\" </get>.<pad> a_char = \"0\" align = \"right\" length = 3</pad>"));
            expressions.insert(misc.MakeExpression(".<get> \"OPEN_USERID\"  if_missing = \"\" </get>.<pad> a_char = \"0\" align = \"right\" length = 3</pad>"));
          }
        }
        else if (groupBy == c_CASL.c_object("Group_by.of.core_file"))
        {
          expressions.insert(misc.MakeExpression(".<get> \"file_nbr\"     if_missing = \"\" </get>"));
          expressions.insert(misc.MakeExpression("String.<from> .<get> \"SOURCE_GROUP\" if_missing = \"\" </get> </from>.<format_source_group_string/>"));
          expressions.insert(misc.MakeExpression(".<get> \"EVENTNBR\"     if_missing = \"\" </get>.<pad> a_char = \"0\" align = \"right\" length = 3</pad>"));
          expressions.insert(misc.MakeExpression(".<get> \"TRANNBR\"      if_missing = \"\" </get>.<pad> a_char = \"0\" align = \"right\" length = 3</pad>"));
          expressions.insert(misc.MakeExpression(".<get> \"RECEIPT_REF\"  if_missing = \"\" </get>"));
          expressions.insert(misc.MakeExpression(".<get> \"GLACCTNBR\"    if_missing = \"\" </get>"));
          expressions.insert(misc.MakeExpression(".<get> \"TTDESC\"       if_missing = \"\" </get>"));
          expressions.insert(misc.MakeExpression(".<get> \"TNDRDESC\"     if_missing = \"\" </get>"));
        }

        else
        {
          expressions.insert(misc.MakeExpression(".<get> \"file_nbr\"     if_missing = \"\" </get>"));
          expressions.insert(misc.MakeExpression("String.<from> .<get> \"SOURCE_GROUP\" if_missing = \"\" </get> </from>.<format_source_group_string/>"));
          expressions.insert(misc.MakeExpression(".<get> \"EVENTNBR\"     if_missing = \"\" </get>.<pad> a_char = \"0\" align = \"right\" length = 3</pad>"));
          expressions.insert(misc.MakeExpression(".<get> \"TRANNBR\"      if_missing = \"\" </get>.<pad> a_char = \"0\" align = \"right\" length = 3</pad>"));
        }
        // End Bug #13772 Mike O
        if (expressions.getLength() != 0) { misc.SortOn(items, expressions); }
        #endregion
        GenericObject valueGeo;
        for (int i = 0; i < items.getLength(); i++)
        {
          valueGeo = items.get(i) as GenericObject;
          returnVector.insert(GroupLevel_Htm_RowItem(subject, valueGeo, group));
        }
      }

      return returnVector;
    }

    public static object CASLGroupLevel_Htm_RowGroupLevel(params object[] argPairs)
    {
      GenericObject args = misc.convert_args(argPairs);
      GenericObject subject = args.get("_subject") as GenericObject;
      GenericObject group = args.get("a_group") as GenericObject;
      return GroupLevel_Htm_RowGroupLevel(subject, group);
    }

    private static object GroupLevel_Htm_RowGroupLevel(GenericObject subject, GenericObject group)
    {
      GenericObject fieldOrdering = GetFieldOrdering(group);
      GenericObject fieldsToSum = group.get("fields_to_sum", null, true) as GenericObject;
      if (fieldsToSum == null) { return null; }
      string firstSumKey = fieldsToSum.get(0, "") as string;
      int firstSumKeyIndex = -1;
      object keyOf = fieldOrdering.key_of(firstSumKey);
      if (firstSumKey.Length == 0) { firstSumKeyIndex = fieldOrdering.getLength(); }
      else if (keyOf is int) { firstSumKeyIndex = (int)keyOf; }

      GenericObject returnVector = null;
      if (firstSumKeyIndex != -1)
      {
        returnVector = c_CASL.c_make("vector") as GenericObject;
        GenericObject row, cell;
        GenericObject summary = subject.get("_summary", null, true) as GenericObject;
        GenericObject itemClass = group.get("item_class", null, true) as GenericObject;
        object depth = subject.get("depth", null, true);

        // Only Depth 0 has a header
        if (depth.Equals(0)) { returnVector.insert(GroupLevel_Htm_GroupLevelHeading(subject, group)); }

        row = c_CASL.c_make("TR", "class", String.Format("above_group_level_{0}", depth)) as GenericObject;
        row.insert(c_CASL.c_make("TD", 0, "&nbsp;"));
        returnVector.insert(row);

        row = c_CASL.c_make("TR", "class", String.Format("group_level_{0}", depth)) as GenericObject;
        cell = c_CASL.c_make("TD", "class", "group_name", "colspan", firstSumKeyIndex) as GenericObject;
        // All other depths have a name
        if (!depth.Equals(0))
        {
          object name = subject.get("_name", null, true);
          object si = c_CASL.c_object("System_interface.of");
          if ((si as GenericObject).has("34")) { si = (si as GenericObject).get("34"); }
          else { si = false; }
          if (!si.Equals(false))
          {
            if (misc.base_is_a(subject, c_CASL.c_object("group_level.user")))
            {
              object newName = misc.CASL_call("a_method", "get_user_name",
                "args", new GenericObject(
                  "_subject", si,
                  "user_id", name));
              if (!newName.Equals(false)) { name = String.Format("{0} - {1}", name, newName); }
            }
            else if (misc.base_is_a(subject, c_CASL.c_object("group_level.bank_id")))
            {
              object newName = misc.CASL_call("a_method", "get_bank_name",
                "args", new GenericObject(
                  "_subject", si,
                  "bank_id", name));
              if (!newName.Equals(false)) { name = String.Format("{0} - {1}", name, newName); }
            }
          }
          cell.insert(name);
        }
        row.insert(cell);

        foreach (object o in
          fieldOrdering.vectors.GetRange(firstSumKeyIndex, fieldOrdering.vectors.Count - firstSumKeyIndex))
        {
          if (summary.has(o))
          {
            cell = c_CASL.c_make("TD", "class", misc.GetCSSClass(itemClass, o)) as GenericObject;
            // Bug #10646 Mike O - Added ACC_TOTAL
            if (o.Equals("TOTAL") || o.Equals("BANK_TOTAL") || o.Equals("POST_TOTAL") || o.Equals("ACC_TOTAL") ||
              o.Equals("OVER_SHORT") || o.Equals("amount") || o.Equals("trans_amount"))
            {
              cell.insert(misc.format_dollar_amount("_subject", summary.get(o)));
              //Bug #11395 Mike O - Highlight in red if negative
              decimal amount;
              if (Decimal.TryParse(summary.get(o).ToString(), out amount))
              {
                if (amount.CompareTo(Decimal.Parse("0.00")) < 0)
                  row.set("class", "void " + row.get("class", ""));
              }
              // End Bug #11395 Mike O
            }
            else
            {
              cell.insert(summary.get(o));
            }
            row.insert(cell);
          }
          else { row.insert(c_CASL.c_make("TD")); }
        }

        returnVector.insert(row);
      }
      return returnVector;
    }
    #endregion

    #region Data acquisition
    public static object CASLAddItemsToGroup(params object[] argPairs)
    {
      GenericObject args = misc.convert_args(argPairs);
      GenericObject subject = args.get("_subject") as GenericObject;

      return AddItemsToGroup(subject);
    }

    public static object AddItemsToGroup(GenericObject subject)
    {
      subject.set("top", c_CASL.c_make("group_level"));

      GenericObject items = subject.get("items", null, true) as GenericObject;
      if (items == null)
      {
        return misc.MakeCASLError("AddItems Error", "200",
          String.Format("Could not find items in {0}", misc.ToPath(subject, c_CASL.GEO)), true);
      }
      GenericObject groupBy = subject.get("group_by", null, true) as GenericObject;
      if (groupBy == null)
      {
        return misc.MakeCASLError("AddItems Error", "200",
          String.Format("Could not find group_by in {0}", misc.ToPath(subject, c_CASL.GEO)), true);
      }

      object item, groupIndex;
      GenericObject currentGroup, itemsVector, groupLevel;
      for (int i = 0; i < items.getLength(); i++)
      {
        item = items.get(i);
        currentGroup = subject.get("top", null, true) as GenericObject;
        for (int j = 0; j < groupBy.getLength(); j++)
        {
          groupLevel = groupBy.get(j) as GenericObject;
          groupIndex = misc.CASL_call(
            "a_method", groupLevel.get("group_method", null, true),
            "_subject", item);
          if (!currentGroup.has(groupIndex))
          {
            currentGroup.set(groupIndex,
              misc.MakePart(c_CASL.c_make(groupLevel) as GenericObject, currentGroup, groupIndex));
          }
          currentGroup = currentGroup.get(groupIndex) as GenericObject;
        }

        if (!currentGroup.has("_items"))
        {
          itemsVector = c_CASL.c_make("vector") as GenericObject;
          currentGroup.set("_items", itemsVector);
        }
        else
        {
          itemsVector = currentGroup.get("_items") as GenericObject;
        }
        itemsVector.insert(item);
      }
      return subject;
    }
    #endregion

    #region Summarize
    public static object CalcSum(GenericObject subject, GenericObject fields, GenericObject report)
    {
      GenericObject summary = c_CASL.c_GEO();
      subject.set("_summary", summary);
      string key = null;
      for (int i = 0; i < fields.getLength(); i++)
      {
        key = fields.get(i) as string;
        if (key != null)
          summary.set(key, CalcSingleSum(subject, key, report));
      }
      return subject;
    }

    public static object CalcSingleSum(GenericObject subject, string sumKey, GenericObject report)
    {
      decimal sum = 0.0m;
      bool allInt = true;
      object sumValue;

      bool voidOnly = false;
      if (report != null && report.get("included_voided", false, true).Equals("void"))
      {
        voidOnly = true;
      }

      ArrayList keys = subject.getStringKeys();
      GenericObject value = null, summary = null;
      foreach (string key in keys)
      {
        if (key != null)
        {
          value = subject.get(key, c_CASL.c_GEO()) as GenericObject;

          if (!(voidOnly && value.has("VOIDDT") && !value.get("VOIDDT").Equals("")))
          {
            summary = value.get("_summary", c_CASL.c_GEO(), true) as GenericObject;
            sumValue = summary.get(sumKey, 0);
            if (!(sumValue is int))
            {
              allInt = false;
            }
            sum += Convert.ToDecimal(sumValue);
          }
        }
      }

      GenericObject items = subject.get("_items", null, true) as GenericObject;
      object oTotal;
      decimal total;
      for (int i = 0; i < items.getLength(); i++)
      {
        total = 0.0m;
        value = items.get(i, c_CASL.c_GEO()) as GenericObject;
        oTotal = value.get("TOTAL", "0.0"); // Needs a default of zero
        if (oTotal is string)
        {
          Decimal.TryParse(oTotal as string, out total);
        }
        else if (oTotal.GetType().IsValueType)
        {
          total = Convert.ToDecimal(oTotal);
        }
        value.set("TOTAL", total);

        bool addToSum = false;
        if (report != null && report.has("deposit_types"))
        {
          // If it has a report
          object reportKind = report.get("report_kind");
          if (reportKind is string)
          {
            string reportString = (string)reportKind;
            string depositTypes = report.get("deposit_types", String.Empty, true) as string;
            string depositType = value.get("DEPOSITTYPE", String.Empty, true) as string;
            if (reportString.Equals("summary") &&
              (!depositTypes.Equals("all") || !depositType.Equals("PRE_DEPOSIT")))
            {
              addToSum = true;
            }
            else if (reportString.Equals("detail"))
            {
              bool noPre = depositTypes.Equals("no_pre_deposits");
              bool all = !noPre && depositTypes.Equals("all");
              bool onlyPre = !(noPre || all) && depositTypes.Equals("only_pre_deposits");
              bool isPre = depositType.Equals("PRE_DEPOSIT");
              if ((noPre && !isPre) || (all && !isPre) || (onlyPre && isPre))
              {
                addToSum = true;
              }
            }
          }
        }
        else
        {
          addToSum = true;
        }
        if (addToSum)
        {
          sumValue = value.get(sumKey, 0);
          if (!(sumValue is int))
          {
            allInt = false;
          }
          // Bug #11395 Mike O - Subtract voided totals in tranaction type reports
          if (sumKey.Equals("TOTAL") &&
               misc.base_is_a(report, c_CASL.c_object(
                  "report.transaction_type_summary_report")))
          {
            if (value.has("VOIDDT") && !value.get("VOIDDT").Equals(""))
              sum -= Convert.ToDecimal(sumValue);
            else
              sum += Convert.ToDecimal(sumValue);
          }
          else
            sum += Convert.ToDecimal(sumValue);
        }
      }
      if (allInt)
      {
        return Convert.ToInt32(sum);
      }
      else
      {
        return sum;
      }
    }

    public static object CalcCount(GenericObject subject)
    {
      if (!(subject.get("is_count", false, true).Equals(false)))
      {
        int itemCount = 0;
        if (subject.has("_items"))
        {
          itemCount = (subject.get("_items", c_CASL.c_GEO()) as GenericObject).getLength();
        }
        int groupCount = 0;
        ArrayList keys = subject.getRegularKeys();
        GenericObject summary = null, value = null;
        object countValue;
        // Bug #8269 Mike O - key may be any object
        foreach (object key in keys)
        {
          value = subject.get(key, c_CASL.c_GEO()) as GenericObject;
          if (!(value.get("is_count", false, true).Equals(false)))
          {
            summary = value.get("_summary", null, true) as GenericObject;
            countValue = summary.get("count", 0, true);
            if (!(countValue is ValueType))
            {
              countValue = 0;
            }
            groupCount += Convert.ToInt32(countValue);
          }
          else
          {
            groupCount = 0;
          }
        }
        summary = subject.get("_summary", null, true) as GenericObject;
        summary.set("count", itemCount + groupCount);
      }
      return subject;
    }

    public static object CASLSummarize(params object[] argPairs)
    {
      GenericObject args = misc.convert_args(argPairs);
      GenericObject subject = args.get("_subject") as GenericObject;
      GenericObject fields = args.get("fields_to_sum", c_CASL.c_req) as GenericObject;
      GenericObject report = args.get("report", null) as GenericObject;
      return Summarize(subject, fields, report);
    }

    public static object Summarize(GenericObject subject, GenericObject fields, GenericObject report)
    {
      ArrayList keys = subject.getStringKeys();
      GenericObject value = null;
      foreach (string key in keys)
      {
        if (key != null)
          value = subject.get(key) as GenericObject;
        else
          value = null;

        // Bug 26202 UMN add null check and error logging
        if (value != null)
          Summarize(value, fields, report);
        else
          Logger.cs_log("Reports: value is null before recursive call to Summarize", Logger.cs_flag.error);
      }
      CalcSum(subject, fields, report);
      CalcCount(subject);
      return subject;
    }
    #endregion

    #region Field ordering
    public static GenericObject CASLGetFieldOrdering(params object[] argPairs)
    {
      GenericObject args = misc.convert_args(argPairs);
      GenericObject group = args.get("a_group", null) as GenericObject;
      return GetFieldOrdering(group);
    }

    public static GenericObject GetFieldOrdering(GenericObject group)
    {
      if (group.has("field_ordering", true))
      {
        return group.get("field_ordering", null, true) as GenericObject;
      }
      else
      {
        GenericObject itemClass = group.get("item_class", null, true) as GenericObject;
        if (itemClass == null)
          return null;
        else
          return itemClass.get("_field_order", null, true) as GenericObject;
      }
    }
    #endregion
  }

}
