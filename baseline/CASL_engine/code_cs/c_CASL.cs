using System;
using System.Reflection;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Xml;
using System.Text;
using System.Windows.Forms;
using System.Web;
using System.Collections.Specialized;
using System.Diagnostics;

namespace CASL_engine
{
  /// <summary>
  /// Summary description for CASLInterpreter.
  /// </summary>
  public class c_CASL
  {
    // TODO:parser have the init GEO right now, may move to here.
    // c# references for basic GEO objects, which is frequently used by CASL_engine C# API etc.

    //
    // The steps being counted are calls to execute_expr() in the
    // CASLInterpreter; they can be used instead of time stamps in the
    // flight recorder to get reproduceable traces in single threaded
    // situations.  IPAY-85
    //
    public static ulong master_step_count = 0;

    public static Stopwatch master_stopwatch = new Stopwatch();
    public static GenObj_Class GEO = null; //GEO created in init() called from application_start() of Global.aspx
    public static GenObj_Class c_expr_symbol = null; //GEO.expr_symbol
    public static GenObj_Class c_expr_path = null; //GEO.expr_path
    public static GenObj_Class c_expr_call = null; //GEO.expr_call
    public static GenObj_Class c_opt = null; //GEO.opt
    public static GenObj_Class c_req = null; //GEO.req

    public static GenObj_Code c_undefined = null; // used only in c#
    public static GenObj_Class c_error = null; //GEO.error
    public static GenObj_Class c_return = null; //GEO.return
    public static GenObj_Class c_method = null;  // Method class

    public static GenericObject c_break = null; //GEO.break
    public static GenericObject c_opt_symbol = null; // used only in c#
    //public static GenObj_Symbol c_opt_symbol = null; // used only in c#
    public static GenericObject c_req_symbol = null; // used only in c#
    //public static GenObj_Symbol c_req_symbol = null; // used only in c#
    public static GenericObject c_unevaluated = null; // used only in c#

    public static GenObj_Class c_Boolean = null; // used only in c# 
    public static GenObj_Class c_Number = null; // used only in c# 
    public static GenObj_Class c_Decimal = null; // used only in c# 
    public static GenObj_Class c_Integer = null; // used only in c#
    public static GenObj_Class c_String = null; // used only in c# 
    public static GenObj_Class c_Bytes = null; // used only in c#
    public static bool now_loading = false;

    private static mshtml.HTMLWindow2Class browser_window=null; // for calling JavaScript in alert method
    public static string code_base = "";
    private static NameValueCollection appSettings;
    public static bool show_verbose_error = false;//BUG#8952
    public static bool debug = false;//CASL DEBUG MODE BUG#11361


    // Config_Load_Version.  This allows for easy checking
    // for differences with existing Config.  IPAY-85 GMB
    public enum Load_Config {
      legacy,
      next_gen_v1,
      next_gen_v2
    }
    public static Load_Config Config_Load_Version = Load_Config.next_gen_v1;


    public static bool Add_Log_Entry = false; // GH Testing ANU
    public static bool Add_cs_Log_Entry = false; // Bug 19856 UMN
    public static bool disable_echo = false; // GH Testing ANU
    
    private static string m_strConnectionMinPoolSize = "";// BUG# 8056 DH
    private static string m_strConnectionMaxPoolSize = "";// BUG# 8056 DH
    private static string m_strConnectionPacketSize = "";// BUG# 8077 DH
    private static string m_strSQLQueryTimeout = "";// BUG# 8135 DH

    public static String m_ebdk2 = "p5pZhZ2/ScXKfZ0hw3w29Q=="; //BUG16062 10 digits For ANSI KEY "6jUpY/nkqiZ1Ru1THUQn8Q==";

    public static bool IsLoaded = false; // Bug 24902 MJO

   // Only called by: initialize_session_if_necessary in c_CASL.cs
   public static GenericObject create_session_my () 
   {

    GenericObject new_my=new GenericObject("_parent",GEO.get("My"),
        "_container",c_CASL.GEO, "_name","my");
    new_my.set("my",new_my);
    return new_my;
   }

   // Would like to move to casl_client.cs so we don't need to use mshtml DLL for the server version
   // Called by: initialize_casl_engine_client in casl_client.cs 
   public static void initialize_casl_engine_client (mshtml.HTMLWindow2Class browser_window_1, string new_code_base)
   {
    browser_window=browser_window_1;
    code_base=new_code_base;    
    initialize_casl_engine();
    //     misc.CASL_call("a_method","session_start", "_subject",c_CASL.GEO.get("My"));  // might move this to c_CASL.init
    c_CASL.GEO.set("my", create_session_my());
    misc.CASL_call("a_method","use_casl_client");  // might move this to c_CASL.init
   }

   // Bug #8029 Mike O - Set appSettings to be stored in GEO after it's built
   public static void set_app_settings(System.Collections.Specialized.NameValueCollection new_appSettings)
   {
     appSettings = new_appSettings;
   }

   // Bug #8029 Mike O - Set appSettings to be stored in GEO after it's built
   public static System.Collections.Specialized.NameValueCollection get_app_settings()
   {
     return appSettings;
   }

    // Called by: c_CASL.init, c_CASL.c_make, c_CASL.c_object, c_CASL.Path.   Deprecate and replace with c_CASL.GEO
    public static GenericObject get_GEO ()
    {
      return GEO;
    }


   public static void set_code_base (string new_code_base)
   {
    code_base=new_code_base;
   }

   public static void set_show_verbose_error (bool new_show_verbose_error)
   {
    show_verbose_error = new_show_verbose_error;
    if (GEO != null)
      GEO.set("show_verbose_error", new_show_verbose_error);
   }

    public static bool get_show_verbose_error()
    {
      return show_verbose_error;
    }

    //
    // Config_Load_Version allows easy switching between the legacy config
    // load that was soooo slllooooowwwww and the newer versions.  IPAY-85
    //
    public static void set_Config_Load_Version (string new_cvc)
    {
      if (new_cvc == "legacy")
        Config_Load_Version = Load_Config.legacy;
      else if (new_cvc == "next_gen_v1")
        Config_Load_Version = Load_Config.next_gen_v1;
      else if (new_cvc == "next_gen_v2")
        Config_Load_Version = Load_Config.next_gen_v2;
      else {
        Logger.cs_log("Invalid Config Option " + new_cvc + " defaulting to next_gen", Logger.cs_flag.error);
        Config_Load_Version = Load_Config.next_gen_v1;
        new_cvc = "next_gen_v1";
      }
      
      if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
        Flt_Rec.log("set_Config_Load_Version ", new_cvc.ToString());

      if (GEO != null)
        GEO.set("Config_Load_Version", new_cvc);
    }


    // BUG# 8056 DH
    public static void SetConnectionMinPoolSize(string strConnectionMinPoolSize)
    {
      m_strConnectionMinPoolSize = strConnectionMinPoolSize;
    }
    // GH Testing ANU
    public static void set_Add_Log_Entry(bool new_Add_Log_Entry)
    {
      Add_Log_Entry = new_Add_Log_Entry;
    }
		public static void set_Add_cs_Log_Entry(bool new_Add_cs_Log_Entry)
		{
			Add_cs_Log_Entry = new_Add_cs_Log_Entry; // Bug 19856 UMN
		}
    // GH Testing ANU
    public static void set_disable_echo(bool new_disable_echo)
    {
      disable_echo = new_disable_echo;
    }

    // BUG# 8056 DH
    public static void SetConnectionMaxPoolSize(string strConnectionMaxPoolSize)
    {
      m_strConnectionMaxPoolSize = strConnectionMaxPoolSize;
    }

    public static void set_debug(bool new_debug)
    {
      debug = new_debug;
      if (GEO != null)
        GEO.set("debug", new_debug);
    }
    
    public static bool get_debug()
    {
      return debug;
    }
    
    // BUG# 8077 DH
    public static void SetConnectionPacketSize(string strConnectionPacketSize)
    {
      m_strConnectionPacketSize = strConnectionPacketSize;
    }

    // BUG# 8135 DH
    public static void SetQueryTimeout(string strQueryTimeout)
    {
      m_strSQLQueryTimeout = strQueryTimeout;
    }

   // Called by: initialize_casl_engine_client, Page_Load in handle_request_2.cs.   Assumes c_CASL.GEO is null, and nothing has been loaded.  Calls init.
   public static string initialize_casl_engine ()
    {
      now_loading=true;

#if DEBUG
      CASL_Stack.verify(null);
#endif



      init_aux();
      string base_for_casl_load_file=misc.CASL_get_code_base();

      // Bug #8029 Mike O - Add all appSettings to GEO
      foreach (string key in appSettings.AllKeys)
      {
        string value = appSettings.Get(key);
        switch (value)
        {
          case "true":
            GEO.set(key, true);
            break;
          case "false":
            GEO.set(key, false);
            break;
          default:
            GEO.set(key, value);
            break;
        }
      }

      if (File.Exists(base_for_casl_load_file + "/baseline/CASL_engine/code_casl/GEO.casle"))
        GEO.set("use_encrypted",true);
    
      string config_startup_file_name = base_for_casl_load_file + "/config_startup.casl";

      CASL_Frame orig_frame = CASL_Stack.stack_top;
      GenericObject local_env = c_local_env();
      CASL_Frame frame = CASL_Stack.Alloc_Frame(EE_Steps.execute_file_frame,
                                                null, local_env);
      frame.args =               
        new GenericObject("_subject",config_startup_file_name);

      misc.fCASL_execute_file(frame);
      CASL_Stack.pop_to(orig_frame);

      string master_load_file_name = c_CASL.GEO.get("site_physical") + "/load.casl";

      frame = CASL_Stack.Alloc_Frame(EE_Steps.execute_file_frame,
                                     null, local_env);
      frame.args =
        new GenericObject("_subject",master_load_file_name);
      object return_value=misc.fCASL_execute_file(frame);
      CASL_Stack.pop_to(orig_frame);

      if (return_value is GenericObject rv &&
        rv.get("_parent").Equals(c_CASL.c_object("error")) )// return value is an error
      {
        // Bug 25532 MJO - Improved logging
        string inner_msg = rv.get("message", "no message").ToString();
        string in_cs_stk = rv.get("cs_stack_string",
                                  "no internal C# stack").ToString();
        string in_casl_stk = rv.get("casl_stack",
                                    "no internal CASL stack").ToString();
        Logger.cs_log("Message: " + inner_msg + Environment.NewLine +
                      "  inner c# stack:" + Environment.NewLine +
                      in_cs_stk + Environment.NewLine +
                      "  inner casl stack:" + Environment.NewLine +
                      in_casl_stk, Logger.cs_flag.error);

        string msg = rv.get("message", "Error returned from loading " + master_load_file_name)
            as string;
        throw CASL_error.create_obj(msg, rv);
      }
      //if (System.Web.HttpContext.Current != null) CASLInterpreter.initialize_flight_recorder();

      // Bug #9335 Mike O - Execute any CASL files in the upgrade directory
      misc.ExecuteUpgradeFiles();

      if (Flt_Rec.when == Flt_Rec.when_t.after_init)
        Flt_Rec.active = true;
      now_loading=false;
      IsLoaded = true; // Bug 24902 MJO - Make it known that the site has loaded
      return base_for_casl_load_file;
    }

    //
    //    iPayment relies heavily on GenericObjects and this happens during
    // Application_Start before the first request is processed (see pci.pci.setbdk()).
    // In some odd cases, all known cases involve a corrupt or out-of-date Config,
    // a call to GenericObject.get() causes a stack overflow.  This initialization
    // avoids that behavior.
    //
    public static void Critical_Initialization()
    {
      // Call hand crafted GEO creation as it is unique.

      GEO = GenObj_Class.create_GEO();
      c_error = GenObj_Class.create_c_error();
    }

    /// <summary>
    /// load basic casl object before we can load any files
    /// </summary>
    /// <returns></returns>
    public static GenericObject init_aux ()
    {
      // NOTE: We could use to get the actual file name (still use getFileIdx())
      // and then get the current line when defining classes or methods by hand.
      //
      // string currentFile = new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileName(); 
      // int currentLine = new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileLineNumber();
      //
      // I just felt it was overkill.  So the simple approach is:

      string this_file_name = "baseline/CASL_engine/code_cs/c_CASL.cs";
      int file_index = CASLInterpreter.getFileIdx(this_file_name);
      code_location code_local = code_location.builtin_location;


      //--------------------------------------------------
      // Manually setup basic GEO objects before loading CASL_engine casl API.
      // specific order is very important here.
      //--------------------------------------------------

      GEO.set("use_client",false); // get_GEO();
      GEO.loc = code_local;

      // Set no_subject
      GEO.set("no_subject", CASLInterpreter.no_subject);

      // Set debug
      c_CASL.GEO.set("debug", debug);

      c_CASL.GEO.set("connection_pool_min", m_strConnectionMinPoolSize);// BUG# 8056 DH
      c_CASL.GEO.set("connection_pool_max", m_strConnectionMaxPoolSize);// BUG# 8056 DH
      c_CASL.GEO.set("connection_packet_size", m_strConnectionPacketSize);// BUG# 8077 DH
      c_CASL.GEO.set("sql_query_timeout", m_strSQLQueryTimeout);// BUG# 8135 DH

      // GH Testing ANU
      c_CASL.GEO.set("Add_Log_Entry", Add_Log_Entry);
			c_CASL.GEO.set("Add_cs_Log_Entry", Add_cs_Log_Entry);  // Bug 19856 UMN

      // GH Testing ANU
      c_CASL.GEO.set("disable_echo", disable_echo);

#if DEBUG
      //
      // New Config Version accessable from CASL.  IPAY-85.
      //
      GEO.set("Config_Load_Version", Config_Load_Version);
#endif
     
      // expression classes used by the parser
      c_expr_symbol = new GenObj_Class("expr_symbol", GEO);
      GEO.set("expr_symbol",c_expr_symbol); 
      c_expr_path = new GenObj_Class("expr_path", GEO);
      GEO.set("expr_path",c_expr_path); 
      c_expr_call = new GenObj_Class("expr_call", GEO);
      GEO.set("expr_call",c_expr_call); 

      // create opt, req as class.  Must occur here before being used.
      c_opt = new GenObj_Class("opt", GEO);
      c_opt.decl_loc = code_local;
      GEO.set("opt",c_opt); 
      GenObj_Symbol tmp_sym = new GenObj_Symbol("opt");
      tmp_sym.loc = code_local;
      c_opt_symbol = tmp_sym;

      c_req = new GenObj_Class("req", GEO);
      c_req.decl_loc = code_local;
      GEO.set("req",c_req); 
      tmp_sym =  new GenObj_Symbol("req");
      tmp_sym.loc = code_local;
      c_req_symbol = tmp_sym;

      // create undefined c# object
      c_undefined = new GenObj_Code(GO_kind.casl_code, code_local, "_parent",GEO, "_name","undefined");

      //frame
      //GenericObject frame_class = new GenObj_Code(GO_kind.casl_code, code_local, "_parent",GEO, "_name","frame","_container",GEO);
      //GEO.set("frame",frame_class); 
    
      //Manually define class "method"
      c_method = new GenObj_Class("method", GEO);
      c_method.decl_loc = code_local;

      GEO.set("method",c_method); 
      //c_method = new GenObj_Class("_parent",GEO,"_name","method","_container",GEO);
      //c_method.file_index = file_index;
      //GEO.set("method",c_method); 

      // create return as class
      c_return =  new GenObj_Class("return", GEO);
      c_return.set(
        "value",null,
        "_field_order",
          new GenObj_Code(GO_kind.casl_code, code_local,
                          "_parent",GEO, 0,"value")
        );
      c_return.loc = code_local;
      GEO.set("return",c_return);

      //      GEO.<defclass> _name="return" value=null </defclass>
       
      // set c_Boolean, c_Number, c_Decimal
      // FIXME: set Declaration Location when merge.

      c_Boolean = new GenObj_Class("Boolean", GEO);
      c_Boolean.loc = code_local;
      GEO.set("Boolean", c_Boolean);


      c_Number = new GenObj_Class("Number", GEO);
      c_Number.loc = code_local;
      GEO.set("Number",c_Number);

      c_Decimal = new GenObj_Class("Decimal", GEO);
      c_Decimal.loc = code_local;
      GEO.set("Decimal",c_Decimal);

      c_Integer = new GenObj_Class("Integer", GEO);
      c_Integer.loc = code_local;
      GEO.set("Integer",c_Integer);

      c_String = new GenObj_Class("String", GEO);
      c_String.loc = code_local;
      GEO.set("String",c_String);

      c_Bytes = new GenObj_Class("Bytes", GEO);  // <defclass _name='Bytes'/>
      c_Bytes.loc = code_local;
      GEO.set("Bytes",c_Bytes);
       
      //Manually define the init method
      GenObj_Symbol sym = new GenObj_Symbol("_subject");
      sym.loc = code_local;
      GenObj_CASL_Method a_method = new GenObj_CASL_Method();
      // Using an _impl should be faster.
      a_method.method_name = "init";
      a_method.set(
        "_parent",c_method,
        "_name","init", 
        "_container",GEO, 
        //"_new_object",c_CASL.c_object("req"), 
        0, sym
        );//Tool.string_to_path_or_symbol("_subject"));
      a_method.loc = code_local;
      GEO.set("init", a_method);
      a_method.full_name = "GEO.init";
      CASLInterpreter.dbg_meth_dict_add(a_method.full_name, a_method);


      //Manually define "Unevaluated" object
      c_unevaluated = misc.CASL_execute_string("_subject",
          @"<GEO> _name='Unevaluated' _container=GEO safe=false </GEO>")
          as GenericObject;
      GEO.set("Unevaluated", c_unevaluated);
              
      #region Basic C# implemented Methods

      GenObj_CS_Method new_method=null;

      new_method=misc.defmethod_impl("set", GEO, true, c_opt, c_opt,
          "cs.CASL_engine.misc.CASL_set" );
      GEO.set("set", new_method);

      GenObj_Code field_order_load =
        new GenObj_Code(GO_kind.casl_code, code_local,
                        "_parent",GEO, 0,"name", 1,"folder",
                        2,"OK_if_missing");
      new_method=misc.defmethod_impl("load", GEO,
          new GenObj_Code(GO_kind.casl_code, code_local,
                          "name",c_req,
                          "folder",c_req,
                          "OK_if_missing",c_opt,
                          "_field_order", field_order_load),
          "cs.CASL_engine.misc.CASL_load");

      GEO.set("load", new_method);

      GenObj_Code local_env = new GenObj_Code(GO_kind.casl_code,
          code_local,
          "_parent",GEO);
      GenObj_Code field_order = new GenObj_Code(GO_kind.casl_code,
          code_local,
          "_parent",GEO, 0, "local_env",
          1, "encrypt_file", 2, "secret_key");
      GenObj_Code new_method_args = new GenObj_Code(GO_kind.casl_code,
          code_local,
         "local_env", local_env,
          "encrypt_file", false,
          "secret_key", "",
          "_field_order", field_order);

      new_method=misc.defmethod_impl("execute_file", GEO,
        new_method_args,
        "cs.CASL_engine.misc.fCASL_execute_file");
      GEO.set("execute_file", new_method);

      new_method=misc.defmethod_impl("get_code_base", GEO, false, false, false, "cs.CASL_engine.misc.CASL_get_code_base_impl");
      GEO.set("get_code_base", new_method);

      new_method=misc.defmethod_impl("get_site_name", GEO, false, false,
        false, "cs.CASL_engine.misc.get_site_name_impl");
      GEO.set("get_site_name", new_method);

      new_method = misc.defmethod_impl("concat", GEO, false, false, c_opt, "cs.CASL_engine.misc.CASLConcat");
      GEO.set("concat", new_method);

      // Bug #14079 Mike O - Added skip_empty argument
      new_method=misc.defmethod_impl("join", GEO,
          new GenObj_Code(GO_kind.casl_code, code_local,
                          "_other_unkeyed",c_opt, "formatter","htm",
                          "separator","", "skip_empty",true,
                          "_field_order",new GenObj_Code(GO_kind.casl_code,
                                                         code_local,
                                                         "_parent",GEO)),
                                     "cs.CASL_engine.misc.CASLJoin");
      GEO.set("join", new_method);

      new_method=misc.defmethod_impl("if", GEO,
        new GenObj_Code(GO_kind.casl_code, code_local, "_other_unkeyed",c_req, "_do_not_execute_vector_args",true, "_field_order",new GenObj_Code(GO_kind.casl_code, code_local, "_parent",GEO)),
        "cs.CASL_engine.misc.fCASL_if");
      GEO.set("if", new_method);


      new_method=misc.defmethod_impl("has", GEO,
        new GenObj_Code(GO_kind.casl_code, code_local, "key",c_req, "lookup",false, "_pass_outer_env",true, "_field_order",new GenObj_Code(GO_kind.casl_code, code_local, "_parent",GEO, "_parent",GEO, 0,"key", 1,"lookup")),
        "cs.CASL_engine.misc.CASL_has");
      GEO.set("has", new_method);

      new_method=misc.defmethod_impl("not", GEO, false, false, false, "cs.CASL_engine.misc.CASL_not");
      GEO.set("not", new_method);

      new_method=misc.defmethod_impl("defmethod", GEO, new GenObj_Code(GO_kind.casl_code, code_local, "_other_keyed",c_opt,
        "_other_unkeyed",c_opt, "_do_not_execute_vector_args",true, "_do_not_execute_string_args",true,
        "_def",true, "_save_expr",true, "_name_f_req",true, "_field_order",new GenObj_Code(GO_kind.casl_code, code_local, "_parent",GEO, 0,"_name")),
        "cs.CASL_engine.misc.fCASL_defmethod");
      GEO.set("defmethod", new_method);

      new_method=misc.defmethod_impl("defclass", GEO,
        new GenObj_Code(GO_kind.casl_code, code_local, "_other_keyed",c_opt, "_other_unkeyed",c_opt,
          "_do_not_execute_vector_args",true, "_def",true, "_save_expr",true, "_name_f_req",true,
          "_field_order",new GenObj_Code(GO_kind.casl_code, code_local, "_parent",GEO, 0,"_name")),
        "cs.CASL_engine.misc.fCASL_defclass");
      GEO.set("defclass", new_method);

      #endregion

      //Manually define break
      c_break = new GenObj_Code(GO_kind.casl_code, code_local, "_parent",GEO, 
        "_name","break", 
        "_container",GEO, 
        "value",null,
        "_field_order",
        new GenObj_Code(GO_kind.casl_code, code_local, "_parent",GEO, 0,"value")
        );
      // GEO.<defclass _name='break'> value=null </defclass>
      GEO.set("break",c_break); 

      GenericObject frame_class = new GenObj_Code(GO_kind.casl_code, code_local, "_parent",GEO, 
       "_name","frame", 
       "_container",GEO, 
       "a_method",null,
       "a_expr",null
       );
      GEO.set("frame",frame_class); // GEO.<defclass _name='frame'>   Moved to here because "frame" not found when trying to report an error during load.

      return GEO;
    }

    /// <summary>
    /// Makes a new, empty GenericObject with GEO as the parent.
    /// </summary>
    /// <returns>an empty GEO with parent of GEO</returns>
    public static GenericObject c_GEO()
    {
      return new GenericObject("_parent", c_CASL.GEO);
    }
    public static GenericObject c_local_env()
    {
      GenericObject local_env = c_GEO();
      local_env.set("_local",local_env);
      return local_env;
    }
  

    public static GenericObject c_instance(string path, params object[] args)
    {
      // NI!
      GenericObject a_shrubbery = c_object(path) as GenericObject; // NI!
      GenericObject NI = new GenericObject("_parent", a_shrubbery ); // NI=new instance
      NI.set(args);   // Where is our shrubbery!?
      return NI; // NU!  I mean, NI!
    }
    
    /// <summary>
    /// return new instance with _parent = parent_path and fields from key-value pairs
    /// </summary>
    /// <param name="parent_path"></param>
    /// <returns></returns>

    public static Object c_make(string path, params object[] args)
    {
      GenericObject new_object = new GenericObject(args);
      GenericObject a_class = get_GEO().get_path(path) as GenericObject;
      new_object.set("_parent",a_class);
      object init_method = a_class.get("init",null,true);//get_w_lookup("init",null);
      if( init_method == null )
      {
        return new_object;
        //        string s_message = string.Format("'init' method not found in {0}.",misc.to_eng(a_class));
        //        throw CASL_error.create("message",s_message);
      }
      GenericObject local_env = c_CASL.c_local_env();
      local_env.set("_subject",new_object);
      Object new_instance = misc.CASL_apply("a_method",init_method, "local_env", local_env) ;
      return new_instance;
    }
  
    public static GenericObject c_make(GenericObject parent, params object[] args)
    {
      GenericObject newObject = new GenericObject(args);
      newObject.set("_parent", parent);

      object initMethod = parent.get("init", null, true);
      if (initMethod == null) { return newObject; }

      GenericObject localEnv = c_CASL.c_local_env();
      localEnv.set("_subject", newObject);
      GenericObject newInstance = misc.CASL_apply("a_method", initMethod, "local_env", localEnv) as GenericObject;
      return newInstance;
    }

    /// <summary>
    /// return geo object by path
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public static Object c_object(string path)
    {
      return c_CASL.GEO.get_path(path, true, false); //(getAtPath(path);
    }
    public static GenericObject c_call(string call_name, params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      GenericObject call_expr = new GenericObject("_parent", c_sy_n_pa(call_name));
      call_expr.set(geo_args.getObjectArr_All());
      return call_expr;
    }

#if dead_code
    //
    // IPAY-325 GMB -- this is not used and was always going to return false anyway
    // because get_path() rejects anything but a string in the first argument.
    //
    public static Object Path (GenericObject path_or_symbol)
    {
      GenericObject the_GEO = get_GEO();
      return the_GEO.get_path(path_or_symbol); //getAtPath(Tool.symbol_or_path_to_string(path_or_symbol));
    }
#endif

    public static GenObj_Path c_path(params Object[] path_parts)
    {
      GenObj_Path g_path = new GenObj_Path();
      int i=0;
      foreach(object path_part in path_parts )
      {
        if (i==0)
        {
          if ( (path_part is String) )
            g_path.insert(new GenObj_Symbol(path_part.ToString()));
          else
            return null;
        }
        else
          g_path.insert(path_part);
        i++;
      }
      return g_path;
    }
    public static GenericObject c_sy_n_pa(string a_string)
    {
      if( a_string.IndexOf('.') < 0 )
      {
        GenObj_Symbol a_symbol = new GenObj_Symbol(a_string);
        return a_symbol;
      }
      else
      {
        string[] path_parts = a_string.Split('.');
        GenObj_Path a_path = new GenObj_Path();
        int i=0;
        foreach(string path_part in path_parts)
        {
          if ( i==0 )
            a_path.insert(new GenObj_Symbol(path_part));
          else
          {
            Object part_path_obj = path_part; // can be interger or string
            char[] char_parts = path_part.ToCharArray();
            if(ConciseXMLCharSet2.IS_DIGIT(char_parts[0]))
              part_path_obj = Convert.ChangeType(path_part,typeof(int));
            a_path.insert(part_path_obj);
          }
          i++;
        }
        return a_path;
      }
    }

    public static Object execute(string expr_string)
    {
      Object a_method = CASLParser.decode_conciseXML(expr_string,"");
      return CASLInterpreter.execute_expr(
          CASL_Stack.Alloc_Frame(EE_Steps.standard, a_method, c_CASL.c_GEO()));
    }

    public static void alert (string message) 
    {
      if (browser_window!=null)
        browser_window.alert(message);
      //else   //MP caused error: It is invalid to show a modal dialog or form when the application is not running in UserInteractive mode. Specify the ServiceNotification or DefaultDesktopOnly style to display a notification from a service application
      //  System.Windows.Forms.MessageBox.Show(message);
      // write to log??
    }
    // called by: GEO.alert
    public static Object casl_alert (params Object[] args) 
    {
      GenericObject g_args = misc.convert_args(args);
      string message = (string)g_args.get("message","");
      alert(message);  // MP: changed to function alert so it avoids execScript
      //if (browser_window!=null)
      //  browser_window.execScript("alert('" + message.Replace("'","\'") + "')", "JavaScript");
      // write to log??
      return true; // Maybe should be false if browser_window == null?
    }

  }
}
      
          
