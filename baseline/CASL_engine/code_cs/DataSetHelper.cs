using System;
using System.Collections;
using System.ComponentModel;
using System.Data;

namespace CASL_engine
{
	/// <summary>
	/// Microsoft DataSetHelper Class 
	/// Helper class perform any of the other relational operators beyond dataset API restriction
	/// </summary>
	public class DataSetHelper
	{
		public DataSet ds;
		public DataSetHelper(DataSet DataSet)
		{
			ds = DataSet;
		}
		public DataSetHelper()
		{
			ds = null;
		}
		// Implement CREATE TABLE
		public DataTable CreateTable(string TableName, string FieldList)
		{	
			DataTable dt = new DataTable(TableName);
			DataColumn dc;
			string[] Fields= FieldList.Split(',');    
			string[] FieldsParts;
			string Expression;
			foreach(string Field in Fields)
			{
				FieldsParts = Field.Trim().Split(" ".ToCharArray(), 3); // allow for spaces in the expression
				// add fieldname and datatype			
				if (FieldsParts.Length == 2)
				{	
					dc = dt.Columns.Add(FieldsParts[0].Trim(), Type.GetType("System." + FieldsParts[1].Trim(),true,true));
					dc.AllowDBNull = true;
				}
				else if (FieldsParts.Length == 3)  // add fieldname, datatype, and expression
				{
					Expression = FieldsParts[2].Trim();
					if (Expression.ToUpper() == "REQ")
					{				
						dc = dt.Columns.Add(FieldsParts[0].Trim(), Type.GetType("System." + FieldsParts[1].Trim(), true, true));
						dc.AllowDBNull = false;
					}
					else
					{
						dc = dt.Columns.Add(FieldsParts[0].Trim(), Type.GetType("System." + FieldsParts[1].Trim(), true, true), Expression);
					}
				}
				else
				{
					throw new ArgumentException("Invalid field definition: '" + Field + "'.");
				}
			}
			if (ds != null) 
			{
				ds.Tables.Add(dt);
			}
			return dt;
		}
		//------------------------------------------------------------------------

        //------------------------------------------------------------------------
		// Implement SELECT DISTINCT
		private bool ColumnEqual(object A, object B)
		{
	
			// Compares two values to see if they are equal. Also compares DBNULL.Value.
			// Note: If your DataTable contains object fields, then you must extend this
			// function to handle them in a meaningful way if you intend to group on them.
			
			if ( A == DBNull.Value && B == DBNull.Value ) //  both are DBNull.Value
				return true; 
			if ( A == DBNull.Value || B == DBNull.Value ) //  only one is DBNull.Value
				return false; 
			return ( A.Equals(B) );  // value type standard comparison
		}
		public DataTable SelectDistinct(string TableName, DataTable SourceTable, string FieldName)
		{	
			DataTable dt = new DataTable(TableName);
			dt.Columns.Add(FieldName, SourceTable.Columns[FieldName].DataType);
			
			object LastValue = null; 
			foreach (DataRow dr in SourceTable.Select("", FieldName))
			{
				if (  LastValue == null || !(ColumnEqual(LastValue, dr[FieldName])) ) 
				{
					LastValue = dr[FieldName]; 
					dt.Rows.Add(new object[]{LastValue});
				}
			}
			if (ds != null) 
				ds.Tables.Add(dt);
			return dt;
		}
		//------------------------------------------------------------------------

		//------------------------------------------------------------------------
		// Implement JOIN

	





	}

}
