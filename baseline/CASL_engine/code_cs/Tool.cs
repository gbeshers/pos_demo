/* -*- mode: c; -*- */
// #define DEBUG
// #define DEBUG_allocation_type

using System;
using System.Reflection;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Xml;
using System.Text;
using System.Windows.Forms;
using System.Web;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace CASL_engine
{
	/// <summary>
	/// Summary description for CASLInterpreter.
	/// </summary>
	public class Tool
	{
          //
          // Place any path part which begins with a digit into
          // quotes so that if it is passed to execute_string()
          // it is interpreted to the same object.
          //
          // The test for the empty string as a path part is not
          // pedantic, it *does* happen.  Should be corrected.  GMB
          //
          // Keys which are neither strings or ints return null.
          //
          public static string caslize_path_part(object obj)
          {
            if (obj is string s) {
              if (s == "")
                return "\"\"";
              if (ConciseXMLCharSet2.IS_DIGIT(s[0]))
                return "\"" + s + "\"";
              if (s.IndexOf(".") >= 0)
                return "\"" + s + "\"";
              return s;
            }
            if (obj is int i)
              return i.ToString();
            if (obj is GenObj_Symbol sym)
              return symbol_to_string(sym);
            if (obj is GenObj_Class goc) {
              Logger.cs_log("caslize_path_part path part is a GenObj_Class" +
                          goc.class_name);
              return goc.class_name;
            }
            if (obj is GenericObject geo && geo.has("_name")) {
              Logger.cs_log("caslize_path_part path part is a GenericObject" +
                            geo.get("_name"));
              return null;
            }
            Logger.cs_log("caslize_path_part path part is an Object" +
                          obj.ToString());
            return null;
          }

          //
          // Convert a path to a period punctuated string.
          //
          public static string path_to_string(GenObj_Path a_path)
          {
            String s_return = "";
            
            if (a_path.getLength() > 0) {
              if (a_path.get(0) is GenObj_Symbol)
                s_return += symbol_to_string(a_path.get(0) as GenObj_Symbol);
              else
                s_return += caslize_path_part(a_path.get(0));
            }

            for(int i=1; i<a_path.getLength(); i++) {
              s_return += "." + caslize_path_part(a_path.get(i));
            }
            return s_return;
          }

		public static string symbol_or_path_to_string(GenericObject a_symbol_or_path)
		{
			if ( a_symbol_or_path is GenObj_Symbol)
        return symbol_to_string(a_symbol_or_path as GenObj_Symbol);
			else if(a_symbol_or_path is GenObj_Path)
                    return path_to_string(a_symbol_or_path as GenObj_Path);
			else
				return "";
		}

    //
    // Change Tool.symbol_to_string() from type Object to type
    // GenObj_Symbol and eliminate null check.  Instead, when
    // DEBUG is turned on, it checks to make sure that the
    // member 'name' and the dictionary entry for 'name' are
    // identicial.
    //
    // This paves the way for switching to using the member
    // name when looking up a symbol from C# with a performance
    // improvement.
    //
    // NOTE: the current procedure requires searching for "name"
    // in the symbol's dictionary and then searching for the
    // result in the current environment.
    //

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static string symbol_to_string(GenObj_Symbol a_symbol)
      {
        string symbol_name = a_symbol.name;
#if DEBUG
        string name_check = a_symbol.get("name", "") as string;
        if (symbol_name != name_check) {
          if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
            Flt_Rec.log("execute_expr GO_kind.symbol",
                        " name is \"" + symbol_name +
                        "\" but get(\"name\", \"\") is \"" +
                        name_check + "\"");
        }
#endif
        return symbol_name;
      }



		// path_string definition: not start with digit, no quote, part is string or interger, 
		// convert interget part
		// symol_string definition
		/// <summary>
		/// path_string definition: start with letter, no quote, part is string or interger, 
		/// convert digit part to interger, at least one dot, no white space
		/// TODO: handle any complex object
		/// symol_string definition: start with letter, no white space, no dot
		/// </summary>
		/// <param name="a_string"> a_string=<one_of>path_string symbol_string</one_of> </param>
		/// <returns></returns>
		public static GenericObject string_to_path_or_symbol(string a_string)
		{
                  if( a_string.IndexOf('.') < 0 )
                    {
                      object symbol_name= a_string;
                      char[] char_parts = a_string.ToCharArray();
                      if(ConciseXMLCharSet2.IS_DIGIT(char_parts[0])) {
                        Logger.cs_log("ERROR: Tool.string_to_path_or_symbol()" +
                                      " trying to make integer into symbol",
                                      Logger.cs_flag.error);
                        symbol_name = Convert.ChangeType(a_string,typeof(int));
                      }
                      GenObj_Symbol a_symbol = new GenObj_Symbol(a_string);
                      return a_symbol;
                    }
                  else
                    {
                      string[] path_parts = a_string.Split('.');
                      GenObj_Path a_path = new GenObj_Path();
                      int i=0;
                      foreach(string path_part in path_parts)
                        {
                          if ( i==0 )
                            {
                              object symbol_name= path_part;
                              char[] char_parts = path_part.ToCharArray();
                              if(ConciseXMLCharSet2.IS_DIGIT(char_parts[0])) {
                                Logger.cs_log("ERROR: Tool.string_to_path_or_symbol()" +
                                              " trying to make integer into symbol" +
                                              " as the first part of the path",
                                              Logger.cs_flag.error);
                                //symbol_name = Convert.ChangeType(path_part,typeof(int));
                              }
                              a_path.insert(new GenObj_Symbol(path_part));
                            }
                          else
                            {
                              Object part_path_obj = path_part; // can be interger or string
                              char[] char_parts = path_part.ToCharArray();
                              if(ConciseXMLCharSet2.IS_DIGIT(char_parts[0]))
                                part_path_obj = Convert.ChangeType(path_part,typeof(int));
                              a_path.insert(part_path_obj);
                            }
                          i++;
                        }
                      return a_path;
                    }
		}

		public static bool is_symbol_or_path(object any_obj)
		{
			if (any_obj is GenObj_Symbol || any_obj is GenObj_Path)
				return true;
			return false;
		}

                // is_symbol() removed in favor of 'is GenObj_Symbol'

                
                // is_path() removed in favor of 'is GenObj_Path'

                
		// (uri) path_string definition: type=string ; no space; at least 1 dot, start with letter; 
		public static bool is_path_string(string any_string)
		{
			if( any_string.IndexOf(" ")>=0 )
				return false;
			if (any_string.IndexOf(".")<0)
				return false;
			return true;
		}

		public static bool is_type_for(object any_obj, string type_name)
		{
			if (! (any_obj is GenericObject) )
				return false;
			Object parent = (any_obj as GenericObject).get("_parent", null);
			if ( ! (parent is GenericObject))
			        return false;
			if( type_name.Equals((parent as GenericObject).get("_name","")) )
				return true;
			return false;
		}
			
		// symbol.name = call_name
		public static bool is_call_for(object any_obj, string call_name)
		{
			if (! (any_obj is GenericObject) )
				return false;
			Object parent = (any_obj as GenericObject).get("_parent", null);
			if (parent == null)
			        return false;
			if( parent is GenObj_Symbol )
			{
                                string a_call_string_name = symbol_to_string(parent as GenObj_Symbol);
				if ( a_call_string_name.Equals(call_name) )
					return true;
			}
			else if ( parent is GenObj_Path )
			{
				string a_call_string_name = (parent as GenericObject).top_of().ToString();
				if ( a_call_string_name.Equals(call_name) )
					return true;
			}
			return false;
		}

		// symbol.name = symbol_name
		public static bool is_symbol_for(object any_obj, string symbol_name)
		{
			if( any_obj is GenObj_Symbol )
			{
				string a_call_string_name = symbol_or_path_to_string(any_obj as GenericObject);
				if ( a_call_string_name.Equals(symbol_name) )
					return true;
			}
			return false;
		}

		public static bool is_defclass(object any_obj)
		{
                  if (any_obj is GenericObject geo) {
                    var p = geo.get("_parent", null) as GenericObject;
                    var c = geo.get("_container", null) as GenericObject;
                    if (p != null && p == c)
                      return true;
                  }
                  return false;
		}
#if dead_code
        // IPAY-325 GMB : the use of get_path() here is bogus and the switch to GenObj_Method had
		// been stable for a couple of years.  Remove this code.
		//
		// method_call definition: type=GO, _parent=req=symbol/path , lookup _parent which has _name 
		// TODO: _parent can be an object which evaluated already in post_parser_evaluation
		public static bool is_method_call(object any_obj)
		{
			if (! (any_obj is GenericObject) )
				return false;
			Object parent = (any_obj as GenericObject).get("_parent", null);
			if (parent == null)
			        return false;
				
			if( is_symbol_or_path(parent) )
			{
				parent = c_CASL.GEO.get_path(parent,true,null);//get_at_path_w_lookup(parent);
			}
			if ( parent!=null && parent is GenericObject )
			{
				if( "method".Equals((parent as GenericObject).get("_name", "")))// change definition of method
					return true;
			}
			return false;
		}
#endif

		// check if object:nested is nested in object:subject
		// TOFIX: this function may cause stackoverflow exception because it doesn't check circular reference.
		public static bool is_nested(GenericObject subject, GenericObject nested)
		{
			if( subject == null || nested== null) return false;
			if( nested == subject )
				return true;
			ArrayList values = subject.getValues();
			foreach(object value in values)
			{
				if( value == null) continue;
				else if( value == nested)
					return true;
				else
				{
					if( is_nested(value as GenericObject,nested))
						return true;
					else
						continue;
				}
			}
			return false;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="args">_subject=req=GenericObject</param>
		/// <returns></returns>
		public static bool is_call(params Object[] args)
		{
			GenericObject g_args = misc.convert_args(args);
			GenericObject g_subject = g_args.get("_subject", null) as GenericObject;
            if (g_subject == null)
                return false;

			Object parent = g_subject.get("_parent",null);
			if( is_symbol_or_path(parent) )
			{
				return true;
			}
			return false;
		}


		/// <summary>
		/// system call definition: type=GO, _parent=symbol , find symbol in system_calls map
		/// test case: 
		/// true - <if></if> ;<is></is>; <is_not></is_not>
		/// false - <fi></fi>; <do_if></do_if>
		/// </summary>
		/// <param name="any_obj"></param>
		/// <returns></returns>
		//		public static bool is_system_call(object any_obj)
		//		{
		//			if (! (any_obj is GenericObject) )
		//				return false;
		//			Object parent = (any_obj as GenericObject).get("_parent");
		//			if( is_symbol(parent) )
		//			{
		//				string a_call_string_name = symbol_or_path_to_string(parent as GenericObject);
		//				if ( CASLInterpreter.system_calls.has(a_call_string_name) )
		//					return true;
		//			}
		//			return false;
		//		}
		// cs call definition: type=GO, _parent=symbol/path , symbol.name="cs" eval(symbol)._parent.has(_name), 
		public static bool is_cs_call(object any_obj)
		{
			if (! (any_obj is GenericObject) )
				return false;
			Object parent = (any_obj as GenericObject).get("_parent", null);
			if (parent == null)
			        return false;
			if( parent is GenObj_Symbol )
			{
				string a_call_string_name = symbol_or_path_to_string(parent as GenericObject);
				if ( a_call_string_name.StartsWith("cs") )
					return true;
			}
			else if ( parent is GenObj_Path )
			{
				//string a_call_string_name = (parent as GenericObject).top_of().ToString();
				if ( Tool.is_symbol_for((parent as GenericObject).get(0, ""),"cs") )
					return true;
			}
			return false;
		}

		public static bool is_cs_path(object any_obj)
		{
			if( any_obj is GenObj_Path )
			{
				if( Tool.is_symbol_for((any_obj as GenericObject).get(0, ""),"cs") )
					return true;
			}
			return false;
		}

                public static bool is_casl_class(object any_obj)
                {
                  if( any_obj is GenericObject)
                    {
                      GenericObject g_any_obj = any_obj as GenericObject;
                      object parent =   g_any_obj.get("_parent",null);
                      object container = g_any_obj.get("_container",null);
                      object name =     g_any_obj.get("_name",null);
                      if ( name!= null && "GEO".Equals(name)) {
                        // if (g_any_obj.kind != GO_kind.casl_class)
                        //   Debugger.Break();
                        return true;
                      }
                      if ( parent!= null && container!= null && parent.Equals(container)) {
                        // if (g_any_obj.kind != GO_kind.casl_class)
                        //   Debugger.Break();
                        return true;
                      }
                      // if (g_any_obj.kind == GO_kind.casl_class)
                      //   Debugger.Break();
                      return false;
                    }
                  return false;
                }

		public static bool is_casl_method(object any_obj)
		{
			if( any_obj is GenericObject)
			{
				GenericObject g_any_obj = any_obj as GenericObject;
				object parent = 	g_any_obj.get("_parent", null);
				if( parent !=null && parent.Equals(c_CASL.c_object("method")))
					return true;
			}
			return false;
		}

		/// <summary>
		/// check if it's primitive defined in casl (null, number, boolean, string)
		/// </summary>
		/// <param name="any_obj"></param>
		/// <returns></returns>
		public static bool is_casl_primitive(object any_obj)
		{
			if( any_obj == null || any_obj.GetType().IsValueType || any_obj.GetType()== typeof(System.String))
				return true;
			return false;
		}

		/// <summary>
		/// check if it's cs instance
		/// if casl primitive, return false.
		/// else if GenericObjct without _parent, return true,
		/// else if  not GenericObjct, return true,
		/// else return false
		/// </summary>
		/// <param name="any_obj"></param>
		/// <returns></returns>
		public static bool is_cs_instance(object any_obj)
		{
			if ( is_casl_primitive(any_obj))
				return false;
			else if( any_obj!= null && c_CASL.GEO.Equals(any_obj))
				return false;
			else if( (any_obj is GenericObject) && (any_obj as GenericObject).get("_parent", null) == null)
				return true;
			else if (!(any_obj is GenericObject) )
				return true;
			return false;
		}

		public static bool is_cs_class(object any_obj)
		{
			if (any_obj is Type)
				return true;
			return false;
		}

		public string convert_stream_to_string(Stream a_stream) 
		{
			System.Text.Encoding encoding = System.Text.Encoding.UTF8;
			// Read string from binary file with UTF8 encoding
			byte[] buffer = new byte[30];
			a_stream.Read(buffer, 0, 30);
			return encoding.GetString(buffer);
		}

  //http://www.vbforums.com/showthread.php?t=287324

  public static string base64Encode (Object [] arg_pairs)
  {
   GenericObject args = misc.convert_args(arg_pairs);
   string data = args.get("data_to_encrypt", "") as string;
   try
   {
    byte[] encData_byte = new byte[data.Length];
    encData_byte = System.Text.Encoding.UTF8.GetBytes(data);    
    string encodedData = Convert.ToBase64String(encData_byte);
    return encodedData;
   }
   catch(Exception e)
   {
    throw new Exception("Error in base64Encode" + e.Message);
   }
  }


  public static string base64Decode (Object [] arg_pairs)   
   {
   GenericObject args = misc.convert_args(arg_pairs);
   string data = args.get("encrypted_data", "") as string;

   try
   {
    System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();  
    System.Text.Decoder utf8Decode = encoder.GetDecoder();
    
    byte[] todecode_byte = Convert.FromBase64String(data);
    int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);    
    char[] decoded_char = new char[charCount];
    utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);                   
    string result = new String(decoded_char);
    return result;
   }
   catch(Exception e)
   {
    throw new Exception("Error in base64Decode" + e.Message);
   }
  }

	}	
}
