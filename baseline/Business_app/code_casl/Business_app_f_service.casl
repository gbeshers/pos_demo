<do>

<!-- Bug 15184 UMN - promote from cbc to here -->
Business_app.<defmethod _name='lookup_referrer'> referrer_host=req
  .referrer_host_list.<for_each> include='string_keys'
    <if> referrer_host.<ends_with> key </ends_with> 
      <return> true </return>
    </if>
  </for_each>
  <return> false </return>
</defmethod>

<!-- Bug 15184 UMN - rewrite to use new NoReferrer value -->
Business_app.<defmethod _name='check_referrer'>  
  <if> .referrer_host_list.<is_not> false </is_not>
    <do>
      <set> 
        referrer_host = my.<get> "referrer_host" if_missing="NoReferrer" </get> 
      </set>
      <if> .<lookup_referrer> referrer_host = referrer_host </lookup_referrer>.<is> true </is> 
        <return> true </return>
      else
        <do>
          <log> "UNAUTHORIZED REFERRER:" referrer_host </log>
          <return> false </return>
        </do>
      </if>
    </do>
  else
    <return> true </return>
  </if>
</defmethod>

  <!-- BUG#23154 prevent duplicate transactions in active event 
  Called by 
     Create_core_item_app.continue 
     Enter_core_item_app.enter_core_item_to_core_event 
     Process_core_event_app.recall_core_item
  -->
  Business_app.<defmethod _name='get_msg_if_trans_is_duplicate'>
    the_core_item = req
    <!-- _return_type Message or GEO-->

    <set>
      top_app = .<get_app/>
      ret_val = <GEO/>
      a_core_event = the_core_item.<get_core_event/>
      current_texts_for_duplicat_keys=<GEO/>
      texts_for_duplicat_keys=<GEO/>
      duplicate_msg= "A duplicate transaction has been identified in the active receipt. Posting duplicate transaction is not allowed."
    </set>

    <set>
      current_texts_for_duplicat_keys = .<get_texts_for_duplicate_keys> the_core_item = the_core_item </get_texts_for_duplicate_keys>
    </set>
    a_core_event.transactions.<for_each>
      <set> a_tran = value </set>
      <if>
        <and>
          a_tran.<is_not> the_core_item </is_not>
          a_tran.<get_status/>.<is> a_tran.completed </is>
        </and>
        <do>
          <set>
            texts_for_duplicat_keys = .<get_texts_for_duplicate_keys> the_core_item = a_tran </get_texts_for_duplicate_keys>
          </set>
        </do>
      </if>
      texts_for_duplicat_keys.<for_each>
        include="string_key"
        <set> a_text_for_duplicat_key = key </set>
        <if>
            current_texts_for_duplicat_keys.<has> a_text_for_duplicat_key </has>
          <do>
            <set>
              ret_val = <Message>
                id =  "duplicate_trans_error" 
                content = <message_warning_box>
                  message =duplicate_msg 
                </message_warning_box>
                actions = Message.Alert.actions
                condition = true
              </Message>
            </set>
            <break/>
          </do>

        </if>
      </for_each>
      <if>
        ret_val.<is_a> Message</is_a>
          <break/>
      </if>
    </for_each>
    <return> ret_val  </return>
  </defmethod>

  <!-- BUG#23154 prevent duplicate transactions in active event 
  Called by Business_app.get_msg_if_trans_is_duplicate 
   -->
  Business_app.<defmethod _name='get_texts_for_duplicate_keys'>
    the_core_item = req

    <set>
      the_texts_for_duplicat_keys=<GEO/>
      the_text_for_current_core_item = .<get_text_for_duplicate_keys> the_core_item = the_core_item </get_text_for_duplicate_keys>
    </set>

    <if>
      the_text_for_current_core_item.<is_not> ""</is_not>
      the_texts_for_duplicat_keys.<set_value> key=the_text_for_current_core_item value=""</set_value>
    </if>
    <if>
      the_core_item.<has> "core_items" </has>
      <do>
        the_core_item.core_items.<for_each>
          <set> a_btt = value </set>

          <if>
            <or>
              a_btt.<get_status/>.<is> a_btt.completed </is>
              a_btt.<is_selected/>
            </or>
            <do>
              <set>
                the_text_for_btt = .<get_text_for_duplicate_keys> the_core_item = a_btt </get_text_for_duplicate_keys>
              </set>

              <if>
                the_text_for_btt.<is_not> ""</is_not>
                the_texts_for_duplicat_keys.<set_value> key=the_text_for_btt value=""</set_value>
              </if>
            </do>
          </if>
        </for_each>
      </do>
    </if>
    the_texts_for_duplicat_keys
  </defmethod>

  <!-- BUG#23154 prevent duplicate transactions in active event 
  Called by Business_app.get_msg_if_trans_is_duplicate 
   -->
  Business_app.<defmethod _name='get_text_for_duplicate_keys'>
    the_core_item = req

    <set>
      duplicate_keys = the_core_item.<get>
        "duplicate_keys" if_missing="" lookup=true
      </get>
    </set>
    <set> text_for_duplicate_keys ="" </set>
    duplicate_keys.<for_each>
      <set>
        fieldtagname = value.<get> "tagname" if_missing=""</get>
      </set>
      <!-- BUG# 12207-->
      <set>
        fieldvalue = the_core_item.<get> fieldtagname if_missing=""</get>
      </set>

      <if>
        <and>
          fieldtagname.<is_not> "" </is_not>
          fieldvalue.<is_not> "" </is_not>
          fieldvalue.<is_not> opt </is_not>
        </and>
        <set>
          text_for_duplicate_keys =
          <join>
            text_for_duplicate_keys
            <join>
              fieldvalue
            </join>
            separator=","
          </join>
        </set>
      </if>
    </for_each>

    <return> text_for_duplicate_keys </return>
  </defmethod>
<!-- Bug 19641 UMN Working funds. Hooked into MikeO's deposit transfer check, 
     but rewrote as a function to avoid cloned code. Called by:
          Enter_core_item_app.enter_core_item_to_core_event, Process_core_event_app.recall_core_item
       * Don't add another transaction if existing one is exclusive 
       * Don't add exclusive transaction if there's existing trans
     Returns a message, or an empty GEO, so caller can test using .<is_a> Message </is_a>
-->
Business_app.<defmethod _name='get_msg_if_trans_is_exclusive'>
  the_core_item = req

  <set> 
    top_app = .<get_app/>
    ret_val = <GEO/> <!-- good practice to set a var for ret value cause can see it in trace_on -->
  </set>

  <!-- Bug #10646 Mike O - If there's a deposit transfer transaction involved, make sure it's alone -->
  <!-- Bug #11396 Mike O - Fixed so there's only one for loop -->
  <if>
    <and>
      the_core_item.<is_a> Transaction </is_a>
      <!-- Bug 19641 UMN can't assume that there's a process_core_event_app -->
      top_app.<has> "a_process_core_event_app" </has>
      top_app.a_process_core_event_app.<is_not> opt </is_not>
      top_app.a_process_core_event_app.<has> "a_core_event" </has>
    </and>
    <do>
      <set> existing_transaction = false </set>
      top_app.a_process_core_event_app.a_core_event.transactions.<for_each>
        <if>
          <!-- Bug 25684 MJO - Ignore voided and cancelled transactions -->
          <and>
            value.<get_status/>.<is_not> value.cancelled </is_not>
            value.<get_status/>.<is_not> value.voided </is_not>
            <or>
              value.<has_deposit_transfer/>
              value.is_exclusive.<is> true </is> <!-- Bug 19641 UMN Working funds. Don't add another transaction if existing one is exclusive -->
            </or>
          </and>
          <do>
            <!-- Bug 19641 UMN -->
            <set> 
              ret_val = <Message>
                id = <if> value.is_exclusive.<is> true </is> "exclusive_trans_error" else "deposit_transfer_error" </if>
                content = <message_warning_box>
                  message = <join> "Unable to add a new transaction when there is an active " value.description " transaction." </join>
                </message_warning_box>
                actions = Message.Alert.actions
                condition = true
              </Message>
            </set>
            <break/>
          </do>          
        </if>
        
        <if>
          value.status.<is_not> Transaction.cancelled </is_not>
          <set> existing_transaction = true </set>
        </if>
      </for_each>
      
      <if>
        <or>
          <and>
            the_core_item.<has_deposit_transfer/>
            existing_transaction
          </and>
          <and>
            <!-- Bug 19641 UMN Working funds. Don't add exclusive transaction if there's existing trans -->
            the_core_item.is_exclusive.<is> true </is> 
            top_app.a_process_core_event_app.a_core_event.transactions.<length/>.<more> 0 </more>
            
            <!-- Bug 21941 MJO - Ignore cancelled and voided transactions -->
            <!-- Bug 22344 MJO - Ignore fee and tax transactions too -->
            top_app.a_process_core_event_app.a_core_event.transactions.<for_each>
              <if>
                <and>
                  value.<get_status/>.<is_not> value.cancelled </is_not>
                  value.<get_status/>.<is_not> value.voided </is_not>
                  value.<has> "_fee_key" </has>.<not/>
                  value.<is_a> Transaction.Tax </is_a>.<not/>
                </and>
                <break> true </break>
              </if>
              false
            </for_each>
          </and>
        </or>
        <do>
          <set>
            ret_val = <Message>
              <!-- Bug 19641 UMN -->
              id = <if> the_core_item.is_exclusive.<is> true </is> "exclusive_trans_error" else "deposit_transfer_error" </if>
              content = <message_warning_box>
                message = <join> "Unable to add a new " the_core_item.description " transaction when there are other active transactions." </join>
              </message_warning_box>
              actions = Message.Alert.actions
              condition = true
            </Message>
          </set>
        </do>
      </if>
    </do>
  </if>
  <return> ret_val </return>
</defmethod>

  Business_app.<defmethod _name='get_status'>
    <!-- Change the if_missing to error when we initialize with active status -->
    .<get> "status" if_missing=false </get>
  </defmethod>

Business_app.<defmethod _name='show_in_tab'>
  true
</defmethod>

<!-- comment methods shared by multiple apps --> 
Business_app.<defmethod _name='get_form_data'>    <!-- SEE DOC -->
<!-- For STEPPING:
_local.<set_value> "_subject" my.0.tender_section_list.1 </set_value>
-->
  <set> data_class = .data_class </set>
  <if> data_class.<is_a> Transaction </is_a>
          <GEO/>
       data_class.<is_a> Tender </is_a>
         <do> 
          <set> top_app=.<get_app/> </set>
          <set> remaining_total=
            top_app.a_process_core_event_app.a_core_event.<get_tender_remaining_amount>
            tender_obj=.data_class
          </get_tender_remaining_amount>
          </set>
          <GEO> total=remaining_total </GEO>
         </do>
       else
         <GEO/> 
  </if>
 </defmethod>
 

<!-- No longer used.
Business_app.<defmethod _name='set_process_order'>
  <set> VSO = <vector/> </set>
  _subject.<for_each> include="string_key"
    <if> value.<is_a> Business_app </is_a>
           VSO.<insert> value._name </insert>
    </if>
  </for_each>
  .<set> _process_order = VSO.<sort/> </set>
  <set> db_field_order=Config.<generate_db_field_order> _subject </generate_db_field_order> </set>
  <if> db_field_order.<key_of> "_db_field_order" </key_of>.<not/> db_field_order.<insert> "_db_field_order" </insert> </if>
  .<set> _db_field_order=db_field_order </set>
 </defmethod>
 -->

Business_app.<defmethod _name='post_continue'> key=opt task=opt </defmethod>

  </do>