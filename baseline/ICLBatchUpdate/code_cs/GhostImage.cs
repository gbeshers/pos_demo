﻿using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using TranSuiteServices;
using System.Runtime.InteropServices;
using System.Data.SqlClient;

using CASL_engine;



namespace ICLBatchUpdate
{
  class GhostImage
  {
    public Bitmap ConvertToBitonal(Bitmap original)
    {
      Bitmap source = null;

      // If original bitmap is not already in 32 BPP, ARGB format, then convert
      if (original.PixelFormat != PixelFormat.Format32bppArgb)
      {
        source = new Bitmap(original.Width, original.Height, PixelFormat.Format32bppArgb);
        source.SetResolution(original.HorizontalResolution, original.VerticalResolution);
        using (Graphics g = Graphics.FromImage(source))
        {
          g.DrawImageUnscaled(original, 0, 0);
        }
      }
      else
      {
        source = original;
      }

      // Lock source bitmap in memory
      BitmapData sourceData = source.LockBits(new Rectangle(0, 0, source.Width, source.Height), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);

      // Copy image data to binary array
      int imageSize = sourceData.Stride * sourceData.Height;
      byte[] sourceBuffer = new byte[imageSize];
      Marshal.Copy(sourceData.Scan0, sourceBuffer, 0, imageSize);

      // Unlock source bitmap
      source.UnlockBits(sourceData);

      // Create destination bitmap
      Bitmap destination = new Bitmap(source.Width, source.Height, PixelFormat.Format1bppIndexed);

      // Lock destination bitmap in memory
      BitmapData destinationData = destination.LockBits(new Rectangle(0, 0, destination.Width, destination.Height), ImageLockMode.WriteOnly, PixelFormat.Format1bppIndexed);

      // Create destination buffer
      imageSize = destinationData.Stride * destinationData.Height;
      byte[] destinationBuffer = new byte[imageSize];

      int sourceIndex = 0;
      int destinationIndex = 0;
      int pixelTotal = 0;
      byte destinationValue = 0;
      int pixelValue = 128;
      int height = source.Height;
      int width = source.Width;
      int threshold = 500;

      // Iterate lines
      for (int y = 0; y < height; y++)
      {
        sourceIndex = y * sourceData.Stride;
        destinationIndex = y * destinationData.Stride;
        destinationValue = 0;
        pixelValue = 128;

        // Iterate pixels
        for (int x = 0; x < width; x++)
        {
          // Compute pixel brightness (i.e. total of Red, Green, and Blue values)
          pixelTotal = sourceBuffer[sourceIndex + 1] + sourceBuffer[sourceIndex + 2] + sourceBuffer[sourceIndex + 3];
          if (pixelTotal > threshold)
          {
            destinationValue += (byte)pixelValue;
          }
          if (pixelValue == 1)
          {
            destinationBuffer[destinationIndex] = destinationValue;
            destinationIndex++;
            destinationValue = 0;
            pixelValue = 128;
          }
          else
          {
            pixelValue >>= 1;
          }
          sourceIndex += 4;
        }
        if (pixelValue != 128)
        {
          destinationBuffer[destinationIndex] = destinationValue;
        }
      }

      // Copy binary image data to destination bitmap
      Marshal.Copy(destinationBuffer, 0, destinationData.Scan0, imageSize);

      // Unlock destination bitmap
      destination.UnlockBits(destinationData);

      // Dispose of source if not originally supplied bitmap
      if (source != original)
      {
        source.Dispose();    // More dodgy Dispose's from Min.  Change to using(...)
      }

      // Return
      return destination;
    }

    private void writeToGhostImage(Image templateImage, List<String> textLines, string micrText)
    {
      int iX = 10;
      int iY = 15;

      Graphics gt = Graphics.FromImage(templateImage);
      Font verbiageFont = new Font("Arial", 16, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      SolidBrush myBrush = new SolidBrush(Color.Black);
      StringFormat myFormat = new StringFormat();


      foreach (String textLine in textLines)
      {
        gt.DrawString(textLine, verbiageFont, myBrush, new System.Drawing.Point(iX, iY), myFormat);
        iY += 20;
      }

      iY += 190;

      Font micrFont = new Font("MICR", 14, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      gt.DrawString(micrText, micrFont, myBrush, new System.Drawing.Point(iX, iY), myFormat);

      //int dpiY = Convert.ToInt32(gt.DpiY);

      //iY = dpiY;
      //gt.DrawString(micrText, micrFont, myBrush, new System.Drawing.Point(iX, iY), myFormat);
    }


    public ImageCodecInfo GetEncoderInfo(string mimeType)
    {
      ImageCodecInfo[] encoders = ImageCodecInfo.GetImageEncoders();
      for (int j = 0; j < encoders.Length; j++)
      {
        if (encoders[j].MimeType == mimeType)
          return encoders[j];
      }

      throw new Exception(mimeType + " mime type not found in ImageCodecInfo");
    }


    /// <summary>
    /// Method to drive the creation of the TIFF, starting with a simple 
    /// TIFF template file.
    /// TODO: Figure out a way to create this TIFF without the pesky template file
    /// </summary>
    /// <param name="textLines"></param>
    /// <param name="micrText"></param>
    /// <param name="systemInterface"></param>
    /// <returns></returns>
    public byte[] ghostDriver(List<String> textLines, string micrText, GenericObject systemInterface)
    {
      ImageCodecInfo info = GetEncoderInfo("image/tiff");
      System.Drawing.Imaging.Encoder encSave = System.Drawing.Imaging.Encoder.Compression;
      EncoderParameters encoderParameters = new EncoderParameters(1);
      encoderParameters.Param[0] = new System.Drawing.Imaging.EncoderParameter(encSave, (long)System.Drawing.Imaging.EncoderValue.CompressionCCITT4);

      ImageConverter imageConverter = new System.Drawing.ImageConverter();

      string strTemplateFilePath = (string)systemInterface.get("TIFF_Template_File", "").ToString();
      if (!File.Exists(strTemplateFilePath))
        throw new Exception(
          string.Format("Deposit Ticket creation error:  Cannot find the template TIF file: {0}", strTemplateFilePath));

      Image templateFileImage = Image.FromFile(strTemplateFilePath);
      using (Bitmap templateImage = new Bitmap(new Bitmap(templateFileImage)))
      {

        // This creates a small black image.  TODO: improve this
        //templateImage = new Bitmap(new Bitmap(200,200));

        // Write the lines to the ghost image  
        writeToGhostImage(templateImage, textLines, micrText);

        // Convert templateImage to byte array
        using (Bitmap convertedTemplateBitmap = ConvertToBitonal(templateImage))
        {
          convertedTemplateBitmap.SetResolution(200, 200);
          //return imageToByteArrayViaTempFile(convertedTemplateBitmap, systemInterface, info, encoderParameters);
          return imageToByteArray(convertedTemplateBitmap, info, encoderParameters);
        }
      }

    }

    /// <summary>
    /// Instead of writing to file and reading back, do it all in memory.
    /// </summary>
    /// <param name="convertedTemplateBitmap"></param>
    /// <param name="info"></param>
    /// <param name="encoderParameters"></param>
    /// <returns></returns>
    private byte[] imageToByteArray(
        System.Drawing.Image convertedTemplateBitmap,
        ImageCodecInfo info,
        EncoderParameters encoderParameters)
    {
      using (MemoryStream ms = new MemoryStream())
      {
        convertedTemplateBitmap.Save(ms, info, encoderParameters);
        return ms.ToArray();
      }
    }

    /// <summary>
    /// Convert Image to byte array by writing to a temp file.
    /// </summary>
    /// <param name="convertedTemplateBitmap"></param>
    /// <param name="systemInterface"></param>
    /// <param name="info"></param>
    /// <param name="encoderParameters"></param>
    /// <returns></returns>
    [Obsolete("Try to use imageToByteArray() instead since this does not use a file", false)]
    private byte[] imageToByteArrayViaTempFile(
        System.Drawing.Image convertedTemplateBitmap,
        GenericObject systemInterface,
        ImageCodecInfo info,
        EncoderParameters encoderParameters)
    {
      string temppath = (string)systemInterface.get("Local_Temp_Directory", "").ToString();
      string tempfilename = Path.Combine(temppath, "TempBatchTicket.tif");

      // Save the Tiff so we can read it into a byte array
      convertedTemplateBitmap.Save(tempfilename, info, encoderParameters);

      // Read it back into a byte array
      byte[] tiffBytes = File.ReadAllBytes(tempfilename);
      return tiffBytes;
    }


    /// <summary>
    /// Scour the Tender table for any tenders configured for ICL and for this particular CORE file.
    /// Total up the amount and the count of the checks.
    /// If the tenderPredicate is a null string, it means that no checks 
    /// were configured so return 0's.
    /// </summary>
    /// <param name="geoFile"></param>
    /// <param name="numberOfChecks"></param>
    /// <param name="configSystemInterface"></param>
    /// <param name="tenderPredicate"></param>
    /// <returns></returns>
    private Decimal getTotalCheckAmount(GenericObject geoFile, out int numberOfChecks, GenericObject configSystemInterface, string tenderPredicate)
    {
      // Initialize
      numberOfChecks = 0;
      if (string.IsNullOrEmpty(tenderPredicate))
        return Decimal.Zero;

      int depfilenbr = Convert.ToInt32(geoFile.get("DEPFILENBR", 0));
      int depfileseq = Convert.ToInt32(geoFile.get("DEPFILESEQ", 0));

      String dataReaderSQL =
          string.Format("select SUM(AMOUNT), COUNT(AMOUNT) from TG_TENDER_DATA where DEPFILENBR = {0} AND DEPFILESEQ = {1} and TNDRID in {2} AND VOIDDT is NULL AND TYPEIND = '4'",
              depfilenbr, depfileseq, tenderPredicate);

      SqlConnection dataReaderConnection = new SqlConnection();
      SqlDataReader dataReader = null;
      try
      {
        GenericObject dbConnection = c_CASL.c_object("TranSuite_DB.Data.db_connection_info") as GenericObject;
        String conString = (String)dbConnection.get("db_connection_string");

        // Open a connection for the DataReader
        dataReaderConnection.ConnectionString = conString;
        dataReaderConnection.Open();

        SqlCommand dataReaderCommand = new SqlCommand(dataReaderSQL, dataReaderConnection);
        dataReader = dataReaderCommand.ExecuteReader();

        if (!dataReader.Read())
        {
          string msg = "Warning: getTotalCheckAmount: This query did not return a result: " + dataReaderSQL;
          Logger.Log(msg, "ICL Batch Update: Warning", "warn");
          return Decimal.Zero;
        }

        if (dataReader.IsDBNull(0))
        {
          string msg = string.Format("Warning: Cannot find any checks with a tender ID of '{0}' with this SQL {1}.  The amount and count will be 0. Is the 'Tender IDs To Include' configured correctly?", tenderPredicate, dataReaderSQL);
          Logger.Log(msg, "ICL Batch Update: Warning", "warn");
          numberOfChecks = 0;
          return Decimal.Zero;
        }
        else
        {
          double totalAmount = dataReader.GetDouble(0);

          numberOfChecks = dataReader.GetInt32(1);
          return Convert.ToDecimal(totalAmount);   // Bug 19713
        }
      }
      catch (Exception e)
      {
        string msg = "Error: readSlipNumber: Cannot run or parse this query: " + dataReaderSQL + " Error: " + e.ToString();
        Logger.Log(msg, "ICL Batch Update", "err");
        return Decimal.Zero;
      }
      finally
      {
        if (dataReader != null && !dataReader.IsClosed)
          dataReader.Close();
        dataReaderConnection.Close();
      }
    }

    /// <summary>
    /// Synthesize a deposit ticket giving a deposit slip number, total amount & number of checks, 
    /// account number, and timestamp.  
    /// The timestamp must be the timestamp when the deposit was made, not when the batch update was run.
    /// The user should be the user who created the deposit, not who ran batch update.
    /// </summary>
    /// <param name="geoFile"></param>
    /// <param name="configSystemInterface"></param>
    /// <param name="depositSlipNumber"></param>
    /// <param name="pConfigSystemInterface"></param>
    /// <param name="tenderPredicate"></param>
    /// <param name="numberOfChecks">Out parameter to indicate the number of checks found</param>  // Bug 15521
    /// <returns></returns>
    public ICL.CheckDetail setDepositTicket(
        GenericObject geoFile,
        GenericObject configSystemInterface,
        string depositSlipNumber,
        GenericObject pConfigSystemInterface,
        string tenderPredicate,
        string createrUserId,
        DateTime postDt,
        out int numberOfChecks)     // Bug 15521
    {
      ICL.CheckDetail checkDetail = new ICL.CheckDetail();
      checkDetail.dITEM_AMOUNT = getTotalCheckAmount(geoFile, out numberOfChecks, configSystemInterface, tenderPredicate);

      checkDetail.dtFILE_POST_DATE = postDt;

      // Bug 26684 Save the string, not numeric, workgroup ID
      checkDetail.workgroupID = ICLUtil.getWorkGroupID(geoFile);

      string iPaymentFilename = geoFile.get("FILENAME", "0") as string;  // Stored as a string in the file Geo
      checkDetail.iFILENBR = misc.Parse<long>(iPaymentFilename);   // Deposit file number or CORE file number
      checkDetail.iEventNbr = -1;     // Bug 21327 No event number
      checkDetail.iTndrNbr = -1;      // Bug 21327 No tender number

      string bankAccount = configSystemInterface.get("Payor_Account_Number", "").ToString();


      // Tentative setup for the 61 record
      checkDetail.sCreditRecordSerialNumber = depositSlipNumber;

      // Let the ICL module know that this check image is the deposit slip
      checkDetail.bIsDepositSlip = true;

      // Set the Routing number and check digit.  Default to unset.  
      // Use the special Deposit Ticket Routing number, not the regular routing number
      string strDepositTicketRountingNbr = configSystemInterface.get("Deposit_Ticket_Routing_Number", "").ToString();
      if (strDepositTicketRountingNbr.Length == 9)
      {
        checkDetail.sROUTING_NUMBER = strDepositTicketRountingNbr.Substring(0, 8);
        checkDetail.sCHECK_DIGIT = strDepositTicketRountingNbr.Substring(8, 1);
      }
      else
      {
        // The Routing number *should* be nine, so I am only guessing here
        checkDetail.sROUTING_NUMBER = strDepositTicketRountingNbr;
        // Bug 17864
        //if (strDepositTicketRountingNbr.Length > 0)
        //  checkDetail.sCHECK_DIGIT = strDepositTicketRountingNbr.Substring(strDepositTicketRountingNbr.Length - 1, 1);
        // For KeyBank they want this to be blank if the number has a length of 8 instead of 9
        checkDetail.sCHECK_DIGIT = " ";
        // End bug 17864
      }
      checkDetail.sECE_SEQUENCE_NUMBER = ICLUtil.constructEceSeqNumber(iPaymentFilename);

      // Bug 24223 : Although it would be tempting to pull the On-Us field from new custom fields,
      // we cannot do it here since this is called from ProcessDeposit which does not have access
      // to a particular tender and the GetMICRFieldFromCustomField needs the GeoTender geo.
      checkDetail.sON_US = ICLUtil.constructBBT_OnUs("13", configSystemInterface);

      List<string> textLines = new List<string>();
      textLines.Add("Electronic Deposit Ticket");
      textLines.Add("KeyBank");
      textLines.Add("Cleveland Clinic Foundation       Slip " + depositSlipNumber);
      textLines.Add("Account Number " + bankAccount);

      textLines.Add(string.Format("Created on {0} at {1} by {2}", postDt.ToString("MM-dd-yyyy"), postDt.ToString("HH:mm"), createrUserId));

      textLines.Add(string.Format("Deposited {0} checks totaling {1:C}", numberOfChecks, checkDetail.dITEM_AMOUNT));

      // Write the special MICR in the MICR font
      string micrText = createMICRText(pConfigSystemInterface, strDepositTicketRountingNbr, checkDetail.sON_US, checkDetail.dITEM_AMOUNT);


      byte[] frontImageData = ghostDriver(textLines, micrText, pConfigSystemInterface);

      checkDetail.iFrontImageLen = frontImageData.Length;
      checkDetail.FrontImage = frontImageData;

      // All the back image has to say is "Back of Electronic Deposit Ticket"
      List<string> backText = new List<string>();
      backText.Add("Back of Electronic Deposit Ticket");
      byte[] backImageData = ghostDriver(backText, null, pConfigSystemInterface);

      checkDetail.iBackImageLen = backImageData.Length;
      checkDetail.BackImage = backImageData;

      return checkDetail;
    }


    /// <summary>
    /// Create an approximation of what is needed in the MICR font,
    /// using the special MICR punctuation marks for Transit, Dash, and Amount.
    /// We have to use a normal ascii character for the punctuation and that 
    /// gets translated when in the MIRC font.
    /// </summary>
    /// <param name="configSystemInterface"></param>
    /// <param name="depositTicketRoutingNumber">The special deposit ticket routing number</param>
    /// <param name="sOnUs"></param>
    /// <param name="amount"></param>
    /// <returns></returns>
    private string createMICRText(GenericObject configSystemInterface, string depositTicketRoutingNumber, string sOnUs, Decimal amount)
    {
      const char TRANSIT = 'a';
      const char DASH = 'd';
      const char AMOUNT = 'b';
      //long longAmount = Convert.ToInt64((amount) * 100);
      object trunc = Math.Truncate((amount * 100.00M) + 0.50M);     // Bug 19713: Let's keep it all decimals to avoid round off issues
      long longAmount = Convert.ToInt64(trunc);
      string stringAmount = (Convert.ToString(longAmount)).PadLeft(10);
      string bankAccount = configSystemInterface.get("Payor_Account_Number", "").ToString();

      StringBuilder sb = new StringBuilder();
      sb.Append(TRANSIT).Append(depositTicketRoutingNumber.PadLeft(9)).Append(TRANSIT)   // Bracket with the Transit symbol
          .Append(DASH).Append(bankAccount).Append(DASH).Append("13".PadRight(6))     // Bracket with Dash symbol
          .Append(AMOUNT).Append(stringAmount).Append(AMOUNT);                 // Bracket with the Amount field

      return sb.ToString();
    }
  }
}
