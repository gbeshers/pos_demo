﻿
using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using CASL_engine;
using System.IO;
using System.Text;
using ICL;
using ftp;



namespace ICLBatchUpdate
{
  /// <summary>
  /// ICL batch update.  This module connects iPayment to the ICL module. 
  /// </summary>
  /// 
  public class ICLBatchUpdateSI : AbstractBatchUpdate
  {

    /// <summary>
    /// Flag to not delete the temp files after sending.
    /// </summary>
    private bool m_debug = false;

    /// <summary>
    /// Set in ProcessCoreEvents and use in WriteTrailer for email and other values
    /// </summary>
    public GenericObject m_ConfigSystemInterface = null;

    /// <summary>
    /// Header information
    /// </summary>
    private ICL.MiscInfo m_miscInfo = null;

    /// <summary>
    /// Set this list of checks in ProcessCoreEvent and output them in WriteTrailer.
    /// </summary>
    private List<ICL.CheckDetail> m_lstCheck = new List<ICL.CheckDetail>();

    /// <summary>
    /// Keep track of CORE files that have an event with a null check
    /// Bug 24223
    /// </summary>
    private HashSet<Int64> m_defectiveChecks = new HashSet<Int64>();

    /// <summary>
    /// Path to temporary file.  Set here so we can delete file in ProcessCleanup.
    /// </summary>
    private string m_strTempPathName = "";

    /// <summary>
    /// The filename that is constructed for the ICL file based on config settings and date.
    /// </summary>
    private string m_strICLFileName = "";

    /// <summary>
    /// Accumulator for Deposit Tickets for Activity Log.  Bug 15126.
    /// </summary>
    private StringBuilder depositTicketList = null;

    /// <summary>
    /// Accumulator for Core Files in the batch update for Activity Log.  Bug 15126.
    /// </summary>
    private StringBuilder coreFileList = null;

    /// <summary>
    /// Track the number of checks for the whole batch update.
    /// If the number is 0, do not generate an output ICL file.
    /// Bug 15521.
    /// Had to monkey with this for Bug 22678 since I need
    /// to generate a file without the deposit slip for JP Morgan Chase
    /// </summary>
    private int m_totalNumberOfDeposits = 0;

    /// <summary>
    /// Bug 15126: FT. Collect detailed information for Activity Log
    /// </summary>
    private StringBuilder m_detailList = new StringBuilder();

    /// <summary>
    /// Flag to tell SendFile that the file was created correctly and is ready to send.
    /// </summary>
    private bool m_SendFile = false;

    /// <summary>
    /// If ProcessCoreEvent fails, do not try to WriteTrailer
    /// </summary>
#if UNUSED      // TTS#24695: Is the fact that this is unused a bug?  GMB.
    private bool m_WriteTrailer = true;
#endif

    /// <summary>
    /// Tells Cleanup whether to delete temp file.
    /// Bug 15659
    /// </summary>
    private bool m_deleteTempFile = true;

    /// <summary>
    /// This artifact from Liberty currently does nothing
    /// except accumulate messages, but these messages are 
    /// never sent.
    /// TODO: log or email these results
    /// </summary>
    public StringBuilder m_writeTrailerSummaryComments = new StringBuilder();

    public static int m_ECESequenceNumberCounter = 0;


    /// <summary>
    /// Bug 18994: Turn off prefix
    /// </summary>
    private bool m_WellsFargoFormat = false;


    public ICLBatchUpdateSI()
    {
      depositTicketList = new StringBuilder();  // Bug 15126.
      coreFileList = new StringBuilder();       // Bug 15126.
      m_totalNumberOfDeposits = 0;                  // Bug 15521
      m_SendFile = false;
#if UNUSED
      m_WriteTrailer = true;
#endif
      m_deleteTempFile = true;                    // Bug 15659
      m_writeTrailerSummaryComments = new StringBuilder();
      m_ECESequenceNumberCounter = 0;
    }

    public override AbstractBatchUpdate GetInstance(GenericObject sysInt)
    {
      return base.GetInstance(sysInt);
    }


    public override string GetCASLTypeName()
    {
      return "ICLBatchUpdate";
    }




    public override bool Project_ProcessCoreEvent(string strSystemInterface, GenericObject pEventGEO, GenericObject pConfigSystemInterface/*Access to the Config System Interface*/, GenericObject pFileGEO/*BUG# 8182 DH*/, object pProductLevelMainClass/*BUG# 8224 DH*/, string strCurrentFile/*BUG# 8224 DH*/, ref eReturnAction ReturnAction/*Bug 15464*/)
    {
      CASL_engine.BatchUpdate_ProductLevel pBatchUpdate_ProductLevel = (BatchUpdate_ProductLevel)pProductLevelMainClass;

      string strError = "Processing Core Event";
      Logger.Log(strError, "ICLBatchUpdate", "info");
      strError = "";

      // Get a set of the tenders attached to the ICL system interface
      HashSet<string> tenderSet = getTenderSet(pConfigSystemInterface);

      GenericObject geoFile = new GenericObject();
      GenericObject currentTender = new GenericObject();

      try
      {
        geoFile = pFileGEO.get(0, null) as GenericObject;

        if (m_ConfigSystemInterface == null)
          m_ConfigSystemInterface = pConfigSystemInterface;

        // Bug 22678: Moved the miscInfo creation here so it is available immediatly
        m_miscInfo = createMiscInfo(pConfigSystemInterface);

        // Bug 22678: Make the Wells Fargo format configurable.  This format skips the record length prefix in field 0
        string useWellsFargoFormat = pConfigSystemInterface.get("Wells_Fargo_Format", "false").ToString();
        m_WellsFargoFormat = useWellsFargoFormat == "True" ? true : false;

        GenericObject geoTrans;
        if (pEventGEO.has("TRANSACTIONS"))
          geoTrans = pEventGEO.get("TRANSACTIONS") as GenericObject;
        else
          geoTrans = new GenericObject();

        GenericObject geoTenders;
        if (pEventGEO.has("TENDERS"))
          geoTenders = pEventGEO.get("TENDERS") as GenericObject;
        else
          geoTenders = new GenericObject();


        int iCountofTenders = geoTenders.getIntKeyLength();

        GenericObject databaseConnection = (GenericObject)c_CASL.c_object("TranSuite_DB.Data.db_connection_info");

        // GET Tenders
        for (int i = 0; i < iCountofTenders; i++)
        {
          currentTender = (GenericObject)geoTenders.get(i);

          // Get the tender class for filtering
          // Filter out any tenders that are not checks (i.e. not configured with ICL)
          GenericObject geoTenderClass;
          // Bug 19388: Ignore bad config
          try
          {
            geoTenderClass = (GenericObject)((GenericObject)c_CASL.c_object("Tender.of")).get(currentTender.get("TNDRID", "").ToString(), "");
          }
          catch (Exception)
          {
            strError = string.Format("Project_ProcessCoreEvent: cannot read the Tender casl class for Tender: {0}", currentTender.get("TNDRID", "No TNDRID"));
            Logger.Log(strError, "ICLBatchUpdate", "err");
            continue;
          }
          // Amazingly enough, the "id" field is not set, just the "TNDR" field.
          //string tndrid = geoTender.get("TNDRID", "Not found") as string;
          //if (!tenderSet.Contains(tndrid))      // Mega hack!
          //  continue;
          // From the ACH code: Don't process tender without SI
          if ((!IsValidICLTender(geoFile, currentTender, geoTenderClass, pConfigSystemInterface)))
            continue;

          // Read the images from the database
          GenericObject geoImageData = GetTenderImageData(currentTender, databaseConnection);

          ICL.CheckDetail checkDetail = setCheckDetail(pConfigSystemInterface, geoFile, currentTender, geoImageData);
          m_lstCheck.Add(checkDetail);
        }


      }
      catch (Exception e)
      {
        // Bug 23587: Enhanced error message
        string iPaymentFilename = geoFile.get("FILENAME", "0") as string;  // Stored as a string in the file Geo
        var eventNbr = currentTender.get("EVENTNBR", "0");
        var tenderNbr = currentTender.get("TNDRNBR", "0");
        decimal amt = getAmount(currentTender, "AMOUNT");
        string checkNumber = currentTender.get("CC_CK_NBR", "") as string;


        strError = string.Format("Project_ProcessCoreEvent: File: {0}, Event: {1}, Tender: {2}, amt: {3}, check number: {4}, trace: {5} ",
            iPaymentFilename,
            eventNbr,
            tenderNbr,
            amt,
            checkNumber,
            e.ToString());
        // End Bug 23587

        // Bug 24743: Special formatting for the activity log
        // Unfortunately this causes redundancy in message but I guess this is OK
        string actlogError = "Error:" + misc.cleanUpDetailsForActLog(
                string.Format("{5}, Project_ProcessCoreEvent File {0}, Event {1}, Tender {2}, amt {3}, check number {4}",
                    iPaymentFilename,
                    eventNbr,
                    tenderNbr,
                    amt,
                    checkNumber,
                    e.Message)
                );

        Logger.Log(strError, "ICLBatchUpdate", "err");
        pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError(strCurrentFile, strSystemInterface, strError);

        // Bug 24743: Send error message to the activity log
        string userID = pBatchUpdate_ProductLevel.m_strUserName;
        try
        {
          misc.CASL_call("a_method", "actlog", "args",
            new GenericObject("user", userID, "app", "Portal", "action", "ICL Batch Update", "summary", "Error", "detail",
              actlogError));
        }
        catch (Exception actlogExp)
        {
          Logger.Log($"Project_CoreEvent: Cannot send error message to the activity log: {actlogExp.ToString()}", "ICLBatchUpdate", "err");
          throw;
        }
        // End 24743

        // Bug 24223 Mark this core file as defective so it is not included in the output
        Int64 iFILENBR = misc.Parse<Int64>(iPaymentFilename);   // Deposit file number or CORE file number
        m_defectiveChecks.Add(iFILENBR);
        // End Bug 24223

        // In CORE mode, we should still send the ICL but without this CORE file
        string sMode = (string)(m_ConfigSystemInterface.get("processing_mode", "SYSTEM"));
        if (sMode == "SYSTEM")
        {
#if UNUSED
          m_WriteTrailer = false;
#endif
          m_SendFile = false;
        }
        return false;           // Bug 19288: If there is any error, abort immediatly.  Do not continue and create an defective ICL file
      }

      strError = "";

      return true;
    }

    /// <summary>
    /// Check if tender is a check and has the ICL Batch Updte system interface assigned to it.
    /// </summary>
    /// <param name="geoTenderClass"></param>
    /// <param name="SysInt"></param>
    /// <returns></returns>
    private bool IsValidICLTender(
        GenericObject geoFile,          // Bug 23587: for error msg
        GenericObject currentTender,    // Bug 23587: for error msg
        GenericObject geoTenderClass,
        GenericObject SysInt)
    {
      decimal amt = getAmount(currentTender, "AMOUNT");

      // Tender is valid only if it is a check and has ICL SI assigned to it.
      try
      {
        GenericObject geoSystemInterfaces = geoTenderClass.get("system_interfaces") as GenericObject;

        GenericObject geoParent = geoTenderClass.get("_parent") as GenericObject;
        // Bug 25686 MJO - Skip negative checks
        // IPAY-261 - skip zero amount transaction also
        return geoSystemInterfaces.has(SysInt) && (geoParent.get("type", "").ToString() == "Check") && amt > 0.00M;
      }
      // Bug 23587: Catch weird or misconfigured or mixed check types.
      catch (Exception e)
      {
        string iPaymentFilename = geoFile.get("FILENAME", "0") as string;  // Stored as a string in the file Geo
        var eventNbr = currentTender.get("EVENTNBR", "0");
        var tenderNbr = currentTender.get("TNDRNBR", "0");
        string strError = string.Format("Project_ProcessCoreEvent: IsValidICLTender(): Error Tender is not a proper scanned check: File: {0}, Event: {1}, Tender: {2}, amt: {3}, Error: {4} ",
            iPaymentFilename,
            eventNbr,
            tenderNbr,
            amt,
            e.Message);
        Logger.Log(strError, "ICLBatchUpdate", "err");
        return false;
      }
      // End bug 23587
    }



    /// <summary>
    /// Create a HashSet of the tenders associated with the ICL system interface.
    /// Use the HashSet for rapid Contains lookup.
    /// For some odd reason, the "id" field has the right value
    /// while the TNDRID does not.
    /// </summary>
    /// <param name="pConfigSystemInterface"></param>
    /// <returns>HasSet of Tenders in attached to the ICL system interface</returns>
    private HashSet<string> getTenderSet(GenericObject pConfigSystemInterface)
    {
      HashSet<string> tenderSet = new HashSet<string>();

      try
      {
        GenericObject tenders = pConfigSystemInterface.get("tenders") as GenericObject;

        foreach (object tenderObj in tenders.Values)
        {
          if (tenderObj is GenericObject)
          {
            GenericObject tenderGeo = tenderObj as GenericObject;
            // For some weird reason, TNDRID does not have the correct value, but id does!
            if (!tenderGeo.has("id"))
              continue;
            string tenderID = tenderGeo.get("id") as string;
            tenderSet.Add(tenderID);
          }
        }

        return tenderSet;
      }
      catch (Exception e)
      {
        Logger.Log($"Failure in getTenderSet: {e.ToString()}", "ICLBatchUpdate", "err");
        return tenderSet;
      }
    }

    /// <summary>
    /// Main functionality for this batch update.
    /// Set the check details before sending off to the ICL module to generate the ICL file.
    /// </summary>
    /// <param name="geoFile"></param>
    /// <param name="geoTender"></param>
    /// <param name="geoImageData"></param>
    /// <returns></returns>
    private ICL.CheckDetail setCheckDetail(
      GenericObject pConfigSystemInterface,           // Bug 17864
      GenericObject geoFile,
      GenericObject geoTender,
      GenericObject geoImageData)
    {
      ICL.CheckDetail checkDetail = new ICL.CheckDetail();
      checkDetail.dITEM_AMOUNT = getAmount(geoTender, "AMOUNT");

      // Not sure whether we should use the effective date 
      if (geoFile.has("EFFECTIVEDT"))
        checkDetail.dtFILE_POST_DATE = (DateTime)geoFile.get("EFFECTIVEDT");
      else
        checkDetail.dtFILE_POST_DATE = DateTime.Now;

      // Bug 26684 Save the string, not numeric, workgroup ID
      checkDetail.workgroupID = ICLUtil.getWorkGroupID(geoFile);

      string iPaymentFilename = geoFile.get("FILENAME", "0") as string;  // Stored as a string in the file Geo
      checkDetail.iFILENBR = misc.Parse<long>(iPaymentFilename);   // Bug 25279: Switch to long to avoaid overflow. Deposit file number or CORE file number
      checkDetail.iEventNbr = Convert.ToInt32(geoTender.get("EVENTNBR", "0"));      // Bug 21327
      checkDetail.iTndrNbr = Convert.ToInt32(geoTender.get("TNDRNBR", "0"));        // Bug 21327

      // Get the check number and bank account directly from the TENDER table, not from the MICR
      string bankAccount = getBankAccountNumber(geoTender);
      string checkNumber = geoTender.get("CC_CK_NBR", "") as string;
      if (string.IsNullOrWhiteSpace(bankAccount))
      {
        string msg = string.Format("Cannot process this batch update file.  No bank account found in check in CORE Filenbr: {0}, Event: {1}, Tender: {2}, Check: {3}",
            iPaymentFilename, checkDetail.iEventNbr, checkDetail.iTndrNbr, checkNumber);
        throw new Exception(msg);
      }


      // Set the Routing number and check digit.  Default to unset.  Cloned from ACHBatchUpdate
      string strBankRountingNbr = geoTender.get("BANKROUTINGNBR", "").ToString();
      if (strBankRountingNbr.Length == 9)
      {
        checkDetail.sROUTING_NUMBER = strBankRountingNbr.Substring(0, 8);
        checkDetail.sCHECK_DIGIT = strBankRountingNbr.Substring(8, 1);
      }
      else
      {
        // The Routing number *should* be nine, so I am only guessing here
        checkDetail.sROUTING_NUMBER = strBankRountingNbr;
        if (strBankRountingNbr.Length > 0)
          checkDetail.sCHECK_DIGIT = strBankRountingNbr.Substring(strBankRountingNbr.Length - 1, 1);
      }

      // Bug 24223: Use 4 digits for the DEPFILESEQ number
      long depfilenbr = Convert.ToInt32(geoTender.get("DEPFILENBR", "0"));
      long depfileseq = Convert.ToInt32(geoTender.get("DEPFILESEQ", "0"));

      checkDetail.sECE_SEQUENCE_NUMBER = ICLUtil.constructEceSeqNumberWith4digitDepfileseq(depfilenbr, depfileseq);
      // End Bug 24223

      // Bug 17864
      checkDetail.sReturnAcceptanceIndicator = pConfigSystemInterface.get("Return_Acceptance_Indicator", "0") as string;
      checkDetail.sArchiveTypeIndicator = pConfigSystemInterface.get("Archive_Type_Indicator", "B") as string;
      // End Bug 17864

      int iCountofRec = geoImageData.getIntKeyLength();
      if (iCountofRec == 0)
        return checkDetail;

      setCheckImages(geoTender, checkDetail, geoImageData, bankAccount, checkNumber);

      return checkDetail;
    }


    /// <summary>
    /// Grab the check image, front and back.
    /// Then read the MICR data.
    /// </summary>
    /// <param name="checkDetail"></param>
    /// <param name="geoImageList"></param>
    /// <param name="bankAccount"></param>
    /// <param name="checkNumber"></param>
    private void setCheckImages(
      GenericObject geoTender,
      ICL.CheckDetail checkDetail,
      GenericObject geoImageList,
      string bankAccount,
      string checkNumber)
    {
      int iCountofRec = geoImageList.getIntKeyLength();
      for (int j = 0; j < iCountofRec; j++)
      {
        GenericObject geoImage = (GenericObject)geoImageList.get(j);
        if (geoImage.has("DOC_IMAGE"))
        {
          byte[] imageData = (byte[])geoImage.get("DOC_IMAGE");
          int imageLen = imageData.Length;
          if (imageLen == 0)
          {
            // Bug 24223: Flag empty image fields
            string msg = string.Format("Empty check image found for file: {0}, Event: {1}, Tender: {2}", checkDetail.iFILENBR, checkDetail.iEventNbr, checkDetail.iTndrNbr);
            throw new Exception(msg);
          }
          int seqNbr = Convert.ToInt32(geoImage.get("SEQ_NBR", "0"));
          if (seqNbr == 0)
          {
            checkDetail.iFrontImageLen = imageLen;
            checkDetail.FrontImage = imageData;
          }
          else
          {
            checkDetail.iBackImageLen = imageLen;
            checkDetail.BackImage = imageData;
          }

          getMICRFields(geoTender, checkDetail, geoImage, bankAccount, checkNumber);

          // Obsolete: do not use the Liberty bank but rather the account on the check
          //checkDetail.sON_US = ICLUtil.constructBBT_OnUs(checkNumber, m_ConfigSystemInterface);

        }
        else
        {
          // Bug 24223: Check from missing image
          string msg = string.Format("Null check image found for file: {0}, Event: {1}, Tender: {2}, Image Type: {3}", checkDetail.iFILENBR, checkDetail.iEventNbr, checkDetail.iTndrNbr, geoImage.get("IMAGING_TYPE", ""));
          throw new Exception(msg);
        }
      }
    }


    /// <summary>
    /// Read the encrypted MICR line from the Image Geo (read from the database)
    /// and decrypt it.
    /// Then send it off to try to parse out the On-Us and, if available,
    /// the Aux On-Us (Business checks only).
    /// </summary>
    /// <param name="checkDetail"></param>
    /// <param name="geoImageData"></param>
    /// <param name="bankAccount"></param>
    /// <param name="checkNumber"></param>
    private void getMICRFields(
      GenericObject geoTender,
      ICL.CheckDetail checkDetail,
      GenericObject geoImageData,
      string bankAccount,
      string checkNumber)
    {
      // See Dan Hofman's enhancement to the MICR handling: Bug 24736
      if (geoImageData.has("IMAGING_TYPE"))
      {
        string imagingType = geoImageData.get("IMAGING_TYPE") as string;
        if (imagingType.Contains("-"))
        {
          // Found Dan's enhancement
          if (imagingType.EndsWith("CHECK - FRONT"))
          {
            Logger.LogTrace("Found CHECK-FRONT marker in IMAGING_TYPE field", "ICLBatchUpdate.readOnUsFromMICR");
          }
          else
          {
            // If this is not the check front, do not try to process the MICR
            return;
          }
        }
      }
      // End Bug 24736

      // Bug 24223: We can get the On-us without parsing the MIFCR so do this now
      string micrOnUs = ICLUtil.GetEncryptedMICRFieldFromCustomField(geoTender, "MICR_ON_US");
      if (micrOnUs == null)
        checkDetail.sON_US = constructOnUsFromTenderFields(bankAccount, checkNumber);
      else
        checkDetail.sON_US = micrOnUs;


      if (geoImageData.has("COMPLETE_MICR_LINE"))
      {
        string RAWMICR = geoImageData.get("COMPLETE_MICR_LINE") as string;    // Encrypted MICR from database
        if (!string.IsNullOrEmpty(RAWMICR) && RAWMICR.Length % 4 == 0) //HX: IPAY-692 The length of a base64 encoded string is always a multiple of 4.
        {
          try
          {
            string secret_key = Crypto.get_secret_key();
            byte[] content_bytes = Convert.FromBase64String(RAWMICR);
            byte[] decrypted_bytes = Crypto.symmetric_decrypt("data", content_bytes, "secret_key", secret_key);
            string decryptedMICR = System.Text.Encoding.Default.GetString(decrypted_bytes);

            // The RDM uses uppercase T for Transit Code and O for On-us code, so translate to lower
            decryptedMICR = decryptedMICR.Replace('T', 't');
            decryptedMICR = decryptedMICR.Replace('O', 'o');

            decryptedMICR = decryptedMICR.TrimStart();      // Bug 19347
            string micAuxrOnUs = ICLUtil.GetEncryptedMICRFieldFromCustomField(geoTender, "MICR_AUX_ON_US");
            if (micAuxrOnUs == null)
              checkDetail.sAUXILIARY_ON_US = parseAuxOnUSFromMICR(decryptedMICR);
            else
              checkDetail.sAUXILIARY_ON_US = micAuxrOnUs;

            string micEPC = ICLUtil.GetMICRFieldFromCustomField(geoTender, "MICR_EPC");
            if (micEPC == null)
              checkDetail.sEXT_PROCESSING_CODE = parseEPCFromMICR(decryptedMICR);
            else
              checkDetail.sEXT_PROCESSING_CODE = micEPC;

          }
          catch (Exception e)
          {
            // See the all-important TTS 9831 and 14734 and the stealth fix for the encryption.
            // This could be a bug when the MICR was encrypted in Create_core_item_app_f_controller.casl 
            // NOT here where it is decrypted.
            // Bug 21327
            string msg = string.Format(@"readOnUsFromMICR: Cannot decrypt the MICR line: Error: {0}, CORE filename plus sequence number: {1}, check number: {2}, amount: {3}, RAWMICR: '{4},' filename: {5}, eventnbr: {6}, tndrnbr: {7}, Stack: {8}",
                e.Message, checkDetail.sECE_SEQUENCE_NUMBER, checkNumber, checkDetail.dITEM_AMOUNT, RAWMICR,
                checkDetail.iFILENBR,
                checkDetail.iEventNbr,
                checkDetail.iTndrNbr,
                e.ToString()
              );
            Logger.Log(msg, "ICLBatchUpdate", "err");
          }
        }
        else
        {
          //Hx IPAY-692  I need to log the problem
          if (RAWMICR == "N/A")
          {
            string msg = string.Format(@"readOnUsFromMICR: RAW MICR set as (N/A), CORE filename plus sequence number: {0}, check number: {1}, amount: {2}, RAWMICR: '{3},' filename: {4}, eventnbr: {5}, tndrnbr: {6}",
              checkDetail.sECE_SEQUENCE_NUMBER, checkNumber, checkDetail.dITEM_AMOUNT, RAWMICR,
              checkDetail.iFILENBR,
              checkDetail.iEventNbr,
              checkDetail.iTndrNbr
            );

            Logger.Log(msg, "ICLBatchUpdate", "err");
          }
          else
          {
            string msg = string.Format(@"readOnUsFromMICR: Invalid RAWMICR, CORE filename plus sequence number: {0}, check number: {1}, amount: {2}, RAWMICR: '{3},' filename: {4}, eventnbr: {5}, tndrnbr: {6}",
               checkDetail.sECE_SEQUENCE_NUMBER, checkNumber, checkDetail.dITEM_AMOUNT, RAWMICR,
               checkDetail.iFILENBR,
               checkDetail.iEventNbr,
               checkDetail.iTndrNbr
             );

            Logger.Log(msg, "ICLBatchUpdate", "err");
          }

        }
      }
    }


    /// <summary>
    /// Simplified way to get the On-Us.
    /// Bug 24223
    /// </summary>
    /// <param name="bankAccount"></param>
    /// <param name="checkNumber"></param>
    /// <returns></returns>
    private string constructOnUsFromTenderFields(string bankAccount, string checkNumber)
    {
      string onUs = bankAccount + m_miscInfo.onUsSymbol + checkNumber;
      return onUs;
    }


    /// <summary>
    /// External Processing Code Field (EPC): 
    /// An optional field, using a single digit, 
    /// for the special purposes as authorized by the Accredited Standards Committee X9B.  
    /// It is located to the left of the routing field on a check.
    /// 
    /// For personal checks, the format should be:
    /// EPC 't' Routing 't' On-Us (which can contain a 'o')
    /// For business checks, it should be
    /// 'o' Aux On-us 'o' EPC 't' Routing 't' On-Us (which can contain a 'o')
    /// 
    /// </summary>
    /// <param name="decryptedMICR"></param>
    /// <returns></returns>
    private string parseEPCFromMICR(string decryptedMICR)
    {
      if (decryptedMICR.Length == 0)
        return " ";

      // Bug 17864: the PerTech uses ':' instead of 't' and 'o'
      if (!decryptedMICR.Contains("t") && !decryptedMICR.Contains("o"))
      {
        if (decryptedMICR.Contains(":"))
        {
          string[] parts = decryptedMICR.Split(':');
          if (parts.Length <= 3)
          {
            // Personal check with 3 parts: no EPC
            return " ";
          }
          else
          {
            // Business check with 4 or more parts
            return parts[1];
          }
        }
      }
      // End Bug 17864


      // If this is a business check, then the EPC is between the 
      // Aux-OnUs and the Routing field.
      if (decryptedMICR[0] == 'o')
      {
        string[] micrFields = decryptedMICR.Split('t');
        // The first field should contain the Aux On-Us and EPC
        if (micrFields.Length == 0)
          return " ";
        string auxOnUsEPC = micrFields[0];
        if (auxOnUsEPC.Length == 0)
          return " ";
        // If it ends with 'o', then no EPC
        if (auxOnUsEPC.EndsWith("o"))
          return " ";
        else
          return auxOnUsEPC.Substring(auxOnUsEPC.Length - 1, 1);  // EPS is last character
      }
      else
      {
        // A personal check.
        if (decryptedMICR.StartsWith("t") || decryptedMICR.StartsWith(":"))     // Bug 17864: the Pertech uses : instead of t
          return " ";   // If it starts with the transit code, then no EPC
        else
        {
          // Bug 19347: The EPC can only be 4 or a blank.
          string putativeEPC = decryptedMICR.Substring(0, 1);
          if (putativeEPC == "4")
            return putativeEPC;
          else
            return " ";  // The EPC can only be 4 or a blank.
        }
      }
    }

    /// <summary>
    /// The auxiliary on-us field is not present on small format checks (e.g., 6" personal style checks)
    /// as it would extend past the end of the check. 
    /// On larger format business checks, this field is allowed to the left of the
    /// transit number field. The auxiliary on-us field usually contains the check serial 
    /// number, and it may also contain accounting control information specific to that 
    /// account.
    /// In complete check line, it is in the most left position quoted by 'o'
    /// </summary>
    /// <param name="onUs"></param>
    /// <returns>First substring separated by 'o'</returns>
    private string parseAuxOnUSFromMICR(string decryptedRAWMICR)
    {

      // Bug 17864: the PerTech uses : instead of t and o
      if (!decryptedRAWMICR.Contains("t") && !decryptedRAWMICR.Contains("o"))
      {
        if (decryptedRAWMICR.Contains(":"))
        {
          string[] parts = decryptedRAWMICR.Split(':');
          if (parts.Length <= 3)
          {
            // Personal check with 3 parts: no Aux On-Us
            return " ";
          }
          else
          {
            // Business check with 4 or more parts
            return parts[0];
          }
        }
      }
      // End Bug 17864


      if (decryptedRAWMICR.Length > 0)
      {
        // The onUs silliness is to mimic the C++ code from ElecChkConversion.cpp in OneStep
        string csTemp = decryptedRAWMICR;

        // Only business checks have Aux On-Us. Business checks should start with the On-Us symbol
        if (csTemp[0] != 'o')                             // Make sure this is a business check.
        {
          if (csTemp[0] == 't')
            return "";
          else
          {
            // So it does not start woth 'o' or 't'!
            // Maybe there is an Aux On-us field with no delimiter
            string[] onus = csTemp.Split('t');
            return onus[0];
          }
        }

        // Trim off the leading On-Us symbol to faciliate splitting
        csTemp = csTemp.Substring(1);                     // Trim off leading 'o'

        // Return there are no more On-Us separators, then skip this
        if (csTemp.IndexOf('o') == -1)
          return "";

        // Split out the remaining On-Us                     
        string[] segments = csTemp.Split('o');
        return segments[0];
      }
      else
        return "";
    }


    /// <summary>
    /// Grab the Bank Account number directly from the TENDER table and decrypt
    /// </summary>
    /// <param name="geoTender"></param>
    /// <returns></returns>
    private string getBankAccountNumber(GenericObject geoTender)
    {
      string strEncryptedAccountNumber = geoTender.get("BANKACCTNBR", "") as string;
      if (!string.IsNullOrEmpty(strEncryptedAccountNumber))
      {
        string secret_key = Crypto.get_secret_key();
        byte[] content_bytes = Convert.FromBase64String(strEncryptedAccountNumber);
        byte[] decrypted_bytes = Crypto.symmetric_decrypt("data", content_bytes, "secret_key", secret_key);
        return System.Text.Encoding.Default.GetString(decrypted_bytes);
      }
      else
      {
        Logger.Log("Cannot decrypt the bank account number", "ICLBatchUpdate", "err");
        return "";
      }
    }


    /// <summary>
    /// Parse out the amount field.
    /// </summary>
    /// <param name="geoTender"></param>
    /// <param name="amount"></param>
    /// <returns></returns>
    private Decimal getAmount(GenericObject geoTender, string amount)
    {
      if (!geoTender.has(amount))
      {
        Logger.cs_log("getAmount: warning: AMOUNT not set");
        return Decimal.Zero;
      }
      //object amountObject = geoTender.get(amount, "0.0");
      //decimal decimalDouble = misc.Parse<Decimal>(amountObject);       // Bug 19713: I am a little uneasy about Parse<> since this is not a string but...

      object amountObject = geoTender.get(amount, 0M);
      decimal decimalAmount = Convert.ToDecimal(amountObject);           // Bug 19713: the amount field is numeric already

      return decimalAmount;
    }

    /// <summary>
    /// Scan through the DEPOSIT table and try to find the deposit slip number
    /// for the ICL deposit.
    /// Bug 20089
    /// </summary>
    /// <param name="depfilenbr"></param>
    /// <param name="depfileseq"></param>
    /// <param name="bankID">The configured BANKID of the Check Settlements (the ICL checks)</param>
    /// <param name="createrUserId"></param>
    /// <param name="postDt"></param>
    /// <returns></returns>
    private string getDepositData(int depfilenbr, int depfileseq, string bankID, out string createrUserId, out DateTime postDt)
    {

      // Start with some plausible, but useless, defaults
      createrUserId = "Unset"; postDt = DateTime.Now;

      // Read the slip number, user, and deposit timestamp
      String dataReaderSQL =
          string.Format(
          @"SELECT DEPSLIPNBR, CREATERUSERID, POSTDT FROM TG_DEPOSIT_DATA 
              WHERE DEPFILENBR = {0} AND DEPFILESEQ = {1} 
              AND BANKID = {2}
              and VOIDDT is NULL and DEPSLIPNBR is not null",
              depfilenbr, depfileseq, bankID);

      SqlConnection dataReaderConnection = new SqlConnection();
      SqlDataReader dataReader = null;
      try
      {
        GenericObject dbConnection = c_CASL.c_object("TranSuite_DB.Data.db_connection_info") as GenericObject;
        String conString = (String)dbConnection.get("db_connection_string");

        // Open a connection for the DataReader
        dataReaderConnection.ConnectionString = conString;
        dataReaderConnection.Open();

        SqlCommand dataReaderCommand = new SqlCommand(dataReaderSQL, dataReaderConnection);
        dataReader = dataReaderCommand.ExecuteReader();


        // Loop until you find a non-empty slip number
        // Bug 20089: Try to focus in on the correct deposit slip but the best
        // we can do for now is to pick the first non-null one
        while (dataReader.Read())
        {
          string depositSlipNumber = dataReader.GetString(0);
          createrUserId = dataReader.GetString(1);
          postDt = dataReader.GetDateTime(2);
          return depositSlipNumber;
        }
        return "";      // So use the lame defaults to the createrUserId and postDt fields

      }
      catch (Exception e)
      {
        string msg = "readSlipNumber: Cannot read the deposit record with this query: " + dataReaderSQL + " Error: " + e.ToString();
        Logger.Log(msg, "ICLBatchUpdate", "err");
        return "";
      }
      finally
      {
        if (dataReader != null && !dataReader.IsClosed)
          dataReader.Close();
        dataReaderConnection.Close();
      }
    }


    /// <summary>
    /// Called for every CORE file processed.
    /// Each CORE file should have a deposit slip, 
    /// so capture that plus other deposit information
    /// and create a digital Deposit Ticket.  
    /// Create the batch ticket and store it in the checks list
    /// so that WriteTrailer can sent it off to ICL.
    /// </summary>
    /// <param name="pFileDeposits"></param>
    /// <param name="pConfigSystemInterface"></param>
    /// <param name="pProductLevelMainClass"></param>
    /// <param name="strSysInterface"></param>
    /// <param name="pCurrentFile"></param>
    /// <returns></returns>
    public override bool Project_ProcessDeposits(GenericObject pFileDeposits, GenericObject pConfigSystemInterface, object pProductLevelMainClass/*BUG# 8224 DH*/, string strSysInterface/*BUG# 8224 DH*/, GenericObject pCurrentFile/*BUG# 8332*/, ref eReturnAction ReturnAction/*Bug 15464*/)
    {
      CASL_engine.BatchUpdate_ProductLevel pBatchUpdate_ProductLevel = (BatchUpdate_ProductLevel)pProductLevelMainClass;
      GenericObject geoFile = pCurrentFile.get(0, null) as GenericObject;

      try
      {
        GhostImage ghostImage = new GhostImage();
        // Bug 26710: Moved here so that the core information is available if not checks are found
        int depfilenbr = Convert.ToInt32(geoFile.get("DEPFILENBR", 0));
        int depfileseq = Convert.ToInt32(geoFile.get("DEPFILESEQ", 0));
        // Bug 15126
        if (coreFileList.Length > 0)
        {
          coreFileList.Append('|');
        }
        coreFileList.Append(depfilenbr).Append(depfileseq.ToString("D3"));
        // End Bug 15126


        if (m_ConfigSystemInterface == null)
        {
          m_ConfigSystemInterface = pConfigSystemInterface;
          return true;
        }
        string bankId = m_ConfigSystemInterface.get("Bank_ID_Deposit_Slip", "").ToString();
        if (string.IsNullOrEmpty(bankId))
        {
          Logger.Log("No Bank_ID_Deposit_Slip configured: not sending file", "ICLBatchUpdate", "err");
          return false;
        }

        // Bug 22678: JP Morgan Chase does not want a deposit ticket so I made it configurable
        bool includeDepositTicket = Convert.ToBoolean(pConfigSystemInterface.get("Include_Deposit_Ticket", "true"));
        if (!includeDepositTicket)
        {
          Logger.Log("Not configured to include a deposit ticket", "ICLBatchUpdate", "err");
          return true;
        }
        // End bug 22678

        string createrUserId;
        DateTime postDt;
        string depositSlipNumber = getDepositData(depfilenbr, depfileseq, bankId, out createrUserId, out postDt);

        // Bug 15126: Collect batch update information for the Activity Log
        // Separate with pipes so that the Activity Log can substitute commas
        if (depositTicketList.Length > 0)
        {
          depositTicketList.Append('|');
        }
        depositTicketList.Append(depositSlipNumber);
        // Bug 24743: Took out append of a comma; this was causing formatting issue


        string tenderPredicate = getTenders(pConfigSystemInterface);

        // Do not attempt to create a deposit ticket if no check tenders
        if (string.IsNullOrEmpty(tenderPredicate))
          return true;

        // Do not generate a bundle if there are no checks.
        // If Heidi's ICL module will only start a new bundle if it sees another Deposit Ticket.
        // If a deposit ticket is not in the check list, no new bundle.
        // Before this fix, there would be a new bundle with zero checks
        // which BB&T objected to.
        // Bug 15521.
        int numberOfChecks;
        ICL.CheckDetail depositTicket = ghostImage.setDepositTicket(geoFile, pConfigSystemInterface, depositSlipNumber, pConfigSystemInterface, tenderPredicate, createrUserId, postDt, out numberOfChecks);
        if (numberOfChecks > 0)
          m_lstCheck.Add(depositTicket);
        // Check the total number of checks in this batch update; if they are zero, do not generate a file.
        m_totalNumberOfDeposits += numberOfChecks;     // Bug 22678: What we have done here is create a deposit ticket, but a check
        // End bug 15521
        return true;
      }
      catch (Exception e)
      {
        // Bug 24223 Mark this core file as defective so it is not included in the output
        string iPaymentFilename = geoFile.get("FILENAME", "0") as string;  // Stored as a string in the file Geo
        Int64 iFILENBR = misc.Parse<Int64>(iPaymentFilename);   // Deposit file number or CORE file number
        m_defectiveChecks.Add(iFILENBR);
        // End Bug 24223

        // In CORE mode, we should still send the ICL but without this CORE file
        string sMode = (string)(m_ConfigSystemInterface.get("processing_mode", "SYSTEM"));
        if (sMode == "SYSTEM")
        {
#if UNUSED
          m_WriteTrailer = false;
#endif
          m_SendFile = false;
        }

        Logger.Log("Failure in ProcessDeposits: {e.ToString()}", "ICLBatchUpdate", "err");
        string msg = "Batch Update Failed: ProcessDeposits " + e.ToString();
        pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError("SYS_INTERFACE_SCOPE_ONLY", strSysInterface, msg);
        return false;
      }

    }

    /// <summary>
    /// Step through the System Interface configuration for ICL
    /// and capture which check tenders are attached to it.
    /// Format that information in a form suitable for 
    /// a predicate in an SQL query.
    /// </summary>
    /// <param name="pConfigSystemInterface"></param>
    /// <returns></returns>
    private string getTenders(GenericObject pConfigSystemInterface)
    {
      try
      {
        StringBuilder sb = new StringBuilder();
        sb.Append('(');
        GenericObject tenders = pConfigSystemInterface.get("tenders") as GenericObject;

        int tenderCount = 0;
        bool first = true;
        foreach (object tenderObj in tenders.Values)
        {
          if (tenderObj is GenericObject)
          {
            GenericObject tenderGeo = tenderObj as GenericObject;
            if (!tenderGeo.has("id"))
              continue;
            string tender = tenderGeo.get("id") as string;
            if (first)
              first = false;
            else
              sb.Append(',');
            sb.Append("'").Append(tender).Append("'");
            tenderCount++;
          }
        }
        sb.Append(")");
        if (tenderCount > 0)
          return sb.ToString();
        else
          return "";
      }
      catch (Exception e)
      {
        Logger.Log($"Failure in getTenders: {e.ToString()}", "ICLBatchUpdate", "err");
        string msg = "Batch Update Failed: ProcessDeposits " + e.ToString();
        return "('002')";
      }
    }


    /// <summary>
    /// Collect the checks objects in the checks list and send this off
    /// to Heidi's ICL module to write out the ICL file.
    /// First create the Misc Info object with general parameters and use to create the ICL class.
    /// Then send the list of checks and deposit tickets off to ICL to create the file.
    /// </summary>
    /// <param name="pProductLevelMainClass"></param>
    /// <param name="strSysInterface"></param>
    /// <param name="bEarlyTerminationRequestedByProject"></param>
    /// <returns></returns>
    public override bool Project_WriteTrailer(object pProductLevelMainClass, string strSysInterface, ref bool bEarlyTerminationRequestedByProject)
    {

      CASL_engine.BatchUpdate_ProductLevel pBatchUpdate_ProductLevel = (BatchUpdate_ProductLevel)pProductLevelMainClass;

      List<CheckDetail> cleanedList = new List<CheckDetail>();

      // Bug 24223: Remove any files that have a missing check image in of its
      // events
      if (m_defectiveChecks.Count > 0)
      {
        foreach (CheckDetail checkDetail in m_lstCheck)
        {
          Int64 iFILENBR = checkDetail.iFILENBR;
          if (!m_defectiveChecks.Contains(iFILENBR))
          {
            cleanedList.Add(checkDetail);
          }
        }
        m_lstCheck = cleanedList;
      }
      // End Bug 24223

      // If there are no checks in any of the CORE files, do not generate an ICL file
      // Bug 15521
      // Bug 22678: Had to change this from m_totalNumberOfDeposits because if there 
      // is no deposit ticket the m_totalNumberOfDeposits would be zero and the file would not be generated
      if (m_lstCheck.Count == 0)
      {
        // Bug 26710: Clean up message when no checks are found
        string msg = string.Format("No valid '{0}' checks found in any of the Core payfiles ({1}); the ICL file will not be created for this batch",
          strSysInterface,
          coreFileList.Replace("|", ",").ToString());

        // Do not set this because it outputs scary messages to the screen
        //pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError("SYS_INTERFACE_SCOPE_ONLY", strSysInterface, msg);
        // Just email a warning message
        m_writeTrailerSummaryComments.Append("<br/>ICL Summary:<br/>");
        m_writeTrailerSummaryComments.Append(msg).Append("<br/>");
        m_SendFile = false;

        return true;    // Do not return false and bomb all of the batch update...
      }
      // End 15521


      // If m_ConfigSystemInterface has not been set, then ProcessCoreEvent has not been called
      // which means that the ICL system interface was not configured for these transactions
      // which means I should bail out now before I access m_ConfigSystemInterface.
      if (m_ConfigSystemInterface == null)
      {
        // Return true to not disrupt the batch update
        m_SendFile = false;
        return true;
      }

      m_detailList.Append("Core Files:").Append(coreFileList).Replace("|", ",").Append(',');
      m_detailList.Append("Deposit Tickets:").Append(depositTicketList).Append(',');
      m_detailList.Append("Update ID:").Append(pBatchUpdate_ProductLevel.m_strUpdateID);

      string userID = pBatchUpdate_ProductLevel.m_strUserName;
      //string session = pBatchUpdate_ProductLevel.m_strSessionID;    // Bug 15126

      try
      {
        if (m_WellsFargoFormat)
          m_miscInfo.bTurnOffPrefixLength = true;         // Create Wells Fargo format
        else
          m_miscInfo.bTurnOffPrefixLength = false;         // Prefix every recore with a length field


        ICLClass icl = new ICLClass(m_miscInfo);      // Bug 15659: Change to ICLClass so I can get at the global
        bool success = icl.CreateICLFile(m_lstCheck);
        if (!success)
        {
          string errorMessage = icl.GetErrorMessage();
          // Bug 21327: Do not let this message get lost!!!!!
          Logger.Log($"Cannot create ICL file: {errorMessage}", "ICLBatchUpdate", "err");
          m_detailList.Append(',').Append("Output Filename:").Append(m_strICLFileName);
          m_detailList.Append(',').Append("Error:").Append(misc.cleanUpDetailsForActLog(errorMessage));  // Bug 15732
          try
          {
            misc.CASL_call("a_method", "actlog", "args",
                new GenericObject("user", userID, "app", "Portal", "action", "ICL Batch Update", "summary", "Error", "detail", m_detailList.ToString()));
          }
          catch (Exception actlogExp)
          {
            String strError = "Project_WriteTrailer: Cannot send error message to the activity log: " + m_detailList.ToString() + ": " + actlogExp.ToString();
            Logger.Log(strError, "ICLBatchUpdate", "err");
            throw;
          }

          // Bug 15614. Email results
          string strBody = "ICL Batch results:<br/>" + m_detailList.ToString().Replace(",", "<br/>").Replace("|", ", ").Replace(":", ": ");
          //Send_Notification_Email(strSubject, strBody);     // Bug 19218: do not send error emails
          m_writeTrailerSummaryComments.Append("<br/>ICL Summary:<br/>");
          m_writeTrailerSummaryComments.Append('\t').Append(errorMessage).Append("<br/>");
          // End Bug 15614
          pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError("SYS_INTERFACE_SCOPE_ONLY", strSysInterface, errorMessage);
          m_SendFile = false;    // Redundant since returning false will abort the batch update anyway
          return false;   // Bug 24746: Let the BU system know there was a problem
        }


        //m_writeTrailerSummaryComments.Length = 0;     // TODO in .NET 4.0 use .Clear()
        m_writeTrailerSummaryComments.Append("<br/>ICL Summary:<br/>");
        // Need divide m_dTotalAmtInFile since it adds in the BatchTicket amount to the total
        m_writeTrailerSummaryComments.Append('\t').Append("Created local ICL file: ").Append(m_strICLFileName).Append(" from these CORE files: ").Append(coreFileList).Append("<br/>");
        Decimal totalAmount = Decimal.Divide(icl.m_dTotalAmtInFile, 2.0M);   // Bug 19713: switched to decimal.  See unfixed Bugs 15663 and 15662
        m_writeTrailerSummaryComments.Append('\t').Append("Total Amount: ").Append(totalAmount.ToString("C")).Append("<br/>");
        m_writeTrailerSummaryComments.Append('\t').Append("Total Number of Checks (including Deposit Tickets): ").Append(icl.m_iTotalChkCntInFile).Append("<br/>");
        m_writeTrailerSummaryComments.Append('\t').Append("Total Number of Records: ").Append(icl.m_iTotalRecCntInFile).Append("<br/>");
        // This will only confuse the user; print the path name in the SendFile()
        //m_writeTrailerSummaryComments.Append("Successfully wrote ICL local file here: ").Append(m_strTempPathName).Append("<br/>");


        m_SendFile = true;
        return true;

      }
      catch (Exception e)
      {
        Logger.Log($"Cannot create ICL file: {e.ToString()}", "ICLBatchUpdate", "err");
        string msg = "Batch Update Failed: " + e.Message;
        pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError("SYS_INTERFACE_SCOPE_ONLY", strSysInterface, msg);

        // Bug 15126: Write out detailed error information
        m_detailList.Append(',').Append("Error:").Append(misc.cleanUpDetailsForActLog(e.Message));
        try
        {
          misc.CASL_call("a_method", "actlog", "args",
            new GenericObject("user", userID, "app", "Portal", "action", "ICL Batch Update", "summary", "Error", "detail", m_detailList.ToString()));
        }
        catch (Exception actlogExp)
        {
          string strError = "Project_WriteTrailer: Cannot send ICL create error message to the activity log: " + m_detailList.ToString() + ": " + actlogExp.ToString();
          Logger.Log(strError, "ICLBatchUpdate", "err");
          throw;
        }

        // End 15126
        // Bug 15614
        string strBody = "ICL Batch results: Cannot create or SFTP the ICL file: " + e.Message + "<br/>" + m_detailList.ToString().Replace(",", "<br/>").Replace("|", ", ").Replace(":", ": ");
        //Send_Notification_Email(strSubject, strBody);     // Bug 19218: do not send error emails
        // End 15614
        m_SendFile = false;        // Redundant since returning false will abort Batch Update

        return false;  // Bug 24746: Let the BU system know there was a problem
      }
    }

    /// <summary>
    /// Do the ftp/sftp part of the batch update.
    /// </summary>
    /// <param name="pBatchUpdate_ProductLevel">For posting errors</param>
    /// <param name="strSysInterface">For posting errors</param>
    /// <param name="strLocalTempFile"></param>
    /// <returns></returns>
    private bool sendICLFile(CASL_engine.BatchUpdate_ProductLevel pBatchUpdate_ProductLevel, string strSysInterface, string strLocalTempFile)
    {
      string strBatchFTPAddress = m_ConfigSystemInterface.get("ftp_server", "").ToString();
      if (string.IsNullOrEmpty(strBatchFTPAddress))
      {
        Logger.Log("No FTP Server configured: not sending file", "ICLBatchUpdate", "err");
        return false;
      }

      string strBatchFTPPort = m_ConfigSystemInterface.get("ftp_port", "").ToString();
      string strBatchFTPUserName = m_ConfigSystemInterface.get("ftp_username", "").ToString();
      string strBatchFTPPassword = m_ConfigSystemInterface.get("ftp_password", "").ToString();
      string strBatchSFTPKeyFile = m_ConfigSystemInterface.get("ftp_private_key_file", "").ToString(); // Bug 27621/26553 File Transmission Option Changing To S-FTP with ID and Key
      string strBatchSFTPKeyPassphrase = m_ConfigSystemInterface.get("ftp_private_key_passphrase", "").ToString(); // Bug 27621/26553 File Transmission Option Changing To S-FTP with ID and Key
      string strBatchFTPPath = m_ConfigSystemInterface.get("ftp_path", "").ToString();
      string strBatchUpdateMethod = ((string)m_ConfigSystemInterface.get("ftp_method", "ftp")).ToLower();

      int connectionTimeout = Convert.ToInt32(m_ConfigSystemInterface.get("ftp_connection_timeout", "60000"));  // Bug  25577 / 23202
      bool logVerbose = (bool)m_ConfigSystemInterface.get("rebex_verbose_log", false);   // Bug  25577 / 24836 
      string rebexLogPath = (string)m_ConfigSystemInterface.get("rebex_log_path", "");   // Bug  25577 / 24836 


      // TTS 14634
      bool bAltDirSep = (bool)m_ConfigSystemInterface.get("alt_directory_separator", false);
      string strServerFileName = "";

      if (string.IsNullOrWhiteSpace(strBatchFTPPath) || strBatchFTPPath == "/" || strBatchFTPPath == "\\")
      {
        strServerFileName = m_strICLFileName;
      }
      else
      {
        if (bAltDirSep)
        {
          strServerFileName = strBatchFTPPath + Path.AltDirectorySeparatorChar + m_strICLFileName;
        }
        else
          strServerFileName = Path.Combine(strBatchFTPPath, m_strICLFileName);
      }
      // End 14634


      // Decrypt FTP Password
      if (!string.IsNullOrEmpty(strBatchFTPPassword))
      {
        string secret_key = Crypto.get_secret_key();
        byte[] content_bytes = Convert.FromBase64String(strBatchFTPPassword);
        byte[] decrypted_bytes = Crypto.symmetric_decrypt("data", content_bytes, "secret_key", secret_key);
        strBatchFTPPassword = System.Text.Encoding.Default.GetString(decrypted_bytes);
      }

      // Bug 27621: Decrypt passphrase
      if (!string.IsNullOrEmpty(strBatchSFTPKeyPassphrase))
      {
        string secret_key = Crypto.get_secret_key();
        byte[] content_bytes = Convert.FromBase64String(strBatchSFTPKeyPassphrase);
        byte[] decrypted_bytes = Crypto.symmetric_decrypt("data", content_bytes, "secret_key", secret_key);
        strBatchSFTPKeyPassphrase = System.Text.Encoding.Default.GetString(decrypted_bytes);
      }

      // Transfer File

      // Bug 13690: FT/UMN: Switched to new FTP/SFTP class
      FTP ftp = null;
      SFTP sftp = null;
      const bool pwdIsEncrypted = false;  // Force the FTP classes to decrypt

      try
      {
        bool transferSuccessful = false;

        if (strBatchUpdateMethod == "ftp")
        {
          ftp = new FTP(strBatchFTPAddress, strBatchFTPUserName, strBatchFTPPassword, strBatchFTPPort, true, pwdIsEncrypted, false, false, false);

          ftp.Connect(connectionTimeout, logVerbose, rebexLogPath);      // Bug  25577 / 23202
          ftp.SendFile(strLocalTempFile, strServerFileName);
          // Bug 19217: Temporary hack to get around Wells Fargo anomalous error
          if (!ftp.FileExists(strServerFileName))
          {
            string msg = string.Format("WARNING: cannot confirm a successful ICL File Transfer of file {0}.", m_strICLFileName);
            Logger.Log(msg, "ICLBatchUpdate", "warn");
          }
          transferSuccessful = true;

        }
        else if (strBatchUpdateMethod == "sftp")
        {
          // Bug 27621
          sftp = new SFTP(strBatchFTPAddress, strBatchFTPUserName, strBatchFTPPassword, strBatchFTPPort, true, pwdIsEncrypted, strBatchSFTPKeyFile, "", strBatchSFTPKeyPassphrase);

          sftp.Connect(connectionTimeout, logVerbose, rebexLogPath);      // Bug  25577/23202
          sftp.SendFile(strLocalTempFile, strServerFileName);
          // Bug 19217: Temporary hack to get around Wells Fargo anomalous error
          if (!sftp.FileExists(strServerFileName))
          {
            string msg = string.Format("WARNING: cannot confirm a successful ICL File Transfer of file {0}.", m_strICLFileName);
            Logger.Log(msg, "ICLBatchUpdate", "warn");
          }
          transferSuccessful = true;
        }

        if (transferSuccessful)
        {
          //Succeed to upload file to FTP, log and send email
          string msg = string.Format("ICL File Transfer Process of file {0} Succeeded.  ICL Batch Update Successful", m_strICLFileName);
          Logger.Log(msg, "ICLBatchUpdate", "info");
          return true;
        }
        else
        {
          string strError = string.Format("Cannot Write to ICL File '{0}' to '{1}' on the FTP server", strLocalTempFile, strServerFileName);
          Logger.Log(strError, "ICLBatchUpdate", "err");
          string errorMsg = "Failed to tranfer ICL File: " + strError;
          pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError("SYS_INTERFACE_SCOPE_ONLY", strSysInterface, errorMsg);
          m_writeTrailerSummaryComments.Append("<br/>ICL Transmission Error:<br/>");
          m_writeTrailerSummaryComments.Append('\t').Append(strError).Append("<br/>");
          m_writeTrailerSummaryComments.Append('\t').Append(strLocalTempFile).Append(" not sent.").Append("<br/>");
          return false;
        }

      }
      finally
      {
        // Destructor will call Disconnect, but with .NET it's async scheduled. So calling
        // disconnect here will free the connection faster
        if (ftp != null) ftp.Disconnect();
        if (sftp != null) sftp.Disconnect();
      }

    }

    /// <summary>
    /// Set the MiscInfo fields.
    /// </summary>
    /// <returns></returns>
    private ICL.MiscInfo createMiscInfo(GenericObject pConfigSystemInterface)
    {
      ICL.MiscInfo miscInfo = new ICL.MiscInfo();

      // Create filename
      string strClientShortName = pConfigSystemInterface.get("Filename_Client_Short_Name", "").ToString();
      string strDateFormat = pConfigSystemInterface.get("Filename_Date_Format", "MMddyy").ToString();
      string strTypeIndicator = pConfigSystemInterface.get("Filename_Type_Indicator", "P").ToString();
      string strExtension = pConfigSystemInterface.get("Filename_Extension", "X937I").ToString();
      if (m_debug)
        strExtension = "937";
      StringBuilder sb = new StringBuilder();
      /* Bug 17234:  Need to change to KeyBank Format
      sb.Append(strClientShortName)
        .Append('_')
        .Append(DateTime.Now.ToString(strDateFormat))
        .Append(DateTime.Now.ToString("HHmmss"))
        .Append('_')
        .Append(strTypeIndicator)
        .Append('.')
        .Append(strExtension);
      */
      // Bug 17234: KeyBank format
      /*
      DateTime now = DateTime.Now;
      string immediateOriginRoutingNumber = pConfigSystemInterface.get("Immediate_Origin_Routing_Number", "").ToString();     // Bug 17864: They want the Origin routing number in the name, not the destination (their) routing number
      sb.Append("gway.")
        .Append(immediateOriginRoutingNumber)      // Bug 17864
        .Append('.')
        .Append(now.ToString("yyyyMMdd"))     // File creation date in YYYYMMDD format
        .Append('.')
        .Append(now.ToString("HHmmss"))       // HHMMSS File creation time in military time format
        .Append(".vip.kbvip");
      string fileName = sb.ToString();
      m_strICLFileName = fileName;
      */

      string immediateOriginRoutingNumber = pConfigSystemInterface.get("Immediate_Origin_Routing_Number", "").ToString();     // Bug 17864: They want the Origin routing number in the name, not the destination (their) routing number
      string filenameClientShortName = pConfigSystemInterface.get("Filename_Client_Short_Name", "Miami").ToString();
      string filenameDateFormat = pConfigSystemInterface.get("Filename_Date_Format", "yyMMdd").ToString();
      string filenameTypeIndicator = pConfigSystemInterface.get("Filename_Type_Indicator", "T").ToString();
      string filenameExtension = pConfigSystemInterface.get("Filename_Extension", "x9").ToString();
      DateTime now = DateTime.Now;
      sb.Append(filenameClientShortName)
        .Append('_')      // Bug 17864
        .Append(now.ToString(filenameDateFormat))     // File creation date in configured format
        .Append('_')
        .Append(filenameTypeIndicator)
        .Append('.')
        .Append(filenameExtension);
      string fileName = sb.ToString();
      m_strICLFileName = fileName;

      // Set the T or P flag for the File Header Record
      miscInfo.sFileType = strTypeIndicator;

      //Local_Temp_Directory
      string strTempPath = pConfigSystemInterface.get("Local_Temp_Directory", ".").ToString();
      m_strTempPathName = Path.Combine(strTempPath, fileName);
      miscInfo.sBatchPathFile = m_strTempPathName;

      // miscInfo.sClientName = pConfigSystemInterface.get("Client_Name", "").ToString();  Not used
      // Bug 24223: This field should be blank for most banks
      miscInfo.sClientID = pConfigSystemInterface.get("ICL_Client_ID", "").ToString();
      int n;
      bool isNumeric = int.TryParse("123", out n);
      if (!isNumeric)
        throw new Exception("The ICL Client ID must be numeric");

      string immediateDestinationRoutingNumber = pConfigSystemInterface.get("Immediate_Destination_Routing_Number", "").ToString();     // Bug 17864: They want the Origin routing number in the name, not the destination (their) routing number
      miscInfo.sDestinationRN = immediateDestinationRoutingNumber;

      // Bug 17234: Use the Immediate Origin value instead of the Immediate Destination Routing number!
      miscInfo.sOriginRN = immediateOriginRoutingNumber;
      // End Bug 17234

      miscInfo.sReturnLocationRN = immediateOriginRoutingNumber;

      miscInfo.cashLetterIDBankLocation = pConfigSystemInterface.get("Cash_Letter_ID_Bank_Location", "051").ToString();

      miscInfo.sECEInstitutionRN = pConfigSystemInterface.get("Institution_Routing_Number", "053101121").ToString();
      miscInfo.sImageCreatorRN = pConfigSystemInterface.get("Image_Creater_Routing_Number", "053101121").ToString();  // Bug 22678

      miscInfo.sDestinationName = pConfigSystemInterface.get("Destination_Name", "BBT").ToString();
      miscInfo.sOriginName = pConfigSystemInterface.get("Origin_Name", "BBT").ToString(); ;

      miscInfo.sOriginatorContactName = pConfigSystemInterface.get("Originator_Contact_Name", "").ToString();
      // Get phone number and strip special characters
      miscInfo.sOriginatorContactNumber = pConfigSystemInterface.get("Originator_Contact_Number", "").ToString();
      miscInfo.sOriginatorContactNumber = miscInfo.sOriginatorContactNumber.Replace("-", "").Replace("(", "").Replace(")", "");

      miscInfo.sFileIDModifier = pConfigSystemInterface.get("ICL_File_ID_Modifier", "1").ToString();

      miscInfo.sBOFD_IND = pConfigSystemInterface.get("BOFD_Indicator", "U").ToString();
      miscInfo.sBOFD_Conversion_IND = pConfigSystemInterface.get("BOFD_Conversion_Indicator", "2").ToString();
      miscInfo.iMaxCheckNumInBundle = Convert.ToInt32(pConfigSystemInterface.get("Maximum_Checks_in_Bundle", 300));

      // Include the credit record
      miscInfo.includeCreditRecord = Convert.ToBoolean(pConfigSystemInterface.get("Include_Credit_Record", "false"));

      // Credit (type 61) Record, field 3. The Credit Account Number.
      miscInfo.creditRecordAccountNumber = pConfigSystemInterface.get("Credit_Record_Account_Number", "").ToString();

      // Credit (type 61) Record, field 4. The Process Control field
      miscInfo.creditRecordProcessControl = pConfigSystemInterface.get("Credit_Record_Process_Control", "6586").ToString();

      // Credit (type 61) Record, field 5. The Payor Bank Routing Number
      miscInfo.creditRecordRoutingNumber = pConfigSystemInterface.get("Credit_Record_Routing_Number", "500000377").ToString();

      // Credit (type 61) Record, field 6. The Serial Number (Auxillary On-Us).  
      miscInfo.creditRecordSerialNumber = pConfigSystemInterface.get("Credit_Record_Serial_Number", "").ToString();

      miscInfo.onUsSymbol = Convert.ToChar(pConfigSystemInterface.get("On_Us_Symbol", "/").ToString());

      string debugString = pConfigSystemInterface.get("Write_ICL_Log_File", "false").ToString();
      miscInfo.bDebug = debugString == "True" ? true : false;
      miscInfo.sDebugPathFile = pConfigSystemInterface.get("ICL_Log_File_Path", "C:/temp/icl.log").ToString();

      // Allow us to toggle between ascii and ebcdic
      string useEBCDICString = pConfigSystemInterface.get("Use_EBCDIC_Encoding", "false").ToString();
      miscInfo.encodeEBCDIC = useEBCDICString == "True" ? true : false;

      // Bug 22678
      string useBigEndian = pConfigSystemInterface.get("Use_Big_Endian_Encoding", "false").ToString();
      miscInfo.useBigEndian = useBigEndian == "True" ? true : false;
      // End Bug 22678

      return miscInfo;
    }

    public override void Project_Rollback(object pProductLevelMainClass, bool bEarlyTerminationRequestedByProject)
    {
      // If there is a bad check in the m_lstCheck, delete it here,
      //but I think we are OK
    }

    public override bool Project_RollbackCoreFile(
      string strSystemInterface,
      GenericObject pConfigSystemInterface,
      object pProductLevelMainClass,
      GenericObject pCurrentFile,
      string strCoreFile)

    {
      // Add code here if you want to do something on failure (delete the file?)
      return true;
    }

    public override bool Project_Cleanup(bool bEarlyTerminationRequestedByProject, object pProductLevelMainClass)
    {
      // Clear out the accumulators just in case they persist between calls.
      depositTicketList = null;
      coreFileList = null;
      m_totalNumberOfDeposits = 0;

      // Clear out the detail list
      m_lstCheck = new List<ICL.CheckDetail>();
      m_defectiveChecks = new HashSet<Int64>();
      m_miscInfo = null;

      bool batchUpdateFailed = bEarlyTerminationRequestedByProject;

      if (m_deleteTempFile)         //  Bug 15659
      {
        // Clean up the temp file
        if (!m_debug)
        {
          if (File.Exists(m_strTempPathName))
            File.Delete(m_strTempPathName);
        }
      }

      m_strTempPathName = "";
      m_strICLFileName = "";
      m_detailList.Length = 0;    // Bug 15659: Make sure this is cleared.  TODO: in .NET 4.9 change to .Clear()
      m_SendFile = false;
      return true;
    }


    /// <summary>
    /// Read the Image for the check from the database.
    /// From SFGov.
    /// </summary>
    /// <param name="geoTender"></param>
    /// <param name="databaseConnection"></param>
    /// <returns></returns>
    public GenericObject GetTenderImageData(GenericObject geoTender, GenericObject databaseConnection)
    {
      string strSQL = "";
      long iTenderNbr = (int)geoTender.get("TNDRNBR");
      long iEventNbr = (int)geoTender.get("EVENTNBR");
      long lFileNbr = (int)geoTender.get("DEPFILENBR");
      long lFileSeqNbr = (int)geoTender.get("DEPFILESEQ");
      strSQL = "SELECT * FROM TG_IMAGE_DATA WHERE";
      strSQL += string.Format(" TG_IMAGE_DATA.DEPFILENBR ={0} AND TG_IMAGE_DATA.DEPFILESEQ ={1} AND  TG_IMAGE_DATA.EVENTNBR ={2} AND TG_IMAGE_DATA.POSTNBR={3} AND", lFileNbr, lFileSeqNbr, iEventNbr, iTenderNbr);
      strSQL += " TG_IMAGE_DATA.POST_TYPE='TNDR' AND IMAGE_FORMAT ='image/tiff' ORDER BY SEQ_NBR ASC";
      GenericObject geoImageData = GetRecords(databaseConnection, strSQL);

      // Bug 15043. Do not process null check images
      if (geoImageData.vectors.Count == 0)
      {
        throw new Exception(string.Format("Cannot process this batch update file.  No check images found in CORE Filenbr: {0}, Seq: {1}, Event: {2}, Tender: {3}",
            lFileNbr, lFileSeqNbr, iEventNbr, iTenderNbr));
      }
      // End 15043
      validateCheckImageExistence(lFileNbr, lFileSeqNbr, iEventNbr, iTenderNbr, geoImageData);

      return geoImageData;
    }

    /// <summary>
    /// Make sure that each Tender has a front and back image.
    /// This uses the new values that Dan added to the IMAGING_TYPE field,
    /// N - CHECK - FRONT and N - CHECK - BACK
    /// Bug 24223
    /// </summary>
    /// <param name="lFileNbr"></param>
    /// <param name="lFileSeqNbr"></param>
    /// <param name="iEventNbr"></param>
    /// <param name="iTenderNbr"></param>
    /// <param name="geoImageData"></param>
    private void validateCheckImageExistence(long lFileNbr, long lFileSeqNbr, long iEventNbr, long iTenderNbr, GenericObject geoImageData)
    {
      bool frontImageFound = false;
      bool backImageFound = false;

      foreach (GenericObject vector in geoImageData.vectors)
      {
        string imagingType = vector.get("IMAGING_TYPE", "none") as string;
        if (imagingType.Length == 1)
        {
          // Old style IMAGING_TYPE field
          // All we can do is count the records
          if (geoImageData.vectors.Count < 2)
          {
            throw new Exception(string.Format("Cannot process this batch update.  This is missing the front or back image. CORE Filenbr: {0}, Seq: {1}, Event: {2}, Tender: {3}",
                lFileNbr, lFileSeqNbr, iEventNbr, iTenderNbr));
          }
          return;     // Bug 26892: Bug out if there is no error.
        }
        // OK, now we have the new type of IMAGING_TYPE courtesy of Dan H
        if (imagingType.Contains("FRONT"))
        {
          frontImageFound = true;
        }
        if (imagingType.Contains("BACK"))
        {
          backImageFound = true;
        }
      }

      // OK through the images: let's see if one is missing
      if (!frontImageFound)
      {
        throw new Exception(string.Format("Cannot process this batch update.  Front check image not found in CORE Filenbr: {0}, Seq: {1}, Event: {2}, Tender: {3}",
            lFileNbr, lFileSeqNbr, iEventNbr, iTenderNbr));
      }
      if (!backImageFound)
      {
        throw new Exception(string.Format("Cannot process this batch update.  Back check image not found in CORE Filenbr: {0}, Seq: {1}, Event: {2}, Tender: {3}",
            lFileNbr, lFileSeqNbr, iEventNbr, iTenderNbr));
      }



    }

    private GenericObject GetRecords(GenericObject geoDBInfo, string strSQL)
    {

      GenericObject geoDBReturnData = null;
      // ONLY Support for sql server 
      db_vector_reader_sql pReader = (db_vector_reader_sql)db_vector_reader_sql.CASL_make("db_connection", geoDBInfo, "sql_statement", strSQL);

      geoDBReturnData = new GenericObject();
      geoDBReturnData = (GenericObject)db_vector_reader_sql.CASL_insert_data("reader", pReader, "_subject", geoDBReturnData, "start", 0, "insert_size", 0);

      pReader = null;
      return geoDBReturnData;
    }

    /// <summary>
    /// Send Notification Email. Use Email server and Email from which are defined in config.casl.
    /// Bug 24223.
    /// </summary>
    /// <param name="strEmail_to"></param>
    /// <param name="strSubject"></param>
    /// <param name="strBody"></param>
    private void Send_Notification_Email(string strSubject, string strBody)
    {
      // Sanity check
      if (m_ConfigSystemInterface == null)
        return;

      // Email must be explicitly turned on
      bool bEmailResults = (bool)m_ConfigSystemInterface.get("Email_Results", false);
      if (!bEmailResults)
        return;

      string strEmail_to = m_ConfigSystemInterface.get("Notification_Email_To", "").ToString();
      string strEmail_from = c_CASL.GEO.get("ipayment_sender_email", "") as string;
      string strEmail_server = c_CASL.GEO.get("email_server", "") as string;

      Send_Notification_Email(strEmail_to, strEmail_from, strSubject, strBody);
    }

    /// <summary>
    /// Send results out to one or more recipients
    /// </summary>
    /// <param name="strEmail_to"></param>
    /// <param name="strEmail_from"></param>
    /// <param name="strSubject"></param>
    /// <param name="strBody"></param>
    public void Send_Notification_Email(string strEmail_to, string strEmail_from, string strSubject, string strBody)
    {
      try
      {
        //string strEmail_from = c_CASL.GEO.get("ipayment_sender_email", "") as string;
        string strEmail_server = c_CASL.GEO.get("smtp_host", "") as string; // Bug 22627 UMN
        string strProvider = c_CASL.GEO.get("smtp_provider", "") as string; // Bug 26910 NAM
        string strPort = c_CASL.GEO.get("smtp_port", "") as string;
        if (!String.IsNullOrEmpty(strEmail_from) && !String.IsNullOrEmpty(strEmail_to) && !String.IsNullOrEmpty(strEmail_server))
        {
          emails.CASL_email(
          "to", strEmail_to,
          "from", strEmail_from,
          "host_smtp_server", strEmail_server,
          "subject", strSubject,
          "body", strBody,
          "provider", strProvider,      // Bug 26910 NAM
          "port", strPort
          );
        }
        else
        {
          Logger.LogError("Failed to send email: Email address can not be empty.", "EMAIL SEND");
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in ICL Send_Notification_Email", e);

        Logger.LogError($"Failed to send email: {e.Message}", "EMAIL SEND");
      }
    }

    /// <summary>
    /// File is sent here, after WriteTrailer.
    /// Always return true so that the whole Batch Update does not abort.
    /// Depend on logging to tell the user if there is a problem.
    /// </summary>
    /// <param name="pProductLevelMainClass"></param>
    /// <param name="strSysInterface"></param>
    /// <param name="bEarlyTerminationRequestedByProject"></param>
    /// <returns></returns>
    public override bool Project_SendFiles(object pProductLevelMainClass, string strSysInterface)
    {
      // Bug 15614
      // Bug 19218: Adjust subject for Wells Fargo
      // End 19218
      // Bug 26710: Send email if no checks found or other issue
      string originName = (m_ConfigSystemInterface == null) ? "ICL" : m_ConfigSystemInterface.get("Origin_Name", "").ToString();
      StringBuilder sb = new StringBuilder();
      sb.Append(originName).Append(": ").Append(DateTime.Now.Date.ToString("d")).Append(": ICL Batch Update Succeeded");
      string strSubject = sb.ToString();
      // End 19218

      // Bug 26710: Send message if not ICL checks are found
      if (!m_SendFile)
      {
        Send_Notification_Email(strSubject, m_writeTrailerSummaryComments.ToString());   // Bug 19218: Only send success message to Wells Fargo
        return true;
      }
      // End 26710
      m_deleteTempFile = false;         // Bug 15659: Set to false incase an exception is raised.

      CASL_engine.BatchUpdate_ProductLevel pBatchUpdate_ProductLevel = (BatchUpdate_ProductLevel)pProductLevelMainClass;
      string userID = pBatchUpdate_ProductLevel.m_strUserName;
      //string session = pBatchUpdate_ProductLevel.m_strSessionID;    // Bug 15126

      try
      {
        bool sendSuccess = sendICLFile(pBatchUpdate_ProductLevel, strSysInterface, m_strTempPathName);
        // Bug 15126: Write detailed activity log entry
        m_detailList.Append(',').Append("Output Filename:").Append(m_strICLFileName);
        if (sendSuccess)
        {
          try
          {
            misc.CASL_call("a_method", "actlog", "args",
              new GenericObject("user", userID, "app", "Portal", "action", "ICL Batch Update", "summary", "Success", "detail", m_detailList.ToString()));
          }
          catch (Exception actlogExp)
          {
            string strError = "Project_SendFiles: Cannot send success message to the activity log: " + actlogExp.ToString();
            Logger.Log(strError, "ICLBatchUpdate", "err");
            throw;
          }

          string strBody = "ICL Batch results:<br/>" + m_detailList.ToString().Replace(",", "<br/>").Replace("|", ", ").Replace(":", ": ");
          Send_Notification_Email(strSubject, strBody);   // Bug 19218: Only send success message to Wells Fargo
          m_deleteTempFile = true;        // Bug 15659
          return true;  // Bug 24746: Let the BU system there was no problem
          // End 15614
        }
        else
        {
          m_detailList.Append("Error:Cannot transmit this ICL file '").Append(m_strTempPathName).Append("'");

          m_writeTrailerSummaryComments.Append("<br/>ICL Error:<br/>");
          m_writeTrailerSummaryComments.Append('\t').Append("Error:Cannot transmit ICL file: ").Append(m_strTempPathName).Append("<br/>");

          try
          {
            misc.CASL_call("a_method", "actlog", "args",
new GenericObject("user", userID, "app", "Portal", "action", "ICL Batch Update", "summary", "Sending Error", "detail", m_detailList.ToString()));
          }
          catch (Exception actlogExp)
          {
            string strError = "Project_SendFiles: Cannot send transmit error message to the activity log: " + m_detailList.ToString() + ": " + actlogExp.ToString();
            Logger.Log(strError, "ICLBatchUpdate", "err");
            throw;
          }

          // Bug 15614
          strSubject = "ICL Batch Update Send Failed";
          string strBody = "ICL Batch results:<br/>" + m_detailList.ToString().Replace(",", "<br/>").Replace("|", ", ").Replace(":", ": ");
          //Send_Notification_Email(strSubject, strBody);     // Bug 19218: removed
          m_deleteTempFile = false;        // Bug 15659. Keep temp file around so that it can be sent by hand
          return false;     // Bug 24746: FT: Let the batch update know that this file could not be sent.
        }

      }
      catch (Exception e)
      {
        Logger.Log($"Cannot send ICL file: {e.ToString()}", "ICLBatchUpdate", "err");
        string msg = "Batch Update Failed: " + e.Message;
        pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError("SYS_INTERFACE_SCOPE_ONLY", strSysInterface, msg);

        // Bug 15126: Write out detailed error information
        m_detailList.Append(',').Append("Error:").Append(misc.cleanUpDetailsForActLog(e.Message));
        try
        {
          misc.CASL_call("a_method", "actlog", "args",
            new GenericObject(userID, "app", "Portal", "action", "ICL Batch Update", "summary", "Error", "detail", m_detailList.ToString()));
        }
        catch (Exception actlogExp)
        {
          string strError = "Project_SendFiles: Cannot send outer transmit error to the activity log: " + m_detailList.ToString() + ":" + actlogExp.ToString();
          Logger.Log(strError, "ICLBatchUpdate", "err");
          throw;
        }

        // End 15126
        // Bug 15614
        strSubject = "ICL Batch Update Failed";
        string strBody = "ICL Batch results: Cannot transmit the ICL file: " + e.Message + "<br/>" + m_detailList.ToString().Replace(",", "<br/>").Replace("|", ", ").Replace(":", ": ");
        //Send_Notification_Email(strSubject, strBody);   // Bug 19218: do not send error emails

        m_writeTrailerSummaryComments.Append("<br/>ICL Error:<br/>");
        m_writeTrailerSummaryComments.Append('\t').Append("Failed to transmit ICL file for CORE files: ").Append(coreFileList).Append("<br/>");
        m_writeTrailerSummaryComments.Append('\t').Append("Local ICL file: ").Append(m_strTempPathName).Append(" could not be transmitted").Append("<br/>");
        m_writeTrailerSummaryComments.Append('\t').Append("Error: ").Append(e.Message).Append("<br/>");

        // End 15614
        return false;   // Bug 24746:  Let the BU system know that the file has not been sent.
      }
    }
  }

}

