﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using CASL_engine;
using System.Text;
using TranSuiteServices;

namespace ICLBatchUpdate
{
  class ICLUtil
  {
    /// <summary>
    /// Get the Workgroup as the branch number.
    /// </summary>
    /// <param name="geoFile"></param>
    /// 
    /// <returns></returns>
    public static int getBranchNumber(GenericObject geoFile)
    {
      // If we want to get the workgroup from the tender start here, but it does not make sense...
      // GenericObject geoTenderClass = (GenericObject)((GenericObject)c_CASL.c_object("Tender.of")).get(geoTender.get("TNDRID", "").ToString());

      // Get department from the file instead
      string department = geoFile.get("DEPTID", "001") as string;

      int deptNumber;
      bool result = Int32.TryParse(department, out deptNumber);
      if (result)
      {
        return deptNumber;
      }
      else
      {
        return 1;   // For non-numeric departments
      }
    }

    /// <summary>
    /// Create the tricky sECE_SEQUENCE_NUMBER.
    /// The format is YYJJJWWWWNNNNSSS.
    /// I think that the fields must break down as this:
    /// YY--Year
    /// JJJ—Julian Date
    /// WWWW—the WSNBR which I think is the workstation number
    /// NNNN—Tender Transaction number: using the Tender event number
    /// SSS—Tender Transaction Sequence number: Using the Tender number
    /// 
    /// The iPayment CORE filename is thus:
    /// YYYYJJJSSS:
    /// YYYY: Year
    /// JJJ: Julian
    /// SSS: Sequence number
    /// 
    /// So I think we need the Sequence number in the in the sECE_SEQUENCE_NUMBER;
    /// it will be added as the WWWW.
    /// </summary>
    /// <param name="iPaymentFilename"></param>
    /// <param name="geoTender"></param>
    /// <returns></returns>
    public static string constructEceSeqNumber(string iPaymentFilename)
    {
      string depFileSeq = iPaymentFilename.Substring(7, 3);

      return constructEceSeqNumber_internal(iPaymentFilename, depFileSeq, 0, 0);
    }


    /// <summary>
    /// Create a unique number for each 25 record (check detail) and 61 record (credit record).
    /// Construct the ECE Sequence number but with the DEPFILESEQ formated to 4 digits, not 3.
    /// The sequence counter will remain at 4 digits to fill the number out to 15 digits.
    /// </summary>
    /// <param name="depfilenbr"></param>
    /// <param name="depfileseq"></param>
    /// <returns></returns>
    public static string constructEceSeqNumberWith4digitDepfileseq(long depfilenbr, long depfileseq)
    {
      StringBuilder sb = new StringBuilder();
      sb.Append(depfilenbr)
        .Append(depfileseq.ToString("D4"))
        .Append(ICLBatchUpdateSI.m_ECESequenceNumberCounter.ToString("D4"));

      ICLBatchUpdateSI.m_ECESequenceNumberCounter++;
      return sb.ToString();
    }

    /// <summary>
    /// Create a unique number for each 25 record (check detail) and 61 record (credit record).
    /// Modified for Wells Fargo to be unique.
    /// Start with the Core filename such as this (10 digits):  20161180130
    /// Then add in a 4-digit counter.
    /// Bug 19189
    /// </summary>
    /// <param name="iPaymentFilename"></param>
    /// <param name="depFileSeq"></param>
    /// <param name="eventNbr"></param>
    /// <param name="tenderNbr"></param>
    /// <returns></returns>

    // [Obsolete()]: Brief slack exchange with Fred who felt that the one
    // call to constructEceSeqNumber was probably correct.
    private static string constructEceSeqNumber_internal(string iPaymentFilename, string depFileSeq, int eventNbr, int tenderNbr)
    {
      StringBuilder sb = new StringBuilder();
      sb.Append(iPaymentFilename).Append(ICLBatchUpdateSI.m_ECESequenceNumberCounter.ToString("D4"));
      ICLBatchUpdateSI.m_ECESequenceNumberCounter++;
      return sb.ToString();

      /*
      sb.Append(iPaymentFilename.Substring(2, 2))   // The last to digits of the YYYY in the Core filename
        .Append(iPaymentFilename.Substring(4, 3))   // The Julian date
        .Append(depFileSeq.PadLeft(3, '0'))          // No workstation number.  Using the DEPFILESEQ
        .Append(eventNbr.ToString("D4"))            // Substituting Event number for Tender Transaction number
        .Append(tenderNbr.ToString("D3"));          // Substituting Tender Number for Tender Transaction seq. number
      return sb.ToString();
       */
    }

    /// <summary>
    /// Return the OnUs in the format that BB&T has specified
    /// From the OnSite ICL File Format JUNE-2011 b.doc from BB&T:
    /// 
    /// Payor’s account number, followed by “/” (On-Us symbol), followed by 13 (transaction code) 
    /// if a deposit ticket or check number or spaces. 
    /// Valid characters: numbers, spaces, “-“ (dash), 
    /// Examples: 
    /// Dep Tkt Ex:   “511111111/13        ”
    /// Chk Ex:       “511111111/1018    ”
    /// 
    /// Note how the example is ambiguous about the size of the field, but another column in the 
    /// table in the spec says that the field size must be 20 so we pad out to 20.
    /// 
    /// </summary>
    /// <param name="AccountNumber"></param>
    /// <param name="checkNumber"></param>
    /// <returns></returns>
    public static string constructBBT_OnUs(string checkNumber, GenericObject configSystemInterface)
    {
      string payorAccountNumber = "";
      if (configSystemInterface != null)
        payorAccountNumber = configSystemInterface.get("Payor_Account_Number", "").ToString();
      else
      {
        Logger.Log("m_ConfigSystemInterface not set: cannot create OnUs", "ICLUtil", "err");
      }

      string onUs = payorAccountNumber;
      onUs += "/";
      onUs += checkNumber;

      // The C++ code seemed to want to trim, but the field size MUST be 20 so pad here.
      return onUs.PadLeft(20, ' ');
    }


    /// <summary>
    /// Get the value of a MICR custom field based on the CUSTID.
    /// See Bug 24223
    /// </summary>
    /// <param name="geoFile"></param>
    /// <param name="geoTender"></param>
    /// <param name="">custid</param>
    /// <returns></returns>
    public static string GetEncryptedMICRFieldFromCustomField(GenericObject geoTender, string custid)
    {
      string encryptededField = GetMICRFieldFromCustomField(geoTender, custid);
      if (string.IsNullOrEmpty(encryptededField))
        return encryptededField;

      try
      {
        string secret_key = Crypto.get_secret_key();
        byte[] content_bytes = Convert.FromBase64String(encryptededField);
        byte[] decrypted_bytes = Crypto.symmetric_decrypt("data", content_bytes, "secret_key", secret_key);
        string decryptedMICR = System.Text.Encoding.Default.GetString(decrypted_bytes);
        return decryptedMICR.Trim();
      }

      catch (Exception e)
      {
        // See the all-important TTS 9831 and 14734 and the stealth fix for the encryption.
        // This could be a bug when the MICR was encrypted in Create_core_item_app_f_controller.casl 
        // NOT here where it is decrypted.
        // Bug 21327
        string msg = string.Format(@"GetEncryptedMICRFieldFromCustomField: 
          Cannot decrypt the {0} MICR element
          in this Tender: DEPFILENBR {1}, DEPFILESEQ: {2}, EVENTNBR: {3}, TNDRNBR: {4}
          Error: {5}",
          custid,
          geoTender.get("DEPFILENBR", ""),
          geoTender.get("DEPFILESEQ", ""),
          geoTender.get("EVENTNBR", ""),
          geoTender.get("TNDRNBR", ""),
          e.Message
          );
        Logger.cs_log(msg, Logger.cs_flag.error);
        throw;
      }

    }


    public static string GetMICRFieldFromCustomField(GenericObject geoTender, string custid)
    {
      string sqlParameterized = @"SELECT CUSTVALUE FROM CUST_FIELD_DATA
                                WHERE 
	                                DEPFILENBR = @depfilenbr 
	                                AND DEPFILESEQ = @depfileseq 
	                                AND EVENTNBR = @eventnbr 
	                                AND POSTNBR = @postnbr 
	                                AND CUSTID = @custid";

      try
      {
        int DEPFILENBR = Convert.ToInt32(geoTender.get("DEPFILENBR"));
        int DEPFILESEQ = Convert.ToInt32(geoTender.get("DEPFILESEQ"));
        int EVENTNBR = Convert.ToInt32(geoTender.get("EVENTNBR"));
        int TNDRNBR = Convert.ToInt32(geoTender.get("TNDRNBR"));

        GenericObject dbConnection = c_CASL.c_object("TranSuite_DB.Data.db_connection_info") as GenericObject;
        String conString = (String)dbConnection.get("db_connection_string");


        HybridDictionary sql_args = new HybridDictionary();
        sql_args["depfilenbr"] = DEPFILENBR;
        sql_args["depfileseq"] = DEPFILESEQ;
        sql_args["eventnbr"] = EVENTNBR;
        sql_args["postnbr"] = TNDRNBR;      // Note the name switcheroo
        sql_args["custid"] = custid;


        using (SqlConnection connection = new SqlConnection(conString))
        {
          using (SqlCommand command = ParamUtils.CreateParamCommand(connection, sqlParameterized, sql_args))
          {
            command.Connection = connection;
            command.CommandType = CommandType.Text;

            connection.Open();

            return command.ExecuteScalar() as string;
          }
        }
      }
      catch (SqlException ex)
      {
        string msg = string.Format("GetMICRFieldFromCustomField: SQL Server error when trying to the micr custom fields table with this sql {0}.  Error: {1}", sqlParameterized, ex.ToString());
        Logger.Log(msg, "ICLUtil", "err");
        return null;
      }
      catch (Exception ex)
      {
        string msg = string.Format("GetMICRFieldFromCustomField: Error when trying to query the micr custom fields this sql {0}.  Error: {1}", sqlParameterized, ex.ToString());
        Logger.Log(msg, "ICLUtil", "err");
        return null;
      }
    }

    /// <summary>
    /// Pull the Workgroup ID from the TG_DEPFILE_DATA table.
    /// Note: consider using a configurable default if we cannot find the DEPTID
    /// Bug 26684
    /// </summary>
    /// <param name="geoFile"></param>
    /// <returns></returns>
    internal static string getWorkGroupID(GenericObject geoFile)
    {
      // If we want to get the workgroup from the tender start here, but it does not make sense...
      // GenericObject geoTenderClass = (GenericObject)((GenericObject)c_CASL.c_object("Tender.of")).get(geoTender.get("TNDRID", "").ToString());

      // Get department from the file instead
      string department = geoFile.get("DEPTID", "001") as string;

      return department;
  }
}
}
