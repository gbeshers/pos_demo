﻿function validate_form(this_form) {
  //if (validate_and_show_dialog(this_form) == false) 
  //  return false;
  if(this_form.transaction_type.value!='' && this_form.tender_type.value!='') {
    alert('Invalid search request. You may search on Transaction Type or Tender Type but not both. ');
    return false;
  }
  return true;
}

function submit_form (this_form) {
  var is_valid = validate_form(this_form);
  if(!needed_fields(this_form)) {
    return false;
  }
  
  //lert("is_valid: " + is_valid);
  if(is_valid) {
    this_form.submit();
  }
  
  return false;
}

function needed_fields (this_form) {
  //BUG 5593-change Receipt Nbr to Reference Number
  if( this_form.file_id.value == "" &&
      this_form.receipt_id.value == "" &&
      this_form.file_date_to.value =="" &&
      this_form.file_date_from.value == "" ) {
    alert("One of the following fields are required: Reference Number, File ID, Date");
    return false;
  }
  return true;
}

function getFieldsAndRefresh(theElement, url) {
  var theForm = theElement.form
  var theValue = ""
  for(var x=0; x < theForm.elements.length; x++) {
    if(theForm.elements[x].id != "") {
      url = url + theForm.elements[x].id + "=" + theForm.elements[x].value.replace(/\//g, "%2f");
      if( x < theForm.elements.length - 2 ) url = url +"&";
    }
  }
  refresh(url);
}

function refresh(url) {
  location.href = url;
  return location.href;
}