using System;
using System.Data;
using System.Threading;
using System.Collections;
using CASL_engine;

namespace CBT_Transaction_Search
{
	/// <summary>
	/// Summary description for transaction_search.
	/// </summary>
	public class transaction_search
	{
    // Only for testing - temporary DSN connection string.
		public transaction_search()
		{
		}

    public static string search(params Object[] args)
    {
      GenericObject arg = misc.convert_args(args);

      string payment_screen_OR_tender_nbr = "";
      //---------------------------------------------------------------------------------------
      // First check if user selected screen or tender type.
      bool bScreenSelected = false, bTenderSelected = false;
      object ScreenName          =  arg.get("screens", CASLInterpreter.CASL_NOT_FOUND);
      if((ScreenName is GenericObject) || (ScreenName.ToString()=="no_selection"))
        bScreenSelected = false;
      else
      {
        payment_screen_OR_tender_nbr = ScreenName.ToString();// Screen number
        bScreenSelected = true;
      }
        
      object TenderType          =  arg.get("tenders", CASLInterpreter.CASL_NOT_FOUND);
      if((TenderType is GenericObject) || (TenderType.ToString()=="no_selection"))
        bTenderSelected = false;
      else
      {
        payment_screen_OR_tender_nbr = TenderType.ToString();// Tender number
        bTenderSelected = true;
      }
      
      if(bScreenSelected == false && bTenderSelected == false)
        return "ERROR:  Screen Name or Tender Type is required";
      //---------------------------------------------------------------------------------------

      string PayfileNbr      = arg.get("payfile_nbr", "") as String;
      string TransactionNbr  = arg.get("trans_nbr", "") as String;
      string CustomerNbr     = arg.get("cust_nbr", "") as String;
      string Amt_From        = arg.get("amt_from", "") as String;
      string Amt_To          = arg.get("amt_to", "") as String;
      string vj_Text         = arg.get("vj_search_text", "") as String;

      if(PayfileNbr == null)
        PayfileNbr="";
      if(TransactionNbr == null)
        TransactionNbr="";
      if(CustomerNbr == null)
        CustomerNbr="";
      if(Amt_From == null)
        Amt_From="";
      if(Amt_To == null)
        Amt_To="";
      if(vj_Text == null)
        vj_Text="";
                                      
      object parent_class_obj    = arg.get("return_class", CASLInterpreter.CASL_NOT_FOUND);      
    
      // Test return more records.
      string strSQL = "SELECT PAYFILENBR, WSNBR, TRANSNBR, CUSTNBR, TRANSTOTAL, TRANSTIME, ACCTNBR FROM PAYTRANS WHERE ";
      strSQL += transaction_search.Create_SQL_Criteria_Filter( PayfileNbr, 
                                            "",
                                            TransactionNbr,
                                            CustomerNbr,
                                            vj_Text,
                                            payment_screen_OR_tender_nbr,
                                            Amt_From,
                                            Amt_To,
                                            bScreenSelected);
      


       OdbcConnection OdbcConn = get_db_connection();
      
       db_request SM = db_request.make_db_request("a_connection", OdbcConn,  
         "sql_statement",     strSQL, 
         "class",             parent_class_obj,
         "NextGroupCount",    0,
         "group_timeout",     1,
         "query_timeout",     30,
         "get_all",           "N" );
       
      //-------------------------------------------------
      // Add this instance to MY.
      GenericObject my = CASLParser.CASL.getAtPath("session.my") as GenericObject; 
      if(!my.has("MySearches"))
        my.set("MySearches", new GenericObject() );      
      //-------------------------------------------------

      GenericObject Search_Results = my.get("MySearches") as GenericObject;
      Search_Results.set(SM.GetInstanceID(), SM);

      string str = SM.to_htm_large("class_name", "my_transaction_search", "target_name", "result_list");// Call only after search complete. Otherwize it may point to other object that GEO.
      return str;
    }

    public static string Create_SQL_Criteria_Filter( string strPayFile, 
                                        string strWSNbr,
                                        string strTranNbr,
                                        string strID,
                                        string strVJText,
                                        string strPaymentType,
                                        string strAmtFrom,
                                        string strAmtTo,
                                        bool bPayScreenSearch)
    {
      bool bSomethingAdded = false;

      string strRV="";
      strPayFile.Trim();
      strWSNbr.Trim();
      strTranNbr.Trim();
      strID.Trim();
      strVJText.Trim();
      strPaymentType.Trim();
      strAmtFrom.Trim();
      strAmtTo.Trim();

      // Account number only applyes to tenders.
      if(bPayScreenSearch)
        strID = "";

      //----------------------------------
      // PAYFILE NUMBER
      if(strPayFile.Length != 0)
      {
        if(bSomethingAdded)
          strRV += " AND PAYFILENBR=";
        else
          strRV += "PAYFILENBR=";

        strRV += strPayFile;
        bSomethingAdded = true;
      }
      //----------------------------------

      //----------------------------------
      // WORKSTATION NUMBER
      if(strWSNbr.Length != 0)
      {
        if(bSomethingAdded)
          strRV += " AND WSNBR=";
        else
          strRV += "WSNBR=";

        strRV += strWSNbr;
        bSomethingAdded = true;
      }
      //----------------------------------

      //----------------------------------
      // TRANSACTION NUMBER
      if(strTranNbr.Length != 0)
      {
        if(bSomethingAdded)
          strRV += " AND TRANSNBR=";
        else
          strRV += "TRANSNBR=";

        strRV += strTranNbr;
        bSomethingAdded = true;
      }
      //----------------------------------

      //----------------------------------
      // ACCOUNT NUMBER
      if(strID.Length != 0)
      {
        //FormatSQLString(strID);
        if(bSomethingAdded)
          strRV += " AND ACCTNBR='";
        else
          strRV += "ACCTNBR='";

        strRV += strID;
        strRV += "'";
        bSomethingAdded = true;
      }
      //----------------------------------

      //----------------------------------
      // VIDEO JOURNAL TEXT
      if(strVJText.Length != 0)
      {
        //FormatSQLString(strVJText);

        if(bSomethingAdded)
          strRV += " AND VJTEXT LIKE '%";
        else
          strRV += "VJTEXT LIKE '%";

        strRV += strVJText;
        strRV += "%'";
        bSomethingAdded = true;
      }
      //-------------------------------------------


      //-------------------------------------------
      if(bSomethingAdded)
      {
        if(bPayScreenSearch)
          strRV  += " AND TRANSTYPE=1";
        else
          strRV  += " AND TRANSTYPE=2";
      }
      else
      {
        if(bPayScreenSearch)
          strRV  += "TRANSTYPE=1";
        else
          strRV  += "TRANSTYPE=2";
      }

      bSomethingAdded = true;

      //-------------------------------------------
      //-------------------------------------------
      // PAYMENT SCREEN
      if(strPaymentType.Length != 0)
      {
        if(bSomethingAdded)
          strRV += " AND ITEMTYPE=";
        else
          strRV += "ITEMTYPE=";

        strRV += strPaymentType;
        bSomethingAdded = true;
      }
      //-------------------------------------------

      //-------------------------------------------
      // TRANSACTION AMOUNTS
      if(strAmtFrom.Length != 0 && strAmtTo.Length == 0)
      { 
        if(bSomethingAdded)
          strRV  += " AND TRANSTOTAL=";
        else
          strRV  += "TRANSTOTAL=";

        strRV  += strAmtFrom;
        bSomethingAdded = true;
      }

      else if(strAmtFrom.Length != 0 && strAmtTo.Length != 0)
      { 
        if(bSomethingAdded)
          strRV  += " AND TRANSTOTAL >=";
        else
          strRV  += "TRANSTOTAL >=";

        strRV  += strAmtFrom;
        bSomethingAdded = true;
      } 

      if(strAmtTo.Length!= 0)
      { 
        if(bSomethingAdded)
          strRV  += " AND TRANSTOTAL <=";
        else
          strRV  += "TRANSTOTAL <=";

        strRV  += strAmtTo;
        bSomethingAdded = true;
      }
      //-------------------------------------------
      return strRV;
    }

    public static string GetImageList(params Object[] args)
    {
      GenericObject arg = misc.convert_args(args);

      string str="";
      object strSQL = arg.get("sql", CASLInterpreter.CASL_NOT_FOUND);
      OdbcConnection OdbcConn = get_db_connection();
      
      object parent_class    = arg.get("return_class", CASLInterpreter.CASL_NOT_FOUND);
      object parent_class_obj= CASLParser.CASL.getAtPath(parent_class.ToString());

      try
      {
        db_request SM = db_request.make_db_request("a_connection", OdbcConn,  
          "sql_statement",     strSQL,
          "class",             parent_class_obj,
          "NextGroupCount",    0,
          "group_timeout",     1,
          "query_timeout",     30,
          "get_all",           "N" );

        if(SM.IsErr())
        {
           return SM.GetErrMsg().ToString();
        }

          //-------------------------------------------------
          // Add this instance to MY.
          GenericObject my = CASLParser.CASL.getAtPath("session.my") as GenericObject; 
          if(!my.has("MySearches"))
            my.set("MySearches", new GenericObject() );      
          //-------------------------------------------------

          GenericObject Search_Results = my.get("MySearches") as GenericObject;
          Search_Results.set(SM.GetInstanceID(), SM);

          str = SM.to_htm_large("class_name","my_transaction_search");// Call only after search complete. Otherwize it may point to other object that GEO.
      }
      catch(Exception e)
      {
        str = e.Message.ToString();
      }
      
      return str;
    }

    public static string GetMoreRecords(GenericObject geo_args)
    {
      GenericObject my = CASLParser.CASL.getAtPath("session.my") as GenericObject; 
      GenericObject mySearches = my.get("MySearches")as GenericObject; 
      string strID = geo_args.get("SearchID").ToString();
      db_request SM = mySearches.get(strID) as db_request;

      long display_offset = 0;
      if(geo_args.has("last"))
        display_offset = (long)Convert.ChangeType(geo_args.get("last"), typeof(long));
            
      SM.set_display_offset(display_offset);

      string str = SM.to_htm_large("class_name", "my_transaction_search", "target_name","result_list");
      return str;
    }

    public static long CASL_execSQL(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      
      string strSQL = (string)geo_args.get("sql_statement");
      OdbcConnection OdbcConn = get_db_connection();
      long rows = 0;
      try
      {
        rows = db_request.db_exec_sql("a_connection", OdbcConn, "sql_statement", "insert into forms values(7, '\aa\')");
      }
      catch(Exception e)
      {
        throw CASL_error.create_str(e.Message.ToString());
      }

      return rows;
    }

    public static OdbcConnection get_db_connection()
    {
      return db_request.get_db_connection("db_connection", "DSN=TESTDB;UID=onestep;PWD=onestep;");
    }

	}
}
