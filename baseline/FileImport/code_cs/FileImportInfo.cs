using System;
using System.Collections;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Schema;
namespace FileImport
{
	/// <summary>
	/// Summary description for ImportFileType.
	/// </summary>
	public class FileImportInfo
	{

		//Configurable Fields 
		[XmlElement("ImportedFile")]
		public string m_ImportedFile;
		[XmlElement("FileImportID")]
		public string m_FileImportID; 
		[XmlElement("CoreFileDesc")]
		public string m_CoreFileDesc;
		[XmlElement("CoreFileEffDate")]
		public string m_CoreFileEffDate;
		[XmlElement("WorkGroupID")]
		public string m_WorkGroupID;
		[XmlElement("TenderID")]
		public string m_TenderID;
		[XmlElement("UserID")]
		public string m_UserID;

		[XmlElement("TenderFieldInfo",typeof(TenderFieldInfo))]
		public ArrayList m_TenderFields;
			//		[XmlElement("TenderField",typeof(TenderField))]
//		public ArrayList m_TenderFields;

		public FileImportInfo()
		{
			m_ImportedFile ="";
			m_FileImportID="";
			m_CoreFileDesc="";
			m_CoreFileEffDate="";
			m_WorkGroupID="";
			m_TenderID="";
			m_UserID="";
		}
		public string setFileImportID()
		{
			return "";
		}

	}
}
