using System;
using CASL_engine;
using System.Xml;
using System.IO;	//Stream
using System.Xml.Serialization;
using System.Collections;

namespace FileImport
{
  /// <summary>
  /// Summary description for Class1.
  /// </summary>
  public class IpaymentFileImport
  {
    private static CreateLogFiles m_Err = new CreateLogFiles();
    public IpaymentFileImport()
    {
      //
      // TODO: Add constructor logic here
      //
    }

    /// <summary>
    /// Imports a file
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static string CASL_import(params object[] args)
    {
      GenericObject geoARGS = misc.convert_args(args);
      GenericObject file_import_info = (GenericObject)geoARGS.get("file_import_info");
      string session_id = (string)geoARGS.get("session_id", "");
      string LOGIN_ID = (string)geoARGS.get("LOGIN_ID", "");
      return Import(file_import_info, session_id, LOGIN_ID) as string;
    }

    /// <summary>
    /// Does the actual import of a file
    /// </summary>
    /// <param name="geoFileImportInfo"></param>
    /// <param name="strSessionID"></param>
    /// <param name="LOGIN_ID"></param>
    /// <returns></returns>
    public static string Import(GenericObject geoFileImportInfo, string strSessionID, string LOGIN_ID)
    {
      string strReturn = "";

      string fileimport_id = geoFileImportInfo.get("fileimport_id", "", false) as string;
      string corefile_desc = geoFileImportInfo.get("description", "", false) as string;
      string corefile_eff_date = geoFileImportInfo.get("effective_date", "", false) as string;
      string workgroup_id = geoFileImportInfo.get("department", "", false) as string;
      string tender_id = geoFileImportInfo.get("tender_id", "", false) as string;
      GenericObject geoTenderInfo = geoFileImportInfo.get("a_tender", new GenericObject(), false) as GenericObject;
      // IPAY-1618 DJD: File Import With Separate Receipts (Events)
      object oSeparateEvents = geoFileImportInfo.get("separate_events", "false");
      bool separateEvents = oSeparateEvents.GetType() == typeof(System.String) ? (bool)(((string)oSeparateEvents) == "true") : (bool)oSeparateEvents;

      #region retrieve uploaded file
      string fileContent = "";
      try
      {
        string upload_folder = c_CASL.GEO.get("site_physical") + "/upload_files/";//temp storing file in server
        string timestamp = System.DateTime.Now.ToString("yyyy_MM_dd_HH-mm-ss");
        System.Web.HttpFileCollection allUploadedFiles = System.Web.HttpContext.Current.Request.Files;
        System.Web.HttpPostedFile uploadedFile = allUploadedFiles[0];
        string destinationFile = @upload_folder + "FileImport_" + timestamp + ".txt";
        uploadedFile.SaveAs(destinationFile);
        StreamReader textReader = new StreamReader(destinationFile);//, Encoding.UTF8);
        fileContent = textReader.ReadToEnd();
        if (fileContent == "")
        {
          string strMessage = "Empty file contains no records to import.";
          Logger.LogError(strMessage, "File Import");
          return strMessage;
        }
      }
      catch (Exception ex)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in Import", ex);
        strReturn = "Error Occurred: " + ex.Message;
        Logger.LogError(strReturn, "File Import");
        return strReturn;
      }
      #endregion

      #region create request xml for FileImport module 
      FileImportInfo FII = new FileImportInfo();
      FII.m_FileImportID = fileimport_id;
      FII.m_ImportedFile = fileContent;
      FII.m_CoreFileDesc = corefile_desc;
      FII.m_CoreFileEffDate = corefile_eff_date;
      FII.m_WorkGroupID = workgroup_id;
      FII.m_TenderID = tender_id;
      FII.m_UserID = LOGIN_ID;

      // Handle tender fields 
      FII.m_TenderFields = new ArrayList();
      IDictionaryEnumerator myEnumerator = geoTenderInfo.GetEnumerator() as IDictionaryEnumerator;
      while (myEnumerator.MoveNext())
      {
        string tendername = myEnumerator.Key as string;
        if (!tendername.StartsWith("_"))
        {
          TenderFieldInfo TF = new TenderFieldInfo();
          TF.m_FieldName = "" + myEnumerator.Key;
          TF.m_FieldValue = "" + myEnumerator.Value;
          FII.m_TenderFields.Add(TF);
        }
      }
      //			TenderFieldInfo tenderfield1 = new TenderFieldInfo("abck","abcv");
      //			FII.m_TenderFields.Add(tenderfield1);
      // end of Handle tender fields 

      string xmlFileImportInfo = "";
      XmlSerializer serializerFII = new XmlSerializer(typeof(FileImportInfo));
      StringWriter stringWriterFII = new StringWriter();
      try
      {
        serializerFII.Serialize(stringWriterFII, FII);
        String xmlFII = stringWriterFII.ToString();
        XmlDocument xml_doc = new XmlDocument();
        xml_doc.LoadXml(xmlFII);
        xmlFileImportInfo = string.Format("<FileImportInfo>{0}</FileImportInfo>", xml_doc.SelectSingleNode("FileImportInfo").InnerXml);

      }
      catch (Exception ex)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in Import", ex);
        strReturn = "Error Occurred: " + ex.Message;
        Logger.LogError(strReturn, "File Import");
        return strReturn;
      }
      string log_folder = c_CASL.GEO.get("baseline_physical") + "/FileImport/log/";//temp storing file in server
      m_Err.ErrorLog(log_folder, xmlFileImportInfo);//DEBUG_CODE
      #endregion

      #region call FileImport.CreateImportXML
      string xmlFileToImport = "";
      try
      {
        string strDBConn = c_CASL.c_object("TranSuite_DB.Config.db_connection_info.db_connection_string") as String;
        xmlFileToImport = FileImport.CreateImportXML(xmlFileImportInfo, strDBConn);
      }
      catch (Exception ex)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in Import", ex);
        strReturn = "Error Occurred :" + ex.Message;
        Logger.LogError(strReturn, "File Import");
        return strReturn;
      }
      m_Err.ErrorLog(log_folder, xmlFileToImport);//DEBUG_CODE
      #endregion

      #region post to ipayment 

      try
      {
        GenericObject geoDEPFILE = null;
        geoDEPFILE = CreateIpaymentFilePhysics(xmlFileToImport, separateEvents); // IPAY-1618 DJD: File Import With Separate Receipts (Events)
        GenericObject R = TranSuiteServices.TranSuiteServices.CreateAndPostFileToT3("_subject", geoDEPFILE,
          "data", c_CASL.c_object("TranSuite_DB.Data.db_connection_info"),
          "config", c_CASL.c_object("TranSuite_DB.Config.db_connection_info"),
          "session_id", strSessionID
          );
        if (R.has("FILENAME"))
        {
          geoFileImportInfo.set("FILENAME", R.get("FILENAME", ""));
        }
      }
      catch (Exception ex)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in Import", ex);
        strReturn = "Error Occurred: " + ex.Message;
        Logger.LogError(strReturn, "File Import");
        return strReturn;
      }
      #endregion
      return strReturn;
    }

    // IPAY-1618 DJD: File Import With Separate Receipts (Events) - Added separateEvents argument
    public static GenericObject CreateIpaymentFilePhysics(string xmlFileToImport, bool separateEvents)
    {
      GenericObject geoDEPFILE = null;

      #region Deserialize xmlFileToImport 
      XmlSerializer serializerRequest = new XmlSerializer(typeof(FileToImport));
      StringWriter stringWriterRequest = new StringWriter();
      string xmlFileImportInfoBase64 = Convert.ToBase64String(System.Text.Encoding.Default.GetBytes(xmlFileToImport));
      byte[] byteGEOBytes = Convert.FromBase64String(xmlFileImportInfoBase64);
      MemoryStream ms = new MemoryStream();
      ms.Write(byteGEOBytes, 0, byteGEOBytes.Length);
      ms.Position = 0;
      FileToImport FTI = (FileToImport)serializerRequest.Deserialize(ms);
      string SOURCE_TYPE = FTI.m_SourceType;
      string SOURCE_GROUP = FTI.m_SourceGroup;
      string SOURCE_DATE = FTI.m_SourceDate;
      string USERID = FTI.m_UserID;
      #endregion

      #region ipayment physical instance TG_DEPFILE_DATA 

      // TG_DEPFILE_DATA
      geoDEPFILE = c_CASL.c_instance("TranSuite_DBAPI.TG_DEPFILE_DATA");
      geoDEPFILE.set("FILETYPE", "");
      geoDEPFILE.set("FILEDESC", FTI.m_CoreFileDesc);
      geoDEPFILE.set("STATUS", "");
      geoDEPFILE.set("EFFECTIVEDT", FTI.m_CoreFileEffDate);
      geoDEPFILE.set("SOURCE_TYPE", SOURCE_TYPE);
      geoDEPFILE.set("SOURCE_GROUP", SOURCE_GROUP);
      geoDEPFILE.set("SOURCE_DATE", SOURCE_DATE);
      geoDEPFILE.set("DEPTID", FTI.m_WorkGroupID);
      geoDEPFILE.set("CREATOR_ID", USERID);
      geoDEPFILE.set("OPEN_USERID", USERID);//
      geoDEPFILE.set("LOGIN_ID", USERID);

      // TG_PAYEVENT_DATA
      GenericObject geoPAYEVENTs = c_CASL.c_instance("vector");
      geoDEPFILE.set("table_TG_PAYEVENT_DATA", geoPAYEVENTs);

      // IPAY-1618 DJD: CREATE the TG_TENDER_DATA template to be used later
      GenericObject geoTENDER_TEMPLATE = null;
      GenericObject geoTenderConfig = null;
      CreateTender(FTI, SOURCE_DATE, out geoTENDER_TEMPLATE, out geoTenderConfig);

      GenericObject geoPAYEVENT_ONE = null; // IPAY-1618 DJD: Used for a single event
      GenericObject geoTRANs_ONE = null; // IPAY-1618 DJD: Used for a single event

      // IPAY-1618 DJD: Create the single Event for all transactions
      if (!separateEvents)
      {
        geoPAYEVENT_ONE = CreatePayEvent(SOURCE_TYPE, SOURCE_GROUP, SOURCE_DATE, USERID);
        geoPAYEVENTs.insert(geoPAYEVENT_ONE);
        geoTRANs_ONE = c_CASL.c_instance("vector");
        geoPAYEVENT_ONE.set("table_TG_TRAN_DATA", geoTRANs_ONE);
      }

      // TG_TRAN_DATA
      ArrayList ITRANS = FTI.m_Transactions;
      int TRANNBR = 0;

      // TTS20929 Support multiple transaction types per import file 
      GenericObject geoTranConfig = null;
      string strTTIDFILE = "";
      if (ITRANS.Count > 0)
      {
        TranToImport ITRAN = ITRANS[0] as TranToImport;
        strTTIDFILE = ITRAN.m_TTID;
        string strTranPath = string.Format("Transaction.of.{0}", strTTIDFILE);
        geoTranConfig = c_CASL.get_GEO().get_path(strTranPath, true, null) as GenericObject;
        if (geoTranConfig == null)
          throw new Exception(" Can not find Transaction (" + ITRAN.m_TTID + ").");
      }

      foreach (object IObj in ITRANS)
      {
        GenericObject geoPAYEVENT_ONE_PER_TRAN = null; // IPAY-1618 DJD: Used for separate events
        GenericObject geoTRANs_ONE_PER_TRAN = null; // IPAY-1618 DJD: Used for separate events

        if (separateEvents) // IPAY-1618 DJD
        {
          geoPAYEVENT_ONE_PER_TRAN = CreatePayEvent(SOURCE_TYPE, SOURCE_GROUP, SOURCE_DATE, USERID);
          geoPAYEVENTs.insert(geoPAYEVENT_ONE_PER_TRAN);
          geoTRANs_ONE_PER_TRAN = c_CASL.c_instance("vector");
          geoPAYEVENT_ONE_PER_TRAN.set("table_TG_TRAN_DATA", geoTRANs_ONE_PER_TRAN);
        }

        TranToImport ITRAN = IObj as TranToImport;
        decimal dTranAmount = ITRAN.m_Amount; // IPAY-1618 DJD
        // TTS 20929 support multiple transaction type per import file 
        if (ITRAN.m_TTID != strTTIDFILE)
        {
          string strTranPath = string.Format("Transaction.of.{0}", ITRAN.m_TTID);
          geoTranConfig = c_CASL.get_GEO().get_path(strTranPath, true, null) as GenericObject;
          if (geoTranConfig == null)
            throw new Exception(" Can not find Transaction (" + ITRAN.m_TTID + ").");
        }
        TRANNBR += 1;

        GenericObject geoTRAN = c_CASL.c_instance("TranSuite_DBAPI.TG_TRAN_DATA");
        if (separateEvents) // IPAY-1618 DJD 
        {
          geoTRANs_ONE_PER_TRAN.insert(geoTRAN);
        }
        else 
        {
          geoTRANs_ONE.insert(geoTRAN);
        }
        geoTRAN.set("TRANNBR", separateEvents? 1 : TRANNBR); // IPAY-1618 DJD
        geoTRAN.set("TTID", ITRAN.m_TTID);
        geoTRAN.set("TTDESC", geoTranConfig.get("description"));
        geoTRAN.set("TRANAMT", dTranAmount);
        geoTRAN.set("DISTAMT", 0.00);
        geoTRAN.set("ITEMIND", "S");
        geoTRAN.set("COMMENTS", "");
        geoTRAN.set("CONTENTTYPE", "1");
        geoTRAN.set("TAXEXIND", "N");
        geoTRAN.set("SYSTEXT", "");
        geoTRAN.set("SOURCE_DATE", SOURCE_DATE);
        geoTRAN.set("SOURCE_REFID", "");

        // Handle custom fields
        // e.g.			FName="George";
        //				FName_f_id="1";
        //				FName_f_label="F.Name"
        GenericObject geoCustomFields = c_CASL.c_GEO();
        geoTRAN.set("_custom_keys", geoCustomFields);
        GenericObject geoCustomFieldsConfig = geoTranConfig.get("_custom_keys", geoCustomFields) as GenericObject;
        //geoTRAN.set("_custom_keys", geoCustomFieldsConfig.vectors);

        //TTS20929 support GLACCTNBR imported as allocation GLACCTNBR from one of import field 
        string strGLACCTNBR = "";
        foreach (object objField in ITRAN.m_CustomFields)
        {
          CustomFieldToImport CF = objField as CustomFieldToImport;
          string strCFTagName = CF.m_CustomFieldName;

          string strFieldPath = string.Format("Transaction.of.{0}.{1}_f_custom_field", ITRAN.m_TTID, CF.m_CustomFieldName);
          GenericObject CFConfig = c_CASL.get_GEO().get_path(strFieldPath, true, new GenericObject()) as GenericObject;

          if (strCFTagName == "GLACCTNBR")
          {
            CF.m_CustomFieldValue = CF.m_CustomFieldValue.Trim();
            strGLACCTNBR = CF.m_CustomFieldValue;

          }
          geoCustomFields.insert(strCFTagName);
          // [custom_field]=value
          geoTRAN.set(strCFTagName, CF.m_CustomFieldValue);
          // [custom_field]_f_id=..
          string strCFIDKey = string.Format("{0}_f_id", strCFTagName);
          string strCFIDValue = CFConfig.get("id", "") as string;
          geoTRAN.set(strCFIDKey, "");
          // [custom_field]_f_label=..
          string strCFLabelKey = string.Format("{0}_f_label", strCFTagName);
          string strCFLabelValue = CFConfig.get("label", "") as string;
          geoTRAN.set(strCFLabelKey, strCFLabelValue);
        }
        // store configured custom fields belong to transaction type
        foreach (object objField in geoCustomFieldsConfig.vectors)
        {
          string strCFTagName = objField as string;
          if (!geoCustomFields.vectors.Contains(strCFTagName))
          {
            string strFieldPath = string.Format("{0}_f_custom_field", strCFTagName);
            GenericObject CFConfig = geoTranConfig.get(strFieldPath, null) as GenericObject;
            if (CFConfig != null)
            {
              geoCustomFields.insert(strCFTagName);
              // [custom_field]=value
              if (!geoTRAN.has(strCFTagName))
                geoTRAN.set(strCFTagName, CFConfig.get("default", ""));
              // [custom_field]_f_id=..
              string strCFIDKey = string.Format("{0}_f_id", strCFTagName);
              string strCFIDValue = CFConfig.get("id", "") as string;
              geoTRAN.set(strCFIDKey, "");
              // [custom_field]_f_label=..
              string strCFLabelKey = string.Format("{0}_f_label", strCFTagName);
              string strCFLabelValue = CFConfig.get("label", "") as string;
              geoTRAN.set(strCFLabelKey, strCFLabelValue);
            }
          }
        }

        #region Handle system custom field update_system_interface_list BUG#7665 
        string strUpdateSIListPath = string.Format("Business.Transaction.of.{0}.update_system_interface_list", ITRAN.m_TTID);
        GenericObject geoUpdateSIListConfig = c_CASL.get_GEO().get_path(strUpdateSIListPath, true, c_CASL.c_instance("vector")) as GenericObject;
        if (geoUpdateSIListConfig.vectors.Count > 0)
        {
          string xmlUpdateSIList = misc.CASL_to_xml_large("_subject", geoUpdateSIListConfig);
          string strUpdateSICFTagName = "update_system_interface_list";
          geoCustomFields.insert(strUpdateSICFTagName);
          // [custom_field]=value
          geoTRAN.set(strUpdateSICFTagName, xmlUpdateSIList);
          // [custom_field]_f_id=..
          string strUpdateSICFIDKey = string.Format("{0}_f_id", strUpdateSICFTagName);
          geoTRAN.set(strUpdateSICFIDKey, "");
          // [custom_field]_f_label=..
          string strUpdateSICFLabelKey = string.Format("{0}_f_label", strUpdateSICFTagName);
          geoTRAN.set(strUpdateSICFLabelKey, "");
        }
        #endregion

        // TG_ITEM_DATA		
        string strAGPath = string.Format("Business.Transaction.of.{0}.allocation_group_classes._objects.0.allocation_classes", ITRAN.m_TTID);
        GenericObject geoAGConfig = c_CASL.get_GEO().get_path(strAGPath, true, new GenericObject()) as GenericObject;
        decimal decTranAmount = ITRAN.m_Amount;
        string strDescription = "";

        GenericObject geoITEMs = c_CASL.c_instance("vector");
        geoTRAN.set("table_TG_ITEM_DATA", geoITEMs);
        GenericObject geoItemConfig = null;
        if (geoAGConfig.getLength() == 0) // WITHOUT CONFIGURED ALLOCATION
        {
          GenericObject geoITEM = c_CASL.c_instance("TranSuite_DBAPI.TG_ITEM_DATA");
          geoITEMs.insert(geoITEM);
          geoITEM.set("ITEMNBR", 1);
          geoITEM.set("AMOUNT", decTranAmount);
          geoITEM.set("TOTAL", decTranAmount);
          geoITEM.set("QTY", 1);
          geoITEM.set("GLACCTNBR", strGLACCTNBR);
          geoITEM.set("ITEMDESC", strDescription);
          geoITEM.set("ACCTID", "");
          geoITEM.set("ITEMACCTID", "");
          geoITEM.set("ITEMID", "-1");
          geoITEM.set("TAXED", "FALSE");
        }
        else // WITH CONFIGURED ALLOCATIONS
        {
          int iITEMNBR = 1;
          foreach (object objItemConfig in geoAGConfig.vectors)
          {
            geoItemConfig = objItemConfig as GenericObject;

            strGLACCTNBR = GetGLACCTNBRFromGEO(geoItemConfig.get("GL_nbr", new GenericObject()) as GenericObject);
            strDescription = geoItemConfig.get("description", "") as string;
            decimal decDistPercentage = (decimal)geoItemConfig.get("distribution_percentage", 1.00M);
            decimal decItemAmount = decTranAmount * decDistPercentage; //TODO ROUND IT?

            GenericObject geoITEM = c_CASL.c_instance("TranSuite_DBAPI.TG_ITEM_DATA");
            geoITEMs.insert(geoITEM);
            geoITEM.set("ITEMNBR", iITEMNBR);
            geoITEM.set("AMOUNT", decItemAmount);
            geoITEM.set("TOTAL", decItemAmount);
            geoITEM.set("QTY", 1);
            geoITEM.set("GLACCTNBR", strGLACCTNBR);
            geoITEM.set("ITEMDESC", strDescription);
            geoITEM.set("ACCTID", "");
            geoITEM.set("ITEMACCTID", "");
            geoITEM.set("ITEMID", "-1");
            geoITEM.set("TAXED", "FALSE");
            iITEMNBR = iITEMNBR + 1;
          }
        }

        // IPAY-1618 DJD: Add a tender for each transaction (in separate events)
        if (separateEvents)
        {
          // Find the tender bank id through the transaction bank rule
          string strBANKACCTID = GetTenderBankAccount(geoTenderConfig, geoTranConfig);

          GenericObject geoTENDERs = c_CASL.c_instance("vector");
          geoPAYEVENT_ONE_PER_TRAN.set("table_TG_TENDER_DATA", geoTENDERs);

          GenericObject geoTENDERinEachEvent = geoTENDER_TEMPLATE.copy(c_CASL.c_opt, c_CASL.c_opt);
          geoTENDERs.insert(geoTENDERinEachEvent);
          geoTENDERinEachEvent.set("AMOUNT", dTranAmount);
          geoTENDERinEachEvent.set("BR_BANK_ACCT_ID", strBANKACCTID);
        }
      }

      // IPAY-1618 DJD: Add a single tender for all transactions in the single event
      if (!separateEvents)
      {
        // Find the tender bank id through the transaction bank rule
        string strBANKACCTID = GetTenderBankAccount(geoTenderConfig, geoTranConfig);

        GenericObject geoTENDERs = c_CASL.c_instance("vector");
        geoPAYEVENT_ONE.set("table_TG_TENDER_DATA", geoTENDERs);

        GenericObject geoTENDERinOneEvent = geoTENDER_TEMPLATE.copy(c_CASL.c_opt, c_CASL.c_opt);
        geoTENDERs.insert(geoTENDERinOneEvent);
        geoTENDERinOneEvent.set("AMOUNT", FTI.m_Tender.m_Amount);
        geoTENDERinOneEvent.set("BR_BANK_ACCT_ID", strBANKACCTID);
      }

      #endregion
      return geoDEPFILE;
    }

    // IPAY-1618 DJD: File Import With Separate Receipts (Events) - Added Method
    private static GenericObject CreatePayEvent(string SOURCE_TYPE, string SOURCE_GROUP, string SOURCE_DATE, string USERID)
    {
      GenericObject geoPAYEVENT = c_CASL.c_instance("TranSuite_DBAPI.TG_PAYEVENT_DATA");
      geoPAYEVENT.set("EVENTNBR", "");
      geoPAYEVENT.set("FILENAME", "");
      geoPAYEVENT.set("USERID", USERID);
      geoPAYEVENT.set("SOURCE_DATE", SOURCE_DATE);
      geoPAYEVENT.set("SOURCE_GROUP", SOURCE_GROUP);
      geoPAYEVENT.set("SOURCE_REFID", "");
      geoPAYEVENT.set("SOURCE_TYPE", SOURCE_TYPE);
      return geoPAYEVENT;
    }

    // IPAY-1618 DJD: File Import With Separate Receipts (Events) - Added Method
    private static string GetTenderBankAccount(GenericObject geoTenderConfig, GenericObject geoTranConfig)
    {
      string strBANKACCTID = "";
      GenericObject geoBRConfig = geoTranConfig.get("bank_rule", new GenericObject()) as GenericObject;
      GenericObject geoBRItemsConfig = geoBRConfig.get("bank_rule_items", new GenericObject()) as GenericObject;
      foreach (Object objBRItemConfig in geoBRItemsConfig.vectors)
      {
        GenericObject geoBRItemConfig = objBRItemConfig as GenericObject;
        GenericObject geoTenderInBR = geoBRItemConfig.get("tenders") as GenericObject;
        if (geoTenderInBR.Contains(geoTenderConfig))
        {
          GenericObject geoBankConfig = geoBRItemConfig.get("bank_account", null) as GenericObject;
          if (geoBankConfig != null)
            strBANKACCTID = geoBankConfig.get("id", "") as string;
          break;
        }
      }

      return strBANKACCTID;
    }

    // IPAY-1618 DJD: File Import With Separate Receipts (Events) - Added Method
    private static void CreateTender(FileToImport FTI, string SOURCE_DATE, out GenericObject geoTENDER, out GenericObject geoTenderConfig)
    {
      // CREATE TENDER TG_TENDER_DATA to use later 
      geoTENDER = c_CASL.c_instance("TranSuite_DBAPI.TG_TENDER_DATA");

      // TG_TENDER_DATA
      TenderToImport ITender = FTI.m_Tender;
      string strTenderPath = string.Format("Business.Tender.of.\"{0}\"", ITender.m_TNDRID);
      geoTenderConfig = c_CASL.get_GEO().get_path(strTenderPath, true, new GenericObject()) as GenericObject;

      // HANDLE TENDER FIELDS
      GenericObject geoTenderStdFields = new GenericObject();
      GenericObject geoTenderCustFields = new GenericObject();
      GetTenderFields(geoTenderStdFields, geoTenderCustFields, geoTenderConfig, ITender);

      // geoTENDER.set("AMOUNT", ITender.m_Amount);
      geoTENDER.set("TNDRNBR", 1);
      geoTENDER.set("TNDRID", ITender.m_TNDRID);
      geoTENDER.set("TNDRDESC", geoTenderConfig.get("description", ""));
      geoTENDER.set("TYPEIND", geoTenderConfig.get("TYPEIND", "", true));
      geoTENDER.set("ADDRESS", geoTenderStdFields.get("ADDRESS", ""));
      geoTENDER.set("AUTHACTION", geoTenderStdFields.get("AUTHACTION", ""));
      geoTENDER.set("AUTHNBR", geoTenderStdFields.get("AUTHNBR", ""));
      geoTENDER.set("AUTHSTRING", geoTenderStdFields.get("AUTHSTRING", ""));
      geoTENDER.set("BANKACCTNBR", geoTenderStdFields.get("BANKACCTNBR", ""));
      geoTENDER.set("BANKROUTINGNBR", geoTenderStdFields.get("BANKROUTINGNBR", ""));
      geoTENDER.set("CC_CK_NBR", geoTenderStdFields.get("CC_CK_NBR", ""));
      geoTENDER.set("CCNAME", geoTenderStdFields.get("CCNAME", ""));
      geoTENDER.set("EXPMONTH", geoTenderStdFields.get("EXPMONTH", ""));
      geoTENDER.set("EXPYEAR", geoTenderStdFields.get("EXPYEAR", ""));
      geoTENDER.set("SYSTEXT", geoTenderStdFields.get("SYSTEXT", ""));
      // geoTENDER.set("BR_BANK_ACCT_ID", strBANKACCTID);
      geoTENDER.set("SOURCE_DATE", SOURCE_DATE);
      geoTENDER.set("SOURCE_REFID", "");
      GenericObject geoTENDERCustomFields = c_CASL.c_GEO();
      geoTENDER.set("_custom_keys", geoTENDERCustomFields);
      // Handle Tender custom fields 
      IDictionaryEnumerator myEnumerator = geoTenderCustFields.GetEnumerator() as IDictionaryEnumerator;
      while (myEnumerator.MoveNext())
      {
        string strTCFName = myEnumerator.Key as string;
        string strTCFValue = "" + myEnumerator.Value;
        if (!strTCFName.StartsWith("_"))
        {
          string strFieldPath = string.Format("Tender.of.{0}.{1}_f_custom_field", ITender.m_TNDRID, strTCFName);
          GenericObject CFConfig = c_CASL.get_GEO().get_path(strFieldPath, true, new GenericObject()) as GenericObject;

          geoTENDERCustomFields.insert(strTCFName);
          // [custom_field]=value
          geoTENDER.set(strTCFName, strTCFValue);
          // [custom_field]_f_id=..
          string strCFIDKey = string.Format("{0}_f_id", strTCFName);
          string strCFIDValue = CFConfig.get("id", "") as string;
          geoTENDER.set(strCFIDKey, "");
          // [custom_field]_f_label=..
          string strCFLabelKey = string.Format("{0}_f_label", strTCFName);
          string strCFLabelValue = CFConfig.get("label", "") as string;
          geoTENDER.set(strCFLabelKey, strCFLabelValue);

        }
      }
      // Store configured custom fields belong to tender type
      GenericObject geoTenderCustomFieldsConfig = geoTenderConfig.get("_custom_keys", geoTENDERCustomFields) as GenericObject;
      foreach (object objField in geoTenderCustomFieldsConfig.vectors)
      {
        string strCFTagName = objField as string;
        if (!geoTENDERCustomFields.vectors.Contains(strCFTagName))
        {
          string strFieldPath = string.Format("{0}_f_custom_field", strCFTagName);
          GenericObject CFConfig = geoTenderConfig.get(strFieldPath, null) as GenericObject;
          if (CFConfig != null)
          {
            geoTENDERCustomFields.insert(strCFTagName);
            // [custom_field]=value
            object sCFValue = CFConfig.get("default", "");
            sCFValue = geoTenderConfig.get(strCFTagName + "_f_override", sCFValue);
            if (!geoTENDER.has(strCFTagName))
              geoTENDER.set(strCFTagName, sCFValue);
            // [custom_field]_f_id=..
            string strCFIDKey = string.Format("{0}_f_id", strCFTagName);
            string strCFIDValue = CFConfig.get("id", "") as string;
            geoTENDER.set(strCFIDKey, "");
            // [custom_field]_f_label=..
            string strCFLabelKey = string.Format("{0}_f_label", strCFTagName);
            string strCFLabelValue = CFConfig.get("label", "") as string;
            geoTENDER.set(strCFLabelKey, strCFLabelValue);
            // [custom_field]_f_store_in_DB=..
            string strCFStoreDBKey = string.Format("{0}_f_store_in_DB", strCFTagName);
            object strCFStoreDBValue = CFConfig.get("store_in_DB", false);
            geoTENDER.set(strCFStoreDBKey, strCFStoreDBValue);
          }
        }
      }
      // END of Handle Tender custom fields
    }

    /// <summary>
    /// seperate Tender Standard Fields and Custom Fields based on Tender Type configuration and TenderToImport Custom Fields
    /// </summary>
    /// <param name="geoTenderStdFields"></param>
    /// <param name="geoTenderCustFields"></param>
    /// <param name="geoTenderConfig"></param>
    /// <param name="ITender"></param>
    /// <returns></returns>
    public static string GetTenderFields(GenericObject geoTenderStdFields, GenericObject geoTenderCustFields, GenericObject geoTenderConfig, TenderToImport ITender)
    {
      string strCCNAME_fname = "";
      string strCCNAME_lname = "";
      string strADDRESS_address = "";
      string strADDRESS_address2 = "";
      string strADDRESS_city = "";
      string strADDRESS_state = "";
      string strADDRESS_zip = "";
      string strADDRESS_country = "";

      foreach (object objCustomField in ITender.m_CustomFields)
      {
        CustomFieldToImport ICustomField = objCustomField as CustomFieldToImport;

        if (ICustomField.m_CustomFieldName == "credit_card_nbr" || ICustomField.m_CustomFieldName == "check_nbr")
          geoTenderStdFields.set("CC_CK_NBR", ICustomField.m_CustomFieldValue); //standard field: CC_CK_NBR TODO: mask it CC_CK_NBR 
        else if (ICustomField.m_CustomFieldName == "exp_year")
          geoTenderStdFields.set("EXPMONTH", ICustomField.m_CustomFieldValue);//standard field: EXPMONTH
        else if (ICustomField.m_CustomFieldName == "exp_year")
          geoTenderStdFields.set("EXPYEAR", ICustomField.m_CustomFieldValue);//standard field: EXPYEAR
        else if (ICustomField.m_CustomFieldName == "bank_routing_nbr")
          geoTenderStdFields.set("BANKROUTINGNBR", ICustomField.m_CustomFieldValue);//standard field: BANKROUTINGNBR
        else if (ICustomField.m_CustomFieldName == "bank_account_nbr")
          geoTenderStdFields.set("BANKACCTNBR", ICustomField.m_CustomFieldValue); //standard field: BANKACCTNBR TODO: mask it BANKACCTNBR 
        else if (ICustomField.m_CustomFieldName == "system_comments")
          geoTenderStdFields.set("SYSTEXT", ICustomField.m_CustomFieldValue);//standard field: SYSTEXT
        else if (ICustomField.m_CustomFieldName == "auth_nbr")
          geoTenderStdFields.set("AUTHNBR", ICustomField.m_CustomFieldValue);//standard field: AUTHNBR
        else if (ICustomField.m_CustomFieldName == "auth_str")
          geoTenderStdFields.set("AUTHSTRING", ICustomField.m_CustomFieldValue);//standard field: AUTHSTRING
        else if (ICustomField.m_CustomFieldName == "auth_action")
          geoTenderStdFields.set("AUTHACTION", ICustomField.m_CustomFieldValue);//standard field: AUTHACTION
        else if (ICustomField.m_CustomFieldName == "payer_fname")
          strCCNAME_fname = ICustomField.m_CustomFieldValue;
        else if (ICustomField.m_CustomFieldName == "payer_lname")
          strCCNAME_lname = ICustomField.m_CustomFieldValue;
        else if (ICustomField.m_CustomFieldName == "address")
          strADDRESS_address = ICustomField.m_CustomFieldValue;
        else if (ICustomField.m_CustomFieldName == "address2")
          strADDRESS_address2 = ICustomField.m_CustomFieldValue;
        else if (ICustomField.m_CustomFieldName == "city")
          strADDRESS_city = ICustomField.m_CustomFieldValue;
        else if (ICustomField.m_CustomFieldName == "state")
          strADDRESS_state = ICustomField.m_CustomFieldValue;
        else if (ICustomField.m_CustomFieldName == "zip")
          strADDRESS_zip = ICustomField.m_CustomFieldValue;
        else if (ICustomField.m_CustomFieldName == "country")
          strADDRESS_country = ICustomField.m_CustomFieldValue;

        // CUSTOM FIELD
        geoTenderCustFields.set(ICustomField.m_CustomFieldName, ICustomField.m_CustomFieldValue);
      }
      //standard field: CCNAME
      string strCCNAME = (strCCNAME_fname + " " + strCCNAME_lname) as string;
      if (strCCNAME.Length > 40)
        strCCNAME = strCCNAME.Substring(0, 40);
      geoTenderStdFields.set("CCNAME", strCCNAME);
      //standard field: ADDRESS
      string strADDRESS =
          strADDRESS_address + " "
        + strADDRESS_address2 + " "
        + strADDRESS_city + " "
        + strADDRESS_state + " "
        + strADDRESS_zip + " "
        + strADDRESS_country;
      if (strADDRESS.Length > 100)
        strADDRESS = strADDRESS.Substring(0, 100);
      geoTenderStdFields.set("ADDRESS", strADDRESS);

      return "";
    }


    public static string GetGLACCTNBRFromGEO(GenericObject geoGLACCTNBR)
    {

      string strGLACCTNBR = "";
      ArrayList alGLACCTNBR = new ArrayList();

      string strGLSegmentsPath = string.Format("Gl_number_format.of.primary.segments");
      GenericObject geoGLSegmentsConfig = c_CASL.get_GEO().get_path(strGLSegmentsPath, true, new GenericObject()) as GenericObject;

      int j = 0;
      foreach (object GLPart in geoGLACCTNBR.vectors)
      {
        string strGLPart = GLPart as string;
        GenericObject geoGLSegmentConfig = geoGLSegmentsConfig.vectors[j] as GenericObject;

        string strPadding = "";
        int iPadLength = (int)geoGLSegmentConfig.get("max_size", 0);
        for (int i = strGLPart.Length; i < iPadLength; i++)
          strPadding += " ";
        strGLPart = strPadding + strGLPart;
        strGLACCTNBR = strGLACCTNBR + strGLPart;
        j = j + 1;
      }
      return strGLACCTNBR;
    }
  }
}
