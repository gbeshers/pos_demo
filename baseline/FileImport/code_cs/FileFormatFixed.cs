using System;
using System.Xml.Serialization;

namespace FileImport
{
	/// <summary>
	/// Summary description for FileFormatFixedLength.
	/// </summary>
	public class FileFormatFixed: FileFormat
	{
		//configurable fields 
		[XmlElement("ENDOFLINECHAR")]
		public string m_EndOfLineChar;
		[XmlElement("HEADERRECORDFORMATFIXED")]
		public HeaderRecordFormatFixed m_HeaderRecordFormatObj;
		[XmlElement("TRAILERRECORDFORMATFIXED")]
		public TrailerRecordFormatFixed m_TrailerRecordFormatObj;
		[XmlElement("DETAILEDRECORDFORMATFIXED")]
		public DetailedRecordFormatFixed m_DetailedRecordFormatObj;
		
		public FileFormatFixed()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}
