using System;
using System.Xml.Serialization;
using System.Collections;

namespace FileImport
{
	/// <summary>
	/// Summary description for RecordFormatDelimited.
	/// </summary>
	public class FieldFormatDelimited
	{
		//---------------------------------------------------
		// m_DataType Const
		public const String TEXT = "TEXT";
		public const String CURRENCY = "CURRENCY";

		// Configurable Fields
		[XmlElement("FIELDNBR")]
		public int m_FieldNbr;
		[XmlElement("DATATYPE")]
		public string m_DataType;
		[XmlElement("TRANAMOUNTIND")]
		public bool m_TranAmountInd;
		[XmlElement("IPAYMENTFIELD")]
		public string m_IPaymentField;

		public FieldFormatDelimited()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}

	/// <summary>
	/// Summary description for RecordFormatDelimited.
	/// </summary>
	public class DetailedRecordFormatDelimited
	{
		// Configurable Fields
		[XmlElement("FIELDCOUNT")]
		public int m_FieldCount;
		[XmlElement("IDENTIFIERFIELDNBR")]
		public int m_IdentifierFieldNbr;
		[XmlElement("IDENTIFIER")]
		public string m_Identifier;
		[XmlElement("IMPORTFIELD",typeof(FieldFormatDelimited))]
		public ArrayList m_FieldFormatArray;
//		[XmlElement("IMPORTFIELD")]
//		public FieldFormatDelimited[] m_FieldFormatDelimitedArray;

		public DetailedRecordFormatDelimited()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
	/// <summary>
	/// Summary description for RecordFormatDelimited.
	/// </summary>
	public class HeaderRecordFormatDelimited
	{
		// Configurable Fields
		[XmlElement("FIELDCOUNT")]
		public int m_FieldCount;
		[XmlElement("IDENTIFIERFIELDNBR")]
		public int m_IdentifierFieldNbr;
		[XmlElement("IDENTIFIER")]
		public string m_Identifier;
		[XmlElement("RECORDCOUNTFIELDNBR")]
		public int m_RecordCountFieldNbr;
		[XmlElement("RECORDTOTALFIELDNBR")]
		public int m_RecordTotalFieldNbr;
		[XmlElement("SOURCEGROUPFIELDNBR")]
		public int m_SourceGroupFieldNbr;
		[XmlElement("SOURCEDATEFIELDNBR")]
		public int m_SourceDateFieldNbr;

		public HeaderRecordFormatDelimited()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}

	/// <summary>
	/// Summary description for RecordFormatDelimited.
	/// </summary>
	public class TrailerRecordFormatDelimited
	{
		// Configurable Fields
		[XmlElement("FIELDCOUNT")]
		public int m_FieldCount;
		[XmlElement("IDENTIFIERFIELDNBR")]
		public int m_IdentifierFieldNbr;
		[XmlElement("IDENTIFIER")]
		public string m_Identifier;
		[XmlElement("RECORDCOUNTFIELDNBR")]
		public int m_RecordCountFieldNbr;
		[XmlElement("RECORDTOTALFIELDNBR")]
		public int m_RecordTotalFieldNbr;
		[XmlElement("SOURCEGROUPFIELDNBR")]
		public int m_SourceGroupFieldNbr;
		[XmlElement("SOURCEDATEFIELDNBR")]
		public int m_SourceDateFieldNbr;

		public TrailerRecordFormatDelimited()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}
