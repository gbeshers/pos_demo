using System;
using System.Xml.Serialization;
using System.Collections;

namespace FileImport
{
	/// <summary>
	/// Summary description for RecordFormatFixed.
	/// </summary>
	public class FieldFormatFixed
	{
		//---------------------------------------------------
		// m_DataType Const
		public const String TEXT = "TEXT";
		public const String CURRENCY = "CURRENCY";

		// Configurable Fields
		[XmlElement("FIELDSTART")]
		public int m_FieldStart;
		[XmlElement("FIELDLENGTH")]
		public int m_FieldLength;		
		[XmlElement("DATATYPE")]
		public string m_DataType;
		[XmlElement("TRANAMOUNTIND")]
		public bool m_TranAmountInd;
		[XmlElement("IPAYMENTFIELD")]
		public string m_IPaymentField;
		[XmlElement("PADCHARLEFT")]
		public string m_PadCharleft;
		[XmlElement("PADCHARRIGHT")]
		public string m_PadCharRight;

		public FieldFormatFixed()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}

	/// <summary>
	/// Summary description for RecordFormatFixed.
	/// </summary>
	public class DetailedRecordFormatFixed
	{
		// Configurable Fields
		[XmlElement("RECORDLENGTH")]
		public int m_RecordLength;
		[XmlElement("IDENTIFIERSTART")]
		public int m_IdentifierStart;
		[XmlElement("IDENTIFIER")]
		public string m_Identifier;
		[XmlElement("IMPORTFIELD",typeof(FieldFormatFixed))]
		public ArrayList m_FieldFormatArray;
		//public FieldFormatFixed[] m_FieldFormatArray;// change to Arraylist

		public DetailedRecordFormatFixed()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
	/// <summary>
	/// Summary description for RecordFormatFixed.
	/// </summary>
	public class HeaderRecordFormatFixed
	{
		// Configurable Fields
		[XmlElement("RECORDLENGTH")]
		public int m_RecordLength;
		[XmlElement("IDENTIFIERSTART")]
		public int m_IdentifierStart;
		[XmlElement("IDENTIFIER")]
		public string m_Identifier;
		[XmlElement("RECORDCOUNTSTART")]
		public int m_RecordCountStart;
		[XmlElement("RECORDCOUNTLENGTH")]
		public int m_RecordCountLength;
		[XmlElement("RECORDTOTALSTART")]
		public int m_RecordTotalStart;
		[XmlElement("RECORDTOTALLENGTH")]
		public int m_RecordTotalLength;
		[XmlElement("SOURCEGROUPSTART")]
		public int m_SourceGroupStart;
		[XmlElement("SOURCEGROUPLENGTH")]
		public int m_SourceGroupLength;
		[XmlElement("SOURCEDATESTART")]
		public int m_SourceDateStart;
		[XmlElement("SOURCEDATELENGTH")]
		public int m_SourceDateLength;

		public HeaderRecordFormatFixed()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}

	/// <summary>
	/// Summary description for RecordFormatFixed.
	/// </summary>
	public class TrailerRecordFormatFixed
	{
		// Configurable Fields
		[XmlElement("RECORDLENGTH")]
		public int m_RecordLength;
		[XmlElement("IDENTIFIERSTART")]
		public int m_IdentifierStart;
		[XmlElement("IDENTIFIER")]
		public string m_Identifier;
		[XmlElement("RECORDCOUNTSTART")]
		public int m_RecordCountStart;
		[XmlElement("RECORDCOUNTLENGTH")]
		public int m_RecordCountLength;
		[XmlElement("RECORDTOTALSTART")]
		public int m_RecordTotalStart;
		[XmlElement("RECORDTOTALLENGTH")]
		public int m_RecordTotalLength;
		[XmlElement("SOURCEGROUPSTART")]
		public int m_SourceGroupStart;
		[XmlElement("SOURCEGROUPLENGTH")]
		public int m_SourceGroupLength;
		[XmlElement("SOURCEDATESTART")]
		public int m_SourceDateStart;
		[XmlElement("SOURCEDATELENGTH")]
		public int m_SourceDateLength;

		public TrailerRecordFormatFixed()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}
