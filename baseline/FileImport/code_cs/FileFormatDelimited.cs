using System;
using System.Xml.Serialization;

namespace FileImport
{
	/// <summary>
	/// Summary description for FileFormatVariableLength.
	/// </summary>
	public class FileFormatDelimited: FileFormat
	{
		//---------------------------------------------------
		// Delimiter Const
		public const String COMMA = ",";
		public const String VERTICALBAR = "|";

		//configurable fields 
		[XmlElement("DELIMITER")]
		public string m_Delimiter; 
		[XmlElement("ENDOFLINECHAR")]
		public string m_EndOfLineChar;
		[XmlElement("HEADERRECORDFORMATDELIMITED")]
		public HeaderRecordFormatDelimited m_HeaderRecordFormatObj;
		[XmlElement("TRAILERRECORDFORMATDELIMITED")]
		public TrailerRecordFormatDelimited m_TrailerRecordFormatObj;
		[XmlElement("DETAILEDRECORDFORMATDELIMITED")]
		public DetailedRecordFormatDelimited m_DetailedRecordFormatObj;

		public FileFormatDelimited()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}
