using System;
using System.Xml.Serialization;
using System.Collections;

namespace FileImport
{
	/// <summary>
	/// Summary desc
	/// ription for ImportFileType.
	/// </summary>
	[XmlRoot("File")]
	public class FileToImport
	{
		/*
		<File> 
		<CoreFileDesc/>
		<CoreFileEffDate/>
		<Transaction>
			<Amount/>
			<Term/>
		</> 
		*/
		//Fields 
		[XmlElement("CoreFileDesc")]
		public string m_CoreFileDesc;
		[XmlElement("CoreFileEffDate")]
		public string m_CoreFileEffDate;
		[XmlElement("WorkGroupID")]
		public string m_WorkGroupID;
		[XmlElement("SourceType")]
		public string m_SourceType;
		[XmlElement("SourceGroup")]
		public string m_SourceGroup;
		[XmlElement("SourceDate")]
		public string m_SourceDate;
		[XmlElement("UserID")]
		public string m_UserID;
		

		//[XmlElement("Transaction")]
		[XmlElement("Transaction",typeof(TranToImport))]
		public ArrayList m_Transactions;//Array of TranToImport	
		[XmlElement("Tender")]
		public TenderToImport m_Tender;	
	

		public FileToImport()
		{		
			//
			// TODO: Add constructor logic here
			//
		}

	}
}
