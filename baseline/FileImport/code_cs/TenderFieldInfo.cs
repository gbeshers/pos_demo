using System;
using System.Collections;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Schema;

namespace FileImport
{
    [XmlRoot("TenderFieldInfo")]
	public class TenderFieldInfo
	{

		[XmlElement("Name")]
		public string m_FieldName;		
		[XmlElement("Value")]
		public string m_FieldValue;
		//Configurable Fields 
		public TenderFieldInfo()
		{
		}
		public TenderFieldInfo(string fieldname,string fieldvalue)
		{
			m_FieldName = fieldname;
			m_FieldValue = fieldvalue;
		}

	}
}
