using System;
using System.Xml.Serialization;
using System.Collections;

namespace FileImport
{
	/// <summary>
	/// Summary desc
	/// ription for ImportFileType.
	/// </summary>
	[XmlRoot("Tender")]
	public class TenderToImport
	{
		/*
        <Transaction>
			<Amount/>
			<Term/> 
		</> 
		*/
		//Fields 
		[XmlElement("TNDRID")]
		public string m_TNDRID;		
		[XmlElement("Amount")]
		public decimal m_Amount;
		[XmlElement("CustomField",typeof(CustomFieldToImport))]
		public ArrayList m_CustomFields;

		public TenderToImport()
		{

		}
	}
}
