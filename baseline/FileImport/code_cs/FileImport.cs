using System;
using CASL_engine;
using System.Xml;
using System.Xml.Serialization;
using TranSuiteServices;
using System.Web;
using System.Web.SessionState;
using System.Web.Util;
using System.IO;	//Stream
using System.Text.RegularExpressions;
using System.Collections;


namespace FileImport
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class FileImport
	{
		private static CreateLogFiles m_Err = new CreateLogFiles();
		public FileImport()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="FileImportInfoXML"></param>
		/// <returns> FileImportXML as following format
		///  <File> 
		///    <Description>
		///    <EffectiveDate>
		///    <Transaction></>
		///      <AccountNbr></>
		///      <Amount></>
		///      <Term></>
		///      <GL></>
		///      <Tender></>
		///   </>
		///  </> 
		///  return <ERROR> <CODE>XXX</CODE> <MESSAGE> XXXX </MESSAGE></ERROR>
		/// </returns>
		public static string CreateImportXML( string FileImportInfoXML, string DBConnectionString)
		{
			
			# region Deserialize FileImportInfo (user input)
			XmlSerializer serializerRequest = new XmlSerializer(typeof(FileImportInfo));
			StringWriter stringWriterRequest = new StringWriter();
			string xmlFileImportInfoBase64= Convert.ToBase64String(System.Text.Encoding.Default.GetBytes(FileImportInfoXML));
			byte[] byteGEOBytes = Convert.FromBase64String(xmlFileImportInfoBase64);
			MemoryStream ms = new MemoryStream();
			ms.Write(byteGEOBytes, 0, byteGEOBytes.Length);
			ms.Position = 0;
			FileImportInfo FII = (FileImportInfo)serializerRequest.Deserialize(ms);
			string strTextFile = FII.m_ImportedFile;
			string strFileImportID = FII.m_FileImportID;
			string strUserID = FII.m_UserID;
			#endregion

			# region retrieve File Import Configuartion
			ConfigurationManagement CM = new ConfigurationManagement(DBConnectionString);
			if(!CM.m_objFileImportTypes.Contains(strFileImportID))
				throw new Exception("iPayment was unable to process the File Import Type "+ strFileImportID + ". It must first be configured.");
			ImportFileType FIT = (ImportFileType)CM.m_objFileImportTypes[strFileImportID];
			String FileFormatType = FIT.m_FileFormatType;
			#endregion

			# region parse file and create FileToImport XML
			// create FileToImport Object 
			string xmlFileToImport = "";
			
			FileToImport FTI = new FileToImport();
			FTI.m_CoreFileDesc=FII.m_CoreFileDesc;
			FTI.m_CoreFileEffDate = FII.m_CoreFileEffDate;
			FTI.m_WorkGroupID = FII.m_WorkGroupID;
			FTI.m_SourceType = FIT.m_IPaymentSourceID;
			FTI.m_SourceGroup =""; // To be filed by file header/trailer
			FTI.m_SourceDate = "";// To be filed by file header/trailer
			FTI.m_UserID = strUserID;

			ArrayList Trans = new ArrayList(); //new TranToImport[100];
			FTI.m_Transactions=Trans; // To be filed  by file detailed records

			TenderToImport Tender= new TenderToImport();
			FTI.m_Tender = Tender;
			Tender.m_TNDRID = FII.m_TenderID;
			Tender.m_Amount = 0.00M; // To be filed by fileTotal
			// Handle tender fields (standard and custom fields) 
			ArrayList FieldsInTender= new ArrayList();
			Tender.m_CustomFields=FieldsInTender; // To be filed  by user input
			foreach(object objTenderField in FII.m_TenderFields)
			{
				TenderFieldInfo tenderField = objTenderField as TenderFieldInfo;
				if(tenderField!=null)
				{
					CustomFieldToImport tenderCFI= new CustomFieldToImport();
					tenderCFI.m_CustomFieldName = tenderField.m_FieldName;
					tenderCFI.m_CustomFieldValue = tenderField.m_FieldValue;
					FieldsInTender.Add(tenderCFI);
				}
			}
			// End of Handle tender fields (standard and custom fields) 


			//Parse the delimited file (validate before parsing) and populate FileToImport 
			string strReturn ="";
			if(FileFormatType == ImportFileType.FIXEDLENGTH)
			 strReturn = ParseFixedFile(FTI, FII,FIT);
			else if (FileFormatType == ImportFileType.VARIABLELENGTH)
			 strReturn = ParseDelimitedFile(FTI, FII,FIT);

			try
			{
				XmlSerializer serializerTemp2 = new XmlSerializer(typeof(FileToImport));
				StringWriter stringWriterTemp2 = new StringWriter();
				serializerTemp2.Serialize(stringWriterTemp2, FTI);
				String xmlTemp2 =  stringWriterTemp2.ToString();
				XmlDocument xml_doc = new XmlDocument();
				xml_doc.LoadXml(xmlTemp2);
				xmlFileToImport= string.Format("<File>{0}</File>",xml_doc.SelectSingleNode("File").InnerXml);

				string log_folder = c_CASL.GEO.get("baseline_physical")+"/FileImport/log/";//temp storing file in server
				m_Err.ErrorLog(log_folder,xmlFileToImport);//DEBUG_CODE
			}
			catch(Exception e)
			{
				Exception ie = e.InnerException;
				string strError = e.Message;
				if(ie!=null)
					strError = strError + ";" + ie.Message;
				string log_folder = c_CASL.GEO.get("baseline_physical")+"/FileImport/log/";//temp storing file in server
				m_Err.ErrorLog(log_folder,strError);
				throw new Exception("Error Occur: "+ strError);
			}

			#endregion

			return xmlFileToImport;
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="arrString"></param>
		/// <param name="index1Based"></param>
		/// <returns> return value string  or empty string if not found</returns>
		public static string getValueFrom1BaseIndex(string[] arrString, int index1Based)
		{
			if(arrString == null || index1Based==0 || index1Based>arrString.Length)
				return "";
			return arrString[index1Based-1];
		}

		public static string getValueFromPosition(string strInput, int start, int length)
		{
			if(strInput == null || start==0 || (start-1+length)>strInput.Length)
				return "";
			return strInput.Substring(start-1,length);
		}

		public static string ConvertTextToCurrency(string text)
		{
            // Bug #12055 Mike O - Use misc.Parse to log errors
            string strCurrency = "0.00";
            if (text.IndexOf('.') > 0)//currency format with decimal point 
                strCurrency = String.Format("{0,0:N2}", misc.Parse<Decimal>(text));
            else //currency format without decimal point 
                strCurrency = String.Format("{0,0:N2}", misc.Parse<Int32>(text) / 100.0);
			return strCurrency;
		}
		/// <summary>
		/// convert decimal to currency format without decimal point
		/// e.g. 1.01 -> 101 
		/// </summary>
		/// <param name="dInput"></param>
		/// <returns></returns>
		public static string ConvertCurrencyTextFromDecimal(decimal dInput)
		{
			string strOutput = String.Format("{0,0:N0}", dInput*100);
			return strOutput;
		}

		public static string ParseDelimitedFile(FileToImport fileToImport, FileImportInfo fileImportInfo, ImportFileType importFileType)
		{
			string strTextFile = fileImportInfo.m_ImportedFile; 

			String FileFormatType = importFileType.m_FileFormatType;
			FileFormatDelimited configFileFormatType = (FileFormatDelimited)importFileType.m_FileFormatObj;
			HeaderRecordFormatDelimited configFileHeader = (HeaderRecordFormatDelimited)configFileFormatType.m_HeaderRecordFormatObj;
			TrailerRecordFormatDelimited configFileTrailer = (TrailerRecordFormatDelimited)configFileFormatType.m_TrailerRecordFormatObj;
			DetailedRecordFormatDelimited configFileDetailed = (DetailedRecordFormatDelimited)configFileFormatType.m_DetailedRecordFormatObj;

			ArrayList Trans = fileToImport.m_Transactions;
			TenderToImport Tender= fileToImport.m_Tender;

			StringReader SR = new StringReader(strTextFile);
			string input = null;
			decimal fileTotal = 0.00M;
			int nextIndexTran = 0;
			string[] fileHeader = null;
			string[] fileTrailer = null;
			while ((input = SR.ReadLine()) != null)
			{

				String[] tmp = Regex.Split(input, configFileFormatType.m_Delimiter);
                if (input.Length == 0) // TTS20929 ignore empty line 
                    continue;
                else if (tmp.Length == 1 && configFileFormatType.m_Delimiter == "")
                {
                    throw new Exception("Invalid File Format. ");
                }
				if(configFileHeader!=null && tmp.Length == configFileHeader.m_FieldCount && getValueFrom1BaseIndex(tmp,configFileHeader.m_IdentifierFieldNbr)==configFileHeader.m_Identifier )
				{   // Header 
					fileHeader = tmp;
					fileToImport.m_SourceGroup =getValueFrom1BaseIndex(tmp,configFileHeader.m_SourceGroupFieldNbr); 
					fileToImport.m_SourceDate = getValueFrom1BaseIndex(tmp,configFileHeader.m_SourceDateFieldNbr);
				}
				else if ( configFileTrailer!=null && tmp.Length == configFileTrailer.m_FieldCount && getValueFrom1BaseIndex(tmp,configFileTrailer.m_IdentifierFieldNbr)==configFileTrailer.m_Identifier )
				{  // Trailer 
					fileTrailer = tmp;
					fileToImport.m_SourceGroup =getValueFrom1BaseIndex(tmp,configFileTrailer.m_SourceGroupFieldNbr); 
					fileToImport.m_SourceDate = getValueFrom1BaseIndex(tmp,configFileTrailer.m_SourceDateFieldNbr);
				}
				else if (configFileDetailed!=null && tmp.Length == configFileDetailed.m_FieldCount && getValueFrom1BaseIndex(tmp,configFileDetailed.m_IdentifierFieldNbr)==configFileDetailed.m_Identifier )
				{  // Detail Record
					TranToImport tempTran= new TranToImport();
					//					Trans[nextIndexTran] = tempTran;
					Trans.Add(tempTran);
					tempTran.m_TTID = importFileType.m_IPaymentTranTypeID;
					tempTran.m_Amount = 0.00M;
					//					CustomFieldToImport[] tranCustomFields= new CustomFieldToImport[1];
					ArrayList tranCustomFields = new ArrayList();
					tempTran.m_CustomFields=tranCustomFields;
					foreach ( object field in configFileDetailed.m_FieldFormatArray)
					{
						FieldFormatDelimited configField = field as FieldFormatDelimited;
						int fieldIndex = configField.m_FieldNbr-1;
						string fieldType = configField.m_DataType;
						string fieldValue = getValueFrom1BaseIndex(tmp,configField.m_FieldNbr);
						string fieldName = configField.m_IPaymentField;
						bool is_amount = configField.m_TranAmountInd;
                        //TTS20929 support multiple tran types in single import file
                        if (fieldName == "IPAYMENTTRANTYPEID")
                        {
                            tempTran.m_TTID = fieldValue.Trim();
                            continue;
                        }
                        else if (fieldType == "CURRENCY")
							fieldValue = ConvertTextToCurrency(fieldValue);
						if(is_amount) 
						{
                            // Bug #12055 Mike O - Use misc.Parse to log errors
							tempTran.m_Amount = misc.Parse<Decimal>(fieldValue);
							fileTotal = fileTotal + tempTran.m_Amount;
						}
						else
						{
							CustomFieldToImport tempCustomField = new CustomFieldToImport();
							//							tranCustomFields[0]=tempCustomField; // Change to ArrayList
							tranCustomFields.Add(tempCustomField);
							tempCustomField.m_CustomFieldName=fieldName;
							tempCustomField.m_CustomFieldValue=fieldValue;
						}	
					}
					nextIndexTran = nextIndexTran+1;
				}
				else
				{
					throw new Exception("Invalid File Format. ");
				}			
			}
			Tender.m_Amount = fileTotal;
			// validate fields in header
			if(fileHeader!=null)	
			{
				int totalHeaderIndex  = configFileHeader.m_RecordTotalFieldNbr;
				int countHeaderIndex  = configFileHeader.m_RecordCountFieldNbr;
				// check file total
				if( totalHeaderIndex != 0 )
				{
					string strTotalFileHeader = getValueFrom1BaseIndex(fileHeader,totalHeaderIndex);
					strTotalFileHeader = ConvertTextToCurrency(strTotalFileHeader);
                    // Bug #12055 Mike O - Use misc.Parse to log errors
					strTotalFileHeader = ConvertCurrencyTextFromDecimal(misc.Parse<Decimal>(strTotalFileHeader));
					string strTotalFileActual = ConvertCurrencyTextFromDecimal(fileTotal);
					
					if(strTotalFileHeader!=strTotalFileActual)
						throw new Exception("Invalid Total Field("+ strTotalFileHeader + ") in File Header. Expected ("+strTotalFileActual+")");
				}
				// check file count
				if( countHeaderIndex != 0 )
				{
					string strCountFileHeader = getValueFrom1BaseIndex(fileHeader,countHeaderIndex);
					int iCountFileHeader = 0;
                    // Bug #12055 Mike O - Use misc.Parse to log errors
					if(strCountFileHeader!="") 
						iCountFileHeader = misc.Parse<Int32>(strCountFileHeader);
					int iCountFileActual = nextIndexTran;
					if(iCountFileHeader!=iCountFileActual)
						throw new Exception("Invalid File Count Field("+ iCountFileHeader + ") in File Header. Expected ("+iCountFileActual+")");
				}
	
			}
			// validate fields in Trailer
			if(fileTrailer!=null)	
			{
				int totalTrailerIndex  = configFileTrailer.m_RecordTotalFieldNbr;
				int countTrailerIndex  = configFileTrailer.m_RecordCountFieldNbr;
				// check file total
				if( totalTrailerIndex != 0 )
				{
					string strTotalFileTrailer = getValueFrom1BaseIndex(fileTrailer,totalTrailerIndex);
					strTotalFileTrailer = ConvertTextToCurrency(strTotalFileTrailer);
                    // Bug #12055 Mike O - Use misc.Parse to log errors
					strTotalFileTrailer = ConvertCurrencyTextFromDecimal(misc.Parse<Decimal>(strTotalFileTrailer));
					string strTotalFileActual = ConvertCurrencyTextFromDecimal(fileTotal);
					
					if(strTotalFileTrailer!=strTotalFileActual)
						throw new Exception("Invalid File Total Amount("+ strTotalFileTrailer + ") in File Trailer. Expected ("+strTotalFileActual+")");
				}
				// check file count
				if( countTrailerIndex != 0 )
				{
					string strCountFileTrailer = getValueFrom1BaseIndex(fileTrailer,countTrailerIndex);
					int iCountFileTrailer = 0;
                    // Bug #12055 Mike O - Use misc.Parse to log errors
					if(strCountFileTrailer!="") 
						iCountFileTrailer = misc.Parse<Int32>(strCountFileTrailer);
					int iCountFileActual = nextIndexTran;
					if(iCountFileTrailer!=iCountFileActual)
						throw new Exception("Invalid File Count Field("+ iCountFileTrailer + ") in File Trailer. Expected ("+iCountFileActual+")");
				}
	
			}
			SR.Close();
			return "";
		}

		public static string ParseFixedFile(FileToImport fileToImport, FileImportInfo fileImportInfo, ImportFileType importFileType)
		{
			string strTextFile = fileImportInfo.m_ImportedFile; 

			String FileFormatType = importFileType.m_FileFormatType;
			FileFormatFixed configFileFormatType = (FileFormatFixed)importFileType.m_FileFormatObj;
			HeaderRecordFormatFixed configFileHeader = (HeaderRecordFormatFixed)configFileFormatType.m_HeaderRecordFormatObj;
			TrailerRecordFormatFixed configFileTrailer = (TrailerRecordFormatFixed)configFileFormatType.m_TrailerRecordFormatObj;
			DetailedRecordFormatFixed configFileDetailed = (DetailedRecordFormatFixed)configFileFormatType.m_DetailedRecordFormatObj;

			ArrayList Trans = fileToImport.m_Transactions;
			TenderToImport Tender= fileToImport.m_Tender;

			StringReader SR = new StringReader(strTextFile);
			string input = null;
			decimal fileTotal = 0.00M;
			int nextIndexTran = 0;
			string fileHeader = "";
			string fileTrailer = "";
			while ((input = SR.ReadLine()) != null)
			{
				input = input.TrimEnd(' ');
                if (input.Length == 0) // TTS20929 ignore empty line 
                    continue;
				else if(configFileHeader!=null && input.Length == configFileHeader.m_RecordLength && getValueFromPosition(input,configFileHeader.m_IdentifierStart,configFileHeader.m_Identifier.Length)==configFileHeader.m_Identifier )
				{   // Header 
					fileHeader = input;
					fileToImport.m_SourceGroup =getValueFromPosition(input,configFileHeader.m_SourceGroupStart,configFileHeader.m_SourceGroupLength); 
					fileToImport.m_SourceDate = getValueFromPosition(input,configFileHeader.m_SourceDateStart,configFileHeader.m_SourceDateLength);
				}
				else if ( configFileTrailer!=null && input.Length == configFileTrailer.m_RecordLength && getValueFromPosition(input,configFileTrailer.m_IdentifierStart,configFileTrailer.m_Identifier.Length)==configFileTrailer.m_Identifier )
				{  // Trailer 
					fileTrailer = input;
					fileToImport.m_SourceGroup =getValueFromPosition(input,configFileTrailer.m_SourceGroupStart,configFileTrailer.m_SourceGroupLength); 
					fileToImport.m_SourceDate = getValueFromPosition(input,configFileTrailer.m_SourceDateStart,configFileTrailer.m_SourceGroupLength);
				}
				else if (configFileDetailed!=null && input.Length == configFileDetailed.m_RecordLength && getValueFromPosition(input,configFileDetailed.m_IdentifierStart,configFileDetailed.m_Identifier.Length)==configFileDetailed.m_Identifier )
				{  // Detail Record
					TranToImport tempTran= new TranToImport();
					Trans.Add(tempTran);
					tempTran.m_TTID = importFileType.m_IPaymentTranTypeID;
					tempTran.m_Amount = 0.00M;
					ArrayList tranCustomFields = new ArrayList();
					tempTran.m_CustomFields=tranCustomFields;
					foreach ( object field in configFileDetailed.m_FieldFormatArray)
					{
						FieldFormatFixed configField = field as FieldFormatFixed;
						string fieldType = configField.m_DataType;
						string fieldValue = getValueFromPosition(input,configField.m_FieldStart,configField.m_FieldLength);
						string fieldName = configField.m_IPaymentField;
						bool is_amount = configField.m_TranAmountInd;
                        //TTS20929 support multiple tran types in single import file
                        if (fieldName == "IPAYMENTTRANTYPEID")
                        {
                            tempTran.m_TTID = fieldValue.Trim();
                            continue;
                        }
                        else if (fieldType == "CURRENCY")
							fieldValue = ConvertTextToCurrency(fieldValue);
						else if(fieldType=="TEXT" && configField.m_PadCharleft!=null && configField.m_PadCharleft!="" )
							fieldValue = fieldValue.TrimStart(configField.m_PadCharleft.ToCharArray()); // trim the leading zero
						else if(fieldType=="TEXT" && configField.m_PadCharRight!=null && configField.m_PadCharRight!="" )
							fieldValue = fieldValue.TrimEnd(configField.m_PadCharRight.ToCharArray()); // trim the leading zero
						
						if(is_amount) 
						{
                            // Bug #12055 Mike O - Use misc.Parse to log errors
							tempTran.m_Amount = misc.Parse<Decimal>(fieldValue);
							fileTotal = fileTotal + tempTran.m_Amount;
						}
						else
						{
							CustomFieldToImport tempCustomField = new CustomFieldToImport();
							//tranCustomFields[0]=tempCustomField; // Change to ArrayList
							tranCustomFields.Add(tempCustomField);
							tempCustomField.m_CustomFieldName=fieldName;
							tempCustomField.m_CustomFieldValue=fieldValue;
						}	
					}
					nextIndexTran = nextIndexTran+1;
				}
				else
				{
					throw new Exception("Invalid File Format. ");
				}			
			}
			Tender.m_Amount = fileTotal;
			// validate fields in header
			if(fileHeader!="")	
			{
				string strTotalFileHeader  = getValueFromPosition(fileHeader, configFileHeader.m_RecordTotalStart, configFileHeader.m_RecordTotalLength);
				string strCountFileHeader  = getValueFromPosition(fileHeader, configFileHeader.m_RecordCountStart, configFileHeader.m_RecordCountLength); 
				
				// check file total
				if( strTotalFileHeader != "" )
				{
					strTotalFileHeader = ConvertTextToCurrency(strTotalFileHeader);
                    // Bug #12055 Mike O - Use misc.Parse to log errors
					strTotalFileHeader = ConvertCurrencyTextFromDecimal(misc.Parse<Decimal>(strTotalFileHeader));
					string strTotalFileActual = ConvertCurrencyTextFromDecimal(fileTotal);
					
					if(strTotalFileHeader!=strTotalFileActual)
						throw new Exception("Invalid Total Field("+ strTotalFileHeader + ") in File Header. Expected ("+strTotalFileActual+")");
				}
				// check file count
				if( strCountFileHeader != "" )
				{
					int iCountFileHeader = 0;
                    // Bug #12055 Mike O - Use misc.Parse to log errors
					if(strCountFileHeader!="") 
						iCountFileHeader = misc.Parse<Int32>(strCountFileHeader);
					int iCountFileActual = nextIndexTran;
					if(iCountFileHeader!=iCountFileActual)
						throw new Exception("Invalid File Count Field("+ iCountFileHeader + ") in File Header. Expected ("+iCountFileActual+")");
				}
	
			}
			// validate fields in Trailer
			if(fileTrailer!="")	
			{
				string strTotalFileTrailer  = getValueFromPosition(fileTrailer, configFileTrailer.m_RecordTotalStart, configFileTrailer.m_RecordTotalLength);
				string strCountFileTrailer  = getValueFromPosition(fileTrailer, configFileTrailer.m_RecordCountStart, configFileTrailer.m_RecordCountLength); 
				// check file total
				if( strTotalFileTrailer != "" )
				{
					strTotalFileTrailer = ConvertTextToCurrency(strTotalFileTrailer);
                    // Bug #12055 Mike O - Use misc.Parse to log errors
					strTotalFileTrailer = ConvertCurrencyTextFromDecimal(misc.Parse<Decimal>(strTotalFileTrailer));
					string strTotalFileActual = ConvertCurrencyTextFromDecimal(fileTotal);
					
					if(strTotalFileTrailer!=strTotalFileActual)
						throw new Exception("Invalid File Total Amount("+ strTotalFileTrailer + ") in File Trailer. Expected ("+strTotalFileActual+")");
				}
				// check file count
				if( strCountFileTrailer != "" )
				{
					int iCountFileTrailer = 0;
                    // Bug #12055 Mike O - Use misc.Parse to log errors
					if(strCountFileTrailer!="") 
						iCountFileTrailer = misc.Parse<Int32>(strCountFileTrailer);
					int iCountFileActual = nextIndexTran;
					if(iCountFileTrailer!=iCountFileActual)
						throw new Exception("Invalid File Count Field("+ iCountFileTrailer + ") in File Trailer. Expected ("+iCountFileActual+")");
				}	
			}
			SR.Close();
			return "";
		}

	}
}

