using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Text;
using System.Text.RegularExpressions; 
using System.Runtime.InteropServices;
using System.Net;  
using System.Threading;
using System.Globalization;
using System.Configuration;

namespace FileImport
{
	/// <summary>
	/// Summary description for WebForm1.
	/// </summary>
	public class FileImportConfig : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.TextBox txtFileID;
		protected System.Web.UI.WebControls.Label lblFileID;
		protected System.Web.UI.WebControls.Label lblFileFormatType;
		protected System.Web.UI.WebControls.TextBox txtFileFormatType;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.TextBox txtIpaymentTranTypeID;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Label Label1;

		private ConfigurationManagement m_ConfigurationManagement=new ConfigurationManagement("");
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			//Creating Log file under [site]\Log\

			if (Page.IsValid)
			{
				ImportFileType objFileType = null;
				string strFileID = txtFileID.Text.ToString().Trim();
				bool is_existed= m_ConfigurationManagement.m_objFileImportTypes.Contains(strFileID);
				if ( is_existed )
					objFileType = m_ConfigurationManagement.m_objFileImportTypes[strFileID] as ImportFileType;
				else 
					objFileType = new ImportFileType();

				string strFileFormatType = txtFileFormatType.Text.ToString().Trim();

				objFileType.m_FileID = txtFileID.Text.ToString().Trim();
				objFileType.m_FileFormatType = strFileFormatType;
				objFileType.m_IPaymentTranTypeID = txtIpaymentTranTypeID.Text.ToString().Trim();
				objFileType.m_IPaymentSourceID = "EPIC";

				if (strFileFormatType == ImportFileType.VARIABLELENGTH)
				{
					FileFormatDelimited objFFD= new FileFormatDelimited();
					objFileType.m_FileFormatObj = objFFD;
					objFFD.m_Delimiter = ",";
					objFFD.m_EndOfLineChar ="LF";

					// Header Format
					HeaderRecordFormatDelimited objHRFD = new HeaderRecordFormatDelimited();
					objFFD.m_HeaderRecordFormatObj  = objHRFD;
					objHRFD.m_FieldCount = 3;
					objHRFD.m_IdentifierFieldNbr=1;
					objHRFD.m_Identifier="H";
					objHRFD.m_RecordCountFieldNbr=2;
					objHRFD.m_RecordTotalFieldNbr=3;

					// Detail Record Format
					DetailedRecordFormatDelimited objDRFD = new DetailedRecordFormatDelimited();
					objFFD.m_DetailedRecordFormatObj  = objDRFD;
					objDRFD.m_FieldCount = 6;
					objDRFD.m_IdentifierFieldNbr=1;
					objDRFD.m_Identifier="D";

					ArrayList arrayFFD = new ArrayList();
					objDRFD.m_FieldFormatArray = arrayFFD;

					//Field Format: ACCOUNTNUMBER
					FieldFormatDelimited FFD0 = new FieldFormatDelimited();
					FFD0.m_FieldNbr=2;
					FFD0.m_DataType="TEXT";
					FFD0.m_TranAmountInd=false;
					FFD0.m_IPaymentField="ACCOUNTNUMBER";
					arrayFFD.Add(FFD0);

					//Field Format: AMOUNT
					FieldFormatDelimited FFD1 = new FieldFormatDelimited();
					FFD1.m_FieldNbr=5;
					FFD1.m_DataType="CURRENCY";
					FFD1.m_TranAmountInd=true;
					FFD1.m_IPaymentField="";
					arrayFFD.Add(FFD1);
				}
				else if (strFileFormatType == ImportFileType.FIXEDLENGTH)
				{
					FileFormatFixed objFFF= new FileFormatFixed();
					objFileType.m_FileFormatObj = objFFF;
					objFFF.m_EndOfLineChar ="LF";

					// Header Format
					HeaderRecordFormatFixed objHRFF = new HeaderRecordFormatFixed();
					objFFF.m_HeaderRecordFormatObj  = objHRFF;
					objHRFF.m_RecordLength = 12;
					objHRFF.m_IdentifierStart=1;
					objHRFF.m_Identifier="H";
					objHRFF.m_SourceDateStart=2;
					objHRFF.m_SourceDateLength =6;
					objHRFF.m_SourceGroupStart=8;
					objHRFF.m_SourceGroupLength =5;

					// Detail Record Format
					DetailedRecordFormatFixed objDRFF = new DetailedRecordFormatFixed();
					objFFF.m_DetailedRecordFormatObj  = objDRFF;
					objDRFF.m_RecordLength = 53;
					objDRFF.m_IdentifierStart=1;
					objDRFF.m_Identifier="D";

					ArrayList arrayFFF = new ArrayList();
					objDRFF.m_FieldFormatArray = arrayFFF;

					//Field Format: STATEMENTDATE
					FieldFormatFixed FFF0 = new FieldFormatFixed();
					FFF0.m_FieldStart = 2;
					FFF0.m_FieldLength = 6;
					FFF0.m_DataType="TEXT";
					FFF0.m_TranAmountInd=false;
					FFF0.m_IPaymentField="STATEMENTDATE";
					arrayFFF.Add(FFF0);
					//Field: STATEMENTID
					FieldFormatFixed FFF1 = new FieldFormatFixed();
					FFF1.m_FieldStart = 8;
					FFF1.m_FieldLength = 10;
					FFF1.m_DataType=FieldFormatFixed.TEXT;
					FFF1.m_TranAmountInd=false;
					FFF1.m_IPaymentField="STATEMENTID";
					arrayFFF.Add(FFF1);
					//Field Format: GUARANTORID
					FieldFormatFixed FFF2 = new FieldFormatFixed();
					FFF2.m_FieldStart = 18;
					FFF2.m_FieldLength = 10;
					FFF2.m_DataType=FieldFormatFixed.TEXT;
					FFF2.m_TranAmountInd=false;
					FFF2.m_IPaymentField="GUARANTORID";
					arrayFFF.Add(FFF2);
					//Field Format: AMOUNT (OWNED)
					FieldFormatFixed FFF3 = new FieldFormatFixed();
					FFF3.m_FieldStart = 28;
					FFF3.m_FieldLength = 8;
					FFF3.m_DataType=FieldFormatFixed.CURRENCY;
					FFF3.m_TranAmountInd=true;
					FFF3.m_IPaymentField="";
					arrayFFF.Add(FFF3);
					//Field Format: AMOUNTPAID
					FieldFormatFixed FFF4 = new FieldFormatFixed();
					FFF4.m_FieldStart = 36;
					FFF4.m_FieldLength = 8;
					FFF4.m_DataType=FieldFormatFixed.CURRENCY;
					FFF4.m_TranAmountInd=false;
					FFF4.m_IPaymentField="AMOUNTPAID";
					arrayFFF.Add(FFF4);
					//Field Format: CHECKNBR
					FieldFormatFixed FFF5 = new FieldFormatFixed();
					FFF5.m_FieldStart = 44;
					FFF5.m_FieldLength = 10;
					FFF5.m_DataType=FieldFormatFixed.TEXT;
					FFF5.m_TranAmountInd=false;
					FFF5.m_IPaymentField="CHECKNBR";
					FFF5.m_PadCharleft ="0";
					arrayFFF.Add(FFF5);

					//Trailer Format
					TrailerRecordFormatFixed objTRFF = new TrailerRecordFormatFixed();
					objFFF.m_TrailerRecordFormatObj  = objTRFF;
					objTRFF.m_RecordLength = 16;
					objTRFF.m_IdentifierStart=1;
					objTRFF.m_Identifier="T";
					objTRFF.m_RecordCountStart=2;
					objTRFF.m_RecordCountLength =6;
					objTRFF.m_RecordTotalStart=8;
					objTRFF.m_RecordTotalLength =9;
				}
				
				// 
				if (is_existed)
					m_ConfigurationManagement.UpdateFileImportType(objFileType);
				else 
					m_ConfigurationManagement.AddFileImportType(objFileType);


			}
		}
	}
}
