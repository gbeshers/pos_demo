using System;
using System.Xml.Serialization;
using System.Collections;

namespace FileImport
{
	/// <summary>
	/// Summary desc
	/// ription for ImportFileType.
	/// </summary>
	[XmlRoot("CustomField")]
	public class CustomFieldToImport
	{
		//Fields
		[XmlElement("Name")]
		public string m_CustomFieldName;		
		[XmlElement("Value")]
		public string m_CustomFieldValue;

		public CustomFieldToImport()
		{

		}
	}
}
