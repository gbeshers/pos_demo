using System;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Collections;

namespace FileImport
{
	/// <summary>
	/// Summary desc
	/// ription for ImportFileType.
	/// </summary>
	//[Serializable]
	[XmlType("Transaction")]
	public class TranToImport//:ISerializable
	{
		/*
		<Transaction>
			<Amount/>
			<Term/> 
		</> 
		*/
		//Fields 
		[XmlElement("TTID")]
		public string m_TTID;		
		[XmlElement("Amount")]
		public decimal m_Amount;

		[XmlElement("CustomField",typeof(CustomFieldToImport))]//[NonSerialized]
		public ArrayList m_CustomFields;

		public TranToImport()
		{

		}
	}
}
