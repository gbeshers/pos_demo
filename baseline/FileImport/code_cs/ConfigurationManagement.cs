using System;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Web.Util;
using System.IO;
using System.Text;
using CASL_engine;
using TranSuiteServices;

namespace FileImport
{
  /// <summary>
  /// Summary description for ConfigurationManagement.
  /// </summary>
  public class ConfigurationManagement
  {
    public Hashtable m_objFileImportTypes = null;
    private CreateLogFiles m_Err = null;
    private string m_LogPath = "";
    public string m_strConn = @"Data Source=SQL2005;Initial Catalog=Grouphealth;User Id=cashier;Password=cashier";
    public ConfigurationManagement(string strConn)
    {
      //
      // TODO: Add constructor logic here
      //
      m_objFileImportTypes = new Hashtable();
      if (strConn != "") m_strConn = strConn;
      RetrieveFileImportTypes();

      m_Err = new CreateLogFiles();
      m_LogPath = HttpContext.Current.Server.MapPath("Log/");
    }

    public Boolean RetrieveFileImportTypes()
    {
      string strConn = m_strConn;
      string strSQL = String.Format(@"SELECT * FROM CBT_FILEIMPORT_CONFIG");
      DataTable dtImportTypes = RetrieveDB(strConn, strSQL);

      try
      {
        m_objFileImportTypes = new Hashtable();

        foreach (DataRow drImportType in dtImportTypes.Rows)
        {
          String strFileID = (String)drImportType["FILETYPE"];
          String strFileDefintion = (String)drImportType["FILEDEFINITION"];
          # region Deserialize strFileDefintion xml to ImportFileType
          XmlSerializer serializerRequest = new XmlSerializer(typeof(ImportFileType));
          StringWriter stringWriterRequest = new StringWriter();
          string xmlFileDefinitonBase64 = Convert.ToBase64String(System.Text.Encoding.Default.GetBytes(strFileDefintion));
          byte[] byteGEOBytes = Convert.FromBase64String(xmlFileDefinitonBase64);
          MemoryStream ms = new MemoryStream();
          ms.Write(byteGEOBytes, 0, byteGEOBytes.Length);
          ms.Position = 0;
          ImportFileType IFT = (ImportFileType)serializerRequest.Deserialize(ms);
          # endregion
          m_objFileImportTypes[strFileID] = IFT;

        }
      }
      catch (Exception e)
      {
        m_Err.ErrorLog(m_LogPath, e.Message);

        // Bug 17849
        Logger.cs_log_trace("Error in RetrieveFileImportTypes", e);
        return false;
      }
      return true;
    }
    
    // IPAY-1681 DJD: Separate Events Default In File Format (New method)
    public static object CASLGetSeparateEventDefaults(params object[] argPairs)
    {
      GenericObject ret = new GenericObject();

      try
      {
        string conn = c_CASL.c_object("TranSuite_DB.Config.db_connection_info.db_connection_string") as string;
        string sql = string.Format(@"SELECT * FROM CBT_FILEIMPORT_CONFIG");
        DataTable dtImportTypes = new DataTable();

        using (SqlConnection sqlConn = new SqlConnection(conn))
        {
          sqlConn.Open();
          using (SqlDataAdapter myDataAdapter = new SqlDataAdapter(sql, conn))
          {
            myDataAdapter.Fill(dtImportTypes);
          }
        }

        foreach (DataRow drImportType in dtImportTypes.Rows)
        {
          string fileID = (string)drImportType["FILETYPE"];
          string fileDefintion = (string)drImportType["FILEDEFINITION"];
          
          // Deserialize strFileDefintion xml to ImportFileType
          XmlSerializer serializerRequest = new XmlSerializer(typeof(ImportFileType));
          StringWriter stringWriterRequest = new StringWriter();
          string xmlFileDefinitonBase64 = Convert.ToBase64String(System.Text.Encoding.Default.GetBytes(fileDefintion));
          byte[] byteGEOBytes = Convert.FromBase64String(xmlFileDefinitonBase64);
          MemoryStream ms = new MemoryStream();
          ms.Write(byteGEOBytes, 0, byteGEOBytes.Length);
          ms.Position = 0;
          ImportFileType IFT = (ImportFileType)serializerRequest.Deserialize(ms);

          ret[fileID] = IFT.m_SeparateEvents == null? false : IFT.m_SeparateEvents.ToLower() == "true";
        }
      }
      catch (Exception e)
      {
        Logger.cs_log_trace("Error in CASLGetSeparateEventSettings", e);
        return null;
      }

      return ret;
    }

    public Boolean UpdateFileImportType(ImportFileType objFileType)
    {
      string strFileID = objFileType.m_FileID;

      // A: generate xml string
      // serialized to xml 
      XmlSerializer serializerRequest = new XmlSerializer(typeof(ImportFileType));
      StringWriter stringWriterRequest = new StringWriter();
      // TODO 062509 remove xml header and other attribute (xmlns:xsd,xmlns:xsi)

      serializerRequest.Serialize(stringWriterRequest, objFileType);
      String xmlRequest = stringWriterRequest.ToString();
      XmlDocument xml_doc = new XmlDocument();
      xml_doc.LoadXml(xmlRequest);
      string xmlFileImportType = string.Format("<IMPORTFILETYPE>{0}</IMPORTFILETYPE>", xml_doc.SelectSingleNode("IMPORTFILETYPE").InnerXml);

      // write to a file
      m_Err.ErrorLog(HttpContext.Current.Server.MapPath("Log/"), xmlRequest);

      // B. save to database
      string strConn = m_strConn;//@"Data Source=SXIAO-W2K3\SQLSERVER2005;Initial Catalog=GROUPHEALTH_Config;User Id=cashier;Password=cashier";
      //UPDATE OTC_PRODUCT SET OtcTaxSwipe='N'  WHERE OtcItem='00302'
      string strSQL = String.Format(@"UPDATE CBT_FILEIMPORT_CONFIG SET FILEDEFINITION='{0}' WHERE FILETYPE='{1}'", xmlFileImportType, strFileID);
      RunSql(strConn, strSQL);
      return true;
    }

    public Boolean AddFileImportType(ImportFileType objFileType)
    {
      string strFileID = objFileType.m_FileID;
      m_objFileImportTypes.Add(strFileID, objFileType);

      // A: generate xml string
      // serialized to xml 
      XmlSerializer serializerRequest = new XmlSerializer(typeof(ImportFileType));
      StringWriter stringWriterRequest = new StringWriter();
      // TODO 062509 remove xml header and other attribute (xmlns:xsd,xmlns:xsi)

      serializerRequest.Serialize(stringWriterRequest, objFileType);
      String xmlRequest = stringWriterRequest.ToString();
      XmlDocument xml_doc = new XmlDocument();
      xml_doc.LoadXml(xmlRequest);
      string xmlFileImportType = string.Format("<IMPORTFILETYPE>{0}</IMPORTFILETYPE>", xml_doc.SelectSingleNode("IMPORTFILETYPE").InnerXml);

      // write to a file
      m_Err.ErrorLog(HttpContext.Current.Server.MapPath("Log/"), xmlFileImportType);

      // B. save to database
      string strConn = m_strConn;// @"Data Source=SXIAO-W2K3\SQLSERVER2005;Initial Catalog=GROUPHEALTH_Config;User Id=cashier;Password=cashier";
      string strSQL = String.Format(@"INSERT INTO CBT_FILEIMPORT_CONFIG(FILETYPE,FILEDEFINITION) VALUES ('{0}','{1}')", strFileID, xmlFileImportType);
      RunSql(strConn, strSQL);

      return true;
    }
    public object RunSql(string ConnString, string SQLString)
    {
      // Bug 16433 [21921] DJD - add using
      try
      {
        using (SqlConnection myConnection = new SqlConnection(ConnString))
        {
          myConnection.Open();
          using (SqlCommand myCommand = myConnection.CreateCommand())
          {
            myCommand.CommandText = (string)SQLString;
            myCommand.ExecuteNonQuery();
          }
        }
      }
      catch (Exception e)
      {
        m_Err.ErrorLog(m_LogPath, "Error Occurs - " + e.Message);

        // Bug 17849
        Logger.cs_log_trace("Error in RunSql", e);
      }
      return null;
    }
    public DataTable RetrieveDB(string ConnString, string SQLString)
    {
      // Bug 16433 [21921] DJD - add using
      DataTable DT = new DataTable();
      try
      {
        using (SqlConnection myConnection = new SqlConnection(ConnString))
        {
          myConnection.Open();
          using (SqlDataAdapter myDataAdapter = new SqlDataAdapter(SQLString, ConnString))
          {
            myDataAdapter.Fill(DT);
          }
        }
      }
      catch (Exception e)
      {
        m_Err.ErrorLog(m_LogPath, "Error Occurs - " + e.Message);

        // Bug 17849
        Logger.cs_log_trace("Error in RetrieveDB", e);
      }

      return DT;

    }
  }
}
