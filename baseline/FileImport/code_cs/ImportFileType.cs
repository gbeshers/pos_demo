using System;
using System.Xml.Serialization;

namespace FileImport
{
	/// <summary>
	/// Summary description for ImportFileType.
	/// </summary>
	[XmlRoot("IMPORTFILETYPE")] 
	public class ImportFileType
	{
		//---------------------------------------------------
		// FileFormatType VALUES
		public const String VARIABLELENGTH = "VARIABLELENGTH";
		public const String FIXEDLENGTH = "FIXEDLENGTH";

		//---------------------------------------------------

		//Configurable Fields 
		[XmlElement("FILEID")]
		public string m_FileID;
		[XmlElement("FILEFORMATTYPE")]
		public string m_FileFormatType; 
		[XmlElement("IPAYMENTTRANTYPEID")]
		public string m_IPaymentTranTypeID;
		[XmlElement("IPAYMENTSOURCEID")]
		public string m_IPaymentSourceID;
		[XmlElement("SEPARATE_EVENTS")] // IPAY-1681 DJD: Separate Events Default In File Format
		public string m_SeparateEvents; // IPAY-1681 DJD: Separate Events Default In File Format
		//[XmlElement("FILEFORMATDELIMITED")]
		[XmlElement("FILEFORMATDELIMITED",typeof(FileFormatDelimited))]
		[XmlElement("FILEFORMATFIXED",typeof(FileFormatFixed))]
		public FileFormat m_FileFormatObj;
		//[XmlElement("FILEFORMATDELIMITED")]
		//public FileFormatDelimited m_FileFormatObj;

		public ImportFileType()
		{
			//
			// TODO: Add constructor logic here
			//

		}
	}
}
