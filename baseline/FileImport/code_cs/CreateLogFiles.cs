using System;
using System.IO;

using System.Text;


namespace FileImport
{
  /// <summary>
  /// Summary description for Crypto.
  /// </summary>
  public class CreateLogFiles
  {
    private string sLogFormat;
    private string sErrorTime;


    public  CreateLogFiles()
    {
      //sLogFormat used to create log files format :
      // dd/mm/yyyy hh:mm:ss AM/PM ==> Log Message
      sLogFormat = DateTime.Now.ToShortDateString().ToString()+" "+DateTime.Now.ToLongTimeString().ToString()+" ==> ";
            
      //this variable used to create log filename format "
      //for example filename : ErrorLogYYYYMMDD
      string sYear    = DateTime.Now.Year.ToString();
      string sMonth    = DateTime.Now.Month.ToString();
      string sDay    = DateTime.Now.Day.ToString();
      sErrorTime = sYear+sMonth+sDay;
    }

    public void ErrorLog(string sPathName, string sErrMsg)
    {
      string FileName = sPathName+sErrorTime+".txt";
      StreamWriter sw ;

      if(File.Exists(FileName))
      {
        sw=File.AppendText(FileName);
      }
      else
      {
       sw = new StreamWriter(FileName,true);
        
      }
      sw.WriteLine(sLogFormat + sErrMsg);
      sw.Flush();
      sw.Close();

    }
  }
}