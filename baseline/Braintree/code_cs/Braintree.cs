﻿using Braintree;
using CASL_engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Threading;

// Bug 25590 MJO - Paypal support (Braintree API)

namespace baseline
{
  class BraintreeClient
  {
    private static object OrderIdLock = new object();
    private static string PreviousOrderId = "";

    private BraintreeClient()
    {
    }

    private static BraintreeGateway CreateGateway(GenericObject systemInterface)
    {
      return new BraintreeGateway
      {
        Environment = systemInterface.get("environment").ToString() == "SANDBOX" ? Braintree.Environment.SANDBOX : Braintree.Environment.PRODUCTION,
        MerchantId = systemInterface.get("merchant_id").ToString(),
        PublicKey = systemInterface.get("public_key").ToString(),
        PrivateKey = DecryptPrivateKey(systemInterface.get("private_key").ToString())
      };
    }

    private static string DecryptPrivateKey(string password)
    {
      byte[] bUserPwdEncrypted = Convert.FromBase64String(password);
      byte[] bUserPwdDecrypted = CASL_engine.Crypto.symmetric_decrypt("data", bUserPwdEncrypted, "secret_key", CASL_engine.Crypto.get_secret_key());
      return Encoding.Default.GetString(bUserPwdDecrypted);
    }

    public static object fCASL_GenerateClientToken(CASL_Frame frame)
    {
      GenericObject systemInterface = frame.args.get("_subject") as GenericObject;

      try
      {
        return GenerateClientToken(systemInterface);
      }
      catch (Exception e)
      {
        return CASL_error.create_str(e.Message).casl_object;
      }
    }

    public static String GenerateClientToken(GenericObject systemInterface)
    {
      return CreateGateway(systemInterface).ClientToken.Generate();
    }

    public static object fCASL_ProcessPayment(CASL_Frame frame)
    {
      GenericObject args = frame.args;

      GenericObject data_obj = args.get("data_obj") as GenericObject;
      string payload = args.get("payload").ToString();
      GenericObject systemInterface = args.get("_subject") as GenericObject;

      return ProcessPayment(data_obj, payload, systemInterface);
    }

    private static object ProcessPayment(GenericObject tender, string payloadJSON, GenericObject systemInterface)
    {
      try
      {
        Payload payload = JsonConvert.DeserializeObject<Payload>(payloadJSON);
        decimal total = Convert.ToDecimal(tender.get("total"));

        List<TransactionLineItemRequest> lineItems = new List<TransactionLineItemRequest>();

        GenericObject geoLineItems = tender.get("braintree_line_items", new GenericObject()) as GenericObject;

        for (int i = 0; i < geoLineItems.getLength(); i++)
        {
          GenericObject item = geoLineItems.get(i) as GenericObject;

          decimal quantity = Convert.ToDecimal(item.get("quantity"));
          decimal unitAmount = Convert.ToDecimal(item.get("unit_amount"));
          string itemName = item.get("name").ToString();

          if (itemName.Length > 35)
            itemName = itemName.Substring(0, 35);

          lineItems.Add(new TransactionLineItemRequest
            {
              Quantity = quantity,
              UnitAmount = unitAmount,
              TotalAmount = quantity * unitAmount,
              Name = itemName,
              LineItemKind = item.get("kind").ToString() == "debit" ? TransactionLineItemKind.DEBIT : TransactionLineItemKind.CREDIT
            }
          );
        }

        Logger.LogInfo("Processing payment for " + tender.get("total") + System.Environment.NewLine + payload.ToString(), "BraintreeClient.ProcessPayment");

        string orderId;
        
        // Bug 26502 MJO - It's unlikely, but make sure that there aren't duplicate ids
        lock (OrderIdLock)
        {
          orderId = DateTime.Now.ToString("yyMMddHHmmssfff");

          while (orderId == PreviousOrderId)
          {
            Thread.Sleep(1);
            orderId = DateTime.Now.ToString("yyMMddHHmmssfff");
          }

          PreviousOrderId = orderId;
        }

        var orderIdField = systemInterface.get("order_id_field", null);
        string orderIdTagname = "PAYMT_ID"; // Bug 26698 MJO - Default to this, but use the configured custom field if available

        // Bug 26502 MJO - Store id as field in tender
        if (orderIdField is GenericObject)
          orderIdTagname = (orderIdField as GenericObject).get("tagname").ToString();


        tender.set(orderIdTagname, orderId);

        var request = new TransactionRequest
        {
          Amount = total,
          OrderId = orderId,
          MerchantAccountId = systemInterface.get("merchant_account_id").ToString(),
          LineItems = lineItems.ToArray(),
          PaymentMethodNonce = payload.nonce,
          Options = new TransactionOptionsRequest
          {
            SubmitForSettlement = true,
            PayPal = new TransactionOptionsPayPalRequest
            {
              CustomField = systemInterface.get("custom_field").ToString(),
              Description = lineItems.Count > 0 ? lineItems[0].Name : "iPayment Sale"
            }
          }
        };

        if (payload.deviceData != null)
          request.DeviceData = payload.deviceData;

        tender.set("credit_card_nbr", "XXXX");

        misc.CASL_call("_subject", c_CASL.execute("System_interface_tracking"), "a_method", "start_request", "args", new GenericObject("a_core_item", tender,
        "request_xml", "Braintree Request", "action", "SALE", "system", systemInterface, "merchant_key", systemInterface.get("merchant_id").ToString()));

        Result<Transaction> result = CreateGateway(systemInterface).Transaction.Sale(request);

        Transaction transaction = result.Target;

        if (result.IsSuccess())
        {
          misc.CASL_call("_subject", c_CASL.execute("System_interface_tracking"), "a_method", "update", "args", new GenericObject("a_core_item", tender,
          "status", c_CASL.execute("System_interface_tracking.PostedSuccess"), "reference", transaction.Id, "response", payload.details.ToString(), "system", systemInterface));

          tender.set(
            "payer_fname", payload.details.firstName,
            "payer_lname", payload.details.lastName,
            "payer_name", payload.details.firstName + " " + payload.details.lastName,
            "payment_type", payload.type,
            "payer_id", payload.details.payerId,
            "auth_str", transaction.Id, // Bug 27327 - Use auth string
            orderIdTagname, transaction.Id
          );

          // Bug 26030 MJO - Only set email if it's not there
          if (String.IsNullOrWhiteSpace((string)tender.get("EMAIL_RECEIPT", "")))
            tender.set("EMAIL_RECEIPT", payload.details.email);

          Logger.LogInfo("Sale processed successfully. Transaction ID: " + transaction.Id + ", Order ID: " + orderId, "BraintreeClient.ProcessSale");

          return true;
        }
        else
        {
          misc.CASL_call("_subject", c_CASL.execute("System_interface_tracking"), "a_method", "update", "args", new GenericObject("a_core_item", tender,
          "status", c_CASL.execute("System_interface_tracking.PostedFailure"), "response", result.Message, "system", systemInterface));

          
          Logger.LogInfo("Sale response error: " + result.Message, "BraintreeClient.ProcessPayment");

          return CASL_error.create_str(result.Message).casl_object;
        }

      }
      catch (Exception e)
      {
        Logger.LogError("Braintree error: " + e.Message, "ProcessPayment");
        return CASL_error.create_str(e.Message).casl_object;
      }
    }

    public static object fCASL_ProcessRefund(CASL_Frame frame)
    {
      GenericObject args = frame.args;

      GenericObject data_obj = args.get("data_obj") as GenericObject;
      string transactionId = args.get("transaction_id").ToString();
      GenericObject systemInterface = args.get("_subject") as GenericObject;

      return ProcessRefund(data_obj, transactionId, systemInterface);
    }

    private static object ProcessRefund(GenericObject tender, string transactionId, GenericObject systemInterface)
    {
      try
      {
        if (tender != null)
        {
          tender.set("credit_card_nbr", "XXXX");

          misc.CASL_call("_subject", c_CASL.execute("System_interface_tracking"), "a_method", "start_request", "args", new GenericObject("a_core_item", tender,
          "request_xml", "Braintree Request", "action", "VOID", "system", systemInterface, "merchant_key", systemInterface.get("merchant_id").ToString()));
        }

        Result<Transaction> result = CreateGateway(systemInterface).Transaction.Refund(transactionId);

        if (result.IsSuccess())
        {
          // Bug IPAY-132 MJO - Store refund transaction id
          string refundId = result.Target.Id;

          if (tender != null)
          {
            misc.CASL_call("_subject", c_CASL.execute("System_interface_tracking"), "a_method", "update", "args", new GenericObject("a_core_item", tender,
            "status", c_CASL.execute("System_interface_tracking.Completed"), "reference", refundId, "response", "Success", "system", systemInterface));
          }

          Logger.LogInfo("Refund processed successfully. Transaction ID: " + refundId, "BraintreeClient.ProcessRefund");

          return true;
        }
        else
        {
          if (tender != null)
          {
            misc.CASL_call("_subject", c_CASL.execute("System_interface_tracking"), "a_method", "update", "args", new GenericObject("a_core_item", tender,
            "status", c_CASL.execute("System_interface_tracking.PostedFailure"), "response", result.Message, "system", systemInterface));
          }

          Logger.LogInfo("Refund response error: " + result.Message, "BraintreeClient.ProcessRefund");
        }

        return false;
      }
      catch (Exception e)
      {
        Logger.LogError("Braintree error: " + e.Message, "ProcessRefund");
        return CASL_error.create_str(e.Message).casl_object;
      }
    }

#pragma warning disable 0649
    [JsonObject(MemberSerialization.OptIn)]
    private class Payload
    {
      [JsonProperty]
      public string nonce;
      [JsonProperty]
      public string type;
      [JsonProperty]
      public PayloadDetails details;
      [JsonProperty]
      public string deviceData;

      public override string ToString()
      {
        StringBuilder sb = new StringBuilder();

        sb.AppendLine("Nonce: " + nonce);
        sb.AppendLine("Type: " + type);
        sb.AppendLine("Device Data: " + deviceData);
        sb.Append(details.ToString());

        return sb.ToString();
      }
    }
    [JsonObject(MemberSerialization.OptIn)]
    private class PayloadDetails
    {
      [JsonProperty]
      public string email;
      [JsonProperty]
      public string firstName;
      [JsonProperty]
      public string lastName;
      [JsonProperty]
      public string payerId;
      [JsonProperty]
      public string countryCode;

      public override string ToString()
      {
        StringBuilder sb = new StringBuilder();

        sb.AppendLine("E-mail: " + email);
        sb.AppendLine("First Name: " + firstName);
        sb.AppendLine("Last Name: " + lastName);
        sb.AppendLine("Payer Id: " + payerId);
        sb.AppendLine("Country Code: " + countryCode);

        return sb.ToString();
      }
    }
#pragma warning restore 0649
  }
}
