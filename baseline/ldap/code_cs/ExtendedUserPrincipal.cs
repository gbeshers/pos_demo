﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.DirectoryServices;
using System.Security.Authentication;
using System.DirectoryServices.AccountManagement;
//using ActiveDs;  // Depends on the COM/Active DS type library!?

namespace ldap
{
  [DirectoryObjectClass("user")]
  [DirectoryRdnPrefix("CN")]
  public class ExtendedUserPrincipal : UserPrincipal
  {
    /*
     * Many of the important behaviors associated with a Windows account in Active Directory, 
     * such as enabled/disabled status, are controlled by an attribute called userAccountControl. 
     * This attribute contains a 32-bit integer that represents a bitwise enumeration of various 
     * flags that control account behavior.
     *
     * These flags are represented in ADSI by an enumerated constant called ADS_USER_FLAG. 
     * Because this enumeration is so important in terms of working with user objects in 
     * System.DirectoryServices (SDS), we will convert the ADSI enumeration into a .NET-style enumeration
     * as shown below:
    */
    public enum AdsUserFlags
    {
      Script = 1,                  // 0x1
      AccountDisabled = 2,              // 0x2
      HomeDirectoryRequired = 8,           // 0x8 
      AccountLockedOut = 16,             // 0x10
      PasswordNotRequired = 32,           // 0x20
      PasswordCannotChange = 64,           // 0x40
      EncryptedTextPasswordAllowed = 128,      // 0x80
      TempDuplicateAccount = 256,          // 0x100
      NormalAccount = 512,              // 0x200
      InterDomainTrustAccount = 2048,        // 0x800
      WorkstationTrustAccount = 4096,        // 0x1000
      ServerTrustAccount = 8192,           // 0x2000
      PasswordDoesNotExpire = 65536,         // 0x10000
      MnsLogonAccount = 131072,           // 0x20000
      SmartCardRequired = 262144,          // 0x40000
      TrustedForDelegation = 524288,         // 0x80000
      AccountNotDelegated = 1048576,         // 0x100000
      UseDesKeyOnly = 2097152,            // 0x200000
      DontRequirePreauth = 4194304,          // 0x400000
      PasswordExpired = 8388608,           // 0x800000
      TrustedToAuthenticateForDelegation = 16777216, // 0x1000000
      NoAuthDataRequired = 33554432         // 0x2000000
    };

    public ExtendedUserPrincipal(PrincipalContext context) : base(context) { }

    public ExtendedUserPrincipal(PrincipalContext context,
                string samAccountName,
                string password,
                bool enabled
          )
      : base(context, samAccountName, password, enabled) { }


    [DirectoryProperty("msds-user-account-control-computed")]
    public bool IspasswordExpired
    {
      get
      {
        object[] result = this.ExtensionGet("msds-user-account-control-computed");
        if (result != null)
        {
          int ret = (int)result[0];
          return (ret & (int)AdsUserFlags.PasswordExpired) == (int)AdsUserFlags.PasswordExpired ? true : false;
        }
        else
        {
          return false;
        }
      }
    }

    // Note: "LdapUser" was used in example which may be a subclass of UserPrincipal
    public static new ExtendedUserPrincipal FindByIdentity(PrincipalContext context, string identityValue)
    {
      return (ExtendedUserPrincipal)FindByIdentityWithType(context,
              typeof(ExtendedUserPrincipal),
              identityValue
                );
    }

    public static new ExtendedUserPrincipal FindByIdentity(PrincipalContext context,
        IdentityType identityType,
        string identityValue)
    {
      return (ExtendedUserPrincipal)FindByIdentityWithType(context,
        typeof(ExtendedUserPrincipal),
        identityType,
        identityValue
      );
    }
  }
}

