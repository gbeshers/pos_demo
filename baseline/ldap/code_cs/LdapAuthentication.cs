using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Security.Authentication;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices;

using CASL_engine;

//bug 12018 disable compiler warnings.
#pragma warning disable 162
namespace ldap
{
  /// <summary>
  /// Class used to connect to LDAP and check a user's credentials and group membership.
  /// If speed is a concern, we could cache the PrincipalContext as an instance (or even static) 
  /// variable and only change when the domain, context-options, and/or baseDN changes.
  /// If we saved this as instance variable, it could speed up group checks.  If we stored as a
  /// static then it could speed all queries, but access would have to be synchonized.
  /// Preliminary testing does not show much of a performance hit when connecting so it is
  /// is nto clear that this would help much.
  /// <seealso>http://172.16.1.141/bugzilla/show_bug.cgi?id=9757</seealso>
  /// <seealso>http://172.16.1.141/bugzilla/show_bug.cgi?id=12257</seealso>
  /// </summary>
  public class LdapAuthentication
  {
    private ContextType _contextType = ContextType.Domain;  // In the future may want to make settable

    const bool timeOps = false;

    // Cached connection parameters
    private string _domain;
    private string _baseDN = null;
    private ContextOptions _contextOptions = new ContextOptions();
    private bool _contextOptionsSet = false;
    private bool _checkGroup = false;
    private String _groupMembership = "";


    /// <summary>
    /// Cached PrincipalContext.  Used so that the group check does not have to re-establish 
    /// a connection.
    /// </summary>
    //private PrincipalContext cachedPrincipalContext = null;

    private const String _contextHelp =
            @"This field should be a combination of 
            Negotiate, Signing, Sealing, and ServerBind 
            separated by a | character.  
            This field must contain Negotiate.  
            Only choose ServerBind when using a specific server name in the Active Directory Domain field.";

    /// <summary>
    /// Create a new LdapAuthentication object with just the group and domain parameters.
    /// </summary>
    /// <param name="checkGroup"></param>
    /// <param name="groupMembership"></param>
    /// <param name="domain"></param>
    public LdapAuthentication(bool checkGroup, String groupMembership, string domain)
    {
      _checkGroup = checkGroup;
      _groupMembership = groupMembership;
      _domain = domain;

    }

    /// <summary>
    /// Create a new LdapAuthentication object with group, domain, and baseDN 
    /// set.
    /// </summary>
    /// <param name="checkGroup"></param>
    /// <param name="groupMembership"></param>
    /// <param name="domain"></param>
    /// <param name="baseDN"></param>
    public LdapAuthentication(bool checkGroup, String groupMembership, string domain, String baseDN)
    {
      _checkGroup = checkGroup;
      _groupMembership = groupMembership;
      _domain = domain;
      _baseDN = baseDN;
    }

    /// <summary>
    /// Create a new LdapAuthentication object with group, domain, base DN, 
    /// and connection context options set.
    /// </summary>
    /// <param name="checkGroup"></param>
    /// <param name="groupMembership"></param>
    /// <param name="domain"></param>
    /// <param name="baseDN"></param>
    /// <param name="contextOptions"></param>
    public LdapAuthentication(bool checkGroup, String groupMembership, string domain, String baseDN, List<String> contextOptions)
    {
      _checkGroup = checkGroup;
      _groupMembership = groupMembership;
      _domain = domain;
      _baseDN = baseDN;
      _contextOptions = convertContextOptions(contextOptions);
      _contextOptionsSet = true;
    }

    /// <summary>
    /// Setup the connection based on the Base DN and secure channel options.
    /// </summary>
    /// <returns></returns>
    private PrincipalContext getPrincipalContext()
    {
      DateTime startTime = DateTime.Now;
      try
      {
        if (_baseDN == null && _contextOptionsSet == false)
          return new PrincipalContext(_contextType, _domain);
        else if (_contextOptionsSet == false)
          return new PrincipalContext(_contextType, _domain, _baseDN);
        else if (_baseDN == null)
          throw new Exception("If a Context is specified, the BaseDN must also be specified");
        else
          return new PrincipalContext(_contextType, _domain, _baseDN, _contextOptions);
      }
      finally
      {
        if (timeOps)
        {
          DateTime stopTime = DateTime.Now;
          TimeSpan duration = stopTime - startTime;
          Logger.cs_log("getPrincipalContext() took this long (milliseconds): " +
            duration.Milliseconds);
        }
      }
    }

    /// <summary>
    /// Setup the connection based on the Base DNa dn secure channel options and
    /// use the username and password.
    /// </summary>
    /// <param name="userName"></param>
    /// <param name="password"></param>
    /// <returns></returns>
    private PrincipalContext getPrincipalContext(string userName, string password)
    {
      DateTime startTime = DateTime.Now;
      try
      {
        if (_baseDN == null && _contextOptionsSet == false)
          return new PrincipalContext(_contextType, _domain, userName, password);
        else if (_contextOptionsSet == false)
          return new PrincipalContext(_contextType, _domain, _baseDN, userName, password);
        else if (_baseDN == null)
          throw new Exception("If a Context is specified, the BaseDN must also be specified");
        else
          return new PrincipalContext(_contextType, _domain, _baseDN, _contextOptions, userName, password);
      }
      finally 
      {
        if (timeOps)
        {
          DateTime stopTime = DateTime.Now;
          TimeSpan duration = stopTime - startTime;
          Logger.cs_log("getPrincipalContext(username, password) took this long (milliseconds): " +
            duration.Milliseconds);
        }
      }

    }


    /// <summary>
    /// Main method for authenticating a user against LDAP. 
    /// Ideally we want to use ValidateCredentials because that 
    /// seems to be faster. However if a user has specified 
    /// a Base DN or secure channel options then ValidateCredentials
    /// will not use them so I am forced to use the potentially
    /// slower FindByIdentity.
    /// 
    /// Note:  There may be a minimum set of group properties an AD account 
    /// should have the Read permission for the FindByIdentity method 
    /// to instantiate the corresponding GroupPrincipal object.
    /// See http://forums.asp.net/p/1615859/4137185.aspx#4137185?GroupPrincipal.FindByIdentity+Permissions
    /// 
    /// Note: Page 34 here shows a way to use validateCredentials() with a base DN and context:
    /// http://www.docstoc.com/docs/23931588/Whats-New-in-NET-Directory-Services-Programming
    /// Create the context with the base DN and then run the validateCredentials() with the 
    /// context.
    /// See http://msdn.microsoft.com/en-us/library/bb300969.aspx.
    /// 
    /// Note: At least one serious bug was found in .NET 3.5 support for AM 
    /// (http://travisspencer.com/blog/2009/07/creating-users-that-work-with.html) that could affect us 
    /// (users created with ADSI edit may not be found by AccountManagement unless precautions are taken when the user is created).  
    /// 
    /// There are other bugs in the .NET 3.5 Active Directory interface such as a memory leak, 
    /// some of which are fixed in this hotpatch: 
    /// http://code.msdn.microsoft.com/KB969166 and more importantly: http://support.microsoft.com/kb/969166.
    /// 
    /// </summary>
    /// <param name="userName"></param>
    /// <param name="password"></param>
    /// <returns></returns>
    public bool validateUser(string userName, string password)
    {
      try
      {
        // If the BaseDN and context are not set; do a simple full search
        if (_baseDN == null && _contextOptionsSet == false)
        {
          using (PrincipalContext pc = getPrincipalContext(userName, password))
          {
            return pc.ValidateCredentials(userName, password);
          }
        }
        // If there is Context and/or BaseDN, then ValidateCredentials() will not check them!
        // Use FindByIdentity instead.
        // NOTE: FindByIdentity can be slow but it is only called when a BaseDN or context is
        // used.  The query might actually go faster with no BaseDN!
        else
        {
          using (PrincipalContext pc = getPrincipalContext(userName, password))
          {
            
            UserPrincipal user =
                UserPrincipal.FindByIdentity(
                    pc,
                    userName);

            return user != null;

          }
        }

      }
      /// Translate errors for humans
      catch (PrincipalServerDownException e)
      {
        String humanReadable;
        String msg = e.Message;

        if (msg.Contains("The LDAP server is unavailable"))
          humanReadable = "Unable to contact the LDAP server.  Check the LDAP Domain setting in the configuration page or the local DNS settings.";
        else if (msg.StartsWith("The server could not be contacted"))
          humanReadable = "Unable to contact the LDAP server.  Check the Domain and Context settings.";
        else if (msg.Contains("The server is not operational")) 
          humanReadable = @"The LDAP server cannot be contacted. 
              Check the Active Directory Domain and Channel Security Context fields.
              ServerBind cannot be used against a domain."; 
        else
          humanReadable = e.Message;
        Logger.cs_log(humanReadable + "  Exception: " + e.ToMessageAndCompleteStacktrace()); // Bug 17849
        // Bug 10099: FT: Setup messages for activity log
        throw CASL_error.create(
           "message", humanReadable,
           "log_summary", "LDAP connection error",
           "log_detail", "Error:" + humanReadable);     // Bug 10099

      }
      catch (Exception e)
      {
        // TODO: Catch all these exceptions based on the exception name instead of grouping here
        String msg = e.Message;
        String humanReadable;
        bool showFullError = false;
        // First catch the most common case; a simple bad username or password
        if (msg.Contains("Logon failure: unknown user name or bad password"))
          return false;
        if (userName == null || userName == "")
          humanReadable = "The Username must be set";
        else if (password == null || password == "")
          humanReadable = "The Password must be set";
        else if (msg.StartsWith("There is no such object on the server"))  // PrincipalOperationException
          humanReadable = "The Base DN was not found on the Active Directory server";
        else if (msg.StartsWith("An invalid dn syntax has been specified"))
          humanReadable = "The Base DN has an incorrect format";
        else if (msg.StartsWith("The ContextOptions passed are invalid for this store type"))  // System.ArgumentException
          humanReadable = @"The Channel Security Context field is not valid.  " + _contextHelp;
        else if (msg.Contains("The server is not operational"))
        {
          humanReadable = "The server is not operational.  This may be caused by using the ServerBind option while identifying the LDAP server with a domain";
          showFullError = true;
        }
        else if (msg.StartsWith("The server could not be contacted"))
        {
          humanReadable = "Unable to contact the LDAP server.  Check the Active Directory Domain settings";
          showFullError = true;
        }
        else if (msg.Contains("A referral was returned from the server"))
        {
          humanReadable = "The Base DN was not found or you need to use a fully qualified Domain";
          showFullError = true;
        }
        else if (msg.Contains("PrincipalServerDownException"))
        {
          humanReadable = @"Unable to contact the LDAP server.  
            LDAP may be down or IIS may not be configured to support the chosen Channel Security Context fields";
          showFullError = true;
        }
        else if (msg.Contains("Object reference not set to an instance of an object")) {
          humanReadable = @"LDAP settings are incorrect.  Check the Base DN and other settings";
          showFullError = true;
        }
        else if (msg.Contains("An operations error occurred"))
        {
          humanReadable = @"An operations error occurred.  
                      Check the LDAP settings such as the Base DN and Channel Security Context Options";
          showFullError = true;
        }
        else
        {
          humanReadable = "Unexpected error";
          showFullError = true;
        }

        // Give more information for the unusual errors
        if (showFullError)
        {
          String message = String.Format("User '{0}': {1}",
            userName, humanReadable);

          // Bug 10230: FT: Fixed format
          String detail = String.Format("LDAP: {0}: {1}: {2}",
            message, getLdapArgs(), e.Message);

          // Bug 10099: FT: Setup messages for activity log
          throw CASL_error.create(
            "message", humanReadable,
            "log_summary", "Login Failed",
            "log_detail", "Error:" + detail.Replace(',','|'));     // Bug 10099
        }
        else
        {
          String message =
            String.Format("LDAP: User '{0}': {1}",
              userName, humanReadable);
          String detail = message +  getLdapArgs();

          // Bug 10099: FT: Setup messages for activity log
          throw CASL_error.create(
            "message", humanReadable,
            "log_summary", "Login Failed",
            "log_detail", "Error:" + detail.Replace(',','|'));     // Bug 10099
        }

      }
    }

    /// <summary>
    /// Bug 10099: FT: Summarize the LDAP settings for activity log
    /// </summary>
    /// <returns></returns>
    private string getLdapArgs()
    {
      var ldapArgs = new StringBuilder();
      ldapArgs.Append("Domain: ").Append(_domain);    // Bug 10099
      if (_baseDN != null)
        ldapArgs.Append(",Base DN: ").Append(_baseDN);    // Bug 10099
      if (_contextOptionsSet)
        ldapArgs.Append(",Context: ").Append(_contextOptions.ToString());   // Bug 10099

      if (_checkGroup)
        ldapArgs.Append(",Group: ").Append(_groupMembership);   // Bug 10099

      return ldapArgs.ToString();
    }

    /// <summary>
    /// 
    /// If the user cannot be found (because of, say, a bad baseDN), you will get this message:
    /// PrincipalOperationException: "Object reference not set to an instance of an object"
    /// 
    /// If the username/password is bad, then expect this:
    /// "System.Security.Authentication.AuthenticationException: Logon failure: unknown user name or bad password"
    /// 
    /// If the PrincipleContext is created without a username/password, you will get this:
    /// System.Runtime.InteropServices.COMException (0x80072020): An operations error occurred
    /// 
    /// If you want to speed this up, cache the PrincipalContext in an instance variable.  
    /// 
    /// Consider calling the isSecurityGroup() to only search for security groups instead of all
    /// groups.  [Non-security groups are groups that are used for things like email or distribution
    /// lists]
    /// 
    /// Note that there is a memory leak in FindByIdentity() that is fixed by the below patch or
    /// by upgrading to .NET 4.0.
    /// See <seealso>http://support.microsoft.com/kb/969166</seealso>  
    /// The GetAuthorizationGroups() call may be effected by this also.
    /// 
    /// </summary>
    /// <param name="userName"></param>
    /// <param name="password"></param>
    /// <param name="groupName"></param>
    /// <param name="groups"></param>
    /// <returns></returns>
    public bool checkGroupMembership(string userName, string password, string groupName, out string groups)
    {
      groups = "";
      StringBuilder groupList = new StringBuilder();

      try
      {
        // Determine if the group name is a DN or just a CN
        bool dn = groupName.Contains("=");

        using (PrincipalContext pc = getPrincipalContext(userName, password))
        {
          DateTime startTime = DateTime.Now;

          UserPrincipal user =
              UserPrincipal.FindByIdentity(
                  pc,
                  userName);

            DateTime stopTime = DateTime.Now;
            TimeSpan duration = stopTime - startTime;
            if (timeOps)
            {
              Logger.cs_log("FindByIdentity took this long (milliseconds): " +
              duration.Milliseconds);
          }

          // get the authorization groups for the user principal and
          // store the results in a PrincipalSearchResult object
          startTime = DateTime.Now;
          PrincipalSearchResult<Principal> results =
              user.GetAuthorizationGroups();

          if (timeOps)
          {
            stopTime = DateTime.Now;
            duration = stopTime - startTime;
            Logger.cs_log("GetAuthorizationGroups took this long (milliseconds): " +
              duration.Milliseconds);
          }

          // display the names of the groups to which the
          // user belongs
          foreach (Principal result in results)
          {
            String name;
            if (dn)
              name = result.DistinguishedName;
            else  // Do I need Name, SamAccountName, or UserPrincipleName?
              name = result.Name;

            if (groupList.Length == 0)
              groupList.Append("'");
            else
              groupList.Append(", '");
            groupList.Append(name).Append("'");

            if (name.Equals(groupName))
              return true;
          }
          groups = groupList.ToString();
          return false;
        }

      }

      catch (PrincipalOperationException e)
      {
        String humanReadable;
        // An exception can be thrown in FindByIdentity
        if (e.Message.Contains("An invalid dn syntax has been specified"))
          humanReadable = "The Base DN has an incorrect format";
        else if (e.Message.Contains("Information about the domain could not be retrieved"))
          humanReadable = "Cannot read the LDAP groups because the LDAP server domain could not be found.  Is the server setup in DNS?";
        else
          humanReadable = e.Message;

        // Bug 10099: FT: Setup messages for activity log
        throw CASL_error.create(
          "message", humanReadable,
          "log_summary", "Login Failed",
          "log_detail", "Error: " + humanReadable + e.Message);     // Bug 10099

      }

      catch (AuthenticationException e)
      {
        // An exception can be thrown in FindByIdentity
        if (e.Message.Contains("unknown user name or bad password"))
          return false;
        else 
        {
          Logger.cs_log_trace("Ldap error", e);
          String msg = "Cannot query the LDAP groups: " + e.Message;
          // Bug 10099: FT: Setup messages for activity log
          throw CASL_error.create(
            "message", msg,
            "log_summary", "LDAP group check failure",
            "log_detail", "Error:" + e.Message);      // Bug 10099
        }
      }
      catch (Exception e)
      {
        String msg;
        msg = "Error reading the Active Directory groups.  Error " + e.Message;
        //Logger.cs_log(String.Format("{0}: Ldap error: {1}", msg, e.ToString()));
        Logger.cs_log(String.Format("{0}: Ldap error: {1}", msg, e.ToMessageAndCompleteStacktrace())); // Bug 17849
        // Bug 10099: FT: Setup messages for activity log
        throw CASL_error.create(
         "message", msg,
         "log_summary", "LDAP group check failure",
         "log_detail", "Error:" + e.Message);     // Bug 10099
      }

    }


    private ContextOptions convertContextOptions(List<string> stringContextOptions)
    {
      ContextOptions options = new ContextOptions();

      foreach (String option in stringContextOptions)
      {
        switch (option)
        {
          case "Negotiate":
            options = options | ContextOptions.Negotiate;
            break;
          // Bug 10141: FT: Disable SecureSocketLayer and SimpleBind
          //case "SimpleBind":
            //options = options | ContextOptions.SimpleBind;
            //break;
          //case "SecureSocketLayer":
          //  options = options | ContextOptions.SecureSocketLayer;
           // break;
          case "Signing":
            options = options | ContextOptions.Signing;
            break;
          case "Sealing":
            options = options | ContextOptions.Sealing;
            break;
          case "ServerBind":
            options = options | ContextOptions.ServerBind;
            break;
          default:
            String msg = String.Format(@"Bad Channel Security Context option '{0}' used. " + _contextHelp,
                  option);
            // Bug 10099: FT: Setup messages for activity log
            throw CASL_error.create(
               "message", msg,
               "log_summary", "LDAP config error",
               "log_detail", "Error: " + msg);   // Bug 10099
        }
      }
      return options;
    }


    /// <summary>
    /// Test method that could be used for displaying all the groups that a user is a 
    /// member of.  Consider checking the isSecurityGroup() method.
    /// </summary>
    /// <param name="userName"></param>
    /// <param name="password"></param>
    /// <param name="baseDN"></param>
    /// <param name="contextOptions"></param>
    /// <returns></returns>
    public List<String> GetAllGroups(string userName, string password,
      string baseDN, ContextOptions contextOptions)
    {
      var groups = new List<String>();
      try
      {


        // Set the container (Base DN) and secure channel (ContextOptions) also
        PrincipalContext domainContext = new PrincipalContext(ContextType.Domain,
          _domain,
          baseDN,
          contextOptions,
          userName, password);

        /*
         * Joe Kaplan recommends: use a GroupPrincipal as the QBE object and just specify Name = "*" 
         * */
        UserPrincipal user =
            UserPrincipal.FindByIdentity(
                domainContext,
                userName);

        // Get the authorization groups for the user principal and
        // store the results in a PrincipalSearchResult object.
        // This may only get the "Security" groups which is what we want.
        PrincipalSearchResult<Principal> results =
            user.GetAuthorizationGroups();

        // display the names of the groups to which the
        // user belongs
        foreach (Principal result in results)
        {
          groups.Add(result.DistinguishedName);
        }
        return groups;

      }
      catch (Exception e)
      {
        Logger.cs_log(String.Format("GetGroups: Error reading groups for {0} in base DN {2}.  Error: {1}", userName, e.ToMessageAndCompleteStacktrace(), baseDN)); // Bug 17849
        return groups;
      }

    }

    /// <summary>
    /// Test the settings configured in the LDAP configuration page to make
    /// sure that a connection can be made.
    /// </summary>
    /// <param name="argPairs"></param>
    /// <returns></returns>
    public static GenericObject TestConnectionCASL(params object[] argPairs)
    {
      DateTime startTime = DateTime.Now;
      DateTime groupStartTime = DateTime.Now;
      DateTime groupStopTime = DateTime.Now;

      try
      {
        GenericObject geoParams = misc.convert_args(argPairs);
        String username = (String)geoParams.get("username");
        String password = (String)geoParams.get("password");
        String domain = (String)geoParams.get("domain");
        String baseDN = (String)geoParams.get("baseDN", "");
        String contextString = (String)geoParams.get("channelContext", "");
        bool checkGroup = (bool)geoParams.get("checkGroup", false);
        String groupName = (String)geoParams.get("groupName", "");
        bool enableVerification = (bool)geoParams.get("enableVerification", false);

        GenericObject results = new GenericObject();

        if (!enableVerification)
        {
          results.Add("success", true);
          return results;
        }


        String connectionInfo = "domain: " + domain;
        LdapAuthentication ldap;
        if (baseDN == "" && contextString == "")
          ldap = new LdapAuthentication(checkGroup, groupName, domain);
        else if (contextString == "")
        {
          ldap = new LdapAuthentication(checkGroup, groupName, domain, baseDN);
          connectionInfo = connectionInfo + " baseDN: " + baseDN;
        }
        else if (baseDN == "")
        {
          // Bug 10099: FT: Setup messages for activity log
          throw CASL_error.create(
             "message", "The Base DN field must be set if the Channel Security Context is set.",
             "log_summary", "Login LDAP config error",
             "log_detail", "Error:The Base DN field must be set if the Channel Security Context is set."); // Bug 10099
        }
        else
        {
          var contextOptions = new List<String>();

          // Catch Context format errors
          try
          {
            var contextArray = contextString.Split('|');
            contextOptions = new List<String>(contextArray);
            connectionInfo = connectionInfo + " Context: ";
            foreach (String context in contextOptions)
              connectionInfo = connectionInfo + context + ", ";
          }
          catch (Exception)//12081 removed unused variable
          {
            // Bug 10099: FT: Setup messages for activity log
            throw CASL_error.create(
               "message", "The Channel Security Context is not correct." + _contextHelp,
               "log_summary", "Login LDAP config error",
               "log_detail", "Error: The Channel Security Context is not correct");  // Bug 10099
          }
          // Get a connection with a baseDN and context
          ldap = new LdapAuthentication(checkGroup, groupName, domain, baseDN, contextOptions);
        }

        // Will only work if there is an LDAP admin account
        //LdapStatus status;
        //bool expired = ldap.isAccountExpiredOrLocked(username, out status);

        bool validated = ldap.validateUser(username, password);
        if (!validated)
        {
          // Bug 10099: FT: Setup messages for activity log
          String detailedError = String.Format("User {0} was not found in Active Directory or the password could be incorrect/expired or the account could be locked.",
            username);
          results.Add("success", false);
          results.Add("human_readable_error", "Username or password did not validate against Active Directory. The password could be incorrect/expired or the account could be locked/expired.");
          results.Add("detailed_error", detailedError);
          results.Add("log_summary", "Login Failed");
          results.Add("log_detail", "Error:" + detailedError.Replace(',','|'));  // Bug 10099: FT: Added Username to log message
          return results;
        }


        if (!checkGroup)
        {
          results.Add("success", true);
          return results;
        }


        // User validated; now check group membership
        String groups;
        groupStartTime = DateTime.Now;
        bool inGroup = ldap.checkGroupMembership(username, password, groupName, out groups);
        groupStopTime = DateTime.Now;


        results.Add("success", inGroup);

        if (inGroup)
          results.Add("success_message", String.Format("User was found in '{0}' and in the '{1}' group.", connectionInfo, groupName));
        else
          results.Add("human_readable_error",
            String.Format("User '{0}' was found in Active Directory but not in group '{1}'. This user is a member of the following groups: {2}.",
        username, groupName, groups));

        return results;
      }
      finally 
      {
        if (timeOps)
        {
          DateTime stopTime = DateTime.Now;
          TimeSpan duration = stopTime - startTime;
          Logger.cs_log("Total LDAP credential check took this long (milliseconds): " +
            duration.Milliseconds);
          TimeSpan groupDuration = groupStopTime - groupStartTime;
          Logger.cs_log("Group credential check took this long (milliseconds): " +
            groupDuration.Milliseconds);
          Logger.cs_log("Just credential check took this long (milliseconds): " +
            (duration.Milliseconds - groupDuration.Milliseconds));
        }
      }
    }

    /// <summary>
    /// Test method for demonstrating a method for checking if an account is locked out
    /// or its password is expired.  This depends on a "Service" account to establish
    /// the initial connection which has not currently been approved.
    /// This is not currently used.
    /// This really has a problem if the username or password is null so protect against this.
    /// </summary>
    /// <param name="userName"></param>
    /// <param name="password"></param>
    /// <param name="status"></param>
    /// <returns></returns>
    private bool isAccountExpiredOrLocked(string userName, out LdapStatus status)
    {
      const int PASSWORD_EXPIRED_BIT = 8388608;
      status = new LdapStatus(true);

      using (PrincipalContext pc = getPrincipalContext("some-guest-admin_username", "some-guest_password"))
      {
        DateTime startTime = DateTime.Now;

        UserPrincipal user =
            UserPrincipal.FindByIdentity(
                pc,
                userName);

        if (user.IsAccountLockedOut())
        {
          status.setErr("Account is locked", "Account is locked out", false);
          return true;
        }


        DirectoryEntry directoryEntry = (DirectoryEntry)user.GetUnderlyingObject();


        string msDS = "msDS-User-Account-Control-Computed";

        using (directoryEntry)
        {
          directoryEntry.RefreshCache(
              new string[] { msDS }
              );

          int isExpired = (int)directoryEntry.Properties[msDS].Value
              & PASSWORD_EXPIRED_BIT;

          //Logger.cs_log("EXPIRATION CHECKING: " + userName + "expired: " + Convert.ToBoolean(isExpired));
          bool expired = Convert.ToBoolean(isExpired);
          if (expired)
            status.setErr("The password for this account has expired", "The password for this account has expired", false);
          return expired;
        }
      }
    }
  }
}
#pragma warning restore 162
