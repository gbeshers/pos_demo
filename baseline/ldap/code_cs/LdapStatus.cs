﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace ldap
{
  /// <summary>
  /// Object for holding detailed error and informational errors.
  /// <seealso>http://172.16.1.141/bugzilla/show_bug.cgi?id=9757</seealso>
  /// <seealso>http://172.16.1.141/bugzilla/show_bug.cgi?id=12257</seealso>
  /// </summary>
  class LdapStatus
  {
    internal bool success { get; set; }
    /// <summary>
    /// Can be set when success is true
    /// </summary>
    internal String comments { get; set; }

    internal String humanReadableError { get; set; }
    internal String detailedError { get; set; }
    internal String trace { get; set; }

    /// <summary>
    /// Construct with a status
    /// </summary>
    /// <param name="success"></param>
    public LdapStatus(bool success)
    {
      this.success = success;
    }

    internal void setErr(String humanReadable, String error, bool dumpStack)
    {
      this.humanReadableError = humanReadable;
      this.detailedError = error;

      if (dumpStack)
      {
        var stackInfo = new StringBuilder();
        var stackTrace = new StackTrace(true);
        String indent = "";
        int counter = 0;
        foreach (var r in stackTrace.GetFrames())
        {
          // Skip the setErr() method
          if (counter++ == 0)
            continue;
          // Enough is enough
          if (counter > 6)
            break;
          stackInfo.
              Append(indent).
              Append(String.Format("Filename: {0} Method: {1} Line: {2} Column: {3}",
              r.GetFileName(), r.GetMethod(), r.GetFileLineNumber(),
              r.GetFileColumnNumber())).
              Append("\n\n");
          indent = indent + "  ";
        }
        this.trace = stackInfo.ToString();
      }
      else
      {
        trace = "";
      }
    }
  }
}
