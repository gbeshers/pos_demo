﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace pci.CoreRepositoryService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="CompositeType", Namespace="http://schemas.datacontract.org/2004/07/CoreRepositoryService")]
    [System.SerializableAttribute()]
    public partial class CompositeType : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool BoolValueField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string StringValueField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool BoolValue {
            get {
                return this.BoolValueField;
            }
            set {
                if ((this.BoolValueField.Equals(value) != true)) {
                    this.BoolValueField = value;
                    this.RaisePropertyChanged("BoolValue");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string StringValue {
            get {
                return this.StringValueField;
            }
            set {
                if ((object.ReferenceEquals(this.StringValueField, value) != true)) {
                    this.StringValueField = value;
                    this.RaisePropertyChanged("StringValue");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="RepositoryItem", Namespace="http://schemas.datacontract.org/2004/07/CoreRepositoryService")]
    [System.SerializableAttribute()]
    public partial class RepositoryItem : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ext_01Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ext_02Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ext_03Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ext_04Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ext_05Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ext_06Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ext_07Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ext_08Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ext_09Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ext_10Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ext_11Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ext_12Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ext_13Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ext_14Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ext_15Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ext_16Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ext_17Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ext_18Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ext_19Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ext_20Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<System.DateTime> ext_date_01Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<System.DateTime> ext_date_02Field;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ext_01 {
            get {
                return this.ext_01Field;
            }
            set {
                if ((object.ReferenceEquals(this.ext_01Field, value) != true)) {
                    this.ext_01Field = value;
                    this.RaisePropertyChanged("ext_01");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ext_02 {
            get {
                return this.ext_02Field;
            }
            set {
                if ((object.ReferenceEquals(this.ext_02Field, value) != true)) {
                    this.ext_02Field = value;
                    this.RaisePropertyChanged("ext_02");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ext_03 {
            get {
                return this.ext_03Field;
            }
            set {
                if ((object.ReferenceEquals(this.ext_03Field, value) != true)) {
                    this.ext_03Field = value;
                    this.RaisePropertyChanged("ext_03");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ext_04 {
            get {
                return this.ext_04Field;
            }
            set {
                if ((object.ReferenceEquals(this.ext_04Field, value) != true)) {
                    this.ext_04Field = value;
                    this.RaisePropertyChanged("ext_04");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ext_05 {
            get {
                return this.ext_05Field;
            }
            set {
                if ((object.ReferenceEquals(this.ext_05Field, value) != true)) {
                    this.ext_05Field = value;
                    this.RaisePropertyChanged("ext_05");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ext_06 {
            get {
                return this.ext_06Field;
            }
            set {
                if ((object.ReferenceEquals(this.ext_06Field, value) != true)) {
                    this.ext_06Field = value;
                    this.RaisePropertyChanged("ext_06");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ext_07 {
            get {
                return this.ext_07Field;
            }
            set {
                if ((object.ReferenceEquals(this.ext_07Field, value) != true)) {
                    this.ext_07Field = value;
                    this.RaisePropertyChanged("ext_07");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ext_08 {
            get {
                return this.ext_08Field;
            }
            set {
                if ((object.ReferenceEquals(this.ext_08Field, value) != true)) {
                    this.ext_08Field = value;
                    this.RaisePropertyChanged("ext_08");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ext_09 {
            get {
                return this.ext_09Field;
            }
            set {
                if ((object.ReferenceEquals(this.ext_09Field, value) != true)) {
                    this.ext_09Field = value;
                    this.RaisePropertyChanged("ext_09");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ext_10 {
            get {
                return this.ext_10Field;
            }
            set {
                if ((object.ReferenceEquals(this.ext_10Field, value) != true)) {
                    this.ext_10Field = value;
                    this.RaisePropertyChanged("ext_10");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ext_11 {
            get {
                return this.ext_11Field;
            }
            set {
                if ((object.ReferenceEquals(this.ext_11Field, value) != true)) {
                    this.ext_11Field = value;
                    this.RaisePropertyChanged("ext_11");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ext_12 {
            get {
                return this.ext_12Field;
            }
            set {
                if ((object.ReferenceEquals(this.ext_12Field, value) != true)) {
                    this.ext_12Field = value;
                    this.RaisePropertyChanged("ext_12");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ext_13 {
            get {
                return this.ext_13Field;
            }
            set {
                if ((object.ReferenceEquals(this.ext_13Field, value) != true)) {
                    this.ext_13Field = value;
                    this.RaisePropertyChanged("ext_13");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ext_14 {
            get {
                return this.ext_14Field;
            }
            set {
                if ((object.ReferenceEquals(this.ext_14Field, value) != true)) {
                    this.ext_14Field = value;
                    this.RaisePropertyChanged("ext_14");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ext_15 {
            get {
                return this.ext_15Field;
            }
            set {
                if ((object.ReferenceEquals(this.ext_15Field, value) != true)) {
                    this.ext_15Field = value;
                    this.RaisePropertyChanged("ext_15");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ext_16 {
            get {
                return this.ext_16Field;
            }
            set {
                if ((object.ReferenceEquals(this.ext_16Field, value) != true)) {
                    this.ext_16Field = value;
                    this.RaisePropertyChanged("ext_16");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ext_17 {
            get {
                return this.ext_17Field;
            }
            set {
                if ((object.ReferenceEquals(this.ext_17Field, value) != true)) {
                    this.ext_17Field = value;
                    this.RaisePropertyChanged("ext_17");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ext_18 {
            get {
                return this.ext_18Field;
            }
            set {
                if ((object.ReferenceEquals(this.ext_18Field, value) != true)) {
                    this.ext_18Field = value;
                    this.RaisePropertyChanged("ext_18");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ext_19 {
            get {
                return this.ext_19Field;
            }
            set {
                if ((object.ReferenceEquals(this.ext_19Field, value) != true)) {
                    this.ext_19Field = value;
                    this.RaisePropertyChanged("ext_19");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ext_20 {
            get {
                return this.ext_20Field;
            }
            set {
                if ((object.ReferenceEquals(this.ext_20Field, value) != true)) {
                    this.ext_20Field = value;
                    this.RaisePropertyChanged("ext_20");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<System.DateTime> ext_date_01 {
            get {
                return this.ext_date_01Field;
            }
            set {
                if ((this.ext_date_01Field.Equals(value) != true)) {
                    this.ext_date_01Field = value;
                    this.RaisePropertyChanged("ext_date_01");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<System.DateTime> ext_date_02 {
            get {
                return this.ext_date_02Field;
            }
            set {
                if ((this.ext_date_02Field.Equals(value) != true)) {
                    this.ext_date_02Field = value;
                    this.RaisePropertyChanged("ext_date_02");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="RepositoryResponse", Namespace="http://schemas.datacontract.org/2004/07/CoreRepositoryService")]
    [System.SerializableAttribute()]
    public partial class RepositoryResponse : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string detailedErrorMsgField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string errorCodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string humanReadableErrorMsgField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private CoreRepositoryService.RepositoryResponse.SeverityLevel severityLevelField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool successField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string detailedErrorMsg {
            get {
                return this.detailedErrorMsgField;
            }
            set {
                if ((object.ReferenceEquals(this.detailedErrorMsgField, value) != true)) {
                    this.detailedErrorMsgField = value;
                    this.RaisePropertyChanged("detailedErrorMsg");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string errorCode {
            get {
                return this.errorCodeField;
            }
            set {
                if ((object.ReferenceEquals(this.errorCodeField, value) != true)) {
                    this.errorCodeField = value;
                    this.RaisePropertyChanged("errorCode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string humanReadableErrorMsg {
            get {
                return this.humanReadableErrorMsgField;
            }
            set {
                if ((object.ReferenceEquals(this.humanReadableErrorMsgField, value) != true)) {
                    this.humanReadableErrorMsgField = value;
                    this.RaisePropertyChanged("humanReadableErrorMsg");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public CoreRepositoryService.RepositoryResponse.SeverityLevel severityLevel {
            get {
                return this.severityLevelField;
            }
            set {
                if ((this.severityLevelField.Equals(value) != true)) {
                    this.severityLevelField = value;
                    this.RaisePropertyChanged("severityLevel");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool success {
            get {
                return this.successField;
            }
            set {
                if ((this.successField.Equals(value) != true)) {
                    this.successField = value;
                    this.RaisePropertyChanged("success");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
        [System.Runtime.Serialization.DataContractAttribute(Name="RepositoryResponse.SeverityLevel", Namespace="http://schemas.datacontract.org/2004/07/CoreRepositoryService")]
        public enum SeverityLevel : int {
            
            [System.Runtime.Serialization.EnumMemberAttribute()]
            no_error = 0,
            
            [System.Runtime.Serialization.EnumMemberAttribute()]
            fatal = 1,
            
            [System.Runtime.Serialization.EnumMemberAttribute()]
            error = 2,
            
            [System.Runtime.Serialization.EnumMemberAttribute()]
            warning = 3,
            
            [System.Runtime.Serialization.EnumMemberAttribute()]
            info = 4,
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="CoreRepositoryService.ICoreRepositoryService")]
    public interface ICoreRepositoryService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICoreRepositoryService/GetData", ReplyAction="http://tempuri.org/ICoreRepositoryService/GetDataResponse")]
        string GetData(int value);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICoreRepositoryService/GetData", ReplyAction="http://tempuri.org/ICoreRepositoryService/GetDataResponse")]
        System.Threading.Tasks.Task<string> GetDataAsync(int value);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICoreRepositoryService/GetDataUsingDataContract", ReplyAction="http://tempuri.org/ICoreRepositoryService/GetDataUsingDataContractResponse")]
        CoreRepositoryService.CompositeType GetDataUsingDataContract(CoreRepositoryService.CompositeType composite);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICoreRepositoryService/GetDataUsingDataContract", ReplyAction="http://tempuri.org/ICoreRepositoryService/GetDataUsingDataContractResponse")]
        System.Threading.Tasks.Task<CoreRepositoryService.CompositeType> GetDataUsingDataContractAsync(CoreRepositoryService.CompositeType composite);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICoreRepositoryService/addRepositoryItem", ReplyAction="http://tempuri.org/ICoreRepositoryService/addRepositoryItemResponse")]
        CoreRepositoryService.RepositoryResponse addRepositoryItem(CoreRepositoryService.RepositoryItem repositoryItem);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICoreRepositoryService/addRepositoryItem", ReplyAction="http://tempuri.org/ICoreRepositoryService/addRepositoryItemResponse")]
        System.Threading.Tasks.Task<CoreRepositoryService.RepositoryResponse> addRepositoryItemAsync(CoreRepositoryService.RepositoryItem repositoryItem);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICoreRepositoryService/getRepositoryItem", ReplyAction="http://tempuri.org/ICoreRepositoryService/getRepositoryItemResponse")]
        CoreRepositoryService.getRepositoryItemResponse getRepositoryItem(CoreRepositoryService.getRepositoryItemRequest request);
        
        // CODEGEN: Generating message contract since the operation has multiple return values.
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICoreRepositoryService/getRepositoryItem", ReplyAction="http://tempuri.org/ICoreRepositoryService/getRepositoryItemResponse")]
        System.Threading.Tasks.Task<CoreRepositoryService.getRepositoryItemResponse> getRepositoryItemAsync(CoreRepositoryService.getRepositoryItemRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICoreRepositoryService/removeRepositoryItem", ReplyAction="http://tempuri.org/ICoreRepositoryService/removeRepositoryItemResponse")]
        CoreRepositoryService.RepositoryResponse removeRepositoryItem(string s);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICoreRepositoryService/removeRepositoryItem", ReplyAction="http://tempuri.org/ICoreRepositoryService/removeRepositoryItemResponse")]
        System.Threading.Tasks.Task<CoreRepositoryService.RepositoryResponse> removeRepositoryItemAsync(string s);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICoreRepositoryService/deleteRepositoryItem", ReplyAction="http://tempuri.org/ICoreRepositoryService/deleteRepositoryItemResponse")]
        CoreRepositoryService.RepositoryResponse deleteRepositoryItem(string s);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICoreRepositoryService/deleteRepositoryItem", ReplyAction="http://tempuri.org/ICoreRepositoryService/deleteRepositoryItemResponse")]
        System.Threading.Tasks.Task<CoreRepositoryService.RepositoryResponse> deleteRepositoryItemAsync(string s);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICoreRepositoryService/modifyRepositoryItem", ReplyAction="http://tempuri.org/ICoreRepositoryService/modifyRepositoryItemResponse")]
        CoreRepositoryService.RepositoryResponse modifyRepositoryItem(string s, CoreRepositoryService.RepositoryItem repositoryItem);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICoreRepositoryService/modifyRepositoryItem", ReplyAction="http://tempuri.org/ICoreRepositoryService/modifyRepositoryItemResponse")]
        System.Threading.Tasks.Task<CoreRepositoryService.RepositoryResponse> modifyRepositoryItemAsync(string s, CoreRepositoryService.RepositoryItem repositoryItem);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="getRepositoryItem", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class getRepositoryItemRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public string s;
        
        public getRepositoryItemRequest() {
        }
        
        public getRepositoryItemRequest(string s) {
            this.s = s;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="getRepositoryItemResponse", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class getRepositoryItemResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public CoreRepositoryService.RepositoryItem getRepositoryItemResult;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=1)]
        public CoreRepositoryService.RepositoryResponse repositoryResponse;
        
        public getRepositoryItemResponse() {
        }
        
        public getRepositoryItemResponse(CoreRepositoryService.RepositoryItem getRepositoryItemResult, CoreRepositoryService.RepositoryResponse repositoryResponse) {
            this.getRepositoryItemResult = getRepositoryItemResult;
            this.repositoryResponse = repositoryResponse;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ICoreRepositoryServiceChannel : CoreRepositoryService.ICoreRepositoryService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class CoreRepositoryServiceClient : System.ServiceModel.ClientBase<CoreRepositoryService.ICoreRepositoryService>, CoreRepositoryService.ICoreRepositoryService {
        
        public CoreRepositoryServiceClient() {
        }
        
        public CoreRepositoryServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public CoreRepositoryServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public CoreRepositoryServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public CoreRepositoryServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public string GetData(int value) {
            return base.Channel.GetData(value);
        }
        
        public System.Threading.Tasks.Task<string> GetDataAsync(int value) {
            return base.Channel.GetDataAsync(value);
        }
        
        public CoreRepositoryService.CompositeType GetDataUsingDataContract(CoreRepositoryService.CompositeType composite) {
            return base.Channel.GetDataUsingDataContract(composite);
        }
        
        public System.Threading.Tasks.Task<CoreRepositoryService.CompositeType> GetDataUsingDataContractAsync(CoreRepositoryService.CompositeType composite) {
            return base.Channel.GetDataUsingDataContractAsync(composite);
        }
        
        public CoreRepositoryService.RepositoryResponse addRepositoryItem(CoreRepositoryService.RepositoryItem repositoryItem) {
            return base.Channel.addRepositoryItem(repositoryItem);
        }
        
        public System.Threading.Tasks.Task<CoreRepositoryService.RepositoryResponse> addRepositoryItemAsync(CoreRepositoryService.RepositoryItem repositoryItem) {
            return base.Channel.addRepositoryItemAsync(repositoryItem);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        CoreRepositoryService.getRepositoryItemResponse CoreRepositoryService.ICoreRepositoryService.getRepositoryItem(CoreRepositoryService.getRepositoryItemRequest request) {
            return base.Channel.getRepositoryItem(request);
        }
        
        public CoreRepositoryService.RepositoryItem getRepositoryItem(string s, out CoreRepositoryService.RepositoryResponse repositoryResponse) {
            CoreRepositoryService.getRepositoryItemRequest inValue = new CoreRepositoryService.getRepositoryItemRequest();
            inValue.s = s;
            CoreRepositoryService.getRepositoryItemResponse retVal = ((CoreRepositoryService.ICoreRepositoryService)(this)).getRepositoryItem(inValue);
            repositoryResponse = retVal.repositoryResponse;
            return retVal.getRepositoryItemResult;
        }
        
        public System.Threading.Tasks.Task<CoreRepositoryService.getRepositoryItemResponse> getRepositoryItemAsync(CoreRepositoryService.getRepositoryItemRequest request) {
            return base.Channel.getRepositoryItemAsync(request);
        }
        
        public CoreRepositoryService.RepositoryResponse removeRepositoryItem(string s) {
            return base.Channel.removeRepositoryItem(s);
        }
        
        public System.Threading.Tasks.Task<CoreRepositoryService.RepositoryResponse> removeRepositoryItemAsync(string s) {
            return base.Channel.removeRepositoryItemAsync(s);
        }
        
        public CoreRepositoryService.RepositoryResponse deleteRepositoryItem(string s) {
            return base.Channel.deleteRepositoryItem(s);
        }
        
        public System.Threading.Tasks.Task<CoreRepositoryService.RepositoryResponse> deleteRepositoryItemAsync(string s) {
            return base.Channel.deleteRepositoryItemAsync(s);
        }
        
        public CoreRepositoryService.RepositoryResponse modifyRepositoryItem(string s, CoreRepositoryService.RepositoryItem repositoryItem) {
            return base.Channel.modifyRepositoryItem(s, repositoryItem);
        }
        
        public System.Threading.Tasks.Task<CoreRepositoryService.RepositoryResponse> modifyRepositoryItemAsync(string s, CoreRepositoryService.RepositoryItem repositoryItem) {
            return base.Channel.modifyRepositoryItemAsync(s, repositoryItem);
        }
    }
}
