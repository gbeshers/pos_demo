﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using CASL_engine;
using pci.PrehDecrypt;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Security;
using System.Threading;

// This file was created for bug #6996, and holds Preh keyboard-specific code
// for importing their unmanaged DLLs for use by iPayment Mike Okola 03/31/10
// Bug 14425/15054 UMN Moved DLL into a Web service to isolate 32-bit code 
//           from iPayment 06/26/2013

namespace pci
{
  public static class Preh
  {
    // Bug 27153 DH - Commented out
    //static String testCardStr = "U^)K)TWW0N'/9JKF[]-'MAZN\"CW%HK\\S=3NH>JAB)*M_9\"$ <4; +WG5;<-DRKULBTG2W Y*'I736Z.+\"W<\\=L7*)FRQ!HTSP[B$?(5\a4A83";
    //static String testCardRes = "%B5424180279791765^VERIFONE TEST 1^14121011000 1111A123456789012?;5424180279791765=14121011000001234567?";

    /// <summary>
    /// Invoke the proxy client call to DecryptMSR
    /// </summary>
    /// <param name="encryptedMSR"></param>
    /// <returns></returns>
    public static string DecryptMSR(String encryptedMSR)
    {
      string result = "";
      PrehDecryptSvcClient client = null;
      try
      {
        string endpointAddr = (c_CASL.c_object("Business.Misc.data")
          as GenericObject).get("prehWS_endpoint_address", "").ToString();
        EndpointAddress endpointAddress = new EndpointAddress(new Uri(endpointAddr));

        client = new PrehDecryptSvcClient();
        client.Endpoint.Address = endpointAddress;
        result = client.DecryptMSR(encryptedMSR);
        client.Close(); // close proxy when done to avoid timeout issues
      }
      catch (Exception e)
      {
        if (client != null) client.Abort();
        Logger.cs_log(string.Format(" PrehDecrypt Exception."));
        // Bug 17849
        Logger.cs_log(e.ToMessageAndCompleteStacktrace());
        //Logger.cs_log(e.Message + Environment.NewLine + e.StackTrace);
        result = "Can't decrypt; please try again";
      }
      return result;
    }

    /// <summary>
    /// Invoke the proxy client call to proxy client call to DecryptKeyboard
    /// </summary>
    /// <param name="encryptedInput"></param>
    /// <returns></returns>
    public static string DecryptKeyboard(String encryptedInput)
    {
      string result = "";
      PrehDecryptSvcClient client = null;
      try
      {
        string endpointAddr = (c_CASL.c_object("Business.Misc.data")
          as GenericObject).get("prehWS_endpoint_address", "").ToString();
        EndpointAddress endpointAddress = new EndpointAddress(new Uri(endpointAddr));

        client = new PrehDecryptSvcClient();
        client.Endpoint.Address = endpointAddress;
        result = client.DecryptKeyboard(encryptedInput);
        client.Close(); // close proxy when done to avoid timeout issues
      }
      catch (Exception e)
      {
        if (client != null) client.Abort();
        Logger.cs_log(string.Format(" PrehDecrypt Exception."));
        // Bug 17849
        Logger.cs_log(e.ToMessageAndCompleteStacktrace());
        //Logger.cs_log(e.Message + Environment.NewLine + e.StackTrace);
        result = "Can't decrypt; please try again";
      }
      return result;
    }
  }
}
