using System;
using System.Reflection;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Xml;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Runtime.InteropServices;
using System.Net;  //.HttpWebRequest;  //System. HttpWebRequest
using System.Threading;
using System.Globalization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Data.SqlClient;
using CASL_engine;
using System.Security.Cryptography;
using System.Diagnostics;
using System.Security; // Bug 16285 UMN
using System.Collections.Generic; // Bug 18129
using pci.eWalletService; // Bug 18129 DJD - Support for Admin Transactions and eWallet
using pci.CoreRepositoryService; // Bug 18129 DJD - Support for CORE Repository
using System.ServiceModel.Dispatcher;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.Xml.Linq;
using System.Linq;
using TranSuiteServices;


namespace pci
{
  public class SecureStringClass
  {
    /////////////////////////////////////////////////////////////////////////////////////////
    // Class created by Daniel Hofman on 03/25/2010 for bug# 8705.
    // The purpose of this class is to hold an encrypted string for 
    // secure credit card number handling and provide basic string manipulations functions.
    // NEVER RETURN THE FULL DECRYPTED STRING DATA TO ANY CASL FUNCTION!!!

    // Usage:
    //    Call the CreateSecureString() function to get back an instance 
    //    PLEASE LOOK AT THE CASL WRAPPER 'SecureStringClass.casl' FOR SAMPLE CASL USAGE.
    /////////////////////////////////////////////////////////////////////////////////////////
    /*

    590 - "Request Failed. A connection error occurred."
    600 - "Error Reading card. Invalid Track data for debit transaction." 
    */

    private System.Security.SecureString m_sSecureString;
    public byte[] m_byteEntropy = new byte[] { 9, 4, 7, 2, 6 };
    public string m_strDataType = "";
    // Bug #11516 Mike O - Regex to validate tracks 1 and 2
    // Bug #12762 Mike O - Apparently card numbers can have spaces in them
    // Bug 22896 MJO - Added hyphen support for names
    private static readonly Regex TrackDataValidation = new Regex("%(B([0-9 ]){1,19}\\^[a-zA-Z0-9/\\- ]{1,26}\\^[a-zA-Z0-9/ ]*){0,76}\\?;([0-9]{1,19}=[0-9]*){0,37}\\?");

    public SecureStringClass() { }

    /// <summary>
    /// Bug 21009 [Bug 18129] DJD - Support for CORE Repository (eWallet)
    /// </summary>
    /// <param name="theDataType"></param>
    /// <param name="theSecureString"></param>
    public SecureStringClass(string theDataType, System.Security.SecureString theSecureString)
    {
      m_strDataType = theDataType;
      m_sSecureString = theSecureString;
      m_sSecureString.MakeReadOnly();
    }

    /// <summary>
    /// Bug 21009 [Bug 18129] DJD - Support for eWallet ADMIN Functions
    /// Get Accessor for m_sSecureString
    /// </summary>
    public System.Security.SecureString theSecureString   // the Name property
    {
      get
      {
        return m_sSecureString;
      }
    }

    public static GenericObject Init(params Object[] arg_pairs)
    {
      // This is the only chance to create and modify the encrypted string.
      // It is made read-only as soon as it is initialized.

      GenericObject args = misc.convert_args(arg_pairs);

      string str = (string)args.get("base16string", c_CASL.c_undefined);
      bool bIsManual = (bool)args.get("is_manual", false); // Bug 21199 MJO
      bool biPayment = (bool)args.get("biPayment", true); // SUPPORT NOT IPAYMENT CREDIT PROCESS     

      // Bug# 20948 DH - Encrypted card data
      bool bRot47 = false;
      string str2 = "";
      if (args.get("triple_des") != c_CASL.c_opt)// optional param to keep compatibility across iPayment
        str2 = (string)args.get("triple_des", c_CASL.c_undefined);

      if (str2.Length > 4)
      {

        // remove the Ingenico indicator
        str2 = str2.Substring(3);

        // Save the pre amble
        string strPrefix = "";
        if (str2[0] == '%' && str2[1] == 'B')
        {
          strPrefix = str2.Substring(0, 2);
          str2 = str2.Remove(0, 2);
        }
        else
        {
          strPrefix = str2.Substring(0, 1);
          str2 = str2.Remove(0, 1);
        }

        // Save the pre amble
        string strPostfix = str2.Substring(str2.Length - 1, 1);

        // Remove the post amble
        str2 = str2.Remove(str2.Length - 1, 1);

        strPrefix = Rot47("str", strPrefix);
        strPostfix = Rot47("str", strPostfix);

        // Decrypt it
        byte[] arrDataBytes = Convert.FromBase64String(str2);
        byte[] key = Convert.FromBase64String("Ye1/fHWFsvZRhxFj9AjONIzeen85v+fO");// Keep it iPayment compatible
        byte[] IV = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 };// Keep it iPayment compatible
        TripleDESCryptoServiceProvider t = new TripleDESCryptoServiceProvider();
        ICryptoTransform xfrm = t.CreateDecryptor(key, IV);
        byte[] outBlock = xfrm.TransformFinalBlock(arrDataBytes, 0, arrDataBytes.Length);

        // Add back the pre and post amble so the rest of the code can work as usual.
        str = strPrefix;
        str += Encoding.Unicode.GetString(outBlock);
        str += strPostfix;

        bRot47 = true;
      }
      // end Bug# 20948 DH - Encrypted card data

      SecureStringClass _this = new SecureStringClass();
      _this.m_sSecureString = new System.Security.SecureString();
      bool bProcessHex = true;
      bool bPreh = false; // BUG 12666 NK adding identifier for Preh
                          // Special handling for the PREH keyboard data. Decrypt it before adding it in.
                          // Bug #12762 Mike O - Added Preh manual key encryption support
      string theStr = str;
      if (str.Length > 5 && str.Substring(1, 5) == "_PREH")// Prefix added by device_swipe_receive JS
      {

        bool isManualKeyEncryption = str.StartsWith("K");
        string strTemp = SecureStringClass.HexToString("str", str.Substring(6));
        strTemp = strTemp.Substring(0, strTemp.Length - 1);
        bPreh = true;

        str = pci.Decrypt_preh_key_tec_data("encrypted_data", strTemp, "encryption_type", str.Substring(0, 6));
        if (!isManualKeyEncryption)
        {
          // Bug #11516 Mike O - Validate the track data, and strip out anything extra (eg. track 3)
          Match match = TrackDataValidation.Match(str);
          // Bug 23822 find extra character following last ? after decrpytion,  ignore validation for OLD Preh
          if (theStr.Substring(0, 6) != "1_PREH")
          {
            if (!match.Success)
            {
              Logger.cs_log("Error reading Card with Invalid Track data.");
              return misc.MakeCASLError("Error reading card track data", "600", "Unable to post tender. Please try again.");
            }
            else
              str = match.Value;
          }
          // End Bug #11516 Mike O
        }

        // Process plain 
        // Bug #12762 - Strip out spaces in the card number (before the first ^
        bool stripSpaces = true;

        for (int i = 0; i < str.Length - (isManualKeyEncryption ? 0 : 1); i++)
        {
          char c = str[i];
          if (!(stripSpaces && c == ' '))
            _this.m_sSecureString.AppendChar(c);
          if (c == '^')
            stripSpaces = false;
          c = '\0';
        }

        if (isManualKeyEncryption)
        {
          // Validate manually entered card number with the Luhn algorithm
          if (!IsValidCreditCardNumber(_this.m_sSecureString))
          {
            return misc.MakeCASLError("Invalid credit card number", "600", "Unable to post tender. Please try again.");
          }
        }

        bProcessHex = false;
        _this.m_strDataType = isManualKeyEncryption ? "PREH_KEY_TEC_MANUAL" : "PREH_KEY_TEC";
      }
      // Bug #14288 Mike O - Added manual VeriFone device
      if (str.StartsWith("+"))
      {
        _this.m_strDataType = "VERIFONE_MANUAL";

        _this.m_sSecureString.AppendChar('+');

        str = str.Substring(1);
      }

      // Process HEX input
      if (bProcessHex || bRot47) // Bug# 20948 DH BUG 12666 NK
      {
        // Bug 8888 NJ
        // Bug# 20948 DH - Bug# 8888 - THIS IS A TERRIBLE APPROACH AND WILL COMAPROMISE DATA. I WILL NOT TOUCH IT BECAUSE I DON'T HAVE THE TIME AND QA HAS NOT SEE ANY PLAIN TEXT CARD DATA, BUT STILL, THIS IS BAD CODE!!!!!
        string trackData = "";

        // Bug 21181 MJO - Non-Ingenico is base-64 encoded
        if (bRot47 || !bPreh)// Bug# 20948 DH BUG 12666 NK
        {
          try
          {
            trackData = Rot47("str", Encoding.UTF8.GetString(Convert.FromBase64String(str)));
          }
          catch (Exception)
          {
            trackData = Rot47("str", str);
          }
        }
        else
          trackData = HexToString("str", str);

        // Bug 19513 MJO - Magtek swipe support
        if (trackData.StartsWith("%B") && trackData.Contains("||"))
        {
          _this.m_strDataType = "MAGTEK";

          string[] splitTrackData = trackData.Split('|');

          str = pci.Decrypt_DUKPT("encryptedstring", splitTrackData[2].Trim(), "KSN", splitTrackData[9].Trim(), "mode", "PROD").Trim().Replace("\0", "");

          for (int i = 0; i < str.Length; i++)
          {
            char c = str[i];
            _this.m_sSecureString.AppendChar(c);
            c = '\0';
          }
          bProcessHex = false;
        }
        // Bug 19513 MJO - Magtek manual entry support
        else if (trackData.StartsWith("%M") && trackData.Contains("||"))
        {
          _this.m_strDataType = "MAGTEK_MANUAL";

          string[] splitTrackData = trackData.Split('|');

          // Bug 19759 MJO - Removed extra "%M"
          str = pci.Decrypt_DUKPT("encryptedstring", splitTrackData[2].Trim(), "KSN", splitTrackData[9].Trim(), "mode", "PROD").Trim().Replace("\0", "");

          // This is a funky one. Just insert the card number, and return expiration and CCV with the response
          for (int i = 0; i < str.IndexOf('^'); i++)
          {
            char c = str[i];
            _this.m_sSecureString.AppendChar(c);
            c = '\0';
          }
          bProcessHex = false;

          _this.m_sSecureString.MakeReadOnly();
          GenericObject aGEO = new GenericObject();
          aGEO.insert(_this);
          aGEO.set("exp_year", str.Substring(str.LastIndexOf('^') + 1, 2));
          aGEO.set("exp_month", str.Substring(str.LastIndexOf('^') + 3, 2));
          aGEO.set("ccv", str.Substring(str.LastIndexOf('^') + 11, 4)); // Bug 19759 MJO - Pull 4 digits for AmEx

          return aGEO;
        }

        double crNum;
        if (trackData.IndexOf('=') >= 0 && !trackData.Contains("||") && trackData.StartsWith(";"))
        {
          string num = trackData.Substring(1, trackData.IndexOf('=') - 1);
          bool isNumeric = double.TryParse(num, out crNum);
          if (num == "" || num.Length < 13 || num.Length > 16 || !isNumeric)
          {
            Logger.cs_log("Error reading Card with Invalid Track data.");
            return misc.MakeCASLError("Error reading card", "600", "Unable to post transaction. Please try again.");
          }
        }
        //End bug 8888
      }

      // Bug 19513 MJO - Skip this if we've already populated it
      if (bProcessHex || bRot47)
      {

        if (bRot47 || (!bPreh && biPayment))// Bug# 20948 DH
        {
          // Bug 21181 MJO - Non-Ingenico is base-64 encoded
          try
          {
            str = Rot47("str", Encoding.UTF8.GetString(Convert.FromBase64String(str)));
          }
          catch (Exception)
          {
            str = Rot47("str", str);
          }
          for (int i = 0; i < str.Length; i++)
          {
            // Bug 21199 MJO - Make sure card number only contains numbers
            if (bIsManual && !char.IsNumber(str[i]))
            {
              Logger.cs_log("Validation error: manually entered card number contained non-numeric character '" + str[i] + "'.");
              return misc.MakeCASLError("Invalid credit card number", "600", "Unable to post transaction. Please try again.");
            }

            _this.m_sSecureString.AppendChar(str[i]);
          }
        }
        else
          // Bug #12420 Mike O - Clarified the double increment here
          for (int i = 0; i < str.Length; i += 2)
          {
            char c = _this.ConvertHexCharToASCII(str.Substring(i, 2));
            _this.m_sSecureString.AppendChar(c);
            c = '\0';
          }
      }

      _this.m_sSecureString.MakeReadOnly();
      GenericObject geo = new GenericObject();
      geo.insert(_this);

      return geo;
    }

    public static string Rot47(params Object[] arg_pairs)  //Adumbrate in js
    {
      // Bug# 20948 DH

      GenericObject args = misc.convert_args(arg_pairs);
      string str = (string)args.get("str", "");

      string sData = "";

      foreach (char c in str.ToCharArray())
      {
        sData += Rot47(c).ToString();
      }

      return sData;
    }

    private static char Rot47(char chr)
    {
      // Bug# 20948 DH

      if (chr == ' ') return ' ';
      int ascii = chr;
      ascii += 47;
      if (ascii > 126) ascii -= 94;
      if (ascii < 33) ascii += 94;
      return (char)ascii;
    }

    public string DecryptText(string encryptedMessage)
    {
      string iv = "abcdefgh";
      string secret_key = "123456789012345678901234";
      byte[] arrDataBytes =
      Encoding.Default.GetBytes(encryptedMessage);
      byte[] tripleDesIV = Encoding.Default.GetBytes(iv);
      byte[] keyArray = Encoding.Default.GetBytes(secret_key);
      TripleDESCryptoServiceProvider pProvider = new
      TripleDESCryptoServiceProvider();
      pProvider.Padding = PaddingMode.Zeros;
      pProvider.Mode = CipherMode.CBC;
      ICryptoTransform pTranform = pProvider.CreateDecryptor(keyArray, tripleDesIV);
      MemoryStream decryptedStream = new MemoryStream();
      CryptoStream cryptStream = new CryptoStream(decryptedStream, pTranform,
      CryptoStreamMode.Write);
      cryptStream.Write(arrDataBytes, 0, arrDataBytes.Length);
      cryptStream.FlushFinalBlock();
      decryptedStream.Position = 0;
      Byte[] result = new Byte[decryptedStream.Length];
      decryptedStream.Read(result, 0, decryptedStream.ToArray().Length);
      decryptedStream.Close();
      return Encoding.Default.GetString(result);
    }

    public static Object SetDataSourceType(params Object[] arg_pairs)
    {
      // Sets the card source type:
      // Ingenico, Mag Reader etc.

      GenericObject args = misc.convert_args(arg_pairs);
      SecureStringClass _this = (SecureStringClass)args.get("this", c_CASL.c_undefined);
      string strDataType = (string)args.get("data_type", c_CASL.c_undefined);

      _this.m_strDataType = strDataType;
      return null;
    }

    public static string GetDataSourceType(params Object[] arg_pairs)
    {
      // Gets the card source type:
      // Ingenico, Mag Reader etc.

      GenericObject args = misc.convert_args(arg_pairs);
      SecureStringClass _this = (SecureStringClass)args.get("this", c_CASL.c_undefined);

      return _this.m_strDataType;
    }

    public static unsafe string GetStringData(params Object[] arg_pairs)
    {
      // Return the DECRYPTED string
      // SHOULD NEVER USE THIS!
      // TESTING ONLY!!!

      GenericObject args = misc.convert_args(arg_pairs);
      SecureStringClass _this = (SecureStringClass)args.get("this", c_CASL.c_undefined);

      string strData = "";
      IntPtr unmanagedRef = IntPtr.Zero;
      try
      {
        unmanagedRef = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(_this.m_sSecureString);
        strData = new string((char*)unmanagedRef);
      }
      finally
      {
        if (unmanagedRef != IntPtr.Zero)
        {
          // System.Runtime.InteropServices.Marshal.ZeroFreeCoTaskMemUnicode(unmanagedRef); BUG# 9972 DH - Will not work on Windows 2008 Server.
          System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(unmanagedRef);
        }
      }

      return strData;
    }

    public static Object Length_impl(params Object[] arg_pairs)
    { return Length(arg_pairs); }

    public static int Length(params Object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      SecureStringClass _this = (SecureStringClass)args.get("this", c_CASL.c_undefined);

      return _this.m_sSecureString.Length;
    }

    public static unsafe Object IndexOf_impl(params Object[] arg_pairs)
    { return IndexOf(arg_pairs); }

    public static unsafe int IndexOf(params Object[] arg_pairs)
    {
      // Find a single character in the encryopted string. !CASE SENSITIVE!
      //  Return value:
      //    -1 If not found
      //    Zero based integer position of the found character

      GenericObject args = misc.convert_args(arg_pairs);
      SecureStringClass _this = (SecureStringClass)args.get("this", 0);
      string s = (string)args.get("char", c_CASL.c_undefined);

      if (s.Length == 0)
        return -1;

      char c = (char)s[0];
      int iCharIndex = -1;// Return -1 if not found

      IntPtr unmanagedRef = IntPtr.Zero;
      try
      {
        unmanagedRef = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(_this.m_sSecureString);
        char* p = (char*)unmanagedRef;

        for (int i = 0; i < _this.m_sSecureString.Length; i++)
        {
          if (*p == c)
          {
            iCharIndex = i;
            break;
          }
          p++;
        }
      }
      catch (Exception /*e*/) //12081 NJ-unused variable)
      {
        iCharIndex = -1;
      }
      finally
      {
        if (unmanagedRef != IntPtr.Zero)
        {
          // System.Runtime.InteropServices.Marshal.ZeroFreeCoTaskMemUnicode(unmanagedRef); BUG# 9972 DH - Will not work on Windows 2008 Server.
          System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(unmanagedRef);
        }
      }

      return iCharIndex;
    }
    public static Object StartsWith_impl(params Object[] arg_pairs)
    { return StartsWith(arg_pairs); }

    public static bool StartsWith(params Object[] arg_pairs)
    {
      // Test if encrypted data starts with "strCompChars" !EXACT MATCH ONLY! !CASE SENSITIVE!
      // Allow only 4 characters so no one will pass the whole encrypted string for comparison.
      //  Return value:
      //    TRUE or FALSE

      GenericObject args = misc.convert_args(arg_pairs);
      SecureStringClass _this = (SecureStringClass)args.get("this", 0);
      string s = (string)args.get("starts_with", c_CASL.c_undefined);

      return _this.Starts_OR_Ends_With(ref _this, s, false);
    }
    public static Object EndsWith_impl(params Object[] arg_pairs)
    {
      return EndsWith(arg_pairs);
    }
    public static bool EndsWith(params Object[] arg_pairs)
    {
      // Test if encrypted data ends with "strCompChars" !EXACT MATCH ONLY! !CASE SENSITIVE!
      // Allow only 4 characters so no one will pass the whole encrypted string for comparison.
      //  Return value:
      //    TRUE or FALSE

      GenericObject args = misc.convert_args(arg_pairs);
      SecureStringClass _this = (SecureStringClass)args.get("this", 0);
      string s = (string)args.get("ends_with", c_CASL.c_undefined);

      return _this.Starts_OR_Ends_With(ref _this, s, true);
    }

    public static unsafe string SubStringCASL(params Object[] arg_pairs)
    {
      // Bug 22307 MJO - 6 characters are needed to get the full BIN for FSA
      // Find substring. Will return ONLY 6 characters for security reasons.
      // CASL has a different implementation for Substring than C# version because it deals with vector tyopes as well.

      // Params:
      // start:
      //    Starting position
      // end:
      //    end position within the string
      //
      // Return value:
      //    Empty string if not found
      //    Substring if found

      GenericObject args = misc.convert_args(arg_pairs);
      SecureStringClass _this = (SecureStringClass)args.get("this", 0);
      int iStartPos = (int)args.get("start", c_CASL.c_undefined);
      int iEndPos = (int)args.get("end", c_CASL.c_undefined);
      string strReturnValue = "";

      // Simple validation here
      if (iStartPos < 0 || iEndPos <= 0 || iStartPos == iEndPos)
        return strReturnValue;

      if (iStartPos < 0)
        iStartPos = 0;

      if (iStartPos > iEndPos)
        iStartPos = iEndPos;

      IntPtr unmanagedRef = IntPtr.Zero;
      try
      {
        unmanagedRef = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(_this.m_sSecureString);
        char* pStart = (char*)unmanagedRef;

        int i = 0;
        for (i = 0; i < iStartPos; i++)
          pStart++;

        int j = 1;
        for (j = 1; j < iEndPos; j++) ;

        int x = j - i;
        for (int n = 0; n < x; n++)
          strReturnValue += *pStart++;
      }
      catch (Exception /*e*/) //12081 NJ-unused variable
      {
        strReturnValue = "";
      }
      finally
      {
        if (unmanagedRef != IntPtr.Zero)
        {
          // System.Runtime.InteropServices.Marshal.ZeroFreeCoTaskMemUnicode(unmanagedRef); BUG# 9972 DH - Will not work on Windows 2008 Server.
          System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(unmanagedRef);
        }
      }

      // Bug 22307 MJO - 6 characters are needed to get the full BIN for FSA
      if (strReturnValue.Length > 6)
        throw new InvalidOperationException("For security reasons the SecureString cannot return more than six characters");

      return strReturnValue;
    }

    public static unsafe string SubString(params Object[] arg_pairs)
    {
      // Find substring. Will return ONLY 4 characters for security reasons.

      // Params:
      // start_pos:
      //    Starting position
      // char_count:
      //    Number of characters from start_pos to read.
      //
      // Return value:
      //    Empty string if not found
      //    Substring if found

      GenericObject args = misc.convert_args(arg_pairs);
      SecureStringClass _this = (SecureStringClass)args.get("this", 0);
      int iStartPos = (int)args.get("start_pos", c_CASL.c_undefined);
      int iEndPos = (int)args.get("char_count", c_CASL.c_undefined);
      string strReturnValue = "";

      // Simple validation here
      if (iStartPos < 0 || iEndPos <= 0)
        return strReturnValue;

      if (iStartPos + iEndPos > _this.m_sSecureString.Length || iEndPos > _this.m_sSecureString.Length)
        throw new InvalidOperationException("Index and length must refer to a location within the string.");

      IntPtr unmanagedRef = IntPtr.Zero;
      try
      {
        unmanagedRef = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(_this.m_sSecureString);
        char* pStart = (char*)unmanagedRef;
        char* pEnd = (char*)unmanagedRef;

        for (int i = 0; i < iStartPos; i++)
          pStart++;

        for (int j = 0; j < iEndPos; j++)
          strReturnValue += *pStart++;
      }
      catch (Exception /*e*/) //12081 NJ-unused variable
      {
        strReturnValue = "";
      }
      finally
      {
        if (unmanagedRef != IntPtr.Zero)
        {
          // System.Runtime.InteropServices.Marshal.ZeroFreeCoTaskMemUnicode(unmanagedRef); BUG# 9972 DH - Will not work on Windows 2008 Server.
          System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(unmanagedRef);
        }
      }

      if (strReturnValue.Length > 4)
        throw new InvalidOperationException("For security reasons the SecureString cannot return more than four characters");

      return strReturnValue;
    }

    public static unsafe object KeyOf(params Object[] arg_pairs)
    {
      // Only single character may be found. This should be plenty for the secure card data.
      // Note: if bStartFromEnd is true then iStartAt should count characters starting at end progressing to the front.
      // Problems is that the CASL implementation is incorrectly going forward making begining character no foundable and this class has
      // to have the same bug to be backward compatable.

      GenericObject args = misc.convert_args(arg_pairs);
      SecureStringClass _this = (SecureStringClass)args.get("this", 0);

      object oRV = false;
      bool bStartFromEnd = (bool)args.get("from_end", false);
      int iStartPos = (int)args.get("start_at", 0);
      string stringToFind = (string)args.get("a_string", 0);

      if (stringToFind.Length == 0)
        throw new InvalidOperationException("missing parameter for key_of");

      char c = stringToFind[0];//geo_args.get("_subject") as string;

      if (iStartPos > _this.m_sSecureString.Length)
        throw new InvalidOperationException("Invalid start post for key_of function.");

      IntPtr unmanagedRef = IntPtr.Zero;
      try
      {
        unmanagedRef = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(_this.m_sSecureString);
        char* p = (char*)unmanagedRef;

        if (!bStartFromEnd)
        {
          int i = 0;
          for (i = 0; i < iStartPos; i++)
            p++;

          for (int j = i; j < _this.m_sSecureString.Length; j++)
          {
            if (*p++ == c)
            {
              oRV = j;
              break;
            }
          }
          string s = i.ToString();
        }

        if (bStartFromEnd)
        {
          int i = 0;
          for (i = 0; i < _this.m_sSecureString.Length; i++)
            p++;

          for (int j = _this.m_sSecureString.Length; j > 0; j--)
          {
            if (j >= iStartPos)
            {
              if (*p-- == c)
              {
                oRV = j;
                break;
              }
            }
            else
              p--;
          }
          string s = i.ToString();
        }
      }
      catch (Exception) //12081 NJ- removed unused variable
      {
        oRV = false;
      }
      finally
      {
        if (unmanagedRef != IntPtr.Zero)
        {
          // System.Runtime.InteropServices.Marshal.ZeroFreeCoTaskMemUnicode(unmanagedRef); BUG# 9972 DH - Will not work on Windows 2008 Server.
          System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(unmanagedRef);
        }
      }

      return oRV;
    }

    private unsafe bool Starts_OR_Ends_With(ref SecureStringClass _this, string strCompChars, bool bStartFromEnd)
    {
      // Test if encrypted data starts/ends with "strCompChars" !EXACT MATCH ONLY! !CASE SENSITIVE!
      // Allow only 8 characters so no one will pass the whole encrypted string for comparison.
      //  Return value:
      //    TRUE or FALSE

      // Note: bStartFromEnd determins if we are looking for start with or ends with.

      bool bRV = true;
      if (strCompChars.Length > 8)
      {
        // Throw an exception if we are trying to compare more than 4 characters.
        throw new InvalidOperationException("For security reasons the SecureString cannot compare more than 8 characters at a time");
      }

      if (strCompChars.Length > _this.m_sSecureString.Length || strCompChars.Length == 0)
        bRV = false;

      if (bRV)
      {
        IntPtr unmanagedRef = IntPtr.Zero;
        try
        {
          unmanagedRef = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(_this.m_sSecureString);
          char* p = (char*)unmanagedRef;

          if (bStartFromEnd)
          {
            for (int x = 0; x < _this.m_sSecureString.Length - strCompChars.Length; x++)
              p++;
          }

          for (int i = 0; i < strCompChars.Length; i++)
          {
            if (*p != strCompChars[i])
            {
              bRV = false;
              break;
            }
            p++;
          }
        }
        catch (Exception /*e*/) //12081 NJ-unused variable
        {
          bRV = false;
        }
        finally
        {
          if (unmanagedRef != IntPtr.Zero)
          {
            // System.Runtime.InteropServices.Marshal.ZeroFreeCoTaskMemUnicode(unmanagedRef); BUG# 9972 DH - Will not work on Windows 2008 Server.
            System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(unmanagedRef);
          }
        }
      }

      return bRV;
    }

    public static unsafe string GetFirstName(params Object[] arg_pairs)
    {
      // Try to return track first name from track data

      GenericObject args = misc.convert_args(arg_pairs);
      SecureStringClass _this = (SecureStringClass)args.get("this", 0);
      string strName = "";

      IntPtr unmanagedRef = IntPtr.Zero;
      try
      {
        unmanagedRef = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(_this.m_sSecureString);
        char* p = (char*)unmanagedRef;
        char* l;//Bug 10410 NJ

        // Find the start of first name
        int i = 0;
        for (i = 0; i < _this.m_sSecureString.Length; i++)
        {
          if (*p == '^')
          {
            p++;// Get it in position
            break;
          }
          p++;
        }

        //Bug 10410 NJ
        //Find the end of last name
        int x = i;
        l = p;
        for (; x < _this.m_sSecureString.Length; x++)
        {
          if (*p == '^')
          {
            p++;
            break;
          }
          p++;
        }

        // Find the end of first name
        int j = i;
        //for (j = i; j < _this.m_sSecureString.Length; j++)
        for (j = i; j < x; j++)
        {
          //if (*p == '/')
          if (*l == '/')
          {
            l++;//p++
            break;
          }
          l++;//p++
        }

        // Read until end of last name
        //for (int x = j; x < _this.m_sSecureString.Length; j++)
        for (; j < x; j++)
        {
          //if (*p != '^')
          if (*l != '^')
          {
            strName += *l;//*p
          }
          else
            break;
          l++;//p++
        }

        //End bug 10410 NJ

      }
      catch (Exception) //12081 NJ- removed unused variable)
      {
        strName = "";
      }
      finally
      {
        if (unmanagedRef != IntPtr.Zero)
        {
          // System.Runtime.InteropServices.Marshal.ZeroFreeCoTaskMemUnicode(unmanagedRef); BUG# 9972 DH - Will not work on Windows 2008 Server.
          System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(unmanagedRef);
        }
      }

      // Bug 15455 MJO - Remove quotes from the name
      return strName.Replace("'", "");
    }

    public static unsafe string GetLastName(params Object[] arg_pairs)
    {
      // Try to return track first name from track data

      GenericObject args = misc.convert_args(arg_pairs);
      SecureStringClass _this = (SecureStringClass)args.get("this", 0);
      string strName = "";

      IntPtr unmanagedRef = IntPtr.Zero;
      try
      {
        unmanagedRef = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(_this.m_sSecureString);
        char* p = (char*)unmanagedRef;

        // Find the start of first name
        int i = 0;
        for (i = 0; i < _this.m_sSecureString.Length; i++)
        {
          if (*p == '^')
          {
            p++;// Get it in position
            break;
          }
          p++;
        }

        for (int j = i; j < _this.m_sSecureString.Length; j++)
        {
          if (*p != '/')
          {
            if (*p == '^')
            {
              j = _this.m_sSecureString.Length;
              break;
            }
            strName += *p;
          }
          else
            break;
          p++;
        }
      }
      catch (Exception) //12081 NJ-unused variable
      {
        strName = "";
      }
      finally
      {
        if (unmanagedRef != IntPtr.Zero)
        {
          // System.Runtime.InteropServices.Marshal.ZeroFreeCoTaskMemUnicode(unmanagedRef); BUG# 9972 DH - Will not work on Windows 2008 Server.
          System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(unmanagedRef);
        }
      }

      // Bug 15455 MJO - Remove quotes from the name
      return strName.Replace("'", "");
    }

    // Bug 14661 UMN - fix hard-coding of space, and order of last and first names
    public static string GetFullName(params Object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      SecureStringClass _this = (SecureStringClass)args.get("this", 0);
      string strFirstName = SecureStringClass.GetFirstName("this", _this);
      string strLastName = SecureStringClass.GetLastName("this", _this);
      return pci.MakeFullName(strFirstName, strLastName);
    }

    public static unsafe string GetExpYear(params Object[] arg_pairs)
    {
      // Get the track data's expiration year

      GenericObject args = misc.convert_args(arg_pairs);
      SecureStringClass _this = (SecureStringClass)args.get("this", 0);
      string strExpYear = "";

      IntPtr unmanagedRef = IntPtr.Zero;
      try
      {
        unmanagedRef = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(_this.m_sSecureString);
        char* p = (char*)unmanagedRef;

        int iPosAfterName = (int)SecureStringClass.KeyOf("this", _this, "a_string", "^", "start_at", 0, "from_end", true);

        // Find the start of first name
        for (int i = 0; i < iPosAfterName; i++)
          p++;

        for (int j = iPosAfterName; j < iPosAfterName + 2; j++)
          strExpYear += *++p;
      }
      catch (Exception)//12081 NJ- removed unused variable
      {
        strExpYear = "";
      }
      finally
      {
        if (unmanagedRef != IntPtr.Zero)
        {
          // System.Runtime.InteropServices.Marshal.ZeroFreeCoTaskMemUnicode(unmanagedRef); BUG# 9972 DH - Will not work on Windows 2008 Server.
          System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(unmanagedRef);
        }
      }

      return strExpYear;
    }

    public static unsafe string GetExpMonth(params Object[] arg_pairs)
    {
      // Get the track data's expiration month

      GenericObject args = misc.convert_args(arg_pairs);
      SecureStringClass _this = (SecureStringClass)args.get("this", 0);
      string strExpMonth = "";

      IntPtr unmanagedRef = IntPtr.Zero;
      try
      {
        unmanagedRef = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(_this.m_sSecureString);
        char* p = (char*)unmanagedRef;

        int iPosAfterName = (int)SecureStringClass.KeyOf("this", _this, "a_string", "^", "start_at", 0, "from_end", true);

        // Find the start of first name
        for (int i = 0; i < iPosAfterName + 2; i++)
          p++;

        for (int j = iPosAfterName; j < iPosAfterName + 2; j++)
          strExpMonth += *++p;
      }
      catch (Exception)//12081 NJ- removed unused variable
      {
        strExpMonth = "";
      }
      finally
      {
        if (unmanagedRef != IntPtr.Zero)
        {
          // System.Runtime.InteropServices.Marshal.ZeroFreeCoTaskMemUnicode(unmanagedRef); BUG# 9972 DH - Will not work on Windows 2008 Server.
          System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(unmanagedRef);
        }
      }

      return strExpMonth;
    }

    public static unsafe string GetLastFourChars(params Object[] arg_pairs)
    {
      // Get the track data's expiration month

      GenericObject args = misc.convert_args(arg_pairs);
      SecureStringClass _this = (SecureStringClass)args.get("this", 0);
      string strLastFour = "";

      IntPtr unmanagedRef = IntPtr.Zero;
      try
      {
        unmanagedRef = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(_this.m_sSecureString);
        char* p = (char*)unmanagedRef;

        int i = 0;
        for (i = 0; i < _this.m_sSecureString.Length; i++)
        {
          if (*p == '^')
          {
            for (int x = 0; x < 4; x++)
              p--;

            break;
          }
          p++;
        }

        for (int n = 0; n < 4; n++)
          strLastFour += *p++;
      }
      catch (Exception)//12081 NJ- removed unused variable
      {
        strLastFour = "";
      }
      finally
      {
        if (unmanagedRef != IntPtr.Zero)
        {
          // System.Runtime.InteropServices.Marshal.ZeroFreeCoTaskMemUnicode(unmanagedRef); BUG# 9972 DH - Will not work on Windows 2008 Server.
          System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(unmanagedRef);
        }
      }

      return strLastFour;
    }

    public static unsafe GenericObject ReturnStringAsEncryptedByteArray(params Object[] arg_pairs)
    {
      // Transfer what is in the secure string into a secure byte[] array and return it.
      GenericObject args = misc.convert_args(arg_pairs);
      SecureStringClass _this = (SecureStringClass)args.get("this", 0);

      Byte[] byteProtectedData = null;
      byte[] byteData = new byte[_this.m_sSecureString.Length];
      IntPtr unmanagedRef = IntPtr.Zero;
      try
      {
        unmanagedRef = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(_this.m_sSecureString);
        char* p = (char*)unmanagedRef;

        for (int i = 0; i < _this.m_sSecureString.Length; i++)
          byteData.SetValue((byte)*p++, i);

        byteProtectedData = ProtectedData.Protect(byteData, _this.m_byteEntropy, DataProtectionScope.CurrentUser);
      }
      catch (Exception e)
      {
        throw new InvalidOperationException("Data was not decrypted. An error occurred. Error: [" + e.ToString() + "]");
      }
      finally
      {
        if (unmanagedRef != IntPtr.Zero)
        {
          // System.Runtime.InteropServices.Marshal.ZeroFreeCoTaskMemUnicode(unmanagedRef); BUG# 9972 DH - Will not work on Windows 2008 Server.
          System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(unmanagedRef);
        }
      }

      // ZERO element is the protected byte array
      GenericObject resulGEO = new GenericObject();
      resulGEO.insert(byteProtectedData);
      return resulGEO;
    }

    public static unsafe string GetMaskedCreditCardNumber(params Object[] arg_pairs)
    {
      // Return the masked card number only.

      GenericObject args = misc.convert_args(arg_pairs);
      SecureStringClass _this = (SecureStringClass)args.get("this", 0);

      // Bug 20186 DH - Masked value cannot be taken from the end of a full track data.
      bool bHasSeparator = false;
      IntPtr ip = IntPtr.Zero;
      SecureString pNewtestString = _this.m_sSecureString.Copy();
      ip = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(pNewtestString);
      char* c = (char*)ip;
      int iSeparatorPos = 0;
      for (iSeparatorPos = 0; iSeparatorPos < pNewtestString.Length; iSeparatorPos++)
      {
        if (*c == '=')
        {
          bHasSeparator = true;
          break;
        }
        c++;
      }
      if (!bHasSeparator)
        iSeparatorPos = _this.m_sSecureString.Length;
      // end Bug 20186 DH - Masked value cannot be taken from the end of a full track data.

      string strCC = "";
      IntPtr unmanagedRef = IntPtr.Zero;
      try
      {
        unmanagedRef = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(_this.m_sSecureString);
        char* p = (char*)unmanagedRef;

        //-------------------------------------------------------------------------------------------------
        // BUG# 9924 DH - There are instances where the CC data is already masked. (CC Tracker second pass)
        // Just return the original string in that case. Do a little test for masked chars as well.
        char* pTest = (char*)unmanagedRef;
        int iMaskCount = 0;
        for (int n = 0; n < iSeparatorPos && n < 4; n++)// Bug 20186 DH - Masked value cannot be taken from the end of a full track data.
        {
          if (*pTest == '*')
            iMaskCount++;

          if (iMaskCount == 4)// Assume this is a masked value.
          {
            // Masked data. Just return it to the caller.
            GenericObject g = new GenericObject("this", _this);
            string strMaskedVal = GetStringData(g);
            return strMaskedVal;
          }
          pTest++;
        }
        //------------------------------------------------------------------------------------------------

        // Bug #13414 Mike O - Rewrote this whole thing so it won't need tweaks for future prefixes, etc.
        // Build a fully masked card number, skipping any prefixes
        for (int i = 0; i < iSeparatorPos; i++)// Bug 20186 DH - Masked value cannot be taken from the end of a full track data.
        {
          if (Char.IsDigit(*p))
            strCC += "*";

          // Bug 20186 MJO - Catch end of XPI manual entry card number
          if (*p == '^' || (_this.m_strDataType == "VERIFONE_MANUAL" && *p == '|'))
            break;

          ++p;
        }

        // Go back to the first of the last four digits (going back 5 since we?re one step past the end)
        for (int i = 0; i < 4; i++)
          --p;

        // Remove the last four masked digits
        strCC = strCC.Substring(0, strCC.Length - 4);

        // Append the last four unmasked digits
        for (int i = 0; i < 4; i++)
        {
          strCC += *p;
          ++p;
        }
        // End Bug #13414 Mike O
      }
      catch (Exception /*e*/) //12081 NJ-unused variable
      {
        strCC = "";
      }
      finally
      {
        if (unmanagedRef != IntPtr.Zero)
        {
          // System.Runtime.InteropServices.Marshal.ZeroFreeCoTaskMemUnicode(unmanagedRef); BUG# 9972 DH - Will not work on Windows 2008 Server.
          System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(unmanagedRef);
        }
      }

      return strCC;
    }

    /// <summary>
    /// Bug 22013 DJD - Brand-specific card tenders allow processing of other brands' cards
    /// Bug 21009 [Bug 18129] (Bug 11079) Mike O - Return the first n digits (default to 6 digits if not passed)
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public unsafe static string GetBINValue(params Object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      SecureStringClass _this = (SecureStringClass)args.get("this", 0);

      // Bug 12417: BIN Validation Not Working With PIN-Based Debit Cards
      object theObj = args.get("digits", 6);
      int _digits = theObj is int ? (int)theObj : 6;

      string strReturnValue = "";

      IntPtr unmanagedRef = IntPtr.Zero;
      try
      {
        unmanagedRef = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(_this.m_sSecureString);
        char* p = (char*)unmanagedRef;

        // Track I: Find position of card number in track data.
        // Example: %B4012002000060016^VI TEST CREDIT^251210118039000000000396?
        int i = 0;
        if (SecureStringClass.KeyOf("this", _this, "a_string", "%").GetType() == typeof(int))
        {
          for (; i < _this.m_sSecureString.Length; i++)
          {
            if (*p == '%')
            {
              p += 2;
              break;
            }
            p++;
          }
        }

        // Bug 12417: BIN Validation Not Working With PIN-Based Debit Cards
        // Track II: (Debit Card sends Track II only) Find position of card number in track data.
        // Example: ;4012002000060016=25121011803939600000?
        // Preh manual entry starts with a pipe ("|") [Example: |4012002000060016]
        // Bug 23150 DJD Move to next character if this is NOT a digit (various devices use different chars) 
        if (!pci.IsDigit(*p))
        {
          p++;
        }

        for (int n = 0; n < _digits; n++)
          strReturnValue += *p++;
      }
      catch (Exception ex)
      {
        // Bug 12417: BIN Validation Not Working With PIN-Based Debit Cards
        StringBuilder sb = new StringBuilder();
        sb.Append("\r\nError while retrieving BIN value from credit or debit card data:");
        sb.Append(string.Format("\r\nMessage ---\r\n{0}", ex.Message));
        if (ex.InnerException != null)
        {
          sb.Append(string.Format("\r\nInner Exception ---\r\n{0}", ex.InnerException.Message));
        }
        sb.Append(string.Format("\r\nSource ---\r\n{0}", ex.Source));
        sb.Append(string.Format("\r\nStack Trace ---\r\n{0}", ex.StackTrace));
        sb.Append(string.Format("\r\nTarget Site ---\r\n{0}", ex.TargetSite));
        Logger.cs_log(sb.ToString());
        strReturnValue = "";
      }
      finally
      {
        if (unmanagedRef != IntPtr.Zero)
        {
          // System.Runtime.InteropServices.Marshal.ZeroFreeCoTaskMemUnicode(unmanagedRef); BUG# 9972 DH - Will not work on Windows 2008 Server.
          System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(unmanagedRef);
        }
      }

      return strReturnValue;
    }

    /// <summary>
    /// Bug 18129 DJD - Support for CORE Repository and eWallet
    /// Gets the midsection of a PAN; this is the part of the PAN that is masked (i.e., the digits in the middle behind the
    /// asterisks): 123456******1234.  The first 6 (the BIN) and the last 4 digits are omitted from the value returned.
    /// </summary>
    /// <param name="sCc_track_info"></param>
    /// <returns></returns>
    public unsafe static SecureString GetPANMidsection(SecureStringClass sscTrackInfo)
    {
      int digitsToOmitAtStart = 6;
      int digitsToOmitAtEnd = 4;
      SecureString strReturnValue = new SecureString();
      IntPtr unmanagedRef = IntPtr.Zero;
      try
      {
        unmanagedRef = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(sscTrackInfo.m_sSecureString);
        char* p = (char*)unmanagedRef;
        bool bTrackI = SecureStringClass.KeyOf("this", sscTrackInfo, "a_string", "%").GetType() == typeof(int);
        bool bManualEntry = false;

        // Find start position of PAN (card number) in track data.
        int i = 0;
        if (bTrackI) // Track I
        {
          // Track I Example: %B4012002000060016^VI TEST CREDIT^251210118039000000000396?
          for (; i < sscTrackInfo.m_sSecureString.Length; i++)
          {
            if (*p == '%')
            {
              p += 2;
              break;
            }
            p++;
          }
        }
        else // Track II (Debit Card sends Track II only) or Manual Entry
        {
          // Track II Example: ;4012002000060016=25121011803939600000?
          if (*p == ';')
          {
            p++;
          }
          else
          {
            // Preh manual entry starts with a pipe ("|")
            if (*p == '|')
            {
              p++;
            }
            bManualEntry = true;
          }
        }

        // The maximum length of any credit card number is 19 digits.
        int cardLength = bManualEntry ? (sscTrackInfo.m_sSecureString.Length - digitsToOmitAtEnd) : 19;
        for (int n = 1; n <= cardLength; n++)
        {
          if (!bManualEntry && (*p == '=' || *p == '^'))
          {
            break;
          }
          if (n > digitsToOmitAtStart)
          {
            strReturnValue.AppendChar(*p);
          }
          p++;
        }
        if (!bManualEntry && (strReturnValue.Length > digitsToOmitAtEnd))
        {
          // Remove the extra digits at the end if not manual entry
          for (int r = 0; r > digitsToOmitAtEnd; r++)
          {
            strReturnValue.RemoveAt(strReturnValue.Length - 1);
          }
        }
      }
      catch (Exception ex)
      {
        Logger.cs_log(string.Format("{0}Error while retrieving PAN Midsection value from credit or debit card data:{0}{1}"
        , Environment.NewLine, ex.ToMessageAndCompleteStacktrace()));
        strReturnValue.Clear();
      }
      finally
      {
        if (unmanagedRef != IntPtr.Zero)
        {
          System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(unmanagedRef);
        }
      }
      return strReturnValue;
    }

    // Bug 23150 DJD Added to allow swiping devices and adding items to eWallet
    public unsafe SecureString GetSecureString()
    {
      // TODO: Need to address ALL the device specific extra characters in the secure string
      SecureString ss = null;
      switch (m_strDataType)
      {
        case "INGENICO":
          ss = theSecureString.Copy();
          ss.RemoveAt(0); // Remove the first character from start of secure string
          break;

        case "VERIFONE_MANUAL":
          ss = theSecureString.Copy();
          ss.RemoveAt(0); // Remove the first character ("+") from start of secure string
          for (int n = 0; n < 5; n++)
          {
            int lastIndex = (ss.Length - 1);
            ss.RemoveAt(lastIndex); // Remove last 5 characters from secure string [|YYMM : where YY is encrypted year MM is month]
          }
          break;

        case "PREH_KEY_TEC":
          ss = GetSecureStringForPrehSwipe();
          break;

        case "PREH_KEY_TEC_MANUAL":
          ss = theSecureString.Copy();
          ss.RemoveAt(0); // Remove pipe character ("|") from start of secure string
          break;

        case "MAGTEK_MANUAL":
          ss = theSecureString.Copy();
          ss.RemoveAt(0); // Remove the two characters from start of secure string
          ss.RemoveAt(0);
          break;

        case "MAG_READER":
        case "MAGTEK":
        case "VERIFONE":
        case "MANUAL_ENTRY":
        case "EWALLET_TOKEN":
        default:
          ss = theSecureString.Copy();
          break;
      }

      return ss;
    }

#if DEBUG
    /// <summary>
    /// This method will be used and available for DEBUG only.
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    String SecureStringToString(SecureString value)
    {
      IntPtr valuePtr = IntPtr.Zero;
      try
      {
        valuePtr = Marshal.SecureStringToGlobalAllocUnicode(value);
        return Marshal.PtrToStringUni(valuePtr);
      }
      finally
      {
        Marshal.ZeroFreeGlobalAllocUnicode(valuePtr);
      }
    }
#endif

    private unsafe SecureString GetSecureStringForPrehSwipe()
    {
      SecureString ss;
      IntPtr unmanagedRef = IntPtr.Zero;
      try
      {
        ss = new SecureString();
        unmanagedRef = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(m_sSecureString);
        char* p = (char*)unmanagedRef;
        for (int x = 0; x < m_sSecureString.Length; x++)
        {
          if (*p == ';')
          {
            p++;
            for (int y = x; y < m_sSecureString.Length - 1; y++)
            {
              ss.AppendChar(*p);
              p++;
            }
            ss.AppendChar('?');
            break;
          }
          p++;
        }
      }
      catch (Exception e)
      {
        throw new InvalidOperationException(e.ToString());
      }
      finally
      {
        if (unmanagedRef != IntPtr.Zero)
        {
          System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(unmanagedRef);
        }
      }
      return ss;
    }

    public static unsafe GenericObject ReplacePlaceholderTAGWith_TrackData(params Object[] arg_pairs)
    {
      // Replace the placeholder without using string objects. This will prevent copying strings in memory.
      // Return value is encrypted byte array.
      GenericObject args = misc.convert_args(arg_pairs);
      SecureStringClass _this = (SecureStringClass)args.get("this", 0);
      string strXMLString = (string)args.get("xml", 0);
      Byte[] byteProtectedData = null;
      int iFullLength = 0;
      int iStartOfPlaceholder = strXMLString.IndexOf("pci.SecureStringClass");
      int iEndOfPlaceholder = iStartOfPlaceholder + 21;
      IntPtr unmanagedRef = IntPtr.Zero;

      try
      {
        unmanagedRef = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(_this.m_sSecureString);
        char* p = (char*)unmanagedRef;

        string sPart1 = strXMLString.Substring(0, iStartOfPlaceholder);
        string sPart2 = strXMLString.Substring(iEndOfPlaceholder);

        // Bug 17414 MJO - If Preh manual entry, decremented iFullLength and start copying one character later
        bool isPrehManual = *p == '|';
        // Bug 19513 MJO - Remove prefix for Magtek manual entry
        bool isMagtekManual = *p == '%' && !_this.Starts_OR_Ends_With(ref _this, "?", true);

        iFullLength = sPart1.Length + _this.m_sSecureString.Length + sPart2.Length;

        if (isPrehManual)
          --iFullLength;

        // Bug 19513 MJO - Remove prefix for Magtek manual entry
        if (isMagtekManual)
          iFullLength -= 2;

        byte[] byteData = new byte[iFullLength];

        int j = 0;
        for (j = 0; j < sPart1.Length; j++)
          byteData[j] = (byte)sPart1[j];

        // Bug #12762 Mike O - Added Preh manual key encryption support
        int startIndex = 0;
        if (isPrehManual)
        {
          p++;
          ++startIndex;
        }
        // End Bug 17414 MJO

        // Bug 19513 MJO - Remove prefix for Magtek manual entry
        if (isMagtekManual)
        {
          p++;
          p++;
          startIndex += 2;
        }

        for (int n = startIndex; n < _this.m_sSecureString.Length; n++)
          byteData[j++] = (byte)*p++;

        for (int n = 0; n < sPart2.Length; n++)
          byteData[j++] = (byte)sPart2[n];

        //Testing data: 
        //string s = ASCIIEncoding.ASCII.GetString(byteData);

        byteProtectedData = ProtectedData.Protect(byteData, _this.m_byteEntropy, DataProtectionScope.CurrentUser);
      }
      catch (Exception e)
      {
        throw new InvalidOperationException(e.ToString());
      }
      finally
      {
        if (unmanagedRef != IntPtr.Zero)
        {
          // System.Runtime.InteropServices.Marshal.ZeroFreeCoTaskMemUnicode(unmanagedRef); BUG# 9972 DH - Will not work on Windows 2008 Server.
          System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(unmanagedRef);
        }
      }

      GenericObject resulGEO = new GenericObject();
      resulGEO.insert(byteProtectedData);
      return resulGEO;
    }

    public static unsafe GenericObject ReplacePlaceholderTAGWith_PreKeyTecData(params Object[] arg_pairs)
    {
      // Replace the placeholder without using string objects. This will prevent copying strings in memory.
      // Return value is encrypted byte array.

      GenericObject args = misc.convert_args(arg_pairs);
      SecureStringClass _this = (SecureStringClass)args.get("this", 0);
      string strXMLString = (string)args.get("xml", 0);
      Byte[] byteProtectedData = null;
      int iFullLength = 0;
      int iStartOfPlaceholder = strXMLString.IndexOf("pci.SecureStringClass");
      int iEndOfPlaceholder = iStartOfPlaceholder + 21;
      IntPtr unmanagedRef = IntPtr.Zero;

      try
      {
        unmanagedRef = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(_this.m_sSecureString);
        char* p = (char*)unmanagedRef;

        string sPart1 = strXMLString.Substring(0, iStartOfPlaceholder);
        string sPart2 = strXMLString.Substring(iEndOfPlaceholder);

        iFullLength = sPart1.Length + _this.m_sSecureString.Length + sPart2.Length;
        byte[] byteData = new byte[iFullLength];

        int j = 0;
        for (j = 0; j < sPart1.Length; j++)
          byteData[j] = (byte)sPart1[j];

        for (int x = 0; x < _this.m_sSecureString.Length; x++)
        {
          if (*p == ';')
          {
            p++;
            for (int y = x; y < _this.m_sSecureString.Length - 1; y++)
              byteData[j++] = (byte)*p++;

            byteData[j++] = (byte)'?';
            break;
          }
          p++;
        }

        for (int n = 0; n < sPart2.Length; n++)
          byteData[j++] = (byte)sPart2[n];

        byte[] a = new byte[j];
        for (int k = 0; k < j; k++)
          a[k] = (byte)byteData[k];

        byteProtectedData = ProtectedData.Protect(a, _this.m_byteEntropy, DataProtectionScope.CurrentUser);
      }
      catch (Exception e)
      {
        throw new InvalidOperationException(e.ToString());
      }
      finally
      {
        if (unmanagedRef != IntPtr.Zero)
        {
          // System.Runtime.InteropServices.Marshal.ZeroFreeCoTaskMemUnicode(unmanagedRef); BUG# 9972 DH - Will not work on Windows 2008 Server.
          System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(unmanagedRef);
        }
      }

      GenericObject resulGEO = new GenericObject();
      resulGEO.insert(byteProtectedData);
      return resulGEO;
    }

    public static unsafe GenericObject ReplacePlaceholderTAGWith_IngenicoData(params Object[] arg_pairs)
    {
      // Replace the placeholder without using string objects. This will prevent copying strings in memory.
      // Return value is encrypted byte array.
      GenericObject args = misc.convert_args(arg_pairs);
      SecureStringClass _this = (SecureStringClass)args.get("this", 0);
      string strXMLString = (string)args.get("xml", 0);
      Byte[] byteProtectedData = null;
      int iFullLength = 0;
      int iStartOfPlaceholder = strXMLString.IndexOf("pci.SecureStringClass");
      int iEndOfPlaceholder = iStartOfPlaceholder + 21;
      IntPtr unmanagedRef = IntPtr.Zero;

      try
      {
        unmanagedRef = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(_this.m_sSecureString);
        char* p = (char*)unmanagedRef;
        p++;
        string sPart1 = strXMLString.Substring(0, iStartOfPlaceholder);
        string sPart2 = strXMLString.Substring(iEndOfPlaceholder);

        iFullLength = sPart1.Length + _this.m_sSecureString.Length + sPart2.Length;
        byte[] byteData = new byte[iFullLength - 1];

        int j = 0;
        for (j = 0; j < sPart1.Length; j++)
          byteData[j] = (byte)sPart1[j];

        for (int n = 0; n < _this.m_sSecureString.Length; n++)
          byteData[j++] = (byte)*p++;

        j--;
        for (int n = 0; n < sPart2.Length; n++)
          byteData[j++] = (byte)sPart2[n];

        //Testing data: string s = ASCIIEncoding.ASCII.GetString(byteData);

        byteProtectedData = ProtectedData.Protect(byteData, _this.m_byteEntropy, DataProtectionScope.CurrentUser);
      }
      catch (Exception e)
      {
        throw new InvalidOperationException(e.ToString());
      }
      finally
      {
        if (unmanagedRef != IntPtr.Zero)
        {
          // System.Runtime.InteropServices.Marshal.ZeroFreeCoTaskMemUnicode(unmanagedRef); BUG# 9972 DH - Will not work on Windows 2008 Server.
          System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(unmanagedRef);
        }
      }

      GenericObject resulGEO = new GenericObject();
      resulGEO.insert(byteProtectedData);
      return resulGEO;
    }

    public static unsafe GenericObject ReplacePlaceholderTAGWith_MagReaderData(params Object[] arg_pairs)
    {
      // Replace the placeholder without using string objects. This will prevent copying strings in memory.
      // Return value is encrypted byte array.
      GenericObject args = misc.convert_args(arg_pairs);
      SecureStringClass _this = (SecureStringClass)args.get("this", 0);
      string strXMLString = (string)args.get("xml", 0);
      Byte[] byteProtectedData = null;
      int iFullLength = 0;
      int iStartOfPlaceholder = strXMLString.IndexOf("pci.SecureStringClass");
      int iEndOfPlaceholder = iStartOfPlaceholder + 21;
      IntPtr unmanagedRef = IntPtr.Zero;

      try
      {
        unmanagedRef = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(_this.m_sSecureString);
        char* p = (char*)unmanagedRef;

        string sPart1 = strXMLString.Substring(0, iStartOfPlaceholder);
        string sPart2 = strXMLString.Substring(iEndOfPlaceholder);

        iFullLength = sPart1.Length + _this.m_sSecureString.Length + sPart2.Length;
        byte[] byteData = new byte[iFullLength - 1];

        int j = 0;
        for (j = 0; j < sPart1.Length; j++)
          byteData[j] = (byte)sPart1[j];

        p++;
        for (int n = 0; n < _this.m_sSecureString.Length - 1; n++)
          byteData[j++] = (byte)*p++;

        for (int n = 0; n < sPart2.Length; n++)
          byteData[j++] = (byte)sPart2[n];

        byteProtectedData = ProtectedData.Protect(byteData, _this.m_byteEntropy, DataProtectionScope.CurrentUser);
      }
      catch (Exception e)
      {
        throw new InvalidOperationException(e.ToString());
      }
      finally
      {
        if (unmanagedRef != IntPtr.Zero)
        {
          // System.Runtime.InteropServices.Marshal.ZeroFreeCoTaskMemUnicode(unmanagedRef); BUG# 9972 DH - Will not work on Windows 2008 Server.
          System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(unmanagedRef);
        }
      }

      GenericObject resulGEO = new GenericObject();
      resulGEO.insert(byteProtectedData);
      return resulGEO;
    }

    public static unsafe GenericObject ReplacePlaceholderTAGWith_CreditCardData(params Object[] arg_pairs)
    {
      // Replace the placeholder without using string objects. This will prevent copying strings in memory.
      // Return value is encrypted byte array.
      GenericObject args = misc.convert_args(arg_pairs);
      SecureStringClass _this = (SecureStringClass)args.get("this", 0);
      string strXMLString = (string)args.get("xml", 0);
      Byte[] byteProtectedData = null;
      int iFullLength = 0;
      int iStartOfPlaceholder = strXMLString.IndexOf("pci.SecureStringClass");
      int iEndOfPlaceholder = iStartOfPlaceholder + 21;
      IntPtr unmanagedRef = IntPtr.Zero;

      try
      {
        unmanagedRef = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(_this.m_sSecureString);
        char* p = (char*)unmanagedRef;

        string sPart1 = strXMLString.Substring(0, iStartOfPlaceholder);
        string sPart2 = strXMLString.Substring(iEndOfPlaceholder);
        int iEndOfCCData = 0;

        p++;
        p++;
        int x = 1;
        for (x = 1; x < _this.m_sSecureString.Length; x++)
        {
          if (*p == '^')
            break;
          p++;
        }
        iEndOfCCData = x;

        for (int z = 1; z < x; z++)
          p--;

        iFullLength = sPart1.Length + x + sPart2.Length;
        byte[] byteData = new byte[iFullLength - 1];

        int j = 0;
        for (j = 0; j < sPart1.Length; j++)
          byteData[j] = (byte)sPart1[j];

        for (int n = 1; n < iEndOfCCData; n++)
          byteData[j++] = (byte)*p++;

        for (int n = 0; n < sPart2.Length; n++)
          byteData[j++] = (byte)sPart2[n];

        byteProtectedData = ProtectedData.Protect(byteData, _this.m_byteEntropy, DataProtectionScope.CurrentUser);
      }
      catch (Exception e)
      {
        throw new InvalidOperationException(e.ToString());
      }
      finally
      {
        if (unmanagedRef != IntPtr.Zero)
        {
          // System.Runtime.InteropServices.Marshal.ZeroFreeCoTaskMemUnicode(unmanagedRef); BUG# 9972 DH - Will not work on Windows 2008 Server.
          System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(unmanagedRef);

        }
      }

      GenericObject resulGEO = new GenericObject();
      resulGEO.insert(byteProtectedData);
      return resulGEO;
    }

    // Bug #14288 Mike O - Added VeriFone device
    public static unsafe GenericObject ReplacePlaceholderTAGWith_VeriFoneData(params Object[] arg_pairs)
    {
      // Replace the placeholder without using string objects. This will prevent copying strings in memory.
      // Return value is encrypted byte array.
      GenericObject args = misc.convert_args(arg_pairs);
      SecureStringClass _this = (SecureStringClass)args.get("this", 0);
      string strXMLString = (string)args.get("xml", 0);
      Byte[] byteProtectedData = null;
      int iFullLength = 0;
      int iStartOfPlaceholder = strXMLString.IndexOf("pci.SecureStringClass");
      int iEndOfPlaceholder = iStartOfPlaceholder + 21;
      IntPtr unmanagedRef = IntPtr.Zero;

      try
      {
        unmanagedRef = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(_this.m_sSecureString);
        char* p = (char*)unmanagedRef;
        p++;
        string sPart1 = strXMLString.Substring(0, iStartOfPlaceholder);
        string sPart2 = strXMLString.Substring(iEndOfPlaceholder);

        iFullLength = sPart1.Length + _this.m_sSecureString.Length - 5 + sPart2.Length;
        byte[] byteData = new byte[iFullLength - 1];

        int j = 0;
        for (j = 0; j < sPart1.Length; j++)
          byteData[j] = (byte)sPart1[j];

        for (int n = 0; n < _this.m_sSecureString.Length - 5; n++)
          byteData[j++] = (byte)*p++;

        j--;
        for (int n = 0; n < sPart2.Length; n++)
          byteData[j++] = (byte)sPart2[n];

        //string s = ASCIIEncoding.ASCII.GetString(byteData);

        byteProtectedData = ProtectedData.Protect(byteData, _this.m_byteEntropy, DataProtectionScope.CurrentUser);
      }
      catch (Exception e)
      {
        throw new InvalidOperationException(e.ToString());
      }
      finally
      {
        if (unmanagedRef != IntPtr.Zero)
        {
          // System.Runtime.InteropServices.Marshal.ZeroFreeCoTaskMemUnicode(unmanagedRef); BUG# 9972 DH - Will not work on Windows 2008 Server.
          System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(unmanagedRef);
        }
      }

      GenericObject resulGEO = new GenericObject();
      resulGEO.insert(byteProtectedData);
      return resulGEO;
    }


    private char ConvertHexCharToASCII(string strHexVal)
    {
      char c = '\0';
      c = System.Convert.ToChar(System.Convert.ToUInt32(strHexVal.Substring(0, 2), 16)).ToString()[0];
      return c;
    }

    public static string StringToHex(params Object[] arg_pairs)
    {
      // This is not secure! Use it for fake and not real secure data. 
      // Use the ConvertHexCharToASCII for sucure data!

      GenericObject args = misc.convert_args(arg_pairs);
      string str = (string)args.get("str", "");
      string sHex = "";
      string sValue;

      if (str == "")
        return "";

      foreach (char c in str.ToCharArray())
      {
        sValue = String.Format("{0:X}", Convert.ToUInt32(c));
        sHex = sHex + sValue;
      }

      return sHex;
    }

    public static string HexToString(params Object[] arg_pairs)
    {
      // Convert HEX to ascii string. Reserve for private internal use in this class. Too risky for public access.
      GenericObject args = misc.convert_args(arg_pairs);
      string str = (string)args.get("str", "");

      string sData = "";
      for (int i = 0; i < str.Length; i += 2)
        sData += System.Convert.ToChar(System.Convert.ToInt32(str.Substring(i, 2), 16));

      return sData;
    }
    // Bug 22014 DJD - Card tenders allow processing of invalid card numbers (with a bad check digit)
    public static unsafe bool IsValidCreditCardNumber(SecureStringClass ssc)
    {
      return IsValidCreditCardNumber(ssc.m_sSecureString);
    }

    // Bug #12762 Mike O - Validate card number using Luhn algorithm (just for Preh right now)
    public static unsafe bool IsValidCreditCardNumber(System.Security.SecureString str)
    {
      bool doubleNextDigit = false;
      int checksum = 0;

      IntPtr unmanagedRef = IntPtr.Zero;
      try
      {
        unmanagedRef = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(str);
        char* p = (char*)unmanagedRef;

        for (int x = 0; x < str.Length; x++)
          p++;

        // Just in case the last character is an escape...
        if (!Char.IsNumber(*p))
          p--;

        for (int i = 0; i < str.Length; i++)
        {

          if (!Char.IsNumber(*p))
            break;

          if (doubleNextDigit)
          {
            int num = (((int)*p) - 48) * 2;

            while (num != 0)
            {
              checksum += num % 10;
              num /= 10;
            }
          }
          else
          {
            checksum += (int)*p - 48;
          }

          doubleNextDigit = !doubleNextDigit;
          p--;
        }
      }
      catch (Exception)//12081 NJ-removed unused variable
      {
        return false;
      }
      finally
      {
        if (unmanagedRef != IntPtr.Zero)
        {
          // System.Runtime.InteropServices.Marshal.ZeroFreeCoTaskMemUnicode(unmanagedRef); BUG# 9972 DH - Will not work on Windows 2008 Server.
          System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(unmanagedRef);
        }
      }

      return ((checksum % 10) == 0);
    }

    public static unsafe string GetPaddedCardNbr(params Object[] arg_pairs)
    {
      // Bug 12483: HVMA Phase II: Generic Any Card Not Identifying Cards Correctly

      // This method returns the first n digits (defaults to 6 digits if not passed)
      // of the Card Number with the rest of the number padded with a numeric 
      // character (defaults to "1" if not passed).  It returns the proper length
      // of the card number so it's type may be properly identified (Visa, MC, Amex).
      // For example, "4012002000060016" would be returned as "4012001111111111".

      GenericObject args = misc.convert_args(arg_pairs);
      SecureStringClass _this = (SecureStringClass)args.get("this", 0);

      object theObj = args.get("digits", 6);
      int _digits = theObj is int ? (int)theObj : 6;

      theObj = args.get("pad_str", "1");
      string _pad = theObj is string ? (string)theObj : "1";

      string strReturnValue = "";

      IntPtr unmanagedRef = IntPtr.Zero;
      try
      {
        unmanagedRef = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(_this.m_sSecureString);
        char* p = (char*)unmanagedRef;

        // Find length and start position of card number in track data.
        // Track I Example: %B4012002000060016^VI TEST CREDIT^251210118039000000000396?
        // Track II Example: ;4012002000060016=25121011803939600000? (Debit Card sends Track II only)

        string sTrack = "";
        if (SecureStringClass.KeyOf("this", _this, "a_string", "%").GetType() == typeof(int))
        { sTrack = "I"; }
        else if (SecureStringClass.KeyOf("this", _this, "a_string", ";").GetType() == typeof(int))
        { sTrack = "II"; }

        int Length = 0;
        bool bCountLength = (sTrack == "");
        char cPad = (char)_pad[0];
        if (!(cPad >= '0' && cPad <= '9'))
        {
          cPad = '1';
        }

        for (int x = 0; x < _this.m_sSecureString.Length; x++)
        {
          char cNext = *p;
          bool bDigit = (cNext >= '0' && cNext <= '9');
          // Need to make certain that the character is a digit because some swipes have spaces
          // in the card nbr; e.g.: %B3727 006992 51018^AMEX TEST CARD^2512990502700?

          if (bCountLength && bDigit)
          {
            strReturnValue += (++Length > _digits) ? cPad : cNext;
          }

          switch (sTrack)
          {
            case "I":   // Track I
              {
                if (cNext == 'B')
                {
                  bCountLength = true;
                }
              }
              break;

            case "II":  // Track II
              {
                if (cNext == ';')
                {
                  bCountLength = true;
                }
              }
              break;

            default:    // Card Number Only
              break;
          }

          p++;

          if ((cNext == '^') || // Track I
          (cNext == '='))   // Track II
          {
            break;
          }
        }
      }
      catch (Exception ex)
      {
        StringBuilder sb = new StringBuilder();
        sb.Append("\r\nError while retrieving Padded Card Nbr value from credit or debit card data:");
        sb.Append(string.Format("\r\nMessage ---\r\n{0}", ex.Message));
        if (ex.InnerException != null)
        {
          sb.Append(string.Format("\r\nInner Exception ---\r\n{0}", ex.InnerException.Message));
        }
        sb.Append(string.Format("\r\nSource ---\r\n{0}", ex.Source));
        sb.Append(string.Format("\r\nStack Trace ---\r\n{0}", ex.StackTrace));
        sb.Append(string.Format("\r\nTarget Site ---\r\n{0}", ex.TargetSite));
        Logger.cs_log(sb.ToString());
        strReturnValue = "";
      }
      finally
      {
        if (unmanagedRef != IntPtr.Zero)
        {
          // System.Runtime.InteropServices.Marshal.ZeroFreeCoTaskMemUnicode(unmanagedRef); BUG# 9972 DH - Will not work on Windows 2008 Server.
          System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(unmanagedRef);
        }
      }
      return strReturnValue;
    }

  }

  /// <summary>
  /// Summary description for Class1.
  /// </summary>
  public class pci
  {
    // Bug 18129 DJD - Support for eWallet Transactions [***** Credit Card and eCheck Tenders *****]
    public enum RepositoryEnum { None, PAYWARE_TOKEN, PAYWARE, CORE } // NOTE: eCheck is ALWAYS CORE
    public enum ewItemType { None, CreditCard, eCheck }
    public static readonly string[] ewItemTypes = { "", "Credit Card", "eCheck" };
    public const string EW_HOST_REFERENCE_DEFAULT = "iPayment";
    const string ADMIN_SUCCESS = "ADMIN_TRANS_SUCCESS";

    //BUG 6006 Add new encrypted devices BLL

    const byte nCryptBoxLen = 64;
    const byte nCryptKEYLen = 32;

    static string Key;
    private static String preh_key_bad_read_string = "whst4653zhguikdbmaqp20dKmw2640SF";
    private static String m_BDK_test = "0123456789ABCDEFFEDCBA9876543210";//BUG16062  ANSI KEY
    private static String m_ebdk1 = "lev9DOknTpeG4hO4wezlDg==";//BUG16062  10 digits For ANSI KEY "TGAti10jmiuJfLgI+OYS+g==" 
    private static SecureString m_BDK_secure = null; //BUG16293 
    const string NOT_AUTHORIZED_RESULT_CODE = "2029999"; // Bug 22823 DJD: Email Notification for PAYware "Not Authorized" Response

    // Bug 12634: Add FSA Card Tender to Generic "ANY" Card
    // Healthcare Amount Error will be true if the FSA Card tender amount was greater 
    // than the remaining eligible healthcare transaction amount (i.e., TTA map failed
    // with this amount).
    protected static bool _bHealthcareAmtError = false;


    public static String get_preh_key_bad_read_string()
    {
      return preh_key_bad_read_string;
    }

    public static string MakeFullName(string firstname, string lastname)
    {
      string strFullName = firstname.TrimEnd();
      if (!String.IsNullOrEmpty(firstname))
        strFullName += " ";
      strFullName += lastname;
      return strFullName;
    }

    public static GenericObject Decrypt_preh_key_tec_data_partial(params Object[] arg_pairs)
    {
      //returns only non-sensitive data...credit_card_nbr is masked
      String decrypted_data = Decrypt_preh_key_tec_data(arg_pairs);
      GenericObject return_args = new GenericObject();
      GenericObject parsed_data = Parse_decrypted_swipe_data(decrypted_data);
      //Bug#6326  ANU
      if (parsed_data.get("_parent", null) == c_CASL.c_error)
      {
        return parsed_data;
      }
      //Bug#6326      
      String sCreditCardNbr = (string)parsed_data.get("sCreditCardNbr");
      return_args.set("credit_card_nbr_for_type", Get_credit_card_for_type(sCreditCardNbr));
      // Bug 14661 UMN -set payer_name
      return_args.set("payer_name", MakeFullName(parsed_data.get("sFirstName") as string, parsed_data.get("sLastName") as string));
      return_args.set("payer_lname", parsed_data.get("sLastName"));
      return_args.set("payer_fname", parsed_data.get("sFirstName"));
      return_args.set("exp_month", parsed_data.get("sExpMonth"));
      return_args.set("exp_year", parsed_data.get("sExpYear"));
      return_args.set("credit_card_nbr", sCreditCardNbr.Substring(sCreditCardNbr.Length - 4).PadLeft(sCreditCardNbr.Length, '*'));
      //set fields containing sensitive information equal to null
      decrypted_data = null;
      sCreditCardNbr = null;
      parsed_data = null;
      return return_args;
    }

    public static string Decrypt_preh_key_tec_data(params Object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      String strEncryptedData = (string)args.get("encrypted_data");
      String strEncryptionType = (string)args.get("encryption_type"); // BUG# 8705 DH 06-15-2010

      // BUG# 8705 DH 06-15-2010
      if (strEncryptionType == "2_PREH")// New encryption algorithm
      {
        return Preh.DecryptMSR(strEncryptedData);
      }
      else if (strEncryptionType == "1_PREH")// Old encryption algorithm
      {
        String sBadReadString = get_preh_key_bad_read_string();
        Crypt(sBadReadString);
        return Decrypt(strEncryptedData);
      }
      // Bug #12762 Mike O - Added Preh manual key encryption support
      else if (strEncryptionType == "K_PREH")// Manual key encryption
      {
        return "|" + Preh.DecryptKeyboard(strEncryptedData);
      }

      return "";
    }
    /// <summary>
    /// BUG16293 Convert SecureString to String 
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    static String SecureStringToString(SecureString value)
    {
      IntPtr bstr = Marshal.SecureStringToBSTR(value);

      try
      {
        return Marshal.PtrToStringBSTR(bstr);
      }
      finally
      {
        Marshal.FreeBSTR(bstr);
      }
    }
    public static void setbdk(string sebdk3)
    {
      if (sebdk3 == null) return;
      m_BDK_secure = new SecureString();
      byte[] bebdk1 = Convert.FromBase64String(m_ebdk1);
      byte[] bbdk1 = Crypto.symmetric_decrypt("data", bebdk1, "secret_key", Crypto.get_secret_key());
      string sbdk1 = System.Text.Encoding.Default.GetString(bbdk1);
      foreach (var character in sbdk1.ToCharArray())
      {
        m_BDK_secure.AppendChar(character);
      }
      sbdk1 = null;

      byte[] bebdk2 = Convert.FromBase64String(c_CASL.m_ebdk2);
      byte[] bbdk2 = Crypto.symmetric_decrypt("data", bebdk2, "secret_key", Crypto.get_secret_key());
      string sbdk2 = System.Text.Encoding.Default.GetString(bbdk2);
      foreach (var character in sbdk2.ToCharArray())
      {
        m_BDK_secure.AppendChar(character);
      }
      sbdk1 = null;

      byte[] bebdk3 = Convert.FromBase64String(sebdk3);
      byte[] bbdk3 = Crypto.symmetric_decrypt("data", bebdk3, "secret_key", Crypto.get_secret_key());
      string sbdk3 = System.Text.Encoding.Default.GetString(bbdk3);
      foreach (var character in sbdk3.ToCharArray())
      {
        m_BDK_secure.AppendChar(character);
      }
      return;
    }

    /// <summary>
    /// BUG#16062 ANSI X9.24 DUKPT TRIPLE DES Decryption is used by Mag Tek Encryption Swipe
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static string Decrypt_DUKPT(params Object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      String strEncrypted = (string)args.get("encryptedstring");
      String strKSN = (string)args.get("KSN");

      //BUG16293 initialize secure BDK only one time 
      //if (m_BDK_secure == null)
      //{
      //  m_BDK_secure = new SecureString();
      //  SecureStringClass  ssc = new SecureStringClass();
      //  String strInitBDK = "0123456789ABCDEFFEDCBA9876543210";
      //  byte[] byteInitBDK = Encoding.Unicode.GetBytes(strInitBDK);
      //  byte[] byteEncryptedInitBDK = ProtectedData.Protect(byteInitBDK,ssc.m_byteEntropy,System.Security.Cryptography.DataProtectionScope.CurrentUser);
      //  string strEncryptedInitBDK = Convert.ToBase64String(byteEncryptedInitBDK);

      //  // store strEncryptedInitBDK to database or file 
      //  Logger.cs_log("strEncryptedInitBDK=" + strEncryptedInitBDK);

      //  byte[] byteEncryptedBDK =Convert.FromBase64String(strEncryptedInitBDK);
      //  byte[] byteDecryptedBDK = ProtectedData.Unprotect(byteEncryptedBDK, ssc.m_byteEntropy, System.Security.Cryptography.DataProtectionScope.CurrentUser);

      //  string strDecryptedBDK = Encoding.Unicode.GetString(byteDecryptedBDK);
      //  foreach (var character in strDecryptedBDK.ToCharArray())
      //  {
      //    m_BDK_secure.AppendChar(character);
      //  }
      ////}
      String strMode = (string)args.get("mode", "TEST");
      string strBDK = m_BDK_test;
      if (strMode == "PROD")
        strBDK = SecureStringToString(m_BDK_secure);

      //String strBDK = (string)args.get("BDK", strDefaultBDK);
      //if (strBDK == "") strBDK = strDefaultBDK;

      var dec = new AnsiProvider();

      //TEST DATA A: strPlaintext = "%B379014544596863^YOU/A GIFT FOR            ^12121220707769230000000000000000?\0\0\0?b??E??44596863=121212207077692300000?\0"	
      //string strEncrypted = "5EB5BE44DB777DFACF35B17AE198591856DB1D15F67892B319A86964108DC4A293AB5DD5A3D396ECEF60153E13C9039BFEEB5C88FBB2B05D7D8E27960F783139D99F02B7133A0E583BCC55C19A74A9C3181C2074C8093696D52E9E19920B8899ED4B82E581A2D518C9392D79189750E790975D1D0DE96374";//[YOUR ENCRYPTED STRING]
      //string strBDK = "0123456789ABCDEFFEDCBA9876543210";//[YOUR BDK]
      //string strKSN = "9010010B27CB3800001E";//YOUR KSN

      //TEST DATA B: strPlaintext = "%B5452300551227189^HOGAN/PAUL ^08043210000000725000000?\x00\x00\x00\x00"
      //string strEncrypted = "C25C1D1197D31CAA87285D59A892047426D9182EC11353C051ADD6D0F072A6CB3436560B3071FC1FD11D9F7E74886742D9BEE0CFD1EA1064C213BB55278B2F12";//[YOUR ENCRYPTED STRING]
      //string strBDK = "0123456789ABCDEFFEDCBA9876543210";//[YOUR BDK]
      //string strKSN = "FFFF9876543210E00008";//YOUR KSN

      //string strPlaintext = dec.Decrypt(strBDK, strKSN, strEncrypted);
      byte[] decBytes = Dukpt.Decrypt(strBDK, strKSN, BigInt.FromHex(strEncrypted).GetBytes());
      string strPlaintext = UTF8Encoding.UTF8.GetString(decBytes);

      return strPlaintext;
    }

    public static object Build_and_send_http_request(params Object[] args)
    {
      //build request_args to send to SendHttpRequest
      GenericObject build_args = misc.convert_args(args);
      GenericObject a_uri = new GenericObject();
      GenericObject subject = new GenericObject();
      GenericObject request_args = new GenericObject();
      a_uri.set("uri_string", build_args.get("uri_string"));
      a_uri.set("body", build_args.get("body"));
      a_uri.set("method", build_args.get("method"));
      a_uri.set("content_type", build_args.get("content_type"));
      a_uri.set("scheme", build_args.get("scheme"));
      a_uri.set("user_agent", build_args.get("user_agent"));
      subject.set("a_uri", a_uri);
      request_args.set("_subject", subject);
      request_args.set("timeout", build_args.get("timeout"));
      request_args.set("domain", build_args.get("domain"));
      request_args.set("userid", build_args.get("userid"));
      request_args.set("password", build_args.get("password"));
      request_args.set("workgroup", build_args.get("workgroup", null)); // Bug 22823 DJD: Email Notification for PAYware "Not Authorized" Response

      // BUG# 8705 DH - Pass the secure data separate from the XML string. It will be used to replace track data.
      a_uri.set("SecureStringClass", (SecureStringClass)build_args.get("SecureStringClass"));

      //SEND and RECEIVE
      return SendHttpRequest(request_args) as GenericObject;
    }

    //---------------------------------------------------------------
    // Added by Daniel [8/1/2005]
    // BUG 6207 PCI BLL: moved from misc.cs 11/13/08
    public static object SendHttpRequest(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      GenericObject subject = geo_args.get("_subject") as GenericObject;
      int timeout = (int)geo_args.get("timeout", 9000); // Bug 16285 timeout is now in secs; convert to ms
      string strDomain = geo_args.get("domain") as string;//ANN test credentials
      string strUserID = geo_args.get("userid") as string;//ANN test credentials
      string strPassword = geo_args.get("password") as string;//ANN test credentials
      GenericObject RetVal = c_CASL.c_make("Response") as GenericObject;// Response object
      GenericObject a_uri = subject.get("a_uri") as GenericObject;
      string uri_string = a_uri.get("uri_string") as String;
      String scheme = (string)a_uri.get("scheme", c_CASL.c_error, true);
      String user_agent = (string)a_uri.get("user_agent", c_CASL.c_error, true);
      String http_method = ((string)a_uri.get("method", c_CASL.c_error, true)).ToUpper();
      String content_type = (string)a_uri.get("content_type", c_CASL.c_error, true);
      String body = (string)a_uri.get("body", c_CASL.c_error, true);

      //----------------------------------------------------------------------------
      // BUG# 8705 DH - Get the real track data for transfer and replace the xml 
      // placeholder with real track data.
      string strDataSourceType = "";// MAG_READER, INGENICO
      SecureStringClass pSecureStringClass = null;
      if (a_uri.has("SecureStringClass"))
      {
        pSecureStringClass = (SecureStringClass)a_uri.get("SecureStringClass");
        if (pSecureStringClass != null) // Bug 18129
        {
          strDataSourceType = SecureStringClass.GetDataSourceType("this", pSecureStringClass);
        }
      }
      //----------------------------------------------------------------------------
      // Bug #11852 Mike O - Don't catch exceptions here

      string header = a_uri.get("header", "") as string;

      string[] headers = header.Split(';');
      string soap_action_url = "";
      foreach (string str in headers)
      {
        if (str.IndexOf("SOAPAction") > -1)
        {
          soap_action_url = str.Substring(str.IndexOf(":") + 1);
        }
      }
      //end bug 10719 NJ

      //Bug 13136 -stub mode for testing
      /*if stub_mode is Y and tender amount is $2.05, don't proceed with the void, instead return an error response
      depecting Void window expiration to facilitate addition of an offsetting refund */
      string strResponse = "";
      decimal test_amount = 2.05M;

      // Bug 15259 MJO - Implicit decimal conversion doesn't work on doubles
      if (a_uri.get("stub_mode", "N").Equals("Y") && body.Contains("<PAYMENT_TYPE>CREDIT</PAYMENT_TYPE>")
      && body.Contains("<COMMAND>VOID</COMMAND>") && Convert.ToDecimal(a_uri.get("amount", 0.00)) == test_amount)
      {
        strResponse = "<RESPONSE><AUTH_CODE>08122D</AUTH_CODE> " +
        "<CLIENT_ID>***********01</CLIENT_ID>" +
        "<COMMAND>SALE</COMMAND>" +
        "<CTROUTD>272</CTROUTD>" +
        "<INTRN_SEQ_NUM>274974681</INTRN_SEQ_NUM>" +
        "<INVOICE>20120702091600</INVOICE>" +
        "<PAYMENT_TYPE>CREDIT</PAYMENT_TYPE>" +
        "<RESPONSE_TEXT>REV OUTSIDE WIN - 914 </RESPONSE_TEXT>" +
        "<RESULT>ERROR</RESULT>" +
        "<RESULT_CODE>3705</RESULT_CODE>" +
        "<TERMINATION_STATUS>NOT_PROCESSED</TERMINATION_STATUS>" +
        "<TRANS_AMOUNT>159.00</TRANS_AMOUNT>" + "</RESPONSE>";
      }
      else // else NOT STUB mode
      {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri_string);

        if (strUserID != null) //ANN test credentials
          request.Credentials = new NetworkCredential(strUserID, strPassword, strDomain); //ANN test credentials
        string PostingData = body;

        request.Method = http_method; // Method;
        request.UserAgent = user_agent; // UserAgent;
        request.AllowAutoRedirect = false;
        request.Timeout = timeout;

        if (http_method == "POST")
        {
          request.ContentType = content_type; // ContentType;

          //===================================================================================
          // BUG# 8705 DH
          // request.ContentLength = PostingData.Length; 
          // Replace the placeholder and put it into encrypted byte array. Then decrypt it and send it to IPCharge decrypted.
          GenericObject geoEncryptedData = null;
          byte[] bytes = null;
          if (pSecureStringClass != null)// BUG# 9697 DH - Check to see if there is a encrypted class. Otherwise process normally.
          {
            if (strDataSourceType == "INGENICO")
              geoEncryptedData = (GenericObject)SecureStringClass.ReplacePlaceholderTAGWith_IngenicoData("this", pSecureStringClass, "xml", PostingData);
            else if (strDataSourceType == "PREH_KEY_TEC")
              geoEncryptedData = (GenericObject)SecureStringClass.ReplacePlaceholderTAGWith_PreKeyTecData("this", pSecureStringClass, "xml", PostingData);
            else if (strDataSourceType == "MAG_READER")
              geoEncryptedData = (GenericObject)SecureStringClass.ReplacePlaceholderTAGWith_MagReaderData("this", pSecureStringClass, "xml", PostingData);
            else if (strDataSourceType == "MANUAL_ENTRY" || strDataSourceType == "EWALLET_TOKEN") // Bug 18129 DJD - Added EWALLET_TOKEN
              geoEncryptedData = (GenericObject)SecureStringClass.ReplacePlaceholderTAGWith_TrackData("this", pSecureStringClass, "xml", PostingData);
            else if (strDataSourceType == "VERIFONE_MANUAL") // Bug #14288 Mike O - Added VeriFone device
              geoEncryptedData = (GenericObject)SecureStringClass.ReplacePlaceholderTAGWith_VeriFoneData("this", pSecureStringClass, "xml", PostingData);
            else
              geoEncryptedData = (GenericObject)SecureStringClass.ReplacePlaceholderTAGWith_TrackData("this", pSecureStringClass, "xml", PostingData);

            bytes = System.Security.Cryptography.ProtectedData.Unprotect((byte[])geoEncryptedData.get(0), pSecureStringClass.m_byteEntropy, System.Security.Cryptography.DataProtectionScope.CurrentUser);
          }

          if (pSecureStringClass == null)// BUG# 9697 DH - Check to see if there is a encrypted class. Otherwise process normally
          {
            UTF8Encoding encoding = new UTF8Encoding();
            bytes = encoding.GetBytes(PostingData);
          }
          //==================================================================================
          //Bug 10719 NJ
          if (!soap_action_url.Equals(""))
          {
            request.Headers.Add("SOAPAction", soap_action_url);
          }
          //end bug 10719 NJ
          request.ContentLength = bytes.Length;

          // Bug 16433 UMN ensure that the stream is closed if an exception occurs
          using (Stream writeStream = request.GetRequestStream()) writeStream.Write(bytes, 0, bytes.Length);
        }  // end if http_method == "POST"

        // Bug 16389 UMN ensure that the stream is closed if an exception occurs
        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        {
          // Bug 16389 UMN ensure that the stream is closed if an exception occurs
          using (Stream responseStream = response.GetResponseStream())
          {
            using (StreamReader readStream = new StreamReader(responseStream, Encoding.UTF8))
            {
              strResponse = readStream.ReadToEnd();
            }
          }
          RetVal.set("content_type", response.ContentType.ToString());
          HttpContext.Current.ApplicationInstance.CompleteRequest(); // Bug 16461
        }
        // Bug #11990 Mike O - Removed response string logging to CS log

      } // END else NOT STUB mode

      // Bug 22823 DJD: Email Notification for PAYware "Not Authorized" Response
      if (ResponseIsNotAuthorized(strResponse))
      {
        GenericObject workgroup = geo_args.get("workgroup", null) as GenericObject;
        if (workgroup == null)
        {
          Logger.cs_log(string.Format("Unable To Send Not Authorized Email: No workgroup found."));
        }
        else
        {
          sendNotAuthorizedEmail(workgroup);
        }
      }
      else // Bug 26330 DJD: Email Notification for Refund Response Missing Auth Code
      {
        string pmtMedia = null;
        if (ResponseIsRefundMissingAuthCode(strResponse, ref pmtMedia))
        {
          GenericObject workgroup = geo_args.get("workgroup", null) as GenericObject;
          if (workgroup == null)
          {
            Logger.cs_log(string.Format("Unable To Send Refund Missing Auth Code Email: No workgroup found."));
          }
          else
          {
            sendMissingAuthCodeEmail(body, pmtMedia, workgroup);
          }
        }
      }

      RetVal.set("body", strResponse);
      return RetVal;
    }

    /// <summary>
    /// Bug 26330 DJD: Email Notification for Refund Response Missing Auth Code
    /// </summary>
    /// <param name="body"></param>
    /// <param name="pmtMedia"></param>
    /// <param name="workgroup"></param>
    private static void sendMissingAuthCodeEmail(string body, string pmtMedia, GenericObject workgroup)
    {
      try
      {
        string workgroupID = workgroup.get("id", "") as string;
        string workgroupName = workgroup.get("name", "") as string;
        string emailReceiver = GetEmailForError(workgroup, workgroupID, workgroupName);
        if (!string.IsNullOrWhiteSpace(emailReceiver))
        {
          // Get the receipt number from request
          string receiptNbr = "";
          const string TAG_TRANSACTION = "TRANSACTION";
          const string TAG_COL_3 = "COL_3";
          XDocument xdoc = XDocument.Parse(body);
          foreach (XElement xeItem in (xdoc.Descendants(TAG_TRANSACTION)))
          {
            receiptNbr = xeItem.Elements(TAG_COL_3).Count() > 0 ? (xeItem.Element(TAG_COL_3).Value) : "";
          }
          string msg = string.Format("{1}{0}{2}{0}{3}{0}{4}{0}", "<BR/>"
          , "The PAYware response for a refund is missing the Auth Code."
          , string.Format("    Receipt Number: {0}", receiptNbr)
          , string.Format("    Payment Media: {0}", pmtMedia)
          , string.Format("    Workgroup ID: {0}, Workgroup Name: {1}", workgroupID, workgroupName)
          );
          GenericObject geoErr = new GenericObject("_parent", "GEO.error", "code", "90901", "message", msg);
          emails.CASL_send_email_notification(
          "module", "PAYware Connect Payment Gateway",
          "action", "Processing Refund Card Request",
          "email_subject", "Missing AUTH CODE in Response from Card Refund",
          "email_receiver", emailReceiver,
          "error", geoErr);
          // Log missing AUTH CODE message
          Logger.cs_log(msg.Replace("<BR/>", Environment.NewLine).Trim());
        }
      }
      catch (Exception ex)
      {
        Logger.cs_log(string.Format("Send Refund Missing Auth Code Email Error:{0}{1}", Environment.NewLine, ex.ToMessageAndCompleteStacktrace()));
      }
    }

    /// <summary>
    /// Bug 26330 DJD: Email Notification for Refund Response Missing Auth Code
    /// </summary>
    /// <param name="workgroup"></param>
    /// <param name="workgroupID"></param>
    /// <param name="workgroupName"></param>
    /// <returns></returns>
    private static string GetEmailForError(GenericObject workgroup, string workgroupID, string workgroupName)
    {
      string emailReceiver = c_CASL.GEO.get("email_for_error", "") as string;
      if (string.IsNullOrEmpty(emailReceiver))
      {
        emailReceiver = workgroup.get("email_for_error", "") as string; // Check for workgroup email address (Log error if not found)
        if (string.IsNullOrEmpty(emailReceiver))
        {
          Logger.cs_log(string.Format("{0}{1}{0}No 'email for error' address found in either the config.casl or workgroup [{2}: {3}] settings."
          , Environment.NewLine
          , "Configuration Error: Failed to send email notification for NOT AUTHORIZED card response."
          , workgroupID
          , workgroupName
          ));
        }
      }

      return emailReceiver;
    }

    //end BUG 6207 PCI: code moved

    /// <summary>
    /// Bug 22823 DJD: Email Notification for PAYware "Not Authorized" Response
    /// Sends an email notification for a "NOT AUTHORIZED" card response.  Workgroup CC Credentials may be wrongly configured.
    /// Uses the 'email for error' address found in the config.casl file (and then the workgroup if missing in config.casl).
    /// </summary>
    /// <param name="workgroup"></param>
    private static void sendNotAuthorizedEmail(GenericObject workgroup)
    {
      try
      {
        string workgroupID = workgroup.get("id", "") as string;
        string workgroupName = workgroup.get("name", "") as string;
        string emailReceiver = c_CASL.GEO.get("email_for_error", "") as string;
        if (string.IsNullOrEmpty(emailReceiver))
        {
          emailReceiver = workgroup.get("email_for_error", "") as string; // Check for workgroup email address (Log error if not found)
          if (string.IsNullOrEmpty(emailReceiver))
          {
            Logger.cs_log(string.Format("{0}{1}{0}No 'email for error' address found in either the config.casl or workgroup [{2}: {3}] settings."
            , Environment.NewLine
            , "Configuration Error: Failed to send email notification for NOT AUTHORIZED card response."
            , workgroupID
            , workgroupName
            ));
            return;
          }
        }
        string msg = string.Format("{1}{0}{2}{0}{3}{0}{4}{0}{5}", "<BR/>"
        , "The response from PAYware indicates an invalid Merchant Key, Client ID, User ID, or Password."
        , "Please verify the following workgroup configuration properties: CC MERCHANTKEY, CC CLIENTID, CC USERID, and CC USERPW."
        , string.Format("    Workgroup ID: {0}", workgroupID)
        , string.Format("    Workgroup Name: {0}", workgroupName)
        , "PAYware Response Text: NOT AUTHORIZED "
        );
        GenericObject geoErr = new GenericObject("_parent", "GEO.error", "code", NOT_AUTHORIZED_RESULT_CODE, "message", msg);
        emails.CASL_send_email_notification(
        "module", "PAYware Connect Payment Gateway",
        "action", "Processing Card Request",
        "email_subject", "NOT AUTHORIZED Response from Card Request",
        "email_receiver", emailReceiver,
        "error", geoErr);
      }
      catch (Exception ex)
      {
        Logger.cs_log(string.Format("Send Not Authorized Email Error:{0}{1}", Environment.NewLine, ex.ToMessageAndCompleteStacktrace()));
      }
    }

    /// <summary>
    /// Bug 22823 DJD: Email Notification for PAYware "Not Authorized" Response
    /// Determines if the PAYware response is NOT AUTHORIZED.
    /// RESULT_CODE: 2029999 ? Incorrect Client ID, Invalid Merchant Key, or Command not authorized. For Example:
    ///  <RESPONSE>
    ///    <RESPONSE_TEXT>NOT_AUTHORIZED</RESPONSE_TEXT>
    ///    <RESULT>ERROR</RESULT>
    ///    <RESULT_CODE>2029999</RESULT_CODE>
    ///    <TERMINATION_STATUS>NOT_PROCESSED</TERMINATION_STATUS>
    ///  </RESPONSE>
    /// </summary>
    /// <param name="pAYwareResponse"></param>
    /// <returns>True if NOT AUTHORIZED response, False otherwise</returns>
    private static bool ResponseIsNotAuthorized(string pAYwareResponse)
    {
      bool isNotAuthResp = false;
      try
      {
        const string TAG_RESPONSE = "RESPONSE";
        const string TAG_RESULT_CODE = "RESULT_CODE";
        if (pAYwareResponse.Contains(NOT_AUTHORIZED_RESULT_CODE)) // Quick check for a "Not Authorized" result code 
        {
          // Parse XML to make certain the "2029999" value is in the RESULT_CODE field
          string resultCodeValue = "";
          XDocument xdoc = XDocument.Parse(pAYwareResponse);
          foreach (XElement xeItem in (xdoc.Descendants(TAG_RESPONSE))) { resultCodeValue = (xeItem.Element(TAG_RESULT_CODE).Value); }
          isNotAuthResp = (resultCodeValue.Trim() == NOT_AUTHORIZED_RESULT_CODE);
        }
      }
      catch (Exception ex)
      {
        Logger.cs_log(string.Format("Response Is Not Authorized Check Error:{0}{1}", Environment.NewLine, ex.ToMessageAndCompleteStacktrace()));
      }
      return isNotAuthResp;
    }

    /// <summary>
    /// Bug 26330 DJD: Visa Return Authorization Support
    /// This is a check for an captured refund transaction (Command CREDIT) to make certain that an AUTH_CODE was
    /// returned in the response.  (An email message will be sent if it is not - in addition it will be logged.)
    /// </summary>
    /// <param name="pAYwareResponse"></param>
    /// <param name="pmtMedia"></param>
    /// <returns>True if email notification is needed, False otherwise</returns>
    private static bool ResponseIsRefundMissingAuthCode(string pAYwareResponse, ref string pmtMedia)
    {
      bool missingAuthCode = false;
      try
      {
        const string COMMANDCREDIT = @"<COMMAND>CREDIT</COMMAND>";
        if (pAYwareResponse.Contains(COMMANDCREDIT)) // Quick check for a "refund" (Command: Credit) response 
        {
          const string TAG_RESPONSE = "RESPONSE";
          const string TAG_PMT_MEDIA = "PAYMENT_MEDIA";
          const string TAG_RESULT = "RESULT";
          const string TAG_PMTTYPE = "PAYMENT_TYPE";
          const string TAG_AUTH_CODE = "AUTH_CODE";
          string authCode = "";
          string pmtType = "";
          string result = "";
          XDocument xdoc = XDocument.Parse(pAYwareResponse);
          foreach (XElement xeItem in (xdoc.Descendants(TAG_RESPONSE)))
          {
            pmtMedia = xeItem.Elements(TAG_PMT_MEDIA).Count() > 0 ? (xeItem.Element(TAG_PMT_MEDIA).Value) : "";
            authCode = xeItem.Elements(TAG_AUTH_CODE).Count() > 0 ? (xeItem.Element(TAG_AUTH_CODE).Value) : "";
            pmtType = xeItem.Elements(TAG_PMTTYPE).Count() > 0 ? (xeItem.Element(TAG_PMTTYPE).Value) : "";
            result = xeItem.Elements(TAG_RESULT).Count() > 0 ? (xeItem.Element(TAG_RESULT).Value) : "";
          }
          missingAuthCode = (pmtType == "CREDIT" && result == "CAPTURED" && string.IsNullOrWhiteSpace(authCode));
        }
      }
      catch (Exception ex)
      {
        Logger.cs_log(string.Format("Response Is Refund Missing Auth ERROR:{0}{1}", Environment.NewLine, ex.ToMessageAndCompleteStacktrace()));
      }
      return missingAuthCode;
    }

    public static GenericObject Parse_decrypted_swipe_data(String sDecrypted_data)
    {
      GenericObject parsed_data = new GenericObject();
      try
      {
        parsed_data.set("sCreditCardNbr", sDecrypted_data.Substring(2, sDecrypted_data.IndexOf("^") - 2));
        int iNameStart = sDecrypted_data.IndexOf("^");
        String sNameToEnd = sDecrypted_data.Substring(iNameStart + 1);
        int iEndOfName = sNameToEnd.LastIndexOf("^");
        String sFullName = sDecrypted_data.Substring(iNameStart + 1, iEndOfName);
        //Bug#6326 work around To resolve length cannot be less than zero.- ANU
        if (sFullName.IndexOf("/") > 0)
        {
          parsed_data.set("sLastName", sFullName.Substring(0, sFullName.IndexOf("/")));
          parsed_data.set("sFirstName", sFullName.Substring(sFullName.IndexOf("/") + 1));
        }
        else
        {
          parsed_data.set("sLastName", sFullName);
          parsed_data.set("sFirstName", sFullName);
        }
        //Bug#6326 end 
        parsed_data.set("sExpMonth", sDecrypted_data.Substring(iNameStart + sFullName.Length + 4, 2));
        parsed_data.set("sExpYear", sDecrypted_data.Substring(iNameStart + sFullName.Length + 2, 2));
        parsed_data.set("sTrackData", sDecrypted_data.Substring(sDecrypted_data.LastIndexOf(";") + 1));
      }
      //Bug#6326  
      catch (System.IndexOutOfRangeException e)
      {

        GenericObject geoError = (GenericObject)c_CASL.c_make("error", "code", "CC-101", "message", e.ToString(), "title", "Card swipe error", "throw", false);

        return geoError;

      }
      //Bug#6326  
      return parsed_data;
    }

    public static GenericObject Parse_decrypted_manual_data(String sDecrypted_data)
    {
      //THIS IS NOT CODED YET, NOT SURE YET HOW THIS WILL BE FORMATTED
      GenericObject parsed_data = new GenericObject();
      parsed_data.set("sCreditCardNbr", "");
      parsed_data.set("sLastName", "");
      parsed_data.set("sFirstName", "");
      parsed_data.set("sExpMonth", "");
      parsed_data.set("sExpYear", "");
      // Bug #14191 Mike O - Changed to CVV2
      parsed_data.set("sCVV2", "");
      parsed_data.set("sAddress", "");
      parsed_data.set("sZip", "");
      return parsed_data;
    }

    public static string Get_credit_card_for_type(string sCreditCardNumber)
    {
      if (sCreditCardNumber.StartsWith("6011") || sCreditCardNumber.StartsWith("65"))
        return "6011111111111111";
      if (sCreditCardNumber.StartsWith("4") && (sCreditCardNumber.Length == 16 || sCreditCardNumber.Length == 13))
        return "4111111111111111";
      if (sCreditCardNumber.StartsWith("3"))
        return "3111111111111111";
      else
      {
        string sPrefix = sCreditCardNumber.Substring(0, 2);
        // Bug #12055 Mike O - Use misc.Parse to log errors
        // MasterCard numbers either start with the numbers 51 through 55 or with the numbers 2221 through 2720. All have 16 digits.
        // Bug 18632 master card bin number change accepting new series 22 to 27
        int iPrefix = misc.Parse<Int32>(sPrefix);
        if ((iPrefix > 50 && iPrefix < 56) || (iPrefix > 21 && iPrefix < 28))
          return "5411111111111111";
        else
          return "7777777777777777"; //no match
      }
    }

    // Bug 16285/17690 UMN removed IPCharge_get_cbc_timestamp

    // BUG 6207 PCI BLL: IPCharge
    //
    // Bug 16285 UMN Implement new duplicate detection code to handle the case of a timeout on the primary 
    //   IPCharge URL, and a retry on any other alternate IPCharge URL. If any IPCharge calls timeout, 
    //   but continue to process the charge, then a customer will get one or more duplicate charges. 
    //   This fix relies on the duplicate detection being turned on in the PayWare Console, using the PayWare 
    //   fields: ACCT_NUM, TRANS_AMOUNT and INVOICE. This code will fail over gracefully in that if duplicate 
    //   detection is not turned on in the Payware console, then payments will still process but with the 
    //   same double charge weakness as before.
    //
    public static GenericObject IPCharge_build_send_receive(params Object[] arg_pairs)
    {
      // BUILD
      /*
      * IMPORTANT: May or may not be encrypted data in tender.cc_track_info
      *           May or may not be "swiped" data coming in
      */
      GenericObject return_value = null; // Bug #13644 Mike O - Default to null, so we can easily tell if it has been set
      GenericObject args = misc.convert_args(arg_pairs);

      string userid = args.get("LOGIN_ID") as string;   //  Bug 18732

      // Bug #14905 Mike O - Pull in IPCharge override configuration from the tender, if available (for convenience fees)
      GenericObject tender = args.get("a_tender") as GenericObject;
      //First check that Dept is configured for IPCharge, this is our log in information
      GenericObject this_dept = args.get("this_dept") as GenericObject;

      MerchantKeyAuthentication paywareAuth = new MerchantKeyAuthentication(this_dept, tender); // Bug 21009 [Bug 18129] eWallet Support
                                                                                                // End Bug #14905 Mike O

      string errMsg = "";
      if (!paywareAuth.Validate(ref errMsg)) // Return an error (Bug 21009 [Bug 18129] eWallet Support)
      {
        throw CASL_error.create("message", errMsg, "code", 576); // Bug #13574 Mike O - Moved the error code into its own parameter
      }

      //Set up rest of information needed to create the ipcharge request xml
      decimal dTotal = (decimal)tender.get("total");

      // BEGIN Bug 18129 Pre Auth and Post Auth (Completion) data
      bool bPreAuthTrans = false;
      bool bCompletionTrans = false;
      bool bPreAuthTransRefTroutD = false; // For a "Re-Auth"
      decimal dTotalPreAuth = 0.00M;
      string previousTROUTD = ""; // Used for another preauth (reauth) or completion
                                  // END Bug 18129 Pre Auth and Post Auth (Completion) data

      // BEGIN Bug 18129 Support for PAYware ADD-ON ADMIN Functions (e.g., Customer and Contract)
      // eWallet, CORE Repository Admin Transaction, or PAYware Admin Transaction items:
      bool eWalletAdminTrans = false;
      bool eWalletAdminTransAfterCardTrans = false;
      string eWalletPAYwareToken = ""; // The "TKN_PAYMENT" returned from card trans used for an "Admin Trans After Card Trans"
      bool eWalletTokenTrans = false;
      GenericObject eWalletSysInt = null;
      RepositoryEnum eWalletRepository = RepositoryEnum.None;
      List<CardActionEnum> adminCommandList = new List<CardActionEnum>(); // Identifies the PAYware Admin AddOn Commands to be executed
      string eWalletHostSystemID = ""; // Identifies the host system using the wallet
      string eWalletHostSystemID2 = ""; // Bug 18912 Optional wallet value: stores the PAYware MerchantKey hashed value
      string eWalletPersonID = ""; // Identifies the eWallet person (MRN, SSN, etc...)
      string eWalletToken = ""; // Used for a card transaction (Sale, PreAuth) that uses an eWallet Token
      string merchantCustomerID = ""; // Used for a customer/contract card transaction
      string merchantContractID = ""; // Used for a customer/contract card transaction
      EWalletDemographics eWalletOwnerDemograhics = null; // Bug 23150: For retrieving owner demographics in a transaction (add wallet item)
                                                          // END Bug 18129

      GenericObject system_interface = args.get("a_system_interface") as GenericObject;
      GenericObject core_event = args.get("a_core_event") as GenericObject;
      String sFile_number = (string)args.get("core_file_number");

      // ***** BEGIN Bug 18129 DJD - Support for eWallet Token and/or ADD-ON ADMIN Functions (e.g., PAYware Customer and Contract) *****
      // NOTE: To keep things consistent and simple, the PAYware Connect Admin commands (payware_admin_command) are used to create 
      // eWallet owner/items EVEN when the repository is CORE (not PAYware).
      string err = "";
      string acString = GetEWalletAdminCommands(tender, core_event, ref eWalletOwnerDemograhics, adminCommandList, ref err);
      if (!string.IsNullOrEmpty(err))
      {
        return misc.MakeCASLError("eWallet Error", "EWALLET-001", string.Format("CARDERROR {0}", err), true);
      }
      eWalletAdminTrans = (adminCommandList.Count > 0);
      eWalletHostSystemID = tender.get("host_system_id", "", true) as string;

      eWalletPersonID = tender.get("host_system_individual_id", "") as string;
      if (!eWalletAdminTrans)
      {
        eWalletToken = tender.get("ewallet_token", "") as string;
        eWalletTokenTrans = !string.IsNullOrEmpty(eWalletToken);
      }
      // Get the eWallet System Interface if this is an eWallet related transaction (Admin or Token)
      GenericObject geoRetVal = GetEWalletSysIntAndRepository(eWalletAdminTrans, eWalletTokenTrans, ref eWalletSysInt, ref eWalletRepository);
      if (geoRetVal != null)
      {
        return geoRetVal; // Return CASL error if there was a problem.
      }
      string sValidateToken = eWalletSysInt == null ? "N" : eWalletSysInt.get("require_token_validation", "Y") as string;
      bool validateToken = (sValidateToken == "Y");
      // ***** END Bug 18129 *****

      // BEGIN Bug 21009 [Bug 18129] Support for eWallet ADMIN Functions
      // Check transaction(s) for the eWalletPersonID [and eWalletHostSystemID] if eWalletPersonID is missing and this is an Admin Trans
      if (string.IsNullOrEmpty(eWalletPersonID) && eWalletAdminTrans)
      {
        GenericObject trans = core_event.get("transactions", new GenericObject()) as GenericObject;
        int iCountofTransactions = trans.getIntKeyLength();
        for (int i = 0; i < iCountofTransactions; i++)
        {
          GenericObject tran = (GenericObject)trans.get(i);
          if (ItemHasValidStatus(tran))
          {
            if (GetPersonIDAndHostIDFromTrans(tran, ref eWalletPersonID, ref eWalletHostSystemID))
            {
              break;
            }
          }
        }
      }

      // Make certain that the required Person ID is provided for an eWallet Admin or Token transaction
      if (string.IsNullOrEmpty(eWalletPersonID))
      {
        if (eWalletAdminTrans || (eWalletTokenTrans && validateToken))
        {
          errMsg = string.Format("The eWallet {0} transaction is missing a required data element: Person ID.", eWalletAdminTrans ? "Admin" : "Token");
          Logger.cs_log(errMsg);
          return misc.MakeCASLError("eWallet Error", "EWALLET-002", string.Format("CARDERROR {0}", errMsg), true);
        }
      }
      else // Set Individual ID and Host ID to the tender
      {
        tender.set("host_system_individual_id", eWalletPersonID);
        tender.set("host_system_id", eWalletHostSystemID);
      }

      // Bug 18129 Determine if this is a PreAuth Transaction (i.e., has "preauth_total" amount) or Completion Transaction
      GetPreAuthRelatedItems(args, tender, ref bPreAuthTrans, ref bCompletionTrans, ref bPreAuthTransRefTroutD, ref dTotalPreAuth, ref previousTROUTD);

      GenericObject previous_tender = args.get("previous_tender", null) as GenericObject;
      previousTROUTD = previous_tender == null ?
      tender.get("PAYwarePreviousTranID", "") as string :
      previous_tender.get("auth_str", "") as string;

      if (!string.IsNullOrEmpty(previousTROUTD))
      {
        if (bPreAuthTrans)
        {
          bPreAuthTransRefTroutD = true;
          bPreAuthTrans = false;
        }
        else
        {
          bCompletionTrans = true;
        }
      }

      // UMN removed invoicenbr; now set from serial_id
      String sCol3 = (string)args.get("col_3");

      bool bIs_swipe = (bool)args.get("is_swipe");
      tender.set("ewallet_isSwipe", bIs_swipe); // Bug 23150 DJD Save value for eWallet Update S.I.

      String file_source_type = (string)args.get("file_source_type"); // Bug #10663 Mike O

      // Bug 16285 UMN - get these from the system interface
      String host = (string)system_interface.get("host");
      String adminHost = (string)system_interface.get("admin_host", ""); // Bug 18129 DJD - Support for PAYware ADD-ON ADMIN Functions (e.g., Customer and Contract)
      int timeout = (int)system_interface.get("timeout", 9000); // Bug 16285 timeout is now in secs; convert to ms
      int pause1 = (int)system_interface.get("pause1", 0); // Bug 16285 pause is in secs; convert to ms
      String althost1 = (string)system_interface.get("alternatehost1");
      int timeout1 = (int)system_interface.get("timeout1", 9000); // Bug 16285 timeout is now in secs; convert to ms
      int pause2 = (int)system_interface.get("pause2", 0); // Bug 16285 pause is in secs; convert to ms
      String althost2 = (string)system_interface.get("alternatehost2");
      int timeout2 = (int)system_interface.get("timeout2", 9000); // Bug 16285 timeout is now in secs; convert to ms

      String device_name = (string)args.get("device_name", null);
      String serial_nbr = (string)args.get("serial_nbr", null);
      String device_key = (string)args.get("device_key", null);

      // BEGIN Bug 18129 DJD - Support for PAYware ADD-ON ADMIN Functions: Validate Admin transaction host configuration
      if (eWalletAdminTrans && eWalletRepository == RepositoryEnum.PAYWARE && string.IsNullOrEmpty(adminHost))
      {
        err = string.Format("PAYware Admin Host is not configured for System Interface: {0}. Unable to perform {1} transaction(s)."
        , system_interface.get("name") as string
        , acString);
        Logger.cs_log(err);
        return misc.MakeCASLError("System Interface Error", "PWADMIN-001", string.Format("CARDERROR{0}", err), true);
      }
      // END Bug 18129 DJD - Support for PAYware ADD-ON ADMIN Functions

      // BUG# 8705 DH
      SecureStringClass sCc_track_info = null;
      object o = tender.get("cc_track_info");
      if (!bCompletionTrans && !bPreAuthTransRefTroutD && !eWalletTokenTrans) // BUG 18129
      {
        if (o is GenericObject)
        {
          GenericObject pTrackData = (GenericObject)o;
          sCc_track_info = (SecureStringClass)pTrackData.get(0);
        }
        else // Probably manual entry. Try the credit_card_nbr_secure_class.
        {
          if (tender.has("credit_card_nbr_secure_class"))
          {
            GenericObject pTrackData = (GenericObject)tender.get("credit_card_nbr_secure_class");
            sCc_track_info = (SecureStringClass)pTrackData.get(0);

            // BEGIN Bug 22013 and Bug 22014 DJD 
            string dataSourceType = SecureStringClass.GetDataSourceType("this", sCc_track_info);
            if (dataSourceType == "MANUAL_ENTRY" && tender.has("credit_card_nbr_f_type", true))
            {
              // Bug 22014 DJD - Validate manually entered card number with the Luhn algorithm [Card tenders allow processing of invalid card numbers (with a bad check digit)]
              if (!SecureStringClass.IsValidCreditCardNumber(sCc_track_info))
              {
                GenericObject cASLError = misc.MakeCASLError("Invalid Card Number", "100CN", string.Format("Card Number is not a valid credit card number."), true);
                return cASLError;
              }

              // Bug 22013 DJD - Validate manually entered card number for the tender card brand (MasterCard, Visa, etc...)
              GenericObject CCType = (GenericObject)tender.get("credit_card_nbr_f_type", null, true);
              if (CCType != null)
              {
                string maskVal = GetMaskedCardNbr(sCc_track_info);
                Boolean cardTypeOK = (Boolean)misc.CASL_call("_subject", CCType, "a_method", "is_type_for", "args", new GenericObject("a_value", maskVal));
                if (!cardTypeOK)
                {
                  object tenderName = ((GenericObject)((GenericObject)tender.get("_parent")).get("_parent")).get("_name", "", true);
                  GenericObject cASLError = misc.MakeCASLError("Invalid Card Number", "200CN", string.Format("Card Number is not valid for the {0} tender.", tenderName), true);
                  return cASLError;
                }
              }
            }
          }
          // END   Bug 22013 and Bug 22014 DJD 
        }
      }
      // String sCc_track_info = (string)tender.get("cc_track_info");
      // Bug #11869 Mike O - Removed session validation as it's done elsewhere

      // Bug#7227 Debit card processing ANU
      string sPin_block = null;
      string sPin = null;
      string sKey_serial_nbr = null;
      if (!bCompletionTrans && !bPreAuthTransRefTroutD && !bPreAuthTrans && !eWalletTokenTrans) // BUG 18129
      {
        if (tender.has("payment_type") && tender.get("payment_type") is string)
        {
          if ((string)tender.get("payment_type") == "debit")
          {
            sPin_block = (string)tender.get("debit_pin");
            sPin = sPin_block.Substring(0, 16);
            sKey_serial_nbr = sPin_block.Substring(20, 16);//Bug#7514 "FFFF" should not be included ANU // Bug#7227 Changed for Debit card processing as per Ingenico.
          }
          // Bug 16093 MJO - Support VeriFone debit
          else if ((string)tender.get("payment_type") == "VeriFone" && !(tender.get("debit_pin", new GenericObject()) is GenericObject))
          {
            sPin_block = (string)tender.get("debit_pin");
            if (sPin_block != null && sPin_block != "" && sPin_block.Length >= 16)// Bug 13720 DH - iPayment Peripheral Overhaul - I added a check here.
            {
              sKey_serial_nbr = sPin_block.Substring(0, 16);
              sPin = sPin_block.Substring(sPin_block.Length - 16);
            }
          }
        }
      }
      // Bug 12634: Add FSA Card Tender to Generic "ANY" Card
      // Healthcare Amount Error (TTA map failed with this amount; "ANY Card" related error).
      Object theObj = args.get("healthcare_amt_error", false);
      _bHealthcareAmtError = theObj is bool ? (bool)theObj : false;

      GenericObject result_set = new GenericObject();
      // populate result_set to be sent to build_ipcharge_request_xml
      // IPCharge Required Parameters
      result_set.set("FUNCTION_TYPE", "PAYMENT");
      // Bug#7227 Debit card processing ANU
      if (sPin_block == null || sPin_block == "")
        result_set.set("PAYMENT_TYPE", "CREDIT");
      else
        result_set.set("PAYMENT_TYPE", "DEBIT");

      String sTotal = String.Format("{0:F2}", dTotal); // BUG#6222;

      string strCOMMAND = ""; //BUG#14175
                              // BEGIN Bug 18129 - Expansion of the Payware Integration
      if (bPreAuthTrans || bPreAuthTransRefTroutD) // Pre-authorization request (PRE_AUTH comand)
      {
        string sTotalPreAuth = string.Format("{0:F2}", dTotalPreAuth);
        result_set.set("COMMAND", "PRE_AUTH");
        result_set.set("TRANS_AMOUNT", sTotalPreAuth);
        if (bPreAuthTransRefTroutD)
        {
          // Bug 18129 A new Pre Auth transaction referencing card information from a previous Pre Auth transaction.
          if (previousTROUTD.StartsWith("C")) // IPAY-13 DJD: Use PAYware CTROUTD for follow-on transactions
          {
            result_set.set("REF_CTROUTD", previousTROUTD.Substring(1));
          }
          else
          {
            result_set.set("REF_TROUTD", previousTROUTD);
          }
        }
      }
      else if (bCompletionTrans) // Post-authorization request (COMPLETION comand)
      {
        result_set.set("COMMAND", "COMPLETION");
        result_set.set("TRANS_AMOUNT", sTotal);
        if (previousTROUTD.StartsWith("C")) // IPAY-13 DJD: Use PAYware CTROUTD for follow-on transactions
        {
          result_set.set("CTROUTD", previousTROUTD.Substring(1));
        }
        else
        {
          result_set.set("TROUTD", previousTROUTD);
        }
      }
      else // Sale or Credit
      {
        // Bug# 7410 To process negative amount with IPCharge ANU
        if (sTotal.StartsWith("-")) // COMMAND is CREDIT
        {
          strCOMMAND = "CREDIT";
          result_set.set("COMMAND", "CREDIT");
          result_set.set("TRANS_AMOUNT", sTotal.Replace("-", "")); // BUG#6222
        }
        else // COMMAND is SALE
        {
          strCOMMAND = "SALE";
          result_set.set("COMMAND", "SALE");
          result_set.set("TRANS_AMOUNT", sTotal); // BUG#6222

          // Bug 20746: Partial Authorization Support. Bug 27014 DJD: NOT Allowed for Business Center SALE Transactions
          string strPartialAuth = "0";
          if (misc.getApplicationName() != "cbc")
          {
            strPartialAuth = ((bool)system_interface.get("allow_partial_authorization", false)) ? "1" : "0";
          }
          result_set.set("PARTIAL_AUTH", strPartialAuth);
        }
        // Bug# 7410 end  
      }

      // Bug 18129 - Support for eWallet Token Transactions
      WalletOwner wo = null; // Bug 26819 DJD: eWallet TOKEN EXPIRED - Need to send extend token expiration date call to Payware after each use
      if (eWalletTokenTrans)
      {
        // Get the eWallet owner information and the item
        // Bug 20796 only send eWalletPersonID if system interface is configured to be "Require Token Validation" as Y
        if (validateToken)
          return_value = GetEWalletWithItem(eWalletToken, eWalletHostSystemID, eWalletHostSystemID2, eWalletPersonID, ref eWalletSysInt, ref wo);
        else
          return_value = GetEWalletWithItem(eWalletToken, eWalletHostSystemID, eWalletHostSystemID2, "", ref eWalletSysInt, ref wo);

        if (return_value != null) // Return the CASL Error
        {
          return return_value;
        }

        // Get the eWallet repository for the item
        eWalletRepository = GetRepositoryFromOwnerWithItem(wo);

        switch (eWalletRepository)
        {
          case RepositoryEnum.PAYWARE:
            {
              merchantCustomerID = wo.itemList[0].ext_01;
              merchantContractID = wo.itemList[0].ext_02;
              result_set.set("RBCUSTOMER_ID", merchantCustomerID);
              result_set.set("RBCONTRACT_ID", merchantContractID);
              result_set.set("RBACCOUNT", "P");
              result_set.set("RBEMAIL", "N");
            }
            break;

          case RepositoryEnum.PAYWARE_TOKEN:
            {
              string PAYwareToken = wo.itemList[0].ext_01;
              result_set.set("TKN_PAYMENT", PAYwareToken);

              // Bug 26819 DJD: Renew the token with the sale or preauth
              int tokenRenewalDays = 720;
              if (eWalletSysInt != null)
              {
                object objRenewDays = eWalletSysInt.get("payware_token_renewal_days", 720);
                tokenRenewalDays = objRenewDays is string ? Convert.ToInt32(objRenewDays) : (objRenewDays is int ? (int)objRenewDays : 720);
                if (tokenRenewalDays <= 20)
                {
                  tokenRenewalDays = 720;
                }
              }
              result_set.set("TKN_RENEW", "1");
              result_set.set("TKN_RENEW_DAYS", string.Format("{0}", tokenRenewalDays));
            }
            break;

          case RepositoryEnum.CORE:
            {
              return_value = SetCreditCardDataFromEWallet(eWalletToken, eWalletHostSystemID, eWalletHostSystemID2,
              eWalletPersonID, ref result_set, ref sCc_track_info, ref eWalletSysInt);
            }
            break;

          case RepositoryEnum.None:
            {
              return misc.MakeCASLError("System Interface Error", "EWALLET-003", "Failed to get the eWallet repository for a token transaction.");
            }
        }

        if (return_value != null) // Return the CASL Error
        {
          return return_value;
        }

      }
      else if (eWalletRepository == RepositoryEnum.PAYWARE_TOKEN) // Bug 23150 DJD If this item is to be added to the wallet then generate the PAYware token
      {
        if (adminCommandList.Contains(CardActionEnum.NewCustomerWithContract) || adminCommandList.Contains(CardActionEnum.AddContractToCustomer))
        {
          result_set.set("TKN_PROCESS", "2"); // Create a token with the sale/preauth transaction
        }
      }
      // END Bug 18129 - Expansion of the Payware Integration

      // Bug 17690 UMN see below for explanation of chicken-egg
      result_set.set("INVOICE", "See serial_id");
      result_set.set("COL_3", sCol3);  // tender reference nbr
                                       // End Bug 17690 changes

      result_set.set("BATCH_TRACE_ID", sFile_number);


      if (!bCompletionTrans && !bPreAuthTransRefTroutD && !eWalletTokenTrans) // Bug 18129 - Expansion of the Payware Integration
      {
        if (bIs_swipe)
        {
          // BUG 6211 changed % to @
          if (SecureStringClass.GetDataSourceType("this", sCc_track_info) == "PREH_KEY_TEC")// BUG# 8705 DH
          {
            result_set.set("TRACK_DATA", "pci.SecureStringClass");
          }
          else
          {
            // IPCharge Conditional Parameters
            result_set.set("TRACK_DATA", "pci.SecureStringClass");// BUG# 8705 DH
                                                                  // Bug#7277 Debit card processing ANU
            if (sPin != null && sKey_serial_nbr != null)
            {
              result_set.set("PIN_BLOCK", sPin);
              result_set.set("KEY_SERIAL_NUMBER", sKey_serial_nbr);
            }
          }

          // Bug 26935 MJO - XPI tap/swipe indicator
          if (tender.has("CC_PRESENT_FLAG"))
          {
            result_set.set("PRESENT_FLAG", tender.get("CC_PRESENT_FLAG"));
          }
          else
          {
            // IPCharge Optional Parameters
            result_set.set("PRESENT_FLAG", "3"); //CARD IS SWIPED
          }
        }
        else
        {
          // manually entered data
          // BUG 6211 BLL: change from % to @
          if (sCc_track_info != null && SecureStringClass.GetDataSourceType("this", sCc_track_info) == "PREH_KEY_TEC")// BUG# 8705 DH 
          {
            // THIS IS DEFINATELY -NOT- WORKING!!! Don't even know yet how this might work!
            // code here is just a template of how it might go
            // encrypted, need to decrypt first
            String sDecrypted_data = "";
            String encrypted_cc_info = (string)tender.get("cc_track_info");
            GenericObject encrypted_data = new GenericObject();
            String sEncrypted_data = encrypted_cc_info.Substring(8, encrypted_cc_info.Length - 1);
            encrypted_data.set("encrypted_data", sEncrypted_data);
            sDecrypted_data = Decrypt_preh_key_tec_data(encrypted_data);
            GenericObject parsed_decrypted_data = Parse_decrypted_manual_data(sDecrypted_data);
            result_set.set("CUSTOMER_STREET", parsed_decrypted_data.get("sAddress"));
            result_set.set("CUSTOMER_ZIP", ((string)parsed_decrypted_data.get("sZip"))?.Replace("-", "")); // Bug 20505 NK
            result_set.set("ACCT_NUM", parsed_decrypted_data.get("sCreditCardNbr"));
            // Bug #14191 Mike O - Changed to CVV2
            result_set.set("CVV2", parsed_decrypted_data.get("sCVV2"));
            result_set.set("EXP_MONTH", parsed_decrypted_data.get("sExpMonth"));
            result_set.set("EXP_YEAR", parsed_decrypted_data.get("sExpYear"));
            sDecrypted_data = null;
            parsed_decrypted_data = null;
          }
          else
          {
            //not encrypted, just use what came in on tender
            if (tender.has("address"))
            {
              result_set.set("CUSTOMER_STREET", TrimTo(tender.get("address").ToString(), 20));//BUG#13276 
            }
            if (tender.has("zip"))
            {
              result_set.set("CUSTOMER_ZIP", ((string)tender.get("zip"))?.Replace("-", "")); // Bug 20505 NK
            }

            // Bug 26068 MJO - Don't set ACCT_NUM for token payment
            if (!result_set.has("TKN_PAYMENT"))
              result_set.set("ACCT_NUM", "pci.SecureStringClass"); // BUG# 8705 DH - Placeholder for the real acct.

            // Bug #14191 Mike O - Changed to CVV2
            if (tender.has("ccv"))
            {
              result_set.set("CVV2", tender.get("ccv"));
            }
            // Bug 26104 MJO - Make sure they're not false
            if (tender.has("exp_month") && tender.get("exp_month").GetType() != typeof(bool))
            {
              result_set.set("EXP_MONTH", tender.get("exp_month"));
            }
            if (tender.has("exp_year") && tender.get("exp_year").GetType() != typeof(bool))
            {
              result_set.set("EXP_YEAR", tender.get("exp_year"));
            }
          }

          // BEGIN Bug 21932 DJD - Use PRESENT_FLAG=1 To Indicate Card Not Present
          const string CARD_NOT_PRESENT = "1", CARD_PRESENT = "2"; // Default to card present
                                                                   // Bug 27008 DJD: ALWAYS set Business Center transactions to "card not present" regardless of workgroup/tender config
          string presentFlag = CARD_NOT_PRESENT;
          bool isBusinessCenter = (misc.getApplicationName() == "cbc");
          if (!isBusinessCenter)
          {
            presentFlag = tender.has("CC_PRESENT_FLAG") ? (string)tender.get("CC_PRESENT_FLAG", CARD_PRESENT) : (string)this_dept.get("CC_PRESENT_FLAG", CARD_PRESENT);
          }
          // Manually entered card is "present" or "not present" based on Business Center app, workgroup, or tender value
          result_set.set("PRESENT_FLAG", (presentFlag == CARD_NOT_PRESENT || presentFlag == CARD_PRESENT) ? presentFlag : CARD_PRESENT);
          // END   Bug 21932 DJD - Use PRESENT_FLAG=1 To Indicate Card Not Present
        }
        //IPCharge Conditional Parameters
        // Bug #13605 Mike O - First name is optional
        // BEGIN Bug 18129 - Allow "payer_lname" to be overridden as optional for manual entry
        string fname = SecurityElement.Escape((string)tender.get("payer_fname", ""));
        string lname = SecurityElement.Escape((string)tender.get("payer_lname", "")); // SecurityElement.Escape
        string sCH = string.Format("{0}{1}{2}"
        , lname, ((!string.IsNullOrEmpty(lname) && !string.IsNullOrEmpty(fname)) ? "&#44; " : ""), fname);
        if (!string.IsNullOrEmpty(sCH.Trim()))
        {
          result_set.set("CARDHOLDER", sCH);
        }
        // END Bug 18129 - Allow "payer_lname" to be overridden as optional for manual entry
      }

      // Bug 25482 MJO - Remove unnecessary fields for token request [Bug 26010 DJD Added null check]
      if (sCc_track_info != null && SecureStringClass.GetDataSourceType("this", sCc_track_info) == "POINT_TOKEN")
      {
        result_set.set("TKN_PAYMENT", "pci.SecureStringClass");
        result_set.remove("CUSTOMER_STREET", false);
        result_set.remove("CUSTOMER_ZIP", false);
        result_set.remove("ACCT_NUM", false);
        result_set.remove("CVV2", false);

        if (tender.get("entry_method", "").ToString() != "Keyed")
          result_set.set("CARDHOLDER", tender.get("payer_name"));
        else
          result_set.remove("CARDHOLDER");
      }

      // Bug #14288 Mike O - Use DEVICEKEY instead of MERCHANTKEY for VeriFone
      // Bug 18129 - Expansion of the Payware Integration (added not null condition)
      if (sCc_track_info != null && SecureStringClass.GetDataSourceType("this", sCc_track_info).Contains("VERIFONE"))
      {
        result_set.set("DEVICEKEY", device_key);
        tender.set("ewallet_DEVICEKEY", device_key); // Bug 23150 DJD Save value for eWallet Update S.I.
      }
      else
      {
        result_set.set("MERCHANTKEY", paywareAuth.merchantKey); // Bug 21009 [Bug 18129] eWallet Support
                                                                // Bug 16611 MJO - Don't send USER_ID or USER_PW with VeriFone
        result_set.set("USER_PW", paywareAuth.userPW); // Bug 21009 [Bug 18129] eWallet Support
        result_set.set("USER_ID", paywareAuth.userID); // Bug 21009 [Bug 18129] eWallet Support
      }
      result_set.set("CLIENT_ID", paywareAuth.clientID); // Bug 18912 Added HOST_REFERENCE_2 to store MerchantKey value
      tender.set("ewallet_CLIENT_ID", paywareAuth.clientID); // Bug 23150 DJD Save value for eWallet Update S.I.

      // Bug #14288 Mike O - VeriFone device-specific arguments
      // Bug 18129 - Expansion of the Payware Integration (added not null condition)
      if (sCc_track_info != null && SecureStringClass.GetDataSourceType("this", sCc_track_info).Contains("VERIFONE"))
      {
        if (device_name != null)
        {
          result_set.set("DEVTYPE", device_name);
          tender.set("ewallet_DEVTYPE", device_name); // Bug 23150 DJD Save value for eWallet Update S.I.
        }
        if (serial_nbr != null)
        {
          result_set.set("SERIAL_NUM", serial_nbr);
          tender.set("ewallet_SERIAL_NUM", serial_nbr); // Bug 23150 DJD Save value for eWallet Update S.I.
        }
      }
      // Get the Healthcare Amounts [FSA HRA Card Processing: Healthcare type support for FSA]
      GenericObject geoHCAmts = args.get("healthcare_amounts", null) as GenericObject;

      // Bug IPAY-766 RDM - Don't send healthcare data if doing completion (healthcare stuff happens with the preauth)
      if (!bCompletionTrans && geoHCAmts != null)
      {
        //bool bFSAHRA_Card = true;

        if (geoHCAmts.has("HEALTHCARE"))
        {
          theObj = geoHCAmts.get("HEALTHCARE");
          decimal dTotalHealthcare = theObj is decimal ? (decimal)theObj : 0.00M;
          result_set.set("AMOUNT_HEALTHCARE", string.Format("{0:F2}", Math.Abs(dTotalHealthcare)));
        }
        if (geoHCAmts.has("PRESCRIPTION"))
        {
          theObj = geoHCAmts.get("PRESCRIPTION");
          decimal dTotalPrescription = theObj is decimal ? (decimal)theObj : 0.00M;
          result_set.set("AMOUNT_PRESCRIPTION", string.Format("{0:F2}", Math.Abs(dTotalPrescription)));
        }
        if (geoHCAmts.has("VISION"))
        {
          theObj = geoHCAmts.get("VISION");
          decimal dTotalVision = theObj is decimal ? (decimal)theObj : 0.00M;
          result_set.set("AMOUNT_VISION", string.Format("{0:F2}", Math.Abs(dTotalVision)));
        }
        if (geoHCAmts.has("CLINIC"))
        {
          theObj = geoHCAmts.get("CLINIC");
          decimal dTotalClinic = theObj is decimal ? (decimal)theObj : 0.00M;
          result_set.set("AMOUNT_CLINIC", string.Format("{0:F2}", Math.Abs(dTotalClinic)));
        }
        if (geoHCAmts.has("DENTAL"))
        {
          theObj = geoHCAmts.get("DENTAL");
          decimal dTotalDental = theObj is decimal ? (decimal)theObj : 0.00M;
          result_set.set("AMOUNT_DENTAL", string.Format("{0:F2}", Math.Abs(dTotalDental)));
        }

        //BUG#14175
        if (strCOMMAND == "SALE")
        {
          // Bug 14175: The Partial Auth value sent should be changed from Y/N to 1/0:
          string strPartialAuth = ((bool)system_interface.get("allow_partial_authorization", true)) ? "1" : "0";
          result_set.set("PARTIAL_AUTH", strPartialAuth);
        }
      }

      // BEGIN Bug 18129 DJD - Support for eWallet ADMIN Functions (e.g., Creating or Updating eWallet Items)
      // NOTE: ONLY perform this now if it is a ZERO dollar transaction [i.e., NOT combined with a CC SALE OR CC PREAUTH (Bug 26719 DJD)]
      eWalletAdminTransAfterCardTrans = (eWalletAdminTrans && (dTotal != 0.00M || dTotalPreAuth != 0.00M));
      if (eWalletAdminTrans && !eWalletAdminTransAfterCardTrans)
      {
        try
        {
          PerformAdminTransactions(tender, paywareAuth, adminCommandList, eWalletHostSystemID, eWalletPersonID, ref merchantCustomerID,
          ref merchantContractID, adminHost, host, timeout, sCc_track_info, "", eWalletRepository, ref system_interface,
          ref eWalletSysInt, eWalletOwnerDemograhics, ref return_value);
        }
        catch (Exception ex)
        {
          errMsg = string.Format("Error Performing Admin Transactions:{0}{1}", Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
          Logger.cs_log(errMsg);
          return_value = misc.MakeCASLError("System Interface Error", "PWADMIN-002", string.Format("{0}", errMsg), true);
        }
        // If there is no "follow-up" card trans after the Admin trans (or Error occurred or zero dollar tender) then return
        if (!eWalletTokenTrans || (return_value != null) || (dTotal == 0.00M))
        {
          return return_value;
        }
      }
      // END Bug 18129 DJD - Support for eWallet ADMIN Functions (e.g., Creating or Updating eWallet Items)

      String sIPCharge_request_xml;
      GenericObject build_args;
      BuildIPChargeRequest(host, timeout, sCc_track_info, result_set, this_dept, out sIPCharge_request_xml, out build_args); // Bug 22823 DJD [Added workgroup]

      // Bug 15417 MJO - Pass in merchant key
      // Bug 16008 MJO - Catch exceptions writing to the tracking table and fail out (can't proceed without a STARTED record)
      try
      {
        // Bug 17690 UMN: We have a chicken-egg problem here. The invoice number is set from the _serial_id. But we
        // don't get that until after start_request is called. Yet we must log the request in comments in the start 
        // request call, so we need the invoice number! So for now, log the incorrect number on the start_request. 
        // The actual value will be updated when the request completes.

        // Bug #13644 Mike O - Insert new request into system interface tracker
        string maskedXML = (string)misc.CASL_call("_subject", c_CASL.execute("System_interface.IPCharge"), "a_method", "mask_sensitive_fields",
        "args", new GenericObject("xml", sIPCharge_request_xml));

        misc.CASL_call("_subject", c_CASL.execute("System_interface_tracking"), "a_method", "start_request", "args", new GenericObject("a_core_item", tender,
        "request_xml", maskedXML, "action", result_set.get("COMMAND"), "system", system_interface, "merchant_key", paywareAuth.merchantKey)); // Bug 18912 Added HOST_REFERENCE_2 to store MerchantKey value

        // Bug 17690 UMN limit invoice nbr to 6-digits per VeriFone recommendation. 
        // Must do after start_request, which is what sets _serial_id
        string sInvoice_nbr = ((int)tender.get("_serial_id")).ToString();
        if (sInvoice_nbr.Length > 6) sInvoice_nbr = sInvoice_nbr.Substring(sInvoice_nbr.Length - 6);
        result_set.set("INVOICE", sInvoice_nbr);

        // rebuild the request object with the changed invoice number
        BuildIPChargeRequest(host, timeout, sCc_track_info, result_set, this_dept, out sIPCharge_request_xml, out build_args); // Bug 22823 DJD [Added workgroup]
      }
      catch (CASL_error e)
      {
        Logger.cs_log("Failed to write to TG_SYSTEM_INTERFACE_TRACKING: " + e.casl_object.get("message"));
        return misc.MakeCASLError("System interface tracking error", "200", "Failed to write tracking record.");
      }

      // log ipcharge request xml in c# than casl log BUG#14135
      bool _bLogRequest = (bool)system_interface.get("log_request", true, true);
      if (_bLogRequest)
      {
        string transInfo = ""; // Bug 18129 - If transaction is based on a TroutD then exclude the card nbr info (unavailable)
        if (!bCompletionTrans && !bPreAuthTransRefTroutD) // Bug 18129
        {
          // LOG the REQUEST:
          string CardNbr = (string)tender.get("credit_card_nbr", "");

          // Bug 15958 MJO - Always prefer the card number from the SecureStringClass
          // Bug 12570: ATM Debit Card Nbr Not Recorded In Log File
          if (sCc_track_info != null)
          {
            // Bug 20186 - DH Masked value cannot be taken from the end of a full track data.
            CardNbr = SecureStringClass.GetMaskedCreditCardNumber("this", sCc_track_info);
          }
          CardNbr = CardNbr.Length > 4 ? CardNbr.Substring(CardNbr.Length - 4) : CardNbr;
          transInfo = " [Credit Card Nbr: " + CardNbr + "]"; // Bug 18129
        }

        string sLogMsg = string.Format("Calling Payware CC Gateway {0}", transInfo); // Bug 18129 - Expansion of the Payware Integration (added not null condition)

        string sRespXML = sIPCharge_request_xml.Trim();

        // Bug #14574 Mike O - Changed to CVV2, so mask that instead
        // Bug 17690 UMN don't mask CLIENT_ID per FAE request
        // Bug 20179 MJO - Added DEVICEKEY
        // Bug 25482 MJO - Token support
        string[] tags = { "MERCHANTKEY", "USER_ID", "USER_PW", "ACCT_NUM", "CVV2", "EXP_YEAR", "EXP_MONTH", "TRACK_DATA", "PIN_BLOCK", "KEY_SERIAL_NUMBER", "DEVICEKEY", "TKN_PAYMENT" };// BUG#14877 mask PIN_BLOCK

#if !DEBUG
        MaskTags(ref sRespXML, tags);
#endif

        // Format XML so it is readable:
        string sFormatted = FormatXml(sRespXML);
        if (sFormatted != "") { sRespXML = sFormatted; }
        Logger.LogInfo(String.Format("CC Info={0}, IPCharge Request={1}", sLogMsg, sFormatted), "IPCharge Request");

        // Bug 18732
        try
        {
          string details = TranSuiteServices.ActivityLogUtils.parseXMLSetDetails(sFormatted);
          misc.CASL_call("a_method", "actlog", "args",
          new GenericObject(
          // Bug 20435 UMN PCI-DSS don't pass session
          "user", userid,
          "app", file_source_type,
          "action", "IP Charge",
          "summary", "Request",
          "detail", details));
        }
        catch (Exception e)
        {
          Logger.cs_log("IPCharge_build_send_receive: cannot write to the activity log: " + e.ToString());
        }
        // End Bug 18732
      }

      // Bug#7306 To redirect to alternate IPCHARGE Host ANU 
      try
      {
        return_value = Build_and_send_http_request(build_args) as GenericObject;
        if (timeout == 987654321) // QA sentinal to throw a timeout exception
        {
          throw new WebException("QA timeout", WebExceptionStatus.Timeout);
        }
      }
      ///// Begin Bug 16285 UMN duplicate IPCHARGE handling
      catch (WebException e)
      {
        try
        {
          // Bug 19256 DJD: ALWAYS log the error:
          string msg = e.ToMessageAndCompleteStacktrace();
          Logger.cs_log(string.Format("IPCharge Error:{0}{1}", Environment.NewLine, msg));
          switch (e.Status)
          {
            case WebExceptionStatus.ConnectionClosed:
            case WebExceptionStatus.NameResolutionFailure:
            case WebExceptionStatus.ProxyNameResolutionFailure:
            case WebExceptionStatus.Timeout:
            case WebExceptionStatus.ProtocolError: // Bug 17622 MJO - Added for 404 errors
              Logger.cs_log("IPCharge: Calling althost #1 using URL: " + althost1);
              Logger.LogInfo(String.Format("IPCharge: Calling althost #1 using URL: {0}", althost1), "IPCharge Request");

              // Bug 16285 UMN: primarily for testing, to make sure 1st response has time to process, so 2nd becomes a duplicate
              // Avoid a possible context switch by not calling if pauseN is 0
              if (pause1 > 0) Thread.Sleep(pause1);

              build_args.set("uri_string", althost1);
              build_args.set("timeout", timeout1); // Bug #13896 Mike O - Configurable timeout
              return_value = SendRequestHandlingDuplicate(build_args);
              break;

            default:
              // Bug #8131 Mike O - Replaced actual error message with generic message
              return_value = misc.MakeCASLError("Request Failed", "590", "A connection error occurred.");
              break;
          }
        }
        catch (WebException ce)
        {
          switch (e.Status)
          {
            case WebExceptionStatus.ConnectionClosed:
            case WebExceptionStatus.NameResolutionFailure:
            case WebExceptionStatus.ProxyNameResolutionFailure:
            case WebExceptionStatus.Timeout:
            case WebExceptionStatus.ProtocolError: // Bug 17622 MJO - Added for 404 errors
              Logger.cs_log("IPCharge Error: " + ce.Message);
              Logger.cs_log("IPCharge: Calling althost #2 using URL: " + althost2);
              Logger.LogInfo(String.Format("IPCharge: Calling althost #2 using URL: {0}", althost2), "IPCharge Request");

              // Bug 16285 UMN: primarily for testing, to make sure 1st response has time to process, so 2nd becomes a duplicate
              // Avoid a possible context switch by not calling if pauseN is 0
              if (pause2 > 0) Thread.Sleep(pause2);

              build_args.set("timeout", timeout2); // Bug #13896 Mike O - Configurable timeout
              build_args.set("uri_string", althost2);
              // Bug #8131 Mike O - Catch errors thrown by althost2 connection attempt
              try
              {
                return_value = SendRequestHandlingDuplicate(build_args);
              }
              // Bug 16370 MJO - Mark timeout errors
              catch (WebException ex)
              {
                Logger.cs_log("IPCharge Error: " + ex.Message);
                return_value = misc.MakeCASLError("Request Failed", "590", "A connection error occurred.");

                if (ex.Status == WebExceptionStatus.Timeout)
                  return_value.set("is_timeout", true);
              }
              catch (Exception ex)
              {
                Logger.cs_log("IPCharge Error: " + ex.Message);
                return_value = misc.MakeCASLError("Request Failed", "590", "A connection error occurred.");
              }
              // End Bug #8131
              break;

            default:
              // Bug #8131 Mike O - Replaced actual error message with generic message
              return_value = misc.MakeCASLError("Request Failed", "590", "A connection error occurred.");
              break;
          }
        }
        catch (Exception ex)
        {
          Logger.cs_log("IPCharge Error: " + ex.Message);
          // Bug #8131 Mike O - Replaced actual error message with generic message
          return_value = misc.MakeCASLError("Request Failed", "590", "A connection error occurred.");
        }
      }
      catch (Exception ex)
      {
        Logger.cs_log("IPCharge Error: " + ex.Message);
        return_value = misc.MakeCASLError("Request Failed", "590", "A connection error occurred.");
      }
      ///// END Bug 16285 UMN duplicate IPCHARGE handling

      // Bug#7306 ANU end 

      // Bug #13644 Mike O - Update the tracking table
      if (return_value != null)
      {
        if (return_value.isError())
        {
          // Bug 16370 MJO - Added request failure and timeout statuses
          misc.CASL_call("_subject", c_CASL.execute("System_interface_tracking"), "a_method", "update", "args", new GenericObject("a_core_item", tender,
          "status", c_CASL.execute((bool)return_value.get("is_timeout", false) ? "System_interface_tracking.RequestTimeout" : "System_interface_tracking.RequestFailure"),
          "system", system_interface));
        }
        else
        {
          // Bug 24215 Manual CC support for Core Direct
          string unmaskedXML = (string)return_value.get("body", "");
          string receiptNbr = (string)core_event.get("receipt_nbr", "2000245001-1");  // Sentinel value from year 2000

          string maskedXML = (string)misc.CASL_call("_subject", c_CASL.execute("System_interface.IPCharge"), "a_method", "mask_sensitive_fields",
          "args", new GenericObject("xml", (string)return_value.get("body", "")));

          GenericObject result = (GenericObject)misc.CASL_call("_subject", maskedXML, "a_method", "make_inst_from_xml",
          "args", new GenericObject("namespace", c_CASL.execute("IPCharge_physical_model")));

          // BEGIN Bug 18129 - Expansion of the Payware Integration
          string sResult = ((string)result.get("RESULT", "")).Trim().ToUpper();
          var listApprovedResults = new List<string> { "CAPTURED", "APPROVED" };
          bool bBankApproved = listApprovedResults.Contains(sResult);

          // If approved, set eWallet and PAYware Token items from response to the tender
          if (bBankApproved)
          {
            string errorMsg = "";
            Dictionary<string, string> dicResp = null;
            if (PAYwareConnectTrans.RespToDic(ref maskedXML, ref dicResp, ref errorMsg)) // Set PAYware Connect response XML key-value pairs to dictionary
            {
              if (dicResp.ContainsKey("TKN_PAYMENT"))
              {
                eWalletPAYwareToken = dicResp["TKN_PAYMENT"];
                tender.set("PAYwareToken", eWalletPAYwareToken);
              }

              string PAYwareTokenExpDate = ""; // Bug 26819 DJD: eWallet TOKEN EXPIRED - Need to send extend token expiration date call to Payware after each use
              if (dicResp.ContainsKey("TKN_EXPDATE"))
              {
                PAYwareTokenExpDate = dicResp["TKN_EXPDATE"];
                tender.set("PAYwareTokenExpDate", PAYwareTokenExpDate);
              }

              // Get data that will identify the card item being added
              string MaskedCardNumber = "";
              string ExpirationYear = "";
              string ExpirationMonth = "";
              DateTime? ExpirationDate = null;
              GetMaskedNbrAndExpDate(tender, sCc_track_info, ref MaskedCardNumber, ref ExpirationYear, ref ExpirationMonth, ref ExpirationDate);

              if (!string.IsNullOrWhiteSpace(MaskedCardNumber))
              {
                tender.set("MaskedCardNumber", MaskedCardNumber);
              }

              if (ExpirationDate.HasValue)
              {
                const string FMT = "O";
                tender.set("ExpirationDate", ((DateTime)ExpirationDate).ToString(FMT));
              }

              // Bug 26819 DJD: Update the eWallet item for a Token Transaction [Sale or Preauth]
              if (eWalletTokenTrans && wo != null && eWalletSysInt != null)
              {
                UpdateWalletItem(tender, eWalletSysInt, wo, PAYwareTokenExpDate, ExpirationDate);
              }

              if (!string.IsNullOrWhiteSpace(errorMsg))
              {
                Logger.cs_log(errorMsg);
              }
            }
          }
          else // Result was NOT approved [probably declined]
          {
            if (eWalletAdminTransAfterCardTrans)
            {
              Logger.cs_log(string.Format("Card transaction was not successful; unable to perform eWallet Transaction for Person ID: {0}.", eWalletPersonID));
              eWalletAdminTransAfterCardTrans = false; // Should only be performed AFTER a successful card trans
            }
          }
          // END Bug 21009 [Bug 18129] - Expansion of the Payware Integration

          if (result.isError())
          {
            // Bug 16370 MJO - Added request failure and timeout statuses
            misc.CASL_call("_subject", c_CASL.execute("System_interface_tracking"), "a_method", "update", "args", new GenericObject("a_core_item", tender,
            "status", c_CASL.execute((bool)return_value.get("is_timeout", false) ? "System_interface_tracking.RequestTimeout" : "System_interface_tracking.RequestFailure"),
            "response", maskedXML, "system", system_interface));
          }
          else
          {
            // IPAY-13 DJD: Use PAYware CTROUTD for follow-on transactions [IPAY-1182: Do not record "C" by itself]
            string cTROUTD = string.IsNullOrWhiteSpace((string)result.get("CTROUTD", "")) ? "" : string.Format("C{0}", (string)result.get("CTROUTD", "")); // Bug 18129
            if (result.get("TERMINATION_STATUS", "").Equals("SUCCESS") && bBankApproved) // Bug 18129
            {
              tender.set("PAYwareTranID", cTROUTD); // Used for another PRE_AUTH (i.e., re-auth) or a COMPLETION "Follow On" transaction performed after a pre auth. // Bug 18129

              // Bug 15502 UMN set entry mode based on the strDataType
              // sCc_track_info != null && SecureStringClass.GetDataSourceType("this", sCc_track_info)
              SetCardEntryMethod(tender, sCc_track_info);
              misc.CASL_call("_subject", c_CASL.execute("System_interface_tracking"), "a_method", "update", "args", new GenericObject("a_core_item", tender,
              // Bug IPAY-521 MJO - Store the entry method in the comments (DJD added default to get entry_method)
              "status", c_CASL.execute("System_interface_tracking.PostedSuccess"), "reference", cTROUTD, "response", maskedXML + "|" + tender.get<string>("entry_method", ""), "system", system_interface));
            }
            else
            {
              misc.CASL_call("_subject", c_CASL.c_object("System_interface_tracking"), "a_method", "update", "args", new GenericObject("a_core_item", tender,
              "status", c_CASL.execute("System_interface_tracking.PostedFailure"), "reference", cTROUTD, "response", maskedXML, "system", system_interface));
            }
          }

          // Bug 24215 UMN - Store the response for CoreDirect
          PointCache.CachePointResponse(receiptNbr, unmaskedXML);
        }
      }

      // Bug 18912 BEGIN DJD Support for eWallet ADMIN Functions performed AFTER successful card trans
      if (eWalletAdminTransAfterCardTrans)
      {
        bool hasWalletUpdateSysInt = hasWalletUpdateSI(tender);
        bool gatewaySavePaymentMethod = tender.has("gateway_save_payment_method"); // Bug 26719 DJD Update eWallet and Gateway Processing
        if (!hasWalletUpdateSysInt || gatewaySavePaymentMethod) // Bug 26719 DJD Update eWallet and Gateway Processing
        {
          GenericObject retVal = null;
          try
          {
            PerformAdminTransactions(tender, paywareAuth, adminCommandList, eWalletHostSystemID, eWalletPersonID, ref merchantCustomerID,
            ref merchantContractID, adminHost, host, timeout, sCc_track_info, eWalletPAYwareToken, eWalletRepository, ref system_interface,
            ref eWalletSysInt, eWalletOwnerDemograhics, ref retVal);
          }
          catch (Exception ex)
          {
            errMsg = string.Format("Error Performing Admin Transactions:{0}{1}", Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
            Logger.cs_log(errMsg);
          }
        }
      }
      // Bug 21009 [Bug 18129] END DJD - Support for eWallet ADMIN Functions

      // clear out these variables!! (actually does nothing-- GC will take its time)
      result_set = null;
      sIPCharge_request_xml = null;

      return return_value;
    }

    // Bug 26819 DJD: eWallet TOKEN EXPIRED - Need to send extend token expiration date call to Payware after each use
    private static bool UpdateWalletItem(GenericObject tender, GenericObject eWalletSysInt, WalletOwner wo, string PAYwareTokenExpDate, DateTime? ExpirationDate)
    {
      string errMsg = "";
      bool successModifyItem = false;
      WalletDBServiceClient client = null;
      try
      {
        WalletItem wi = wo.itemList[0];
        client = ConstructEWalletClient(eWalletSysInt, ref errMsg);
        if (client != null)
        {
          WalletResponse wr = null;

          string itemName = tender.get("ewallet_tender_nickname", "") as string;
          if (!string.IsNullOrWhiteSpace(itemName))
          {
            wi.itemName = itemName;
          }

          string itemShareName = tender.get("ewallet_share_item_with", "") as string;
          if (!string.IsNullOrWhiteSpace(itemShareName))
          {
            wi.ext_10 = itemShareName;
          }

          if (ExpirationDate.HasValue)
          {
            wi.expirationDate = ExpirationDate;
          }

          if (!string.IsNullOrWhiteSpace(PAYwareTokenExpDate))
          {
            wi.ext_02 = PAYwareTokenExpDate; // Set PAYware Token Exp Date (PAYWARE_TOKEN) to ext_02
          }

          string itemID = client.ModifyWalletItem(wi.itemID, wi, out wr);

          successModifyItem = wr.success;
          if (successModifyItem)
          {
            string maskedToken = MaskInput(itemID, 4);
            Logger.cs_log(string.Format("SUCCESS: eWallet Item used in Transaction [ID: {0}] modified.", maskedToken));
          }
          else // Log error
          {
            errMsg = string.Format("FAILURE: eWallet item used in Transaction NOT modified for eWallet Owner [{2}]:{0}{1}", Environment.NewLine, wr.detailedErrorMsg, wo.ownerID);
            Logger.cs_log(errMsg);
          }
        }
      }
      catch (Exception ex)
      {
        errMsg = string.Format("Error modifying eWallet item used in Transaction for [Owner ID {0}]:{1}{2}", wo.ownerID, Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
        Logger.cs_log(errMsg);
      }
      finally // Close the client
      {
        if (client != null)
        {
          client.Close();
        }
      }
      return successModifyItem;
    }

    private static bool hasWalletUpdateSI(GenericObject tender)
    {
      bool hasWalletUpdateSysInt = false;
      GenericObject sysInts = tender.get("system_interfaces", null, true) as GenericObject;
      if (sysInts != null)
      {
        GenericObject walletUpdateSysInt = GetConfiguredSystemInterface("eWalletUpdate");
        if (walletUpdateSysInt != null && sysInts.has(walletUpdateSysInt))
        {
          hasWalletUpdateSysInt = true;
        }
      }
      return hasWalletUpdateSysInt;
    }

    private static void SetCardEntryMethod(GenericObject tender, SecureStringClass sCc_track_info)
    {
      if (sCc_track_info != null)
      {
        string sourceType = SecureStringClass.GetDataSourceType("this", sCc_track_info);
        switch (sourceType)
        {
          case "MAG_READER":
          case "INGENICO":
            tender.set("entry_method", "Basic Swipe"); // Bug 15502 MJO - Change to Basic (unencrypted swipe)
            break;

          case "MAGTEK_MANUAL":
          case "PREH_KEY_TEC_MANUAL":
          case "VERIFONE_MANUAL":
            tender.set("entry_method", "Keyed");
            break;

          case "MAGTEK":
          case "PREH_KEY_TEC":
            tender.set("entry_method", "Swiped");
            break;

          case "EWALLET_TOKEN":
            tender.set("entry_method", "Wallet");
            break;
        }
      }
    }

    /// <summary>
    /// Bug 21009 [Bug 18129] DJD - Gets the eWallet Person ID and Host ID from a 
    /// transaction (i.e., it may be a container transaction with nested block 
    /// transactions).  Host ID will default to "iPayment" if not found.
    /// </summary>
    /// <param name="tran"></param>
    /// <param name="personID"></param>
    /// <param name="hostID"></param>
    /// <returns></returns>
    private static bool GetPersonIDAndHostIDFromTrans(GenericObject tran, ref string personID, ref string hostID)
    {
      bool PersonIDAndHostIDFound = GetPersonIDAndHostID(tran, ref personID, ref hostID);
      // Check the nested trans (BTTs)
      if (!PersonIDAndHostIDFound && tran.has("core_items"))
      {
        GenericObject nestedTransactions = tran.get("core_items") as GenericObject;
        int nestedTranCount = nestedTransactions.getIntKeyLength();
        for (int k = 0; k < nestedTranCount; k++)
        {
          GenericObject nestedTran = nestedTransactions.get(k) as GenericObject;
          PersonIDAndHostIDFound = GetPersonIDAndHostID(nestedTran, ref personID, ref hostID);
          if (PersonIDAndHostIDFound)
          {
            break;
          }
        }
      }
      return PersonIDAndHostIDFound;
    }

    /// <summary>
    /// Bug 21009 [Bug 18129] DJD - Gets the eWallet Person ID and Host ID from a single transaction 
    /// (i.e., not a container with blocks).  Host ID will default to "iPayment" if not found.
    /// </summary>
    /// <param name="tran"></param>
    /// <param name="personIDs"></param>
    /// <param name="hostID"></param>
    /// <returns></returns>
    private static bool GetPersonIDAndHostID(GenericObject tran, ref string personIDs, ref string hostID)
    {
      bool foundWalletOwnerAndHostInTrans = false;
      string hostIDTag = tran.get("host_system_ID_tagname", "", true) as string;
      string personIDTags = tran.get("person_ID_tagname", "", true) as string;
      if (!string.IsNullOrEmpty(personIDTags))
      {
        hostID = string.IsNullOrEmpty(hostIDTag) ? EW_HOST_REFERENCE_DEFAULT : (tran.get(hostIDTag, EW_HOST_REFERENCE_DEFAULT) as string);
        personIDs = "";

        // Allow Person ID Tags to be a comma-delimited list of tags (i.e., more than 1 data element makes up the ID)
        bool personIDValuesFound = true;
        string missingPersonIDTag = "";
        string[] personIDTagArray = personIDTags.Split(',');
        int j = 0;
        foreach (string personIDTag in personIDTagArray)
        {
          j++;
          string personID = tran.get(personIDTag, "") as string;
          if (string.IsNullOrEmpty(personID))
          {
            personIDValuesFound = false;
            missingPersonIDTag = personIDTag;
            break;
          }
          personIDs += string.Format("{0}{1}", personID, personIDTagArray.Length == j ? "" : ",");
        }
        foundWalletOwnerAndHostInTrans = personIDValuesFound;
      }
      return foundWalletOwnerAndHostInTrans;
    }

    /// <summary>
    /// Bug 21009 [Bug 18129] DJD - Gets the CORE Repository enum from a wallet owner with an item
    /// </summary>
    /// <param name="wo"></param>
    /// <returns></returns>
    private static RepositoryEnum GetRepositoryFromOwnerWithItem(WalletOwner wo)
    {
      RepositoryEnum eWalletRepository = RepositoryEnum.None;
      try
      {
        if (wo.itemList.Count() > 0)
        {
          WalletItem wi = wo.itemList[0];
          eWalletRepository = GetRepositoryFromItem(wi);
        }
      }
      catch (Exception ex)
      {
        Logger.cs_log(string.Format("Error Getting eWallet Repository:{0}{1}", Environment.NewLine, ex.ToMessageAndCompleteStacktrace()));
        eWalletRepository = RepositoryEnum.None;
      }
      return eWalletRepository;
    }

    /// <summary>
    /// Bug 21009 [Bug 18129] DJD - Gets the CORE Repository enum from a wallet item
    /// </summary>
    /// <param name="wi"></param>
    /// <returns></returns>
    private static RepositoryEnum GetRepositoryFromItem(WalletItem wi)
    {
      RepositoryEnum eWalletRepository = RepositoryEnum.None;
      string rep = wi.repositoryID;
      if (!string.IsNullOrEmpty(rep))
      {
        eWalletRepository = (RepositoryEnum)Enum.Parse(typeof(RepositoryEnum), rep, true);
        if (!Enum.IsDefined(typeof(RepositoryEnum), eWalletRepository))
        {
          Logger.cs_log(string.Format("{0} is not an underlying value of the RepositoryEnum enumeration.", eWalletRepository));
          eWalletRepository = RepositoryEnum.None;
        }
      }
      return eWalletRepository;
    }

    /// <summary>
    /// Bug 18129 DJD - Gets Preauth items
    /// Populates the variables related to PreAuth and Completion transactions.
    /// </summary>
    /// <param name="args"></param>
    /// <param name="tender"></param>
    /// <param name="bPreAuthTrans"></param>
    /// <param name="bCompletionTrans"></param>
    /// <param name="bPreAuthTransRefTroutD"></param>
    /// <param name="dTotalPreAuth"></param>
    /// <param name="previousTROUTD"></param>
    private static void GetPreAuthRelatedItems(GenericObject args, GenericObject tender, ref bool bPreAuthTrans, ref bool bCompletionTrans, ref bool bPreAuthTransRefTroutD, ref decimal dTotalPreAuth, ref string previousTROUTD)
    {
      try
      {
        object objTotalPreAuth = tender.get("preauth_total", 0.00M);
        dTotalPreAuth = (objTotalPreAuth is string && string.IsNullOrEmpty(objTotalPreAuth as string)) ?
        0.00M : Convert.ToDecimal(objTotalPreAuth);
      }
      catch
      {
        dTotalPreAuth = 0.00M;
      }
      bPreAuthTrans = dTotalPreAuth > 0.00M;
      GenericObject previous_tender = args.get("previous_tender", null) as GenericObject;
      previousTROUTD = previous_tender == null ? tender.get("PAYwarePreviousTranID", "") as string : previous_tender.get("auth_str", "") as string;
      if (!string.IsNullOrEmpty(previousTROUTD))
      {
        if (bPreAuthTrans)
        {
          bPreAuthTransRefTroutD = true;
          bPreAuthTrans = false;
        }
        else
        {
          bCompletionTrans = true;
        }
      }
    }

    /// <summary>
    /// Bug 18129 DJD - Gets Admin Commands
    /// Populates the "adminCommandList" with the eWallet commnds.
    /// PRE: adminCommandList is instantiated.
    /// </summary>
    /// <param name="tender"></param>
    /// <param name="adminCommandList"></param>
    /// <param name="err"></param>
    /// <returns></returns>
    public static string GetEWalletAdminCommands(GenericObject tender, List<CardActionEnum> adminCommandList, ref string err)
    {
      EWalletDemographics ewd = null;
      string acString = GetEWalletAdminCommands(tender, null, ref ewd, adminCommandList, ref err);
      return acString;
    }

    /// <summary>
    /// Bug 26575 MJO - Added new overload
    /// Bug 18129 DJD - Gets Admin Commands
    /// Populates the "adminCommandList" with the eWallet commnds.
    /// Inspects tender and zero-dollar transaction for "add_item_to_ewallet" checkbox
    /// PRE: adminCommandList is instantiated.
    /// </summary>
    /// <param name="tender"></param>
    /// <param name="coreEvent"></param>
    /// <param name="adminCommandList"></param>
    /// <param name="err"></param>
    /// <returns></returns>
    public static string GetEWalletAdminCommands(GenericObject tender, GenericObject coreEvent, List<CardActionEnum> adminCommandList, ref string err)
    {
      EWalletDemographics ewd = null;
      string acString = GetEWalletAdminCommands(tender, coreEvent, ref ewd, adminCommandList, ref err);
      return acString;
    }

    /// <summary>
    /// Bug 18129 DJD - Gets Admin Commands
    /// Populates the "adminCommandList" with the eWallet commnds.
    /// Inspects tender and zero-dollar transaction for "add_item_to_ewallet" checkbox
    /// PRE: adminCommandList is instantiated.
    /// </summary>
    /// <param name="tender"></param>
    /// <param name="coreEvent"></param>
    /// <param name="adminCommandList"></param>
    /// <param name="err"></param>
    /// <returns></returns>
    public static string GetEWalletAdminCommands(GenericObject tender, GenericObject coreEvent, ref EWalletDemographics ownerDemograhics, List<CardActionEnum> adminCommandList, ref string err)
    {
      // If "Add the item to eWallet" was selected then perform the standard "NewCustomerWithContract,ActivateContract" commands
      string acString = "";
      try
      {
        // Bug 18129 Allow "add_item_to_ewallet" checkbox to be used for adding an item to the eWallet
        object objAddToEWallet = tender.get("add_item_to_ewallet", "false");
        bool addToEWallet = objAddToEWallet.GetType() == typeof(System.String) ? (bool)(((string)objAddToEWallet) == "true") : (bool)objAddToEWallet;
        if (!addToEWallet && coreEvent != null)
        {
          // Check event to see if this is a zero-dollar cc tender to add item to eWallet
          decimal dTotal = (decimal)tender.get("total");
          if (dTotal == 0.00M)
          {
            GenericObject trans = coreEvent.get("transactions", new GenericObject()) as GenericObject;
            int iCountofTransactions = trans.getIntKeyLength();
            for (int i = 0; i < iCountofTransactions; i++)
            {
              GenericObject tran = (GenericObject)trans.get(i);
              if (ItemHasValidStatus(tran))
              {
                objAddToEWallet = tran.get("add_item_to_ewallet", "false", true);
                addToEWallet = objAddToEWallet.GetType() == typeof(System.String) ? (bool)(((string)objAddToEWallet) == "true") : (bool)objAddToEWallet;
                if (addToEWallet)
                {
                  if (ownerDemograhics == null) // Check this transaction for Owner demographics
                  {
                    ownerDemograhics = new EWalletDemographics(tran);
                    if (!ownerDemograhics.hasTranValues)
                    {
                      ownerDemograhics = null;
                    }
                  }
                  break;
                }
              }
            }
          }
        }

        if (addToEWallet)
        {
          adminCommandList.Add(CardActionEnum.NewCustomerWithContract);
          adminCommandList.Add(CardActionEnum.ActivateContract);
          acString = string.Format("{0},{1}", CardActionEnum.NewCustomerWithContract.ToString(), CardActionEnum.ActivateContract.ToString());
        }
        else
        {
          acString = tender.get("payware_admin_command", null) as string;
          if (!string.IsNullOrEmpty(acString))
          {
            try
            {
              string[] commands = acString.Split(',');
              foreach (string command in commands)
              {
                CardActionEnum adminCommand = (CardActionEnum)Enum.Parse(typeof(CardActionEnum), command);
                if (Enum.IsDefined(typeof(CardActionEnum), adminCommand))
                {
                  adminCommandList.Add(adminCommand);
                }
                else
                {
                  err = string.Format("{0} is not an underlying value of the CardActionEnum enumeration.", command);
                  Logger.cs_log(err);
                }
              }
            }
            catch (ArgumentException)
            {
              err = string.Format("'{0}' could not be parsed into members of the CardActionEnum enumeration.", acString);
              Logger.cs_log(err);
            }
          }
        }
      }
      catch (Exception ex)
      {
        err = string.Format("Error occurred getting the eWallet admin commands.");
        Logger.cs_log(string.Format("{0}:{1}{2}", err, Environment.NewLine, ex.ToMessageAndCompleteStacktrace()));
      }
      return acString;
    }

    /// <summary>
    /// Bug 18129 DJD - Gets eWallet System Interface and Repository enum
    /// Gets the eWallet System Interface if this either an eWallet Admin or Token transaction. Also, if it is an Admin
    /// transaction, it gets the repository from the eWallet System Interface.
    /// </summary>
    /// <param name="bAdminTrans"></param>
    /// <param name="bTokenTrans"></param>
    /// <param name="geoEWalletSysInt"></param>
    /// <param name="eWalletRepository"></param>
    /// <returns>NULL if successful, CASL Error otherwise</returns>
    private static GenericObject GetEWalletSysIntAndRepository(bool bAdminTrans, bool bTokenTrans, ref GenericObject geoEWalletSysInt, ref RepositoryEnum eWalletRepository)
    {
      GenericObject geoRetVal = null;
      string err = "";
      try
      {
        if (bAdminTrans || bTokenTrans)
        {
          geoEWalletSysInt = GetEWalletSysInt();
          if (geoEWalletSysInt == null)
          {
            err = string.Format("eWallet System Interface is not configured. Unable to perform {0} transaction.", bAdminTrans ? "Admin" : "Token");
            Logger.cs_log(err);
            geoRetVal = misc.MakeCASLError("System Interface Error", "EWALLET-004", string.Format("CARDERROR{0}", err), true);
          }
          if (bAdminTrans)
          {
            string rep = geoEWalletSysInt.get("repository", "") as string;
            if (!string.IsNullOrEmpty(rep))
            {
              eWalletRepository = (RepositoryEnum)Enum.Parse(typeof(RepositoryEnum), rep, true);
              if (!Enum.IsDefined(typeof(RepositoryEnum), eWalletRepository))
              {
                Logger.cs_log(string.Format("{0} is not an underlying value of the RepositoryEnum enumeration.", eWalletRepository));
                eWalletRepository = RepositoryEnum.PAYWARE_TOKEN; // Default to PAYware
              }
            }
          }
        }
      }
      catch (Exception ex)
      {
        err = "Error Getting eWallet System Interface or Repository";
        Logger.cs_log(string.Format("{0}:{1}{2}", err, Environment.NewLine, ex.ToMessageAndCompleteStacktrace()));
        geoRetVal = misc.MakeCASLError("System Interface Error", "EWALLET-005", string.Format("CARDERROR{0}", err), true);
      }
      return geoRetVal;
    }

    /// <summary>
    /// Bug 21009 [Bug 18129] DJD - Support for Admin Transactions and eWallet Web Service
    /// Constructs an WalletDBServiceClient based on first eWallet S.I. configuration found.
    /// </summary>
    /// <param name="errMsg"></param>
    /// <returns></returns>
    private static WalletDBServiceClient ConstructEWalletClient(ref string errMsg)
    {
      WalletDBServiceClient retClient = ConstructEWalletClient(null, ref errMsg);
      return retClient;
    }

    /// <summary>
    /// Bug 18129 DJD - Support for Admin Transactions and eWallet Web Service
    /// Constructs an WalletDBServiceClient based on eWallet S.I. configuration.
    /// Returns null if wronlgy configured or error occurs.
    /// Allow caller to pass the eWallet System Interface.
    /// </summary>
    /// <returns>Newly Constructed WalletDBServiceClient or null</returns>
    private static WalletDBServiceClient ConstructEWalletClient(GenericObject geoEWalletSysInt, ref string errMsg)
    {
      const string HTTP_CONFIG_NAME = "BasicHttpBinding_IWalletDBService";
      const string HTTPS_CONFIG_NAME = "BasicHttpsBinding_IWalletDBService";
      string remoteAddress = null;
      string endpointConfigName = null;
      WalletDBServiceClient retClient = null;
      errMsg = null;
      try
      {
        if (geoEWalletSysInt == null)
        {
          geoEWalletSysInt = GetEWalletSysInt();
        }
        if (geoEWalletSysInt == null)
        {
          errMsg = string.Format("The eWallet System Interface is NOT configured.");
          Logger.cs_log(errMsg);
          return null;
        }
        remoteAddress = geoEWalletSysInt.get("endpoint_address", "") as string;
        if (string.IsNullOrEmpty(remoteAddress))
        {
          errMsg = string.Format("The eWallet System Interface is NOT configured.");
          Logger.cs_log(errMsg);
          return null;
        }
        else
        {
          remoteAddress = remoteAddress.Trim();
          endpointConfigName = remoteAddress.StartsWith("https:", StringComparison.OrdinalIgnoreCase) ?
          HTTPS_CONFIG_NAME : HTTP_CONFIG_NAME;
        }
        string sIsLog = geoEWalletSysInt.get("is_log", "N") as string;
        bool logReqResp = (sIsLog == "Y");

        // Use the 'eWallet client' variable to call operations on the service.
        retClient = new WalletDBServiceClient(endpointConfigName, remoteAddress);

        // After instantiating the client, add the behavior to inspect messages to the endpoint.
        if (logReqResp)
        {
          retClient.Endpoint.Behaviors.Add(new EWalletInspectorBehavior());
        }
      }
      catch (Exception ex)
      {
        errMsg = string.Format("Failed to construct eWallet web service client:{0}{1}", Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
        Logger.cs_log(errMsg);
      }
      finally // Close the client if error occurred.
      {
        if (!string.IsNullOrEmpty(errMsg) && retClient != null)
        {
          retClient.Close();
          retClient = null;
        }
      }
      return retClient;
    }

    /// <summary>
    /// Bug 21009 [Bug 18129] DJD - Support for CORE Repository Web Service
    /// Constructs an CoreRepositoryServiceClient based on first eWallet S.I. configuration found.
    /// </summary>
    /// <param name="errMsg"></param>
    /// <returns></returns>
    private static CoreRepositoryServiceClient ConstructCoreRepositoryClient(ref string errMsg)
    {
      CoreRepositoryServiceClient retClient = ConstructCoreRepositoryClient(null, ref errMsg);
      return retClient;
    }

    /// <summary>
    /// Bug 18129 DJD - Support for CORE Repository Web Service
    /// Constructs an CoreRepositoryServiceClient based on eWallet S.I. configuration.
    /// Returns null if wronlgy configured or error occurs.
    /// Allow caller to pass the eWallet System Interface.
    /// </summary>
    /// <returns>Newly Constructed WalletDBServiceClient or null</returns>
    private static CoreRepositoryServiceClient ConstructCoreRepositoryClient(GenericObject geoEWalletSysInt, ref string errMsg)
    {
      const string HTTP_CONFIG_NAME = "BasicHttpBinding_ICoreRepositoryService";
      const string HTTPS_CONFIG_NAME = "BasicHttpsBinding_ICoreRepositoryService";
      string remoteAddress = null;
      string endpointConfigName = null;
      CoreRepositoryServiceClient retClient = null;
      errMsg = null;
      try
      {
        if (geoEWalletSysInt == null)
        {
          geoEWalletSysInt = GetEWalletSysInt();
        }
        if (geoEWalletSysInt == null)
        {
          errMsg = string.Format("The eWallet System Interface is NOT configured.");
          Logger.cs_log(errMsg);
          return null;
        }
        remoteAddress = geoEWalletSysInt.get("CORE_repository_endpoint_address", "") as string;
        if (string.IsNullOrEmpty(remoteAddress))
        {
          errMsg = string.Format("The eWallet System Interface is NOT configured.");
          Logger.cs_log(errMsg);
          return null;
        }
        else
        {
          remoteAddress = remoteAddress.Trim();
          endpointConfigName = remoteAddress.StartsWith("https:", StringComparison.OrdinalIgnoreCase) ?
          HTTPS_CONFIG_NAME : HTTP_CONFIG_NAME;
        }
        string sIsLog = geoEWalletSysInt.get("is_log", "N") as string;
        bool logReqResp = (sIsLog == "Y");

        // Use the 'Core Repository client' variable to call operations on the service.
        retClient = new CoreRepositoryServiceClient(endpointConfigName, remoteAddress);

        // After instantiating the client, add the behavior to inspect messages to the endpoint.
        if (logReqResp)
        {
          retClient.Endpoint.Behaviors.Add(new EWalletInspectorBehavior());
        }
      }
      catch (Exception ex)
      {
        errMsg = string.Format("Failed to construct Core Repository web service client:{0}{1}", Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
        Logger.cs_log(errMsg);
      }
      finally // Close the client if error occurred.
      {
        if (!string.IsNullOrEmpty(errMsg) && retClient != null)
        {
          retClient.Close();
          retClient = null;
        }
      }
      return retClient;
    }

    /// <summary>
    /// Bug 18129 DJD - Support for Admin Transactions and eWallet
    /// Returns the eWallet system interface object if configured; otherwise, null is returned.
    /// </summary>
    /// <returns></returns>
    private static GenericObject GetEWalletSysInt()
    {
      return GetConfiguredSystemInterface("eWallet");
    }

    /// <summary>
    /// Bug 18129 DJD - Support for Admin Transactions and eWallet
    /// Returns the system interface GEO object if configured for the type; otherwise, null is returned.
    /// NOTE: This method is useful for System Interface types that will be configured ONE TIME (e.g.,
    ///       "IPCharge", "eWallet").
    /// </summary>
    /// <returns></returns>
    private static GenericObject GetConfiguredSystemInterface(string typeName)
    {
      GenericObject geoReturnSystemInterface = null;
      GenericObject geoAllSystemInterfaces = (GenericObject)c_CASL.c_object("System_interface.of");
      ArrayList listSystemInterfaces = geoAllSystemInterfaces.getStringKeys();
      foreach (string si in listSystemInterfaces)
      {
        GenericObject geoSystemInterface = (GenericObject)geoAllSystemInterfaces.get(si, null);
        if (geoSystemInterface != null && geoSystemInterface.has("_parent"))
        {
          GenericObject geoSIParent = (GenericObject)geoSystemInterface.get("_parent");
          string name = geoSIParent.get("_name", "") as string;
          if (name == typeName)
          {
            geoReturnSystemInterface = geoSystemInterface;
            break;
          }
        }
      }
      return geoReturnSystemInterface;
    }

    /// <summary>
    /// Bug 18129 DJD - Support for Admin Transactions and eWallet
    /// Returns the tender category [i.e., granparent's _name (MC, VISA, DISC, AMEX)] for the tender
    /// object provided; null is returned if the tender category cannot be determined.
    /// </summary>
    /// <param name="geoTender"></param>
    /// <returns></returns>
    private static string GetTenderCategory(GenericObject geoTender)
    {
      string category = null;
      GenericObject parent = null;
      GenericObject parentsParent = null;

      if (geoTender == null)
      {
        return category;
      }

      parent = geoTender.get("_parent", null) as GenericObject;
      if (parent == null || !(parent is GenericObject)) { return category; } // parent is invalid

      parentsParent = parent.get("_parent", null) as GenericObject;
      if (parentsParent == null || !(parentsParent is GenericObject)) { return category; } // parentsParent is invalid

      category = parentsParent.get("_name", null) as string;

      // If the category is Tender or Online (went up too far) then return the parents name
      if (category == "Tender" || category == "Online")
      {
        category = parent.get("_name", null) as string;
      }

      // Bug 26719 DJD Update eWallet and Gateway Processing
      if (!string.IsNullOrWhiteSpace(category))
      {
        geoTender.set("TenderCategory", category);
      }

      return category;
    }

    /// <summary>
    /// Bug 18129 DJD - Support for eWallet Token Transaction [***** eCheck Tenders *****]
    /// This method will set the eCheck Tender geo values with the data retrieved from the eWallet/CORE Repository databases.
    /// The eWalletToken, hostSystemID, and personID (credentials) are used to find the eWallet item.
    /// Returns null if the eWallet item is found and values successfully set in the Tender geo; otherwise, returns a 
    /// CASL error object.
    /// </summary>
    /// <param name="eWalletToken"></param>
    /// <param name="hostSystemID"></param>
    /// <param name="personID"></param>
    /// <param name="tender"></param>
    /// <param name="geoEWSysInt"></param>
    /// <returns></returns>
    private static GenericObject SetECheckDataFromEWallet(string eWalletToken, string hostSystemID,
    string personID, ref GenericObject tender, ref GenericObject geoEWSysInt)
    {
      GenericObject retValue = null; // If NULL is returned it means success
      string errMsg = "";
      WalletDBServiceClient client = null;
      try
      {
        client = ConstructEWalletClient(geoEWSysInt, ref errMsg);
        if (client == null)
        {
          retValue = misc.MakeCASLError("System Interface Error", "EWALLET-006", string.Format("{0}", errMsg), true);
        }
        else
        {
          WalletResponse wr = null;
          WalletCredentials wc = null;
          if (!string.IsNullOrEmpty(personID))
          {
            wc = new WalletCredentials();
            wc.PersonID = personID;
            wc.hostReference = hostSystemID;
            // Bug 18912 Removed and deprecated hostReference_2 (HOST_REFERENCE_2) as an eWallet owner credential
          }

          WalletOwner wo = client.GetWalletWithItem(wc, eWalletToken, out wr);

          if (wr.success)
          {
            if (wo.itemList.Count() > 0)
            {
              if (string.Equals(wo.itemList[0].itemType, ewItemTypes[(int)ewItemType.eCheck], StringComparison.OrdinalIgnoreCase))
              {
                tender.set("payer_fname", wo.firstName);
                tender.set("payer_lname", wo.lastName);
                tender.set("bank_routing_nbr", wo.itemList[0].ext_02);
                tender.set("account_type", wo.itemList[0].ext_01);
                tender.set("address", wo.address1);
                tender.set("city", wo.city);
                tender.set("state", wo.ownerState);
                tender.set("zip", wo.zip);

                SecureString ssBankAcctNbrEnd = new SecureString();
                GetItemFromCORERepository(eWalletToken, ref ssBankAcctNbrEnd, ref geoEWSysInt, ref retValue);
                string bankAcctNbrStart = "";
                if (retValue == null)
                {
                  bankAcctNbrStart = ssBankAcctNbrEnd.ConvertToUnsecureString();
                }
                string bankAcctNbrEnd = wo.itemList[0].maskedValue;
                bankAcctNbrEnd = bankAcctNbrEnd.Trim().TrimStart('*');

                tender.set("bank_account_nbr", string.Format("{0}{1}", bankAcctNbrStart, bankAcctNbrEnd));
              }
              else // This is not an eCheck Tender
              {
                errMsg = string.Format("The eWallet transaction must have item type: {0}.", ewItemTypes[(int)ewItemType.eCheck]);
                Logger.cs_log(errMsg);
                retValue = misc.MakeCASLError("System Interface Error", "EWALLET-007", string.Format("CARDERROR {0}", errMsg), true);
              }

            }
            else // This probably can't happen
            {
              errMsg = string.Format("An eWallet item was not found for token: {0}.", eWalletToken);
              Logger.cs_log(errMsg);
              retValue = misc.MakeCASLError("System Interface Error", "EWALLET-008", string.Format("CARDERROR {0}", errMsg), true);
            }

          }
          else // Return error (cannot get eWallet owner using token and credentials)
          {
            // Show the Error if WDB-5 (Cannot find Wallet Owner) or WDB-37 (Cannot find wallet item)
            // or WDB-35 (Expired Card)
            string showErr = (wr.errorCode == "WDB-5" || wr.errorCode == "WDB-35" || wr.errorCode == "WDB-37") ? "CARDERROR " : "";
            errMsg = wr.detailedErrorMsg;
            Logger.cs_log(errMsg);
            retValue = misc.MakeCASLError("System Interface Error", "EWALLET-009", string.Format("{0}{1}", showErr, errMsg), true);
          }
        }
      }
      catch (Exception ex)
      {
        errMsg = string.Format("Error setting the eCheck data from eWallet using credentials:{0}{1}"
        , Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
        Logger.cs_log(errMsg);
        retValue = misc.MakeCASLError("System Interface Error", "EWALLET-010", string.Format("{0}", errMsg), true);
      }
      finally // Close the client
      {
        if (client != null)
        {
          client.Close();
        }
      }
      return retValue;
    }

    /// <summary>
    /// Bug 21009 [Bug 18129] DJD - Gets the eWallet (Owner) with an item
    /// </summary>
    /// <param name="ewToken"></param>
    /// <param name="hostID"></param>
    /// <param name="hostID2"></param>
    /// <param name="personID"></param>
    /// <param name="geoEWSysInt"></param>
    /// <param name="wo"></param>
    /// <returns></returns>
    private static GenericObject GetEWalletWithItem(string ewToken, string hostID, string hostID2,
    string personID, ref GenericObject geoEWSysInt, ref WalletOwner wo)
    {
      GenericObject retValue = null; // If NULL is returned it means success
      string errMsg = "";
      WalletDBServiceClient client = null;
      try
      {
        client = ConstructEWalletClient(geoEWSysInt, ref errMsg);
        if (client == null)
        {
          retValue = misc.MakeCASLError("System Interface Error", "EWALLET-011", string.Format("{0}", errMsg), true);
        }
        else
        {
          WalletResponse wr = null;
          WalletCredentials wc = null;
          if (!string.IsNullOrEmpty(personID))
          {
            wc = new WalletCredentials();
            wc.PersonID = personID;
            wc.hostReference = hostID;
          }
          wo = client.GetWalletWithItem(wc, ewToken, out wr);
          if (!wr.success) // Return error (cannot get eWallet owner using token and credentials)
          {
            // Show the Error if WDB-5 (Cannot find Wallet Owner) or WDB-37 (Cannot find wallet item) or WDB-35 (Expired Card)
            string showErr = (wr.errorCode == "WDB-5" || wr.errorCode == "WDB-35" || wr.errorCode == "WDB-37") ? "CARDERROR " : "";
            errMsg = wr.detailedErrorMsg;
            Logger.cs_log(errMsg);
            retValue = misc.MakeCASLError("System Interface Error", "EWALLET-012", string.Format("{0}{1}", showErr, errMsg), true);
          }
        }
      }
      catch (Exception ex)
      {
        errMsg = string.Format("Error getting an eWallet with item using credentials:{0}{1}"
        , Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
        Logger.cs_log(errMsg);
        retValue = misc.MakeCASLError("System Interface Error", "EWALLET-013", string.Format("{0}", errMsg), true);
      }
      finally // Close the client
      {
        if (client != null)
        {
          client.Close();
        }
      }
      return retValue;
    }

    /// <summary>
    /// Bug 21009 [Bug 18129] DJD - Sets credit card data from eWallet
    /// </summary>
    /// <param name="ewToken"></param>
    /// <param name="hostID"></param>
    /// <param name="hostID2"></param>
    /// <param name="personID"></param>
    /// <param name="geoResultSet"></param>
    /// <param name="sscAcctNbr"></param>
    /// <param name="geoEWSysInt"></param>
    /// <returns></returns>
    private static GenericObject SetCreditCardDataFromEWallet(string ewToken, string hostID, string hostID2,
    string personID, ref GenericObject geoResultSet, ref SecureStringClass sscAcctNbr, ref GenericObject geoEWSysInt)
    {
      GenericObject retValue = null; // If NULL is returned it means success
      string errMsg = "";
      WalletDBServiceClient client = null;
      try
      {
        client = ConstructEWalletClient(geoEWSysInt, ref errMsg);
        if (client == null)
        {
          retValue = misc.MakeCASLError("System Interface Error", "EWALLET-014", string.Format("{0}", errMsg), true);
        }
        else
        {
          WalletResponse wr = null;
          WalletCredentials wc = null;
          if (!string.IsNullOrEmpty(personID))
          {
            wc = new WalletCredentials();
            wc.PersonID = personID;
            wc.hostReference = hostID;
          }

          WalletOwner wo = client.GetWalletWithItem(wc, ewToken, out wr);

          if (wr.success)
          {
            if (wo.itemList.Count() > 0)
            {
              if (string.Equals(wo.itemList[0].itemType, ewItemTypes[(int)ewItemType.CreditCard], StringComparison.OrdinalIgnoreCase))
              {
                // BEGIN Bug 25177 - DJD: eWallet Zip Handling
                // CC Tender: Card Owner Address (if provided)
                string address = wo.itemList[0].ext_06;
                if (!string.IsNullOrWhiteSpace(address))
                {
                  geoResultSet.set("CUSTOMER_STREET", TrimTo(address, 20));
                }

                // CC Tender: Card Owner Zip (if provided)
                string zip = wo.itemList[0].ext_09;
                if (!string.IsNullOrWhiteSpace(zip))
                {
                  geoResultSet.set("CUSTOMER_ZIP", zip);
                }
                // END Bug 25177

                geoResultSet.set("ACCT_NUM", "pci.SecureStringClass"); // Placeholder for CC account number.
                if (wo.itemList[0].expirationDate.HasValue)
                {
                  DateTime dtExp = (DateTime)wo.itemList[0].expirationDate;
                  geoResultSet.set("EXP_MONTH", dtExp.ToString("MM"));
                  geoResultSet.set("EXP_YEAR", dtExp.ToString("yy"));
                }
                geoResultSet.set("PRESENT_FLAG", "1"); // Card is not present

                SecureString ssAcctNbr = new SecureString();
                GetItemFromCORERepository(ewToken, ref ssAcctNbr, ref geoEWSysInt, ref retValue);
                if (retValue == null)
                {
                  string maskedAcctNbr = wo.itemList[0].maskedValue.Trim();
                  int counter = 0;
                  bool beforeAsterisks = true;
                  foreach (char c in maskedAcctNbr)
                  {
                    if (c == '*')
                    {
                      if (beforeAsterisks)
                      {
                        beforeAsterisks = false;
                      }
                    }
                    else
                    {
                      if (beforeAsterisks)
                      {
                        ssAcctNbr.InsertAt(counter, c);
                        counter++;
                      }
                      else // After asterisks
                      {
                        ssAcctNbr.AppendChar(c);
                      }
                    }
                  }
                  sscAcctNbr = new SecureStringClass("EWALLET_TOKEN", ssAcctNbr);
                }
              }
              else // This is not a Credit Card Tender
              {
                errMsg = string.Format("The eWallet transaction must have item type: {0}.", ewItemTypes[(int)ewItemType.CreditCard]);
                Logger.cs_log(errMsg);
                retValue = misc.MakeCASLError("System Interface Error", "EWALLET-015", string.Format("CARDERROR {0}", errMsg), true);
              }
            }
            else // This probably can't happen
            {
              errMsg = string.Format("An eWallet item was not found for token: {0}.", ewToken);
              Logger.cs_log(errMsg);
              retValue = misc.MakeCASLError("System Interface Error", "EWALLET-016", string.Format("CARDERROR {0}", errMsg), true);
            }
          }
          else // Return error (cannot get eWallet owner using token and credentials)
          {
            // Show the Error if WDB-5 (Cannot find Wallet Owner) or WDB-37 (Cannot find wallet item) or WDB-35 (Expired Card)
            string showErr = (wr.errorCode == "WDB-5" || wr.errorCode == "WDB-35" || wr.errorCode == "WDB-37") ? "CARDERROR " : "";
            errMsg = wr.detailedErrorMsg;
            Logger.cs_log(errMsg);
            retValue = misc.MakeCASLError("System Interface Error", "EWALLET-017", string.Format("{0}{1}", showErr, errMsg), true);
          }
        }
      }
      catch (Exception ex)
      {
        errMsg = string.Format("Error setting the Credit Card data from eWallet using credentials:{0}{1}"
        , Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
        Logger.cs_log(errMsg);
        retValue = misc.MakeCASLError("System Interface Error", "EWALLET-018", string.Format("{0}", errMsg), true);
      }
      finally // Close the client
      {
        if (client != null)
        {
          client.Close();
        }
      }
      return retValue;
    }

    /// <summary>
    /// Bug 21009 [Bug 18129] DJD - Support for Admin Transactions and eWallet [***** Credit Card Tenders ONLY *****]
    /// Performs the PAYware Admin Transaction(s) [e.g., Create PAYware Customer and Contract] OR stores 
    /// the card data to CORE repository based on the card action commands provided in the "adminCmdList".
    /// GenericObject retValue is set to CASL error if an error occurs; returns null if successful.
    /// </summary>
    /// <param name="tender"></param>
    /// <param name="pwAuth"></param>
    /// <param name="adminCmdList"></param>
    /// <param name="hostSystemID"></param>
    /// <param name="personID"></param>
    /// <param name="customerID"></param>
    /// <param name="contractID"></param>
    /// <param name="adminHost"></param>
    /// <param name="timeout"></param>
    /// <param name="sCc_track_info"></param>
    /// <param name="rep">Repository: CORE or PAYWARE or PAYWARE_TOKEN</param>
    /// <param name="geoEWSysInt"></param>
    /// <param name="retValue"></param>
    private static void PerformAdminTransactions(GenericObject tender, MerchantKeyAuthentication pwAuth,
    List<CardActionEnum> adminCmdList, string hostSystemID, string personID, ref string customerID,
    ref string contractID, String adminHost, String host, int timeout, SecureStringClass sCc_track_info,
    string PAYwareTKN, RepositoryEnum rep, ref GenericObject geoPWCSysInt, ref GenericObject geoEWSysInt,
    EWalletDemographics ownerDemographics, ref GenericObject retValue)
    {
      const string SEC_STR_PLACEHOLDER = "pci.SecureStringClass";
      string ownerID = ""; // Identifies the eWallet owner (eWallet generated ID value)
      string returnToken = ""; // Used for an admin transaction that creates an eWallet item/PAYware contract
      string existingReturnToken = ""; // Used for an admin transaction that attempts to create a customer/contract when it already exists
      string existingPAYwareToken = ""; // Used for a PAYware Token transaction that attempts to create a token when it already exists
      string PAYwareToken = ""; // Used for a PAYware Token transaction that returns the newly created token
      string PAYwareTokenExpDate = ""; // The expiration date for a PAYware Token
      bool lastActionSucceeded = true; // Each action is dependent on the success of the previous one.
      string cardNbrMasked = null;
      SecureString cardNbrMidsection = null;
      string expYear = null;
      string expMonth = null;
      DateTime? expDate = null;
      GatewayInfo gatewayInfo = null;
      GatewayInfo gatewayInfoVerifone = null; // Bug 23150 DJD Added for device authentication
      ReqSender reqSender = null;
      string hashedMerchantKey = null;
      WalletItem existingItem = null;
      WalletItem addedItem = null;
      bool changingItemRepository = false;
      bool PAYwareTokenRenewed = false; // To prevent making a Token renewal call twice

      // Bug 23150 DJD Added for device entry
      object objSwipe = tender.get("ewallet_isSwipe", false);
      bool cardSwiped = objSwipe.GetType() == typeof(System.String) ? ((string)objSwipe) == "true" : (bool)objSwipe;

      try
      {
        if (rep == RepositoryEnum.PAYWARE)
        {
          // Get the hashed merchantkey for identifying PAYware CC item
          hashedMerchantKey = pwAuth.GetHashedMerchantKey();

          // Initialize customerID and contractID with randomly generated ID values
          GenerateCustomerAndContractIDs(ref customerID, ref contractID);
        }

        if (rep == RepositoryEnum.PAYWARE)
        {
          // Populate PAYware gateway info
          gatewayInfo = PopulateGatewayInfo_MerchantKeyAuthentication(pwAuth.userID, pwAuth.userPW, pwAuth.clientID, pwAuth.merchantKey, adminHost, timeout);
        }

        if (rep == RepositoryEnum.PAYWARE_TOKEN)
        {
          ManagedDeviceAuthentication mdAuth = new ManagedDeviceAuthentication(geoPWCSysInt);
          gatewayInfo = PopulateGatewayInfo_ManagedDeviceAuthentication(mdAuth.clientID, mdAuth.serialNum, mdAuth.deviceType, mdAuth.deviceKey, host, timeout);
          if (gatewayInfo == null)
          {
            string ewCfgErr = string.Format(@"eWallet Config Error: The PAYWARE_TOKEN repository requires configuration of IPCHARGE system interface properties [Enterprise CC Client ID, Enterprise CC Serial Num, Enterprise CC Device Type] used for the persistent token processing.");
            throw new Exception(ewCfgErr);
          }

          // Bug 23150 DJD Added for device authentication
          ManagedDeviceAuthentication mdAuthVerifone = new ManagedDeviceAuthentication(tender, true);
          if (!string.IsNullOrWhiteSpace(mdAuthVerifone.deviceKey))
          {
            gatewayInfoVerifone = PopulateGatewayInfo_ManagedDeviceAuthentication(mdAuthVerifone.clientID, mdAuthVerifone.serialNum, mdAuthVerifone.deviceType, mdAuthVerifone.deviceKey, host, timeout);
          }
        }

        if (rep == RepositoryEnum.PAYWARE || rep == RepositoryEnum.PAYWARE_TOKEN)
        {
          // The ReqSender is passed but currently not used
          reqSender = new ReqSender();
        }

        // Get data that will identify the card item being added
        GetMaskedNbrAndExpDate(tender, sCc_track_info, ref cardNbrMasked, ref expYear, ref expMonth, ref expDate);

        // Get Card Number "Mid Section" to be stored encrypted in the CORE Repository
        if (rep == RepositoryEnum.CORE)
        {
          cardNbrMidsection = SecureStringClass.GetPANMidsection(sCc_track_info);
        }

        foreach (CardActionEnum action in adminCmdList)
        {
          if (lastActionSucceeded)
          {
            CardTransaction cardTransInfo = new CardTransaction();
            CardActionEnum pwAction = action; // PAYware Action
            CardActionEnum ewAction = action; // eWallet Action

            bool updateCardExpDate = false; // Bug 18129 21131 DJD - eWallet: Update Card Expiration Date for PAYware Token 
            if (pwAction == CardActionEnum.NewCustomerWithContract)
            {
              CheckForExistingOwnerAndItem(ewItemType.CreditCard, rep, hostSystemID, personID, ref ownerID,
              ref returnToken, ref existingReturnToken, ref existingPAYwareToken, ref customerID, ref contractID, cardNbrMasked,
              expDate, hashedMerchantKey, ref existingItem, ref pwAction, ref updateCardExpDate, ref geoEWSysInt, ref retValue);

              // The return_value is an error if it is NOT null
              if (retValue != null)
              {
                lastActionSucceeded = false;
                continue;
              }
              ewAction = pwAction; // Set the eWallet card action on the PAYware card action returned from CheckForExistingOwnerAndItem
              changingItemRepository = (ewAction == CardActionEnum.UpdateContract && rep.ToString() != existingItem.repositoryID);

              switch (rep)
              {
                case RepositoryEnum.PAYWARE_TOKEN:
                  {
                    // Bug 18129 21131 DJD - eWallet: Update Card Expiration Date for PAYware Token
                    pwAction = updateCardExpDate ? CardActionEnum.TokenUpdate : CardActionEnum.TokenQuery;
                    customerID = "";
                    contractID = "";
                  }
                  break;

                case RepositoryEnum.PAYWARE:
                case RepositoryEnum.CORE:
                  if (changingItemRepository) // Changing repositories
                  {
                    pwAction = CardActionEnum.NewCustomerWithContract;
                  }
                  break;
              }
            }

            cardTransInfo.Action = pwAction;

            switch (pwAction)
            {
              case CardActionEnum.TokenQuery:
              case CardActionEnum.TokenUpdate: // Bug 18129 21131 DJD - eWallet: Update Card Expiration Date for PAYware Token
              case CardActionEnum.AddContractToCustomer:
              case CardActionEnum.UpdateContract:
              case CardActionEnum.NewCustomerWithContract:
                if (pwAction == CardActionEnum.TokenQuery && (!string.IsNullOrEmpty(existingPAYwareToken) || (!string.IsNullOrEmpty(PAYwareTKN))))
                {
                  // Renew the token expiration date
                  cardTransInfo.Token = string.IsNullOrEmpty(PAYwareTKN) ? existingPAYwareToken : PAYwareTKN;
                  cardTransInfo.TokenRenewal = true;
                  object theObj = geoEWSysInt.get("payware_token_renewal_days", 0);
                  cardTransInfo.TokenRenewalDays = theObj is string ? Convert.ToInt32(theObj) : (theObj is int ? (int)theObj : 730);
                }
                else
                {
                  // Bug 18129 21131 DJD - eWallet: Update Card Expiration Date for PAYware Token
                  if (!string.IsNullOrEmpty(existingPAYwareToken))
                  {
                    cardTransInfo.Token = existingPAYwareToken;
                  }

                  // ********** CUSTOMER INFORMATION **********
                  cardTransInfo.MerchantCustomerID = customerID;
                  if (pwAction == CardActionEnum.NewCustomerWithContract || pwAction == CardActionEnum.TokenQuery)
                  {
                    // Bug 26719 DJD Update eWallet and Gateway Processing
                    SetCardTransDemographicInfoFromTender(tender, cardTransInfo);
                  }

                  // ********** CONTRACT INFORMATION **********
                  cardTransInfo.MerchantContractID = contractID;
                  // IPAY-786 [IPAY-745] DJD: eWallet Error with Point Device (Added Null Check)
                  if (sCc_track_info != null)
                  {
                    cardTransInfo.secureData = sCc_track_info.GetSecureString();
                    cardTransInfo.secureDataPlaceHolder = SEC_STR_PLACEHOLDER;
                    if (cardSwiped) // Bug 23150 DJD Added for device entry
                    {
                      cardTransInfo.TrackData = SEC_STR_PLACEHOLDER;
                    }
                    else
                    {
                      cardTransInfo.AccountNumber = SEC_STR_PLACEHOLDER;
                      // Bug 24436 DJD: Add To Wallet Trans Generating Invalid Token With Verifone Device Manual Entry
                      if (pwAction == CardActionEnum.TokenQuery)
                      {
                        // Use ExpDate for UGP requests when generating a payment token Ex: 1216
                        cardTransInfo.ExpDate = string.Format("{0}{1}", expMonth.PadLeft(2, '0'), expYear.PadLeft(2, '0'));
                      }
                      else
                      {
                        cardTransInfo.ExpMonth = expMonth;
                        cardTransInfo.ExpYear = expYear;
                      }
                    }
                  }
                  else if (!string.IsNullOrWhiteSpace(expMonth) && !string.IsNullOrWhiteSpace(expYear))
                  {
                    // Bug 24436 DJD: Add To Wallet Trans Generating Invalid Token With Verifone Device Manual Entry
                    if (pwAction == CardActionEnum.TokenQuery)
                    {
                      // Use ExpDate for UGP requests when generating a payment token Ex: 1216
                      cardTransInfo.ExpDate = string.Format("{0}{1}", expMonth.PadLeft(2, '0'), expYear.PadLeft(2, '0'));
                    }
                    else
                    {
                      cardTransInfo.ExpMonth = expMonth;
                      cardTransInfo.ExpYear = expYear;
                    }
                  }
                }
                break;

              case CardActionEnum.ActivateContract:
                if (rep == RepositoryEnum.PAYWARE)
                {
                  cardTransInfo.MerchantCustomerID = customerID;
                  cardTransInfo.MerchantContractID = contractID;
                }
                else if (rep == RepositoryEnum.PAYWARE_TOKEN && !string.IsNullOrEmpty(existingPAYwareToken))
                {
                  // Send another token query to renew the PAYware token expiration date and update eWallet
                  pwAction = CardActionEnum.TokenQuery;
                  ewAction = CardActionEnum.UpdateContract;
                  cardTransInfo.Action = pwAction;
                  cardTransInfo.Token = existingPAYwareToken;
                  cardTransInfo.TokenRenewal = true;
                  object theObj = geoEWSysInt.get("payware_token_renewal_days", 0);
                  cardTransInfo.TokenRenewalDays = theObj is string ? Convert.ToInt32(theObj) : (theObj is int ? (int)theObj : 730);
                }
                break;

              case CardActionEnum.None:
                // Do nothing
                break;
            }

            // Bug 18129 21131 DJD - eWallet: Update Card Expiration Date for PAYware Token (Added "TokenUpdate" code)
            if ((pwAction != CardActionEnum.None && string.IsNullOrEmpty(existingReturnToken)) ||
            (pwAction == CardActionEnum.TokenQuery && !string.IsNullOrEmpty(existingPAYwareToken)) ||
            (pwAction == CardActionEnum.TokenUpdate && !string.IsNullOrEmpty(existingPAYwareToken))
            )
            {
              PAYwareConnectTrans PWCardTrans = null;
              bool continueProcessing = true;
              if ((rep == RepositoryEnum.PAYWARE) || (pwAction == CardActionEnum.TokenQuery) || (pwAction == CardActionEnum.TokenUpdate))
              {
                // Do NOT send a duplicate request if the PAYware token was already renewed
                bool sendPAYwareRequest = (cardTransInfo.TokenRenewal.HasValue && (bool)cardTransInfo.TokenRenewal && PAYwareTokenRenewed) ? false : true;
                if (sendPAYwareRequest)
                {
                  if (pwAction == CardActionEnum.TokenQuery && gatewayInfoVerifone != null) // Bug 23150 DJD Added for device entry
                  {
                    PWCardTrans = new PAYwareConnectTrans(reqSender, gatewayInfoVerifone, cardTransInfo, null);
                  }
                  else
                  {
                    PWCardTrans = new PAYwareConnectTrans(reqSender, gatewayInfo, cardTransInfo, null);
                  }
                  continueProcessing = PWCardTrans.BuildSendRequest();

                  // This MAY be an existing token (not a newly created one); need to send a token update to change the expiration date
                  if (continueProcessing && (pwAction == CardActionEnum.TokenQuery))
                  {
                    // Bug 27311 DJD: Also update exp date for newly created token
                    bool updateExp = !string.IsNullOrEmpty(PWCardTrans._resp.PaymentToken) && !string.IsNullOrEmpty(cardTransInfo.ExpMonth) && !string.IsNullOrEmpty(cardTransInfo.ExpYear);
                    if (!updateExp && ewAction == CardActionEnum.AddContractToCustomer)
                    {
                      updateExp = !string.IsNullOrEmpty(PWCardTrans._resp.PaymentToken) && !string.IsNullOrEmpty(expMonth) && !string.IsNullOrEmpty(expYear);
                    }
                    if (updateExp)
                    {
                      CardTransaction cardTransTokenUpdate = new CardTransaction();
                      cardTransTokenUpdate.Action = CardActionEnum.TokenUpdate;
                      cardTransTokenUpdate.Token = PWCardTrans._resp.PaymentToken;
                      cardTransTokenUpdate.ExpMonth = string.IsNullOrWhiteSpace(cardTransInfo.ExpMonth) ? expMonth : cardTransInfo.ExpMonth;
                      cardTransTokenUpdate.ExpYear = string.IsNullOrWhiteSpace(cardTransInfo.ExpYear) ? expYear : cardTransInfo.ExpYear;
                      // Create and send token update request
                      PAYwareConnectTrans PWCardTransTokenUpdate = new PAYwareConnectTrans(reqSender, gatewayInfo, cardTransTokenUpdate, null);
                      continueProcessing = PWCardTransTokenUpdate.BuildSendRequest();
                    }
                  }
                }

                if (continueProcessing)
                {
                  continueProcessing = true;
                  if ((rep == RepositoryEnum.PAYWARE || rep == RepositoryEnum.PAYWARE_TOKEN) && PWCardTrans != null)
                  {
                    continueProcessing = PWCardTrans._resp.GatewaySuccess;
                    PAYwareTokenRenewed = (continueProcessing && cardTransInfo.TokenRenewal.HasValue && (bool)cardTransInfo.TokenRenewal);
                  }

                  // Bug 18129 21131 DJD - eWallet: Update Card Expiration Date for PAYware Token (Added "TokenUpdate" code)
                  if (continueProcessing && (rep == RepositoryEnum.PAYWARE_TOKEN) && PWCardTrans != null)
                  {
                    PAYwareToken = pwAction == CardActionEnum.TokenUpdate ? existingPAYwareToken : PWCardTrans._resp.PaymentToken;
                    PAYwareTokenExpDate = PWCardTrans._resp.PaymentTokenExpDate;
                    if (pwAction != CardActionEnum.TokenUpdate)
                    {
                      // Set Existing PAYware Token to newly created token for renewal (otherwise, clear Existing PAYware Token value that is already renewed)
                      existingPAYwareToken = string.IsNullOrEmpty(existingPAYwareToken) ? PAYwareToken : "";
                    }
                  }

                  if (continueProcessing)
                  {
                    string ext01 = rep == RepositoryEnum.PAYWARE_TOKEN ? PAYwareToken : customerID;
                    string ext02 = rep == RepositoryEnum.PAYWARE_TOKEN ? PAYwareTokenExpDate : contractID;

                    // PAYware admin transaction successful OR using CORE Repository: Update the eWallet DB (and get token)
                    switch (ewAction)
                    {
                      case CardActionEnum.AddContractToCustomer:
                      case CardActionEnum.NewCustomerWithContract:
                        if (ewAction == CardActionEnum.NewCustomerWithContract)
                        {
                          // Bug 26719 DJD Update eWallet and Gateway Processing
                          if (ownerDemographics == null)
                          {
                            SetCardTransDemographicInfoFromTender(tender, cardTransInfo);
                          }
                          lastActionSucceeded = CreateEWalletOwner(hostSystemID, personID, ref ownerID,
                            customerID, cardTransInfo, ownerDemographics, ref geoEWSysInt, ref retValue);
                        }
                        if (lastActionSucceeded)
                        {
                          lastActionSucceeded = AddItemToEWallet(ewItemType.CreditCard, tender, personID, hostSystemID, ownerID, rep, ext01, ext02, sCc_track_info,
                          hashedMerchantKey, cardNbrMasked, cardTransInfo, expDate, ref addedItem, ref returnToken, ref geoEWSysInt, ref retValue);
                        }
                        break;

                      case CardActionEnum.UpdateContract:
                        if (lastActionSucceeded)
                        {
                          // Need to modify the existing (or newly added) item in the eWallet DB (e.g., changing the repository)
                          bool itemAdded = false;
                          WalletItem wi = existingItem;
                          if (wi == null && addedItem != null)
                          {
                            wi = addedItem;
                            itemAdded = true;
                          }
                          if (wi != null)
                          {

                            lastActionSucceeded = ModifyEWalletItem(tender, rep, ownerID, expDate, hashedMerchantKey, wi,
                            ext01, ext02, ref geoEWSysInt, ref retValue);
                          }
                          // Bug 27311 DJD: Make EXIPRED eWallet items active again
                          bool activateExistingItem = (existingItem != null && existingItem.itemStatus != WalletItem.ItemStatus.ACTIVE);
                          if (lastActionSucceeded && (itemAdded || activateExistingItem))
                          {
                            lastActionSucceeded = SetEWalletItemToActive(returnToken, ref geoEWSysInt, ref retValue);
                            if (lastActionSucceeded && activateExistingItem)
                            {
                              existingItem.itemStatus = WalletItem.ItemStatus.ACTIVE;
                            }
                          }
                        }
                        break;

                      case CardActionEnum.ActivateContract: // Set eWallet item to ACTIVE now that contract is activated

                        if (rep == RepositoryEnum.CORE)
                        {
                          // 1) Update the CORE Repository with the encrypted PAN Midsection & encrypted token reference
                          // 2A) If successful, update CORE repository with encrypted PAN Midsection & mark eWallet item to ACTIVE.
                          // 2B) If unsuccessful, Mark the eWallet item as removed.
                          lastActionSucceeded = AddItemToCORERepository(cardNbrMidsection, returnToken, ref geoEWSysInt, ref retValue);
                        }
                        if (lastActionSucceeded)
                        {
                          lastActionSucceeded = SetEWalletItemToActive(returnToken, ref geoEWSysInt, ref retValue);
                        }
                        break;
                    }
                  }
                  else // PAYware Repository
                  {
                    retValue = misc.MakeCASLError("Admin Request Failed", "PWADMIN-010", string.Format("CARDERROR{0}", PWCardTrans._resp.GatewayMessage), true);
                    lastActionSucceeded = false;
                  }
                }
                else // PAYware Repository
                {
                  retValue = misc.MakeCASLError("Admin Request Failed", "PWADMIN-011", string.Format("CARDERROR{0}", PWCardTrans._resp.GatewayMessage), true);
                  lastActionSucceeded = false;
                }
              }
            }
          }

          // All Admin AddOn transactions succeeded because return_value is not an error.
          if (retValue == null)
          {
            retValue = new GenericObject();
            retValue.set(ADMIN_SUCCESS, true);
            tender.set("approval_code", ADMIN_SUCCESS);

            // Set auth string to -9091 for Admin Transactions ONLY when there is no value there already (i.e., the TROUTD)
            string authStr = tender.get("auth_str", "") as string;
            if (string.IsNullOrWhiteSpace(authStr))
            {
              tender.set("auth_str", -9091);
              // Bug 26719 BEGIN DJD Update eWallet and Gateway Processing
              if (!string.IsNullOrWhiteSpace(cardNbrMasked))
              {
                tender.set("MaskedCardNumber", cardNbrMasked);
              }
              if (expDate.HasValue)
              {
                const string FMT = "O";
                tender.set("ExpirationDate", ((DateTime)expDate).ToString(FMT));
              }
              // Bug 26719 END
            }

            if (!string.IsNullOrEmpty(returnToken))
            {
              bool bReturnToken = true;
              if (bReturnToken)
              {
                string maskedToken = MaskInput(returnToken, 4);
                Logger.cs_log(string.Format("SUCCESS: eWallet Item [ID: {0}] returned.", maskedToken));
                tender.set("ewallet_return_token", returnToken);
              }
            }
            else if (!string.IsNullOrEmpty(existingReturnToken))
            {
              string maskedToken = MaskInput(existingReturnToken, 4);
              Logger.cs_log(string.Format("SUCCESS: eWallet Item [ID: {0}] that already existed is returned.", maskedToken));
              tender.set("ewallet_return_token", existingReturnToken);
            }
            else
            {
              Logger.cs_log(string.Format("An eWallet Item ID (token) was not generated or returned."));
            }
          }
        }
      }
      finally
      {
        SetTokenToTender(tender, existingItem, addedItem);
      }
    }

    // Bug 26719 DJD Update eWallet and Gateway Processing [New method]
    private static void SetCardTransDemographicInfoFromTender(GenericObject tender, CardTransaction cardTransInfo)
    {
      cardTransInfo.CustomerFirstname = tender.get("payer_fname", null) as string;
      cardTransInfo.CustomerLastname = tender.get("payer_lname", null) as string;
      cardTransInfo.CustomerStreet = tender.get("address", null) as string;
      cardTransInfo.CustomerStreet2 = tender.get("address2", null) as string;
      cardTransInfo.CustomerCity = tender.get("city", null) as string;
      cardTransInfo.CustomerState = tender.get("state", null) as string;
      cardTransInfo.CustomerZip = tender.get("zip", null) as string;
      if (!string.IsNullOrWhiteSpace(cardTransInfo.CustomerZip)) // Bug 25177 - DJD: eWallet Zip Handling
      {
        cardTransInfo.CustomerZip = MakeZipValidForWallet(cardTransInfo.CustomerZip);
      }
    }

    /// <summary>
    /// Bug 23150 DJD
    /// This method will set the value of a newly created or existing token that has been updated to the tender.
    /// If this is a "zero dollar" tender (add item) then the token will be set to "auth_nbr" key; if not, it will
    /// be set to "ewallet_item_id" key.
    /// </summary>
    /// <param name="tender"></param>
    /// <param name="existingItem"></param>
    /// <param name="addedItem"></param>
    private static void SetTokenToTender(GenericObject tender, WalletItem existingItem, WalletItem addedItem)
    {
      string token = "";
      if (existingItem != null) { token = existingItem.itemID; }
      if (string.IsNullOrWhiteSpace(token))
      {
        if (addedItem != null) { token = addedItem.itemID; }
      }
      if (!string.IsNullOrWhiteSpace(token))
      {
        decimal dTotal = (decimal)tender.get("total");
        if (dTotal == 0.00M)
        {
          tender.set("auth_nbr", token);
        }
        tender.set("ewallet_item_id", token);
      }
    }

    /// <summary>
    /// Bug 21009 [Bug 18129] DJD - Support for eWallet
    /// Modifies eWallet item properties to those sent.
    /// </summary>
    /// <param name="tender"></param>
    /// <param name="rep"></param>
    /// <param name="ownerID"></param>
    /// <param name="expDate"></param>
    /// <param name="hashedMerchantKey"></param>
    /// <param name="existingItem"></param>
    /// <param name="ext01"></param>
    /// <param name="ext02"></param>
    /// <param name="geoEWSysInt"></param>
    /// <param name="retValue"></param>
    /// <returns></returns>
    private static bool ModifyEWalletItem(GenericObject tender, RepositoryEnum rep, string ownerID, DateTime? expDate, string hashedMerchantKey,
    WalletItem existingItem, string ext01, string ext02, ref GenericObject geoEWSysInt, ref GenericObject retValue)
    {
      bool successModifyItem = false;
      string errMsg = "";
      WalletDBServiceClient client = null;
      try
      {
        client = ConstructEWalletClient(geoEWSysInt, ref errMsg);
        if (client != null)
        {
          WalletResponse wr = null;
          string itemName = tender.get("ewallet_tender_nickname", "") as string;
          if (string.IsNullOrEmpty(itemName))
          {
            if (string.IsNullOrEmpty(existingItem.itemName))
            {
              existingItem.itemName = tender.get("description", "") as string;
            }
          }
          else
          {
            existingItem.itemName = itemName;
          }

          // Bug 26719 DJD Added for "Share Token With" from Gateway
          string itemShareName = tender.get("ewallet_share_item_with", "") as string;
          if (!string.IsNullOrEmpty(itemShareName))
          {
            existingItem.ext_10 = itemShareName;
          }

          existingItem.creationDate = DateTime.Now;
          existingItem.expirationDate = expDate;
          existingItem.repositoryID = rep.ToString();
          existingItem.ext_01 = ext01; // Set PAYware Merchant Customer ID (PAYWARE) or Token (PAYWARE_TOKEN) to ext_01
          existingItem.ext_02 = ext02; // Set PAYware Merchant Contract ID (PAYWARE) or Token Exp Date (PAYWARE_TOKEN) to ext_02
          string tenderID = tender.get("id", "", true) as string;
          existingItem.ext_03 = string.IsNullOrEmpty(tenderID) ? null : tenderID;
          existingItem.ext_04 = hashedMerchantKey;

          PopulateWalletItemWithTenderDemographics(tender, existingItem); // Bug 25177 - DJD: eWallet Zip Handling

          string itemID = client.ModifyWalletItem(existingItem.itemID, existingItem, out wr);

          successModifyItem = wr.success;
          if (successModifyItem)
          {
            string maskedToken = MaskInput(itemID, 4);
            Logger.cs_log(string.Format("SUCCESS: eWallet Item [ID: {0}] modified.", maskedToken));
            tender.set("ewallet_return_token", itemID);
          }
          else // Log error
          {
            errMsg = string.Format("FAILURE: eWallet item NOT modified for eWallet Owner [{2}]:{0}{1}"
            , Environment.NewLine, wr.detailedErrorMsg, ownerID);
            Logger.cs_log(errMsg);
          }
        }
      }
      catch (Exception ex)
      {
        errMsg = string.Format("Error modifying eWallet item to [Owner ID {0}]:{1}{2}"
        , ownerID, Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
        Logger.cs_log(errMsg);
      }
      finally // Close the client
      {
        if (client != null)
        {
          client.Close();
        }
      }
      if (!successModifyItem)
      {
        retValue = misc.MakeCASLError("Admin Request Failed", "EWALLET-024", string.Format("{0}", errMsg), true);
      }
      return successModifyItem;
    }

    // Bug 25177 - DJD: eWallet Zip Handling
    private static void PopulateWalletItemWithTenderDemographics(GenericObject tender, WalletItem wi)
    {
      string cardFirstname = tender.get("payer_fname", "") as string;
      string cardLastname = tender.get("payer_lname", "") as string;
      string cardName = (string.Format("{0} {1}", cardFirstname, cardLastname)).Trim();
      string cardAddress = tender.get("address", "") as string;
      string cardCity = tender.get("city", "") as string;
      string cardState = tender.get("state", "") as string;
      string cardZip = tender.get("zip", "") as string;
      cardZip = MakeZipValidForWallet(cardZip);

      if (!string.IsNullOrWhiteSpace(cardName)) { wi.ext_05 = cardName; }     // CC Tender: Card Owner Name (if provided)
      if (!string.IsNullOrWhiteSpace(cardAddress)) { wi.ext_06 = cardAddress; }  // CC Tender: Card Owner Address (if provided)
      if (!string.IsNullOrWhiteSpace(cardCity)) { wi.ext_07 = cardCity; }     // CC Tender: Card Owner City (if provided)
      if (!string.IsNullOrWhiteSpace(cardState)) { wi.ext_08 = cardState; }    // CC Tender: Card Owner State (if provided)
      if (!string.IsNullOrWhiteSpace(cardZip)) { wi.ext_09 = cardZip; }      // CC Tender: Card Owner Zip Code (if provided)
    }

    /// <summary>
    /// Bug 18129 DJD - Support for Admin Transactions and eWallet [***** eCheck or Credit Card Tenders *****]
    /// Called from the "eWallet Update" system interface assigned to a tender (eCheck or CC).
    /// For eCheck, it stores the owner/item (e.g., customer and bank account) data to the eWallet DB and 
    /// CORE repository using the admin action commands provided in the "adminCmdList".
    /// GenericObject retValue is set to CASL error if an error occurs; returns null if successful.
    /// </summary>
    /// <param name="tender"></param>
    /// <param name="itemType"></param>
    /// <param name="adminCmdList"></param>
    /// <param name="hostSystemID"></param>
    /// <param name="personID"></param>
    /// <param name="geoEWSysInt"></param>
    private static GenericObject PerformAdminTransactions(GenericObject tender, ewItemType itemType, RepositoryEnum ewRep, List<CardActionEnum> adminCmdList, string hostSystemID,
    string personID, ref GenericObject geoEWSysInt, EWalletDemographics ownerDemographics)
    {
      GenericObject retValue = null;

      // Call the CC PerformAdminTransactions method
      if (itemType == ewItemType.CreditCard)
      {
        PerformAdminTransForCreditCard(tender, ewRep, adminCmdList, hostSystemID, personID, ref geoEWSysInt, ownerDemographics, ref retValue);
        return retValue;
      }

      string ownerID = ""; // Identifies the eWallet owner (eWallet generated ID value)
      string returnToken = ""; // Used for an admin transaction that creates an eWallet item/PAYware contract
      string existingReturnToken = ""; // Used for an admin transaction that attempts to create a customer/contract when it already exists
      bool lastActionSucceeded = true; // Each action is dependent on the success of the previous one.

      string routNbr = tender.get("bank_routing_nbr", null) as string;
      string acctNbr = tender.get("bank_account_nbr", null) as string;
      string acctType = tender.get("account_type", null) as string;
      string acctNbrMasked = "";
      string acctNbrStartSection = "";
      string exPWTkn = null;
      string exPWTknExpDate = null;
      DateTime? cardExpDate = null;

      if (itemType == ewItemType.eCheck)
      {
        if (routNbr == null || acctNbr == null)
        {
          string err = "Missing bank routing or account number.";
          retValue = misc.MakeCASLError("Admin Request Failed", "ECADMIN-001", string.Format("CARDERROR {0}", err), true);
          lastActionSucceeded = false;
        }
        else
        {
          // Split the bank account number into a masked value (stored in the eWallet DB) and the 
          // digits of the section that is masked (stored encrypted in the CORE Repository DB).
          acctNbr = acctNbr.Trim();
          const int UNMASKED_DIGITS = 4;
          int len = acctNbr.Length;
          if (len > UNMASKED_DIGITS)
          {
            acctNbrMasked = acctNbr.Substring(len - UNMASKED_DIGITS).PadLeft(len, '*');
            acctNbrStartSection = acctNbr.Substring(0, len - UNMASKED_DIGITS);
          }
          else // Bank account number length needs to be greater than 4
          {
            string err = "Bank account number length needs to be greater than 4.";
            retValue = misc.MakeCASLError("Admin Request Failed", "ECADMIN-002", string.Format("CARDERROR {0}", err), true);
            lastActionSucceeded = false;
          }
        }
      }

      foreach (CardActionEnum action in adminCmdList)
      {
        if (lastActionSucceeded)
        {
          // NOTE: The "CardTransaction" object is simply used to hold info about the eWallet owner
          CardTransaction eCheckTransInfo = new CardTransaction();
          // NOTE: The "CardActionEnum" value is used to keep things consistent with Credit Cards (i.e., Customer = Owner, Contract = Item)
          CardActionEnum adminAction = action;

          string custID = null;
          string contID = null;
          if (adminAction == CardActionEnum.NewCustomerWithContract)
          {
            // Bug 18912 Added HOST_REFERENCE_2 to store MerchantKey value
            WalletItem ExistingItem = null;
            bool updateCardExpDate = false; // Bug 18129 21131 DJD - eWallet: Update Card Expiration Date for PAYware Token
            CheckForExistingOwnerAndItem(itemType, ewRep, hostSystemID, personID, ref ownerID, ref returnToken,
            ref existingReturnToken, ref exPWTkn, ref custID, ref contID, acctNbrMasked, cardExpDate, null,
            ref ExistingItem, ref adminAction, ref updateCardExpDate, ref geoEWSysInt, ref retValue);

            // The return_value is an error if it is NOT null
            if (retValue != null)
            {
              lastActionSucceeded = false;
              continue;
            }
          }

          eCheckTransInfo.Action = adminAction;
          if (adminAction == CardActionEnum.NewCustomerWithContract)
          {
            eCheckTransInfo.CustomerFirstname = tender.get("payer_fname", null) as string;
            eCheckTransInfo.CustomerLastname = tender.get("payer_lname", null) as string;
            eCheckTransInfo.CustomerStreet = tender.get("address", null) as string;
            eCheckTransInfo.CustomerStreet2 = tender.get("address2", null) as string;
            eCheckTransInfo.CustomerCity = tender.get("city", null) as string;
            eCheckTransInfo.CustomerState = tender.get("state", null) as string;
            eCheckTransInfo.CustomerZip = tender.get("zip", null) as string;
          }

          if (adminAction != CardActionEnum.None && string.IsNullOrEmpty(existingReturnToken))
          {
            // PAYware admin transaction successful OR using CORE Repository: Update the eWallet DB (and get token)
            switch (adminAction)
            {
              case CardActionEnum.AddContractToCustomer:
              case CardActionEnum.NewCustomerWithContract:
                if (adminAction == CardActionEnum.NewCustomerWithContract)
                {
                  lastActionSucceeded = CreateEWalletOwner(hostSystemID, personID, ref ownerID, "", eCheckTransInfo, null, ref geoEWSysInt, ref retValue);
                }
                if (lastActionSucceeded)
                {
                  WalletItem addedItem = null;

                  string ext01 = itemType == ewItemType.CreditCard ? exPWTkn : acctType; // Bug 26719 DJD Save acct type for eCheck
                  string ext02 = itemType == ewItemType.CreditCard ? exPWTknExpDate : routNbr;

                  lastActionSucceeded = AddItemToEWallet(itemType, tender, personID, hostSystemID, ownerID, ewRep, ext01, ext02, null,
                  null, acctNbrMasked, eCheckTransInfo, cardExpDate, ref addedItem, ref returnToken, ref geoEWSysInt, ref retValue);
                }

                break;

              case CardActionEnum.ActivateContract: // Set eWallet item to ACTIVE now that contract is activated

                // 1) Update the CORE Repository with the encrypted Bank Account Number "masked digits" start section & encrypted token reference
                // 2A) If successful, mark eWallet item to ACTIVE.
                // 2B) If unsuccessful, mark the eWallet item as removed.
                SecureString ssAcctNbrStartSection = new SecureString();
                foreach (char c in acctNbrStartSection)
                {
                  ssAcctNbrStartSection.AppendChar(c);
                }
                lastActionSucceeded = AddItemToCORERepository(ssAcctNbrStartSection, returnToken, ref geoEWSysInt, ref retValue);

                if (lastActionSucceeded)
                {
                  lastActionSucceeded = SetEWalletItemToActive(returnToken, ref geoEWSysInt, ref retValue);
                }
                break;
            }
          }
        }

        // All Admin AddOn transactions succeeded because return_value is not an error.
        if (retValue == null)
        {
          retValue = new GenericObject();
          retValue.set(ADMIN_SUCCESS, true);
          tender.set("approval_code", ADMIN_SUCCESS);

          // Set auth string to -9091 for Admin Transactions ONLY when there is no value there already (i.e., the TROUTD from a sale)
          string authStr = tender.get("auth_str", "") as string;
          if (string.IsNullOrWhiteSpace(authStr))
          {
            tender.set("auth_str", -9091);
          }

          if (!string.IsNullOrEmpty(returnToken))
          {
            bool bReturnToken = true;
            if (bReturnToken)
            {
              string maskedToken = MaskInput(returnToken, 4);
              Logger.cs_log(string.Format("SUCCESS: eWallet Item [ID: {0}] returned.", maskedToken));
              tender.set("ewallet_return_token", returnToken);
            }
          }
          else if (!string.IsNullOrEmpty(existingReturnToken))
          {
            string maskedToken = MaskInput(existingReturnToken, 4);
            Logger.cs_log(string.Format("SUCCESS: eWallet Item [ID: {0}] that already existed is returned.", maskedToken));
            tender.set("ewallet_return_token", existingReturnToken);
          }
          else
          {
            Logger.cs_log(string.Format("An eWallet Item ID (token) was not generated or returned."));
          }
        }
      }
      return retValue;
    }

    private static void PerformAdminTransForCreditCard(GenericObject tender, RepositoryEnum ewRep, List<CardActionEnum> adminCmdList, string hostSystemID, string personID,
    ref GenericObject geoEWSysInt, EWalletDemographics ownerDemographics, ref GenericObject retValue)
    {
      try
      {
        MerchantKeyAuthentication pwAuth = new MerchantKeyAuthentication(); // UNUSED: PAYware Customer/Contract [DEPRECATED]
        string customerID = "";                                             // UNUSED: PAYware Customer/Contract [DEPRECATED]
        string contractID = "";                                             // UNUSED: PAYware Customer/Contract [DEPRECATED]
        string adminHost = "";                                              // UNUSED: PAYware Customer/Contract [DEPRECATED]
        GenericObject IPChargeSysInt = ewRep == RepositoryEnum.PAYWARE_TOKEN ? GetConfiguredSystemInterface("IPCharge") : null;
        string host = IPChargeSysInt == null ? "" : (string)IPChargeSysInt.get("host");
        int timeout = IPChargeSysInt == null ? 0 : (int)IPChargeSysInt.get("timeout", 9000);
        SecureStringClass sCc_track_info = GetSecureStringTrackDataOrAccountNumber(tender);
        string pwTkn = tender.get("PAYwareToken", "") as string;
        // Bug 25251 DJD: Added "PointToken" to identify a Point device transaction
        if (string.IsNullOrWhiteSpace(pwTkn)) { pwTkn = tender.get("PointToken", "") as string; }
        PerformAdminTransactions(tender, pwAuth, adminCmdList, hostSystemID, personID, ref customerID, ref contractID, adminHost,
        host, timeout, sCc_track_info, pwTkn, ewRep, ref IPChargeSysInt, ref geoEWSysInt, ownerDemographics, ref retValue);
      }
      catch (Exception ex)
      {
        string errMsg = string.Format("Error Performing Admin Transactions:{0}{1}", Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
        Logger.cs_log(errMsg);
        retValue = misc.MakeCASLError("System Interface Error", "PWADMIN-003", string.Format("{0}", errMsg), true);
      }
    }

    private static SecureStringClass GetSecureStringTrackDataOrAccountNumber(GenericObject tender)
    {
      SecureStringClass sCc_track_info = null;
      object o = tender.get("cc_track_info", null);
      if (o != null && o is GenericObject)
      {
        GenericObject pTrackData = (GenericObject)o;
        sCc_track_info = (SecureStringClass)pTrackData.get(0);
      }
      else // Probably manual entry. Try the credit_card_nbr_secure_class.
      {
        if (tender.has("credit_card_nbr_secure_class"))
        {
          GenericObject pTrackData = (GenericObject)tender.get("credit_card_nbr_secure_class");
          sCc_track_info = (SecureStringClass)pTrackData.get(0);
        }
      }
      return sCc_track_info;
    }

    /// <summary>
    /// Bug 18129 DJD - Support for Admin Transactions and eWallet
    /// Bug 18129 21131 DJD - eWallet: Update Card Expiration Date for PAYware Token (Added "updateCardExpDate")
    /// Checks eWallet for existing owner and item.  Modifies the card action if either exist:
    /// Action changed to AddContractToCustomer if just owner exists.
    /// If item also exists then do not "AddContractToCustomer"; look at the item status and
    /// just activate the contract if needed (i.e., ItemStatus.INACTIVE).
    /// GenericObject retValue is set to CASL error if an error occurs.
    /// </summary>
    /// <param name="itemType"></param>
    /// <param name="rep"></param>
    /// <param name="hostSystemID"></param>
    /// <param name="personID"></param>
    /// <param name="ownerID"></param>
    /// <param name="returnToken"></param>
    /// <param name="existingReturnToken"></param>
    /// <param name="existingPAYwareToken"></param>
    /// <param name="customerID"></param>
    /// <param name="contractID"></param>
    /// <param name="maskedValue"></param>
    /// <param name="expDate"></param>
    /// <param name="hashedMerchantKey"></param>
    /// <param name="ExistingItem"></param>
    /// <param name="cardAction"></param>
    /// <param name="updateCardExpDate"></param>
    /// <param name="geoEWSysInt"></param>
    /// <param name="retValue"></param>
    private static void CheckForExistingOwnerAndItem(ewItemType itemType, RepositoryEnum rep, string hostSystemID, string personID,
    ref string ownerID, ref string returnToken, ref string existingReturnToken, ref string existingPAYwareToken, ref string customerID,
    ref string contractID, string maskedValue, DateTime? expDate, string hashedMerchantKey, ref WalletItem ExistingItem,
    ref CardActionEnum cardAction, ref bool updateCardExpDate, ref GenericObject geoEWSysInt, ref GenericObject retValue)
    {
      string errMsg = "";
      WalletDBServiceClient client = null;
      try
      {
        client = ConstructEWalletClient(geoEWSysInt, ref errMsg);
        if (client == null)
        {
          retValue = misc.MakeCASLError("System Interface Error", "PWADMIN-012", string.Format("{0}", errMsg), true);
          return;
        }

        // Check eWallet to see if the Owner/Customer already exists; if so, change action to AddContractToCustomer
        WalletCredentials wc = new WalletCredentials();
        wc.PersonID = personID;
        wc.hostReference = string.IsNullOrEmpty(hostSystemID) ? EW_HOST_REFERENCE_DEFAULT : hostSystemID;
        WalletResponse wr = null;
        WalletOwner wo = client.GetWallet(wc, out wr);

        if (wr.success)
        {
          ownerID = wo.ownerID;
          Logger.cs_log(string.Format("The eWallet owner [ID: {0}] already exists; iPayment will add the item to this owner.", wo.ownerID));
          customerID = wo.ext_01;
          cardAction = CardActionEnum.AddContractToCustomer;
          bool updateItem = false;
          updateCardExpDate = false; // Bug 18129 21131 DJD - eWallet: Update Card Expiration Date for PAYware Token

          // Owner/Customer already exists; Now check eWallet to see if the item to be added already exists.
          ExistingItem = null;
          foreach (WalletItem item in wo.itemList)
          {
            bool merchantKeyMatches = string.IsNullOrEmpty(hashedMerchantKey) ? true : (item.ext_04 == hashedMerchantKey);

            // Bug 26719 DJD Bug Fix: eCheck eWallet entries were getting wrongly updated with CC data
            if ((rep == RepositoryEnum.PAYWARE_TOKEN && item.repositoryID == "PAYWARE_TOKEN" &&
            !string.IsNullOrWhiteSpace(existingPAYwareToken) && existingPAYwareToken == item.ext_01)
            || (item.maskedValue == maskedValue && merchantKeyMatches))
            {
              // Bug 27311 DJD: Update exp date for EXIPRED eWallet items
              if (item.itemStatus != WalletItem.ItemStatus.REMOVED)
              {
                // Bug 18129 21131 DJD - eWallet: Update Card Expiration Date for PAYware Token
                // Card Expiration Date needs to be modified and the repository is NOT changing [NOTE: ALWAYS update card exp date for PAYware Token]
                updateCardExpDate = (expDate.HasValue) && (rep.ToString() == item.repositoryID) && (item.expirationDate != expDate || rep == RepositoryEnum.PAYWARE_TOKEN);
                if (item.expirationDate != expDate || rep.ToString() != item.repositoryID || rep == RepositoryEnum.PAYWARE_TOKEN)
                {
                  updateItem = true; // Need to update repository, card expiration date, and/or token expiration date
                }
                ExistingItem = item;
                break;
              }
            }
          }

          if (ExistingItem != null)
          {
            Logger.cs_log(string.Format("The eWallet {0} item [ID: {1}] already exists.{2}{3}"
            , ExistingItem.itemStatus.ToString()
            , ExistingItem.itemID
            , ExistingItem.itemStatus == WalletItem.ItemStatus.INACTIVE ? " Item will be activated." : ""
            , updateCardExpDate ? " Card expiration date will be updated." : "" // Bug 18129 21131 DJD - eWallet: Update Card Expiration Date for PAYware Token
            ));

            cardAction = CardActionEnum.None; // Don't try to "AddContractToCustomer" (it already exists)
            if (ExistingItem.repositoryID == RepositoryEnum.PAYWARE.ToString())
            {
              contractID = ExistingItem.ext_02;
            }
            if (ExistingItem.repositoryID == RepositoryEnum.PAYWARE_TOKEN.ToString())
            {
              existingPAYwareToken = ExistingItem.ext_01;
            }
            if (updateItem)
            {
              cardAction = CardActionEnum.UpdateContract;
              returnToken = ExistingItem.itemID;
            }
            else
            {
              if (ExistingItem.itemStatus == WalletItem.ItemStatus.ACTIVE) // Do not try to activate contract; just return the token
              {
                existingReturnToken = ExistingItem.itemID;
              }
              else // Attempt to activate contract and then (if successful) update eWallet item status to ACTIVE
              {
                cardAction = CardActionEnum.ActivateContract;
                returnToken = ExistingItem.itemID;
              }
            }
          }
        }
        else
        {
          if (wr.errorCode != "WDB-5") // "WDB-5" means "Cannot find Wallet Owner" (All other codes are actual eWallet errors)
          {
            errMsg = string.Format("eWallet Error: {0}", wr.detailedErrorMsg);
            Logger.cs_log(errMsg);
            retValue = misc.MakeCASLError("Admin Request Failed", "PWADMIN-013", errMsg, true);
          }
        }
      }
      catch (Exception ex)
      {
        errMsg = string.Format("Error Checking For Existing Owner And Item in eWallet:{0}{1}"
        , Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
        Logger.cs_log(errMsg);
        retValue = misc.MakeCASLError("System Interface Error", "PWADMIN-014", string.Format("{0}", errMsg), true);
      }
      finally // Close the client
      {
        if (client != null)
        {
          client.Close();
        }
      }
    }

    /// <summary>
    /// Bug 18129 DJD - Support for Admin Transactions and eWallet
    /// Generates and assigns new "globally unique identifier" values for the Merchant 
    /// Customer ID and Merchant Contract ID.
    /// </summary>
    /// <param name="merchantCustomerID"></param>
    /// <param name="merchantContractID"></param>
    private static void GenerateCustomerAndContractIDs(ref string merchantCustomerID, ref string merchantContractID)
    {
      string guid = Guid.NewGuid().ToString("N");
      merchantCustomerID = string.Format("CST{0}", guid);
      merchantContractID = string.Format("CNT{0}", guid);
    }

    /// <summary>
    /// Bug 18129 DJD - Support for Admin Transactions and eWallet
    /// Instantiates and populates a "GatewayInfo" object with the values provided.
    /// Merchant Key Authentication
    /// </summary>
    /// <param name="userID"></param>
    /// <param name="userPW"></param>
    /// <param name="clientID"></param>
    /// <param name="merchantKey"></param>
    /// <param name="adminHost"></param>
    /// <param name="timeout"></param>
    /// <returns></returns>
    private static GatewayInfo PopulateGatewayInfo_MerchantKeyAuthentication(string userID, string userPW, string clientID, string merchantKey, string adminHost, int timeout)
    {
      GatewayInfo gatewayInfo = new GatewayInfo();
      gatewayInfo.Host = adminHost;
      gatewayInfo.Timeout = timeout;
      gatewayInfo.ClientID = clientID;
      gatewayInfo.MerchantKey = merchantKey;
      gatewayInfo.User = userID;
      gatewayInfo.Password = userPW;
      return gatewayInfo;
    }

    /// <summary>
    /// Bug 21009 [Bug 18129] DJD - Populates Gateway Info (UGP Protocol - Managed Device Authentication)
    /// </summary>
    /// <param name="clientID"></param>
    /// <param name="serialNum"></param>
    /// <param name="deviceType"></param>
    /// <param name="deviceKey"></param>
    /// <param name="host"></param>
    /// <param name="timeout"></param>
    /// <returns></returns>
    private static GatewayInfo PopulateGatewayInfo_ManagedDeviceAuthentication(string clientID, string serialNum, string deviceType, string deviceKey, string host, int timeout)
    {
      // Bug 18912 Added code to store the POSAPP managed device authentication values in the proper table (TG_DEVICE_ID)
      // Parse Client ID into Account, Site, and Term. For example:
      //             01234567890123 [Length=14]
      // <CLIENT_ID> 13859100020001 </CLIENT_ID>
      // Term is last four digits, Site is second to last 4 digits, and Account is the remaining leading digits:
      // <ACCOUNT>138591</ACCOUNT> <SITE>0002</SITE> <TERM>0001</TERM>
      GatewayInfo gatewayInfo = null;
      if (clientID.Length > 8)
      {
        string account = clientID.Substring(0, clientID.Length - 8);
        string site = clientID.Substring(clientID.Length - 8, 4);
        string term = clientID.Substring(clientID.Length - 4);
        gatewayInfo = PopulateGatewayInfo_ManagedDeviceAuthentication(account, site, term, serialNum, deviceType, deviceKey, host, timeout);
      }
      return gatewayInfo;
    }

    /// <summary>
    /// Bug 21009 [Bug 18129] DJD - Populates Gateway Info (UGP Protocol - Managed Device Authentication)
    /// </summary>
    /// <param name="account"></param>
    /// <param name="site"></param>
    /// <param name="term"></param>
    /// <param name="serialNum"></param>
    /// <param name="deviceType"></param>
    /// <param name="deviceKey"></param>
    /// <param name="host"></param>
    /// <param name="timeout"></param>
    /// <returns></returns>
    private static GatewayInfo PopulateGatewayInfo_ManagedDeviceAuthentication(string account, string site, string term, string serialNum, string deviceType, string deviceKey, string host, int timeout)
    {
      GatewayInfo gatewayInfo = new GatewayInfo();
      gatewayInfo.Host = host;
      gatewayInfo.Timeout = timeout;
      gatewayInfo.Account = account;
      gatewayInfo.Site = site;
      gatewayInfo.Term = term;
      gatewayInfo.SerialNumber = serialNum;
      gatewayInfo.DeviceType = deviceType;
      gatewayInfo.DeviceKey = deviceKey;
      return gatewayInfo;
    }

    /// <summary>
    /// Bug 18129 DJD - Support for Admin Transactions and eWallet
    /// Gets the masked card number and the expiration date from the CC tender object provided.
    /// </summary>
    /// <param name="tender"></param>
    /// <param name="sCc_track_info"></param>
    /// <param name="maskVal"></param>
    /// <param name="expYear"></param>
    /// <param name="expMonth"></param>
    /// <param name="expDate"></param>
    private static void GetMaskedNbrAndExpDate(GenericObject tender, SecureStringClass sCc_track_info, ref string maskVal, ref string expYear, ref string expMonth, ref DateTime? expDate)
    {
      if (sCc_track_info != null)
      {
        maskVal = GetMaskedCardNbr(sCc_track_info);
      }
      if (string.IsNullOrWhiteSpace(maskVal))
      {
        maskVal = tender.get("MaskedCardNumber", "") as string;

        // Bug 25251 DJD: Added "credit_card_nbr" lookup for Point devices
        if (string.IsNullOrWhiteSpace(maskVal))
        {
          maskVal = tender.get("credit_card_nbr", "") as string;
        }
      }
      try
      {
        expMonth = SecureStringClass.GetExpMonth("this", sCc_track_info);
        expMonth = expMonth.PadLeft(2, '0');
        expYear = SecureStringClass.GetExpYear("this", sCc_track_info);
        expYear = expYear.PadLeft(2, '0');
        expDate = expMonth == "00" ? null : GetExpirationDate(expYear, expMonth);
      }
      catch
      {
        expDate = null;
      }

      if (expDate == null)
      {
        object objExpDate = null;
        objExpDate = tender.get("exp_month", null);
        expMonth = objExpDate is string ? ((string)objExpDate).PadLeft(2, '0') : (objExpDate as int? ?? 0).ToString().PadLeft(2, '0');
        objExpDate = tender.get("exp_year", null);
        expYear = objExpDate is string ? ((string)objExpDate).PadLeft(2, '0') : (objExpDate as int? ?? 0).ToString().PadLeft(2, '0');
        expDate = GetExpirationDate(expYear, expMonth);
        if (!validateExpDate(ref expDate))
        {
          expDate = null; // Don't store exp date if not valid
        }
      }
      return;
    }

    // Bug 23150 Validate the expiration date so that invalid dates are NOT stored in eWallet DB
    // NOTE: MX915 XPI Device encrypts the year value with incorrect digits for manual entry.
    private static bool validateExpDate(ref DateTime? expDate)
    {
      bool valid = true;
      if (expDate.HasValue)
      {
        DateTime dtNow = DateTime.Now;
        DateTime dtExp = (DateTime)expDate;
        if (dtExp < dtNow)
        {
          valid = false;
        }
        if (dtExp > dtNow.AddYears(10))
        {
          valid = false;
        }
      }
      return valid;
    }

    /// <summary>
    /// Bug 21009 [Bug 18129] DJD - Support for Admin Transactions and eWallet
    /// Creates an Owner in the eWallet.  Returns true if successful; false otherwise.
    /// GenericObject retValue is set to CASL error if false is returned (error occurs).
    /// </summary>
    /// <param name="hostSystemID"></param>
    /// <param name="personID"></param>
    /// <param name="ownerID"></param>
    /// <param name="pwCustomerID"></param>
    /// <param name="cardTransInfo"></param>
    /// <param name="retValue"></param>
    /// <returns></returns>
    private static bool CreateEWalletOwner(string hostSystemID, string personID, ref string ownerID, string pwCustomerID, CardTransaction cardTransInfo,
    EWalletDemographics ownerDemographics, ref GenericObject geoEWSysInt, ref GenericObject retValue)
    {
      bool successCreateOwner = false;
      string errMsg = "";
      WalletDBServiceClient client = null;
      try
      {
        client = ConstructEWalletClient(geoEWSysInt, ref errMsg);
        if (client != null)
        {
          WalletResponse wr = null;
          WalletOwner wo = new WalletOwner();
          wo.personID = personID;
          wo.hostReference = hostSystemID;
          wo.firstName = ownerDemographics == null ? cardTransInfo.CustomerFirstname : ownerDemographics.FIRST_NAME;
          wo.lastName = ownerDemographics == null ? cardTransInfo.CustomerLastname : ownerDemographics.LAST_NAME;
          wo.address1 = ownerDemographics == null ? cardTransInfo.CustomerStreet : ownerDemographics.ADDRESS1;
          wo.address2 = ownerDemographics == null ? cardTransInfo.CustomerStreet2 : ownerDemographics.ADDRESS2;
          wo.city = ownerDemographics == null ? cardTransInfo.CustomerCity : ownerDemographics.CITY;
          wo.ownerState = ownerDemographics == null ? cardTransInfo.CustomerState : ownerDemographics.OWNER_STATE;
          wo.zip = ownerDemographics == null ? cardTransInfo.CustomerZip : ownerDemographics.ZIP;
          if (ownerDemographics != null)
          {
            wo.email = ownerDemographics.EMAIL;
          }
          wo.ext_01 = pwCustomerID;
          //DateTime? expirationDate { get; set; }
          ownerID = client.CreateWalletOwner(wo, out wr);
          successCreateOwner = wr.success;
          if (successCreateOwner)
          {
            Logger.cs_log(string.Format("SUCCESS: eWallet Owner [ID: {0}] created.", ownerID));
          }
          else // Log error
          {
            errMsg = string.Format("FAILURE: eWallet Owner NOT created:{0}{1}", Environment.NewLine, wr.detailedErrorMsg);
            Logger.cs_log(errMsg);
          }
        }
      }
      catch (Exception ex)
      {
        errMsg = string.Format("Error creating eWallet Owner [LastName {0}]:{1}{2}"
        , cardTransInfo.CustomerLastname, Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
        Logger.cs_log(errMsg);
      }
      finally // Close the client
      {
        if (client != null)
        {
          client.Close();
        }
      }
      if (!successCreateOwner) // Create CASL error (retValue)
      {
        retValue = misc.MakeCASLError("Admin Request Failed", "EWALLET-019", string.Format("{0}", errMsg), true);
      }
      return successCreateOwner;
    }

    /// <summary>
    /// Bug 21009 [Bug 18129] DJD - Support for Admin Transactions and eWallet
    /// Adds a tender item to an eWallet Owner.  Returns true if successful; false otherwise.
    /// GenericObject retValue is set to CASL error if false is returned (error occurs).
    /// </summary>
    /// <param name="tender"></param>
    /// <param name="personID"></param>
    /// <param name="hostSystemID"></param>
    /// <param name="ownerID">eWallet Owner ID sent</param>
    /// <param name="ext01">CC (PAYware Rep): PAYware Merchant Customer ID</param>
    /// <param name="ext02">CC (PAYware Rep): PAYware Merchant Contract ID, eChk: Bank Routing Number</param>
    /// <param name="sCCInfo">CC: Secure CC Info (Track Data or PAN)</param>
    /// <param name="maskNbr">CC: Masked PAN, eChk: Masked Bank Account Number</param>
    /// <param name="transInfo">Tender Transaction Info (and CC or eChk Owner Info)</param>
    /// <param name="expDate">CC: Expiration Date</param>
    /// <param name="retToken">eWallet Item ID returned</param>
    /// <param name="retValue">NULL if successful; CASL error otherwise</param>
    /// <returns></returns>
    private static bool AddItemToEWallet(ewItemType itemType, GenericObject tender, string personID, string hostSystemID, string ownerID,
    RepositoryEnum rep, string ext01, string ext02, SecureStringClass sCCInfo, string hashedMerchantKey, string maskedNbr, CardTransaction transInfo,
    DateTime? expDate, ref WalletItem addedItem, ref string retToken, ref GenericObject geoEWSysInt, ref GenericObject retValue)
    {
      bool successAddItem = false;
      string errMsg = "";
      WalletDBServiceClient client = null;
      try
      {
        client = ConstructEWalletClient(geoEWSysInt, ref errMsg);
        if (client != null)
        {
          WalletResponse wr = null;
          addedItem = new WalletItem();
          addedItem.ownerID = ownerID;
          addedItem.itemName = tender.get("ewallet_tender_nickname", "") as string; // Bug 18874
          if (string.IsNullOrEmpty(addedItem.itemName)) // Bug 18874
          {
            addedItem.itemName = tender.get("description", "") as string;
          }
          // Bug 26719 DJD Added for "Share Token With" from Gateway
          string itemShareName = tender.get("ewallet_share_item_with", "") as string;
          if (!string.IsNullOrEmpty(itemShareName))
          {
            addedItem.ext_10 = itemShareName;
          }
          addedItem.itemType = ewItemTypes[(int)itemType];
          addedItem.maskedValue = maskedNbr;

          if (sCCInfo == null)
          {
            if (itemType == ewItemType.CreditCard)
            {
              // For CC, the first six characters of maskedNbr should be the BIN
              addedItem.itemBin = maskedNbr.Substring(0, 6);
            }
          }
          else
          {
            addedItem.itemBin = SecureStringClass.GetBINValue("this", sCCInfo);
          }
          addedItem.creationDate = DateTime.Now;
          addedItem.expirationDate = expDate;
          addedItem.itemStatus = WalletItem.ItemStatus.INACTIVE; // Set to ACTIVE after contract is activated
          string tenderCategory = GetTenderCategory(tender);
          addedItem.tenderValue = string.IsNullOrEmpty(tenderCategory) ? "UNKNOWN" : tenderCategory;
          //wi.itemRank
          addedItem.repositoryID = rep.ToString();
          addedItem.ext_01 = ext01; // Set PAYware Merchant Customer ID to ext_01
          addedItem.ext_02 = ext02; // Set PAYware Merchant Contract ID to ext_02
          string tenderID = tender.get("id", "", true) as string;
          addedItem.ext_03 = string.IsNullOrEmpty(tenderID) ? null : tenderID;
          addedItem.ext_04 = hashedMerchantKey;

          PopulateWalletItemWithTenderDemographics(tender, addedItem); // Bug 25177 - DJD: eWallet Zip Handling

          WalletCredentials wc = new WalletCredentials();
          wc.PersonID = personID;
          wc.hostReference = hostSystemID;

          retToken = client.AddWalletItemToWalletOwner(wc, addedItem, out wr);

          successAddItem = wr.success;
          if (successAddItem)
          {
            addedItem.itemID = retToken;
            string maskedToken = MaskInput(retToken, 4);
            Logger.cs_log(string.Format("SUCCESS: eWallet Item [ID: {0}] created.", maskedToken));
            tender.set("ewallet_return_token", retToken);
          }
          else // Log error
          {
            errMsg = string.Format("FAILURE: eWallet item NOT added for eWallet Owner [{2}]:{0}{1}"
            , Environment.NewLine, wr.detailedErrorMsg, ownerID);
            Logger.cs_log(errMsg);
          }
        }
      }
      catch (Exception ex)
      {
        errMsg = string.Format("Error adding item to eWallet [Owner ID {0}]:{1}{2}"
        , ownerID, Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
        Logger.cs_log(errMsg);
      }
      finally // Close the client
      {
        if (client != null)
        {
          client.Close();
        }
      }
      if (!successAddItem)
      {
        retValue = misc.MakeCASLError("Admin Request Failed", "EWALLET-020", string.Format("{0}", errMsg), true);
      }
      return successAddItem;
    }

    /// <summary>
    /// Bug 18129 DJD - Adds item to CORE Repository (stored values are encryted)
    /// </summary>
    /// <param name="acctNbrSection"></param>
    /// <param name="ewToken"></param>
    /// <param name="geoEWSysInt"></param>
    /// <param name="retValue"></param>
    /// <returns></returns>
    private static bool AddItemToCORERepository(SecureString acctNbrSection, string ewToken, ref GenericObject geoEWSysInt, ref GenericObject retValue)
    {
      bool successAddItem = false;
      string errMsg = "";
      CoreRepositoryServiceClient client = null;
      try
      {
        bool encryptValues = false;
        string encryptedEWalletToken = "";
        string encryptedAcctNbrSection = "";
        if (EncryptString(ewToken, ref encryptedEWalletToken))
        {
          if (EncryptString(acctNbrSection.ConvertToUnsecureString(), ref encryptedAcctNbrSection))
          {
            encryptValues = true;
          }
          else
          {
            errMsg = string.Format("FAILURE: Unable to encrypt account number section.");
          }
        }
        else
        {
          errMsg = string.Format("FAILURE: Unable to encrypt eWallet Token.");
        }

        client = ConstructCoreRepositoryClient(geoEWSysInt, ref errMsg);
        if ((client != null) && encryptValues)
        {
          RepositoryResponse rr = null;
          RepositoryItem ri = new RepositoryItem();

          ri.ext_01 = encryptedEWalletToken; // Set eWallet token (Item ID) to ext_01 TODO: Encrypt/Hash this value!!!!
          ri.ext_02 = encryptedAcctNbrSection; // Set Card Number midsection to ext_02 TODO: Encrypt this value!!!!

          rr = client.addRepositoryItem(ri);

          successAddItem = rr.success;
          if (successAddItem)
          {
            Logger.cs_log(string.Format("SUCCESS: CORE Repository Item created."));
          }
          else // Log error
          {
            errMsg = string.Format("FAILURE: CORE Repository Item NOT added:{0}{1}", Environment.NewLine, rr.detailedErrorMsg);
            Logger.cs_log(errMsg);
          }
        }
      }
      catch (Exception ex)
      {
        errMsg = string.Format("Error adding item to CORE Repository:{0}{1}", Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
        Logger.cs_log(errMsg);
      }
      finally // Close the client
      {
        if (client != null)
        {
          client.Close();
        }
      }
      if (!successAddItem)
      {
        retValue = misc.MakeCASLError("CORE Repository Error", "EWALLET-021", string.Format("{0}", errMsg), true);
      }
      return successAddItem;
    }

    /// <summary>
    /// Bug 21009 [Bug 18129] DJD - Gets an item from the CORE Repository
    /// </summary>
    /// <param name="ewToken"></param>
    /// <param name="acctNbrSection"></param>
    /// <param name="geoEWSysInt"></param>
    /// <param name="retValue"></param>
    /// <returns></returns>
    private static bool GetItemFromCORERepository(string ewToken, ref SecureString acctNbrSection, ref GenericObject geoEWSysInt, ref GenericObject retValue)
    {
      bool successGetItem = false;
      RepositoryItem ri = null;
      string errMsg = "";
      CoreRepositoryServiceClient client = null;
      try
      {
        client = ConstructCoreRepositoryClient(geoEWSysInt, ref errMsg);
        if (client != null)
        {
          RepositoryResponse rr = new RepositoryResponse();
          string encryptedToken = ""; // Encrypt eWallet Token to find record
          if (EncryptString(ewToken, ref encryptedToken))
          {
            ri = client.getRepositoryItem(encryptedToken, out rr);

            if (rr.success)
            {
              // TODO: Decrypt EXT_02 (Bank acctNbrSection)
              string decryptedAcctNbrSection = "";
              if (DecryptString(ri.ext_02, ref decryptedAcctNbrSection))
              {
                if (string.IsNullOrEmpty(decryptedAcctNbrSection))
                {
                  errMsg = string.Format("FAILURE: Account number was not retrieved from the CORE Repository.");
                  Logger.cs_log(errMsg);
                }
                else
                {
                  foreach (char c in decryptedAcctNbrSection)
                  {
                    acctNbrSection.AppendChar(c);
                  }
                  Logger.cs_log(string.Format("SUCCESS: CORE Repository Item retrieved."));
                  successGetItem = true;
                }
              }
              else
              {
                errMsg = string.Format("FAILURE: Unable to decrypt account number.");
              }
            }
            else // Log error
            {
              errMsg = string.Format("FAILURE: CORE Repository item was NOT retrieved:{0}{1}", Environment.NewLine, rr.detailedErrorMsg);
              Logger.cs_log(errMsg);
            }
          }
          else
          {
            errMsg = string.Format("FAILURE: Unable to encrypt eWallet token.");
          }
        }
      }
      catch (Exception ex)
      {
        errMsg = string.Format("Error retrieving item to CORE Repository:{0}{1}", Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
        Logger.cs_log(errMsg);
      }
      finally // Close the client
      {
        if (client != null)
        {
          client.Close();
        }
      }
      if (!successGetItem)
      {
        retValue = misc.MakeCASLError("CORE Repository Error", "EWALLET-022", string.Format("{0}", errMsg), true);
      }
      return successGetItem;
    }

    /// <summary>
    /// Bug 21009 [Bug 18129] DJD - Decrypts a string using CORE standard decryption
    /// </summary>
    /// <param name="stringToDecrypt"></param>
    /// <param name="decryptedString"></param>
    /// <returns></returns>
    private static bool DecryptString(string stringToDecrypt, ref string decryptedString)
    {
      try
      {
        if (!string.IsNullOrEmpty(stringToDecrypt))
        {
          byte[] bValueEncrypted = Convert.FromBase64String(stringToDecrypt);
          byte[] bValueDecrypted = Crypto.symmetric_decrypt("data", bValueEncrypted, "secret_key", Crypto.get_secret_key());
          decryptedString = System.Text.Encoding.Default.GetString(bValueDecrypted);
        }
      }
      catch (Exception ex)
      {
        string errMsg = string.Format("Error decrypting string:{0}{1}{2}", stringToDecrypt, Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
        Logger.cs_log(errMsg);
        return false;
      }
      return true;
    }

    /// <summary>
    /// Bug 21009 [Bug 18129] DJD - Encrypts a string using CORE standard encryption
    /// </summary>
    /// <param name="stringToEncrypt"></param>
    /// <param name="encryptedString"></param>
    /// <returns></returns>
    private static bool EncryptString(string stringToEncrypt, ref string encryptedString)
    {
      try
      {
        if (!string.IsNullOrEmpty(stringToEncrypt))
        {
          string stringToEncryptBase64 = misc.StringToBase64String(stringToEncrypt);
          GenericObject bytes = Crypto.symmetric_encrypt("data", stringToEncryptBase64, "secret_key", Crypto.get_secret_key(), "is_ascii_string", false);
          byte[] data = (byte[])bytes.get("data");
          encryptedString = Convert.ToBase64String(data);
        }
      }
      catch (Exception ex)
      {
        string errMsg = string.Format("Error encrypting string:{0}{1}", Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
        Logger.cs_log(errMsg);
        return false;
      }
      return true;
    }

    /// <summary>
    /// Bug 18129 DJD
    /// Get eWalletInfo including itemBin (first 6 digit cc nbr) and maskedValue (masked cc nbr)
    /// by passing eWalletToken/eWallet ItemID 
    /// </summary>
    /// <param name="arg_pairs">eWalletToken</param>
    /// <returns></returns>
    public static Object GetEWalletInfo(params Object[] arg_pairs)
    {
      GenericObject geoReturn = c_CASL.c_GEO();
      GenericObject args = misc.convert_args(arg_pairs);
      string eWalletToken = (string)args.get("eWalletToken", "");
      string errMsg = "";
      WalletDBServiceClient client = null;
      try
      {
        client = ConstructEWalletClient(ref errMsg);
        if (client != null)
        {
          WalletResponse wr = null;
          WalletItem wi = client.GetWalletItem(eWalletToken, out wr);
          // Bug 26719 DJD Enhancement: Return ALL properties (not just BIN and masked acct nbr)
          if (wr.success)
          {
            geoReturn.set(
            "itemBin", wi.itemBin,
            "maskedValue", wi.maskedValue,
            "creationDate", wi.creationDate.HasValue ? ((DateTime)wi.creationDate).ToString() : "",
            "expirationDate", wi.expirationDate.HasValue ? ((DateTime)wi.expirationDate).ToString() : "",
            "ext_01", wi.ext_01,
            "ext_02", wi.ext_02,
            "ext_03", wi.ext_03,
            "ext_04", wi.ext_04,
            "ext_05", wi.ext_05,
            "ext_06", wi.ext_06,
            "ext_07", wi.ext_07,
            "ext_08", wi.ext_08,
            "ext_09", wi.ext_09,
            "ext_10", wi.ext_10,
            "itemName", wi.itemName,
            "itemRank", wi.itemRank,
            "itemStatus", wi.itemStatus.HasValue ? wi.itemStatus.ToString() : "",
            "itemType", wi.itemType,
            "ownerID", wi.ownerID,
            "removalDate", wi.removalDate.HasValue ? ((DateTime)wi.removalDate).ToString() : "",
            "repositoryID", wi.repositoryID,
            "tenderValue", wi.tenderValue
            );
          }
        }
      }
      catch (Exception ex)
      {
        errMsg = string.Format("Error getting item BIN from eWallet [Item {0}]:{1}{2}"
        , eWalletToken, Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
        Logger.cs_log(errMsg);
      }
      finally // Close the client
      {
        if (client != null)
        {
          client.Close();
        }
      }
      return geoReturn;
    }

    /// <summary>
    /// Remove eWallet Token by passing eWalletToken
    /// return "SUCCESS" if success, return error message if failed
    /// </summary>
    /// <param name="arg_pairs">eWalletToken</param>
    /// <returns></returns>
    public static string CASL_RemoveItemFromEWallet(params Object[] arg_pairs)
    {
      GenericObject geoReturn = c_CASL.c_GEO();
      GenericObject args = misc.convert_args(arg_pairs);
      String token = (string)args.get("eWalletToken");
      string err = null;
      bool bResult = RemoveItemFromEWallet(token, ref err);
      if (bResult)
        return "SUCCESS";
      else
        return err;
    }


    /// <summary>
    /// Bug 23935 - CCF Upgrade: Adding eWallet Methods to the CORE Generic Payment Gateway
    /// Update eWallet item by passing eWalletToken and new expiration date (MMYY).
    /// Item is updated in eWallet DB as well as the PAYware for PAYWARE_TOKEN repository.
    /// return "SUCCESS" if success, return error message if failed
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static string CASL_UpdateEWalletItem(params Object[] arg_pairs)
    {
      GenericObject geoReturn = c_CASL.c_GEO();
      GenericObject args = misc.convert_args(arg_pairs);
      String token = (string)args.get("eWalletToken");
      string expMonth = (string)args.get("expirationMonth", null);
      string expYear = (string)args.get("expirationYear", null);
      if (expMonth == null || expYear == null)
      {
        string expDate = (string)args.get("expirationDate", null);
        if (expDate != null && expDate.Length == 4)
        {
          expMonth = expDate.Substring(0, 2);
          expYear = expDate.Substring(2);
        }
      }
      string itemName = null;
      object objItemName = args.get("itemName", null);
      if (objItemName != c_CASL.c_opt && objItemName != null)
      {
        itemName = (string)objItemName;
      }

      // Bug 25177 - DJD: eWallet Zip Handling
      string zipCode = null;
      object objZipCode = args.get("zipCode", null);
      if (objZipCode != c_CASL.c_opt && objZipCode != null)
      {
        zipCode = (string)objZipCode;
      }

      // Bug 25177 - DJD: eWallet Address Handling
      string address = null;
      object objAddress = args.get("address", null);
      if (objAddress != c_CASL.c_opt && objAddress != null)
      {
        address = (string)objAddress;
      }

      // Bug 26719 DJD Added for "Share Token With" from Gateway
      string shareTokenName = GetStringFromArgs(args, "shareTokenName");

      string err = null;
      bool bResult = UpdateEWalletItem(token, expMonth, expYear, address, zipCode, itemName, shareTokenName, ref err);
      if (bResult)
        return "SUCCESS";
      else
        return err;
    }

    // Bug 26719 DJD Added for "Share Token With" from Gateway [New method]
    private static string GetStringFromArgs(GenericObject args, string keyName)
    {
      string retString = null;
      object objArg = args.get(keyName, null);
      if (objArg != c_CASL.c_opt && objArg != null)
      {
        retString = (string)objArg;
      }
      return retString;
    }

    /// <summary>
    /// Bug 23935 - CCF Upgrade: Adding eWallet Methods to the CORE Generic Payment Gateway
    /// Update eWallet item by passing eWalletToken, new expiration date (MMYY), and/or new zip.
    /// Item is updated in eWallet DB as well as the PAYware for PAYWARE_TOKEN repository.
    /// Bug 25177 - DJD: eWallet Zip and Address Handling [Added address and zipCode arguments]
    /// </summary>
    /// <param name="token"></param>
    /// <param name="expMonth"></param>
    /// <param name="expYear"></param>
    /// <param name="address"></param>
    /// <param name="zipCode"></param>
    /// <param name="itemName"></param>
    /// <param name="shareTokenName"></param>
    /// <param name="errMsg"></param>
    /// <returns></returns>
    public static bool UpdateEWalletItem(string token, string expMonth, string expYear, string address, string zipCode, string itemName, string shareTokenName, ref string errMsg)
    {
      bool successUpateItem = false;
      WalletDBServiceClient client = null;
      RepositoryEnum rep = RepositoryEnum.None;
      string tokenExpirationDate = null; // Bug 26819 DJD - eWallet TOKEN EXPIRED - Need to send extend token expiration date call to Payware after each use

      try
      {
        bool cardIsExpired = false; // Bug 27311 DJD
        client = ConstructEWalletClient(ref errMsg);
        if (client != null)
        {
          WalletResponse wr = null;
          WalletItem wi = client.GetWalletItem(token, out wr);
          if (!wr.success && wi != null) { cardIsExpired = wi.itemStatus == WalletItem.ItemStatus.EXPIRED; } // Bug 27311 DJD
          if (wr.success || cardIsExpired) // Bug 27311 DJD
          {
            // First, update PAYware token for the PAYWARE_TOKEN repository
            bool PAYwareUpdateSuccess = false;

            // Bug 26819 DJD: Get the configured "payware_token_renewal_days" for calculating the new token expiration date
            int renewDays = 720;
            GenericObject geoEWSysInt = GetEWalletSysInt();
            if (geoEWSysInt != null)
            {
              object theObj = geoEWSysInt.get("payware_token_renewal_days", 0);
              renewDays = theObj is string ? Convert.ToInt32(theObj) : (theObj is int ? (int)theObj : 730);
              if (renewDays <= 20) { renewDays = 720; }
            }

            rep = GetRepositoryFromItem(wi);
            if (rep == RepositoryEnum.PAYWARE_TOKEN)
            {
              string PAYwareToken = wi.ext_01;
              CardTransaction cardTransTokenUpdate = new CardTransaction();
              cardTransTokenUpdate.Action = CardActionEnum.TokenUpdate;
              cardTransTokenUpdate.Token = PAYwareToken;
              cardTransTokenUpdate.ExpMonth = expMonth;
              cardTransTokenUpdate.ExpYear = expYear;
              if (!string.IsNullOrWhiteSpace(zipCode)) // Bug 25177 - DJD: eWallet Zip Handling
              {
                zipCode = MakeZipValidForWallet(zipCode);
                cardTransTokenUpdate.TokenZip = zipCode;
              }
              if (!string.IsNullOrWhiteSpace(address)) // Bug 25177 - DJD: eWallet Address Handling
              {
                cardTransTokenUpdate.TokenAddress = address;
              }

              // Bug 26819 DJD: Renew the token expiration date with the update
              DateTime tokenExpDT = DateTime.Now.AddDays(renewDays);
              tokenExpirationDate = tokenExpDT.ToString("MMddyyyy");
              cardTransTokenUpdate.TokenExpDate = tokenExpirationDate;

              // Create and send token update request
              ReqSender reqSender = new ReqSender(); // Currently unused

              // Get the gateway info
              GenericObject geoPWCSysInt = GetConfiguredSystemInterface("IPCharge");
              if (geoPWCSysInt == null)
              {
                Logger.cs_log(string.Format(@"eWallet Config Error: The PAYWARE_TOKEN repository requires configuration of IPCHARGE system interface."));
              }
              else
              {
                String host = (string)geoPWCSysInt.get("host");
                int timeout = (int)geoPWCSysInt.get("timeout", 9000); // Bug 16285 timeout is now in secs; convert to ms
                ManagedDeviceAuthentication mdAuth = new ManagedDeviceAuthentication(geoPWCSysInt);
                GatewayInfo gatewayInfo = PopulateGatewayInfo_ManagedDeviceAuthentication(mdAuth.clientID, mdAuth.serialNum, mdAuth.deviceType, mdAuth.deviceKey, host, timeout);
                if (gatewayInfo == null)
                {
                  Logger.cs_log(string.Format(@"eWallet Config Error: The PAYWARE_TOKEN repository requires configuration of IPCHARGE system interface properties [Enterprise CC Client ID, Enterprise CC Serial Num, Enterprise CC Device Type] used for the persistent token processing."));
                }
                else
                {
                  PAYwareConnectTrans PWCardTransTokenUpdate = new PAYwareConnectTrans(reqSender, gatewayInfo, cardTransTokenUpdate, null);
                  PAYwareUpdateSuccess = PWCardTransTokenUpdate.BuildSendRequest();
                }
              }
            }
            else
            {
              PAYwareUpdateSuccess = true; // Not PAYware Token - Just update eWallet DB
            }

            // Next, update the eWallet database if PAYware was updated successfully
            if (PAYwareUpdateSuccess)
            {
              if (!string.IsNullOrWhiteSpace(itemName))
              {
                wi.itemName = itemName;
              }

              // Bug 26819 DJD: Update the token expiration date in eWallet DB
              if (!string.IsNullOrWhiteSpace(tokenExpirationDate))
              {
                wi.ext_02 = tokenExpirationDate;
              }

              // Bug 26719 DJD Added for "Share Token With" from Gateway
              if (!string.IsNullOrWhiteSpace(shareTokenName))
              {
                wi.ext_10 = shareTokenName;
              }

              // Bug 25177 - DJD: eWallet Address Handling
              if (!string.IsNullOrWhiteSpace(address))
              {
                wi.ext_06 = address;
              }

              // Bug 25177 - DJD: eWallet Zip Handling
              if (!string.IsNullOrWhiteSpace(zipCode))
              {
                wi.ext_09 = zipCode;
              }

              // Bug 26719 DJD Don't update exp date for ACH (eCheck)
              DateTime? expDate = GetExpirationDate(expYear, expMonth);
              if ((expDate.HasValue) && (wi.itemType == "Credit Card"))
              {
                wi.expirationDate = expDate;
              }

              wr = null;
              client.ModifyWalletItem(token, wi, out wr);
              successUpateItem = wr.success;

              // Bug 27311 DJD: If wallet item was modified successfully then update the status to active if needed
              if (successUpateItem && (cardIsExpired || wi.itemStatus != WalletItem.ItemStatus.ACTIVE))
              {
                wr = null;
                client.SetItemStatus(token, WalletItem.ItemStatus.ACTIVE, out wr);
                successUpateItem = wr.success;
              }

              if (successUpateItem)
              {
                string maskedToken = MaskInput(token, 4);
                Logger.cs_log(string.Format("SUCCESS: eWallet Item [ID: {0}] updated.", maskedToken));
              }
              else // Log error
              {
                string maskedToken = MaskInput(token, 4);
                errMsg = string.Format("FAILURE: eWallet item [ID: {0}] was NOT updated:{1}{2}"
                , maskedToken, Environment.NewLine, wr.detailedErrorMsg);
                Logger.cs_log(errMsg);
              }
            }
          }
          else
          {
            string maskedToken = MaskInput(token, 4);
            errMsg = string.Format("FAILURE: eWallet item [ID: {0}] was NOT found for updating expiration date:{1}{2}"
            , maskedToken, Environment.NewLine, wr.detailedErrorMsg);
            Logger.cs_log(errMsg);
          }
        }
      }
      catch (Exception ex)
      {
        errMsg = string.Format("Error updating item in eWallet [Item {0}]:{1}{2}"
        , token, Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
        Logger.cs_log(errMsg);
      }
      finally // Close the client
      {
        if (client != null)
        {
          client.Close();
        }
      }
      return successUpateItem;
    }

    /// <summary>
    /// Bug 25177 - DJD: Convert Zip to valid value (remove dash and make certain max length is 9) for eWallet
    /// </summary>
    /// <param name="zipCode"></param>
    /// <returns></returns>
    private static string MakeZipValidForWallet(string zipCode)
    {
      string zipValid = zipCode.Replace("-", "").Trim(); // Remove any dashes and trim
      zipValid = zipValid.Truncate(9); // Make certain length does not exceed 9
      return zipValid;
    }

    /// <summary>
    /// Bug 18129 DJD - Support for Admin Transactions and eWallet
    /// Removes an item from the eWallet
    /// </summary>
    /// <param name="token"></param>
    /// <returns></returns>
    public static bool RemoveItemFromEWallet(string token)
    {
      string err = null;
      return RemoveItemFromEWallet(token, ref err);
    }

    /// <summary>
    /// Bug 18129 DJD - Support for Admin Transactions and eWallet
    /// Removes an item from the eWallet. Returns true if successful; false otherwise.
    /// If false, an error message is set in errMsg.
    /// </summary>
    /// <param name="token"></param>
    /// <param name="err"></param>
    /// <returns></returns>
    public static bool RemoveItemFromEWallet(string token, ref string errMsg)
    {
      bool successRemoveItem = false;
      WalletDBServiceClient client = null;
      try
      {
        client = ConstructEWalletClient(ref errMsg);
        if (client != null)
        {
          RepositoryEnum rep = RepositoryEnum.None;
          WalletResponse wr = null;
          WalletItem wi = client.GetWalletItem(token, out wr);
          if (wr.success)
          {
            rep = GetRepositoryFromItem(wi);
          }
          else
          {
            string maskedToken = MaskInput(token, 4);
            errMsg = string.Format("FAILURE: eWallet item [ID: {0}] was NOT found for removal:{1}{2}"
            , maskedToken, Environment.NewLine, wr.detailedErrorMsg);
            Logger.cs_log(errMsg);
          }

          wr = null;
          client.RemoveWalletItem(token, out wr);
          successRemoveItem = wr.success;
          if (successRemoveItem)
          {
            string maskedToken = MaskInput(token, 4);
            Logger.cs_log(string.Format("SUCCESS: eWallet Item [ID: {0}] removed.", maskedToken));
          }
          else // Log error
          {
            string maskedToken = MaskInput(token, 4);
            errMsg = string.Format("FAILURE: eWallet item [ID: {0}] was NOT removed:{1}{2}"
            , maskedToken, Environment.NewLine, wr.detailedErrorMsg);
            Logger.cs_log(errMsg);
          }

          // Remove the item from the CORE Repository
          if (successRemoveItem && rep == RepositoryEnum.CORE)
          {
            switch (rep)
            {
              case RepositoryEnum.CORE:
                CoreRepositoryServiceClient repClient = ConstructCoreRepositoryClient(ref errMsg);
                if (repClient != null)
                {
                  bool removeRepItem = false;
                  RepositoryResponse rr = null;
                  string encryptedEWalletToken = "";
                  if (EncryptString(token, ref encryptedEWalletToken))
                  {
                    rr = repClient.removeRepositoryItem(encryptedEWalletToken);
                    removeRepItem = rr.success;
                  }
                  if (!removeRepItem)
                  {
                    string maskedToken = MaskInput(token, 4);
                    errMsg = string.Format("FAILURE: eWallet item [ID: {0}] was NOT removed from CORE repository.{1}{2}"
                    , maskedToken, Environment.NewLine, rr == null ? "" : rr.detailedErrorMsg);

                    Logger.cs_log(errMsg);
                  }
                }
                break;

              case RepositoryEnum.PAYWARE_TOKEN:
                string PAYwareToken = wi.ext_01;
                // TODO: Delete the PAYware token
                break;
            }
          }
        }
      }
      catch (Exception ex)
      {
        errMsg = string.Format("Error removing item from eWallet [Item {0}]:{1}{2}"
        , token, Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
        Logger.cs_log(errMsg);
      }
      finally // Close the client
      {
        if (client != null)
        {
          client.Close();
        }
      }
      return successRemoveItem;
    }

    /// <summary>
    /// Bug 18129 DJD - Support for Admin Transactions and eWallet
    /// Sets an eWallet item's status to active. Returns true if successful; false otherwise.
    /// GenericObject retValue is set to CASL error if false is returned (error occurs).
    /// </summary>
    /// <param name="returnToken"></param>
    /// <param name="retValue"></param>
    /// <returns></returns>
    private static bool SetEWalletItemToActive(string returnToken, ref GenericObject geoEWSysInt, ref GenericObject retValue)
    {
      string errMsg = null;
      bool successItemActive = false;
      WalletDBServiceClient client = null;
      try
      {
        client = ConstructEWalletClient(geoEWSysInt, ref errMsg);
        if (client != null)
        {
          WalletResponse wr = null;
          client.SetItemStatus(returnToken, WalletItem.ItemStatus.ACTIVE, out wr);
          successItemActive = wr.success;
          if (successItemActive)
          {
            string maskedToken = MaskInput(returnToken, 4);
            Logger.cs_log(string.Format("SUCCESS: eWallet item [ID: {0}] status was modified to {1}."
            , maskedToken
            , WalletItem.ItemStatus.ACTIVE.ToString()));
          }
          else
          {
            string maskedToken = MaskInput(returnToken, 4);
            errMsg = string.Format("FAILURE: eWallet item [ID: {0}] status was NOT modified to {1}:{2}{3}"
            , maskedToken
            , WalletItem.ItemStatus.ACTIVE.ToString()
            , Environment.NewLine
            , wr.detailedErrorMsg);
            Logger.cs_log(errMsg);
          }
        }
      }
      catch (Exception ex)
      {
        string maskedToken = MaskInput(returnToken, 4);
        errMsg = string.Format("Error setting eWallet item [ID: {0}] active:{1}{2}"
        , maskedToken, Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
        Logger.cs_log(errMsg);
      }
      finally // Close the client
      {
        if (client != null)
        {
          client.Close();
        }
      }
      if (!successItemActive)
      {
        retValue = misc.MakeCASLError("Admin Request Failed", "EWALLET-023", string.Format("{0}", errMsg), true);
      }
      return successItemActive;
    }

    /// <summary>
    /// Generic method to mask strings
    /// </summary>
    /// <param name="input"></param>
    /// <param name="charactersToShowAtEnd"></param>
    /// <returns></returns>
    static private string MaskInput(String input, int charactersToShowAtEnd)
    {
      if (input.Length < charactersToShowAtEnd)
      {
        charactersToShowAtEnd = input.Length;
      }
      string endCharacters = input.Substring(input.Length - charactersToShowAtEnd);
      return string.Format("{0}{1}"
      , "".PadLeft(input.Length - charactersToShowAtEnd, '*')
      , endCharacters
      );
    }

    /// <summary>
    /// Bug 18129 DJD - Support for Admin Transactions and eWallet
    /// Mask card number so that BIN and last four are exposed: 123456******1234
    /// </summary>
    /// <param name="sCc_track_info"></param>
    /// <returns></returns>
    private static string GetMaskedCardNbr(SecureStringClass sCc_track_info)
    {
      string retMaskedValue = null;
      try
      {
        if (sCc_track_info == null)
        {
          return null;
        }
        retMaskedValue = SecureStringClass.GetMaskedCreditCardNumber("this", sCc_track_info);
        if (retMaskedValue != null && retMaskedValue.Length > 6 && retMaskedValue.Substring(0, 6) == "******")
        {
          string binVal = null;
          binVal = SecureStringClass.GetBINValue("this", sCc_track_info) as string;
          if (binVal != null && binVal.Length == 6)
          {
            retMaskedValue = string.Format("{0}{1}", binVal, retMaskedValue.Substring(6));
          }
        }
      }
      catch (Exception ex)
      {
        Logger.cs_log(string.Format("Error getting Masked Card Nbr:{0}{1}", Environment.NewLine, ex.ToMessageAndCompleteStacktrace()));
      }
      return retMaskedValue;
    }

    /// <summary>
    /// Bug 18129 DJD - Support for Admin Transactions and eWallet
    /// Converts two-digit strings of the expiration year and month to a DateTime? object.
    /// </summary>
    /// <param name="sExpYear"></param>
    /// <param name="sExpMonth"></param>
    /// <returns></returns>
    private static DateTime? GetExpirationDate(string sExpYear, string sExpMonth)
    {
      DateTime? expDate = null;
      try
      {
        if (sExpMonth != "00" && IsDigitsOnly(sExpYear) && IsDigitsOnly(sExpMonth))
        {
          int nExpYear = 2000 + Int32.Parse(sExpYear);
          int nExpMonth = Int32.Parse(sExpMonth);
          int nExpDay = System.DateTime.DaysInMonth(nExpYear, nExpMonth);
          expDate = new DateTime(nExpYear, nExpMonth, nExpDay);
        }
      }
      catch (Exception ex)
      {
        Logger.cs_log(string.Format("Error getting Expiration DateTime from Month: '{0}' and Year: '{1}'.{2}{3}"
        , sExpMonth, sExpYear, Environment.NewLine, ex.ToMessageAndCompleteStacktrace()));
        expDate = null;
      }
      return expDate;
    }

    // Bug 23150 DJD: Add method to determine if a string contains all digits
    private static bool IsDigitsOnly(string str)
    {
      if (string.IsNullOrWhiteSpace(str))
      {
        return false;
      }
      foreach (char c in str)
      {
        if (!IsDigit(c))
        {
          return false;
        }
      }
      return true;
    }

    // Bug 23150 DJD: Add method to determine if a character is a digit
    public static bool IsDigit(char c)
    {
      if (c < '0' || c > '9')
      {
        return false;
      }
      else
      {
        return true;
      }
    }

    /// <summary>
    /// Bug 18129 DJD - EWallet: Finds an eCheck tender associated with a Department
    /// Returns the first eCheck tender found.
    /// Returns false if it is not found or there is an error.
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static object GetECheckTenderForDepartment(params Object[] arg_pairs)
    {
      string errMsg = "";
      object tenderType = false;
      try
      {
        GenericObject args = misc.convert_args(arg_pairs);
        GenericObject dept = args.get("dept", null) as GenericObject;
        GenericObject tenders = dept.get("tenders", null) as GenericObject;
        foreach (DictionaryEntry de in tenders)
        {
          object tender = de.Value;
          if ((tender is GenericObject) && (GetTenderCategory((GenericObject)tender) == "Check"))
          {
            // Bug 26881 DJD: Paying With eWallet Items in Cashiering [eCheck]
            GenericObject parent = ((GenericObject)tender).get("_parent", null) as GenericObject;
            if (parent != null && parent is GenericObject)
            {
              bool? e_check = (parent.get("_name", "") as string) == "Check_ECheck";
              if (e_check.HasValue && (bool)e_check)
              {
                tenderType = tender;
                break;
              }
            }
          }
        }
      }
      catch (Exception ex)
      {
        errMsg = string.Format("Error Getting eCheck Tender For Department:{0}{1}"
        , Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
        Logger.cs_log(errMsg);
        tenderType = false;
      }
      return tenderType;
    }

    /// <summary>
    /// Bug 21009 [Bug 18129] eWallet Support 
    /// Gets an eWallet [Owner & Items] from the eWallet web service based on the transaction sent.  If found, sets the 
    /// eWallet Owner & Items to a Generic Object and returns it.  Otherwise, returns a CASL error.
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static object CASL_ewallet_retrieve(params Object[] arg_pairs)
    {
      string errMsg = "";
      GenericObject geoWallet = c_CASL.c_GEO();
      geoWallet.set("WalletFound", false);
      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject sysInt = args.get("SI", null) as GenericObject;
      GenericObject transaction = args.get("a_core_item", null) as GenericObject;
      GenericObject dept = args.get("this_dept", null) as GenericObject; // Bug 21009 [Bug 18129] eWallet Support

      if (transaction != null)
      {
        string hostIDTag = transaction.get("host_system_ID_tagname", "", true) as string;
        string personIDTags = transaction.get("person_ID_tagname", "", true) as string;

        if (!string.IsNullOrEmpty(personIDTags))
        {
          string hostID = string.IsNullOrEmpty(hostIDTag) ? EW_HOST_REFERENCE_DEFAULT : (transaction.get(hostIDTag, EW_HOST_REFERENCE_DEFAULT) as string); // Bug 21009 [Bug 18129] eWallet Support
          string personIDs = "";
          MerchantKeyAuthentication pwAuth = dept == null ? null : new MerchantKeyAuthentication(dept);
          string hashedMerchantKey = pwAuth == null ? null : pwAuth.GetHashedMerchantKey();

          // Allow Person ID Tags to be a comma-delimited list of tags (i.e., more than 1 data element makes up the ID)
          bool personIDValuesFound = true;
          string missingPersonIDTag = "";
          string[] personIDTagArray = personIDTags.Split(',');
          int i = 0;
          foreach (string personIDTag in personIDTagArray)
          {
            i++;
            string personID = transaction.get(personIDTag, "") as string;
            if (string.IsNullOrEmpty(personID))
            {
              personIDValuesFound = false;
              missingPersonIDTag = personIDTag;
              break;
            }
            personIDs += string.Format("{0}{1}", personID, personIDTagArray.Length == i ? "" : ",");
          }
          if (!string.IsNullOrEmpty(hostID) && personIDValuesFound)
          {
            WalletDBServiceClient client = null;
            try
            {
              client = sysInt == null ? ConstructEWalletClient(ref errMsg) : ConstructEWalletClient(sysInt, ref errMsg);
              if (client == null)
              {
                geoWallet = misc.MakeCASLError("System Interface Error", "PWADMIN-015", string.Format("{0}", errMsg), true);
                return geoWallet;
              }

              // Check eWallet web service to see if the Owner's wallet exists
              WalletCredentials wc = new WalletCredentials();
              wc.PersonID = personIDs;
              wc.hostReference = hostID;
              WalletResponse wr = null;
              WalletOwner wo = client.GetWallet(wc, out wr);
              if (wr.success)
              {
                SetWalletOwnerAndItemsToGEO(geoWallet, wo, hashedMerchantKey);
              }
            }
            catch (Exception ex)
            {
              errMsg = string.Format("Error Checking For Existing Owner And Item in eWallet:{0}{1}"
              , Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
              Logger.cs_log(errMsg);
              geoWallet = misc.MakeCASLError("System Interface Error", "PWADMIN-016", string.Format("{0}", errMsg), true);
            }
            finally // Close the client
            {
              if (client != null)
              {
                client.Close();
              }
            }
          }
          else
          {
            errMsg = string.Format("eWallet Error: Transaction missing {0}."
            , personIDValuesFound ? ("Host ID value: " + hostIDTag) : ("Person ID value: " + missingPersonIDTag));
            Logger.cs_log(errMsg);
            geoWallet = misc.MakeCASLError("System Interface Error", "PWADMIN-017", string.Format("{0}", errMsg), true);
          }
        }
        else
        {
          errMsg = string.Format("eWallet Config Error: Transaction missing {0} Tag."
          , string.IsNullOrEmpty(personIDTags) ? "Person ID" : "Host ID");
          Logger.cs_log(errMsg);
          geoWallet = misc.MakeCASLError("System Interface Error", "PWADMIN-018", string.Format("{0}", errMsg), true);
        }
        return geoWallet;
      }
      return geoWallet;
    }

    /// <summary>
    /// Bug 23935 - CCF Upgrade: Adding eWallet Methods to the CORE Generic Payment Gateway
    /// NOTE: This is for use with the PAYWARE_TOKEN repository
    /// Gets an eWallet [Owner & Items] from the eWallet web service based on the individual ID and host reference sent.  If found, sets the 
    /// eWallet Owner & Items to a Generic Object and returns it.  Otherwise, returns a CASL error.
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static object CASL_GetEWallet(params Object[] arg_pairs)
    {
      string errMsg = "";
      GenericObject geoWallet = c_CASL.c_GEO();
      geoWallet.set("WalletFound", false);

      GenericObject args = misc.convert_args(arg_pairs);
      string individualID = (string)args.get("individualID", "");
      string hostID = EW_HOST_REFERENCE_DEFAULT;
      object objHost = args.get("hostReference", c_CASL.c_opt);
      if (!c_CASL.c_opt.Equals(objHost))
      {
        hostID = (string)objHost;
      }

      if (!string.IsNullOrWhiteSpace(hostID) && !string.IsNullOrWhiteSpace(individualID))
      {
        WalletDBServiceClient client = null;
        try
        {
          client = ConstructEWalletClient(ref errMsg);
          if (client == null)
          {
            geoWallet = misc.MakeCASLError("Constructing EWallet Client Error", "PWADMIN-019", string.Format("{0}", errMsg), true);
            return geoWallet;
          }

          // Check eWallet web service to see if the Owner's wallet exists
          WalletCredentials wc = new WalletCredentials();
          wc.PersonID = individualID;
          wc.hostReference = hostID;
          WalletResponse wr = null;
          WalletOwner wo = client.GetWallet(wc, out wr);
          if (wr.success)
          {
            SetWalletOwnerAndItemsToGEO(geoWallet, wo, null);
          }
        }
        catch (Exception ex)
        {
          errMsg = string.Format("Error Checking For Owner in eWallet:{0}{1}", Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
          Logger.cs_log(errMsg);
          geoWallet = misc.MakeCASLError("System Interface Error", "PWADMIN-016", string.Format("{0}", errMsg), true);
        }
        finally // Close the client
        {
          if (client != null)
          {
            client.Close();
          }
        }
      }
      return geoWallet;
    }

    /// <summary>
    /// Bug 21009 [Bug 18129] DJD - Sets eWallet Owner and Items to Generic Object (skips items if
    /// MerchantKey does not match when repository is PAYware).
    /// </summary>
    /// <param name="geoWallet"></param>
    /// <param name="wo"></param>
    /// <param name="hashedMerchantKey"></param>
    private static void SetWalletOwnerAndItemsToGEO(GenericObject geoWallet, WalletOwner wo, string hashedMerchantKey)
    {
      geoWallet.set("WalletFound", true);
      geoWallet.set("OwnerID", wo.ownerID);
      geoWallet.set("PersonID", wo.personID);
      geoWallet.set("HostReference", wo.hostReference);
      geoWallet.set("FirstName", wo.firstName);
      geoWallet.set("MiddleInitial", wo.mi);
      geoWallet.set("LastName", wo.lastName);
      geoWallet.set("Address1", wo.address1);
      geoWallet.set("Address2", wo.address2);
      geoWallet.set("City", wo.city);
      geoWallet.set("OwnerState", wo.ownerState);
      geoWallet.set("Zip", wo.zip);
      geoWallet.set("Email", wo.email);

      GenericObject geoItemList = c_CASL.c_instance("vector") as GenericObject;//Bug23935 use vector instead of GEO for easy JSON format

      foreach (WalletItem wi in wo.itemList)
      {
        // If the repository is PAYware then the MerchantKey must match the "ext_04" value stored with the
        // item (Customer/Contracts are specific to a Merchant). Do not add the item if it does NOT match.
        bool addItem = true;

        if (wi.repositoryID == RepositoryEnum.PAYWARE.ToString() &&
        !string.IsNullOrEmpty(wi.ext_04) &&
        !string.IsNullOrEmpty(hashedMerchantKey) &&
        wi.ext_04 != hashedMerchantKey)
        {
          addItem = false;
        }

        if (addItem)
        {
          GenericObject geoItem = c_CASL.c_GEO();
          geoItem.set("ItemID", wi.itemID);
          geoItem.set("TenderName", wi.itemName);
          geoItem.set("ItemType", wi.itemType);
          geoItem.set("MaskedValue", wi.maskedValue);
          geoItem.set("CreationDate", wi.creationDate.HasValue ? ((DateTime)wi.creationDate).ToString("MM/dd/yyyy") : "");
          geoItem.set("ExpirationDate", wi.expirationDate.HasValue ? ((DateTime)wi.expirationDate).ToString("MM/dd/yyyy") : "");
          geoItem.set("Status", wi.itemStatus.HasValue ? wi.itemStatus.ToString() : "");
          geoItem.set("TenderCategory", wi.tenderValue);
          geoItem.set("ItemRank", wi.itemRank);
          geoItem.set("RepositoryID", wi.repositoryID);
          geoItem.set("Ext01", wi.ext_01);     // PAYWARE_TOKEN: PAYware persistent token value, eCheck: Acct Type (Saving, Checking)
          geoItem.set("Ext02", wi.ext_02);     // PAYWARE_TOKEN: Token expiration date, eCheck: Routing Number
          geoItem.set("TenderID", wi.ext_03);  // Tender ID
          geoItem.set("Ext04", wi.ext_04);     // PAYWARE: MerchantKey (deprecated)
          geoItem.set("Ext05", wi.ext_05);     // CC Tender: Card Owner Name (if provided)
          geoItem.set("Ext06", wi.ext_06);     // CC Tender: Card Owner Address (if provided)
          geoItem.set("Ext07", wi.ext_07);     // CC Tender: Card Owner City (if provided)
          geoItem.set("Ext08", wi.ext_08);     // CC Tender: Card Owner State (if provided)
          geoItem.set("Ext09", wi.ext_09);     // CC Tender: Card Owner Zip Code (if provided)
          geoItem.set("Ext10", wi.ext_10);
          geoItem.set("ItemBIN", wi.itemBin);
          geoItem.set("TenderCategory", wi.tenderValue); // Bug 26881 DJD: Paying With eWallet Items in Cashiering [Credit Card]
          geoItemList.vectors.Add(geoItem);
        }
      }
      geoWallet.set("ItemList", geoItemList);
    }

    /// <summary>
    /// Bug 18129 DJD - Support for eWallet Admin and Token Transaction [***** eCheck & Credit Card Tenders *****]
    /// NOTE: The "eWallet Update" system interface should NOT be called for Credit Card tenders for zero-dollar transactions because 
    /// the eWallet update for CCs works with (piggy-backs on) the PAYware Connect (IPCharge) update.
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static GenericObject eWalletUpdate(params Object[] arg_pairs)
    {
      const string sERR_MSG = "The payment item was NOT added the Wallet.  Please contact Administrator for help.";
      GenericObject return_value = null;

      try
      {
        GenericObject args = misc.convert_args(arg_pairs);
        GenericObject tender = args.get("tender") as GenericObject;
        GenericObject core_event = args.get("this_event") as GenericObject;
        processEWalletUpdate(ref tender, core_event, ref return_value);
      }
      catch (Exception ex)
      {
        string errMsg = string.Format("Error Performing eWalletUpdate:{0}{1}", Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
        Logger.cs_log(errMsg);
        return_value = misc.MakeCASLError("eWallet Update Error", "EWALLETUPD-004", string.Format("CARDERROR {0}", errMsg), true);
      }

      if (misc.base_is_a(return_value, c_CASL.c_error)) // This is a CASL Error - Return message to be displayed (but do NOT return an error or tender will not post).
      {
        Logger.cs_log_genobj("log_content", return_value);
        return_value = new GenericObject("msg", sERR_MSG);
      }

      return return_value;
    }

    public static void processEWalletUpdate(ref GenericObject tender, GenericObject core_event, ref GenericObject return_value)
    {
      string errMsg = "";
      ewItemType itemType = misc.base_is_a(tender, c_CASL.c_object("Tender.Credit_card")) ? ewItemType.CreditCard :
      (misc.base_is_a(tender, c_CASL.c_object("Tender.Check")) ? ewItemType.eCheck : ewItemType.None);

      // Currently, only Credit Card or eCheck tenders should be applied to this System Interface
      if (itemType == ewItemType.None)
      {
        errMsg = "The tender assigned to eWallet Update System Interface must be of type: Tender.Credit_card or Tender.Check.";
        return_value = misc.MakeCASLError("eWallet Update Error", "EWALLETUPD-001", string.Format("CARDERROR {0}", errMsg), true);
        return;
      }

      decimal dTotal = (decimal)tender.get("total");
      bool eWalletAdminTrans = false;
      bool eWalletTokenTrans = false;
      RepositoryEnum eWalletRepository = RepositoryEnum.None;
      GenericObject eWalletSysInt = null;
      // List of eWallet Commands to be executed (based on "PAYware Admin AddOn" commands to keep things consistent)
      List<CardActionEnum> adminCommandList = new List<CardActionEnum>();
      string eWalletHostSystemID = ""; // Identifies the host system using the wallet
      string eWalletPersonID = ""; // Identifies the eWallet person (MRN, SSN, etc...)
      string eWalletToken = ""; // Used for a eCheck transaction (Sale, Void) that uses the eWallet

      // ***** BEGIN Bug 18129 DJD - Support for eWallet Token ADMIN Functions *****
      // NOTE: To keep things consistent and simple, the PAYware Connect Admin commands (payware_admin_command) are
      // used to create eWallet owner/items EVEN when the tender is eCheck and the repository is CORE (not PAYware).
      string acString = GetEWalletAdminCommands(tender, core_event, adminCommandList, ref errMsg);
      if (!string.IsNullOrEmpty(errMsg))
      {
        return_value = misc.MakeCASLError("eWallet Update Error", "EWALLETUPD-002", string.Format("CARDERROR {0}", errMsg), true);
        return;
      }

      eWalletAdminTrans = (adminCommandList.Count > 0);
      if (itemType == ewItemType.CreditCard && !eWalletAdminTrans) // If this is a Credit Card update with NO Admin Wallet Commands - set return to NULL
      {
        return_value = null;
        return;
      }

      eWalletHostSystemID = tender.get("host_system_id", "", true) as string;
      eWalletPersonID = tender.get("host_system_individual_id", "") as string;
      if (!eWalletAdminTrans)
      {
        eWalletToken = tender.get("ewallet_token", "") as string;
        eWalletTokenTrans = !string.IsNullOrEmpty(eWalletToken);
      }

      // Bug 26719 DJD Update eWallet and Gateway Processing
      if (!eWalletAdminTrans && !eWalletTokenTrans)
      {
        return; // There's nothing to update
      }

      // Get the eWallet System Interface and repository
      return_value = GetEWalletSysIntAndRepository(eWalletAdminTrans, eWalletTokenTrans, ref eWalletSysInt, ref eWalletRepository);
      if (return_value != null)
      {
        return;
      }
      if (itemType == ewItemType.eCheck) // The eCheck tender ALWAYS uses the CORE repository
      {
        eWalletRepository = RepositoryEnum.CORE;
      }

      string sValidateToken = eWalletSysInt.get("require_token_validation", "Y") as string;
      bool validateToken = (sValidateToken == "Y");

      // Check transaction(s) for the eWalletPersonID [and eWalletHostSystemID] if eWalletPersonID is missing and this is an Admin Trans
      if (string.IsNullOrEmpty(eWalletPersonID) && eWalletAdminTrans)
      {
        GetPersonIDAndHostIDFromEvent(core_event, ref eWalletHostSystemID, ref eWalletPersonID); // Bug 25251 DJD: Added GetPersonIDAndHostIDFromEvent method
      }

      // Make certain that the required Person ID is provided for an eWallet Admin or Token transaction
      if (string.IsNullOrEmpty(eWalletPersonID))
      {
        if (eWalletAdminTrans || (eWalletTokenTrans && validateToken))
        {
          errMsg = string.Format("The eWallet {0} transaction is missing a required data element: Person ID.", eWalletAdminTrans ? "Admin" : "Token");
          Logger.cs_log(errMsg);
          return_value = misc.MakeCASLError("eWallet Update Error", "EWALLETUPD-003", string.Format("CARDERROR {0}", errMsg), true);
          return;
        }
      }

      // If adding a new wallet owner - check the transaction(s) for demographic data
      EWalletDemographics ownerDemographics = null;
      if (adminCommandList.Contains(CardActionEnum.NewCustomerWithContract))
      {
        GenericObject trans = core_event.get("transactions", new GenericObject()) as GenericObject;
        int iCountofTransactions = trans.getIntKeyLength();
        for (int i = 0; i < iCountofTransactions; i++)
        {
          GenericObject tran = (GenericObject)trans.get(i);
          // Make certain tran was not voided or cancelled
          if (ItemHasValidStatus(tran))
          {
            ownerDemographics = new EWalletDemographics(tran);
            if (!ownerDemographics.hasTranValues)
            {
              ownerDemographics = null;
            }
            if (ownerDemographics != null)
            {
              break;
            }
          }
        }
      }

      if (eWalletAdminTrans) // eWallet ADMIN Functions (e.g., Adding or creating owner and item)
      {
        try
        {
          return_value = PerformAdminTransactions(tender, itemType, eWalletRepository, adminCommandList, eWalletHostSystemID, eWalletPersonID, ref eWalletSysInt, ownerDemographics);
        }
        catch (Exception ex)
        {
          errMsg = string.Format("Error Performing Admin Transactions:{0}{1}", Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
          Logger.cs_log(errMsg);
          return_value = misc.MakeCASLError("System Interface Error", "PWADMIN-004", string.Format("{0}", errMsg), true);
        }
      }

      // Support for eWallet Token (Item ID) Transaction [Paying with an eWallet item (eCheck)]
      if (return_value == null && itemType == ewItemType.eCheck && eWalletTokenTrans)
      {
        return_value = SetECheckDataFromEWallet(eWalletToken, eWalletHostSystemID, eWalletPersonID, ref tender, ref eWalletSysInt);
      }

    }

    // Bug 25251 DJD: Added GetPersonIDAndHostIDFromEvent method
    private static bool GetPersonIDAndHostIDFromEvent(GenericObject coreEvent, ref string hostSystemID, ref string personID)
    {
      bool found = false;
      GenericObject trans = coreEvent.get("transactions", new GenericObject()) as GenericObject;
      int iCountofTransactions = trans.getIntKeyLength();
      for (int i = 0; i < iCountofTransactions; i++)
      {
        GenericObject tran = (GenericObject)trans.get(i);
        if (ItemHasValidStatus(tran))
        {
          found = (GetPersonIDAndHostIDFromTrans(tran, ref personID, ref hostSystemID));
          if (found)
          {
            break;
          }
        }
      }
      return found;
    }

    /// <summary>
    /// Returns TRUE if item (transaction or tender) status is ACTIVE, FINISHED, or COMPLETE.  False otherwise.
    /// </summary>
    /// <param name="item"></param>
    /// <param name="isValid"></param>
    /// <returns></returns>
    private static bool ItemHasValidStatus(GenericObject item)
    {
      bool isValid = false;
      const string sSTATUS_ACTIVE = "active";
      const string sSTATUS_FINISHED = "finished";
      const string sSTATUS_COMPLETED = "completed";
      //const string sSTATUS_CANCELLED = "cancelled";
      //const string sSTATUS_VOIDED    = "voided";
      GenericObject geoStatus = item == null ? null : (GenericObject)item.get("status", null);
      string sStatus = geoStatus == null ? "" : (string)geoStatus.get("_name");
      isValid = (sStatus == sSTATUS_COMPLETED || sStatus == sSTATUS_FINISHED || sStatus == sSTATUS_ACTIVE);
      return isValid;
    }

    // Bug 17690 UMN refactored so can call again. Bug 22823 DJD [Added workgroup]
    private static void BuildIPChargeRequest(String host, int timeout, SecureStringClass sCc_track_info, GenericObject result_set, GenericObject workgroup, out String sIPCharge_request_xml, out GenericObject build_args)
    {
      sIPCharge_request_xml = Build_ipcharge_request_xml(result_set);
      build_args = new GenericObject();
      build_args.set("uri_string", host);
      build_args.set("body", sIPCharge_request_xml);
      build_args.set("method", "POST");
      build_args.set("content_type", "text/xml");
      build_args.set("scheme", "http");
      build_args.set("user_agent", "corebt.com CASL");

      if (timeout == 987654321) // QA sentinal to throw a timeout exception
        build_args.set("timeout", 9000);
      else
        build_args.set("timeout", timeout); // Bug #13896 Mike O - Configurable timeout

      build_args.set("domain", null);
      build_args.set("userid", null);
      build_args.set("password", null);
      build_args.set("workgroup", workgroup); // Bug 22823 DJD [Added workgroup]

      // BUG# 8705 DH - Pass the secure data separate from the XML string. It will be used to replace track data.
      build_args.set("SecureStringClass", sCc_track_info);
    }

    /// <summary>
    /// Sends the IPCharge request and if the result is a duplicate, then it processes the dulicate into a valid
    /// IPCharge resonse. NOTE: Only use this call on the alternate host URLS!
    /// </summary>
    /// <param name="build_args"></param>
    /// <returns></returns>
    private static GenericObject SendRequestHandlingDuplicate(GenericObject build_args)
    {
      GenericObject return_value = Build_and_send_http_request(build_args) as GenericObject;

      // Bug 16285 UMN 
      string body = return_value.get("body") as string;
      if (body.Contains("DUP_TROUTD"))
      {
        Logger.cs_log("Duplicate detected! " + Environment.NewLine + body + Environment.NewLine);
        Regex rgx = new Regex(@"<ACCT_NUM>[^<]*</ACCT_NUM>");
        string result = body.Replace("DUP_", "");
        result = rgx.Replace(result, "<RESULT>CAPTURED</RESULT>");  // we rely on this in CASL code!
        return_value.set("body", result);
      }
      return return_value;
    }

    /// <summary>
    /// Decrypt merchant password if it is encrypted.
    /// Bug 17296 UMN
    /// </summary>
    /// <param name="tender"></param>
    /// <param name="workgroup"></param>
    /// <param name="password"></param>
    /// <returns></returns>
    private static String DecryptCCUserPwd(string userPwdType, GenericObject workgroup, String password)
    {
      try
      {
        byte[] bUserPwdEncrypted = Convert.FromBase64String(password);
        byte[] bUserPwdDecrypted = Crypto.symmetric_decrypt("data", bUserPwdEncrypted, "secret_key", Crypto.get_secret_key());
        password = System.Text.Encoding.Default.GetString(bUserPwdDecrypted);
      }
      catch
      {
        // on the fly update the password to be encrypted
        UpdateCCPwdToEncrypted(userPwdType, workgroup, password);
      }
      return password;
    }

    /// <summary>
    /// Update the CC_USERPW or CC_USERPW_OVERRIDE to be encrypted as per Bug 17296 UMN
    /// </summary>
    /// <param name="userPwdType">Either "CC_USERPW" or "CC_USERPW_OVERRIDE"</param>
    /// <param name="workgroup"></param>
    /// <param name="password"></param>
    private static void UpdateCCPwdToEncrypted(string userPwdType, GenericObject workgroup, String password)
    {
      try
      {
        // Uggh all of crypto code is designed to be called from CASL. It has no granular functions that
        // can be called from C#. What a PITA.
        // Need to do following in C#: crypto.symmetric.<encrypt> new_value.<to_base64string/> </encrypt>.data.<to_base64/>

        string pwdBase64 = misc.StringToBase64String(password);
        GenericObject bytes = Crypto.symmetric_encrypt("data", pwdBase64, "is_ascii_string", false);
        byte[] data = (byte[])bytes.get("data");
        string encPwd = Convert.ToBase64String(data);
        workgroup.set(userPwdType, encPwd);

        // Freeze the workgroup
        // Bug 25984 UMN delete revision
        misc.CASL_call("a_method", "freeze", "_subject", workgroup, "args", new GenericObject());
      }
      catch (Exception ex)
      {
        Logger.cs_log(String.Format("TTS 17296 exception: {0}", ex.Message));
      }
    }

    public static string Build_ipcharge_request_xml(params Object[] arg_pairs)
    {
      GenericObject result_set = misc.convert_args(arg_pairs);
      String sRequest_xml = "<TRANSACTION>";
      ArrayList keys = result_set.getKeys();
      foreach (object key in keys)
      {
        sRequest_xml += "<" + key + ">" + result_set.get(key) + "</" + key + ">";
      }
      sRequest_xml += "</TRANSACTION>";
      arg_pairs = null;
      return sRequest_xml;
    }
    //end BUG 6207 PCI

    // Bug #14288 Mike O - Send an admin packet to initialize VSP with VeriFone devices, and return the device key
    public static string Build_and_send_verifone_admin_packet(params Object[] arg_pairs)
    {
      GenericObject geo_args = misc.convert_args(arg_pairs);

      string userID = geo_args.get("userid") as string;
      string password = geo_args.get("password") as string;
      string uri = geo_args.get("uri") as string;
      string client_id = geo_args.get("client_id") as string;
      string serial_num = geo_args.get("serial_nbr") as string;
      string devtype = geo_args.get("device_name") as string;
      string devicekey = geo_args.get("device_key") as string;

      // Bug 16611 MJO - Mask DEVICEKEY
      string xml = String.Format("<TRANSACTION><CLIENT_ID>{0}</CLIENT_ID><SERIAL_NUM>{1}</SERIAL_NUM><DEVTYPE>{2}</DEVTYPE><DEVICEKEY>{3}</DEVICEKEY><COMMAND>SETUP_REQUEST_V2</COMMAND><FUNCTION_TYPE>ADMIN</FUNCTION_TYPE></TRANSACTION>",
      client_id, serial_num, devtype, devicekey == null ? "" : devicekey);

      // Bug 17400 MJO - Only mask the logged version of the XML
      string xmlMasked = String.Format("<TRANSACTION><CLIENT_ID>{0}</CLIENT_ID><SERIAL_NUM>{1}</SERIAL_NUM><DEVTYPE>{2}</DEVTYPE><DEVICEKEY>{3}</DEVICEKEY><COMMAND>SETUP_REQUEST_V2</COMMAND><FUNCTION_TYPE>ADMIN</FUNCTION_TYPE></TRANSACTION>",
      client_id, serial_num, devtype, devicekey == null ? "" : devicekey.Substring(devicekey.Length - 8).PadLeft(devicekey.Length, '*'));

      // Bug #15032 Mike O - Log if show_verbose_error is true
      if (c_CASL.get_show_verbose_error())
        Logger.cs_log("Admin packet request: " + xmlMasked);

      // Bug 17296 UMN password is now encrypted, so need to decrypt.
      GenericObject workgroup = geo_args.get("workgroup") as GenericObject;
      password = DecryptCCUserPwd("CC_USERPW", workgroup, password);

      string response = (SendHttpRequest("domain", null, "userid", userID, "password", password, "_subject", new GenericObject("a_uri", new GenericObject(
      "uri_string", uri, "scheme", "", "user_agent", "corebt.com", "method", "POST", "content_type", "text/xml", "body", xml))) as GenericObject).get("body") as string;

      // Bug #15032 Mike O - Log if show_verbose_error is true
      // Bug 16611 MJO - Mask the device key in the response
      if (c_CASL.get_show_verbose_error())
      {
        string maskedResponse;

        if (response.IndexOf("<DEVICEKEY>") != -1)
        {
          int startKey = response.IndexOf("<DEVICEKEY>") + 11;
          int endKey = response.IndexOf("</DEVICEKEY>");
          string key = response.Substring(startKey, endKey - startKey);
          maskedResponse = response.Substring(0, startKey) + key.Substring(key.Length - 8).PadLeft(key.Length, '*') + response.Substring(endKey);
        }
        else
        {
          maskedResponse = response;
        }

        Logger.cs_log("Admin packet response: " + maskedResponse);
      }

      XmlDocument doc = new XmlDocument();
      doc.LoadXml(response);
      XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
      XmlNodeList nodes = doc.SelectNodes("//DEVICEKEY", nsmgr);
      if (nodes.Count == 0)
        return null;
      return nodes[0].InnerText;
    }

    // Bug #14288 Mike O - Send a transaction containing the VeriFone command card data (NOT FOR USE DURING NORMAL CC PROCESSING)
    public static Object Build_and_send_verifone_command_card(params Object[] arg_pairs)
    {
      GenericObject geo_args = misc.convert_args(arg_pairs);

      string userID = geo_args.get("userid") as string;
      string password = geo_args.get("password") as string;
      string uri = geo_args.get("uri") as string;
      string client_id = geo_args.get("client_id") as string;
      string serial_num = geo_args.get("serial_nbr") as string;
      string devtype = geo_args.get("device_name") as string;
      string devicekey = geo_args.get("device_key") as string;
      string track_data = geo_args.get("track_data") as string;

      string xml = String.Format("<TRANSACTION><CLIENT_ID>{0}</CLIENT_ID><SERIAL_NUM>{1}</SERIAL_NUM><DEVTYPE>{2}</DEVTYPE><DEVICEKEY>{3}</DEVICEKEY><TRACK_DATA>{4}</TRACK_DATA>",
      client_id, serial_num, devtype, devicekey, track_data) +
      "<FUNCTION_TYPE>PAYMENT</FUNCTION_TYPE><COMMAND>SALE</COMMAND><PAYMENT_TYPE>CREDIT</PAYMENT_TYPE><PRESENT_FLAG>3</PRESENT_FLAG><TRANS_AMOUNT>1.00</TRANS_AMOUNT></TRANSACTION>";

      // Bug 17400 MJO - Only mask the logged version of the XML
      string xmlMasked = String.Format("<TRANSACTION><CLIENT_ID>{0}</CLIENT_ID><SERIAL_NUM>{1}</SERIAL_NUM><DEVTYPE>{2}</DEVTYPE><DEVICEKEY>{3}</DEVICEKEY><TRACK_DATA>{4}</TRACK_DATA>",
      client_id, serial_num, devtype, devicekey.Substring(devicekey.Length - 8).PadLeft(devicekey.Length, '*'), track_data) +
      "<FUNCTION_TYPE>PAYMENT</FUNCTION_TYPE><COMMAND>SALE</COMMAND><PAYMENT_TYPE>CREDIT</PAYMENT_TYPE><PRESENT_FLAG>3</PRESENT_FLAG><TRANS_AMOUNT>1.00</TRANS_AMOUNT></TRANSACTION>";

      // Bug #15032 Mike O - Log if show_verbose_error is true
      if (c_CASL.get_show_verbose_error())
        Logger.cs_log("Command card request: " + xmlMasked);

      // Bug 17296 UMN password is now encrypted, so need to decrypt.
      GenericObject workgroup = geo_args.get("workgroup") as GenericObject;
      password = DecryptCCUserPwd("CC_USERPW", workgroup, password);

      string response = (SendHttpRequest("domain", null, "userid", userID,
      "password", password,
      "_subject", new GenericObject(
      "a_uri", new GenericObject(
      "uri_string", uri, "scheme", "",
      "user_agent", "corebt.com", "method", "POST",
      "content_type", "text/xml", "body", xml))
      ) as GenericObject).get("body") as string;

      // Bug #15032 Mike O - Log if show_verbose_error is true
      if (c_CASL.get_show_verbose_error())
        Logger.cs_log("Command card response: " + response);

      XmlDocument doc = new XmlDocument();
      doc.LoadXml(response);
      XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
      XmlNodeList nodes = doc.SelectNodes("//RESPONSE_TEXT", nsmgr);
      // Bug #14891 Mike O - Added successful response to derived key request
      // Bug 18811 MJO - Mx915 returns a slightly different message, so support that too
      return nodes.Count == 1 && (nodes[0].InnerText == "New Keys Posted" || nodes[0].InnerText.StartsWith("Start Encryption Success"));
    }
    //BEGIN code copied from files sent by PrehKeyTec for decrypting
    public static void Crypt(string strKey)
    {
      Key = strKey;
    }

    public static string Decrypt(string strMSR)
    {
      byte i = 0;
      byte uci = 0;
      byte ucy = 0;
      byte ucctemp = 0;
      int intx = 0;
      string strRKey = "";
      int intRKey = 0;
      byte[] ucfCryptBox = new byte[nCryptBoxLen];   // Bug 10438: Moved this static declaration so that it is thread safe.
      byte[] ucCryptKEY = new byte[nCryptKEYLen];    // Bug 10438: Moved this static declaration so that it is thread safe.

      strRKey = strMSR.Substring(strMSR.Length - 4);
      strMSR = strMSR.Substring(0, strMSR.Length - 4);
      intRKey = Convert.ToInt32(strRKey, 16);
      intRKey -= 0x4000;
      for (intx = 0; intx < nCryptKEYLen; intx++)
      {
        ucCryptKEY[intx] = binbuffer[intx + intRKey];
      }
      uci = 0;
      ucy = 0;
      for (i = 0; i < nCryptBoxLen; i++)
      {
        ucfCryptBox[i] = (byte)i;
      }
      for (i = 0; i < nCryptBoxLen; i++)
      {
        ucy = (byte)((ucy + ucfCryptBox[i] + ucCryptKEY[i & (nCryptKEYLen - 1)]) & (nCryptBoxLen - 1));
        ucctemp = ucfCryptBox[i];
        ucfCryptBox[i] = ucfCryptBox[ucy];
        ucfCryptBox[ucy] = ucctemp;
      }
      ucCryptKEY = StrToByteArray(Key);
      ucy = 0;
      for (i = 0; i < nCryptBoxLen; i++)
      {
        ucy = (byte)((ucy + ucfCryptBox[i] + ucCryptKEY[i & (nCryptKEYLen - 1)]) & (nCryptBoxLen - 1));
        ucctemp = ucfCryptBox[i];
        ucfCryptBox[i] = ucfCryptBox[ucy];
        ucfCryptBox[ucy] = ucctemp;
      }
      byte[] cp = StrToByteArray(strMSR);
      byte[] cp2 = new byte[cp.Length];
      for (i = 0; i < cp.Length; i++)
        cp[i] -= 0x20;
      uci = 0;
      ucy = 0;
      for (i = 0; i < cp.Length; i++)
      {
        byte inchar;
        uci = (byte)((uci + 1) & (nCryptBoxLen - 1));
        ucy = (byte)((ucy + ucfCryptBox[uci]) & (nCryptBoxLen - 1));
        ucctemp = ucfCryptBox[uci];
        ucfCryptBox[uci] = ucfCryptBox[ucy];
        ucfCryptBox[ucy] = ucctemp;
        ucctemp = (byte)((ucfCryptBox[uci] + ucfCryptBox[ucy]) & (nCryptBoxLen - 1));
        ucctemp = ucfCryptBox[ucctemp];
        inchar = cp[i];
        cp2[i] = (byte)((inchar ^ ucctemp) + 0x20);
      }

      return ByteArrayToStr(cp2);
    }

    private static byte[] StrToByteArray(string str)
    {
      System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
      return encoding.GetBytes(str);
    }

    private static string ByteArrayToStr(byte[] dBytes)
    {
      System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
      return enc.GetString(dBytes);
    }

    /// <summary>
    /// Mask the tags in XML string.
    /// </summary>
    public static void MaskTags(ref string sXml, string[] arrTags)
    {
      foreach (string sTagName in arrTags) // Loop over tags to mask
      {
        string sStartTag = string.Format("<{0}>", sTagName);
        string sEndTag = string.Format("</{0}>", sTagName);
        int nStartIndex = sXml.IndexOf(sStartTag);
        int nEndIndex = sXml.IndexOf(sEndTag);

        if ((nStartIndex > -1) && (nEndIndex > nStartIndex))
        {
          string sStrToReplace = sXml.Substring(nStartIndex, ((nEndIndex - nStartIndex) + sEndTag.Length));
          string sMaskedContent = new string('*', sStrToReplace.Length - (sStartTag.Length + sEndTag.Length));
          if (sMaskedContent.Length > 21)
          {   // If length is greater than 21 characters then expose the last eight characters
            sMaskedContent = sMaskedContent.Substring(8) + sXml.Substring((nEndIndex - 8), 8);
          }
          else if (sMaskedContent.Length > 14)
          {   // If length is greater than 14 characters then expose the last four characters
            sMaskedContent = sMaskedContent.Substring(4) + sXml.Substring((nEndIndex - 4), 4);
          }
          string sReplacementStr = string.Format("{0}{1}{2}", sStartTag, sMaskedContent, sEndTag);
          sXml = sXml.Replace(sStrToReplace, sReplacementStr);
        }
      }
    }

    // IPAY-13 DJD: Use PAYware CTROUTD for follow-on transactions
    public static Object FormatXml(params Object[] arg_pairs)
    {
      string retXml = "";
      GenericObject args = misc.convert_args(arg_pairs);
      string xml = (string)args.get("xml_string", "");
      string errMsg = "";
      try
      {
        retXml = FormatXml(xml);
      }
      catch (Exception ex)
      {
        errMsg = string.Format("Error Formatting XML: {0}{1}{2}", xml, Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
        Logger.cs_log(errMsg);
      }
      return retXml;
    }

    /// <summary>
    /// BUG#14135 Returns formatted xml string (indent and newlines) from unformatted XML
    /// string for display in eg textboxes.
    /// </summary>
    /// <param name="sUnformattedXml">Unformatted xml string.</param>
    /// <returns>Formatted xml string and any exceptions that occur.</returns>
    private static string FormatXml(string sUnformattedXml)
    {
      //load unformatted xml into a dom
      XmlDocument xd = new XmlDocument();
      xd.LoadXml(sUnformattedXml);

      //will hold formatted xml
      StringBuilder sb = new StringBuilder();

      //pumps the formatted xml into the StringBuilder above
      StringWriter sw = new StringWriter(sb);

      //does the formatting
      XmlTextWriter xtw = null;

      try
      {
        //point the xtw at the StringWriter
        xtw = new XmlTextWriter(sw);

        //we want the output formatted
        xtw.Formatting = Formatting.Indented;

        //get the dom to dump its contents into the xtw 
        xd.WriteTo(xtw);
      }
      catch
      {
        return "";
      }
      finally
      {
        //clean up even if error
        if (xtw != null)
          xtw.Close();
      }

      //return the formatted xml
      return sb.ToString();
    }


    public static string TrimTo(string str, int maxLength)
    {
      if (str.Length <= maxLength)
      {
        return str;
      }
      return str.Substring(0, maxLength);
    }
    #region static byte[] binbuffer    
    static byte[] binbuffer = {
      0x8b,0xcd,0x40,0x6,0x8a,0x80,0x81,0x8b,
      0xcd,0x48,0x49,0x8a,0x80,0x6e,0x2,0x1f,
      0xcc,0x40,0xf5,0x3f,0,0x3f,0x4,0x6e,
      0x3,0x1,0x6e,0x3,0x5,0x6e,0x67,0x2,
      0x6e,0xff,0x6,0x6e,0xdf,0x3,0x6e,0xf0,
      0x7,0xb6,0x68,0xaa,0x7,0xb7,0x68,0x6e,
      0x3f,0x8,0x6e,0xc0,0x9,0xa6,0xff,0xc7,
      0xff,0xff,0x3f,0x77,0x1c,0x6,0x1c,0x2,
      0xcd,0x44,0x7,0xcd,0x94,0xca,0x9a,0x81,
      0,0x41,0xdc,0,0x6,0x40,0x51,0xde,
      0xc8,0,0x80,0,0xe,0x1,0,0x1,
      0,0x8,0,0,0xdc,0x9,0,0,
      0xdc,0xa,0,0,0xdc,0xb,0,0,
      0x24,0x8b,0x89,0x95,0xe6,0x4,0x87,0xee,
      0x5,0x8a,0xf6,0xaf,0x1,0x9e,0xef,0x6,
      0x8b,0x88,0x9e,0xef,0x5,0x88,0x8a,0x81,
      0xa7,0xfc,0xc6,0x40,0x4c,0x4c,0x95,0xe7,
      0x1,0xc6,0x40,0x4b,0x4c,0xf7,0xce,0x40,
      0x4d,0x89,0x8a,0xce,0x40,0x4e,0x20,0x1f,
      0x89,0x8b,0xf6,0x87,0xe6,0x2,0x4c,0x9e,
      0xe7,0x6,0xe6,0x3,0xee,0x1,0x8a,0x4c,
      0x20,0x3,0x7f,0xaf,0x1,0x4b,0xfb,0x9e,
      0x6b,0x5,0xf7,0x8a,0x88,0xaf,0x4,0x9e,
      0x6b,0x2,0xdd,0x9e,0x6b,0x1,0xd9,0xc6,
      0x40,0x50,0x87,0xc6,0x40,0x4f,0x87,0xad,
      0xa0,0x97,0x4c,0x9e,0xe7,0x3,0xad,0x99,
      0x4c,0x9e,0xe7,0x4,0x4a,0x26,0x3,0x51,
      0,0x18,0xad,0x8d,0x87,0x8a,0xad,0x89,
      0x97,0x20,0x5,0xad,0x84,0xf7,0xaf,0x1,
      0x9e,0x6b,0x4,0xf7,0x9e,0x6b,0x3,0xf3,
      0x20,0xd5,0xa7,0x6,0x81,0x9b,0xa6,0x40,
      0xc7,0xfe,0x8,0xa6,0xc,0xc7,0xfe,0xb,
      0x3f,0x77,0xc6,0x40,0x48,0xa5,0x2,0x26,
      0x4,0x45,0x4,0x80,0x94,0xcd,0x40,0x80,
      0xce,0x40,0x49,0x89,0x8a,0xce,0x40,0x4a,
      0xfd,0x20,0xe7,0xa7,0xfb,0x95,0x6f,0x3,
      0x6f,0x2,0x9b,0xcd,0x41,0xc8,0xa4,0xf7,
      0xaa,0x10,0xb7,0x2,0xcd,0x92,0x5e,0x95,
      0x6f,0x1,0xa6,0x20,0xf7,0x95,0xe6,0x1,
      0xfe,0xcd,0xf8,0xc5,0x95,0xf6,0xab,0x2,
      0xf7,0xee,0x1,0x87,0x8a,0x65,0x40,0,
      0x25,0xeb,0xa6,0x4,0x95,0xe7,0x1,0x7f,
      0xa6,0xff,0xc7,0xff,0xff,0x9d,0x95,0xf6,
      0x87,0xee,0x1,0x8a,0xd6,0xc3,0xff,0x95,
      0xe7,0x4,0xad,0x56,0x95,0x6c,0x1,0x26,
      0x1,0x7c,0xad,0x65,0x24,0x5,0xd6,0xc3,
      0xff,0xad,0x47,0x89,0x8b,0x86,0xab,0x20,
      0x87,0x9e,0xe6,0x7,0xde,0xc3,0xff,0xcd,
      0xf8,0,0xa7,0x2,0x95,0x6c,0x1,0x26,
      0x1,0x7c,0xad,0x45,0x25,0xc2,0x45,0x20,
      0x1,0x89,0x8b,0xc6,0xca,0x3,0xce,0xca,
      0x4,0xcd,0xf8,0,0xa7,0x2,0x45,0x20,
      0x3,0x89,0x8b,0x95,0xe6,0x4,0xee,0x5,
      0xcd,0xf8,0,0xa7,0x2,0xad,0x19,0xa4,
      0xe7,0xb7,0x2,0x9a,0xcd,0x92,0x4c,0xa7,
      0x5,0x81,0x9e,0xeb,0x6,0x9e,0xe7,0x6,
      0x4f,0x9e,0xe9,0x5,0x9e,0xe7,0x5,0x81,
      0xb6,0x6,0xaa,0x1f,0xb7,0x6,0xb6,0x2,
      0x81,0x95,0xe6,0x2,0x87,0xee,0x3,0x8a,
      0x65,0x6,0x4,0x81,0xcd,0x40,0x13,0xcc,
      0x49,0x5,0x9e,0x6d,0x4,0x26,0x1a,0x95,
      0xe6,0x7,0xee,0x4,0x8c,0x52,0x9e,0xe7,
      0x4,0x9e,0xe6,0x9,0x52,0x9e,0xe7,0x5,
      0x9e,0x6f,0x8,0x8b,0x86,0x9e,0xe7,0x9,
      0x81,0x4f,0x87,0xae,0x8,0x98,0x9e,0x69,
      0xa,0x9e,0x69,0x9,0x9e,0x69,0x1,0x9e,
      0xe6,0x5,0x9e,0xe1,0x1,0x22,0x1d,0x26,
      0x8,0x9e,0xe6,0x6,0x9e,0xe1,0x9,0x22,
      0x13,0x9e,0xe6,0x9,0x9e,0xe0,0x6,0x9e,
      0xe7,0x9,0x9e,0xe6,0x1,0x9e,0xe2,0x5,
      0x9e,0xe7,0x1,0x99,0x5b,0xd0,0x9e,0xe6,
      0xa,0x49,0x9e,0xe7,0x6,0x9e,0xe6,0x9,
      0x9e,0xe7,0xa,0x86,0x9e,0xe7,0x8,0x9e,
      0x6f,0x4,0x81,0xa7,0xfe,0x9e,0xff,0x1,
      0x87,0xcd,0x41,0xe2,0x86,0x8a,0x88,0x81,
      0x87,0x8b,0x52,0x24,0x6,0x8c,0xad,0xeb,
      0x9f,0x8b,0x88,0xa7,0x2,0x81,0x9e,0x6d,
      0x3,0x27,0x6,0x57,0x46,0x9e,0x6b,0x3,
      0xfa,0x81,0x9e,0x6d,0x3,0x27,0x6,0x48,
      0x59,0x9e,0x6b,0x3,0xfa,0x81,0x9e,0xe3,
      0x3,0x26,0xc,0x95,0xe1,0x3,0x27,0x7,
      0x4f,0x25,0x2,0xa6,0x2,0xa1,0x1,0x8a,
      0x88,0xa7,0x2,0xfc,0x58,0x49,0x87,0x9f,
      0x95,0xeb,0x2,0x97,0x86,0x9e,0xe9,0x1,
      0x87,0x8a,0xa7,0x2,0xf6,0xee,0x1,0x87,
      0x8a,0xfc,0x87,0x9f,0x95,0xeb,0x2,0x97,
      0x86,0x9e,0xe9,0x1,0x87,0x8a,0xa7,0x2,
      0x9f,0xfb,0x97,0x8b,0x86,0xa9,0,0x87,
      0x8a,0xec,0x1,0x8a,0x89,0x87,0x9e,0xee,
      0x3,0xf1,0x22,0x1d,0x26,0x7,0xe6,0x1,
      0x9e,0xe1,0x2,0x23,0x14,0xaf,0x2,0x9e,
      0x68,0x2,0x9e,0x69,0x1,0x9f,0x9e,0xeb,
      0x2,0x97,0x8b,0x86,0x9e,0xe9,0x1,0x87,
      0x8a,0xa7,0x3,0xe6,0x2,0xee,0x3,0x87,
      0x8a,0xfc,0x4d,0x9f,0x8a,0x88,0x26,0x12,
      0xf1,0x24,0xf,0xaf,0x1,0x89,0x9e,0xeb,
      0x1,0x87,0x8b,0x86,0xa9,0,0x87,0x8a,
      0x88,0x86,0x9f,0xeb,0x1,0x97,0x8b,0x86,
      0xa9,0,0x87,0x8a,0xec,0x2,0x8a,0x8b,
      0x89,0x9e,0xee,0x3,0x4d,0x86,0x26,0xa,
      0xaf,0x2,0xf1,0x22,0xfb,0x27,0x3,0x8a,
      0x88,0x65,0xa7,0x2,0x9f,0xeb,0x1,0x97,
      0x8b,0x86,0xa9,0,0x87,0x8a,0xec,0x2,
      0x87,0x85,0x87,0x9e,0xe6,0x4,0x87,0x9e,
      0xe6,0x4,0x87,0x9e,0xe6,0x4,0x81,0x9e,
      0xe7,0x4,0x86,0x9e,0xe7,0x4,0x86,0x9e,
      0xe7,0x4,0x86,0x84,0x86,0x81,0xa8,0x5,
      0x87,0xb6,0x2,0xa4,0xf8,0x87,0x95,0xe6,
      0x1,0xa4,0x7,0xfa,0xb7,0x2,0xa7,0x2,
      0x81,0x87,0xb6,0x1,0xa4,0xfc,0x87,0x95,
      0xe6,0x1,0xa4,0x3,0xfa,0xb7,0x1,0xa7,
      0x2,0x81,0xa6,0x4,0xcd,0x43,0x69,0x1a,
      0x7,0x1b,0x3,0x81,0x87,0x95,0x7d,0x26,
      0xe,0xa6,0x6,0xcd,0x43,0x69,0xa6,0x7,
      0xcd,0x43,0x69,0xa6,0x6,0x20,0xc,0xa6,
      0x4,0xcd,0x43,0x69,0xa6,0x5,0xcd,0x43,
      0x69,0xa6,0x4,0xcd,0x43,0x69,0x6e,0x3,
      0x5,0x8a,0x81,0x87,0xb6,0xa,0x87,0x95,
      0xe8,0x1,0xa4,0x7,0xf8,0xb7,0xa,0x18,
      0xa,0xa7,0x2,0x81,0xc6,0x1,0x23,0x27,
      0x3,0x1b,0xa,0x81,0x1a,0xa,0x18,0xa,
      0x4,0x10,0x7,0x6e,0x2,0x10,0x6e,0x1a,
      0x10,0x81,0x6e,0x12,0x10,0x6e,0x1e,0x10,
      0x81,0xc6,0x1,0x25,0xce,0x1,0x24,0xbf,
      0x11,0xb7,0x12,0x4,0x10,0x6,0x6e,0x42,
      0x10,0x6e,0x5a,0x10,0x81,0xc6,0x1,0x23,
      0x26,0x7,0xa6,0x1,0xc7,0x1,0x23,0xad,
      0xc3,0x4f,0x81,0xc7,0x1,0x25,0xcf,0x1,
      0x24,0xad,0xd6,0x1c,0xa,0x4f,0x81,0x6e,
      0x10,0xa,0x6e,0x12,0x10,0x6e,0x1e,0x10,
      0x1c,0xa,0xa6,0x64,0xc7,0x1,0x25,0xa6,
      0x3d,0xc7,0x1,0x24,0xa6,0x1,0xc7,0x1,
      0x23,0x6e,0x2,0x11,0x6e,0x9,0xe,0x6e,
      0x58,0x12,0x6e,0xc6,0xf,0x4f,0xcd,0x43,
      0xab,0xcc,0x43,0xbc,0x8b,0xcd,0x44,0x3a,
      0x8a,0x80,0x1f,0xa,0xd,0x2,0x9,0xa6,
      0xff,0xc7,0xff,0xff,0x1c,0x6,0x1d,0x2,
      0xc6,0x1,0x22,0x27,0x5,0x45,0x1,0x22,
      0x7a,0x81,0xa6,0x14,0xc7,0x1,0x22,0xcc,
      0x48,0x69,0xcd,0x43,0x7a,0x4f,0xc7,0x1,
      0x27,0x81,0xcd,0x44,0x6e,0x4d,0x27,0x2,
      0xa6,0x1,0x81,0xb6,0,0x81,0xa7,0xfc,
      0x4f,0xc7,0x1,0x28,0xce,0x1,0x28,0x8c,
      0xd6,0x1,0,0x41,0xff,0xa,0xc6,0x1,
      0x27,0x26,0x5,0xa6,0x1,0xc7,0x1,0x27,
      0x45,0x1,0x28,0x7c,0xcd,0x46,0x3e,0x25,
      0xe3,0xc6,0x1,0x27,0x26,0x35,0xcd,0x46,
      0x31,0x4f,0xc7,0x1,0x28,0x4f,0xcd,0x43,
      0x84,0x45,0x1,0x28,0x7c,0xcd,0x46,0x3e,
      0x25,0xf3,0xb6,0x7,0xaa,0xf,0xb7,0x7,
      0xb6,0x3,0xa4,0xf0,0xb7,0x3,0xad,0xb3,
      0x4c,0x26,0x10,0xd,0x83,0xa,0x3,0x87,
      0x7,0xa6,0x1,0xcd,0x51,0xd8,0x13,0x87,
      0xcc,0x45,0xe2,0x4f,0xc7,0x1,0x28,0x95,
      0x6f,0x2,0xc7,0x1,0x27,0xcc,0x45,0xc2,
      0x95,0x7f,0xcd,0x46,0x31,0xc6,0x1,0x28,
      0xa1,0x4,0x23,0xd,0xa6,0x1,0xcd,0x43,
      0x84,0xb6,0x7,0xa4,0xfc,0xb7,0x7,0x20,
      0x3,0xcd,0x47,0x89,0xa6,0x19,0x5f,0xcd,
      0x94,0x7c,0xa6,0x1,0xcd,0x52,0x87,0x1c,
      0x6,0x1c,0x2,0xb6,0,0xc7,0x1,0x29,
      0xb1,0,0x26,0x2,0x95,0x7c,0xcd,0x46,
      0x2b,0x26,0x2,0x95,0x7c,0xcd,0x46,0x2b,
      0x26,0x2,0x95,0x7c,0xcd,0x46,0x2b,0x26,
      0x2,0x95,0x7c,0x1c,0x6,0x1c,0x2,0x95,
      0xf6,0xa1,0x3,0x24,0x25,0x7f,0xc6,0x1,
      0x28,0xcd,0x47,0x89,0xcd,0x46,0x2b,0x26,
      0x4,0xa6,0x1,0x95,0xf7,0xcd,0x46,0x2b,
      0x26,0x2,0x95,0x7c,0xcd,0x46,0x2b,0x26,
      0x2,0x95,0x7c,0xcd,0x46,0x2b,0x26,0x2,
      0x95,0x7c,0x1c,0x6,0x1c,0x2,0x95,0xf6,
      0xa1,0x3,0x25,0x62,0xc6,0x1,0x29,0x41,
      0xff,0x10,0x45,0x1,0x27,0x7c,0xa6,0x32,
      0x5f,0xcd,0x94,0x7c,0xcd,0xa2,0xe7,0x4d,
      0x27,0x71,0xce,0x1,0x28,0x8c,0xc6,0x1,
      0x29,0xd7,0x1,0x99,0xd8,0x1,0,0xd4,
      0x1,0,0xc7,0x1,0x29,0xa3,0x10,0x25,
      0x4,0x9f,0xab,0x2,0x21,0x9f,0x48,0x48,
      0x48,0x95,0xe7,0x3,0xa6,0x1,0xe7,0x1,
      0x1c,0x6,0x1c,0x2,0xc6,0x1,0x29,0xe4,
      0x1,0xe1,0x1,0x26,0x10,0xe6,0x3,0xcd,
      0x56,0x17,0x4d,0x27,0x8,0xc6,0x1,0x29,
      0x27,0x3,0x95,0x6c,0x2,0x95,0x68,0x1,
      0x6c,0x3,0x6d,0x1,0x26,0xda,0x45,0x1,
      0x28,0x7c,0xad,0x7a,0x24,0x3,0xcc,0x44,
      0xd8,0x95,0xe6,0x2,0xa1,0x1,0x23,0x15,
      0xc6,0x1,0x2c,0x2b,0x10,0x2,0x83,0xa,
      0xcd,0x95,0xbd,0x12,0x83,0xa6,0xff,0xc7,
      0x1,0x15,0x4f,0x20,0x43,0x3,0x83,0x11,
      0xc6,0x1,0x27,0xa1,0x2,0x24,0x4,0x13,
      0x83,0x20,0x6,0x13,0x83,0xa6,0x1,0x20,
      0x2f,0x1c,0x6,0x1c,0x2,0x4f,0xc7,0x1,
      0x28,0xce,0x1,0x28,0x8c,0xd6,0x1,0x99,
      0xc7,0x1,0x29,0xd1,0x1,0,0x26,0x4,
      0x15,0x83,0x20,0xc,0xd6,0x1,0,0xc7,
      0x1,0x2a,0xcd,0x46,0x44,0x4d,0x26,0x8,
      0x45,0x1,0x28,0x7c,0xad,0x18,0x25,0xd9,
      0xa7,0x4,0x81,0xb6,0,0xc1,0x1,0x29,
      0x81,0xb6,0x7,0xaa,0x3,0xb7,0x7,0xb6,
      0x3,0xaa,0x3,0xb7,0x3,0x81,0xc6,0x1,
      0x28,0xa1,0x13,0x81,0xa7,0xfb,0x95,0x7f,
      0x6f,0x2,0xcd,0x47,0x72,0x1c,0x6,0x1c,
      0x2,0xa6,0x32,0x5f,0xcd,0x94,0x7c,0xc6,
      0x1,0x29,0xb1,0,0x26,0x2,0x95,0x7c,
      0x95,0x6c,0x2,0xe6,0x2,0xa1,0x5,0x25,
      0xe1,0xf6,0xa1,0x3,0x25,0x6a,0x6f,0x4,
      0xc6,0x1,0x29,0xcd,0x48,0x26,0x4d,0x27,
      0x36,0x95,0x6f,0x2,0x6f,0x4,0xc6,0x1,
      0x28,0xe1,0x2,0x27,0x21,0x1c,0x6,0x1c,
      0x2,0xe6,0x2,0xcd,0x47,0x89,0xb6,0,
      0x95,0xf7,0xb6,0,0xf1,0x26,0xee,0xc6,
      0x1,0x29,0xfa,0xcd,0x48,0x26,0x4d,0x27,
      0x5,0xa6,0x1,0x95,0xe7,0x4,0x95,0x6c,
      0x2,0xe6,0x2,0xa1,0x13,0x25,0xcf,0xcd,
      0x47,0x72,0xa6,0x19,0x5f,0xcd,0x94,0x7c,
      0xb6,0,0x95,0xf7,0xb6,0,0xf1,0x26,
      0xee,0xc6,0x1,0x29,0xf1,0x26,0x11,0x6d,
      0x4,0x27,0xf,0x4,0x83,0xa,0x14,0x83,
      0xcd,0x95,0xbd,0xa6,0xff,0xc7,0x1,0x15,
      0x20,0x7a,0x15,0x83,0x45,0x1,0x2a,0xf8,
      0xf4,0xcd,0x47,0x7c,0x25,0x2,0xab,0x2,
      0x48,0x48,0x48,0x95,0xe7,0x3,0x20,0x35,
      0x95,0xe6,0x1,0xf4,0x27,0x2a,0xc6,0x1,
      0x26,0xe1,0x3,0x26,0x5,0xc6,0xb,0x22,
      0x26,0x6c,0xe6,0x3,0xc7,0x1,0x26,0xae,
      0xc,0xcf,0xb,0x22,0x5f,0xcd,0x54,0x7b,
      0x4d,0x27,0x3a,0xce,0x1,0x28,0x9e,0xe6,
      0x2,0x8c,0xd8,0x1,0,0xd7,0x1,0,
      0x95,0x68,0x1,0x6c,0x3,0x6d,0x1,0x26,
      0xc7,0x45,0x1,0x29,0xf6,0xc8,0x1,0x2a,
      0xf4,0xad,0x49,0x25,0x2,0xab,0x2,0x48,
      0x48,0x48,0x95,0xe7,0x3,0x20,0x2b,0x95,
      0xe6,0x1,0xf4,0x27,0x20,0xe6,0x3,0xcd,
      0x55,0x6,0x4d,0x26,0xb,0x1c,0x6,0x1c,
      0x2,0xcd,0x95,0xbd,0xa6,0x1,0x20,0x17,
      0xce,0x1,0x28,0x8c,0x9e,0xe6,0x2,0xd8,
      0x1,0,0xd7,0x1,0,0x95,0x68,0x1,
      0x6c,0x3,0x6d,0x1,0x26,0xd1,0x4f,0xa7,
      0x5,0x81,0x1c,0x6,0x1c,0x2,0xc6,0x1,
      0x28,0xcc,0x47,0x89,0x95,0xe7,0x2,0xa6,
      0x1,0xe7,0x3,0xc6,0x1,0x28,0xa1,0x10,
      0x81,0x87,0xa6,0x12,0x1c,0x6,0x1c,0x2,
      0x6e,0xf0,0x7,0x20,0x7,0x87,0xa6,0x1,
      0xcd,0x43,0x84,0x86,0x97,0x4a,0x5d,0x26,
      0xf4,0x1c,0x6,0x1c,0x2,0x95,0xf6,0xa1,
      0x4,0x24,0x37,0x7d,0x26,0xa,0xa6,0xe,
      0xcd,0x48,0x38,0x6e,0xf1,0x7,0x8a,0x81,
      0xa1,0x1,0x26,0xa,0xa6,0xd,0xcd,0x48,
      0x38,0x6e,0xf2,0x7,0x8a,0x81,0xa1,0x2,
      0x26,0xa,0xa6,0xb,0xcd,0x48,0x38,0x6e,
      0xf4,0x7,0x8a,0x81,0xa1,0x3,0x26,0x23,
      0xa6,0x7,0xcd,0x48,0x38,0x6e,0xf8,0x7,
      0x8a,0x81,0xa0,0x4,0x87,0x4f,0xcd,0x43,
      0x84,0x1c,0x6,0x1c,0x2,0x20,0x6,0x87,
      0xa6,0x1,0xcd,0x43,0x84,0x86,0x97,0x4a,
      0x5d,0x26,0xf4,0x8a,0x81,0x87,0x44,0x44,
      0x44,0xad,0x86,0xb6,0,0x1c,0x6,0x1c,
      0x2,0xb6,0,0xb6,0,0x87,0x95,0xe6,
      0x1,0xa4,0x7,0x97,0x86,0x5d,0x27,0x3,
      0x44,0x5b,0xfd,0xa5,0x1,0x26,0x4,0xa6,
      0x1,0x8a,0x81,0x4f,0x8a,0x81,0x87,0x95,
      0x73,0x26,0x3,0x20,0x6,0x74,0xf6,0xa5,
      0x1,0x27,0xfa,0xf6,0xa4,0xfe,0x8a,0x81,
      0x87,0xb6,0x3,0xa4,0xf0,0x87,0x95,0xe6,
      0x1,0xa4,0xf,0xfa,0xb7,0x3,0xa7,0x2,
      0x81,0x6e,0x2,0x1f,0x3f,0x77,0xcc,0x40,
      0xf5,0x81,0x81,0x81,0xcd,0x43,0xed,0xcc,
      0xa2,0xd5,0xcc,0xa2,0xa6,0xcc,0xa2,0xdc,
      0xcc,0xa2,0xd5,0xcc,0xa2,0xad,0xcc,0xa2,
      0xa5,0xcc,0x61,0xf5,0x8b,0x95,0x7f,0x8c,
      0x9e,0xee,0x1,0x4f,0xd7,0xb,0x8,0x95,
      0x7c,0xf6,0xa1,0x5,0x25,0xf1,0x8a,0x81,
      0x87,0x8b,0x9e,0x6f,0x1,0x1c,0x6,0x1c,
      0x2,0xc6,0x1,0x1d,0xa5,0x1,0x27,0x57,
      0xcf,0xb,0x8,0xc6,0xb,0x9,0x26,0x8,
      0x95,0xe6,0x1,0xc7,0xb,0x9,0x20,0x28,
      0xc6,0xb,0xa,0x26,0x8,0x95,0xe6,0x1,
      0xc7,0xb,0xa,0x20,0x1b,0xc6,0xb,0xb,
      0x26,0x8,0x95,0xe6,0x1,0xc7,0xb,0xb,
      0x20,0xe,0xc6,0xb,0xc,0x26,0x9,0x95,
      0xe6,0x1,0xc7,0xb,0xc,0xa6,0x1,0xf7,
      0x95,0x6d,0x1,0x27,0x2,0x4f,0x65,0xa6,
      0x1,0xfa,0x27,0x13,0xc6,0xb,0x9,0x27,
      0xe,0xcd,0x48,0xeb,0x4f,0x45,0xb,0x9,
      0xf7,0xe7,0x1,0xe7,0x2,0xe7,0x3,0x4f,
      0xa7,0x2,0x81,0xa6,0x14,0xcd,0x62,0x5b,
      0x20,0xa,0xb6,0x86,0xa5,0x80,0x26,0xc,
      0x1c,0x6,0x1c,0x2,0xa6,0x82,0xcd,0xa1,
      0xdd,0x4d,0x26,0xee,0x81,0x8b,0xcd,0x51,
      0xf7,0xcd,0x4e,0x84,0x11,0x82,0x13,0x82,
      0x15,0x82,0x17,0x82,0x19,0x82,0x1b,0x82,
      0x1d,0x82,0x1f,0x82,0x11,0x83,0x13,0x83,
      0x15,0x83,0x17,0x83,0x19,0x83,0x1b,0x83,
      0x1d,0x83,0x1f,0x83,0x11,0x84,0x13,0x84,
      0x15,0x84,0x17,0x84,0x19,0x84,0x1b,0x84,
      0x1d,0x84,0x1f,0x84,0x11,0x85,0x13,0x85,
      0x15,0x85,0x17,0x85,0x19,0x85,0x1b,0x85,
      0x1d,0x85,0x1f,0x85,0x11,0x86,0x13,0x86,
      0x15,0x86,0x17,0x86,0x19,0x86,0x1b,0x86,
      0x1d,0x86,0x1f,0x86,0x11,0x87,0x15,0x87,
      0x17,0x87,0x19,0x87,0x1b,0x87,0x1d,0x87,
      0x1f,0x87,0x11,0x88,0x13,0x88,0x15,0x88,
      0x17,0x88,0x19,0x88,0x1b,0x88,0x1d,0x88,
      0x1f,0x88,0x10,0x89,0x13,0x89,0x15,0x89,
      0x17,0x89,0x19,0x89,0x1b,0x89,0x1d,0x89,
      0x1f,0x89,0x1d,0x8a,0x15,0x8d,0x17,0x8d,
      0x19,0x8d,0x1b,0x8d,0x4f,0xcd,0x51,0xd8,
      0xcd,0x48,0x6c,0xcd,0x4e,0x7b,0xcd,0x4e,
      0x9f,0xcd,0x4e,0x84,0xc6,0xfe,0x1,0xa5,
      0x38,0x27,0x9,0x1e,0x86,0x4f,0xc7,0x1,
      0x2b,0xcc,0x4b,0x5f,0xa6,0x2e,0xcd,0x4e,
      0x76,0x26,0x3,0xcc,0x4b,0x5f,0xa6,0x1e,
      0xcd,0x4e,0x76,0x27,0x3,0xcc,0x4b,0x5f,
      0xa6,0x2e,0xcd,0x4e,0x76,0x26,0x3,0xcc,
      0x4b,0x5f,0xa6,0x1e,0xcd,0x4e,0x76,0x27,
      0x3,0xcc,0x4b,0x5f,0xa6,0x37,0xae,0x3c,
      0xcd,0x4e,0xb1,0xcd,0x4e,0x76,0x26,0x3,
      0xcc,0x4b,0x5f,0xa6,0x1e,0xcd,0x4e,0x76,
      0x27,0x3,0xcc,0x4b,0x5f,0xae,0x3c,0xa6,
      0x37,0xcd,0x4e,0xb1,0xcd,0x4e,0x76,0x26,
      0x3,0xcc,0x4b,0x5f,0xa6,0x1e,0xcd,0x4e,
      0x76,0x27,0x3,0xcc,0x4b,0x5f,0xa6,0x86,
      0xcd,0x4e,0x76,0x27,0x19,0xa6,0x2e,0xcd,
      0x4e,0x76,0x27,0x12,0xa6,0x1e,0xcd,0x4e,
      0x76,0x26,0xb,0xa6,0x26,0xcd,0x4e,0x76,
      0x26,0x4,0x4f,0xcd,0x52,0x69,0xa6,0x2,
      0xcd,0x4e,0x76,0x27,0x1a,0xa6,0x2e,0xcd,
      0x4e,0x76,0x27,0x13,0xa6,0x1e,0xcd,0x4e,
      0x76,0x26,0xc,0xa6,0x26,0xcd,0x4e,0x76,
      0x26,0x5,0xa6,0x3,0xcd,0x52,0x69,0xa6,
      0x1,0xcd,0x4e,0x76,0x27,0x1a,0xa6,0x2e,
      0xcd,0x4e,0x76,0x27,0x13,0xa6,0x1e,0xcd,
      0x4e,0x76,0x26,0xc,0xa6,0x26,0xcd,0x4e,
      0x76,0x26,0x5,0xa6,0x4,0xcd,0x52,0x69,
      0xa6,0x2e,0xcd,0x4e,0x76,0x27,0x44,0xa6,
      0x1e,0xcd,0x4e,0x76,0x26,0x3d,0xa6,0x26,
      0xcd,0x4e,0x76,0x27,0x36,0xa6,0x10,0xcd,
      0x4e,0x76,0x26,0x2f,0xa6,0x18,0xcd,0x4e,
      0x76,0x27,0x28,0x10,0x84,0xa6,0x37,0xae,
      0x14,0xcd,0x95,0x57,0xa6,0xa,0xae,0x14,
      0xcd,0x95,0x57,0xcd,0x4e,0x84,0xa6,0x2e,
      0xcd,0x4e,0x76,0x26,0xe8,0xa6,0x26,0xcd,
      0x4e,0x76,0x26,0xe1,0xa6,0x18,0xcd,0x4e,
      0x76,0x26,0xda,0xa6,0x2e,0xcd,0x4e,0x76,
      0x27,0x3e,0xa6,0x1e,0xcd,0x4e,0x76,0x26,
      0x3e,0xa6,0x26,0xcd,0x4e,0x76,0x27,0x30,
      0xa6,0x10,0xcd,0x4e,0x76,0x26,0x30,0xa6,
      0x1,0xcd,0x4e,0x76,0x26,0x7e,0xa6,0x2,
      0xcd,0x4e,0x76,0x27,0x77,0xa6,0x86,0xcd,
      0x4e,0x76,0x26,0x70,0xcd,0x4e,0x8e,0xcd,
      0x4e,0x76,0x27,0x68,0xa6,0x1e,0xcd,0x4e,
      0x76,0x26,0x61,0xa6,0x26,0xcd,0x4e,0x76,
      0x27,0x5a,0xa6,0x10,0xcd,0x4e,0x76,0x26,
      0x53,0xa6,0x1,0xcd,0x4e,0x76,0x26,0x4c,
      0xa6,0x2,0xcd,0x4e,0x76,0x27,0x45,0xa6,
      0x86,0xcd,0x4e,0x76,0x26,0x3e,0xae,0x64,
      0xa6,0xf,0xcd,0x4e,0xb1,0xcd,0x4e,0x76,
      0x27,0x32,0xa6,0x1e,0xcd,0x4e,0x76,0x26,
      0x2b,0xa6,0x26,0xcd,0x4e,0x76,0x27,0x24,
      0xa6,0x10,0xcd,0x4e,0x76,0x26,0x1d,0xa6,
      0x1,0xcd,0x4e,0x76,0x26,0x16,0xa6,0x2,
      0xcd,0x4e,0x76,0x27,0xf,0xa6,0x86,0xcd,
      0x4e,0x76,0x26,0x8,0xa6,0xcb,0xc7,0x1,
      0xad,0xcd,0x98,0x26,0xcd,0x4e,0x84,0xa6,
      0x2e,0xcd,0x4e,0x76,0x27,0x2a,0xa6,0x1e,
      0xcd,0x4e,0x76,0x26,0x23,0xa6,0x26,0xcd,
      0x4e,0x76,0x27,0x1c,0xa6,0x10,0xcd,0x4e,
      0x76,0x26,0x15,0xa6,0x1,0xcd,0x4e,0x76,
      0x27,0xe,0xa6,0x2,0xcd,0x4e,0x76,0x26,
      0x7,0xa6,0x86,0xcd,0x4e,0x76,0x27,0x6,
      0xcd,0x92,0x72,0x4d,0x26,0x36,0xcd,0x4e,
      0x8e,0xcd,0x4e,0x76,0x27,0x2a,0xa6,0x1e,
      0xcd,0x4e,0x76,0x26,0x23,0xa6,0x26,0xcd,
      0x4e,0x76,0x27,0x1c,0xa6,0x10,0xcd,0x4e,
      0x76,0x26,0x15,0xa6,0x1,0xcd,0x4e,0x76,
      0x27,0xe,0xa6,0x2,0xcd,0x4e,0x76,0x26,
      0x7,0xa6,0x86,0xcd,0x4e,0x76,0x27,0x6,
      0xcd,0x92,0x72,0x4d,0x26,0x4d,0xcd,0x4e,
      0x8e,0xcd,0x4e,0x76,0x27,0x2a,0xa6,0x1e,
      0xcd,0x4e,0x76,0x26,0x23,0xa6,0x26,0xcd,
      0x4e,0x76,0x27,0x1c,0xa6,0x10,0xcd,0x4e,
      0x76,0x26,0x15,0xa6,0x1,0xcd,0x4e,0x76,
      0x27,0xe,0xa6,0x2,0xcd,0x4e,0x76,0x26,
      0x7,0xa6,0x86,0xcd,0x4e,0x76,0x27,0x6,
      0xcd,0x92,0x72,0x4d,0x26,0x15,0x6e,0x77,
      0x75,0xa6,0x37,0xae,0x32,0xcd,0x95,0x57,
      0xcd,0x41,0x1b,0x14,0x82,0xcd,0x4e,0x7b,
      0xcd,0x4e,0x9f,0xcd,0x4e,0x95,0x19,0x82,
      0xc6,0x20,0x4,0xa5,0x8,0x27,0x2,0x18,
      0x82,0xcd,0x6d,0x7d,0x4f,0xcd,0x83,0xe7,
      0x4f,0xc7,0x1,0x2b,0x4a,0xcc,0x4c,0xe1,
      0xcd,0x4e,0x84,0xcd,0x4f,0xf,0xd,0x83,
      0x2,0x10,0x56,0xb6,0x86,0xa5,0x80,0x27,
      0xd,0x1f,0x86,0xd,0x83,0x3,0xcd,0xc1,
      0x3e,0xa6,0x5,0xc7,0x1,0x2b,0xc6,0x1,
      0x2b,0x26,0x30,0xcd,0x4e,0x7b,0xcd,0x4e,
      0x9f,0xa6,0x37,0xcd,0x62,0x5f,0x4f,0xc7,
      0xb,0x12,0xcd,0x4e,0x84,0xcd,0x4e,0x84,
      0xcd,0x4e,0x95,0xcd,0x61,0x7a,0xcd,0x4e,
      0x84,0xcd,0x44,0x5a,0xa6,0x1,0xc7,0x1,
      0x2b,0xcd,0x7d,0x7b,0xa6,0xa,0xc7,0x1,
      0x49,0x20,0x69,0xa1,0x1,0x26,0x28,0xc6,
      0x1,0x49,0x26,0x60,0x4f,0xcd,0x83,0xe7,
      0xcd,0x52,0x6,0xcd,0x4e,0x84,0xd,0x83,
      0xf,0xa6,0x2,0xc7,0x1,0x2b,0xa6,0x37,
      0xcd,0x62,0x5f,0xcd,0xc1,0x3c,0x20,0x44,
      0xa6,0x9,0xc7,0x1,0x2b,0x20,0x3d,0xa1,
      0x2,0x26,0x1c,0x4f,0xcd,0x52,0x87,0xcd,
      0x4e,0x84,0xc6,0xb,0x12,0xa1,0x5,0x26,
      0x2b,0xc6,0x1,0xf7,0xa1,0x5,0x26,0x7e,
      0xa6,0x4,0xc7,0x1,0x2b,0x20,0x77,0xa1,
      0x4,0x26,0x8,0xcd,0x4e,0x7b,0xcd,0x4e,
      0xa6,0x20,0xe,0xa1,0x9,0x26,0xf,0x4f,
      0xcd,0x52,0x87,0xcd,0x4e,0x7b,0xcd,0x4e,
      0xa6,0xcd,0x62,0x5f,0x20,0x58,0xa1,0x5,
      0x26,0x57,0xb6,0x6,0xaa,0x1f,0xb7,0x6,
      0x87,0xb6,0x2,0xcd,0x4e,0x9f,0x86,0xaa,
      0x1f,0xb7,0x6,0x87,0xb6,0x2,0xa4,0xfb,
      0xaa,0x4,0xb7,0x2,0x86,0xaa,0x1f,0xb7,
      0x6,0x13,0x2,0xaa,0x1f,0xb7,0x6,0xb6,
      0x2,0xa4,0xfe,0xaa,0x1,0xb7,0x2,0xcd,
      0x94,0xca,0x18,0x7,0x18,0x3,0x1a,0x6,
      0x1a,0x2,0xa6,0x14,0xcd,0x62,0x63,0xcd,
      0xc1,0x3c,0xcd,0xa2,0xad,0x4f,0xc7,0x1,
      0x2b,0x1d,0x83,0x1f,0x86,0xc7,0xb,0x12,
      0xcd,0x4e,0x84,0xcd,0x51,0xf7,0xcc,0x4e,
      0x6d,0xa1,0xa,0x27,0x3,0xcc,0x4e,0x6f,
      0xd,0x83,0x28,0xc6,0x1,0xf7,0xa1,0x5,
      0x25,0x2,0x5f,0x65,0xae,0x1,0xc6,0xb,
      0x12,0xa1,0x5,0x25,0x2,0x4f,0x65,0xa6,
      0x1,0x89,0x95,0xfa,0x8a,0x27,0x15,0xa6,
      0x2,0xc7,0x1,0x2b,0xa6,0xff,0xcd,0x62,
      0x5f,0x20,0x9,0xc6,0xb,0x12,0xa1,0x5,
      0x26,0x2,0x1c,0x83,0xcd,0x4e,0x84,0xcd,
      0xc1,0x82,0xcd,0x4e,0x89,0xcd,0x4e,0x84,
      0x5,0x82,0x54,0xae,0x20,0x89,0x8a,0xae,
      0x4,0xf6,0x95,0xf7,0xcd,0x8b,0x88,0xcd,
      0x4e,0x84,0x95,0xf6,0xa1,0x4,0x26,0x4,
      0xa6,0x4,0x20,0x6,0xa1,0x3,0x26,0x5,
      0xa6,0x3,0xcd,0x52,0x69,0x15,0x82,0x19,
      0x82,0xc6,0x20,0x4,0xa5,0x8,0x27,0x2,
      0x18,0x82,0xcd,0x4e,0x84,0xcd,0x4e,0x95,
      0xa6,0x37,0xae,0x1e,0xcd,0x95,0x57,0xa6,
      0x5,0xae,0x1e,0xcd,0x95,0x57,0xae,0x1e,
      0xa6,0x37,0xcd,0x95,0x57,0xcd,0x6d,0x7d,
      0xcd,0x4e,0x7b,0xa4,0xe7,0xb7,0x2,0xcd,
      0x4e,0x84,0xd,0x89,0x9,0x19,0x83,0xa6,
      0x3,0xcd,0x4f,0x20,0x1d,0x89,0x5,0x84,
      0x8,0x19,0x83,0x4f,0xcd,0x4f,0x20,0x15,
      0x84,0,0x83,0x7,0xc6,0x1,0x2d,0xa5,
      0x20,0x27,0x11,0xcd,0x7f,0xc9,0x4d,0x27,
      0xb,0x11,0x83,0x65,0xad,0x6b,0xcd,0x5d,
      0x1a,0x4d,0x26,0xf8,0xad,0x5e,0xad,0x61,
      0xb6,0x82,0xa5,0x80,0x26,0x7,0xc6,0x1,
      0x2d,0xa5,0x8,0x27,0x13,0xad,0x4d,0xcd,
      0x6d,0x83,0x4d,0x27,0xb,0x1f,0x82,0x65,
      0xad,0x47,0xcd,0x5d,0x1a,0x4d,0x26,0xf8,
      0x8,0x83,0x28,0xcd,0x62,0x75,0xad,0x34,
      0xad,0x37,0xad,0x30,0xcd,0x44,0x62,0x4d,
      0x27,0x2,0xad,0x2d,0xad,0x26,0xcd,0x62,
      0x75,0xad,0x26,0xad,0x1f,0xcd,0x44,0x62,
      0x4d,0x27,0x8,0xad,0x1c,0x20,0x4,0x4f,
      0xc7,0x1,0x2b,0xcc,0x4c,0x38,0xcd,0x47,
      0xfd,0x4d,0x81,0xb6,0x6,0xaa,0x1f,0xb7,
      0x6,0xb6,0x2,0x81,0x1c,0x6,0x1c,0x2,
      0x81,0xa6,0x1,0xcc,0x52,0x87,0xa6,0xf,
      0xae,0x64,0xad,0x1d,0x81,0xcd,0x82,0x5,
      0xc7,0x1,0x2d,0xcf,0x1,0x2c,0x81,0xa4,
      0xf7,0xaa,0x10,0xb7,0x2,0x81,0xa4,0xe7,
      0xb7,0x2,0xa6,0xa,0xc7,0x1,0x2b,0x4f,
      0x81,0xcd,0x95,0x57,0xa6,0x2e,0x81,0x87,
      0x1c,0x6,0x1c,0x2,0xb6,0x83,0xa5,0x80,
      0x27,0x12,0xd,0x83,0xf,0x95,0xf6,0xae,
      0x4,0xcd,0x48,0x80,0x4f,0xae,0x4,0xcd,
      0x48,0x80,0x8a,0x81,0x95,0xf6,0xae,0x2,
      0xcd,0x59,0xc0,0x8a,0x81,0x87,0x89,0x89,
      0x8a,0x97,0x7d,0x27,0x27,0xf6,0xa1,0x80,
      0x25,0x19,0x20,0x20,0x95,0xf6,0x87,0xee,
      0x1,0x8a,0xf6,0x97,0xa0,0x20,0xa1,0x5e,
      0x22,0x3,0x9f,0xad,0xba,0x95,0x6c,0x1,
      0x26,0x1,0x7c,0x95,0xf6,0x87,0xee,0x1,
      0x8a,0x7d,0x26,0xe0,0xa7,0x2,0x81,0x3f,
      0x77,0x3f,0x75,0x3f,0x69,0x3f,0x38,0xc6,
      0x1,0x48,0x26,0x3,0xcc,0x94,0xca,0x81,
      0x87,0xa7,0xfa,0x41,0x1,0x4,0xa1,0x2,
      0x26,0x3,0xa6,0x2,0x65,0xa6,0x3,0x95,
      0xe7,0x5,0xb6,0x83,0xa5,0x80,0x27,0xa,
      0xd,0x83,0x7,0xa6,0x86,0xcd,0x4e,0xb7,
      0x20,0x3,0xcd,0x61,0x64,0x95,0x6f,0x4,
      0x20,0x54,0xe6,0x6,0xa1,0x3,0x22,0x33,
      0xa0,0x1,0x97,0x4f,0xcd,0x42,0xf2,0x3,
      0x2a,0x2,0x11,0x20,0x95,0xee,0x4,0x58,
      0x8c,0xd6,0xca,0x6,0x9e,0xe7,0x4,0xd6,
      0xca,0x5,0x20,0x25,0x95,0xee,0x4,0x58,
      0x8c,0xd6,0xca,0xa,0x9e,0xe7,0x4,0xd6,
      0xca,0x9,0x20,0x15,0x95,0x6f,0x3,0xa6,
      0x1f,0x20,0xe,0x95,0xee,0x4,0x58,0x8c,
      0xd6,0xca,0xe,0x9e,0xe7,0x4,0xd6,0xca,
      0xd,0x95,0xe7,0x2,0xe6,0x3,0xee,0x2,
      0xcd,0x50,0xb3,0x95,0x6c,0x4,0x95,0xe6,
      0x5,0xe1,0x4,0x22,0xa5,0x6d,0x6,0x27,
      0x3,0xcc,0x50,0x76,0xa6,0x42,0xcd,0x50,
      0xa3,0xcd,0x50,0x9e,0xae,0x1e,0xa6,0xf0,
      0xcd,0x4e,0xdd,0xcd,0x50,0x9e,0xd,0x83,
      0x9,0xa6,0x55,0xcd,0x50,0xab,0xa6,0x42,
      0x20,0x7,0xa6,0x50,0xcd,0x50,0xab,0xa6,
      0x32,0xcd,0x4e,0xb7,0xcd,0x50,0x9e,0xae,
      0x4,0x9e,0xef,0x2,0xae,0x20,0x89,0x9e,
      0xef,0x2,0x8a,0xae,0x4,0xf6,0xa4,0x7,
      0xa1,0x4,0x26,0x4,0xa6,0x55,0x20,0xc,
      0xf6,0xa4,0x7,0xa1,0x3,0x26,0x3,0xa6,
      0x50,0x65,0xa6,0x41,0xcd,0x4e,0xb7,0x3,
      0x85,0x8,0xcd,0x50,0x9e,0xa6,0x4d,0xcd,
      0x50,0xa3,0x5,0x8d,0x12,0xcd,0x50,0x9e,
      0xa6,0x54,0xcd,0x4e,0xb7,0xa6,0x43,0xcd,
      0x4e,0xb7,0xa6,0x4f,0xcd,0x4e,0xb7,0xa6,
      0xa,0xcd,0x4e,0xb7,0xae,0x2a,0x9e,0xef,
      0x2,0xae,0x1f,0x89,0x9e,0xef,0x2,0x8a,
      0xae,0x2a,0x7d,0x27,0x18,0xf6,0xa1,0x80,
      0x24,0x13,0xa6,0x49,0xcd,0x4e,0xb7,0xa6,
      0x44,0xcd,0x4e,0xb7,0xad,0x58,0x95,0xfe,
      0x9e,0xe6,0x2,0xad,0x66,0x95,0x6f,0x1,
      0xae,0x1f,0x89,0x9e,0xef,0x2,0x95,0xee,
      0x2,0x8a,0x7d,0x27,0x19,0xf6,0xa1,0x80,
      0x24,0x14,0xa6,0x53,0xcd,0x4e,0xb7,0xa6,
      0x4e,0xcd,0x4e,0xb7,0xad,0x30,0x95,0xfe,
      0x9e,0xe6,0x2,0xcd,0x4e,0xdd,0xb6,0x83,
      0xa5,0x80,0x27,0xe,0xd,0x83,0xb,0xa6,
      0x96,0xcd,0x4e,0xb7,0x4f,0xcd,0x4e,0xb7,
      0x20,0x3,0xcd,0x61,0x71,0x1f,0x83,0xb6,
      0x6,0xaa,0x1f,0xb7,0x6,0xb6,0x2,0xa4,
      0xe7,0xb7,0x2,0xa7,0x7,0x81,0xa6,0x20,
      0xcc,0x4e,0xb7,0xcd,0x4e,0xb7,0xa6,0x4c,
      0xcc,0x4e,0xb7,0xcd,0x4e,0xb7,0xa6,0x53,
      0xcc,0x4e,0xb7,0xcd,0x4e,0xdd,0xa6,0xa,
      0xcc,0x4e,0xb7,0x87,0x8b,0x12,0x87,0xa4,
      0xf8,0xa1,0xe0,0x26,0x15,0x95,0xe6,0x1,
      0xa4,0x7,0x97,0xa6,0x1,0x5d,0x27,0x3,
      0x48,0x5b,0xfd,0x45,0xb,0,0xfa,0xf7,
      0x20,0x43,0xc6,0xb,0x7,0x26,0x24,0xa6,
      0x2,0x95,0xf7,0xfe,0x8c,0xd6,0xb,0,
      0x26,0x8,0x9e,0xe6,0x2,0xd7,0xb,0,
      0x20,0x2b,0x9e,0xe6,0x2,0xd1,0xb,0,
      0x27,0x23,0x95,0x7c,0xf6,0xa1,0x8,0x24,
      0x1c,0x20,0xe0,0xa6,0x2,0x95,0xf7,0xfe,
      0x8c,0xd6,0xb,0,0x26,0x8,0x9e,0xe6,
      0x2,0xd7,0xb,0,0x20,0x7,0x95,0x7c,
      0xf6,0xa1,0x8,0x25,0xea,0xa6,0x14,0xcd,
      0x62,0x5b,0xa6,0x2a,0xc7,0x1,0x49,0x20,
      0x1c,0x1c,0x6,0x1c,0x2,0xb6,0x86,0xa5,
      0x80,0x26,0x17,0xa6,0x81,0xcd,0xa1,0xdd,
      0x95,0xf7,0x27,0x15,0xa1,0x8,0x24,0x5,
      0x7,0x8d,0xa,0x20,0xa,0xc6,0x1,0x49,
      0x26,0xdf,0x6,0x8d,0x2,0x1e,0x86,0xa6,
      0x1,0xa7,0x2,0x81,0x87,0x8b,0x1c,0x6,
      0x1c,0x2,0xa4,0xf8,0xa1,0xe0,0x26,0x16,
      0x95,0xe6,0x1,0xa4,0x7,0x97,0xa6,0x1,
      0x5d,0x27,0x3,0x48,0x5b,0xfd,0x43,0x45,
      0xb,0,0xf4,0xf7,0x20,0x2b,0xa6,0x2,
      0x95,0xf7,0xfe,0x8c,0x9e,0xe6,0x2,0xd1,
      0xb,0,0x27,0xc,0x20,0x14,0xfe,0x8c,
      0xd6,0xb,0x1,0xd7,0xb,0,0x95,0x7c,
      0x95,0xf6,0xa1,0x7,0x25,0xf0,0x4f,0xc7,
      0xb,0x7,0x95,0x7c,0xf6,0xa1,0x8,0x25,
      0xd9,0xa6,0x14,0xcd,0x62,0x5b,0xa6,0x2a,
      0xc7,0x1,0x49,0x20,0x1c,0x1c,0x6,0x1c,
      0x2,0xb6,0x86,0xa5,0x80,0x26,0x17,0xa6,
      0x81,0xcd,0xa1,0xdd,0x95,0xf7,0x27,0x15,
      0xa1,0x8,0x24,0x5,0x7,0x8d,0xa,0x20,
      0xa,0xc6,0x1,0x49,0x26,0xdf,0x6,0x8d,
      0x2,0x1e,0x86,0xa6,0x1,0xa7,0x2,0x81,
      0x87,0x8b,0x95,0x7f,0x8c,0x9e,0xee,0x1,
      0x4f,0xd7,0xb,0,0x95,0x7c,0xf6,0xa1,
      0x8,0x25,0xf1,0x6d,0x1,0x27,0x5,0xa6,
      0x81,0xcd,0xa1,0xdd,0xa7,0x2,0x81,0x18,
      0x7,0x18,0x3,0x1a,0x6,0x1a,0x2,0x1c,
      0x6,0x1c,0x2,0xcc,0xc1,0x3c,0xa7,0xfe,
      0xae,0x4,0x9e,0xef,0x2,0xa6,0x20,0x95,
      0xf7,0x14,0x85,0x1f,0x83,0xcd,0x82,0x49,
      0xc7,0x1,0x1a,0x95,0xf6,0x87,0x8a,0xae,
      0x4,0xf6,0xa4,0x7,0x41,0x4,0x10,0x41,
      0x3,0xa,0x1c,0x6,0x1c,0x2,0xcd,0x62,
      0xdb,0x4d,0x26,0x3,0x1d,0x83,0x65,0x1c,
      0x83,0xd,0x83,0x1d,0xcd,0x8c,0x7f,0x1e,
      0x9,0x1e,0x8,0xcd,0xc1,0x3c,0xa6,0x1,
      0xcd,0x69,0x55,0x1e,0x9,0x1f,0x8,0x18,
      0x7,0x18,0x3,0x1a,0x6,0x1b,0x2,0x20,
      0xd,0x18,0x7,0x19,0x3,0x1a,0x6,0x1b,
      0x2,0x1a,0x84,0xcd,0x62,0xc4,0xa7,0x2,
      0x81,0x87,0xa7,0xfe,0xae,0x4,0x9e,0xef,
      0x2,0xa6,0x20,0x9e,0xe7,0x1,0x87,0x8a,
      0xf6,0xa4,0xf8,0x9e,0xeb,0x3,0xf7,0xcd,
      0x92,0xc5,0x14,0x82,0xa7,0x3,0x81,0x87,
      0x8b,0x6,0x85,0x6e,0x1c,0x6,0x1c,0x2,
      0xc6,0x1,0x13,0xa1,0x1,0x23,0x2,0x16,
      0x85,0x45,0x1,0x13,0x7a,0xc6,0x1,0x13,
      0x26,0xc,0xcd,0x53,0x11,0x4c,0xc7,0x1,
      0x13,0x95,0xe6,0x1,0xf7,0x65,0x95,0x7f,
      0xd,0x83,0x3b,0x18,0x7,0x18,0x3,0x1a,
      0x6,0x1b,0x2,0x6d,0x1,0x27,0x28,0xc6,
      0xb,0x12,0xa1,0x5,0x25,0x2,0x5f,0x65,
      0xae,0x1,0xc6,0x1,0xf7,0xa1,0x5,0x25,
      0x2,0x4f,0x65,0xa6,0x1,0x89,0x95,0xfa,
      0x8a,0x27,0xc,0xa6,0x2,0xc7,0x1,0x2b,
      0xa6,0xff,0xcd,0x62,0x5f,0x20,0x1d,0x95,
      0xf6,0xcd,0x8c,0xc3,0x20,0xc,0x18,0x7,
      0x19,0x3,0x1a,0x6,0x1b,0x2,0xf6,0xcd,
      0x67,0x3d,0x95,0x7d,0x27,0x6,0xcd,0x5d,
      0x1a,0x4d,0x26,0x85,0xa7,0x2,0x81,0x45,
      0x1,0x13,0xf6,0xab,0x3c,0xf7,0x16,0x85,
      0x81,0xa7,0xfe,0x1c,0x6,0x1c,0x2,0x6,
      0x84,0x3,0x4f,0x20,0x32,0xc6,0x1,0x2e,
      0xa5,0x7f,0x27,0xb,0xa4,0x7,0xab,0x8,
      0x95,0xf7,0xc6,0x1,0x2e,0x20,0xc,0xc6,
      0x1,0x1a,0xa4,0x7,0xab,0x8,0x95,0xf7,
      0xc6,0x1,0x1a,0x44,0x44,0x44,0xa4,0x3,
      0xe7,0x1,0x20,0x3,0x78,0x6a,0x1,0x6d,
      0x1,0x26,0xf9,0xf6,0x44,0x44,0x44,0xa7,
      0x2,0x81,0x87,0xa7,0xfe,0xa1,0x90,0x25,
      0x7,0x8c,0x97,0xde,0xca,0x3,0x20,0x9,
      0xa1,0x80,0x24,0x8,0x8c,0x97,0xde,0xca,
      0x13,0x9e,0xef,0x3,0x95,0xe6,0x2,0xa1,
      0x9e,0x25,0x18,0xa0,0x9e,0x48,0x87,0x48,
      0x4f,0xa2,0,0x87,0x95,0xe6,0x1,0xab,
      0x30,0xe7,0x3,0x86,0xa9,0x21,0xe7,0x2,
      0x8a,0x20,0xd,0x48,0x5f,0x59,0xab,0x8,
      0x9e,0xe7,0x2,0x9f,0xa9,0x20,0x95,0xf7,
      0x95,0xf6,0x87,0xee,0x1,0x8a,0xe6,0x1,
      0xfa,0x27,0x34,0xf6,0xee,0x1,0x87,0x8a,
      0x65,0x20,0,0x90,0x2a,0x1c,0x6,0x1c,
      0x2,0x65,0x1,0,0x90,0x24,0x95,0xe6,
      0x1,0xc7,0x1,0x30,0xf6,0xc7,0x1,0x2f,
      0x45,0x1,0x2f,0x6c,0x1,0x26,0x1,0x7c,
      0xce,0x1,0x2f,0x89,0x8a,0xce,0x1,0x30,
      0xf6,0xa0,0x62,0xa1,0x6,0x23,0x70,0xcc,
      0x54,0x77,0x95,0xfe,0x89,0x95,0xee,0x2,
      0x8a,0xee,0x1,0x9e,0xef,0x2,0xab,0x20,
      0x95,0xf7,0x18,0x87,0x1c,0x6,0x1c,0x2,
      0x95,0xf6,0x87,0xee,0x1,0x8a,0xf6,0xee,
      0x1,0x87,0x8a,0x65,0x20,0,0x24,0x77,
      0xc6,0x1,0x32,0xa5,0x1,0x26,0x2,0x4f,
      0x65,0xa6,0x2,0x87,0xc6,0x1,0x32,0xa4,
      0xfe,0x95,0xfa,0xf7,0xc6,0x1,0x33,0xa4,
      0xc,0xfa,0xf7,0xcd,0x84,0x42,0x95,0xfa,
      0xf7,0xe6,0x1,0x87,0xee,0x2,0x8a,0x86,
      0xe4,0x3,0xe1,0x2,0x26,0x1d,0x9f,0xab,
      0x5,0xc7,0x1,0x30,0x8b,0x86,0xa9,0,
      0xc7,0x1,0x2f,0x87,0x8a,0xce,0x1,0x30,
      0xf6,0xa0,0x62,0xa1,0x6,0x22,0x30,0xa6,
      0x1,0x20,0x2d,0xf6,0xee,0x1,0x87,0x8a,
      0x65,0x1,0x2e,0x23,0x22,0x95,0xfe,0x89,
      0x95,0xee,0x2,0x8a,0xee,0x1,0x9e,0xef,
      0x2,0xab,0x20,0x9e,0xe7,0x1,0x87,0x8a,
      0x65,0x20,0,0x27,0x3,0xcc,0x53,0xec,
      0x4f,0xc7,0x1,0x30,0xc7,0x1,0x2f,0x4f,
      0xa7,0x3,0x81,0x87,0x89,0x8b,0xa1,0x90,
      0x25,0x7,0x8c,0x97,0xde,0xca,0x3,0x20,
      0x9,0xa1,0x80,0x24,0x8,0x8c,0x97,0xde,
      0xca,0x13,0x9e,0xef,0x3,0x95,0xe6,0x2,
      0xc7,0x1,0xac,0xae,0x1,0xcd,0x56,0x76,
      0x95,0xf7,0x6d,0x1,0x27,0x7,0xa6,0xff,
      0xc7,0x1,0x15,0x20,0x54,0x8,0x87,0x4e,
      0x4d,0x2a,0x13,0,0x85,0x10,0xa6,0xe0,
      0x5f,0xcd,0x59,0xc0,0x95,0xf6,0xa4,0x7f,
      0x5f,0xcd,0x59,0xc0,0x20,0x3b,0x7d,0x27,
      0x38,0x5f,0xcd,0x59,0xc0,0x20,0x32,0xc,
      0x83,0x2,0x5f,0x65,0xae,0xff,0x9e,0xe6,
      0x1,0x41,0xf8,0x2,0x4f,0x65,0xa6,0x1,
      0x89,0x95,0xf4,0x8a,0x27,0x7,0xa6,0x1,
      0xcd,0x59,0x61,0x20,0xc,0x95,0xf6,0xa,
      0x87,0x2,0x5f,0x65,0xae,0x2,0xcd,0x59,
      0xc0,0xcd,0x59,0x1d,0x95,0xf7,0x7d,0x26,
      0xce,0xa6,0x1,0xa7,0x3,0x81,0x87,0x8b,
      0xa1,0x90,0x25,0x7,0x8c,0x97,0xde,0xca,
      0x3,0x20,0x9,0xa1,0x80,0x24,0x8,0x8c,
      0x97,0xde,0xca,0x13,0x9e,0xef,0x2,0x95,
      0xe6,0x1,0x45,0x1,0xac,0xf7,0xc6,0x1,
      0x32,0xaa,0x80,0xc7,0x1,0x32,0xf6,0x5f,
      0xcd,0x56,0x76,0x95,0xf7,0x8,0x87,0x66,
      0x1,0x85,0x15,0x7d,0x27,0x63,0xa1,0x81,
      0x24,0x5f,0xa6,0xf0,0x5f,0xcd,0x59,0xc0,
      0x95,0xf6,0x5f,0xcd,0x59,0xc0,0x20,0x51,
      0x4d,0x2a,0x6,0xa6,0xe0,0x5f,0xcd,0x59,
      0xc0,0x95,0x7d,0x27,0x44,0xf6,0xaa,0x80,
      0x5f,0xcd,0x59,0xc0,0x20,0x3b,0xd,0x87,
      0x7,0xa,0x87,0x4,0xf6,0xaa,0x80,0xf7,
      0xc,0x83,0x2,0x5f,0x65,0xae,0xff,0x9e,
      0xe6,0x1,0x41,0xf8,0x2,0x4f,0x65,0xa6,
      0x1,0x89,0x95,0xf4,0x8a,0x27,0x6,0x4f,
      0xcd,0x59,0x61,0x20,0xc,0x95,0xf6,0xa,
      0x87,0x2,0x5f,0x65,0xae,0x2,0xcd,0x59,
      0xc0,0xcd,0x59,0x1d,0x95,0xf7,0x7d,0x26,
      0xc5,0x45,0x1,0x32,0xf6,0xa4,0x7f,0xf7,
      0xc6,0x1,0x15,0x95,0xe1,0x1,0x26,0x5,
      0xa6,0xff,0xc7,0x1,0x15,0xe6,0x1,0xa1,
      0x8b,0x23,0x2,0x17,0x84,0xa1,0x80,0x24,
      0x2,0x17,0x84,0xa6,0x1,0xa7,0x2,0x81,
      0x8b,0xc6,0x1,0x15,0x41,0xff,0x44,0xc7,
      0x1,0xac,0xae,0x1,0xcd,0x56,0x76,0x95,
      0xf7,0x27,0x6,0xc6,0x1,0x15,0xc7,0x1,
      0xac,0x8,0x87,0x2c,0xf6,0x2a,0x10,0xa6,
      0xe0,0x5f,0xcd,0x59,0xc0,0x95,0xf6,0xa4,
      0x7f,0x5f,0xcd,0x59,0xc0,0x20,0x1c,0x7d,
      0x27,0x19,0x5f,0xcd,0x59,0xc0,0x20,0x13,
      0xf6,0xa,0x87,0x2,0x5f,0x65,0xae,0x2,
      0xcd,0x59,0xc0,0xcd,0x59,0x1d,0x95,0xf7,
      0x7d,0x26,0xed,0xa6,0x1,0x8a,0x81,0x87,
      0xa7,0xfe,0xa1,0x90,0x25,0x7,0x8c,0x97,
      0xde,0xca,0x3,0x20,0x9,0xa1,0x80,0x24,
      0x8,0x8c,0x97,0xde,0xca,0x13,0x9e,0xef,
      0x3,0x95,0xe6,0x2,0xa1,0x9e,0x25,0x18,
      0xa0,0x9e,0x48,0x87,0x48,0x4f,0xa2,0,
      0x87,0x95,0xe6,0x1,0xab,0x30,0xe7,0x3,
      0x86,0xa9,0x21,0xe7,0x2,0x8a,0x20,0xd,
      0x48,0x5f,0x59,0xab,0x8,0x9e,0xe7,0x2,
      0x9f,0xa9,0x20,0x95,0xf7,0x95,0xf6,0x87,
      0xee,0x1,0x8a,0xe6,0x1,0xfa,0x26,0x9,
      0x4f,0xc7,0x1,0x30,0xc7,0x1,0x2f,0x4f,
      0x65,0xa6,0x1,0xa7,0x3,0x81,0x87,0x89,
      0xa7,0xfb,0x19,0x87,0x1d,0x87,0x1f,0x87,
      0x1,0x84,0x32,0xcd,0x95,0x93,0x95,0xe6,
      0x6,0xa1,0x50,0x23,0x4,0xa0,0x51,0xe7,
      0x6,0xe6,0x6,0xa1,0x50,0x23,0x4,0xa0,
      0x51,0xe7,0x6,0xe6,0x6,0xa1,0x44,0x26,
      0x4,0xa6,0x51,0x20,0x79,0xa1,0x45,0x26,
      0x4,0xa6,0x52,0x20,0x71,0xa1,0x39,0x26,
      0xf,0xa6,0x53,0x20,0x69,0x1c,0x6,0x1c,
      0x2,0x1,0x85,0x65,0xa1,0x80,0x24,0x3,
      0x4c,0x20,0x5b,0xa1,0x8d,0x23,0x3,0xcc,
      0x58,0xff,0xa0,0x80,0x97,0x4f,0xcd,0x42,
      0xc3,0,0xe,0x58,0xff,0x56,0xf1,0x57,
      0x9,0x56,0xf5,0x57,0xd,0x56,0xf9,0x57,
      0x11,0x57,0x1,0x57,0x19,0x57,0x5,0x57,
      0x1c,0x57,0x5,0x57,0x1c,0x56,0xfd,0x57,
      0x15,0xa6,0x81,0x20,0x29,0xa6,0x82,0x20,
      0x25,0xa6,0x83,0x20,0x21,0xa6,0x84,0x20,
      0x1d,0xa6,0x85,0x20,0x19,0xa6,0x86,0x20,
      0x15,0xa6,0x91,0x20,0x11,0xa6,0x92,0x20,
      0xd,0xa6,0x93,0x20,0x9,0xa6,0x94,0x20,
      0x5,0xa6,0x95,0x65,0xa6,0x96,0xcc,0x57,
      0xa6,0xa1,0x9e,0x25,0x18,0xa0,0x9e,0x48,
      0x87,0x48,0x4f,0xa2,0,0x87,0x95,0xe6,
      0x1,0xab,0x30,0xe7,0x5,0x86,0xa9,0x21,
      0xe7,0x4,0x8a,0x20,0xe,0x48,0x5f,0x59,
      0xab,0x8,0x9e,0xe7,0x4,0x9f,0xa9,0x20,
      0x95,0xe7,0x2,0xcd,0x59,0x3,0xe6,0x1,
      0xfa,0x26,0x3,0xcc,0x58,0xf3,0xf6,0xee,
      0x1,0x87,0x8a,0x65,0x20,0,0x91,0x3,
      0xcc,0x58,0xf8,0x1c,0x6,0x1c,0x2,0x65,
      0x1,0,0x90,0x3d,0x95,0xe6,0x3,0xc7,
      0x1,0x30,0xe6,0x2,0xc7,0x1,0x2f,0x45,
      0x1,0x2f,0x6c,0x1,0x26,0x1,0x7c,0xcd,
      0x59,0xb,0x7d,0x27,0x1d,0x17,0x84,0x95,
      0x6d,0x5,0x27,0xe,0xcd,0x95,0x93,0xc6,
      0x1,0x15,0x95,0xe1,0x6,0x27,0x3,0xcd,
      0x59,0x14,0xd,0x83,0x5,0xa6,0xff,0xc7,
      0x1,0x15,0xcd,0x59,0xb,0xf6,0xcc,0x58,
      0xd0,0x95,0xee,0x2,0x89,0x95,0xee,0x4,
      0x8a,0xee,0x1,0x9e,0xef,0x4,0xab,0x20,
      0x95,0xe7,0x2,0x18,0x87,0x1c,0x6,0x1c,
      0x2,0xcd,0x59,0x3,0xf6,0xee,0x1,0x87,
      0x8a,0x65,0x20,0,0x25,0x3,0xcc,0x58,
      0xf8,0xcd,0x59,0x3,0xe6,0x4,0x2a,0x19,
      0xc6,0x1,0x32,0xa4,0x80,0x9e,0xe7,0x1,
      0xca,0x1,0x14,0x9e,0xe7,0x5,0x6d,0x2,
      0x26,0x37,0x9e,0xe6,0x1,0xea,0x2,0x20,
      0x2d,0xc6,0x1,0x32,0xa5,0x1,0x26,0x2,
      0x4f,0x65,0xa6,0x2,0x95,0xe7,0x1,0xc6,
      0x1,0x32,0xa4,0xfe,0xea,0x1,0x87,0xc6,
      0x1,0x33,0xa4,0xc,0x95,0xfa,0xf7,0xcd,
      0x84,0x42,0x95,0xfa,0xf7,0xe6,0x3,0x87,
      0xee,0x4,0x8a,0x86,0xe4,0x3,0x95,0xe7,
      0x4,0xcd,0x59,0x3,0xe6,0x2,0x9e,0xe1,
      0x5,0x27,0x3,0xcc,0x58,0xd2,0xe6,0x4,
      0xa5,0x40,0x26,0x3,0x1b,0x87,0x65,0x1a,
      0x87,0xe6,0x4,0xa5,0x8,0x26,0x3,0x1d,
      0x87,0x65,0x1c,0x87,0xe6,0x4,0xa5,0x20,
      0x26,0x3,0x17,0x84,0x65,0x16,0x84,0x9f,
      0xab,0x5,0xc7,0x1,0x30,0x8b,0x86,0xa9,
      0,0xc7,0x1,0x2f,0x87,0x8a,0xce,0x1,
      0x30,0x7d,0x27,0x5f,0xcd,0x59,0x3,0xe6,
      0x4,0xa5,0x10,0x26,0x20,0xcd,0x61,0x64,
      0x1e,0x87,0xa,0x87,0x18,0xcd,0x59,0x3,
      0xe6,0x4,0x2b,0x11,0xe6,0x2,0xa4,0xfe,
      0x87,0xc6,0x1,0x32,0xa4,0x30,0x95,0xfa,
      0x5f,0xcd,0x60,0x13,0x8a,0x95,0x6d,0x5,
      0x27,0x31,0xad,0x6f,0xe6,0x4,0xa5,0x1,
      0x27,0x3,0xcd,0x95,0x93,0xd,0x83,0xa,
      0xd,0x87,0x7,0xa6,0xff,0xc7,0x1,0x15,
      0x20,0x19,0xad,0x57,0xe6,0x4,0xa5,0x2,
      0x27,0xc,0xc6,0x1,0x15,0x95,0xe1,0x6,
      0x27,0x9,0xad,0x58,0x20,0x5,0xa6,0xff,
      0xc7,0x1,0x15,0xad,0x46,0xf6,0xaf,0x1,
      0xcf,0x1,0x30,0x8b,0x88,0xcf,0x1,0x2f,
      0x20,0x2e,0xf6,0xee,0x1,0x87,0x8a,0x65,
      0x1,0x2e,0x23,0x23,0x95,0xee,0x2,0x89,
      0x95,0xee,0x4,0x8a,0xee,0x1,0x9e,0xef,
      0x4,0xab,0x20,0x9e,0xe7,0x3,0x87,0x8a,
      0x65,0x20,0,0x27,0x3,0xcc,0x57,0xbd,
      0x4f,0xc7,0x1,0x30,0xc7,0x1,0x2f,0x4f,
      0xa7,0x7,0x81,0x95,0xe6,0x4,0x87,0xee,
      0x5,0x8a,0x81,0xce,0x1,0x2f,0x89,0x8a,
      0xce,0x1,0x30,0x81,0x95,0xe6,0x8,0xc7,
      0x1,0x15,0xcc,0x62,0x8d,0x1c,0x6,0x1c,
      0x2,0xc6,0x1,0x30,0xca,0x1,0x2f,0x27,
      0x5,0xad,0x2d,0x7d,0x26,0x1e,0xad,0x28,
      0x7d,0x26,0x17,0xb6,0x87,0xa5,0x80,0x27,
      0xa,0xa6,0x1,0xcd,0x52,0x87,0xcd,0x61,
      0x71,0x1f,0x87,0x4f,0xc7,0x1,0x30,0xc7,
      0x1,0x2f,0x4f,0x81,0xf6,0xaf,0x1,0xcf,
      0x1,0x30,0x8b,0x88,0xcf,0x1,0x2f,0x81,
      0xce,0x1,0x2f,0x89,0x8a,0xce,0x1,0x30,
      0x81,0x87,0x8b,0xad,0xb8,0x95,0xf7,0xa6,
      0x1,0xcd,0x52,0x87,0xcd,0x5d,0x1a,0x4d,
      0x26,0xf5,0x95,0xf6,0xa1,0x10,0x26,0x12,
      0xad,0xa3,0x95,0xf7,0x6d,0x1,0x27,0x5,
      0xcd,0x50,0xbb,0x20,0x30,0xcd,0x51,0x54,
      0x20,0x2b,0xc7,0xb,0x8,0xad,0x8e,0xc7,
      0xb,0x9,0xad,0x89,0xc7,0xb,0xa,0xad,
      0x84,0xc7,0xb,0xb,0xcd,0x59,0x1d,0xc7,
      0xb,0xc,0x95,0x6d,0x1,0x26,0xb,0x4f,
      0x45,0xb,0x9,0xf7,0xe7,0x1,0xe7,0x2,
      0xe7,0x3,0xcd,0x48,0xeb,0xa7,0x2,0x81,
      0x4f,0xc7,0x1,0x37,0xc7,0x1,0x36,0x81,
      0x87,0x89,0x20,0x10,0x1c,0x6,0x1c,0x2,
      0xb6,0x86,0xa4,0x80,0x41,0x80,0x14,0xa6,
      0x1,0xcd,0x52,0x87,0x95,0xe6,0x1,0xfe,
      0xcd,0x5a,0x14,0x4d,0x27,0xe6,0xa6,0x1,
      0xcd,0x52,0x87,0xa7,0x2,0x81,0x87,0x8b,
      0xa4,0xf0,0x62,0xa4,0xf,0x95,0xf7,0xa1,
      0xa,0x24,0x3,0xab,0x30,0x65,0xab,0x57,
      0xae,0x2,0xcd,0x5a,0x14,0x95,0xe6,0x1,
      0xa4,0xf,0xf7,0xa1,0xa,0x24,0x3,0xab,
      0x30,0x65,0xab,0x57,0xae,0x2,0xcd,0x5a,
      0x14,0xa7,0x2,0x81,0x87,0x89,0xa7,0xfc,
      0x1c,0x6,0x1c,0x2,0xcd,0x5d,0x1a,0xa1,
      0x6,0x25,0x3,0xcc,0x5b,0x76,0x95,0xee,
      0x4,0xa3,0x4,0x22,0x1e,0x4f,0xcd,0x42,
      0xc3,0,0x5,0x5b,0x73,0x5a,0x3f,0x5a,
      0x44,0x5a,0x4e,0x5b,0x17,0x5a,0x44,0x95,
      0xe6,0x5,0x20,0x1b,0x95,0xe6,0x5,0xa1,
      0xe0,0x26,0x14,0xcc,0x5b,0x15,0xb6,0x84,
      0xa5,0x80,0x27,0xe,0xcd,0x5b,0x91,0x26,
      0x3,0xcc,0x5b,0x15,0x95,0xe6,0x5,0xcc,
      0x5b,0x12,0xcd,0x5b,0x91,0x26,0x3,0xcc,
      0x5b,0x15,0x95,0xe6,0x5,0xcd,0x5d,0x49,
      0x9e,0xe7,0x4,0x9e,0xef,0x3,0x9e,0xea,
      0x3,0x26,0x3,0xcc,0x5b,0x17,0x9e,0xe6,
      0x4,0x9e,0xe7,0x6,0xa3,0x7,0x22,0x7a,
      0x9f,0xa0,0x1,0x97,0x4f,0xcd,0x42,0xf2,
      0x7,0x70,0x6,0xd,0x1a,0x27,0x2e,0x3b,
      0x4e,0xcd,0x5b,0x7a,0xcd,0x5f,0xd5,0x20,
      0x61,0xcd,0x5f,0xa3,0xcd,0x5f,0xef,0xcd,
      0x5f,0xd5,0xcd,0x5f,0x8b,0x20,0x53,0xcd,
      0x5f,0x5e,0xcd,0x5f,0xef,0xcd,0x5f,0xd5,
      0xcd,0x5f,0xc1,0x20,0x45,0xcd,0x5b,0x7a,
      0xcd,0x5f,0xe2,0x20,0x3d,0xcd,0x5f,0x5e,
      0xcd,0x5f,0xa3,0xcd,0x5f,0xd5,0xcd,0x60,
      0x1,0x20,0x2f,0x8,0x82,0xc,0xc6,0x1,
      0x32,0xa5,0x40,0x27,0x5,0xcd,0x5f,0x8b,
      0x20,0x17,0xcd,0x5f,0x5e,0x20,0x12,0x8,
      0x82,0xc,0xc6,0x1,0x32,0xa5,0x40,0x27,
      0x5,0xcd,0x5f,0x5e,0x20,0x3,0xcd,0x5f,
      0x8b,0xcd,0x5f,0xa3,0xcd,0x5f,0xd5,0xcd,
      0x5f,0xef,0x95,0xe6,0x5,0xcd,0x5c,0xf0,
      0xc6,0x1,0x2e,0x2b,0x66,0x95,0xe6,0x5,
      0xaa,0x80,0xcd,0x5c,0xf0,0x20,0x5c,0xad,
      0x78,0x27,0x58,0xad,0x5d,0xcd,0x5f,0xd5,
      0xcd,0x5f,0xe2,0x95,0xe6,0x5,0x8c,0xae,
      0x64,0xcd,0x42,0x58,0x95,0xe7,0x1,0x8c,
      0x97,0xd6,0xda,0xc3,0x95,0xf7,0xcd,0x5c,
      0xf0,0x95,0xf6,0xaa,0x80,0xcd,0x5c,0xf0,
      0x95,0xe6,0x1,0xae,0x64,0x42,0x87,0x95,
      0xe6,0x6,0xf0,0xe7,0x6,0xae,0xa,0x8c,
      0xcd,0x42,0x58,0x95,0xe7,0x2,0x8c,0x97,
      0xd6,0xda,0xc3,0xad,0x26,0xa6,0xa,0x95,
      0xee,0x2,0x42,0x95,0xf7,0xe6,0x6,0xf0,
      0x8c,0x97,0xd6,0xda,0xc3,0xad,0x14,0xcd,
      0x5f,0xd5,0x8a,0xa6,0x1,0x21,0x4f,0xa7,
      0x6,0x81,0xcd,0x5f,0x5e,0xcd,0x5f,0xa3,
      0xcc,0x5f,0xef,0x95,0xe7,0x3,0xcd,0x5c,
      0xf0,0x95,0xe6,0x3,0xaa,0x80,0xcc,0x5c,
      0xf0,0x95,0xe6,0x7,0xcd,0x5b,0x99,0x4d,
      0x81,0xa7,0xfd,0x1c,0x6,0x1c,0x2,0xcd,
      0x82,0x92,0x9e,0xe7,0x2,0x9e,0xef,0x1,
      0x95,0xfa,0x27,0x57,0xee,0x1,0x9e,0xe6,
      0x1,0x87,0x8a,0xf6,0x95,0xe7,0x2,0x6c,
      0x1,0x26,0x1,0x7c,0xa1,0x8,0x23,0x4,
      0xa6,0x1,0x20,0x41,0xcd,0x5f,0x5e,0xcd,
      0x5f,0xa3,0xcd,0x5f,0xef,0xcd,0x5f,0xd5,
      0x95,0xe6,0x2,0xa1,0x1,0x26,0x1f,0xf6,
      0x87,0xee,0x1,0x8a,0xf6,0x41,0x80,0x21,
      0x20,0x14,0x95,0xf6,0x87,0xee,0x1,0x8a,
      0xf6,0xaf,0x1,0x8b,0x9e,0xef,0x3,0x88,
      0x9e,0xef,0x1,0xcd,0x5c,0xf0,0x95,0xe6,
      0x2,0x97,0x4a,0x9e,0xe7,0x3,0x5d,0x26,
      0xe1,0x4f,0x65,0xa6,0x2,0xa7,0x3,0x81,
      0x87,0x8b,0x17,0x82,0x1c,0x6,0x1c,0x2,
      0xce,0x1,0x37,0xc3,0x1,0x36,0x27,0x4b,
      0x8c,0xd6,0x1,0x38,0x9e,0xe7,0x1,0xc,
      0x83,0x24,0x41,0xe1,0x21,0x41,0xe0,0x1e,
      0xb,0x84,0x1b,0xa1,0xea,0x24,0x17,0xa1,
      0x80,0x23,0x13,0x9e,0xe6,0x2,0xa1,0x1,
      0x26,0x8,0x9e,0xe6,0x1,0xa4,0x7f,0xd7,
      0x1,0x38,0xa6,0xf0,0x20,0x21,0x95,0xe6,
      0x1,0x4b,0xb,0x45,0x1,0x37,0x7c,0x45,
      0x1,0x37,0xf6,0xa4,0xf,0xf7,0x95,0xf6,
      0xa1,0xff,0x26,0xe,0x6d,0x1,0x26,0x3,
      0xcd,0x95,0xbd,0x16,0x82,0xa6,0x80,0xcc,
      0x5c,0xed,0xa1,0xfe,0x26,0x9,0x6d,0x1,
      0x26,0xf1,0xcd,0x53,0x7,0x20,0xec,0xa1,
      0xfb,0x26,0x8,0x6d,0x1,0x26,0xe4,0x1e,
      0x85,0x20,0xe0,0xa1,0xfc,0x26,0x9,0x6d,
      0x1,0x26,0xd8,0xcd,0x84,0x3a,0x20,0xd3,
      0xa1,0xfd,0x26,0x9,0x6d,0x1,0x26,0xcb,
      0xcd,0x84,0x3e,0x20,0xc6,0xa1,0xfa,0x26,
      0xf,0xd,0x83,0x7,0xa6,0x1,0xcd,0x51,
      0xd8,0x20,0xb8,0xcd,0x48,0x49,0x20,0xb3,
      0xa1,0xf9,0x26,0x7,0xa6,0xa0,0xc7,0x1,
      0x14,0x20,0xa8,0xce,0x1,0x14,0xa3,0xa0,
      0x26,0x7,0xa4,0x7f,0xc7,0x1,0x14,0x20,
      0x9a,0xd,0x83,0x14,0xa1,0x80,0x23,0x8,
      0x95,0xee,0x1,0xcd,0x91,0x32,0x20,0x11,
      0x95,0xee,0x1,0xcd,0x90,0x9b,0x20,0x9,
      0xb,0x84,0x8,0xc,0x84,0x5,0xcd,0x5d,
      0x3f,0x95,0xf7,0x95,0xf6,0xa7,0x2,0x81,
      0x87,0xb6,0x84,0xa5,0x80,0x26,0xb,0,
      0x85,0x8,0x95,0xf6,0xcd,0x5d,0x85,0x4d,
      0x27,0x16,0xce,0x1,0x36,0x8c,0x9e,0xe6,
      0x1,0xd7,0x1,0x38,0x5c,0x9f,0xa4,0xf,
      0xc7,0x1,0x36,0xa6,0xff,0xcd,0x62,0x5b,
      0x8a,0x81,0x8b,0x1c,0x6,0x1c,0x2,0xc6,
      0x1,0x36,0xc0,0x1,0x37,0x95,0xf7,0xce,
      0x1,0x36,0xc3,0x1,0x37,0x24,0x4,0xab,
      0x10,0x95,0xf7,0x95,0x7d,0x26,0x4,0x4f,
      0xcd,0x62,0x5b,0x95,0xf6,0x8a,0x81,0xa1,
      0xe0,0x24,0x5,0x8c,0x97,0xd6,0xda,0xcd,
      0x81,0x87,0x3,0x85,0x5,0xcd,0x83,0xe7,
      0x8a,0x81,0xc6,0x20,0x4,0xa4,0xf8,0x62,
      0xa4,0xf,0xae,0x9,0x89,0x5f,0xcd,0x42,
      0x72,0x8a,0x87,0x9e,0xe6,0x2,0x48,0x87,
      0x4f,0x49,0x87,0x9e,0xe6,0x3,0x9e,0xeb,
      0x2,0x9e,0xe7,0x2,0x9f,0x95,0xf9,0x87,
      0xee,0x1,0x8a,0xd6,0xca,0xc4,0xde,0xca,
      0xc3,0xa7,0x3,0x8a,0x81,0x87,0x8b,0xa1,
      0xe0,0x26,0x4,0x12,0x89,0x20,0x72,0xa1,
      0xe1,0x26,0x4,0x14,0x89,0x20,0x6a,0xa4,
      0x7f,0x95,0xf7,0x2,0x89,0x3,0xcc,0x5e,
      0x2a,0x5,0x89,0x5,0xcd,0x5e,0xed,0x20,
      0x7f,0xa6,0x1,0xfe,0xcd,0x5f,0x2,0x4d,
      0x27,0x5,0xcd,0x5e,0xed,0x20,0x71,0xa6,
      0x6,0x95,0xfe,0xcd,0x5f,0x2,0x95,0xf7,
      0x26,0xb,0xe6,0x1,0x2b,0x2,0x17,0x89,
      0xcd,0x5e,0xfd,0x20,0x7b,0xe6,0x1,0x2a,
      0xc,0xf6,0x43,0x45,0x1,0x33,0xf4,0xf7,
      0xcd,0x5e,0xed,0x20,0x77,0xf6,0xa5,0x70,
      0x27,0x40,0xc6,0x1,0x32,0xa5,0x4,0x27,
      0xf,0x7,0x89,0x7,0xe6,0x1,0x41,0x45,
      0x2,0x17,0x89,0xcd,0x5e,0xed,0x20,0x76,
      0xc6,0x1,0x33,0xf4,0x27,0x5,0xcd,0x5e,
      0xed,0x20,0x6b,0xcd,0x5e,0xf3,0x9,0x82,
      0xc,0x95,0xf6,0xa1,0x40,0x26,0x6,0xc6,
      0x1,0x32,0xfa,0x20,0x5,0xc6,0x1,0x32,
      0x95,0xf8,0xc7,0x1,0x32,0xcd,0x5e,0xed,
      0x20,0x4c,0xcd,0x5e,0xf3,0xcd,0x5e,0xed,
      0x20,0x44,0x5,0x89,0x29,0xa6,0x3,0xfe,
      0xcd,0x5f,0x2,0x4d,0x27,0x5,0x95,0xe6,
      0x1,0x20,0x33,0x95,0xf6,0xa1,0x45,0x26,
      0x10,0xe6,0x1,0x2a,0x5,0xcd,0x5e,0xfd,
      0x20,0x24,0x16,0x89,0xcd,0x5e,0xfd,0x20,
      0x1d,0xcd,0x5e,0xed,0x20,0x18,0xa6,0x6,
      0xfe,0xcd,0x5f,0x2,0x4d,0x26,0x11,0x95,
      0xe6,0x1,0x2b,0x7,0x17,0x89,0xcd,0x5e,
      0xfd,0x20,0x3,0xcd,0x5e,0xfd,0x20,0x7a,
      0x95,0xfe,0xa6,0x6,0xcd,0x5f,0x2,0x95,
      0xf7,0xee,0x1,0x2a,0x16,0xa5,0x70,0x27,
      0x9,0x43,0xc4,0x1,0x33,0xc7,0x1,0x33,
      0x20,0x5d,0x43,0xc4,0x1,0x32,0xc7,0x1,
      0x32,0x20,0x54,0xa5,0x70,0x27,0x35,0xc6,
      0x1,0x32,0xa5,0x4,0x27,0x7,0x51,0x45,
      0x2,0x17,0x89,0x20,0x42,0xc6,0x1,0x33,
      0x9e,0xe4,0x1,0x26,0x3a,0xad,0x44,0x9,
      0x82,0xf,0x9e,0xe6,0x1,0xa1,0x40,0x26,
      0x8,0xc6,0x1,0x32,0x9e,0xea,0x1,0x20,
      0x6,0xc6,0x1,0x32,0x9e,0xe8,0x1,0xc7,
      0x1,0x32,0x20,0x1b,0xc6,0x1,0x32,0x9e,
      0xea,0x1,0xc7,0x1,0x32,0x9,0x82,0xf,
      0x9e,0xe6,0x1,0xa5,0x3,0x27,0x8,0xc6,
      0x1,0x32,0xa4,0xbf,0xc7,0x1,0x32,0xad,
      0x14,0x9f,0xa7,0x2,0x81,0xad,0xe,0x95,
      0xe6,0x3,0x81,0xc6,0x1,0x33,0x9e,0xea,
      0x3,0xc7,0x1,0x33,0x81,0x13,0x89,0x15,
      0x89,0x81,0x89,0xa1,0x6,0x22,0x54,0x97,
      0x4f,0xcd,0x42,0xf2,0x7,0x4d,0x42,0x37,
      0x2c,0x21,0x16,0xb,0,0x95,0xf6,0xa1,
      0x3a,0x26,0x4,0xa6,0x40,0x8a,0x81,0x95,
      0xf6,0xa1,0x45,0x26,0x4,0xa6,0x20,0x8a,
      0x81,0x95,0xf6,0xa1,0x46,0x26,0x4,0xa6,
      0x10,0x8a,0x81,0x95,0xf6,0xa1,0x38,0x26,
      0x4,0xa6,0x8,0x8a,0x81,0x95,0xf6,0xa1,
      0x1d,0x26,0x4,0xa6,0x4,0x8a,0x81,0x95,
      0xf6,0xa1,0x2a,0x26,0x4,0xa6,0x2,0x8a,
      0x81,0x95,0xf6,0xa1,0x36,0x26,0x4,0xa6,
      0x1,0x8a,0x81,0x4f,0x8a,0x81,0xc6,0x1,
      0x32,0xa5,0x1,0x27,0x5,0xa6,0xb6,0xcd,
      0x5c,0xf0,0xc6,0x1,0x32,0xa5,0x2,0x27,
      0x5,0xa6,0xaa,0xcd,0x5c,0xf0,0x9,0x82,
      0x11,0xc6,0x1,0x32,0xa5,0x40,0x27,0xa,
      0xa6,0x2a,0xcd,0x5c,0xf0,0xa6,0xaa,0xcc,
      0x5c,0xf0,0x81,0x9,0x82,0x8,0xc6,0x1,
      0x32,0xa5,0x43,0x27,0x8,0x81,0xc6,0x1,
      0x32,0xa5,0x3,0x26,0x5,0xa6,0x2a,0xcc,
      0x5c,0xf0,0x81,0xc6,0x1,0x33,0xa5,0x4,
      0x27,0xa,0xa6,0xe0,0xcd,0x5c,0xf0,0xa6,
      0x9d,0xcd,0x5c,0xf0,0xc6,0x1,0x32,0xa5,
      0x4,0x27,0x5,0xa6,0x9d,0xcc,0x5c,0xf0,
      0x81,0xc6,0x1,0x33,0xa5,0x4,0x26,0xc,
      0xc6,0x1,0x32,0xa5,0x4,0x26,0x5,0xa6,
      0x1d,0xcc,0x5c,0xf0,0x81,0xc6,0x1,0x32,
      0xa5,0x8,0x27,0x5,0xa6,0xb8,0xcc,0x5c,
      0xf0,0x81,0xc6,0x1,0x32,0xa5,0x8,0x26,
      0x5,0xa6,0x38,0xcc,0x5c,0xf0,0x81,0xc6,
      0x1,0x33,0xa5,0x8,0x27,0xa,0xa6,0xe0,
      0xcd,0x5c,0xf0,0xa6,0xb8,0xcc,0x5c,0xf0,
      0x81,0xc6,0x1,0x33,0xa5,0x8,0x26,0xa,
      0xa6,0xe0,0xcd,0x5c,0xf0,0xa6,0x38,0xcc,
      0x5c,0xf0,0x81,0x87,0x89,0xc6,0x1,0x32,
      0x95,0xe1,0x1,0x26,0x3,0xcc,0x60,0xbd,
      0xe8,0x1,0xa5,0x1,0x27,0xe,0xe6,0x1,
      0xa5,0x1,0x27,0x3,0xa6,0x36,0x65,0xa6,
      0xb6,0xcd,0x61,0x54,0xcd,0x61,0x48,0xa5,
      0x2,0x27,0xf,0x95,0xe6,0x1,0xa5,0x2,
      0x27,0x3,0xa6,0x2a,0x65,0xa6,0xaa,0xcd,
      0x61,0x54,0xcd,0x61,0x48,0xa5,0x4,0x27,
      0xf,0x95,0xe6,0x1,0xa5,0x4,0x27,0x3,
      0xa6,0x1d,0x65,0xa6,0x9d,0xcd,0x61,0x54,
      0xcd,0x61,0x48,0xa5,0x8,0x27,0xf,0x95,
      0xe6,0x1,0xa5,0x8,0x27,0x3,0xa6,0x38,
      0x65,0xa6,0xb8,0xcd,0x61,0x54,0xcd,0x61,
      0x48,0xa5,0x10,0x27,0x1a,0xc6,0x1,0x33,
      0xa5,0x10,0x27,0x9,0xa6,0xc6,0xcd,0x61,
      0x54,0xa6,0x46,0x20,0x7,0xa6,0x46,0xcd,
      0x61,0x54,0xa6,0xc6,0xcd,0x61,0x54,0xcd,
      0x61,0x48,0xa5,0x20,0x27,0x1a,0xc6,0x1,
      0x33,0xa5,0x20,0x27,0x9,0xa6,0xc5,0xcd,
      0x61,0x54,0xa6,0x45,0x20,0x7,0xa6,0x45,
      0xcd,0x61,0x54,0xa6,0xc5,0xcd,0x61,0x54,
      0xcd,0x61,0x48,0xa5,0x40,0x27,0x45,0x9,
      0x82,0x2b,0xc6,0x1,0x32,0xa5,0x40,0x27,
      0x15,0xa5,0x2,0x27,0x9,0xa6,0xaa,0xcd,
      0x61,0x54,0xa6,0x2a,0x20,0x2c,0xa6,0x2a,
      0xad,0x7a,0xa6,0xaa,0x20,0x24,0xc6,0x1,
      0x33,0xa5,0x40,0x26,0xf,0xa6,0x3a,0xad,
      0x6b,0xa6,0xba,0x20,0x15,0xc6,0x1,0x33,
      0xa5,0x40,0x27,0x8,0xa6,0xba,0xad,0x5c,
      0xa6,0x3a,0x20,0x6,0xa6,0x3a,0xad,0x54,
      0xa6,0xba,0xad,0x50,0xc6,0x1,0x33,0x95,
      0xf8,0xa5,0xc,0x27,0x38,0xad,0x49,0xa5,
      0x4,0x27,0x16,0x95,0xf6,0xa5,0x4,0x27,
      0x8,0xa6,0xe0,0xad,0x37,0xa6,0x1d,0x20,
      0x6,0xa6,0xe0,0xad,0x2f,0xa6,0x9d,0xad,
      0x2b,0xad,0x2d,0xa5,0x8,0x27,0x16,0x95,
      0xf6,0xa5,0x8,0x27,0x8,0xa6,0xe0,0xad,
      0x1b,0xa6,0x38,0x20,0x6,0xa6,0xe0,0xad,
      0x13,0xa6,0xb8,0xad,0xf,0xa7,0x2,0x81,
      0xa6,0x1,0xcd,0x52,0x87,0xc6,0x1,0x32,
      0x95,0xe8,0x3,0x81,0x5f,0xcc,0x59,0xc0,
      0xa6,0x1,0xcd,0x52,0x87,0xc6,0x1,0x33,
      0x95,0xe8,0x2,0x81,0xc6,0x1,0x32,0xc7,
      0x1,0x34,0xc6,0x1,0x33,0xc7,0x1,0x35,
      0x81,0xc6,0x1,0x34,0xce,0x1,0x35,0xcc,
      0x60,0x13,0xc6,0x1,0x32,0xa5,0x40,0x27,
      0x11,0xad,0x6b,0xb6,0x2,0xa4,0xfd,0xaa,
      0x2,0xb7,0x2,0xc6,0x1,0xad,0xaa,0x4,
      0x20,0x9,0xad,0x5a,0x13,0x2,0xc6,0x1,
      0xad,0xa4,0xfb,0xc7,0x1,0xad,0xc6,0x1,
      0x32,0xa5,0x20,0x27,0xb,0xad,0x47,0x11,
      0x2,0xc6,0x1,0xad,0xaa,0x2,0x20,0xf,
      0xad,0x3c,0xb6,0x2,0xa4,0xfe,0xaa,0x1,
      0xb7,0x2,0xc6,0x1,0xad,0xa4,0xfd,0xc7,
      0x1,0xad,0xc6,0x1,0x32,0xa5,0x10,0x27,
      0xb,0xad,0x23,0x15,0x2,0xc6,0x1,0xad,
      0xaa,0x1,0x20,0xf,0xad,0x18,0xb6,0x2,
      0xa4,0xfb,0xaa,0x4,0xb7,0x2,0xc6,0x1,
      0xad,0xa4,0xfe,0xc7,0x1,0xad,0x45,0x1,
      0xad,0xf6,0xa4,0x7f,0xf7,0x81,0xb6,0x6,
      0xaa,0x1f,0xb7,0x6,0x81,0xc6,0x1,0x48,
      0x27,0xc,0x45,0x1,0x48,0x7a,0xc6,0x1,
      0x48,0x26,0x3,0xcd,0x94,0xf4,0x45,0x1,
      0x4c,0x7a,0xc6,0x1,0x4c,0x26,0x35,0xa6,
      0xc,0xc7,0x1,0x4c,0xc6,0x1,0x4a,0x27,
      0xb,0x45,0x1,0x4a,0x7a,0xc6,0x1,0x4a,
      0x26,0x2,0x1e,0x86,0xc6,0x1,0x4b,0x27,
      0xb,0x45,0x1,0x4b,0x7a,0xc6,0x1,0x4b,
      0x26,0x2,0x1e,0x86,0xc6,0x1,0xcb,0x27,
      0xb,0x45,0x1,0xcb,0x7a,0xc6,0x1,0xcb,
      0x26,0x2,0x14,0x82,0xc6,0x1,0x49,0x27,
      0x4,0x45,0x1,0x49,0x7a,0xc6,0xb,0x22,
      0x27,0x4,0x45,0xb,0x22,0x7a,0x17,0x85,
      0x15,0x85,0x81,0xc7,0x1,0x4a,0x81,0xc7,
      0x1,0x4b,0x81,0xc7,0x1,0x4a,0x20,0x4,
      0x1c,0x6,0x1c,0x2,0xb6,0x86,0xa5,0x80,
      0x27,0xf6,0x1f,0x86,0x81,0x4,0x85,0x14,
      0x14,0x85,0x45,0xb,0x23,0x7a,0xc6,0xb,
      0x23,0x26,0x9,0xcd,0x62,0xa0,0xc7,0xb,
      0x23,0xcc,0x55,0xc8,0x81,0xc6,0x1,0x1a,
      0x62,0xa4,0xf,0x44,0xa4,0x3,0x4c,0xae,
      0x1e,0x42,0xc7,0xb,0x23,0x14,0x85,0x81,
      0xa7,0xfe,0xc6,0x1,0x1a,0xa4,0x7,0xab,
      0x8,0x95,0xf7,0xc6,0x1,0x1a,0x44,0x44,
      0x44,0xa4,0x3,0xe7,0x1,0x20,0x3,0x78,
      0x6a,0x1,0x6d,0x1,0x26,0xf9,0xf6,0x44,
      0x44,0xa7,0x2,0x81,0x1e,0x9,0x1f,0x8,
      0x1c,0x9,0x1d,0x8,0x1f,0x84,0x1a,0x84,
      0x1d,0x84,0x1d,0x83,0x11,0x85,0x1f,0x84,
      0xcc,0x63,0x9c,0xa7,0xfe,0x95,0x6f,0x1,
      0x7f,0x1c,0x83,0xcd,0x63,0x8a,0xa4,0xef,
      0xaa,0x8,0xb7,0x2,0xa6,0x2,0xc7,0x1,
      0x49,0x20,0x21,0xcd,0x63,0x93,0x27,0xe,
      0xcd,0x6d,0x2e,0x4d,0x27,0x8,0x95,0x6c,
      0x1,0x26,0x7,0x7c,0x20,0x4,0x95,0x6f,
      0x1,0x7f,0xf6,0x87,0xee,0x1,0x8a,0x65,
      0,0x14,0x92,0x39,0xc6,0x1,0x49,0x26,
      0xda,0x18,0x7,0x18,0x3,0x1a,0x6,0x1b,
      0x2,0xa6,0x2,0xc7,0x1,0x49,0x20,0x4,
      0x1c,0x6,0x1c,0x2,0xc6,0x1,0x49,0x26,
      0xf7,0xa6,0xff,0xc7,0x1,0x49,0x20,0xa,
      0xad,0x59,0x26,0xb,0xcd,0x6d,0x2e,0x4d,
      0x26,0x5,0xc6,0x1,0x49,0x26,0xf1,0xcd,
      0x6d,0x2e,0x4d,0x26,0x8,0xad,0x3b,0xa4,
      0xe7,0xb7,0x2,0x20,0x2a,0x18,0x7,0x18,
      0x3,0x1a,0x6,0x1a,0x2,0xa6,0x2,0xc7,
      0x1,0x49,0x20,0x4,0x1c,0x6,0x1c,0x2,
      0xc6,0x1,0x49,0x26,0xf7,0xad,0x1b,0xa4,
      0xe7,0xb7,0x2,0xcd,0x6d,0x2e,0x4d,0x26,
      0x6,0xcd,0x6d,0x36,0x4d,0x27,0x3,0x4f,
      0x20,0x5,0xc6,0xb,0x12,0xa6,0x1,0xa7,
      0x2,0x81,0xb6,0x6,0xaa,0x1f,0xb7,0x6,
      0xb6,0x2,0x81,0x1c,0x6,0x1c,0x2,0xcd,
      0x6d,0x36,0x4d,0x81,0xa6,0xff,0xc7,0x1,
      0x15,0xcc,0x59,0xb8,0x87,0x13,0x86,0x15,
      0x86,0x17,0x83,0x17,0x86,0x1b,0x86,0x19,
      0x86,0x95,0x7d,0x27,0xe,0x13,0x82,0x11,
      0x82,0x1b,0x83,0x1b,0x82,0x1d,0x82,0x15,
      0x87,0x17,0x87,0x8a,0x81,0x87,0x8b,0x1c,
      0x6,0x1c,0x2,0xa1,0xff,0x22,0x49,0xa1,
      0xf6,0x26,0x3,0xcc,0x64,0x56,0xa1,0xf6,
      0x95,0xf7,0x25,0x8,0xa1,0xfb,0x25,0x7c,
      0xa1,0xfd,0x23,0x7f,0xa1,0xec,0x25,0x15,
      0xa1,0xf5,0x22,0x11,0xa0,0xec,0x97,0x4f,
      0xcd,0x42,0xaa,0xe,0x1c,0x20,0x23,0x2c,
      0x88,0x33,0x48,0x52,0x57,0x4c,0x26,0x18,
      0x20,0x6c,0xc6,0x1,0x2c,0xa5,0x4,0x26,
      0x78,0xad,0x7a,0x1a,0x83,0x16,0x83,0x20,
      0x70,0xad,0x7b,0x4f,0x20,0x31,0xad,0x6d,
      0x20,0x67,0xad,0x69,0x1a,0x83,0x16,0x83,
      0x18,0x83,0x20,0x5d,0xcd,0x63,0x9c,0xad,
      0x65,0xa6,0x1,0x20,0x1a,0xad,0x66,0x12,
      0x86,0x18,0x83,0x45,0x1,0x32,0xf6,0xa4,
      0xf0,0xf7,0x45,0x1,0x33,0xf6,0xa4,0xf0,
      0xf7,0x20,0x3e,0xad,0x49,0xa6,0x1,0xcd,
      0x63,0xa4,0x16,0x83,0x20,0x33,0xad,0x35,
      0x19,0x83,0x20,0xa,0x18,0x83,0xcd,0x82,
      0x49,0xc7,0x1,0x1a,0xad,0x37,0xcd,0x63,
      0x9c,0x20,0x1e,0xad,0x30,0xcd,0x63,0x9c,
      0x16,0x83,0xad,0x22,0x20,0x13,0x4f,0xc7,
      0x1,0xb0,0x20,0x4,0x1c,0x6,0x1c,0x2,
      0xcd,0x6d,0x2e,0x4d,0x27,0xf6,0xcc,0x40,
      0xf5,0x4f,0xa7,0x2,0x81,0x95,0xe6,0x3,
      0xc7,0x1,0xae,0xad,0x8,0x81,0x95,0xe6,
      0x3,0xc7,0x1,0xae,0x81,0xa6,0x1,0xcc,
      0x63,0xa4,0x87,0x8b,0x1c,0x6,0x1c,0x2,
      0xa1,0xfe,0x22,0x1b,0xa1,0xec,0x95,0xf7,
      0x25,0xf,0xa1,0xef,0x22,0xb,0xa0,0xec,
      0x97,0x4f,0xcd,0x42,0xaa,0x14,0x8,0x16,
      0xa,0x41,0xf1,0xe,0x41,0xfe,0x14,0xa6,
      0xfa,0x20,0x13,0xc6,0x1,0x2c,0xa5,0x2,
      0x27,0xf5,0xa6,0xfe,0x20,0x8,0x95,0xe6,
      0x1,0x20,0x3,0xc6,0x1,0xaf,0xa7,0x2,
      0x81,0x87,0x8b,0xc6,0x1,0xae,0xa1,0xfd,
      0x22,0x25,0xa1,0xfb,0x95,0xf7,0x25,0x4,
      0xa1,0xfd,0x23,0x7a,0xa1,0xec,0x25,0x10,
      0xa1,0xf0,0x22,0xc,0xa0,0xec,0x97,0x4f,
      0xcd,0x42,0xaa,0xe,0x12,0x9,0x69,0x70,
      0xa1,0xf3,0x26,0x3,0xcc,0x65,0xb7,0xcc,
      0x65,0xb5,0x95,0xe6,0x1,0x20,0x30,0x95,
      0xe6,0x1,0xa4,0x30,0xa1,0x10,0x26,0x9,
      0xe6,0x1,0xa4,0xf,0xc7,0x1,0x4d,0x20,
      0x64,0xa1,0x20,0x26,0xf,0xcd,0x65,0xc9,
      0xc6,0x1,0x2c,0xa5,0x2,0x26,0x19,0xc6,
      0x1,0x4d,0x20,0x37,0xe6,0x1,0xa4,0x30,
      0xa1,0x30,0x26,0x8,0xcd,0x65,0xc9,0xcd,
      0x6c,0xec,0x20,0x2a,0xe6,0x1,0xa5,0xf8,
      0x26,0x71,0xc6,0x1,0x32,0xa4,0xf,0x87,
      0x95,0xe6,0x2,0x62,0xa4,0xf0,0xfa,0xc7,
      0x1,0x32,0xcd,0x61,0x7a,0x17,0x83,0x95,
      0xe6,0x2,0xc7,0x1,0xad,0x8a,0x20,0x5c,
      0x95,0xe6,0x1,0xcd,0x69,0x6e,0x20,0x56,
      0x95,0xee,0x1,0xa3,0x5,0x22,0x3c,0x4f,
      0xcd,0x42,0xf2,0x6,0x36,0x5,0x8,0xf,
      0x16,0x1d,0x26,0x16,0x86,0x20,0x3b,0x1b,
      0x84,0x1d,0x84,0x1f,0x84,0x20,0x16,0x1a,
      0x84,0x1d,0x84,0x1f,0x84,0x20,0xe,0x1a,
      0x84,0x1c,0x84,0x1f,0x84,0x20,0x6,0x1b,
      0x84,0x1d,0x84,0x1e,0x84,0x11,0x85,0x20,
      0x19,0x1b,0x84,0x1d,0x84,0x1f,0x84,0x10,
      0x85,0x20,0xf,0x17,0x83,0x20,0x4,0xe6,
      0x1,0x2a,0x4,0xa6,0xfe,0x20,0x7,0xc7,
      0x1,0x1a,0x17,0x83,0xa6,0xfa,0xa7,0x2,
      0x81,0x95,0xe6,0x3,0x62,0xa4,0xf0,0x45,
      0x1,0x4d,0xfa,0xf7,0x17,0x83,0x81,0xa7,
      0xfd,0x1c,0x6,0x1c,0x2,0xcd,0x66,0x60,
      0x26,0xe,0xcd,0x6d,0x2e,0x4d,0x27,0x8,
      0xad,0x76,0x26,0x4,0xad,0x72,0x27,0x5,
      0x4f,0xae,0x3,0x20,0x68,0xa6,0x9,0x95,
      0xe7,0x1,0x95,0x74,0xad,0x62,0x27,0x5,
      0x95,0xf6,0xaa,0x80,0xf7,0xcd,0x67,0xd,
      0x95,0x6b,0x1,0xee,0xad,0x52,0x27,0x2,
      0xa6,0x1,0x95,0xe7,0x2,0xcd,0x67,0xd,
      0xad,0x46,0x27,0x28,0x1e,0x9,0x1e,0x8,
      0xa6,0x1,0xcd,0x69,0x55,0xcd,0x67,0xd,
      0x1e,0x9,0x1f,0x8,0xa6,0xe,0xcd,0x69,
      0x55,0x95,0xf6,0xcd,0x6d,0xc,0x95,0xe1,
      0x2,0x27,0x4,0xf6,0x5f,0x20,0x1e,0xf6,
      0xae,0x1,0x20,0x19,0x1c,0x6,0x1c,0x2,
      0xcd,0x67,0xd,0xad,0x13,0x27,0xf9,0x1e,
      0x9,0x1e,0x8,0xcd,0x67,0xd,0x1e,0x9,
      0x1f,0x8,0x4f,0xae,0x2,0xa7,0x3,0x81,
      0xcd,0x6d,0x36,0x4d,0x81,0x87,0xa7,0xfe,
      0x1c,0x6,0x1c,0x2,0xa6,0x96,0xcd,0x69,
      0x55,0x95,0xe6,0x2,0xcd,0x6d,0xc,0x95,
      0xe7,0x1,0xcd,0x6d,0x2e,0x4d,0x27,0x70,
      0xcd,0x6d,0x36,0x4d,0x27,0x6a,0x1e,0x9,
      0x1e,0x8,0xa6,0x3,0xcd,0x69,0x55,0xa6,
      0x9,0x95,0xf7,0x1c,0x6,0x1c,0x2,0xcd,
      0x6d,0x2e,0x4d,0x27,0x53,0xad,0x65,0x95,
      0x7a,0x27,0x1e,0x1c,0x9,0x1d,0x8,0xa6,
      0x6,0xcd,0x69,0x55,0x95,0xe6,0x2,0xa5,
      0x1,0x27,0x6,0x1e,0x9,0x1f,0x8,0x20,
      0x4,0x1e,0x9,0x1e,0x8,0x64,0x2,0x20,
      0xd2,0x1c,0x9,0x1d,0x8,0xa6,0x2,0xcd,
      0x69,0x55,0x95,0x6d,0x1,0x27,0x6,0x1e,
      0x9,0x1e,0x8,0x20,0x4,0x1e,0x9,0x1f,
      0x8,0xcd,0x6d,0x2e,0x4d,0x27,0x11,0xa6,
      0x3,0xad,0x15,0xcd,0x69,0x55,0x1e,0x9,
      0x1f,0x8,0xa6,0x4,0xad,0xa,0x20,0x5,
      0x1e,0x9,0x1f,0x8,0x4f,0xa7,0x3,0x81,
      0xcd,0x69,0x55,0xad,0x7,0x1c,0x9,0x1d,
      0x8,0xa6,0x1,0x81,0x1c,0x9,0x1c,0x8,
      0xa6,0x6,0xcc,0x69,0x55,0x1c,0x6,0x1c,
      0x2,0x1c,0x9,0x1c,0x8,0xcd,0x6d,0x2e,
      0x4d,0x26,0xfa,0x1c,0x6,0x1c,0x2,0,
      0x85,0x3,0xa6,0x6,0x65,0xa6,0x5,0xcd,
      0x69,0x55,0x1c,0x9,0x1d,0x8,0xcd,0x6d,
      0x2e,0x4d,0x27,0xfa,0x1c,0x6,0x1c,0x2,
      0xa6,0x7,0xcc,0x69,0x55,0x87,0xa7,0xfc,
      0xa6,0x19,0xcd,0x62,0x5b,0x1c,0x6,0x1c,
      0x2,0xcd,0x65,0xd7,0x9e,0xe7,0x2,0x9e,
      0xef,0x1,0xb6,0x86,0xa5,0x80,0x27,0x2,
      0x17,0x83,0x95,0xee,0x1,0x9e,0xe6,0x1,
      0x87,0x8a,0x65,0x2,0,0x25,0x3,0xcc,
      0x68,0x11,0x95,0xf6,0x87,0xee,0x1,0x8a,
      0x65,0x1,0,0x24,0x39,0xa6,0x19,0xcd,
      0x62,0x5b,0x95,0xe6,0x1,0xa0,0xd0,0xa1,
      0x4,0x22,0x2f,0xd,0x8a,0x2c,0xe6,0x1,
      0xa1,0xd4,0x27,0x18,0xa1,0xd0,0x26,0x9,
      0x45,0x1,0xb0,0xf6,0xaa,0x2,0xf7,0x20,
      0xd,0xa1,0xd1,0x26,0xd,0x45,0x1,0xb0,
      0xf6,0xaa,0x1,0xf7,0x10,0x83,0xa6,0xfa,
      0x20,0x1d,0xa1,0xd2,0x26,0x1c,0xa6,0xfe,
      0x20,0x15,0xe6,0x1,0xa1,0xec,0x25,0x5,
      0xcd,0x64,0x9a,0x20,0xa,0x6,0x83,0x4,
      0xa6,0xfe,0x20,0x3,0xcd,0x64,0xd9,0xc7,
      0x1,0x1b,0x1c,0x6,0x1c,0x2,0xa6,0xff,
      0xc7,0xff,0xff,0x9d,0xc6,0x1,0x1b,0xcd,
      0x66,0x65,0x4d,0x27,0x36,0xc6,0x1,0x1b,
      0x41,0xfe,0x15,0xc7,0x1,0xaf,0x95,0xe6,
      0x1,0xa1,0xec,0x25,0xb,0xcd,0x63,0xc5,
      0x4d,0x27,0x5,0xa6,0x1,0xcc,0x69,0x52,
      0x7,0x87,0x9,0x95,0xe6,0x1,0xc7,0x1,
      0xad,0xcd,0x98,0x26,0x3,0x84,0x6,0x6e,
      0x77,0x75,0xcd,0x94,0xa8,0x4f,0xc7,0x1,
      0x1b,0x20,0x20,0xcd,0x65,0xd7,0x9e,0xe7,
      0x4,0x9e,0xef,0x3,0x89,0x8a,0x97,0x65,
      0x2,0,0x24,0xaa,0x8b,0x95,0xe7,0x2,
      0x86,0xe7,0x1,0xc6,0x1,0x1b,0x27,0x3,
      0xcc,0x67,0x6a,0x7,0x83,0x3,0xcc,0x67,
      0x45,0x4f,0xcd,0x62,0x5b,0xd,0x88,0x3,
      0xcd,0x84,0xa1,0x3,0x86,0x5,0xa6,0xab,
      0xc7,0x1,0x1b,0x5,0x86,0x5,0xa6,0x83,
      0xc7,0x1,0x1b,0x7,0x86,0x23,0x1,0x85,
      0x4,0xa6,0x5,0x20,0x19,0xb6,0x84,0xa5,
      0x80,0x27,0x4,0xa6,0x4,0x20,0xf,0xd,
      0x84,0x4,0xa6,0x3,0x20,0x8,0xb,0x84,
      0x3,0xa6,0x2,0x65,0xa6,0x1,0xc7,0x1,
      0x1b,0x1c,0x6,0x1c,0x2,0x9,0x86,0x15,
      0xa6,0x2c,0x5f,0xcd,0x5a,0x14,0xa6,0xac,
      0x5f,0xcd,0x5a,0x14,0x19,0x86,0x19,0x83,
      0xa6,0x1,0xcd,0x63,0xa4,0xb,0x86,0x76,
      0xa,0x86,0x2,0x4f,0x65,0xa6,0xff,0xcd,
      0x5d,0x3f,0x20,0x47,0x1c,0x6,0x1c,0x2,
      0x1,0x82,0x9,0x2,0x82,0x6,0x4f,0xcd,
      0x89,0x81,0x20,0x7,0xb,0x82,0x7,0x4f,
      0xcd,0x8a,0x36,0xc7,0x1,0x1b,0xc6,0x1,
      0x1b,0xcd,0x66,0x65,0x4d,0x27,0x55,0x45,
      0x1,0x1b,0xf6,0xc7,0x1,0xaf,0x4f,0xf7,
      0x1,0x82,0x6,0x4c,0xcd,0x89,0x81,0x20,
      0x35,0xb,0x82,0x6,0x4c,0xcd,0x8a,0x36,
      0x20,0x2c,0x3,0x86,0xb,0x13,0x86,0x14,
      0x86,0xa6,0x83,0xc7,0x1,0x1b,0x20,0x1e,
      0x5,0x86,0x4,0x15,0x86,0x19,0x83,0x7,
      0x86,0x2,0x17,0x86,0x9,0x86,0x8,0x19,
      0x86,0x19,0x83,0x4c,0xcd,0x63,0xa4,0xb,
      0x86,0x4,0x1b,0x86,0x19,0x83,0x1,0x82,
      0x3,0x3,0x82,0x90,0xa,0x82,0x8d,0xc6,
      0x1,0x1b,0x26,0x88,0xc6,0x1,0x1b,0x26,
      0x26,0x95,0x6d,0x4,0x27,0x21,0xcd,0x5d,
      0x1a,0x4d,0x27,0x25,0x4f,0xcd,0x5c,0x8,
      0x6,0x82,0xa,0x4f,0xcd,0x5c,0x8,0xcd,
      0x66,0x65,0x4d,0x27,0x14,0xa6,0x1,0xcd,
      0x5c,0x8,0xc7,0x1,0xaf,0x20,0xa,0x1c,
      0x6,0x1c,0x2,0x4d,0x27,0x3,0xcc,0x67,
      0x40,0x4f,0xa7,0x5,0x81,0x87,0x20,0x10,
      0xa6,0xff,0xc7,0xff,0xff,0x9d,0x9d,0x9d,
      0x9d,0x9d,0x9d,0x9d,0x9d,0x9d,0x9d,0x7a,
      0x95,0x7d,0x26,0xec,0x8a,0x81,0x87,0x8b,
      0x1c,0x6,0x1c,0x2,0x1,0x82,0xc,0xcd,
      0x88,0x38,0x4d,0x27,0x26,0x41,0x1,0x1a,
      0xcc,0x6b,0x30,0x3,0x82,0x1f,0xcd,0x86,
      0x57,0x4d,0x27,0x17,0x41,0x8,0xd,0x19,
      0x83,0x1a,0x88,0x1c,0x88,0xa6,0x1,0xcd,
      0x63,0xa4,0x20,0x76,0xa6,0x1,0xcd,0x63,
      0xa4,0x1c,0x88,0x20,0x6d,0xd,0x82,0x6,
      0xcd,0x8a,0xe8,0xcc,0x6b,0xc2,0xc6,0x1,
      0x66,0x27,0xf,0x95,0xe6,0x1,0xcd,0x96,
      0x1f,0x4d,0x27,0x56,0x4f,0xc7,0x1,0x66,
      0x20,0x67,0xc6,0xb,0x1c,0x27,0xf,0x95,
      0xe6,0x1,0xcd,0xc3,0x47,0x4d,0x27,0x42,
      0x4f,0xc7,0xb,0x1c,0x20,0x53,0xc6,0x1,
      0x14,0xa1,0x90,0x26,0xa,0x95,0xe6,0x1,
      0xa4,0x7f,0xc7,0x1,0x14,0x20,0x42,0xd,
      0x86,0x2b,0x19,0x83,0x1b,0x83,0x1d,0x86,
      0x17,0x83,0x95,0xe6,0x1,0xa1,0x7,0x23,
      0x3,0xcc,0x6b,0xae,0xc6,0x1,0x2d,0xa4,
      0xf8,0x87,0x95,0xe6,0x2,0xfb,0x45,0x1,
      0x2c,0xe7,0x1,0x4f,0xf9,0xf7,0xcd,0x6d,
      0x7d,0x8a,0xcc,0x6b,0x50,0x5,0x87,0x18,
      0x95,0xe6,0x1,0xa1,0xcb,0x26,0x2,0x16,
      0x87,0xa1,0x67,0x26,0x2,0x12,0x84,0x15,
      0x87,0x1b,0x83,0x19,0x83,0xcc,0x6c,0xcf,
      0x95,0xe6,0x1,0xa1,0x88,0x23,0x3,0xcc,
      0x6b,0x27,0xa1,0x6,0x26,0x3,0xcc,0x6b,
      0x95,0xa1,0x6,0xf7,0x25,0x1c,0xa1,0xa,
      0x24,0x3,0xcc,0x6b,0xa2,0x26,0x3,0xcc,
      0x6b,0xb7,0xa1,0xb,0x25,0xc,0xa1,0xe,
      0x24,0x3,0xcc,0x6b,0xb0,0x26,0x3,0xcc,
      0x6c,0x4c,0xa1,0x31,0x25,0x1d,0xa1,0x39,
      0x22,0x65,0xa0,0x31,0x97,0x4f,0xcd,0x42,
      0x94,0x6c,0x19,0x6c,0x25,0x6c,0x2e,0x6b,
      0x2c,0x6b,0x2c,0x6c,0x3a,0x6c,0x42,0x6c,
      0x48,0x6c,0x48,0xa1,0,0x25,0x4a,0xa1,
      0x5,0x22,0xb,0x97,0x4f,0xcd,0x42,0xaa,
      0xa2,0xa5,0xae,0xbf,0xc9,0xd0,0xa1,0xf,
      0x25,0x7d,0xa1,0x23,0x22,0x79,0xa0,0xf,
      0x97,0x4f,0xcd,0x42,0x94,0x6c,0x9f,0x6b,
      0xc5,0x6c,0x44,0x6b,0xc9,0x6b,0x2c,0x6b,
      0x2c,0x6b,0x2c,0x6b,0x2c,0x6b,0x2c,0x6b,
      0xcd,0x6b,0xd4,0x6b,0xdb,0x6b,0xdf,0x6b,
      0xe6,0x6c,0x44,0x6b,0xed,0x6b,0xf4,0x6b,
      0xfb,0x6b,0xff,0x6c,0x6,0x6c,0x44,0xa1,
      0x40,0x25,0x44,0xa1,0x49,0x22,0x1b,0xa0,
      0x40,0x97,0x4f,0xcd,0x42,0x94,0x6c,0x4c,
      0x6c,0x9f,0x6c,0x53,0x6c,0x5b,0x6c,0x65,
      0x6c,0x71,0x6b,0x2c,0x6c,0x7c,0x6b,0x2c,
      0x6c,0x85,0xa1,0x50,0x25,0x21,0xa1,0x5a,
      0x22,0x1d,0xa0,0x50,0x97,0x4f,0xcd,0x42,
      0x94,0x6c,0x8c,0x6b,0x2c,0x6c,0x90,0x6c,
      0x94,0x6c,0x97,0x6c,0x9d,0x6c,0xaa,0x6c,
      0xae,0x6b,0x2c,0x6c,0xb2,0x6c,0xc2,0xa1,
      0x29,0x26,0x3,0xcc,0x6c,0x10,0xa1,0x2b,
      0x26,0x3,0xcc,0x6c,0x14,0xa1,0x88,0x26,
      0x3,0xcc,0x6c,0xcb,0x1b,0x83,0x17,0x83,
      0xcc,0x6b,0xbe,0x18,0x86,0x20,0x69,0x1a,
      0x82,0xae,0x4,0x8c,0x89,0x8b,0x4f,0x20,
      0x5a,0x1a,0x83,0x1c,0x82,0xae,0x4,0x8c,
      0x89,0x8b,0x4f,0xcd,0x84,0x4a,0xa7,0x2,
      0xcc,0x6c,0x7a,0,0x84,0x4,0x14,0x82,
      0x20,0x71,0x11,0x84,0x20,0x6d,0x1a,0x82,
      0x4f,0x87,0x87,0x4f,0x20,0x35,0x10,0x84,
      0xa6,0x2f,0xc7,0x1,0x2d,0xa6,0x8,0xc7,
      0x1,0x2c,0x1b,0x83,0xc6,0x1,0x32,0xa5,
      0x40,0x27,0x62,0xa6,0x3a,0x5f,0xcd,0x59,
      0xc0,0xa6,0xba,0x5f,0xcd,0x59,0xc0,0xa6,
      0x2a,0x5f,0xcd,0x59,0xc0,0xa6,0xaa,0x5f,
      0xcd,0x59,0xc0,0x20,0x48,0x1a,0x82,0x4f,
      0x87,0x87,0x4c,0xcd,0x84,0x4a,0xa7,0x2,
      0x20,0x70,0x1b,0x83,0x19,0x83,0xcd,0x81,
      0x75,0x4d,0x27,0x14,0x20,0x64,0x20,0x10,
      0xa1,0xb,0x26,0x5,0xcd,0x84,0x3a,0x20,
      0x3,0xcd,0x84,0x3e,0x1b,0x83,0x19,0x83,
      0xa6,0xfe,0xcc,0x6c,0xd3,0x14,0x84,0x20,
      0x7f,0x14,0x82,0x20,0x77,0xcd,0x6c,0xdf,
      0xaa,0x8,0x20,0x37,0xcd,0x6c,0xdf,0xa4,
      0xf7,0x20,0x30,0x1e,0x82,0x20,0x65,0xcd,
      0x6c,0xdf,0xaa,0x10,0x20,0x25,0xcd,0x6c,
      0xdf,0xa4,0xef,0x20,0x1e,0xcd,0x6c,0xdf,
      0xaa,0x20,0x20,0x17,0xcd,0x6c,0xdf,0xa4,
      0xdf,0x20,0x10,0x10,0x83,0x20,0x45,0xcd,
      0x6c,0xdf,0xaa,0x80,0x20,0x5,0xcd,0x6c,
      0xdf,0xa4,0x7f,0xc7,0x1,0x2d,0x20,0x5f,
      0x1a,0x86,0x20,0x5b,0xcd,0x95,0xbd,0x20,
      0x2b,0xcd,0x76,0xb,0xcd,0x6c,0xd6,0xa4,
      0xe7,0xb7,0x2,0x20,0x1f,0x19,0x83,0xc6,
      0x1,0x2c,0xaa,0x80,0x20,0x7,0x19,0x83,
      0xc6,0x1,0x2c,0xa4,0x7f,0xc7,0x1,0x2c,
      0x20,0x35,0x1a,0x83,0x18,0x83,0x1c,0x86,
      0x20,0x36,0x1f,0x82,0x19,0x83,0x20,0x27,
      0x18,0x83,0x20,0x23,0x1a,0x83,0x18,0x83,
      0x4f,0x20,0x52,0x19,0x83,0xad,0x7f,0xa4,
      0xe7,0x20,0x12,0x19,0x83,0xad,0x77,0xa4,
      0xef,0xaa,0x8,0x20,0x8,0x19,0x83,0xad,
      0x6d,0xa4,0xf7,0xaa,0x10,0xb7,0x2,0x20,
      0x5c,0x1a,0x83,0x14,0x87,0xcd,0x63,0x9c,
      0x16,0x83,0x20,0x55,0xad,0x67,0xa6,0x1,
      0xc7,0x1,0x66,0x20,0x4c,0xc6,0xb,0x1c,
      0x27,0x2f,0x20,0x45,0x1e,0x83,0x20,0x3d,
      0xa6,0x3,0x20,0x4,0xa6,0x4,0x21,0x4f,
      0xcd,0x52,0x69,0x20,0x30,0x1a,0x89,0x1a,
      0x83,0x18,0x83,0xa6,0x1,0xcd,0x84,0x68,
      0x20,0x27,0x1c,0x89,0x20,0x1f,0x1e,0x89,
      0x20,0x1b,0xc6,0xb,0x1c,0x26,0x1a,0x1e,
      0x83,0xad,0x2a,0xa6,0x1,0xc7,0xb,0x1c,
      0x20,0xf,0xad,0x21,0xa6,0x90,0xc7,0x1,
      0x14,0x20,0x6,0x1e,0x86,0x1b,0x83,0x17,
      0x83,0xa6,0xfa,0xa7,0x2,0x81,0xb6,0x6,
      0xaa,0x1f,0xb7,0x6,0xb6,0x2,0x81,0x19,
      0x83,0xc6,0x1,0x2d,0x81,0x1a,0x83,0x19,
      0x83,0x16,0x83,0x81,0x87,0x95,0x7d,0x26,
      0x8,0x1b,0x83,0x17,0x83,0xa6,0xfa,0x8a,
      0x81,0xa1,0x7,0x26,0x5,0xcd,0x95,0xbd,
      0x20,0xf3,0xcd,0xc3,0xfd,0x4d,0x26,0xed,
      0xa6,0xfe,0x8a,0x81,0x87,0xa7,0xfe,0x95,
      0x7f,0x6f,0x1,0xe6,0x2,0x2a,0x1,0x7c,
      0x68,0x2,0x6c,0x1,0xe6,0x1,0xa1,0x8,
      0x25,0xf1,0xf6,0xa5,0x1,0x27,0x3,0xa6,
      0x1,0x21,0x4f,0xa7,0x3,0x81,0x3,0x5e,
      0x3,0xa6,0x1,0x81,0x4f,0x81,0x1,0x5e,
      0x3,0xa6,0x1,0x81,0x4f,0x81,0x87,0xa7,
      0xfe,0xa6,0xff,0xc7,0xff,0xff,0x9d,0x1,
      0x85,0xa,0x95,0xe6,0x2,0xae,0x4,0xcd,
      0x48,0x80,0x20,0x26,0x95,0x6f,0x1,0x7f,
      0x1c,0x6,0x1c,0x2,0x95,0x6c,0x1,0x26,
      0x1,0x7c,0xf6,0x87,0xee,0x1,0x8a,0x65,
      0,0x64,0x91,0xec,0x95,0xe6,0x2,0x6,
      0x8b,0x3,0xae,0x2,0x65,0xae,0x3,0xcd,
      0x59,0xc0,0xa7,0x3,0x81,0xcd,0x92,0x3e,
      0x15,0x8a,0x81,0xa7,0xf7,0x95,0x6f,0x5,
      0x1f,0x8a,0x11,0x8b,0x13,0x8b,0x15,0x8b,
      0x17,0x8b,0x19,0x8b,0xc6,0x1,0x2c,0xa5,
      0x8,0x27,0x3,0x1b,0x8b,0x65,0x1a,0x8b,
      0xa5,0x10,0x26,0x3,0x1d,0x8b,0x65,0x1c,
      0x8b,0x1f,0x8b,0x1f,0x84,0x11,0x85,0x1b,
      0x8a,0x11,0x8d,0x11,0x8c,0x13,0x8c,0x17,
      0x8c,0x1b,0x8c,0x1d,0x8c,0x45,0x1,0xc9,
      0x6c,0x1,0x26,0x1,0x7c,0x4,0x8a,0x5,
      0xcd,0x92,0x3e,0x20,0x12,0x9,0x83,0x5,
      0x15,0x8a,0xcd,0x75,0x78,0xc6,0x1,0x2d,
      0xa5,0x7,0x26,0x6,0xcd,0x76,0x13,0xcc,
      0x70,0x37,0x11,0x85,0x1c,0x6,0x1c,0x2,
      0xd,0x83,0xe,0xa6,0x80,0xcd,0x53,0x52,
      0x4d,0x27,0x6,0x10,0x85,0x1a,0x8b,0x1c,
      0x8b,0x1c,0x6,0x1c,0x2,0x1,0x84,0x2,
      0x11,0x85,0xcd,0x82,0x6c,0x89,0x8a,0x97,
      0x65,0,0,0x27,0x5,0xa6,0x1,0x95,
      0xe7,0x5,0xcd,0x7d,0x7b,0x9,0x83,0x3,
      0xcc,0x70,0x37,0x11,0x8a,0x4f,0x45,0x8,
      0x5a,0xf7,0xc7,0x9,0x5a,0xc7,0xa,0x5a,
      0xf6,0x26,0x24,0x45,0x8,0,0x89,0x8b,
      0x45,0x8,0x5a,0x89,0x8b,0xa6,0x45,0x87,
      0xa6,0x1f,0x87,0xa6,0x40,0x87,0x4a,0x87,
      0xa6,0x7,0x87,0xa6,0x7f,0x87,0xa6,0x20,
      0xae,0x8,0xcd,0x76,0xb5,0xa7,0xa,0xc6,
      0x9,0x5a,0x26,0x22,0x45,0x9,0,0x89,
      0x8b,0x45,0x9,0x5a,0x89,0x8b,0xa6,0xb,
      0x87,0xa6,0x1f,0x87,0xae,0x10,0x89,0x44,
      0x87,0xa6,0x5,0x87,0xa6,0x1f,0x87,0xa6,
      0x30,0xcd,0x76,0xb5,0xa7,0xa,0xc6,0xa,
      0x5a,0x26,0x22,0x45,0xa,0,0x89,0x8b,
      0x45,0xa,0x5a,0x89,0x8b,0xa6,0xb,0x87,
      0xa6,0x1f,0x87,0xae,0x10,0x89,0x5a,0x89,
      0xae,0x5,0x89,0x87,0xa6,0x30,0xae,0x18,
      0xcd,0x76,0xb5,0xa7,0xa,0xc6,0x9,0x5a,
      0x26,0x27,0x1,0x8c,0x24,0x45,0x9,0,
      0x89,0x8b,0x45,0x9,0x5a,0x89,0x8b,0xa6,
      0xb,0x87,0xa6,0x1c,0x87,0xa6,0x10,0x87,
      0x4a,0x87,0xa6,0x5,0x87,0xa6,0x1f,0x87,
      0xa6,0x30,0xae,0x38,0xcd,0x76,0xb5,0xa7,
      0xa,0xc6,0x9,0x5a,0x26,0x25,0x1,0x8c,
      0x22,0x45,0x9,0,0x89,0x8b,0x45,0x9,
      0x5a,0x89,0x8b,0xa6,0xd,0x87,0xa6,0x1f,
      0x87,0xae,0x10,0x89,0x5a,0x89,0xae,0x5,
      0x89,0x87,0xa6,0x30,0xae,0x40,0xcd,0x76,
      0xb5,0xa7,0xa,0xc6,0xa,0x5a,0x26,0x27,
      0x1,0x8c,0x24,0x45,0xa,0,0x89,0x8b,
      0x45,0xa,0x5a,0x89,0x8b,0xa6,0xb,0x87,
      0xa6,0x1c,0x87,0xa6,0x10,0x87,0x4a,0x87,
      0xa6,0x5,0x87,0xa6,0x1f,0x87,0xa6,0x30,
      0xae,0x38,0xcd,0x76,0xb5,0xa7,0xa,0xc6,
      0xa,0x5a,0x26,0x25,0x1,0x8c,0x22,0x45,
      0xa,0,0x89,0x8b,0x45,0xa,0x5a,0x89,
      0x8b,0xa6,0xd,0x87,0xa6,0x1f,0x87,0xae,
      0x10,0x89,0x5a,0x89,0xae,0x5,0x89,0x87,
      0xa6,0x30,0xae,0x40,0xcd,0x76,0xb5,0xa7,
      0xa,0xb6,0x8b,0xa5,0x80,0x27,0x48,0xc6,
      0x8,0x5a,0x26,0x1f,0x45,0x8,0,0x89,
      0x8b,0x45,0x8,0x5a,0x89,0x8b,0xa6,0xff,
      0x87,0x87,0xae,0x80,0x89,0x5a,0x89,0xae,
      0x8,0x89,0x87,0x4f,0xae,0x30,0xcd,0x76,
      0xb5,0xa7,0xa,0xc6,0xa,0x5a,0x26,0x1f,
      0x45,0xa,0,0x89,0x8b,0x45,0xa,0x5a,
      0x89,0x8b,0xa6,0xff,0x87,0x87,0xae,0x80,
      0x89,0x5a,0x89,0xae,0x8,0x89,0x87,0x4f,
      0xae,0x30,0xcd,0x76,0xb5,0xa7,0xa,0xc6,
      0xa,0x5a,0x26,0x2a,0xb6,0x8a,0xa5,0x80,
      0x27,0x24,0x45,0xa,0,0x89,0x8b,0x45,
      0xa,0x5a,0x89,0x8b,0xa6,0x45,0x87,0xa6,
      0x1f,0x87,0xa6,0x40,0x87,0x4a,0x87,0xa6,
      0x7,0x87,0xa6,0x7f,0x87,0xa6,0x20,0xae,
      0x28,0xcd,0x76,0xb5,0xa7,0xa,0x1,0x8b,
      0x13,0xc6,0x8,0x5a,0x26,0xe,0x45,0x8,
      0,0x89,0x8b,0xa6,0x5a,0xae,0x8,0xcd,
      0x79,0x47,0xa7,0x2,0x1,0x8b,0x13,0xc6,
      0xa,0x5a,0x26,0xe,0x45,0xa,0,0x89,
      0x8b,0xa6,0x5a,0xae,0xa,0xcd,0x79,0x47,
      0xa7,0x2,0xc6,0x8,0x5a,0x26,0x1a,0xc6,
      0x9,0x5a,0x26,0x15,0xc6,0xa,0x5a,0x26,
      0x10,0xb6,0x8a,0xa8,0x1,0xb7,0x8a,0x1c,
      0x6,0x1c,0x2,0x1,0x8a,0x3,0xcc,0x6e,
      0x1d,0xa6,0xff,0xc7,0xff,0xff,0x9d,0xcd,
      0x75,0xf6,0xa4,0xef,0xaa,0x8,0xb7,0x2,
      0xc6,0x8,0x5a,0x26,0x16,0xc6,0x9,0x5a,
      0x26,0x11,0xc6,0xa,0x5a,0x26,0xc,0xcd,
      0x76,0x13,0xcd,0x75,0x78,0x11,0x85,0x4f,
      0xcc,0x75,0x75,0xcd,0x7e,0x53,0xc,0x8a,
      0x3,0xcc,0x71,0xd2,0xc6,0x1,0xb0,0xa5,
      0x2,0x26,0x3,0xcc,0x70,0xf3,0x95,0x6f,
      0x6,0xa6,0x65,0xcd,0x75,0x88,0xa6,0x60,
      0xcd,0x75,0x88,0xa6,0x41,0xcd,0x75,0x88,
      0xc6,0x8,0x5a,0x27,0x1f,0xa6,0x48,0xcd,
      0x75,0x88,0xc6,0x8,0x5a,0x4c,0xcd,0x7c,
      0x73,0xc6,0x8,0x5a,0x4a,0x87,0x45,0x8,
      0x5c,0x89,0x8b,0xa6,0x1,0xcd,0x7c,0xca,
      0xa7,0x3,0x20,0x6,0xc6,0x8,0x5b,0xcd,
      0x7c,0xac,0xa6,0x42,0xcd,0x75,0x88,0xc6,
      0x9,0x5a,0x27,0x1f,0xa6,0x48,0xcd,0x75,
      0x88,0xc6,0x9,0x5a,0x4c,0xcd,0x7c,0x73,
      0xc6,0x9,0x5a,0x4a,0x87,0x45,0x9,0x5c,
      0x89,0x8b,0xa6,0x2,0xcd,0x7c,0xca,0xa7,
      0x3,0x20,0x6,0xc6,0x9,0x5b,0xcd,0x7c,
      0xac,0xa6,0x43,0xcd,0x75,0x88,0xc6,0xa,
      0x5a,0x27,0x1f,0xa6,0x48,0xcd,0x75,0x88,
      0xc6,0xa,0x5a,0x4c,0xcd,0x7c,0x73,0xc6,
      0xa,0x5a,0x4a,0x87,0x45,0xa,0x5c,0x89,
      0x8b,0xa6,0x2,0xcd,0x7c,0xca,0xa7,0x3,
      0x20,0x6,0xc6,0xa,0x5b,0xcd,0x7c,0xac,
      0xa6,0x44,0xcd,0x75,0x88,0xcd,0x75,0x78,
      0xcc,0x71,0xcc,0xc6,0x8,0x5a,0x26,0xd,
      0xc6,0x9,0x5a,0x26,0x8,0xc6,0xa,0x5a,
      0x26,0x3,0xcc,0x71,0xcc,0x95,0x6f,0x7,
      0xc6,0x1,0xb0,0xa5,0x1,0x27,0x3,0xcc,
      0x72,0x5,0xcd,0x61,0x64,0xc6,0x1,0x32,
      0xa5,0x40,0x27,0x14,0xa6,0x3a,0xcd,0x75,
      0x88,0xa6,0xba,0xcd,0x75,0x88,0xa6,0x2a,
      0xcd,0x75,0x88,0xa6,0xaa,0xcd,0x75,0x88,
      0xcd,0x75,0xe5,0xa6,0x1d,0xcd,0x75,0x88,
      0xa6,0x9d,0xcd,0x75,0x88,0xa6,0x9d,0xcd,
      0x75,0x88,0xa6,0x63,0xcd,0x75,0xcf,0xa6,
      0x31,0xcd,0x75,0xcf,0xc6,0x8,0x5a,0x27,
      0x18,0xa6,0x30,0xcd,0x75,0xcf,0xc6,0x8,
      0x5a,0x4a,0x87,0x45,0x8,0x5c,0x89,0x8b,
      0xa6,0x3,0xcd,0x7c,0xca,0xa7,0x3,0x20,
      0x6,0xc6,0x8,0x5b,0xcd,0x75,0xd4,0xa6,
      0x32,0xcd,0x75,0xcf,0xc6,0x9,0x5a,0x27,
      0x18,0xa6,0x30,0xcd,0x75,0xcf,0xc6,0x9,
      0x5a,0x4a,0x87,0x45,0x9,0x5c,0x89,0x8b,
      0xa6,0x4,0xcd,0x7c,0xca,0xa7,0x3,0x20,
      0x6,0xc6,0x9,0x5b,0xcd,0x75,0xd4,0xa6,
      0x33,0xcd,0x75,0xcf,0xc6,0xa,0x5a,0x27,
      0x18,0xa6,0x30,0xcd,0x75,0xcf,0xc6,0xa,
      0x5a,0x4a,0x87,0x45,0xa,0x5c,0x89,0x8b,
      0xa6,0x4,0xcd,0x7c,0xca,0xa7,0x3,0x20,
      0x6,0xc6,0xa,0x5b,0xcd,0x75,0xd4,0xa6,
      0x1c,0xcd,0x75,0x88,0xa6,0x9c,0xcd,0x75,
      0x88,0xcd,0x61,0x71,0xcd,0x76,0x13,0xcc,
      0x73,0x85,0xc6,0x8,0x5a,0x26,0xd,0xc6,
      0x9,0x5a,0x26,0x8,0xc6,0xa,0x5a,0x26,
      0x3,0xcc,0x73,0x88,0x95,0x6d,0x5,0x27,
      0x3,0x3,0x8b,0x12,0xc6,0x8,0x5a,0x26,
      0xd,0xc6,0x9,0x5a,0x26,0x8,0xc6,0xa,
      0x5a,0x26,0x3,0xcc,0x73,0x88,0x6f,0x8,
      0xc6,0x1,0xb0,0xa5,0x1,0x27,0x3,0xcc,
      0x75,0x73,0,0x85,0x3,0xcc,0x73,0x8b,
      0xae,0x1,0x9e,0xef,0x4,0x11,0x85,0xa6,
      0x80,0xcd,0x54,0x7b,0x10,0x85,0xa6,0x81,
      0xcd,0x6d,0x3e,0xc6,0x8,0x5a,0x95,0xe7,
      0x4,0xa6,0x5c,0xe7,0x2,0xa6,0x8,0xe7,
      0x1,0x20,0x1c,0xcd,0x75,0xee,0xf6,0xa1,
      0x5f,0x23,0x3,0x9e,0x6f,0x4,0xf6,0xa1,
      0x20,0x24,0x3,0x95,0x6f,0x3,0x95,0x6a,
      0x4,0x6c,0x2,0x26,0x2,0x6c,0x1,0x6d,
      0x4,0x26,0xe0,0x1c,0x6,0x1c,0x2,0xc6,
      0x8,0x5a,0x27,0x9,0xe6,0x3,0x4b,0x5,
      0xcd,0x75,0x9f,0x20,0x2d,0xcd,0x75,0xdb,
      0x26,0x28,0xc6,0x8,0x5a,0x26,0x23,0,
      0x84,0x20,0xa6,0x1,0x87,0xce,0x8,0x5b,
      0x89,0xb,0x8b,0x3,0x5,0x8b,0x1,0x4f,
      0x95,0xe7,0x2,0xd,0x8b,0x3,0x5,0x8b,
      0x2,0x5f,0x65,0xae,0x1,0xcd,0x7d,0x7a,
      0xa7,0x2,0xcd,0x75,0x8c,0x20,0x1c,0xcd,
      0x75,0xee,0xf6,0xa1,0x5f,0x23,0x3,0x9e,
      0x6f,0x4,0xf6,0xa1,0x20,0x24,0x3,0x95,
      0x6f,0x3,0x95,0x6a,0x4,0x6c,0x2,0x26,
      0x2,0x6c,0x1,0x95,0x6d,0x4,0x26,0xdf,
      0xa6,0x91,0xcd,0x6d,0x3e,0xa6,0x82,0xcd,
      0x6d,0x3e,0xc6,0x9,0x5a,0x27,0xa,0x95,
      0xe6,0x3,0x4b,0x5,0xcd,0x75,0xaf,0x20,
      0x30,0xcd,0x75,0xdb,0x26,0x2b,0xc6,0x9,
      0x5a,0x26,0x26,0,0x84,0x23,0xa6,0x2,
      0x87,0xc6,0x9,0x5b,0x87,0xb,0x8b,0x3,
      0x5,0x8b,0x2,0x4f,0x65,0xa6,0x1,0x95,
      0xe7,0x2,0xd,0x8b,0x3,0x5,0x8b,0x2,
      0x5f,0x65,0xae,0x1,0xcd,0x7d,0x7a,0xa7,
      0x2,0xcd,0x75,0x8c,0x20,0x1c,0xcd,0x75,
      0xee,0xf6,0xa1,0x5f,0x23,0x3,0x9e,0x6f,
      0x4,0xf6,0xa1,0x20,0x24,0x3,0x95,0x6f,
      0x3,0x95,0x6a,0x4,0x6c,0x2,0x26,0x2,
      0x6c,0x1,0x95,0x6d,0x4,0x26,0xdf,0xa6,
      0x92,0xcd,0x6d,0x3e,0xa6,0x83,0xcd,0x6d,
      0x3e,0xc6,0xa,0x5a,0x27,0xa,0x95,0xe6,
      0x3,0x4b,0x5,0xcd,0x75,0xbf,0x20,0x30,
      0xcd,0x75,0xdb,0x26,0x2b,0xc6,0xa,0x5a,
      0x26,0x26,0,0x84,0x23,0xa6,0x3,0x87,
      0xc6,0xa,0x5b,0x87,0xb,0x8b,0x3,0x5,
      0x8b,0x2,0x4f,0x65,0xa6,0x1,0x95,0xe7,
      0x2,0xd,0x8b,0x3,0x5,0x8b,0x2,0x5f,
      0x65,0xae,0x1,0xcd,0x7d,0x7a,0xa7,0x2,
      0xcd,0x7f,0x80,0xa6,0x93,0xcd,0x6d,0x3e,
      0x4f,0xcd,0x6d,0x3e,0x11,0x85,0xa6,0x85,
      0xae,0x1,0xcd,0x54,0x7b,0xcd,0x75,0x78,
      0xcc,0x75,0x61,0,0x8d,0x21,0xcd,0x7c,
      0x5d,0xc6,0x1,0x32,0xa5,0x40,0x27,0x14,
      0xa6,0x3a,0xcd,0x75,0x88,0xa6,0xba,0xcd,
      0x75,0x88,0xa6,0x2a,0xcd,0x75,0x88,0xa6,
      0xaa,0xcd,0x75,0x88,0xcd,0x75,0xe5,0x9,
      0x8b,0x5,0xc6,0x8,0x5a,0x27,0x12,0xc6,
      0x8,0x5b,0x26,0x8,0xc6,0x8,0x5a,0x26,
      0x3,0x2,0x8b,0x78,0xc6,0x1,0x2d,0xa5,
      0x1,0x27,0x71,0x1f,0x85,0,0x84,0xa,
      0xa6,0x80,0xcd,0x75,0xff,0xa6,0x80,0xcd,
      0x55,0x6,0xcd,0x75,0xdb,0x26,0x23,0xc6,
      0x8,0x5a,0x27,0x1e,0x1,0x8d,0x5,0xcd,
      0x7c,0x5d,0x20,0x3,0xcd,0x75,0x7f,0xcd,
      0x75,0x9f,0x1,0x8d,0x5,0xcd,0x7c,0x6a,
      0x20,0x3,0xcd,0x75,0x7f,0xcd,0x75,0xe0,
      0x20,0x2d,0xcd,0x75,0xdb,0x26,0x28,0xc6,
      0x8,0x5a,0x26,0x23,0,0x84,0x20,0xa6,
      0x1,0x87,0xce,0x8,0x5b,0x89,0xb,0x8b,
      0x3,0x5,0x8b,0x1,0x4f,0x95,0xe7,0x2,
      0xd,0x8b,0x3,0x5,0x8b,0x2,0x5f,0x65,
      0xae,0x1,0xcd,0x7d,0x7a,0xa7,0x2,0,
      0x84,0xa,0xa6,0x81,0xcd,0x75,0xff,0xa6,
      0x81,0xcd,0x55,0x6,0x9,0x8b,0x5,0xc6,
      0x9,0x5a,0x27,0x12,0xc6,0x9,0x5b,0x26,
      0x8,0xc6,0x9,0x5a,0x26,0x3,0x2,0x8b,
      0x7b,0xc6,0x1,0x2d,0xa5,0x2,0x27,0x74,
      0x1f,0x85,0,0x84,0xa,0xa6,0x82,0xcd,
      0x75,0xff,0xa6,0x82,0xcd,0x55,0x6,0xcd,
      0x75,0xdb,0x26,0x23,0xc6,0x9,0x5a,0x27,
      0x1e,0x1,0x8d,0x5,0xcd,0x7c,0x5d,0x20,
      0x3,0xcd,0x75,0x7f,0xcd,0x75,0xaf,0x1,
      0x8d,0x5,0xcd,0x7c,0x6a,0x20,0x3,0xcd,
      0x75,0x7f,0xcd,0x75,0xe0,0x20,0x30,0xcd,
      0x75,0xdb,0x26,0x2b,0xc6,0x9,0x5a,0x26,
      0x26,0,0x84,0x23,0xa6,0x2,0x87,0xc6,
      0x9,0x5b,0x87,0xb,0x8b,0x3,0x5,0x8b,
      0x2,0x4f,0x65,0xa6,0x1,0x95,0xe7,0x2,
      0xd,0x8b,0x3,0x5,0x8b,0x2,0x5f,0x65,
      0xae,0x1,0xcd,0x7d,0x7a,0xa7,0x2,0,
      0x84,0xa,0xa6,0x83,0xcd,0x75,0xff,0xa6,
      0x83,0xcd,0x55,0x6,0x9,0x8b,0x5,0xc6,
      0xa,0x5a,0x27,0x12,0xc6,0xa,0x5b,0x26,
      0x8,0xc6,0xa,0x5a,0x26,0x3,0x2,0x8b,
      0x7a,0xc6,0x1,0x2d,0xa5,0x4,0x27,0x73,
      0x1f,0x85,0,0x84,0xa,0xa6,0x84,0xcd,
      0x75,0xff,0xa6,0x84,0xcd,0x55,0x6,0xcd,
      0x75,0xdb,0x26,0x1f,0xc6,0xa,0x5a,0x27,
      0x1a,0x1,0x8d,0x4,0xcd,0x7c,0x5d,0x65,
      0xad,0x75,0xcd,0x75,0xbf,0x1,0x8d,0x4,
      0xcd,0x7c,0x6a,0x65,0xad,0x69,0xcd,0x75,
      0xe0,0x20,0x30,0xcd,0x75,0xdb,0x26,0x2b,
      0xc6,0xa,0x5a,0x26,0x26,0,0x84,0x23,
      0xa6,0x3,0x87,0xc6,0xa,0x5b,0x87,0xb,
      0x8b,0x3,0x5,0x8b,0x2,0x4f,0x65,0xa6,
      0x1,0x95,0xe7,0x2,0xd,0x8b,0x3,0x5,
      0x8b,0x2,0x5f,0x65,0xae,0x1,0xcd,0x7d,
      0x7a,0xa7,0x2,0xcd,0x7f,0x80,0,0x84,
      0xa,0xa6,0x85,0xcd,0x75,0xff,0xa6,0x85,
      0xcd,0x55,0x6,0,0x8d,0x3,0xcd,0x7c,
      0x6a,0xb,0x8a,0x6,0xd,0x8c,0x3,0xcd,
      0x95,0xbd,0xcd,0x76,0x13,0xad,0x9,0xad,
      0x6f,0x11,0x85,0xa6,0x1,0xa7,0x9,0x81,
      0xad,0x7c,0xa4,0xe7,0xb7,0x2,0x81,0xc6,
      0x1,0x32,0xa4,0x30,0x5f,0xcc,0x60,0x13,
      0x5f,0xcc,0x59,0xc0,0xa6,0x1,0x95,0xe7,
      0x5,0xc6,0x9,0x5a,0xe7,0x6,0xa6,0x5c,
      0xe7,0x4,0xa6,0x9,0xe7,0x3,0x81,0xc6,
      0x8,0x5a,0x87,0x45,0x8,0x5c,0x89,0x8b,
      0x4f,0xcd,0x7c,0xca,0xa7,0x3,0x81,0xc6,
      0x9,0x5a,0x87,0x45,0x9,0x5c,0x89,0x8b,
      0x4f,0xcd,0x7c,0xca,0xa7,0x3,0x81,0xc6,
      0xa,0x5a,0x87,0x45,0xa,0x5c,0x89,0x8b,
      0x4f,0xcd,0x7c,0xca,0xa7,0x3,0x81,0xae,
      0x2,0xcc,0x59,0xc0,0xcd,0x7c,0xac,0x4f,
      0xcc,0x59,0xe6,0xb6,0x85,0xa5,0x80,0x81,
      0xa6,0x1,0xcc,0x52,0x87,0xc6,0x1,0x32,
      0xa4,0x70,0x5f,0xcc,0x60,0x13,0x95,0xe6,
      0x3,0x87,0xee,0x4,0x8a,0x81,0xb6,0x6,
      0xaa,0x1f,0xb7,0x6,0xb6,0x2,0x81,0x5f,
      0xcc,0x54,0x7b,0x4,0x8a,0x2,0x4f,0x81,
      0xa6,0xff,0x81,0xcd,0x76,0x13,0x15,0x8a,
      0xcc,0x76,0x13,0x8b,0x15,0x8a,0xcd,0x76,
      0xad,0x95,0xfe,0x8c,0x4f,0xd7,0x8,0,
      0x8c,0xd7,0x9,0,0xd7,0xa,0,0x95,
      0x7c,0xf6,0xa1,0x5a,0x25,0xeb,0x4f,0xc7,
      0x8,0x5a,0xad,0x79,0x8c,0x9e,0xee,0x1,
      0x4f,0xd7,0x8,0x5c,0x95,0x7c,0xf6,0xa1,
      0x78,0x25,0xf1,0x4f,0xc7,0x8,0x5a,0xa6,
      0,0x45,0x1,0xb4,0xe7,0x3,0xa6,0x8,
      0xe7,0x2,0x4f,0xf7,0xe7,0x1,0xc7,0x9,
      0x5a,0xad,0x52,0x8c,0x9e,0xee,0x1,0x4f,
      0xd7,0x9,0x5c,0x95,0x7c,0xf6,0xa1,0x78,
      0x25,0xf1,0x4f,0xc7,0x9,0x5a,0xa6,0,
      0x45,0x1,0xba,0xe7,0x3,0xa6,0x9,0xe7,
      0x2,0x4f,0xf7,0xe7,0x1,0xc7,0xa,0x5a,
      0xad,0x2b,0x1c,0x6,0x1c,0x2,0x8c,0x9e,
      0xee,0x1,0x4f,0xd7,0xa,0x5c,0x95,0x7c,
      0xf6,0xa1,0x78,0x25,0xed,0x4f,0xc7,0xa,
      0x5a,0xa6,0,0x45,0x1,0xc0,0xe7,0x3,
      0xa6,0xa,0xe7,0x2,0x4f,0xf7,0xe7,0x1,
      0xcd,0x92,0x3e,0x8a,0x81,0x1c,0x6,0x1c,
      0x2,0x95,0x6f,0x2,0x81,0x87,0x89,0xa7,
      0xfb,0x95,0xe6,0x12,0xe7,0x2,0xe6,0x11,
      0xe7,0x1,0xc6,0x1,0x2c,0xa5,0x8,0x26,
      0x3,0xa6,0x1,0x21,0x4f,0xe7,0x3,0xc6,
      0x1,0x2c,0xa5,0x10,0x26,0x2,0x4f,0x65,
      0xa6,0x1,0xe7,0x4,0xcd,0x79,0x23,0x6f,
      0x1,0x7f,0x1,0x85,0x7,0xa6,0x1,0x95,
      0xe7,0x3,0xe7,0x4,0xa6,0x1,0x95,0xf7,
      0xf6,0xeb,0x2,0x87,0x4f,0xe9,0x1,0x87,
      0x8a,0x88,0x7d,0x26,0x7,0x95,0x7c,0xf6,
      0xa1,0x5a,0x25,0xec,0x95,0xf6,0xa1,0x5a,
      0x25,0xc,0x1c,0x6,0x1c,0x2,0xcd,0x79,
      0x23,0x6f,0x1,0xcc,0x79,0xe,0x1,0x8a,
      0xc,0xe6,0x2,0xab,0x5a,0xe7,0x12,0xe6,
      0x1,0xa9,0,0xe7,0x11,0,0x8a,0x3,
      0xa6,0x1,0x65,0xa6,0x80,0xf7,0x4f,0xc7,
      0x1,0xb5,0x17,0x8a,0xcc,0x77,0xbe,0x1c,
      0x6,0x1c,0x2,0x95,0xe6,0x11,0x87,0xee,
      0x12,0x9e,0xe6,0x2,0x8a,0xf4,0xc7,0x1,
      0xb3,0x1,0x8a,0x11,0x95,0x74,0x26,0x1a,
      0xa6,0x80,0xf7,0x6d,0x12,0x26,0x2,0x6a,
      0x11,0x6a,0x12,0x20,0xd,0x95,0x78,0x26,
      0x9,0xa6,0x1,0xf7,0x6c,0x12,0x26,0x2,
      0x6c,0x11,0x6,0x8a,0x54,0x45,0x1,0xb5,
      0x74,0xc6,0x1,0xb3,0x27,0x3,0xcd,0x79,
      0x35,0xcd,0x79,0x3e,0xc6,0x1,0xb5,0x95,
      0xe1,0xe,0x26,0x3a,0x16,0x8a,0x19,0x8a,
      0xc6,0x1,0xb5,0xc7,0x1,0xc7,0x4f,0xc7,
      0x1,0xb4,0xc7,0x1,0xc6,0xe6,0x10,0xab,
      0x2,0x97,0x9e,0xe6,0x10,0xcf,0x1,0xb7,
      0xa9,0,0xc7,0x1,0xb6,0xcd,0x79,0x23,
      0x7f,0x95,0x6d,0x3,0x27,0x10,0xcd,0x79,
      0x12,0xf7,0xcd,0x79,0x23,0x7c,0x45,0x1,
      0xb6,0x6c,0x1,0x26,0x1,0x7c,0xcc,0x78,
      0xb5,0x45,0x1,0xb5,0x74,0xc6,0x1,0xb3,
      0x27,0xf,0xcd,0x79,0x35,0xc6,0x1,0xc6,
      0x27,0x2,0x4f,0x65,0xa6,0x1,0xc7,0x1,
      0xc6,0xcd,0x79,0x3e,0x45,0x1,0xb4,0x7c,
      0xc6,0x1,0xb4,0x95,0xe1,0xa,0x27,0x3,
      0xcc,0x78,0xb2,0xc6,0x1,0xc6,0x26,0xb,
      0x17,0x8a,0xe6,0x5,0xaa,0x2,0xcd,0x79,
      0x2b,0xe7,0x1,0x4f,0xc7,0x1,0xc6,0xc7,
      0x1,0xb4,0x45,0x1,0xc7,0xf6,0xc8,0x1,
      0xb5,0xf7,0x8,0x8a,0x3,0xcc,0x78,0x99,
      0x95,0xe4,0xb,0x26,0x75,0xcd,0x79,0x23,
      0xf6,0xa1,0x2,0xcd,0x43,0x38,0x4f,0xcd,
      0x43,0x47,0x26,0x1,0x4c,0x9e,0xe4,0x4,
      0x7d,0x27,0x4,0x97,0x4f,0x20,0x3,0x97,
      0xa6,0x1,0x89,0x95,0xfa,0xa7,0x1,0x27,
      0x2f,0x1,0x8a,0xf,0x6d,0x13,0x26,0x2,
      0x6a,0x12,0x6b,0x13,0x2,0x6a,0x12,0x6a,
      0x13,0x20,0xc,0x6c,0x13,0x26,0x2,0x6c,
      0x12,0x6c,0x13,0x26,0x2,0x6c,0x12,0xe6,
      0x12,0x87,0xee,0x13,0x8a,0x7d,0x27,0x8,
      0xcd,0x79,0x23,0x7f,0x6f,0x1,0x20,0x1f,
      0x95,0x6d,0x4,0x27,0x15,0xcd,0x79,0x12,
      0xf7,0xcd,0x79,0x23,0x7c,0x9e,0xe6,0x6,
      0xe7,0x1,0x45,0x1,0xb6,0x6c,0x1,0x26,
      0x1,0x7c,0xcd,0x79,0x23,0x6f,0x1,0xcc,
      0x79,0xf,0x17,0x8a,0xe6,0x5,0xaa,0x4,
      0xcd,0x79,0x2b,0xe7,0x1,0x17,0x8a,0x20,
      0x5e,0xc6,0x1,0xb5,0x95,0xe1,0xd,0x26,
      0x16,0x18,0x8a,0x6d,0x3,0x27,0x1e,0xad,
      0x69,0xf7,0xad,0x77,0x7c,0x45,0x1,0xb6,
      0x6c,0x1,0x26,0x11,0x7c,0x20,0xe,0xad,
      0x59,0xf7,0xad,0x67,0x7c,0x45,0x1,0xb6,
      0x6c,0x1,0x26,0x1,0x7c,0,0x8a,0x1e,
      0x95,0xe6,0x2,0xab,0x5a,0x87,0xe6,0x1,
      0xa9,0,0xee,0x12,0x89,0x95,0xee,0x13,
      0x89,0x97,0x9e,0xe6,0x3,0xcd,0x42,0x7e,
      0x86,0x25,0x3,0xcc,0x77,0x37,0x1,0x8a,
      0xe,0x95,0xe6,0x12,0xe0,0x2,0xe6,0x11,
      0xe2,0x1,0x25,0x3,0xcc,0x77,0x37,0x1c,
      0x6,0x1c,0x2,0x95,0xee,0x10,0x9e,0xe6,
      0x10,0x87,0x8a,0x6d,0x1,0x26,0x5,0x9e,
      0xe6,0x6,0xe7,0x1,0xad,0x15,0x7f,0xa7,
      0x7,0x81,0xc6,0x1,0xb5,0x95,0xe4,0xd,
      0xeb,0x8,0xce,0x1,0xb6,0x89,0x8a,0xce,
      0x1,0xb7,0x81,0x95,0xe6,0x11,0x87,0xee,
      0x12,0x8a,0x81,0x87,0x95,0xe6,0x12,0x87,
      0xee,0x13,0x8a,0x86,0x81,0x45,0x1,0xb5,
      0xf6,0x9e,0xea,0xf,0xf7,0x81,0x45,0x1,
      0xb5,0xf6,0x9e,0xe4,0xc,0xf7,0x81,0x87,
      0x89,0xa7,0xfb,0x95,0xe6,0xa,0xe7,0x2,
      0xe6,0x9,0xe7,0x1,0xc6,0x1,0x2c,0xa5,
      0x8,0x26,0x3,0xa6,0x1,0x21,0x4f,0xe7,
      0x3,0xc6,0x1,0x2c,0xa5,0x10,0x26,0x2,
      0x4f,0x65,0xa6,0x1,0xe7,0x4,0xee,0x6,
      0x9e,0xe6,0x6,0x87,0x8a,0x6f,0x1,0x1,
      0x85,0x7,0xa6,0x1,0x95,0xe7,0x3,0xe7,
      0x4,0x1,0x8a,0xd,0x95,0xe6,0x2,0xab,
      0x5a,0xe7,0xa,0xe6,0x1,0xa9,0,0xe7,
      0x9,0xa6,0x1,0x95,0xf7,0xf6,0xeb,0x2,
      0x87,0x4f,0xe9,0x1,0x87,0x8a,0x88,0x7d,
      0x26,0x7,0x95,0x7c,0xf6,0xa1,0x5a,0x25,
      0xec,0x95,0xf6,0xa1,0x5a,0x25,0xc,0x1c,
      0x6,0x1c,0x2,0xcd,0x7b,0x35,0x6f,0x1,
      0xcc,0x7a,0xc0,0,0x8a,0x3,0xa6,0x1,
      0x65,0xa6,0x80,0xf7,0x4f,0xc7,0x1,0xb5,
      0x17,0x8a,0xcc,0x7a,0x5a,0x1c,0x6,0x1c,
      0x2,0x95,0xe6,0x9,0x87,0xee,0xa,0x9e,
      0xe6,0x2,0x8a,0xf4,0xc7,0x1,0xb3,0x1,
      0x8a,0x11,0x95,0x74,0x26,0x1a,0xa6,0x80,
      0xf7,0x6d,0xa,0x26,0x2,0x6a,0x9,0x6a,
      0xa,0x20,0xd,0x95,0x78,0x26,0x9,0xa6,
      0x1,0xf7,0x6c,0xa,0x26,0x2,0x6c,0x9,
      0x6,0x8a,0x5a,0x45,0x1,0xb5,0x74,0xc6,
      0x1,0xb3,0x27,0x3,0xcd,0x7b,0x50,0xcd,
      0x7b,0x4a,0x45,0x1,0xb5,0xf7,0xf6,0x41,
      0x5,0x5,0xc6,0x1,0xb5,0x4b,0x3b,0x16,
      0x8a,0x19,0x8a,0xc6,0x1,0xb5,0xc7,0x1,
      0xc7,0x4f,0xc7,0x1,0xb4,0xc7,0x1,0xc6,
      0x95,0xe6,0x6,0xab,0x2,0x97,0x9e,0xe6,
      0x6,0xcf,0x1,0xb7,0xa9,0,0xc7,0x1,
      0xb6,0xcd,0x7b,0x35,0x7f,0x95,0x6d,0x3,
      0x27,0x10,0xcd,0x7b,0x3d,0xf7,0xcd,0x7b,
      0x35,0x7c,0x45,0x1,0xb6,0x6c,0x1,0x26,
      0x1,0x7c,0xcc,0x7a,0xdf,0x45,0x1,0xb5,
      0x74,0xc6,0x1,0xb3,0x27,0x3,0xcd,0x7b,
      0x50,0xcd,0x7b,0x4a,0xc7,0x1,0xb5,0x45,
      0x1,0xb4,0x7c,0xc6,0x1,0xb4,0xa1,0x6,
      0x26,0x75,0x4f,0xc7,0x1,0xc6,0xc7,0x1,
      0xb4,0x45,0x1,0xc7,0xf6,0xc8,0x1,0xb5,
      0xf7,0x9,0x8a,0x37,0xa5,0x3f,0x26,0x2a,
      0x95,0x6d,0x4,0x27,0x1c,0xcd,0x7b,0x4a,
      0xab,0x20,0xce,0x1,0xb7,0x89,0xce,0x1,
      0xb6,0x89,0x8a,0x88,0xf7,0xcd,0x7b,0x35,
      0x7c,0x45,0x1,0xb6,0x6c,0x1,0x26,0x1,
      0x7c,0xcd,0x7b,0x35,0xa6,0x20,0xe7,0x1,
      0x20,0x78,0xad,0x79,0xa6,0x24,0xe7,0x1,
      0x7f,0x20,0x6f,0xc6,0x1,0xb5,0xa1,0x1f,
      0x26,0x17,0x18,0x8a,0x95,0x6d,0x3,0x27,
      0x1e,0xad,0x6a,0xf7,0xad,0x5f,0x7c,0x45,
      0x1,0xb6,0x6c,0x1,0x26,0x11,0x7c,0x20,
      0xe,0xad,0x5a,0xf7,0xad,0x4f,0x7c,0x45,
      0x1,0xb6,0x6c,0x1,0x26,0x1,0x7c,0,
      0x8a,0x1e,0x95,0xe6,0x2,0xab,0x5a,0x87,
      0xe6,0x1,0xa9,0,0xee,0xa,0x89,0x95,
      0xee,0xb,0x89,0x97,0x9e,0xe6,0x3,0xcd,
      0x42,0x7e,0x86,0x25,0x3,0xcc,0x79,0xcd,
      0x1,0x8a,0xe,0x95,0xe6,0xa,0xe0,0x2,
      0xe6,0x9,0xe2,0x1,0x25,0x3,0xcc,0x79,
      0xcd,0xad,0x12,0xa6,0x20,0xe7,0x1,0x7f,
      0x17,0x8a,0x4f,0xc7,0x1,0xb5,0x1c,0x6,
      0x1c,0x2,0xa7,0x7,0x81,0x95,0xe6,0x7,
      0x87,0xee,0x8,0x8a,0x81,0xad,0xb,0xab,
      0x20,0xce,0x1,0xb6,0x89,0x8a,0xce,0x1,
      0xb7,0x81,0xc6,0x1,0xb5,0xa4,0x3f,0x81,
      0x45,0x1,0xb5,0xf6,0xaa,0x20,0xf7,0x81,
      0xa7,0xf8,0x95,0x7f,0x6f,0x1,0x6f,0x2,
      0xa6,0x80,0xe7,0x3,0xe7,0x4,0xe7,0x5,
      0xb6,0x1,0xa4,0xa8,0xe7,0x6,0xcd,0x7c,
      0x54,0xa4,0xf7,0xaa,0x10,0xb7,0x2,0x2e,
      0x3,0xcc,0x7b,0xfc,0xa6,0xff,0xc7,0xff,
      0xff,0x9d,0xb6,0x1,0xa4,0xfc,0xb7,0x80,
      0xa4,0xa8,0x95,0xe7,0x7,0xe8,0x6,0x27,
      0x68,0xe4,0x6,0xb7,0x81,0xee,0x7,0x9e,
      0xef,0x7,0x4d,0x27,0x5c,0xf,0x81,0x1b,
      0xc,0x80,0,0x95,0x66,0x3,0x24,0x13,
      0xf6,0x97,0x4c,0x9e,0xe7,0x1,0x9e,0xe6,
      0x4,0x43,0x8c,0xd7,0x8,0,0xa6,0x80,
      0x95,0xe7,0x3,0xb,0x81,0x1c,0x8,0x80,
      0,0x95,0x66,0x4,0x24,0x14,0xe6,0x1,
      0x97,0x4c,0x9e,0xe7,0x2,0x9e,0xe6,0x5,
      0x43,0x8c,0xd7,0x9,0,0xa6,0x80,0x95,
      0xe7,0x4,0x7,0x81,0x9a,0x4,0x80,0,
      0x95,0x66,0x5,0x24,0x92,0xe6,0x2,0x97,
      0x4c,0x9e,0xe7,0x3,0x9e,0xe6,0x6,0x43,
      0x8c,0xd7,0xa,0,0xa6,0x80,0x95,0xe7,
      0x5,0xcc,0x7b,0x77,0xcd,0x92,0x43,0xad,
      0x53,0xa4,0xef,0xaa,0x8,0xb7,0x2,0x95,
      0xfe,0x89,0x5c,0x9e,0xef,0x2,0x95,0xe6,
      0x4,0x43,0x88,0x8c,0xd7,0x8,0,0x9e,
      0xee,0x1,0x4f,0xd7,0x8,0,0x9e,0xee,
      0x2,0x89,0x5c,0x9e,0xef,0x3,0x9e,0xe6,
      0x6,0x43,0x88,0xd7,0x9,0,0x9e,0xee,
      0x2,0x4f,0xd7,0x9,0,0x9e,0xee,0x3,
      0x89,0x5c,0x9e,0xef,0x4,0x9e,0xe6,0x7,
      0x43,0x88,0xd7,0xa,0,0x9e,0xee,0x3,
      0x4f,0xd7,0xa,0,0xc7,0x1,0x91,0x14,
      0x8a,0xa7,0x8,0x81,0xb6,0x6,0xaa,0x1f,
      0xb7,0x6,0xb6,0x2,0x81,0xc6,0x1,0x32,
      0xc7,0x1,0xb1,0xc6,0x1,0x33,0xc7,0x1,
      0xb2,0x81,0xc6,0x1,0xb1,0xce,0x1,0xb2,
      0xcc,0x60,0x13,0x87,0xa1,0x64,0x25,0xd,
      0xa6,0x31,0x5f,0xcd,0x59,0xc0,0x95,0xf6,
      0xa0,0x64,0xf7,0x20,0x6,0xa6,0x30,0x5f,
      0xcd,0x59,0xc0,0x95,0xf6,0x8c,0xae,0xa,
      0xcd,0x42,0x58,0x87,0xab,0x30,0x5f,0xcd,
      0x59,0xc0,0x86,0xae,0xa,0x42,0x87,0x95,
      0xe6,0x1,0xf0,0xab,0x30,0x5f,0xcd,0x59,
      0xc0,0xa7,0x2,0x81,0xa5,0x4,0x26,0x4,
      0xa5,0x2,0x27,0x3,0xa6,0x4a,0x65,0xa6,
      0x49,0x5f,0xcc,0x59,0xc0,0x87,0x95,0x7d,
      0x26,0x2,0xa6,0x40,0x5f,0xcd,0x59,0xc0,
      0x8a,0x81,0x87,0x8b,0x7,0x8c,0x2,0x16,
      0x84,0x95,0xe6,0x6,0xf7,0x20,0x47,0x6d,
      0x1,0x26,0x10,0xcd,0x7d,0x72,0xf6,0xa0,
      0x20,0xcd,0x7f,0x37,0xab,0x20,0xcd,0x6d,
      0x3e,0x20,0x2b,0xe6,0x1,0xa1,0x1,0x26,
      0x9,0xad,0x7f,0xf6,0xa0,0x20,0xad,0xc5,
      0x20,0x1c,0xa1,0x2,0x26,0x9,0xad,0x72,
      0xf6,0xa0,0x30,0xad,0xb8,0x20,0xf,0x41,
      0x3,0x4,0xa1,0x4,0x26,0x8,0xad,0x62,
      0xf6,0xae,0x2,0xcd,0x59,0xc0,0x95,0x7a,
      0x6c,0x5,0x26,0x2,0x6c,0x4,0x7d,0x26,
      0xb6,0xe6,0x1,0xa1,0x1,0x26,0xf,0xa6,
      0x1f,0x5f,0xcd,0x59,0xc0,0xad,0x43,0xf6,
      0xa0,0x20,0xad,0x89,0x20,0x39,0xa1,0x2,
      0x26,0x10,0xa6,0xf,0x5f,0xcd,0x59,0xc0,
      0xad,0x30,0xf6,0xa0,0x30,0xcd,0x7c,0xbd,
      0x20,0x25,0xa1,0x3,0x26,0xe,0xa6,0x3f,
      0xae,0x2,0xcd,0x59,0xc0,0xad,0x1b,0xf6,
      0xa0,0x20,0x20,0x10,0xa1,0x4,0x26,0xf,
      0xa6,0x3f,0xae,0x2,0xcd,0x59,0xc0,0xad,
      0x9,0xf6,0xa0,0x30,0xcd,0x59,0xe6,0xa7,
      0x2,0x81,0x95,0xe6,0x6,0x87,0xee,0x7,
      0x8a,0x81,0x81,0xa7,0xfe,0xcd,0x82,0x6c,
      0x9e,0xe7,0x2,0x9e,0xef,0x1,0x95,0xfa,
      0x26,0x3,0xcc,0x7e,0x4b,0xcc,0x7e,0x45,
      0x95,0xf6,0x87,0xee,0x1,0x8a,0xf6,0xa1,
      0xff,0x26,0x3,0xcc,0x7e,0x4b,0xa1,0x5c,
      0x27,0x3,0xcc,0x7e,0x3b,0x1c,0x6,0x1c,
      0x2,0xe6,0x1,0xa1,0x41,0x26,0x2,0x1e,
      0x8a,0xe6,0x1,0xa1,0x42,0x26,0x2,0x1a,
      0x8a,0xe6,0x1,0xa1,0x43,0x26,0x2,0x10,
      0x8b,0xe6,0x1,0xa1,0x44,0x26,0x2,0x12,
      0x8b,0xe6,0x1,0xa1,0x45,0x26,0x2,0x1a,
      0x8c,0xe6,0x1,0xa1,0x47,0x26,0x2,0x14,
      0x8c,0xe6,0x1,0xa1,0x4a,0x26,0x2,0x1e,
      0x8b,0xe6,0x1,0xa1,0x4c,0x26,0x2,0x16,
      0x8c,0xe6,0x1,0xa1,0x4d,0x26,0x2,0x10,
      0x8c,0xe6,0x1,0xa1,0x4e,0x26,0x2,0x14,
      0x8b,0xe6,0x1,0xa1,0x50,0x26,0x2,0x12,
      0x8d,0xe6,0x1,0xa1,0x51,0x26,0xc,0xc6,
      0x1,0x1c,0xa1,0x30,0x26,0x3,0x18,0x83,
      0x65,0x19,0x83,0xe6,0x1,0xa1,0x52,0x26,
      0x2,0x10,0x8d,0xe6,0x1,0xa1,0x55,0x26,
      0x2,0x16,0x8b,0xe6,0x1,0xa1,0x57,0x26,
      0xe,0x1c,0x8a,0x1b,0x8b,0x1c,0x8b,0xc6,
      0x1,0x2c,0xaa,0x18,0xc7,0x1,0x2c,0xe6,
      0x1,0xa1,0x58,0x26,0x2,0x18,0x8b,0x95,
      0x6c,0x1,0x26,0x1,0x7c,0xf6,0x87,0xee,
      0x1,0x8a,0x7d,0x27,0x3,0xcc,0x7d,0x90,
      0xa7,0x2,0x81,0xa7,0xfe,0xcd,0x7f,0x25,
      0xc6,0x1,0xc9,0xa4,0x3f,0xce,0x1,0xca,
      0xcf,0x1,0xca,0xab,0x40,0xc7,0x1,0xc9,
      0x4f,0xc7,0x1,0x4e,0x1c,0x6,0x1c,0x2,
      0xce,0x1,0x4e,0x8c,0x9f,0xd7,0x8,0,
      0x45,0x1,0x4e,0x7c,0xc6,0x1,0x4e,0xa1,
      0x40,0x25,0xe9,0x4f,0xc7,0x1,0x4e,0xce,
      0x1,0x4e,0xc6,0x1,0x4f,0x8c,0xdb,0x8,
      0,0x87,0x9f,0xa4,0x1f,0xcb,0x1,0xca,
      0x87,0x4f,0xc9,0x1,0xc9,0x87,0x8a,0x88,
      0x86,0xfb,0xcd,0x7f,0x2d,0xd6,0x8,0,
      0xce,0x1,0x4f,0x87,0xd6,0x8,0,0xce,
      0x1,0x4e,0xd7,0x8,0,0xce,0x1,0x4f,
      0x86,0xd7,0x8,0,0x1c,0x6,0x1c,0x2,
      0x45,0x1,0x4e,0x7c,0xc6,0x1,0x4e,0xa1,
      0x40,0x25,0xbc,0xad,0x58,0xcd,0x82,0x6c,
      0x9e,0xe7,0x2,0x9e,0xef,0x1,0x95,0xfa,
      0x27,0x46,0x4f,0xc7,0x1,0x4e,0xce,0x1,
      0x4e,0xc6,0x1,0x4f,0x8c,0xdb,0x8,0,
      0x87,0x9f,0xa4,0x1f,0x95,0xeb,0x2,0x87,
      0x4f,0xe9,0x1,0x87,0x8a,0x88,0x86,0xfb,
      0xad,0x33,0xd6,0x8,0,0xce,0x1,0x4f,
      0x87,0xd6,0x8,0,0xce,0x1,0x4e,0xd7,
      0x8,0,0xce,0x1,0x4f,0x86,0xd7,0x8,
      0,0x1c,0x6,0x1c,0x2,0x45,0x1,0x4e,
      0x7c,0xc6,0x1,0x4e,0xa1,0x40,0x25,0xbe,
      0xad,0x3,0xa7,0x2,0x81,0x4f,0xc7,0x1,
      0x4e,0xc7,0x1,0x4f,0x81,0xa4,0x3f,0xc7,
      0x1,0x4f,0x8c,0xce,0x1,0x4e,0x81,0x87,
      0x45,0x1,0x4e,0xf6,0x4c,0xa4,0x3f,0xf7,
      0xc6,0x1,0x4f,0x8c,0xce,0x1,0x4e,0xdb,
      0x8,0,0xa4,0x3f,0xc7,0x1,0x4f,0xd6,
      0x8,0,0xce,0x1,0x4f,0x87,0xd6,0x8,
      0,0xce,0x1,0x4e,0xd7,0x8,0,0xce,
      0x1,0x4f,0x86,0xd7,0x8,0,0x1c,0x6,
      0x1c,0x2,0xd6,0x8,0,0xce,0x1,0x4e,
      0xdb,0x8,0,0xa4,0x3f,0x97,0xd6,0x8,
      0,0xc7,0x1,0xc8,0x95,0xf8,0x8a,0x81,
      0xa6,0xc,0xad,0x35,0xd6,0xde,0x34,0xcd,
      0x6d,0x3e,0xa6,0x8,0xad,0x2b,0xd6,0xde,
      0x34,0xcd,0x6d,0x3e,0xc6,0x1,0xca,0xce,
      0x1,0xc9,0x57,0x46,0x57,0x46,0x57,0x46,
      0x57,0x46,0xa4,0xf,0x8c,0x97,0xd6,0xde,
      0x34,0xcd,0x6d,0x3e,0xc6,0x1,0xca,0xa4,
      0xf,0x8c,0x97,0xd6,0xde,0x34,0xcc,0x6d,
      0x3e,0x87,0xc6,0x1,0xca,0xce,0x1,0xc9,
      0xcd,0x42,0x66,0x8a,0xa4,0xf,0x8c,0x97,
      0x81,0x8b,0xcd,0x81,0x9f,0x95,0xf7,0xcd,
      0x81,0x9f,0x95,0xf1,0x26,0x12,0x7d,0x27,
      0xf,0xc6,0x1,0x1c,0x41,0x58,0x5,0xf1,
      0x27,0x2,0x10,0x87,0xf6,0xc7,0x1,0x1c,
      0,0x87,0x6,0x9,0x83,0x3,0xcc,0x81,
      0x35,0xcd,0x7d,0x7b,0,0x87,0x6,0,
      0x83,0x3,0xcc,0x81,0x35,0x1f,0x85,0,
      0x84,0x24,0xd,0x83,0x21,0xa6,0x8c,0xcd,
      0x53,0x52,0x4d,0x27,0x19,0xa6,0x84,0xcd,
      0x81,0x63,0x4d,0x26,0x5f,0xc6,0x1,0x1c,
      0xcd,0x81,0x63,0xa6,0x94,0xcd,0x81,0x63,
      0x4f,0xcd,0x81,0x63,0x20,0x4e,0xc,0x8a,
      0x3,0xcc,0x80,0xae,0xc6,0x1,0xb0,0xa5,
      0x1,0x27,0x43,0xa6,0x65,0xcd,0x81,0x49,
      0xc6,0x1,0x1c,0xa1,0x34,0x26,0x5,0xa6,
      0x1a,0xcd,0x81,0x49,0xc6,0x1,0x1c,0xa1,
      0x33,0x26,0x5,0xa6,0x74,0xcd,0x81,0x49,
      0xc6,0x1,0x1c,0xa1,0x32,0x26,0x5,0xa6,
      0x28,0xcd,0x81,0x49,0xc6,0x1,0x1c,0xa1,
      0x31,0x26,0x5,0xa6,0x73,0xcd,0x81,0x49,
      0xc6,0x1,0x1c,0xa1,0x30,0x26,0x3c,0xa6,
      0x6d,0xcd,0x81,0x49,0x20,0x35,0xcd,0x61,
      0x64,0xc6,0x1,0x32,0xa5,0x40,0x27,0x3,
      0xcd,0x81,0x38,0xc6,0x1,0x32,0xa4,0x20,
      0x5f,0xcd,0x60,0x13,0xa6,0x1d,0xcd,0x81,
      0x49,0xa6,0x9d,0xcd,0x81,0x49,0xa6,0x9d,
      0xcd,0x81,0x49,0xa6,0x6b,0xae,0x2,0xcd,
      0x59,0xc0,0xcd,0x81,0x6d,0xcd,0x81,0x68,
      0xcd,0x61,0x71,0xcc,0x81,0x2f,0x3,0x8d,
      0x4c,0xc6,0x1,0x1c,0xa1,0x30,0x26,0x3,
      0xcd,0x81,0x4d,0xc6,0x1,0x1c,0xa1,0x31,
      0x26,0x3,0xcd,0x81,0x58,0xc6,0x1,0x1c,
      0xa1,0x32,0x26,0xb,0xa6,0x88,0x5f,0xcd,
      0x54,0x7b,0xa6,0x88,0xcd,0x55,0x6,0xc6,
      0x1,0x1c,0xa1,0x33,0x26,0xb,0xa6,0x89,
      0x5f,0xcd,0x54,0x7b,0xa6,0x89,0xcd,0x55,
      0x6,0xc6,0x1,0x1c,0xa1,0x34,0x26,0x3d,
      0xa6,0x8a,0x5f,0xcd,0x54,0x7b,0xa6,0x8a,
      0xcd,0x55,0x6,0x20,0x30,0xcd,0x7c,0x5d,
      0xc6,0x1,0x32,0xa5,0x40,0x27,0x2,0xad,
      0x2f,0,0x84,0x2,0xad,0x3f,0xad,0x58,
      0xb6,0x85,0xa5,0x80,0x27,0x4,0xad,0x50,
      0x20,0x8,0xcd,0x61,0x64,0xad,0x4e,0xcd,
      0x61,0x71,0xc,0x8a,0x5,0,0x84,0x2,
      0xad,0x2e,0xcd,0x7c,0x6a,0xad,0x39,0x11,
      0x87,0xa6,0x1,0x8a,0x81,0x4f,0x8a,0x81,
      0xa6,0x3a,0xad,0xd,0xa6,0xba,0xad,0x9,
      0xa6,0x2a,0xad,0x5,0xa6,0xaa,0xad,0x1,
      0x81,0x5f,0xcc,0x59,0xc0,0xa6,0x8c,0x5f,
      0xcd,0x54,0x7b,0xa6,0x8c,0xcc,0x55,0x6,
      0xa6,0x8d,0x5f,0xcd,0x54,0x7b,0xa6,0x8d,
      0xcc,0x55,0x6,0xae,0x4,0xcc,0x48,0x80,
      0xa6,0x1,0xcc,0x52,0x87,0xc6,0x1,0x1c,
      0xae,0x2,0xcc,0x59,0xc0,0xa1,0x9,0x22,
      0x24,0xa0,0x7,0x97,0x4f,0xcd,0x42,0xf2,
      0x3,0x1b,0x2,0x8,0xe,0x10,0x87,0x10,
      0x83,0xa6,0x1,0x81,0xc6,0x1,0x2d,0xaa,
      0x20,0x20,0x5,0xc6,0x1,0x2d,0xa4,0xdf,
      0xc7,0x1,0x2d,0x20,0xec,0x4f,0x81,0x8b,
      0x1c,0x6,0x1c,0x2,0xa6,0x13,0xcd,0x47,
      0x89,0x1c,0x6,0x1c,0x2,0xa6,0x4,0x5f,
      0xcd,0x94,0x7c,0xb6,0,0x95,0xf7,0xbe,
      0,0x87,0x9e,0xe3,0x1,0x86,0x26,0x42,
      0xb6,0,0x95,0xf1,0x26,0x3c,0xb6,0,
      0x1c,0x6,0x1c,0x2,0xf1,0x26,0x33,0xf6,
      0xa4,0xb6,0xa1,0xb6,0x22,0x27,0x97,0x4f,
      0xcd,0x43,0x16,0xb6,0x20,0x36,0xa,0x96,
      0xc,0xa6,0xe,0xb2,0x13,0xb4,0xe,0xb6,
      0x14,0xa6,0x34,0x20,0xd,0xa6,0x33,0x20,
      0x9,0xa6,0x30,0x20,0x5,0xa6,0x31,0x65,
      0xa6,0x32,0x95,0xf7,0x65,0x95,0x7f,0xf6,
      0x8a,0x81,0x4f,0x8a,0x81,0xa7,0xfe,0xa6,
      0x1,0xcd,0x83,0x32,0x9e,0xe7,0x2,0x9e,
      0xef,0x1,0x9e,0x6d,0x2,0x26,0x9,0x9e,
      0x6d,0x1,0x26,0x4,0xa6,0xbf,0x20,0x25,
      0x89,0x8a,0x97,0xe6,0x3,0xa1,0x3,0x26,
      0x1a,0xe6,0x7,0xc7,0x1,0x2e,0xe6,0x6,
      0x87,0x4f,0x9e,0xee,0x3,0x87,0xe6,0x5,
      0x95,0xfb,0xf7,0x4f,0xe9,0x1,0x97,0x86,
      0x8a,0x20,0x3,0xe6,0x5,0x5f,0xa7,0x2,
      0x81,0xa7,0xfe,0xa6,0x2,0xcd,0x83,0x32,
      0x9e,0xe7,0x2,0x9e,0xef,0x1,0x9e,0x6d,
      0x2,0x26,0x9,0x9e,0x6d,0x1,0x26,0x4,
      0xa6,0x2c,0x20,0x5,0x89,0x8a,0x97,0xe6,
      0x4,0xa7,0x2,0x81,0xa7,0xfe,0xa6,0x5,
      0xcd,0x83,0x32,0x9e,0xe7,0x2,0x9e,0xef,
      0x1,0x9e,0x6d,0x2,0x26,0x9,0x9e,0x6d,
      0x1,0x26,0x4,0x5f,0x4f,0x20,0x8,0xab,
      0x4,0x87,0x9f,0xa9,0,0x97,0x86,0xa7,
      0x2,0x81,0x87,0xa7,0xfa,0x1,0x89,0x60,
      0xa6,0x6,0xcd,0x83,0x32,0x9e,0xe7,0x6,
      0x9e,0xef,0x5,0x9e,0x6d,0x6,0x26,0x9,
      0x9e,0x6d,0x5,0x26,0x4,0x11,0x89,0x20,
      0x47,0x89,0x8a,0x97,0xe6,0x3,0x9e,0xe7,
      0x4,0x9e,0x6c,0x4,0x9f,0xab,0x4,0x8b,
      0x95,0xe7,0x3,0x86,0xa9,0,0xe7,0x2,
      0x20,0x58,0x1c,0x6,0x1c,0x2,0x95,0xe6,
      0x2,0xee,0x1,0x89,0x87,0x4c,0x26,0x1,
      0x5c,0x9e,0xe7,0x5,0x9e,0xef,0x4,0x95,
      0xe6,0x8,0xee,0x1,0x89,0x8a,0x88,0xf1,
      0x86,0x26,0x7,0x95,0xe6,0x2,0xee,0x1,
      0x20,0x3d,0x95,0x6a,0x3,0x27,0x36,0x65,
      0x20,0x33,0xe6,0x1,0x87,0xee,0x2,0x8a,
      0xf6,0xa1,0x8,0x22,0x28,0x9e,0xe7,0x1,
      0x9e,0xe6,0x4,0xf1,0x25,0x1f,0xf0,0x95,
      0xe7,0x3,0xf6,0xeb,0x2,0xe7,0x2,0x4f,
      0xe9,0x1,0xe7,0x1,0x6c,0x2,0x26,0x2,
      0x6c,0x1,0x95,0xe6,0x3,0x97,0x4a,0x9e,
      0xe7,0x4,0x5d,0x26,0x9d,0x5f,0x4f,0xa7,
      0x7,0x81,0x87,0xa7,0xfb,0x1,0x84,0x2,
      0x20,0x4a,0xc6,0x20,0x7,0x87,0xc6,0x20,
      0x6,0x87,0xc6,0x21,0x2f,0xce,0x21,0x2e,
      0xcd,0x42,0x7e,0x26,0x15,0xc6,0x20,0x9,
      0x87,0xc6,0x20,0x8,0x87,0xc6,0x21,0x31,
      0xce,0x21,0x30,0xcd,0x42,0x7e,0x26,0x2,
      0x20,0x73,0xa6,0x1,0x95,0xe7,0x3,0x6f,
      0x2,0x6f,0x4,0xa6,0x2e,0xe7,0x1,0xa6,
      0x21,0xf7,0x20,0x5a,0x1c,0x6,0x1c,0x2,
      0xe6,0x4,0x97,0x4c,0x9e,0xe7,0x5,0xa3,
      0x9,0x22,0x52,0x65,0x20,0x4f,0x95,0xf6,
      0x87,0xee,0x1,0x8a,0xfe,0x89,0x9e,0xef,
      0x4,0x8a,0x87,0x95,0xee,0x2,0x8a,0xee,
      0x1,0x9e,0xef,0x4,0x9e,0xe6,0x3,0x87,
      0x8a,0x65,0x1,0x2e,0x93,0x2f,0x65,0x20,
      0,0x90,0x2a,0x9f,0xab,0,0x8b,0x95,
      0xe7,0x2,0x86,0xad,0x25,0xe1,0x2,0x27,
      0xf,0x95,0xe6,0x3,0xab,0,0xe7,0x1,
      0xe6,0x2,0xad,0x16,0xe1,0x2,0x26,0x6,
      0x95,0xe6,0x1,0xfe,0x20,0x9,0x95,0xe6,
      0x3,0xea,0x2,0x26,0x9f,0x5f,0x4f,0xa7,
      0x6,0x81,0xa9,0x20,0x95,0xe7,0x2,0x87,
      0xee,0x3,0x9e,0xe6,0x9,0x8a,0x81,0x87,
      0xa7,0xfc,0xa6,0x9,0xcd,0x83,0x32,0x9e,
      0xe7,0x2,0x9e,0xef,0x1,0x95,0x6d,0x1,
      0x26,0x7,0x7d,0x26,0x4,0x5f,0x4f,0x20,
      0x36,0x12,0x85,0xe6,0x4,0x48,0x5f,0x59,
      0x87,0x9e,0xe6,0x3,0x9e,0xeb,0x1,0x9e,
      0xe7,0x1,0x9f,0x95,0xe9,0x1,0x87,0x8a,
      0x88,0xee,0x4,0x9e,0xef,0x4,0x95,0xe6,
      0x4,0x48,0x5f,0x59,0x9e,0xeb,0x2,0x87,
      0x9f,0x95,0xe9,0x1,0x87,0x8a,0x88,0xee,
      0x3,0x9e,0xef,0x3,0x9e,0xe6,0x4,0xa7,
      0x5,0x81,0x18,0x85,0x4f,0x81,0x19,0x85,
      0x4f,0x81,0x9,0x85,0x3,0xa6,0x1,0x81,
      0x4f,0x81,0x87,0x95,0xe6,0x4,0xc7,0x1,
      0x5d,0xe6,0x3,0xc7,0x1,0x5c,0xf6,0xa5,
      0x1,0x26,0x3,0x1d,0x85,0x65,0x1c,0x85,
      0x1b,0x85,0x4f,0xc7,0x1,0x5b,0x8a,0x81,
      0x87,0x12,0x82,0xa5,0x1,0x26,0x3,0x10,
      0x82,0x65,0x11,0x82,0x4f,0xc7,0x1,0x50,
      0xc7,0x1,0xcf,0xc7,0x1,0xce,0xc7,0x1,
      0xcd,0xc7,0x1,0x51,0xc7,0x1,0x5b,0x1b,
      0x85,0x1b,0x88,0x1f,0x88,0x95,0x7d,0x27,
      0xe,0xb6,0x6,0xaa,0x1f,0xb7,0x6,0xb6,
      0x2,0xa4,0xf7,0xaa,0x10,0xb7,0x2,0x8a,
      0x81,0xa7,0xf8,0x4f,0x1c,0x6,0x1c,0x2,
      0x95,0x6f,0x1,0x95,0xee,0x1,0x8c,0xd8,
      0x1,0x52,0x95,0x6c,0x1,0xee,0x1,0xa3,
      0x9,0x25,0xf0,0xc7,0x1,0x5b,0x1c,0x6,
      0x1c,0x2,0x6e,0x77,0x75,0x4d,0x27,0x3,
      0xcc,0x85,0xd9,0xb,0x88,0x3,0xcc,0x85,
      0xfc,0xb,0x89,0x5a,0xce,0x1,0xce,0x89,
      0x8a,0xce,0x1,0xcf,0x65,0x12,0x34,0x26,
      0x24,0xa6,0x4c,0x95,0xe7,0x3,0xa6,0x1f,
      0xe7,0x2,0x95,0xe6,0x3,0xee,0x2,0xcd,
      0xf8,0xc5,0x95,0xe6,0x2,0xab,0x2,0xe7,
      0x2,0xee,0x3,0x87,0x8a,0x65,0x20,0,
      0x91,0xe8,0xcc,0x85,0xd5,0x95,0x6f,0x1,
      0xc6,0x1,0xcf,0xab,0x1,0x87,0xc6,0x1,
      0xce,0xa9,0,0x87,0x95,0xee,0x3,0x8c,
      0xd6,0x1,0x52,0xde,0x1,0x53,0xcd,0xf8,
      0,0xa7,0x2,0x4d,0x26,0x5,0xcd,0x86,
      0x24,0x25,0xdd,0xcc,0x85,0xdb,0xc6,0x1,
      0xcf,0xa1,0x4,0x26,0x26,0xc6,0x1,0xce,
      0x26,0x21,0xa6,0x1,0x95,0xe7,0x5,0xa6,
      0x20,0xe7,0x4,0x95,0xe6,0x5,0xee,0x4,
      0xcd,0xf8,0xc5,0x95,0xe6,0x4,0xab,0x2,
      0xe7,0x4,0xee,0x5,0x87,0x8a,0x65,0x40,
      0,0x91,0xe8,0x45,0x1,0xce,0x6c,0x1,
      0x26,0x1,0x7c,0x95,0x6f,0x1,0xc6,0x1,
      0xcf,0x87,0xc6,0x1,0xce,0xab,0x20,0x87,
      0x95,0xee,0x3,0x8c,0xd6,0x1,0x52,0xde,
      0x1,0x53,0xcd,0xf8,0,0xa7,0x2,0x4d,
      0x26,0x5,0xcd,0x86,0x24,0x25,0xdf,0xa6,
      0x7,0x95,0xe7,0x1,0x45,0x1,0xce,0xe6,
      0x1,0xa0,0x2,0xe7,0x1,0xf6,0xa2,0,
      0xf7,0xee,0x1,0x9e,0xef,0x8,0xab,0x20,
      0x95,0xe7,0x6,0x1c,0x6,0x1c,0x2,0xe6,
      0x6,0x87,0xee,0x7,0x8a,0xf6,0x95,0xee,
      0x1,0x9e,0xef,0x1,0x8c,0xd1,0x1,0x52,
      0x26,0x1b,0x45,0x1,0xce,0x6d,0x1,0x26,
      0x1,0x7a,0x6a,0x1,0x95,0x6d,0x7,0x26,
      0x2,0x6a,0x6,0x6a,0x7,0xe6,0x1,0x4a,
      0xe7,0x1,0xf6,0x26,0xce,0x95,0xe6,0x1,
      0x4c,0x26,0x21,0xd,0x83,0x8,0xad,0x66,
      0xaa,0x20,0xad,0x57,0x20,0x10,0xad,0x5e,
      0xaa,0x20,0x5f,0xcd,0x5a,0x14,0xad,0x56,
      0xaa,0xa0,0x5f,0xcd,0x5a,0x14,0xad,0x55,
      0x1e,0x88,0x20,0x17,0xd,0x83,0x6,0xa6,
      0x10,0xad,0x38,0x20,0xc,0xa6,0x10,0x5f,
      0xcd,0x5a,0x14,0xa6,0x90,0x5f,0xcd,0x5a,
      0x14,0xad,0x3a,0xa6,0x1,0xcd,0x63,0xa4,
      0xb,0x89,0x6,0x4f,0xc7,0x1,0xcb,0x1b,
      0x89,0xa7,0x8,0x81,0x45,0x1,0xce,0xe6,
      0x1,0xab,0x2,0xe7,0x1,0xf6,0xa9,0,
      0xf7,0x95,0xe6,0x3,0xab,0x2,0xe7,0x3,
      0xa1,0x8,0x81,0xae,0x4,0xcd,0x48,0x80,
      0x4f,0xae,0x4,0xcc,0x48,0x80,0xc6,0x1,
      0xcc,0x62,0xa4,0x3,0x81,0x19,0x83,0xa6,
      0xff,0xc7,0x1,0xcb,0x1d,0x88,0x81,0x87,
      0x1c,0x6,0x1c,0x2,0,0x82,0x5,0xa6,
      0x28,0xc7,0x1,0xcb,0xc6,0x1,0x50,0x97,
      0x4c,0xc7,0x1,0x50,0xa3,0xf,0x22,0x48,
      0x4f,0xcd,0x42,0xc3,0,0x10,0x86,0xb8,
      0x86,0x98,0x86,0xbb,0x86,0xe5,0x87,0,
      0x87,0x12,0x87,0x2f,0x87,0x47,0x87,0x5b,
      0x87,0x6d,0x87,0x86,0x87,0xa2,0x87,0xb5,
      0x87,0xc2,0x87,0xde,0x87,0xf8,0x88,0xb,
      0x95,0xf6,0xc7,0x1,0x5b,0xa4,0xc0,0xa1,
      0x80,0x26,0x76,0xa6,0xa,0x87,0xc6,0x1,
      0x5b,0x5f,0xcd,0x42,0x72,0x8a,0xcd,0x88,
      0x31,0x9f,0xca,0x1,0xce,0xc7,0x1,0xce,
      0x4f,0x8a,0x81,0xcd,0x88,0x1c,0xa4,0xc0,
      0xa1,0x40,0x26,0x55,0x95,0xf6,0x5f,0x48,
      0x59,0x48,0x59,0x48,0x59,0x48,0x59,0xa4,
      0xf0,0x87,0x9f,0xa4,0x3,0x87,0x95,0xe6,
      0x1,0xcd,0x88,0x31,0x86,0x45,0x1,0xce,
      0xfa,0xf7,0x8a,0x20,0xd3,0x95,0xf6,0x45,
      0x1,0xcc,0xf7,0xc8,0x1,0x5b,0xc7,0x1,
      0x5b,0xf6,0xa5,0xc0,0x26,0x23,0xa4,0xf,
      0xcd,0x88,0x31,0xc6,0x1,0xce,0x20,0xb5,
      0xcd,0x88,0x1c,0xa4,0xc0,0xa1,0x80,0x26,
      0x10,0x95,0xf6,0x48,0x48,0xc7,0x1,0x52,
      0x20,0xa6,0xcd,0x88,0x1c,0xa4,0xc0,0xa1,
      0x40,0x26,0x72,0x95,0xf6,0x62,0xa4,0x3,
      0xca,0x1,0x52,0xc7,0x1,0x52,0xf6,0x62,
      0xa4,0xf0,0xc7,0x1,0x53,0x20,0x89,0xcd,
      0x88,0x1c,0xa5,0xc0,0x26,0x57,0x44,0x44,
      0xa4,0xf,0x45,0x1,0x53,0xfa,0xf7,0xcd,
      0x88,0x28,0xc7,0x1,0x54,0x20,0x9c,0xcd,
      0x88,0x1c,0xa4,0xc0,0xa1,0x80,0x26,0x3d,
      0x95,0xf6,0xa4,0x3f,0x45,0x1,0x54,0xfa,
      0xf7,0x20,0xd2,0xcd,0x88,0x1c,0xa4,0xc0,
      0xa1,0x40,0x26,0x29,0x95,0xf6,0x48,0x48,
      0xc7,0x1,0x55,0x20,0xc0,0xcd,0x88,0x1c,
      0xa5,0xc0,0x26,0x19,0x62,0xa4,0x3,0x45,
      0x1,0x55,0xfa,0xf7,0x95,0xf6,0x62,0xa4,
      0xf0,0xc7,0x1,0x56,0x20,0xa7,0xcd,0x88,
      0x1c,0xa4,0xc0,0xa1,0x80,0x26,0x6f,0x95,
      0xf6,0x44,0x44,0xa4,0xf,0x45,0x1,0x56,
      0xfa,0xf7,0xcd,0x88,0x28,0xc7,0x1,0x57,
      0x20,0x8b,0xad,0x78,0xa4,0xc0,0xa1,0x40,
      0x26,0x54,0x95,0xf6,0xa4,0x3f,0x45,0x1,
      0x57,0xfa,0xf7,0x20,0xa4,0xad,0x65,0xa5,
      0xc0,0x26,0x43,0x48,0x48,0xc7,0x1,0x58,
      0x20,0x83,0xad,0x58,0xa4,0xc0,0xa1,0x80,
      0x26,0x34,0x95,0xf6,0x62,0xa4,0x3,0xca,
      0x1,0x58,0xc7,0x1,0x58,0xf6,0x62,0xa4,
      0xf0,0xc7,0x1,0x59,0x20,0xe2,0xad,0x3c,
      0xa4,0xc0,0xa1,0x40,0x26,0x18,0x95,0xf6,
      0x44,0x44,0xa4,0xf,0x45,0x1,0x59,0xfa,
      0xf7,0xad,0x35,0xc7,0x1,0x5a,0x20,0xc8,
      0xad,0x22,0xa5,0xc0,0x27,0x4,0xa6,0x1,
      0x8a,0x81,0xa4,0x3f,0x45,0x1,0x5a,0xfa,
      0xf7,0x20,0xb5,0xc6,0x1,0x5b,0xa4,0x3f,
      0x95,0xf1,0x27,0x4,0xa6,0x2,0x8a,0x81,
      0xa6,0x8,0x8a,0x81,0xc6,0x1,0x5b,0x95,
      0xe8,0x2,0xc7,0x1,0x5b,0xe6,0x2,0x81,
      0x95,0xe6,0x2,0x46,0x46,0x46,0xa4,0xc0,
      0x81,0xca,0x1,0xcf,0xc7,0x1,0xcf,0x81,
      0x87,0x1c,0x6,0x1c,0x2,0xa1,0x30,0x25,
      0xc,0xa0,0x3a,0xa1,0x6,0x23,0x6,0x95,
      0xf6,0xa1,0x46,0x23,0x8,0x13,0x82,0x11,
      0x82,0xa6,0x3,0x8a,0x81,0,0x82,0x5,
      0xa6,0x28,0xc7,0x1,0xcb,0xf6,0xa0,0x30,
      0xf7,0xa1,0x9,0x23,0x3,0xa0,0x7,0xf7,
      0xc6,0x1,0x50,0x97,0x4c,0xc7,0x1,0x50,
      0xa3,0x7,0x23,0x3,0xcc,0x89,0x26,0x4f,
      0xcd,0x42,0xf2,0x8,0xa9,0x7,0x19,0x2a,
      0x3c,0x4a,0x58,0x64,0x6f,0xcd,0x89,0x68,
      0xa6,0xc,0x87,0x95,0xe6,0x1,0x5f,0xcd,
      0x42,0x72,0x8a,0xcd,0x89,0x71,0x20,0x41,
      0xcd,0x89,0x68,0x95,0xf6,0x87,0x4f,0xcd,
      0x89,0x7a,0x86,0x45,0x1,0xce,0xfa,0xf7,
      0x20,0x3c,0xcd,0x89,0x68,0x95,0xf6,0x5f,
      0x48,0x59,0x48,0x59,0x48,0x59,0x48,0x59,
      0xcd,0x89,0x71,0x20,0x35,0xcd,0x89,0x68,
      0x95,0xf6,0xcd,0x89,0x7a,0x45,0x1,0xce,
      0xf6,0xf7,0x20,0x70,0xcd,0x89,0x68,0x95,
      0xf6,0x62,0xa4,0xf0,0x45,0x1,0xcd,0xfa,
      0xf7,0x20,0x61,0xcd,0x89,0x68,0xc6,0x1,
      0xcd,0x95,0xfa,0xc7,0x1,0xcd,0x20,0x7d,
      0x95,0xf6,0x62,0xa4,0xf0,0x45,0x1,0x51,
      0xfa,0xf7,0x20,0x71,0xc6,0x1,0x51,0x95,
      0xfa,0x45,0x1,0x5b,0xf0,0xc7,0x1,0x51,
      0x4f,0xf7,0xce,0x1,0x51,0x27,0x6,0x13,
      0x82,0x11,0x82,0x20,0x50,0xce,0x1,0xcf,
      0xcf,0x1,0x5d,0xce,0x1,0xce,0xcf,0x1,
      0x5c,0x1,0x82,0x4a,0x1b,0x83,0x17,0x83,
      0x13,0x82,0xa6,0x1,0x8a,0x81,0xc6,0x1,
      0xcd,0x27,0x2,0xad,0x3b,0xc6,0x1,0x50,
      0xa5,0x1,0x27,0xa,0x95,0xf6,0x62,0xa4,
      0xf0,0xc7,0x1,0x51,0x20,0x27,0xc6,0x1,
      0x51,0x95,0xfa,0xc7,0x1,0x51,0xce,0x1,
      0xcd,0x26,0x16,0x13,0x82,0xc0,0x1,0x5b,
      0xc7,0x1,0x51,0x5f,0xcf,0x1,0x5b,0x4d,
      0x27,0xc8,0xcf,0x1,0x51,0xa6,0x2,0x8a,
      0x81,0x45,0x1,0xcd,0x7a,0x4f,0x8a,0x81,
      0x45,0x1,0x5b,0xf6,0x9e,0xeb,0x3,0xf7,
      0x81,0xad,0x7,0x9f,0x45,0x1,0xce,0xfa,
      0xf7,0x81,0xca,0x1,0xcf,0xc7,0x1,0xcf,
      0x81,0x87,0x8b,0x1c,0x6,0x1c,0x2,0xc6,
      0x1,0xcd,0x26,0x5,0xc6,0x1,0x5b,0x20,
      0xb,0xce,0x1,0x5d,0xc6,0x1,0x5c,0xab,
      0x20,0x87,0x8a,0xf6,0x95,0xf7,0xd,0x83,
      0x26,0x6d,0x1,0x27,0x78,0xc6,0x1,0xcd,
      0x27,0x9,0xc6,0x1,0x5b,0xfb,0xc7,0x1,
      0x5b,0x20,0x4,0x19,0x83,0x11,0x82,0x45,
      0x1,0xcd,0x7a,0x45,0x1,0x5c,0x6c,0x1,
      0x26,0x1,0x7c,0x1b,0x85,0x20,0x56,0xb,
      0x85,0x2d,0x6d,0x1,0x27,0x1c,0xc6,0x1,
      0xcd,0x27,0x5,0xf6,0xad,0x58,0x20,0x4,
      0x19,0x83,0x11,0x82,0x45,0x1,0xcd,0x7a,
      0x45,0x1,0x5c,0x6c,0x1,0x26,0x1,0x7c,
      0x1b,0x85,0xd,0x83,0x6,0x95,0xf6,0xa4,
      0xf,0x20,0x38,0x95,0xf6,0x20,0x1e,0x6d,
      0x1,0x27,0xb,0x1a,0x85,0xc6,0x1,0xcd,
      0x27,0x4,0xf6,0x62,0xad,0x28,0xd,0x83,
      0x7,0x95,0xf6,0x62,0xa4,0xf,0x20,0x1b,
      0x95,0xf6,0x62,0xa4,0xf,0xcd,0x8b,0x80,
      0x95,0xf7,0xd,0x83,0x4,0x95,0xf6,0x20,
      0xa,0xa,0x84,0x3,0xf6,0x20,0x4,0xf6,
      0xcd,0x5d,0x3f,0xa7,0x2,0x81,0xa4,0xf,
      0x45,0x1,0x5b,0xfb,0xf7,0x81,0x87,0xa7,
      0xfd,0x1c,0x6,0x1c,0x2,0xc,0x85,0x2,
      0x5f,0x65,0xae,0xff,0x9e,0xef,0x1,0xcd,
      0x8a,0xdc,0x90,0x2,0x4f,0x65,0xa6,0x1,
      0x9e,0xee,0x1,0x89,0x9e,0xe4,0x1,0x88,
      0xd,0x85,0x2,0x5f,0x65,0xae,0x1,0x9e,
      0xe7,0x2,0xc6,0x1,0x5d,0xc0,0x20,0x7,
      0xc6,0x1,0x5c,0xc2,0x20,0x6,0x90,0x2,
      0x4f,0x65,0xa6,0x1,0x89,0x9e,0xe4,0x1,
      0x9e,0xea,0x3,0x86,0x27,0xd,0x95,0x6d,
      0x3,0x27,0x4,0x19,0x83,0x1b,0x82,0xa6,
      0xec,0x20,0x4e,0xce,0x1,0x5d,0x8b,0x86,
      0xab,0x20,0x87,0x8a,0xf6,0x95,0xe7,0x2,
      0xb,0x85,0x1b,0x6d,0x3,0x27,0x12,0x45,
      0x1,0x5c,0x6c,0x1,0x26,0x1,0x7c,0xad,
      0x33,0x91,0x4,0x19,0x83,0x1b,0x82,0x1b,
      0x85,0xc,0x83,0x25,0x20,0x11,0x6d,0x3,
      0x27,0x2,0x1a,0x85,0xd,0x83,0x5,0x62,
      0xa4,0xf,0x20,0x15,0x62,0xa4,0xf,0xcd,
      0x8b,0x80,0x95,0xe7,0x2,0xa,0x84,0x4,
      0xe6,0x2,0x20,0x5,0xe6,0x2,0xcd,0x5d,
      0x3f,0xa7,0x4,0x81,0xce,0x1,0x5c,0x89,
      0x8a,0xce,0x1,0x5d,0x65,0x20,0,0x81,
      0x87,0x1c,0x6,0x1c,0x2,0xa1,0x40,0x26,
      0x8,0xcd,0x8b,0x88,0xcd,0x8b,0x77,0x20,
      0x7a,0xa1,0x3f,0x26,0x12,0xcd,0x8b,0x88,
      0xad,0x75,0xc6,0x1,0x5b,0x27,0x4,0xa6,
      0xfe,0x8a,0x81,0xa6,0xfa,0x8a,0x81,0xa1,
      0x40,0x23,0x4,0xa0,0x7,0x95,0xf7,0x95,
      0xf6,0xa4,0xf,0xf7,0x1c,0x6,0x1c,0x2,
      0xb,0x85,0x42,0xc6,0x1,0x5e,0xfa,0xc7,
      0x1,0x5e,0x45,0x1,0x5b,0xfb,0xf7,0xce,
      0x1,0x5d,0xc6,0x1,0x5c,0xab,0x20,0x87,
      0x8a,0xf6,0xc1,0x1,0x5e,0x26,0x3,0xa6,
      0xfa,0x65,0xa6,0xfe,0x95,0xf7,0xce,0x1,
      0x5c,0x89,0x8a,0xce,0x1,0x5d,0x65,0x20,
      0,0x90,0x4,0xa1,0xfa,0x27,0x2,0xad,
      0x1e,0x45,0x1,0x5c,0x6c,0x1,0x26,0x1,
      0x7c,0x1b,0x85,0x20,0xa,0x62,0xa4,0xf0,
      0xc7,0x1,0x5e,0x1a,0x85,0x20,0x4,0xa1,
      0xfa,0x26,0x2,0xa6,0xfa,0x8a,0x81,0x1d,
      0x82,0x1b,0x83,0x17,0x83,0x19,0x83,0x81,
      0xa4,0xf,0x8c,0x97,0xd6,0xdb,0x4d,0x81,
      0xa7,0xfc,0x95,0x6f,0x1,0x7f,0x4f,0xc7,
      0x1,0xcb,0x4c,0xcd,0x63,0xa4,0xa6,0x4,
      0x95,0xe7,0x3,0xa6,0x20,0xe7,0x2,0x20,
      0x1a,0x95,0xe6,0x2,0x87,0xee,0x3,0x8a,
      0xf6,0x95,0xeb,0x1,0xe7,0x1,0x4f,0xf9,
      0xf7,0x1c,0x6,0x1c,0x2,0x6c,0x3,0x26,
      0x2,0x6c,0x2,0xc6,0x20,0x6,0xab,0x20,
      0xee,0x3,0x89,0x95,0xee,0x3,0x89,0x97,
      0xc6,0x20,0x7,0xcd,0x42,0x7e,0x22,0xd1,
      0x1c,0x6,0x1c,0x2,0x4f,0xae,0x20,0xcd,
      0x94,0x7,0x4d,0x26,0x1c,0xc6,0xca,0x3,
      0xc7,0x8,0,0xc6,0xca,0x4,0xc7,0x8,
      0x1,0x95,0xf6,0xc7,0x8,0x2,0xe6,0x1,
      0xc7,0x8,0x3,0x4f,0xae,0x20,0xcd,0x93,
      0x6d,0xb6,0x88,0xa5,0x80,0x27,0x20,0xd,
      0x83,0xf,0xa6,0x2,0xae,0x4,0xcd,0x48,
      0x80,0x4f,0xae,0x4,0xcd,0x48,0x80,0x20,
      0xc,0xa6,0x2,0x5f,0xcd,0x5a,0x14,0xa6,
      0x82,0x5f,0xcd,0x5a,0x14,0x1f,0x88,0xa7,
      0x4,0x81,0x87,0xc6,0x1,0x64,0xa1,0x20,
      0x25,0x4,0x4f,0xc7,0x1,0x64,0xc6,0x1,
      0x64,0x97,0x4c,0xc7,0x1,0x64,0x8c,0x9e,
      0xe6,0x1,0xd7,0x1,0xd1,0xc6,0x1,0x64,
      0xab,0x3,0xa4,0x1f,0xc1,0x1,0x65,0x26,
      0x4,0xa6,0x1,0x8a,0x81,0x4f,0x8a,0x81,
      0xc6,0x1,0x64,0xc1,0x1,0x65,0x26,0x2,
      0x4f,0x81,0xc6,0x1,0x65,0xa1,0x20,0x25,
      0x4,0x4f,0xc7,0x1,0x65,0xc6,0x1,0x65,
      0x97,0x4c,0xc7,0x1,0x65,0x8c,0xd6,0x1,
      0xd1,0x81,0xc6,0x1,0x64,0xc1,0x1,0x65,
      0x26,0x3,0xa6,0x1,0x81,0x4f,0x81,0x1f,
      0x84,0x1a,0x84,0x1d,0x84,0x1c,0x83,0x11,
      0x85,0x1f,0x84,0x4f,0xc7,0x1,0x65,0xc7,
      0x1,0x64,0xcd,0x59,0xb8,0x17,0x88,0x81,
      0xad,0xd8,0x4d,0x27,0x4,0x4f,0xae,0x2,
      0x81,0x7,0x88,0x2,0x17,0x88,0xad,0xa8,
      0x5f,0x81,0x87,0x95,0x7d,0x27,0x8,0xcd,
      0x50,0xbb,0x95,0xf6,0xcd,0x51,0x54,0xa6,
      0x1,0x8a,0x81,0xa4,0xf,0x8c,0x97,0xd6,
      0xdc,0x3f,0x81,0x87,0xa7,0xfc,0xcd,0x8f,
      0x75,0xa6,0xff,0xc7,0x1,0x49,0xb6,0x89,
      0xa5,0x80,0x27,0x5,0xa6,0x5,0xc7,0x1,
      0x49,0x9,0x88,0xc,0xad,0x94,0x4d,0x27,
      0x7,0x19,0x88,0x6e,0xa0,0x4b,0x18,0x4b,
      0xcd,0x8f,0x75,0xad,0xab,0x9e,0xe7,0x2,
      0x9e,0xef,0x1,0xb6,0x89,0xa5,0x80,0x26,
      0x7,0xc6,0x1,0x49,0x26,0x2,0x17,0x83,
      0x95,0xee,0x1,0x9e,0xe6,0x1,0x87,0x8a,
      0x65,0x2,0,0x24,0x74,0xb6,0x89,0xa5,
      0x80,0x27,0x71,0xc6,0x1,0x49,0x26,0x3,
      0xcc,0x8e,0xcd,0xce,0x1,0x60,0xa3,0x1,
      0x23,0x54,0x4f,0xc7,0x1,0x60,0xc6,0x1,
      0x62,0x95,0xe1,0x1,0x26,0x1c,0x7d,0x26,
      0x19,0xc6,0x1,0x61,0xe1,0x1,0x26,0x3,
      0x7d,0x27,0x49,0xc6,0x1,0x61,0xc1,0x1,
      0x63,0x26,0x41,0xe6,0x1,0xc7,0x1,0x61,
      0x20,0x3a,0xc6,0x1,0x61,0xe1,0x1,0x26,
      0x3,0x7d,0x27,0x30,0xc6,0x1,0x62,0xc1,
      0x1,0x61,0x26,0x28,0xc6,0x1,0x61,0xc1,
      0x1,0x63,0x26,0x5,0xe6,0x1,0xc7,0x1,
      0x61,0xe6,0x1,0xc7,0x1,0x63,0xc6,0x1,
      0x62,0xe7,0x1,0x7f,0x20,0xe,0x8c,0x9e,
      0xe6,0x2,0xd7,0x1,0x61,0x45,0x1,0x60,
      0x7c,0xcc,0x8e,0x1d,0xcd,0x8f,0x75,0x95,
      0xf6,0x87,0xee,0x1,0x8a,0x65,0x1,0,
      0x24,0x17,0xa6,0xff,0xc7,0x1,0x49,0xa3,
      0xec,0x25,0x6,0x9f,0xcd,0x64,0x9a,0x20,
      0xa,0x7,0x83,0x5,0x9f,0xcd,0x64,0xd9,
      0x65,0xa6,0xfe,0xc7,0x1,0x5f,0xcd,0x8f,
      0x75,0xcd,0x8f,0x75,0xc6,0x1,0x5f,0x41,
      0xfe,0x4,0xa1,0xfa,0x26,0x4,0x4f,0xc7,
      0x1,0x5f,0x95,0xe6,0x1,0xa1,0xec,0x25,
      0x9,0xcd,0x63,0xc5,0x4d,0x27,0x3,0xcc,
      0x8e,0xcf,0xc6,0x1,0x5f,0xcd,0x8c,0xaa,
      0x4b,0x20,0xc6,0x1,0x5f,0x26,0x15,0x7,
      0x87,0x9,0x95,0xe6,0x1,0xc7,0x1,0xad,
      0xcd,0x98,0x26,0x3,0x84,0x6,0x6e,0x77,
      0x75,0xcd,0x94,0xa8,0x4f,0xc7,0x1,0x5f,
      0x20,0x20,0xcd,0x8c,0x98,0x9e,0xe7,0x4,
      0x9e,0xef,0x3,0x89,0x8a,0x97,0x65,0x2,
      0,0x24,0xa3,0x8b,0x95,0xe7,0x2,0x86,
      0xe7,0x1,0xc6,0x1,0x5f,0x27,0x3,0xcc,
      0x8d,0x84,0xcd,0x8f,0x75,0x7,0x83,0x3,
      0xcc,0x8c,0xd9,0xd,0x88,0x3,0xcd,0x84,
      0xa1,0xcd,0x5d,0x1a,0x9,0x86,0xc,0xa6,
      0x1d,0xc7,0x1,0x5f,0xa6,0x1,0xcd,0x63,
      0xa4,0x19,0x86,0x1,0x82,0x3,0x3,0x82,
      0x3,0xb,0x82,0xf,0xa6,0x2,0xc7,0x1,
      0x49,0x20,0x3,0xcd,0x8f,0x75,0xc6,0x1,
      0x49,0x26,0xf8,0xcc,0x8e,0xe6,0xcd,0x8f,
      0x75,0x1,0x82,0x9,0x2,0x82,0x6,0x4f,
      0xcd,0x89,0x81,0x20,0x7,0xb,0x82,0x7,
      0x4f,0xcd,0x8a,0x36,0xc7,0x1,0x5f,0,
      0x82,0x2,0x5f,0x65,0xae,0xff,0xa,0x82,
      0x2,0x4f,0x65,0xa6,0xff,0x89,0x95,0xfa,
      0x86,0x27,0x4f,0xd,0x83,0x2e,0xc6,0x1,
      0x5f,0x26,0x4,0xa6,0xfc,0x20,0x18,0xa1,
      0x80,0x26,0x4,0xa6,0xfd,0x20,0x10,0xa1,
      0x80,0x25,0x7,0xa6,0xfe,0xae,0x4,0xcd,
      0x48,0x80,0xc6,0x1,0x5f,0xa4,0x7f,0xae,
      0x4,0xcd,0x48,0x80,0x4f,0xae,0x4,0xcd,
      0x48,0x80,0x20,0x27,0xc6,0x1,0x5f,0xcd,
      0x8c,0xbb,0xc7,0x1,0x5f,0x26,0xa,0x11,
      0x82,0x13,0x82,0x1b,0x82,0x19,0x83,0x20,
      0x67,0xcd,0x8c,0xaa,0x4d,0x26,0xc,0xcc,
      0x8f,0x62,0xc6,0x1,0x5f,0xcd,0x8c,0xaa,
      0x4d,0x27,0x55,0x4f,0xc7,0x1,0x5f,0x1,
      0x82,0x6,0x4c,0xcd,0x89,0x81,0x20,0x31,
      0xb,0x82,0x6,0x4c,0xcd,0x8a,0x36,0x20,
      0x28,0x3,0x86,0xb,0x13,0x86,0x14,0x86,
      0xa6,0x83,0xc7,0x1,0x5f,0x20,0x1a,0x5,
      0x86,0x4,0x15,0x86,0x19,0x83,0x7,0x86,
      0x2,0x17,0x86,0x9,0x86,0x4,0x19,0x86,
      0x19,0x83,0xb,0x86,0x4,0x1b,0x86,0x19,
      0x83,0x1,0x82,0x6,0x2,0x82,0x3,0xcc,
      0x8e,0x56,0xb,0x82,0x3,0xcc,0x8e,0x56,
      0xc6,0x1,0x5f,0x27,0x3,0xcc,0x8e,0x56,
      0xad,0x43,0xc6,0x1,0x5f,0x26,0x2d,0x95,
      0x6d,0x4,0x27,0x28,0xcd,0x5d,0x1a,0x4d,
      0x27,0x30,0xcd,0x92,0x43,0x4f,0xcd,0x5c,
      0x8,0x6,0x82,0x8,0xa6,0x2,0xcd,0x5c,
      0x8,0x4d,0x27,0x1e,0xa6,0x1,0xcd,0x5c,
      0x8,0xcd,0x76,0x3,0x4d,0x26,0x13,0xcd,
      0x92,0x3e,0x20,0xe,0xa6,0xff,0xc7,0xff,
      0xff,0x9d,0xc6,0x1,0x5f,0x27,0x3,0xcc,
      0x8c,0xd9,0xa7,0x5,0x81,0x1c,0x6,0x1c,
      0x2,0x81,0x87,0x8b,0xa6,0xff,0xc7,0xff,
      0xff,0x9d,0x3,0x88,0x17,0x95,0xe6,0x1,
      0xa1,0x1d,0x26,0x4,0x16,0x82,0x20,0x2c,
      0xa1,0x45,0x26,0x4,0xa6,0x48,0x20,0x3e,
      0xa6,0x7e,0x20,0x3a,0x1,0x88,0x21,0x95,
      0x7f,0xfe,0x58,0x9e,0xe6,0x2,0x8c,0xd1,
      0xdb,0xdd,0x26,0x5,0xd6,0xdb,0xde,0x20,
      0x25,0x95,0x7c,0xf6,0xa1,0x13,0x25,0xe9,
      0x16,0x82,0x11,0x88,0xa6,0x80,0x20,0x16,
      0x95,0xe6,0x1,0xa1,0xe0,0x24,0x9,0xa4,
      0x7f,0x8c,0x97,0xd6,0xdb,0x5d,0x20,0x6,
      0xa1,0x80,0x26,0x2,0x16,0x82,0xa7,0x2,
      0x81,0x87,0x89,0x8b,0x9f,0xa5,0x2,0x27,
      0x77,0x95,0x7f,0xcd,0x90,0x8f,0xd1,0xdc,
      0xb,0x26,0x28,0x9e,0xe6,0x2,0x2a,0xd,
      0xcd,0x48,0x6c,0xa6,0x3,0xc7,0xb,0x8,
      0xcd,0x48,0xeb,0x20,0x77,0xa6,0x3,0xc7
    };
    #endregion

  }

  /// <summary>
  /// Bug 21009 [Bug 18129] DJD - Support for Admin Transactions and eWallet
  /// This is PAYware Connect's basic authentication and it consists of the following four values:
  /// CLIENT_ID, USER_ID, USER_PW, MERCHANTKEY
  /// Optionally, the device serial number: SERIAL_NUM
  /// </summary>
  public class MerchantKeyAuthentication
  {
    public string clientID { get; set; }
    public string userID { get; set; }
    public string userPW { get; set; }
    public string merchantKey { get; set; }
    public string serialNum { get; set; } // Optional
    public string deptId { get; set; }

    public bool workgroupFeeOverride { get; set; }

    // Constructor
    public MerchantKeyAuthentication()
    {
      workgroupFeeOverride = false;
    }

    // Constructor
    public MerchantKeyAuthentication(GenericObject dept, GenericObject tender)
    {
      clientID = (tender != null && tender.has("CC_CLIENTID")) ? (string)tender.get("CC_CLIENTID") : (string)dept.get("CC_CLIENTID", "");
      userID = (tender != null && tender.has("CC_USERID")) ? (string)tender.get("CC_USERID") : (string)dept.get("CC_USERID", "");
      userPW = (tender != null && tender.has("CC_USERPW")) ? (string)tender.get("CC_USERPW") : (string)dept.get("CC_USERPW", "");
      merchantKey = (tender != null && tender.has("CC_MERCHANTKEY")) ? (string)tender.get("CC_MERCHANTKEY") : (string)dept.get("CC_MERCHANTKEY", "");
      workgroupFeeOverride = (tender != null && tender.has("CC_CLIENTID"));

      // Bug 17296 UMN password is now encrypted, so need to decrypt
      userPW = DecryptCCUserPwd(tender.has("_is_CC_override") ? "CC_USERPW_OVERRIDE" : "CC_USERPW", dept, userPW);
    }

    // Constructor
    public MerchantKeyAuthentication(GenericObject dept)
    {
      clientID = (string)dept.get("CC_CLIENTID", "");
      userID = (string)dept.get("CC_USERID", "");
      userPW = (string)dept.get("CC_USERPW", "");
      merchantKey = (string)dept.get("CC_MERCHANTKEY", "");
      workgroupFeeOverride = false;

      // Bug 17296 UMN password is now encrypted, so need to decrypt
      userPW = DecryptCCUserPwd("CC_USERPW", dept, userPW);
    }

    /// <summary>
    /// Decrypt merchant password if it is encrypted.
    /// Bug 17296 UMN
    /// </summary>
    /// <param name="tender"></param>
    /// <param name="workgroup"></param>
    /// <param name="password"></param>
    /// <returns></returns>
    private static String DecryptCCUserPwd(string userPwdType, GenericObject workgroup, String password)
    {
      try
      {
        byte[] bUserPwdEncrypted = Convert.FromBase64String(password);
        byte[] bUserPwdDecrypted = Crypto.symmetric_decrypt("data", bUserPwdEncrypted, "secret_key", Crypto.get_secret_key());
        password = System.Text.Encoding.Default.GetString(bUserPwdDecrypted);
      }
      catch
      {
        // on the fly update the password to be encrypted
        UpdateCCPwdToEncrypted(userPwdType, workgroup, password);
      }
      return password;
    }

    /// <summary>
    /// Update the CC_USERPW or CC_USERPW_OVERRIDE to be encrypted as per Bug 17296 UMN
    /// </summary>
    /// <param name="userPwdType">Either "CC_USERPW" or "CC_USERPW_OVERRIDE"</param>
    /// <param name="workgroup"></param>
    /// <param name="password"></param>
    private static void UpdateCCPwdToEncrypted(string userPwdType, GenericObject workgroup, String password)
    {
      try
      {
        // Uggh all of crypto code is designed to be called from CASL. It has no granular functions that
        // can be called from C#. What a PITA.
        // Need to do following in C#: crypto.symmetric.<encrypt> new_value.<to_base64string/> </encrypt>.data.<to_base64/>

        string pwdBase64 = misc.StringToBase64String(password);
        GenericObject bytes = Crypto.symmetric_encrypt("data", pwdBase64, "is_ascii_string", false);
        byte[] data = (byte[])bytes.get("data");
        string encPwd = Convert.ToBase64String(data);
        workgroup.set(userPwdType, encPwd);

        // Freeze the workgroup
        misc.CASL_call("a_method", "freeze", "_subject", workgroup, "args", new GenericObject("revision", 0));
      }
      catch (Exception ex)
      {
        Logger.cs_log(String.Format("TTS 17296 exception: {0}", ex.Message));
      }
    }


    public bool Validate(ref string errMsg)
    {
      if (string.IsNullOrEmpty(merchantKey) || string.IsNullOrEmpty(clientID) ||
      string.IsNullOrEmpty(userID) || string.IsNullOrEmpty(userPW))
      {
        errMsg = workgroupFeeOverride ?
        "Workgroup fee override is not configured for IPCharge." :
        "Workgroup is not configured for IPCharge.";

        // Bug 24484 MJO - Log more info
        Logger.LogWarn(
        String.Format("IPCharge merchant account{0} configuration missing in workgroup {1}", workgroupFeeOverride ? " fee" : "", deptId),
        "Merchant account validation"
        );

        return false;
      }
      return true;
    }

    /// <summary>
    /// Returns a hashed MerchantKey value (ClientID is used for the salt).
    /// In cryptography, a salt is data that is used as an additional input
    /// to a one-way function that "hashes" a password or passphrase.
    /// Returns null if no MerchantKey/ClientID values or if an error occurs.
    /// </summary>
    /// <returns>Hashed MerchantKey or null</returns>
    public string GetHashedMerchantKey()
    {
      string hashedMerchantKey = null;
      try
      {
        if (string.IsNullOrEmpty(merchantKey) || string.IsNullOrEmpty(clientID))
        {
          return hashedMerchantKey;
        }
        string salt = clientID;
        string data = merchantKey;
        hashedMerchantKey = Crypto.CASLComputeHash("data", data, "salt", salt);
      }
      catch (Exception ex)
      {
        string errMsg = string.Format("Error Hashing Merchant Key value:{0}{1}", Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
        Logger.cs_log(errMsg);
      }
      return misc.StringToBase64String(hashedMerchantKey);
    }
  }

  // Bug 18129 DJD - Support for eWallet (UGP Protocol - Managed device authentication)
  public class ManagedDeviceAuthentication
  {
    public string clientID { get; set; }
    public string serialNum { get; set; }
    public string deviceType { get; set; }
    public string deviceKey { get; set; }

    // Constructor
    public ManagedDeviceAuthentication()
    {
    }

    // Constructor
    public ManagedDeviceAuthentication(GenericObject PWC_SysInt)
    {
      clientID = (string)PWC_SysInt.get("enterprise_CC_client_ID", "");
      serialNum = (string)PWC_SysInt.get("enterprise_CC_serial_num", "");
      deviceType = (string)PWC_SysInt.get("enterprise_CC_device_type", "");
      deviceKey = getDeviceKeyFromDeviceIDTable(clientID, serialNum, deviceType);
    }

    // Bug 23150 DJD Added for device authentication
    public ManagedDeviceAuthentication(GenericObject tender, bool verifoneDevice)
    {
      if (verifoneDevice)
      {
        clientID = (string)tender.get("ewallet_CLIENT_ID", "");
        serialNum = (string)tender.get("ewallet_SERIAL_NUM", "");
        deviceType = (string)tender.get("ewallet_DEVTYPE", "");
        deviceKey = (string)tender.get("ewallet_DEVICEKEY", "");
      }
    }

    // Bug 18129 Added code to store the POSAPP managed device authentication values in the proper table (TG_DEVICE_ID).
    private string getDeviceKeyFromDeviceIDTable(string clientID, string serialNum, string deviceType)
    {
      string deviceKey = null;
      try
      {
        using (SqlConnection dataReaderConnection = new SqlConnection())
        {
          string deviceSql = @"SELECT * FROM TG_DEVICE_ID WHERE DEVICE_TYPE=@device_type AND SERIAL_NBR=@serial_nbr";

          string conString = c_CASL.c_object("TranSuite_DB.Config.db_connection_info.db_connection_string") as string;
          dataReaderConnection.ConnectionString = conString;
          dataReaderConnection.Open();

          // Create the command
          using (SqlCommand dataReaderCommand = new SqlCommand(deviceSql, dataReaderConnection))
          {
            // Get the timeout from Web.config.  This is typically set to 0
            object timeout = c_CASL.GEO.get("sql_query_timeout", "360"); // Default to 6 minutes if not set
            int commandTimeout = System.Convert.ToInt32(timeout);
            dataReaderCommand.CommandTimeout = commandTimeout;

            // Set the parameters
            dataReaderCommand.Parameters.Add("@device_type", SqlDbType.VarChar, 100).Value = deviceType;
            dataReaderCommand.Parameters.Add("@serial_nbr", SqlDbType.VarChar, 100).Value = string.Format("{0}|{1}", serialNum, clientID);

            using (SqlDataReader dataReader = dataReaderCommand.ExecuteReader(CommandBehavior.CloseConnection))
            {
              while (dataReader.Read())
              {
                int ord = dataReader.GetOrdinal("EXTERNAL_ID");
                deviceKey = dataReader.IsDBNull(ord) ? null : dataReader.GetString(ord);
              }
            }
          }
        }
      }
      catch (Exception ex)
      {
        string errMsg = string.Format("Error getting Device Key value from TG_DEVICE_ID table:{0}{1}", Environment.NewLine, ex.ToMessageAndCompleteStacktrace());
        Logger.cs_log(errMsg);
      }
      return deviceKey;
    }
  }

  /// <summary>
  /// Bug 18129 DJD - Support for Admin Transactions and eWallet
  /// Intercept and log the XML message for the eWallet WCF web service at the client 
  /// using an IClientMessageInspector.
  /// </summary>
  public class EWalletMessageInspector : IClientMessageInspector
  {
    /// <summary>
    /// Enables inspection or modification of a message after a reply message is
    /// received but prior to passing it back to the client application.
    /// </summary>
    /// <param name="reply">The message to be transformed into types and handed back to the client application.</param>
    /// <param name="correlationState">Correlation state data.</param>
    public void AfterReceiveReply(ref Message reply, object correlationState)
    {
      // eWallet Response
      string respMaskedXML = (reply.ToString());

      // Mask the sensitive data
      string[] tags = { "a:itemID", "AddWalletItemToWalletOwnerResult", "SetItemStatusResult" };

#if !DEBUG
      pci.MaskTags(ref respMaskedXML, tags);
#endif

      // ***** LOG THE REQUEST *****
      Logger.cs_log(string.Format("{0}{1}{0}{2}{0}{3}{0}"
      , Environment.NewLine                                                                                 // {0}
      , "====================================   eWallet:  RESPONSE    ====================================" // {1}
      , respMaskedXML                                                                                       // {2}
      , "=================================================================================================" // {3}
      ));
      return;
    }

    /// <summary>
    /// Summary:
    ///     Enables inspection or modification of a message before a request message
    ///     is sent to a service.
    /// Returns:
    ///     The object that is returned as the correlationState argument of the 
    ///     System.ServiceModel.Dispatcher.IClientMessageInspector.AfterReceiveReply
    ///     (System.ServiceModel.Channels.Message@,System.Object) method. This is null
    ///     if no correlation state is used. The best practice is to make this a 
    ///     System.Guid to ensure that no two correlationState objects are the same.
    /// </summary>
    /// <param name="request">The message to be sent to the service.</param>
    /// <param name="channel">The WCF client object channel.</param>
    /// <returns></returns>
    public object BeforeSendRequest(ref Message request, IClientChannel channel)
    {
      // eWallet Request
      string reqMaskedXML = (request.ToString());

      // Mask the sensitive data
      string[] tags = { "itemID" };

#if !DEBUG
pci.MaskTags(ref reqMaskedXML, tags);
#endif

      // ***** LOG THE REQUEST *****
      Logger.cs_log(string.Format("{0}{1}{0}{2}{0}{3}{0}"
      , Environment.NewLine                                                                                 // {0}
      , "====================================   eWallet:  REQUEST    =====================================" // {1}
      , reqMaskedXML                                                                                        // {2}
      , "=================================================================================================" // {3}
      ));

      return null;
    }
  }

  /// <summary>
  /// Bug 18129 DJD - Support for Admin Transactions and eWallet 
  /// The methods in the EWalletMessageInspector interface, BeforeSendRequest and AfterReceiveReply, 
  /// provide access to the request and reply. To use the inspector, add it to an IEndpointBehavior:
  /// </summary>
  public class EWalletInspectorBehavior : IEndpointBehavior
  {
    public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
    {
      clientRuntime.MessageInspectors.Add(new EWalletMessageInspector());
    }
    public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters) { return; }
    public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher) { return; }
    public void Validate(ServiceEndpoint endpoint) { return; }
  }

  // Bug 23150 DJD: Added owner demographics in transaction (i.e., returned from custom inquiry)
  public class EWalletDemographics
  {
    public string FIRST_NAME { get; set; }
    public string MI { get; set; }
    public string LAST_NAME { get; set; }
    public string ADDRESS1 { get; set; }
    public string ADDRESS2 { get; set; }
    public string CITY { get; set; }
    public string OWNER_STATE { get; set; }
    public string ZIP { get; set; }
    public string EMAIL { get; set; }

    public bool hasTranValues { get; set; }

    // Constructor
    public EWalletDemographics()
    {
      hasTranValues = false;
    }

    // Constructor
    public EWalletDemographics(GenericObject tran)
    {
      FIRST_NAME = tran.get("FIRST_NAME", null) as string;
      // Bug 25251 DJD: Added "FirstName" lookup
      if (string.IsNullOrWhiteSpace(FIRST_NAME)) { FIRST_NAME = tran.get("FirstName", null) as string; }
      MI = tran.get("MI", null) as string;
      LAST_NAME = tran.get("LAST_NAME", null) as string;
      // Bug 25251 DJD: Added "LastName" lookup
      if (string.IsNullOrWhiteSpace(LAST_NAME)) { LAST_NAME = tran.get("LastName", null) as string; }
      ADDRESS1 = tran.get("ADDRESS1", null) as string;
      ADDRESS2 = tran.get("ADDRESS2", null) as string;
      CITY = tran.get("CITY", null) as string;
      OWNER_STATE = tran.get("OWNER_STATE", null) as string;
      ZIP = tran.get("ZIP", null) as string;
      EMAIL = tran.get("EMAIL", null) as string;

      if (string.IsNullOrWhiteSpace(LAST_NAME))
      {
        hasTranValues = false;
      }
      else
      {
        hasTranValues = true;
      }
    }
  }

}
