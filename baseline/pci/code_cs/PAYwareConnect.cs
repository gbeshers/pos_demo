﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.IO;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Data.SqlClient;
using CASL_engine;
using ProjectBaseline;
using System.Security;
using System.Text.RegularExpressions;

// PAYwareConnect File Added: Bug 18129 DJD - Support for PAYware ADD-ON ADMIN Functions (e.g., Customer and Contract)

namespace pci
{
  public enum CardActionEnum
  {
    None = 0,
    CreditSale,
    CreditPreAuth,
    CreditCompletion,
    CreditVoid,
    CreditReturn,
    DebitSale,
    DebitVoid,
    DebitReturn,

    // Bug 18129 DJD - Actions for UGP requests (i.e.,persistent token)
    TokenQuery,   // MUST be FIRST UGP action (Any new UGP actions must be added between first and last)
    TokenUpdate,  // MUST be LAST UGP action (Any new UGP actions must be added between first and last)

    // Actions for the PAYware Connect Admin Addon Transactions
    ActivateContract, // MUST be FIRST Admin action (Any new Admin actions must be added between first and last)
    CancelContract,
    AddContractToCustomer,
    NewCustomer,
    NewCustomerWithContract,
    UpdateContract,
    UpdateCustomer, // MUST be LAST Admin action
  }

  /// <summary>
  /// Each card request must include PAYware gateway information for 
  /// the gateway service; it will provide the connection information
  /// and login credentials.
  /// </summary>
  public class GatewayInfo
  {
    public string Host;
    public string AlternateHost1;
    public string AlternateHost2;
    public int    Timeout = 10000; // Default to 10000

    // Client ID [Authentication]
    public string ClientID;

    // Bug 18129 DJD - Client ID Components for UGP Protocol [Account, Site, Term]
    public string Account;
    public string Site;
    public string Term;

    // Merchant Key Authentication:
    public string MerchantKey;
    public string User;
    public string Password;
    // PAYware's Managed Device Authentication uses: CLIENT_ID, SERIAL_NUM, DEVTYPE, DEVICEKEY
    public string SerialNumber;
    public string DeviceType;
    public string DeviceKey;

    public string LicenseIdentifier;
    public string SiteIdentifier;
    public string DeviceIdentifier;
    public string SiteTrace;
  }

  /// <summary>
  /// Each generic card request must include a RequestSender element with the name of 
  /// the company making the request.  Optionally, it may contain a workgroup ID,
  /// workgroup name, cashier ID, and cashier name.
  /// </summary>
  public class ReqSender
  {
    public string CompanyName;
    public string WorkgroupID;
    public string WorkgroupName;
    public string CashierID;
    public string CashierName;
  }

  /// <summary>
  /// A Credit Sale Transaction authorizes a sale made with a credit
  /// card; the transaction will be placed in the open batch.
  /// 
  /// A Credit Void removes a transaction (e.g., Sale, Return) from the
  /// open batch and is typically used for same day returns or to correct
  /// cashier mistakes; it can only be performed before the batch is 
  /// settled or closed.
  /// 
  /// </summary>
  [Serializable]
  public class CardTransaction
  {
    public CardActionEnum Action;
    public string TransactionID;
    public string PinBlock;
    public string KeySerialNbr;

    // Hopefully, this secure string storage is temporary
    [XmlIgnore]
    public System.Security.SecureString secureData;
    [XmlIgnore]
    public string secureDataPlaceHolder;

    public string AccountNumber;
    public string CVV;
    public string ExpMonth;
    public string ExpYear;
    public string ExpDate; // Bug 24436 DJD: Valid only for UGP requests when generating a payment token Ex: 1216
    public decimal? TransactionAmount = null;
    [XmlIgnore]
    public bool TransactionAmountSpecified { get { return TransactionAmount.HasValue; } }
    public decimal? ConvFeeAmount = null;
    [XmlIgnore]
    public bool ConvFeeAmountSpecified { get { return ConvFeeAmount.HasValue; } }
    public bool? CardPresent = null;
    [XmlIgnore]
    public bool CardPresentSpecified { get { return CardPresent.HasValue; } }
    public bool? PassedByRFID = null; // Radio Frequency Identification Device
    [XmlIgnore]
    public bool PassedByRFIDSpecified { get { return PassedByRFID.HasValue; } }
    public bool? CardReaderPresent = null; // Heartland specific
    [XmlIgnore]
    public bool CardReaderPresentSpecified { get { return CardReaderPresent.HasValue; } }
    public bool? ForceDuplicateTrans = null;
    [XmlIgnore]
    public bool ForceDuplicateTransSpecified { get { return ForceDuplicateTrans.HasValue; } }
    public bool? AllowPartialAuth = null;
    [XmlIgnore]
    public bool AllowPartialAuthSpecified { get { return AllowPartialAuth.HasValue; } }

    // This boolean indicates an eCommerce sale [e.g., Internet sale (Business Center - NOT Cashiering)]
    public bool eCommerceSale;
    public string TrackData;
    public string DeviceID;
    public string CustomerSalutation; // The salutation, if desired (e.g., Mr., Mrs., Dr., etc.).
    public string CustomerFirstname;
    public string CustomerLastname;
    public string CustomerMiddleInitial;
    public string CustomerStreet;  // Address_1
    public string CustomerStreet2; // Address_2
    public string CustomerCity;
    public string CustomerState;
    public string CustomerZip;
    public string CashierNumber;
    public string InvoiceNbr;
    public string CustomerID;
    public string TransDesc;
    public string MerchantData05;
    public string MerchantData06;
    public string MerchantData07;
    public string MerchantData08;
    public string MerchantData09;
    public string MerchantData10;

    public string Token;                  // Bug 18129 DJD
    public bool? TokenRenewal = null;    // Bug 18129 DJD
    public int TokenRenewalDays = 0;      // Bug 18129 DJD
    public string TokenAddress;           // Bug 25177 DJD: Card address for token update
    public string TokenZip;               // Bug 25177 DJD:Card zip code for token update
    public string TokenExpDate;           // Bug 26819 DJD - eWallet TOKEN EXPIRED - Need to send extend token expiration date call to Payware after each use

    public bool? IIAS = null;
    [XmlIgnore]
    public bool IIASSpecified { get { return IIAS.HasValue; } }
    public decimal AmountHealthcare = 0.0M;
    [XmlIgnore]
    public bool AmountHealthcareSpecified { get { return AmountHealthcare != 0.0M; } }
    public decimal AmountPrescription = 0.0M;
    [XmlIgnore]
    public bool AmountPrescriptionSpecified { get { return AmountPrescription != 0.0M; } }
    public decimal AmountVision = 0.0M;
    [XmlIgnore]
    public bool AmountVisionSpecified { get { return AmountVision != 0.0M; } }
    public decimal AmountClinic = 0.0M;
    [XmlIgnore]
    public bool AmountClinicSpecified { get { return AmountClinic != 0.0M; } }
    public decimal AmountDental = 0.0M;
    [XmlIgnore]
    public bool AmountDentalSpecified { get { return AmountDental != 0.0M; } }

    // ----------------------------------------------------------------------
    // PAYware Connect ADMIN ADD-ON Payment Transactions
    // Add-On transactions are transactions performed on previously stored customer credit cards using only the customer records. These transactions can be processed without passing credit card numbers. These transactions are performed on add-on contracts, recurring contracts, as well as installment contracts. The Add-On transactions feature is available within the Recurring Billing Dashboard in the Store Portal, as well as through API.
    // Add-On Transactions allow transactions to be processed on demand for previously stored customers and contracts. It is necessary to pass a customer ID as well as the contract ID for PAYware Connect to know which account needs to be processed. Typical Add-On transactions are: Sale, Pre-Authorization, and Credit/Return.
    // public string MerchantCustomerID; (RBCUSTOMER_ID)
    // public string MerchantContractID; (RBCONTRACT_ID)
    public bool? PrimaryAccount = null;  // Optional
    [XmlIgnore]
    public bool PrimaryAccountSpecified { get { return PrimaryAccount.HasValue; } }
    public bool? SendEmail = null;       // Optional
    [XmlIgnore]
    public bool SendEmailSpecified { get { return SendEmail.HasValue; } }
    // ----------------------------------------------------------------------

    // ----------------------------------------------------------------------
    // PAYware Connect Admin Request Transaction:
    // ********** CUSTOMER INFORMATION **********
    public string MerchantCustomerID;
    public string CompanyName;
    // Salutation             [CustomerSalutation]
    // FirstName              [CustomerFirstname]
    // LastName               [CustomerLastname]
    // MiddleInitial          [CustomerMiddleInitial]
    // Address1               [CustomerStreet]
    // Address2               [CustomerStreet2]
    // City                   [CustomerCity]
    // State                  [CustomerState]
    // Zip                    [CustomerZip]
    public string PrimaryContactName;
    public string PrimaryPhone;
    public string PrimaryEmail;
    public string AlternateContactName;
    public string AlternatePhone;
    public string AlternateEmail;
    // ********** CONTRACT INFORMATION **********
    public string MerchantContractID;
    public string CardType; // Must be in all caps. Example: MC, VISA, DISC
    // PrimaryAccountNumber   [AccountNumber]
    // PrimaryExpirationMonth [ExpMonth]
    // PrimaryExpirationYear  [ExpYear] 
    public bool? AlternatePayment = null;
    [XmlIgnore]
    public bool AlternatePaymentSpecified { get { return AlternatePayment.HasValue; } }
    public string AlternateAccountNumber;
    public string AlternateExpMonth;
    public string AlternateExpYear;
    public string CustomNotes; // Notes related to the contract.
    // ----------------------------------------------------------------------
  }

  public class iPaymentTracker
  {
    public string DatasourceName;
    public string DatabaseName;
    public string LoginName;
    public string LoginPassword;
    public int DepFileNbr;
    public int DepFileSeq;
    public int EventNbr;
    public string SystemInterfaceID;
    public string SessionID;
    public string AcctNbr_Last4Digits;
  }

  /// <summary>
  /// This enum indicates the type of error that is returned in a generic response.
  /// </summary>
  public enum ErrorType
  {
    GatewayConnector,
    Gateway,
    Processor,
  }

  public class CardResponse
  {
    public bool GatewaySuccess = false;
    public string GatewayCode;
    public string GatewayMessage;
    public bool ProcessorApproval = false;
    public string ProcessorCode;
    public string ProcessorMessage;
    public string TransactionID;
    public string PaymentToken;        // Bug 18129 DJD
    public string PaymentTokenExpDate; // Bug 18129 DJD
    public string PaymentTokenStatus;  // Bug 18129 DJD
    public string BankAuthCode;
    public decimal PartialAuthAmount;
    public string GatewayRequestMsg;
    public string GatewayResponseMsg;
    public string AVS_Code;
    public string AVS_Msg;
    public string CVV_Code;
    public string CVV_Msg;
    public string ConnectorErrorMsg;
    public string ConnectorErrorCode;
    public string AdditionalInfoMsg;
  }

  /// <summary>
  /// This class implements credit/debit card and admin transactions for the PAYware Connect Gateway.
  /// Requests are built and sent to PAYware Connect.
  /// 
  /// NOTE: All ADMIN ADDON commands pertaining to contracts/customers must be sent to the following
  /// test URL: https://apidemo.ipcharge.net/IPCAdminAPI/RH.ipc
  /// (Integrators will be sent a production version of the above URL in a letter upon validation.)
  /// Add-on Payment Transactions (e.g., Add-on Sale, Add-on Credit, Add-on Pre-Authorization) 
  /// should be sent to the regular PAYware Connect URL.
  /// </summary>
  public class PAYwareConnectTrans
  {
    // Constructor:
    public PAYwareConnectTrans(ReqSender sender, GatewayInfo gatewayInfo, CardTransaction TransInfo, iPaymentTracker TrackerInfo)
    {
      _sender = sender;
      _gatewayInfo = gatewayInfo;
      _reqInfo = TransInfo;
      _cardAction = TransInfo.Action;
      _trackerInfo = TrackerInfo;
      _resp = new CardResponse();
    }

    public ReqSender _sender;
    public GatewayInfo _gatewayInfo;
    public CardTransaction _reqInfo;
    public CardActionEnum _cardAction;
    public iPaymentTracker _trackerInfo;
    public CardResponse _resp;

    protected CardActivityTracker _tracker;

    // These are the Error Codes used by the Gateway Connector Service (i.e., iPayment)
    public const string sCODE_UNEXPECTED_ERROR = "570";
    public const string sCODE_REQUEST_DECLINED = "580";
    public const string sCODE_CONNECTION_FAILURE = "590";
    
    // Dictionary key and results for the response to ADMIN ADDON commands
    public const string ADMIN_RESPONSE_KEY      = "AdminResponse";
    public const string ADMIN_RESP_MSG_SUCCESS  = "SUCCESS";
    public const string ADMIN_RESP_CODE_SUCCESS = "1";

    // Token Result Code     Token Result Text
    // TKN_RESULT_CODE       TKN_RESULT_TEXT
    // 0                     SUCCESS
    // 10                    MAX BATCH SIZE EXCEEDED
    // 11                    RECORD NOT FOUND
    // 12                    ILLEGAL REQUEST DATA
    // 13                    EXPIRED TOKEN
    // 15                    INVALID CREDENTIALS
    // 80                    BATCH COMPLETED WITH ERRORS
    // 99                    UNKNOWN ERROR, CHECK LOG
    // Bug 18129 DJD - Dictionary key and results for the response to UGP Token commands
    public const string TKN_RESULT_KEY = "TKN_RESULT_CODE";
    public const string TKN_RESULT_TEXT_SUCCESS = "SUCCESS";
    public const string TKN_RESULT_CODE_SUCCESS = "0";

    /// <summary>
    /// The Response Properties [tag names] returned from a PAYware Connect request.
    /// </summary>
    public enum ResponseTag
    {
      ACCT_NUM,                 // Gift Card Account Number
      AMOUNT_BALANCE,           // Gift Card Available Balance
      APPROVED_AMOUNT,          // Approved Amount: Indicates a partial authorization (partial auth)
      AUTH_CODE,                // The authorization code received from the issuing bank (Up to 16 characters).
      AUTH_RESP_CODE,           // Authorization Response Code: Populated if declined. Error code returned from network. Only returned when using the processor Global, CardNet or Paymentech.
      AVAIL_BALANCE,            // Gift Card Available Balance: Returns Gift Card balance if available from card.
      AVS_CODE,                 // The result of an AVS check
      BATCH_BALANCE,            // The amount of the settled batch segment
      BATCH_COUNT,              // The amount of transactions in the settled batch segment
      BATCH_NUM,                // The Batch Number returned by the processor.
      BATCH_SEQ_NUM,            // The Batch sequence number of the settled batch segment
      BATCH_TRACE_ID,           // Value used to trace a transaction response back to its original request.
      CB_AVAIL_BALANCE,         // Cash Benefits Available Balance. Returns Cash Benefits balance, if available, for card.
      CMRCL_TYPE,               // Commercial Card Type
      CVV2_CODE,                // The result of a CVV2 check
      DIFF_AMOUNT_DUE,          // Difference due from the customer. If processing a Gift card partial redemption, then Paymentech will approve the transaction as long as there is a balance on the card. If the card‟s available balance will not cover the TRANS_AMOUNT, Paymentech will approve the amount up to the balance on the card. APPROVED_AMOUNT contains the amount Paymentech approved for the partial redemption. If the customer owes a difference then the DIFF_AMOUNT_DUE field will contain the amount the customer owes towards the transaction.
      ERR_SEQ_NUM,              // Error Sequence Number. This value may be returned if the batch settlement fails. This value is the INTRN_SEQ_NUM of a record that is being rejected by the processor and causing the batch to fail.
      EXCEPTION_DETAIL,         // Exception Detail: If the batch contains one or more bad or corrupted transactions, this tag may be returned. This tag will contain tags that contain the INTRN_SEQ_NUM of exception transactions. Exception transactions are individual transactions that did not settle even though the batch was successfully settled.
      FS_AVAIL_BALANCE,         // Food Stamp Available Balance. Returns Food Stamp balance, if available, for card.
      INTRN_SEQ_NUM,            // Internal Sequence Number: Note: This value is assigned and returned by PAYware Connect. A unique value identifying the transaction.
      INVOICE,                  // The Invoice or Ticket number defined by the merchant. Returned for Debit. Maximum Value: 20 characters
      NET_ID,                   // The Network ID. Returned for Debit. Note: For Debit transactions, this value must be printed on the customer receipt.
      ORIG_TRANS_AMOUNT,        // Original Transaction Amount.
      PAYMENT_MEDIA,            // Code that indicates the issuer of the card. Valid Return Values: VISA, MC, AMEX, DISC, CBLN (Carte Blance), JAL, JCB, ENRT (Enroute), DCCB (Diner's Club), SWCH (Switch)
      PY_RESP_CODE,             // Response code returned by processing company.
      R_AUTH_CODE,              // Auth code returned from processor. Note: Reports Only
      REFERENCE,                // Reference code returned from processor. Note: For Debit transactions, this is the retrieval reference number. Note: Up to 16 characters may be returned.
      RESPONSE_REFERENCE,       // Reference returned from processorin the response. Note: Reports only
      RESPONSE_TEXT,            // Response text from the processor or PAYware Connect.  Transaction specific data that is returned by either the processor, bank, or PAYware Connect. Varies by transaction type and processor.
      RESULT,                   // The result of the transaction / API call
      RESULT_CODE,              // The result code of the transaction / API call
      STATUS_CODE,              // The status code of the transaction / API call.  The status code is stored in the database request table to determine the status of a transaction. It is used internally by PAYware Connect, and listed in various report types including SUMMARY, DETAIL, and TRANSACTION SEARCH. Valid STATUS_CODE values are the same as for the most common RESULT_CODE. Currently, any transaction that comes back with an error will not have a STATUS_CODE but will have a RESULT_CODE - the response field that is sent back to the client application.
      SETTLE_EXCEPTION_DETAILS, // Settlement Exception Details.  If the batch contains one or more bad or corrupted transactions, this tag may be returned. This tag will contain tags that reference the INTRN_SEQ_NUM of exception transactions and why they failed. Exception transactions are individual transactions that did not settle even though the batch was successfully settled.
      SUR_CHARGE,               // If surcharge is set up with Paymentech. It is returned in the response.
      TERMINATION_STATUS,       // The Termination Status of the transaction request
      TRACE_CODE,               // Value returned by the processor used for tracking purposes. Note: For Debit, this value must be printed on the customer receipt.
      TRANSACTION_CODE,         // Merchant specified value. This value is tracked by the processor in case it is needed for auditing purposes and for further validation of cancels. Stored by the processor. Note: Givex only
      TRANS_SEQ_FIRST,          // The sequence number of the first transaction in the settled batch segment
      TRANS_SEQ_LAST,           // The sequence number of the last transaction in the settled batch segment
      TRANS_SEQ_NUM,            // The sequence number of the transaction in the current open batch.
      TROUTD,                   // The Transaction Routing ID: The TROUTD (Transaction Routing ID) is used when performing “Follow On” transactions. The TROUTD is a PAYware Connect-assigned unique identifier that will be associated with a transaction and any subsequent transactions related to it. Maximum Value: 10 characters
      TKN_PAYMENT,              // Bug 18129 DJD - Payment Token (two-way). This is a token that can be used only by the payment gateway for future payment transactions without requiring Track Data or PAN data. Case-sensitive.
      TKN_EXPDATE,              // Bug 18129 DJD - Token expiration date
      TKN_PAYMENT_EXPDATE,      // Bug 18129 21131 DJD - eWallet: Update Card Expiration Date for PAYware Token
      CARD_TOKEN,               // Bug 18129 DJD
      TKN_MATCHING,             // Bug 18129 DJD - Matching Token (one-way). This is used for future payment purposes
      TKN_RESULT_CODE,          // Bug 18129 DJD - Token result code returned from VHT
      TKN_RESULT_TEXT,          // Bug 18129 DJD - Token result text returned from VHT
      TKN_STATUS,               // Bug 18129 DJD - Token Status. D – When a token is marked as deleted A – When a Token is active it is available S – When a Token is suspended
    }

    /// <summary>
    /// The Admin Response Properties [tag names] returned from a PAYware Connect Admin request.
    /// <AdminResponse>
    ///   <Status_Code>1</Status_Code>
    ///   <Message>SUCCESS</Message>
    /// </AdminResponse>
    /// </summary>
    public enum AdminResponseTag
    {
      Status_Code,              // The status code of the Admin transaction / API call.
      Message,                  // The text for the status of the PAYware Connect Admin request. If SUCCESS is returned, it indicates that PAYware Connect processed the request properly.
    }

    /// <summary>
    /// The TERMINATION_STATUS of the PAYware Connect request. If SUCCESS is returned, it simply 
    /// indicates that PAYware Connect processed the request properly. It DOES NOT mean that a request,
    /// transaction, settlement, etc., was accepted or closed by the processor.
    /// NOTE: If SUCCESS is returned, check the RESULT or RESULT_CODE value to determine if the transaction
    /// was approved, declined, etc.
    /// </summary>
    public enum TermStatus
    {
      SUCCESS,                             // Successful transaction request
      NOT_PROCESSED,                       // API call not successful
      NOT_PROCESSED_DB_FAILURE,            // API call not successful / Database failure
      NOT_PROCESSED_PARAMETER_ERROR,       // API call not successful / Parameter Error
      NOT_PROCESSED_COMM_FAILURE,          // API call not successful / Communication Failure
      NOT_PROCESSED_ENGINE_NOT_ACCESSIBLE, // Backend payment engine is not accessible
      INDETERMINATE_STATUS,                // API call not successful / Indeterminate Status
      PROCESSED_DB_FAILURE,                // API call successful / Database failure
      UNKNOWN,                             // Unknown error
    }

    /// <summary>
    /// The RESULT_CODE of the transaction / API call
    /// Note: This value is assigned and returned by PAYware Connect.
    /// See RESULT for descriptions of each return value.
    /// </summary>
    public enum ResultCodeEnum
    {
      XML_FORMAT_INCORRECT = -2,
      SUCCESS = -1,
      UNKNOWN = 0,
      SETTLED = 2,
      CAPTURED = 4,
      APPROVED = 5,
      DECLINED = 6,
      VOIDED = 7,
      COMPLETED = 10,
      PARTCOMP = 16,
      TIP_MODIFIED = 17,
      SETTLEMENT_SCHEDULED = 21,
      INVALID_CARD_NUMBER = 93,
      INVALID_EXPIRATION_DATE = 97,
      INVALID_AMOUNT = 1010,
      INCORRECT_USER_ID_OR_USER_PW = 3100,
      INVALID_REF_TROUTD = 3705,    // Either the TROUTD is incorrect or not eligible for performing transaction (possibly over 180days old).
      REF_TROUTD_ERROR = 3745,    // The Only COMMANDS supported by REF_TROUTD are Sale, Pre-Auth, Voice-Auth, and Post-Auth. If any other command is sent, this error will be generated.
      INCORRECT_CLIENT_ID = 2029999, // Invalid Merchant Key or Command not authorized.
      BACKEND_PAYMENT_ENGINE_NOT_ACCESSIBLE = 30000,
    }

    /// <summary>
    /// The RESULT of the PAYware Connect transaction / API call.  Note: See the RESPONSE_TEXT value for 
    /// additional information.
    /// </summary>
    public enum ResultEnum
    {
      SUCCESS,      // Successful API call
      UNKNOWN,      // Unknown result
      CAPTURED,     // The transaction was approved and captured in the batch
      APPROVED,     // The transaction was approved
      DECLINED,     // The transaction was declined. For example, if the FUNCTION_TYPE is PAYMENT and a DECLINED value is returned, the transaction request was not successful. See the RESPONSE_TEXT value that is returned for transaction information returned by the processor
      VOIDED,       // The transaction was successfully voided
      DEFERRED,     // The result of the transaction is deferred to each batch segment
      COMPLETED,    // The transaction was completed successfully and placed in the batch
      PARTCOMP,     // Partial Completion
      TIP_MODIFIED, // Tip Modification [String returned: "TIP MODIFIED"]
      ERROR,        // The ERROR value will be returned if the TERMINATION_STATUS is anything other than SUCCESS.
    }

    #region // Dictionary for AVS Codes & Descriptions
    public static Dictionary<string, string> AVS_Code = new Dictionary<string, string>()
    {
    // The result of an AVS check: Code, Description
    {"S", "Service not supported"},
    {"E", "Not a mail/phone order"},
    {"R", "Issuer system unavailable"},
    {"U", "Address unavailable"},
    {"N", "No address or zip match"},
    {"Z", "5-digit zip match only"},
    {"W", "9-digit zip match only"},
    {"A", "Address match only (If Discover with Paymentech, address and 5-digit zip match)"},
    {"Y", "Exact match, 5 digit zip (If Discover with Paymentech, address only match)"},
    {"X", "Exact match, 9 digit zip"},
    {"0", "No AVS performed"},
    {"G", "Non-US Issuer – does not participate"},
    {"B", "Intl – address match, postal format error"},
    {"C", "Intl – address and postal format errors"},
    {"D", "Intl – address and postal matches"},
    {"I", "Intl – address not verified (Visa)"},
    {"M", "Intl – address and postal matches"},
    {"P", "Intl – postal match, address format error"},
    {"T", "9-character zip match only (Discover)"},
    };
    #endregion

    #region // Dictionary for CVV Codes & Descriptions
    public static Dictionary<string, string> CVV_Code = new Dictionary<string, string>()
    {
    // The result of an CVV check: Code, Description
    {"U", "Issuer not certified and/or has not provided encryption keys"},
    {"S", "Merchant indicated that CVV2 was not present on card"},
    {"P", "Not processed"},
    {"N", "CVV2 No Match"},
    {"M", "CVV2 Match"},
    {"X", "No response"},
    };
    #endregion

    #region // Dictionary for mapping Payment Type or Request Type to the Card Action
    public static Dictionary<CardActionEnum, string> Action_PmtTypeOrReqType = new Dictionary<CardActionEnum, string>()
        {
          {CardActionEnum.None, ""},
          {CardActionEnum.CreditSale, "CREDIT"},
          {CardActionEnum.CreditPreAuth, "CREDIT"},
          {CardActionEnum.CreditCompletion, "CREDIT"},
          {CardActionEnum.CreditVoid, "CREDIT"},
          {CardActionEnum.CreditReturn, "CREDIT"},
          {CardActionEnum.TokenQuery, "TOKEN"},  // Bug 18129 DJD
          {CardActionEnum.TokenUpdate, "TOKEN"}, // Bug 18129 DJD
          {CardActionEnum.DebitSale, "DEBIT"},
          {CardActionEnum.DebitVoid, "DEBIT"},
          {CardActionEnum.DebitReturn, "DEBIT"},
          {CardActionEnum.ActivateContract, "IPCRB_ADMIN"},
          {CardActionEnum.CancelContract, "IPCRB_ADMIN"},
          {CardActionEnum.AddContractToCustomer, "IPCRB_ADMIN"},
          {CardActionEnum.NewCustomer, "IPCRB_ADMIN"},
          {CardActionEnum.NewCustomerWithContract, "IPCRB_ADMIN"},
          {CardActionEnum.UpdateContract, "IPCRB_ADMIN"},
          {CardActionEnum.UpdateCustomer, "IPCRB_ADMIN"}, 
        };
    #endregion

    #region // Dictionary for mapping Function Type to the Card Action
    public static Dictionary<CardActionEnum, string> Action_FunctionType = new Dictionary<CardActionEnum, string>()
        {
          {CardActionEnum.None, ""},
          {CardActionEnum.CreditSale, "PAYMENT"},
          {CardActionEnum.CreditPreAuth, "PAYMENT"},
          {CardActionEnum.CreditCompletion, "PAYMENT"},
          {CardActionEnum.CreditVoid, "PAYMENT"},
          {CardActionEnum.CreditReturn, "PAYMENT"},
          {CardActionEnum.DebitSale, "PAYMENT"},
          {CardActionEnum.DebitVoid, "PAYMENT"},
          {CardActionEnum.DebitReturn, "PAYMENT"},
          {CardActionEnum.TokenQuery, "PAYMENT"},  // Bug 18129 DJD
          {CardActionEnum.TokenUpdate, "PAYMENT"}, // Bug 18129 DJD
          {CardActionEnum.ActivateContract, "ADDON"},
          {CardActionEnum.CancelContract, "ADDON"},
          {CardActionEnum.AddContractToCustomer, "ADDON"},
          {CardActionEnum.NewCustomer, "ADDON"},
          {CardActionEnum.NewCustomerWithContract, "ADDON"},
          {CardActionEnum.UpdateContract, "ADDON"},
          {CardActionEnum.UpdateCustomer, "ADDON"}, 
        };
    #endregion

    #region // Dictionary for mapping Commands (or Command Types) to Card Actions
    public static Dictionary<CardActionEnum, string> Action_CommandType = new Dictionary<CardActionEnum, string>()
        {
          {CardActionEnum.None, ""},
          {CardActionEnum.CreditSale, "SALE"},
          {CardActionEnum.CreditPreAuth, "PRE_AUTH"},
          {CardActionEnum.CreditCompletion, "COMPLETION"},
          {CardActionEnum.CreditVoid, "VOID"},
          {CardActionEnum.CreditReturn, "CREDIT"},
          {CardActionEnum.DebitSale, "SALE"},
          {CardActionEnum.DebitVoid, "VOID"},
          {CardActionEnum.DebitReturn, "CREDIT"},
          {CardActionEnum.TokenQuery, "QUERY"},   // Bug 18129 DJD
          {CardActionEnum.TokenUpdate, "UPDATE"}, // Bug 18129 DJD
          {CardActionEnum.ActivateContract, "ADDON_ACTIVATE_CONTRACT"},
          {CardActionEnum.CancelContract, "ADDON_CANCEL_CONTRACT"},
          {CardActionEnum.AddContractToCustomer, "ADDON_NEW_CONTRACT"},
          {CardActionEnum.NewCustomer, "ADDON_NEW_CUSTOMER"},
          {CardActionEnum.NewCustomerWithContract, "ADDON_NEW_CUSTOMER_CONTRACT"},
          {CardActionEnum.UpdateContract, "ADDON_UPDATE_CONTRACT"},
          {CardActionEnum.UpdateCustomer, "ADDON_UPDATE_CUSTOMER"}, 
        };
    #endregion

    public bool BuildSendRequest()
    {
      // Maximum number of attempts (3) using host and alternate hosts
      const int nREQUEST_URI_ATTEMPTS = 3;
      const int nADMIN_REQUEST_URI_ATTEMPTS = 1;
      int requestAttempts = IsAdminRequest() ? nADMIN_REQUEST_URI_ATTEMPTS : nREQUEST_URI_ATTEMPTS;

      try
      {
        string reqXML = string.Empty;
        string respXML = string.Empty;
        int altURL = 0; // 0=Primary Host, 1=AltHost1, 2=AltHost2, 3=Stop retrying
        // IMPORTANT: "SendRequest" method increments the altURL value.

        // Build the card transaction XML
        if (BuildRequest(ref reqXML))
        {
          // PAYware Connect Request successfully built
          string reqMaskedXML = reqXML;
          // Mask the sensitive data (Bug 18912 Added DEVICEKEY, USERID and USERPWD)
          //string[] tags = { "CLIENT_ID", "USERID", "USER_ID", "USERPWD", "USER_PW", "MERCHANTKEY", "DEVICEKEY", "ACCT_NUM", "TRACK_DATA", "EXP_MONTH", "EXP_YEAR", "CVV2", "TKN_PAYMENT_EXPDATE" };
          List<string> tags = new List<string>(new string[] { "CLIENT_ID", "USERID", "USER_ID", "USERPWD", "USER_PW", "MERCHANTKEY", "DEVICEKEY", "ACCT_NUM", "TRACK_DATA", "EXP_MONTH", "EXP_YEAR", "CVV2", "TKN_PAYMENT_EXPDATE" });
#if !DEBUG
          //pci.MaskTags(ref reqMaskedXML, tags);
          CStandardSystemInterface.MaskTags(ref reqMaskedXML, tags);
#endif
          _resp.GatewayRequestMsg = reqMaskedXML;
          // ***** LOG THE REQUEST *****
          Logger.cs_log(string.Format("{0}{1}{0}{2}{0}{3}{0}"
                , Environment.NewLine                                                                                 // {0}
                , "================================   PAYware Connect:  REQUEST    =================================" // {1}                                                                                             // {1}
                , reqMaskedXML                                                                                        // {2}
                , "=================================================================================================" // {3}
                ));

          // Send (or re-send with alt host) the card transaction request to PAYware Connect
          int whileCount = 0;
          while (altURL < requestAttempts)
          {
            // Record "STARTED" Tracker entry before sending request to gateway
            if (_tracker != null)
            {
              if (!_tracker.StartRequest(reqMaskedXML))
              {
                //return _Resp; /***************************************************************************/
              }
            }

            if (SendRequest(ref reqXML, ref respXML, ref altURL))
            {
              // PAYware Connect Response successfully received
              /***************************************************************************/
              //_resp.GatewayResponseMsg = string.Format("\r\n<![CDATA[\r\n{0}\r\n]]>\r\n  ", respXML);
              // ***** LOG THE RESPONSE *****
              _resp.GatewayResponseMsg = respXML;

             Logger.cs_log(string.Format("{0}{1}{0}{2}{0}{3}{0}"
                    , Environment.NewLine                                                                                 // {0}
                    , "================================    PAYware Connect: RESPONSE    ================================" // {1}                                                                                             // {1}
                    , respXML                                                                                             // {2}
                    , "=================================================================================================" // {3}
                    ));

              // Set PAYware Connect response XML key-value pairs to dictionary
              Dictionary<string, string> dicResp = null;
              if (ResponseToDictionary(ref respXML, ref dicResp))
              { // Evaluate and convert the PAYware Connect response
                ConvertResponse(ref dicResp); // Just return response [with the error message]
              }
              // else Just return response [with the error message]

              // Record results in Tracker after sending request to gateway
              if (_tracker != null)
              {
                if (!_tracker.RecordRequestResults())
                {
                  // Uncertain what to do if tracker fails after request is sent
                }
              }
              break; // Break out of "Try Alternate URL" loop (and return Response)
            }
            else // Record results to tracker after EACH failed attempt to send the request
            {
              // Record results in Tracker after sending request to gateway
              if (_tracker != null)
              {
                if (!_tracker.RecordRequestResults())
                {
                  // Uncertain what to do if tracker fails after request is sent
                }
              }
            }
            altURL = whileCount++ > requestAttempts ? requestAttempts : altURL; // Make certain there's no "infinite loop"
          }
        }
        else
        {
          string err = "Error While Building PAYware Connect Request";
          SetRespError(ErrorType.GatewayConnector, "PWC-0001", err);
          return false;
        }
      }
      catch (Exception ex)
      {
        string err = "Error While Building And Sending PAYware Connect Request";
        SetRespError(ErrorType.GatewayConnector, "PWC-0002", err, ex);
        LogError(ex, err);
        return false;
      }

      return true;
    }

    /// <summary>
    /// Records an error to the cs log flat file.
    /// </summary>
    /// <param name="ex"></param>
    /// <param name="strErrorDescription"></param>
    /// <param name="bProjectLevel"></param>
    public void LogError(Exception ex, string strErrorDescription)
    {
      string strErrorInfo = "";
      string strExceptionInfo = ex == null ? "" : string.Format("{0}{1}", ex.ToMessageAndCompleteStacktrace(), Environment.NewLine);
      if (!string.IsNullOrEmpty(strErrorDescription))
      {
        strErrorInfo = ex == null ? string.Format("{0}{1}", strErrorDescription, Environment.NewLine) :
                                    string.Format("Error Info    : {0}{1}", strErrorDescription, Environment.NewLine);
      }
     Logger.cs_log(string.Format("{0}{1}{0}{2}{3}{4}{0}"
      , Environment.NewLine                                                                                 // {0}
      , "================================= PAYware Connect:  Trans ERROR =================================" // {1}                                                                                             // {1}
      , strErrorInfo                                                                                        // {2}
      , strExceptionInfo                                                                                    // {3}
      , "=================================================================================================" // {4}
      ));

      // Write error to the Activity Log
      //string sAction = string.Format("Batch Update Error - {0} Level", bProjectLevel ? "Project" : "Product");
      //WriteToActivityLog(sAction, strErrorDescription, ex);
    }

    /// <summary>
    /// The "ConvertResponse" method will populate fields in the member 
    /// variable "_Resp" (generic response) with the values returned from
    /// the PAYware Connect response (pwcResp).
    /// </summary>
    /// <param name="pwcResp"></param>
    /// <returns></returns>
    private bool ConvertResponse(ref Dictionary<string, string> pwcResp)
    {
      try
      {
        // Check if Admin Request: Only requires a "Gateway" status (SUCCESS/FAIL) returned (no processor involved).
        if (pwcResp.ContainsKey(ADMIN_RESPONSE_KEY))
        {
          _resp.GatewaySuccess = pwcResp[AdminResponseTag.Message.ToString()].Equals(ADMIN_RESP_MSG_SUCCESS)
            && pwcResp[AdminResponseTag.Status_Code.ToString()].Equals(ADMIN_RESP_CODE_SUCCESS);
          _resp.GatewayCode = pwcResp[AdminResponseTag.Status_Code.ToString()];
          _resp.GatewayMessage = pwcResp[AdminResponseTag.Message.ToString()];
          return true;
        }

        //Bug 18129 DJD - Check if Query Token Request:
        if (pwcResp.ContainsKey(TKN_RESULT_KEY))
        {
            _resp.GatewaySuccess = pwcResp[ResponseTag.TKN_RESULT_CODE.ToString()].Equals(TKN_RESULT_CODE_SUCCESS);
            _resp.GatewayCode = pwcResp[ResponseTag.TKN_RESULT_CODE.ToString()];
            _resp.GatewayMessage = pwcResp[ResponseTag.TKN_RESULT_TEXT.ToString()];
        }

        // Check the success of PAYware Connect processing the request.
        if (pwcResp.ContainsKey(ResponseTag.TERMINATION_STATUS.ToString()))
        {
          _resp.GatewaySuccess = pwcResp[ResponseTag.TERMINATION_STATUS.ToString()].Equals(TermStatus.SUCCESS.ToString());
          _resp.GatewayCode = pwcResp[ResponseTag.TERMINATION_STATUS.ToString()];
          _resp.GatewayMessage = pwcResp[ResponseTag.TERMINATION_STATUS.ToString()].Replace("_", " ");
        }

        // Check if the Processor approved the response
        if (pwcResp.ContainsKey(ResponseTag.RESULT.ToString()))
        {
          string DesiredResult = "";

          switch (_cardAction)
          {
            case CardActionEnum.CreditSale:
            case CardActionEnum.DebitSale:
              DesiredResult = ResultEnum.CAPTURED.ToString();
              break;

            case CardActionEnum.CreditVoid:
            case CardActionEnum.DebitVoid:
              DesiredResult = ResultEnum.VOIDED.ToString();
              break;

            case CardActionEnum.CreditReturn:
              DesiredResult = ResultEnum.CAPTURED.ToString();
              break;
          }

          _resp.ProcessorApproval = pwcResp[ResponseTag.RESULT.ToString()].Equals(DesiredResult);

          _resp.ProcessorCode = pwcResp.ContainsKey(ResponseTag.RESULT_CODE.ToString()) ?
              string.Format("{0}: {1}"
              , pwcResp[ResponseTag.RESULT_CODE.ToString()]
              , pwcResp[ResponseTag.RESULT.ToString()]) :
              pwcResp[ResponseTag.RESULT.ToString()];

          _resp.ProcessorMessage = pwcResp.ContainsKey(ResponseTag.RESPONSE_TEXT.ToString()) ?
              pwcResp[ResponseTag.RESPONSE_TEXT.ToString()] :
              pwcResp[ResponseTag.RESULT.ToString()];
        }

        // Get the TROUTD (Transaction ID)
        if (pwcResp.ContainsKey(ResponseTag.TROUTD.ToString()))
        {
          _resp.TransactionID = pwcResp[ResponseTag.TROUTD.ToString()];
        }

        // Bug 18129 DJD - Get the Payment Token
        if (pwcResp.ContainsKey(ResponseTag.TKN_PAYMENT.ToString()))
        {
            _resp.PaymentToken = pwcResp[ResponseTag.TKN_PAYMENT.ToString()];
        }

        // Bug 18129 DJD - Get the Payment Token Expiration Date
        if (pwcResp.ContainsKey(ResponseTag.TKN_EXPDATE.ToString()))
        {
            _resp.PaymentTokenExpDate = pwcResp[ResponseTag.TKN_EXPDATE.ToString()];
        }

        // Bug 18129 DJD - Get the Payment Token Expiration Date
        if (pwcResp.ContainsKey(ResponseTag.TKN_STATUS.ToString()))
        {
            _resp.PaymentTokenStatus = pwcResp[ResponseTag.TKN_STATUS.ToString()];
        }

        // Get the Bank Auth Code
        if (pwcResp.ContainsKey(ResponseTag.AUTH_CODE.ToString()))
        {
          _resp.BankAuthCode = pwcResp[ResponseTag.AUTH_CODE.ToString()];
        }

        // Get the partial auth amount
        if (pwcResp.ContainsKey(ResponseTag.APPROVED_AMOUNT.ToString()))
        {
          _resp.PartialAuthAmount =
              System.Convert.ToDecimal(pwcResp[ResponseTag.APPROVED_AMOUNT.ToString()]);
        }

        // Get the AVS Code and Description
        if (pwcResp.ContainsKey(ResponseTag.AVS_CODE.ToString()))
        {
          _resp.AVS_Code = pwcResp[ResponseTag.AVS_CODE.ToString()];
          if (AVS_Code.ContainsKey(_resp.AVS_Code))
          {
            _resp.AVS_Msg = AVS_Code[_resp.AVS_Code];
          }
        }

        // Get the CVV Code and Description
        if (pwcResp.ContainsKey(ResponseTag.CVV2_CODE.ToString()))
        {
          _resp.CVV_Code = pwcResp[ResponseTag.CVV2_CODE.ToString()];
          if (CVV_Code.ContainsKey(_resp.CVV_Code))
          {
            _resp.CVV_Msg = CVV_Code[_resp.CVV_Code];
          }
        }
      }
      catch (Exception ex)
      {
        string err = "Error Converting PAYware Connect Response";
        SetRespError(ErrorType.GatewayConnector, "PWC-0003", err, ex);
        LogError(ex, err);
        return false;
      }

      return true;
    }

    /// <summary>
    /// Moves the response XML key-value pairs to a dictionary
    /// so that the response values are easier to work with.
    /// </summary>
    /// <param name="sRespXml"></param>
    /// <param name="dic"></param>
    /// <returns></returns>
    private bool ResponseToDictionary(ref string sRespXml, ref Dictionary<string, string> responseDictionary)
    {
      #region RESPONSE XML Example Comment
      // The PAYware Connect transaction response will be placed within the RESPONSE tag. The
      // transaction response will consist of multiple XML tags and values. For example:
      // 
      // <RESPONSE>
      //   <AUTH_AMOUNT>1.00</AUTH_AMOUNT>
      //   <AUTH_CODE>OK3928</AUTH_CODE>
      //   <CLIENT_ID>3349300010001</CLIENT_ID>
      //   <COMMAND>SALE</COMMAND>
      //   <CTROUTD>127</CTROUTD>
      //   <CVV2_CODE>X</CVV2_CODE>
      //   <INTRN_SEQ_NUM>3920749</INTRN_SEQ_NUM>
      //   <INVOICE>123456</INVOICE>
      //   <PAYMENT_MEDIA>VISA</PAYMENT_MEDIA>
      //   <PAYMENT_TYPE>CREDIT</PAYMENT_TYPE>
      //   <REFERENCE>000000000127</REFERENCE>
      //   <RESPONSE_TEXT>APPROVAL        : 00 </RESPONSE_TEXT>
      //   <RESULT>CAPTURED</RESULT>
      //   <RESULT_CODE>4</RESULT_CODE>
      //   <TERMINATION_STATUS>SUCCESS</TERMINATION_STATUS>
      //   <TRACE_CODE>000127</TRACE_CODE>
      //   <TRANS_AMOUNT>1.00</TRANS_AMOUNT>
      //   <TRANS_DATE>2013.01.23</TRANS_DATE>
      //   <TRANS_SEQ_NUM>127</TRANS_SEQ_NUM>
      //   <TRANS_TIME>14:31:41</TRANS_TIME>
      //   <TROUTD>3920749</TROUTD>
      //   <LPTOKEN>1</LPTOKEN>
      // </RESPONSE>
      //
      // Admin Response example:
      //
      // <AdminResponse>
      //   <Status_Code>1</Status_Code>
      //   <Message>SUCCESS</Message>
      // </AdminResponse>
      //
      #endregion RESPONSE XML Example Comment

      string err = "";
      bool retVal = RespToDic(ref sRespXml, ref responseDictionary, ref err);
      if (!retVal && !string.IsNullOrEmpty(err))
      {
        LogError(null, err);
      }
      return retVal;
    }

    /// <summary>
    /// Static method that moves the response XML key-value pairs to a dictionary
    /// so that the response values are easier to work with.
    /// </summary>
    /// <param name="sRespXml"></param>
    /// <param name="responseDictionary"></param>
    /// <returns></returns>
    public static bool RespToDic(ref string sRespXml, ref Dictionary<string, string> responseDictionary, ref string err)
    {
      try
      {
        // Start with newly created or cleared dictionary
        if (responseDictionary == null)
        {
          responseDictionary = new Dictionary<string, string>();
        }
        else
        {
          responseDictionary.Clear();
        }
        bool bAdminResponse = false;

        // Create an XmlReader
        using (XmlReader reader = XmlReader.Create(new StringReader(sRespXml)))
        {
          XmlDocument xmlDoc = new XmlDocument();
          xmlDoc.Load(reader); // Load the file into the XmlDocument
          foreach (XmlNode rootNode in xmlDoc.ChildNodes)
          {
            if (rootNode.Name == ADMIN_RESPONSE_KEY)
            {
              bAdminResponse = true;
              responseDictionary[ADMIN_RESPONSE_KEY] = "true";
            }
            // Bug 18129 DJD - Added UGPRESPONSE
            if (rootNode.Name == "RESPONSE" || rootNode.Name == "UGPRESPONSE" || bAdminResponse)
            {
              foreach (XmlNode currentNode in rootNode.ChildNodes)
              {
                responseDictionary[currentNode.Name] = currentNode.InnerText;
              }
            }
          }
        }
      }
      catch (Exception e)
      {
        err = string.Format("Error Parsing PAYware Connect Response XML: {0}", e.ToMessageAndCompleteStacktrace());
        return false;
      }
      return true;
    }

    /// <summary>
    /// The SendRequest method builds and sends an Http Web Request based on the
    /// PAYware Connect formatted request sent to it.  If sending the request fails,
    /// it will increment the Alternate Host (iAltHost) value if there are alternate
    /// host values and the error conditions warrent it.  If sending the request succeeds,
    /// the response is written to refence arguement "sRespXml".
    /// 
    /// PRECONDITION: The "sReqXml" argument must be populated with a valid PAYware 
    /// Connect formatted request.
    /// </summary>
    private bool SendRequest(ref string sReqXml, ref string sRespXml, ref int iAltHost)
    {
      //Stream newStream = null;
      //HttpWebResponse response = null;
      //Stream receiveStream = null;
      //StreamReader readStream = null;

      string URL = string.Empty;
      sRespXml   = string.Empty;

      try
      {
        // Select the Host (Primary, Alternate1, or Alternate2)
        switch (iAltHost)
        {
          case 0:
            URL = _gatewayInfo.Host;
            break;
          case 1:
            URL = _gatewayInfo.AlternateHost1;
            LogError(null, string.Format("{0}{1}", "Calling PAYware Connect with Host failed; ", ("Retrying with Alt Host 1 URL: " + URL)));
            break;
          case 2:
            URL = _gatewayInfo.AlternateHost2;
            LogError(null, string.Format("{0}{1}", "Calling PAYware Connect with Alt Host 1 failed; ", ("Retrying with Alt Host 2 URL: " + URL)));
            break;
          default:
            break;
        }

        if (string.IsNullOrEmpty(URL))
        {
          // Set Error Message
          LogError(null, string.Format("PAYware Connect Host ({0}) value is missing.", 
            iAltHost == 0 ? "Primary" : string.Format("Alternate {0}", iAltHost)));
          return false;
        }

        /*
        // Build Web Request
        HttpWebRequest wrReq = (HttpWebRequest)WebRequest.Create(URL);
        wrReq.Method = "POST";
        ASCIIEncoding encoding = new ASCIIEncoding();
        byte[] byte1 = encoding.GetBytes(sReqXml);
        wrReq.ContentType = "text/xml";
        wrReq.ContentLength = sReqXml.Length;
        newStream = wrReq.GetRequestStream();
        newStream.Write(byte1, 0, byte1.Length);
        response = (HttpWebResponse)wrReq.GetResponse();
        receiveStream = response.GetResponseStream();
        readStream = new StreamReader(receiveStream, Encoding.UTF8);
        sRespXml += readStream.ReadToEnd();
        */

        SendHttpRequest(URL, sReqXml, ref sRespXml, true);
      }
      catch (Exception ex)
      {
        string sErr = ex.Message;
        bool retry = false;

        // Check if alternate host(s) were sent with request.
        switch (++iAltHost) // Increment alt host #
        {
          case 1:
            retry = !string.IsNullOrEmpty(_gatewayInfo.AlternateHost1);
            break;
          case 2:
            retry = !string.IsNullOrEmpty(_gatewayInfo.AlternateHost2);
            break;
          default:
            break;
        }

        // Check error conditions to retry the connection.
        if (retry)
        {
          retry = (sErr.IndexOf("connection was closed") != -1 ||
                   sErr.IndexOf("could not be resolved") != -1 ||
                   ex is System.Net.WebException); // Retry on any WebException
        }

        if (!retry) // Do NOT retry with an Alternate Host: Set the Error
        {
          iAltHost = 3;
          // Set Error Message
          SetRespError(ErrorType.GatewayConnector, "PWC-0004", "Unable Send Request To PAYware Connect:", ex);
          LogError(ex, "Unable To Send Request To PAYware Connect");
        }

        return false;
      }
      finally
      {
        /*
        if (newStream != null)     { newStream.Close(); }
        if (response != null)      { response.Close(); }
        if (receiveStream != null) { receiveStream.Close(); }
        if (readStream != null)    { readStream.Close(); }
        */
      }
      return true;
    }

    public bool SendHttpRequest(string uri, string reqXml, ref string respXml, bool rethrowErr)
    {
      try
      {
        const string METHOD = "POST";
        const string CONTENT_TYPE = "text/xml";
        const string USER_AGENT = "corebt.com CASL";

        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
        request.Method = METHOD;
        request.ContentType = CONTENT_TYPE;
        request.UserAgent = USER_AGENT;
        request.AllowAutoRedirect = false;
        request.Timeout = _gatewayInfo.Timeout;

        UTF8Encoding encoding = new UTF8Encoding();
        byte[] bytes = _reqInfo.secureData == null ?
          encoding.GetBytes(reqXml) :
          ReplacePlaceholderWithSecureData(reqXml, _reqInfo.secureData, _reqInfo.secureDataPlaceHolder);

        request.ContentLength = bytes.Length;
        using (Stream writeStream = request.GetRequestStream())
        {
          writeStream.Write(bytes, 0, bytes.Length);
        }
        //string resp = "";
        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        {
          using (Stream responseStream = response.GetResponseStream())
          {
            using (StreamReader readStream = new StreamReader(responseStream, Encoding.UTF8))
            {
              respXml = readStream.ReadToEnd();
            }
          }
        }
      }
      catch (Exception ex)
      {
        // Log the error [NOTE: Specific gateway implementation SHOULD make sure to catch all the errors when rethrowErr is true.]
        LogError(ex, "Error Sending Http Request to PAYware Connect");
        if (rethrowErr)
        {
          throw;
        }
        else
        {
          return false;
        }
      }
      return true;
    }

    /// <summary>
    /// Replace the placeholder without using string objects. This will prevent copying strings in memory.
    /// Return value is byte array.
    /// </summary>
    /// <param name="xml"></param>
    /// <param name="secureData"></param>
    /// <param name="secureDataByteEntropy"></param>
    /// <param name="secureDataPlaceholder"></param>
    /// <returns></returns>
    public static unsafe byte[] ReplacePlaceholderWithSecureData(string xml, SecureString secStr, string secStrPlaceholder)
    {
      //byte[] returnByteData = null;
      byte[] byteData = null;
      int iFullLength = 0;
      int iStartOfPlaceholder = xml.IndexOf(secStrPlaceholder); // "pci.SecureStringClass"

      if (iStartOfPlaceholder == -1) // Nothing to replace: just return XML in byte array
      {
        byteData = new byte[xml.Length];
        for (int n = 0; n < xml.Length; n++)
        {
          byteData[n] = (byte)xml[n];
        }
        
        // string s = ASCIIEncoding.ASCII.GetString(byteData);

        return byteData;
      }

      int iEndOfPlaceholder = iStartOfPlaceholder + secStrPlaceholder.Length;
      IntPtr unmanagedRef = IntPtr.Zero;

      try
      {
        unmanagedRef = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(secStr);
        char* p = (char*)unmanagedRef;
        string sPart1 = xml.Substring(0, iStartOfPlaceholder);
        string sPart2 = xml.Substring(iEndOfPlaceholder);
        iFullLength = sPart1.Length + secStr.Length + sPart2.Length;
        byteData = new byte[iFullLength];
        int j = 0;
        for (j = 0; j < sPart1.Length; j++)
          byteData[j] = (byte)sPart1[j];
        for (int n = 0; n < secStr.Length; n++)
          byteData[j++] = (byte)*p++;
        for (int n = 0; n < sPart2.Length; n++)
          byteData[j++] = (byte)sPart2[n];

        // Testing data: 
        string s = ASCIIEncoding.ASCII.GetString(byteData);

        //returnByteData = ProtectedData.Protect(byteData, secStrByteEntropy, DataProtectionScope.CurrentUser);
      }
      catch (Exception e)
      {
        throw new InvalidOperationException(e.ToString());
      }
      finally
      {
        if (unmanagedRef != IntPtr.Zero)
        {
          System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(unmanagedRef);
        }
      }
      //return ProtectedData.Unprotect(returnByteData, secStrByteEntropy, DataProtectionScope.CurrentUser);
      return byteData;
    }

    /// <summary>
    /// The BuildRequest method converts the "generic" request into a PAYware Connect
    /// formatted request.  Returns an XML string for the request.
    /// 
    /// PRECONDITION: The "_Req" member variable must be populated with a valid "generic"
    /// request.
    /// 
    /// For example, the XML for a Credit Sale request may look like this:
    /// <TRANSACTION>
    /// <CLIENT_ID>100010001</CLIENT_ID>
    /// <USER_ID>APIUser</USER_ID>
    /// <USER_PW>APIPassword</USER_PW>
    /// <MERCHANTKEY>123456789</MERCHANTKEY>
    /// <FUNCTION_TYPE>PAYMENT</FUNCTION_TYPE>
    /// <COMMAND>SALE</COMMAND>
    /// <PAYMENT_TYPE>CREDIT</PAYMENT_TYPE>
    /// <ACCT_NUM>4005550000000019</ACCT_NUM>
    /// <PRESENT_FLAG>3</PRESENT_FLAG>
    /// <TRACK_DATA>4005550000000019=07121011000012345678</TRACK_DATA>
    /// <EXP_MONTH>12</EXP_MONTH>
    /// <EXP_YEAR>07</EXP_YEAR>
    /// <TRANS_AMOUNT>1.00</TRANS_AMOUNT>
    /// <INVOICE>123456</INVOICE>
    /// </TRANSACTION>
    /// 
    /// </summary>
    /// <param name="sXml">The XML for the card request</param>
    /// <returns>True if request is successfully built; False otherwise</returns>
    public bool BuildRequest(ref string sXml)
    {
      try
      {
        // Check if the action is for an Admin Request
        if (IsAdminRequest())
        {
          return BuildAdminRequest(ref sXml);
        }

        sXml = string.Empty;
        TRANSACTION PC_Req = new TRANSACTION();

        // Firstly, set values for Authentication:
        PC_Req.CLIENT_ID = _gatewayInfo.ClientID;

        // Bug 18129 DJD - Client ID Components for UGP Protocol [Account, Site, Term]
        PC_Req.ACCOUNT = _gatewayInfo.Account;
        PC_Req.SITE = _gatewayInfo.Site;
        PC_Req.TERM = _gatewayInfo.Term;

        // Merchant Key Authentication:
        PC_Req.MERCHANTKEY = _gatewayInfo.MerchantKey;
        PC_Req.USER_ID = _gatewayInfo.User;
        PC_Req.USER_PW = _gatewayInfo.Password;

        // Managed Device Authentication (NOTE: Currently, this authentication is used exclusively for the Vx805 device):
        PC_Req.SERIAL_NUM = _gatewayInfo.SerialNumber;
        PC_Req.DEVTYPE = _gatewayInfo.DeviceType;
        PC_Req.DEVICEKEY = _gatewayInfo.DeviceKey;

        // Secondly, set values for transaction type
        PC_Req.FUNCTION_TYPE = Action_FunctionType[_reqInfo.Action];
        PC_Req.PAYMENT_TYPE = Action_PmtTypeOrReqType[_reqInfo.Action];
        PC_Req.COMMAND = Action_CommandType[_reqInfo.Action];

        // Thirdly, set values for transaction details
        PC_Req.TROUTD = _reqInfo.TransactionID;

        // NOTE: PAYware sends the 16 character key serial number and Pin block separately.
        PC_Req.PIN_BLOCK = _reqInfo.PinBlock;
        PC_Req.KEY_SERIAL_NUMBER = _reqInfo.KeySerialNbr;

        PC_Req.ACCT_NUM = _reqInfo.AccountNumber;
        PC_Req.CVV2 = _reqInfo.CVV;
        PC_Req.EXP_MONTH = _reqInfo.ExpMonth;
        PC_Req.EXP_YEAR = _reqInfo.ExpYear;
        PC_Req.EXP_DATE  = _reqInfo.ExpDate; // Bug 24436 DJD: Valid only for UGP requests when generating a payment token Ex: 1216
        if (_reqInfo.TransactionAmount.HasValue)
        {
          PC_Req.TRANS_AMOUNT = string.Format("{0:N2}", _reqInfo.TransactionAmount);
        }

        // ----------------------------------------------------------------------
        // Add-On transactions are transactions performed on previously stored 
        // customer credit cards using only the customer records. These transactions
        // can be processed without passing credit card numbers. These transactions
        // are performed on add-on contracts.
        if (!string.IsNullOrEmpty(_reqInfo.MerchantCustomerID) &&
            !string.IsNullOrEmpty(_reqInfo.MerchantContractID)) 
        {
          PC_Req.RBCUSTOMER_ID = _reqInfo.MerchantCustomerID;
          PC_Req.RBCONTRACT_ID = _reqInfo.MerchantContractID;
          // Account to utilize: “P” for Primary or “A” for Alternate Account Number.
          if (_reqInfo.PrimaryAccount.HasValue)
          {
            PC_Req.RBACCOUNT = (bool)_reqInfo.PrimaryAccount ? "P" : "A";
          }
          // Y – send notification e-mail, N – do not send notification e-mail
          if (_reqInfo.SendEmail.HasValue)
          {
            PC_Req.RBEMAIL = (bool)_reqInfo.SendEmail ? "Y" : "N";
          }
        }
        // ----------------------------------------------------------------------

        // ----------------------------------------------------------------------
        // Bug 18129 DJD - Query Token trans on existing Payment Token (Renew Expiration Date)
        if (!string.IsNullOrEmpty(_reqInfo.Token))
        {
            PC_Req.TKN_PAYMENT = _reqInfo.Token;
            if (_reqInfo.TokenRenewal.HasValue)
            {
                if ((bool)_reqInfo.TokenRenewal)
                {
              PC_Req.TKN_RENEW = "1";        // Renew the token
              if (_reqInfo.TokenRenewalDays > 0)
              {
                PC_Req.TKN_RENEW_DAYS = _reqInfo.TokenRenewalDays.ToString(); // Renew the token for configured number of days (e.g., 730 is 2 years)
                }
            }
        }

          // BEGIN Bug 18129 21131 DJD - eWallet: Update Card Expiration Date for PAYware Token
          // NOTE: It's uncertain from the PAYware documentation if "TKN_PAYMENT_EXPDATE" should be sent or "EXP_MONTH" and "EXP_YEAR" - so both will be sent.
          if (_reqInfo.Action == CardActionEnum.TokenUpdate && !string.IsNullOrEmpty(PC_Req.EXP_MONTH) && !string.IsNullOrEmpty(PC_Req.EXP_YEAR))
          {
            PC_Req.TKN_PAYMENT_EXPDATE = string.Format("{0}{1}", PC_Req.EXP_MONTH, PC_Req.EXP_YEAR);
          }
          // END Bug 18129 21131 DJD - eWallet: Update Card Expiration Date for PAYware Token
        }

        // BEGIN Bug 25177 - DJD: eWallet Zip Handling
        if (!string.IsNullOrEmpty(_reqInfo.TokenAddress))
        {
          PC_Req.TKN_CADDR = _reqInfo.TokenAddress;
        }
        if (!string.IsNullOrEmpty(_reqInfo.TokenZip))
        {
          PC_Req.TKN_CPCODE = _reqInfo.TokenZip;
        }
        // END Bug 25177
        // ----------------------------------------------------------------------

        // Bug 26819 DJD - eWallet TOKEN EXPIRED - Need to send extend token expiration date call to Payware after each use
        if (!string.IsNullOrEmpty(_reqInfo.TokenExpDate))
        {
          PC_Req.TKN_EXPDATE = _reqInfo.TokenExpDate;
        }

        // Bug 18129 DJD - Valid values:
        // 0 – Tokenization is not used
        // 1 – Create Matching Token (one-way) Only
        // 2 – Create Payment Token (two-way) and Matching Token (one-way)
        if (PC_Req.COMMAND == "QUERY" && string.IsNullOrEmpty(_reqInfo.Token)) // Token Query
        {
            PC_Req.TKN_PROCESS = "2"; // Create Payment Token (two-way) if it does not already exist
        }
        
        //string.Format("{0:N2}", _transInfo.ConvFeeAmount);

        // PRESENT_FLAG is optional; It is highly recommended to pass a Present Flag for all Business types.
        // For retail and restaurant, you must pass a present flag value of either 2 for manually entered or 3 for swiped.
        // Valid Values: 1 - Card not present, 2 - Card present, 3 - Card swiped, 4 - Passed by RFID (Radio Frequency Identification) device
        if (string.IsNullOrEmpty(_reqInfo.TransactionID)) // Flag is NOT needed if TroutD is being used for void, credit, or completion
        {
          if (PC_Req.PAYMENT_TYPE != "TOKEN") // Bug 18129 DJD - Flag is NOT needed for Token transactions
          {
          if (_reqInfo.PassedByRFID == true)
          {
            PC_Req.PRESENT_FLAG = "4"; // Passed by RFID
          }
          else
          {
            if (string.IsNullOrEmpty(_reqInfo.TrackData)) // No track data (manually entered)
            {
              if (_reqInfo.eCommerceSale) // Business Center
              {
                PC_Req.PRESENT_FLAG = "1"; // Card not present
              }
              else
              {
                PC_Req.PRESENT_FLAG = _reqInfo.CardPresent == true ? "2" : "1";
              }
            }
            else // Has track data
            {
              PC_Req.PRESENT_FLAG = "3"; // Card swiped
            }
          }
        }
        }

        PC_Req.FORCE_FLAG = _reqInfo.ForceDuplicateTrans == true ? "TRUE" : null;

        if (PC_Req.COMMAND == "SALE")
        {
          if (_reqInfo.eCommerceSale) // Business Center
          {
            PC_Req.PARTIAL_AUTH = "0";
          }
          else
          {
            PC_Req.PARTIAL_AUTH = _reqInfo.AllowPartialAuth == true ? "1" : "0";
          }
        }

        PC_Req.TRACK_DATA = _reqInfo.TrackData;
        // _transInfo.DeviceID;

        if (!string.IsNullOrEmpty(_reqInfo.CustomerLastname))
        {
          if (!string.IsNullOrEmpty(_reqInfo.CustomerFirstname))
          {
            PC_Req.CARDHOLDER = string.Format("{0}, {1}", _reqInfo.CustomerLastname, _reqInfo.CustomerFirstname);
          }
          else
          {
            PC_Req.CARDHOLDER = _reqInfo.CustomerLastname;
          }
        }

        PC_Req.CUSTOMER_STREET = _reqInfo.CustomerStreet;
        PC_Req.CUSTOMER_CITY = _reqInfo.CustomerCity;
        PC_Req.CUSTOMER_ZIP = _reqInfo.CustomerZip?.Replace("-", ""); //Bug 20505 NK / BUG 25690 RDM ZIP may be null
        PC_Req.CASHIER_NUM = _reqInfo.CashierNumber;
        PC_Req.INVOICE = _reqInfo.InvoiceNbr;
        PC_Req.COL_3 = _reqInfo.CustomerID;
        PC_Req.COL_4 = _reqInfo.TransDesc;
        PC_Req.COL_5 = _reqInfo.MerchantData05;
        PC_Req.COL_6 = _reqInfo.MerchantData06;
        PC_Req.COL_7 = _reqInfo.MerchantData07;
        PC_Req.COL_8 = _reqInfo.MerchantData08;
        PC_Req.COL_9 = _reqInfo.MerchantData09;
        PC_Req.COL_10 = _reqInfo.MerchantData10;

        if (_reqInfo.IIAS == true)
        {
          PC_Req.AMOUNT_HEALTHCARE = _reqInfo.AmountHealthcare > 0.00M ?
              string.Format("{0:N2}", _reqInfo.AmountHealthcare) : null;

          PC_Req.AMOUNT_PRESCRIPTION = _reqInfo.AmountPrescription > 0.00M ?
              string.Format("{0:N2}", _reqInfo.AmountPrescription) : null;

          PC_Req.AMOUNT_VISION = _reqInfo.AmountVision > 0.00M ?
              string.Format("{0:N2}", _reqInfo.AmountVision) : null;

          PC_Req.AMOUNT_CLINIC = _reqInfo.AmountClinic > 0.00M ?
              string.Format("{0:N2}", _reqInfo.AmountClinic) : null;

          PC_Req.AMOUNT_DENTAL = _reqInfo.AmountDental > 0.00M ?
              string.Format("{0:N2}", _reqInfo.AmountDental) : null;
        }

        // If this is a Token Update then there is no need to send the Account Number or Track Data
        if (!string.IsNullOrEmpty(_reqInfo.Token) && _reqInfo.Action == CardActionEnum.TokenUpdate)
        {
          PC_Req.ACCT_NUM = null;
          PC_Req.TRACK_DATA = null;
        }

        if (XmlConverter.Serialize(PC_Req, ref sXml))
        {
            // Bug 18129 DJD - If UGP Request replace <TRANSACTION> with <UGPREQUEST VER='1.0'> and </TRANSACTION> with </UGPREQUEST>
            if (IsUGPRequest())
            {
                const string UGP_START_TAG = @"<UGPREQUEST VER='1.0'>";
                const string UGP_END_TAG = @"</UGPREQUEST>";
                const string TRANS_START_TAG = @"<TRANSACTION>";
                const string TRANS_END_TAG = @"</TRANSACTION>";
                sXml = sXml.Replace(TRANS_START_TAG, UGP_START_TAG).Replace(TRANS_END_TAG, UGP_END_TAG);
            }
        }
        else
        {
          LogError(null, sXml);
          SetRespError(ErrorType.GatewayConnector, "PWC-0005", sXml);
          return false;
        }
      }
      catch (Exception ex)
      {
        string err = "Error Building PAYware Connect Request";
        LogError(ex, err);
        SetRespError(ErrorType.GatewayConnector, "PWC-0006", err, ex);
        return false;
      }

      if (string.IsNullOrEmpty(sXml))
      {
        string err = string.Format("{0}{1}"
            , "Error Building PAYware Connect Request:\r\n"
            , "There was no XML string generated (request was not serialized)."
            );
        LogError(null, err);
        SetRespError(ErrorType.GatewayConnector, "PWC-0007", err);
        return false;
      }
      return true;
    }

    /// <summary>
    /// Checks if the action is for an Admin Request
    /// </summary>
    /// <returns></returns>
    private bool IsAdminRequest()
    {
      bool adminRequest = false;
      adminRequest = (_reqInfo.Action >= CardActionEnum.ActivateContract && _reqInfo.Action <= CardActionEnum.UpdateCustomer);
      return adminRequest;
    }

    /// <summary>
    /// Bug 18129 DJD - Checks if the action is for a UGP Request
    /// </summary>
    /// <returns></returns>
    private bool IsUGPRequest()
    {
        bool UGPRequest = false;
        UGPRequest = (_reqInfo.Action >= CardActionEnum.TokenQuery && _reqInfo.Action <= CardActionEnum.TokenUpdate);
        return UGPRequest;
    }

    /// <summary>
    /// PAYware Connect provides Add-On capabilities via the ADD-ON ADMIN function type.
    /// The following Add-On Admin functions are available:
    /// Add New Customer (ADDON_NEW_CUSTOMER)
    /// Update Customer (ADDON_UPDATE_CUSTOMER)
    /// Add New Contract (ADDON_NEW_CONTRACT)
    /// Activate Contract (ADDON_ACTIVATE_CONTRACT)
    /// Add New Customer and Contract (ADDON_NEW_CUSTOMER_CONTRACT)
    /// Update Contract (ADDON_UPDATE_CONTRACT)
    /// Cancel Contract (ADDON_CANCEL_CONTRACT)
    /// </summary>
    /// <param name="sXml"></param>
    /// <returns>True if Admin request is successfully built; False otherwise</returns>
    public bool BuildAdminRequest(ref string sXml)
    {
      try
      {
        sXml = string.Empty;
        AdminRequest PC_Req = new AdminRequest();
        
        // Firstly, set values for the Admin Header
        PC_Req.AdminHeader = new AdminRequestAdminHeader();
        PC_Req.AdminHeader.RequestType      = Action_PmtTypeOrReqType[_cardAction];
        PC_Req.AdminHeader.FunctionType     = Action_FunctionType[_cardAction];
        PC_Req.AdminHeader.CommandType      = Action_CommandType[_cardAction];

        // Secondly, set values for login credentials and merchant info [Admin Body: HEADER section]
        PC_Req.AdminBody                    = new AdminRequestAdminBody();
        PC_Req.AdminBody.HEADER             = new AdminRequestAdminBodyHEADER();
        PC_Req.AdminBody.HEADER.Client_ID   = _gatewayInfo.ClientID;
        PC_Req.AdminBody.HEADER.MERCHANTKEY = _gatewayInfo.MerchantKey;
        PC_Req.AdminBody.HEADER.USERID      = _gatewayInfo.User;
        PC_Req.AdminBody.HEADER.USERPWD     = _gatewayInfo.Password;

        // Thirdly, set values for the customer info (if required for action) [Admin Body: DETAIL: CUSTOMER section]
        PC_Req.AdminBody.DETAIL        = new AdminRequestAdminBodyDETAIL();
        PC_Req.AdminBody.DETAIL.RECORD = new AdminRequestAdminBodyDETAILRECORD();
        if ( (_cardAction == CardActionEnum.NewCustomer)
          || (_cardAction == CardActionEnum.NewCustomerWithContract)
          || (_cardAction == CardActionEnum.UpdateCustomer))
        {
          PC_Req.AdminBody.DETAIL.RECORD.CUSTOMER                      = new AdminRequestAdminBodyDETAILRECORDCUSTOMER();
          PC_Req.AdminBody.DETAIL.RECORD.CUSTOMER.Merchant_Customer_ID = _reqInfo.MerchantCustomerID;
          PC_Req.AdminBody.DETAIL.RECORD.CUSTOMER.Company_Name         = _reqInfo.CompanyName;
          PC_Req.AdminBody.DETAIL.RECORD.CUSTOMER.Salutation           = _reqInfo.CustomerSalutation;
          PC_Req.AdminBody.DETAIL.RECORD.CUSTOMER.First_Name           = _reqInfo.CustomerFirstname;
          PC_Req.AdminBody.DETAIL.RECORD.CUSTOMER.Last_Name            = _reqInfo.CustomerLastname;
          PC_Req.AdminBody.DETAIL.RECORD.CUSTOMER.Middle_Initial       = _reqInfo.CustomerMiddleInitial;
          PC_Req.AdminBody.DETAIL.RECORD.CUSTOMER.Address_1            = _reqInfo.CustomerStreet;
          PC_Req.AdminBody.DETAIL.RECORD.CUSTOMER.Address_2            = _reqInfo.CustomerStreet2;
          PC_Req.AdminBody.DETAIL.RECORD.CUSTOMER.City                 = _reqInfo.CustomerCity;
          PC_Req.AdminBody.DETAIL.RECORD.CUSTOMER.State                = _reqInfo.CustomerState;
          PC_Req.AdminBody.DETAIL.RECORD.CUSTOMER.Zip                  = _reqInfo.CustomerZip;
          PC_Req.AdminBody.DETAIL.RECORD.CUSTOMER.Pri_Contact          = _reqInfo.PrimaryContactName;
          PC_Req.AdminBody.DETAIL.RECORD.CUSTOMER.Pri_Phone            = _reqInfo.PrimaryPhone;
          PC_Req.AdminBody.DETAIL.RECORD.CUSTOMER.Pri_Email            = _reqInfo.PrimaryEmail;
          PC_Req.AdminBody.DETAIL.RECORD.CUSTOMER.Alt_Contact          = _reqInfo.AlternateContactName;
          PC_Req.AdminBody.DETAIL.RECORD.CUSTOMER.Alt_Phone            = _reqInfo.AlternatePhone;
          PC_Req.AdminBody.DETAIL.RECORD.CUSTOMER.Alt_Email            = _reqInfo.AlternateEmail;
        }

        // Lastly, set values for the contract info (if required for action) [Admin Body: DETAIL: CONTRACT section]
        if ( (_cardAction == CardActionEnum.AddContractToCustomer)
          || (_cardAction == CardActionEnum.UpdateContract)
          || (_cardAction == CardActionEnum.CancelContract)
          || (_cardAction == CardActionEnum.NewCustomerWithContract)
          || (_cardAction == CardActionEnum.ActivateContract) )
        {
          PC_Req.AdminBody.DETAIL.RECORD.CONTRACT = new AdminRequestAdminBodyDETAILRECORDCONTRACT();
          if(_cardAction != CardActionEnum.NewCustomerWithContract)
          {
            PC_Req.AdminBody.DETAIL.RECORD.CONTRACT.Merchant_Customer_ID = _reqInfo.MerchantCustomerID;
          }
          PC_Req.AdminBody.DETAIL.RECORD.CONTRACT.Merchant_Contract_ID   = _reqInfo.MerchantContractID;
          PC_Req.AdminBody.DETAIL.RECORD.CONTRACT.Card_Type              = _reqInfo.CardType;
          PC_Req.AdminBody.DETAIL.RECORD.CONTRACT.Pri_Account_Number     = _reqInfo.AccountNumber;
          PC_Req.AdminBody.DETAIL.RECORD.CONTRACT.Pri_Exp_Month          = _reqInfo.ExpMonth;
          PC_Req.AdminBody.DETAIL.RECORD.CONTRACT.Pri_Exp_Year           = _reqInfo.ExpYear;
          if (_reqInfo.AlternatePayment.HasValue)
          {
            PC_Req.AdminBody.DETAIL.RECORD.CONTRACT.fAlt_Payment         = (bool)_reqInfo.AlternatePayment ? "true" : "false";
          }
          PC_Req.AdminBody.DETAIL.RECORD.CONTRACT.Alt_Account_Number     = _reqInfo.AlternateAccountNumber;
          PC_Req.AdminBody.DETAIL.RECORD.CONTRACT.Alt_Exp_Month          = _reqInfo.AlternateExpMonth;
          PC_Req.AdminBody.DETAIL.RECORD.CONTRACT.Alt_Exp_Year           = _reqInfo.AlternateExpYear;
          PC_Req.AdminBody.DETAIL.RECORD.CONTRACT.Custom_Notes           = _reqInfo.CustomNotes;
        }

        if (!XmlConverter.Serialize(PC_Req, ref sXml))
        {
          SetRespError(ErrorType.GatewayConnector, "PWC-0008", sXml);
          return false;
        }
      }
      catch (Exception ex)
      {
        string err = "Error Building PAYware Connect Admin Request";
        LogError(ex, err);
        SetRespError(ErrorType.GatewayConnector, "PWC-0009", err, ex);
        return false;
      }

      if (string.IsNullOrEmpty(sXml))
      {
        string err = string.Format("{0}{1}"
            , "Error Building PAYware Connect Admin Request From Generic Format:\r\n"
            , "There was no XML string generated (request was not serialized)."
            );
        SetRespError(ErrorType.GatewayConnector, "PWC-0010", err);
        LogError(null, err);
        return false;
      }

      return true;
    }

    public bool SetRespError(ErrorType errType, string errCode, string errMsg)
    {
      return SetRespError(errType, errCode, errMsg, null);
    }

    public bool SetRespError(ErrorType errType, string errCode, string errMsg, Exception ex)
    {
      if (_resp == null) { return false; }

      if (ex != null)
      {
        StringBuilder sb = new StringBuilder();
        sb.Append(string.Format("\r\n{0}\r\n", errMsg));
        sb.Append(ex.ToMessageAndCompleteStacktrace());
        errMsg = sb.ToString();
      }

      switch (errType)
      {
        case ErrorType.GatewayConnector:
          {
            _resp.ConnectorErrorCode = errCode;
            _resp.ConnectorErrorMsg = errMsg;
          }
          break;

        case ErrorType.Gateway:
          {
            _resp.GatewaySuccess = false;
            _resp.GatewayCode = errCode;
            _resp.GatewayMessage = errMsg;
          }
          break;

        case ErrorType.Processor:
          {
            _resp.ProcessorApproval = false;
            _resp.ProcessorCode = errCode;
            _resp.ProcessorMessage = errMsg;
          }
          break;
      }

      return true;
    }
  }


  /// <summary>
  /// The TRANSACTION class is used for setting the PAYware Connect data elements
  /// (and later generating XML from it) for the following Card Transactions:
  /// 
  /// Credit Sale
  /// A SALE transaction reduces the cardholder's limit to buy, and places the
  /// transaction into the open batch.
  /// 
  /// Credit Void
  /// A VOID removes a SALE, CREDIT, POST_AUTH, or COMPLETION transaction from 
  /// the open batch. No funds will be deposited into the merchant's bank account
  /// at settlement/close. The VOID command is typically used for same day returns
  /// or to correct cashier mistakes. This action can only be performed before the
  /// batch is settled or closed (this usually means the command has to be performed
  /// on the same day as the sale). To perform a VOID, submit the SALE, CREDIT, 
  /// POST_AUTH, or COMPLETION's TROUTD value.
  /// 
  /// Credit Return [PAYMENT_TYPE: CREDIT]
  /// A Return is used to refund money to the cardholder. This command is typically
  /// used after the batch that contains the original SALE, POST_AUTH, or COMPLETION
  /// transaction has been settled or closed. A Return will increase the cardholder's
  /// limit to buy once the batch containing the return has been settled or closed.
  /// 
  /// Debit Sale
  /// A SALE transaction reduces the cardholder's limit to buy, and places the 
  /// transaction into the open batch.
  /// </summary>
  public class TRANSACTION
  {
    // The type of Function PAYware Connect will perform. Each API call requires, at minimum, a valid FUNCTION_TYPE.
    // Valid Values: For Payment transactions: PAYMENT [Unused: For Batch transactions: BATCH, For Admin transactions: ADMIN, For Report transactions: REPORT]
    public string FUNCTION_TYPE { get; set; }

    // The type of payment that PAYware Connect will perform or the Payment type to be used for Batch Settlement.
    // Valid Values: CREDIT, DEBIT [Unused: CHECK, GIFT, EBT, CASH]
    public string PAYMENT_TYPE { get; set; }

    // The type of command PAYware Connect will perform.
    // Valid Values (PAYMENT): PRE_AUTH, SALE, CREDIT, VOID, COMPLETION, POST_AUTH, [Unused: COMMERCIAL, ADD_TIP, RESET_TIP, VOICE_AUTH, INQUIRY,
    // STATUS, VERIFY, REGISTER, ADD_VALUE, ACTIVATE, GIFT_CLOSE, BALANCE, TRANSFER, ADD_POINTS, REDEEM_POINTS, REACTIVATE, ISSUE]
    public string COMMAND { get; set; }

    // Merchant Key Authentication consists of the following four values: CLIENT_ID, USER_ID, USER_PW, MERCHANTKEY
    public string CLIENT_ID { get; set; }
    public string USER_ID { get; set; }
    public string USER_PW { get; set; }
    public string MERCHANTKEY { get; set; }

    // Bug 18129 DJD - Client ID Components for UGP Protocol [Account, Site, Term]
    public string ACCOUNT { get; set; }
    public string SITE { get; set; }
    public string TERM { get; set; }

    // Managed Device Authentication consists of the following four values: CLIENT_ID, SERIAL_NUM, DEVTYPE, DEVICEKEY
    // NOTE: Currently, this authentication is used exclusively for the Vx805 device.
    public string SERIAL_NUM { get; set; }
    public string DEVTYPE { get; set; }
    public string DEVICEKEY { get; set; }


    public string TRANS_AMOUNT { get; set; }
    public string TRACK_DATA { get; set; }
    public string INVOICE { get; set; }
    public string BATCH_TRACE_ID { get; set; }
    public string CARDHOLDER { get; set; }
    public string PRESENT_FLAG { get; set; }

    /// <summary>
    /// ADD-ON Payment Transactions:
    /// Add-On transactions are transactions performed on previously stored customer
    /// credit cards using only the customer records. These transactions can be processed
    /// without passing credit card numbers. These transactions are performed on add-on
    /// contracts. The Add-On transactions feature is available within the Recurring
    /// Billing Dashboard in the Store Portal, as well as through API.
    /// Add-On Transactions allow transactions to be processed on demand for previously
    /// stored customers and contracts. It is necessary to pass a customer ID as well
    /// as the contract ID for PAYware Connect to know which account needs to be processed.
    /// Typical Add-On transactions are: Sale, Pre-Authorization, and Credit/Return.
    /// 
    /// RBCUSTOMER_ID: Recurring Billing Customer ID Flag
    /// Flag to indicate the customer ID configured in Recurring Billing for this CLIENT_ID
    /// 
    /// RBCONTRACT_ID: Recurring Billing Contract ID Flag
    /// Flag to indicate the contract ID configured in Recurring Billing for this CLIENT_ID.
    /// 
    /// RBACCOUNT: Recurring Billing Account Flag
    /// Flag to indicate the Account Flag to utilize. “P” for Primary or “A” for Alternate Account Number.
    /// This will default to Primary if this tag is not sent.
    /// Valid Values: P – Primary, A – Alternate
    /// 
    /// RBEMAIL: Recurring Billing E-mail Flag
    /// Flag to indicate whether notification e-mail will be sent upon completion of processing successfully.
    /// If this tag is not sent, no e-mail will be attempted.
    /// Valid Values: Y – Yes, send notification e-mail, N – No, do not send notification e-mail
    /// </summary>
    public string RBCUSTOMER_ID { get; set; }
    public string RBCONTRACT_ID { get; set; }
    public string RBACCOUNT { get; set; }
    public string RBEMAIL { get; set; }

    /// <summary>
    /// Cross Reference Data.
    /// 
    /// This allows the merchant to pass a value that can trace a transaction response
    /// back to its original request.  When a value for COL_X is passed in, that same 
    /// value will be returned in the response and can be used as a filter for reports.
    ///  
    /// Valid Values: COL_3 thru COL_10 (COL_1 and COL_2 are reserved for internal use)
    /// </summary>
    public string COL_3 { get; set; }
    public string COL_4 { get; set; }
    public string COL_5 { get; set; }
    public string COL_6 { get; set; }
    public string COL_7 { get; set; }
    public string COL_8 { get; set; }
    public string COL_9 { get; set; }
    public string COL_10 { get; set; }

    /// <summary> FSA/HRA
    /// 
    /// FSA = Flexible Spending Account
    /// HRA = Health Reimbursement Arrangement
    /// IIAS = Inventory Information Approval System
    /// 
    /// NOTE: PAYware Connect will support FSA/HRA transactions with the Fifth Third
    /// St Pete platform only.
    /// 
    /// * An IIAS program is an inventory control system that helps to categorize saleable 
    ///   items. FSA/HRA deals with the healthcare-related categories. With this system in
    ///   place, the merchant will know based on an item's SKU number whether or not it is
    ///   FSA/HRA eligible. When a customer purchases items, the IIAS system will 
    ///   automatically flag the healthcare-related items so that the customer can purchase
    ///   using an FSA/HRA card.
    ///   
    /// * FSA/HRA cards are similar to prepaid credit cards, but are different in that they
    ///   are for healthcare benefits. For FSA cards, the employee can "load" money onto the
    ///   card through payroll deduction and then they can use this card to purchase 
    ///   healthcare related items that are not covered under their normal healthcare 
    ///   insurance program. The employer can also load an FSA card for this purpose. 
    ///   For HRA cards, only the employer can load the cards with the monetary value.
    ///   
    /// * Eligible items currently include: prescriptions (including over the counter 
    ///   medications), vision care, clinical (doctor visits, labs, etc) and dental. 
    ///   The list of eligible medical expenses is part of the Internal Revenue Code. The 
    ///   merchant is responsible for the maintenance of all eligible SKUs so that when a 
    ///   purchase is made, the eligible items can purchased using a FSA/HRA card.
    ///   
    /// * FSA/HRA cards currently are only Visa/MC. There are specific BIN ranges for these
    ///   cards.
    /// </summary>
    public string AMOUNT_HEALTHCARE { get; set; }
    public string AMOUNT_PRESCRIPTION { get; set; }
    public string AMOUNT_VISION { get; set; }
    public string AMOUNT_CLINIC { get; set; }
    public string AMOUNT_DENTAL { get; set; }

    public string TROUTD { get; set; }
    public string PIN_BLOCK { get; set; }
    public string KEY_SERIAL_NUMBER { get; set; }
    public string ACCT_NUM { get; set; }
    public string CVV2 { get; set; }
    public string EXP_YEAR { get; set; }
    public string EXP_MONTH { get; set; }
    public string EXP_DATE { get; set; } // Bug 24436 DJD: Valid only for UGP requests when generating a payment token Ex: 1216
    public string FORCE_FLAG { get; set; }
    public string PARTIAL_AUTH { get; set; } // Partial Authorization Support flag. Valid Values: 1 or 0
    public string CUSTOMER_STREET { get; set; }
    public string CUSTOMER_CITY { get; set; }
    public string CUSTOMER_ZIP { get; set; }
    public string CASHIER_NUM { get; set; }

    // Bug 18129 DJD - Valid TKN_PROCESS values:
    // 0 – Tokenization is not used
    // 1 – Create Matching Token (one-way) Only
    // 2 – Create Payment Token (two-way) and Matching Token (one-way)
    public string TKN_PROCESS { get; set; }    // Token processing action used in the TOKEN QUERY request. This tag directs the API’s actions if a token 
                                               // does not exist for the PAN submitted. This only applies if the query is submitted with PAN or track data.
    public string TKN_RENEW { get; set; } // 1 – True (update) 0 – False. Flag to update token expiration date. (Today + Entity default)
    public string TKN_RENEW_DAYS { get; set; } // Update token expiration date by (Today + TKN_RENEW_DAYS)

    // Bug 18129 DJD Payment Token (two-way). This is a token that can be used only by the
    // payment gateway for future payment transactions without requiring Track
    // Data or PAN data. Case-sensitive.
    public string TKN_PAYMENT { get; set; }

    // Bug 18129 21131 DJD - eWallet: Update Card Expiration Date for PAYware Token
    public string TKN_PAYMENT_EXPDATE { get; set; } // Card expiration date MMYY

    public string TKN_CADDR { get; set; } // Bug 25177 DJD: Card address for token update
    public string TKN_CPCODE { get; set; } // Bug 25177 DJD: Card zip code for token update

    public string TKN_EXPDATE { get; set; } // Bug 26819 DJD - eWallet TOKEN EXPIRED - Need to send extend token expiration date call to Payware after each use

    // ***** CURRENTLY UNUSED TAGS *****
    public string COMMAND2 { get; set; }
    public string ABA_NUM { get; set; }
    public string ACH_TYPE { get; set; }
    public string ALT_TAX_ID { get; set; }
    public string AMX_CHARGE_DESC { get; set; }
    public string AMX_CHARGE_DESC_1 { get; set; }
    public string AMX_CHARGE_DESC_2 { get; set; }
    public string AMX_CHARGE_DESC_3 { get; set; }
    public string AMX_CHARGE_DESC_4 { get; set; }
    public string AUTH_CODE { get; set; }
    public string BANK_ACCOUNT_TYPE { get; set; }
    public string BANKNET_DATE { get; set; }
    public string BEVERAGE_AMOUNT { get; set; }
    public string BILLING_STATE { get; set; }
    public string BILLING_ZIP { get; set; }
    public string BUSINESS_DATE { get; set; }
    public string C_ACCT_TYPE { get; set; }
    public string C_DOB { get; set; }
    public string CARD_ID_CODE { get; set; }
    public string CASHBACK_AMNT { get; set; }
    public string CHECK_NUM { get; set; }
    public string CHECK_TYPE { get; set; }
    public string CMRCL_FLAG { get; set; }
    public string COMMA_RESPONSE { get; set; }
    public string CUSTOMER_ACCT_TYPE { get; set; }
    public string CUSTOMER_ADDRESS { get; set; }
    public string CUSTOMER_BANK { get; set; }
    public string CUSTOMER_CODE { get; set; }
    public string CUSTOMER_COUNTRY { get; set; }
    public string CUSTOMER_EMAIL { get; set; }
    public string CUSTOMER_FIRSTNAME { get; set; }
    public string CUSTOMER_ID_DD { get; set; }
    public string CUSTOMER_ID_MM { get; set; }
    public string CUSTOMER_ID_NUM { get; set; }
    public string CUSTOMER_ID_TYPE { get; set; }
    public string CUSTOMER_ID_YY { get; set; }
    public string CUSTOMER_IP_ADDRESS { get; set; }
    public string CUSTOMER_LASTNAME { get; set; }
    public string CUSTOMER_PHONE { get; set; }
    public string CUSTOMER_STATE { get; set; }
    public string CV_RSP_TYPE { get; set; }
    public string DEBIT_TYPE { get; set; }
    public string DEST_COUNTRY_CODE { get; set; }
    public string DEST_ZIP_CODE { get; set; }
    public string DISCOUNT_AMOUNT { get; set; }
    public string DUTY_AMOUNT { get; set; }
    public string EBT_TYPE { get; set; }
    public string EBT_VOUCHER_NUM { get; set; }
    public string ECOMM_GOODS_IND { get; set; }
    public string EMP_NUM { get; set; }
    public string ENQUEUE { get; set; }
    public string FREIGHT_AMOUNT { get; set; }
    public string GIFT_SEQ_NUM { get; set; }
    public string GIFT_UNITS { get; set; }
    public string IND_TYPE { get; set; }
    public string INSTALLMENT { get; set; }
    public string ISSUE_NUM { get; set; }
    public string ITEM_AMOUNT { get; set; }
    public string ITEM_COMM_CODE { get; set; }
    public string ITEM_DESC { get; set; }
    public string ITEM_DISCOUNT_AMOUNT { get; set; }
    public string ITEM_DISCOUNT_IND { get; set; }
    public string ITEM_ID { get; set; }
    public string ITEM_PRODUCT_CODE { get; set; }
    public string ITEM_QUANTITY { get; set; }
    public string ITEM_TAX_AMOUNT { get; set; }
    public string ITEM_TAX_IND { get; set; }
    public string ITEM_TAX_RATE { get; set; }
    public string ITEM_TAX_TYPE { get; set; }
    public string ITEM_UNIT_COST { get; set; }
    public string ITEM_UOM { get; set; }
    public string KEY_SERIAL_NUM { get; set; }
    public string LANGUAGE_CODE { get; set; }
    public string LOYALTY_FLAG { get; set; }
    public string MAC_BLOCK { get; set; }
    public string MANAGER_NUM { get; set; }
    public string MCC { get; set; }
    public string MERCHANT_TYPE { get; set; }
    public string MICR { get; set; }
    public string MIDDLENAME { get; set; }
    public string MODIFIER { get; set; }
    public string MULTI_CLR_SEQ_CNT { get; set; }
    public string MULTI_CLR_SEQ_NUM { get; set; }
    public string MULTI_FLAG { get; set; }
    public string ORDER_DATE { get; set; }
    public string ORIG_PURCH_DATA { get; set; }
    public string ORIG_SEQ_NUM { get; set; }
    public string PARTIAL_REDEMPTION_FLAG { get; set; }
    public string PATH { get; set; }
    public string PAY_INFO { get; set; }
    public string Number_Payments { get; set; }
    public string PAYEE { get; set; }
    public string POS_AUTH_SRC { get; set; }
    public string POS_CAPABILTY { get; set; }
    public string POS_CARD_ID { get; set; }
    public string POS_ENTRY_MODE { get; set; }
    public string POST_TYPE { get; set; }
    public string PRINT_RECEIPTS_FLAG { get; set; }
    public string PROMO_CODE { get; set; }
    public string PT_TRN_TYPE { get; set; }
    public string PURCHASE_ID { get; set; }
    public string QUASICASH { get; set; }
    public string RECURRING { get; set; }
    public string REFERENCE { get; set; }
    public string REFUND_FLAG { get; set; }
    public string RETURN_ACI { get; set; }
    public string SHIFT_ID { get; set; }
    public string SHIP_FROM_ZIP_CODE { get; set; }
    public string SOURCE_ACCT_NUM { get; set; }
    public string STARTDATE { get; set; }
    public string STMT_DESC { get; set; }
    public string STMT_INFO { get; set; }
    public string STMT_NAME { get; set; }
    public string SUFFIX { get; set; }
    public string TAX_AMOUNT { get; set; }
    public string TAX_IND { get; set; }
    public string TIP_AMOUNT { get; set; }
    public string TIP_AMOUNT_EST { get; set; }
    public string TOT_NUM_CARDS { get; set; }
    public string TRAN_SEQ_FLAG { get; set; }
    public string TRANS_ID { get; set; }
    public string TRANSACTION_CODE { get; set; }
    public string TRANSACTION_ID { get; set; }
    public string CTROUTD { get; set; }
    public string REF_TROUTD { get; set; }
    public string END_TROUTD { get; set; }
    public string VALID_CODE { get; set; }
    public string RETURN_FLD_HDRS { get; set; }
    public string USERID { get; set; }
    public string USERPWD { get; set; }
    public string INTRN_SEQ_NUM { get; set; }
    public string PROCESSOR_ID { get; set; }
    public string BATCH_SEQ_NUM { get; set; }
    public string TRANS_SEQ_NUM { get; set; }
    public string TRANS_DATE { get; set; }
    public string TRANS_TIME { get; set; }
    public string STATUS_CODE { get; set; }
    public string PAYMENT_MEDIA { get; set; }
    public string TRACE_CODE { get; set; }
    public string AVS_CODE { get; set; }
    public string CVV2_CODE { get; set; }
    public string RESULT_CODE { get; set; }
    public string REQUEST_COMMAND { get; set; }
    public string START_TRANS_AMOUNT { get; set; }
    public string START_TRANS_DATE { get; set; }
    public string START_TRANS_TIME { get; set; }
    public string START_TROUTD { get; set; }
    public string END_TRANS_AMOUNT { get; set; }
    public string END_TRANS_DATE { get; set; }
    public string END_TRANS_TIME { get; set; }
    public string DELIMITER { get; set; }
    public string FORMAT { get; set; }
    public string MAX_NUM_RECORDS_RETURNED { get; set; }
  }

  /// <summary>
  /// The Admin Request classes are used for setting the PAYware Connect data elements
  /// (and later generating XML from it) for the following Add-On Admin functions:
  /// 
  /// Add New Customer (ADDON_NEW_CUSTOMER)
  /// Update Customer (ADDON_UPDATE_CUSTOMER)
  /// Add New Contract (ADDON_NEW_CONTRACT)
  /// Activate Contract (ADDON_ACTIVATE_CONTRACT)
  /// Add New Customer and Contract (ADDON_NEW_CUSTOMER_CONTRACT)
  /// Update Contract (ADDON_UPDATE_CONTRACT)
  /// Cancel Contract (ADDON_CANCEL_CONTRACT)
  /// 
  /// NOTE: Either type of Authentication Level (Merchant Key or Managed Device) may
  /// be used in the PAYMENT transaction(s) with Contract/Customer. However, the ADMIN
  /// transactions MUST use Merchant Key Authentication.
  /// </summary>  [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
  [System.SerializableAttribute()]
  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.ComponentModel.DesignerCategoryAttribute("code")]
  [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
  [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
  public partial class AdminRequest
  {
    private AdminRequestAdminHeader adminHeaderField;
    private AdminRequestAdminBody adminBodyField;

    /// <remarks/>
    public AdminRequestAdminHeader AdminHeader
    {
      get
      {
        return this.adminHeaderField;
      }
      set
      {
        this.adminHeaderField = value;
      }
    }

    /// <remarks/>
    public AdminRequestAdminBody AdminBody
    {
      get
      {
        return this.adminBodyField;
      }
      set
      {
        this.adminBodyField = value;
      }
    }
  }

  /// <remarks/>
  [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
  [System.SerializableAttribute()]
  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.ComponentModel.DesignerCategoryAttribute("code")]
  [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
  public partial class AdminRequestAdminHeader
  {
    private string requestTypeField;
    private string functionTypeField;
    private string commandTypeField;

    /// <remarks/>
    public string RequestType
    {
      get
      {
        return this.requestTypeField;
      }
      set
      {
        this.requestTypeField = value;
      }
    }

    /// <remarks/>
    public string FunctionType
    {
      get
      {
        return this.functionTypeField;
      }
      set
      {
        this.functionTypeField = value;
      }
    }

    /// <remarks/>
    public string CommandType
    {
      get
      {
        return this.commandTypeField;
      }
      set
      {
        this.commandTypeField = value;
      }
    }
  }

  /// <remarks/>
  [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
  [System.SerializableAttribute()]
  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.ComponentModel.DesignerCategoryAttribute("code")]
  [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
  public partial class AdminRequestAdminBody
  {
    private AdminRequestAdminBodyHEADER hEADERField;
    private AdminRequestAdminBodyDETAIL dETAILField;

    /// <remarks/>
    public AdminRequestAdminBodyHEADER HEADER
    {
      get
      {
        return this.hEADERField;
      }
      set
      {
        this.hEADERField = value;
      }
    }

    /// <remarks/>
    public AdminRequestAdminBodyDETAIL DETAIL
    {
      get
      {
        return this.dETAILField;
      }
      set
      {
        this.dETAILField = value;
      }
    }
  }

  /// <remarks/>
  [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
  [System.SerializableAttribute()]
  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.ComponentModel.DesignerCategoryAttribute("code")]
  [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
  public partial class AdminRequestAdminBodyHEADER
  {

    private string client_IDField;

    private string merchantKeyField;

    private string userIDField;

    private string userPwdField;

    /// <remarks/>
    public string Client_ID
    {
      get
      {
        return this.client_IDField;
      }
      set
      {
        this.client_IDField = value;
      }
    }

    /// <remarks/>
    public string MERCHANTKEY
    {
      get
      {
        return this.merchantKeyField;
      }
      set
      {
        this.merchantKeyField = value;
      }
    }

    /// <remarks/>
    public string USERID
    {
      get
      {
        return this.userIDField;
      }
      set
      {
        this.userIDField = value;
      }
    }

    /// <remarks/>
    public string USERPWD
    {
      get
      {
        return this.userPwdField;
      }
      set
      {
        this.userPwdField = value;
      }
    }
  }

  /// <remarks/>
  [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
  [System.SerializableAttribute()]
  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.ComponentModel.DesignerCategoryAttribute("code")]
  [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
  public partial class AdminRequestAdminBodyDETAIL
  {

    private AdminRequestAdminBodyDETAILRECORD recordField;

    /// <remarks/>
    public AdminRequestAdminBodyDETAILRECORD RECORD
    {
      get
      {
        return this.recordField;
      }
      set
      {
        this.recordField = value;
      }
    }
  }

  /// <remarks/>
  [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
  [System.SerializableAttribute()]
  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.ComponentModel.DesignerCategoryAttribute("code")]
  [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
  public partial class AdminRequestAdminBodyDETAILRECORD
  {

    private AdminRequestAdminBodyDETAILRECORDCUSTOMER customerField;

    private AdminRequestAdminBodyDETAILRECORDCONTRACT contractField;

    /// <remarks/>
    public AdminRequestAdminBodyDETAILRECORDCUSTOMER CUSTOMER
    {
      get
      {
        return this.customerField;
      }
      set
      {
        this.customerField = value;
      }
    }

    /// <remarks/>
    public AdminRequestAdminBodyDETAILRECORDCONTRACT CONTRACT
    {
      get
      {
        return this.contractField;
      }
      set
      {
        this.contractField = value;
      }
    }
  }

  /// <remarks/>
  [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
  [System.SerializableAttribute()]
  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.ComponentModel.DesignerCategoryAttribute("code")]
  [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
  public partial class AdminRequestAdminBodyDETAILRECORDCUSTOMER
  {

    private string company_NameField;

    private string salutationField;

    private string first_NameField;

    private string last_NameField;

    private string middle_InitialField;

    private string address_1Field;

    private string address_2Field;

    private string cityField;

    private string stateField;

    private string zipField;

    private string pri_ContactField;

    private string pri_PhoneField;

    private string pri_EmailField;

    private string alt_ContactField;

    private string alt_PhoneField;

    private string alt_EmailField;

    private string merchant_Customer_IDField;

    /// <remarks/>
    public string Company_Name
    {
      get
      {
        return this.company_NameField;
      }
      set
      {
        this.company_NameField = value;
      }
    }

    /// <remarks/>
    public string Salutation
    {
      get
      {
        return this.salutationField;
      }
      set
      {
        this.salutationField = value;
      }
    }

    /// <remarks/>
    public string First_Name
    {
      get
      {
        return this.first_NameField;
      }
      set
      {
        this.first_NameField = value;
      }
    }

    /// <remarks/>
    public string Last_Name
    {
      get
      {
        return this.last_NameField;
      }
      set
      {
        this.last_NameField = value;
      }
    }

    /// <remarks/>
    public string Middle_Initial
    {
      get
      {
        return this.middle_InitialField;
      }
      set
      {
        this.middle_InitialField = value;
      }
    }

    /// <remarks/>
    public string Address_1
    {
      get
      {
        return this.address_1Field;
      }
      set
      {
        this.address_1Field = value;
      }
    }

    /// <remarks/>
    public string Address_2
    {
      get
      {
        return this.address_2Field;
      }
      set
      {
        this.address_2Field = value;
      }
    }

    /// <remarks/>
    public string City
    {
      get
      {
        return this.cityField;
      }
      set
      {
        this.cityField = value;
      }
    }

    /// <remarks/>
    public string State
    {
      get
      {
        return this.stateField;
      }
      set
      {
        this.stateField = value;
      }
    }

    /// <remarks/>
    public string Zip
    {
      get
      {
        return this.zipField;
      }
      set
      {
        this.zipField = value;
      }
    }

    /// <remarks/>
    public string Pri_Contact
    {
      get
      {
        return this.pri_ContactField;
      }
      set
      {
        this.pri_ContactField = value;
      }
    }

    /// <remarks/>
    public string Pri_Phone
    {
      get
      {
        return this.pri_PhoneField;
      }
      set
      {
        this.pri_PhoneField = value;
      }
    }

    /// <remarks/>
    public string Pri_Email
    {
      get
      {
        return this.pri_EmailField;
      }
      set
      {
        this.pri_EmailField = value;
      }
    }

    /// <remarks/>
    public string Alt_Contact
    {
      get
      {
        return this.alt_ContactField;
      }
      set
      {
        this.alt_ContactField = value;
      }
    }

    /// <remarks/>
    public string Alt_Phone
    {
      get
      {
        return this.alt_PhoneField;
      }
      set
      {
        this.alt_PhoneField = value;
      }
    }

    /// <remarks/>
    public string Alt_Email
    {
      get
      {
        return this.alt_EmailField;
      }
      set
      {
        this.alt_EmailField = value;
      }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string Merchant_Customer_ID
    {
      get
      {
        return this.merchant_Customer_IDField;
      }
      set
      {
        this.merchant_Customer_IDField = value;
      }
    }
  }

  /// <remarks/>
  [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
  [System.SerializableAttribute()]
  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.ComponentModel.DesignerCategoryAttribute("code")]
  [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
  public partial class AdminRequestAdminBodyDETAILRECORDCONTRACT
  {

    private string merchant_Customer_IDField;

    private string merchant_Contract_IDField;

    private string card_TypeField;

    private string pri_Account_NumberField;

    private string pri_Exp_MonthField;

    private string pri_Exp_YearField;

    private string fAlt_PaymentField;

    private string alt_Account_NumberField;

    private string alt_Exp_MonthField;

    private string alt_Exp_YearField;

    private string custom_NotesField;

    /// <remarks/>
    public string Merchant_Customer_ID
    {
      get
      {
        return this.merchant_Customer_IDField;
      }
      set
      {
        this.merchant_Customer_IDField = value;
      }
    }

    /// <remarks/>
    public string Merchant_Contract_ID
    {
      get
      {
        return this.merchant_Contract_IDField;
      }
      set
      {
        this.merchant_Contract_IDField = value;
      }
    }

    /// <remarks/>
    public string Card_Type
    {
      get
      {
        return this.card_TypeField;
      }
      set
      {
        this.card_TypeField = value;
      }
    }

    /// <remarks/>
    public string Pri_Account_Number
    {
      get
      {
        return this.pri_Account_NumberField;
      }
      set
      {
        this.pri_Account_NumberField = value;
      }
    }

    /// <remarks/>
    public string Pri_Exp_Month
    {
      get
      {
        return this.pri_Exp_MonthField;
      }
      set
      {
        this.pri_Exp_MonthField = value;
      }
    }

    /// <remarks/>
    public string Pri_Exp_Year
    {
      get
      {
        return this.pri_Exp_YearField;
      }
      set
      {
        this.pri_Exp_YearField = value;
      }
    }

    /// <remarks/>
    public string fAlt_Payment
    {
      get
      {
        return this.fAlt_PaymentField;
      }
      set
      {
        this.fAlt_PaymentField = value;
      }
    }

    /// <remarks/>
    public string Alt_Account_Number
    {
      get
      {
        return this.alt_Account_NumberField;
      }
      set
      {
        this.alt_Account_NumberField = value;
      }
    }

    /// <remarks/>
    public string Alt_Exp_Month
    {
      get
      {
        return this.alt_Exp_MonthField;
      }
      set
      {
        this.alt_Exp_MonthField = value;
      }
    }

    /// <remarks/>
    public string Alt_Exp_Year
    {
      get
      {
        return this.alt_Exp_YearField;
      }
      set
      {
        this.alt_Exp_YearField = value;
      }
    }

    /// <remarks/>
    public string Custom_Notes
    {
      get
      {
        return this.custom_NotesField;
      }
      set
      {
        this.custom_NotesField = value;
      }
    }
  }

  public static class XmlConverter
  {
    public static bool Serialize<T>(T value, ref string sRetVal)
    {
      return Serialize(value, ref sRetVal, false, null, true);
    }

    /// <summary>
    /// This method returns TRUE if the object passed was successfully
    /// serialized and converted to an XML string - FALSE, otherwise.
    /// XML is set to referenced string "sRetVal" if successful.
    /// Error message is set to referenced string "sRetVal" if NOT successful.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value"></param>
    /// <param name="sRetVal"></param>
    /// <param name="IncludeEmptyTags"></param>
    /// <param name="arrMask"></param>
    /// <param name="IncludeEntities"></param>
    /// <returns>TRUE if the object passed was successfully serialized and converted to an XML string - FALSE, otherwise.</returns>
    public static bool Serialize<T>(T value, ref string sRetVal, bool IncludeEmptyTags, string[] arrMask, bool IncludeEntities)
    {
      // Reserved characters in HTML and XML are replaced with character entities:
      const string LESS_THAN = @"&lt;";  // <
      const string GREATER_THAN = @"&gt;";  // >
      const string AMPERSAND = @"&amp;"; // &

      // To revert the escape process, use:
      //SecurityElement securityElement = System.Security.SecurityElement.FromString("<test>H&amp;M</test>");
      //string unescapedText = securityElement.Text;
      //Console.WriteLine(unescapedText); // Result: H&M

      sRetVal = "";
      if (value == null) { return true; }

      try
      {
        // NOTE: Sending the "xns" will put "q1:" in the Heartland response tags
        // Remove XSI and XSD namespaces:
        XmlSerializerNamespaces xns = new XmlSerializerNamespaces();
        xns.Add(string.Empty, string.Empty);

        XmlSerializer serializer = new XmlSerializer(typeof(T));

        XmlWriterSettings settings = new XmlWriterSettings();
        settings.Encoding = new UnicodeEncoding(false, false); // no BOM in a .NET string
        settings.Indent = true;
        settings.OmitXmlDeclaration = true;
        //settings.NewLineOnAttributes = true;

        using (StringWriter textWriter = new StringWriter())
        {
          using (XmlWriter xmlWriter = XmlWriter.Create(textWriter, settings))
          {
            serializer.Serialize(xmlWriter, value, xns);
            //serializer.Serialize(xmlWriter, value);
          }

          sRetVal = IncludeEmptyTags ? textWriter.ToString() : CleanEmptyTags(textWriter.ToString());

          if (arrMask != null)
          {
#if !DEBUG
            List<string> tags = new List<string>(arrMask);
            CStandardSystemInterface.MaskTags(ref sRetVal, tags);
            //pci.MaskTags(ref sRetVal, arrMask);
#endif
          }

          if (!IncludeEntities)
          {
            sRetVal = sRetVal.Replace(LESS_THAN, @"<").Replace(GREATER_THAN, @">").Replace(AMPERSAND, @"&");
          }

          return true;
        }
      }
      catch (Exception ex)
      {
        StringBuilder sb = new StringBuilder();
        sb.Append(string.Format("Message ---\r\n{0}", ex.Message));
        if (ex.InnerException != null)
        {
          sb.Append(string.Format("\r\nInner Exception ---\r\n{0}", ex.InnerException.Message));
        }
        sb.Append(string.Format("\r\nSource ---\r\n{0}", ex.Source));
        sb.Append(string.Format("\r\nStack Trace ---\r\n{0}", ex.StackTrace));
        sb.Append(string.Format("\r\nTarget Site ---\r\n{0}\r\n  ", ex.TargetSite));
        sRetVal = sb.ToString();
        return false;
      }
    }

    /// <summary>
    /// Mask the tags in XML string.
    /// </summary>
    public static void MaskTags(ref string sXml, string[] arrTags)
    {
      foreach (string sTagName in arrTags) // Loop over tags to mask
      {
        string sStartTag = string.Format("<{0}>", sTagName);
        string sEndTag = string.Format("</{0}>", sTagName);
        int nStartIndex = sXml.IndexOf(sStartTag);
        int nEndIndex = sXml.IndexOf(sEndTag);

        if ((nStartIndex > -1) && (nEndIndex > nStartIndex))
        {
          string sStrToReplace = sXml.Substring(nStartIndex, ((nEndIndex - nStartIndex) + sEndTag.Length));
          string sMaskedContent = new string('*', sStrToReplace.Length - (sStartTag.Length + sEndTag.Length));
          if (sMaskedContent.Length > 21)
          {   // If length is greater than 21 characters then expose the last eight characters
            sMaskedContent = sMaskedContent.Substring(8) + sXml.Substring((nEndIndex - 8), 8);
          }
          else if (sMaskedContent.Length > 14)
          {   // If length is greater than 14 characters then expose the last four characters
            sMaskedContent = sMaskedContent.Substring(4) + sXml.Substring((nEndIndex - 4), 4);
          }
          string sReplacementStr = string.Format("{0}{1}{2}", sStartTag, sMaskedContent, sEndTag);
          sXml = sXml.Replace(sStrToReplace, sReplacementStr);
        }
      }
    }

    /// <summary>
    /// Deletes empty Xml tags from the passed xml
    /// </summary>
    public static string CleanEmptyTags(String xml)
    {
      Regex regex = new Regex(@"(\s)*<(\w)*(\s)*/>");
      return regex.Replace(xml, string.Empty);
    }

    public static T Deserialize<T>(string xml)
    {
      if (string.IsNullOrEmpty(xml))
      {
        return default(T);
      }

      XmlSerializer serializer = new XmlSerializer(typeof(T));

      XmlReaderSettings settings = new XmlReaderSettings();
      // No settings need modifying here

      using (StringReader textReader = new StringReader(xml))
      {
        using (XmlReader xmlReader = XmlReader.Create(textReader, settings))
        {
          return (T)serializer.Deserialize(xmlReader);
        }
      }
    }
  }

  /// <summary>
  /// Values used to populate the STATUS column of the TG_SYSTEM_INTERFACE_TRACKING table.
  /// NOTE: The enums must be converted to a string [e.g., Status.STARTED.ToString()]
  /// </summary>
  public enum Status
  {
    NONE,          // No Status value
    STARTED,       // Status set PRIOR to sending a request
    POSTEDFAILURE, // Status set after sending a request with an unsuccessful response
    POSTEDSUCCESS, // Status set after sending a request with a successful response
    COMPLETED,     // Status set after tender is posted (set by iPayment app) 
    // [NOTE: Gateway Connector sets COMPLETED to Void transactions (skips POSTEDSUCCESS status)]
  }

  /// <summary>
  /// Values used to populate the ACTIVITY column of the TG_SYSTEM_INTERFACE_TRACKING table.
  /// NOTE: The enums must be converted to a string [e.g., Activity.SALE.ToString()]
  /// </summary>
  public enum Activity
  {
    NONE,   // No Activity value
    SALE,   // Sale transaction request
    CREDIT, // Credit Return transaction request
    VOID,   // Void transaction request
  }

  /// <summary>
  /// The CardActivityTracker class is used exclusively for the iPayment application and
  /// will write to the TG_SYSTEM_INTERFACE_TRACKING table when:
  ///    1) A request is sent to the gateway (or error occurs).
  ///    2) A response is recieved from the gateway (or error occurs).
  /// 
  /// TG_SYSTEM_INTERFACE_TRACKING Description:
  /// 
  /// Column             Type               Comments
  /// ------             ----               --------
  /// SERIAL_ID          int IDENTITY(1,1)  Auto-incrementing ID number.
  /// USERID             varchar(25)        iPayment User ID for individual making card transaction.
  /// INITIAL_DATETIME   datetime           Date time when initial request is sent.
  /// INITIAL_SESSION_ID varchar(25)        Session ID when initial request is sent. 
  /// DEPFILENBR         int                First part of CORE file number (Julian Date).
  /// DEPFILESEQ         int                Second part of CORE file number (Sequence Number).
  /// EVENTNBR           int                Event number for the transaction.
  /// SYSTEM_TYPE        varchar(100)       System Interface ID for gateway.
  /// ACTIVITY           varchar(50)        Type of activity (Sale, Credit, or Void).
  /// ACCOUNT_NUMBER     varchar(25)        Last 4 digits of Card Account Number.
  /// AMOUNT             float              The transaction amount.
  /// STATUS             varchar(50)        The status of transaction.
  /// LAST_DATETIME      datetime           Date time when status is updated (e.g., response received, error, tender posted).
  /// LAST_SESSION_ID    varchar(25)        Session ID when status is updated (e.g., response received, error, tender posted).
  /// REFERENCE          varchar(25)        The Card Transaction ID (e.g., TROUTD in IPCharge, GatewayTxnId in Heartland)
  /// COMMENTS           varchar(max)       The request and/or response (i.e., XML text) for the card transaction.
  /// </summary>
  public class CardActivityTracker
  {
    // Reference to the containing gateway object so that Req and Resp are accessible
    private PAYwareConnectTrans _cardTrans;

    // Database connection for iPayment Tracker Database
    private SqlConnection _sqlConn = null;

    // Connection string for iPayment Tracker Database
    private string _strConn = "";

    // CardActivityTracker Constructor
    public CardActivityTracker(PAYwareConnectTrans cardTrans)
    {
      _cardTrans = cardTrans; // Reference to the containing gateway
      if (cardTrans._trackerInfo != null)
      {   // Build the connectionString for SqlConnection construction
        _strConn = string.Format("Data Source={0}; Initial Catalog={1}; User Id={2}; Password={3}"
        , cardTrans._trackerInfo.DatasourceName
        , cardTrans._trackerInfo.DatabaseName
        , cardTrans._trackerInfo.LoginName
        , cardTrans._trackerInfo.LoginPassword);
      }
    }

    // CardActivityTracker Destructor
    ~CardActivityTracker()
    {   // Cleanup...
      if (_sqlConn != null)
      {
        _sqlConn.Dispose();
      }
    }

    /// <summary>
    /// The StartRequest method is called PRIOR to sending a Card transaction request.
    /// This method will add a new request record to the TG_SYSTEM_INTERFACE_TRACKING table.
    /// If a record is successfully added then true is returned.
    /// If an error occurs then false and error message/code is returned. 
    /// </summary>
    public bool StartRequest(string request)
    {
      string sqlStatement = string.Empty;
      SqlCommand sqlComm = null;

      try
      {
        // Get the SQL Connection
        if (_sqlConn == null)
        {
          _sqlConn = Helper.GetConnection(_strConn);
        }
        else
        {
          // If connection is not null then make certain the
          // connection string is correct.
          if (_strConn.IndexOf(_sqlConn.ConnectionString) == -1)
          {
            _sqlConn.Dispose();
            _sqlConn = Helper.GetConnection(_strConn);
          }
          else
          {
            // If connection string is correct then make certain 
            // the connection is open
            if (_sqlConn.State == ConnectionState.Closed)
            {
              _sqlConn.Open();
            }
          }
        }

        // Build SQL to insert new record into TG_SYSTEM_INTERFACE_TRACKING
        StringBuilder sb = new StringBuilder();
        sb.Append("INSERT INTO TG_SYSTEM_INTERFACE_TRACKING (");
        sb.Append("USERID, INITIAL_DATETIME, INITIAL_SESSION_ID, ");
        sb.Append("DEPFILENBR, DEPFILESEQ, EVENTNBR, SYSTEM_TYPE, ");
        sb.Append("ACTIVITY, ACCOUNT_NUMBER, AMOUNT, STATUS, ");
        sb.Append("LAST_DATETIME, LAST_SESSION_ID, COMMENTS");
        sb.Append(") VALUES (");
        sb.Append("@UserID, @CurrentDateTime, @SessionID, ");
        sb.Append("@DepFileNbr, @DepFileSeq, @EventNbr, @SystemType, ");
        sb.Append("@Activity, @AccountNbr, @Amount, @Status, ");
        sb.Append("@CurrentDateTime, @SessionID, @Comments)");
        sqlStatement = sb.ToString();

        // Create the SQL Command
        sqlComm = Helper.GetCommand(_sqlConn, sqlStatement, CommandType.Text);

        //string request = _masterGW._resp.GatewayRequestMsg.Replace("<![CDATA[", "").Replace("]]>", "").Trim();

        // Add the parameters
        Activity cardActivity = GetActivity(_cardTrans._cardAction);
        Helper.AddParameter(sqlComm, "UserID", _cardTrans._sender.CashierID, SqlDbType.VarChar);
        Helper.AddParameter(sqlComm, "CurrentDateTime", DateTime.Now, SqlDbType.DateTime);
        Helper.AddParameter(sqlComm, "SessionID", _cardTrans._trackerInfo.SessionID, SqlDbType.VarChar);
        Helper.AddParameter(sqlComm, "DepFileNbr", _cardTrans._trackerInfo.DepFileNbr, SqlDbType.Int);
        Helper.AddParameter(sqlComm, "DepFileSeq", _cardTrans._trackerInfo.DepFileSeq, SqlDbType.Int);
        Helper.AddParameter(sqlComm, "EventNbr", _cardTrans._trackerInfo.EventNbr, SqlDbType.Int);
        Helper.AddParameter(sqlComm, "SystemType", _cardTrans._trackerInfo.SystemInterfaceID, SqlDbType.VarChar);
        Helper.AddParameter(sqlComm, "Activity", cardActivity.ToString(), SqlDbType.VarChar);
        Helper.AddParameter(sqlComm, "AccountNbr", _cardTrans._trackerInfo.AcctNbr_Last4Digits, SqlDbType.VarChar);
        Helper.AddParameter(sqlComm, "Amount", _cardTrans._reqInfo.TransactionAmount, SqlDbType.Float);
        Helper.AddParameter(sqlComm, "Status", Status.STARTED.ToString(), SqlDbType.VarChar);
        Helper.AddParameter(sqlComm, "Comments", _cardTrans._resp.GatewayRequestMsg, SqlDbType.VarChar);
        //Helper.AddParameter(sqlComm, "Comments", request, SqlDbType.VarChar);

        // Execute command
        sqlComm.ExecuteNonQuery();
      }
      catch (Exception ex)
      {
        string err = "Cannot write STARTED entry to TG_SYSTEM_INTERFACE_TRACKING Table";
        _cardTrans.LogError(ex, err);
#if DEBUG
        if (!string.IsNullOrEmpty(sqlStatement))
        {
          err = err + (string.Format("\r\nSQL Statement:\r\n{0}", sqlStatement));
        }
#endif
        //_masterGW.SetRespError(PaymentGateway.ErrorType.GatewayConnector, "SIT-0000", err, ex);
        return false;
      }
      finally
      {
        if (sqlComm != null)
        {
          sqlComm.Dispose();
        }
        if (_sqlConn != null)
        {
          _sqlConn.Close();
        }
      }
      return true;
    }

    /// <summary>
    /// The RecordRequestResults method is called AFTER sending a Card transaction request.
    /// This method will Update a request record in the TG_SYSTEM_INTERFACE_TRACKING table
    /// based on the results from the processor/gateway and the type of request.
    /// 
    /// If a record is successfully updated then true is returned.
    /// If an error occurs then false and error message/code is returned.
    /// 
    /// NOTE: If the action was a successful void then the original transaction status will
    /// also be set to COMPLETED.
    /// </summary>
    public bool RecordRequestResults()
    {
      if (_cardTrans._resp.ProcessorApproval) // Card request was successfully processed:
      {
        if ((_cardTrans._cardAction == CardActionEnum.CreditVoid) ||
        (_cardTrans._cardAction == CardActionEnum.DebitVoid))
        {
          if (UpdateRecord(Status.COMPLETED)) // Voided transactions go right to COMPLETED status
          {   // Make certain the ORIGINAL card transaction is also set to COMPLETED
            UpdateRecord(_cardTrans._reqInfo.TransactionID, Status.COMPLETED);
            return true;
          }
          else
          {
            return false;
          }
        }
        else
        {
          return (UpdateRecord(Status.POSTEDSUCCESS));
        }
      }

      // Card request was NOT successfully processed:
      return (UpdateRecord(Status.POSTEDFAILURE));
    }

    /// <summary>
    /// This method will update an existing request record in the TG_SYSTEM_INTERFACE_TRACKING table.
    /// If a record is successfully updated then true is returned.
    /// If no record is found to update then false is returned.
    /// If an error occurs then false is returned (and "geoError" will NOT be NULL).
    /// NOTE: When there is no Partial Authorization Amount, "PartialAuthAmount" should be 0.00.
    /// </summary>
    /// <param name="recStatus"></param>
    /// <returns></returns>
    public bool UpdateRecord(Status recStatus)
    {
      string sqlStatement = string.Empty;
      SqlCommand sqlComm = null;

      try
      {
        // Get the SQL Connection
        if (_sqlConn == null)
        {
          _sqlConn = Helper.GetConnection(_strConn);
        }
        else
        {
          // If connection is not null then make certain the connection string is correct.
          if (_strConn.IndexOf(_sqlConn.ConnectionString) == -1)
          {
            _sqlConn.Dispose();
            _sqlConn = Helper.GetConnection(_strConn);
          }
          else
          {
            // If connection string is correct then make certain the connection is open
            if (_sqlConn.State == ConnectionState.Closed)
            {
              _sqlConn.Open();
            }
          }
        }

        // Get the Serial ID
        int SerialID = 0;
        if (!GetLatestEntryID(ref SerialID))
        {
          return false; // Return false if error occurs.
        }

        // Create the SQL Command
        sqlComm = Helper.GetCommand(_sqlConn, sqlStatement, CommandType.Text);

        // Add the parameters
        Activity cardActivity = GetActivity(_cardTrans._cardAction);
        Helper.AddParameter(sqlComm, "DepFileNbr", _cardTrans._trackerInfo.DepFileNbr, SqlDbType.Int);
        Helper.AddParameter(sqlComm, "DepFileSeq", _cardTrans._trackerInfo.DepFileSeq, SqlDbType.Int);
        Helper.AddParameter(sqlComm, "EventNbr", _cardTrans._trackerInfo.EventNbr, SqlDbType.Int);
        Helper.AddParameter(sqlComm, "SystemType", _cardTrans._trackerInfo.SystemInterfaceID, SqlDbType.VarChar);
        Helper.AddParameter(sqlComm, "AccountNbr", _cardTrans._trackerInfo.AcctNbr_Last4Digits, SqlDbType.VarChar);
        Helper.AddParameter(sqlComm, "Amount", _cardTrans._reqInfo.TransactionAmount, SqlDbType.Float);
        Helper.AddParameter(sqlComm, "CurrentDateTime", DateTime.Now, SqlDbType.DateTime);
        Helper.AddParameter(sqlComm, "SessionID", _cardTrans._trackerInfo.SessionID, SqlDbType.VarChar);
        Helper.AddParameter(sqlComm, "SerialID", SerialID, SqlDbType.Int);

        string sStatusID = (recStatus == Status.NONE) ? "" : recStatus.ToString();

        // Update record in TG_SYSTEM_INTERFACE_TRACKING table:
        StringBuilder sb = new StringBuilder();
        sb.Append("UPDATE TG_SYSTEM_INTERFACE_TRACKING SET ");
        sb.Append("LAST_DATETIME = @CurrentDateTime, LAST_SESSION_ID = @SessionID");
        if (sStatusID != "")
        {
          Helper.AddParameter(sqlComm, "Status", sStatusID, SqlDbType.VarChar);
          sb.Append(", STATUS = @Status");
        }
        if (!string.IsNullOrEmpty(_cardTrans._resp.TransactionID))
        {
          Helper.AddParameter(sqlComm, "Reference", _cardTrans._resp.TransactionID, SqlDbType.VarChar);
          sb.Append(", REFERENCE = @Reference");
        }

        if (!string.IsNullOrEmpty(_cardTrans._resp.GatewayResponseMsg))
        {
          //string response = _cardTrans._resp.GatewayResponseMsg.Replace("<![CDATA[", "").Replace("]]>", "").Trim();
          Helper.AddParameter(sqlComm, "Comments", _cardTrans._resp.GatewayResponseMsg, SqlDbType.VarChar);
          //Helper.AddParameter(sqlComm, "Comments", response, SqlDbType.VarChar);
          sb.Append(", COMMENTS = @Comments");
        }
        else // No response: check for a connector error message
        {
          if (!string.IsNullOrEmpty(_cardTrans._resp.ConnectorErrorMsg))
          {
            Helper.AddParameter(sqlComm, "Comments", _cardTrans._resp.ConnectorErrorMsg, SqlDbType.VarChar);
            sb.Append(", COMMENTS = @Comments");
          }
        }

        if (_cardTrans._resp.PartialAuthAmount > 0.00M) // Was there a Partial Authorization Amount?
        {
          Helper.AddParameter(sqlComm, "Amount", _cardTrans._resp.PartialAuthAmount, SqlDbType.Float);
          sb.Append(", AMOUNT = @PartialAuthAmount");
        }
        sb.Append(" WHERE ");
        sb.Append("DEPFILENBR = @DepFileNbr AND ");
        sb.Append("DEPFILESEQ = @DepFileSeq AND ");
        sb.Append("EVENTNBR = @EventNbr AND ");
        sb.Append("SYSTEM_TYPE = @SystemType AND ");
        sb.Append("ACCOUNT_NUMBER = @AccountNbr AND ");
        sb.Append("AMOUNT = @Amount AND ");
        sb.Append("STATUS <> 'COMPLETED' AND "); // Pretend completed requests don't exist
        sb.Append("SERIAL_ID = @SerialID");
        sqlStatement = sb.ToString();

        // Set SQL statement to command
        sqlComm.CommandText = sqlStatement;

        // Execute command
        sqlComm.ExecuteNonQuery();
      }
      catch (Exception ex)
      {
        string err = "Cannot update record in TG_SYSTEM_INTERFACE_TRACKING table";
#if DEBUG
        if (!string.IsNullOrEmpty(sqlStatement))
        {
          err = err + (string.Format("\r\nSQL Statement:\r\n{0}", sqlStatement));
        }
#endif
        //_cardTrans.SetRespError(PaymentGateway.ErrorType.GatewayConnector, "SIT-0000", err, ex);
        _cardTrans.LogError(ex, err);
        return false;
      }
      finally
      {
        if (sqlComm != null)
        {
          sqlComm.Dispose();
        }
        if (_sqlConn != null)
        {
          _sqlConn.Close();
        }
      }
      return true;
    }

    /// <summary>
    /// This method will get the latest Serial ID in the TG_SYSTEM_INTERFACE_TRACKING table
    /// for the current parameters in the request.
    /// If Serial ID is successfully found then true is returned (and "SerialID" is set).
    /// If no record is found then false is returned.
    /// If an error occurs then false is returned and an error message is set in the response.
    /// </summary>
    /// <param name="SerialID"></param>
    /// <returns></returns>
    public bool GetLatestEntryID(ref int SerialID)
    {
      bool bRet = false;
      string sqlStatement = string.Empty;
      SqlCommand sqlComm = null;

      try
      {
        // Get the SQL Connection
        if (_sqlConn == null)
        {
          _sqlConn = Helper.GetConnection(_strConn);
        }
        else
        {
          // If connection is not null then make certain the
          // connection string is correct.
          if (_strConn.IndexOf(_sqlConn.ConnectionString) == -1)
          {
            _sqlConn.Dispose();
            _sqlConn = Helper.GetConnection(_strConn);
          }
          else
          {
            // If connection string is correct then make certain 
            // the connection is open
            if (_sqlConn.State == ConnectionState.Closed)
            {
              _sqlConn.Open();
            }
          }
        }

        // Create the SQL Command
        sqlComm = Helper.GetCommand(_sqlConn, sqlStatement, CommandType.Text);

        // Add the parameters
        Activity cardActivity = GetActivity(_cardTrans._cardAction);
        Helper.AddParameter(sqlComm, "DepFileNbr", _cardTrans._trackerInfo.DepFileNbr, SqlDbType.Int);
        Helper.AddParameter(sqlComm, "DepFileSeq", _cardTrans._trackerInfo.DepFileSeq, SqlDbType.Int);
        Helper.AddParameter(sqlComm, "EventNbr", _cardTrans._trackerInfo.EventNbr, SqlDbType.Int);
        if (!string.IsNullOrEmpty(_cardTrans._trackerInfo.SystemInterfaceID))
        {
          Helper.AddParameter(sqlComm, "SystemType", _cardTrans._trackerInfo.SystemInterfaceID, SqlDbType.VarChar);
        }
        if (!string.IsNullOrEmpty(_cardTrans._trackerInfo.AcctNbr_Last4Digits))
        {
          Helper.AddParameter(sqlComm, "AccountNbr", _cardTrans._trackerInfo.AcctNbr_Last4Digits, SqlDbType.VarChar);
        }
        if (_cardTrans._reqInfo.TransactionAmount > 0.00M)
        {
          Helper.AddParameter(sqlComm, "Amount", _cardTrans._reqInfo.TransactionAmount, SqlDbType.Float);
        }

        // Build SQL to get latest SERIAL_ID from TG_SYSTEM_INTERFACE_TRACKING
        StringBuilder sb = new StringBuilder();
        sb.Append("SELECT SERIAL_ID FROM TG_SYSTEM_INTERFACE_TRACKING AS s1 ");
        sb.Append("WHERE ");
        sb.Append("s1.SERIAL_ID = (SELECT MAX(SERIAL_ID) FROM ");
        sb.Append("TG_SYSTEM_INTERFACE_TRACKING AS s2 WHERE ");
        sb.Append("s2.DEPFILENBR = @DepFileNbr AND s2.DEPFILESEQ = @DepFileSeq");
        if (sqlComm.Parameters.Contains("SystemType"))
        {
          sb.Append(" AND s2.SYSTEM_TYPE = @SystemType");
        }
        if (sqlComm.Parameters.Contains("AccountNbr"))
        {
          sb.Append(" AND s2.ACCOUNT_NUMBER = @AccountNbr");
        }
        if (sqlComm.Parameters.Contains("Amount"))
        {
          sb.Append(" AND s2.AMOUNT = @Amount");
        }
        sb.Append(" AND s2.EVENTNBR = @EventNbr)");
        sqlStatement = sb.ToString();

        // Set SQL statement to command
        sqlComm.CommandText = sqlStatement;

        // Execute command
        SqlDataReader myReader = sqlComm.ExecuteReader();
        if (myReader.Read())
        {
          SerialID = myReader.GetInt32(0);
          bRet = true;
        }
        myReader.Close();
      }
      catch (Exception ex)
      {
        string err = "Cannot get latest SERIAL_ID from TG_SYSTEM_INTERFACE_TRACKING Table";
#if DEBUG
        if (!string.IsNullOrEmpty(sqlStatement))
        {
          err = err + (string.Format("\r\nSQL Statement:\r\n{0}", sqlStatement));
        }
#endif
        //_cardTrans.SetRespError(PaymentGateway.ErrorType.GatewayConnector, "SIT-0000", err, ex);
        _cardTrans.LogError(ex, err);
        return false;
      }
      finally
      {
        if (sqlComm != null)
        {
          sqlComm.Dispose();
        }
        if (!bRet) // Close connection if Serial ID was NOT successfully retrieved
        {
          if (_sqlConn != null)
          {
            _sqlConn.Close();
          }
        }
      }
      return bRet;
    }

    /// <summary>
    /// This method is used to mark original sale/credit transaction COMPLETED after voiding
    /// 
    /// This method will update the status of an existing request record in the 
    /// TG_SYSTEM_INTERFACE_TRACKING table based on the reference number (e.g.,
    /// TROUTD in IPCharge, GatewayTxnId in Heartland) sent.
    /// If a record is successfully updated then true is returned.
    /// If no record is found to update then true is returned.
    /// If an error occurs then false is returned and an error message is set in the response.
    /// 
    /// NOTE: This method is used for updating the status of a record that has been
    /// been SUCCESSFULLY voided and marked completed; This method makes certain that
    /// the ORIGINAL SALE record is also marked COMPLETED.
    /// </summary>
    /// <param name="Reference"></param>
    /// <param name="recStatus"></param>
    /// <returns></returns>
    public bool UpdateRecord(string Reference, Status recStatus)
    {
      string sqlStatement = string.Empty;
      SqlCommand sqlComm = null;

      try
      {
        // Get the SQL Connection
        if (_sqlConn == null)
        {
          _sqlConn = Helper.GetConnection(_strConn);
        }
        else
        {
          // If connection is not null then make certain the connection string is correct.
          if (_strConn.IndexOf(_sqlConn.ConnectionString) == -1)
          {
            _sqlConn.Dispose();
            _sqlConn = Helper.GetConnection(_strConn);
          }
          else
          {
            // If connection string is correct then make certain the connection is open
            if (_sqlConn.State == ConnectionState.Closed)
            {
              _sqlConn.Open();
            }
          }
        }

        // Create the SQL Command
        sqlComm = Helper.GetCommand(_sqlConn, sqlStatement, CommandType.Text);

        // Add the parameters
        Helper.AddParameter(sqlComm, "SystemType", _cardTrans._trackerInfo.SystemInterfaceID, SqlDbType.VarChar);
        Helper.AddParameter(sqlComm, "CurrentDateTime", DateTime.Now, SqlDbType.DateTime);
        Helper.AddParameter(sqlComm, "SessionID", _cardTrans._trackerInfo.SessionID, SqlDbType.VarChar);
        Helper.AddParameter(sqlComm, "Reference", Reference, SqlDbType.VarChar);
        string sStatusID = (recStatus == Status.NONE) ? "" : recStatus.ToString();

        // Build SQL to update record in TG_SYSTEM_INTERFACE_TRACKING table:
        StringBuilder sb = new StringBuilder();
        sb.Append("UPDATE TG_SYSTEM_INTERFACE_TRACKING SET ");
        sb.Append("LAST_DATETIME = @CurrentDateTime, LAST_SESSION_ID = @SessionID");
        if (sStatusID != "")
        {
          Helper.AddParameter(sqlComm, "Status", sStatusID, SqlDbType.VarChar);
          sb.Append(", STATUS = @Status");
        }
        sb.Append(" WHERE ");
        sb.Append("REFERENCE = @Reference AND ");
        sb.Append("SYSTEM_TYPE = @SystemType AND ");
        sb.Append("STATUS <> 'COMPLETED'"); // Treat completed requests as nonexistant
        sqlStatement = sb.ToString();

        // Set SQL statement to command
        sqlComm.CommandText = sqlStatement;

        // Execute command
        sqlComm.ExecuteNonQuery();
      }
      catch (Exception ex)
      {
        string err = "Cannot update record in TG_SYSTEM_INTERFACE_TRACKING table:";
#if DEBUG
        if (!string.IsNullOrEmpty(sqlStatement))
        {
          err = err + (string.Format("\r\nSQL Statement:\r\n{0}", sqlStatement));
        }
#endif
        //_cardTrans.SetRespError(PaymentGateway.ErrorType.GatewayConnector, "SIT-0000", err, ex);
        _cardTrans.LogError(ex, err);
        return false;
      }
      finally
      {
        if (sqlComm != null)
        {
          sqlComm.Dispose();
        }
        if (_sqlConn != null)
        {
          _sqlConn.Close();
        }
      }
      return true;
    }

    Activity GetActivity(CardActionEnum Action)
    {
      switch (Action)
      {
        case CardActionEnum.CreditSale:
        case CardActionEnum.DebitSale:
          return Activity.SALE;

        case CardActionEnum.CreditVoid:
        case CardActionEnum.DebitVoid:
          return Activity.VOID;

        case CardActionEnum.CreditReturn:
          return Activity.CREDIT;
      }
      return Activity.NONE;
    }
  }

  public static class Helper
  {
    /// <summary>
    /// creates and open a sqlconnection
    /// </summary>
    /// <param name="connectionString">
    /// A <see cref="System.String"/> that contains the sql connection parameters
    /// </param>
    /// <returns>
    /// A <see cref="SqlConnection"/> 
    /// </returns>
    public static SqlConnection GetConnection(string connectionString)
    {
      SqlConnection connection = null;
      try
      {
        connection = new SqlConnection(connectionString);
        connection.Open();
      }
      catch
      {
        // Dispose of the connection to avoid connections leak
        if (connection != null)
        {
          connection.Dispose();
        }
        throw;
      }
      return connection;
    }

    /// <summary>
    /// Creates a sqlcommand
    /// </summary>
    /// <param name="connection">
    /// A <see cref="SqlConnection"/>
    /// </param>
    /// <param name="commandText">
    /// A <see cref="System.String"/> of the sql query.
    /// </param>
    /// <param name="commandType">
    /// A <see cref="CommandType"/> of the query type.
    /// </param>
    /// <returns>
    /// A <see cref="SqlCommand"/>
    /// </returns>
    public static SqlCommand GetCommand(this SqlConnection connection, string commandText, CommandType commandType)
    {
      SqlCommand command = connection.CreateCommand();
      command.CommandTimeout = connection.ConnectionTimeout;
      command.CommandType = commandType;
      command.CommandText = commandText;
      return command;
    }

    /// <summary>
    /// Adds a parameter to the command parameter array.
    /// </summary>
    /// <param name="command">
    /// A <see cref="SqlCommand"/> 
    /// </param>
    /// <param name="parameterName">
    /// A <see cref="System.String"/> of the named parameter in the sql query.
    /// </param>
    /// <param name="parameterValue">
    /// A <see cref="System.Object"/> of the parameter value.
    /// </param>
    /// <param name="parameterSqlType">
    /// A <see cref="SqlDbType"/>
    /// </param>
    public static void AddParameter(this SqlCommand command, string parameterName, object parameterValue, SqlDbType parameterSqlType)
    {
      if (!parameterName.StartsWith("@"))
      {
        parameterName = "@" + parameterName;
      }
      command.Parameters.Add(parameterName, parameterSqlType);
      command.Parameters[parameterName].Value = parameterValue;
    }
  }
}
