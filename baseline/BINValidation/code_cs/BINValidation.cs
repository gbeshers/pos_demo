﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BINValidation.BINValidationServiceReference;
using CASL_engine;
using System.Net;

// Bug 17049 MJO - BIN Validation web service client
namespace BINValidation
{
  public class BINValidationInquiry
  {
    // Bug 17049 MJO - Return true if this is a debit or check card
    public static Object IsDebit(params Object[] arg_pairs)
    {
      GenericObject geoARGS = misc.convert_args(arg_pairs);
      string BIN = (string)geoARGS.get("BIN");
      string address = (string)geoARGS.get("web_service_address");
      bool ignoreCertificateErrors = (bool)geoARGS.get("ignore_certificate_errors");

      if (ignoreCertificateErrors)
      {
        ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
      }
      else
      {
        ServicePointManager.ServerCertificateValidationCallback = null;
      }

      // Bug 19576 - Put client in using block
      BINResult result;

      using (BINValidationServiceClient client = new BINValidationServiceClient("BasicHttpsBinding_IBINValidationService", address))
        result = client.GetBINInfo(BIN, "");

      return result != null && (result.Check || result.Debit);
    }
    // Bug 17301 
    public static Object validateBIN(params Object[] arg_pairs)
    {
      GenericObject geoARGS = misc.convert_args(arg_pairs);
      string BIN = (string)geoARGS.get("BIN");
      string BINTYPE = (string)geoARGS.get("BINTYPE");
      string address = (string)geoARGS.get("web_service_address");
      bool ignoreCertificateErrors = (bool)geoARGS.get("ignore_certificate_errors");

      if (ignoreCertificateErrors)
      {
        ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
      }
      else
      {
        ServicePointManager.ServerCertificateValidationCallback = null;
      }

      // Bug 19576 - Put client in using block
      BINResult result;

      using (BINValidationServiceClient client = new BINValidationServiceClient("BasicHttpsBinding_IBINValidationService", address))
          result = client.GetBINInfo(BIN, "");

      if(BINTYPE=="FSA")
        return result != null && (result.Check || result.HSA_FSA);
      return false;
    }
  }
}
