﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using CASL_engine;
using System.Collections;

// Bug #15860 Mike O - Open File Scheduled Job

namespace baseline
{
  /// <summary>
  /// Opens a new file in each of the selected departments
  /// </summary>
  class OpenFileJob : ScheduledJob
  {
    public override string type
    {
      get { return "open_file"; }
    }
    public override string description
    {
      get { return "Open File"; }
    }

    // Bug IPAY-16 MJO - Arguments are now passed in in their original CASL form
    public override void Execute(Hashtable args)
    {
      GenericObject departments = args["departments"] as GenericObject;

      foreach (GenericObject department in departments.getObjectKeys())
      {
        object dateCurrent = misc.CASL_call("_subject", c_CASL.execute("Datetime"), "a_method", "current", "args", new GenericObject());
        object date = misc.CASL_call("_subject", dateCurrent, "a_method", "to_ipayment_format", "args", new GenericObject());
        object description = misc.CASL_call("_subject", dateCurrent, "a_method", "get_formatted", "args", new GenericObject("format", "MM/dd/yyyy"));
        // Bug 19519 MJO - Use file type from workgroup configuration
        string fileType = (department.get("file_type") as string) == "Shared" ? "S" : "I";

        // Bug 19519 MJO - Use file type from workgroup configuration
        misc.CASL_call("_subject", c_CASL.c_object("Core_file"), "a_method", "sv_create_new_file",
          "args", new GenericObject("department", department.get("_name"), "user", "INTERNET", "creator_id", "INTERNET", "type", fileType,
          "effective_date", date, "description", description, "login_id", "INTERNET"));
      }
    }
  }
}
