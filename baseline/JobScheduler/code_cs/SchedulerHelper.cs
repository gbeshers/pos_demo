﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using CASL_engine;
using System.Reflection;

// Bug #13092 Mike O - Misc. helper methods for the job scheduler (CASL communicates with it through here)

// Bug 20467 UMN Support import Jobs

namespace baseline
{
  static class SchedulerHelper
  {
    private static JobScheduler scheduler = new JobScheduler();
    private static Hashtable jobTypes = null;

    /// <summary>
    /// Creates or updates the provided job in the scheduler (CASL-friendly function)
    /// </summary>
    /// <param name="arg_pairs">
    /// job=Business.Job to be (re)scheduled
    /// </param>
    /// <returns>Whether or not the job was successfully updated</returns>
    public static Object UpdateJob(params object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject job = (GenericObject)args.get("job", null);

      return UpdateJob(job);
    }

    /// <summary>
    /// Creates or updates the provided job in the scheduler
    /// </summary>
    /// <param name="job">
    /// Business.Job to be (re)scheduled
    /// </param>
    /// <returns>Whether or not the job was successfully updated</returns>
    private static bool UpdateJob(GenericObject job)
    {
      string id = job.get("id", "").ToString();
      bool deleted = (bool)job.get("_deleted", false);
      bool success = true;

      bool isOldJob = !job.has("start_date");

      // Bug 21475 UMN delete job if user sets time to empty string
      string date;
      
      if (isOldJob)
        date = DateTime.Now.ToShortDateString();
      else
        date = (string)job.get("start_date", "");

      string tod = (string)job.get("time_of_day", "");  // Todd the politest psychopath ever.

      if (String.IsNullOrWhiteSpace(tod))
        tod = "12:00AM";

      DateTime time;

      if (deleted || String.IsNullOrWhiteSpace(date) || !DateTime.TryParse(date + " " + tod, out time))
      {
        scheduler.removeJob(id);
      }
      else
      {
      	// Bug 26519 MJO - Moved argument construction into job execution function
        JobScheduler.Frequency frequency;

        if (isOldJob)
        {
          frequency = JobScheduler.Frequency.Daily;
        }
        else if (!Enum.TryParse<JobScheduler.Frequency>((string)job.get("frequency"), out frequency))
        {
          Logger.cs_log(String.Format("Error creating or updating Hangfire job {1}: Unable to parse frequency '{0}'", job.get("frequency"), id));
          return false;
        }

        success = scheduler.updateJob(id, time, frequency);
      }

      return success;
    }

    /// <summary>
    /// Bug 21402: Add Scheduled Job To Run Auto Batch Update
    /// Returns a comma delimited string of System Interface IDs based on the "batch_systems" selected.
    /// If no "batch_systems" are selected then the "all" string is returned.
    /// </summary>
    /// <param name="job"></param>
    /// <returns></returns>
    public static string getBatchSystemsString(GenericObject job)
    {
      string batchSystems = null;
      GenericObject systems = (GenericObject)job.get("batch_systems", null);
      if (systems != null)
      {
        foreach (GenericObject geoSI in ((GenericObject)systems).vectors)
        {
          string si = geoSI.get("_name", null, true) as string;
          if (si != null)
          {
            batchSystems = batchSystems == null ? si : string.Format("{0},{1}", batchSystems, si);
          }
        }
      }
      if (batchSystems == null) { batchSystems = "all"; }
      return batchSystems;
    }

    /// <summary>
    /// String together tuples for the DETAILS field of the
    /// activity log.
    /// Rememeber that each tuple consists of a Name:Value
    /// and is separated from the the other tuples with a 
    /// comma.  To add commas to the output, use vertical bars.
    /// </summary>
    /// <param name="sKey"></param>
    /// <param name="sValue"></param>
    /// <param name="sDetails"></param>
    private static void AddToActLogDetails(string sKey, string sValue, ref string sDetails)
    {
      // Replace any commas with pipes in Value:
      sValue = sValue.Replace(',', '|');

      sDetails = string.Format("{0}{1}:{2}"
      , string.IsNullOrEmpty(sDetails) ? "" : string.Format("{0},", sDetails)
      , sKey
      , sValue);
    }


    /// <summary>
    /// Immediately execute the provided job (CASL-friendly function)
    /// </summary>
    /// <param name="arg_pairs">
    /// job=Business.Job to be (re)scheduled
    /// </param>
    /// <returns>An error string if an error occurred, or null if successful</returns>
    // Bug IPAY-15 MJO - Rewrote to support arbitrary arguments
    public static string ExecuteJob(params object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject job = (GenericObject)args.get("job", null);
      GenericObject jobType = job.get("_parent") as GenericObject;

      ScheduledJob scheduledJob = GetJobTypeInstance((string)jobType.get("_name"));
      GenericObject jobConfigFields = jobType.get("_config_field_order") as GenericObject;

      // Bug 22209
      string sDetails = "";

      Hashtable tbl = new Hashtable();

      for (int i = 0; i < jobConfigFields.getLength(); i++)
      {
        string configKey = jobConfigFields.get(i).ToString();
        object configValue = job.get(configKey, null, true); // Bug IPAY-16 MJO - Support default values
        
        // Bug IPAY-1088 MJO - Field may not be set
        if (configValue != null)
        {
          if (typeof(GenericObject) == configValue.GetType())
          {
            List<string> ids = new List<string>();

            foreach (GenericObject geo in ((GenericObject)configValue).getObjectKeys())
            {
              string primaryKey = geo.get("_primary_key", "id").ToString();

              if (!geo.has(primaryKey))
                primaryKey = "_name";

              ids.Add(geo.get(primaryKey).ToString());
            }

            AddToActLogDetails(configKey, String.Join(", ", ids.ToArray()), ref sDetails);
          }
          else
          {
            AddToActLogDetails(configKey, configValue.ToString(), ref sDetails);
          }
        }

        tbl.Add(configKey, configValue);
      }

      // Bug 21339 [20038]: Balance Files and Clean Files Jobs are Failing with an Error [Added BEGIN/END logging]
      bool err = false;
      try
      {
        Logger.cs_log(string.Format("BEGIN [Execute Now Button]: Scheduled Job {0}", scheduledJob.description));

        // Bug 22209
        misc.CASL_call("a_method", "actlog", "args",
        new GenericObject(
          "app", "Config"
          // Bug 20435 UMN PCI-DSS don't pass session
          , "user", Logger.GetUserId()
          , "action", "Manually executed Job"
          , "summary", scheduledJob.description
          , "detail", sDetails
          )
          );
        // End Bug 22209

        scheduledJob.Execute(tbl);
        return null;
      }
      catch (Exception e)
      {
        // Bug 21339 [20038] (17835) UMN add exception logging
        err = true;
        Logger.cs_log(string.Format("ERROR [Execute Now Button]: Scheduled Job {0} failed to execute successfully{1}{2}"
        , scheduledJob.description, Environment.NewLine, e.ToMessageAndCompleteStacktrace()));
        string msg = String.Format("Exception for {0}: {1}", (string)job.get("_name"), e.Message);
        Logger.Log(msg, "ExecuteJob Failed", "err");

        // BEGIN Bug 21402: Add Scheduled Job To Run Auto Batch Update
        string caslErrMsg = "";
        if (e is CASL_error)
        {
          CASL_error ce = (CASL_error)e;
          caslErrMsg = ce.casl_object == null ? "" : (ce.casl_object.get("message", "") as string);
        }
        return (string.Format("{0}{1}{2}", e.Message, string.IsNullOrEmpty(caslErrMsg) ? "" : "<br/>", caslErrMsg)).Trim();
        // END Bug 21402: Add Scheduled Job To Run Auto Batch Update
      }
      finally
      {
        Logger.cs_log(string.Format("END   [Execute Now Button]: Scheduled Job {0}{1}", scheduledJob.description, (err ? "" : " was executed successfully")));
      }
    }

    /// <summary>
    /// Get the ScheduledJob of the given type
    /// </summary>
    /// <param name="typeName">
    /// The ScheduledJob "type" property
    /// </param>
    /// <returns>An instance of the ScheduledJob matching the typeName</returns>
    public static ScheduledJob GetJobTypeInstance(string typeName)
    {
      if (jobTypes == null)
        CacheTypes();

      if (!jobTypes.Contains(typeName))
        throw new KeyNotFoundException("Scheduled job type '" + typeName + "' not found.");

      return (ScheduledJob)Activator.CreateInstance((Type)jobTypes[typeName]);
    }

    /// <summary>
    /// Cache all the ScheduledJob subclasses by type property, for easy lookup later
    /// </summary>
    private static void CacheTypes()
    {
      var type = typeof(ScheduledJob);
      var types = AppDomain.CurrentDomain.GetAssemblies().ToList()
              // Bug 25649 MJO - Ignore exceptions
              .SelectMany(new Func<Assembly, IEnumerable<Type>>(s =>
              {
                try
                {
                  return s.GetTypes();
                }
                catch (Exception)
                {
                  return new List<Type>();
                }
              }))
      .Where(p => type.IsAssignableFrom(p) && p != type);

      GenericObject Job = c_CASL.c_object("Business.Job") as GenericObject;

      jobTypes = new Hashtable();
      foreach (Type t in types)
      {
        ScheduledJob tempJob = (ScheduledJob)Activator.CreateInstance(t);
        jobTypes.Add(tempJob.type, t);

        if (!Job.has(tempJob.type))
          Logger.cs_log($"Warning: Job type {tempJob.type} is defined in C# but does not have a corresponding CASL Job subclass");
      }
    }

    /// <summary>
    /// Dynamically construct Business.Job.type_f_type.
    /// This enables new test types to be added without having to change any CASL code
    /// </summary>
    /// <returns>A one_of instance containing all possible ScheduledJob types (in the form of OPTIONs)</returns>
    public static GenericObject BuildJobTypes(params Object[] args)
    {
      if (jobTypes == null)
        CacheTypes();

      GenericObject types = c_CASL.c_instance("v");

      // Bug 23198 [20901] DJD - Sort the Job Types alphabetically
      List<string> jobTypesSorted = new List<string>();
      foreach (string jt in jobTypes.Keys)
      {
        jobTypesSorted.Add(jt);
      }
      jobTypesSorted.Sort();

      foreach (string jobType in jobTypesSorted)
      {
        ScheduledJob job = GetJobTypeInstance(jobType);

        object jobClass = c_CASL.c_object("Business.Job." + job.type);

        if (typeof(GenObj_Class) == jobClass.GetType())
          types.insert(c_CASL.c_instance("OPTION", "value", jobClass, 0, job.description));
      }

      return types;
    }
  }
}
