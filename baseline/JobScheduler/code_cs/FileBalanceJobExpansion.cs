﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using CASL_engine;
using System.Data.SqlClient;

namespace baseline
{
  /// <summary>
  /// Bug 23198 [20901] DJD - File Balancing Job Expansion:
  /// Balance Inactive Files:    This job will mark all open files with NO activity as balanced for the targeted workgroups.
  /// Balance Deposited Files:   This job will mark all open files with deposits as balanced if the posted total - deposited total is equal to zero for 
  ///                            the targeted workgroups.
  /// Balance Zero Dollar Files: This job will mark all open files with no deposits and a net posted total of zero as balanced for the targeted workgroups.  
  ///                            This would include files that have only voided (or reversed) transactions and tenders.
  ///                            
  /// NOTE: None of the jobs listed above will create deposits - they are ALL open CORE files that (in effect) have a zero balance.
  /// </summary>
  public class FileBalanceJobExpansion
  {
    /// <summary>
    /// Enumerated type used to identify the type of balancing: Inactive, Deposited, or Zero_Dollar.
    /// </summary>
    public enum FileState { Inactive, Deposited, Zero_Dollar, Unknown };

    /// <summary>
    /// This "_impl" method balances Inactive files for the targeted workgroups.
    /// It can be called from a vb script or executed directly from a browser 
    /// (e.g., "http://localhost/geisinger_trunk_phase_2/Business/auto_balance_core_files_inactive_aux.htm?&"). 
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static string BalanceInactiveFiles(params Object[] arg_pairs)
    {
      GenericObject geo_args = misc.convert_args(arg_pairs);
      object objDepts = geo_args.get("departments", "all");
      string sReturn = BalanceInactiveFiles_aux(objDepts);
      return sReturn;
    }

    /// <summary>
    /// This method balances Inactive files for the targeted workgroups.
    /// It can be called from the iPayment "Jobs" module.
    /// </summary>
    /// <param name="objWkGrps"></param>
    /// <returns></returns>
    public static string BalanceInactiveFiles_aux(object objWkGrps)
    {
      return BalanceFiles(objWkGrps, FileState.Inactive);
    }

    /// <summary>
    /// This "_impl" method balances Deposited files for the targeted workgroups.
    /// It can be called from a vb script or executed directly from a browser 
    /// (e.g., "http://localhost/geisinger_trunk_phase_2/Business/auto_balance_core_files_deposited_aux.htm?&"). 
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static string BalanceDepositedFiles(params Object[] arg_pairs)
    {
      GenericObject geo_args = misc.convert_args(arg_pairs);
      object objDepts = geo_args.get("departments", "all");
      string sReturn = BalanceDepositedFiles_aux(objDepts);
      return sReturn;
    }

    /// <summary>
    /// This method balances Deposited files for the targeted workgroups.
    /// It can be called from the iPayment "Jobs" module.
    /// </summary>
    /// <param name="objWkGrps"></param>
    /// <returns></returns>
    public static string BalanceDepositedFiles_aux(object objWkGrps)
    {
      return BalanceFiles(objWkGrps, FileState.Deposited);
    }

    /// <summary>
    /// This "_impl" method balances Zero Dollar files for the targeted workgroups.
    /// It can be called from a vb script or executed directly from a browser 
    /// (e.g., "http://localhost/geisinger_trunk_phase_2/Business/auto_balance_core_files_zero_dollar_aux.htm?&"). 
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static string BalanceZeroDollarFiles(params Object[] arg_pairs)
    {
      GenericObject geo_args = misc.convert_args(arg_pairs);
      object objDepts = geo_args.get("departments", "all");
      string sReturn = BalanceZeroDollarFiles_aux(objDepts);
      return sReturn;
    }

    /// <summary>
    /// This method balances Zero Dollar files for the targeted workgroups.
    /// It can be called from the iPayment "Jobs" module.
    /// </summary>
    /// <param name="objWkGrps"></param>
    /// <returns></returns>
    public static string BalanceZeroDollarFiles_aux(object objWkGrps)
    {
      return BalanceFiles(objWkGrps, FileState.Zero_Dollar);
    }

    /// <summary>
    /// This method will call the suitable "GetFileListFromDB" method for the FileState sent (Inactive, Deposited, Zero_Dollar) and then
    /// call the "auto_balance_core_file_aux" CASL method to balance each file.  A SUCCESS/FAILURE result string is returned and logged.
    /// </summary>
    /// <param name="objWkGrps"></param>
    /// <param name="fs"></param>
    /// <returns></returns>
    private static string BalanceFiles(object objWkGrps, FileState fs)
    {
      string sReturn = "";
      List<string> coreFileIDList = new List<string>();
      string fileState = fs.ToString().Replace('_', ' ');

      try
      {
        // Get the where clause and parameteres for the targeted workgroups
        List<SqlParameter> sqlParamList = new List<SqlParameter>();
        string sqlWkGrpWhereClause = "";
        if (!GetWorkGroupWhereClause(objWkGrps, fileState, ref sReturn, sqlParamList, ref sqlWkGrpWhereClause))
        {
          return sReturn;
        }

        switch (fs) 
        {
          case FileState.Inactive:
            GetInactiveFileListFromDB(coreFileIDList, sqlParamList, sqlWkGrpWhereClause);
            break;

          case FileState.Deposited:
            GetDepositedFileListFromDB(coreFileIDList, sqlParamList, sqlWkGrpWhereClause);
            break;

          case FileState.Zero_Dollar:
            GetZeroDollarFileListFromDB(coreFileIDList, sqlParamList, sqlWkGrpWhereClause);
            break;
        }

        if (coreFileIDList.Count == 0)
        {
          sReturn = string.Format("SUCCESS: No CORE File is found for Auto Balance {0} Files.", fileState);
          return sReturn;
        }

        int balancingErrorCount = 0;
        foreach (string coreFileID in coreFileIDList)
        {
          object result = misc.CASL_call(
           "_subject", c_CASL.execute("Business"),
           "a_method", "auto_balance_core_file_aux",
           "args", new GenericObject("core_file_id", coreFileID)
           );
          if (IsError(result))
          {
            balancingErrorCount++;
            Logger.cs_log(string.Format("Failed to Auto Balance {0} File: {1}{2}{3}"
              , fileState
              , coreFileID
              , Environment.NewLine
              , GetErrorMessage(result)));
          }
          else
          {
            Logger.cs_log(string.Format("Successfully Auto Balanced {0} File: {1}", fileState, coreFileID));
          }
        }

        if (balancingErrorCount > 0)
        {
          if (balancingErrorCount == coreFileIDList.Count)
          {
            sReturn = string.Format("FAILURE: Failed to Auto Balance {0} {1} Files.", balancingErrorCount, fileState);
          }
          else // Partial failure
          {
            sReturn = string.Format("FAILED to Auto Balance {0} out of {1} {2} Files.", balancingErrorCount, coreFileIDList.Count, fileState);
          }
        }
        else
        {
          sReturn = string.Format("SUCCESS: Auto Balanced {0} {1} Files.", coreFileIDList.Count, fileState);
        }
      }
      catch (Exception ex)
      {
        sReturn = string.Format("FAILURE: Error during method to Auto Balance {0} Files.", fileState);
        Logger.cs_log(string.Format("{0}", ex.ToMessageAndCompleteStacktrace()));
      }
      finally
      {
        Logger.cs_log(sReturn);
      }
      return sReturn;
    }

    /// <summary>
    /// Queries DB and populates a list of CORE File IDs (coreFileIDList) for those files that meet the "Inactive" status.
    /// </summary>
    /// <param name="coreFileIDList"></param>
    /// <param name="sqlParamList"></param>
    /// <param name="sqlWkGrpWhereClause"></param>
    private static void GetInactiveFileListFromDB(List<string> coreFileIDList, List<SqlParameter> sqlParamList, string sqlWkGrpWhereClause)
    {
      SqlConnection dbConnection = null;
      SqlDataReader dataReader = null;
      string sql = "";
      sql = string.Format(
      @"SELECT [FILENAME]
        FROM TG_DEPFILE_DATA f 
        WHERE
          (f.OPENDT IS NOT NULL AND f.CLOSEDT IS NULL AND f.BALDT IS NULL AND f.UPDATEDT IS NULL)
          {0}
          AND (NOT EXISTS (SELECT e.DEPFILENBR, e.DEPFILESEQ FROM TG_PAYEVENT_DATA e WHERE f.DEPFILENBR = e.DEPFILENBR AND f.DEPFILESEQ = e.DEPFILESEQ))"
          , string.IsNullOrEmpty(sqlWkGrpWhereClause) ? "" : string.Format("AND ({0})", sqlWkGrpWhereClause));
      try
      {
        ExecuteSQLDataReader(ref dbConnection, ref dataReader, sqlParamList, sql);
        while (dataReader.Read())
        {
          string coreFileID = dataReader.GetString(dataReader.GetOrdinal("FILENAME"));
          coreFileIDList.Add(coreFileID);
        }
      }
      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
        if (dbConnection != null)
        {
          dbConnection.Close();
        }
      }
    }

    /// <summary>
    /// Queries DB and populates a list of CORE File IDs (coreFileIDList) for those files that meet the "Deposited" status.
    /// </summary>
    /// <param name="coreFileIDList"></param>
    /// <param name="sqlParamList"></param>
    /// <param name="sqlWkGrpWhereClause"></param>
    private static void GetDepositedFileListFromDB(List<string> coreFileIDList, List<SqlParameter> sqlParamList, string sqlWkGrpWhereClause)
    {
      // Get all open CORE files for the targeted workgroups that have deposits and add to dictionary.
      Dictionary<string, COREFILE> coreFileWithDepDic = new Dictionary<string, COREFILE>(); // Key is "CoreFileNbr|CoreFileSeq" (eg "2014055|3")
      SqlParameter spActEvent = param("eventstatus", SqlDbType.VarChar, "1"); // Exclude any CORE files with active events (status = 1) (add parameter)
      sqlParamList.Add(spActEvent);
      string sql = string.Format(
      @"SELECT [FILENAME], [DEPFILENBR], [DEPFILESEQ]
        FROM TG_DEPFILE_DATA f 
        WHERE
          (f.OPENDT IS NOT NULL AND f.CLOSEDT IS NULL AND f.BALDT IS NULL AND f.UPDATEDT IS NULL)
          {0}
          AND (EXISTS (SELECT d.DEPFILENBR, d.DEPFILESEQ FROM TG_DEPOSIT_DATA d WHERE f.DEPFILENBR = d.DEPFILENBR AND f.DEPFILESEQ = d.DEPFILESEQ))
          AND (NOT EXISTS (SELECT e.DEPFILENBR, e.DEPFILESEQ FROM TG_PAYEVENT_DATA e WHERE f.DEPFILENBR = e.DEPFILENBR AND f.DEPFILESEQ = e.DEPFILESEQ AND e.STATUS=@eventstatus))"
          , string.IsNullOrEmpty(sqlWkGrpWhereClause) ? "" : string.Format("AND ({0})", sqlWkGrpWhereClause));
      AddOpenFilesToDictionary(sql, sqlParamList, sqlWkGrpWhereClause, coreFileWithDepDic);

      // Return if no files are initially found
      if (coreFileWithDepDic.Count == 0)
      {
        return;
      }
      
      List<SqlParameter> sqlCoreFileParamList = new List<SqlParameter>();
      string sqlCoreFileWhereClause = GetCoreFilesWithDepositsWhereClause(coreFileWithDepDic, sqlCoreFileParamList);

      // Get the deposit total amounts for the CORE files with deposits and add to dictionary
      GetDepositTotalsFromDB(coreFileWithDepDic, sqlCoreFileParamList, sqlCoreFileWhereClause);

      // Get the expected deposit total amounts for the CORE files with deposits and add to dictionary
      GetExpectedDepositTotalsFromDB(coreFileWithDepDic, sqlCoreFileParamList, sqlCoreFileWhereClause);

      // Add the CORE files to the coreFileIDList where the expected deposit amt equals the deposit amt
      foreach (KeyValuePair<string, COREFILE> cfEntry in coreFileWithDepDic)
      {
        if (cfEntry.Value.AMT_2 == cfEntry.Value.AMT_1) 
        {
          coreFileIDList.Add(cfEntry.Value.NAME);          
        }
      }
    }

    /// <summary>
    /// Queries the TG_DEPOSIT_DATA table and populates the "AMT_1" property in the "coreFilesDic" dictionary with the 
    /// Deposit amount total for each CORE file in the dictionary.
    /// </summary>
    /// <param name="coreFileWithDepDic"></param>
    /// <param name="sqlCoreFileParamList"></param>
    /// <param name="sqlCoreFileWhereClause"></param>
    private static void GetDepositTotalsFromDB(Dictionary<string, COREFILE> coreFilesDic, List<SqlParameter> sqlCoreFileParamList, string sqlCoreFileWhereClause)
    {
      SqlParameter spDepType = param("deposittype", SqlDbType.VarChar, "PRE_DEPOSIT"); // Exclude the PRE_DEPOSIT deposit type (add parameter)
      sqlCoreFileParamList.Add(spDepType);
      SqlConnection dbConnection = null;
      SqlDataReader dataReader = null;
      string sqlDeposits =
        string.Format(@"SELECT [DEPFILENBR], [DEPFILESEQ], SUM(AMOUNT) AS DEPOSIT_TOTAL
                        FROM TG_DEPOSIT_DATA
                        WHERE 
                        (VOIDDT IS NULL) AND NOT(DEPOSITTYPE=@deposittype) AND
                        ({0})
                        GROUP BY [DEPFILENBR], [DEPFILESEQ]", sqlCoreFileWhereClause);
      try
      {
        ExecuteSQLDataReader(ref dbConnection, ref dataReader, sqlCoreFileParamList, sqlDeposits);
        while (dataReader.Read())
        {
          decimal amt = System.Convert.ToDecimal(dataReader.GetDouble(dataReader.GetOrdinal("DEPOSIT_TOTAL")));
          int nbr = dataReader.GetInt32(dataReader.GetOrdinal("DEPFILENBR"));
          int seq = dataReader.GetInt32(dataReader.GetOrdinal("DEPFILESEQ"));
          string key = GetCoreFileKey(nbr, seq);
          if (coreFilesDic.ContainsKey(key))
          {
            coreFilesDic[key].AMT_1 = amt;
          }
        }
      }
      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
        if (dbConnection != null)
        {
          dbConnection.Close();
        }
      }
      // Remove the "PRE_DEPOSIT" deposit type parameter from the sqlCoreFileParamList (last item added)
      sqlCoreFileParamList.RemoveAt(sqlCoreFileParamList.Count - 1);
    }

    /// <summary>
    /// Queries the TG_TENDER_DATA table and populates the "AMT_2" property in the "coreFilesDic" dictionary with the
    /// Expected Deposit amount total for each CORE file in the dictionary.
    /// IMPORTANT: Need to consider each tender INDIVIDUALLY because there could be offsetting tenders (10.00 cash and 
    /// -10.00 Credit Card) in a Zero-Dollar file.  This is a really improbable scenario but it could happen.
    /// </summary>
    /// <param name="coreFileWithDepDic"></param>
    /// <param name="sqlCoreFileParamList"></param>
    /// <param name="sqlCoreFileWhereClause"></param>
    private static void GetExpectedDepositTotalsFromDB(Dictionary<string, COREFILE> coreFilesDic, List<SqlParameter> sqlCoreFileParamList, string sqlCoreFileWhereClause)
    {
      SqlParameter spTndrID = param("tenderid", SqlDbType.VarChar, "SYSOVERSHORT"); // Exclude the 'SYSOVERSHORT' tender type (add parameter)
      sqlCoreFileParamList.Add(spTndrID);
      SqlConnection dbConnection = null;
      SqlDataReader dataReader = null;
      string sqlDeposits =
        string.Format(@"SELECT [DEPFILENBR], [DEPFILESEQ], SUM(AMOUNT) AS EXPECTED_DEPOSIT_TOTAL
                        FROM TG_TENDER_DATA
                        WHERE
                        (VOIDDT IS NULL) AND NOT(TNDRID=@tenderid) AND
                        ({0})
                        GROUP BY [DEPFILENBR], [DEPFILESEQ], [TNDRID]", sqlCoreFileWhereClause);
      try
      {
        ExecuteSQLDataReader(ref dbConnection, ref dataReader, sqlCoreFileParamList, sqlDeposits);
        while (dataReader.Read())
        {
          decimal amt = System.Convert.ToDecimal(dataReader.GetDouble(dataReader.GetOrdinal("EXPECTED_DEPOSIT_TOTAL")));
          int nbr = dataReader.GetInt32(dataReader.GetOrdinal("DEPFILENBR"));
          int seq = dataReader.GetInt32(dataReader.GetOrdinal("DEPFILESEQ"));
          string key = GetCoreFileKey(nbr, seq);
          if (coreFilesDic.ContainsKey(key) && coreFilesDic[key].AMT_2 == 0.00M)
          {
            coreFilesDic[key].AMT_2 = amt;
          }
        }
      }
      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
        if (dbConnection != null)
        {
          dbConnection.Close();
        }
      }
    }

    /// <summary>
    /// Iterates over the coreFileWithDepDic dictionary building the "where" clause 
    /// and populating the corresponding parameters in the sqlCoreFileParamList.
    /// </summary>
    /// <param name="coreFileWithDepDic"></param>
    /// <param name="sqlCoreFileParamList"></param>
    /// <returns></returns>
    private static string GetCoreFilesWithDepositsWhereClause(Dictionary<string, COREFILE> coreFileWithDepDic, List<SqlParameter> sqlCoreFileParamList)
    {
      string sqlWhereClause = "";
      int x = 0;
      foreach (KeyValuePair<string, COREFILE> cfEntry in coreFileWithDepDic)
      {
        // Ex: (DEPFILENBR=2014169 AND DEPFILESEQ=1) OR ... [cfEntry.Value or cfEntry.Key]
        string paramNameNbr = string.Format("CF_Nbr{0}", x.ToString("000000"));
        string paramNameSeq = string.Format("CF_Seq{0}", x.ToString("000000"));
        sqlWhereClause +=
          string.Format("{2}(DEPFILENBR=@{0} AND DEPFILESEQ=@{1})"
          , paramNameNbr, paramNameSeq, x == 0 ? "" : " OR ");
        SqlParameter spNbr = param(paramNameNbr, SqlDbType.Int, cfEntry.Value.NBR);
        SqlParameter spSeq = param(paramNameSeq, SqlDbType.Int, cfEntry.Value.SEQ);
        sqlCoreFileParamList.Add(spNbr);
        sqlCoreFileParamList.Add(spSeq);
        x++;
      }
      return sqlWhereClause;
    }

    /// <summary>
    /// Executes the SQL script sent and populates the "coreFilesDic" dictionary with the CORE files returned.
    /// </summary>
    /// <param name="sql"></param>
    /// <param name="sqlParamList"></param>
    /// <param name="sqlWkGrpWhereClause"></param>
    /// <param name="coreFilesDic"></param>
    private static void AddOpenFilesToDictionary(string sql, List<SqlParameter> sqlParamList, string sqlWkGrpWhereClause, Dictionary<string, COREFILE> coreFilesDic)
    {
      SqlConnection dbConnection = null;
      SqlDataReader dataReader = null;

      try
      {
        ExecuteSQLDataReader(ref dbConnection, ref dataReader, sqlParamList, sql);
        while (dataReader.Read())
        {
          string name = dataReader.GetString(dataReader.GetOrdinal("FILENAME"));
          int nbr     = dataReader.GetInt32(dataReader.GetOrdinal("DEPFILENBR"));
          int seq     = dataReader.GetInt32(dataReader.GetOrdinal("DEPFILESEQ"));
          COREFILE cf = new COREFILE(name, nbr, seq);
          string key = GetCoreFileKey(nbr, seq);
          coreFilesDic.Add(key, cf);
        }
      }
      finally
      {
        if (dataReader != null)
        {
          dataReader.Close();
        }
        if (dbConnection != null)
        {
          dbConnection.Close();
        }
      }
    }

    /// <summary>
    /// Builds "key" string for CORE file in this format: "CoreFileNbr|CoreFileSeq" (eg "2014055|3")
    /// </summary>
    /// <param name="nbr"></param>
    /// <param name="seq"></param>
    /// <returns></returns>
    private static string GetCoreFileKey(int nbr, int seq)
    {
      string key = string.Format("{0}|{1}", nbr, seq);
      return key;
    }

    /// <summary>
    /// Queries DB and populates a list of CORE File IDs (coreFileIDList) for those files that meet the "Zero Dollar" status.
    /// </summary>
    /// <param name="coreFileIDList"></param>
    /// <param name="sqlParamList"></param>
    /// <param name="sqlWkGrpWhereClause"></param>
    private static void GetZeroDollarFileListFromDB(List<string> coreFileIDList, List<SqlParameter> sqlParamList, string sqlWkGrpWhereClause)
    {
      // Get all open CORE files for the targeted workgroups that have events (but none that are NOT active) and have NO deposits and add to dictionary.
      Dictionary<string, COREFILE> coreFileEventsAndNoDepDic = new Dictionary<string, COREFILE>(); // Key is "CoreFileNbr|CoreFileSeq" (eg "2014055|3")
      SqlParameter spActEvent = param("eventstatus", SqlDbType.VarChar, "1"); // Exclude any CORE files with active events (status = 1) (add parameter)
      sqlParamList.Add(spActEvent);
      string sql = string.Format(
      @"SELECT [FILENAME], [DEPFILENBR], [DEPFILESEQ]
        FROM TG_DEPFILE_DATA f
        WHERE
          (f.OPENDT IS NOT NULL AND f.CLOSEDT IS NULL AND f.BALDT IS NULL AND f.UPDATEDT IS NULL)
          {0}
          AND (NOT EXISTS (SELECT d.DEPFILENBR, d.DEPFILESEQ FROM TG_DEPOSIT_DATA d WHERE f.DEPFILENBR = d.DEPFILENBR AND f.DEPFILESEQ = d.DEPFILESEQ))
          AND (NOT EXISTS (SELECT e.DEPFILENBR, e.DEPFILESEQ FROM TG_PAYEVENT_DATA e WHERE f.DEPFILENBR = e.DEPFILENBR AND f.DEPFILESEQ = e.DEPFILESEQ AND e.STATUS=@eventstatus))
          AND (EXISTS (SELECT e.DEPFILENBR, e.DEPFILESEQ FROM TG_PAYEVENT_DATA e WHERE f.DEPFILENBR = e.DEPFILENBR AND f.DEPFILESEQ = e.DEPFILESEQ))"
          , string.IsNullOrEmpty(sqlWkGrpWhereClause) ? "" : string.Format("AND ({0})", sqlWkGrpWhereClause));
      AddOpenFilesToDictionary(sql, sqlParamList, sqlWkGrpWhereClause, coreFileEventsAndNoDepDic);

      // Return if no files are initially found
      if (coreFileEventsAndNoDepDic.Count == 0)
      {
        return;
      }

      List<SqlParameter> sqlCoreFileParamList = new List<SqlParameter>();
      string sqlCoreFileWhereClause = GetCoreFilesWithDepositsWhereClause(coreFileEventsAndNoDepDic, sqlCoreFileParamList);

      // Get the expected deposit total amounts (i.e., tender totals) for the CORE files with deposits and add to dictionary
      GetExpectedDepositTotalsFromDB(coreFileEventsAndNoDepDic, sqlCoreFileParamList, sqlCoreFileWhereClause);

      // Add the CORE files to the coreFileIDList where the expected deposit amt equals zero
      foreach (KeyValuePair<string, COREFILE> cfEntry in coreFileEventsAndNoDepDic)
      {
        if (cfEntry.Value.AMT_2 == 0.00M)
        {
          coreFileIDList.Add(cfEntry.Value.NAME);
        }
      }
    }

    /// <summary>
    /// Executes the SQL script sent for the referenced SqlDataReader.
    /// </summary>
    /// <param name="dataReaderConnection"></param>
    /// <param name="dataReader"></param>
    /// <param name="sqlParamList"></param>
    /// <param name="sql"></param>
    private static void ExecuteSQLDataReader(ref SqlConnection dataReaderConnection, ref SqlDataReader dataReader, List<SqlParameter> sqlParamList, string sql)
    {
      string connectionString = GetDBConnectionString();
      dataReaderConnection = new SqlConnection(connectionString); // Open a connection for DataReader
      dataReaderConnection.Open();
      using (SqlCommand dataReaderCommand = new SqlCommand(sql, dataReaderConnection))
      {
        // Set parameters
        foreach (SqlParameter param in sqlParamList)
        {
          dataReaderCommand.Parameters.Add(param);
        }
        dataReader = dataReaderCommand.ExecuteReader(CommandBehavior.CloseConnection);

        // Clear any paramters [Prevents the Error: "The SqlParameter is already contained by another SqlParameterCollection"]
        dataReaderCommand.Parameters.Clear();
      }
    }

    /// <summary>
    /// Returns database connection string for iPayment transactional database
    /// </summary>
    /// <returns></returns>
    public static string GetDBConnectionString()
    {
      GenericObject database = c_CASL.c_object("TranSuite_DB.Data.db_connection_info") as GenericObject;
      string strDBConnectionString = database.get("db_connection_string", "") as string;
      return strDBConnectionString;
    }

    /// <summary>
    /// This method will create the workgroup where clause and populate the sqlParamList for the workgroup IDs sent.
    /// If "all" is sent then the workgroup where clause will be an empty string.
    /// Method returns true if successful; false otherwise ("sReturn" will have return error message).
    /// </summary>
    /// <param name="objWkGrps"></param>
    /// <param name="sReturn"></param>
    /// <param name="sqlParamList"></param>
    /// <param name="allWkGrps"></param>
    /// <param name="sqlWkGrpWhereClause"></param>
    private static bool GetWorkGroupWhereClause(object objWkGrps, string fileState, ref string sReturn, List<SqlParameter> paramList, ref string whereClause)
    {
      bool allWkGrps = false;
      whereClause = "";
      try
      {
        ArrayList deptList = objWkGrps is ArrayList ? (ArrayList)objWkGrps : new ArrayList();
        if (objWkGrps is string)
        {
          string depts = (string)objWkGrps;
          allWkGrps = (string.Equals("all", depts, StringComparison.OrdinalIgnoreCase));
          if (!allWkGrps)
          {
            sReturn = string.Format("FAILURE: Unrecognized value sent to method Auto Balance {0} Files.", fileState);
            return false;
          }
        }
        else if (objWkGrps is GenericObject)
        {
          deptList = ((GenericObject)objWkGrps).vectors;
          if (deptList.Count == 1)
          {
            string depts = (string)deptList[0];
            allWkGrps = (string.Equals("all", depts, StringComparison.OrdinalIgnoreCase));
          }
        }

        int deptListCount = deptList.Count;

        // Check if the ArrayList only contains a single data element "all" 
        // indicating that all workgroups should be processed
        if (!allWkGrps && deptListCount == 1)
        {
          string depts = (string)deptList[0];
          allWkGrps = (string.Equals("all", depts, StringComparison.OrdinalIgnoreCase));
        }

        if (!allWkGrps)
        {
          if (deptListCount > 0)
          {
            string wkGroupList = "";
            for (int x = 0; x < deptListCount; x++)
            {
              string paramName = string.Format("WkGrp{0}", x.ToString("000000"));
              wkGroupList += string.Format("@{0}{1}", paramName, (x == (deptListCount - 1)) ? "" : ", ");
              SqlParameter sp = param(paramName, SqlDbType.VarChar, deptList[x]);
              paramList.Add(sp);
            }
            if (!string.IsNullOrEmpty(wkGroupList))
            {
              whereClause = string.Format("f.DEPTID IN ({0})", wkGroupList);
            }
          }
          else
          {
            sReturn = string.Format("FAILURE: No workgroups provided for Auto Balance {0} Files.", fileState);
            return false;
          }
        }
      }
      catch (Exception ex)
      {
        sReturn = string.Format("FAILURE: Error getting Workgroup IDs during Auto Balance {0} Files.", fileState);
        Logger.cs_log(string.Format("{0}", ex.ToMessageAndCompleteStacktrace()));
        return false;
      }
      return true;
    }

    /// <summary>
    /// Instntiates, sets properties and returns SqlParameter object.
    /// </summary>
    /// <param name="name"></param>
    /// <param name="type"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    private static SqlParameter param(String name, SqlDbType type, object value)
    {
      SqlParameter param = new SqlParameter(name, type);
      param.Value = value;
      return param;
    }

    /// <summary>
    /// Determines whether the given object is an exception or a CASL error
    /// </summary>
    /// <param name="obj">
    /// Any object
    /// </param>
    /// <returns>Whether or not the provided object is an exception or a CASL error</returns>
    protected static bool IsError(object obj)
    {
      return obj.GetType() == typeof(CASL_error) ||
          (obj.GetType() == typeof(GenericObject) && ((GenericObject)obj).get("_name", "", true).Equals("error"));
    }

    /// <summary>
    /// Get the error message contained in an object
    /// </summary>
    /// <param name="obj">
    /// A C# Exception or a CASL error
    /// </param>
    /// <returns>
    /// The error message contained in the error object
    /// </returns>
    protected static string GetErrorMessage(object obj)
    {
      return obj.GetType() == typeof(CASL_error) ? ((CASL_error)obj).Message : (string)((GenericObject)obj).get("message");
    }
  }

  /// <summary>
  /// Class used to store CORE file info [deposit amt, expected deposit amt]
  /// </summary>
  public class COREFILE
  {
    public string NAME   { get; set; }
    public int NBR       { get; set; }
    public int SEQ       { get; set; }
    public decimal AMT_1 { get; set; }
    public decimal AMT_2 { get; set; }

    /// <summary>
    /// Constructor
    /// </summary>
    public COREFILE(string name, int nbr, int seq)
    {
      NAME  = name;
      NBR   = nbr;
      SEQ   = seq;
      AMT_1 = 0.00M;
      AMT_2 = 0.00M;
    }
  }
}
