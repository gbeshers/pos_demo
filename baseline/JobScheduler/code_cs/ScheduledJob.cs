﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Reflection;
using CASL_engine;

// Bug #13092 Mike O - Abstract job, which all concrete job implementations must be based on

namespace baseline
{
  /// <summary>
  /// Abstract class that must be implemented to define new job types
  /// </summary>
  abstract public class ScheduledJob
  {    
    /// <summary>
    /// Code that is run when the scheduled task is triggered.
    /// Use this function when you only need the arguments passed into the job, such as "departments", etc.
    /// </summary>
    /// <param name="args">
    /// Contains key-value pairs of all arguments, such as "departments"
    /// </param>
    public abstract void Execute(Hashtable args);

    /// <summary>
    /// Type of the ScheduledJob, for identification in CASL.
    /// Do not change this after jobs of this type have been configured!
    /// </summary>
    public abstract string type
    {
      get;
    }
    /// <summary>
    /// Description/user-friendly name of the ScheduledJob, to be shown in CASL configuration
    /// </summary>
    public abstract string description
    {
      get;
    }

    /// <summary>
    /// Determines whether the given object is an exception or a CASL error
    /// </summary>
    /// <param name="obj">
    /// Any object
    /// </param>
    /// <returns>Whether or not the provided object is an exception or a CASL error</returns>
    protected static bool IsError(object obj)
    {
      // Bug 24732 MJO - It can be GenObj_Error too
      Type type = obj.GetType();

      return type == typeof(CASL_error) ||
        type == typeof(GenObj_Error) ||
          (type == typeof(GenericObject) && ((GenericObject)obj).get("_name", "", true).Equals("error"));
    }

    /// <summary>
    /// Get the error message contained in an object
    /// </summary>
    /// <param name="obj">
    /// A C# Exception or a CASL error
    /// </param>
    /// <returns>
    /// The error message contained in the error object
    /// </returns>
    protected static string GetErrorMessage(object obj)
    {
      // Bug 24732 MJO - It can be GenObj_Error too
      Type type = obj.GetType();

      if (type == typeof(GenObj_Error))
        return ((GenObj_Error)obj).get("message").ToString();

      return obj.GetType() == typeof(CASL_error) ? ((CASL_error)obj).Message : (string)((GenericObject)obj).get("message");
    }
  }
}
