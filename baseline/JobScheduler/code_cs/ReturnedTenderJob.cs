﻿using CASL_engine;
using System.Collections;
using ftp;

// Bug #24902 Mike O - Returned Tender Scheduled Job

namespace baseline
{
  /// <summary>
  /// Retrieves ERIN file(s) from SFTP if so configured then triggers tender return processing in the system interface
  /// </summary>
  class ReturnedTenderJob : ScheduledJob
  {
    public override string type
    {
      get { return "returned_tender"; }
    }
    public override string description
    {
      get { return "Returned Tender"; }
    }

    // Bug IPAY-16 MJO - Arguments are now passed in in their original CASL form
    public override void Execute(Hashtable args)
    {
      GenericObject systemInterfaces = args["system_interfaces"] as GenericObject;

      foreach (GenericObject systemInterface in systemInterfaces.getObjectKeys())
      {
        if (systemInterface.get("_parent") == c_CASL.execute("System_interface.ReturnedTender"))
        {
          if (systemInterface.get("import_method").ToString() == "sftp")
          {
            SFTP sftp = new SFTP(
              systemInterface.get("host") as string,
              systemInterface.get("login_name") as string,
              systemInterface.get("login_password") as string,
              systemInterface.get("port") as string,
              true,
              true,
              "",
              "",
              ""
            );

            sftp.Connect();
            sftp.ChangeRemoteDirectory(systemInterface.get("path") as string);
            sftp.GetFiles("*", systemInterface.get("watch_directory") as string);

            sftp.Disconnect();
          }

          ReturnedTender.ProcessAllFiles(systemInterface);

          return;
        }
      }

      Logger.LogError("No returned tender system interface found.", "Returned Tender Job Execute");
    }
  }
}
