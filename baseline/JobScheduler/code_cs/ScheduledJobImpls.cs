﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using CASL_engine;
using TranSuiteServices;
using System.Threading;

// Bug #13092 Mike O - Scheduled jobs
// Bug IPAY-16 MJO - Modified all jobs with arguments to handle them in their original CASL form

namespace baseline
{
  /// <summary>
  /// Locks all open files in the selected departments
  /// </summary>
  class LockFilesJob : ScheduledJob
  {
    public override string type
    {
      get { return "lock_files"; }
    }
    public override string description
    {
      get { return "Lock Files"; }
    }

    public override void Execute(Hashtable args)
    {
      GenericObject departments = (GenericObject)args["departments"];

      if (departments.getObjectKeys().Count == 0)
        return;

      GenericObject departmentVector = (GenericObject)c_CASL.execute("<v/>");
      foreach (GenericObject department in departments.getObjectKeys())
      {
        departmentVector.insert(department.get("_name"));
      }

      // Bug 15889 MJO - Added Web file support
      object fileFilter = c_CASL.c_instance("File_filter", "departments", departmentVector, "file_status", "unclosed", "file_date_select", "all_dates", "file_date_type", "effective_date", "file_type", "I,S,W");

      if (fileFilter.GetType() == typeof(CASL_error))
        throw new Exception(GetErrorMessage(fileFilter));

      // Bug 19667 MJO - Need to call init
      misc.CASL_call("_subject", fileFilter, "a_method", "init", "args", new GenericObject());

      object files = misc.CASL_call("_subject", c_CASL.execute("Core_file"), "a_method", "get_core_files", "args", new GenericObject("file_filter", fileFilter));

      if (files.GetType() == typeof(CASL_error))
        throw new Exception(GetErrorMessage(files));

      // Bug 21339 [20038]: Balance Files and Clean Files Jobs are Failing with an Error [Added logging]
      Logger.cs_log(string.Format("Executing Job: Number of CORE files to lock: {0}", ((GenericObject)files).vectors.Count));

      foreach (GenericObject file in ((GenericObject)files).vectors)
      {
        object result = misc.CASL_call("_subject", file, "a_method", "sv_close_aux");

        if (files.GetType() == typeof(CASL_error))
          throw new Exception(GetErrorMessage(result));
      }
    }
  }

  /// <summary>
  /// Voids all active events in all locked files in the selected departments
  /// </summary>
  class CleanFilesJob : ScheduledJob
  {
    public override string type
    {
      get { return "clean_files"; }
    }
    public override string description
    {
      get { return "Clean Files"; }
    }

    public override void Execute(Hashtable args)
    {
      GenericObject departments = (GenericObject)args["departments"];

      if (departments.getObjectKeys().Count == 0)
        return;

      GenericObject departmentVector = (GenericObject)c_CASL.execute("<v/>");
      foreach (GenericObject department in departments.getObjectKeys())
      {
        departmentVector.insert(department.get("_name"));
      }

      // Bug 15889 MJO - Added Web file support
      object fileFilter = c_CASL.c_instance("File_filter", "departments", departmentVector, "file_status", "closed", "file_date_select", "all_dates", "file_date_type", "effective_date", "file_type", "I,S,W");

      if (fileFilter.GetType() == typeof(CASL_error))
        throw new Exception(GetErrorMessage(fileFilter));

      // Bug 19667 MJO - Need to call init
      misc.CASL_call("_subject", fileFilter, "a_method", "init", "args", new GenericObject());

      object files = misc.CASL_call("_subject", c_CASL.execute("Core_file"), "a_method", "get_core_files", "args", new GenericObject("file_filter", fileFilter));

      if (IsError(files))
        throw new Exception(GetErrorMessage(files));

      // Bug 21339 [20038]: Balance Files and Clean Files Jobs are Failing with an Error [Added logging]
      Logger.cs_log(string.Format("Executing Job: Number of CORE files to clean: {0}", ((GenericObject)files).vectors.Count));

      int cleanedCount = 0;

      // Bug 24732 MJO - Improved logging and continue onto next event on failure
      foreach (GenericObject file in ((GenericObject)files).vectors)
      {
      	// Bug 24732 MJO - Create fake my so get_department call works
        if (CASLInterpreter.get_my() == null)
          SetMy(file.get("department").ToString());

        try
        {
          string coreFileID = file.get("id") as string; // Bug 21339 [21318]: The Clean Files Job Is Voiding Events With Un-Voided Tenders

          if (!file.has("get_active_core_event_ids", true))
            Logger.cs_log($"CleanFilesJob: file {coreFileID} does not contain get_active_core_event_ids{Environment.NewLine}{file.to_xml()}");

          object activeEventIds = misc.CASL_call("_subject", file, "a_method", "get_active_core_event_ids");

          if (IsError(activeEventIds))
          {
            Logger.cs_log(string.Format("Executing Clean Job: Error getting active events in file {0}. {1}{2}", coreFileID, System.Environment.NewLine, GetErrorMessage(activeEventIds)));
            continue;
          }

          foreach (int eventId in ((GenericObject)activeEventIds).vectors)
          {
            object coreEvent = misc.CASL_call("_subject", c_CASL.execute("Core_event"), "a_method", "get_by_core_file_id_and_id", "args", new GenericObject("id", eventId, "core_file_id", coreFileID));

            if (IsError(coreEvent))
            {
              Logger.cs_log(string.Format("Executing Clean Job: Error getting active event {1}-{0}:{2}{3}"
                , eventId, coreFileID, Environment.NewLine, GetErrorMessage(coreEvent)));
              continue;
            }

            bool voidEvent = true;

            // BEGIN Bug 21339 [21318]: The Clean Files Job Is Voiding Events With Un-Voided Tenders
            // If the active event contains any tenders that are NOT voided then do not void the event
            if (((GenericObject)coreEvent).has("tenders"))
            {
              const string sSTATUS_VOIDED = "voided";
              GenericObject geoTenders = ((GenericObject)coreEvent).get("tenders") as GenericObject;
              int lTenderCount = (int)geoTenders.getLength();
              for (int i = 0; i < lTenderCount; i++)
              {
                GenericObject geoTender = (GenericObject)geoTenders.get(i);
                GenericObject geoStatus = geoTender == null ? null : (GenericObject)geoTender.get("status", null);
                string sStatus = geoStatus == null ? "" : (string)geoStatus.get("_name");
                if (sStatus != sSTATUS_VOIDED)
                {
                  Logger.cs_log(string.Format("Executing Clean Job: Cannot void active event {1}-{0}; it contains an unvoided tender.", eventId, coreFileID));
                  voidEvent = false;
                  break;
                }
              }
            }
            // END Bug 21339 [21318]

            if (!voidEvent)
              continue;

            if (!(coreEvent as GenericObject).has("void_event_in_ipayment_database", true))
              Logger.cs_log($"CleanFilesJob: event {coreFileID}-{eventId} does not contain void_event_in_ipayment_database{Environment.NewLine}{(coreEvent as GenericObject).to_xml()}");

            object result = misc.CASL_call("_subject", coreEvent, "a_method", "void_event_in_ipayment_database", "args", new GenericObject("user_id", "auto"));
            if (IsError(result))
            {
              // Bug 24732 MJO - Fixed bad message and argument
              Logger.cs_log(string.Format("Executing Clean Job: Error voiding active event {1}-{0}:{2}{3}"
              , eventId, coreFileID, Environment.NewLine, GetErrorMessage(result)));
              continue;
            }
            else
            {
              //Bug 22528 HX
              GenericObject busi_notes_data = c_CASL.c_object("Business.Notes.data") as GenericObject;
              bool b_save_system_void = (bool)busi_notes_data.get("void_reason_for_system_voids", false);
              if (b_save_system_void)
              {
                GenericObject voidreasons = c_CASL.c_object("GEO.sys_void_reasons") as GenericObject;
                string sSys_002 = voidreasons.get("sys_002", "") as string;

                GenericObject my_args = new GenericObject();
                GenericObject note_info = new GenericObject();
                note_info["FILEID"] = coreFileID;
                note_info["EVENTNBR"] = eventId;
                note_info["OWNER_TYPE"] = "Event";
                note_info["NOTE_TYPE"] = "VoidReason";
                note_info["VOIDREASON"] = sSys_002;
                note_info["VOID_COMMENT"] = "";
                note_info["NOTE_USER_ID"] = "SYSTEM";
                note_info["NOTE_DATETIME"] = ""; // HX: it will be set in c# function
                my_args["note_info"] = note_info;
                my_args["db_connection"] = c_CASL.c_object("TranSuite_DB.Data.db_connection_info") as GenericObject;
                VoidNotes.WriteNotesRecord(my_args);
              }

              // Bug 21339 [21318]: The Clean Files Job Is Voiding Events With Un-Voided Tenders
              Logger.cs_log(string.Format("Executing Clean Job: Voided active event {1}-{0}", eventId, coreFileID));

              ++cleanedCount;
            }
          }
        }
        finally
        {
          CASLInterpreter.no_session_my = null;
        }
      }

      Logger.cs_log(string.Format("Executed Clean Job: {0} CORE files cleaned", cleanedCount));
    }

    // Bug 24732 MJO - Create fake my so get_department call works
    private static void SetMy(string workgroupID)
    {
      GenericObject my = c_CASL.c_instance("My");
      my.set("login_department", workgroupID);

      CASLInterpreter.no_session_my = new ThreadLocal<GenericObject>();
      CASLInterpreter.no_session_my.Value = my;
    }
  }

  /// <summary>
  /// Balances all locked files with no active events in the selected departments
  /// </summary>
  class BalanceFilesJob : ScheduledJob
  {
    public override string type
    {
      get { return "balance_files"; }
    }
    public override string description
    {
      get { return "Balance Files"; }
    }

    public override void Execute(Hashtable args)
    {
      GenericObject departments = (GenericObject)args["departments"];

      if (departments.getObjectKeys().Count == 0)
        return;

      GenericObject departmentVector = (GenericObject)c_CASL.execute("<v/>");
      foreach (GenericObject department in departments.getObjectKeys())
      {
        departmentVector.insert(department.get("_name"));
      }

      // Bug 15889 MJO - Added Web file support
      object fileFilter = c_CASL.c_instance("File_filter", "departments", departmentVector, "file_status", "closed", "file_date_select", "all_dates", "file_date_type", "effective_date", "file_type", "I,S,W");

      if (IsError(fileFilter))
        throw new Exception(GetErrorMessage(fileFilter));

      // Bug 19667 MJO - Need to call init
      misc.CASL_call("_subject", fileFilter, "a_method", "init", "args", new GenericObject());

      object files = misc.CASL_call("_subject", c_CASL.execute("Core_file"), "a_method", "get_core_files", "args", new GenericObject("file_filter", fileFilter));

      if (IsError(files))
        throw new Exception(GetErrorMessage(files));

      // Bug 21339 [20038]: Balance Files and Clean Files Jobs are Failing with an Error [Added logging]
      Logger.cs_log(string.Format("Executing Job: Number of CORE files to balance: {0}", ((GenericObject)files).vectors.Count));

      foreach (GenericObject file in ((GenericObject)files).vectors)
      {
        string coreFileID = file.get("id") as string;
        if ((bool)misc.CASL_call("_subject", c_CASL.execute("Core_file"), "a_method", "has_active_core_event", "args", new GenericObject("core_file_id", file.get("id"))))
        {
          Logger.cs_log(string.Format("Executing Balance Job: File {0} contains an active event and will not be balanced.", coreFileID));
        }
        else
        {
          object result = misc.CASL_call("_subject", c_CASL.execute("Business"), "a_method", "auto_balance_core_file_aux", "args", new GenericObject("core_file_id", coreFileID)); // Bug 15464
          if (IsError(result))
          {
            Logger.cs_log(string.Format("Executing Balance Job: Error balancing file {0}:{1}{2}"
              , coreFileID, Environment.NewLine, GetErrorMessage(result)));
            throw new Exception(GetErrorMessage(result));
          }
          Logger.cs_log(string.Format("Executing Balance Job: File {0} was successfully balanced.", coreFileID));
        }
      }
    }
  }

  /// <summary>
  /// Bug 21476 - DJD Add Scheduled Job To Run Auto Accept files
  /// Accepts all balanced files in the selected departments
  /// </summary>
  class AcceptFilesJob : ScheduledJob
  {
    public override string type
    {
      get { return "accept_files"; }
    }
    public override string description
    {
      get { return "Accept Files"; }
    }

    public override void Execute(Hashtable args)
    {
      GenericObject departments = (GenericObject)args["departments"];

      if (departments.getObjectKeys().Count == 0)
        return;

      GenericObject departmentVector = (GenericObject)c_CASL.execute("<v/>");
      foreach (GenericObject department in departments.getObjectKeys())
      {
        departmentVector.insert(department.get("_name"));
      }

      object fileFilter = c_CASL.c_instance("File_filter", "departments", departmentVector, "file_status", "balanced", "file_date_select", "all_dates", "file_date_type", "effective_date", "file_type", "I,S,W");

      if (IsError(fileFilter))
        throw new Exception(GetErrorMessage(fileFilter));

      object files = misc.CASL_call("_subject", c_CASL.execute("Core_file"), "a_method", "get_core_files", "args", new GenericObject("file_filter", fileFilter));

      if (IsError(files))
        throw new Exception(GetErrorMessage(files));

      Logger.cs_log(string.Format("Executing Job: Number of CORE files to accept: {0}", ((GenericObject)files).vectors.Count));

      foreach (GenericObject file in ((GenericObject)files).vectors)
      {
        string coreFileID = file.get("id") as string;
        object result = misc.CASL_call("_subject", c_CASL.execute("Business"), "a_method", "auto_accept_core_file_aux", "args", new GenericObject("core_file_id", coreFileID)); // Bug 15464
        if (IsError(result))
        {
          Logger.cs_log(string.Format("Executing Accept Job: Error accepting file {0}:{1}{2}"
            , coreFileID, Environment.NewLine, GetErrorMessage(result)));
          throw new Exception(GetErrorMessage(result));
        }
        Logger.cs_log(string.Format("Executing Accept Job: File {0} was successfully accepted.", coreFileID));
      }
    }
  }

  // Bug 20467 UMN Support import Jobs
  class PBImportJob : ScheduledJob
  {
    public override string type
    {
      get { return "PBImport"; }
    }
    public override string description
    {
      get { return "Import PB Files"; }
    }

    public override void Execute(Hashtable args)
    {
      GenericObject imports = (GenericObject)args["imports"];

      // If no imports chosen, do import all
      if (imports.getObjectKeys().Count == 0)
      {
        misc.CASL_call("_subject", c_CASL.execute("Business.Std_interface_import"), "a_method", "file_import_process");
        return;
      }

      foreach (GenericObject import in imports.getObjectKeys())
      {
        misc.CASL_call("_subject", import, "a_method", "file_import");
      }
    }
  }

  /// <summary>
  /// Bug 21402: Add Scheduled Job To Run Auto Batch Update
  /// Performs an auto batch update on all files of configured status (NOTE: The workgroup or import selection is NOT used for this job.)
  /// </summary>
  class BatchAcceptedFilesJob : ScheduledJob
  {
    public override string type
    {
      get { return "batch_files"; }
    }
    public override string description
    {
      get { return "Batch Update Files"; }
    }

    public override void Execute(Hashtable args)
    {
      string batchFileStatus = (string)args["batch_file_status"];
      GenericObject batchSystems = (GenericObject)args["batch_systems"];

      string batchSystemIdsStr;

      if (batchSystems.getLength() == 0)
      {
        batchSystemIdsStr = "all";
      }
      else
      {
        ArrayList batchSystemIds = new ArrayList();
        foreach (GenericObject batchSystem in batchSystems.vectors)
        {
          batchSystemIds.Add(batchSystem.get("_name"));
        }

        batchSystemIdsStr = String.Join(",", batchSystemIds);
      }

      // Bug 21402 DJD: Create instance of "Batch_update" to prevent "MakePart target is null" error
      GenericObject geoBU = (GenericObject)misc.CASL_call("_subject", c_CASL.execute("Batch_update"), "a_method", "make_inst");
      object result = misc.CASL_call("_subject", geoBU, "a_method", "autorun_batch_update",
        "args", new GenericObject("file_status", batchFileStatus,
                "system_interfaces", batchSystemIdsStr, "system_interfaces_for_check_previous_batch_update", ""));

      if (IsError(result))
      {
        throw new Exception(GetErrorMessage(result));
      }
    }
  }

  /// <summary>
  /// Bug 23198 [20901] DJD - File Balancing Job Expansion
  /// Balances all "inactive" files with no events in the selected departments
  /// </summary>
  class BalanceInactiveFilesJob : ScheduledJob
  {
    public override string type
    {
      get { return "balance_inactive_files"; }
    }
    public override string description
    {
      get { return "Balance Inactive Files"; }
    }

    public override void Execute(Hashtable args)
    {
      GenericObject departments = (GenericObject)args["departments"];

      if (departments.getObjectKeys().Count == 0)
        return;

      ArrayList departmentIds = new ArrayList();
      foreach (GenericObject department in departments.getObjectKeys())
      {
        departmentIds.Add(department.get("_name"));
      }

      string result = FileBalanceJobExpansion.BalanceInactiveFiles_aux(departmentIds);

      if (result.ToLower().Contains("fail"))
      {
        throw new Exception(result);
      }
    }
  }

  /// <summary>
  /// Bug 23198 [20901] DJD - File Balancing Job Expansion
  /// Balances all "deposited" files with no active events in the selected departments
  /// </summary>
  class BalanceDepositedFilesJob : ScheduledJob
  {
    public override string type
    {
      get { return "balance_deposited_files"; }
    }
    public override string description
    {
      get { return "Balance Deposited Files"; }
    }

    public override void Execute(Hashtable args)
    {
      GenericObject departments = (GenericObject)args["departments"];

      if (departments.getObjectKeys().Count == 0)
        return;

      ArrayList departmentIds = new ArrayList();
      foreach (GenericObject department in departments.getObjectKeys())
      {
        departmentIds.Add(department.get("_name"));
      }

      string result = FileBalanceJobExpansion.BalanceDepositedFiles_aux(departmentIds);

      if (result.ToLower().Contains("fail"))
      {
        throw new Exception(result);
      }
    }
  }

  /// <summary>
  /// Bug 23198 [20901] DJD - File Balancing Job Expansion
  /// Balances all "zero dollar" files with no deposits or active events in the selected departments (only voided/reversed activity)
  /// </summary>
  class BalanceZeroDollarFilesJob : ScheduledJob
  {
    public override string type
    {
      get { return "balance_zero_dollar_files"; }
    }
    public override string description
    {
      get { return "Balance Zero Dollar Files"; }
    }

    public override void Execute(Hashtable args)
    {
      GenericObject departments = (GenericObject)args["departments"];

      if (departments.getObjectKeys().Count == 0)
        return;

      ArrayList departmentIds = new ArrayList();
      foreach (GenericObject department in departments.getObjectKeys())
      {
        departmentIds.Add(department.get("_name"));
      }

      string result = FileBalanceJobExpansion.BalanceZeroDollarFiles_aux(departmentIds);

      if (result.ToLower().Contains("fail"))
      {
        throw new Exception(result);
      }
    }
  }
}
