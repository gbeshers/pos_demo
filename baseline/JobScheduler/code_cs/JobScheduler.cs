﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using CASL_engine;
using System.Timers;
using Hangfire;
using Hangfire.Storage;

// Bug #13092 Mike O - Internal job scheduler implementation
// Bug 20467 UMN Support import Jobs
// Bug 26519 MJO - Rewrote using Hangfire to support different frequency jobs

namespace baseline
{
  public class JobScheduler
  {
    /// <summary>
    /// Register a job with the scheduler
    /// </summary>
    /// <param name="id">Unique id for the job (should be the id of the CASL job)</param>
    /// <param name="time">Time of the day that the job should be run</param>
    /// <param name="departments">List of departments the job should be run on</param>
    /// <param name="imports">List of import types for the PB Import job</param>
    /// <param name="fileStatus">Bug 21402: File status (Balanced or Accepted) for the Batch Update job</param>
    /// <param name="batchSystems">Bug 21402: List of systems for the Batch Update job</param>
    /// <param name="job">The job to be registered with the scheduler</param>
    /// <returns>True if successful; false, otherwise.</returns>
    public bool updateJob(string id, DateTime start, Frequency frequency)
    {
      try
      {
        string day = start.Day.ToString();
        string month = start.Month.ToString();
        string year = start.Year.ToString();

        string cronExpr = null;

        switch (frequency)
        {
          case Frequency.Daily:
            cronExpr = Cron.Daily(start.Hour, start.Minute);
            break;
          case Frequency.Weekly:
            cronExpr = Cron.Weekly(start.DayOfWeek, start.Hour, start.Minute);
            break;
          case Frequency.Monthly:
            cronExpr = Cron.Monthly(start.Day, start.Hour, start.Minute);
            break;
          case Frequency.Quarterly:
            // Bug IPAY-1088 MJO - Make sure it starts in the future
            while (start < DateTime.Now)
              start = start.AddMonths(3);
            month = start.Month.ToString();
            month = month + "/3";
            year = "*";
            break;
          case Frequency.Biannually:
            // Bug IPAY-1088 MJO - Make sure it starts in the future
            while (start < DateTime.Now)
              start = start.AddMonths(6);
            month = start.Month.ToString();
            month = month + "/6";
            year = "*";
            break;
          case Frequency.Annually:
            cronExpr = Cron.Yearly(start.Month, start.Day, start.Hour, start.Minute);
            break;
        }

        if (cronExpr == null)
        {
          cronExpr = String.Format("{0} {1} {2} {3} {4}",
            start.Minute.ToString("D2"),
            start.Hour.ToString("D2"),
            day,
            month,
            year
          );
        }

        RecurringJob.AddOrUpdate(id, () => OnTimedEvent(id), cronExpr, TimeZoneInfo.Local);

        Logger.cs_log($"Created/updated Hangfire job {id} with cron {cronExpr}. Next execution: {GetNextExecutionTime(id)}");

        return true;
      }
      catch (Exception e)
      {
        Logger.cs_log(String.Format("Error creating or updating Hangfire job {1}: {0}", e.ToMessageAndCompleteStacktrace(), id));

        return false;
      }
    }

    public void removeJob(string id)
    {
      RecurringJob.RemoveIfExists(id);
    }

    // Bug IPAY-15 MJO - Rewrote to support arbitrary arguments
    public void OnTimedEvent(string id)
    {
      GenericObject job = (c_CASL.c_object("Business.Job.of") as GenericObject).get(id) as GenericObject;
      GenericObject jobType = job.get("_parent") as GenericObject;

      GenericObject jobConfigFields = jobType.get("_config_field_order") as GenericObject;
      string type = (string)jobType.get("_name");
      ScheduledJob jobInst = SchedulerHelper.GetJobTypeInstance(type);

      Hashtable args = new Hashtable();

      for (int i = 0; i < jobConfigFields.getLength(); i++)
      {
        string configKey = jobConfigFields.get(i).ToString();
        object configValue = job.get(configKey, null, true); // Bug IPAY-16 MJO - Support default values

        args.Add(configKey, configValue);
      }

      // Bug 21339 [20038]: Balance Files and Clean Files Jobs are Failing with an Error [Added BEGIN/END logging]
      bool err = false;
      try
            {
                Logger.cs_log(string.Format("BEGIN: Scheduled Job {0} ({1})", id, jobInst.description));

                recordInActivityLog(jobInst, jobConfigFields, job);     // Bug 22209

        jobInst.Execute(args);
            }
            catch (Exception e)
      {
        err = true;
        Logger.cs_log(string.Format("ERROR: Scheduled Job {0} failed to execute successfully{1}{2}"
        , id
        , Environment.NewLine
        , e.ToMessageAndCompleteStacktrace())); // Log the error
        return;
      }
      finally
      {
        Logger.cs_log(string.Format("END  : Scheduled Job {0} ({1}){2}"
        , id, jobInst.description, (err ? "" : $" was executed successfully. Next execution: {GetNextExecutionTime(id)}")));
      }
    }

    /// <summary>
    /// Write job information to the activity log
    /// Bug 22209
    /// </summary>
    /// <param name="job"></param>
    /// <param name="args"></param>
    private static void recordInActivityLog(ScheduledJob job, GenericObject jobConfigFields, GenericObject geoJob)
    {
      StringBuilder details = new StringBuilder();

      for (int i = 0; i < jobConfigFields.getLength(); i++)
      {
        string configKey = jobConfigFields.get(i).ToString();
        object configValue = geoJob.get(configKey);

        if (typeof(GenericObject) == configValue.GetType())
        {
          List<string> ids = new List<string>();

          foreach (GenericObject geo in ((GenericObject)configValue).getObjectKeys())
          {
            string primaryKey = geo.get("_primary_key", "id").ToString();

            if (!geo.has(primaryKey))
              primaryKey = "_name";

            ids.Add(geo.get(primaryKey).ToString());
          }

          details.Append(" " + configKey + ": " + String.Join(", ", ids.ToArray()));
        }
        else
        {
          details.Append(" " + configKey + ": " + configValue.ToString());
        }
      }

      // Try to get the userid of ther person running the batch update
      // which should be in here: Business.Misc.data.autobatchupdate_user
      string userid = "auto";
      GenericObject miscSettings = c_CASL.c_object("Business.Misc.data") as GenericObject;
      if (miscSettings != null && miscSettings.has("autobatchupdate_user"))
      {
        // The autobatchupdate_user only makes sense for batch update jobs
        if (!string.IsNullOrWhiteSpace(job.description) && job.description.Contains("Batch"))
          userid = miscSettings.get("autobatchupdate_user") as string;
      }
      if (string.IsNullOrWhiteSpace(userid))
        userid = "auto";

      misc.CASL_call("a_method", "actlog", "args",
          new GenericObject(
              "app", "Config"
              // Bug 20435 UMN PCI-DSS don't pass session
              , "user", userid        // There is no userid for timed jobs
              , "action", "Scheduled Job"
              , "summary", job.description
              , "detail", details.ToString()
              )
              );
    }

    // Bug IPAY-1088 MJO - Get the next execution time for the job with the given id
    private static string GetNextExecutionTime(string id)
    {
      return JobStorage.Current.GetConnection().GetRecurringJobs().Single(x => x.Id == id).NextExecution.Value.ToLocalTime().ToString("G");
    }

    public enum Frequency
    {
      Daily,
      Weekly,
      Monthly,
      Quarterly,
      Biannually,
      Annually
    }
  }
}
