using System;
using System.Net;
using System.IO;
using System.Text;
using CASL_engine;
using System.Text.RegularExpressions;

// Bug 13638: UMN -- SFTP support. We bought this for RI Courts.
using Rebex.Net;
using System.Linq;
using System.IO.Compression;

// Bug 13050: UMN -- pretty much rewrote this whole file, and exposed everything to CASL, just in case.
// Bug 13638: UMN -- adding SFTP support thru the REBEX library
// I'm a bit ashamed of this code as there is a fair bit of cloning here (because the rebex ftp and 
// sftp intefaces are very similar). But I left it as is, because we will need to diverge the sftp
// interface from the ftp one to support passphrases, key exchange, etc. Really sftp is so different from
// ftp, that there's no reason to limit the sftp class to what ftp supports. By using a FileTransfer
// abstract base class, we specify the minimum compatibilty between the two.
// Ported to Geisinger: Bug 13690
namespace ftp
{

  /// <summary>
  /// Abstract base class for the FTP class. There are two concrete classes
  /// derived from this class: FTP and SFTP.
  /// </summary>
  public abstract class FileTransfer
  {
    protected string host;
    protected string user;
    protected string password;
    protected bool pwdIsEncrypted; // is password encrypted?
    protected string port;
    protected bool useBinary;
    protected int count; // used mainly by GetFiles
    public string errMsg { get; set; }

    /// <summary>
    /// Default timeout.  Bug 23202
    /// </summary>
    protected const int CONNECTION_TIMEOUT_DEFAULT = 60000;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Host"></param>
    /// <param name="User"></param>
    /// <param name="Password"></param>
    /// <param name="Port"></param>
    /// <param name="UseBinary"></param>
    protected FileTransfer(string Host, string User, string Password, string Port, bool UseBinary, bool PwdIsEncrypted)
    {
      host = Host; user = User; password = Password; port = Port; useBinary = UseBinary;
      pwdIsEncrypted = PwdIsEncrypted;
      errMsg = "";

      // Decrypt FTP/SFTP Password
      if (!string.IsNullOrEmpty(password) && pwdIsEncrypted)
      {
        string secret_key = Crypto.get_secret_key();
        byte[] content_bytes = Convert.FromBase64String(password);
        byte[] decrypted_bytes = Crypto.symmetric_decrypt("data", content_bytes, "secret_key", secret_key);
        password = System.Text.Encoding.Default.GetString(decrypted_bytes);
      }
    }

    protected FileTransfer(GenericObject Args)
    {
      errMsg = "";

      // Validate required common parameters.
      if (!Args.has("host")) throw CASL_error.create("message", "FTP host param missing", "code", "FTP-01");
      if (!Args.has("uid")) throw CASL_error.create("message", "FTP uid param missing", "code", "FTP-02");
      if (!Args.has("pwd")) throw CASL_error.create("message", "FTP pwd param missing", "code", "FTP-03");
      if (!Args.has("pwd_is_encrypted")) throw CASL_error.create("message", "FTP pwd_is_encrypted param missing", "code", "FTP-04");

      host = (string)Args.get("host");

      if (!Args.has("remote_path"))
      {
        // NOTE: This needs to be "backwards compatible" and support including the 
        // remote path in the host value (e.g., ftp.hamacher.com/ftpSIGIS/XML).
        char cDELIMTER = '/';
        int nPos = host.IndexOf(cDELIMTER);
        if (nPos != -1)
        {
          string sRemotePath = "";
          if (host.Length > (nPos + 1))
          {
            sRemotePath = host.Substring(nPos + 1);
          }
          if (nPos > 0)
          {
            host = host.Substring(0, nPos);
          }
          if (!string.IsNullOrEmpty(sRemotePath))
          {
            Args.set("remote_path", sRemotePath);
          }
        }
      }

      user = (string)Args.get("uid");
      password = (string)Args.get("pwd");  // will be normal string or base64 encoded string
      pwdIsEncrypted = (bool)Args.get("pwd_is_encrypted");

      // Validate optional common parameters
      object arg = Args.get("port", c_CASL.c_opt);
      port = (arg != c_CASL.c_opt) ? (string)arg : "21";  // default is 21

      arg = Args.get("use_binary", c_CASL.c_opt);
      useBinary = (arg != c_CASL.c_opt) ? (bool)arg : true;  // default is true
    }

    /// <summary>
    /// Connect to the server
    /// </summary>
    public abstract void Connect();

    /// <summary>
    /// Bug 23202: FT: Make the timeout settable
    /// </summary>
    /// <param name="connectionTimeout"></param>
    public abstract void Connect(int connectionTimeout);

    /// <summary>
    /// Allow us to turn on Verbose Rebex logging 
    /// </summary>
    /// <param name="connectionTimeout"></param>
    /// <param name="logVerbose"></param>
    public abstract void Connect(int connectionTimeout, bool logVerbose, string rebexLogPath);

    /// <summary>
    /// Disconnect from the server
    /// </summary>
    public abstract void Disconnect();

    /// <summary>
    /// Get the file located at serverFilePath and store it as the localFilePath
    /// </summary>
    /// <param name="serverFilePath"></param>
    /// <param name="localFilePath"></param>
    /// <returns>Throws exception on error, else void.</returns>
    public abstract void GetFile(string serverFilePath, string localFilePath);

    /// <summary>
    /// Put localFilePath to the file named serverFilePath on the foreign server
    /// </summary>
    /// <param name="localFilePath"></param>
    /// <param name="serverFilePath"></param>
    /// <returns>Throws exception on error, else void.</returns>
    public abstract void SendFile(string localFilePath, string serverFilePath);

    /// <summary>
    /// Append localFilePath to the file named serverFilePath on the foreign server 
    /// </summary>
    /// <param name="localFilePath"></param>
    /// <param name="serverFilePath"></param>
    /// <returns>Throws exception on error, else void.</returns>
    public abstract void AppendFile(string localFilePath, string serverFilePath);

    /// <summary>
    /// Get multiple files using FTP. Requires a local path to put the files,
    /// and a regex to indicate what files to retrieve. Example regex: '*.bai'. 
    /// Set the remote directory first.
    /// Fixed for Bug 27454/27443.  
    /// </summary>
    /// <param name="localFilePath"></param>
    /// <param name="regex"></param>
    public abstract void GetFiles(string serverRegex, string localFilePath);

    /// <summary>
    /// Change directory on the FTP server.
    ///  "." pathnames are ignored.  
    ///  Use forward slash for directory delimeter.
    /// </summary>
    /// <param name="remotePath"></param>
    /// <returns></returns>
    public abstract void ChangeRemoteDirectory(string remotePath);

    /// <summary>
    /// Deletes a file on the remote FTP server identified by serverFilePath
    /// </summary>
    /// <param name="serverFilePath"></param>
    /// <returns>Throws exception on error, else void.</returns>
    public abstract void DeleteFile(string serverFilePath);

    /// <summary>
    /// Does file exist on server?
    /// </summary>
    /// <param name="serverFilePath"></param>
    /// <returns>Returns true on existence, else false.</returns>
    public abstract bool FileExists(string serverFilePath);

    /// <summary>
    /// Unzip the serverFileName from the FTP stream, and write it to localFilePath
    /// Bug 12923: UMN - add zip file support. Make transparent to end user
    /// </summary>
    /// <param name="ftpStream"></param>
    /// <param name="serverFileName"></param>
    /// <param name="localFilePath"></param>
    /// <returns>Throws exception on error, else void.</returns>
    protected void UnzipStream(Stream ftpStream, string serverFilePath, string localFilePath)
    {
      try
      {
      	// Bug IPAY-1626 MJO - Replaced SharpZipLib with standard .NET zip support
        ZipArchive archive = new ZipArchive(ftpStream);

        var result = from currEntry in archive.Entries
                     where Path.GetDirectoryName(currEntry.FullName) == Path.GetPathRoot(currEntry.FullName)
                     where !String.IsNullOrEmpty(currEntry.Name)
                     select currEntry;

        foreach (ZipArchiveEntry entry in result)
        {
          // only want to unzip the file configured in the system interface
          //if (Path.GetFileNameWithoutExtension(zipEntry.Name) == Path.GetFileNameWithoutExtension(serverFilePath))

          // NOTE: This method ASSUMES that a SINGLE file is contained in the zipped folder
          // Only want to unzip the file contained in the zipped folder (i.e., ignore directories)

          using (FileStream streamWriter = File.Create(localFilePath))
          {
            entry.Open().CopyTo(streamWriter);
          }
          return;  // we're done, so get outta here
        }
        // Oops, never found the file we wanted
        throw new Exception(String.Format("FTP Error: Zip file doesn't contain a file: {0}", serverFilePath));
      }
      catch (Exception e)
      {
        errMsg = "FTP Error: " + e.Message + e.ToString();
        // Bug 17849
        Logger.cs_log_trace("Error in UnzipStream", e);
        throw;      // Re-throw and preserve stack
      }
    }
  }    // END class FileTransfer

  public class FTP : FileTransfer
  {
    protected Ftp client = null;

    // implement these in the future
    protected bool keepAlive;
    protected bool usePassive;
    protected bool enableSSL;

    /// <summary>
    /// Defaulted constructor (will mainly be used from C#)
    /// </summary>
    /// <param name="Host"></param>
    /// <param name="User"></param>
    /// <param name="Password"></param>
    /// <param name="Port"></param>
    public FTP(string Host, string User, string Password, string Port)
      : this(Host, User, Password, Port, true, false, false, false, false)
    {
    }

    /// <summary>
    /// Full Constructor
    /// </summary>
    /// <param name="Host"></param>
    /// <param name="User"></param>
    /// <param name="Password"></param>
    /// <param name="Port"></param>
    /// <param name="UseBinary"></param>
    /// <param name="KeepAlive"></param>
    /// <param name="UsePassive"></param>
    /// <param name="EnableSSL"></param>
    public FTP(string Host, string User, string Password, string Port, bool UseBinary,
       bool PwdIsEncrypted, bool KeepAlive, bool UsePassive, bool EnableSSL)
      : base(Host, User, Password, Port, UseBinary, PwdIsEncrypted)
    {
      keepAlive = KeepAlive; usePassive = UsePassive; enableSSL = EnableSSL;
    }

    // Call from C# when interfacing to CASL
    public FTP(GenericObject Args)
      : base(Args)
    {
      object arg = Args.get("keep_alive", c_CASL.c_opt);
      keepAlive = (arg != c_CASL.c_opt) ? (bool)arg : true;  // default is true

      arg = Args.get("use_passive", c_CASL.c_opt);
      usePassive = (arg != c_CASL.c_opt) ? (bool)arg : true;  // default is true

      arg = Args.get("enable_ssl", c_CASL.c_opt);
      enableSSL = (arg != c_CASL.c_opt) ? (bool)arg : false;  // default is false
    }

    ~FTP()
    {
      Disconnect();
    }

    /// <summary>
    /// Create new Rebex client. Connect and login to server
    /// </summary>
    public override void Connect()
    {
      Connect(CONNECTION_TIMEOUT_DEFAULT);        // Bug 23202
    }

    /// <summary>
    /// New version with connection timeout
    /// Bug 23202
    /// </summary>
    /// <param name="connectionTimeout"></param>
    public override void Connect(int connectionTimeout)
    {
      Connect(connectionTimeout, false, "");
    }

    /// <summary>
    /// A version of Connect that writes to the Rebex log.
    /// We need the Rebex log to send problems to Rebex support.
    /// Bug 24836 
    /// </summary>
    /// <param name="connectionTimeout"></param>
    /// <param name="verboseLog"></param>
    /// <param name="rebexLogPath"></param>
    public override void Connect(int connectionTimeout, bool verboseLog, string rebexLogPath)
    {

      client = new Ftp();
      try
      {
        client.Timeout = connectionTimeout;

        // Bug 16762: DJD - FTP/SFTP Is Not Setting Port Number When Connecting
        if (string.IsNullOrEmpty(port))
        {
          client.Connect(host);
        }
        else
        {
          int serverPort = Convert.ToInt32(port);
          client.Connect(host, serverPort);
        }

        client.Login(user, password);
        // Bug 26455: Only need this for things like progress bars.
        // See rebex.net/sftp.net/features/file-transfer.aspx#progress
        //client.TransferProgressChanged 
      }
      catch (Exception e)
      {
        client = null;
        errMsg = "FTP Error: " + e.Message + e.ToString();
        Logger.cs_log(e.Message + Environment.NewLine + e.StackTrace + Environment.NewLine);
        throw;      // Re-throw and preserve stack
      }
    }

    /// <summary>
    /// Disconnect from server
    /// </summary>
    public override void Disconnect()
    {
      if (client != null)
      {
        client.Disconnect();
        client = null;
      }
    }

    /// <summary>
    /// Handler for updating batch progress of GetFiles
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void TransferProgress(object sender, FtpBatchTransferProgressEventArgs e)
    {
      count = e.FilesTransferred;
    }

    /// <summary>
    /// Get the file located at serverFilePath and store it as the localFilePath
    /// </summary>
    /// <param name="serverFilePath"></param>
    /// <param name="localFilePath"></param>
    /// <returns>Throws exception on error, else void.</returns>
    public override void GetFile(string serverFilePath, string localFilePath)
    {
      try
      {
        if (client == null)
        {
          throw new Exception("FTP Error: not connected to server");
        }
        client.GetFile(serverFilePath, localFilePath);
      }
      catch (Exception e)
      {
        errMsg = "FTP Error: " + e.Message + e.ToString();
        Logger.cs_log(e.Message + Environment.NewLine + e.StackTrace + Environment.NewLine);
        throw;      // Re-throw and preserve stack
      }
    }

    /// <summary>
    /// Get the file located at serverFilePath and store it as the localFilePath. If Unzip is
    /// true then unzip file while downloading to localFilePath. (Assumes a single zipped file.)
    /// </summary>
    /// <param name="serverFilePath"></param>
    /// <param name="localFilePath"></param>
    /// <param name="bUnzip"></param>
    /// <returns>Throws exception on error, else void.</returns>
    public void GetFile(string serverFilePath, string localFilePath, bool bUnzip)
    {
      if (bUnzip)
      {
        try
        {
          if (client == null)
          {
            throw new Exception("FTP Error: not connected to server");
          }
          Stream DownloadFtpStream = client.GetDownloadStream(serverFilePath);
          UnzipStream(DownloadFtpStream, serverFilePath, localFilePath);
        }
        catch (Exception e)
        {
          errMsg = "FTP Error: " + e.Message + e.ToString();
          Logger.cs_log(e.Message + Environment.NewLine + e.StackTrace + Environment.NewLine);
          throw;      // Re-throw and preserve stack
        }
      }
      else
      {
        GetFile(serverFilePath, localFilePath);
      }
    }

    /// <summary>
    /// Change directory on the FTP server.
    ///  "." pathnames are ignored.  
    ///  Use forward slash for directory delimeter.
    /// </summary>
    /// <param name="remotePath"></param>
    public override void ChangeRemoteDirectory(string remotePath)
    {
      try
      {
        if (client == null)
        {
          throw new Exception("ChangeRemoteDirectory: FTP Error: not connected to server");
        }
        client.ChangeDirectory(remotePath);
      }
      catch (Exception e)
      {
        errMsg = "FTP Error: ChangeRemoteDirectory: " + e.Message + e.ToString();
        Logger.cs_log(e.Message + Environment.NewLine + e.StackTrace + Environment.NewLine);
        throw;      // Re-throw and preserve stack
      }
    }


    /// <summary>
    /// Put localFilePath to the file named serverFilePath on the foreign server
    /// </summary>
    /// <param name="localFilePath"></param>
    /// <param name="serverFilePath"></param>
    /// <returns>Throws exception on error, else void.</returns>
    public override void SendFile(string localFilePath, string serverFilePath)
    {
      try
      {
        if (client == null)
        {
          throw new Exception("FTP Error: not connected to server");
        }
        client.PutFile(localFilePath, serverFilePath);
      }
      catch (Exception e)
      {
        errMsg = "FTP Error: " + e.Message + e.ToString();
        Logger.cs_log(e.Message + Environment.NewLine + e.StackTrace + Environment.NewLine);
        throw;      // Re-throw and preserve stack
      }
    }

    /// <summary>
    /// Append localFilePath to the file named serverFilePath on the foreign server 
    /// </summary>
    /// <param name="localFilePath"></param>
    /// <param name="serverFilePath"></param>
    /// <returns>Throws exception on error, else void.</returns>
    public override void AppendFile(string localFilePath, string serverFilePath)
    {
      try
      {
        if (client == null)
        {
          throw new Exception("FTP Error: not connected to server");
        }

        // get the length of the remote file so we know where to append
        long remoteOffset = client.GetFileLength(serverFilePath);

        // open the local file for reading 
        using (Stream stream = File.OpenRead(localFilePath))
        {
          // transfer data
          client.PutFile(stream, serverFilePath, remoteOffset, -1);
        }
      }
      catch (Exception e)
      {
        errMsg = "FTP Error: " + e.Message + e.ToString();
        Logger.cs_log(e.Message + Environment.NewLine + e.StackTrace + Environment.NewLine);
        throw;      // Re-throw and preserve stack
      }
    }

    /// <summary>
    /// Get multiple files using FTP. Requires a local path to put the files,
    /// and a regex to indicate what files to retrieve. Example regex: '*.bai'. 
    /// </summary>
    /// <param name="localFilePath"></param>
    /// <param name="regex"></param>
    /// <returns>int - Returns number of files retrieved.</returns>
    public override void GetFiles(string serverRegex, string localFilePath)
    {
      try
      {
        if (client == null)
        {
          throw new Exception("FTP Error: not connected to server");
        }
        // Bug 26455: GetFiles deprecated
        // Bug 27443: Use a simple Download method.  Amazingly enough, I only need to 
        // send in the file search pattern regex to get the files (you can use '*' if you want)
        // since the directory has already been set
        // in a call to sftp.ChangeRemoteDirectory().
        client.Download(serverRegex, localFilePath, Rebex.IO.TraversalMode.MatchFilesShallow, Rebex.IO.TransferMethod.Copy, Rebex.IO.ActionOnExistingFiles.OverwriteAll);

      }
      catch (Exception e)
      {
        errMsg = "FTP Error: " + e.Message;
        Logger.cs_log(errMsg + Environment.NewLine + e.StackTrace + Environment.NewLine);
        throw;      // Re-throw and preserve stack
      }
    }

    /// <summary>
    /// Deletes a file on the remote FTP server identified by serverFilePath
    /// </summary>
    /// <param name="serverFilePath"></param>
    /// <returns>Returns true on success, else false.</returns>
    public override void DeleteFile(string serverFilePath)
    {
      try
      {
        if (client == null)
        {
          throw new Exception("FTP Error: not connected to server");
        }
        if (client.FileExists(serverFilePath))
        {
          client.DeleteFile(serverFilePath);
        }
        else
        {
          Logger.cs_log(String.Format("FTP DeleteFile: {0} doesn't exist on server", serverFilePath));
        }
      }
      catch (Exception e)
      {
        errMsg = "FTP Error: " + e.Message + e.ToString();
        Logger.cs_log(e.Message + Environment.NewLine + e.StackTrace + Environment.NewLine);
        throw;      // Re-throw and preserve stack
      }
    }

    /// <summary>
    /// Does file exist on server?
    /// </summary>
    /// <param name="serverFilePath"></param>
    /// <returns>Returns true on existence, else false.</returns>
    public override bool FileExists(string serverFilePath)
    {
      bool retVal = false;
      try
      {
        if (client == null)
        {
          throw new Exception("FTP Error: not connected to server");
        }
        retVal = client.FileExists(serverFilePath);
      }
      catch (Exception e)
      {
        errMsg = "FTP Error: " + e.Message;
        Logger.cs_log(errMsg + Environment.NewLine + e.StackTrace + Environment.NewLine);
        throw;      // Re-throw and preserve stack
      }
      return retVal;
    }

  } // end FTP class

  public class CASL_FTP
  {
    public FTP ftp; // dispatch to for implementation

    public CASL_FTP(GenericObject Args)
    {
      ftp = new FTP(Args);
    }

    /// <summary>
    /// FTP Append
    /// Parameters
    ///  1: "host" - IP address or name.
    ///  2: "uid"  - user ID
    ///  3: "pwd"  - password
    ///  4: local_file_name
    ///  5: server_file_name
    /// </summary>
    /// <returns></returns>
    public static Object FTP_AppendFile(params object[] args)
    {
      GenericObject Args = misc.convert_args(args);

      CASL_FTP ftp = null;

      try
      {
        // CASL interface is going to be slower, since we open/close connections on
        // each call. But the interface is mainly just for testing
        ftp = new CASL_FTP(Args);

        // Validate & get method-specific parameters
        if (!Args.has("local_file_name")) throw CASL_error.create("message", "FTP local_file_name param missing",
          "code", "FTP-05");
        string strLocal = (string)Args.get("local_file_name");

        if (!Args.has("server_file_name")) throw CASL_error.create("message", "FTP server_file_name param missing",
          "code", "FTP-06");
        string strServer = (string)Args.get("server_file_name");

        ftp.ftp.Connect();
        ftp.ftp.AppendFile(strLocal, strServer);
      }
      catch (Exception)
      {
        Logger.LogError(ftp.ftp.errMsg, "FTP Append File");
        throw CASL_error.create("message", "FTP Error: " + ftp.ftp.errMsg, "code", "FTP-10");
      }
      finally
      {
        if (ftp != null) ftp.ftp.Disconnect();
      }
      return true;
    }

    /// <summary>
    /// Gets a file using FTP. Parameters are taken from CASL.
    /// Added a new parameter: remote_path
    /// </summary>
    /// <param name="args"></param>
    /// <returns>bool</returns>
    public static Object FTP_GetFile(params object[] args)
    {
      GenericObject Args = misc.convert_args(args);

      CASL_FTP ftp = null;

      try
      {
        // CASL interface is going to be slower, since we open/close connections on
        // each call. But the interface is mainly just for testing
        ftp = new CASL_FTP(Args);

        // Validate & get method-specific parameters
        if (!Args.has("local_file_name")) throw CASL_error.create("message", "FTP local_file_name param missing",
        "code", "FTP-05");
        string strLocal = (string)Args.get("local_file_name");

        if (!Args.has("server_file_name")) throw CASL_error.create("message", "FTP server_file_name param missing",
        "code", "FTP-06");
        string strServer = (string)Args.get("server_file_name");

        ftp.ftp.Connect();

        if (Args.has("remote_path"))
        {
          string remotePath = (string)Args.get("remote_path");
          ftp.ftp.ChangeRemoteDirectory(remotePath);
        }

        ftp.ftp.GetFile(strServer, strLocal);
      }
      catch (Exception)
      {
        Logger.LogError(ftp.ftp.errMsg, "FTP Get File");
        throw CASL_error.create("message", "FTP Error: " + ftp.ftp.errMsg, "code", "FTP-10");
      }
      finally
      {
        if (ftp != null) ftp.ftp.Disconnect();
      }
      return true;
    }

    /// <summary>
    /// Gets multiple files using FTP. Parameters are taken from CASL.
    /// Requires a local path to put the files, and a regex to indicate
    /// what files to retrieve. Example regex: '*.bai'.
    /// Returns true if at least one file is retrieved, else false.
    /// </summary>
    /// <param name="args"></param>
    /// <returns>int - Returns number of files retrieved.</returns>
    public static Object FTP_GetFiles(params object[] args)
    {
      int retVal = 0;
      GenericObject Args = misc.convert_args(args);

      CASL_FTP ftp = null;

      try
      {
        // CASL interface is going to be slower, since we open/close connections on
        // each call. But the interface is mainly just for testing
        ftp = new CASL_FTP(Args);

        // Validate & get method-specific parameters
        if (!Args.has("server_path_regex")) throw CASL_error.create("message", "FTP server_path_regex param missing",
        "code", "FTP-07");
        string strServerRegex = (string)Args.get("server_path_regex");

        if (!Args.has("local_path_name")) throw CASL_error.create("message", "FTP local_path_name param missing",
        "code", "FTP-05");
        string strPath = (string)Args.get("local_path_name");

        ftp.ftp.Connect();
        ftp.ftp.GetFiles(strServerRegex, strPath);
      }
      catch (Exception)
      {
        Logger.LogError(ftp.ftp.errMsg, "FTP Get Files");
        throw CASL_error.create("message", "FTP Error: " + ftp.ftp.errMsg, "code", "FTP-10");
      }
      finally
      {
        if (ftp != null) ftp.ftp.Disconnect();
      }
      return retVal;
    }

    /// <summary>
    /// Deletes a file on the remote FTP server. Parameters are taken from CASL.
    /// If the file doesn't exist, this method will return true by design.
    /// </summary>
    /// <param name="args"></param>
    /// <returns>bool</returns>
    public static Object FTP_DeleteFile(params object[] args)
    {
      GenericObject Args = misc.convert_args(args);

      CASL_FTP ftp = null;

      try
      {
        // CASL interface is going to be slower, since we open/close connections on
        // each call. But the interface is mainly just for testing
        ftp = new CASL_FTP(Args);

        // Validate & get method-specific parameters
        if (!Args.has("server_file_name")) throw CASL_error.create("message", "FTP server_file_name param missing",
        "code", "FTP-06");
        string strServer = (string)Args.get("server_file_name");

        ftp.ftp.Connect();
        ftp.ftp.DeleteFile(strServer);
      }
      catch (Exception)
      {
        Logger.LogError(ftp.ftp.errMsg, "FTP Delete File");
        throw CASL_error.create("message", "FTP Error: " + ftp.ftp.errMsg, "code", "FTP-10");
      }
      finally
      {
        if (ftp != null) ftp.ftp.Disconnect();
      }
      return true;
    }

    /// <summary>
    /// Puts (sends) a file to the remote server using FTP.
    /// Takes its arguments from CASL.
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static Object FTP_SendFile(params object[] args)
    {
      GenericObject Args = misc.convert_args(args);

      CASL_FTP ftp = null;

      try
      {
        // CASL interface is going to be slower, since we open/close connections on
        // each call. But the interface is mainly just for testing
        ftp = new CASL_FTP(Args);

        // Validate & get method-specific parameters
        if (!Args.has("local_file_name")) throw CASL_error.create("message", "FTP local_file_name param missing",
        "code", "FTP-05");
        string strLocal = (string)Args.get("local_file_name");

        if (!Args.has("server_file_name")) throw CASL_error.create("message", "FTP server_file_name param missing",
        "code", "FTP-06");
        string strServer = (string)Args.get("server_file_name");

        ftp.ftp.Connect();
        ftp.ftp.SendFile(strLocal, strServer);
      }
      catch (Exception)
      {
        Logger.LogError(ftp.ftp.errMsg, "FTP Send File");
        throw CASL_error.create("message", "FTP Error: " + ftp.ftp.errMsg, "code", "FTP-10");
      }
      finally
      {
        if (ftp != null) ftp.ftp.Disconnect();
      }
      return true;
    }

    /// <summary>
    /// Check if file exists on server. Hook from CASL.
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static Object FTP_FileExists(params object[] args)
    {
      GenericObject Args = misc.convert_args(args);
      bool retVal = false;

      CASL_FTP ftp = null;

      try
      {
        // CASL interface is going to be slower, since we open/close connections on
        // each call. But the interface is mainly just for testing
        ftp = new CASL_FTP(Args);

        // Validate & get method-specific parameters
        if (!Args.has("server_file_name")) throw CASL_error.create("message", "FTP server_file_name param missing",
        "code", "FTP-06");
        string strServer = (string)Args.get("server_file_name");

        ftp.ftp.Connect();
        retVal = ftp.ftp.FileExists(strServer);
      }
      catch (Exception)
      {
        Logger.LogError(ftp.ftp.errMsg, "FTP File Exists");
        throw CASL_error.create("message", "FTP Error: " + ftp.ftp.errMsg, "code", "FTP-10");
      }
      finally
      {
        if (ftp != null) ftp.ftp.Disconnect();
      }
      return retVal;
    }
  }     // END CASL_FTP class

  public class SFTP : FileTransfer
  {
    protected Sftp client = null;

    protected string privateKeyPath;
    protected string hostKeyFingerPrint;
    protected string passphrase;

    /// <summary>
    /// Defaulted constructors (will mainly be used from C#)
    /// </summary>
    /// <param name="Host"></param>
    /// <param name="User"></param>
    /// <param name="Password"></param>
    /// <param name="Port"></param>

    // use from C# if you are only using password and user
    public SFTP(string Host, string User, string Password, string Port)
      : this(Host, User, Password, Port, true, false, "", "", "")
    {
    }

    // use from C# if you are only using password and user
    public SFTP(string Host, string User, string Password, bool PwdIsEncrypted)
      : this(Host, User, Password, "22", true, PwdIsEncrypted, "", "", "")
    {
    }

    // use from C# if you are only using user & public key
    public SFTP(string Host, string User, string PrivateKeyPath, string HostKeyFingerprint, string Passphrase)
      : this(Host, User, "", "22", true, false, PrivateKeyPath, HostKeyFingerprint, HostKeyFingerprint)
    {
    }

    /// <summary>
    /// Full Constructor Use from CASL or C# (if need set everything)
    /// </summary>
    /// <param name="Host"></param>
    /// <param name="User"></param>
    /// <param name="Password"></param>
    /// <param name="Port"></param>
    /// <param name="UseBinary"></param>
    /// <param name="KeepAlive"></param>
    /// <param name="UsePassive"></param>
    /// <param name="EnableSSL"></param>
    public SFTP(string Host, string User, string Password, string Port, bool UseBinary, bool PwdIsEncrypted,
        string PrivateKeyPath, string HostKeyFingerprint, string Passphrase)
      : base(Host, User, Password, Port, UseBinary, PwdIsEncrypted)
    {
      privateKeyPath = PrivateKeyPath;
      hostKeyFingerPrint = HostKeyFingerprint;

      // Decrypt key file passphrase also if needed.  Bug 13690. FT
      if (!string.IsNullOrEmpty(Passphrase) && pwdIsEncrypted)
      {
        string secret_key = Crypto.get_secret_key();
        byte[] content_bytes = Convert.FromBase64String(Passphrase);
        byte[] decrypted_bytes = Crypto.symmetric_decrypt("data", content_bytes, "secret_key", secret_key);
        passphrase = System.Text.Encoding.Default.GetString(decrypted_bytes);
      }
      else
        passphrase = Passphrase;
    }

    // Call from C# when interfacing to CASL
    public SFTP(GenericObject Args)
      : base(Args)
    {
      object arg = Args.get("private_keypath", c_CASL.c_opt);
      privateKeyPath = (arg != c_CASL.c_opt) ? (string)arg : "";

      arg = Args.get("host_key_fingerprint", c_CASL.c_opt);
      hostKeyFingerPrint = (arg != c_CASL.c_opt) ? (string)arg : "";

      arg = Args.get("passphrase", c_CASL.c_opt);
      passphrase = (arg != c_CASL.c_opt) ? (string)arg : "";
    }

    ~SFTP()
    {
      Disconnect();
    }

    /// <summary>
    /// Create new Rebex client. Connect and login to server
    /// </summary>
    public override void Connect()
    {
      Connect(CONNECTION_TIMEOUT_DEFAULT);
    }

    /// <summary>
    /// New version with a connection time out paramter.
    /// Bug 23202
    /// </summary>
    /// <param name="connectionTimeout"></param>
    public override void Connect(int connectionTimeout)
    {
      Connect(connectionTimeout, false, "");
    }

    /// <summary>
    /// A version of Connect that writes to the Rebex log.
    /// We need the Rebex log to send problems to Rebex support.
    /// Bug 24836 
    /// </summary>
    /// <param name="connectionTimeout"></param>
    /// <param name="logVerbose"></param>
    /// <param name="rebexLogPath"></param>
    public override void Connect(int connectionTimeout, bool logVerbose, string rebexLogPath)
    {

      try
      {
        client = new Sftp();
        client.Timeout = connectionTimeout;     // Bug 23202

        if (logVerbose && !string.IsNullOrEmpty(rebexLogPath)) // IPAY-364 Add another condition
        {
          // IPAY-364; add inner [try..catch] only catch filelogwriter problem, such as rebexlogpath is not exist; write error to cs log; and allow batch update continue
          try
          {
            // check rebexLogPath is file or directory; if it is directory, combine file name.
            FileAttributes file_attr = File.GetAttributes(rebexLogPath);
            if (file_attr.HasFlag(FileAttributes.Directory))
            {
              DateTime t = DateTime.Now;
              string file_name = t.ToString("yyyy-MM-dd") + ".txt";
              rebexLogPath = Path.Combine(rebexLogPath, file_name);
            }
            client.LogWriter = new Rebex.FileLogWriter(rebexLogPath, Rebex.LogLevel.Verbose);
          }
          catch (Exception e)
          {
            errMsg = "SFTP Rebex File Log Error: " + e.Message + Environment.NewLine + e.StackTrace + Environment.NewLine;
            Logger.cs_log(errMsg);
          }

        }

        // Bug 16762: DJD - FTP/SFTP Is Not Setting Port Number When Connecting
        if (string.IsNullOrEmpty(port))
        {
          client.Connect(host);
        }
        else
        {
          int serverPort = Convert.ToInt32(port);
          client.Connect(host, serverPort);
        }

        if (hostKeyFingerPrint != "")
        {
          // get the host key fingerprint and compare
          string host_fingerprint = client.Fingerprint;
          if (host_fingerprint != hostKeyFingerPrint)
          {
            errMsg = "SFTP Error: Host key fingerprint doesn't match configured fingerprint";
            Logger.cs_log(errMsg);
            throw new Exception(errMsg);
          }
        }

        // Some servers require public key authentication before password authentication can
        // occur
        if (privateKeyPath != "")
        {
          SshPrivateKey privateKey = new SshPrivateKey(privateKeyPath, passphrase);
          if (string.IsNullOrEmpty(password)) // Bug 15815 DJD: FSA Product Validation Changes - SFTP EPL List Access
          {
            client.Login(user, privateKey);
          }
          else
          {
            client.Login(user, password, privateKey); // Bug 15815 DJD: FSA Product Validation Changes - SFTP EPL List Access
          }
        }
        else
        {
          client.Login(user, password);
        }
        // Bug 26455: Only need this for things like progress bars.
        // See rebex.net/sftp.net/features/file-transfer.aspx#progress
        //client.TransferProgressChanged != ...
      }
      catch (Exception e)
      {
        Disconnect();
        errMsg = "SFTP Error: " + e.Message + e.ToString();
        Logger.cs_log(e.Message + Environment.NewLine + e.StackTrace + Environment.NewLine);
        throw;      // Re-throw and preserve stack
      }
    }

    /// <summary>
    /// Disconnect from server
    /// </summary>
    public override void Disconnect()
    {
      if (client != null)
      {
        client.Disconnect();
        client = null;
      }
    }

    /// <summary>
    /// Handler for updating batch progress of GetFiles
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void TransferProgress(object sender, SftpBatchTransferProgressEventArgs e)
    {
      count = e.FilesTransferred;
    }

    /// <summary>
    /// Get the file located at serverFilePath and store it as the localFilePath
    /// </summary>
    /// <param name="serverFilePath"></param>
    /// <param name="localFilePath"></param>
    /// <returns>Throws exception on error, else void.</returns>
    public override void GetFile(string serverFilePath, string localFilePath)
    {
      try
      {
        if (client == null)
        {
          throw new Exception("FTP Error: not connected to server");
        }
        client.GetFile(serverFilePath, localFilePath);
      }
      catch (Exception e)
      {
        errMsg = "SFTP Error: " + e.Message + e.ToString();
        Logger.cs_log(e.Message + Environment.NewLine + e.StackTrace + Environment.NewLine);
        throw;      // Re-throw and preserve stack
      }
    }

    /// <summary>
    /// Get the file located at serverFilePath and store it as the localFilePath. If Unzip is
    /// true then unzip file while downloading to localFilePath. (Assumes a single zipped file.)
    /// </summary>
    /// <param name="serverFilePath"></param>
    /// <param name="localFilePath"></param>
    /// <param name="bUnzip"></param>
    /// <returns>Throws exception on error, else void.</returns>
    public void GetFile(string serverFilePath, string localFilePath, bool bUnzip)
    {
      if (bUnzip)
      {
        try
        {
          if (client == null)
          {
            throw new Exception("FTP Error: not connected to server");
          }
          Stream DownloadFtpStream = client.GetDownloadStream(serverFilePath);
          UnzipStream(DownloadFtpStream, serverFilePath, localFilePath);
        }
        catch (Exception e)
        {
          errMsg = "SFTP Error: " + e.Message + e.ToString();
          Logger.cs_log(e.Message + Environment.NewLine + e.StackTrace + Environment.NewLine);
          throw;      // Re-throw and preserve stack
        }
      }
      else
      {
        GetFile(serverFilePath, localFilePath);
      }
    }

    /// <summary>
    /// Change directory on the FTP server.
    ///  "." pathnames are ignored.  
    ///  Use forward slash for directory delimeter.
    /// </summary>
    /// <param name="remotePath"></param>
    public override void ChangeRemoteDirectory(string remotePath)
    {
      try
      {
        if (client == null)
        {
          throw new Exception("ChangeRemoteDirectory: FTP Error: not connected to server");
        }
        client.ChangeDirectory(remotePath);
      }
      catch (Exception e)
      {
        errMsg = "FTP Error: ChangeRemoteDirectory: " + e.Message + e.ToString();
        Logger.cs_log(e.Message + Environment.NewLine + e.StackTrace + Environment.NewLine);
        throw;      // Re-throw and preserve stack
      }
    }

    /// <summary>
    /// Put localFilePath to the file named serverFilePath on the foreign server
    /// </summary>
    /// <param name="localFilePath"></param>
    /// <param name="serverFilePath"></param>
    /// <returns>Throws exception on error, else void.</returns>
    public override void SendFile(string localFilePath, string serverFilePath)
    {
      try
      {
        if (client == null)
        {
          throw new Exception("FTP Error: not connected to server");
        }
        client.PutFile(localFilePath, serverFilePath);
      }
      catch (Exception e)
      {
        errMsg = "SFTP Error: " + e.Message + e.ToString();
        Logger.cs_log(e.Message + Environment.NewLine + e.StackTrace + Environment.NewLine);
        throw;      // Re-throw and preserve stack
      }
    }

    /// <summary>
    /// Append localFilePath to the file named serverFilePath on the foreign server 
    /// </summary>
    /// <param name="localFilePath"></param>
    /// <param name="serverFilePath"></param>
    /// <returns>Throws exception on error, else void.</returns>
    public override void AppendFile(string localFilePath, string serverFilePath)
    {
      try
      {
        if (client == null)
        {
          throw new Exception("FTP Error: not connected to server");
        }
        // get the length of the remote file so we know where to append
        long remoteOffset = client.GetFileLength(serverFilePath);

        // open the local file for reading 
        using (Stream stream = File.OpenRead(localFilePath))
        {
          // transfer data
          client.PutFile(stream, serverFilePath, remoteOffset, -1);
        }
      }
      catch (Exception e)
      {
        errMsg = "SFTP Error: " + e.Message + e.ToString();
        Logger.cs_log(e.Message + Environment.NewLine + e.StackTrace + Environment.NewLine);
        throw;      // Re-throw and preserve stack
      }
    }

    /// <summary>
    /// Get multiple files using FTP. Requires a local path to put the files,
    /// and a regex to indicate what files to retrieve. Example regex: '*.bai'. 
    /// </summary>
    /// <param name="localFilePath"></param>
    /// <param name="regex"></param>
    /// <returns>int - Returns number of files retrieved.</returns>
    public override void GetFiles(string serverRegex, string localFilePath)
    {
      try
      {
        if (client == null)
        {
          throw new Exception("FTP Error: not connected to server");
        }
        // Bug 26455: GetFiles deprecated
        // Bug 27443: Use a simple Download method.  Amazingly enough, I only need to 
        // send in the file search pattern regex to get the files since the directory has already been set
        // in a call to sftp.ChangeRemoteDirectory().
        client.Download(serverRegex, localFilePath, Rebex.IO.TraversalMode.MatchFilesShallow, Rebex.IO.TransferMethod.Copy, Rebex.IO.ActionOnExistingFiles.OverwriteAll);

      }
      catch (Exception e)
      {
        errMsg = "SFTP Error: " + e.Message;
        Logger.cs_log(errMsg + Environment.NewLine + e.StackTrace + Environment.NewLine);
        throw;      // Re-throw and preserve stack
      }
    }

    /// <summary>
    /// Deletes a file on the remote FTP server identified by serverFilePath
    /// </summary>
    /// <param name="serverFilePath"></param>
    /// <returns>Throws exception on error, else void.</returns>
    public override void DeleteFile(string serverFilePath)
    {
      try
      {
        if (client == null)
        {
          throw new Exception("FTP Error: not connected to server");
        }
        if (client.FileExists(serverFilePath))
        {
          client.DeleteFile(serverFilePath);
        }
        else
        {
          Logger.cs_log(String.Format("SFTP DeleteFile: {0} doesn't exist on server", serverFilePath));
        }
      }
      catch (Exception e)
      {
        errMsg = "SFTP Error: " + e.Message + e.ToString();
        Logger.cs_log(e.Message + Environment.NewLine + e.StackTrace + Environment.NewLine);
        throw;      // Re-throw and preserve stack
      }
    }

    /// <summary>
    /// Does file exist on server?
    /// </summary>
    /// <param name="serverFilePath"></param>
    /// <returns>Returns true on existence, else false.</returns>
    public override bool FileExists(string serverFilePath)
    {
      bool retVal = false;
      try
      {
        if (client == null)
        {
          throw new Exception("FTP Error: not connected to server");
        }
        retVal = client.FileExists(serverFilePath);
      }
      catch (Exception e)
      {
        errMsg = "SFTP Error: " + e.Message;
        Logger.cs_log(errMsg + Environment.NewLine + e.StackTrace + Environment.NewLine);
        throw;      // Re-throw and preserve stack
      }
      return retVal;
    }

  } // end SFTP class

  public class CASL_SFTP
  {
    public SFTP sftp; // dispatch to for implementation

    public CASL_SFTP(GenericObject Args)
    {
      sftp = new SFTP(Args);
    }

    /// <summary>
    /// SFTP Append
    /// Parameters
    ///  1: "host" - IP address or name.
    ///  2: "uid"  - user ID
    ///  3: "pwd"  - password
    ///  4: local_file_name
    ///  5: server_file_name
    /// </summary>
    /// <returns></returns>
    public static Object SFTP_AppendFile(params object[] args)
    {
      GenericObject Args = misc.convert_args(args);
      CASL_SFTP sftp = null;

      try
      {
        // CASL interface is going to be slower, since we open/close connections on
        // each call. But the interface is mainly just for testing
        sftp = new CASL_SFTP(Args);

        // Validate & get method-specific parameters
        if (!Args.has("local_file_name")) throw CASL_error.create("message", "SFTP local_file_name param missing",
        "code", "SFTP-05");
        string strLocal = (string)Args.get("local_file_name");

        if (!Args.has("server_file_name")) throw CASL_error.create("message", "SFTP server_file_name param missing",
        "code", "SFTP-06");
        string strServer = (string)Args.get("server_file_name");

        sftp.sftp.Connect();
        sftp.sftp.AppendFile(strLocal, strServer);
      }
      catch (Exception)
      {
        Logger.LogError(sftp.sftp.errMsg, "SFTP Append File");
        throw CASL_error.create("message", "SFTP Error: " + sftp.sftp.errMsg, "code", "SFTP-10");
      }
      finally
      {
        if (sftp != null) sftp.sftp.Disconnect();
      }
      return true;
    }

    /// <summary>
    /// Gets a file using SFTP. Parameters are taken from CASL.
    /// </summary>
    /// <param name="args"></param>
    /// <returns>bool</returns>
    public static Object SFTP_GetFile(params object[] args)
    {
      GenericObject Args = misc.convert_args(args);
      CASL_SFTP sftp = null;

      try
      {

        // CASL interface is going to be slower, since we open/close connections on
        // each call. But the interface is mainly just for testing
        sftp = new CASL_SFTP(Args);

        // Validate & get method-specific parameters
        if (!Args.has("local_file_name")) throw CASL_error.create("message", "SFTP local_file_name param missing",
        "code", "SFTP-05");
        string strLocal = (string)Args.get("local_file_name");

        if (!Args.has("server_file_name")) throw CASL_error.create("message", "SFTP server_file_name param missing",
        "code", "SFTP-06");
        string strServer = (string)Args.get("server_file_name");

        sftp.sftp.Connect();
        sftp.sftp.GetFile(strServer, strLocal);
      }
      catch (Exception)
      {
        Logger.LogError(sftp.sftp.errMsg, "SFTP Get File");
        throw CASL_error.create("message", "SFTP Error: " + sftp.sftp.errMsg, "code", "SFTP-10");
      }
      finally
      {
        if (sftp != null) sftp.sftp.Disconnect();
      }
      return true;
    }

    /// <summary>
    /// Gets multiple files using FTP. Parameters are taken from CASL.
    /// Requires a local path to put the files, and a regex to indicate
    /// what files to retrieve. Example regex: '*.bai'.
    /// Returns true if at least one file is retrieved, else false.
    /// </summary>
    /// <param name="args"></param>
    /// <returns>int - Returns number of files retrieved.</returns>
    public static Object SFTP_GetFiles(params object[] args)
    {
      int retVal = 0;
      GenericObject Args = misc.convert_args(args);

      CASL_SFTP sftp = null;

      try
      {
        // CASL interface is going to be slower, since we open/close connections on
        // each call. But the interface is mainly just for testing
        sftp = new CASL_SFTP(Args);

        // Validate & get method-specific parameters
        if (!Args.has("server_path_regex")) throw CASL_error.create("message", "SFTP server_path_regex param missing",
        "code", "SFTP-07");
        string strServerRegex = (string)Args.get("server_path_regex");

        if (!Args.has("local_path_name")) throw CASL_error.create("message", "SFTP local_path_name param missing",
        "code", "SFTP-05");
        string strPath = (string)Args.get("local_path_name");

        sftp.sftp.Connect();
        sftp.sftp.GetFiles(strServerRegex, strPath);
      }
      catch (Exception)
      {
        Logger.LogError(sftp.sftp.errMsg, "SFTP Get Files");
        throw CASL_error.create("message", "SFTP Error: " + sftp.sftp.errMsg, "code", "SFTP-10");
      }
      finally
      {
        if (sftp != null) sftp.sftp.Disconnect();
      }
      return retVal;
    }

    /// <summary>
    /// Deletes a file on the remote SFTP server. Parameters are taken from CASL.
    /// If the file doesn't exist, this method will return true by design.
    /// </summary>
    /// <param name="args"></param>
    /// <returns>bool</returns>
    public static Object SFTP_DeleteFile(params object[] args)
    {
      GenericObject Args = misc.convert_args(args);

      CASL_SFTP sftp = null;

      try
      {
        // CASL interface is going to be slower, since we open/close connections on
        // each call. But the interface is mainly just for testing
        sftp = new CASL_SFTP(Args);

        // Validate & get method-specific parameters
        if (!Args.has("server_file_name")) throw CASL_error.create("message", "SFTP server_file_name param missing",
        "code", "SFTP-06");
        string strServer = (string)Args.get("server_file_name");

        sftp.sftp.Connect();
        sftp.sftp.DeleteFile(strServer);
      }
      catch (Exception)
      {
        Logger.LogError(sftp.sftp.errMsg, "SFTP Delete File");
        throw CASL_error.create("message", "SFTP Error: " + sftp.sftp.errMsg, "code", "SFTP-10");
      }
      finally
      {
        if (sftp != null) sftp.sftp.Disconnect();
      }
      return true;
    }

    /// <summary>
    /// Puts (sends) a file to the remote server using SFTP.
    /// Takes its arguments from CASL.
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static Object SFTP_SendFile(params object[] args)
    {
      GenericObject Args = misc.convert_args(args);

      CASL_SFTP sftp = null;

      try
      {
        // CASL interface is going to be slower, since we open/close connections on
        // each call. But the interface is mainly just for testing
        sftp = new CASL_SFTP(Args);

        // Validate & get method-specific parameters
        if (!Args.has("local_file_name")) throw CASL_error.create("message", "SFTP local_file_name param missing",
        "code", "SFTP-05");
        string strLocal = (string)Args.get("local_file_name");

        if (!Args.has("server_file_name")) throw CASL_error.create("message", "SFTP server_file_name param missing",
        "code", "SFTP-06");
        string strServer = (string)Args.get("server_file_name");

        sftp.sftp.Connect();
        sftp.sftp.SendFile(strLocal, strServer);
      }
      catch (Exception)
      {
        Logger.LogError(sftp.sftp.errMsg, "SFTP Send File");
        throw CASL_error.create("message", "SFTP Error: " + sftp.sftp.errMsg, "code", "SFTP-10");
      }
      finally
      {
        if (sftp != null) sftp.sftp.Disconnect();
      }
      return true;
    }

    /// <summary>
    /// Check if file exists on server. Hook from CASL.
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static Object SFTP_FileExists(params object[] args)
    {
      GenericObject Args = misc.convert_args(args);
      bool retVal = false;

      CASL_SFTP sftp = null;

      try
      {
        // CASL interface is going to be slower, since we open/close connections on
        // each call. But the interface is mainly just for testing
        sftp = new CASL_SFTP(Args);

        // Validate & get method-specific parameters
        if (!Args.has("server_file_name")) throw CASL_error.create("message", "SFTP server_file_name param missing",
        "code", "SFTP-06");
        string strServer = (string)Args.get("server_file_name");

        sftp.sftp.Connect();
        retVal = sftp.sftp.FileExists(strServer);
      }
      catch (Exception)
      {
        Logger.LogError(sftp.sftp.errMsg, "SFTP File Exists");
        throw CASL_error.create("message", "SFTP Error: " + sftp.sftp.errMsg, "code", "SFTP-10");
      }
      finally
      {
        if (sftp != null) sftp.sftp.Disconnect();
      }
      return retVal;
    }

    /// <summary>
    /// Generate a public/private keypair.
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static string SFTP_GenerateKeyPair(params object[] args)
    {
      GenericObject Args = misc.convert_args(args);
      SshHostKeyAlgorithm keyAlgorithm = SshHostKeyAlgorithm.RSA;  // default to rsa
      SshPrivateKeyFormat keyFormat = SshPrivateKeyFormat.OpenSsh; // default to OpenSSH
      string retVal;

      // Validate & get method-specific parameters
      if (!Args.has("algorithm")) throw CASL_error.create("message", "SFTP algorithm param missing",
      "code", "SFTP-06");
      string algorithm = ((string)Args.get("algorithm")).ToLower();

      if (!Args.has("key_size")) throw CASL_error.create("message", "SFTP key_size param missing",
      "code", "SFTP-06");
      int key_size = (int)Args.get("key_size");

      if (!Args.has("key_format")) throw CASL_error.create("message", "SFTP key_format param missing",
      "code", "SFTP-06");
      string key_format = ((string)Args.get("key_format")).ToLower();

      if (!Args.has("private_keypath")) throw CASL_error.create("message", "SFTP private_keypath param missing",
      "code", "SFTP-06");
      string private_keypath = (string)Args.get("private_keypath");

      if (!Args.has("public_keypath")) throw CASL_error.create("message", "SFTP public_keypath param missing",
      "code", "SFTP-06");
      string public_keypath = (string)Args.get("public_keypath");

      object arg = Args.get("passphrase", c_CASL.c_opt);
      string passphrase = (arg != c_CASL.c_opt) ? (string)arg : null;

      // select the key algorithm (RSA or DSS)
      if (algorithm == "dss")
        keyAlgorithm = SshHostKeyAlgorithm.DSS;

      // select the key format
      if (key_format == "pkcs8")
        keyFormat = SshPrivateKeyFormat.Pkcs8;
      else if (key_format == "putty")
        keyFormat = SshPrivateKeyFormat.Putty;

      try
      {
        // generate private key 
        SshPrivateKey privateKey = SshPrivateKey.Generate(keyAlgorithm, key_size);

        // save the private key 
        privateKey.Save(private_keypath, passphrase, keyFormat);

        // save the public key 
        privateKey.SavePublicKey(public_keypath);

        // construct the return value
        // get the raw form of SSH public key
        byte[] rawPublicKey = privateKey.GetPublicKey();

        // the string to be added to OpenSSH's ~/.ssh/authorized_keys file
        string sshPublicKey = string.Format("{0} {1}", algorithm, Convert.ToBase64String(rawPublicKey));

        // and display it
        retVal = string.Format("<p>Add the following line to your ~/.ssh/authorized_keys file:</p><pre>{0}</pre><p/>",
        sshPublicKey);
      }
      catch (Exception e)
      {
        string errMsg = "SFTP Error: " + e.Message;
        Logger.LogError(errMsg, "SFTP Key Pair");
        throw CASL_error.create("message", "SFTP Error: " + errMsg, "code", "SFTP-10");
      }
      return retVal;
    }
  }     // END CASL_SFTP class
}
