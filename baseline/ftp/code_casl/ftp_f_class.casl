<?xml version="1.0" encoding="utf-8" ?> 
<do>

<!-- Rewritten for Bug 13690 and related TTS entries -->
GEO.<defclass _name='Ftp'>

  <!--
  Ftp.<ftp_append_file> 
  host="ftp.corebt.com" 
  uid="rpftp"
  pwd="core2224" 
  port="21"
  local_file_name="c:\0\1.txt"
  server_file_name="1.txt"
  </ftp_append_file> 
  -->
  <defmethod _name='ftp_append_file'>
    host = req 
    uid = req 
    pwd = req  
    port = opt 
    local_file_name = req 
    server_file_name = req
    pwd_is_encrypted = <if> pwd.<is_a> encrypted </is_a> true else false </if>
    
    use_binary  = opt = Boolean
    keep_alive  = opt = Boolean
    use_passive = opt = Boolean
    enable_ssl  = opt = Boolean
    
    _impl = cs.ftp.CASL_FTP.FTP_AppendFile
  </defmethod>


  <!--
  Ftp.<ftp_get_file> 
  host="ftp.corebt.com" 
  uid="rpftp"
  pwd="core2224" 
  port="21"
  local_file_name="c:\1\cats.txt"
  server_file_name="cats_file.txt"
  </ftp_get_file> 
  -->
  <defmethod _name='ftp_get_file'>
    host = req 
    uid = req 
    pwd = req 
    port = opt 
    local_file_name = req 
    server_file_name = req
    pwd_is_encrypted = <if> pwd.<is_a> encrypted </is_a> true else false </if>
    
    use_binary  = opt = Boolean
    keep_alive  = opt = Boolean
    use_passive = opt = Boolean
    enable_ssl  = opt = Boolean
    
    _impl = cs.ftp.CASL_FTP.FTP_GetFile
  </defmethod>

  <!--
  Ftp.<ftp_delete_file> 
  host="ftp.corebt.com" 
  uid="rpftp"
  pwd="core2224" 
  port="21"
  server_file_name="cats_file.txt"
  </ftp_delete_file> 
  -->
  <defmethod _name='ftp_delete_file'> 
    host = req 
    uid = req 
    pwd = req 
    port = opt 
    server_file_name = req
    pwd_is_encrypted = <if> pwd.<is_a> encrypted </is_a> true else false </if>
    
    use_binary  = opt = Boolean
    keep_alive  = opt = Boolean
    use_passive = opt = Boolean
    enable_ssl  = opt = Boolean
    
    _impl = cs.ftp.CASL_FTP.FTP_DeleteFile
  </defmethod>
  
  <!--
  Ftp.<ftp_get_files> 
  host="ftp.corebt.com" 
  uid="rpftp"
  pwd="core2224" 
  port="21"
  local_path_name="c:\\TEMP"
  server_path_regex=".*\\.bai"
  </ftp_get_files> 
  -->
  <defmethod _name='ftp_get_files'> 
    host = req 
    uid = req 
    pwd = req 
    port = opt 
    local_path_name = req = String
    server_path_regex = req = String
    pwd_is_encrypted = <if> pwd.<is_a> encrypted </is_a> true else false </if>
    
    use_binary  = opt = Boolean
    keep_alive  = opt = Boolean
    use_passive = opt = Boolean
    enable_ssl  = opt = Boolean
    
    _impl = cs.ftp.CASL_FTP.FTP_GetFiles
  </defmethod>

<!--
Ftp.<ftp_send_file> 
host="ftp.corebt.com" 
uid="rpftp"
pwd="core2224" 
port="21"
local_file_name="c:\1\cats.txt"
server_file_name="cats_file.txt"
</ftp_send_file> 
-->
  <defmethod _name='ftp_send_file'> 
    host = req 
    uid = req 
    pwd = req 
    port = opt 
    local_file_name = req 
    server_file_name = req
    pwd_is_encrypted = <if> pwd.<is_a> encrypted </is_a> true else false </if>
    
    use_binary  = opt = Boolean
    keep_alive  = opt = Boolean
    use_passive = opt = Boolean
    enable_ssl  = opt = Boolean
    
    _impl = cs.ftp.CASL_FTP.FTP_SendFile
  </defmethod>

  <!--
  Ftp.<ftp_file_exists> 
  host="ftp.corebt.com" 
  uid="rpftp"
  pwd="core2224" 
  port="21"
  server_file_name="cats_file.txt"
  </ftp_file_exists> 
  -->
  <defmethod _name='ftp_file_exists'> 
    host = req 
    uid = req 
    pwd = req 
    port = opt 
    server_file_name = req
    pwd_is_encrypted = <if> pwd.<is_a> encrypted </is_a> true else false </if>
    
    use_binary  = opt = Boolean
    keep_alive  = opt = Boolean
    use_passive = opt = Boolean
    enable_ssl  = opt = Boolean
    
    _impl = cs.ftp.CASL_FTP.FTP_FileExists
  </defmethod>

</defclass>

GEO.<defclass _name='Sftp'>

  <!--
  Sftp.<sftp_append_file> 
  host="sftp.corebt.com" 
  uid="rpftp"
  pwd="core2224" 
  port="22"
  local_file_name="c:\0\1.txt"
  server_file_name="1.txt"
  </sftp_append_file> 
  -->
  <defmethod _name='sftp_append_file'>
    host = req 
    uid = req 
    pwd = req  
    local_file_name = req 
    server_file_name = req
    pwd_is_encrypted = <if> pwd.<is_a> encrypted </is_a> true else false </if>
    
    port = opt = String
    private_keypath = opt = String
    host_key_fingerprint = opt = String
    use_binary  = opt = Boolean
    keep_alive  = opt = Boolean
    use_passive = opt = Boolean
    enable_ssl  = opt = Boolean
    
    _impl = cs.ftp.CASL_SFTP.SFTP_AppendFile
  </defmethod>

  <!--
  Sftp.<sftp_get_file> 
  host="sftp.corebt.com" 
  uid="rpftp"
  pwd="core2224" 
  port="22"
  local_file_name="c:\1\cats.txt"
  server_file_name="cats_file.txt"
  </sftp_get_file> 
  -->
  <defmethod _name='sftp_get_file'>
    host = req 
    uid = req 
    pwd = req 
    local_file_name = req 
    server_file_name = req
    pwd_is_encrypted = <if> pwd.<is_a> encrypted </is_a> true else false </if>
    
    port = opt = String
    private_keypath = opt = String
    host_key_fingerprint = opt = String
    use_binary  = opt = Boolean
    keep_alive  = opt = Boolean
    use_passive = opt = Boolean
    enable_ssl  = opt = Boolean
    
    _impl = cs.ftp.CASL_SFTP.SFTP_GetFile
  </defmethod>

  <!--
  Sftp.<sftp_delete_file> 
  host="sftp.corebt.com" 
  uid="rpftp"
  pwd="core2224" 
  port="22"
  server_file_name="cats_file.txt"
  </sftp_delete_file> 
  -->
  <defmethod _name='sftp_delete_file'> 
    host = req 
    uid = req 
    pwd = req 
    server_file_name = req
    pwd_is_encrypted = <if> pwd.<is_a> encrypted </is_a> true else false </if>
    
    port = opt = String
    private_keypath = opt = String
    host_key_fingerprint = opt = String
    use_binary  = opt = Boolean
    keep_alive  = opt = Boolean
    use_passive = opt = Boolean
    enable_ssl  = opt = Boolean
    
    _impl = cs.ftp.CASL_SFTP.SFTP_DeleteFile
  </defmethod>
  
  <!--
  Sftp.<sftp_get_files> 
  host="sftp.corebt.com" 
  uid="rpftp"
  pwd="core2224" 
  port="22"
  local_path_name="c:\\TEMP"
  server_path_regex=".*\\.bai"
  </sftp_get_files> 
  -->
  <defmethod _name='sftp_get_files'> 
    host = req 
    uid = req 
    pwd = req 
    local_path_name = req = String
    server_path_regex = req = String
    pwd_is_encrypted = <if> pwd.<is_a> encrypted </is_a> true else false </if>
    
    port = opt = String
    private_keypath = opt = String
    host_key_fingerprint = opt = String
    server_file_path = opt = String <!-- relative to root directory -->
    use_binary  = opt = Boolean
    keep_alive  = opt = Boolean
    use_passive = opt = Boolean
    enable_ssl  = opt = Boolean
    
    _impl = cs.ftp.CASL_SFTP.SFTP_GetFiles
  </defmethod>

<!--
Sftp.<sftp_send_file> 
host="sftp.corebt.com" 
uid="rpftp"
pwd="core2224" 
port="22"
local_file_name="c:\1\cats.txt"
server_file_name="cats_file.txt"
</sftp_send_file> 
-->
  <defmethod _name='sftp_send_file'> 
    host = req 
    uid = req 
    pwd = req 
    local_file_name = req 
    server_file_name = req
    pwd_is_encrypted = <if> pwd.<is_a> encrypted </is_a> true else false </if>
    
    port = opt = String
    private_keypath = opt = String
    host_key_fingerprint = opt = String
    use_binary  = opt = Boolean
    keep_alive  = opt = Boolean
    use_passive = opt = Boolean
    enable_ssl  = opt = Boolean
    
    _impl = cs.ftp.CASL_SFTP.SFTP_SendFile
  </defmethod>

  <!--
  Sftp.<sftp_file_exists> 
  host="sftp.corebt.com" 
  uid="rpftp"
  pwd="core2224" 
  port="22"
  server_file_name="cats_file.txt"
  </sftp_file_exists> 
  -->
  <defmethod _name='sftp_file_exists'> 
    host = req 
    uid = req 
    pwd = req 
    server_file_name = req
    pwd_is_encrypted = <if> pwd.<is_a> encrypted </is_a> true else false </if>
    
    port = opt = String
    private_keypath = opt = String
    host_key_fingerprint = opt = String
    use_binary  = opt = Boolean
    keep_alive  = opt = Boolean
    use_passive = opt = Boolean
    enable_ssl  = opt = Boolean
    
    _impl = cs.ftp.CASL_SFTP.SFTP_FileExists
  </defmethod>

  <defmethod _name='sftp_generate_keypair'>
    algorithm = "ssh-rsa" = String      <!-- Either "ssh-rsa" or "ssh-dss" -->
    key_size = 1024 = Integer       <!-- Use a sensible default key size -->
    key_format = "openssh" = String <!-- One of openssh, pkcs8, or putty -->
    private_keypath = req = String  <!-- Path to store private key -->
    public_keypath = req = String   <!-- Path to store public key -->
    passphrase = opt = String       <!-- passphrase to encrypt private key with. Not yet supported -->
    
    _impl = cs.ftp.CASL_SFTP.SFTP_GenerateKeyPair
  </defmethod>
  
  
  
</defclass>


</do>