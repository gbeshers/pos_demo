﻿using CASL_engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomizedACHBatchUpdate
{
  public enum EntryDetailType { Debit, Credit } // debit - tender, credit - deposit
  public class ACHEntryDetailTenderData
  {
    public GenericObject geoWorkgourp { get; set; }
    public GenericObject geoFile { get; set; }
    public GenericObject geoTndr { get; set; }
    public List<GenericObject> geoTrans_applied { get; set; } // transactions applied to tender
    public EntryDetailType entry_detail_type { get; set; }
  }

  public class CoreFileInfo
  {
    public string core_file_name { get; set; }
    public DateTime created_date { get; set; }
    public string deposit_slip_number { get; set; }
  }

  public class ProcessResult
  {
    public string ach_batch { get; set; }
    public decimal total_debit_amount_in_file { get; set; }
  }
  public static class ACHBUExtensionClass
  {
    public static string FormatDate(this DateTime dt, ACHRecordFormat format)
    {
      string rec_str = "";
      rec_str = string.Format($"{{0:{format.format_string}}}", dt);
      return rec_str;
    }

    public static string GetBatchElementValue(this ACHBatchElementInfo element_config_info, ACHEntryDetailTenderData entry_detail_data)
    {
      string rec_str = "";

      if (element_config_info.record_type == RecordType.DetailDebitRecord)
      {
        if (element_config_info.source_type == nameof(SourceType.Standard))
        {
          rec_str = element_config_info.GetStandardTypeData(entry_detail_data);
        }
        else if (element_config_info.source_type == nameof(SourceType.CustomField))
        {
          rec_str = element_config_info.GetCustomFieldValue(entry_detail_data);
        }
        else if (element_config_info.source_type == nameof(SourceType.FixedValue))
        {
          rec_str = element_config_info.fixed_value;
        }
      }
      else if (element_config_info.record_type == RecordType.DetailCreditRecord)
      {
        if (element_config_info.source_type == nameof(CreditSourceType.Standard))
        {
          rec_str = element_config_info.GetStandardTypeData(entry_detail_data);
        }
        else if (element_config_info.source_type == nameof(CreditSourceType.FixedValue))
        {
          rec_str = element_config_info.fixed_value;
        }
      }
      /*else if (element_config_info.record_type == RecordType.BatchRecord)
      {
        if (element_config_info.source_type == nameof(BatchSourceType.Standard))
        {
          rec_str = element_config_info.GetStandardTypeData(entry_detail_data);
        }
        else if (element_config_info.source_type == nameof(BatchSourceType.FixedValue))
        {
          rec_str = element_config_info.fixed_value;
        }
      }*/


      return rec_str;
    }

    public static string GetStandardTypeData(this ACHBatchElementInfo element_config_info, ACHEntryDetailTenderData entry_detail_data)
    {
      string rec_str = "";

      if (element_config_info.record_type == RecordType.DetailDebitRecord)
        rec_str = element_config_info.GetStandardTypeDataForDetailDebitRecord(entry_detail_data);
      else if (element_config_info.record_type == RecordType.DetailCreditRecord)
        rec_str = element_config_info.GetStandardTypeDataForDetailCreditRecord(entry_detail_data);
      /*else if (element_config_info.record_type == RecordType.BatchRecord)
      {}*/

      return rec_str;
    }
    public static string GetCustomFieldValue(this ACHBatchElementInfo element_config_info, ACHEntryDetailTenderData entry_detail_data)
    {
      string rec_str = "";

      if (element_config_info.source_from == nameof(SourceFrom.Tender))
      {
        rec_str = entry_detail_data.geoTndr.GetCustomFieldValueForTndr(element_config_info.source);
      }
      else if (element_config_info.source_from == nameof(SourceFrom.Transaction))
      {
        GenericObject geoTran = entry_detail_data.geoTrans_applied.FirstOrDefault();
        if (geoTran != null)
          rec_str = geoTran.GetCustomFieldValueForTran(element_config_info.source);

      }
      return rec_str;
    }
    public static string GetCustomFieldValueForTran(this GenericObject geoTran, string custom_field_tag_name)
    {
      string rec_str = "";
      if (geoTran != null)
      {
        GenericObject geoCustData = geoTran.get("GR_CUST_FIELD_DATA", new GenericObject()) as GenericObject;
        if (geoCustData != null)
        {
          for (int i = 0; i < geoCustData.getLength(); i++)
          {
            GenericObject cust_field = geoCustData.get(i) as GenericObject;
            string custTag = ((string)cust_field.get("CUSTTAG")).ToLower();
            string custValue = cust_field.get("CUSTVALUE") as string;

            if (string.Compare(custTag, custom_field_tag_name, true) == 0)
            {
              rec_str = custValue;
              break;
            }

          }
        }
      }
      return rec_str;
    }
    public static string GetCustomFieldValueForTndr(this GenericObject geoTndr, string custom_field_tag_name)
    {
      string rec_str = "";
      if (geoTndr != null)
      {
        GenericObject geoCustData = geoTndr.get("CUST_FIELD_DATA", new GenericObject()) as GenericObject;
        int iCountofCF = geoCustData.getIntKeyLength();
        if (geoCustData != null)
        {
          for (int i = 0; i < iCountofCF; i++)
          {
            GenericObject cust_field = geoCustData.get(i) as GenericObject;
            string custTag = ((string)cust_field.get("CUSTTAG", "")).ToLower();
            string custValue = cust_field.get("CUSTVALUE", "") as string;

            if (string.Compare(custTag, custom_field_tag_name, true) == 0)
            {
              rec_str = custValue;
              break;
            }

          }
        }
      }
      return rec_str;
    }
    public static string GetCoreFileName(this GenericObject geoFile)
    {
      string rec_str = "";
      if (geoFile != null)
      {
        string dep_file_nbr = geoFile.get("DEPFILENBR", "").ToString();
        int dep_file_seq = Convert.ToInt32(geoFile.get("DEPFILESEQ", "").ToString());
        rec_str = string.Format("{0}{1:D3}", dep_file_nbr, dep_file_seq);
      }
      return rec_str;
    }

    public static string GetStandardTypeDataForDetailDebitRecord(this ACHBatchElementInfo element_config_info, ACHEntryDetailTenderData entry_detail_data)
    {
      string rec_str = "";

      if (element_config_info.source_from == nameof(SourceFrom.Transaction))
      {
        if (element_config_info.source == nameof(StandardSource.PayFileNbr))
        {
          string dep_file_nbr = entry_detail_data.geoFile.get("DEPFILENBR", "").ToString();
          int dep_file_seq = Convert.ToInt32(entry_detail_data.geoFile.get("DEPFILESEQ", "").ToString());
          rec_str = string.Format("{0}{1:D3}", dep_file_nbr, dep_file_seq);
        }
        else if (element_config_info.source == nameof(StandardSource.RcptRefNbr))
        {
          GenericObject geoTran = entry_detail_data.geoTrans_applied.FirstOrDefault();
          if (geoTran != null)
          {
            string dep_file_nbr = geoTran.get("DEPFILENBR", "").ToString();
            int dep_file_seq = Convert.ToInt32(geoTran.get("DEPFILESEQ", "").ToString());
            int event_nbr = Convert.ToInt32(geoTran.get("EVENTNBR", "").ToString());
            rec_str = string.Format("{0}{1:D3}-{2}", dep_file_nbr, dep_file_seq, event_nbr);
          }
          else
            rec_str = "";
        }
        else if (element_config_info.source == nameof(StandardSource.TransRefNbr))
        {
          GenericObject geoTran = entry_detail_data.geoTrans_applied.FirstOrDefault();
          if (geoTran != null)
          {
            string dep_file_nbr = geoTran.get("DEPFILENBR", "").ToString();
            int dep_file_seq = Convert.ToInt32(geoTran.get("DEPFILESEQ", "").ToString());
            int event_nbr = Convert.ToInt32(geoTran.get("EVENTNBR", "").ToString());
            int tran_nbr = Convert.ToInt32(geoTran.get("TRANNBR", "").ToString());
            rec_str = string.Format("{0}{1:D3}-{2}-{3}", dep_file_nbr, dep_file_seq, event_nbr, tran_nbr);
          }
          else
            rec_str = "";
        }
      }
      else if (element_config_info.source_from == nameof(SourceFrom.Tender))
      {
        if (element_config_info.source == nameof(StandardSource.PayFileNbr))
        {
          string dep_file_nbr = entry_detail_data.geoFile.get("DEPFILENBR", "").ToString();
          int dep_file_seq = Convert.ToInt32(entry_detail_data.geoFile.get("DEPFILESEQ", "").ToString());
          rec_str = string.Format("{0}{1:D3}", dep_file_nbr, dep_file_seq);
        }
        else if (element_config_info.source == nameof(StandardSource.RcptRefNbr))
        {
          string dep_file_nbr = entry_detail_data.geoTndr.get("DEPFILENBR", "").ToString();
          int dep_file_seq = Convert.ToInt32(entry_detail_data.geoTndr.get("DEPFILESEQ", "").ToString());
          int event_nbr = Convert.ToInt32(entry_detail_data.geoTndr.get("EVENTNBR", "").ToString());
          rec_str = string.Format("{0}{1:D3}-{2}", dep_file_nbr, dep_file_seq, event_nbr);

        }
        else if (element_config_info.source == nameof(StandardSource.TransRefNbr))
        {
          string dep_file_nbr = entry_detail_data.geoTndr.get("DEPFILENBR", "").ToString();
          int dep_file_seq = Convert.ToInt32(entry_detail_data.geoTndr.get("DEPFILESEQ", "").ToString());
          int event_nbr = Convert.ToInt32(entry_detail_data.geoTndr.get("EVENTNBR", "").ToString());
          int tran_nbr = Convert.ToInt32(entry_detail_data.geoTndr.get("TNDRNBR", "").ToString());
          rec_str = string.Format("{0}{1:D3}-{2}-{3}", dep_file_nbr, dep_file_seq, event_nbr, tran_nbr);
        }
        else if (element_config_info.source == nameof(StandardSource.CheckNumber))
        {
          rec_str = entry_detail_data.geoTndr.get("CC_CK_NBR", "").ToString();
          return rec_str;
        }
        else if (element_config_info.source == nameof(StandardSource.Check_P_Name))
        {
          rec_str = entry_detail_data.geoTndr.get("CCNAME", "").ToString();
          return rec_str;
        }
        else if (element_config_info.source == nameof(StandardSource.Check_P_Address))
        {
          rec_str = entry_detail_data.geoTndr.get("ADDRESS", "").ToString();
          return rec_str;
        }
      }

      return rec_str;
    }
    public static string GetStandardTypeDataForDetailCreditRecord(this ACHBatchElementInfo element_config_info, ACHEntryDetailTenderData entry_detail_data)
    {
      string rec_str = "";

      // no sourece from needed
      //if (element_config_info.source_from == nameof(SourceFrom.Transaction))
      {
        if (element_config_info.source == nameof(CreditStandardSource.PayfileNbr))
        {
          string dep_file_nbr = entry_detail_data.geoFile.get("DEPFILENBR", "").ToString();
          int dep_file_seq = Convert.ToInt32(entry_detail_data.geoFile.get("DEPFILESEQ", "").ToString());
          rec_str = string.Format("{0}{1:D3}", dep_file_nbr, dep_file_seq);
        }
      }

      return rec_str;
    }
    public static string GetBatchElementValue(this List<ACHBatchElementInfo> lst_element_config_info, ACHEntryDetailTenderData entry_detail_data)
    {
      string str_return = "";
      foreach (ACHBatchElementInfo element_config_info in lst_element_config_info)
      {
        str_return += element_config_info.GetBatchElementValue(entry_detail_data);
      }

      return str_return;
    }

    public static string GetBatchElementValue(this ACHBatchElementInfo element_config_info, CoreFileInfo core_file_info)
    {
      string rec_str = "";
      if (element_config_info.record_type == RecordType.BatchRecord)
      {
        if (element_config_info.source_type == nameof(BatchSourceType.Standard))
        {
          //rec_str = element_config_info.GetStandardTypeData(entry_detail_data);
          if (element_config_info.source == nameof(BatchStandardSource.PayfileNbr))
            rec_str = core_file_info.core_file_name;
          else if (element_config_info.source == nameof(BatchStandardSource.DepositSlipNumber))
            rec_str = core_file_info.deposit_slip_number;
        }
        else if (element_config_info.source_type == nameof(BatchSourceType.FixedValue))
        {
          rec_str = element_config_info.fixed_value;
        }
      }

      return rec_str;
    }

    public static string GetBatchElementValue(this List<ACHBatchElementInfo> lst_element_config_info, CoreFileInfo core_file_info)
    {
      string str_return = "";
      foreach (ACHBatchElementInfo element_config_info in lst_element_config_info)
      {
        str_return += element_config_info.GetBatchElementValue(core_file_info);
      }

      return str_return;
    }
    public static string FormatFixedLengthValue(this string str, int len, bool left_justify = true, bool space_fill = true)
    {
      string rec_str = "";
      if (space_fill)
      {
        if (left_justify)
        {
          rec_str = str.PadRight(len);
        }
        else
        {
          rec_str = str.PadLeft(len);
        }

        rec_str = rec_str.Substring(0, len);
      }
      else // zero fill
      {
        string fmt = "";
        for (int i = 0; i < len; i++)
          fmt += "0";
        fmt += ".##";

        decimal decValue = 0.00m;
        bool rst = decimal.TryParse(str, out decValue);
        if (rst)
        {
          rec_str = decValue.ToString(fmt);
        }
      }
      return rec_str;
    }

    // payment related information - 80 characters of free form text to include in the addenda record
    // Must contain NACHA endorsed ANSI ASC X12 data segments or NACHA endorsed banking conventions.The asterisk(“*”) must be the
    // delimiter between the data elements, and the back slash(“\”) must be the terminator between the data segments. The backslash(“\”) or tilde
    // (“~”) may be used when originating ACK, ATX, CCD, CIE, ENR, IAT, PPD and WEB entries.
    public static List<string> GetAddendaPaymentInfo(this List<ACHBatchElementInfo> lst_element_config_info, ACHEntryDetailTenderData entry_detail_data)
    {
      List<string> lst_str = new List<string>();
      string str = "";
      foreach (var element in lst_element_config_info)
      {
        string temp_value = element.GetBatchElementValue(entry_detail_data);
        if (str.Length + temp_value.Length < 79)
        {
          str += temp_value;
          str += "*";
        }
        else
        {
          // remove last "*"
          str = str.TrimEnd('*');
          str += "\\";
          lst_str.Add(str);

          // reset str
          str = "";
        }
      }

      if (!string.IsNullOrEmpty(str))
      {
        // remove last "*"
        str = str.TrimEnd('*');
        str += "\\";
        lst_str.Add(str);

        // reset str
        str = "";
      }

      return lst_str;
    }
  }
}
