﻿using CASL_engine;
using ftp;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranSuiteServices;

namespace CustomizedACHBatchUpdate
{
  public class ACHBatchUpdateInfo
  {
    // consistant
    public bool use_global_file_id_modifier = true;       // developer can change here based on project request

    // SI File and Ftp Info
    public string strTempBatchFile { get; set; }           // Temp File Name
    public string strFTPServer { get; set; }
    public string strFTPPort { get; set; }
    public string strFTPUserName { get; set; }
    public string strFTPPWD { get; set; }                  // decrypted
    public string strFTPSeverPath { get; set; }
    public string strFTPServerFileName { get; set; }       // FILE NAME on FTP server 
    public string strLocalFolderPath { get; set; }
    public string strLocalFileName { get; set; }           // FILE NAME on local server, used to download file from FTP server first
    public string strFileTransferMethod { get; set; }
    public string strFileExt { get; set; }
    public string strFileDelimiter { get; set; }
    public string strBatchSFTPKeyFile { get; set; }           // Bug 24724
    public string strBatchSFTPKeyPassphrase { get; set; }     // Bug 24724
    public string strEmail_to { get; set; }
    public DateTime dtFileCreateTime { get; set; }
    public string strFileIDModifier { get; set; }
    public string strLetterCase { get; set; }
    //public string strFileNameFormat { get; set; }
    public OutputConnection pOutputConnection = null; // To do be careful using this for the sql connection because method close connection closes all object of output connection ie file that will be ftp'd and sql connection
    public bool config_info_initialed = false;

    public Dictionary<CoreFileInfo, List<ACHEntryDetailTenderData>> dic_ach_entry_tender_data { get; set; }
    public List<ACHEntryDetailTenderData> lst_current_batch_ach_entry_tender_data { get; set; }
    public ACHBatchUpdateConfigData bu_config_data { get; set; }

    public OutputConnection pOutpotConnection = null;
    public bool bAltDirSep;

    public int connectionTimeout = 60000;
    public bool logVerbose = false;
    public string rebexLogPath = "";

    public ACHBatchUpdateInfo()
    {
      lst_current_batch_ach_entry_tender_data = new List<ACHEntryDetailTenderData>();
      dic_ach_entry_tender_data = new Dictionary<CoreFileInfo, List<ACHEntryDetailTenderData>>();
      dtFileCreateTime = DateTime.Now;
    }


    /// <summary>
    /// Bug 21902 DJD: Configurable File Name for ACH Batch Update System Interface
    /// Generate a custom configured or standard ACH File name:
    /// "ACH" +
    /// Creation Date (MMddyy) +
    /// Createion Time (HHmm) +
    /// Increment ID modifier(A B C ... for files created in the same day)
    /// "ACH1206191706H"
    /// </summary>
    /// <returns></returns>
    public string GetACHFileName(string strFileNameFormat)
    {
      string strACHFileName = FormatFileNameWithDateTime(strFileNameFormat);
      if (string.IsNullOrEmpty(strACHFileName))
      {
        strACHFileName = "ACH" +
        dtFileCreateTime.ToString("MMddyyhhmm") +
        strFileIDModifier;
      }
      return strACHFileName;
    }

    /// <summary>
    /// Bug 21902 DJD: Configurable File Name for ACH Batch Update System Interface
    /// This method returns the custom file name with formatted:
    ///   Date/time portion in straight brackets with file creation date/time. e.g., For example, "[MMddyyHHmm]"
    ///   File ID Modifier constant in curly brackets. e.g., "{FILE_ID}"
    /// Example: "ACH[MMddyyHHmm]{FILE_ID}"
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    public string FormatFileNameWithDateTime(string fileName)
    {
      // Format date time in file name
      if (fileName.Contains("[") && fileName.Contains("]"))
      {
        int idxStart = fileName.IndexOf("[");
        int idxFinish = fileName.IndexOf("]");
        if (idxStart < idxFinish)
        {
          string dateFormat = fileName.Substring(idxStart, (idxFinish - idxStart + 1));
          string datePortion = dateFormat.Length > 2 ? dtFileCreateTime.ToString(dateFormat.Substring(1, dateFormat.Length - 2)) : "";
          fileName = fileName.Replace(dateFormat, datePortion);
        }
      }

      // Format File ID Modifier in file name
      const string sFILE_ID = "{FILE_ID}";
      fileName = fileName.Replace(sFILE_ID, strFileIDModifier);

      return fileName;
    }

    public bool GetConfigureInfo(GenericObject pConfigSystemInterface, ref string strError)
    {
      Logger.cs_log("Start GetConfigureInfo");

      if (config_info_initialed == false)
      {
        try
        {
          strFTPServer = pConfigSystemInterface.get("ftp_server", "").ToString();
          strFTPPort = pConfigSystemInterface.get("ftp_port", "").ToString();
          strFTPUserName = pConfigSystemInterface.get("ftp_username", "").ToString();
          strFTPSeverPath = pConfigSystemInterface.get("ftp_path", "").ToString();
          strFTPServerFileName = pConfigSystemInterface.get("ftp_server_file_name", "").ToString();
          strLocalFolderPath = pConfigSystemInterface.get("folder_path", "").ToString();
          strLocalFileName = pConfigSystemInterface.get("folder_local_file_name", "").ToString();
          strFileTransferMethod = ((string)pConfigSystemInterface.get("ftp_method", "sftp")).ToLower();
          strFileExt = pConfigSystemInterface.has("file_name_extension") ? pConfigSystemInterface.get("file_name_extension", "csv").ToString() : "csv";
          strBatchSFTPKeyFile = pConfigSystemInterface.get("ftp_private_key_file", "").ToString(); // Bug 24724 [16445] File Transmission Option Changing To S-FTP with ID and Key
          strBatchSFTPKeyPassphrase = pConfigSystemInterface.get("ftp_private_key_passphrase", "").ToString(); // Bug 24724 [16445] File Transmission Option Changing To S-FTP with ID and Key
          strEmail_to = (string)pConfigSystemInterface.get("Notification_Email_To", ""); // Bug 16549 DJD: Error If NO ACH BU SI Email Configured [Changed ToString() to cast] 
          strLetterCase = (string)pConfigSystemInterface.get("Letter_Case", "");
          strFileDelimiter = ","; // default

          // Bug 25577
          connectionTimeout = Convert.ToInt32(pConfigSystemInterface.get("ftp_connection_timeout", "60000"));   // Bug 23202
          logVerbose = (bool)pConfigSystemInterface.get("rebex_verbose_log", false);                            // Bug 24836 
          rebexLogPath = pConfigSystemInterface.get("rebex_log_path", "").ToString();                           // Bug 24836 

          // local temp file
          string strTempBatchPath = pConfigSystemInterface.get("local_temp_path", "") as string;

          if (string.IsNullOrEmpty(strTempBatchPath) || !Directory.Exists(strTempBatchPath))
          {
            if (!Directory.Exists(strTempBatchPath))
            {
              string msg = string.Format("{0} is not exists. temp file should be created under {1}.", strTempBatchPath, Path.GetTempPath());
              Logger.cs_log(msg);
            }
            strTempBatchPath = Path.GetTempPath();
          }


          // get File ID
          strFileIDModifier = GetFileIdModifier(pConfigSystemInterface);

          // get ACH File Name Format
          string strFileNameFormat = pConfigSystemInterface.get("custom_file_name", "") as string;

          string strFileName = GetACHFileName(strFileNameFormat); // string.Format("CustomizedBatchUpdate{0}.txt", (DateTime.Now.ToString("MMddyyhhmm")));
          strTempBatchFile = Path.Combine(strTempBatchPath, strFileName);

          // remote file name
          if (string.IsNullOrEmpty(strFTPServerFileName))
          {
            strFTPServerFileName = string.Format("{0}.{1}", strFileName, strFileExt);
          }
          else
          {
            string str_file_name = string.Format("{0}{1}", strFTPServerFileName, (DateTime.Now.ToString("MMddyyhhmm")));
            strFTPServerFileName = string.Format("{0}.{1}", str_file_name, strFileExt);
          }

          // local file name
          if (string.IsNullOrEmpty(strLocalFileName))
          {
            strLocalFileName = string.Format("{0}.{1}", strFileName, strFileExt);
          }
          else
          {
            string str_file_name = string.Format("{0}{1}", strLocalFileName, (DateTime.Now.ToString("MMddyyhhmm")));
            strLocalFileName = string.Format("{0}.{1}", str_file_name, strFileExt);
          }

          //FTP Password and  Decrypt FTP Password
          strFTPPWD = pConfigSystemInterface.get("ftp_password", "").ToString();
          if (!string.IsNullOrEmpty(strFTPPWD))
          {
            string secret_key = Crypto.get_secret_key();
            byte[] content_bytes = Convert.FromBase64String(strFTPPWD);
            byte[] decrypted_bytes = Crypto.symmetric_decrypt("data", content_bytes, "secret_key", secret_key);
            strFTPPWD = System.Text.Encoding.Default.GetString(decrypted_bytes);
          }

          // BEGIN Bug 24724 [16445] FT: Transmission Option Changing To S-FTP with ID and Key
          // Decrypt S-FTP Key Passphrase
          if (!string.IsNullOrEmpty(strBatchSFTPKeyPassphrase))
          {
            string secret_key = Crypto.get_secret_key();
            byte[] content_bytes = Convert.FromBase64String(strBatchSFTPKeyPassphrase);
            byte[] decrypted_bytes = Crypto.symmetric_decrypt("data", content_bytes, "secret_key", secret_key);
            strBatchSFTPKeyPassphrase = System.Text.Encoding.Default.GetString(decrypted_bytes);
          }
          // END Bug 24724 [16445] FT: Transmission Option Changing To S-FTP with ID and Key


          bAltDirSep = (bool)pConfigSystemInterface.get("alt_directory_separator", false);


          // config_data
          string strCustomizedBUType_Id = pConfigSystemInterface.get("ach_customized_bu_type", "").ToString();
          if (!string.IsNullOrEmpty(strCustomizedBUType_Id))
          {
            bu_config_data = GetBatchUpdateConfigInfoData(strCustomizedBUType_Id);
            if (bu_config_data == null)
            {
              strError = "GetConfigureInfo Failed: bu_config_data is null, see your administrator";
              Logger.cs_log(strError);
              return false;
            }
          }
          else
          {
            strError = "GetConfigureInfo Failed: no custimized batch update type configured";
            Logger.cs_log(strError);
            return false;
          }

        }
        catch (Exception ex)
        {
          strError = "GetConfigureInfo Failed: " + ex.Message;
          Logger.cs_log(strError);
          return false;
        }

        config_info_initialed = true;
      }

      // here I need update global file id modifer, because it could be multipe Customized ACH types update at same time, we need update file 
      // id modifer for each type
      // there is a problem in case ach update failed, how to deal(?)
      if(use_global_file_id_modifier)
        Update_Global_File_ID_Modifier();

      Logger.cs_log("End GetConfigureInfo");
      return true;
    }
    public string GetFileIdModifier(GenericObject pConfigSystemInterface)
    {
      string strFileIDModifier = "";
      // Bug 21902 DJD: Saving "yyMMdd{File ID Modifier}" now (not the file name)

      string fileID_Value = "";
      if (!use_global_file_id_modifier)
      {
        var varfileIdValue = pConfigSystemInterface.get("Customized_ACH_File_ID_Modifier", "");
        fileID_Value = varfileIdValue != null ? varfileIdValue.ToString() : "";
      }
      else
      {
        // Bug 27212: Get the global File ID Modifier, not the one stored in the system interfaces
        var varFieldIdValue = (c_CASL.c_object("Business.Misc.data") as GenericObject).get("Customized_ACH_Global_File_ID_Modifier", "");
        fileID_Value = varFieldIdValue != null ? varFieldIdValue.ToString() : "";
      }

      if (fileID_Value.Length == 7)
      {
        string strDate = fileID_Value.Substring(0, 6);
        if (string.Format("{0:yyMMdd}", dtFileCreateTime) == strDate)
        {
          char cIDModifier = (char)((fileID_Value[fileID_Value.Length - 1] + 1 - 65) % 26 + 65);
          strFileIDModifier = cIDModifier.ToString();
        }
        else
        {
          strFileIDModifier = "A";
        }
      }
      else
      {
        strFileIDModifier = "A";
      }

      return strFileIDModifier;
    }
    /// <param name="pBatchUpdate_ProductLevel"></param>
    public void Update_Global_File_ID_Modifier()
    {
      // Generate string value using the file creation date and current file id: yyMMdd{File ID}
      string fileID_Value = string.Format("{0:yyMMdd}{1}", dtFileCreateTime, strFileIDModifier);

      // Update value in CASL(memory)
      // Bug: 27212: Set the global File_ID_Modifier.  Don't bother with the one in the system interface
      misc.CASL_call("subject", c_CASL.c_object("Business.Misc.data"), "a_method", "set", "args", new GenericObject("Customized_ACH_Global_File_ID_Modifier", fileID_Value));

      // Update CBT Table
      string strError = "";

      // Bug 15800 MJO - Use Config DB
      GenericObject database = c_CASL.c_object("TranSuite_DB.Config.db_connection_info") as GenericObject;
      ConnectionTypeSynch pDBConn = null;

      try
      {
        pDBConn = new ConnectionTypeSynch(database);
        string err = null;
        if (!pDBConn.EstablishDatabaseConnection(ref err) || err != null)
        {
          throw CASL_error.create("message", "Error opening database connection.");
        }
        string strSQL = string.Format("UPDATE CBT_FLEX SET [field_value] = '\"{0}\"' WHERE [container] = 'Business.Misc.data' and [field_key] = '\"Customized_ACH_Global_File_ID_Modifier\"'", fileID_Value);
        if (!pDBConn.ExecuteSQL_NO_RollBack(strSQL, ref strError))
        {
          throw CASL_error.create("message", "Unable to update File_ID_modifier");
        }
      }
      finally // Clean up
      {
        if (pDBConn != null)
        {
          pDBConn.CloseDBConnection();
          pDBConn = null;
        }
      }
    }

    public void Update_File_ID_Modifier(string strSysInt, BatchUpdate_ProductLevel pBatchUpdate_ProductLevel)
    {
      // Generate string value using the file creation date and current file id: yyMMdd{File ID}
      string fileID_Value = string.Format("{0:yyMMdd}{1}", dtFileCreateTime, strFileIDModifier);

      // Update value in CASL(memory)
      misc.CASL_call("subject", c_CASL.c_object("System_interface.of." + strSysInt), "a_method", "set", "args", new GenericObject("Customized_ACH_File_ID_Modifier", fileID_Value));

      // Update CBT Table
      string strError = "";

      // Bug 15800 MJO - Use Config DB
      GenericObject database = c_CASL.c_object("TranSuite_DB.Config.db_connection_info") as GenericObject;
      ConnectionTypeSynch pDBConn = null;

      try
      {
        pDBConn = new ConnectionTypeSynch(database);
        string err = null;
        if (!pDBConn.EstablishDatabaseConnection(ref err) || err != null)
        {
          throw CASL_error.create("message", "Error opening database connection.");
        }
        string strSQL = string.Format("UPDATE CBT_FLEX SET [field_value] = '\"{0}\"' WHERE [container] = 'Business.System_interface.of.{1}' and [field_key] = '\"Customized_ACH_File_ID_Modifier\"'", fileID_Value, strSysInt);
        if (!pDBConn.ExecuteSQL_NO_RollBack(strSQL, ref strError))
        {
          throw CASL_error.create("message", "Unable to update Customized_ACH_File_ID_Modifier");
        }
      }
      finally // Clean up
      {
        if (pDBConn != null)
        {
          pDBConn.CloseDBConnection();
          pDBConn = null;
        }
      }
    }
    public ACHBatchUpdateConfigData GetBatchUpdateConfigInfoData(string customized_bu_type_id)
    {
      Logger.cs_log("Start GetBatchUpdateConfigInfoData");

      string config_data_str = "";
      ACHBatchUpdateConfigData config_data = new ACHBatchUpdateConfigData();

      string strError = "";
      GenericObject database = c_CASL.c_object("TranSuite_DB.Configurablebu.db_connection_info") as GenericObject;
      ConnectionTypeSynch dbConnType = null;
      try
      {
        dbConnType = new ConnectionTypeSynch(database);
        GenericObject dbConnInfo = dbConnType.GetDBConnectionInfo();
        string configDBConnectStr = (string)dbConnInfo.get("db_connection_string", "");

        using (SqlConnection conn = new SqlConnection(configDBConnectStr))
        {
          conn.Open();
          using (SqlCommand command = new SqlCommand())
          {
            command.Connection = conn;
            command.CommandText = string.Format("SELECT config_data FROM batch_update_config WHERE id = '{0}'", customized_bu_type_id);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
              if (reader.Read())
              {
                config_data_str = reader["config_data"].ToString();

              }
            }
            reader.Close();
            config_data = JsonConvert.DeserializeObject<ACHBatchUpdateConfigData>(config_data_str);
          }
        }

        Logger.cs_log("End GetBatchUpdateConfigInfoData");
        return config_data;
      }
      catch (Exception ex)
      {
        strError = "GetConfigureInfo Failed: " + ex.Message;
        Logger.cs_log(strError);
        Logger.cs_log("End GetBatchUpdateConfigInfoData with Error");
        return config_data;
      }

    }

    public bool AddEntryDetail(GenericObject geoTender, List<GenericObject> geoTrans_applied, GenericObject geoFile, GenericObject geoWorkGroup)
    {
      ACHEntryDetailTenderData ach_entry_tender_data = new ACHEntryDetailTenderData();
      ach_entry_tender_data.geoWorkgourp = geoWorkGroup ?? new GenericObject();
      ach_entry_tender_data.geoFile = geoFile ?? new GenericObject();
      ach_entry_tender_data.geoTndr = geoTender ?? new GenericObject();
      ach_entry_tender_data.geoTrans_applied = geoTrans_applied ?? new List<GenericObject>();

      bool reversal = Decimal.Parse(geoTender.get("AMOUNT", "0.0").ToString()) < 0;
      ach_entry_tender_data.entry_detail_type = reversal ? EntryDetailType.Credit : EntryDetailType.Debit;
      lst_current_batch_ach_entry_tender_data.Add(ach_entry_tender_data);

      return true;
    }

    public bool AddCurrentBatchInfoToDic(CoreFileInfo core_file_info)
    {
      if (dic_ach_entry_tender_data.ContainsKey(core_file_info) == false)
        dic_ach_entry_tender_data.Add(core_file_info, lst_current_batch_ach_entry_tender_data);
      else
        dic_ach_entry_tender_data[core_file_info] = lst_current_batch_ach_entry_tender_data; // should not happened

      return true;
    }
    public static GenericObject GetWorkgroupFromCoreFile(GenericObject coreFile)
    {
      string strDeptID = coreFile.get("DEPTID", "").ToString();
      GenericObject workGroup = ((GenericObject)c_CASL.c_object("Department.of")).get(strDeptID) as GenericObject;
      return workGroup;
    }

    public void CleanupCurrentBatch()
    {
      lst_current_batch_ach_entry_tender_data = new List<ACHEntryDetailTenderData>();
    }

    public decimal GetCurretBatchTotal()
    {
      decimal total_amount = 0;

      foreach (var entry_detail in lst_current_batch_ach_entry_tender_data)
      {
        string strAmount = entry_detail.geoTndr.get("AMOUNT", "0.0").ToString();
        total_amount += Decimal.Parse(strAmount);
      }
      return total_amount;
    }

    string Convert_Case(string strtext, string lettercase)
    {
      switch (lettercase.ToLower())
      {
        case "normal": return strtext;
        case "upper case": return strtext.ToUpper();
        case "lower case": return strtext.ToLower();
        default: return strtext;
      }
    }
    public ProcessResult BuildACHBatchUpdate()
    {
      ACHBatchUpdateProcessor ach_processor = new ACHBatchUpdateProcessor(strFileIDModifier, dtFileCreateTime, bu_config_data, dic_ach_entry_tender_data);
      ProcessResult rst = ach_processor.Process();
      rst.ach_batch = Convert_Case(rst.ach_batch, strLetterCase);

      return rst;
    }
  }


  class CustomizedACHFileBatchUpdate : AbstractBatchUpdate
  {
    public ACHBatchUpdateInfo m_ach_batch_update_info = new ACHBatchUpdateInfo();
    public bool bTerminateBatchUpdate = false;

    #region override methods
    public override AbstractBatchUpdate GetInstance(GenericObject sysInt)
    {
      Logger.cs_log("ACH Customized Batch Update - Stated");
      return base.GetInstance(sysInt);
    }

    /// <summary>
    /// CustomizedACHBatchUpdate is the system_interface type ID 
    /// </summary>
    /// <returns></returns>
    public override string GetCASLTypeName()
    {
      return "CustomizedACHBatchUpdate";
    }

    public override bool Project_Cleanup(bool bEarlyTerminationRequestedByProject, object pProductLevelMainClass)
    {
      CleanUp();
      return true;
    }

    public override bool Project_CoreFile_BEGIN(string strSystemInterface, GenericObject pConfigSystemInterface, object pProductLevelMainClass, GenericObject pCurrentFile, string strCoreFile, ref eReturnAction ReturnAction)
    {
      string strError = string.Empty;
      try
      {
        CASL_engine.BatchUpdate_ProductLevel pBathUpdate_ProductLevel = (BatchUpdate_ProductLevel)pProductLevelMainClass;

        if (!m_ach_batch_update_info.GetConfigureInfo(pConfigSystemInterface, ref strError))
          throw new Exception(strError);

        // reset 
        m_ach_batch_update_info.CleanupCurrentBatch();
      }
      catch (Exception ex)
      {
        Logger.cs_log("Project_CoreFile_BEGIN Failed: " + ex.Message.ToString());
        bTerminateBatchUpdate = true;
        return false;
      }
      return true;

    }
    public override bool Project_ProcessCoreEvent(string strSystemInterface, GenericObject pEventGEO, GenericObject pConfigSystemInterface, GenericObject pFileGEO, object pProductLevelMainClass, string strCurrentFile, ref eReturnAction ReturnAction)
    {
      Logger.cs_log("Start CustomizedACHFileBatchUpdate.Project_ProcessCoreEvent");
      string strError = string.Empty;
      CASL_engine.BatchUpdate_ProductLevel pBatchUpdate_ProductLevel = (BatchUpdate_ProductLevel)pProductLevelMainClass;

      try
      {
        // get File
        pFileGEO = pFileGEO.get(0, null) as GenericObject;
        if (pFileGEO == null)
        {
          strError = "ERROR: CustomizedACHFileBatchUpdate.Project_ProcessCoreEvent:Unable to retrieve CORE File";
          misc.CASL_call("a_method", "log", "args", new GenericObject("ERROR", strError));
          pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError(strCurrentFile, strSystemInterface, strError);
          return false;
        }

        // get the workgroup for the core file
        GenericObject geoWorkGroup = ACHBatchUpdateInfo.GetWorkgroupFromCoreFile(pFileGEO);

        // get transaction
        GenericObject geoTrans = pEventGEO.get("TRANSACTIONS", new GenericObject()) as GenericObject;
        int iCntOfTransactions = geoTrans.getIntKeyLength();

        // get tenders
        GenericObject geoTenders = pEventGEO.get("TENDERS", new GenericObject()) as GenericObject;
        int iCountofTenders = geoTenders.getIntKeyLength();
        for (int i = 0; i < iCountofTenders; i++)
        {
          GenericObject geoTender = (GenericObject)geoTenders.get(i);
          string tender_Id = geoTender.get("TNDRID", "").ToString();
          string tender_desc = geoTender.get("TNDRDESC", "").ToString();
          string tender_nbr = geoTender.get("TNDRNBR", "").ToString();

          // I also need get assocated transactions applied to this tender
          List<GenericObject> geoTrans_applied = new List<GenericObject>();
          for (int k = 0; k < iCntOfTransactions; k++)
          {
            GenericObject geoTran = (GenericObject)geoTrans.get(k);

            GenericObject geo_tta_tenders = geoTran.get("TTA_TENDERS", null) as GenericObject;

            if (geo_tta_tenders != null)
            {
              int cnt_tta = geo_tta_tenders.getIntKeyLength();
              for (int j = 0; j < cnt_tta; j++)
              {
                GenericObject tta_tender = (GenericObject)geo_tta_tenders.get(j);
                string tta_tender_Id = tta_tender.get("TNDRID", "").ToString();
                string tta_tender_desc = tta_tender.get("TNDRDESC", "").ToString();
                string tta_tender_nbr = tta_tender.get("TNDRNBR", "").ToString();

                if (string.Compare(tta_tender_Id, tender_Id, true) == 0 && string.Compare(tta_tender_desc, tender_desc, true) == 0 &&
                   string.Compare(tta_tender_nbr, tender_nbr, true) == 0)
                {
                  geoTrans_applied.Add(geoTran);
                }
              }
            }
          }

          // add entry detail
          m_ach_batch_update_info.AddEntryDetail(geoTender, geoTrans_applied, pFileGEO, geoWorkGroup);

        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log("Error in ACHFile Project_ProcessCoreEvent: " + e.ToMessageAndCompleteStacktrace());

        strError = String.Format("Project_ProcessCoreEvent:{0} {1} {2}", strSystemInterface, strCurrentFile, e.Message).Replace("'", "");
        Logger.LogError(strError, "ACH Batch Update");
        pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError(strCurrentFile, strSystemInterface, strError);
        //BUG 13226 QG
        bTerminateBatchUpdate = true;
        string strSubject = "ACH File Batch Update Error.";
        string strBody = String.Format("Error in ACH File Batch Update, {0}", strError).Replace("'", "");
        Send_Notification_Email(m_ach_batch_update_info.strEmail_to, strSubject, strBody);
        //End of Bug 13226
      }
      return true;
    }

    public override bool Project_ProcessDeposits(GenericObject pFileDeposits, GenericObject pConfigSystemInterface, object pProductLevelMainClass, string strSysInterface, GenericObject pCurrentFile, ref eReturnAction ReturnAction)
    {
      string strError = "";
      string strFileName = pFileDeposits.get("FULL_FILE_NAME").ToString();
      CASL_engine.BatchUpdate_ProductLevel pBatchUpdate_ProductLevel = (BatchUpdate_ProductLevel)pProductLevelMainClass;

      CoreFileInfo core_file_info = new CoreFileInfo();
      core_file_info.core_file_name = strFileName;
      core_file_info.deposit_slip_number = "";
      try
      {
        // Bug 23008 [23026] - ACH Failure Caused By Bank Rec Code Update [Added "GeteCheckDepositForACH" method and several null & count checks]
        // Bug 18217: Put deposit slip number in the CompanyDiscretionaryData field
        if (pFileDeposits != null && pFileDeposits.vectors.Count > 0)
        {
          GenericObject depositTableFields = GeteCheckDepositForACH(pFileDeposits, pConfigSystemInterface);
          if (depositTableFields != null)
          {
            string slipNumber = depositTableFields.get("DEPSLIPNBR", "").ToString();
            if (!string.IsNullOrWhiteSpace(slipNumber))
            {
              core_file_info.deposit_slip_number = slipNumber;
            }
          }
        }

        m_ach_batch_update_info.AddCurrentBatchInfoToDic(core_file_info);
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log("Error in ACHFile Project_ProcessDeposits: " + e.ToMessageAndCompleteStacktrace());

        // Bug 23008 [23026] - ACH Failure Caused By Bank Rec Code Update [Fixed and added more detailed logging]
        string currentCOREFile = (pCurrentFile.vectors.Count > 0) ? ((GenericObject)(pCurrentFile.vectors[0])).get("FILENAME", "").ToString().Trim() : "";
        string strErrorDetail = String.Format("Project_ProcessDeposits: {0} {1}{2}{3}", strSysInterface, currentCOREFile, Environment.NewLine, e.ToMessageAndCompleteStacktrace()).Replace("'", "");
        Logger.LogError(strErrorDetail, "ACH Batch Update ERROR");
        strError = e.Message.Replace("'", "");
        pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError(strFileName, strSysInterface, strError);
        bTerminateBatchUpdate = true;  //Bug 13226
        string strEmail_to = (string)pConfigSystemInterface.get("Notification_Email_To", ""); // Bug 16549 DJD: Error If NO ACH BU SI Email Configured [Changed ToString() to cast] 
        string strSubject = "ACH File Batch Update Error.";
        string strBody = String.Format("Error in ACH File Batch Update, {0}", strError).Replace("'", "");
        Send_Notification_Email(strEmail_to, strSubject, strBody);
        return false;
      }

      return true;
    }

    public override bool Project_WriteTrailer(object pProductLevelMainClass, string strSysInterface, ref bool bEarlyTerminationRequestedByProject)
    {
      Logger.cs_log("Start Project_WriteTrailer");
      string strError = "";
      string strSubject = "";
      string strBody = "";

      CASL_engine.BatchUpdate_ProductLevel pBatchUpdate_ProductLevel = (BatchUpdate_ProductLevel)pProductLevelMainClass;
      if (bTerminateBatchUpdate)
        return false;

      if (bEarlyTerminationRequestedByProject)
      {
        strError = string.Format("Early Termination - ACHCustomizedBatchUpdate.Project_WriteTrailer Termination.");
        Logger.cs_log(string.Format("ACHCustomizedBatchUpdate.Project_WriteTrailer - {0}", strError));
        misc.CASL_call("a_method", "log", "args", new GenericObject("ERROR ACHCustomizedBatchUpdate.Project_WriteTrailer", strError));
        pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError("SYS_INTERFACE_SCOPE_ONLY", strSysInterface, "ACHCustomizedBatchUpdate.Project_WriteTrailer" + strError);
        CleanUp();
        return false;
      }

      try
      {
        if (!ProcessACHBatchUpdate(strSysInterface, pBatchUpdate_ProductLevel, ref strError))
          throw new Exception(strError);
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log("Error in ACHFile Project_WriteTrailer: " + e.ToMessageAndCompleteStacktrace());

        //Logger.LogError(strError, "START ACH Project_WriteTrailer");
        // Log and send failure email
        Logger.LogError(e.Message, "ACH File Batch Update");
        strSubject = "ACH File Batch Update Error.";
        strBody = String.Format("Error in ACH File Batch Update, {0}", e.Message).Replace("'", "");
        Send_Notification_Email(m_ach_batch_update_info.strEmail_to, strSubject, strBody);
        pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError("SYS_INTERFACE_SCOPE_ONLY", strSysInterface, strBody);
        return false;
      }
      return true;
    }

    public override void Project_Rollback(object pProductLevelMainClass, bool bEarlyTerminationRequestedByProject)
    {
      // Add code here if you want to do something on failure (delete the file?)
    }

    #endregion

    /// <summary>
    /// Bug 23008 [23026] - Cleveland Clinic Production Issue: ACH Failure Caused By Bank Rec Code Update
    /// This method loops through deposits and finds the one for eCheck (ACH).
    /// </summary>
    /// <param name="pFileDeposits"></param>
    /// <param name="pConfigSystemInterface"></param>
    /// <returns>eCheck Deposit or null if not found (or an error)</returns>
    protected GenericObject GeteCheckDepositForACH(GenericObject pFileDeposits, GenericObject pConfigSystemInterface)
    {
      GenericObject depositFields = null;
      try
      {
        GenericObject depositFields_TndrType = null;
        // Make certain that the deposit is for the eCheck tender
        foreach (object objDeposit in pFileDeposits.vectors)
        {
          GenericObject geoDeposit = objDeposit as GenericObject;
          if (geoDeposit.has("DEPOSIT_TENDERS"))
          {
            GenericObject depositTenders = geoDeposit["DEPOSIT_TENDERS"] as GenericObject;
            if (depositTenders != null & depositTenders.vectors.Count > 0)
            {
              foreach (object objDepTndr in depositTenders.vectors)
              {
                // There are 2 ways to deposit tender:
                //   1) As a "Tender Deposit" [This uses a SPECIFIC tender like eCheck] or
                //   2) As a "Tender Type"    [This groups all tenders of the same type or category (like "Check" or "Cash")]
                //   An eCheck tender SHOULD be deposited using a "Tender Deposit" but if it's the
                //   only tender of type "Check" then it may be deposited as a "Tender Type".

                // Break out of loop if the eCheck deposit was found
                if (depositFields != null)
                {
                  break;
                }

                GenericObject depositTender = objDepTndr as GenericObject;
                if (depositTender != null)
                {
                  string TndrProp = (string)depositTender.get("TNDRPROP", "");
                  decimal decimalAmt = System.Convert.ToDecimal(depositTender["AMOUNT"]);
                  switch (TndrProp)
                  {
                    case "TndrID":
                      {
                        string tndrID = (string)depositTender["TNDR"];
                        GenericObject geoTenderClass = (GenericObject)((GenericObject)c_CASL.c_object("Tender.of")).get(tndrID, null);
                        if (decimalAmt != 0.00M && IsValidACHTender(geoTenderClass, pConfigSystemInterface))
                        {
                          depositFields = geoDeposit;
                        }
                      }
                      break;
                    case "TypeInd":
                      {
                        // NOTE: eCheck Deposit amount for the CORE file should match the Batch Total amount. (HX: Check Later)
                        if ((string)depositTender["TNDRDESC"] == "Check" && decimalAmt == m_ach_batch_update_info.GetCurretBatchTotal())
                        {
                          depositFields_TndrType = geoDeposit; // This deposit is MOST LIKELY for the eCheck tender
                        }
                      }
                      break;
                    default:
                      break;
                  }
                }
              }
            }
          }
        }
        if (depositFields == null && depositFields_TndrType != null)
        {
          depositFields = depositFields_TndrType;
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log("Error in ACHFile GeteCheckDepositForACH: " + e.ToMessageAndCompleteStacktrace());

        // Log the error but don't fail the ACH System Interface for an error here.
        string currentCOREFile = pFileDeposits.get("FULL_FILE_NAME").ToString();
        string strErrorDetail = String.Format("GeteCheckDepositForACH: {0} {1}{2}{3}", "ACH Batch Update", currentCOREFile, Environment.NewLine, e.ToMessageAndCompleteStacktrace()).Replace("'", "");
        Logger.LogError(strErrorDetail, "ACH Batch Update ERROR");
      }
      return depositFields;
    }

    public bool IsValidACHTender(GenericObject geoTenderClass, GenericObject SysInt)
    {
      // Tender is valid only if it is a check and has ACH SI assigned to it.
      GenericObject geoSystemInterfaces = geoTenderClass.get("system_interfaces") as GenericObject;
      GenericObject geoParent = geoTenderClass.get("_parent") as GenericObject;
      if (geoSystemInterfaces.has(SysInt) && ((geoParent.get("type", "").ToString()) == "Check"))
        return true;
      else
        return false;
    }
    protected bool ProcessACHBatchUpdate(string strSysInterface, BatchUpdate_ProductLevel pBatchUpdate_ProductLevel, ref string strError)
    {
      Logger.cs_log("Start ProcessACHBatchUpdate");

      // delete local temp file if it is existed
      if (File.Exists(m_ach_batch_update_info.strTempBatchFile))
        File.Delete(m_ach_batch_update_info.strTempBatchFile);

      // Build ACH Batch Update
      ProcessResult rst = m_ach_batch_update_info.BuildACHBatchUpdate();

      // write cotent to temp file
      if (!WriteFileContentToTempFile(rst.ach_batch, ref strError))
      {
        Logger.cs_log("End ProcessACHBatchUpdate failed: WriteFileContentToTempFile had problems. ");
        return false;
      }

      // FTP or Copy to local path
      string strTransferMethod = m_ach_batch_update_info.strFileTransferMethod.ToLower();
      if (strTransferMethod == "ftp" || strTransferMethod == "sftp")
      {
        if (!ftpBatchFile(pBatchUpdate_ProductLevel, ref strError))
        {
          Logger.cs_log("End ProcessBatchUpdate failed with transfer method 'ftp' ");
          return false;
        }

        string strSubject = "FINISH ACH File Processing: FILE successfully created and transferred";
        string strBody = String.Format("ACH FILE successfully transfered. <br/>Total Amount: {0}", rst.total_debit_amount_in_file);
        Send_Notification_Email(m_ach_batch_update_info.strEmail_to, strSubject, strBody);

        // update file id modifier if not use global file modifier
        if(!m_ach_batch_update_info.use_global_file_id_modifier)
          m_ach_batch_update_info.Update_File_ID_Modifier(strSysInterface, pBatchUpdate_ProductLevel); // Bug 21902 DJD: Configurable File Name for ACH Batch Update System Interface
      }
      else if (strTransferMethod == "folder")
      {
        if (!flatBatchFile(pBatchUpdate_ProductLevel, ref strError))
        {
          Logger.cs_log("End ProcessBatchUpdate failed with transfer method 'folder' ");
          return false;
        }
      }

      Logger.cs_log("End ProcessACHBatchUpdate");
      return true;
    }

    /// <summary>
    /// Do the ftp/sftp part of the batch update and send the file to the configured (S)FTP server.
    /// </summary>
    /// <param name="pBatchUpdate_ProductLevel"></param>
    /// <param name="strSysInterface"></param>
    /// <param name="strLocalTempFile"></param>
    /// <param name="fileName"></param>
    /// <returns></returns>
    protected bool ftpBatchFile(BatchUpdate_ProductLevel pBatchUpdate_ProductLevel, ref string strError)
    {
      Logger.cs_log("Start ftpBatchFile");
      string strBatchFTPAddress = m_ach_batch_update_info.strFTPServer;
      if (string.IsNullOrEmpty(strBatchFTPAddress))
      {
        misc.CASL_call("a_method", "log", "args", new GenericObject("Cutomized ACH File Update Warning", "No FTP Server configured: not sending file"));
        strError = "FTP Failed - No FTP Server Configured.";
        return false;
      }

      string strFileTransferMethod = m_ach_batch_update_info.strFileTransferMethod.ToLower();

      // TTS 14634
      bool bAltDirSep = m_ach_batch_update_info.bAltDirSep;
      string strServerPathFileName = "";
      if (bAltDirSep)
      {
        strServerPathFileName = m_ach_batch_update_info.strFTPSeverPath + Path.AltDirectorySeparatorChar + m_ach_batch_update_info.strFTPServerFileName;
      }
      else
        strServerPathFileName = Path.Combine(m_ach_batch_update_info.strFTPSeverPath, m_ach_batch_update_info.strFTPServerFileName);
      // End 14634

      // Transfer File
      FTP ftp = null;
      SFTP sftp = null;
      const bool pwdIsEncrypted = false;  // Force the FTP classes to decrypt


      try
      {
        bool transferSuccessful = false;

        if (strFileTransferMethod == "ftp")
        {
          ftp = new FTP(m_ach_batch_update_info.strFTPServer, m_ach_batch_update_info.strFTPUserName, m_ach_batch_update_info.strFTPPWD, m_ach_batch_update_info.strFTPPort, true, pwdIsEncrypted, false, false, false);

          //ftp.Connect();
          ftp.Connect(m_ach_batch_update_info.connectionTimeout, m_ach_batch_update_info.logVerbose, m_ach_batch_update_info.rebexLogPath);
          ftp.SendFile(m_ach_batch_update_info.strTempBatchFile, strServerPathFileName);
          if (ftp.FileExists(strServerPathFileName))
            transferSuccessful = true;

        }
        else if (strFileTransferMethod == "sftp")
        {
          // Bug 24724 [16445] FT: File Transmission Option Changing To S-FTP with ID and Key
          // Only use this if the keyfile path is set
          if (string.IsNullOrWhiteSpace(m_ach_batch_update_info.strBatchSFTPKeyFile))
          {
            // The Key file path is not set, so use the traditional username and password
            sftp = new SFTP(m_ach_batch_update_info.strFTPServer, m_ach_batch_update_info.strFTPUserName, m_ach_batch_update_info.strFTPPWD, m_ach_batch_update_info.strFTPPort, true, pwdIsEncrypted, "", "", "");
          }
          else
          {
            // The Key file path is set, use the username and key method
            sftp = new SFTP(m_ach_batch_update_info.strFTPServer, m_ach_batch_update_info.strFTPUserName, m_ach_batch_update_info.strFTPPWD, m_ach_batch_update_info.strFTPPort, true, pwdIsEncrypted, m_ach_batch_update_info.strBatchSFTPKeyFile, "", m_ach_batch_update_info.strBatchSFTPKeyPassphrase);
          }
          // End Bug 24724

          //sftp.Connect();
          sftp.Connect(m_ach_batch_update_info.connectionTimeout, m_ach_batch_update_info.logVerbose, m_ach_batch_update_info.rebexLogPath);
          sftp.SendFile(m_ach_batch_update_info.strTempBatchFile, strServerPathFileName);
          if (sftp.FileExists(strServerPathFileName))
            transferSuccessful = true;
        }

        if (transferSuccessful)
        {
          //Succeed to upload file to FTP, log
          string msg = string.Format("ACH File Transfer Process of file {0} Succeeded.", m_ach_batch_update_info.strFTPServerFileName);
          misc.CASL_call("a_method", "log", "args", new GenericObject("ACTION", msg));
          return true;
        }
        else
        {
          strError = string.Format("Cannot Write to ACH File '{0}' to '{1}' on the FTP server", m_ach_batch_update_info.strTempBatchFile, strServerPathFileName);
          misc.CASL_call("a_method", "log", "args", new GenericObject("ERROR in Customized ACH File Transfer ", strError));
          string errorMsg = "Fail to transfer Lawson batch File: " + strError;
          // Bug 16431
          pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError("SYS_INTERFACE_SCOPE_ONLY", "ACH File Transfer", errorMsg);

          return false;
        }
      }
      catch (Exception ex)
      {
        strError = string.Format("ftpBatchFile failed.  Cannot FTP batch update file. " + ex.Message);
        misc.CASL_call("a_method", "log", "args", new GenericObject("ERROR in Customized ACH File Transfer ", strError));
        pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError("SYS_INTERFACE_SCOPE_ONLY", "ACH File Transfer", strError);

        if (ftp != null) ftp.Disconnect();
        if (sftp != null) sftp.Disconnect();
        return false;
      }
      finally
      {
        // Destructor will call Disconnect, but with .NET it's async scheduled. So calling
        // disconnect here will free the connection faster
        if (ftp != null) ftp.Disconnect();
        if (sftp != null) sftp.Disconnect();

        Logger.cs_log("End ftpBatchFile");
      }

    }

    /// <summary>
    /// flat batch file send file to local server path - "folder"
    /// </summary>
    /// <param name="pBatchUpdate_ProductLevel"></param>
    /// <param name="strSysInterface"></param>
    /// <param name="strLocalTempFile"></param>
    /// <param name="fileName"></param>
    /// <returns></returns>
    protected bool flatBatchFile(CASL_engine.BatchUpdate_ProductLevel pBatchUpdate_ProductLevel, ref string strError)
    {
      Logger.cs_log("Start flatBatchFile");
      if (string.IsNullOrEmpty(m_ach_batch_update_info.strLocalFolderPath))
      {
        misc.CASL_call("a_method", "log", "args", new GenericObject("Customized ACH File Warning", "No local folder path configured: not sending file"));
        strError = "Batch File Failed - No file path configured.";
        return false;
      }

      string strLocalPathFileName = Path.Combine(m_ach_batch_update_info.strLocalFolderPath, m_ach_batch_update_info.strLocalFileName);

      // coping File
      try
      {
        string fileContent = File.ReadAllText(m_ach_batch_update_info.strTempBatchFile);
        StreamWriter sw;

        if (File.Exists(strLocalPathFileName))
        {
          misc.CASL_call("a_method", "log", "args", new GenericObject("Cusotmized ACH File Warning", "there is existing batch update file: not sending file"));
          strError = "Batch File failed - exiting batch file found.";
          return false;
        }
        else
        {
          sw = new StreamWriter(strLocalPathFileName, true);
        }

        sw.Write(fileContent);
        sw.Flush();
        sw.Close();

        //Succeed to copying file, log
        string msg = string.Format("Customized ACH File Copying Process of file {0} Succeeded.", m_ach_batch_update_info.strLocalFileName);
        misc.CASL_call("a_method", "log", "args", new GenericObject("ACTION", msg));

      }
      catch (Exception e)
      {
        strError = string.Format("Cannot Write to Customized ACH File '{0}' to '{1}': {2}", m_ach_batch_update_info.strTempBatchFile, m_ach_batch_update_info.strLocalFileName, e.Message.ToString());
        misc.CASL_call("a_method", "log", "args", new GenericObject("ERROR in Customized ACH File Update", strError));
        string errorMsg = "Fail to copying batch File: " + strError;
        pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError("SYS_INTERFACE_SCOPE_ONLY", "Customized ACH File Update", errorMsg);

        return false;
      }

      return true;
    }

    protected bool WriteFileContentToTempFile(string strFileContent, ref string strError)
    {
      try
      {
        if (m_ach_batch_update_info.pOutputConnection == null)
          m_ach_batch_update_info.pOutputConnection = new OutputConnection();

        if (m_ach_batch_update_info.pOutputConnection.m_pOutputFile == null)
        {
          // this prevents appending to file if it exists from previous run or was placed in the folder
          m_ach_batch_update_info.pOutputConnection.m_pOutputFile = new StreamWriter(m_ach_batch_update_info.strTempBatchFile);
        }
        else
        {
          if (File.Exists(m_ach_batch_update_info.strTempBatchFile))
            m_ach_batch_update_info.pOutputConnection.m_pOutputFile = File.AppendText(m_ach_batch_update_info.strTempBatchFile);
        }

        m_ach_batch_update_info.pOutputConnection.m_pOutputFile.Write(strFileContent);
        m_ach_batch_update_info.pOutputConnection.m_pOutputFile.Flush();
      }
      catch (Exception ex)
      {
        Logger.cs_log(ex.ToMessageAndCompleteStacktrace());
        m_ach_batch_update_info.pOutputConnection.m_pOutputFile.Close();
        strError = string.Format("Cannot Write to File({0}):{1}", m_ach_batch_update_info.strTempBatchFile, ex.Message);
        misc.CASL_call("a_method", "log", "args", new GenericObject("ERROR in m_batch_update_info.WriteFileContentToTempFile", strError));
        return false;
      }
      finally
      {
        // Bug 16433 UMN handle close in finally clause
        if (m_ach_batch_update_info.pOutputConnection.m_pOutputFile != null)
          m_ach_batch_update_info.pOutputConnection.m_pOutputFile.Close();
      }

      return true;
    }

    protected void CleanUp()
    {
      // delete temp file
      try
      {
        // delete temp file
        if (File.Exists(m_ach_batch_update_info.strTempBatchFile))
          File.Delete(m_ach_batch_update_info.strTempBatchFile);

        // set m_ach_batch_update_info to null
        m_ach_batch_update_info = null;
      }
      catch (Exception ex)
      {
        //should not effect the process next run should overwrite this existing file
        //just write it to the log
        Logger.cs_log(ex.ToMessageAndCompleteStacktrace());
        misc.CASL_call("a_method", "log", "args", new GenericObject("ERROR could not delete the local temp batch file", ex.Message));
      }
      return;
    }

    /// <summary>
    /// Send Notification Email. Use Email server and Email from which are defined in config.casl
    /// </summary>
    /// <param name="strEmail_to"></param>
    /// <param name="strSubject"></param>
    /// <param name="strBody"></param>
    public void Send_Notification_Email(string strEmail_to, string strSubject, string strBody)
    {
      try
      {
        string strEmail_from = c_CASL.GEO.get("ipayment_sender_email", "") as string;
        string strEmail_server = c_CASL.GEO.get("smtp_host", "") as string; // Bug 22627 UMN
        if (!String.IsNullOrEmpty(strEmail_from) && !String.IsNullOrEmpty(strEmail_to) && !String.IsNullOrEmpty(strEmail_server))
        {
          emails.CASL_email(
          "to", strEmail_to,
          "from", strEmail_from,
          "host_smtp_server", strEmail_server,
          "subject", strSubject,
          "body", strBody
          );
        }
        else
        {
          Logger.LogError("Failed to send email: Email address can not be empty.", "EMAIL SEND");
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log("Error in ACHFile Send_Notification_Email: " + e.ToMessageAndCompleteStacktrace());

        Logger.LogError(String.Format("Failed to send email: {0}", e.Message), "EMAIL SEND");
      }
    }


  }
}
