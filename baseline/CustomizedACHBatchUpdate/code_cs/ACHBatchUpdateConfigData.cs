﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace CustomizedACHBatchUpdate
{

  public enum StandardEntryClass { ARC = 1, BOC, CCD, CTX, IAT, POP, PPD, RCK, TEL, WEB }
  public enum ServiceClassCode { MixedDebitCredit = 200, CreditOnly = 220, DebitOnly = 225 }
  public enum BatchGroupType { Workgroup = 1, TransType, Tender }
  public enum AddendaType { CCD = 1, CTX, WEB, IAT }
  public enum RecordType {FileRcord, BatchRecord, DetailDebitRecord, DetailCreditRecord}
  public enum SourceFrom { Transaction, Tender }
  // for detail debit record
  public enum SourceType { Standard, CustomField, FixedValue }
  public enum StandardSource {PayFileNbr, RcptRefNbr, TransRefNbr, CheckNumber, Check_P_Name, Check_P_Address}
  // for detail credit record
  public enum CreditSourceType { Standard, FixedValue}
  public enum CreditStandardSource { PayfileNbr}
  // for batch record
  public enum BatchSourceType { Standard, FixedValue }
  public enum BatchStandardSource { PayfileNbr, DepositSlipNumber }

  // Transaction Type
  // Standard - Checking/Saving:  CheckingCredit = 22,  CheckingDebit = 27,  SavingCredit = 32,SavingDebit = 37,
  // Prenote:  PrenoteOfCheckingCredit = 23, PrenoteOfCheckingDebit = 28,PrenoteOfSavingCredit= 33, PrenoteOfSavingDebit = 38,
  // ZeroDollarWithRD: ZeroDollarCheckingCreditWithRemittanceData = 24, ZeroDollarCheckingDebitWithRemittanceData = 29,ZeroDollarSavingCreditWithRemittanceData = 34,ZeroDollarSavingDebitWithRemittanceData = 39
  public enum TransactionCodeType { Standard, Prenote,ZeroDollarWithRD}
  public enum TransactionCode { 
    CheckingCredit = 22, 
    PrenoteOfCheckingCredit = 23, 
    ZeroDollarCheckingCreditWithRemittanceData = 24, 
    CheckingDebit = 27, 
    PrenoteOfCheckingDebit = 28,
    ZeroDollarCheckingDebitWithRemittanceData = 29,
    SavingCredit = 32,
    PrenoteOfSavingCredit= 33,
    ZeroDollarSavingCreditWithRemittanceData = 34,
    SavingDebit = 37,
    PrenoteOfSavingDebit = 38,
    ZeroDollarSavingDebitWithRemittanceData = 39
  }
  public enum BalanceAccountType { Checking, Saving}

  public class Item
  {
    public string value { get; set; }
    public string text { get; set; }
  }

  public class ACHBatchUpdateConfigData
  {
    public ACHGeneralConfigInfo ach_general_info { get; set; }
    public ACHFileStructure ach_file_structure { get; set; }
  }


  public class ACHGeneralConfigInfo
  {
    public string id { get; set; }
    public Dictionary<string, string> dic_info { get; set; }    // use dictionary instead, the reason is struct is keep same if we add additonal members
    public List<ACHBatchHeaderInfo> lst_batch_header_info { get; set; }

  }

  public class ACHBatchHeaderInfo
  {
    public string id { get; set; }
    public Dictionary<string, string> dic_info { get; set; }            // use dictionary instead, the reason is struct is keep same if we add additonal members; 
    public Dictionary<string, List<Item>> dic_lst_info { get; set; }    // for selected workgroup list, trasansaction list, it is not used
  }

  public class IndividualNameInfo
  {
    public List<ACHBatchElementInfo> lst_individual_name_debit { get; set; }  // list for combined values
    public List<ACHBatchElementInfo> lst_individual_name_credit { get; set; }
    
  }

  public class IndividualIdInfo
  {
    public List<ACHBatchElementInfo> lst_individual_id_debit { get; set; }
    public List<ACHBatchElementInfo> lst_individual_id_credit { get; set; }
  }

  public class CompanyDiscretionaryData
  {
    public List<ACHBatchElementInfo> lst_info { get; set; }
  }

  public class AddendaPymtRelatedInfo
  {
    public List<ACHBatchElementInfo> lst_info_debit { get; set; }   // for detail
    public List<ACHBatchElementInfo> lst_info_credit { get; set; }  // for balance
  }

  // for special element configuration
  public class ACHBatchElementInfo
  {
    public int seq_order { get; set; }
    public string id { get; set; }
    public string source_from { get; set; }
    public string source_type { get; set; }
    public string source { get; set; }
    public string fixed_value { get; set; }
    //public bool is_detail_debit { get; set; }
    public RecordType record_type { get; set; }
  }

  
  public static class MyConstants
  {
    public const string BATCH_TYPE = "ACH";

    // general info
    public const string general_description = "general_description";                // it is bu_config description
    public const string immediate_destination = "immediate_destination";
    public const string immediate_origin = "immediate_origin";
    public const string immediate_destination_name = "immediate_destination_name";
    public const string company_name = "company_name";
    public const string company_id = "company_id";
    public const string originating_dfi_id = "originating_dfi_id";
    public const string receiving_dfi_rt_number = "receiving_dfi_rt_number";
    public const string rt_number_check_digit = "rt_number_check_digit";
    public const string receiving_dfi_account_number = "receiving_dfi_account_number";
    //public const string odfi = "odfi";
    public const string odfi_dicretionary_data = "odfi_dicretionary_data";
    public const string file_internal_reference_code = "file_internal_reference_code";


    // batch header info
    public const string batch_description = "batch_description";
    public const string standard_entry_class = "standard_entry_class";
    public const string company_name_short = "company_name_short";
    public const string service_class_code = "service_class_code";
    public const string transaction_code = "transaction_code";
    public const string transaction_code_type = "transaction_code_type";
    public const string balance_account_type = "balance_account_type";
    public const string company_discretionary_data = "company_discretionary_data";
    public const string company_entry_description = "company_entry_description";
    public const string originator_status_code = "originator_status_code";
    public const string batch_group_by = "batch_group_by";
    public const string include_addenda_record = "include_addenda_record";
    public const string addenda_type = "addenda_type";
    public const string include_iat_717_record = "include_iat_717_record";

    // others for code generated fields
    public const string record_type_code = "record_type_code";
    // file record
    public const string file_creation_date = "file_creation_date";
    public const string file_creation_time = "file_creation_time";
    public const string file_id_modifier = "file_id_modifier";


    // batch header info
    public const string company_descriptive_date = "company_descriptive_date";
    public const string effective_entry_date = "effective_entry_date";
    public const string settlement_julian_date = "settlement_julian_date";
    public const string batch_number = "batch_number";

    // entry detail
    // public const string transaction_code = "transaction_code";
    public const string trans_amount = "trans_amount";
    public const string check_serial_number = "check_serial_number";
    public const string individual_name = "individual_name";
    public const string trace_number = "trace_number";
    public const string addenda_record_indicator = "addenda_record_indicator";
    public const string individual_id_number = "individual_id_number";              // but not ssn
    public const string number_of_addenda_records = "number_of_addenda_records";
    public const string terminal_city = "terminal_city";
    public const string terminal_state = "terminal_state";

    // addenda record
    public const string addenda_type_code = "addenda_type_code";
    public const string addenda_payment_related_info = "addenda_payment_related_info";
    public const string addenda_entry_detail_seq_nbr = "addenda_entry_detail_seq_nbr";
    public const string addenda_seq_nbr = "addenda_seq_nbr";
    public const string entry_detail_seq_nbr = "entry_detail_seq_nbr";

    // batch control record
    public const string entry_addenda_count_in_batch = "entry_addenda_count_in_batch";
    public const string entry_hash_in_batch = "entry_hash_in_batch";
    public const string total_debit_amount_in_batch = "total_debit_amount_in_batch";
    public const string total_credit_amount_in_batch = "total_credit_amount_in_batch";
    public const string message_auth_code = "message_auth_code";

    // file control record
    public const string batch_count = "batch_count";
    public const string block_count = "block_count";
    public const string entry_addenda_count_in_file = "entry_addenda_count_in_file";
    public const string entry_hash_in_file = "entry_hash_in_file";
    public const string total_debit_amount_in_file = "total_debit_amount_in_file";
    public const string total_credit_amount_in_file = "total_credit_amount_in_file";

  }

  public static class MyExtensionClass
  {
    public static string Get(this Dictionary<string, string> dic, string key)
    {
      string rst_str = "";
      if (dic.ContainsKey(key))
        rst_str = dic[key];

      return rst_str;
    }

    public static string Get(this ACHGeneralConfigInfo ach_general_info, string key)
    {
      string rst_str = "";
      if (ach_general_info.dic_info != null)
        rst_str = ach_general_info.dic_info.Get(key);
      return rst_str;
    }

    public static void SetDicInfo(this ACHGeneralConfigInfo ach_general_info, string key, string value)
    {
      ach_general_info = ach_general_info ?? new ACHGeneralConfigInfo();
      ach_general_info.dic_info = ach_general_info.dic_info ?? new Dictionary<string, string>();

      if (ach_general_info.dic_info.ContainsKey(key) == false)
        ach_general_info.dic_info.Add(key, value);
      else
        ach_general_info.dic_info[key] = value;
    }

    public static void SetDicInfo(this ACHBatchHeaderInfo info, string key, string value)
    {
      info = info ?? new ACHBatchHeaderInfo();
      info.dic_info = info.dic_info ?? new Dictionary<string, string>();

      if (info.dic_info.ContainsKey(key) == false)
        info.dic_info.Add(key, value);
      else
        info.dic_info[key] = value;
    }

    public static string Get(this ACHBatchHeaderInfo ach_batch_header, string key)
    {
      string rst_str = "";
      if (ach_batch_header.dic_info != null)
        rst_str = ach_batch_header.dic_info.Get(key);

      rst_str = rst_str ?? "";
      return rst_str;
    }

    public static void SetDicListInfo(this ACHBatchHeaderInfo info, string key, List<Item> lst_item)
    {
      info = info ?? new ACHBatchHeaderInfo();
      info.dic_lst_info = info.dic_lst_info ?? new Dictionary<string, List<Item>>();
      if (lst_item.Count > 0)
      {
        if (info.dic_lst_info.ContainsKey(key) == false)
        {
          info.dic_lst_info.Add(key, lst_item);
        }
        else
          info.dic_lst_info[key] = lst_item;
      }
    }

    public static List<Item> GetDicListInfo(this ACHBatchHeaderInfo ach_batch_header, string key)
    {
      List<Item> lst = new List<Item>();
      if (ach_batch_header != null && ach_batch_header.dic_lst_info != null)
      {
        if (ach_batch_header.dic_lst_info.ContainsKey(key))
          lst = ach_batch_header.dic_lst_info[key];
      }

      return lst;
    }

    public static bool IsNumeric(this string str)
    {
      long number = 0;
      bool canConvert = long.TryParse(str, out number);
      return canConvert;
    }

    public static string FormatValue(this string str, ACHRecordFormat my_format = null)
    {
      str = str ?? "";
      string rec_str = "";

      if (my_format != null)
      {
        if (my_format.filled == Filled.Space)
        {
          if (my_format.justified == Justified.Left)
          {
            rec_str = str.PadRight(my_format.length);
          }
          else
          {
            rec_str = str.PadLeft(my_format.length);
          }

          rec_str = rec_str.Substring(0, my_format.length);
        }
        else // zero fill
        {
          if (string.IsNullOrEmpty(str))
            str = "0";
          string fmt = "";
          for (int i = 0; i < my_format.length; i++)
            fmt += "0";
          fmt += ".##";

          decimal decValue = 0.00m;
          bool rst = decimal.TryParse(str, out decValue);
          if (rst)
          {
            rec_str = decValue.ToString(fmt);
          }
        }
      }
      else
        rec_str = str;
      return rec_str;
    }

    public static bool IsItemValueContained(this List<Item> items, string value)
    {
      int idx = items.FindIndex(o => o.value == value);

      return idx == -1 ? false : true;
    }

    public static T ConvertToObject<T>(this string json_str)
    {
      T my_object;
      
      try
      {
        my_object = JsonConvert.DeserializeObject<T>(json_str);
      }
      catch(Exception)
      {
        my_object = default(T);
      }

      return my_object;
    }

  }


  // ====== format .... classes ======
  public enum FieldProperty { M, R, O }
  public enum ValueProperty { Fixed, Configured, CodeGenerated }
  public enum ValueDataType { Alphanumeric, Alphabetic, Numberic, Date,IndividualIdInfo, IndividualNameInfo, AddendaPymtInfo, CompanyDiscretionaryData }
  public enum Justified { Left, Right }
  public enum Filled { Space, Zero }

  public class ACHRecord
  {
    //public ACHType ach_type { get; set; }
    public List<ACHRecordItem> lstItem { get; set; }
  }

  public class ACHRecordItem
  {
    public int field_seq { get; set; }
    public string name { get; set; }
    public string value { get; set; }
    public int len { get; set; }
    public FieldProperty field_property { get; set; }
    public ValueProperty value_property { get; set; }
    public ValueDataType value_data_type { get; set; }            
    public ACHRecordFormat format { get; set; }
    public List<String> select_options { get; set; }      // may not used
    


  }
  public class ACHRecordFormat
  {
    public Justified justified { get; set; }
    public Filled filled { get; set; }
    public string format_string { get; set; }
    public int length { get; set; }
  }
  public class ACHFileStructure
  {
    public string file_header_record_format { get; set; }
    public Dictionary<string, ACHBatchInfoStructure> dic_batch_info { get; set; } // key is  ACHBatchHeaderInfo's id
    public string file_control_record_format { get; set; }
  }

  public class ACHBatchInfoStructure
  {
    public string std_entry_class { get; set; }
    public string batch_header_record_format { get; set; }
    public string entry_detail_record_format { get; set; }
    public Dictionary<string, string> dic_addenda_record_format { get; set; } // key is addenda_type  there are multi addenda types for IAT
    public string batch_control_record_format { get; set; }
  }
}
