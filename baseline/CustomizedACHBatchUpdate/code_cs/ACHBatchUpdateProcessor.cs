﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CASL_engine;

namespace CustomizedACHBatchUpdate
{
  public class ACHBatchUpdateProcessor
  {
    private string _file_id_modifier;
    private DateTime _dtFileCreateTime;
    private ACHBatchUpdateConfigData _config_data;
    private Dictionary<CoreFileInfo, List<ACHEntryDetailTenderData>> _dic_entry_tender_data;
    public ACHBatchUpdateProcessor(string strFileIdModifier, DateTime dtFileCreateTime, ACHBatchUpdateConfigData bu_config_data, Dictionary<CoreFileInfo, List<ACHEntryDetailTenderData>> dic_ach_entry_tender_data)
    {
      _file_id_modifier = strFileIdModifier;
      _dtFileCreateTime = dtFileCreateTime;
      _config_data = bu_config_data;
      _dic_entry_tender_data = dic_ach_entry_tender_data;
      //_lst_entry_tender_data = lst_ach_entry_tender_data;
    }

    public ProcessResult Process()
    {
      ProcessResult rst = new ProcessResult();

      StringBuilder sb = new StringBuilder();
      int batch_count = 0;
      int total_entry_cnt_in_file = 0;
      int total_addenda_cnt_in_file = 0;
      decimal total_debit_amount_in_file = 0;
      decimal total_credit_amount_in_file = 0;
      long entry_hash_in_file = 0;



      // file header
      string file_header = ProcessFileHeader();
      sb.AppendLine(file_header);

      // now should only one type of batch included
      if (_config_data.ach_file_structure.dic_batch_info != null && _config_data.ach_file_structure.dic_batch_info.Count > 0)
      {
        var pair_batch_info = _config_data.ach_file_structure.dic_batch_info.OrderBy(o => o.Key).First();
        // I also need get this batch configuration
        ACHBatchHeaderInfo bhi = _config_data.ach_general_info.lst_batch_header_info.FirstOrDefault(o => o.id == pair_batch_info.Key);

        //foreach (var pair in _config_data.ach_file_structure.dic_batch_info) 
        {
          // batch Header
          string str_batch_header_config = pair_batch_info.Value.batch_header_record_format;
          ACHRecord batch_header_ach_record_config = str_batch_header_config.ConvertToObject<ACHRecord>();
          // entry detail
          string str_entry_detail_config = pair_batch_info.Value.entry_detail_record_format;
          ACHRecord entry_detail_config = str_entry_detail_config.ConvertToObject<ACHRecord>();
          // batch control
          string str_batch_control_config = pair_batch_info.Value.batch_control_record_format;
          ACHRecord batch_control_ach_record_config = str_batch_control_config.ConvertToObject<ACHRecord>();
          // check if need to include addenda
          bool include_addenda = bhi.Get(MyConstants.include_addenda_record) == "true" ? true : false;


          // service class code
          string str_service_class_code = bhi.Get(MyConstants.service_class_code);
          ServiceClassCode service_class_code = (ServiceClassCode)Enum.Parse(typeof(ServiceClassCode), str_service_class_code);

          // transaction code type
          string transaction_code_type = bhi.Get(MyConstants.transaction_code_type);

          // balance account type
          string balance_account_type = bhi.Get(MyConstants.balance_account_type);

          // for each core file - batch
          foreach (var pair_data in _dic_entry_tender_data)
          {
            // note: _dic_entry_tender_data: key - CoreFileInfo , value - List<ACHEntryDetailTenderData>
            // CoreFileInfo includes core_file_name, created_date, and deposit_slip_number 

            // get entries for the batch - remove it b/c we configure transactions and tenders in system interface level
            // List<ACHEntryDetailTenderData> lst_entry_detail = GetEntryDetailForBatch(bhi, pair_data.Value);
            List<ACHEntryDetailTenderData> lst_entry_detail = GetValidEntryDetail(pair_data.Value);
            // total batch amount
            decimal total_debit_amount_in_batch = 0;
            decimal total_credit_amount_in_batch = 0;

            // entry hash in batch
            long entry_hash_in_batch = GetEntryHashInBatch(entry_detail_config, lst_entry_detail.Count);
            entry_hash_in_file += entry_hash_in_batch;

            batch_count++;
            int entry_cnt_in_batch = 0;   // for detail entry 
            int addenda_cnt_in_batch = 0; // for addenda
            if (pair_batch_info.Value.std_entry_class != nameof(StandardEntryClass.IAT))
            {
              if (lst_entry_detail != null && lst_entry_detail.Count > 0)
              {
                // batch header
                string batch_header = ProcessBatchHeaderForNonIAT(batch_header_ach_record_config, lst_entry_detail, batch_count, pair_data.Key);
                sb.AppendLine(batch_header);

                // for each entry detail ( and addenda if applied)
                foreach (ACHEntryDetailTenderData entry_detail in lst_entry_detail)
                {
                  if ((service_class_code == ServiceClassCode.DebitOnly && entry_detail.entry_detail_type == EntryDetailType.Debit) ||
                       (service_class_code == ServiceClassCode.CreditOnly && entry_detail.entry_detail_type == EntryDetailType.Credit) ||
                       (service_class_code == ServiceClassCode.MixedDebitCredit))
                  {
                    entry_cnt_in_batch++;
                    total_entry_cnt_in_file++;

                    // add debit or credit amount to the batch level
                    if (transaction_code_type == nameof(TransactionCodeType.Standard))
                    {
                      if (entry_detail.entry_detail_type == EntryDetailType.Debit)
                        total_debit_amount_in_batch += GetAbsoluteTnderAmount(entry_detail);
                      else
                        total_credit_amount_in_batch += GetAbsoluteTnderAmount(entry_detail);
                    }

                    // get number of addenda associated with the transaction, if is only used for CTX
                    int nbrOfAddendedRecForTrans = 0;
                    if (include_addenda && pair_batch_info.Value.std_entry_class == nameof(StandardEntryClass.CTX))
                      nbrOfAddendedRecForTrans = GetNbrOfAddendaRecAssociatedWithTrans(pair_batch_info.Value, entry_detail, true);

                    string entry_detail_rec = ProcessEntryDetail(bhi, entry_detail_config, entry_detail, entry_cnt_in_batch, nbrOfAddendedRecForTrans);
                    if (!string.IsNullOrEmpty(entry_detail_rec))
                      sb.AppendLine(entry_detail_rec);

                    // addenda 
                    if (include_addenda)
                    {
                      // only one
                      foreach (var addenda_record_format in pair_batch_info.Value.dic_addenda_record_format)
                      {
                        string str_addenda_config = addenda_record_format.Value;
                        ACHRecord addenda_config = str_addenda_config.ConvertToObject<ACHRecord>();  //JsonConvert.DeserializeObject<ACHRecord>(str_addenda_config);
                        addenda_config = addenda_config ?? new ACHRecord();
                        addenda_config.lstItem = addenda_config.lstItem ?? new List<ACHRecordItem>();
                        List<string> lst_addenda_rec = ProcessAddendaRecord(addenda_config, entry_detail, entry_cnt_in_batch);

                        // set count
                        addenda_cnt_in_batch += lst_addenda_rec.Count;
                        total_addenda_cnt_in_file += lst_addenda_rec.Count;

                        foreach (var addenda_rec in lst_addenda_rec)
                          sb.AppendLine(addenda_rec);
                      }
                    }
                  }

                }

                // balancing
                if (total_debit_amount_in_batch != total_credit_amount_in_batch)
                {
                  bool addCredit = total_debit_amount_in_batch > total_credit_amount_in_batch;
                  decimal entryDetailAmt = Math.Abs(total_debit_amount_in_batch - total_credit_amount_in_batch);

                  if ((service_class_code == ServiceClassCode.CreditOnly && addCredit) ||
                     (service_class_code == ServiceClassCode.DebitOnly && !addCredit) ||
                     (service_class_code == ServiceClassCode.MixedDebitCredit))
                  {
                    entry_cnt_in_batch++;
                    total_entry_cnt_in_file++;

                    int nbrOfAddendedRecForTrans = 0;
                    if (include_addenda && pair_batch_info.Value.std_entry_class == nameof(StandardEntryClass.CTX))
                      nbrOfAddendedRecForTrans = GetNbrOfAddendaRecAssociatedWithTrans(pair_batch_info.Value, lst_entry_detail[0], false);

                    string entry_detail_rec = ProcessEntryDetailToBalanceBatch(bhi, entry_detail_config, lst_entry_detail, entry_cnt_in_batch, addCredit, entryDetailAmt, nbrOfAddendedRecForTrans);
                    sb.AppendLine(entry_detail_rec);

                    if (addCredit)
                      total_credit_amount_in_batch += entryDetailAmt;
                    else
                      total_debit_amount_in_batch += entryDetailAmt;

                    // addenda 
                    if (include_addenda)
                    {
                      // currenlty only one addenda config in dic_addenda_record_format
                      foreach (var addenda_record_format in pair_batch_info.Value.dic_addenda_record_format)
                      {
                        string str_addenda_config = addenda_record_format.Value;
                        ACHRecord addenda_config = str_addenda_config.ConvertToObject<ACHRecord>(); //JsonConvert.DeserializeObject<ACHRecord>(str_addenda_config);
                        List<string> lst_addenda_rec = ProcessAddendaRecordToBalanceBatch(addenda_config, lst_entry_detail[0], entry_cnt_in_batch);

                        // set count
                        addenda_cnt_in_batch += lst_addenda_rec.Count;
                        total_addenda_cnt_in_file += lst_addenda_rec.Count;

                        foreach (var addenda_rec in lst_addenda_rec)
                          sb.AppendLine(addenda_rec);
                      }
                    }
                  }

                }

                // increasing total credit/debit amount in file
                total_debit_amount_in_file += total_debit_amount_in_batch;
                total_credit_amount_in_file += total_credit_amount_in_batch;

                // batch control
                string batch_control = ProcessBatchControl(batch_control_ach_record_config, batch_count, entry_cnt_in_batch + addenda_cnt_in_batch, entry_hash_in_batch, total_debit_amount_in_batch, total_credit_amount_in_batch);
                sb.AppendLine(batch_control);
              }
            }
            else // IAT
            {
              // later
            }
          }
        }
      }


      // process file control
      string file_control = ProcessFileControl(batch_count, total_entry_cnt_in_file + total_addenda_cnt_in_file, total_debit_amount_in_file, total_credit_amount_in_file, entry_hash_in_file);
      sb.AppendLine(file_control);

      rst.ach_batch = sb.ToString();
      rst.total_debit_amount_in_file = total_debit_amount_in_file;
      return rst;
    }

    protected int GetNbrOfAddendaRecAssociatedWithTrans(ACHBatchInfoStructure batch_info, ACHEntryDetailTenderData entry_detail, bool is_detail_record)
    {
      int cnt = 0;

      // currenlty only one addenda config in dic_addenda_record_format
      foreach (var addenda_record_format in batch_info.dic_addenda_record_format)
      {
        string str_addenda_config = addenda_record_format.Value;
        ACHRecord addenda_config = str_addenda_config.ConvertToObject<ACHRecord>();

        List<string> lst_payment_related_info = new List<string>();

        addenda_config = addenda_config ?? new ACHRecord();
        addenda_config.lstItem = addenda_config.lstItem ?? new List<ACHRecordItem>();

        // Get Addenda Payment related Information
        var addenda_payment_info_item = addenda_config.lstItem.FirstOrDefault(o => o.name == MyConstants.addenda_payment_related_info);
        addenda_payment_info_item = addenda_payment_info_item ?? new ACHRecordItem();
        AddendaPymtRelatedInfo addenda_pymt_info = addenda_payment_info_item.value.ConvertToObject<AddendaPymtRelatedInfo>();
        if (addenda_pymt_info != null)
        {
          if (is_detail_record)
            lst_payment_related_info = addenda_pymt_info.lst_info_debit.GetAddendaPaymentInfo(entry_detail);
          else
            lst_payment_related_info = addenda_pymt_info.lst_info_credit.GetAddendaPaymentInfo(entry_detail);
        }

        cnt += lst_payment_related_info.Count;
      }
      return cnt;
    }

    protected List<ACHEntryDetailTenderData> GetEntryDetailForBatch(ACHBatchHeaderInfo bhi, List<ACHEntryDetailTenderData> lst_entry_tender_data)
    {
      List<ACHEntryDetailTenderData> lst = lst_entry_tender_data ?? new List<ACHEntryDetailTenderData>();

      // get entry details for selected workgroups, tenders and transaction applied to tender
      // 1 workgroups
      List<ACHEntryDetailTenderData> selected_entry_detail = new List<ACHEntryDetailTenderData>();
      List<Item> selected_wgs = bhi.GetDicListInfo(nameof(BatchGroupType.Workgroup));
      if (selected_wgs != null && selected_wgs.Count > 0)
      {
        foreach (Item selected_wg in selected_wgs)
        {
          foreach (ACHEntryDetailTenderData entry_detail in lst)
          {
            string wg_id = entry_detail.geoWorkgourp.get("id", "").ToString(); // use tndrid
            if (wg_id == selected_wg.value)
            {
              selected_entry_detail.Add(entry_detail);
            }
          }
        }
      }
      else // if it is null and count = 0, treated as all wg selected
        selected_entry_detail = lst;

      lst = selected_entry_detail;

      // 2 tenders
      selected_entry_detail = new List<ACHEntryDetailTenderData>();
      List<Item> selected_tndrs = bhi.GetDicListInfo(nameof(BatchGroupType.Tender));
      if (selected_tndrs != null && selected_tndrs.Count > 0)
      {
        foreach (Item selected_tndr in selected_tndrs)
        {
          foreach (ACHEntryDetailTenderData entry_detail in lst)
          {
            string tndr_id = entry_detail.geoTndr.get("TNDRID", "").ToString();
            if (tndr_id == selected_tndr.value)
            {
              selected_entry_detail.Add(entry_detail);
            }
          }
        }
      }
      else // if it is null and count = 0, treated as all 
        selected_entry_detail = lst;

      // transactions applied.
      selected_entry_detail = new List<ACHEntryDetailTenderData>();
      List<Item> selected_tts = bhi.GetDicListInfo(nameof(BatchGroupType.TransType));
      if (selected_tts != null && selected_tts.Count > 0)
      {
        foreach (Item selected_tt in selected_tts)
        {
          foreach (ACHEntryDetailTenderData entry_detail in lst)
          {
            foreach (GenericObject geoTT in entry_detail.geoTrans_applied)
            {
              string tt_id = geoTT.get("TTID", "").ToString();
              if (tt_id == selected_tt.value)
              {
                selected_entry_detail.Add(entry_detail);
              }
            }
          }
        }
      }
      else
        selected_entry_detail = lst;

      lst = selected_entry_detail;

      // double check tender type is "check"
      selected_entry_detail = new List<ACHEntryDetailTenderData>();
      foreach (ACHEntryDetailTenderData entry_detail in lst)
      {
        if (IsValidACHTender(entry_detail) && (Decimal.Parse(entry_detail.geoTndr.get("AMOUNT", "0.0").ToString()) >= 0)) // Don't process negtive tender
          selected_entry_detail.Add(entry_detail);
      }

      lst = selected_entry_detail;

      return lst;
    }

    protected List<ACHEntryDetailTenderData> GetValidEntryDetail(List<ACHEntryDetailTenderData> lst_entry_tender_data)
    {
      List<ACHEntryDetailTenderData> lst = lst_entry_tender_data ?? new List<ACHEntryDetailTenderData>();
      List<ACHEntryDetailTenderData> selected_entry_detail = new List<ACHEntryDetailTenderData>();
      foreach (ACHEntryDetailTenderData entry_detail in lst)
      {
        if (IsValidACHTender(entry_detail))
          selected_entry_detail.Add(entry_detail);
      }

      lst = selected_entry_detail;

      return lst;
    }

    protected bool IsValidACHTender(ACHEntryDetailTenderData entry_detail)
    {
      bool rst = false;
      GenericObject geoTenderClass = (GenericObject)((GenericObject)c_CASL.c_object("Tender.of")).get(entry_detail.geoTndr.get("TNDRID", "").ToString(), "");
      GenericObject geoParent = geoTenderClass.get("_parent") as GenericObject;
      if (string.Compare(geoParent.get("type", "").ToString(), "check", true) == 0)
        rst = true;

      return rst;
    }

    protected decimal GetTotalBatchAmount(List<ACHEntryDetailTenderData> lst_entry_detail)
    {
      decimal total_amount = 0;
      foreach (var entry_detail in lst_entry_detail)
      {
        string strAmount = entry_detail.geoTndr.get("AMOUNT", "0.0").ToString();
        total_amount += Decimal.Parse(strAmount);
      }
      return total_amount;
    }

    protected decimal GetTotalDebitBatchAmount(List<ACHEntryDetailTenderData> lst_entry_detail)
    {
      decimal total_amount = 0;
      foreach (var entry_detail in lst_entry_detail)
      {
        bool reversal = Decimal.Parse(entry_detail.geoTndr.get("AMOUNT", "0.0").ToString()) < 0;
        if (!reversal)
        {
          string strAmount = entry_detail.geoTndr.get("AMOUNT", "0.0").ToString();
          total_amount += Decimal.Parse(strAmount);
        }
      }
      return total_amount;
    }

    protected decimal GetTotalCreditBatchAmount(List<ACHEntryDetailTenderData> lst_entry_detail)
    {
      decimal total_amount = 0;
      foreach (var entry_detail in lst_entry_detail)
      {
        bool reversal = Decimal.Parse(entry_detail.geoTndr.get("AMOUNT", "0.0").ToString()) < 0;
        if (reversal)
        {
          string strAmount = entry_detail.geoTndr.get("AMOUNT", "0.0").ToString();
          strAmount = strAmount.Replace("-", "");
          total_amount += Decimal.Parse(strAmount);
        }
      }
      return total_amount;
    }

    protected decimal GetAbsoluteTnderAmount(ACHEntryDetailTenderData entry_detail)
    {
      decimal abs_amount = 0;

      string strAmount = entry_detail.geoTndr.get("AMOUNT", "0.0").ToString();
      strAmount = strAmount.Replace("-", "");
      abs_amount = Decimal.Parse(strAmount);
      return abs_amount;
    }

    protected long GetEntryHashInBatch(ACHRecord entry_detail_config, int total_entry_count_in_batch)
    {
      // Total of eight-character Transit Routing / ABA numbers in the batch (field 3 of the Entry Detail Record).
      // Do not include the Transit Routing Check Digit.Enter the ten low-order(right most) digits of this number.
      // For example, if the sum were 112233445566, you would enter 2233445566.

      long total_entry_hash_in_batch = 0;

      var rdfi = entry_detail_config.lstItem.Where(o => o.name == MyConstants.receiving_dfi_rt_number).FirstOrDefault();
      for (int i = 0; i < total_entry_count_in_batch; i++)
      {
        total_entry_hash_in_batch += Int64.Parse(rdfi.value);
      }

      return total_entry_hash_in_batch;

    }

    protected string ProcessFileHeader()
    {
      StringBuilder sb = new StringBuilder();
      string my_value = "";

      string file_header_fmt = _config_data.ach_file_structure.file_header_record_format;
      ACHRecord ach_record = file_header_fmt.ConvertToObject<ACHRecord>();  //JsonConvert.DeserializeObject<ACHRecord>(file_header_fmt);
      ach_record = ach_record ?? new ACHRecord();
      ach_record.lstItem = ach_record.lstItem ?? new List<ACHRecordItem>();
      ach_record.lstItem = ach_record.lstItem.OrderBy(o => o.field_seq).ToList();

      foreach (var item in ach_record.lstItem)
      {
        my_value = "";

        if (item.value_property != ValueProperty.CodeGenerated)
        {
          my_value = item.value.FormatValue(item.format);
        }
        else // code generated
        {
          DateTime dt = DateTime.Now;
          if (item.name == MyConstants.file_creation_date)
          {
            my_value = dt.FormatDate(item.format).FormatValue(item.format);
          }
          else if (item.name == MyConstants.file_creation_time)
          {
            my_value = dt.FormatDate(item.format).FormatValue(item.format); // format is HHmm

          }
          else if (item.name == MyConstants.file_id_modifier)
          {
            my_value = _file_id_modifier.FormatValue(item.format);
          }
        }

        sb.Append(my_value);
      }
      return sb.ToString();
    }

    protected string ProcessBatchHeaderForNonIAT(ACHRecord ach_record_config, List<ACHEntryDetailTenderData> lst_entry_detail, int batch_nbr, CoreFileInfo core_file_info)
    {
      if (lst_entry_detail == null || lst_entry_detail.Count == 0)
        return "";

      StringBuilder sb = new StringBuilder();
      string my_value = "";
      ach_record_config.lstItem = ach_record_config.lstItem.OrderBy(o => o.field_seq).ToList();


      foreach (var item in ach_record_config.lstItem)
      {
        my_value = "";
        if (item.value_property != ValueProperty.CodeGenerated)
        {
          my_value = item.value.FormatValue(item.format);
        }
        else // code generated
        {
          if (item.name == MyConstants.company_discretionary_data)
          {
            my_value = "";
            if (item.value_data_type == ValueDataType.CompanyDiscretionaryData)
            {
              CompanyDiscretionaryData ccd_info = item.value.ConvertToObject<CompanyDiscretionaryData>();// JsonConvert.DeserializeObject<CompanyDiscretionaryData>(item.value);
              if (ccd_info != null)
              {
                my_value = ccd_info.lst_info.GetBatchElementValue(core_file_info);
                my_value = my_value.FormatValue(item.format);
              }
            }

            // set default
            string temp = my_value;
            temp = temp.Trim();
            if (string.IsNullOrEmpty(temp))
            {
              if (!string.IsNullOrEmpty(core_file_info.deposit_slip_number))
                my_value = core_file_info.deposit_slip_number.FormatValue(item.format);
              else
              {
                // in case deposit slip number is empty
                my_value = core_file_info.core_file_name.FormatValue(item.format);
              }
            }
          }
          if (item.name == MyConstants.company_descriptive_date)
          {
            if (lst_entry_detail[0].geoFile.get("EFFECTIVEDT") is DateTime)
            {
              DateTime dt = (DateTime)lst_entry_detail[0].geoFile.get("EFFECTIVEDT");
              my_value = dt.FormatDate(item.format).FormatValue(item.format);
            }
          }
          else if (item.name == MyConstants.effective_entry_date)
          {
            // Next business day after File generation
            DateTime tmpDT = _dtFileCreateTime;
            do { tmpDT = tmpDT.AddDays(1.00); }
            while ((tmpDT.DayOfWeek == DayOfWeek.Saturday) || (tmpDT.DayOfWeek == DayOfWeek.Sunday));
            my_value = tmpDT.FormatDate(item.format).FormatValue(item.format);
          }
          else if (item.name == MyConstants.settlement_julian_date)
          {
            // leave it blank now
            my_value = item.value.FormatValue(item.format);
          }
          else if (item.name == MyConstants.batch_number)
          {
            my_value = batch_nbr.ToString().FormatValue(item.format);
          }

        }

        sb.Append(my_value);
      }
      return sb.ToString();
    }

    protected string ProcessEntryDetail(ACHBatchHeaderInfo bhi, ACHRecord ach_record_config, ACHEntryDetailTenderData entry_detail, int record_count, int nbr_of_addenda_for_this_trans = 0)
    {
      StringBuilder sb = new StringBuilder();

      // transaction code type
      string transaction_code_type = bhi.Get(MyConstants.transaction_code_type);

      // get list item of configured
      ach_record_config.lstItem = ach_record_config.lstItem.OrderBy(o => o.field_seq).ToList();

      string my_value = "";
      foreach (var item in ach_record_config.lstItem)
      {
        my_value = "";
        if (item.value_property != ValueProperty.CodeGenerated)
        {
          my_value = item.value.FormatValue(item.format);
        }
        else // code generated
        {
          if (item.name == MyConstants.transaction_code)
          {
            my_value = GetTransactionCode(bhi, entry_detail);
            my_value = my_value.FormatValue(item.format);
          }
          else if (item.name == MyConstants.receiving_dfi_rt_number)
          {
            string strBankRountingNbr = entry_detail.geoTndr.get("BANKROUTINGNBR", "").ToString();
            if (strBankRountingNbr.Length == 9)
            {
              my_value = strBankRountingNbr.Substring(0, 8);
              my_value = my_value.FormatValue(item.format);
            }
          }
          else if (item.name == MyConstants.rt_number_check_digit)
          {
            string strBankRountingNbr = entry_detail.geoTndr.get("BANKROUTINGNBR", "").ToString();
            if (strBankRountingNbr.Length == 9)
            {
              my_value = strBankRountingNbr.Substring(8, 1);
              my_value = my_value.FormatValue(item.format);
            }
          }
          else if (item.name == MyConstants.receiving_dfi_account_number)
          {
            string strEncryptedAccountNumber = entry_detail.geoTndr.get("BANKACCTNBR", "") as string;
            if (!string.IsNullOrEmpty(strEncryptedAccountNumber))
            {
              string secret_key = Crypto.get_secret_key();
              byte[] content_bytes = Convert.FromBase64String(strEncryptedAccountNumber);
              byte[] decrypted_bytes = Crypto.symmetric_decrypt("data", content_bytes, "secret_key", secret_key);
              my_value = System.Text.Encoding.Default.GetString(decrypted_bytes);
              my_value = my_value.FormatValue(item.format);
            }
          }
          else if (item.name == MyConstants.trans_amount)
          {
            if (transaction_code_type == nameof(TransactionCodeType.Standard))
            {
              // Entry amount in dollars with two decimal places. Rightjustified, left zero-filled, without a decimal point
              string strAmount = entry_detail.geoTndr.get("AMOUNT", "0.0").ToString();
              strAmount = strAmount.Replace("-", ""); // remove nagtive sign if applied
              my_value = string.Format("{0}", Math.Round(Decimal.Parse(strAmount) * 100));
            }
            else
              my_value = "0"; // set to "0"s for prenote
            my_value = my_value.FormatValue(item.format);

          }
          else if (item.name == MyConstants.check_serial_number)
          {
            my_value = entry_detail.geoTndr.get("CC_CK_NBR", "") as string;
            my_value = my_value.FormatValue(item.format);
          }
          else if (item.name == MyConstants.number_of_addenda_records)
          {
            my_value = nbr_of_addenda_for_this_trans.ToString().FormatValue(item.format);
          }
          else if (item.name == MyConstants.individual_id_number)
          {
            if (item.value_data_type == ValueDataType.IndividualIdInfo)
            {
              IndividualIdInfo individual_id_info = item.value.ConvertToObject<IndividualIdInfo>(); // JsonConvert.DeserializeObject<IndividualIdInfo>(item.value);
              if (individual_id_info != null)
              {
                my_value = individual_id_info.lst_individual_id_debit.GetBatchElementValue(entry_detail);
                my_value = my_value.FormatValue(item.format);
              }
              else
                my_value = "".FormatValue(item.format);
            }

          }
          else if (item.name == MyConstants.individual_name)
          {
            if (item.value_data_type == ValueDataType.IndividualNameInfo)
            {
              IndividualNameInfo individual_name_info = item.value.ConvertToObject<IndividualNameInfo>();  //JsonConvert.DeserializeObject<IndividualNameInfo>(item.value);
              if (individual_name_info != null)
              {
                my_value = individual_name_info.lst_individual_name_debit.GetBatchElementValue(entry_detail);
                my_value = my_value.FormatValue(item.format);
              }
              else
                my_value = "".FormatValue(item.format);
            }

          }
          else if (item.name == MyConstants.trace_number)
          {
            // first 8 characters is originating_dfi_id, second 7 characters are filled with the entry detail sequence number
            string originating_dfi_id = _config_data.ach_general_info.Get(MyConstants.originating_dfi_id).PadRight(8);
            string tndr_seq_nbr = record_count.ToString().PadLeft(7, '0');
            my_value = $"{originating_dfi_id}{tndr_seq_nbr}";

          }
        }

        sb.Append(my_value);


      }

      return sb.ToString();
    }

    protected string GetTransactionCode(ACHBatchHeaderInfo bhi, ACHEntryDetailTenderData entry_detail)
    {
      string my_value = "";

      // transaction code type
      string transaction_code_type = bhi.Get(MyConstants.transaction_code_type);

      GenericObject geoTenderClass = (GenericObject)((GenericObject)c_CASL.c_object("Tender.of")).get(entry_detail.geoTndr.get("TNDRID", "").ToString(), "");
      string strAccountType = geoTenderClass.get("account_type", "").ToString();

      switch (transaction_code_type)
      {
        case nameof(TransactionCodeType.Standard):
          if (strAccountType.ToLower() == "checking account")
            my_value = entry_detail.entry_detail_type == EntryDetailType.Debit ? "27" : "22";
          else if (strAccountType.ToLower() == "saving account")
            my_value = entry_detail.entry_detail_type == EntryDetailType.Debit ? "37" : "32";
          break;
        case nameof(TransactionCodeType.Prenote):
          if (strAccountType.ToLower() == "checking account")
            my_value = entry_detail.entry_detail_type == EntryDetailType.Debit ? "28" : "23";
          else if (strAccountType.ToLower() == "saving account")
            my_value = entry_detail.entry_detail_type == EntryDetailType.Debit ? "38" : "33";
          break;
        case nameof(TransactionCodeType.ZeroDollarWithRD):
          if (strAccountType.ToLower() == "checking account")
            my_value = entry_detail.entry_detail_type == EntryDetailType.Debit ? "29" : "24";
          else if (strAccountType.ToLower() == "saving account")
            my_value = entry_detail.entry_detail_type == EntryDetailType.Debit ? "39" : "34";
          break;
      }
      return my_value;
    }

    protected string GetTransactionCodeForBalanceRecord(ACHBatchHeaderInfo bhi, bool addCredit)
    {
      string my_value = "";

      // transaction code type
      string transaction_code_type = bhi.Get(MyConstants.transaction_code_type);

      // balance account type
      string balance_account_type = bhi.Get(MyConstants.balance_account_type);

      switch (transaction_code_type)
      {
        case nameof(TransactionCodeType.Standard):
          if (balance_account_type == nameof(BalanceAccountType.Checking))
            my_value = !addCredit ? "27" : "22";
          else if (balance_account_type == nameof(BalanceAccountType.Saving))
            my_value = !addCredit ? "37" : "32";
          break;
        case nameof(TransactionCodeType.Prenote):
          if (balance_account_type == nameof(BalanceAccountType.Checking))
            my_value = !addCredit ? "28" : "23";
          else if (balance_account_type == nameof(BalanceAccountType.Saving))
            my_value = !addCredit ? "38" : "33";
          break;
        case nameof(TransactionCodeType.ZeroDollarWithRD):
          if (balance_account_type == nameof(BalanceAccountType.Checking))
            my_value = !addCredit ? "29" : "24";
          else if (balance_account_type == nameof(BalanceAccountType.Saving))
            my_value = !addCredit ? "39" : "34";
          break;
      }
      return my_value;
    }

    protected string ProcessEntryDetailToBalanceBatch(ACHBatchHeaderInfo bhi, ACHRecord ach_record_config, List<ACHEntryDetailTenderData> lst_entry_detail, int record_count, bool addCredit, decimal balanceAmt, int nbr_of_addenda_for_this_trans = 0)
    {
      StringBuilder sb = new StringBuilder();
      string my_value = "";

      // transaction code type
      string transaction_code_type = bhi.Get(MyConstants.transaction_code_type);

      // sorrt first
      ach_record_config.lstItem = ach_record_config.lstItem.OrderBy(o => o.field_seq).ToList();

      foreach (var item in ach_record_config.lstItem)
      {
        my_value = "";
        if (item.value_property != ValueProperty.CodeGenerated)
        {
          my_value = item.value.FormatValue(item.format);
        }
        else // code generated
        {
          if (item.name == MyConstants.transaction_code)
          {
            my_value = GetTransactionCodeForBalanceRecord(bhi, addCredit);
            my_value = my_value.FormatValue(item.format);
          }
          else if (item.name == MyConstants.receiving_dfi_rt_number)
          {
            my_value = item.value.FormatValue(item.format); // use configured default value
          }
          else if (item.name == MyConstants.rt_number_check_digit)
          {
            my_value = item.value.FormatValue(item.format); // use configured default value
          }
          else if (item.name == MyConstants.receiving_dfi_account_number)
          {
            my_value = item.value.FormatValue(item.format); // use configured default value
          }
          else if (item.name == MyConstants.trans_amount)
          {
            if (transaction_code_type == nameof(TransactionCodeType.Standard))
              my_value = string.Format("{0}", balanceAmt * 100);
            else
              my_value = "0";
            my_value = my_value.FormatValue(item.format);
          }
          else if (item.name == MyConstants.check_serial_number)
          {
            // HX later(?): use corefile name instead 
            my_value = lst_entry_detail[0].geoFile.GetCoreFileName();
            my_value = my_value.FormatValue(item.format);
          }
          else if (item.name == MyConstants.number_of_addenda_records)
          {
            my_value = nbr_of_addenda_for_this_trans.ToString().FormatValue(item.format);
          }
          else if (item.name == MyConstants.individual_id_number)
          {
            IndividualIdInfo individual_id_info = item.value.ConvertToObject<IndividualIdInfo>(); // JsonConvert.DeserializeObject<IndividualIdInfo>(item.value);
            if (individual_id_info != null)
            {
              my_value = individual_id_info.lst_individual_id_credit.GetBatchElementValue(lst_entry_detail[0]);
              my_value = my_value.FormatValue(item.format);
            }
            else
            {
              my_value = "";
              my_value = my_value.FormatValue(item.format);
            }
          }
          else if (item.name == MyConstants.individual_name)
          {
            if (item.value_data_type == ValueDataType.IndividualNameInfo)
            {
              IndividualNameInfo individual_name_info = item.value.ConvertToObject<IndividualNameInfo>(); //JsonConvert.DeserializeObject<IndividualNameInfo>(item.value);
              if (individual_name_info != null)
              {
                my_value = individual_name_info.lst_individual_name_credit.GetBatchElementValue(lst_entry_detail[0]);
                my_value = my_value.FormatValue(item.format);
              }
              else
              {
                my_value = "";
                my_value = my_value.FormatValue(item.format);
              }
            }
          }
          else if (item.name == MyConstants.trace_number)
          {
            // first 8 characters is originating_dfi_id, second 7 characters are filled with the entry detail sequence number
            string originating_dfi_id = _config_data.ach_general_info.Get(MyConstants.originating_dfi_id).PadRight(8);
            string tndr_seq_nbr = record_count.ToString().PadLeft(7, '0');
            my_value = $"{originating_dfi_id}{tndr_seq_nbr}";
          }
        }

        sb.Append(my_value);
      }

      return sb.ToString();
    }

    protected string ProcessBatchControl(ACHRecord ach_record_config, int batch_nbr, int cnt_in_batch, long entry_hash, decimal total_debit_amount_in_batch, decimal total_credit_amount_in_batch)
    {
      StringBuilder sb = new StringBuilder();
      string my_value = "";

      foreach (var item in ach_record_config.lstItem)
      {
        my_value = "";

        if (item.value_property != ValueProperty.CodeGenerated)
        {
          my_value = item.value.FormatValue(item.format);
        }
        else // code generated
        {
          if (item.name == MyConstants.entry_addenda_count_in_batch)
          {
            my_value = cnt_in_batch.ToString().FormatValue(item.format);
          }
          else if (item.name == MyConstants.entry_hash_in_batch)
          {
            my_value = (entry_hash % 10000000000).ToString().FormatValue(item.format);
          }
          else if (item.name == MyConstants.total_debit_amount_in_batch)
          {
            my_value = string.Format("{0}", total_debit_amount_in_batch * 100);
            my_value = my_value.FormatValue(item.format);
          }
          else if (item.name == MyConstants.total_credit_amount_in_batch)
          {
            my_value = string.Format("{0}", total_credit_amount_in_batch * 100);
            my_value = my_value.FormatValue(item.format);
          }
          else if (item.name == MyConstants.batch_number)
          {
            my_value = batch_nbr.ToString();
            my_value = my_value.FormatValue(item.format);
          }
        }

        sb.Append(my_value);
      }
      return sb.ToString();
    }

    protected string ProcessFileControl(int batch_count, int total_entry_in_file, decimal total_debit_amount_in_file, decimal total_credit_amount_in_file, long entry_hash_in_file)
    {
      StringBuilder sb = new StringBuilder();
      string my_value = "";

      string file_control_fmt = _config_data.ach_file_structure.file_control_record_format;
      ACHRecord ach_record = file_control_fmt.ConvertToObject<ACHRecord>(); // JsonConvert.DeserializeObject<ACHRecord>(file_control_fmt);
      ach_record = ach_record ?? new ACHRecord();
      ach_record.lstItem = ach_record.lstItem ?? new List<ACHRecordItem>();
      ach_record.lstItem = ach_record.lstItem.OrderBy(o => o.field_seq).ToList();

      foreach (var item in ach_record.lstItem)
      {
        my_value = "";

        if (item.value_property != ValueProperty.CodeGenerated)
        {
          my_value = item.value.FormatValue(item.format);
        }
        else // code generated
        {
          if (item.name == MyConstants.batch_count)
          {
            my_value = batch_count.ToString().FormatValue(item.format);
          }
          else if (item.name == MyConstants.block_count)
          {
            // Field block count must match the number of blocks in the file. 
            // In ACH-file, each complete and partial collection of 10 lines constitute as one block. 
            // This means that a file with 7 lines constitutes as a partial collection of one block, 
            // therefore block count of the file is 1. File with 15 lines wuold have a block count as 2. 
            my_value = ((total_entry_in_file + 2 * batch_count + 2) / 10 + 1).ToString();
            //my_value = ((total_entry_in_file + 2 * batch_count - 1) / 10 + 1).ToString();
            my_value = my_value.FormatValue(item.format);
          }
          else if (item.name == MyConstants.entry_addenda_count_in_file)
          {
            my_value = total_entry_in_file.ToString().FormatValue(item.format);
          }
          else if (item.name == MyConstants.entry_hash_in_file)
          {
            my_value = (entry_hash_in_file % 10000000000).ToString().FormatValue(item.format);
          }
          else if (item.name == MyConstants.total_debit_amount_in_file)
          {
            my_value = Math.Round(total_debit_amount_in_file * 100).ToString().FormatValue(item.format);
          }
          else if (item.name == MyConstants.total_credit_amount_in_file)
          {
            my_value = Math.Round(total_credit_amount_in_file * 100).ToString().FormatValue(item.format);
          }
        }

        sb.Append(my_value);
      }
      return sb.ToString();
    }
    protected List<string> ProcessAddendaRecord(ACHRecord ach_record_config, ACHEntryDetailTenderData entry_detail, int entry_detail_seq_nbr)
    {
      List<string> lst_addenda_record = new List<string>();
      string my_value = "";
      List<string> lst_payment_related_info = new List<string>();

      ach_record_config = ach_record_config ?? new ACHRecord();
      ach_record_config.lstItem = ach_record_config.lstItem ?? new List<ACHRecordItem>();

      // Get Addenda Payment related Information
      var addenda_payment_info_item = ach_record_config.lstItem.FirstOrDefault(o => o.name == MyConstants.addenda_payment_related_info);
      addenda_payment_info_item = addenda_payment_info_item ?? new ACHRecordItem();
      AddendaPymtRelatedInfo addenda_pymt_info = addenda_payment_info_item.value.ConvertToObject<AddendaPymtRelatedInfo>();  //JsonConvert.DeserializeObject<AddendaPymtRelatedInfo>(addenda_payment_info_item.value);
      if (addenda_pymt_info != null)
        lst_payment_related_info = addenda_pymt_info.lst_info_debit.GetAddendaPaymentInfo(entry_detail);

      // payment related information - 80 characters of free form text to include in the addenda record
      // Must contain NACHA endorsed ANSI ASC X12 data segments or NACHA endorsed banking conventions.The asterisk(“*”) must be the
      // delimiter between the data elements, and the back slash(“\”) must be the terminator between the data segments. The backslash(“\”) or tilde
      // (“~”) may be used when originating ACK, ATX, CCD, CIE, ENR, IAT, PPD and WEB entries.

      int cnt = 0;
      foreach (string payment_info in lst_payment_related_info)
      {
        StringBuilder sb = new StringBuilder();
        foreach (var item in ach_record_config.lstItem)
        {
          my_value = "";
          if (item.value_property != ValueProperty.CodeGenerated)
          {
            my_value = item.value.FormatValue(item.format);
          }
          else // code generated
          {
            if (item.name == MyConstants.addenda_payment_related_info)
            {
              my_value = payment_info.FormatValue(item.format);
            }
            else if (item.name == MyConstants.addenda_seq_nbr)
            {
              cnt++;
              my_value = cnt.ToString().FormatValue(item.format);
            }
            else if (item.name == MyConstants.addenda_entry_detail_seq_nbr)
            {
              my_value = entry_detail_seq_nbr.ToString().FormatValue(item.format);
            }

          }

          sb.Append(my_value);
        }

        lst_addenda_record.Add(sb.ToString());
      }

      return lst_addenda_record;
    }
    protected List<string> ProcessAddendaRecordToBalanceBatch(ACHRecord ach_record_config, ACHEntryDetailTenderData entry_detail, int entry_detail_seq_nbr)
    {
      List<string> lst_addenda_record = new List<string>();
      string my_value = "";
      List<string> lst_payment_related_info = new List<string>();

      ach_record_config = ach_record_config ?? new ACHRecord();
      ach_record_config.lstItem = ach_record_config.lstItem ?? new List<ACHRecordItem>();

      // Get Addenda Payment related Information
      var addenda_payment_info_item = ach_record_config.lstItem.FirstOrDefault(o => o.name == MyConstants.addenda_payment_related_info);
      addenda_payment_info_item = addenda_payment_info_item ?? new ACHRecordItem();
      AddendaPymtRelatedInfo addenda_pymt_info = addenda_payment_info_item.value.ConvertToObject<AddendaPymtRelatedInfo>(); // JsonConvert.DeserializeObject<AddendaPymtRelatedInfo>(addenda_payment_info_item.value);
      if (addenda_pymt_info != null)
        lst_payment_related_info = addenda_pymt_info.lst_info_credit.GetAddendaPaymentInfo(entry_detail);

      // payment related information - 80 characters of free form text to include in the addenda record
      // Must contain NACHA endorsed ANSI ASC X12 data segments or NACHA endorsed banking conventions.The asterisk(“*”) must be the
      // delimiter between the data elements, and the back slash(“\”) must be the terminator between the data segments. The backslash(“\”) or tilde
      // (“~”) may be used when originating ACK, ATX, CCD, CIE, ENR, IAT, PPD and WEB entries.

      int cnt = 0;
      foreach (string payment_info in lst_payment_related_info)
      {
        StringBuilder sb = new StringBuilder();
        foreach (var item in ach_record_config.lstItem)
        {
          my_value = "";
          if (item.value_property != ValueProperty.CodeGenerated)
          {
            my_value = item.value.FormatValue(item.format);
          }
          else // code generated
          {
            if (item.name == MyConstants.addenda_payment_related_info)
            {
              my_value = payment_info.FormatValue(item.format);
            }
            else if (item.name == MyConstants.addenda_seq_nbr)
            {
              cnt++;
              my_value = cnt.ToString().FormatValue(item.format);
            }
            else if (item.name == MyConstants.addenda_entry_detail_seq_nbr)
            {
              my_value = entry_detail_seq_nbr.ToString().FormatValue(item.format);
            }

          }

          sb.Append(my_value);
        }

        lst_addenda_record.Add(sb.ToString());
      }

      return lst_addenda_record;

    }


  }
}
