using System;
using System.Net;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using CASL_engine;
using ftp;

/*
 ****** Bug 16586 [Bug 19784] - Load ABA Validation Data into Memory: Added ABAValidation.cs File *****
 
    FedACH Directory File Format
    ============================

    Field Name                             Length         Position
    ==============================================================
    Routing number                              9              1-9
    Telegraphic name                           18            10-27
    Customer name                              36            28-63
    State or territory abbreviation             2            64-65
    City                                       25            66-90
    Funds transfer status:                                        
        Y eligible N ineligible                 1               91
    Funds settlement-only status:                                 
        S settlement-only                       1               92
    Book-Entry Securities transfer status:                        
        N ineligible Y eligible                 1               93
    Date of last revision:                                        
        YYYYMMDD, or blank                      8           94-101
 */

namespace ABAValidation
{
  /// <summary>
  /// Bug 16586 [Bug 19784] - Load ABA Validation Data into Memory UMN: Try a different strategy; Load the 
  /// whole ABA table into memory (just routing nbr => bank name). The code will try to load
  /// baseline\ABAValidation\file_download\FedACHdir.aba, which should be SFTPed as a scheduled job
  /// by a autofetchachfile.vbs file or at iPayment start-up. If the file is not found (job fails),
  /// load a backup file which will be checked in as part of the project: 
  /// baseline\ABAValidation\file_download\FedACHdir.abab. The current checked in
  /// FedACHdir.abab has 8599 lines. So the size of Dictionary will be 8599 * (9 + 36) = 
  /// 386955 bytes, or 386.95KB -- roughly 0.5MB which isn't that much.
  /// 
  /// NOTE: Eventually, the ACH code in main.js and ABAValidation/code_casl will need to be simplified.
  /// </summary>
  public class ABAValidation
  {
    const string DOWNLOAD_DIR_PATH    = @"/baseline/ABAValidation/file_download/";
    const string DOWNLOAD_DIR_FILE    = @"FedACHdir.aba";      // Bug 20434 UMN PCI/DSS change names to prevent access through server
    const string BACK_UP_DIR_FILE     = @"FedACHdir.abab";     // Bug 20434 UMN PCI/DSS change names to prevent access through server
    const int    MIN_LINE_LENGTH      = 63;
    const int    EXPECTED_LINE_LENGTH = 101;
    const long   MIN_FILE_BYTES       = 400000; // File is currently 825KB = 825000 Bytes (MUST be at least half that to pass validation)

    private static Dictionary<String, String> routingToNameMap = new Dictionary<string, string>();

    /// <summary>
    /// CASL initialization routine when iPayment starts up (called from load_apps_common.casl):
    ///   1) Gets the latest file from the SFTP server.
    ///   2) Loads the ABA table into memory.
    ///   3) Always returns null.
    /// </summary>
    /// <param name="arg_pairs"></param>
    public static object PopulateABAData(params object[] arg_pairs)
    {
      GenericObject geoARGS = misc.convert_args(arg_pairs);
      GenericObject geoSI = (GenericObject)geoARGS.get("the_si");
      if (geoSI == null)
      {
        Logger.cs_log("ABA Validation was not initialized; there is no ABAValidation system interface configured.");
        return null;
      }
      try
      {
        Logger.cs_log("START ABA Validation initialization.");
        // Get the latest file from the SFTP server
        GetRoutingFile_aux(geoSI, false);
        // Load the ABA table into memory
        LoadRoutingToNameMap();
      }
      finally
      {
        Logger.cs_log("END ABA Validation initialization.");
      }
      return null;
    }

    /// <summary>
    /// Make certain the file transferred from the SFTP server is in the expected format by checking file size
    /// and line length (of the first 100 lines).  Use Back-Up if file is not valid.
    /// </summary>
    /// <param name="filePath"></param>
    /// <returns></returns>
    public static bool ValidateFileSizeAndFormat(string filePath)
    {
      string errMsg = "";

      // Check the file size
      long fileBytes = new System.IO.FileInfo(filePath).Length;
      if (fileBytes < MIN_FILE_BYTES)
      {
        errMsg = string.Format("File ({0}) size is {1} bytes; this is much smaller than the expected file size.", filePath, fileBytes);
      }
      else // File overall size OK - Check line length
      {
        int recordCount = 0;
        int badLineCount = 0;
        string line;
        using (System.IO.StreamReader fileReader = new System.IO.StreamReader(filePath))
        {
          while ((line = fileReader.ReadLine()) != null)
          {
            if (line.Trim().Length < 1) continue; // Skip blank lines
            // Make certain file is in expected format
            if (line.Length != EXPECTED_LINE_LENGTH)
            {
              badLineCount++;
            }
            if (++recordCount == 100) 
            {
              break;
            }
          }
        }
        if (badLineCount > 5) 
        {
          errMsg = string.Format("The ABA file [{0}] contains too many lines ({1}) that do NOT have the expected line length ({2} characters)."
            , filePath, badLineCount, EXPECTED_LINE_LENGTH);
        }
      }
      bool bValid = string.IsNullOrEmpty(errMsg);
      if (!bValid)
      {
        Logger.cs_log(errMsg);
        SendEmailNotification(false, "Federal Reserve ABA File in Unexpected Format", "", "Inspect ABA File", "INVALID FILE FORMAT:", errMsg);
      }
      return bValid;
    }

    /// <summary>
    /// This method loads the whole ABA table (from file "FedACHdir.aba") into memory ("routingToNameMap" Dictionary).
    /// </summary>
    public static void LoadRoutingToNameMap()
    {
      bool bSuccess = false;
      string warningMsg = "";
      string logMsg     = "";

      try
      {
        // Check if baseline/business/include/FedACHdir.aba downloaded file exists.
        string fedACHfile = string.Format("{0}{1}{2}", misc.CASL_get_code_base(), DOWNLOAD_DIR_PATH, DOWNLOAD_DIR_FILE);
        
        if (File.Exists(fedACHfile))
        {
          if (!ValidateFileSizeAndFormat(fedACHfile)) // Quickly validate file size and format
          {
            // Switch to the back-up file (FedACHdir.abab) if it is not valid
            warningMsg = string.Format("WARNING: {0} does not have expected size or format; switching to backup file ({1}).", fedACHfile, BACK_UP_DIR_FILE);
            fedACHfile = fedACHfile.Replace(DOWNLOAD_DIR_FILE, BACK_UP_DIR_FILE);
          }
        }
        else 
        {
          // Switch to the back-up file (FedACHdir.abab) if it is not found
          warningMsg = string.Format("WARNING: Can't find {0}; switching to backup file ({1}).", fedACHfile, BACK_UP_DIR_FILE);
          fedACHfile = fedACHfile.Replace(DOWNLOAD_DIR_FILE, BACK_UP_DIR_FILE);
        }

        if (File.Exists(fedACHfile))
        {
          string line = "";
          using (System.IO.StreamReader fileReader = new System.IO.StreamReader(fedACHfile))
          {
            int recordCount = 0;

            // If can't find the daily (.aba) or backup (.abab) version of the file, give up
            if (fileReader.Peek() == -1)
            {
              logMsg = string.Format("Can't read {0} file.", fedACHfile);
              return;
            }

            // Process each line in the import file
            while ((line = fileReader.ReadLine()) != null)
            {
              // Skip blank lines and Bug 21840 UMN skip federal reserve banks
              // don't worry, Contains is wicked fast
              if (line.Trim().Length < 1 ||
                line.Contains("FEDERAL RESERVE BANK") ||
                line.Contains("FRB ") ||
                line.Contains("FRB-") ||
                line.Contains("BUREAU OF THE PUBLIC DEBT") ||
                line.Contains("DISBURSING")
                )
              {
                continue;
              }

              // Make certain file is in expected format
              if (line.Length >= MIN_LINE_LENGTH)
              {
                // Insert routing number and bank name in map
                routingToNameMap.Add(line.Substring(0, 9).Trim(), line.Substring(27, 36).Trim());
                recordCount++;
              }
              else 
              {
                warningMsg += (string.IsNullOrEmpty(warningMsg) ? "WARNING: " : "\r\n") + 
                  string.Format("Unexpected Line Format: {0}", line);
              }
            }
            logMsg = string.Format("Loaded {0} records", recordCount);
            bSuccess = true;
          }
        }
        else
        {
          logMsg = string.Format("Can't find {0} file. The backup file is missing.", fedACHfile);
        }
      }
      catch (Exception ex)
      {
        logMsg = ex.ToMessageAndCompleteStacktrace();
      }
      finally
      {
        string action = "Loading Bank ABA Routing Numbers";
        if (!string.IsNullOrEmpty(warningMsg)) { Logger.cs_log(warningMsg); }
        string result = string.Format("{0}: {1}.", (bSuccess ? "SUCCESSFUL" : "FAILED"), action);
        Logger.cs_log(string.Format("{0} {1}", result, logMsg));

        string subject = result.Replace("SUCCESSFUL", "BACKUP FILE USED");
        SendEmailNotification(bSuccess, subject, warningMsg, action, result, logMsg);
      }
    }

    /// <summary>
    /// CASL routine for validating the ABA routing number...
    /// </summary>
    /// <param name="argPairs"></param>
    /// <returns></returns>
    public static object CASL_ABA_Validation(params object[] argPairs)
    {
      GenericObject args = misc.convert_args(argPairs);
      string routingNbr = args.get("ABA", "") as string;
      return ABA_Validation(routingNbr);
    }

    /// <summary>
    /// This method validates a Bank Routing Number by returning a GEO with the name 
    /// of the financial institution if found.  Otherwise, returns a CASL error GEO.
    /// </summary>
    /// <param name="routingNbr"></param>
    /// <returns></returns>
    public static object ABA_Validation(string routingNbr)
    {
      GenericObject objRet = null;
      string bankName = "";

      if (routingToNameMap.Count == 0) // Return error if routingToNameMap is empty, so check first
      {
        objRet = c_CASL.c_make("error",
          "code", "VABA201",
          "message", "ERROR ABA table is NOT loaded.",
          "title", "ABA Validation Error",
          "throw", false) as GenericObject;

        // Bug 26691 - ABA Validation: unrecognized routing number can appear as if it's recognized
        objRet.set("bank_name", "");
      }
      else
      {
        if (routingToNameMap.TryGetValue(routingNbr, out bankName))
        {
          objRet = c_CASL.c_GEO();
          objRet.set("bank_name", bankName);
        }
        else
        {
          objRet = c_CASL.c_make("error",
            "code", "VABA202",
            "message", "Number was not found in ABA table.",
            "title", "ABA Validation Error",
            "throw", false) as GenericObject;

          // Bug 26691 - ABA Validation: unrecognized routing number can appear as if it's recognized
          objRet.set("bank_name", "");
        }
      }
      return objRet;
    }

    /// <summary>
    /// CASL routine for getting routing file from the SFTP Server
    /// </summary>
    /// <param name="argPairs"></param>
    /// <returns></returns>
    public static Object GetRoutingFile(params Object[] arg_pairs)
    {
      GenericObject geoARGS = misc.convert_args(arg_pairs);
      GenericObject geoSI = (GenericObject)geoARGS.get("the_si");
      object objLoadData = geoARGS.get("load_data", false);
      bool bLoadData = objLoadData.GetType() == typeof(System.String) ? (bool)(((string)objLoadData) == "true") : (bool)objLoadData;
      return GetRoutingFile_aux(geoSI, bLoadData);
    }

    /// <summary>
    /// This method will get the Federal Reserve E-Payments Routing Directory text file from the SFTP Server.
    /// NOTE: The Federal Reserve E-Payments Routing Directory URL now uses an agreement page with cookies so
    /// the file will be manually downloaded and saved to the CORE SFTP server.
    /// </summary>
    /// <param name="geoSI">System Interface configuration</param>
    /// <param name="bLoadData">Not used (may want to auto-load the data into memory in the future)</param>
    /// <returns></returns>
    public static bool GetRoutingFile_aux(GenericObject geoSI, bool bLoadData)
    {
      Logger.cs_log("START Get the Federal Reserve E-Payments Routing Directory text file from the SFTP Server");
      bool   bSuccess   = false;
      string errMsg     = "";
      string sftpServer = "";
      string sftpPort   = "";
      string sftpUser   = "";
      string sftpPW     = "";
      string sftpPath   = "";
      string sftpFile   = "";
      SFTP   sftp       = null;

      try
      {
        sftpServer = geoSI.get("sftp_server", "").ToString();
        sftpPort   = geoSI.get("sftp_port", "").ToString();
        sftpUser   = geoSI.get("sftp_username", "").ToString();
        sftpPW     = geoSI.get("sftp_password", "").ToString();
        sftpPath   = geoSI.get("sftp_path", "").ToString();
        sftpFile   = geoSI.get("sftp_file", "").ToString();
        if (!string.IsNullOrEmpty(sftpPW)) // Decrypt SFTP Password
        {
          string secret_key = Crypto.get_secret_key();
          byte[] content_bytes = Convert.FromBase64String(sftpPW);
          byte[] decrypted_bytes = Crypto.symmetric_decrypt("data", content_bytes, "secret_key", secret_key);
          sftpPW = System.Text.Encoding.Default.GetString(decrypted_bytes);
        }
        string sFedDirFileName = string.Format("{0}{1}{2}", misc.CASL_get_code_base(), DOWNLOAD_DIR_PATH, DOWNLOAD_DIR_FILE);

        // SFTP file transfer process
        {
          sftp = new SFTP(sftpServer, sftpUser, sftpPW, sftpPort, true, false, "", "", "");
          sftp.Connect();
          bSuccess = true;
          try
          {
            sftp.GetFile(Path.Combine(sftpPath, sftpFile), sFedDirFileName);
          }
          catch
          {
            bSuccess = false;
          }
        }
      }
      catch (Exception ex)
      {
        errMsg = ex.ToMessageAndCompleteStacktrace();
      }
      finally
      {
        if (sftp != null) sftp.Disconnect();
        if (!string.IsNullOrEmpty(errMsg))
        {
          Logger.cs_log(errMsg);
        }
        if (!bSuccess)
        {
          string action = "Getting Federal Reserve ABA Routing Number Data from SFTP Server.";
          string subject = "FAILED To Get Federal Reserve ABA Routing Number Text File from SFTP Server";
          string details = string.Format("Configured SFTP Server: {0}", sftpServer);
          SendEmailNotification(bSuccess, subject, "", action, errMsg, details);
        }
        Logger.cs_log(string.Format("END Get the Federal Reserve E-Payments Routing Directory text file from the SFTP Server: {0}"
          , (bSuccess ? "SUCCEEDED" : "FAILED")));
      }
      return bSuccess;
    }

    /// <summary>
    /// Sends an email notification if a task is NOT successful or a warning message is sent.
    /// </summary>
    /// <param name="bSuccess"></param>
    /// <param name="subject"></param> 
    /// <param name="warning"></param>
    /// <param name="action"></param>
    /// <param name="result"></param>
    /// <param name="resultDetails"></param>
    private static void SendEmailNotification(bool bSuccess, string subject, string warning, string action, string result, string resultDetails)
    {
      if (!bSuccess || !string.IsNullOrEmpty(warning))
      {
        GenericObject geoContent = c_CASL.c_GEO();
        if (!string.IsNullOrEmpty(warning)) { geoContent.set("Warning", warning); }
        geoContent.set("Result", string.Format("{0} {1}", result, resultDetails));
        try
        {
          emails.CASL_send_email_notification(
            "module", "iPayment Revenue Portal",
            "action", action,
            "email_subject", subject,
            "email_receiver", "DEFAULT",
            "email_content", geoContent);
        }
        catch (Exception e)
        {
          string msg = "Exception sending email: " + e.ToMessageAndCompleteStacktrace();
          Logger.cs_log(msg);
        }
      }
    }

    /// <summary>
    /// Bug# 24172 DH - ABA validation for Peripherals service.
    /// It's impossible to interrupt some device processing flow so this data must be available ahead of the device beginning of a scan.
    /// Data is transferred to the service only on a first login after iPayment is reloaded.
    /// If for some reason the service needs to get a new data set again (restart of the service), it will clear the downloaded flag in the GEO and get the data ASAP and on the next cashier login in case of a failure.
    ///   - A successful service update will again disable it for the subsequent cashier logins.
    /// </summary>
    /// <returns>Return the ABA Validation data (routing only). Compress it to minimize the web service load.</returns>
    public static string get_ABAValidationData(params object[] argPairs)
    {      
      // If the ABA data set is totally empty in the peripherals service, it will fail the next scan and attempt to re-download the ABA data set.
      System.Text.StringBuilder sb = new System.Text.StringBuilder();
      if (routingToNameMap.Count > 0)
      {
        IDictionaryEnumerator DictEnumerator = ((Dictionary<string, string>)routingToNameMap).GetEnumerator();
        while (DictEnumerator.MoveNext())
          sb.Append(DictEnumerator.Key + ",");// Need the comma to simplify search for the routing number at the service.
                
        byte[] buffer = System.Text.Encoding.UTF8.GetBytes(sb.ToString());
        var memoryStream = new MemoryStream();
        using (var gZipStream = new System.IO.Compression.GZipStream(memoryStream, System.IO.Compression.CompressionMode.Compress, true))
        {
          gZipStream.Write(buffer, 0, buffer.Length);
        }

        memoryStream.Position = 0;

        var compressedData = new byte[memoryStream.Length];
        memoryStream.Read(compressedData, 0, compressedData.Length);
        var gZipBuffer = new byte[compressedData.Length + 4];
        Buffer.BlockCopy(compressedData, 0, gZipBuffer, 4, compressedData.Length);
        Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gZipBuffer, 0, 4);
        return Convert.ToBase64String(gZipBuffer);// Decompress and convert back to ascii at the service.
      }

      return "no_data_on_server";// This will help the service determine if it was reloaded or if there is really no data in the ABA file.
     }
  }
}
