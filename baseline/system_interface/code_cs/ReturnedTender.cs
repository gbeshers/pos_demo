using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using CASL_engine;
using System.Data;
using System.IO;
using System.Threading;
using System.Security.Permissions;
using System.Collections.Concurrent;

namespace baseline
{
  // Bug 24902 MJO - Returned Tender Feature

  public class ReturnedTender
  {
    private static bool EnableWatching = false;

    private ReturnedTender()
    {
    }

    public static object fCASL_StartFileWatcher(CASL_Frame frame)
    {
      GenericObject geo_args = frame.args;
      GenericObject systemInterface = geo_args.get("_subject") as GenericObject;

      return StartFileWatcher(systemInterface);
    }

    /// <summary>
    /// Begin monitoring of configured directory for new/changed files
    /// </summary>
    /// <param name="systemInterface"></param>
    /// <returns></returns>
    private static object StartFileWatcher(GenericObject systemInterface)
    {
      Thread watcherThread = new Thread(Watch);
      watcherThread.Start(systemInterface);

      return null;
    }

    /// <summary>
    /// Create FileSystemWatcher that notifies us when a file is new or changed
    /// </summary>
    /// <param name="data"></param>
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    private static void Watch(object data)
    {
      while (!c_CASL.IsLoaded)
      {
        Thread.Sleep(1000);
      }

      CASL_Stack.Initialization();
      EnableWatching = true;

      GenericObject systemInterface = data as GenericObject;

      ProcessAllFiles(systemInterface);
      string watchDirectory = systemInterface.get("watch_directory") as string;

      if (!Directory.Exists(watchDirectory))
      {
        Logger.LogError("Watch Directory does not exist: " + watchDirectory, "ReturnedTender.ProcessAllFiles");
        return;
      }

      // Create a new FileSystemWatcher and set its properties.
      using (FileSystemWatcher watcher = new FileSystemWatcher())
      {
        watcher.Path = watchDirectory;

        // Watch for changes in LastAccess and LastWrite times, and
        // the renaming of files or directories.
        watcher.NotifyFilter = NotifyFilters.FileName;

        // Add event handlers.
        watcher.Changed += (sender, e) => OnWatcherChanged(systemInterface, e);
        watcher.Created += (sender, e) => OnWatcherChanged(systemInterface, e);

        // Begin watching.
        watcher.EnableRaisingEvents = true;

        while (EnableWatching) ;
      }
    }

    /// <summary>
    /// Fire off file processing
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    private static void OnWatcherChanged(object source, FileSystemEventArgs e)
    {
      Logger.LogInfo("Detected change. Beginning returned tender processing of " + e.FullPath, "ReturnedTender.WatcherTriggered");

      GenericObject systemInterface = source as GenericObject;
      Process(e.FullPath, systemInterface);
    }

    /// <summary>
    /// Begin monitoring of configured directory for new/changed files
    /// </summary>
    /// <param name="systemInterface"></param>
    public static void ProcessAllFiles(GenericObject systemInterface)
    {
      string watchDirectory = systemInterface.get("watch_directory") as string;

      if (Directory.Exists(watchDirectory))
      {
        foreach (string filename in Directory.GetFiles(watchDirectory))
        {
          Process(filename, systemInterface);
        }
      }
      else
      {
        Logger.LogError("Watch Directory does not exist: " + watchDirectory, "ReturnedTender.ProcessAllFiles");
      }
    }

    /// <summary>
    /// Delete files in configured directory that have already been processed and are older than a configured number of days
    /// </summary>
    /// <param name="fullPath"></param>
    /// <param name="days"></param>
    private static void DeleteOldFile(string fullPath, int days)
    {
      if (days == 0)
        return;

      DateTime lastDate = File.GetLastWriteTime(fullPath);
      double daysDiff = (DateTime.Now - lastDate).TotalDays;

      if (daysDiff >= days)
      {
        File.Delete(fullPath);
        Logger.LogInfo("Deleted file " + fullPath, "ReturnedTender.DeleteOldFiles");
      }
    }

    /// <summary>
    /// Main function for processing reversal for a provided ERIN file
    /// </summary>
    /// <param name="fullPath"></param>
    /// <param name="systemInterface"></param>
    /// <returns></returns>
    private static bool Process(string fullPath, GenericObject systemInterface)
    {
      ReturnFile file = null;
      List<ReturnEvent> eventsToReverse = new List<ReturnEvent>();

      try
      {
        if (!systemInterface.has("workgroup_id"))
        {
          Logger.LogError("Workgroup ID not configured in system interface", "ReturnedTender.Process");
          return false;
        }
        if (!systemInterface.has("deletion_days"))
        {
          Logger.LogError("Deletion Days not configured in system interface", "ReturnedTender.Process");
          return false;
        }
        if (!systemInterface.has("search_window"))
        {
          Logger.LogError("Search Window not configured in system interface", "ReturnedTender.Process");
          return false;
        }

        string workgroupID = systemInterface.get("workgroup_id") as string;
        int deletionDays = (int)systemInterface.get("deletion_days");
        int searchWindow = (int)systemInterface.get("search_window");

        List<ReturnEvent> events = new List<ReturnEvent>();
      
        Thread.Sleep(1000);
        List<string> lines = new List<string>(File.ReadAllLines(fullPath));

        int numAccountRecords = 0, totalAccountRecords = 0;
        decimal accountAmount = 0.00M, totalAmount = 0.00M;

        // Store file-level information
        file = new ReturnFile(lines[0], Path.GetFileName(fullPath));

        // Skip file if it has been processed. If it hasn't, set the write time so we know to delete it when the time comes
        if (file.AlreadyProcessed())
        {
          Logger.LogInfo("File " + file.FileName + " has already been processed", "ReturnedTender.Process");
          DeleteOldFile(fullPath, deletionDays);
          return false;
        }
        else
        {
          File.SetLastWriteTime(fullPath, DateTime.Now);
        }

        // This thread is not in a session, so we need to create a stub one for TTA mapping to work
        SetMy(workgroupID);

        // Parse file
        for (int i = 1; i < lines.Count; i++)
        {
          string line = lines[i];

          if (line.Length < 2)
            continue;

          string recordType = line.Substring(0, 2);

          if (recordType == "05") // Detail record. Try to find matching tender in iPayment DB
          {
            ++numAccountRecords;
            ++totalAccountRecords;
            ReturnEvent thisEvent = new ReturnEvent(line);
            accountAmount += thisEvent.Amount;
            GetTenderRow(thisEvent, searchWindow, i + 1);

            if (thisEvent.origTENDER != null)
            {
              GetCoreEvent(thisEvent, workgroupID);

              if (thisEvent.OriginalCoreEvent != null)
              {
                GenericObject record = thisEvent.origTENDER;
                string receiptNbr = String.Format("{0}{1}-{2}",
                  record.get("DEPFILENBR"),
                  record.get("DEPFILESEQ").ToString().PadLeft(3, '0'),
                  Convert.ToInt32(record.get("EVENTNBR"))
                );

                if (thisEvent.OriginalCoreEvent.has("reversed_by"))
                {
                  Logger.LogError("Line " + (i + 1) + ": Original event " + receiptNbr + " found, but it has already been reversed (Outcome: -1)", "ReturnedTender.Process");

                  thisEvent.Outcome = -1;
                }
                else
                {
                  GenericObject tenders = thisEvent.OriginalCoreEvent.get("tenders") as GenericObject;
                  if (tenders.getLength() == 1)
                    eventsToReverse.Add(thisEvent);
                  else
                  {
                    Logger.LogError("Line " + (i + 1) + ": Original event " + receiptNbr + " found, but it contains multiple tenders (Outcome: 1)", "ReturnedTender.Process");
                    thisEvent.Outcome = 1;
                  }
                }
              }
              else
              {
                Logger.LogError("Line " + (i + 1) + ": Could not find original tender (Outcome: 3)", "ReturnedTender.Process");
                thisEvent.Outcome = 3;
              }
            }

            events.Add(thisEvent);
          }
          else if (recordType == "08")
          {
            int numChecks = Convert.ToInt32(line.Substring(13, 7));
            decimal amount = Convert.ToDecimal(line.Substring(20, 15)) / 100;

            if (numAccountRecords != numChecks || accountAmount != amount)
            {
              file.SetFailed("Failed to verify details from detail trailer record on line " + (i + 1));
              break;
            }

            numAccountRecords = 0;
            totalAmount += accountAmount;
            accountAmount = 0.00M;
          }
          else if (recordType == "09")
          {
            int numLines = Convert.ToInt32(line.Substring(2, 7));
            int numChecks = Convert.ToInt32(line.Substring(9, 7));
            decimal amount = Convert.ToDecimal(line.Substring(16, 15)) / 100;

            if (numLines != lines.Count || numChecks != totalAccountRecords || amount != totalAmount)
            {

              file.SetFailed("Failed to verify details from file trailer record on line " + (i + 1));
              break;
            }
          }
          else
          {
            file.SetFailed("Unknown record type detected: " + recordType);
            break;
          }
        }

        if (!file.Result)
        {
          PopulateDatabase(file);
          Logger.LogError("Failed to parse file " + file.FileName + ": " + file.ResultDesc, "ReturnedTender.Process");
          return false;
        }

        int reversedCount = 0;

        if (eventsToReverse.Count > 0)
        {
        // Create new Core file
        GenericObject geoFile = misc.CASL_call("_subject", c_CASL.execute("Core_file"), "a_method", "sv_create_new_file", "args", new GenericObject(
          "user", "SYSTEM",
          "creator_id", "SYSTEM",
          "type", "I",
          "description", "ReturnedTenderReversal",
          "effective_date", DateTime.Now.ToString("MM/dd/yyyy h:mm:ss tt"),
          "department", workgroupID,
          "source", c_CASL.execute("Source.of.Cashier"),
          "login_id", "SYSTEM"
        )) as GenericObject;

        file.reversedFilename = geoFile.get("id").ToString();

        // Reverse each event
        for (int i = 0; i < eventsToReverse.Count; i++)
        {
          try
          {
            ReturnEvent thisEvent = eventsToReverse[i];

            GenericObject origTENDER = thisEvent.origTENDER;

            string receiptNbr = String.Format("{0}{1}-{2}",
              origTENDER.get("DEPFILENBR"),
              origTENDER.get("DEPFILESEQ").ToString().PadLeft(3, '0'),
              Convert.ToInt32(origTENDER.get("EVENTNBR"))
            );

            // Create event and file CASL GEOs, using only the minimum number of necessary fields for reversals to work
            GenericObject geoEvent = c_CASL.c_instance("Core_event",
              "created_stamp", new GenericObject("user", "SYSTEM", "datetime", DateTime.Now.ToString("MM/dd/yyyy h:mm:ss tt")),
              "reversal_of", new GenericObject(
                "EVENTNBR", origTENDER.get("EVENTNBR"),
                "DEPFILENBR", origTENDER.get("DEPFILENBR"),
                "DEPFILESEQ", origTENDER.get("DEPFILESEQ"),
                "UpdateReference", String.Format("{0}{1}-{2}", origTENDER.get("DEPFILENBR"), origTENDER.get("DEPFILESEQ").ToString().PadLeft(3, '0'), origTENDER.get("EVENTNBR")),
                "total", Convert.ToDecimal(origTENDER.get("AMOUNT")) * -1M
              ),
              "core_file", geoFile,
              "user_override", "SYSTEM"
            );

            List<GenericObject> toUpdate = new List<GenericObject>();

            // Reverse all transactions
            GenericObject reversedTransactions = new GenericObject();

            bool failedTranReversal = false;
            int cttCount = 0;
            int regTransCount = 0;

            GenericObject transactions = thisEvent.OriginalCoreEvent.get("transactions") as GenericObject;
            for (int j = 0; j < transactions.getLength(); j++)
            {
              GenericObject trans = transactions.get(j) as GenericObject;

                // Bug IPAY-720 MJO - Skip voided transactions
                if (trans.has("voided_stamp"))
                  continue;

                GenericObject coreItems = trans.get("core_items", new GenericObject()) as GenericObject;

              if (coreItems.getLength() > 0)
                cttCount++;
              else if (!misc.base_is_a(trans, c_CASL.execute("Transaction.Tax")))
                regTransCount++;

              if (cttCount > 1 || (cttCount == 1 && regTransCount > 0))
              {
                Logger.LogError("Failed to reverse event " + thisEvent.OriginalCoreEvent.get("receipt_nbr").ToString() + ". A container transaction is present in this event with other transactions.", "ReturnedTender.Process");
                failedTranReversal = true;
                break;
              }

              if (trans.get("is_reversible", "Yes", true).ToString() == "No")
              {
                GenericObject parent = trans.get("_parent") as GenericObject;
                Logger.LogError("Failed to reverse event " + thisEvent.OriginalCoreEvent.get("receipt_nbr").ToString() + ". A transaction of type " + parent.get("_name") + " in this event is configured to not be reversible.", "ReturnedTender.Process");
                failedTranReversal = true;
                break;
              }

              // Bug 25457 MJO - Have to manually add RETURN_DT everywhere
              // Bug 25670 MJO - Added additional fields
              trans.set(
                "RETURN_DT", file.CheckReturnDate,
                "REMT_ADJ_STATUSCODE", "DO",
                "RET_CD", thisEvent.Reason.ToString(),
                "RET_REAS", thisEvent.GetReasonDesc()
              );

              if (!(trans.get("_custom_keys") as GenericObject).ContainsValue("RETURN_DT"))
                (trans.get("_custom_keys") as GenericObject).insert("RETURN_DT");

              if (!(trans.get("_custom_keys") as GenericObject).ContainsValue("REMT_ADJ_STATUSCODE"))
                (trans.get("_custom_keys") as GenericObject).insert("REMT_ADJ_STATUSCODE");

              if (!(trans.get("_custom_keys") as GenericObject).ContainsValue("RET_CD"))
                (trans.get("_custom_keys") as GenericObject).insert("RET_CD");

              if (!(trans.get("_custom_keys") as GenericObject).ContainsValue("RET_REAS"))
                (trans.get("_custom_keys") as GenericObject).insert("RET_REAS");

              GenericObject blockTransactions = trans.get("core_items", new GenericObject()) as GenericObject;
              int numBlocks = blockTransactions.getIntegerKeys().Count;

              for (int k = 0; k < numBlocks; k++)
              {
                GenericObject blockTransaction = blockTransactions.get(k) as GenericObject;

                blockTransaction.set("RETURN_DT", file.CheckReturnDate);

                if (!(blockTransaction.get("_custom_keys") as GenericObject).ContainsValue("RETURN_DT"))
                  (blockTransaction.get("_custom_keys") as GenericObject).insert("RETURN_DT");
              }

              GenericObject reversedTran = misc.CASL_call("_subject", trans, "a_method", "reverse", "args", new GenericObject()) as GenericObject;

              reversedTran.set("source_ref_id", null);

              geoEvent.insert(reversedTran);
              reversedTransactions.insert(reversedTran);
              toUpdate.Add(reversedTran);
            }

            if (failedTranReversal)
            {
              Logger.LogError("Line " + i + ": Original event " + receiptNbr + " found, but transaction reversal failed (Outcome: -1)", "ReturnedTender.Process");
              thisEvent.Outcome = -1;
              continue;
            }

            // Reverse the tender
            GenericObject tender = (thisEvent.OriginalCoreEvent.get("tenders") as GenericObject).get(0) as GenericObject;

            if (tender.get("is_reversible", "Yes", true).ToString() == "No")
            {
              GenericObject parent = tender.get("_parent") as GenericObject;
              Logger.LogError("Line " + i + ": Original event " + receiptNbr + " found, but tender type " + parent.get("_name") + " is configured to not be reversible (Outcome: -1)", "ReturnedTender.Process");
              failedTranReversal = true;
              thisEvent.Outcome = -1;
              break;
            }

            GenericObject reversedTender = misc.CASL_call("_subject", tender, "a_method", "reverse", "args", new GenericObject()) as GenericObject;

            geoEvent.insert(reversedTender);
            toUpdate.Add(reversedTender);

            geoEvent.set(
              "transactions", reversedTransactions,
              "tenders", new GenericObject(0, reversedTender)
            );

            // Make TTA mapping
            GenericObject ttaMapping = misc.CASL_call("_subject", geoEvent, "a_method", "make_tta_mapping", "args", new GenericObject()) as GenericObject;
            misc.CASL_call("_subject", geoEvent, "a_method", "apply_tta_mapping", "args", new GenericObject("tta_mapping", ttaMapping));

            misc.CASL_call("_subject", geoEvent, "a_method", "apply_tta_mapping", "args", new GenericObject("tta_mapping", ttaMapping));

            // Process system interfaces, including posting to iPayment DB
            object result = misc.CASL_call("_subject", geoEvent, "a_method", "process_system_interfaces", "args", new GenericObject());

            if (result is GenericObject && misc.base_is_a(result, c_CASL.execute("error")))
            {
              GenericObject geoResult = result as GenericObject;
              string message = "";

              if (geoResult.has("message"))
                message = geoResult.get("message").ToString();
              else if (geoResult.has("summary"))
                message = geoResult.get("summary").ToString() + ", " + geoResult.get("detail", "").ToString();
              Logger.LogError("Line " + i + ": Failed to post reversal event of event " + receiptNbr + " (Outcome: -1)" + Environment.NewLine + "Message: " + message, "ReturnedTender.Process");
              thisEvent.Outcome = -1;
              continue;
            }

            thisEvent.reversedEventNbr = Int32.Parse(geoEvent.get("receipt_nbr").ToString().Split('-')[1]);

            // Post TTA mapping
            ttaMapping.set("_container", geoEvent);

            misc.CASL_call("_subject", ttaMapping, "a_method", "post", "args", new GenericObject());

            thisEvent.OriginalCoreEvent.set("reversed_by", new GenericObject(
              "EVENTNBR", thisEvent.reversedEventNbr,
              "DEPFILENBR", file.reversedFilename.Substring(0, 7),
              "DEPFILESEQ", file.reversedFilename.Substring(7),
              "MOD_DT", (geoEvent.get("created_stamp") as GenericObject).get("datetime")
            ));

            TranSuiteServices.CoreEventReversal.PostReversalEvent(
              "EVENTNBR", thisEvent.reversedEventNbr,
              "DEPFILENBR", file.reversedFilename.Substring(0, 7),
              "DEPFILESEQ", file.reversedFilename.Substring(7),
              "USERID", "SYSTEM",
              "REVERSED_EVENTNBR", origTENDER.get("EVENTNBR"),
              "REVERSED_DEPFILENBR", origTENDER.get("DEPFILENBR"),
              "REVERSED_DEPFILESEQ", origTENDER.get("DEPFILESEQ"),
              "VOIDED", false,
              "old_event", thisEvent.OriginalCoreEvent,
              "is_partial_reversal", false,
              "data", c_CASL.c_object("TranSuite_DB.Data.db_connection_info")
            );

            reversedCount++;
          }
          catch (Exception e)
          {
            ReturnEvent thisEvent = eventsToReverse[i];
            thisEvent.Outcome = -1;

            if (e is CASL_error)
              Logger.LogError("Failed to reverse " + eventsToReverse[i].TenderData + Environment.NewLine + (string)(e as CASL_error).casl_object.get("message"), "ReturnedTender.Process");
            else
              Logger.LogError("Failed to reverse " + eventsToReverse[i].TenderData + Environment.NewLine + e.ToMessageAndCompleteStacktrace(), "ReturnedTender.Process");
          }
        }

        var balanceResult = misc.CASL_call("_subject", c_CASL.execute("Business"), "a_method", "auto_balance_core_file_aux", "args", new GenericObject(
          "core_file_id", file.reversedFilename
        ));

        if (balanceResult is CASL_error)
        {
          Logger.LogError("Failed to balance and close file " + file.reversedFilename + Environment.NewLine + (string)(balanceResult as CASL_error).casl_object.get("message"), "ReturnedTender.Process");
        }
        }

        // Post the results to the database
        if (PopulateDatabase(file, events))
        {
          Logger.LogInfo("Completed processing file " + file.FileName + ". " + reversedCount + " events reversed.", "ReturnedTender.Process");
          return true;
        }
        else
        {
          Logger.LogError("Failed to process file " + file.FileName + ". Could not post records to the database.", "ReturnedTender.Process");
          file.SetFailed("Could not post records to the database.");
          PopulateDatabase(file, eventsToReverse);
          return false;
        }
      }
      catch (Exception e)
      {
        Logger.LogError("Processing failed for file " + fullPath + ": " + e.ToMessageAndCompleteStacktrace(), "ReturnedTender Process");

        // Bug 26205 MJO - Record exceptions in the import table
        if (file == null)
        {
          file = new ReturnFile();
          file.FileName = Path.GetFileName(fullPath);
        }
        
        file.SetFailed($"Exception: {e.Message}");
        PopulateDatabase(file, eventsToReverse);

        return false;
      }
      finally
      {
        CASLInterpreter.no_session_my = null;
      }
    }

    /// <summary>
    /// Try to find a matching tender in iPayment
    /// </summary>
    private static void GetTenderRow(ReturnEvent theEvent, int searchWindow, int lineNbr)
    {
      String connectionString = (String)(c_CASL.c_object("TranSuite_DB.Data.db_connection_info") as GenericObject).get("db_connection_string");
      Hashtable sqlArgs = new Hashtable();

      // Bug 27491: FT: I don't like the CAST of the POSTDT and SUBSTRING on CC_CK_NBR; these will do a conversion
      // Bug 27491: FT: This is a slow query and it needs an index on the amount, at least
      string sql = @"select * from TG_TENDER_DATA 
        where SUBSTRING(CC_CK_NBR, PATINDEX('%[^0]%', CC_CK_NBR+'.'), LEN(CC_CK_NBR))=@checknbr 
        and BANKROUTINGNBR=@routingNbr 
        and CAST(POSTDT as DATE) >= CAST(@startDate as DATE) 
        and CAST(POSTDT as DATE) < CAST(@endDate as DATE) 
        and AMOUNT=ROUND(@amount, 2) and VOIDDT is null";

      List<Object[]> rows = new List<object[]>();
      List<string> columns = new List<string>();

      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        using (SqlCommand command = new SqlCommand(sql))
        {
          // Bug 27491: FT: Type the parameters
          command.Parameters.Add("@checknbr", SqlDbType.VarChar).Value = theEvent.CheckNumber; // Bug 27491: Also changed checkNbr to checknbr
          command.Parameters.Add("@routingNbr", SqlDbType.VarChar).Value = theEvent.RoutingNumber;
          command.Parameters.Add("@startDate", SqlDbType.DateTime).Value = theEvent.Date.AddDays(searchWindow * -1).Date;
          command.Parameters.Add("@endDate", SqlDbType.DateTime).Value = theEvent.Date.AddDays(1).Date;
          command.Parameters.Add("@amount", SqlDbType.Float).Value = (float) theEvent.Amount;
          // End Bug 27491
          command.Connection = connection;
          connection.Open();

          using (SqlDataReader reader = command.ExecuteReader())
          {
            int acctNbrRow = -1;

            for (int i = 0; i < reader.FieldCount; i++)
            {
              columns.Add(reader.GetName(i));

              if (reader.GetName(i) == "BANKACCTNBR")
                acctNbrRow = i;
            }

            while (reader.Read())
            {
              object[] row = new object[reader.FieldCount];

              reader.GetValues(row);

              string bankAcctNbr = row[acctNbrRow].ToString();
              byte[] content_bytes = Convert.FromBase64String(bankAcctNbr);
              byte[] decrypted_bytes = Crypto.symmetric_decrypt("data", content_bytes, "secret_key", Crypto.get_secret_key());
              bankAcctNbr = System.Text.Encoding.Default.GetString(decrypted_bytes).TrimStart('0');

              if (bankAcctNbr == theEvent.AccountNumber)
              rows.Add(row);
            }
          }
        }
      }

      if (rows.Count == 1)
      {
        GenericObject record = new GenericObject();

        for (int i = 0; i < columns.Count; i++)
          record.set(columns[i], rows[0][i]);

        theEvent.origTENDER = record;
      }
      else if (rows.Count > 1)
      {
        Logger.LogError("Line " + lineNbr + ": Multiple tenders found that match the item (Outcome: 2)", "ReturnedTender.GetTenderRow");
        theEvent.Outcome = 2;
      }
      else
      {
        Logger.LogError("Line " + lineNbr + ": No match was found (Outcome: 3)", "ReturnedTender.GetTenderRow");
        theEvent.Outcome = 3;
      }
    }

    /// <summary>
    /// Get the original event GEO using the tender database row
    /// </summary>
    /// <param name="theEvent"></param>
    /// <param name="workgroupID"></param>
    private static void GetCoreEvent(ReturnEvent theEvent, string workgroupID)
    {
      GenericObject record = theEvent.origTENDER;
      string fileNbr = String.Format("{0}{1}", record.get("DEPFILENBR"), record.get("DEPFILESEQ").ToString().PadLeft(3, '0'));
      int eventNbr = Convert.ToInt32(record.get("EVENTNBR"));

      GenericObject geoEvent = misc.CASL_call("_subject", c_CASL.execute("Core_event"), "a_method", "get_by_core_file_id_and_id", "args", new GenericObject(
        "core_file_id", fileNbr,
        "id", eventNbr
      )) as GenericObject;
      theEvent.OriginalCoreEvent = geoEvent;
    }

    private static void LogResult(ReturnFile file)
    {
      Logger.cs_log("Returned tender processing completed for file " + file.FileName + ". Result: " + (file.Result ? "Success" : "Failure"));
    }

    /// <summary>
    /// Post import result to database
    /// </summary>
    /// <param name="file"></param>
    /// <returns></returns>
    private static bool PopulateDatabase(ReturnFile file)
    {
      return PopulateDatabase(file, new List<ReturnEvent>());
    }

    /// <summary>
    /// Post import and reversal results to database
    /// </summary>
    /// <param name="file"></param>
    /// <param name="reversedEvents"></param>
    /// <returns></returns>
    private static bool PopulateDatabase(ReturnFile file, List<ReturnEvent> reversedEvents)
    {
      if (!InsertImportData(file))
        return false;

      foreach (ReturnEvent reversedEvent in reversedEvents)
      {
        InsertDetailData(file, reversedEvent);
      }

      return true;
    }

    /// <summary>
    /// Insert import row into TG_RETURNED_TENDER_IMPORT_DATA
    /// </summary>
    /// <param name="file"></param>
    /// <param name="reversedEvents"></param>
    /// <returns></returns>
    private static bool InsertImportData(ReturnFile file)
    {
      String connectionString = (String)(c_CASL.c_object("TranSuite_DB.Data.db_connection_info") as GenericObject).get("db_connection_string");

      string sql =
        @"INSERT INTO TG_RETURNED_TENDER_IMPORT_DATA 
        (FILENAME, CREATEDT, IMPORTDT, USERID, RESULT, RESULTDESC)
        OUTPUT INSERTED.PKID
        VALUES 
        (@filename, @createdt, @importdt, @userid, @result, @resultdesc)";

      try
      {
        using (SqlConnection connection = new SqlConnection(connectionString))
        using (SqlCommand command = new SqlCommand(sql))
        {
          SqlParameter outputPKIDParam = new SqlParameter("@PKID", SqlDbType.Int)
          {
            Direction = ParameterDirection.Output
          };

          command.Parameters.Add(outputPKIDParam);

          // Bug 27491: AddWithValue is probably OK for INSERT, but I don't think it is optimal
          command.Parameters.AddWithValue("filename", file.FileName);
          command.Parameters.AddWithValue("createdt", file.CreateDate == null ? DateTime.Now.ToString() : file.CreateDate + " " + file.CreateTime); // Bug 26205 MJO - Handle null
          command.Parameters.AddWithValue("importdt", DateTime.Now.ToString());
          command.Parameters.AddWithValue("userid", "SYSTEM");
          command.Parameters.AddWithValue("result", file.Result ? "SUCCESS" : "FAILURE");
          command.Parameters.AddWithValue("resultdesc", file.ResultDesc);

          command.Connection = connection;
          command.CommandType = CommandType.Text;

          connection.Open();

          file.PKID = (int)command.ExecuteScalar();
        }
      }
      catch (Exception e)
      {
        Logger.LogError("Failed to insert import data for " + file.FileName + ": " + e.Message, "ReturnedTender.InsertImportedData");
        return false;
      }

      return true;
    }

    /// <summary>
    /// Insert reversal or other result into TG_RETURNED_TENDER_DETAIL_DATA
    /// </summary>
    /// <param name="file"></param>
    /// <param name="reversedEvent"></param>
    /// <returns></returns>
    private static bool InsertDetailData(ReturnFile file, ReturnEvent reversedEvent)
    {
      String connectionString = (String)(c_CASL.c_object("TranSuite_DB.Data.db_connection_info") as GenericObject).get("db_connection_string");

      string sql =
        @"INSERT INTO TG_RETURNED_TENDER_DETAIL_DATA 
        (IMPORTPKID, TNDRDT, AMOUNT, TNDRTYPE, TNDRDATA, BANKROUTINGNBR, BANKACCTNBR, CHECKNBR, REASONCODE, REASONDESC,
        OUTCOME, ORIGDEPFILENBR, ORIGDEPFILESEQ, ORIGEVENTNBR, ORIGTNDRNBR, REVERSALDEPFILENBR, REVERSALDEPFILESEQ, REVERSALEVENTNBR, REVERSALTNDRNBR)
        OUTPUT INSERTED.PKID
        VALUES 
        (@importpkid, @tndrdt, @amount, @tndrtype, @tndrdata, @bankroutingnbr, @bankacctnbr, @checknbr, @reasoncode, @reasondesc,
        @outcome, @origdepfilenbr, @origdepfileseq, @origeventnbr, @origtndrnbr, @reversaldepfilenbr, @reversaldepfileseq, @reversaleventnbr, @reversaltndrnbr)";

      try
      {
        using (SqlConnection connection = new SqlConnection(connectionString))
        using (SqlCommand command = new SqlCommand(sql))
        {
          command.Parameters.AddWithValue("@importpkid", file.PKID);
          command.Parameters.AddWithValue("@tndrdt", reversedEvent.Date);
          command.Parameters.AddWithValue("@amount", reversedEvent.Amount);
          command.Parameters.AddWithValue("@tndrtype", "CHECK");
          command.Parameters.AddWithValue("@tndrdata", reversedEvent.TenderData);
          command.Parameters.AddWithValue("@bankroutingnbr", reversedEvent.RoutingNumber);
          command.Parameters.AddWithValue("@bankacctnbr", reversedEvent.AccountNumber);
          command.Parameters.AddWithValue("@checknbr", reversedEvent.CheckNumber);
          command.Parameters.AddWithValue("@reasoncode", reversedEvent.Reason);
          command.Parameters.AddWithValue("@reasondesc", reversedEvent.GetReasonDesc());
          command.Parameters.AddWithValue("@outcome", reversedEvent.Outcome);

          if (reversedEvent.Outcome < 2)
          {
            command.Parameters.AddWithValue("@origdepfilenbr", reversedEvent.origTENDER.get("DEPFILENBR"));
            command.Parameters.AddWithValue("@origdepfileseq", reversedEvent.origTENDER.get("DEPFILESEQ"));
            command.Parameters.AddWithValue("@origeventnbr", reversedEvent.origTENDER.get("EVENTNBR"));
            command.Parameters.AddWithValue("@origtndrnbr", reversedEvent.origTENDER.get("TNDRNBR"));
          }
          else
          {
            command.Parameters.AddWithValue("@origdepfilenbr", DBNull.Value);
            command.Parameters.AddWithValue("@origdepfileseq", DBNull.Value);
            command.Parameters.AddWithValue("@origeventnbr", DBNull.Value);
            command.Parameters.AddWithValue("@origtndrnbr", DBNull.Value);
          }

          if (reversedEvent.Outcome == 0)
          {
            int depfilenbr = Convert.ToInt32(file.reversedFilename.Substring(0, file.reversedFilename.Length - 3));
            int depfileseq = Convert.ToInt32(file.reversedFilename.Substring(file.reversedFilename.Length - 3));

            command.Parameters.AddWithValue("@reversaldepfilenbr", depfilenbr);
            command.Parameters.AddWithValue("@reversaldepfileseq", depfileseq);
            command.Parameters.AddWithValue("@reversaleventnbr", reversedEvent.reversedEventNbr);
            command.Parameters.AddWithValue("@reversaltndrnbr", 1);
          }
          else
          {
            command.Parameters.AddWithValue("@reversaldepfilenbr", DBNull.Value);
            command.Parameters.AddWithValue("@reversaldepfileseq", DBNull.Value);
            command.Parameters.AddWithValue("@reversaleventnbr", DBNull.Value);
            command.Parameters.AddWithValue("@reversaltndrnbr", DBNull.Value);
          }

          command.Connection = connection;
          command.CommandType = CommandType.Text;

          connection.Open();

          if (command.ExecuteNonQuery() != 1)
            throw new Exception("Could not insert row into table");
        }
      }
      catch (Exception e)
      {
        Logger.LogError("This tender has been reversed, but failed to insert import data into the database." + Environment.NewLine + reversedEvent.TenderData + Environment.NewLine + e.Message, "ReturnedTenderInsertImportedData");
        return false;
      }

      return true;
    }

    /// <summary>
    /// Create a stub "my" to allow reversal to work without a proper session
    /// </summary>
    /// <param name="workgroupID"></param>
    private static void SetMy(string workgroupID)
    {
      GenericObject my = c_CASL.c_instance("My");
      my.set("login_department", workgroupID);

      CASLInterpreter.no_session_my = new ThreadLocal<GenericObject>();
      CASLInterpreter.no_session_my.Value = my;
    }

    /// <summary>
    /// Hold ERIN file-level information
    /// </summary>
    private class ReturnFile
    {
      public string CreateDate { get; } = null;
      public string CreateTime { get; } = null;
      public string FileName { get; set; }
      public bool Result { get; set; } = true;
      public string ResultDesc { get; set; } = "";
      public int PKID { get; set; }
      public string reversedFilename { get; set; }
      public string CheckReturnDate { get; set; } // Bug 25457 MJO - Added RETURN_DT

      public ReturnFile() { }

      /// <summary>
      /// Parse "01" header line
      /// </summary>
      /// <param name="line"></param>
      /// <param name="filename"></param>
      public ReturnFile(string line, string filename)
      {
        CreateDate = line.Substring(21, 8);
        CreateTime = String.Format("{0}:{1}", line.Substring(29, 2), line.Substring(31, 2));
        FileName = filename;

        // Bug 25457 MJO - Added RETURN_DT
        string CheckReturnDateRaw = line.Substring(98, 8);
        CheckReturnDate = $"{CheckReturnDateRaw.Substring(0, 2)}/{CheckReturnDateRaw.Substring(2, 2)}/{CheckReturnDateRaw.Substring(4)}";
      }

      public void SetFailed(string resultDesc)
      {
        Result = false;
        ResultDesc = resultDesc;
      }

      /// <summary>
      /// Check to see if this file has already been processed
      /// </summary>
      /// <returns></returns>
      public bool AlreadyProcessed()
      {
        String connectionString = (String)(c_CASL.c_object("TranSuite_DB.Data.db_connection_info") as GenericObject).get("db_connection_string");

        string sql = "select * from TG_RETURNED_TENDER_IMPORT_DATA where FILENAME=@filename and CREATEDT=@createdt";

        try
        {
          using (SqlConnection connection = new SqlConnection(connectionString))
          {
            using (SqlCommand command = new SqlCommand(sql))
            {
              command.Parameters.Add("@filename", SqlDbType.VarChar).Value = FileName;      // Bug 27491: Specify VarChar to prevent promotion to nvarchar
              command.Parameters.AddWithValue("@createdt", CreateDate + " " + CreateTime);  // Bug 27491: Hmmm, I guess this will work. I don't like it, but it should work
              command.Connection = connection;
              command.CommandType = CommandType.Text;

              connection.Open();

              using (SqlDataReader reader = command.ExecuteReader())
              {
                return reader.HasRows;
              }
            }
          }
        }
        catch (Exception e)
        {
          Logger.LogError("Failed to query for previously processed file: " + e.Message + ". Defaulting to already processed.", "ReturnedTender.AlreadyProcessed");
          return true;
        }
      }
    }

    /// <summary>
    /// Holds information for each return tender and associated event, if any
    /// </summary>
    private class ReturnEvent
    {
      public string CheckNumber { get; }
      public string RoutingNumber { get; }
      public string AccountNumber { get; }
      public decimal Amount { get; }
      public DateTime Date { get; }
      public int Reason { get; }
      public int Outcome { get; set; } = 0;
      public GenericObject origTENDER { get; set; } = null;
      public GenericObject OriginalCoreEvent { get; set; } = null;
      public string TenderData { get; }
      public int reversedEventNbr { get; set; }

      private static ConcurrentDictionary<int, string> ReasonDesc = null;

      private ReturnEvent() { }

      /// <summary>
      /// Parse "05" detail line
      /// </summary>
      /// <param name="line"></param>
      public ReturnEvent(string line)
      {
        Amount = Convert.ToDecimal(line.Substring(42, 11)) / 100;
        CheckNumber = line.Substring(2, 11).TrimStart('0');
        RoutingNumber = line.Substring(13, 9);
        Reason = Int32.Parse(line.Substring(108, 2));
        AccountNumber = line.Substring(22, 15).TrimStart('0');

        string century = DateTime.Now.Year.ToString().Substring(0, 2);

        string[] dateSplit = line.Substring(100, 8).Split('/');
        Date = new DateTime(Convert.ToInt32(century + dateSplit[2]), Convert.ToInt32(dateSplit[0]), Convert.ToInt32(dateSplit[1]));

        // Mask account number
        TenderData = line.Substring(0, 22) + "".PadLeft(11, '*') + line.Substring(33);
      }

      private static object lockObj = new object();

      /// <summary>
      /// Get description associated with the reason code
      /// </summary>
      /// <returns></returns>
      public string GetReasonDesc()
      {
        lock (lockObj)
        {
          if (ReasonDesc == null)
          {
            string[] csv = File.ReadAllLines(misc.CASL_get_code_base() + "\\baseline\\system_interface\\resources\\ERINReasonDescriptions.csv");

            ReasonDesc = new ConcurrentDictionary<int, string>();

            foreach (string line in csv)
            {
              if (line.Trim().Length == 0)
                continue;

              string[] split = line.Split(',');

              ReasonDesc[Int32.Parse(split[0])] = split[1];
            }
          }
        }

        if (!ReasonDesc.ContainsKey(Reason))
          return "UNKNOWN REASON";

        return ReasonDesc[Reason];
      }
    }
  }
}