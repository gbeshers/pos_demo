using System;
using System.Collections;
using CASL_engine;
using System.Linq;
using System.Xml.Linq;
using TranSuiteServices;
using System.Data;
using System.Collections.Concurrent;
using System.Collections.Specialized;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Threading;

namespace baseline
{
  /// <summary>
  /// EpicSI is the system interface that fires and updates the CORE_TRANSFER status so that CoreDirect
  /// can return the proper status back to Epic.
  ///</summary>
  public class EpicSI
  {
    // We use this to avoid unnecessary database checks for CoreDirect polling of the transfer status
    private static ConcurrentDictionary<String, GenericObject> _statuses = new ConcurrentDictionary<String, GenericObject>();

    public EpicSI()
    {
    }

    /// <summary>
    /// Handle the update
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static GenericObject CASL_EpicUpdate(params object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject subject = args.get("_subject") as GenericObject; 
      string ref_id = args.get("REFERENCE_ID", "").ToString();
      string receipt_ref = args.get("RECEIPT_REF", "").ToString();
      string tender_type = args.get("TENDER_TYPE", "").ToString();
      string tender_info = args.get("TENDER_INFO", "").ToString();

      GenericObject db_info = (GenericObject)args.get("db_connection_info");
      return EpicUpdate(ref_id, receipt_ref, tender_type, tender_info, db_info);
    }

    /// <summary>
    /// Handle the update
    /// </summary>
    /// <param name="ref_id">reference id from CORE_TRANSFER table</param>
    /// <param name="receipt_ref">iPayment receipt reference number. Example: 2017191001-5</param>
    /// <param name="tender_type"></param>
    /// <param name="tender_info"></param>
    /// <param name="db_info"></param>
    /// <returns></returns>
    public static GenericObject EpicUpdate(string ref_id, string receipt_ref, string tender_type, string tender_info, GenericObject db_info)
    {
#if DEBUG
      Logger.cs_log(
        String.Format("epicsi: EpicUpdate ref_id: '{0}', receipt_ref: '{1}', tender_type: '{2}', tender_info: '{3}'", 
          ref_id, receipt_ref, tender_type, tender_type));
#endif
      GenericObject geoReturn = new GenericObject();

      string strError = "";
      string strSQL = "";
      if (ref_id == "")
      {
        string strErrorMsg = "Cannot Update EPIC System.";
        GenericObject geoError = (GenericObject)c_CASL.c_make("error", "code", "EPICUPDATE-1CS", "message", strErrorMsg, 
          "title", "epic error", "throw", false);
        return geoError;
      }

      // validate if payment/refund request is still active 
      strSQL = "SELECT STATUS, TRAN_DT, REQ_TYPE FROM CORE_TRANSFER WHERE REFERENCE_ID=@reference_id";
      GenericObject geoInquiryData = GetRecords(db_info, strSQL, ref_id);
      if (!geoInquiryData.has(0)) // not found
      {
        string strErrorMsg = "Payment Not Found in EPIC System.";
        GenericObject geoError = (GenericObject)c_CASL.c_make("error", "code", "EPICUPDATE-2CS", "message", strErrorMsg,
          "title", "epic error", "throw", false);
        return geoError;
      }
      geoReturn = (GenericObject)geoInquiryData.get(0); // return single record
      string status = (string)geoReturn.get("STATUS", "");
      if (status == "CANCELED" || status == "VOIDED")
      {
        string strErrorMsg = "EPIC Payment Request is already canceled.";
        GenericObject geoError = (GenericObject)c_CASL.c_make("error", "code", "EPICUPDATE-3CS", "message", strErrorMsg,
          "title", "epic error", "throw", false);
        return geoError;
      }

      // Connect to database.
      ConnectionTypeSynch pDBConn = new ConnectionTypeSynch(db_info);
      string err = null;
      strSQL = @"UPDATE CORE_TRANSFER SET STATUS=@status, RECEIPT_REF=@receipt_ref, TENDER_TYPE=@tender_type, TENDER_INFO=@tender_info WHERE REFERENCE_ID=@reference_id";
      HybridDictionary sql_args = new HybridDictionary();
      sql_args["reference_id"] = ref_id;
      sql_args["receipt_ref"] = receipt_ref;
      sql_args["tender_type"] = tender_type;
      sql_args["tender_info"] = tender_info;
      sql_args["status"] = "COMPLETED";

      if (!pDBConn.EstablishDatabaseConnection(ref err) || err != null)
        throw CASL_error.create("message", "Error opening database connection.", "code", "EPICUPDATE-4CS");


      int count = ParamUtils.runExecuteNonQuery(pDBConn, strSQL, sql_args);
      if (count != 1)
      {
        throw CASL_error.create("message", "Error executing SQL. Unable to Update Epic Table CORE_TRANSFER" + strError,
          "code", "EPICUPDATE-5CS");
      }

      SetStatusChanged(ref_id, "COMPLETED", receipt_ref, tender_type, tender_info, geoReturn);
      UpdateReference(geoReturn);

      return geoReturn;
    }

    /// <summary>
    /// Update CORE_TRANSFER table on transaction post.
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static GenericObject CASL_EpicPreUpdate(params object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      string ref_id = args.get("REFERENCE_ID", "").ToString();
      string receipt_ref = args.get("RECEIPT_REF", "").ToString();
      DateTime dt = DateTime.Now;
      string strTodayDateTime = dt.ToString("MM/dd/yyyy HH:mm:ss");

      string tran_dt = args.get("TRAN_DT", strTodayDateTime).ToString();
      GenericObject db_info = (GenericObject)args.get("db_connection_info");

      return EpicPreUpdate(ref_id, tran_dt, receipt_ref, db_info);
    }

    /// <summary>
    /// update CORE_TRANSFER table
    /// </summary>
    /// <param name="ref_id"></param>
    /// <param name="tran_dt"></param>
    /// <param name="receipt_ref"></param>
    /// <param name="db_info"></param>
    /// <returns></returns>
    public static GenericObject EpicPreUpdate(String ref_id, String tran_dt, String receipt_ref, GenericObject db_info)
    {
#if DEBUG
      Logger.cs_log(String.Format("epicsi: EpicPreUpdate ref_id: '{0}', tran_dt: '{1}, receipt_ref: '{2}'", ref_id, tran_dt, receipt_ref));
#endif
      GenericObject geoReturn = null;

      string strError = "";
      string strSQL = "";

      if (ref_id == "") // update by reference_id
      {
        string strErrorMsg = "Can not Update EPIC System.";
        GenericObject geoError = (GenericObject)c_CASL.c_make("error", "code", "EPICPREUPDATE-1CS", "message", strErrorMsg, 
          "title", "epic error", "throw", false);
        return geoError;
      }

      // validate if payment/refund request is still active 
      strSQL = "SELECT REFERENCE_ID, STATUS, TRAN_DT, TENDER_TYPE, TENDER_INFO FROM CORE_TRANSFER WHERE REFERENCE_ID=@reference_id";
      GenericObject geoInquiryData = GetRecords(db_info, strSQL, ref_id);
      if (!geoInquiryData.has(0)) // not found
      {
        string strErrorMsg = "Payment Not Found in EPIC System.";
        GenericObject geoError = (GenericObject)c_CASL.c_make("error", "code", "EPICPREUPDATE-2CS", "message", strErrorMsg, 
          "title", "epic error", "throw", false);
        return geoError;
      }
      geoReturn = (GenericObject)geoInquiryData.get(0); // return single record
      string STATUS = (string)geoReturn.get("STATUS", "");

      if (STATUS == "CANCELED" || STATUS == "VOIDED")
      {
        throw CASL_error.create("message", "EPIC Payment Request is already canceled. Can not post it in iPayment.",
          "code", "EPICPREUPDATE-3CS");
      }

      ConnectionTypeSynch pDBConn = null;
      pDBConn = new ConnectionTypeSynch(db_info);
      string err = null;

      HybridDictionary sql_args = new HybridDictionary();
      strSQL = @"UPDATE CORE_TRANSFER SET STATUS=@status, TRAN_DT=@tran_dt, RECEIPT_REF=@receipt_ref  WHERE REFERENCE_ID=@reference_id";
      sql_args["reference_id"] = ref_id;
      sql_args["tran_dt"] = tran_dt;
      sql_args["status"] = "POSTED";
      sql_args["receipt_ref"] = receipt_ref;
      geoReturn.set("TRAN_DT", tran_dt);
      geoReturn.set("RECEIPT_REF", receipt_ref); 

      if (!pDBConn.EstablishDatabaseConnection(ref err) || err != null)
        throw CASL_error.create("message", "Error opening database connection.", "code", "EPICPREUPDATE-4CS");

      int count = ParamUtils.runExecuteNonQuery(pDBConn, strSQL, sql_args);
      if (count != 1)
      {
        throw CASL_error.create("message", "Error executing SQL. Unable to Update Epic Table CORE_TRANSFER" + strError,
          "code", "EPICPREUPDATE-5CS");
      }

      SetStatusChanged(ref_id, "POSTED", receipt_ref, "", "", geoReturn);
      UpdateReference(geoReturn);

      return geoReturn;
    }

    /// <summary>
    /// Set the statuschanged value
    /// </summary>
    /// <param name="REFERENCE_ID">reference id from CORE_TRANSFER table</param>
    /// <param name="status"></param>
    /// <param name="receipt_ref">iPayment receipt reference number. Example: 2017191001-5</param>
    /// <param name="TENDER_TYPE"></param>
    /// <param name="TENDER_INFO"></param>
    /// <param name="geoDates"></param>
    /// <param name="statusChanged"></param>
    public static void SetStatusChanged(String REFERENCE_ID, String status, String receipt_ref, String TENDER_TYPE, String TENDER_INFO, GenericObject statusChanged)
    {
      statusChanged.set("REFERENCE_ID", REFERENCE_ID);
      statusChanged.set("STATUS", status);
      statusChanged.set("RECEIPT_REF", receipt_ref);
      statusChanged.set("TENDER_TYPE", TENDER_TYPE);
      statusChanged.set("TENDER_INFO", TENDER_INFO);

      if (!String.IsNullOrWhiteSpace(receipt_ref) && 
          (status == "COMPLETED" ||status == "CANCELED" || status == "VOIDED"))
      {
        // integrate in point response
        PointCache.IntegratePointResponseIntoStatus(receipt_ref, statusChanged);
      }
    }

    /// <summary>
    /// Handle a cancel
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static GenericObject CASL_EpicCancel(params object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      string ref_id = args.get("REFERENCE_ID", "").ToString();
      string receipt_ref = args.get("RECEIPT_REF", "").ToString();
      GenericObject db_info = (GenericObject)args.get("db_connection_info");

      return EpicCancel(ref_id, receipt_ref, db_info);
    }

    /// <summary>
    /// Handle a cancel
    /// </summary>
    /// <param name="ref_id"></param>
    /// <param name="receipt_ref"></param>
    /// <param name="db_info"></param>
    /// <returns> error or GEO </returns>
    public static GenericObject EpicCancel(string ref_id, string receipt_ref, GenericObject db_info)
    {
#if DEBUG
      Logger.cs_log(String.Format("epicsi: EpicCancel ref_id: '{0}', receipt_ref: '{1}'", ref_id, receipt_ref));
#endif
      GenericObject geoReturn = null;
      string strError = "";
      string strSQL = "";

      if (ref_id == "") // update by reference_id
      {
        string strErrorMsg = "Can not Cancel Payment in EPIC System.";
        GenericObject geoError = (GenericObject)c_CASL.c_make("error", "code", "EPICCANCEL-1CS", "message", strErrorMsg, 
          "title", "epic error", "throw", false);
        return geoError;
      }
      // validate if payment/refund request is still active 
      strSQL = "SELECT REFERENCE_ID, REQ_TYPE, STATUS, RECEIPT_REF, TRAN_DT, TENDER_TYPE, TENDER_INFO FROM CORE_TRANSFER WHERE REFERENCE_ID=@reference_id";
      GenericObject geoInquiryData = GetRecords(db_info, strSQL, ref_id);
      if (!geoInquiryData.has(0)) // not found
      {
        string strErrorMsg = "Canceled Payment Not Found in EPIC System.";
        GenericObject geoError = (GenericObject)c_CASL.c_make("error", "code", "EPICCANCEL-2CS", "message", strErrorMsg, 
          "title", "epic error", "throw", false);
        return geoError;
      }
      geoReturn = (GenericObject)geoInquiryData.get(0); // return single record
      string STATUS = (string)geoReturn.get("STATUS", "");
      if (STATUS == "CANCELED" || STATUS == "VOIDED")
        return geoReturn;

      ConnectionTypeSynch pDBConn = new ConnectionTypeSynch(db_info);
      DateTime dt = DateTime.Now;
      string strTodayDateTime = dt.ToString("MM/dd/yyyy HH:mm:ss");
      string err = null;

      if (!pDBConn.EstablishDatabaseConnection(ref err) || err != null)
        throw CASL_error.create("message", "Error opening database connection.", "code", "EPICCANCEL-3CS");

      strSQL = @"UPDATE CORE_TRANSFER SET TRAN_DT=@tran_dt, STATUS=@status, RECEIPT_REF=@receipt_ref WHERE REFERENCE_ID=@reference_id";
      HybridDictionary sql_args = new HybridDictionary();
      sql_args["tran_dt"] = strTodayDateTime;
      sql_args["status"] = "CANCELED";
      sql_args["reference_id"] = ref_id;
      sql_args["receipt_ref"] = receipt_ref;
      geoReturn.set("TRAN_DT", strTodayDateTime);
      geoReturn.set("RECEIPT_REF", receipt_ref); 

      int count = ParamUtils.runExecuteNonQuery(pDBConn, strSQL, sql_args);
      if (count != 1)
      {
        throw CASL_error.create("message", "Error executing SQL. Unable to Update Epic Table CORE_TRANSFER" + strError,
          "code", "EPICCANCEL-4CS");
      }
      SetStatusChanged(ref_id, "CANCELED", (string)geoReturn.get("RECEIPT_REF", ""), 
        (string)geoReturn.get("TENDER_TYPE", ""), (string)geoReturn.get("TENDER_INFO", ""), geoReturn);
      UpdateReference(geoReturn);

      return geoReturn;
    }

    /// <summary>
    /// Handle a void
    /// </summary>
    /// <param name = "arg_pairs"></param>
    /// <returns></returns>
    public static GenericObject CASL_EpicVoid(params object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      string ref_id = args.get("REFERENCE_ID", "").ToString();
      string receipt_ref = args.get("RECEIPT_REF", "").ToString();
      DateTime dt = DateTime.Now;
      string strTodayDateTime = dt.ToString("MM/dd/yyyy HH:mm:ss");

      string tran_dt = args.get("TRAN_DT", strTodayDateTime).ToString();
      GenericObject db_info = (GenericObject)args.get("db_connection_info");

      return EpicVoid(ref_id, tran_dt, receipt_ref, db_info);
    }

    /// <summary>
    /// Handle a void
    /// </summary>
    /// <param name="ref_id"></param>
    /// <param name="tran_dt"></param>
    /// <param name="receipt_ref"></param>
    /// <param name="db_info"></param>
    /// <returns></returns>
    public static GenericObject EpicVoid(string ref_id, string tran_dt, string receipt_ref, GenericObject db_info)
    {
#if DEBUG
      Logger.cs_log(String.Format("epicsi: EpicVoid ref_id: '{0}', receipt_ref: '{1}'", ref_id, receipt_ref));
#endif
      GenericObject geoReturn = null;

      string strError = "";
      string strSQL = "";

      // validate if payment/refund request is still active 
      strSQL = "SELECT REFERENCE_ID, STATUS, TRAN_DT, TENDER_TYPE, TENDER_INFO FROM CORE_TRANSFER WHERE REFERENCE_ID=@reference_id";
      GenericObject geoInquiryData = GetRecords(db_info, strSQL, ref_id);
      if (!geoInquiryData.has(0)) // not found
      {
        string strErrorMsg = "Payment Not Found in EPIC System.";
        GenericObject geoError = (GenericObject)c_CASL.c_make("error", "code", "EPICVOID-1CS", "message", strErrorMsg,
          "title", "epic error", "throw", false);
        return geoError;
      }
      geoReturn = (GenericObject)geoInquiryData.get(0); // return single record
      string STATUS = (string)geoReturn.get("STATUS", "");

      if (STATUS == "CANCELED" || STATUS == "VOIDED")
      {
        return geoReturn;
      }
      if (String.IsNullOrWhiteSpace(ref_id))
      {
        string strErrorMsg = "Can not Update EPIC System.";
        GenericObject geoError = (GenericObject)c_CASL.c_make("error", "code", "EPICVOID-2CS", "message", strErrorMsg,
          "title", "epic error", "throw", false);
        return geoError;
      }

      ConnectionTypeSynch pDBConn = new ConnectionTypeSynch(db_info);
      string err = null;

      if (!pDBConn.EstablishDatabaseConnection(ref err) || err != null)
        throw CASL_error.create("message", "Error opening database connection.", "code", "EPICVOID-3CS");

      strSQL = "UPDATE CORE_TRANSFER SET TRAN_DT=@tran_dt, STATUS=@status, RECEIPT_REF=@receipt_ref WHERE REFERENCE_ID=@reference_id";
      HybridDictionary sql_args = new HybridDictionary();
      sql_args["reference_id"] = ref_id;
      sql_args["tran_dt"] = tran_dt;
      sql_args["status"] = "VOIDED";
      sql_args["receipt_ref"] = receipt_ref;
      geoReturn.set("TRAN_DT", tran_dt);
      geoReturn.set("RECEIPT_REF", receipt_ref); 

      int count = ParamUtils.runExecuteNonQuery(pDBConn, strSQL, sql_args);
      if (count != 1)
      {
        throw CASL_error.create("message", "Error executing SQL. Unable to Update Epic Table CORE_TRANSFER" + strError,
          "code", "EPICVOID-4CS");
      }

      SetStatusChanged(ref_id, "VOIDED", receipt_ref, (string)geoReturn.get("TENDER_TYPE", ""), (string)geoReturn.get("TENDER_INFO", ""), geoReturn);
      UpdateReference(geoReturn);
      return geoReturn;
    }

    /// <summary>
    /// Handle exiting iPayment. Set status to passed in status. The Notification service will
    /// update CoreDirect.
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static GenericObject CASL_EpicExitIpayment(params object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      string ref_id = args.get("REFERENCE_ID", "").ToString();
      GenericObject db_info = (GenericObject)args.get("db_connection_info");

      return EpicExitIpayment(ref_id, db_info);
    }

    /// <summary>
    /// Handle exiting iPayment. Do a cancel.
    /// </summary>
    /// <param name="ref_id"></param>
    /// <param name="status"></param>
    /// <param name="db_info"></param>
    /// <returns></returns>
    public static GenericObject EpicExitIpayment(string ref_id, GenericObject db_info)
    {
#if DEBUG
      Logger.cs_log(String.Format("epicsi: EpicExitIpayment ref_id: '{0}'", ref_id));
#endif
      GenericObject geoReturn = new GenericObject();

      string strError = "";
      string strSQL = "";
      string retStatus;

      ConnectionTypeSynch pDBConn = new ConnectionTypeSynch(db_info);
      DateTime dt = DateTime.Now;
      string strTodayDateTime = dt.ToString("MM/dd/yyyy HH:mm:ss");
      string err = null;

      if (ref_id == "")
      {
        string strErrorMsg = "Can not Update EPIC System.";
        GenericObject geoError = (GenericObject)c_CASL.c_make("error", "code", "EPICEXIT-1CS", "message", strErrorMsg,
          "title", "epic error", "throw", false);
        return geoError;
      }

      // Determine status to set/return
      _statuses.TryGetValue(ref_id, out geoReturn);
      string reqType = geoReturn.get("REQ_TYPE", "request") as string;
      string currStatus = geoReturn.get("STATUS") as string;

      if (currStatus == "COMPLETED" && reqType != "refund")
      {
        return geoReturn;
      }

      switch (reqType)
      {
        case "CLOSESESSION":
        case "POPUP":
          retStatus = "COMPLETED";
          break;

        case "PAYMENTREQUEST":
        case "REFUNDREQUEST":
        case "TRANSACTIONREQUEST":
          if (currStatus == "COMPLETED" || currStatus == "CANCELED" || currStatus == "VOIDED")
          {
            return geoReturn;
          }
          
          // So if the status is POSTED or STARTED, cancel it
          retStatus = "CANCELED";
          break;

        case "refund":
          retStatus = currStatus;
          break;

        default:
          retStatus = "CANCELED";
          break;
      }

      if (!pDBConn.EstablishDatabaseConnection(ref err) || err != null)
        throw CASL_error.create("message", "Error opening database connection.", "code", "EPICEXIT-2CS");

      strSQL = "UPDATE CORE_TRANSFER SET STATUS=@status, TRAN_DT=@tran_dt WHERE REFERENCE_ID=@reference_id";
      HybridDictionary sql_args = new HybridDictionary();
      sql_args["reference_id"] = ref_id;
      sql_args["tran_dt"] = strTodayDateTime;
      sql_args["status"] = retStatus;
      geoReturn.set("TRAN_DT", dt);

      int count = ParamUtils.runExecuteNonQuery(pDBConn, strSQL, sql_args);
      if (count != 1)
      {
        throw CASL_error.create("message", "Error executing SQL. Unable to Update Epic Table CORE_TRANSFER" + strError,
          "code", "EPICEXIT-3CS");
      }

      SetStatusChanged(ref_id, retStatus, "", (string)geoReturn.get("TENDER_TYPE", ""), (string)geoReturn.get("TENDER_INFO", ""), geoReturn);
      UpdateReference(geoReturn);
      return geoReturn;
    }

    /// <summary>
    /// Generic Database Utility Function
    /// </summary>
    /// <param name="geoDBInfo"></param>
    /// <param name="strSQL"></param>
    /// <param name="reference_id"></param>
    /// <returns></returns>
    public static GenericObject GetRecords(GenericObject geoDBInfo, string strSQL, string reference_id)
    {
      Hashtable sql_args = new Hashtable();
      sql_args["reference_id"] = reference_id;

      string error_message = "";
      IDBVectorReader reader = db_reader.Make(geoDBInfo, strSQL, CommandType.Text, 0, sql_args, 0, 0, null, ref error_message);
      if (error_message != "")
      {
        throw new Exception("Error: cannot read from database: Error: " + error_message + " with this SQL: " + strSQL);
      }

      GenericObject results = new GenericObject();
      reader.ReadIntoRecordset(ref results, 0, 0);
      return results;
    }

    /// <summary>
    /// Adds the reference to the internal dictionary.
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static GenericObject CASL_EpicAddReference(params object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      try
      {
        string ref_id   = args.get("REFERENCE_ID", "").ToString();
        _statuses.TryAdd(ref_id, args);
      }
      catch (Exception ex)
      {
        Logger.cs_log(string.Format(" AddReference Exception: {0}", ex.ToMessageAndCompleteStacktrace())); // Bug 17849
      }
      return args;
    }

    /// <summary>
    /// Deletes the reference from the internal dictionary.
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static GenericObject CASL_EpicDeleteReference(params object[] arg_pairs)
    {
      GenericObject status = new GenericObject();
      try
      {
        GenericObject args = misc.convert_args(arg_pairs);
        string ref_id = args.get("REFERENCE_ID", "").ToString();
        _statuses.TryRemove(ref_id, out status);
      }
      catch (Exception ex)
      {
        Logger.cs_log(string.Format(" DeleteReference Exception: {0}", ex.ToMessageAndCompleteStacktrace())); // Bug 17849
      }
      return status;
    }

    /// <summary>
    /// Gets the status from the internal dictionary.
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static GenericObject CASL_EpicGetStatus(params object[] arg_pairs)
    {
      GenericObject status = new GenericObject();
      status.set("STATUS", "MISSING");
      try
      {
        GenericObject args = misc.convert_args(arg_pairs);
        string ref_id = args.get("REFERENCE_ID", "").ToString();
        if (_statuses.ContainsKey(ref_id))
          return _statuses[ref_id];
        else
          return status;
      }
      catch (Exception ex)
      {
        Logger.cs_log(string.Format(" GetStatus Exception: {0}", ex.ToMessageAndCompleteStacktrace())); // bug 17849
        return status;
      }
    }

    // CASL_EpicPostResponse
    /// <summary>
    /// Update CORE_TRANSFER table with XML response sent to Epic.
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static GenericObject CASL_EpicPostResponse(params object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      string ref_id = args.get("REFERENCE_ID", "").ToString();
      string zipped_xml_response = args.get("zipped_xml_response", "").ToString(); // this will be treated as zipped, base64 encoded!
      string session_id = args.get("session_id", "no_session_id").ToString();
      GenericObject db_info = (GenericObject)args.get("db_connection_info");

      return EpicPostResponse(ref_id, zipped_xml_response, session_id, db_info);
    }

    // Don't change! Core Direct sends this when user closes browser!
    public const string userClosedBrowserMsg = "Transaction CANCELED - User closed browser.";

    /// <summary>
    /// Posts the Epic response that Core Direct generates back into the CORE_TRANSFER table.
    /// </summary>
    /// <param name="ref_id">reference id from CORE_TRANSFER table</param>
    /// <param name="xml_response">XML response previously sent to Epic.
    /// Assumed to be zipped, base64-encoded.
    /// </param>
    /// <param name="session_id"></param>
    /// <param name="db_info"></param>
    /// <returns>Nothing really.</returns>
    public static GenericObject EpicPostResponse(string ref_id, string zipped_xml_response, string session_id, GenericObject db_info)
    {
      string xml_response = DecompressString(zipped_xml_response);
#if DEBUG
      Logger.cs_log(
        String.Format("epicsi: EpicPostResponse ref_id: '{0}', xml_response: '{1}'", 
          ref_id, xml_response));
#endif
      GenericObject geoReturn = new GenericObject();

      string strSQL = "";
      if (String.IsNullOrWhiteSpace(ref_id) || String.IsNullOrWhiteSpace(xml_response))
      {
        return geoReturn;
      }

      // validate if payment/refund request is still active 
      strSQL = "SELECT REFERENCE_ID, SESSION_ID FROM CORE_TRANSFER WHERE REFERENCE_ID=@reference_id AND RESPONSE IS NULL";
      GenericObject geoInquiryData = GetRecords(db_info, strSQL, ref_id);
      if (!geoInquiryData.has(0)) // not found means we already have a response
      {
        return geoReturn;
      }

      string err = null;
      HybridDictionary sql_args = new HybridDictionary();
      sql_args["reference_id"] = ref_id;
      sql_args["response"] = xml_response;
      strSQL = @"UPDATE CORE_TRANSFER SET RESPONSE=@response WHERE REFERENCE_ID=@reference_id";

      // Need to update status and abandon session if the user closed the browser
      if (xml_response.Contains(userClosedBrowserMsg))
      {
        geoReturn = (GenericObject)geoInquiryData.get(0); // return single record
        string sessionId = (string)geoReturn.get("SESSION_ID", "missing");
        sql_args["status"] = "CANCELED";
        strSQL = @"UPDATE CORE_TRANSFER SET RESPONSE=@response, STATUS=@status WHERE REFERENCE_ID=@reference_id";

        // only abandon session if session_ids match (pretty unlikely, but worth a shot!)
        if (session_id == sessionId)
        {
          CASLInterpreter.get_my().set("_abandon_session", true);
        }
      }

      // Connect to database.
      ConnectionTypeSynch pDBConn = new ConnectionTypeSynch(db_info);

      if (!pDBConn.EstablishDatabaseConnection(ref err) || err != null)
        return geoReturn;

      ParamUtils.runExecuteNonQuery(pDBConn, strSQL, sql_args);
      return geoReturn;
    }

    /// <summary>
    /// Handle exiting iPayment. Set status to passed in status. The Notification service will
    /// update CoreDirect.
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static GenericObject CASL_EpicUpdateReference(params object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject status = args.get("status") as GenericObject;
      UpdateReference(status);
      return status;
    }

    /// <summary>
    /// Update the reference in the _statuses dictionary
    /// </summary>
    /// <param name="status"></param>
    /// <returns></returns>
    private static void UpdateReference(GenericObject status)
    {
      string ref_id = status.get("REFERENCE_ID") as string;
#if DEBUG
      Logger.cs_log(String.Format(
        "epicsi: UpdateReference ref_id: '{0}', receipt_ref: '{1}', status: '{2}', tran_dt: '{3}', tender_type: '{4}' tender_info: '{5}'",
        ref_id,
        status.get("RECEIPT_REF", ""),
        status.get("STATUS"), 
        status.get("TRAN_DT", ""),
        status.get("TENDER_TYPE", ""), 
        status.get("TENDER_INFO", "")));
#endif
      try
      {
        _statuses[ref_id] = status;
      }
      catch (Exception ex)
      {
        Logger.cs_log(string.Format(" UpdateReference Exception: {0}", ex.ToMessageAndCompleteStacktrace())); // bug 17849
        throw CASL_error.create("message", "Error calling Updating reference." + ex.Message, "code", "EPICUPDREF-1CS");
      }
    }

    /// <summary>
    /// Decompresses the string. Used by EpicPostResponse
    /// </summary>
    /// <param name="compressedText">The compressed text.</param>
    /// <returns></returns>
    public static string DecompressString(string compressedText)
    {
      string response = "";
      try
      {
        byte[] gZipBuffer = misc.Base64UrlDecode(compressedText);
        using (var memoryStream = new MemoryStream())
        {
          int dataLength = BitConverter.ToInt32(gZipBuffer, 0);
          memoryStream.Write(gZipBuffer, 4, gZipBuffer.Length - 4);

          var buffer = new byte[dataLength];

          memoryStream.Position = 0;
          using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Decompress))
          {
            gZipStream.Read(buffer, 0, buffer.Length);
          }

          response = Encoding.UTF8.GetString(buffer);
        }
      }
      catch
      {
      }
      return response;
    }
  }
}
