﻿using CASL_engine;
using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CustomizedBatchUpdate
{

  /// <summary>
  /// used for process transaction detail records, including over/short
  /// </summary>
  public class BatchUpdateDetailRecordProcessor
  {
    private BatchUpdateConfigData _config_data { get; set; }
    private GenericObject _geoFile { get; set; }
    private GenericObject _geoTran { get; set; }
    private GenericObject _geoTranItem { get; set; }
    private List<string> _gl_segs { get; set; }
    private string _gl { get; set; }

    public BatchUpdateDetailRecordProcessor(BatchUpdateConfigData config_data, GenericObject geoFile, GenericObject geoTran, GenericObject geoTranItem)
    {
      _config_data = config_data;
      _geoFile = geoFile;
      _geoTran = geoTran;
      _geoTranItem = geoTranItem;


      // Get GL
      if (geoTranItem != null)
      {
        _gl_segs = new List<string>();
        string gl = _geoTranItem.get("GLACCTNBR", "").ToString();
        string[] gl_segs = gl.Split('|');
        string sTemp = gl.Replace("|", "").Trim();
        if (!string.IsNullOrEmpty(sTemp))
        {
          foreach (string item in gl_segs)
            _gl_segs.Add(item);

        }
        _gl = gl;
      }
      else
      {
        _gl_segs = new List<string>(); // to protect null object
        _gl = "";
      }
    }

    // format detail record
    public BatchUpdateLineRec FormatDetailLineRecord()
    {
      BatchUpdateLineRec bu_rec = new BatchUpdateLineRec();
      bu_rec.lstRec = new List<RecordItem>();

      // before process, I need check if it is sales tax, and if I need included sales tax
      bool is_sales_tax = false;
      
      if (_geoTranItem != null)
      {
        string item_desc = _geoTranItem.get("ITEMDESC").ToString().Trim();
        is_sales_tax = string.Compare(item_desc, "tax", true) == 0 ? true : false;
      }
      else
      {
        string tt_desc = _geoTran.get("TTDESC").ToString().Trim();
        is_sales_tax = string.Compare(tt_desc, "Sales Tax", true) == 0 ? true : false;
      }

      
      if (is_sales_tax && _config_data.general_info.include_sales_tax == false)
      {
        // I do not need to process for sales tax
        return bu_rec;

      }

      // group configuration by sequence order
      ILookup<int, DetailBatchRecord> lookup = _config_data.details.ToLookup(o => o.seq_order);
      List<int> lstSeqOrder = lookup.Select(o => o.Key).ToList();
      lstSeqOrder.Sort();

      RecordMode rec_mode = GetTTRecordMode(); // get this transaction type's record mode, one of summary or detail
      foreach (int seq_order in lstSeqOrder) // usuaaly have one record, combination will have more than one records
      {
        string rec_str = "";
        List<BatchRecord> lstConfigRec = lookup[seq_order].ToList();
        foreach (BatchRecord rec in lstConfigRec)
        {
          // if record mode / record type (detail, summary, overrage, ... ) is specificed
          if (rec.record_type != null && !string.IsNullOrEmpty(rec.record_type.value))
          {
            // for same record mode (here record_type is should be one of detail, summary, overrage ...
            if (string.Compare(rec_mode.ToString(), rec.record_type.value, true) == 0)
            {
              rec_str += FormatDetailRec(rec); // format record item based on configuration
              rec_str += " ";
            }
          }
          else
          {
            rec_str += FormatDetailRec(rec);
            rec_str += " ";
          }
        }

        rec_str = rec_str.Trim();
        // format value based on the type of fixed length, variable length, space or zero filled
        rec_str = rec_str.FormatValue(_config_data.general_info.record_type.value, lstConfigRec[0]);

        // get configuration
        DetailBatchRecord config_rec = lstConfigRec[0];
        RecordItem detail_rec_item = new RecordItem();
        detail_rec_item.seq_order = seq_order;

        if (string.Compare(config_rec.source_type.value, "Fixed_Value", true) == 0)
          detail_rec_item.tag = "Fixed_Value";
        else
          detail_rec_item.tag = config_rec.source.value;

        detail_rec_item.value = rec_str;
        detail_rec_item.detail_rec_config = config_rec;
        bu_rec.lstRec.Add(detail_rec_item);

      }

      // I needed add payfile number, reference number, group id, gl number, transaction discription for later use
      bu_rec.pay_file_name = _geoFile.GetCoreFileName(null);
      bu_rec.rcpt_ref_nbr = _geoTran.GetReceiptRefNbr(null);
      bu_rec.dept_id = _geoFile.GetWorkgroupId();
      bu_rec.trans_desc = _geoTran.GetTTDesc();
      if (_geoTranItem != null)
        bu_rec.amt = Convert.ToDouble(_geoTranItem.get("TOTAL").ToString());
      else
        bu_rec.amt = Convert.ToDouble(_geoTran.get("TRANAMT", "").ToString());
      bu_rec.gl_nbr = _gl;

      bu_rec.record_mode = rec_mode; // one of summary, detail, or none
      return bu_rec;
    }

    public BatchUpdateLineRec FormatOverShortRecord()
    {

      // First I need get offset gl
      string strFileName = _geoFile.get("FILENAME", "") as string;
      string strDeptID = _geoFile.get("DEPTID", "") as string;
      GenericObject geoDept = (GenericObject)((GenericObject)c_CASL.c_object("Business.Department.of")).get(strDeptID);
      GenericObject geoOffsetGL;

      RecordMode rec_mode = RecordMode.none;
      if (Convert.ToDouble(_geoTran.get("TRANAMT").ToString()) < 0)
      {
        geoOffsetGL = geoDept.get("gl_nbr_short", null) as GenericObject;
        rec_mode = RecordMode.Shortage;
      }
      else
      {
        geoOffsetGL = geoDept.get("gl_nbr_over", null) as GenericObject;
        rec_mode = RecordMode.Overage;
      }

      _gl_segs.Clear();
      _gl = "";
      if (geoOffsetGL != null)
      {
        for(int i= 0; i<geoOffsetGL.getIntKeyLength(); i++)
        {
          _gl_segs.Add(geoOffsetGL.get(i).ToString());
          _gl += geoOffsetGL.get(i).ToString();
          _gl += "|";
        }
        _gl = _gl.TrimEnd('|');
      }
    

      BatchUpdateLineRec bu_rec = new BatchUpdateLineRec();
      bu_rec.lstRec = new List<RecordItem>();

      // group configuration by sequence order
      ILookup<int, DetailBatchRecord> lookup = _config_data.details.ToLookup(o => o.seq_order);
      List<int> lstSeqOrder = lookup.Select(o => o.Key).ToList();
      lstSeqOrder.Sort();

      foreach (int seq_order in lstSeqOrder) // usuaaly have one record, combination will have more than one records
      {
        string rec_str = "";
        List<DetailBatchRecord> lstConfigRec = lookup[seq_order].ToList();
        foreach (DetailBatchRecord rec in lstConfigRec)
        {
          // if record mode / record type (detail, summary, overrage, ... ) is specificed
          if (rec.record_type != null && !string.IsNullOrEmpty(rec.record_type.value))
          {
            // for same record mode (here record_type is should be one of detail, summary, overrage, shortage ...
            if (string.Compare(rec.record_type.value, rec_mode.ToString(), true) == 0)
            {
              rec_str += FormatDetailRec(rec); // format record item based on configuration
              rec_str += " ";
            }
          }
          else
          {
            rec_str += FormatDetailRec(rec);
            rec_str += " ";
          }
        }

        rec_str = rec_str.Trim();
        // format value based on the type of fixed length, variable length, space or zero filled
        rec_str = rec_str.FormatValue(_config_data.general_info.record_type.value, lstConfigRec[0]);

        // get configuration
        DetailBatchRecord config_rec = lstConfigRec[0];
        RecordItem detail_rec_item = new RecordItem();
        detail_rec_item.seq_order = seq_order;

        if (string.Compare(config_rec.source_type.value, "Fixed_Value", true) == 0)
          detail_rec_item.tag = "Fixed_Value";
        else
          detail_rec_item.tag = config_rec.source.value;

        detail_rec_item.value = rec_str;
        detail_rec_item.detail_rec_config = config_rec;
        bu_rec.lstRec.Add(detail_rec_item);

      }

      // I needed add payfile number, reference number, group id, gl number, transaction discription for later use
      bu_rec.pay_file_name = _geoFile.GetCoreFileName(null);
      bu_rec.rcpt_ref_nbr = _geoTran.GetReceiptRefNbr(null);
      bu_rec.dept_id = _geoFile.GetWorkgroupId();
      bu_rec.trans_desc = _geoTran.GetTTDesc();
      if (_geoTranItem != null)
        bu_rec.amt = Convert.ToDouble(_geoTranItem.get("TOTAL").ToString());
      else
        bu_rec.amt = Convert.ToDouble(_geoTran.get("TRANAMT", "").ToString());
      bu_rec.gl_nbr = _gl;

      bu_rec.record_mode = rec_mode; // one of summary, detail, overage, shortage ...
      return bu_rec;
    }

    #region protected_funcs
    protected string FormatDetailRec(DetailBatchRecord rec)
    {

      string rec_str = "";
      string source_type = rec.source_type.value;
      if (string.Compare(source_type, "STD_DATA", true) == 0)
      {
        rec_str = FormatStandardDataField(rec);
      }
      else if (string.Compare(source_type, "FIXED_VALUE", true) == 0)
      {
        rec_str = rec.fixed_value;
      }
      else if (string.Compare(source_type, "CUSTOM_FIELD_DATA", true) == 0)
      {
        rec_str = _geoTran.GetCustomFieldValue(rec.source.value); // rec.source.value contains custom field tag name
      }
      else if (string.Compare(source_type, "GL_NUMBER", true) == 0)
      {
        rec_str = _gl_segs.GetGLSegmentValue(rec.source.value); // rec.source.value format: string.Format("{0}|{1}", rec.order, rec.name);
      }
      return rec_str;
    }
    protected string FormatStandardDataField(DetailBatchRecord rec)
    {
      string rec_str = "";
      string source = rec.source.value;
      if (string.Compare(source, "DepFile_Name", true) == 0)
      {
        rec_str = _geoFile.GetCoreFileName(rec.format.value);
      }
      else if (string.Compare(source, "Receipt_Reference_Nbr", true) == 0)
      {
        rec_str = _geoTran.GetReceiptRefNbr(rec.format.value);
      }
      else if (string.Compare(source, "Trans_Reference_Nbr", true) == 0)
      {
        rec_str = _geoTran.GetTransactionRefNbr(rec.format.value);
      }
      else if (string.Compare(source, "File_Open_Date", true) == 0) // file open date
      {
        DateTime date;
        string date_str = _geoFile.get("OPENDT", "").ToString();
        if (!string.IsNullOrEmpty(date_str))
          date = DateTime.Parse(date_str);
        else
          date = DateTime.Now;
        rec_str = date.FormatDate(rec.format.value);
      }
      else if (string.Compare(source, "File_Effective_Date", true) == 0)
      {
        DateTime date;
        string date_str = _geoFile.get("EFFECTIVEDT", "").ToString();
        if (!string.IsNullOrEmpty(date_str))
          date = DateTime.Parse(date_str);
        else
          date = DateTime.Now;
        rec_str = date.FormatDate(rec.format.value);
      }
      else if (string.Compare(source, "Trans_Date", true) == 0)
      {
        DateTime date;
        string date_str = _geoTran.get("POSTDT", "").ToString();
        if (!string.IsNullOrEmpty(date_str))
          date = DateTime.Parse(date_str);
        else
          date = DateTime.Now;
        rec_str = date.FormatDate(rec.format.value);
      }
      else if (string.Compare(source, "User_Name", true) == 0)
      {
        rec_str = _geoFile.GetFileOpenUserName();
      }
      else if (string.Compare(source, "Workgroup_Id", true) == 0)
      {
        rec_str = _geoFile.GetWorkgroupId();
      }
      else if (string.Compare(source, "Workgroup_Name", true) == 0)
      {
        rec_str = _geoFile.GetWorkgroupDescription();
      }
      else if (string.Compare(source, "Transaction_Type_Id", true) == 0)
      {
        rec_str = _geoTran.GetTTID();
      }
      else if (string.Compare(source, "Transaction_Type_Description", true) == 0)
      {
        rec_str = _geoTran.GetTTDesc();
      }
      else if (string.Compare(source, "Trans_Amount", true) == 0 || string.Compare(source, "Trans_Absolute_Amount") == 0)
      {
        double amount = 0;
        if (_geoTranItem != null)
          amount = Convert.ToDouble(_geoTranItem.get("TOTAL").ToString());
        else
          amount = Convert.ToDouble(_geoTran.get("TRANAMT", "").ToString());

        rec_str = amount.FormatAmount(rec.format.value).FormatValue(_config_data.general_info.record_type.value, rec);

      }
      else if (string.Compare(source, "Debit_Credit_Indicator", true) == 0)
      {
        double amount = 0;
        if (_geoTranItem != null)
          amount = Convert.ToDouble(_geoTranItem.get("TOTAL").ToString());
        else
          amount = Convert.ToDouble(_geoTran.get("TRANAMT", "").ToString());

        rec_str = amount.FormatDebitCreditIndicator(rec.format.value);
      }

      return rec_str;
    }
    /// <summary>
    /// Get Transaction Type Record Mode
    /// </summary>
    /// <returns></returns>
    protected RecordMode GetTTRecordMode() // summary, detail, or none
    {
      RecordMode record_mode = RecordMode.none;
      bool summary_mode = false;
      bool detail_mode = false;

      // check if summary mode
      if (_config_data.general_info.summary_mode.mode == true) // contain summary
      {
        summary_mode = true;
        // any condition?
        if (_config_data.general_info.summary_mode.filters.Count > 0)
        {
          foreach (filter f in _config_data.general_info.summary_mode.filters)
          {
            string str_value = GetFilterSourceValue(f);
           
            // check if condition is satisfied
            if (string.Compare(f.logic, "and", true) == 0)
              summary_mode = summary_mode && IsConditionSatisfy(str_value, f);
            else if (string.Compare(f.logic, "or", true) == 0)
              summary_mode = summary_mode || IsConditionSatisfy(str_value, f);
            else // null as "and"
            {
              summary_mode = summary_mode && IsConditionSatisfy(str_value, f);
            }
          }
        }

      }

      // check if detail _mode
      if (_config_data.general_info.detail_mode.mode == true) // contain detail
      {
        detail_mode = true;
        // any condition?
        if (_config_data.general_info.detail_mode.filters.Count > 0)
        {
          foreach (filter f in _config_data.general_info.detail_mode.filters)
          {
            string str_value = GetFilterSourceValue(f);
          
            if (string.Compare(f.logic, "and", true) == 0)
              detail_mode = detail_mode && IsConditionSatisfy(str_value, f);
            else if (string.Compare(f.logic, "or", true) == 0)
              detail_mode = detail_mode || IsConditionSatisfy(str_value, f);
            else // null as "and"
            {
              detail_mode = detail_mode && IsConditionSatisfy(str_value, f);
            }
          }
        }

      }

      if (summary_mode && !detail_mode)
        record_mode = RecordMode.summary;
      else if (!summary_mode && detail_mode)
        record_mode = RecordMode.detail;
      else
        record_mode = RecordMode.none;

      return record_mode;
    }

    protected string GetFilterSourceValue(filter f)
    {
      string str_value = "";
      if (string.Compare(f.source_type, "std_data", true) == 0)
      {
        if (string.Compare(f.source, "Workgroup_Id", true) == 0)
          str_value = _geoFile.GetWorkgroupId();
        else if (string.Compare(f.source, "Workgroup_Name", true) == 0)
          str_value = _geoFile.GetWorkgroupDescription();
        else if (string.Compare(f.source, "Transaction_Type_Id", true) == 0)
          str_value = _geoTran.GetTTID();
        else if (string.Compare(f.source, "Transaction_Type_Description", true) == 0)
          str_value = _geoTran.GetTTDesc();
      }
      else if (string.Compare(f.source_type, "Custom_Field_Data", true) == 0)
      {
        str_value = _geoTran.GetCustomFieldValue(f.source); // f.source is custom field tag name

      }
      else if (string.Compare(f.source_type, "GL_Number", true) == 0)
      {
        str_value = _gl_segs.GetGLSegmentValue(f.source); // f.source's format: string.Format("{0}|{1}", rec.order, rec.name); see CBU conditionComponent.razor
      }

      return str_value;
    }
    protected bool IsConditionSatisfy(string value, filter f)
    {
      bool rst = false;
      double nbr = 0;
      double filter_nbr = 0;
      bool isNumeric = double.TryParse(value, out nbr) && double.TryParse(f.value, out filter_nbr);
      switch (f.condition)
      {
        case ">":
          if (isNumeric)
            rst = nbr > filter_nbr ? true : false;
          else
            rst = string.Compare(value, f.value, true) > 0 ? true : false;
          break;
        case ">=":
          if (isNumeric)
            rst = nbr >= filter_nbr ? true : false;
          else
            rst = string.Compare(value, f.value, true) >= 0 ? true : false;
          break;
        case "<":
          if (isNumeric)
            rst = nbr < filter_nbr ? true : false;
          else
            rst = string.Compare(value, f.value, true) < 0 ? true : false;
          break;
        case "<=":
          if (isNumeric)
            rst = nbr <= filter_nbr ? true : false;
          else
            rst = string.Compare(value, f.value, true) <= 0 ? true : false;
          break;
        case "=":
          if (isNumeric)
            rst = nbr == filter_nbr ? true : false;
          else
            rst = string.Compare(value, f.value, true) == 0 ? true : false;
          break;
        case "!=":
          if (isNumeric)
            rst = nbr != filter_nbr ? true : false;
          else
            rst = string.Compare(value, f.value, true) != 0 ? true : false;
          break;

      }
      return rst;
    }

    #endregion


  }
}
