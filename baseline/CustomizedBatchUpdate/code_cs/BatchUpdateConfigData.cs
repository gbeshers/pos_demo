﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomizedBatchUpdate
{
  
  public class BatchUpdateConfigData
  {
    public GeneralInfoConfig general_info { get; set; }
    public Dictionary<string, List<BatchRecord>> bu_ht_info { get; set; }       // V2 string is for layer name
    public Dictionary<string, List<BatchRecord>> bu_detail_info { get; set; }   // V2 string is for layer name

  }

  public class Item
  {
    public string value { get; set; }
    public string text { get; set; }
  }

  public class GeneralInfoConfig
  {
    public string id { get; set; }                  // unique value                                         
    public Item record_type { get; set; }           
    public string delimiter { get; set; }
    public bool include_eoln { get; set; }
    public bool include_void_trans { get; set; }
    public bool include_sales_tax { get; set; }
    public bool include_deposit_over_short { get; set; }
    public bool amount_sign_reversal { get; set; }
    public bool reversal_trans_seq_in_single_pymt_page { get; set; }

    // level: the smaller of level number, the higher of the level
    // the highest level must be batch level, the lowest level must be detail
    public List<string> layer_outlines { get; set; }         // V2
    public List<Layer> layers { get; set; }                   // V2
    public bool include_bank_deposit { get; set; }
    public SummaryMode summary_mode { get; set; }
    public DetailMode detail_mode { get; set; }
    // IPAY-1588
    public bool offset_summarization { get; set; }
    public string offset_summarization_type { get; set; }
    public List<string> offset_additional_summarization_types { get; set; } 
  }

  public class Layer
  {
    public string id { get; set; }
    public string name { get; set; }
    public Item layer_type { get; set; }
    public Item gl { get; set; }            // only used for gl level, gl groupby info
    public Item custom_field { get; set; }  // only used for cf_level, groupby info
    public string type { get; set; }        // header or trailer
    public bool sum_consolidation { get; set; }
    public string selected_summarization_type { get; set; }           // IPAY-1588
    public List<string> additional_summarization_types { get; set; }  // IPAY-1588
    public int seq_order { get; set; }
  }
  public class SummaryMode
  {
    public bool mode { get; set; } = false;        // true or false
    public string summarization_type { get; set; }
    public List<string> additional_summarization_types { get; set; } // IPAY-1264
    public List<filter> filters { get; set; }
  }

  public class DetailMode
  {
    public bool mode { get; set; } = false;         // true or false
    public List<filter> filters { get; set; }
  }

  public class filter
  {
    public string source_type { get; set; }
    public string source { get; set; }
    public string condition { get; set; }           // > < =
    public string value { get; set; }
    public string logic { get; set; }               // and / or
  }

  public class BatchRecord
  {
    public string id { get; set; }              // unique value
    public Item source_type { get; set; }       // public string source_type { get; set; }
    public Item source { get; set; }            // public string source { get; set; }
    public Item format { get; set; }            // public string format { get; set; }
    public Item workgroup { get; set; }
    public Item trans_type { get; set; }        // may used for detail
    public Item record_type { get; set; }       // such as summary, detail, overage, shortage, used for detail
    public string fixed_value { get; set; }
    public int max_length { get; set; }
    public string jusitfy { get; set; }
    public string fill { get; set; }
    public int seq_order { get; set; }
    public bool combined_value { get; set; }
  }

  
}
