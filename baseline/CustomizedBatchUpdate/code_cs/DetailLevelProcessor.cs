﻿using CASL_engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomizedBatchUpdate
{
  public class DetailLevelProcessor
  {
    private BatchUpdateConfigData _config_data { get; set; }
    private GenericObject _geoFile { get; set; }
    private GenericObject _geoTran { get; set; }
    private GenericObject _geoTranItem { get; set; }
    private List<GenericObject> _geoTenders_Applied { get; set; }
    private Dictionary<string, string> _dic_gl_info { get; set; }

    public DetailLevelProcessor(BatchUpdateConfigData config_data, GenericObject geoFile, GenericObject geoTran, GenericObject geoTranItem, List<GenericObject> geoTenders_Applied)
    {
      _config_data = config_data;
      _geoFile = geoFile;
      _geoTran = geoTran;
      _geoTranItem = geoTranItem;
      _geoTenders_Applied = geoTenders_Applied;


      // Get GL
      _dic_gl_info = new Dictionary<string, string>();
      if (geoTranItem != null)
      {
        string gl = _geoTranItem.get("GLACCTNBR", "").ToString(); // example 1111|2222|3333|4444
        string[] gl_segs = gl.Split('|');
        string sTemp = gl.Replace("|", "").Trim();
        if (!string.IsNullOrEmpty(sTemp))
        {
          int cnt = 0;
          foreach (string item in gl_segs)
          {
            _dic_gl_info.Add(cnt.ToString(), item);
            cnt++;
          }
        }
        _dic_gl_info.Add("gl_nbr", gl); // CBU is using "gl_nbr" to save gl number string
      }

    }

    public Dictionary<string, BatchUpdateLineRec> Process(bool overshort = false)
    {
      // key is layer id
      Dictionary<string, BatchUpdateLineRec> dic_rec = new Dictionary<string, BatchUpdateLineRec>();

      if (_config_data.bu_detail_info != null)
      {
        // KeyValuePair<string, List<BatchRecord>> ==> key is layer id
        foreach (KeyValuePair<string, List<BatchRecord>> pair in _config_data.bu_detail_info)
        {
          // get layer info pair.key is layer.id
          _config_data.general_info.layers = _config_data.general_info.layers ?? new List<Layer>();
          foreach (Layer layer in _config_data.general_info.layers)
          {
            if (layer.id == pair.Key && string.Compare(layer.layer_type.value, nameof(LayerType.Detail_Level_Layer), true) == 0)
            {
              BatchUpdateLineRec line_rec = new BatchUpdateLineRec();
              if (overshort == false)
                line_rec = FormatLineRecord(layer, pair.Value);
              else
              {
                if (_config_data.general_info.include_deposit_over_short) // processed if configured
                  line_rec = FormatOverShortLineRecord(layer, pair.Value);
              }

              if (line_rec.lstRec.Count > 0)
              {
                dic_rec.Add(layer.id, line_rec);
              }
            }
          }
        }
      }
      return dic_rec;
    }

    public BatchDetailRecord Process2(bool overshort = false)
    {
      BatchDetailRecord batch_detail_rec = new BatchDetailRecord();

      if (_config_data.bu_detail_info != null)
      {
        foreach (KeyValuePair<string, List<BatchRecord>> pair in _config_data.bu_detail_info)
        {
          // get layer info pair.key is layer.id
          _config_data.general_info.layers = _config_data.general_info.layers ?? new List<Layer>();
          foreach (Layer layer in _config_data.general_info.layers)
          {
            if (layer.id == pair.Key && string.Compare(layer.layer_type.value, nameof(LayerType.Detail_Level_Layer), true) == 0)
            {
              // format record
              BatchUpdateLineRec line_rec = new BatchUpdateLineRec();
              if (overshort == false)
                line_rec = FormatLineRecord(layer, pair.Value);
              else
              {
                if (_config_data.general_info.include_deposit_over_short) // processed if configured
                  line_rec = FormatOverShortLineRecord(layer, pair.Value);
              }

              if (line_rec.lstRec.Count > 0)
              {
                batch_detail_rec.dicLineRec.Add(layer.id, line_rec);
              }
            }
          }
        }

        // Get group by info
        batch_detail_rec.dic_groupby_data = GetGroupInfo();

      }
      return batch_detail_rec;
    }

    protected BatchUpdateLineRec FormatLineRecord(Layer layer, List<BatchRecord> lst_bu_rec_config)
    {
      BatchUpdateLineRec line_rec = new BatchUpdateLineRec();
      line_rec.lstRec = new List<RecordItem>();
      line_rec.dic_groupby_data = new Dictionary<string, GroupbyData<GenericObject>>();

      // before process, I need check if it is sales tax, and if it is sale tax, I need included sales tax
      bool is_sales_tax = false;

      if (_geoTranItem != null)
      {
        string item_desc = _geoTranItem.get("ITEMDESC", "").ToString().Trim();
        is_sales_tax = string.Compare(item_desc, "tax", true) == 0 ? true : false;
      }
      else
      {
        string tt_desc = _geoTran.get("TTDESC", "").ToString().Trim();
        is_sales_tax = string.Compare(tt_desc, "Sales Tax", true) == 0 ? true : false;
      }

      if (is_sales_tax && _config_data.general_info.include_sales_tax == false)
        return line_rec; // do not process it

      // about configuration
      ILookup<int, BatchRecord> lookup_config = lst_bu_rec_config.ToLookup(o => o.seq_order);
      List<int> lstSeqOrder = lookup_config.Select(o => o.Key).ToList();
      lstSeqOrder.Sort();

      RecordMode rec_mode = GetTTRecordMode(); // get this transaction type's record mode, one of summary or detail
      foreach (int seq_order in lstSeqOrder) // usuaaly have one record, combination will have more than one records
      {
        string rec_str = "";
        List<BatchRecord> lstConfigRec = lookup_config[seq_order].ToList(); // could have combined values or OR
        foreach (BatchRecord rec in lstConfigRec)
        {
          // if record mode / record type (detail, summary, overrage, ... ) is specificed
          if (rec.record_type != null && !string.IsNullOrEmpty(rec.record_type.value))
          {
            // for same record mode (here record_type is should be one of detail, summary, overrage ...
            if (string.Compare(rec_mode.ToString(), rec.record_type.value, true) == 0)
            {
              // if combined set as true, combined value together, 
              // otherwise if pre value is empty, use next value configured. not combined
              if (rec.combined_value == true)
              {
                rec_str += " ";
                rec_str += FormatRecord(rec);
              }
              else
              {
                rec_str.Trim();
                if (string.IsNullOrEmpty(rec_str))
                  rec_str = FormatRecord(rec);

              }
            }
          }
          else
          {
            // if combined set as true, combined value together, 
            // otherwise if pre value is empty, use next value configured. not combined
            if (rec.combined_value)
            {
              rec_str += " ";
              rec_str += FormatRecord(rec);
            }
            else
            {
              rec_str.Trim();
              if (string.IsNullOrEmpty(rec_str))
                rec_str = FormatRecord(rec);
            }
          }
        }

        rec_str = rec_str.Trim();
        // format value based on the type of fixed length, variable length, space or zero filled
        rec_str = rec_str.FormatValue(_config_data.general_info.record_type.value, lstConfigRec[0]);

        // get batchrecord configuration
        BatchRecord config_rec = lstConfigRec[0]; // configuration
        RecordItem detail_rec_item = new RecordItem();

        detail_rec_item.seq_order = seq_order;

        if (string.Compare(config_rec.source_type.value, "Fixed_Value", true) == 0)
          detail_rec_item.tag = "Fixed_Value";
        else
          detail_rec_item.tag = config_rec.source.value;

        detail_rec_item.value = rec_str;    // format record value
        detail_rec_item.rec_config = config_rec;
        line_rec.lstRec.Add(detail_rec_item);
      }

      // save layer info for later use
      line_rec.layer = layer;
      if (_geoTranItem != null)
        line_rec.amt = Convert.ToDouble(_geoTranItem.get("TOTAL", "").ToString());
      else
        line_rec.amt = Convert.ToDouble(_geoTran.get("TRANAMT", "").ToString());
      line_rec.gl_nbr = _dic_gl_info.ContainsKey("gl_nbr") ? _dic_gl_info["gl_nbr"] : "";
      line_rec.record_mode = rec_mode; // one of summary, detail, overage, shortage...

      // for potential summarization
      line_rec.dept_id = _geoFile.GetWorkgroupId();
      line_rec.pay_file_name = _geoFile.GetCoreFileName("");
      line_rec.rcpt_ref_nbr = _geoTran.GetReceiptRefNbr("");
      line_rec.tt_id = _geoTran.GetTTID();
      line_rec.trans_post_date = _geoTran.GetTransPostDate();

      // get groupby data
      line_rec.dic_groupby_data = GetGroupInfo();

      return line_rec;
    }

    protected BatchUpdateLineRec FormatOverShortLineRecord(Layer layer, List<BatchRecord> lst_bu_rec_config)
    {

      // First I need get offset gl
      string strFileName = _geoFile.get("FILENAME", "") as string;
      string strDeptID = _geoFile.get("DEPTID", "") as string;
      GenericObject geoDept = (GenericObject)((GenericObject)c_CASL.c_object("Business.Department.of")).get(strDeptID);
      GenericObject geoOffsetGL;

      RecordMode rec_mode = RecordMode.none;
      if (Convert.ToDouble(_geoTran.get("TRANAMT", "").ToString()) < 0)
      {
        geoOffsetGL = geoDept.get("gl_nbr_short", null) as GenericObject;
        rec_mode = RecordMode.Shortage;
      }
      else
      {
        geoOffsetGL = geoDept.get("gl_nbr_over", null) as GenericObject;
        rec_mode = RecordMode.Overage;
      }

      // set _dic_gl_info
      _dic_gl_info = new Dictionary<string, string>();
      if (geoOffsetGL != null)
      {
        string gl = "";
        for (int i = 0; i < geoOffsetGL.getIntKeyLength(); i++)
        {
          _dic_gl_info.Add(i.ToString(), geoOffsetGL.get(i).ToString()); // gl segment

          gl += geoOffsetGL.get(i).ToString();
          gl += "|";
        }
        gl = gl.TrimEnd('|');

        _dic_gl_info.Add("gl_nbr", gl); // CBU is using "gl_nbr" to save gl number string
      }



      BatchUpdateLineRec line_rec = new BatchUpdateLineRec();
      line_rec.lstRec = new List<RecordItem>();
      line_rec.dic_groupby_data = new Dictionary<string, GroupbyData<GenericObject>>();

      // about configuration
      ILookup<int, BatchRecord> lookup_config = lst_bu_rec_config.ToLookup(o => o.seq_order);
      List<int> lstSeqOrder = lookup_config.Select(o => o.Key).ToList();
      lstSeqOrder.Sort();

      foreach (int seq_order in lstSeqOrder) // usuaaly have one record, combination will have more than one records
      {
        string rec_str = "";
        List<BatchRecord> lstConfigRec = lookup_config[seq_order].ToList();
        foreach (BatchRecord rec in lstConfigRec)
        {
          // if record mode / record type (detail, summary, overrage, ... ) is specificed
          if (rec.record_type != null && !string.IsNullOrEmpty(rec.record_type.value))
          {
            // for same record mode (here record_type is should be one of detail, summary, overrage ...
            if (string.Compare(rec_mode.ToString(), rec.record_type.value, true) == 0)
            {
              // if combined set as true, combined value together, 
              // otherwise if pre value is empty, use next value configured. not combined
              if (rec.combined_value == true)
              {
                rec_str += " ";
                rec_str += FormatRecord(rec);
              }
              else
              {
                rec_str.Trim();
                if (string.IsNullOrEmpty(rec_str))
                  rec_str = FormatRecord(rec);

              }
            }
          }
          else
          {
            // if combined set as true, combined value together, 
            // otherwise if pre value is empty, use next value configured. not combined
            if (rec.combined_value == true)
            {
              rec_str += " ";
              rec_str += FormatRecord(rec);
            }
            else
            {
              rec_str.Trim();
              if (string.IsNullOrEmpty(rec_str))
                rec_str = FormatRecord(rec);

            }
          }
        }

        rec_str = rec_str.Trim();
        // format value based on the type of fixed length, variable length, space or zero filled
        rec_str = rec_str.FormatValue(_config_data.general_info.record_type.value, lstConfigRec[0]);

        // get configuration
        BatchRecord config_rec = lstConfigRec[0];
        RecordItem detail_rec_item = new RecordItem();
        detail_rec_item.seq_order = seq_order;

        if (string.Compare(config_rec.source_type.value, "Fixed_Value", true) == 0)
          detail_rec_item.tag = "Fixed_Value";
        else
          detail_rec_item.tag = config_rec.source.value;

        detail_rec_item.value = rec_str;
        detail_rec_item.rec_config = config_rec;
        line_rec.lstRec.Add(detail_rec_item);
      }

      // save layer info for later use
      line_rec.layer = layer;
      if (_geoTranItem != null)
        line_rec.amt = Convert.ToDouble(_geoTranItem.get("TOTAL", "").ToString());
      else
        line_rec.amt = Convert.ToDouble(_geoTran.get("TRANAMT", "").ToString());
      line_rec.gl_nbr = _dic_gl_info.ContainsKey("gl_nbr") ? _dic_gl_info["gl_nbr"] : "";
      line_rec.record_mode = rec_mode; // one of summary, detail, overage, shortage...

      // for potential summarization
      line_rec.pay_file_name = _geoFile.GetCoreFileName("");
      line_rec.dept_id = _geoFile.GetWorkgroupId();
      line_rec.rcpt_ref_nbr = _geoTran.GetReceiptRefNbr("");
      line_rec.tt_id = _geoTran.GetTTID();
      line_rec.trans_post_date = _geoTran.GetTransPostDate();

      // get groupby data
      line_rec.dic_groupby_data = GetGroupInfo(); // may remove later

      return line_rec;
    }

    protected string FormatRecord(BatchRecord rec)
    {
      string rec_str = "";
      string source_type = rec.source_type.value;
      if (string.Compare(source_type, "STD_DATA", true) == 0)
      {
        rec_str = FormatStandardDataField(rec);
      }
      else if (string.Compare(source_type, "FIXED_VALUE", true) == 0)
      {
        rec_str = rec.fixed_value;
      }
      else if (string.Compare(source_type, "CUSTOM_FIELD_DATA", true) == 0)
      {
        rec_str = _geoTran.GetCustomFieldValue(rec.source.value); // rec.source.value contains custom field tag name
      }
      else if (string.Compare(source_type, "GL_NUMBER", true) == 0)
      {
        rec_str = _dic_gl_info.GetGLValue(rec.source.value);
      }
      return rec_str;
    }

    protected string FormatStandardDataField(BatchRecord rec)
    {
      string rec_str = "";
      string source = rec.source.value;
      if (string.Compare(source, "Core_File_Nbr", true) == 0)
      {
        rec_str = _geoFile.GetCoreFileName(rec.format.value);
      }
      else if (string.Compare(source, "Receipt_Reference_Nbr", true) == 0)
      {
        rec_str = _geoTran.GetReceiptRefNbr(rec.format.value);
      }
      else if (string.Compare(source, "Trans_Reference_Nbr", true) == 0)
      {
        rec_str = _geoTran.GetTransactionRefNbr(rec.format.value);
      }
      else if (string.Compare(source, "Batch_Create_Date", true) == 0) // file open date
      {
        DateTime date = DateTime.Now;
        rec_str = date.FormatDate(rec.format.value);
      }
      else if (string.Compare(source, "Batch_Create_Time", true) == 0) // file open date
      {
        DateTime date = DateTime.Now;
        rec_str = date.FormatTime(rec.format.value);
      }
      else if (string.Compare(source, "File_Open_Date", true) == 0) // file open date
      {
        DateTime date;
        string date_str = _geoFile.get("OPENDT", "").ToString();
        if (!string.IsNullOrEmpty(date_str))
          date = DateTime.Parse(date_str);
        else
          date = DateTime.Now;
        rec_str = date.FormatDate(rec.format.value);
      }
      else if (string.Compare(source, "File_Open_Time", true) == 0) // file open date
      {
        DateTime date;
        string date_str = _geoFile.get("OPENDT", "").ToString();
        if (!string.IsNullOrEmpty(date_str))
          date = DateTime.Parse(date_str);
        else
          date = DateTime.Now;
        rec_str = date.FormatTime(rec.format.value);
      }
      else if (string.Compare(source, "File_Effective_Date", true) == 0)
      {
        DateTime date;
        string date_str = _geoFile.get("EFFECTIVEDT", "").ToString();
        if (!string.IsNullOrEmpty(date_str))
          date = DateTime.Parse(date_str);
        else
          date = DateTime.Now;
        rec_str = date.FormatDate(rec.format.value);
      }
      else if (string.Compare(source, "File_Effective_Time", true) == 0)
      {
        DateTime date;
        string date_str = _geoFile.get("EFFECTIVEDT", "").ToString();
        if (!string.IsNullOrEmpty(date_str))
          date = DateTime.Parse(date_str);
        else
          date = DateTime.Now;
        rec_str = date.FormatTime(rec.format.value);
      }
      else if (string.Compare(source, "Trans_Date", true) == 0)
      {
        DateTime date;
        string date_str = _geoTran.get("POSTDT", "").ToString();
        if (!string.IsNullOrEmpty(date_str))
          date = DateTime.Parse(date_str);
        else
          date = DateTime.Now;
        rec_str = date.FormatDate(rec.format.value);
      }
      else if (string.Compare(source, "Trans_Time", true) == 0)
      {
        DateTime date;
        string date_str = _geoTran.get("POSTDT", "").ToString();
        if (!string.IsNullOrEmpty(date_str))
          date = DateTime.Parse(date_str);
        else
          date = DateTime.Now;
        rec_str = date.FormatTime(rec.format.value);
      }
      else if (string.Compare(source, "User_Name", true) == 0)
      {
        rec_str = _geoFile.GetFileOpenUserName();
      }
      else if (string.Compare(source, "Workgroup_Id", true) == 0)
      {
        rec_str = _geoFile.GetWorkgroupId();
      }
      else if (string.Compare(source, "Workgroup_Name", true) == 0)
      {
        rec_str = _geoFile.GetWorkgroupDescription();
      }
      else if (string.Compare(source, "Transaction_Type_Id", true) == 0)
      {
        rec_str = _geoTran.GetTTID();
      }
      else if (string.Compare(source, "Transaction_Type_Description", true) == 0)
      {
        rec_str = _geoTran.GetTTDesc();
      }
      else if (string.Compare(source, "Trans_Amount", true) == 0 ||
               string.Compare(source, "Trans_Amount_sign_reversal", true) == 0 ||
               string.Compare(source, "Trans_Absolute_Amount") == 0)
      {
        double amount = 0;
        if (_geoTranItem != null)
          amount = Convert.ToDouble(_geoTranItem.get("TOTAL", "").ToString());
        else
          amount = Convert.ToDouble(_geoTran.get("TRANAMT", "").ToString());

        if (string.Compare(source, "Trans_Amount_sign_reversal", true) == 0)
          amount = amount * -1;
        rec_str = amount.FormatAmount(rec.format.value).FormatValue(_config_data.general_info.record_type.value, rec);

      }
      else if (string.Compare(source, "Debit_Credit_Indicator", true) == 0)
      {
        double amount = 0;
        if (_geoTranItem != null)
          amount = Convert.ToDouble(_geoTranItem.get("TOTAL", "").ToString());
        else
          amount = Convert.ToDouble(_geoTran.get("TRANAMT", "").ToString());

        rec_str = amount.FormatDebitCreditIndicator(rec.format.value);
      }
      else if (string.Compare(source, "Record_Sequence_Number", true) == 0) // IPAY-1262
      {
        // placeholder, it will be modified later
        rec_str = "0";
      }
      else if (string.Compare(source, "Payment_Method", true) == 0) // IPAY-1261
      {
        rec_str = _geoTenders_Applied.GetPaymentMethod(rec.format.value);
      }

      return rec_str;
    }

    /// <summary>
    /// Get Transaction Type Record Mode / element mode, for example, this transaction type is only for summary mode or detail mode, see general info
    /// see Chester Field project, for example.
    /// </summary>
    /// <returns></returns>
    protected RecordMode GetTTRecordMode() // summary, detail, or none
    {
      RecordMode record_mode = RecordMode.none;
      bool summary_mode = false;
      bool detail_mode = false;

      // check if summary mode
      if (_config_data.general_info.summary_mode.mode == true) // contain summary
      {
        summary_mode = true;
        // any condition?
        if (_config_data.general_info.summary_mode.filters.Count > 0)
        {
          foreach (filter f in _config_data.general_info.summary_mode.filters)
          {
            string str_value = GetFilterSourceValue(f);

            // check if condition is satisfied
            if (string.Compare(f.logic, "and", true) == 0)
              summary_mode = summary_mode && IsConditionSatisfy(str_value, f);
            else if (string.Compare(f.logic, "or", true) == 0)
              summary_mode = summary_mode || IsConditionSatisfy(str_value, f);
            else // null as "and"
            {
              summary_mode = summary_mode && IsConditionSatisfy(str_value, f);
            }
          }
        }

      }

      // check if detail _mode
      if (_config_data.general_info.detail_mode.mode == true) // contain detail
      {
        detail_mode = true;
        // any condition?
        if (_config_data.general_info.detail_mode.filters.Count > 0)
        {
          foreach (filter f in _config_data.general_info.detail_mode.filters)
          {
            string str_value = GetFilterSourceValue(f);

            if (string.Compare(f.logic, "and", true) == 0)
              detail_mode = detail_mode && IsConditionSatisfy(str_value, f);
            else if (string.Compare(f.logic, "or", true) == 0)
              detail_mode = detail_mode || IsConditionSatisfy(str_value, f);
            else // null as "and"
            {
              detail_mode = detail_mode && IsConditionSatisfy(str_value, f);
            }
          }
        }

      }

      if (summary_mode && !detail_mode)
        record_mode = RecordMode.summary;
      else if (!summary_mode && detail_mode)
        record_mode = RecordMode.detail;
      else
        record_mode = RecordMode.none;

      return record_mode;
    }

    protected string GetFilterSourceValue(filter f)
    {
      string str_value = "";
      if (string.Compare(f.source_type, "std_data", true) == 0)
      {
        if (string.Compare(f.source, "Workgroup_Id", true) == 0)
          str_value = _geoFile.GetWorkgroupId();
        else if (string.Compare(f.source, "Workgroup_Name", true) == 0)
          str_value = _geoFile.GetWorkgroupDescription();
        else if (string.Compare(f.source, "Transaction_Type_Id", true) == 0)
          str_value = _geoTran.GetTTID();
        else if (string.Compare(f.source, "Transaction_Type_Description", true) == 0)
          str_value = _geoTran.GetTTDesc();
      }
      else if (string.Compare(f.source_type, "Custom_Field_Data", true) == 0)
      {
        str_value = _geoTran.GetCustomFieldValue(f.source); // f.source is custom field tag name

      }
      else if (string.Compare(f.source_type, "GL_Number", true) == 0)
      {
        str_value = _dic_gl_info.GetGLValue(f.source);
      }

      return str_value;
    }
    protected bool IsConditionSatisfy(string value, filter f)
    {
      bool rst = false;
      double nbr = 0;
      double filter_nbr = 0;
      bool isNumeric = double.TryParse(value, out nbr) && double.TryParse(f.value, out filter_nbr);
      switch (f.condition)
      {
        case ">":
          if (isNumeric)
            rst = nbr > filter_nbr ? true : false;
          else
            rst = string.Compare(value, f.value, true) > 0 ? true : false;
          break;
        case ">=":
          if (isNumeric)
            rst = nbr >= filter_nbr ? true : false;
          else
            rst = string.Compare(value, f.value, true) >= 0 ? true : false;
          break;
        case "<":
          if (isNumeric)
            rst = nbr < filter_nbr ? true : false;
          else
            rst = string.Compare(value, f.value, true) < 0 ? true : false;
          break;
        case "<=":
          if (isNumeric)
            rst = nbr <= filter_nbr ? true : false;
          else
            rst = string.Compare(value, f.value, true) <= 0 ? true : false;
          break;
        case "=":
          if (isNumeric)
            rst = nbr == filter_nbr ? true : false;
          else
            rst = string.Compare(value, f.value, true) == 0 ? true : false;
          break;
        case "!=":
          if (isNumeric)
            rst = nbr != filter_nbr ? true : false;
          else
            rst = string.Compare(value, f.value, true) != 0 ? true : false;
          break;

      }
      return rst;
    }

    protected Dictionary<string, GroupbyData<GenericObject>> GetGroupInfo()
    {
      Dictionary<string, GroupbyData<GenericObject>> dic_group_info = new Dictionary<string, GroupbyData<GenericObject>>();
      foreach (string outline_level in _config_data.general_info.layer_outlines)
      {
        // only add for outline which contains layer information
        if (_config_data.general_info.layers.HasOutline(outline_level))
        {
          if (string.Compare(outline_level, nameof(LayerType.Detail_Level_Layer), true) != 0)
          {
            GroupbyData<GenericObject> data = new GroupbyData<GenericObject>();
            data.layer_name = outline_level;
            if (string.Compare(outline_level, nameof(LayerType.Batch_Level_Layer), true) == 0)
            {
              data.tag = "";
              data.str_value = "";
              data.geo_value = null;
            }
            else if (string.Compare(outline_level, nameof(LayerType.CoreFile_Level_Layer), true) == 0)
            {
              data.tag = "core_file";
              data.str_value = _geoFile.GetCoreFileName(null);
              data.geo_value = _geoFile;
            }
            else if (string.Compare(outline_level, nameof(LayerType.WG_Level_Layer), true) == 0)
            {
              data.tag = "workgroup";
              data.str_value = _geoFile.GetWorkgroupId();
              data.geo_value = null;
            }
            else if (string.Compare(outline_level, nameof(LayerType.GL_Level_Layer), true) == 0)
            {
              data.tag = _config_data.general_info.layers.GetGLLayerConfiguredGLSegmentName();
              data.str_value = _dic_gl_info.GetGLValue(data.tag); ;
              data.geo_value = null;

            }
            else if (string.Compare(outline_level, nameof(LayerType.TT_Level_Layer), true) == 0)
            {
              data.tag = "tt";
              data.str_value = _geoTran.GetTTID();
              data.geo_value = _geoTran;
            }
            else if (string.Compare(outline_level, nameof(LayerType.CF_Level_Layer), true) == 0)
            {
              data.tag = _config_data.general_info.layers.GetCustomFieldLayerConfiguredFieldTagName();
              data.str_value = _geoTran.GetCustomFieldValue(data.tag);
              data.geo_value = null;
            }

            dic_group_info.Add(outline_level, data);
          }
        }


      }

      return dic_group_info;
    }

  }
}
