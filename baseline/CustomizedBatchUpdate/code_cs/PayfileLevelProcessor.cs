﻿using CASL_engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomizedBatchUpdate
{
  class PayfileLevelProcessor
  {
    private BatchUpdateConfigData _config_data { get; set; }
    private GenericObject _geoFile { get; set; }
    private long _total_count { get; set; }
    private double _total_amount { get; set; }
    private List<RecordGroupInfo> _lst_parent_layer { get; set; }

    public PayfileLevelProcessor(BatchUpdateConfigData config_data, long total_count, double total_amount, List<RecordGroupInfo> lst_parent_layer)
    {
      _config_data = config_data;
      _geoFile = null;
      _total_amount = total_amount;
      _total_count = total_count;
      _lst_parent_layer = lst_parent_layer;

      // I need get _geoFile
      foreach (RecordGroupInfo layer in lst_parent_layer)
      {
        if (layer.layer_name == nameof(LayerType.CoreFile_Level_Layer))
        {
          _geoFile = layer.geo_value;
          break;
        }
      }
    }

    public Dictionary<string, BatchUpdateLineRec> Process()
    {
      Dictionary<string, BatchUpdateLineRec> dicRec = new Dictionary<string, BatchUpdateLineRec>();

      if (_config_data.bu_ht_info != null)
      {

        foreach (KeyValuePair<string, List<BatchRecord>> pair in _config_data.bu_ht_info)
        {
          // get layer info pair.key is layer.id
          _config_data.general_info.layers = _config_data.general_info.layers ?? new List<Layer>();
          foreach (Layer layer in _config_data.general_info.layers)
          {
            if (layer.id == pair.Key && string.Compare(layer.layer_type.value, nameof(LayerType.CoreFile_Level_Layer), true) == 0)
            {
              BatchUpdateLineRec line_rec = FormatLineRecord( pair.Value);
              dicRec.Add(layer.id,line_rec);
            }
          }
        }
      }

      return dicRec;
    }

    protected BatchUpdateLineRec FormatLineRecord(List<BatchRecord> lst_bu_rec_config)
    {
      BatchUpdateLineRec line_rec = new BatchUpdateLineRec();
      line_rec.lstRec = new List<RecordItem>(); // data values saved in recorditem

      // about configuration
      ILookup<int, BatchRecord> lookup_config = lst_bu_rec_config.ToLookup(o => o.seq_order);

      if (lookup_config != null)
      {
        List<int> lstSeqOrder = lookup_config.Select(o => o.Key).ToList();
        lstSeqOrder.Sort();

        string workgourp_id = GetWorkgroupId();
        foreach (int seq_order in lstSeqOrder) // usuaaly have one record, combination will have more than one records
        {
          string rec_str = "";
          List<BatchRecord> lstConfigRec = lookup_config[seq_order].ToList();
          foreach (BatchRecord rec in lstConfigRec)
          {
            // sometime value only for specific workgroup
            if (rec.workgroup != null && !string.IsNullOrEmpty(rec.workgroup.value))
            {
              if (string.Compare(rec.workgroup.value, workgourp_id, true) == 0)
              {
                // if combined set as true, combined value together, 
                // otherwise if pre value is empty, use next value configured. not combined
                if (rec.combined_value)
                {
                  rec_str += " ";
                  rec_str += FormatRecord(rec);
                }
                else
                {
                  rec_str.Trim();
                  if (string.IsNullOrEmpty(rec_str))
                    rec_str = FormatRecord(rec);
                }
              }
            }
            else
            {
              // if combined set as true, combined value together, 
              // otherwise if pre value is empty, use next value configured. not combined
              if (rec.combined_value)
              {
                rec_str += " ";
                rec_str += FormatRecord(rec);
              }
              else
              {
                rec_str.Trim();
                if (string.IsNullOrEmpty(rec_str))
                  rec_str = FormatRecord(rec);
              }
            }
          }

          rec_str = rec_str.Trim();
          // format value based on fixed lenth, ... do not append dilimiter yet;  
          rec_str = rec_str.FormatValue(_config_data.general_info.record_type.value, lstConfigRec[0]);

          // set record item
          RecordItem rec_item = new RecordItem();
          // sequence order
          rec_item.seq_order = seq_order;
          // tag source value
          if (string.Compare(lstConfigRec[0].source_type.value, "Fixed_Value", true) == 0)
            rec_item.tag = "Fixed_Value";
          else
            rec_item.tag = lstConfigRec[0].source.value;
          rec_item.value = rec_str;
          rec_item.rec_config = lstConfigRec[0];
          line_rec.lstRec.Add(rec_item);
        }

        line_rec.amt = _total_amount;
      }

      return line_rec;
    }

    protected string FormatRecord(BatchRecord rec)
    {
      string rec_str = "";
      string source_type = rec.source_type.value;
      if (string.Compare(source_type, "STD_DATA", true) == 0)
      {
        rec_str = FormatStandardDataField(rec);
      }
      else if (string.Compare(source_type, "FIXED_VALUE", true) == 0)
      {
        rec_str = rec.fixed_value;
      }
      return rec_str;
    }

    protected string FormatStandardDataField(BatchRecord rec)
    {
      string rec_str = "";
      string source = rec.source.value;
      if (string.Compare(source, "Core_File_Nbr", true) == 0)
      {
        rec_str = _geoFile.GetCoreFileName(rec.format.value);
      }
      else if (string.Compare(source, "File_Open_Date", true) == 0 || string.Compare(source, "Post_Date", true) == 0) // file open date
      {
        DateTime date = DateTime.Now;
        if (_geoFile != null)
        {
          string date_str = _geoFile.get("OPENDT", "").ToString();
          if (!string.IsNullOrEmpty(date_str))
            date = DateTime.Parse(date_str);
          else
            date = DateTime.Now;
        }

        rec_str = date.FormatDate(rec.format.value);
      }
      else if (string.Compare(source, "File_Open_Time", true) == 0 || string.Compare(source, "Post_Time", true) == 0) // file open date
      {
        DateTime date = DateTime.Now;
        if (_geoFile != null)
        {
          string date_str = _geoFile.get("OPENDT", "").ToString();
          if (!string.IsNullOrEmpty(date_str))
            date = DateTime.Parse(date_str);
          else
            date = DateTime.Now;
        }

        rec_str = date.FormatTime(rec.format.value);
      }
      else if (string.Compare(source, "File_Effective_Date", true) == 0)
      {
        DateTime date = DateTime.Now;
        if (_geoFile != null)
        {
          string date_str = _geoFile.get("EFFECTIVEDT", "").ToString();
          if (!string.IsNullOrEmpty(date_str))
            date = DateTime.Parse(date_str);
          else
            date = DateTime.Now;
        }

        rec_str = date.FormatDate(rec.format.value);
      }
      else if (string.Compare(source, "File_Effective_Time", true) == 0)
      {
        DateTime date = DateTime.Now;
        if (_geoFile != null)
        {
          string date_str = _geoFile.get("EFFECTIVEDT", "").ToString();
          if (!string.IsNullOrEmpty(date_str))
            date = DateTime.Parse(date_str);
          else
            date = DateTime.Now;
        }

        rec_str = date.FormatTime(rec.format.value);
      }
      else if (string.Compare(source, "Batch_Create_Date", true) == 0)
      {
        DateTime date = DateTime.Now;
        rec_str = date.FormatDate(rec.format.value);
      }
      else if (string.Compare(source, "Batch_Create_Time", true) == 0)
      {
        DateTime date = DateTime.Now;
        rec_str = date.FormatTime(rec.format.value);
      }
      else if (string.Compare(source, "User_Name", true) == 0)
      {
        rec_str = _geoFile.GetFileOpenUserName();
      }
      else if (string.Compare(source, "Workgroup_Id", true) == 0)
      {
        rec_str = _geoFile.GetWorkgroupId();
      }
      else if (string.Compare(source, "Workgroup_Name", true) == 0)
      {
        rec_str = _geoFile.GetWorkgroupDescription();
      }
      else if (string.Compare(source, "Total_Amount", true) == 0 || 
        string.Compare(source, "Total_Absolute_Amount", true) == 0 ||
        string.Compare(source, "Total_Amount_sign_reversal", true) == 0)
      {
        double amount = string.Compare(source, "Total_Amount_sign_reversal", true) == 0 ? _total_amount *-1:_total_amount;
        rec_str = amount.FormatAmount(rec.format.value).FormatValue(_config_data.general_info.record_type.value, rec);
      }
      else if (string.Compare(source, "Total_Count", true) == 0)
      {
        rec_str = _total_count.ToString();
      }

      return rec_str;
    }

    protected string GetWorkgroupId()
    {
      return _geoFile.GetWorkgroupId();
    }
  }
}
