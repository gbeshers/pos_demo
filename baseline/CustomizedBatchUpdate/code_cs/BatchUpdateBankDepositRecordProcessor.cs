﻿using CASL_engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomizedBatchUpdate.code_cs
{
  /// <summary>
  /// Used for process bank deposits, it is formated as BatchUpdateLineRec
  /// </summary>
  class BatchUpdateBankDepositRecordProcessor
  {
    private BatchUpdateConfigData _config_data { get; set; }
    private GenericObject _geoFile { get; set; }
    private GenericObject _geoDeposit { get; set; }
    private Dictionary<string, string> _dic_gl_info { get; set; }


    public BatchUpdateBankDepositRecordProcessor(BatchUpdateConfigData config_data, GenericObject geoFile, GenericObject geoDeposit)
    {
      _config_data = config_data;
      _geoFile = geoFile;
      _geoDeposit = geoDeposit;

      string tempBank = string.Format("Business.Bank_account.of.\"{0}\"", geoDeposit.get("BANKID", "").ToString());
      GenericObject bank = (GenericObject)((GenericObject)c_CASL.c_object(tempBank));
      GenericObject geoDepositGL = bank.get("GL_nbr", null) as GenericObject;

      _dic_gl_info = new Dictionary<string, string>();
      if (geoDepositGL != null)
      {
        string gl = "";
        for (int i = 0; i < geoDepositGL.getIntKeyLength(); i++)
        {
          _dic_gl_info.Add(i.ToString(), geoDepositGL.get(i).ToString());
          gl += geoDepositGL.get(i).ToString();
          gl += "|";
        }
        gl = gl.TrimEnd('|');

        _dic_gl_info.Add("gl_nbr", gl); // CBU is using "gl_nbr" to save gl number string
      }

    }

    public Dictionary<string, BatchUpdateLineRec> Process()
    {
      Dictionary<string, BatchUpdateLineRec> dic_rec = new Dictionary<string, BatchUpdateLineRec>();

      if (_config_data.general_info.include_bank_deposit)
      {
        if (_config_data.bu_detail_info != null)
        {
          foreach (KeyValuePair<string, List<BatchRecord>> pair in _config_data.bu_detail_info)
          {
            // get layer info pair.key is layer.id
            _config_data.general_info.layers = _config_data.general_info.layers ?? new List<Layer>();
            foreach (Layer layer in _config_data.general_info.layers)
            {
              if (layer.id == pair.Key && string.Compare(layer.layer_type.value, nameof(LayerType.Detail_Level_Layer), true) == 0)
              {
                BatchUpdateLineRec line_rec = FormatLineRecord(layer, pair.Value);

                if (line_rec.lstRec.Count > 0)
                {
                  dic_rec.Add(layer.id, line_rec);
                }
              }
            }
          }
        }
      }

      return dic_rec;
    }
    public BatchDetailRecord Process2()
    {
      BatchDetailRecord batch_detail_rec = new BatchDetailRecord();

      if (_config_data.general_info.include_bank_deposit)
      {
        if (_config_data.bu_detail_info != null)
        {
          foreach (KeyValuePair<string, List<BatchRecord>> pair in _config_data.bu_detail_info)
          {
            // get layer info pair.key is layer.id
            _config_data.general_info.layers = _config_data.general_info.layers ?? new List<Layer>();
            foreach (Layer layer in _config_data.general_info.layers)
            {
              if (layer.id == pair.Key && string.Compare(layer.layer_type.value, nameof(LayerType.Detail_Level_Layer), true) == 0)
              {
                BatchUpdateLineRec line_rec = FormatLineRecord(layer, pair.Value);

                if (line_rec.lstRec.Count > 0)
                {
                  batch_detail_rec.dicLineRec.Add(layer.id, line_rec);
                }
              }
            }
          }
        }

        batch_detail_rec.dic_groupby_data = GetGroupInfo();
      }

      return batch_detail_rec;
    }

    // format bank deposit record
    public BatchUpdateLineRec FormatLineRecord(Layer layer, List<BatchRecord> lst_bu_rec_config)
    {
      BatchUpdateLineRec line_rec = new BatchUpdateLineRec();
      line_rec.lstRec = new List<RecordItem>();
      line_rec.dic_groupby_data = new Dictionary<string, GroupbyData<GenericObject>>();

      // group configuration by sequence order
      ILookup<int, BatchRecord> lookup = lst_bu_rec_config.ToLookup(o => o.seq_order);
      List<int> lstSeqOrder = lookup.Select(o => o.Key).ToList();
      lstSeqOrder.Sort();

      RecordMode rec_mode = RecordMode.Bank_Deposit;
      foreach (int seq_order in lstSeqOrder) // usuaaly have one record, combination will have more than one records
      {
        string rec_str = "";
        List<BatchRecord> lstConfigRec = lookup[seq_order].ToList();
        foreach (BatchRecord rec in lstConfigRec)
        {
          // if record mode / record type (detail, summary, overrage, ... ) is specificed
          if (rec.record_type != null && !string.IsNullOrEmpty(rec.record_type.value))
          {
            // for same record mode (here record_type is should be bank_deposit
            if (string.Compare(rec_mode.ToString(), rec.record_type.value, true) == 0)
            {
              // if combined set as true, combined value together, 
              // otherwise if pre value is empty, use next value configured. not combined
              if (rec.combined_value)
              {
                rec_str += " ";
                rec_str += FormatDetailRec(rec);
              }
              else
              {
                rec_str.Trim();
                if (string.IsNullOrEmpty(rec_str))
                  rec_str = FormatDetailRec(rec);
              }
            }
          }
          else
          {
            // if combined set as true, combined value together, 
            // otherwise if pre value is empty, use next value configured. not combined
            if (rec.combined_value)
            {
              rec_str += " ";
              rec_str += FormatDetailRec(rec);
            }
            else
            {
              rec_str.Trim();
              if (string.IsNullOrEmpty(rec_str))
                rec_str = FormatDetailRec(rec);
            }
          }
        } // end each

        rec_str = rec_str.Trim();
        // format value based on the type of fixed length, variable length, space or zero filled
        rec_str = rec_str.FormatValue(_config_data.general_info.record_type.value, lstConfigRec[0]);

        // get configuration
        BatchRecord config_rec = lstConfigRec[0];
        RecordItem detail_rec_item = new RecordItem();
        detail_rec_item.seq_order = seq_order;

        if (string.Compare(config_rec.source_type.value, "Fixed_Value", true) == 0)
          detail_rec_item.tag = "Fixed_Value";
        else
          detail_rec_item.tag = config_rec.source.value;

        detail_rec_item.value = rec_str;
        detail_rec_item.rec_config = config_rec;
        line_rec.lstRec.Add(detail_rec_item);
      }

      // save layer info for later use
      line_rec.layer = layer;
      // I needed add payfile number, reference number, group id, gl number, transaction discription for later use
      line_rec.pay_file_name = _geoFile.GetCoreFileName(null);
      line_rec.rcpt_ref_nbr = "";
      line_rec.dept_id = _geoFile.GetWorkgroupId();
      line_rec.tt_id = _geoDeposit.get("BANKID", "").ToString(); //"Deposit";
      line_rec.trans_post_date = _geoDeposit.GetBankDepositPostDate();
      line_rec.cust_field_value = "";
      line_rec.amt = Convert.ToDouble(_geoDeposit.get("AMOUNT", "").ToString());
      line_rec.gl_nbr = _dic_gl_info.ContainsKey("gl_nbr") ? _dic_gl_info["gl_nbr"] : "";
      line_rec.record_mode = rec_mode; // deposit

      // get groupby data
      line_rec.dic_groupby_data = GetGroupInfo(); // I may remove it later
      return line_rec;
    }

    protected string FormatDetailRec(BatchRecord rec)
    {
      string rec_str = "";
      string source_type = rec.source_type.value;
      if (string.Compare(source_type, "STD_DATA", true) == 0)
      {
        rec_str = FormatStandardDataField(rec);
      }
      else if (string.Compare(source_type, "FIXED_VALUE", true) == 0)
      {
        rec_str = rec.fixed_value;
      }
      else if (string.Compare(source_type, "CUSTOM_FIELD_DATA", true) == 0)
      {
        rec_str = "";   // set empty for deposit record
      }
      else if (string.Compare(source_type, "GL_NUMBER", true) == 0)
      {
        rec_str = _dic_gl_info.GetGLValue(rec.source.value);
      }

      return rec_str;
    }

    protected string FormatStandardDataField(BatchRecord rec)
    {
      string rec_str = "";
      string source = rec.source.value;
      if (string.Compare(source, "Core_File_Nbr", true) == 0)
      {
        rec_str = _geoFile.GetCoreFileName(rec.format.value);
      }
      else if (string.Compare(source, "Receipt_Reference_Nbr", true) == 0)
      {
        rec_str = "";   // set empty for deposit record
      }
      else if (string.Compare(source, "Trans_Reference_Nbr", true) == 0)
      {
        rec_str = "";   // set empty for deposit record
      }
      else if (string.Compare(source, "Batch_Create_Date", true) == 0) // file open date
      {
        DateTime date = DateTime.Now;
        rec_str = date.FormatDate(rec.format.value);
      }
      else if (string.Compare(source, "Batch_Create_Time", true) == 0) // file open date
      {
        DateTime date = DateTime.Now;
        rec_str = date.FormatTime(rec.format.value);
      }
      else if (string.Compare(source, "File_Open_Date", true) == 0) // file open date
      {
        DateTime date;
        string date_str = _geoFile.get("OPENDT", "").ToString();
        if (!string.IsNullOrEmpty(date_str))
          date = DateTime.Parse(date_str);
        else
          date = DateTime.Now;
        rec_str = date.FormatDate(rec.format.value);
      }
      else if (string.Compare(source, "File_Open_Time", true) == 0) // file open date
      {
        DateTime date;
        string date_str = _geoFile.get("OPENDT", "").ToString();
        if (!string.IsNullOrEmpty(date_str))
          date = DateTime.Parse(date_str);
        else
          date = DateTime.Now;
        rec_str = date.FormatTime(rec.format.value);
      }
      else if (string.Compare(source, "File_Effective_Date", true) == 0)
      {
        DateTime date;
        string date_str = _geoFile.get("EFFECTIVEDT", "").ToString();
        if (!string.IsNullOrEmpty(date_str))
          date = DateTime.Parse(date_str);
        else
          date = DateTime.Now;
        rec_str = date.FormatDate(rec.format.value);
      }
      else if (string.Compare(source, "File_Effective_Time", true) == 0)
      {
        DateTime date;
        string date_str = _geoFile.get("EFFECTIVEDT", "").ToString();
        if (!string.IsNullOrEmpty(date_str))
          date = DateTime.Parse(date_str);
        else
          date = DateTime.Now;
        rec_str = date.FormatTime(rec.format.value);
      }
      else if (string.Compare(source, "Trans_Date", true) == 0)
      {
        DateTime date;
        string date_str = _geoDeposit.get("POSTDT", "").ToString(); // I should use deposit "POSTDT" (?)
        if (!string.IsNullOrEmpty(date_str))
          date = DateTime.Parse(date_str);
        else
          date = DateTime.Now;
        rec_str = date.FormatDate(rec.format.value);
      }
      else if (string.Compare(source, "Trans_Time", true) == 0)
      {
        DateTime date;
        string date_str = _geoDeposit.get("POSTDT", "").ToString(); // I should use deposit "POSTDT" (?)
        if (!string.IsNullOrEmpty(date_str))
          date = DateTime.Parse(date_str);
        else
          date = DateTime.Now;
        rec_str = date.FormatTime(rec.format.value);
      }
      else if (string.Compare(source, "User_Name", true) == 0)
      {
        rec_str = _geoFile.GetFileOpenUserName();   // should I use [OWNERUSERID], or [CREATERUSERID] of Deposit (??)
      }
      else if (string.Compare(source, "Workgroup_Id", true) == 0)
      {
        rec_str = _geoFile.GetWorkgroupId();
      }
      else if (string.Compare(source, "Workgroup_Name", true) == 0)
      {
        rec_str = _geoFile.GetWorkgroupDescription();
      }
      else if (string.Compare(source, "Transaction_Type_Id", true) == 0)
      {
        rec_str = "";   // set empty for deposit record
      }
      else if (string.Compare(source, "Transaction_Type_Description", true) == 0)
      {
        rec_str = "";   // set empty for deposit record
      }
      else if (string.Compare(source, "Trans_Amount", true) == 0 ||
        string.Compare(source, "Trans_Absolute_Amount") == 0 ||
        string.Compare(source, "Trans_Amount_sign_reversal", true) == 0)
      {
        double amount = 0;
        amount = Convert.ToDouble(_geoDeposit.get("AMOUNT", "").ToString());
        if (string.Compare(source, "Trans_Amount_sign_reversal", true) == 0)
          amount = amount * -1;
        rec_str = amount.FormatAmount(rec.format.value).FormatValue(_config_data.general_info.record_type.value, rec);
      }
      else if (string.Compare(source, "Debit_Credit_Indicator", true) == 0)
      {
        double amount = 0;
        amount = Convert.ToDouble(_geoDeposit.get("AMOUNT", "").ToString());
        rec_str = amount.FormatDebitCreditIndicator(rec.format.value);
      }
      else if (string.Compare(source, "Bank_Deposit_Reference", true) == 0)
      {
        rec_str = _geoDeposit.GetBankDepositRefNbr();
      }
      else if (string.Compare(source, "Bank_Id", true) == 0)
      {
        rec_str = _geoDeposit.GetBankDepositBankId();
      }
      else if (string.Compare(source, "Bank_Name", true) == 0)
      {
        rec_str = _geoDeposit.GetBankDepositBankName();
      }

      return rec_str;
    }

    protected Dictionary<string, GroupbyData<GenericObject>> GetGroupInfo()
    {
      Dictionary<string, GroupbyData<GenericObject>> dic_group_info = new Dictionary<string, GroupbyData<GenericObject>>();
      foreach (string outline_level in _config_data.general_info.layer_outlines)
      {
        // only add for outline which contains layer information
        if (_config_data.general_info.layers.HasOutline(outline_level))
        {
          GroupbyData<GenericObject> data = new GroupbyData<GenericObject>();
          data.layer_name = outline_level;
          if (string.Compare(outline_level, nameof(LayerType.Batch_Level_Layer), true) == 0)
          {
            data.tag = "";
            data.str_value = "";
            data.geo_value = null;
          }
          else if (string.Compare(outline_level, nameof(LayerType.CoreFile_Level_Layer), true) == 0)
          {
            data.tag = "core_file";
            data.str_value = _geoFile.GetCoreFileName(null);
            data.geo_value = _geoFile;
          }
          else if (string.Compare(outline_level, nameof(LayerType.WG_Level_Layer), true) == 0)
          {
            data.tag = "workgroup";
            data.str_value = _geoFile.GetWorkgroupId();
            data.geo_value = null;
          }
          else if (string.Compare(outline_level, nameof(LayerType.GL_Level_Layer), true) == 0)
          {
            data.tag = _config_data.general_info.layers.GetGLLayerConfiguredGLSegmentName();
            data.str_value = _dic_gl_info.GetGLValue(data.tag); ;
            data.geo_value = null;

          }
          else if (string.Compare(outline_level, nameof(LayerType.TT_Level_Layer), true) == 0)
          {
            data.tag = "tt";
            data.str_value = "Bank Deposit";
            data.geo_value = null;
          }
          else if (string.Compare(outline_level, nameof(LayerType.CF_Level_Layer), true) == 0)
          {
            data.tag = _config_data.general_info.layers.GetCustomFieldLayerConfiguredFieldTagName();
            data.str_value = ""; // set empty for bank deposit
            data.geo_value = null;
          }

          dic_group_info.Add(outline_level, data);
        }

      }

      return dic_group_info;
    }
  }
}
