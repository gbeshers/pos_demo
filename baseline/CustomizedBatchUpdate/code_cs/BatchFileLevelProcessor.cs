﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomizedBatchUpdate
{
  class BatchFileLevelProcessor
  {
    private BatchUpdateConfigData _config_data { get; set; }
    private long _total_count { get; set; }
    private double _total_amount { get; set; }
    private List<RecordGroupInfo> _lst_parent_layer { get; set; }

    public BatchFileLevelProcessor(BatchUpdateConfigData config_data, long total_count, double total_amount, List<RecordGroupInfo> lst_parent_layer)
    {
      _config_data = config_data;
      _total_amount = total_amount;
      _total_count = total_count;
      _lst_parent_layer = lst_parent_layer;
    }

    public Dictionary<string, BatchUpdateLineRec> Process()
    {
      Dictionary<string, BatchUpdateLineRec> dicRec = new Dictionary<string, BatchUpdateLineRec>();

      if(_config_data.bu_ht_info != null)
      {
        foreach (KeyValuePair<string, List<BatchRecord>> pair in _config_data.bu_ht_info)
        {
          // get layer info pair.key is layer.id
          _config_data.general_info.layers = _config_data.general_info.layers ?? new List<Layer>();
          foreach(Layer layer in _config_data.general_info.layers)
          {
            if(layer.id == pair.Key && string.Compare(layer.layer_type.value, nameof(LayerType.Batch_Level_Layer), true) == 0)
            {
              BatchUpdateLineRec line_rec = FormatLineRecord(pair.Value);
              dicRec.Add(layer.id, line_rec);

            }
          }
        }
      }
      return dicRec;
    }
    protected BatchUpdateLineRec FormatLineRecord(List<BatchRecord> lst_bu_rec_config )
    {
      BatchUpdateLineRec line_rec = new BatchUpdateLineRec();
      line_rec.lstRec = new List<RecordItem>(); // data values saved in recorditem

      // about configuration
      ILookup<int, BatchRecord> lookup_config = lst_bu_rec_config.ToLookup(o => o.seq_order);

      if(lookup_config != null)
      {
        List<int> lstSeqOrder = lookup_config.Select(o => o.Key).ToList();
        lstSeqOrder.Sort();

        foreach (int seq_order in lstSeqOrder) // usuaaly have one record, combination will have more than one records
        {
          string rec_str = "";
          List<BatchRecord> lstConfigRec = lookup_config[seq_order].ToList();
          foreach (BatchRecord rec in lstConfigRec)
          {
            // if combined set as true, combined value together, 
            // otherwise if pre value is empty, use next value configured. not combined
            if (rec.combined_value)
            {
              rec_str += " ";
              rec_str += FormatRecord(rec);
            }
            else
            {
              rec_str.Trim();
              if (string.IsNullOrEmpty(rec_str))
                rec_str = FormatRecord(rec);
            }

          }

          rec_str = rec_str.Trim();
          // format value based on fixed lenth, ... do not append dilimiter yet;  
          rec_str = rec_str.FormatValue(_config_data.general_info.record_type.value, lstConfigRec[0]);

          // set record item---
          RecordItem rec_item = new RecordItem();
          // sequence order
          rec_item.seq_order = seq_order;
          // tag source value
          if (string.Compare(lstConfigRec[0].source_type.value, "Fixed_Value", true) == 0)
            rec_item.tag = "Fixed_Value";
          else
            rec_item.tag = lstConfigRec[0].source.value;
          rec_item.value = rec_str;
          rec_item.rec_config = lstConfigRec[0];
          line_rec.lstRec.Add(rec_item);
          //---
        }

        // save line amount for later use
        line_rec.amt = _total_amount;
      }
      return line_rec;
    }

    protected string FormatRecord(BatchRecord rec)
    {
      string rec_str = "";
      string source_type = rec.source_type.value;
      if (string.Compare(source_type, "STD_DATA", true) == 0)
      {
        rec_str = FormatStandardDataField(rec);
      }
      else if (string.Compare(source_type, "FIXED_VALUE", true) == 0)
      {
        rec_str = rec.fixed_value;
      }
      return rec_str;
    }

    protected string FormatStandardDataField(BatchRecord rec)
    {
      string rec_str = "";
      string source = rec.source.value;
      if(string.Compare(source, "Batch_Create_Date", true) == 0)
      {
        DateTime date = DateTime.Now;
        rec_str = date.FormatDate(rec.format.value);
      }
      if (string.Compare(source, "Batch_Create_Time", true) == 0)
      {
        DateTime date = DateTime.Now;
        rec_str = date.FormatTime(rec.format.value);
      }
      else if (string.Compare(source, "Total_Amount", true) == 0 || 
        string.Compare(source, "Total_Absolute_Amount", true) == 0 ||
        string.Compare(source, "Total_Amount_sign_reversal", true) == 0)
      {
        double amount = _total_amount;
        if (string.Compare(source, "Total_Amount_sign_reversal", true) == 0)
          amount = amount * -1;
        rec_str = amount.FormatAmount(rec.format.value).FormatValue(_config_data.general_info.record_type.value, rec);
      }
      else if (string.Compare(source, "Total_Count", true) == 0)
      {
        rec_str = _total_count.ToString();
      }

      return rec_str;
    }
  }

 
}
