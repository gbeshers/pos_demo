﻿using CASL_engine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using ftp;
using System.Xml.Serialization;
using System.Xml;
using System.Xml.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.ComponentModel;
using Newtonsoft.Json;
using System.Dynamic;
using TranSuiteServices;
using CustomizedBatchUpdate.code_cs;

namespace CustomizedBatchUpdate
{
  public class BatchUpdateInfo
  {
    //protected BatchUpdateRecordData batch_data = null;
    //protected Dictionary<string, List<BatchUpdateLineRec>> dicDetailRecord { get; set; }
    protected List<BatchDetailRecord> lstDetailRecord { get; set; }

    // SI File and Ftp Info
    public string strTempBatchFile;                // Temp File Name
    public string strFTPServer;
    public string strFTPPort;
    public string strFTPUserName;
    public string strFTPPWD;                       // decrypted
    public string strFTPSeverPath;
    public string strFTPServerFileName;            // FILE NAME on FTP server 
    public string strLocalFolderPath;
    public string strLocalFileName;                // FILE NAME on local server, used to download file from FTP server first
    public string strFileTransferMethod;
    public string strFileExt;
    public string strFileDelimiter;
    public string strBatchSFTPKeyFile;            // Bug 24724
    public string strBatchSFTPKeyPassphrase;      // Bug 24724
    public OutputConnection pOutputConnection = null; // To do be careful using this for the sql connection because method close connection closes all object of output connection ie file that will be ftp'd and sql connection
    public bool config_info_initialed = false;

    public BatchUpdateConfigData bu_config_data { get; set; }
    public bool bAltDirSep;

    public int connectionTimeout = 60000;
    public bool logVerbose = false;
    public string rebexLogPath = "";

    public BatchUpdateInfo()
    {
      // V2
      lstDetailRecord = new List<BatchDetailRecord>();

    }

    protected string FormatFileNameWithDateTime(string configued_file_name)
    {
      // Format date time in file name
      if (configued_file_name.Contains("[") && configued_file_name.Contains("]"))
      {
        int idxStart = configued_file_name.IndexOf("[");
        int idxFinish = configued_file_name.IndexOf("]");
        if (idxStart < idxFinish)
        {
          string dateFormat = configued_file_name.Substring(idxStart, (idxFinish - idxStart + 1));
          string datePortion = dateFormat.Length > 2 ? DateTime.Now.ToString(dateFormat.Substring(1, dateFormat.Length - 2)) : "";
          configued_file_name = configued_file_name.Replace(dateFormat, datePortion);
        }
      }

      return configued_file_name;
    }
    protected string FormatFileName(string configured_file_name)
    {
      string file_name = FormatFileNameWithDateTime(configured_file_name);
      if (string.IsNullOrEmpty(file_name))
        file_name = string.Format("CustomizedBatchUpdate_{0}", (DateTime.Now.ToString("MMddyy_HHmmss")));
        
      return file_name;
    }
    public bool GetConfigureInfo(GenericObject pConfigSystemInterface, ref string strError)
    {
      Logger.cs_log("Start BatchUpdateInfo.GetConfigureInfo");

      if (config_info_initialed == false)
      {
        try
        {
          strFTPServer = pConfigSystemInterface.get("ftp_server", "").ToString();
          strFTPPort = pConfigSystemInterface.get("ftp_port", "").ToString();
          strFTPUserName = pConfigSystemInterface.get("ftp_username", "").ToString();
          strFTPSeverPath = pConfigSystemInterface.get("ftp_path", "").ToString();
          strFTPServerFileName = pConfigSystemInterface.get("ftp_server_file_name", "").ToString();
          strFTPServerFileName = FormatFileName(strFTPServerFileName);
          strLocalFolderPath = pConfigSystemInterface.get("folder_path", "").ToString();
          strLocalFileName = pConfigSystemInterface.get("folder_local_file_name", "").ToString();
          strLocalFileName = FormatFileName(strLocalFileName);
          strFileTransferMethod = ((string)pConfigSystemInterface.get("transfer_method", "ftp")).ToLower();
          strFileExt = pConfigSystemInterface.has("file_name_extension") ? pConfigSystemInterface.get("file_name_extension", "csv").ToString() : "csv";
          strBatchSFTPKeyFile = pConfigSystemInterface.get("ftp_private_key_file", "").ToString(); // Bug 24724 [16445] File Transmission Option Changing To S-FTP with ID and Key
          strBatchSFTPKeyPassphrase = pConfigSystemInterface.get("ftp_private_key_passphrase", "").ToString(); // Bug 24724 [16445] File Transmission Option Changing To S-FTP with ID and Key
          strFileDelimiter = ","; // default

          // Bug 25577
          connectionTimeout = Convert.ToInt32(pConfigSystemInterface.get("ftp_connection_timeout", "60000"));   // Bug 23202
          logVerbose = (bool)pConfigSystemInterface.get("rebex_verbose_log", false);                            // Bug 24836 
          rebexLogPath = pConfigSystemInterface.get("rebex_log_path", "").ToString();                           // Bug 24836 

          // local temp file
          string strTempBatchPath = pConfigSystemInterface.get("local_temp_path", "") as string;

          if (string.IsNullOrEmpty(strTempBatchPath) || !Directory.Exists(strTempBatchPath))
          {
            if (!Directory.Exists(strTempBatchPath))
            {
              string msg = string.Format("{0} is not exists. temp file should be created under {1}.", strTempBatchPath, Path.GetTempPath());
              Logger.cs_log(msg);
            }
            strTempBatchPath = Path.GetTempPath();
          }

          string strFileName = string.Format("CustomizedBatchUpdate{0}.txt", (DateTime.Now.ToString("MMddyy_HHmmss")));
          strTempBatchFile = Path.Combine(strTempBatchPath, strFileName);

          // remote file name + extension
          strFTPServerFileName = string.IsNullOrEmpty(strFileExt) ? strFTPServerFileName : $"{strFTPServerFileName}.{strFileExt}";
          // local file name + extension
          strLocalFileName = string.IsNullOrEmpty(strFileExt) ? strLocalFileName : $"{strLocalFileName}.{strFileExt}";
          
          //FTP Password and  Decrypt FTP Password
          strFTPPWD = pConfigSystemInterface.get("ftp_password", "").ToString();
          if (!string.IsNullOrEmpty(strFTPPWD))
          {
            string secret_key = Crypto.get_secret_key();
            byte[] content_bytes = Convert.FromBase64String(strFTPPWD);
            byte[] decrypted_bytes = Crypto.symmetric_decrypt("data", content_bytes, "secret_key", secret_key);
            strFTPPWD = System.Text.Encoding.Default.GetString(decrypted_bytes);
          }

          // BEGIN Bug 24724 [16445] FT: Transmission Option Changing To S-FTP with ID and Key
          // Decrypt S-FTP Key Passphrase
          if (!string.IsNullOrEmpty(strBatchSFTPKeyPassphrase))
          {
            string secret_key = Crypto.get_secret_key();
            byte[] content_bytes = Convert.FromBase64String(strBatchSFTPKeyPassphrase);
            byte[] decrypted_bytes = Crypto.symmetric_decrypt("data", content_bytes, "secret_key", secret_key);
            strBatchSFTPKeyPassphrase = System.Text.Encoding.Default.GetString(decrypted_bytes);
          }
          // END Bug 24724 [16445] FT: Transmission Option Changing To S-FTP with ID and Key


          bAltDirSep = (bool)pConfigSystemInterface.get("alt_directory_separator", false);

          // config_data
          string strCustomizedBUType_Id = pConfigSystemInterface.get("customized_bu_type", "").ToString();
          if (!string.IsNullOrEmpty(strCustomizedBUType_Id))
          {
            bu_config_data = GetBatchUpdateConfigInfoData(strCustomizedBUType_Id);
            if (bu_config_data == null)
            {
              strError = "GetConfigureInfo Failed: bu_config_data is null, see your administrator";
              Logger.cs_log(strError);
              return false;
            }
          }
          else
          {
            strError = "GetConfigureInfo Failed: no custimized batch update type configured";
            Logger.cs_log(strError);
            return false;
          }

        }
        catch (Exception ex)
        {
          strError = "GetConfigureInfo Failed: " + ex.Message;
          Logger.cs_log(strError);
          return false;
        }

        config_info_initialed = true;
      }

      Logger.cs_log("End BatchUpdateInfo.GetConfigureInfo");
      return true;
    }
    public BatchUpdateConfigData GetBatchUpdateConfigInfoData(string customized_bu_type_id)
    {
      Logger.cs_log("Start BatchUpdateInfo.GetBatchUpdateConfigInfoData");

      string config_data_str = "";
      BatchUpdateConfigData config_data = new BatchUpdateConfigData();

      string strError = "";
      GenericObject database = c_CASL.c_object("TranSuite_DB.Configurablebu.db_connection_info") as GenericObject;
      ConnectionTypeSynch dbConnType = null;
      try
      {
        dbConnType = new ConnectionTypeSynch(database);
        GenericObject dbConnInfo = dbConnType.GetDBConnectionInfo();
        string configDBConnectStr = (string)dbConnInfo.get("db_connection_string", "");

        using (SqlConnection conn = new SqlConnection(configDBConnectStr))
        {
          conn.Open();
          using (SqlCommand command = new SqlCommand())
          {
            command.Connection = conn;
            command.CommandText = string.Format("SELECT config_data FROM batch_update_config WHERE id = '{0}'", customized_bu_type_id);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
              if (reader.Read())
              {
                config_data_str = reader["config_data"].ToString();

              }
            }
            reader.Close();
            config_data = JsonConvert.DeserializeObject<BatchUpdateConfigData>(config_data_str);
          }
        }

        Logger.cs_log("End BatchUpdateInfo.GetBatchUpdateConfigInfoData");
        return config_data;
      }
      catch (Exception ex)
      {
        strError = "GetConfigureInfo Failed: " + ex.Message;
        Logger.cs_log("End BatchUpdateInfo.GetBatchUpdateConfigInfoData with Error: " + strError);
        return config_data;
      }

    }

    public bool AddRecord(GenericObject geoFile, GenericObject geoTran, GenericObject geoTranItem, List<GenericObject> geoTenders_applied)
    {
      Logger.cs_log("Start BatchUpdateInfo.AddRecord");
      string strError = "";
      try
      {

        DetailLevelProcessor detail_level_processor = new DetailLevelProcessor(bu_config_data, geoFile, geoTran, geoTranItem, geoTenders_applied);

        BatchDetailRecord batch_detail_rec = detail_level_processor.Process2();
        lstDetailRecord.Add(batch_detail_rec);
      }
      catch (Exception ex)
      {
        strError = "Error in AddRecord: " + ex.Message;
        Logger.cs_log("End BatchUpdateInfo.AddRecord with Error: " + strError);
        return false;
      }

      Logger.cs_log("End BatchUpdateInfo.AddRecord");
      return true;
    }

    public bool AddGLOverShortRecords(GenericObject geoOSTrans, GenericObject pCurrentFile, ref string strError)
    {
      Logger.cs_log("Start BatchUpdateInfo.AddGLOverShortRecords");

      if (bu_config_data.general_info.include_deposit_over_short)
      {
        try
        {
          GenericObject geoFile = pCurrentFile.get(0) as GenericObject;// File Data is stored in vector key
          for (int i = 0; i < geoOSTrans.getIntKeyLength(); i++)
          {
            GenericObject geoTran = geoOSTrans.get(i) as GenericObject;
            string strTTID = geoTran.get("TTID", "") as string;

            if (string.Compare(strTTID, "SYSOVERSHORT", true) == 0)
            {
              DetailLevelProcessor detail_level_processor = new DetailLevelProcessor(bu_config_data, geoFile, geoTran, null, null);
              BatchDetailRecord batch_detail_rec = detail_level_processor.Process2(true);
              lstDetailRecord.Add(batch_detail_rec);

            }
          }
        }
        catch (Exception ex)
        {
          strError = "Error in AddGLOverShortRecord " + ex.Message;
          Logger.cs_log("End BatchUpdateInfo.AddGLOverShortRecords with Error: " + strError);
          return false;
        }
      }

      Logger.cs_log("End BatchUpdateInfo.AddGLOverShortRecords");
      return true;
    }

    public bool AddBankDepositRecord(GenericObject pFileDeposits, GenericObject pCurrentFile, ref string strError)
    {
      Logger.cs_log("Start BatchUpdateInfo.AddBankDepositRecord");
      try
      {
        if (bu_config_data.general_info.include_bank_deposit)
        {
          GenericObject geoFile = pCurrentFile.get(0) as GenericObject; // File Data is stored in vector key
          foreach (GenericObject geoDeposit in pFileDeposits.vectors)
          {
            BatchUpdateBankDepositRecordProcessor processor = new BatchUpdateBankDepositRecordProcessor(bu_config_data, geoFile, geoDeposit);
            BatchDetailRecord batch_detail_rec = processor.Process2();
            lstDetailRecord.Add(batch_detail_rec);

          }
        }
      }
      catch (Exception ex)
      {
        strError = "Error in AddBankDepositRecord: " + ex.Message;
        Logger.cs_log("Start BatchUpdateInfo.AddBankDepositRecord with Error: " + strError);
        return false;
      }

      Logger.cs_log("End BatchUpdateInfo.AddBankDepositRecord");
      return true;
    }

    public string BuildBUText()
    {
      Logger.cs_log("Start BatchUpdateInfo.BuildBUText");
      // consolidate
      BatchUpdateProcessor processor = new BatchUpdateProcessor(bu_config_data, lstDetailRecord);
      string batch_text = processor.Process();

      Logger.cs_log("End BatchUpdateInfo.BuildBUText");
      return batch_text;
    }
  }
  public class CustomizedBatchUpdate : AbstractBatchUpdate
  {
    public BatchUpdateInfo m_batch_update_info = new BatchUpdateInfo();
    public bool bTerminateBatchUpdate = false;

    #region Override Methods
    /// <summary>
    /// This virtual method is called first during a Batch Update run.  
    /// It should be overridden to perform an action other than just instantiating the class
    /// </summary>
    /// <param name="sysInt"></param>
    /// <returns></returns>
    public override AbstractBatchUpdate GetInstance(GenericObject sysInt)
    {
      Logger.cs_log("CUSTOMIZED_BATCH_UPDATE - Start");
      return base.GetInstance(sysInt);
    }

    /// <summary>
    /// OracleFinancials_BatchUpdate is the system_interface type ID
    /// </summary>
    /// <returns></returns>
    public override string GetCASLTypeName()
    {
      return "Customized_BatchUpdate";
    }

    public override bool Project_Cleanup(bool bEarlyTerminationRequestedByProject, object pProductLevelMainClass)
    {
      Logger.cs_log("Start CustomizedBatchUpdate.Project_Cleanup");
      CleanUp();
      Logger.cs_log("End CustomizedBatchUpdate.Project_Cleanup");
      return true;
    }

    /// <summary>
    /// This virtual method is called when processing a selected CORE file is started
    /// </summary>
    /// <param name="strSystemInterface"></param>
    /// <param name="pConfigSystemInterface"></param>
    /// <param name="pProductLevelMainClass"></param>
    /// <param name="pCurrentFile"></param>
    /// <param name="strCoreFile"></param>
    /// <param name="ReturnAction"></param>
    /// <returns></returns>
    public override bool Project_CoreFile_BEGIN(string strSystemInterface, GenericObject pConfigSystemInterface, object pProductLevelMainClass, GenericObject pCurrentFile, string strCoreFile, ref eReturnAction ReturnAction)
    {
      Logger.cs_log("Start CustomizedBatchUpdate.Project_CoreFile_BEGIN");
      string strError = string.Empty;
      try
      {
        CASL_engine.BatchUpdate_ProductLevel pBathUpdate_ProductLevel = (BatchUpdate_ProductLevel)pProductLevelMainClass;

        if (!m_batch_update_info.GetConfigureInfo(pConfigSystemInterface, ref strError))
          throw new Exception(strError);

      }
      catch (Exception ex)
      {
        Logger.cs_log("End CustomizedBatchUpdate.Project_CoreFile_BEGIN with Error: " + ex.Message.ToString());
        bTerminateBatchUpdate = true;
        return false;
      }

      Logger.cs_log("End CustomizedBatchUpdate.Project_CoreFile_BEGIN");
      return true;
    }

    /// <summary>
    /// This abstract method is called for each CORE Event in a selected CORE file
    /// </summary>
    /// <param name="strSystemInterface"></param>
    /// <param name="pEventGEO"></param>
    /// <param name="pConfigSystemInterface"></param>
    /// <param name="pFileGEO"></param>
    /// <param name="pProductLevelMainClass"></param>
    /// <param name="strCurrentFile"></param>
    /// <param name="ReturnAction"></param>
    /// <returns></returns>
    public override bool Project_ProcessCoreEvent(string strSystemInterface, GenericObject pEventGEO, GenericObject pConfigSystemInterface, GenericObject pFileGEO, object pProductLevelMainClass, string strCurrentFile, ref eReturnAction ReturnAction)
    {
      Logger.cs_log("Start CustomizedBatchUpdate.Project_ProcessCoreEvent");
      string strError = string.Empty;
      CASL_engine.BatchUpdate_ProductLevel pBatchUpdate_ProductLevel = (BatchUpdate_ProductLevel)pProductLevelMainClass;

      try
      {
        // get File
        pFileGEO = pFileGEO.get(0, null) as GenericObject;
        if (pFileGEO == null)
        {
          strError = "ERROR: CustomizedBatchUpdate.Project_ProcessCoreEvent:Unable to retrieve CORE File Dept";
          misc.CASL_call("a_method", "log", "args", new GenericObject("ERROR", strError));
          pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError(strCurrentFile, strSystemInterface, strError);
          return false;
        }

        // get tenders
        GenericObject geoTenders = pEventGEO.get("TENDERS", new GenericObject()) as GenericObject;

        // get transaction
        GenericObject geoTrans = pEventGEO.get("TRANSACTIONS", new GenericObject()) as GenericObject;
        int iCntOfTransactions = geoTrans.getIntKeyLength();

        // IPAY-1477 HX
        // if transaction includes CTT (itemind is T), it will include BTT (itemind is S), group CTT and BTTs together
        // key is sequence
        int seq_key = -1;
        Dictionary<int, List<GenericObject>> dicTrans = new Dictionary<int, List<GenericObject>>();
        for(int i = 0; i < iCntOfTransactions; i++)
        {
          GenericObject geoTran = (GenericObject)geoTrans.get(i);
          string itemind = geoTran.get("ITEMIND", "").ToString();
          if(itemind == "T")
            seq_key += 1;
            
          if(dicTrans.ContainsKey(seq_key) == false)
          {
            List<GenericObject> lst_geoTran = new List<GenericObject>();
            lst_geoTran.Add(geoTran);
            dicTrans.Add(seq_key, lst_geoTran);
          }
          else
          {
            dicTrans[seq_key].Add(geoTran);
          }
        }

        // IPAY-1477 HX
        bool need_reverse = m_batch_update_info.bu_config_data.general_info.reversal_trans_seq_in_single_pymt_page ? true : false;
        foreach (KeyValuePair<int, List<GenericObject>> pair in dicTrans)
        {
          List<GenericObject> my_geoTrans = need_reverse ? pair.Value.OrderByDescending(o =>(int)o.get("TRANNBR", -1)).ToList():pair.Value;
          foreach(GenericObject geoTran in my_geoTrans)
          {
            string itemind = geoTran.get("ITEMIND", "").ToString();

            // exclude transaction which is not belong to strSystemInterface. 
            if (!IsMyBatchUpdate(geoTran, strSystemInterface))
              continue;

            if ((itemind != "S")) // HX: I only need to process with itemind with "S"
              continue;

            // get associated applied tenders
            List<GenericObject> geoTndrs_applied = new List<GenericObject>();
            GenericObject geo_tta_tenders = geoTran.get("TTA_TENDERS", null) as GenericObject;
            int cnt_tta = geo_tta_tenders.getIntKeyLength();
            for (int j = 0; j < cnt_tta; j++)
            {
              GenericObject tta_tender = (GenericObject)geo_tta_tenders.get(j);
              string tta_tender_Id = tta_tender.get("TNDRID", "").ToString();
              GenericObject geoTndr = GetTenderByTenderId(geoTenders, tta_tender_Id);
              geoTndrs_applied.Add(geoTndr);
            }

            // check for multiple item; for example gl item
            GenericObject geoTranItems = geoTran.get("TG_ITEM_DATA", new GenericObject()) as GenericObject;
            int iCountOfTranItems = geoTranItems.getIntKeyLength();
            if (iCountOfTranItems > 0)
            {
              for (int k = 0; k < iCountOfTranItems; k++)
              {
                GenericObject geoTranItem = (GenericObject)geoTranItems.get(k);
                m_batch_update_info.AddRecord(pFileGEO, geoTran, geoTranItem, geoTndrs_applied);
              }
            }
            else
            {
              m_batch_update_info.AddRecord(pFileGEO, geoTran, null, geoTndrs_applied);
            }
          }
        }

        #region removed
        /*for (int i = 0; i < iCntOfTransactions; i++)
        {
          GenericObject geoTran = (GenericObject)geoTrans.get(i);
          string itemind = geoTran.get("ITEMIND", "").ToString();

          // exclude transaction which is not belong to strSystemInterface. 
          if (!IsMyBatchUpdate(geoTran, strSystemInterface))
            continue;

          if ((itemind != "S")) // HX: I only need to process with itemind with "S"
            continue;

          // get associated applied tenders
          List<GenericObject> geoTndrs_applied = new List<GenericObject>();
          GenericObject geo_tta_tenders = geoTran.get("TTA_TENDERS", null) as GenericObject;
          int cnt_tta = geo_tta_tenders.getIntKeyLength();
          for (int j = 0; j < cnt_tta; j++)
          {
            GenericObject tta_tender = (GenericObject)geo_tta_tenders.get(j);
            string tta_tender_Id = tta_tender.get("TNDRID", "").ToString();
            GenericObject geoTndr = GetTenderByTenderId(geoTenders, tta_tender_Id);
            geoTndrs_applied.Add(geoTndr);
          }

          // check for multiple item; for example gl item
          GenericObject geoTranItems = geoTran.get("TG_ITEM_DATA", new GenericObject()) as GenericObject;
          int iCountOfTranItems = geoTranItems.getIntKeyLength();
          if (iCountOfTranItems > 0)
          {
            for (int k = 0; k < iCountOfTranItems; k++)
            {
              GenericObject geoTranItem = (GenericObject)geoTranItems.get(k);
              m_batch_update_info.AddRecord(pFileGEO, geoTran, geoTranItem, geoTndrs_applied);
            }
          }
          else
          {
            m_batch_update_info.AddRecord(pFileGEO, geoTran, null, geoTndrs_applied);
          }

        }*/
        #endregion

      }
      catch (Exception ex)
      {
        bTerminateBatchUpdate = true;
        Logger.cs_log("End CustomizedBatchUpdate.Project_ProcessCoreEvent with Error: " + ex.Message.ToString());
        return false;
      }

      Logger.cs_log("End CustomizedBatchUpdate.Project_ProcessCoreEvent");
      return true;
    }

    public override bool Project_CoreFile_END(string strSystemInterface, GenericObject pConfigSystemInterface, object pProductLevelMainClass, GenericObject pCurrentFile, string strCoreFile, ref long? lItemCount, ref double? dItemAmount, ref eReturnAction ReturnAction)
    {
      Logger.cs_log("Start CustomizedBatchUpdate.Project_CoreFile_END");
      string strError = string.Empty;
      CASL_engine.BatchUpdate_ProductLevel pBatchUpdate_ProductLevel = (BatchUpdate_ProductLevel)pProductLevelMainClass;

      try
      {
        // get File
        pCurrentFile = pCurrentFile.get(0, null) as GenericObject;
        if (pCurrentFile == null)
        {
          strError = "ERROR: CustomizedBatchUpdate.Project_ProcessCoreEvent:Unable to retrieve CORE File Dept";
          misc.CASL_call("a_method", "log", "args", new GenericObject("ERROR", strError));
          pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError(strCoreFile, strSystemInterface, strError);
          return false;
        }
      }
      catch (Exception ex)
      {
        bTerminateBatchUpdate = true;
        Logger.cs_log("END CustomizedBatchUpdate.Project_CoreFile_END with error: " + ex.Message.ToString());
        return false;
      }

      Logger.cs_log("END CustomizedBatchUpdate.Project_CoreFile_END");
      return true;
    }
    public override bool Project_ProcessDeposits(GenericObject pFileDeposits, GenericObject pConfigSystemInterface, object pProductLevelMainClass, string strSysInterface, GenericObject pCurrentFile, ref eReturnAction ReturnAction)
    {
      Logger.cs_log("Start CustomizedBatchUpdate.Project_ProcessDeposits");
      string strError = "";
      string strFileName = pFileDeposits.get("FULL_FILE_NAME", "").ToString();
      CASL_engine.BatchUpdate_ProductLevel pBatchUpdate_ProductLevel = (BatchUpdate_ProductLevel)pProductLevelMainClass;

      try
      {
        foreach (string key in pCurrentFile.getStringKeys())
        {
          if (key.Contains("SYSOVERSHORT"))
          {
            GenericObject geoOVERSHORT = pCurrentFile[key] as GenericObject;
            GenericObject geoOSTran = geoOVERSHORT.get("TRANSACTIONS", new GenericObject()) as GenericObject;

            if (!m_batch_update_info.AddGLOverShortRecords(geoOSTran, pCurrentFile, ref strError))
            {
              Logger.LogInfo(strError, "Project_ProcessDeposits");
              return false;
            }
          }
        }

        //Process Bank Deposits
        if (!m_batch_update_info.AddBankDepositRecord(pFileDeposits, pCurrentFile, ref strError))
        {
          pBatchUpdate_ProductLevel = (BatchUpdate_ProductLevel)pProductLevelMainClass;
          Logger.LogError(strError, "GL Batch Update");
          pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError(strFileName, strSysInterface, strError);
          return false;
        }
      }
      catch (Exception ex)
      {
        strError += ":Project_ProcessDeposits";
        strError += ex.Message;

        Logger.LogError(ex.Message.ToString(), "Project_ProcessDeposits Exception Error");
        return true;
      }

      Logger.cs_log("End CustomizedBatchUpdate.Project_ProcessDeposits");
      return true;

    }

    public override void Project_Rollback(object pProductLevelMainClass, bool bEarlyTerminationRequestedByProject)
    {
      // Add code here if you want to do something on failure (delete the file?)
      return;
    }

    public override bool Project_WriteTrailer(object pProductLevelMainClass, string strSysInterface, ref bool bEarlyTerminationRequestedByProject)
    {
      Logger.cs_log("Start Project_WriteTrailer");
      string strError = "";

      CASL_engine.BatchUpdate_ProductLevel pBatchUpdate_ProductLevel = (BatchUpdate_ProductLevel)pProductLevelMainClass;

      if (bEarlyTerminationRequestedByProject)
      {
        strError = string.Format("Early Termination - CustomizedBatchUpdate.Project_WriteTrailer Termination.");
        Logger.cs_log(string.Format("CustomizedBatchUpdate.Project_WriteTrailer - {0}", strError));
        misc.CASL_call("a_method", "log", "args", new GenericObject("ERROR CustomizedBatchUpdate.Project_WriteTrailer", strError));
        pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError("SYS_INTERFACE_SCOPE_ONLY", strSysInterface, "CustomizedBatchUpdate.Project_WriteTrailer" + strError);
        // CleanUp();
        return false;
      }

      if (bTerminateBatchUpdate)
        return false;

      try
      {
        // add bu file header and trailer
        // m_batch_update_info.AddBatchFileHeaderAndTrailer();

        if (!ProcessBatchUpdate(pBatchUpdate_ProductLevel, ref strError))
          throw new Exception(strError);
      }
      catch (Exception ex)
      {
        bEarlyTerminationRequestedByProject = true;
        Logger.cs_log("CustomizedBatchUpdate.Project_WriteTrailer Failed: " + ex.Message.ToString());
        CleanUp();
        return false;
      }

      Logger.cs_log("End Project_WriteTrailer");
      return true;
    }
    #endregion

    public bool IsMyBatchUpdate(GenericObject geoTran, string strSystemInterface)
    {
      //Bug#13616 QG null value check
      if (geoTran != null)
      {
        GenericObject geoCustData = geoTran.get("GR_CUST_FIELD_DATA", new GenericObject()) as GenericObject;
        GenericObject geoConfigSysInt = new GenericObject();
        string strConfigSysInt = "";
        if (geoCustData != null)
        {
          for (int i = 0; i < geoCustData.getLength(); i++)
          {
            GenericObject cust_field = geoCustData.get(i) as GenericObject;
            string custTag = ((string)cust_field.get("CUSTTAG", "")).ToLower();
            string custValue = cust_field.get("CUSTVALUE", "") as string;

            if (string.Compare(custTag, "update_system_interface_list", true) == 0)
            {
              //geoConfigSysInt = ("_subject",custValue);
              strConfigSysInt = custValue;
              break;
            }
          }
        }

        if (strConfigSysInt.Contains(strSystemInterface))
          return true;
       
      }
      return false;
    }

    protected bool ProcessBatchUpdate(BatchUpdate_ProductLevel pBatchUpdate_ProductLevel, ref string strError)
    {
      Logger.cs_log("Start ProcessBatchUpdate");
      // delete local temp file if it is existed
      if (File.Exists(m_batch_update_info.strTempBatchFile))
        File.Delete(m_batch_update_info.strTempBatchFile);


      // Build Lines
      string strFileContent = m_batch_update_info.BuildBUText();

      // write cotent to temp file
      if (!WriteFileContentToTempFile(strFileContent, ref strError))
      {
        Logger.cs_log("End ProcessBatchUpdate failed: WriteFileContentToTempFile had problems. ");
        return false;
      }

      // temp commeented out
      // FTP or Copy to local path
      string strTransferMethod = m_batch_update_info.strFileTransferMethod.ToLower();
      if (strTransferMethod == "ftp" || strTransferMethod == "sftp")
      {
        if (!ftpBatchFile(pBatchUpdate_ProductLevel, ref strError))
        {
          Logger.cs_log("End ProcessBatchUpdate failed with transfer method 'ftp' ");
          return false;
        }
      }
      else if (strTransferMethod == "folder")
      {
        if (!flatBatchFile(pBatchUpdate_ProductLevel, ref strError))
        {
          Logger.cs_log("End ProcessBatchUpdate failed with transfer method 'folder' ");
          return false;
        }
      }

      Logger.cs_log("End ProcessBatchUpdate");
      return true;
    }

    protected bool WriteFileContentToTempFile(string strFileContent, ref string strError)
    {
      try
      {
        if (m_batch_update_info.pOutputConnection == null)
          m_batch_update_info.pOutputConnection = new OutputConnection();

        if (m_batch_update_info.pOutputConnection.m_pOutputFile == null)
        {
          // this prevents appending to file if it exists from previous run or was placed in the folder
          m_batch_update_info.pOutputConnection.m_pOutputFile = new StreamWriter(m_batch_update_info.strTempBatchFile);
        }
        else
        {
          if (File.Exists(m_batch_update_info.strTempBatchFile))
            m_batch_update_info.pOutputConnection.m_pOutputFile = File.AppendText(m_batch_update_info.strTempBatchFile);
        }

        m_batch_update_info.pOutputConnection.m_pOutputFile.Write(strFileContent);
        m_batch_update_info.pOutputConnection.m_pOutputFile.Flush();
      }
      catch (Exception ex)
      {
        Logger.cs_log(ex.ToMessageAndCompleteStacktrace());
        m_batch_update_info.pOutputConnection.m_pOutputFile.Close();
        strError = string.Format("Cannot Write to File({0}):{1}", m_batch_update_info.strTempBatchFile, ex.Message);
        misc.CASL_call("a_method", "log", "args", new GenericObject("ERROR in m_batch_update_info.WriteFileContentToTempFile", strError));
        return false;
      }
      finally
      {
        // Bug 16433 UMN handle close in finally clause
        if (m_batch_update_info.pOutputConnection.m_pOutputFile != null)
          m_batch_update_info.pOutputConnection.m_pOutputFile.Close();
      }

      return true;
    }

    protected void CleanUp()
    {
      // delete temp file
      try
      {
        if (File.Exists(m_batch_update_info.strTempBatchFile))
          File.Delete(m_batch_update_info.strTempBatchFile);
      }
      catch (Exception ex)
      {
        //should not effect the process next run should overwrite this existing file
        //just write it to the log
        Logger.cs_log(ex.ToMessageAndCompleteStacktrace());
        misc.CASL_call("a_method", "log", "args", new GenericObject("ERROR could not delete the local temp batch file", ex.Message));
      }
      return;
    }

    /// <summary>
    /// Do the ftp/sftp part of the batch update and send the file to the configured (S)FTP server.
    /// </summary>
    /// <param name="pBatchUpdate_ProductLevel"></param>
    /// <param name="strSysInterface"></param>
    /// <param name="strLocalTempFile"></param>
    /// <param name="fileName"></param>
    /// <returns></returns>
    protected bool ftpBatchFile(BatchUpdate_ProductLevel pBatchUpdate_ProductLevel, ref string strError)
    {
      Logger.cs_log("Start ftpBatchFile");
      string strBatchFTPAddress = m_batch_update_info.strFTPServer;
      if (string.IsNullOrEmpty(strBatchFTPAddress))
      {
        misc.CASL_call("a_method", "log", "args", new GenericObject("Oracle Financials Batch Update Warning", "No FTP Server configured: not sending file"));
        strError = "FTP Failed - No FTP Server Configured.";
        return false;
      }

      string strFileTransferMethod = m_batch_update_info.strFileTransferMethod.ToLower();

      // TTS 14634
      bool bAltDirSep = m_batch_update_info.bAltDirSep;
      string strServerPathFileName = "";
      if (bAltDirSep)
      {
        strServerPathFileName = m_batch_update_info.strFTPSeverPath + Path.AltDirectorySeparatorChar + m_batch_update_info.strFTPServerFileName;
      }
      else
        strServerPathFileName = Path.Combine(m_batch_update_info.strFTPSeverPath, m_batch_update_info.strFTPServerFileName);
      // End 14634

      // Transfer File
      FTP ftp = null;
      SFTP sftp = null;
      const bool pwdIsEncrypted = false;  // Force the FTP classes to decrypt


      try
      {
        bool transferSuccessful = false;

        if (strFileTransferMethod == "ftp")
        {
          ftp = new FTP(m_batch_update_info.strFTPServer, m_batch_update_info.strFTPUserName, m_batch_update_info.strFTPPWD, m_batch_update_info.strFTPPort, true, pwdIsEncrypted, false, false, false);

          //ftp.Connect();
          ftp.Connect(m_batch_update_info.connectionTimeout, m_batch_update_info.logVerbose, m_batch_update_info.rebexLogPath);
          ftp.SendFile(m_batch_update_info.strTempBatchFile, strServerPathFileName);
          if (ftp.FileExists(strServerPathFileName))
            transferSuccessful = true;

        }
        else if (strFileTransferMethod == "sftp")
        {
          // Bug 24724 [16445] FT: File Transmission Option Changing To S-FTP with ID and Key
          // Only use this if the keyfile path is set
          if (string.IsNullOrWhiteSpace(m_batch_update_info.strBatchSFTPKeyFile))
          {
            // The Key file path is not set, so use the traditional username and password
            sftp = new SFTP(m_batch_update_info.strFTPServer, m_batch_update_info.strFTPUserName, m_batch_update_info.strFTPPWD, m_batch_update_info.strFTPPort, true, pwdIsEncrypted, "", "", "");
          }
          else
          {
            // The Key file path is set, use the username and key method
            sftp = new SFTP(m_batch_update_info.strFTPServer, m_batch_update_info.strFTPUserName, m_batch_update_info.strFTPPWD, m_batch_update_info.strFTPPort, true, pwdIsEncrypted, m_batch_update_info.strBatchSFTPKeyFile, "", m_batch_update_info.strBatchSFTPKeyPassphrase);
          }
          // End Bug 24724

          sftp.Connect(m_batch_update_info.connectionTimeout, m_batch_update_info.logVerbose, m_batch_update_info.rebexLogPath);
          sftp.SendFile(m_batch_update_info.strTempBatchFile, strServerPathFileName);
          if (sftp.FileExists(strServerPathFileName))
            transferSuccessful = true;
        }

        if (transferSuccessful)
        {
          //Succeed to upload file to FTP, log and send email
          string msg = string.Format("Oracle Financials Batch Update File Transfer Process of file {0} Succeeded.  Oracle Financials Batch Update Successful", m_batch_update_info.strFTPServerFileName);
          misc.CASL_call("a_method", "log", "args", new GenericObject("ACTION", msg));
          return true;
        }
        else
        {
          strError = string.Format("Cannot Write to batch File '{0}' to '{1}' on the FTP server", m_batch_update_info.strTempBatchFile, strServerPathFileName);
          misc.CASL_call("a_method", "log", "args", new GenericObject("ERROR in ORACLE_FINANCIALS_BATCH_UDPATE ", strError));
          string errorMsg = "Fail to transfer Lawson batch File: " + strError;
          // Bug 16431
          pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError("SYS_INTERFACE_SCOPE_ONLY", "Oracle Financils Batch Update", errorMsg);

          return false;
        }
      }
      catch (Exception ex)
      {
        strError = string.Format("ftpBatchFile failed.  Cannot FTP batch update file. " + ex.Message);
        misc.CASL_call("a_method", "log", "args", new GenericObject("ERROR in ORACLE_FINANCIALS_BATCH_UDPATE ", strError));
        pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError("SYS_INTERFACE_SCOPE_ONLY", "Oracle Financils Batch Update", strError);

        if (ftp != null) ftp.Disconnect();
        if (sftp != null) sftp.Disconnect();
        return false;
      }
      finally
      {
        // Destructor will call Disconnect, but with .NET it's async scheduled. So calling
        // disconnect here will free the connection faster
        if (ftp != null) ftp.Disconnect();
        if (sftp != null) sftp.Disconnect();

        Logger.cs_log("End ftpBatchFile");
      }

    }

    /// <summary>
    /// flat batch file send file to local server path - "folder"
    /// </summary>
    /// <param name="pBatchUpdate_ProductLevel"></param>
    /// <param name="strSysInterface"></param>
    /// <param name="strLocalTempFile"></param>
    /// <param name="fileName"></param>
    /// <returns></returns>
    protected bool flatBatchFile(CASL_engine.BatchUpdate_ProductLevel pBatchUpdate_ProductLevel, ref string strError)
    {
      Logger.cs_log("Start flatBatchFile");
      if (string.IsNullOrEmpty(m_batch_update_info.strLocalFolderPath))
      {
        misc.CASL_call("a_method", "log", "args", new GenericObject("Oracle Financials batch Warning", "No local folder path configured: not sending file"));
        strError = "Batch File Failed - No file path configured.";
        return false;
      }

      string strLocalPathFileName = Path.Combine(m_batch_update_info.strLocalFolderPath, m_batch_update_info.strLocalFileName);

      // coping File
      try
      {
        string fileContent = File.ReadAllText(m_batch_update_info.strTempBatchFile);
        StreamWriter sw;

        if (File.Exists(strLocalPathFileName))
        {
          misc.CASL_call("a_method", "log", "args", new GenericObject("Oracle Financials batch Warning", "there is existing batch update file: not sending file"));
          strError = "Batch File failed - exiting batch file found.";
          return false;
        }
        else
        {
          sw = new StreamWriter(strLocalPathFileName, true);
        }

        sw.Write(fileContent);
        sw.Flush();
        sw.Close();

        //Succeed to copying file, log and send email
        string msg = string.Format("Oracle Financials Batch Update File Copying Process of file {0} Succeeded.  Oracle Financials Batch Update Successful", m_batch_update_info.strLocalFileName);
        misc.CASL_call("a_method", "log", "args", new GenericObject("ACTION", msg));

      }
      catch (Exception e)
      {
        strError = string.Format("Cannot Write to Oracle Financials batch File '{0}' to '{1}': {2}", m_batch_update_info.strTempBatchFile, m_batch_update_info.strLocalFileName, e.Message.ToString());
        misc.CASL_call("a_method", "log", "args", new GenericObject("ERROR in Lawson_BATCH_UDPATE ", strError));
        string errorMsg = "Fail to copying batch File: " + strError;
        pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError("SYS_INTERFACE_SCOPE_ONLY", "Oracle Financials Batch Update", errorMsg);

        return false;
      }

      return true;
    }

    protected GenericObject GetTenderByTenderId(GenericObject geoTenders, string tender_id)
    {
      GenericObject geoTender = null;
      int iCountofTenders = geoTenders.getIntKeyLength();
      for (int i = 0; i < iCountofTenders; i++)
      {
        geoTender = (GenericObject)geoTenders.get(i);
        string my_tender_Id = geoTender.get("TNDRID", "").ToString();
        if (string.Compare(my_tender_Id, tender_id, true) == 0)
          break;

      }

      return geoTender;
    }
  }
}
