﻿using CASL_engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CustomizedBatchUpdate
{

  /// <summary>
  /// used for process batch header/trailer, pay file header / trailer
  /// </summary>
  public class BatchUpdateRecordProcessor
  {
    private BatchUpdateConfigData _config_data { get; set; }
    private GenericObject _geoFile { get; set; }
    private long _total_count { get; set; }
    private double _total_amount { get; set; }

    public BatchUpdateRecordProcessor(BatchUpdateConfigData config_data, GenericObject geoFile, long total_count, double total_amount)
    {
      _config_data = config_data;
      _geoFile = geoFile;
      _total_amount = total_amount;
      _total_count = total_count;
    }

    // format batch line record - one of batch file header, batch file trailer, pay file header, pay file trailer
    public BatchUpdateLineRec FormatBatchLineRecord(BU_REC_TYPE bu_rec_type)
    {
      BatchUpdateLineRec bu_rec = new BatchUpdateLineRec();
      bu_rec.lstRec = new List<RecordItem>();

      // about configuration ...
      ILookup<int, BatchRecord> lookup = null; //= _config_data.batch_file_headers.ToLookup(o => o.seq_order);
      switch (bu_rec_type)
      {
        case BU_REC_TYPE.bu_file_header:
          if (_config_data.batch_file_headers != null && _config_data.batch_file_headers.Count > 0)
            lookup = _config_data.batch_file_headers.ToLookup(o => o.seq_order);
          break;
        case BU_REC_TYPE.pay_file_header:
          if (_config_data.payment_file_headers != null && _config_data.payment_file_headers.Count > 0)
            lookup = _config_data.payment_file_headers.ToLookup(o => o.seq_order);
          break;
        case BU_REC_TYPE.pay_file_trailer:
          if (_config_data.payment_file_trailers != null && _config_data.payment_file_trailers.Count > 0)
            lookup = _config_data.payment_file_trailers.ToLookup(o => o.seq_order);
          break;
        case BU_REC_TYPE.bu_file_trailer:
          if (_config_data.batch_file_trailers != null && _config_data.batch_file_trailers.Count > 0)
            lookup = _config_data.batch_file_trailers.ToLookup(o => o.seq_order);
          break;
        default:
          if (_config_data.batch_file_headers != null && _config_data.batch_file_headers.Count > 0)
            lookup = _config_data.batch_file_headers.ToLookup(o => o.seq_order);
          break;

      }
      if (lookup != null)
      {
        List<int> lstSeqOrder = lookup.Select(o => o.Key).ToList();
        lstSeqOrder.Sort();

        foreach (int seq_order in lstSeqOrder) // usuaaly have one record, combination will have more than one records
        {
          string rec_str = "";
          List<BatchRecord> lstRec = lookup[seq_order].ToList();
          foreach (BatchRecord rec in lstRec)
          {
            rec_str += FormatRecord(rec);
            rec_str += " ";
          }

          rec_str = rec_str.Trim();
          // format value based on fixed lenth, ... do not append dilimiter yet
          rec_str = rec_str.FormatValue(_config_data.general_info.record_type.value, lstRec[0]);

          // set record item
          RecordItem rec_item = new RecordItem();
          // sequence order
          rec_item.seq_order = seq_order;
          // tag source value
          if (string.Compare(lstRec[0].source_type.value, "Fixed_Value", true) == 0)
            rec_item.tag = "Fixed_Value";
          else
            rec_item.tag = lstRec[0].source.value;
          rec_item.value = rec_str;
          rec_item.rec_config = lstRec[0];
          bu_rec.lstRec.Add(rec_item);
        }

        // I needed add payfile number, reference number, group id, gl number, transaction discription for later use
        bu_rec.pay_file_name = _geoFile.GetCoreFileName(null);
        bu_rec.dept_id = _geoFile.GetWorkgroupId();
        bu_rec.amt = _total_amount;
      }
      return bu_rec;

    }

    protected string FormatRecord(BatchRecord rec)
    {
      string rec_str = "";
      string source_type = rec.source_type.value;
      if (string.Compare(source_type, "STD_DATA", true) == 0)
      {
        rec_str = FormatStandardDataField(rec);
      }
      else if (string.Compare(source_type, "FIXED_VALUE", true) == 0)
      {
        rec_str = rec.fixed_value;
      }
      return rec_str;
    }

    protected string FormatStandardDataField(BatchRecord rec)
    {
      string rec_str = "";
      string source = rec.source.value;
      if (string.Compare(source, "DepFile_Name", true) == 0)
      {
        rec_str = _geoFile.GetCoreFileName(rec.format.value);
      }
      else if (string.Compare(source, "File_Open_Date", true) == 0 || string.Compare(source, "Post_Date", true) == 0) // file open date
      {
        DateTime date = DateTime.Now;
        if (_geoFile != null)
        {
          string date_str = _geoFile.get("OPENDT", "").ToString();
          if (!string.IsNullOrEmpty(date_str))
            date = DateTime.Parse(date_str);
          else
            date = DateTime.Now;
        }

        rec_str = date.FormatDate(rec.format.value);
      }
      else if (string.Compare(source, "File_Effective_Date", true) == 0)
      {
        DateTime date = DateTime.Now;
        if (_geoFile != null)
        {
          string date_str = _geoFile.get("EFFECTIVEDT", "").ToString();
          if (!string.IsNullOrEmpty(date_str))
            date = DateTime.Parse(date_str);
          else
            date = DateTime.Now;
        }

        rec_str = date.FormatDate(rec.format.value);
      }
      else if (string.Compare(source, "User_Name", true) == 0)
      {
        rec_str = _geoFile.GetFileOpenUserName();
      }
      else if (string.Compare(source, "Workgroup_Id", true) == 0)
      {
        rec_str = _geoFile.GetWorkgroupId();
      }
      else if (string.Compare(source, "Workgroup_Name", true) == 0)
      {
        rec_str = _geoFile.GetWorkgroupDescription();
      }
      else if (string.Compare(source, "Total_Amount", true) == 0 || string.Compare(source, "Total_Absolute_Amount", true) == 0)
      {
        double amount = _total_amount;
        rec_str = amount.FormatAmount(rec.format.value).FormatValue(_config_data.general_info.record_type.value, rec);
      }
      else if (string.Compare(source, "Total_Count", true) == 0)
      {
        rec_str = _total_count.ToString();
      }

      return rec_str;
    }

  }
}
