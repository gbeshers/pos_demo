﻿using CASL_engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomizedBatchUpdate
{
  // used for general purpose, such as consolidate, group by, format final lines.
  public class BatchUpdateProcessor
  {
    private BatchUpdateConfigData _config_data { get; set; }
    private List<BatchDetailRecord> _lstDetailRecord { get; set; }

    private BatchUpdateData batch_update_data { get; set; }

    public BatchUpdateProcessor(BatchUpdateConfigData config_data, List<BatchDetailRecord> lstDetailRecord)
    {
      _config_data = config_data;

      // processed detail records
      _lstDetailRecord = lstDetailRecord;

      // initial batch_update_data
      batch_update_data = new BatchUpdateData();
    }


    public string Process()
    {
      Logger.cs_log("Start BatchUpdateProcessor.Process");
      // check if layer_outlines is configured
      // layer outlines could include Batch_Level_Layer, CoreFile_Level_Layer, WG_Level_Layer, GL_Level_Layer, TT_Level_Layer, CF_Level_Layer, Detail_Level_Layer,see LayerType
      if (!(_config_data != null && _config_data.general_info != null && _config_data.general_info.layer_outlines != null))
        return "";

      try
      {
        // Step 1: Process data for each outline/layer and group data by outline layer info
        Dictionary<List<RecordGroupInfo>, List<BatchDetailRecord>> my_dic = new Dictionary<List<RecordGroupInfo>, List<BatchDetailRecord>>();
        List<string> lst_parent_layer = new List<string>();
        for (int i = 0; i < _config_data.general_info.layer_outlines.Count; i++)
        {
          // I need to make sure layer information is configured for this outline
          if (_config_data.general_info.layers.HasOutline(_config_data.general_info.layer_outlines[i]))
          {
            if (_config_data.general_info.layer_outlines[i] != nameof(LayerType.Detail_Level_Layer))
            {
              // the smaller of i, the higer of the level
              lst_parent_layer.Add(_config_data.general_info.layer_outlines[i]);
              my_dic = GroupBatchDetailRecords(lst_parent_layer);

              // process 
              switch (_config_data.general_info.layer_outlines[i])
              {
                case nameof(LayerType.Batch_Level_Layer):
                  ProcessBatchLevel(my_dic);
                  break;
                case nameof(LayerType.WG_Level_Layer):
                  ProcessWGLevel(my_dic);
                  break;
                case nameof(LayerType.CoreFile_Level_Layer):
                  ProcessCoreFileLevel(my_dic);
                  break;
                case nameof(LayerType.GL_Level_Layer):
                  ProcessGLLevel(my_dic);
                  break;
                case nameof(LayerType.TT_Level_Layer):
                  ProcessTransactionTypeLevel(my_dic);
                  break;
                case nameof(LayerType.CF_Level_Layer):
                  ProcessCustomFieldLevel(my_dic);
                  break;

              }
            }
            else
            {
              // detail must be lowest level
              ProcessDetailLevel(lst_parent_layer);
            }
          }


        }

        // step 2: arragement
        List<BatchUpdateLineRec> lst = new List<BatchUpdateLineRec>();
        List<RecordGroupInfo> lst_group_info = new List<RecordGroupInfo>();
        lst = Arrangement(lst, lst_group_info);

        // step 3: generate batch text
        StringBuilder sbFileContent = new StringBuilder();
        foreach (BatchUpdateLineRec rec in lst)
        {
          string rec_line_text = FormatLine(rec);
          if (_config_data.general_info.include_eoln)
            sbFileContent.AppendLine(rec_line_text);
          else
            sbFileContent.Append(rec_line_text);
        }

        Logger.cs_log("End BatchUpdateProcessor");
        return sbFileContent.ToString();
      }
      catch (Exception ex)
      {
        string error = "Error in BatchUpdateProcessor.Process: " + ex.Message.ToString();
        Logger.cs_log("End BatchUpdateProcessor.Process Failed: " + ex.Message.ToString());
        throw new Exception(error);
      }

    }


    /// <summary>
    /// recursion function
    /// Arrangment batch update records
    /// </summary>
    /// <param name="lst"></param>
    /// <param name="lst_group_info"></param>
    /// <param name="next_level"></param>
    /// <returns></returns>
    protected List<BatchUpdateLineRec> Arrangement(List<BatchUpdateLineRec> lst, List<RecordGroupInfo> lst_group_info, int next_level = 0)
    {
      try
      {
        // if next_level = 0, it is top level - batch level
        string outline_level = _config_data.general_info.layer_outlines.GetItemValue(next_level);
        if (string.IsNullOrEmpty(outline_level))
          return lst;

        if (_config_data.general_info.layers.HasOutline(outline_level))
        {
          if (outline_level != nameof(LayerType.Detail_Level_Layer))
          {
            // Header/trailer information for batch, workgroup, payfile, gl level.. 
            Dictionary<List<RecordGroupInfo>, Dictionary<String, BatchUpdateLineRec>> dicHT = GetLayerHTBatchData(outline_level);
            foreach (KeyValuePair<List<RecordGroupInfo>, Dictionary<String, BatchUpdateLineRec>> pair in dicHT)
            {
              List<BatchUpdateLineRecWithLayerInfo> lst_ordered_line_rec = pair.Value.GetOrderedBatchLineRec(_config_data.general_info.layers);

              // headers
              foreach (BatchUpdateLineRecWithLayerInfo line_rec_with_layer_info in lst_ordered_line_rec)
              {
                // check and if they are header add them to the list
                bool header = string.Compare(line_rec_with_layer_info.layer_info.type, "header", true) == 0 ? true : false;
                if (header)
                {
                  lst.Add(line_rec_with_layer_info.line_rec);
                }
              }

              // I need get set next_layer, lst_ordered_line_rec should contain at least one record
              string my_layer_type = lst_ordered_line_rec[0].layer_info.layer_type.value;
              next_level = _config_data.general_info.layer_outlines.FindIndex(x => x == my_layer_type) + 1;

              // process next level
              Arrangement(lst, pair.Key, next_level);

              // trailers
              foreach (BatchUpdateLineRecWithLayerInfo line_rec_with_layer_info in lst_ordered_line_rec)
              {
                // check and if they are trailer add them to the list
                bool trailer = string.Compare(line_rec_with_layer_info.layer_info.type, "trailer", true) == 0 ? true : false;
                if (trailer)
                {
                  lst.Add(line_rec_with_layer_info.line_rec);
                }
              }
            }
          }
          else // lowest level;inner groups
          {
            List<BatchUpdateLineRec> my_lst = ArrangementForBatchDetailRecords2(lst_group_info);
            lst.AddRange(my_lst);
          }
        }
        else
        {
          // process next level
          Arrangement(lst, lst_group_info, next_level+1);
        }

        return lst;
      }
      catch (Exception ex)
      {
        string error = "Error on BatchUpdateProcessor.Arrangement: " + ex.Message.ToString();
        throw new Exception(error);
      }


    }

    /// <summary>
    /// Process arrangement for Batch Detail Records
    /// </summary>
    /// <param name="lst_group_info"></param>
    /// <returns></returns>
    protected List<BatchUpdateLineRec> ArrangementForBatchDetailRecords(List<RecordGroupInfo> lst_group_info)
    {
      List<BatchUpdateLineRec> lst = new List<BatchUpdateLineRec>();

      try
      {
        List<BatchDetailRecord> lst_detail_rec = GetBatchDetailRecords(lst_group_info);

        // consolidate if applied
        if (_config_data.general_info.summary_mode.mode == true)
          lst_detail_rec = Consolidate(lst_detail_rec); // consolidated payments with transaction type has summary mode; not consolidate payment with detail mode

        // should add detail like as below if there are multiple detail layers configured, for example see CoSD SAP utility batch update
        // item1 record1
        // item2 record1
        // item1 record2
        // item2 record2

        // for each detail record or summarized record
        int rec_seq_nbr = 0;
        foreach (BatchDetailRecord batch_detail_rec in lst_detail_rec)
        {
          rec_seq_nbr++; // increase record sequence number

          // I need order by layer's sequence order
          List<BatchUpdateLineRecWithLayerInfo> lst_line_rec_with_layer_info = batch_detail_rec.GetBatchLineRecOrderbyLayer(_config_data.general_info.layers);
          foreach (BatchUpdateLineRecWithLayerInfo rec in lst_line_rec_with_layer_info)
          {
            // IPAY-1262 HX,  if record sequence is configured, update record sequence number
            foreach (RecordItem rec_item in rec.line_rec.lstRec)
            {
              if (string.Compare(rec_item.rec_config.source.value, "Record_Sequence_Number", true) == 0)
              {
                rec_item.value = rec_seq_nbr.ToString().FormatValue(_config_data.general_info.record_type.value, rec_item.rec_config);
              }
            }
            lst.Add(rec.line_rec);
          }

        }

        // Get list for layer with summary consolidated record; such as consolidated offset and then cosolidation
        // usually for detail mode, because records have been already consolidate above
        Dictionary<string, List<BatchUpdateLineRec>> my_dic = new Dictionary<string, List<BatchUpdateLineRec>>(); // key is layer id
        foreach (BatchDetailRecord batch_detail_rec in lst_detail_rec)
        {
          foreach (KeyValuePair<string, BatchUpdateLineRec> pair in batch_detail_rec.dicLineRec) // key is layer id
          {
            Layer layer_info = _config_data.general_info.layers.GetLayerInfo(pair.Key);
            if (layer_info.sum_consolidation)
            {
              pair.Value.record_mode = RecordMode.summary; // set record mode to summary
              if (my_dic.ContainsKey(pair.Key))
                my_dic[pair.Key].Add(pair.Value);
              else
              {
                List<BatchUpdateLineRec> my_list = new List<BatchUpdateLineRec>();
                my_list.Add(pair.Value);
                my_dic.Add(pair.Key, my_list);
              }
            }
          }
        }

        // consolidate and update record sequence number if applied
        foreach (KeyValuePair<string, List<BatchUpdateLineRec>> pair_2 in my_dic)
        {
          List<BatchUpdateLineRec> my_cosolidated = Consolidate(pair_2.Value);

          // I need update sequence number
          // IPAY-1262 HX,  if record sequence is configured, update record sequence number
          foreach (BatchUpdateLineRec bu_line_rec in my_cosolidated)
          {
            rec_seq_nbr++;
            foreach (RecordItem rec_item in bu_line_rec.lstRec)
            {
              if (string.Compare(rec_item.rec_config.source.value, "Record_Sequence_Number", true) == 0)
              {
                rec_item.value = rec_seq_nbr.ToString().FormatValue(_config_data.general_info.record_type.value, rec_item.rec_config);
              }
            }
          }

          lst.AddRange(my_cosolidated);
        }

        return lst;
      }
      catch (Exception ex)
      {
        string error = "Error in BatchUpdateProcessor.ArrangementForBatchDetailRecords" + ex.Message.ToString();
        throw new Exception(error);
      }

    }

    /// <summary>
    /// arrangment batch detail records based on summarization, sequence in groups (could be by core file, transaction type, gl..) specified  
    /// and be ready to print.
    /// </summary>
    /// <param name="lst_group_info"></param>
    /// <returns></returns>
    protected List<BatchUpdateLineRec> ArrangementForBatchDetailRecords2(List<RecordGroupInfo> lst_group_info)
    {
      List<BatchUpdateLineRec> lst = new List<BatchUpdateLineRec>();

      try
      {
        List<BatchDetailRecord> lst_detail_rec = GetBatchDetailRecords(lst_group_info);

        // consolidate if applied
        if (_config_data.general_info.summary_mode.mode == true)
          lst_detail_rec = Consolidate(lst_detail_rec); // consolidated payments with transaction type has summary mode; not consolidate payment with detail mode


        // Get list of BatchDetailRecord for layer with offset consolidation; if it has consolidation option and then cosolidation
        List<BatchDetailRecord> lst_offset_with_consolidation = new List<BatchDetailRecord>();

        if (_config_data.general_info.offset_summarization == true)
        {
          foreach (BatchDetailRecord batch_detail_rec in lst_detail_rec)
          {
            bool offset_consolidate = false;
            BatchDetailRecord updated_batch_detail = new BatchDetailRecord();
            updated_batch_detail.dicLineRec = new Dictionary<string, BatchUpdateLineRec>();
            updated_batch_detail.dic_groupby_data = batch_detail_rec.dic_groupby_data;

            foreach (KeyValuePair<string, BatchUpdateLineRec> pair in batch_detail_rec.dicLineRec) // key is layer id
            {
              Layer layer_info = _config_data.general_info.layers.GetLayerInfo(pair.Key);
              if (layer_info.sum_consolidation)
              {
                offset_consolidate = true;
                updated_batch_detail.dicLineRec.Add(pair.Key, pair.Value);
              }
            }

            if (offset_consolidate)
              lst_offset_with_consolidation.Add(updated_batch_detail);
          }

          lst_offset_with_consolidation = Consolidate(lst_offset_with_consolidation, true);
        }


        // should add detail like as below if there are multiple detail layers configured, for example see CoSD SAP utility batch update
        // A record1
        // B record1
        // A record2
        // B record2
        // ...

        // for each detail record or summarized record
        string current_group_key = "";
        int rec_seq_nbr = 0;
        foreach (BatchDetailRecord batch_detail_rec in lst_detail_rec)
        {
          if (batch_detail_rec.group_key != current_group_key)
          {
            // I need to check if there is offset with summary consolidation
            BatchDetailRecord offset = FindBatchDetailRecordByGroupKey(lst_offset_with_consolidation, current_group_key);
            if (offset != null)
            {
              rec_seq_nbr++; // increase record sequence number for offset ??
              List<BatchUpdateLineRec> my_lst = RetriveBatchUpdateLineRecs(offset, rec_seq_nbr, true);
              lst.AddRange(my_lst);
            }

            current_group_key = batch_detail_rec.group_key;
          }

          rec_seq_nbr++; // increase record sequence number
          List<BatchUpdateLineRec> my_lst2 = RetriveBatchUpdateLineRecs(batch_detail_rec, rec_seq_nbr);
          lst.AddRange(my_lst2);
        }

        // I need to check if there is offset for last set
        BatchDetailRecord offset2 = FindBatchDetailRecordByGroupKey(lst_offset_with_consolidation, current_group_key);
        if (offset2 != null)
        {
          rec_seq_nbr++; // increase record sequence number for offset ??
          List<BatchUpdateLineRec> my_lst3 = RetriveBatchUpdateLineRecs(offset2, rec_seq_nbr, true);
          lst.AddRange(my_lst3);
        }

        return lst;
      }
      catch (Exception ex)
      {
        string error = "Error in BatchUpdateProcessor.ArrangementForBatchDetailRecords2: " + ex.Message.ToString();
        throw new Exception(error);
      }
    }

    /// <summary>
    /// Retrive Batch Update Line Rec Info from batch detail record and update record sequence number if applied
    /// </summary>
    /// <param name="batch_detail_rec"></param>
    /// <param name="rec_seq_nbr"></param>
    /// <returns></returns>
    protected List<BatchUpdateLineRec> RetriveBatchUpdateLineRecs(BatchDetailRecord batch_detail_rec, int rec_seq_nbr, bool offset = false)
    {
      List<BatchUpdateLineRec> lst = new List<BatchUpdateLineRec>();

      try
      {
        // I need order by layer's sequence order
        List<BatchUpdateLineRecWithLayerInfo> lst_offset_with_layer_info = batch_detail_rec.GetBatchLineRecOrderbyLayer(_config_data.general_info.layers);
        foreach (BatchUpdateLineRecWithLayerInfo rec in lst_offset_with_layer_info)
        {
          if (offset == false) // detail records
          {
            if (rec.layer_info.sum_consolidation == false) // do not process offset with summary consolidation
            {
              // IPAY-1262 HX, if record sequence is configured, update record sequence number
              foreach (RecordItem rec_item in rec.line_rec.lstRec)
              {
                if (string.Compare(rec_item.rec_config.source.value, "Record_Sequence_Number", true) == 0)
                {
                  rec_item.value = rec_seq_nbr.ToString().FormatValue(_config_data.general_info.record_type.value, rec_item.rec_config);
                }
              }
              lst.Add(rec.line_rec);
            }
          }
          else // offset with summary consolidation
          {
            // IPAY-1262 HX, if record sequence is configured, update record sequence number
            foreach (RecordItem rec_item in rec.line_rec.lstRec)
            {
              if (string.Compare(rec_item.rec_config.source.value, "Record_Sequence_Number", true) == 0)
              {
                rec_item.value = rec_seq_nbr.ToString().FormatValue(_config_data.general_info.record_type.value, rec_item.rec_config);
              }
            }
            lst.Add(rec.line_rec);
          }

        }
        return lst;
      }
      catch (Exception ex)
      {
        string error = "Error in BatchUpdateProcessor.RetriveBatchUpdateLineRecs: " + ex.Message.ToString();
        throw new Exception(error);
      }

    }

    protected BatchDetailRecord FindBatchDetailRecordByGroupKey(List<BatchDetailRecord> lst, string group_key)
    {
      BatchDetailRecord batch_detail_rec = null;
      foreach (BatchDetailRecord rec in lst)
      {
        if (rec.group_key == group_key)
        {
          batch_detail_rec = rec; // if have, should have only one
          break;
        }
      }
      return batch_detail_rec;
    }
    protected List<BatchDetailRecord> GetBatchDetailRecords(List<RecordGroupInfo> lst_group_info)
    {
      List<BatchDetailRecord> lst = new List<BatchDetailRecord>();

      // using contains key does not work
      // if (batch_update_data.dicDetailLayerRecord.ContainsKey(lst_group_info))
      // lst = batch_update_data.dicDetailLayerRecord[lst_group_info];

      foreach (KeyValuePair<List<RecordGroupInfo>, List<BatchDetailRecord>> pair in batch_update_data.dicDetailLayerRecord)
      {
        List<RecordGroupInfo> my_list_group_info = pair.Key;

        // compair my_list_group_info with list_group_info
        bool found = true;
        if (my_list_group_info.Count != lst_group_info.Count)
          found = false;
        else
        {
          for (int i = 0; i < my_list_group_info.Count; i++)
          {
            if (my_list_group_info[i].layer_name != lst_group_info[i].layer_name || my_list_group_info[i].str_value != lst_group_info[i].str_value)
            {
              found = false;
              break;
            }
          }
        }

        if (found)
        {
          lst = pair.Value;
          break;
        }
      }

      return lst;
    }

    /// <summary>
    /// Get Header or Trailer Batch Update Info for specific layer
    /// </summary>
    /// <param name="layer_name">Layer Name </param>
    /// <returns></returns>
    protected Dictionary<List<RecordGroupInfo>, Dictionary<String, BatchUpdateLineRec>> GetLayerHTBatchData(string layer_name)
    {
      Dictionary<List<RecordGroupInfo>, Dictionary<String, BatchUpdateLineRec>> dic = new Dictionary<List<RecordGroupInfo>, Dictionary<String, BatchUpdateLineRec>>();
      switch (layer_name)
      {
        case nameof(LayerType.Batch_Level_Layer):
          dic = batch_update_data.dicBatchLayerRecord;
          break;
        case nameof(LayerType.WG_Level_Layer):
          dic = batch_update_data.dicWGLayerRecord;
          break;
        case nameof(LayerType.CoreFile_Level_Layer):
          dic = batch_update_data.dicCoreFileLayerRecord;
          break;
        case nameof(LayerType.GL_Level_Layer):
          dic = batch_update_data.dicGLLayerRecord;
          break;
        case nameof(LayerType.CF_Level_Layer):
          dic = batch_update_data.dicCFLayerRecord;
          break;
        case nameof(LayerType.TT_Level_Layer):
          dic = batch_update_data.dicTTLayerRecord;
          break;
      }

      return dic;
    }

    #region not_used
    /*protected Dictionary<List<RecordGroupInfo>, List<BatchUpdateLineRec>> GetLayerDetailBatchUpdateLnRecord(string current_layer, List<RecordGroupInfo> lst_layer_gourp_info, List<BatchUpdateLineRec> lstDetailLineRec)
    {
      Dictionary<List<RecordGroupInfo>, List<BatchUpdateLineRec>> rst = new Dictionary<List<RecordGroupInfo>, List<BatchUpdateLineRec>>();

      lst_layer_gourp_info = lst_layer_gourp_info ?? new List<RecordGroupInfo>();

      if (current_layer == nameof(LayerType.CoreFile_Level_Layer))
      {
        Dictionary<GenericObject, List<BatchUpdateLineRec>> dic = new Dictionary<GenericObject, List<BatchUpdateLineRec>>();
        dic = lstDetailLineRec.GroupBy(o => o.dic_groupby_data[current_layer].geo_value).ToDictionary(o => o.Key, o => o.ToList());


        foreach (KeyValuePair<GenericObject, List<BatchUpdateLineRec>> pair in dic)
        {
          List<RecordGroupInfo> my_lst_parent_layer = new List<RecordGroupInfo>();
          my_lst_parent_layer.AddRange(lst_layer_gourp_info);
          RecordGroupInfo group_info = new RecordGroupInfo();
          group_info.layer_name = current_layer;
          group_info.str_value = pair.Key.GetCoreFileName(null);
          group_info.geo_value = pair.Key;
          my_lst_parent_layer.Add(group_info);
          rst.Add(my_lst_parent_layer, pair.Value);
        }
      }
      else
      {
        Dictionary<string, List<BatchUpdateLineRec>> dic = new Dictionary<string, List<BatchUpdateLineRec>>();
        dic = lstDetailLineRec.GroupBy(o => o.dic_groupby_data[current_layer].str_value).ToDictionary(o => o.Key, o => o.ToList());

        foreach (KeyValuePair<string, List<BatchUpdateLineRec>> pair in dic)
        {
          List<RecordGroupInfo> my_lst_parent_layer = new List<RecordGroupInfo>();
          my_lst_parent_layer.AddRange(lst_layer_gourp_info);

          RecordGroupInfo group_info = new RecordGroupInfo();
          group_info.layer_name = current_layer;
          group_info.str_value = pair.Key;
          group_info.geo_value = null;
          my_lst_parent_layer.Add(group_info);
          rst.Add(my_lst_parent_layer, pair.Value);
        }
      }
      return rst;
    }

    // recusive function I need test
    protected Dictionary<List<RecordGroupInfo>, List<BatchUpdateLineRec>> GetDetailBatchUpdateLineRecord(List<string> lst_parent_layer, Dictionary<List<RecordGroupInfo>, List<BatchUpdateLineRec>> my_dic)
    {
      Dictionary<List<RecordGroupInfo>, List<BatchUpdateLineRec>> dicTemp = new Dictionary<List<RecordGroupInfo>, List<BatchUpdateLineRec>>();
      if (lst_parent_layer.Count == 0)
        return my_dic;

      foreach (KeyValuePair<List<RecordGroupInfo>, List<BatchUpdateLineRec>> top_pair in my_dic)
      {
        Dictionary<List<RecordGroupInfo>, List<BatchUpdateLineRec>> result = GetLayerDetailBatchUpdateLnRecord(lst_parent_layer[0], top_pair.Key, top_pair.Value);

        foreach (KeyValuePair<List<RecordGroupInfo>, List<BatchUpdateLineRec>> result_pair in result)
        {
          dicTemp.Add(result_pair.Key, result_pair.Value);
        }
      }

      lst_parent_layer.RemoveAt(0);
      return GetDetailBatchUpdateLineRecord(lst_parent_layer, dicTemp);

      //return my_dic;
    }

    protected Dictionary<List<RecordGroupInfo>, List<BatchUpdateLineRec>> GetDetailBatchUpdateLineRecord(List<string> lst_parent_layer, List<BatchUpdateLineRec> default_lstDetailLineRec)
    {
      Dictionary<List<RecordGroupInfo>, List<BatchUpdateLineRec>> my_dic = new Dictionary<List<RecordGroupInfo>, List<BatchUpdateLineRec>>();
      try
      {
        List<RecordGroupInfo> layer_group_info = new List<RecordGroupInfo>();
        my_dic.Add(layer_group_info, default_lstDetailLineRec);

        // create new list for parent_layer, I need edit it during the process. 
        List<string> my_lst_parent_layer = new List<string>();
        my_lst_parent_layer.AddRange(lst_parent_layer);

        my_dic = GetDetailBatchUpdateLineRecord(my_lst_parent_layer, my_dic);
      }
      catch (Exception ex)
      {
        Logger.cs_log(ex.ToMessageAndCompleteStacktrace());
      }

      return my_dic;
    }
    */
    #endregion

    /// <summary>
    /// Group Batch Detail Records by current layer
    /// </summary>
    /// <param name="current_layer"></param> further group by current outline layer
    /// <param name="lst_layer_gourp_info"></param> previous group info
    /// <param name="lst_batch_detail"></param> batch detail records for previous group info
    /// <returns></returns>
    protected Dictionary<List<RecordGroupInfo>, List<BatchDetailRecord>> GroupBatchDetailRecordsByCurrentLayer(string current_layer, List<RecordGroupInfo> lst_layer_gourp_info, List<BatchDetailRecord> lst_batch_detail)
    {
      Dictionary<List<RecordGroupInfo>, List<BatchDetailRecord>> rst = new Dictionary<List<RecordGroupInfo>, List<BatchDetailRecord>>();

      lst_layer_gourp_info = lst_layer_gourp_info ?? new List<RecordGroupInfo>();

      try
      {
        if (current_layer == nameof(LayerType.CoreFile_Level_Layer))
        {
          Dictionary<GenericObject, List<BatchDetailRecord>> dic = new Dictionary<GenericObject, List<BatchDetailRecord>>();
          dic = lst_batch_detail.GroupBy(o => o.dic_groupby_data[current_layer].geo_value).ToDictionary(o => o.Key, o => o.ToList());


          foreach (KeyValuePair<GenericObject, List<BatchDetailRecord>> pair in dic)
          {
            List<RecordGroupInfo> my_lst_parent_layer = new List<RecordGroupInfo>();
            my_lst_parent_layer.AddRange(lst_layer_gourp_info);
            RecordGroupInfo group_info = new RecordGroupInfo();
            group_info.layer_name = current_layer;
            group_info.str_value = pair.Key.GetCoreFileName(null);
            group_info.geo_value = pair.Key;
            my_lst_parent_layer.Add(group_info);
            rst.Add(my_lst_parent_layer, pair.Value);
          }
        }
        else
        {
          Dictionary<string, List<BatchDetailRecord>> dic = new Dictionary<string, List<BatchDetailRecord>>();
          dic = lst_batch_detail.GroupBy(o => o.dic_groupby_data[current_layer].str_value).ToDictionary(o => o.Key, o => o.ToList());

          foreach (KeyValuePair<string, List<BatchDetailRecord>> pair in dic)
          {
            List<RecordGroupInfo> my_lst_parent_layer = new List<RecordGroupInfo>();
            my_lst_parent_layer.AddRange(lst_layer_gourp_info);

            RecordGroupInfo group_info = new RecordGroupInfo();
            group_info.layer_name = current_layer;
            group_info.str_value = pair.Key;
            group_info.geo_value = null;
            my_lst_parent_layer.Add(group_info);
            rst.Add(my_lst_parent_layer, pair.Value);
          }
        }
        return rst;
      }
      catch (Exception ex)
      {
        string error = "Error in BatchUpdateProcessor.GroupBatchDetailRecordsByCurrentLayer: " + ex.Message.ToString();
        throw new Exception(error);
      }
    }

    /// <summary>
    /// recursion function
    /// Group Batch Detail Records by outline layers
    /// </summary>
    /// <param name="lst_parent_layer"></param> outline layers, such as workgroup, corefile, gl...
    /// <param name="my_dic"></param>
    /// <returns></returns>
    protected Dictionary<List<RecordGroupInfo>, List<BatchDetailRecord>> RecGroupBatchDetailRecords(List<string> lst_parent_layer, Dictionary<List<RecordGroupInfo>, List<BatchDetailRecord>> my_dic)
    {
      Dictionary<List<RecordGroupInfo>, List<BatchDetailRecord>> dicTemp = new Dictionary<List<RecordGroupInfo>, List<BatchDetailRecord>>();
      if (lst_parent_layer.Count == 0)
        return my_dic;

      try
      {
        foreach (KeyValuePair<List<RecordGroupInfo>, List<BatchDetailRecord>> top_pair in my_dic)
        {
          Dictionary<List<RecordGroupInfo>, List<BatchDetailRecord>> result = GroupBatchDetailRecordsByCurrentLayer(lst_parent_layer[0], top_pair.Key, top_pair.Value);

          foreach (KeyValuePair<List<RecordGroupInfo>, List<BatchDetailRecord>> result_pair in result)
          {
            dicTemp.Add(result_pair.Key, result_pair.Value);
          }
        }

        lst_parent_layer.RemoveAt(0);
        return RecGroupBatchDetailRecords(lst_parent_layer, dicTemp);
      }
      catch (Exception ex)
      {
        string error = "Error in BatchUpdateProcessor.RecGroupBatchDetailRecords: " + ex.Message.ToString();
        throw new Exception(error);
      }
    }

    /// <summary>
    /// Group Batch Detail Records by lst_parent_layers
    /// </summary>
    /// <param name="lst_parent_layer"></param>
    /// <returns></returns>
    protected Dictionary<List<RecordGroupInfo>, List<BatchDetailRecord>> GroupBatchDetailRecords(List<string> lst_parent_layer)
    {
      // key is list of groupby information
      Dictionary<List<RecordGroupInfo>, List<BatchDetailRecord>> my_dic = new Dictionary<List<RecordGroupInfo>, List<BatchDetailRecord>>();
      try
      {
        List<RecordGroupInfo> layer_group_info = new List<RecordGroupInfo>();
        my_dic.Add(layer_group_info, _lstDetailRecord);

        // create new list for parent_layer, I need edit it during the process. 
        List<string> my_lst_parent_layer = new List<string>();
        my_lst_parent_layer.AddRange(lst_parent_layer);

        my_dic = RecGroupBatchDetailRecords(my_lst_parent_layer, my_dic);
      }
      catch (Exception ex)
      {
        Logger.cs_log(ex.ToMessageAndCompleteStacktrace());
      }

      return my_dic;
    }

    protected void ProcessBatchLevel(Dictionary<List<RecordGroupInfo>, List<BatchDetailRecord>> dic)
    {
      try
      {
        foreach (KeyValuePair<List<RecordGroupInfo>, List<BatchDetailRecord>> pair in dic)
        {
          // calculate 
          double total_amt = 0.0;
          long total_cnt = 0;

          foreach (BatchDetailRecord rec in pair.Value)
          {
            BatchUpdateLineRec line_rec = rec.dicLineRec.GetFirstOrDefault();
            total_amt += line_rec.amt;
            total_cnt++;
          }

          BatchFileLevelProcessor processor = new BatchFileLevelProcessor(_config_data, total_cnt, total_amt, pair.Key);
          Dictionary<string, BatchUpdateLineRec> dicBatchLineRec = processor.Process(); // key is layer id
          batch_update_data.dicBatchLayerRecord.Add(pair.Key, dicBatchLineRec);
        }
      }
      catch (Exception ex)
      {
        string error = "Error in BatchUpdateProcessor.ProcessBatchLevel: " + ex.Message.ToString();
        throw new Exception(error);
      }


    }

    protected void ProcessWGLevel(Dictionary<List<RecordGroupInfo>, List<BatchDetailRecord>> dic)
    {
      try
      {
        foreach (KeyValuePair<List<RecordGroupInfo>, List<BatchDetailRecord>> pair in dic)
        {
          double total_amt = 0.0;
          long total_cnt = 0;
          foreach (BatchDetailRecord rec in pair.Value)
          {
            BatchUpdateLineRec line_rec = rec.dicLineRec.GetFirstOrDefault();
            total_amt += line_rec.amt;
            total_cnt++;
          }

          // process workgroup; 
          WorkgroupLevelProcessor wg_level_processor = new WorkgroupLevelProcessor(_config_data, total_cnt, total_amt, pair.Key);
          Dictionary<string, BatchUpdateLineRec> dicWGLineRec = wg_level_processor.Process(); // key is configured layer id
          batch_update_data.dicWGLayerRecord.Add(pair.Key, dicWGLineRec); // pair.key contains group info which should be unique
        }
      }
      catch (Exception ex)
      {
        string error = "Error in BatchUpdateProcessor.ProcessWGLevel: " + ex.Message.ToString();
        throw new Exception(error);
      }
    }

    protected void ProcessCoreFileLevel(Dictionary<List<RecordGroupInfo>, List<BatchDetailRecord>> dic)
    {
      try
      {
        foreach (KeyValuePair<List<RecordGroupInfo>, List<BatchDetailRecord>> pair in dic)
        {
          double total_amt = 0.0;
          int total_cnt = 0;
          foreach (BatchDetailRecord rec in pair.Value)
          {
            BatchUpdateLineRec line_rec = rec.dicLineRec.GetFirstOrDefault();
            total_amt += line_rec.amt;
            total_cnt++;
          }

          // pay file level
          PayfileLevelProcessor processor = new PayfileLevelProcessor(_config_data, total_cnt, total_amt, pair.Key);
          Dictionary<string, BatchUpdateLineRec> dicCoreFileLineRec = processor.Process();
          batch_update_data.dicCoreFileLayerRecord.Add(pair.Key, dicCoreFileLineRec);

        }
      }
      catch (Exception ex)
      {
        string error = "Error in BatchUpdateProcessor.ProcessCoreFileLevel: " + ex.Message.ToString();
        throw new Exception(error);
      }
    }

    protected void ProcessGLLevel(Dictionary<List<RecordGroupInfo>, List<BatchDetailRecord>> dic)
    {
      try
      {
        foreach (KeyValuePair<List<RecordGroupInfo>, List<BatchDetailRecord>> pair in dic)
        {
          double total_amt = 0.0;
          long total_cnt = 0;
          foreach (BatchDetailRecord rec in pair.Value)
          {
            BatchUpdateLineRec line_rec = rec.dicLineRec.GetFirstOrDefault();
            total_amt += line_rec.amt;
            total_cnt++;
          }

          GLLevelProcessor processor = new GLLevelProcessor(_config_data, total_cnt, total_amt, pair.Key);
          Dictionary<string, BatchUpdateLineRec> dicGLLine = processor.Process();
          batch_update_data.dicCFLayerRecord.Add(pair.Key, dicGLLine);
        }
      }
      catch (Exception ex)
      {
        string error = "Error in BatchUpdateProcessor.ProcessGLLevel: " + ex.Message.ToString();
        throw new Exception(error);
      }

    }

    protected void ProcessCustomFieldLevel(Dictionary<List<RecordGroupInfo>, List<BatchDetailRecord>> dic)
    {
      try
      {
        foreach (KeyValuePair<List<RecordGroupInfo>, List<BatchDetailRecord>> pair in dic)
        {
          double total_amt = 0.0;
          long total_cnt = 0;
          foreach (BatchDetailRecord rec in pair.Value)
          {
            BatchUpdateLineRec line_rec = rec.dicLineRec.GetFirstOrDefault();
            total_amt += line_rec.amt;
            total_cnt++;
          }

          CustomFieldLevelProcessor processor = new CustomFieldLevelProcessor(_config_data, total_cnt, total_amt, pair.Key);
          Dictionary<string, BatchUpdateLineRec> dicCFLine = processor.Process();
          batch_update_data.dicCFLayerRecord.Add(pair.Key, dicCFLine);
        }
      }
      catch (Exception ex)
      {
        string error = "Error in BatchUpdateProcessor.ProcessCustomFieldLevel: " + ex.Message.ToString();
        throw new Exception(error);
      }
    }

    protected void ProcessTransactionTypeLevel(Dictionary<List<RecordGroupInfo>, List<BatchDetailRecord>> dic)
    {
      try
      {
        foreach (KeyValuePair<List<RecordGroupInfo>, List<BatchDetailRecord>> pair in dic)
        {
          double total_amt = 0.0;
          long total_cnt = 0;
          foreach (BatchDetailRecord rec in pair.Value)
          {
            BatchUpdateLineRec line_rec = rec.dicLineRec.GetFirstOrDefault();
            total_amt += line_rec.amt;
            total_cnt++;
          }
          TransactionTypeLevelProcessor processor = new TransactionTypeLevelProcessor(_config_data, total_cnt, total_amt, pair.Key);
          Dictionary<string, BatchUpdateLineRec> dicTTLine = processor.Process();
          batch_update_data.dicTTLayerRecord.Add(pair.Key, dicTTLine);
        }
      }
      catch (Exception ex)
      {
        string error = "Error in BatchUpdateProcessor.ProcessTransactionTypeLevel: " + ex.Message.ToString();
        throw new Exception(error);
      }
    }

    protected void ProcessDetailLevel(List<string> lst_parent_layer)
    {
      Dictionary<List<RecordGroupInfo>, List<BatchDetailRecord>> my_dic = new Dictionary<List<RecordGroupInfo>, List<BatchDetailRecord>>();
      my_dic = GroupBatchDetailRecords(lst_parent_layer);
      batch_update_data.dicDetailLayerRecord = my_dic;
    }

    /// <summary>
    /// Cosolidate within same group, that means all of  batch_detail_rec.dic_groupby_data is same in the list.
    /// </summary>
    /// <param name="lst_detail_rec"></param>
    /// <returns></returns>
    protected List<BatchDetailRecord> Consolidate(List<BatchDetailRecord> lst_detail_rec, bool offset_consolidation = false)
    {
      // new processed consolidate list 
      List<BatchDetailRecord> consolidated_lst_detail_rec = new List<BatchDetailRecord>();

      // key: layer id
      Dictionary<string, List<BatchUpdateLineRec>> dic_of_consolidate_lst = new Dictionary<string, List<BatchUpdateLineRec>>();

      try
      {
        Dictionary<string, List<BatchUpdateLineRec>> dic = lst_detail_rec.Parse();
        foreach (KeyValuePair<string, List<BatchUpdateLineRec>> pair in dic)
        {
          List<BatchUpdateLineRec> lstConsolidate = Consolidate(pair.Value, offset_consolidation);
          dic_of_consolidate_lst.Add(pair.Key, lstConsolidate);
        }

        // make sure lists of each layer have same record count (configuration must have same conditions if there is summary option, confilcated!!)
        if (dic_of_consolidate_lst.Count > 0)
        {
          bool same_cnt = true;
          int cnt = dic_of_consolidate_lst.GetFirstOrDefault().Count;
          foreach (KeyValuePair<string, List<BatchUpdateLineRec>> pair in dic_of_consolidate_lst)
          {
            if (cnt != pair.Value.Count)
              same_cnt = false;
          }

          if (!same_cnt)
            consolidated_lst_detail_rec = lst_detail_rec;   // return original one, configuration not correct
          else
          {
            List<List<BatchUpdateLineRec>> lst_of_consolited_lst = new List<List<BatchUpdateLineRec>>();
            List<string> lstLayer = new List<string>();
            // get list and saved to lst_of_lst
            foreach (KeyValuePair<string, List<BatchUpdateLineRec>> pair in dic_of_consolidate_lst)
            {
              lst_of_consolited_lst.Add(pair.Value);
              lstLayer.Add(pair.Key);
            }

            // convert back to List<BatchDetailRecord> 
            for (int i = 0; i < cnt; i++)
            {
              BatchDetailRecord batch_detail_rec = new BatchDetailRecord();
              for (int k = 0; k < lstLayer.Count; k++)
              {
                batch_detail_rec.dicLineRec.Add(lstLayer[k], lst_of_consolited_lst[k][i]);
                batch_detail_rec.group_key = lst_of_consolited_lst[k][i].group_key; // should have same key for same i
              }
              batch_detail_rec.dic_groupby_data = lst_detail_rec.Count > 0 ? lst_detail_rec[0].dic_groupby_data : new Dictionary<string, GroupbyData<GenericObject>>();
              consolidated_lst_detail_rec.Add(batch_detail_rec);
            }

          }
        }

        return consolidated_lst_detail_rec;
      }
      catch (Exception ex)
      {
        string error = "Error in BatchUpdateProcessor.Consolidate (1): " + ex.Message.ToString();
        throw new Exception(error);
      }
    }

    protected List<BatchUpdateLineRec> Consolidate(List<BatchUpdateLineRec> lstDetailLine, bool offset_summary = false)
    {
      List<BatchUpdateLineRec> lstConsolidated_detail_line = new List<BatchUpdateLineRec>();

      // group by transaction detail line record mode: one of summary,detail, overage, shortage, deposit
      Dictionary<RecordMode, List<BatchUpdateLineRec>> dicByRecordMode = new Dictionary<RecordMode, List<BatchUpdateLineRec>>();

      try
      {
        // group by record mode
        if (offset_summary == false) // not offset
          dicByRecordMode = lstDetailLine.GroupBy(o => o.record_mode).ToDictionary(o => o.Key, o => o.ToList());
        else // offset
          dicByRecordMode.Add(RecordMode.none, lstDetailLine);


        // IPAY-1264 HX: could be more than one groupbys
        List<SUMMARIZATION_TYPE> summary_types = new List<SUMMARIZATION_TYPE>();
        List<SUMMARIZATION_TYPE> offset_summary_types = new List<SUMMARIZATION_TYPE>(); // IPAY-1588

        // get summarization types for summary mode
        if (_config_data.general_info.summary_mode.mode && string.IsNullOrEmpty(_config_data.general_info.summary_mode.summarization_type) == false)
        {
          // first one
          switch (_config_data.general_info.summary_mode.summarization_type.ToUpper())
          {
            case "COREFILE":
              summary_types.Add(SUMMARIZATION_TYPE.core_file);
              break;
            case "GLNBR":
              summary_types.Add(SUMMARIZATION_TYPE.gl_nbr);
              break;
            case "RCPTREFNBR":
              summary_types.Add(SUMMARIZATION_TYPE.receipt_reference_nbr);
              break;
            case "TRANS_TYPE":
              summary_types.Add(SUMMARIZATION_TYPE.trans_type);
              break;
            case "TRANSPOSTDT":
              summary_types.Add(SUMMARIZATION_TYPE.trans_post_date);
              break;
          }

          // additionals
          if (_config_data.general_info.summary_mode.additional_summarization_types != null)
          {
            foreach (var str_sum_type in _config_data.general_info.summary_mode.additional_summarization_types)
            {
              SUMMARIZATION_TYPE sum_type = summary_types.Count > 0 ? summary_types[0] : SUMMARIZATION_TYPE.gl_nbr;
              switch (str_sum_type.ToUpper())
              {
                case "COREFILE":
                  sum_type = SUMMARIZATION_TYPE.core_file;
                  break;
                case "GLNBR":
                  sum_type = SUMMARIZATION_TYPE.gl_nbr;
                  break;
                case "RCPTREFNBR":
                  sum_type = SUMMARIZATION_TYPE.receipt_reference_nbr;
                  break;
                case "TRANS_TYPE":
                  sum_type = SUMMARIZATION_TYPE.trans_type;
                  break;
                case "TRANSPOSTDT":
                  sum_type = SUMMARIZATION_TYPE.trans_post_date;
                  break;
              }

              if (!summary_types.Contains(sum_type))
                summary_types.Add(sum_type);

            }

          }
        }

        // get offset summarization types for offset 
        if (_config_data.general_info.offset_summarization &&
            string.IsNullOrEmpty(_config_data.general_info.offset_summarization_type) == false)
        {
          // first one
          switch (_config_data.general_info.offset_summarization_type.ToUpper())
          {
            case "COREFILE":
              offset_summary_types.Add(SUMMARIZATION_TYPE.core_file);
              break;
            case "GLNBR":
              offset_summary_types.Add(SUMMARIZATION_TYPE.gl_nbr);
              break;
            case "RCPTREFNBR":
              offset_summary_types.Add(SUMMARIZATION_TYPE.receipt_reference_nbr);
              break;
            case "TRANS_TYPE":
              offset_summary_types.Add(SUMMARIZATION_TYPE.trans_type);
              break;
            case "TRANSPOSTDT":
              offset_summary_types.Add(SUMMARIZATION_TYPE.trans_post_date);
              break;
          }

          // additionals
          if (_config_data.general_info.offset_additional_summarization_types != null)
          {
            foreach (var str_sum_type in _config_data.general_info.offset_additional_summarization_types)
            {
              SUMMARIZATION_TYPE sum_type = summary_types.Count > 0 ? summary_types[0] : SUMMARIZATION_TYPE.gl_nbr;
              switch (str_sum_type.ToUpper())
              {
                case "COREFILE":
                  sum_type = SUMMARIZATION_TYPE.core_file;
                  break;
                case "GLNBR":
                  sum_type = SUMMARIZATION_TYPE.gl_nbr;
                  break;
                case "RCPTREFNBR":
                  sum_type = SUMMARIZATION_TYPE.receipt_reference_nbr;
                  break;
                case "TRANS_TYPE":
                  sum_type = SUMMARIZATION_TYPE.trans_type;
                  break;
                case "TRANSPOSTDT":
                  sum_type = SUMMARIZATION_TYPE.trans_post_date;
                  break;
              }

              if (!offset_summary_types.Contains(sum_type))
                offset_summary_types.Add(sum_type);

            }

          }
        }

        if (offset_summary == true)
          summary_types = offset_summary_types;

        foreach (KeyValuePair<RecordMode, List<BatchUpdateLineRec>> my_pair in dicByRecordMode)
        {
          List<SUMMARIZATION_TYPE> my_summary_types = new List<SUMMARIZATION_TYPE>(summary_types); // Clone list into a different object, size of my_summary_types will be removed one by one during processing
          List<BatchUpdateLineRec> my_list = new List<BatchUpdateLineRec>();
          if (my_pair.Key == RecordMode.summary || offset_summary == true)
            my_list = MySummarization(my_pair.Value, my_summary_types);

          // I need group records by offset summarization type
          if (offset_summary == false) // if it is not offset with offset consolidation
          {
            List<SUMMARIZATION_TYPE> my_offset_summary_types = new List<SUMMARIZATION_TYPE>(offset_summary_types); // Clone list into a different object, size of my_summary_types will be removed one by one during processing
            if (my_pair.Key == RecordMode.summary)
              my_list = GroupBySummaryTypes(my_list, my_offset_summary_types);
            else
              my_list = GroupBySummaryTypes(my_pair.Value, my_offset_summary_types); // I need group_key for later use; just grouped not consolidated
          }

          lstConsolidated_detail_line.AddRange(my_list);

        }

        List<SUMMARIZATION_TYPE> my_offset_summary_types_2 = new List<SUMMARIZATION_TYPE>(offset_summary_types); // Clone list into a different object, size of my_summary_types will be removed one by one during processing
        lstConsolidated_detail_line = GroupBySummaryTypes(lstConsolidated_detail_line, my_offset_summary_types_2);
        return lstConsolidated_detail_line;
      }
      catch (Exception ex)
      {
        string error = "Error in BatchUpdateProcessor.Consolidate (2): " + ex.Message.ToString();
        throw new Exception(error);
      }


    }

    /// <summary>
    /// the purpose of this function is assign a group key to each record for later use.
    /// </summary>
    /// <param name="lst"></param>
    /// <param name="lst_sum_type"></param>
    /// <returns></returns>
    protected List<BatchUpdateLineRec> GroupBySummaryTypes(List<BatchUpdateLineRec> lst, List<SUMMARIZATION_TYPE> lst_sum_type)
    {
      List<BatchUpdateLineRec> my_list = new List<BatchUpdateLineRec>();
      // set top dictionary
      Dictionary<string, List<BatchUpdateLineRec>> top_dic = new Dictionary<string, List<BatchUpdateLineRec>>();
      top_dic.Add("all", lst);

      try
      {
        // call recursion function
        // key is combination of all key values for exmple: workgroup id~payfilenbr~transtype
        Dictionary<string, List<BatchUpdateLineRec>> rst_dic = GroupDetailBatchUpdateLnRecodBySummaryTypes(top_dic, lst_sum_type);

        foreach (KeyValuePair<string, List<BatchUpdateLineRec>> entry in rst_dic)
        {
          foreach (BatchUpdateLineRec rec in entry.Value)
          {
            BatchUpdateLineRec bu_rec = rec;
            bu_rec.group_key = entry.Key;

            my_list.Add(bu_rec);
          }

        }
        return my_list;
      }
      catch (Exception ex)
      {
        string error = "Error in BatchUpdateProcessor.GroupBySummaryTypes: " + ex.Message.ToString();
        throw new Exception(error);
      }
    }

    protected List<BatchUpdateLineRec> MySummarization(List<BatchUpdateLineRec> lst, List<SUMMARIZATION_TYPE> lst_sum_type)
    {
      List<BatchUpdateLineRec> lstConsolidated = new List<BatchUpdateLineRec>();

      // set top dictionary
      Dictionary<string, List<BatchUpdateLineRec>> top_dic = new Dictionary<string, List<BatchUpdateLineRec>>();
      top_dic.Add("all", lst);

      try
      {
        // call recursion function
        // key is combination of all key values for exmple: workgroup id~payfilenbr~transtype
        Dictionary<string, List<BatchUpdateLineRec>> rst_dic = GroupDetailBatchUpdateLnRecodBySummaryTypes(top_dic, lst_sum_type);

        // consolidated
        foreach (KeyValuePair<string, List<BatchUpdateLineRec>> entry in rst_dic)
        {
          BatchUpdateLineRec bu_rec = entry.Value[0]; // pick up first record
          bu_rec.group_key = entry.Key;

          // calulate total amount
          double total_amt = 0.00;
          foreach (BatchUpdateLineRec rec in entry.Value)
            total_amt += rec.amt;

          // I need find Trans_Amount rec and set its value as total_amt;
          foreach (RecordItem item in bu_rec.lstRec)
          {
            if (string.Compare(item.tag, "Trans_Amount", true) == 0 ||
                string.Compare(item.tag, "Trans_Absolute_Amount") == 0 ||
                string.Compare(item.tag, "Trans_Amount_sign_reversal", true) == 0)
            {
              double my_total_amt = string.Compare(item.tag, "Trans_Amount_sign_reversal", true) == 0 ? total_amt * -1 : total_amt;
              item.value = my_total_amt.FormatAmount(item.rec_config.format.value).FormatValue(_config_data.general_info.record_type.value, item.rec_config);
            }
          }
          bu_rec.amt = total_amt;

          lstConsolidated.Add(bu_rec);
        }
        return lstConsolidated;
      }
      catch (Exception ex)
      {
        string error = "Error in BatchUpdateProcessor.MySummarization: " + ex.Message.ToString();
        throw new Exception(error);
      }
    }

    /// <summary>
    /// recusion function
    /// Group Detail Batch Update Line Records by Summarization Types
    /// </summary>
    /// <param name="my_dic"></param>
    /// <param name="lst_sum_type"> summarization types</param>
    /// <returns>Dictionay: Key is combination of key values, for example gourp1~payfilenbr~transdate</returns>
    protected Dictionary<string, List<BatchUpdateLineRec>> GroupDetailBatchUpdateLnRecodBySummaryTypes(Dictionary<string, List<BatchUpdateLineRec>> my_dic, List<SUMMARIZATION_TYPE> lst_sum_type)
    {
      Dictionary<string, List<BatchUpdateLineRec>> dicTemp = new Dictionary<string, List<BatchUpdateLineRec>>();
      if (lst_sum_type.Count == 0)
        return my_dic;

      try
      {
        // initial key = "all"
        foreach (KeyValuePair<string, List<BatchUpdateLineRec>> pair in my_dic)
        {
          string key = pair.Key;
          Dictionary<string, List<BatchUpdateLineRec>> result = GetDetailBatchUpdateLnRecodGroupBySummaryType(pair.Value, lst_sum_type[0]);
          foreach (KeyValuePair<string, List<BatchUpdateLineRec>> result_pair in result)
          {
            if (string.Compare(pair.Key, "all", true) != 0)
            {
              key += "~"; // use '~' to separate keys
              key += result_pair.Key;
            }
            else
              key = result_pair.Key;

            dicTemp.Add(key, result_pair.Value);

            // reset key to upper level's key
            key = pair.Key;
          }
        }

        lst_sum_type.RemoveAt(0);
        return GroupDetailBatchUpdateLnRecodBySummaryTypes(dicTemp, lst_sum_type);
      }
      catch (Exception ex)
      {
        string error = "Error in BatchUpdateProcessor.GroupDetailBatchUpdateLnRecodBySummaryTypes: " + ex.Message.ToString();
        throw new Exception(error);
      }

    }

    protected Dictionary<string, List<BatchUpdateLineRec>> GetDetailBatchUpdateLnRecodGroupBySummaryType(List<BatchUpdateLineRec> lst, SUMMARIZATION_TYPE sum_type)
    {
      Dictionary<string, List<BatchUpdateLineRec>> dic = new Dictionary<string, List<BatchUpdateLineRec>>();
      switch (sum_type)
      {
        case SUMMARIZATION_TYPE.core_file:
          dic = lst.GroupBy(o => o.pay_file_name).ToDictionary(o => o.Key, o => o.ToList());
          break;
        case SUMMARIZATION_TYPE.gl_nbr:
          dic = lst.GroupBy(o => o.gl_nbr).ToDictionary(o => o.Key, o => o.ToList());
          break;
        case SUMMARIZATION_TYPE.receipt_reference_nbr:
          dic = lst.GroupBy(o => o.rcpt_ref_nbr).ToDictionary(o => o.Key, o => o.ToList());
          break;
        case SUMMARIZATION_TYPE.trans_type:
          dic = lst.GroupBy(o => o.tt_id).ToDictionary(o => o.Key, o => o.ToList());
          break;
        case SUMMARIZATION_TYPE.trans_post_date:
          dic = lst.GroupBy(o => o.trans_post_date.ToString()).ToDictionary(o => o.Key, o => o.ToList());
          break;
      }

      return dic;
    }

    protected string FormatLine(BatchUpdateLineRec batch_line_info)
    {
      StringBuilder sb = new StringBuilder();
      string rec_str = "";
      foreach (RecordItem rec in batch_line_info.lstRec)
      {
        rec_str = rec.value;
        if (string.Compare(_config_data.general_info.record_type.value, "Variable_Length", true) == 0 && _config_data.general_info.delimiter.Length > 0)
        {
          rec_str += _config_data.general_info.delimiter;
        }
        sb.Append(rec_str);
      }

      string rec_line = sb.ToString();
      if (string.Compare(_config_data.general_info.record_type.value, "Variable_Length", true) == 0 && _config_data.general_info.delimiter.Length > 0)
        rec_line = rec_line.TrimEnd(_config_data.general_info.delimiter[0]);

      return rec_line;
    }

  }
}
