﻿using CASL_engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CustomizedBatchUpdate
{

  public enum RecordMode { none = 0, detail, summary, Shortage, Overage, Bank_Deposit };
  public enum SUMMARIZATION_TYPE { core_file, gl_nbr, receipt_reference_nbr, trans_type, trans_post_date } // name must be same as defined in CBU
  public enum LayerType { Batch_Level_Layer, CoreFile_Level_Layer, WG_Level_Layer, GL_Level_Layer, TT_Level_Layer, CF_Level_Layer, Detail_Level_Layer } // must be same as the define in generalinfolayertype.json

  public class RecordItem
  {
    public int seq_order { get; set; }
    public string tag { get; set; }                 // configured source value
    public string value { get; set; }
    public BatchRecord rec_config { get; set; }
  }
  public class BatchUpdateLineRec
  {
    public List<RecordItem> lstRec { get; set; }    // list of record item
    public Layer layer { get; set; }
    public RecordMode record_mode { get; set; }     // current not used

    public string pay_file_name { get; set; }
    public string dept_id { get; set; }
    public string rcpt_ref_nbr { get; set; }
    public string tt_id { get; set; }
    public string cust_field_value { get; set; }
    //public string trans_desc { get; set; }
    public string gl_nbr { get; set; }
    public double amt { get; set; }
    public DateTime trans_post_date { get; set; } // IPAY-1264

    public string group_key { get; set; } = ""; // used in consolidation if applied it will contains values such as workgroup id~payfilenbr~transtype
    public Dictionary<string, GroupbyData<GenericObject>> dic_groupby_data { get; set; } // only used for detail
  }

  /*public class BatchUpdateRecordData
  {
   
  }*/

  public class BatchUpdateData
  {
    public Dictionary<List<RecordGroupInfo>, Dictionary<String, BatchUpdateLineRec>> dicBatchLayerRecord { get; set; } //  Dictionary<String, BatchUpdateLineRec>; string contains configured layer id
    public Dictionary<List<RecordGroupInfo>, Dictionary<String, BatchUpdateLineRec>> dicWGLayerRecord { get; set; }
    public Dictionary<List<RecordGroupInfo>, Dictionary<String, BatchUpdateLineRec>> dicCoreFileLayerRecord { get; set; }
    public Dictionary<List<RecordGroupInfo>, Dictionary<String, BatchUpdateLineRec>> dicGLLayerRecord { get; set; }
    public Dictionary<List<RecordGroupInfo>, Dictionary<String, BatchUpdateLineRec>> dicTTLayerRecord { get; set; }
    public Dictionary<List<RecordGroupInfo>, Dictionary<String, BatchUpdateLineRec>> dicCFLayerRecord { get; set; }
    public Dictionary<List<RecordGroupInfo>, List<BatchDetailRecord>> dicDetailLayerRecord { get; set; }

    public Dictionary<int, List<BatchUpdateLineRec>> dicList { get; set; }
    public BatchUpdateData()
    {
      dicBatchLayerRecord = new Dictionary<List<RecordGroupInfo>, Dictionary<string, BatchUpdateLineRec>>();
      dicWGLayerRecord = new Dictionary<List<RecordGroupInfo>, Dictionary<string, BatchUpdateLineRec>>();
      dicCoreFileLayerRecord = new Dictionary<List<RecordGroupInfo>, Dictionary<string, BatchUpdateLineRec>>();
      dicGLLayerRecord = new Dictionary<List<RecordGroupInfo>, Dictionary<string, BatchUpdateLineRec>>();
      dicTTLayerRecord = new Dictionary<List<RecordGroupInfo>, Dictionary<string, BatchUpdateLineRec>>();
      dicCFLayerRecord = new Dictionary<List<RecordGroupInfo>, Dictionary<string, BatchUpdateLineRec>>();
      dicDetailLayerRecord = new Dictionary<List<RecordGroupInfo>, List<BatchDetailRecord>>();
    }
  }


  public class BatchDetailRecord
  {
    public Dictionary<string, BatchUpdateLineRec> dicLineRec { get; set; } = new Dictionary<string, BatchUpdateLineRec>(); // key is layer key
    public Dictionary<string, GroupbyData<GenericObject>> dic_groupby_data { get; set; } = new Dictionary<string, GroupbyData<GenericObject>>(); // only used for detail
    public string group_key { get; set; } = ""; // used in consolidation if applied it will contains values such as workgroup id~payfilenbr~transtype
  }

  public class BatchUpdateLineRecWithLayerInfo
  {
    public BatchUpdateLineRec line_rec { get; set; } = new BatchUpdateLineRec();
    public Layer layer_info { get; set; }
  }

  public class RecordGroupInfo
  {
    public string layer_name { get; set; }
    public string str_value { get; set; }
    public GenericObject geo_value { get; set; }
  }

  public class GroupbyData<T>
  {
    public string layer_name { get; set; }
    public string tag { get; set; }     // gl segment name, custom field tag name
    public string str_value { get; set; }
    public T geo_value { get; set; }
  }

  public class BatchUpdateDataType<T>
  {
    public List<T> lstRec { get; set; }
  }

  public class CoreFile_BatchUpdateData // may not used later
  {
    public List<BatchUpdateLineRec> lstPayFileHeader { get; set; }
    public List<BatchUpdateLineRec> lstDetail { get; set; }
    public List<BatchUpdateLineRec> lstPayFileTrailer { get; set; }
  }
  public class Batch_BatchUpdateData // may not used later
  {
    public List<BatchUpdateLineRec> lstBatchFileHeader { get; set; }
    public List<CoreFile_BatchUpdateData> lstCoreFileBUData { get; set; }
    public List<BatchUpdateLineRec> lstBatchFileTrailer { get; set; }
  }

  public static class BUExtensionClass
  {
    public static string FormatValue(this string str, string record_type, BatchRecord rec)
    {
      string rec_str = "";
      if (string.Compare(record_type, "Fixed_Length", true) == 0)
      {
        bool left_justify = string.Compare(rec.jusitfy, "left", true) == 0 ? true : false;
        bool space_fill = string.Compare(rec.fill, "space", true) == 0 ? true : false;
        rec_str = str.FormatFixedLengthValue(rec.max_length, left_justify, space_fill);
      }
      else
      {
        rec_str = str.FormatVariableLengthValue(rec.max_length);
      }

      return rec_str;
    }
    public static string FormatFixedLengthValue(this string str, int len, bool left_justify = true, bool space_fill = true)
    {
      string rec_str = "";
      if (space_fill)
      {
        if (left_justify)
        {
          rec_str = str.PadRight(len);
        }
        else
        {
          rec_str = str.PadLeft(len);
        }

        rec_str = rec_str.Substring(0, len);
      }
      else // zero fill
      {
        string fmt = "";
        for (int i = 0; i < len; i++)
          fmt += "0";
        fmt += ".##";

        if (string.IsNullOrEmpty(str))
          str = "0";

        decimal decValue = 0.00m;
        bool rst = decimal.TryParse(str, out decValue);
        if (rst)
        {
          rec_str = decValue.ToString(fmt);
        }
      }
      return rec_str;
    }
    public static string FormatVariableLengthValue(this string str, int max_len)
    {
      string rec_str = "";
      if (str.Length > max_len)
        rec_str = str.Substring(0, max_len);
      else
        rec_str = str;
      return rec_str;
    }
    public static string FormatAmount(this double amount, string format)
    {
      string rec_str = "";
      if (string.Compare(format, "AmtFmt_1", true) == 0) // "-10000 & 10000"
      {
        rec_str = string.Format("{0}", (int)(amount * 100.00));
      }
      else if (string.Compare(format, "AmtFmt_2", true) == 0) // "-10000 & +0000"
      {
        if (amount >= 0.00)
          rec_str = string.Format("+{0}", (int)(amount * 100.00));
        else
          rec_str = string.Format("-{0}", (int)(amount * -100.00));
      }
      else if (string.Compare(format, "AmtFmt_3", true) == 0) // "10000- & 10000"
      {
        if (amount >= 0.00)
          rec_str = string.Format("{0}", (int)(amount * 100.00));
        else
          rec_str = string.Format("{0}-", (int)(amount * -100.00));
      }
      else if (string.Compare(format, "AmtFmt_4", true) == 0) //  "10000- & 10000+"
      {
        if (amount >= 0.00)
          rec_str = string.Format("{0}+", (int)(amount * 100.00));
        else
          rec_str = string.Format("{0}-", (int)(amount * -100.00));
      }
      else if (string.Compare(format, "AmtFmt_5", true) == 0) // "-100.00 & 100.00"
      {
        if (amount >= 0.00)
          rec_str = string.Format("{0:0.00}", amount);
        else
          rec_str = string.Format("-{0:0.00}", amount * -1);
      }
      else if (string.Compare(format, "AmtFmt_6", true) == 0) // "-100.00 & +100.00"
      {
        if (amount >= 0.00)
          rec_str = string.Format("+{0:0.00}", amount);
        else
          rec_str = string.Format("-{0:0.00}", amount * -1);
      }
      else if (string.Compare(format, "AmtFmt_7", true) == 0) // "100.00- & 100.00"
      {
        if (amount >= 0.00)
          rec_str = string.Format("{0:0.00}", amount);
        else
          rec_str = string.Format("{0:0.00}-", amount * -1);
      }
      else if (string.Compare(format, "AmtFmt_8", true) == 0) // "100.00- & 100.00+"
      {
        if (amount >= 0.00)
          rec_str = string.Format("{0:0.00}+", amount);
        else
          rec_str = string.Format("{0:0.00}-", amount * -1);
      }
      else if (string.Compare(format, "AmtFmt_9", true) == 0) //"10000 & 10000 (absolute value)"
      {
        if (amount >= 0.00)
          rec_str = string.Format("{0}", (int)(amount * 100.00));
        else
          rec_str = string.Format("{0}", (int)(amount * -100.00));
      }
      else if (string.Compare(format, "AmtFmt_10", true) == 0) // "100.00 & 100.00 (absolute value)"
      {
        if (amount >= 0.00)
          rec_str = string.Format("{0:0.00}", amount);
        else
          rec_str = string.Format("{0:0.00}", amount * -1);
      }

      return rec_str;

    }
    public static string FormatDate(this DateTime date, string format)
    {
      string rec_str = "";
      if (string.Compare(format, "DateFmt_1", true) == 0) // MMDD
      {
        rec_str = string.Format("{0:D2}{1:D2}", date.Month, date.Day);
      }
      else if (string.Compare(format, "DateFmt_2", true) == 0) // MMDDYY
      {
        rec_str = string.Format("{0:D2}{1:D2}{2:D2}", date.Month, date.Day, date.Year % 100);
      }
      else if (string.Compare(format, "DateFmt_3", true) == 0) // MMDDYYYY
      {
        rec_str = string.Format("{0:D2}{1:D2}{2:D4}", date.Month, date.Day, date.Year);
      }
      else if (string.Compare(format, "DateFmt_4", true) == 0) // MM/DD
      {
        rec_str = string.Format("{0:D2}/{1:D2}", date.Month, date.Day);
      }
      else if (string.Compare(format, "DateFmt_5", true) == 0) // MM/DD/YY
      {
        rec_str = string.Format("{0:D2}/{1:D2}/{2:D2}", date.Month, date.Day, date.Year % 100);
      }
      else if (string.Compare(format, "DateFmt_6", true) == 0) // MM/DD/YYYY
      {
        rec_str = string.Format("{0:D2}/{1:D2}/{2:D4}", date.Month, date.Day, date.Year);
      }
      else if (string.Compare(format, "DateFmt_7", true) == 0) // MM-DD
      {
        rec_str = string.Format("{0:D2}-{1:D2}", date.Month, date.Day);
      }
      else if (string.Compare(format, "DateFmt_8", true) == 0) // MM-DD-YY
      {
        rec_str = string.Format("{0:D2}-{1:D2}-{2:D2}", date.Month, date.Day, date.Year % 100);
      }
      else if (string.Compare(format, "DateFmt_9", true) == 0) // MM-DD-YYYY
      {
        rec_str = string.Format("{0:D2}-{1:D2}-{2:D4}", date.Month, date.Day, date.Year);
      }
      else if (string.Compare(format, "DateFmt_10", true) == 0) // YYYYMMDD
      {
        rec_str = string.Format("{0:D4}{1:D2}{2:D2}", date.Year, date.Month, date.Day);
      }
      else if (string.Compare(format, "DateFmt_11", true) == 0) // YYMMDD
      {
        rec_str = string.Format("{0:D2}{1:D2}{2:D2}}", date.Year % 100, date.Month, date.Day);
      }
      else if (string.Compare(format, "DateFmt_12", true) == 0) // YYYY/MM/DD
      {
        rec_str = string.Format("{0:D4}/{1:D2}/{2:D2}", date.Year, date.Month, date.Day);
      }
      else if (string.Compare(format, "DateFmt_13", true) == 0) // YY/MM/DD
      {
        rec_str = string.Format("{0:D2}/{1:D2}/{2:D2}}", date.Year % 100, date.Month, date.Day);
      }
      else if (string.Compare(format, "DateFmt_14", true) == 0) // YYYY-MM-DD
      {
        rec_str = string.Format("{0:D4}-{1:D2}-{2:D2}", date.Year, date.Month, date.Day);
      }
      else if (string.Compare(format, "DateFmt_15", true) == 0) // YY-MM-DD
      {
        rec_str = string.Format("{0:D2}-{1:D2}-{2:D2}}", date.Year % 100, date.Month, date.Day);
      }

      return rec_str;
    }
    public static string FormatTime(this DateTime date, string format)
    {
      string rec_str = "";
      if (string.Compare(format, "TimeFmt_1", true) == 0) // HHMMSS
      {
        rec_str = string.Format("{0:D2}{1:D2}{2:D2}", date.Hour, date.Minute, date.Second);
      }
      else if (string.Compare(format, "TimeFmt_2", true) == 0) //HH:MM:SS
      {
        rec_str = string.Format("{0:D2}:{1:D2}:{2:D2}", date.Hour, date.Minute, date.Second);
      }
      else if (string.Compare(format, "TimeFmt_3", true) == 0) // HHMM
      {
        rec_str = string.Format("{0:D2}{1:D2}", date.Hour, date.Minute);
      }
      else if (string.Compare(format, "TimeFmt_4", true) == 0) // HH:MM
      {
        rec_str = string.Format("{0:D2}:{1:D2}", date.Hour, date.Minute);
      }

      return rec_str;
    }
    public static string FormatDebitCreditIndicator(this double amount, string format)
    {
      string rec_str = "";
      bool is_amount_less_zero = amount >= 0.00 ? false : true;
      if (string.Compare(format, "DcInd_1", true) == 0) // "D' or "C'
      {
        rec_str = is_amount_less_zero ? "D" : "C";
      }
      else if (string.Compare(format, "DCInd_2", true) == 0) // "DC" or "CR"
      {
        rec_str = is_amount_less_zero ? "DC" : "CR";
      }
      else if (string.Compare(format, "DCInd_3", true) == 0) // "DB" or "CR"
      {
        rec_str = is_amount_less_zero ? "DB" : "CR";
      }
      return rec_str;
    }
    public static string GetCoreFileName(this GenericObject geoFile, string format)
    {
      string rec_str = "";
      if (geoFile != null)
      {
        string dep_file_nbr = geoFile.get("DEPFILENBR", "").ToString();
        int dep_file_seq = Convert.ToInt32(geoFile.get("DEPFILESEQ", "").ToString());

        if (string.IsNullOrEmpty(format)) // general use
        {
          rec_str = string.Format("{0}{1:D3}", dep_file_nbr, dep_file_seq);
        }
        else if (string.Compare(format, "DepFileFmt_1", true) == 0) // YYYYJJJSSS
        {
          rec_str = string.Format("{0}{1:D3}", dep_file_nbr, dep_file_seq);
        }
        else if (string.Compare(format, "DepFileFmt_2", true) == 0) // YYYYJJJ-SSS
        {
          rec_str = string.Format("{0}-{1:D3}", dep_file_nbr, dep_file_seq);
        }
      }
      return rec_str;
    }
    public static string GetReceiptRefNbr(this GenericObject geoTran, string format)
    {
      string rec_str = "";
      if (geoTran != null)
      {
        string dep_file_nbr = geoTran.get("DEPFILENBR", "").ToString();
        int dep_file_seq = Convert.ToInt32(geoTran.get("DEPFILESEQ", "").ToString());
        int event_nbr = Convert.ToInt32(geoTran.get("EVENTNBR", "").ToString());

        if (string.IsNullOrEmpty(format))
        {
          rec_str = string.Format("{0}{1:D3}-{2}", dep_file_nbr, dep_file_seq, event_nbr);
        }
        else
        {
          if (string.Compare(format, "RcptRefNbrFmt_1", true) == 0) //  "YYYYJJJSSS-R"
          {
            rec_str = string.Format("{0}{1:D3}-{2}", dep_file_nbr, dep_file_seq, event_nbr);
          }
          else if (string.Compare(format, "RcptRefNbrFmt_2", true) == 0) //  "YYYYJJJSSS-RRR"
          {
            rec_str = string.Format("{0}{1:D3}-{2:D3}", dep_file_nbr, dep_file_seq, event_nbr);
          }
          else if (string.Compare(format, "RcptRefNbrFmt_3", true) == 0) //  "YYJJJSSS-R"
          {
            rec_str = string.Format("{0}{1:D3}-{2}", dep_file_nbr.Substring(2, 5), dep_file_seq, event_nbr);
          }
          else if (string.Compare(format, "RcptRefNbrFmt_4", true) == 0) //  "YYJJJSSS-RRR"
          {
            rec_str = string.Format("{0}{1:D3}-{2:D3}", dep_file_nbr.Substring(2, 5), dep_file_seq, event_nbr);
          }
          else if (string.Compare(format, "RcptRefNbrFmt_5", true) == 0) //  "YYYYJJJSSS-RRRR"
          {
            rec_str = string.Format("{0}{1:D3}-{2:D4}", dep_file_nbr, dep_file_seq, event_nbr);
          }
          else if (string.Compare(format, "RcptRefNbrFmt_6", true) == 0) //  "YYJJJSSS-RRRR"
          {
            rec_str = string.Format("{0}{1:D3}-{2:D4}", dep_file_nbr.Substring(2, 5), dep_file_seq, event_nbr);
          }
          else if (string.Compare(format, "RcptRefNbrFmt_7", true) == 0) //  "JJJSSSRRR"
          {
            rec_str = string.Format("{0}{1:D3}{2:D3}", dep_file_nbr.Substring(4, 3), dep_file_seq, event_nbr);
          }
          else if (string.Compare(format, "RcptRefNbrFmt_8", true) == 0) //  "JJJSSSRRRR"
          {
            rec_str = string.Format("{0}{1:D3}{2:D4}", dep_file_nbr.Substring(4, 3), dep_file_seq, event_nbr);
          }
        }
      }


      return rec_str;
    }
    public static string GetTransactionRefNbr(this GenericObject geoTran, string format)
    {
      string rec_str = "";
      if (geoTran != null)
      {
        string dep_file_nbr = geoTran.get("DEPFILENBR", "").ToString();
        int dep_file_seq = Convert.ToInt32(geoTran.get("DEPFILESEQ", "").ToString());
        int event_nbr = Convert.ToInt32(geoTran.get("EVENTNBR", "").ToString());
        int tran_nbr = Convert.ToInt32(geoTran.get("TRANNBR", "").ToString());

        if (string.Compare(format, "TransRefNbrFmt_1", true) == 0) //  "YYYYJJJSSS-R-T"
        {
          rec_str = string.Format("{0}{1:D3}-{2}-{3}", dep_file_nbr, dep_file_seq, event_nbr, tran_nbr);
        }
        else if (string.Compare(format, "TransRefNbrFmt_2", true) == 0) //  "YYYYJJJSSS-RRR-TTT"
        {
          rec_str = string.Format("{0}{1:D3}-{2:D3}-{3:D3}", dep_file_nbr, dep_file_seq, event_nbr, tran_nbr);
        }
        else if (string.Compare(format, "TransRefNbrFmt_3", true) == 0) //  "YYJJJSSS-R-T"
        {
          rec_str = string.Format("{0}{1:D3}-{2}-{3}", dep_file_nbr.Substring(2, 5), dep_file_seq, event_nbr, tran_nbr);
        }
        else if (string.Compare(format, "TransRefNbrFmt_4", true) == 0) //  "YYJJJSSS-RRR-TTT"
        {
          rec_str = string.Format("{0}{1:D3}-{2:D3}-{3:D3}", dep_file_nbr.Substring(2, 5), dep_file_seq, event_nbr, tran_nbr);
        }
      }
      return rec_str;
    }
    public static string GetFileOpenUserName(this GenericObject geoFile)
    {
      string rec_str = "";
      if (geoFile != null)
        rec_str = geoFile.get("OPEN_USERID", "").ToString();
      return rec_str;
    }
    public static string GetWorkgroupId(this GenericObject geoFile)
    {
      string rec_str = "";
      if (geoFile != null)
        rec_str = geoFile.get("DEPTID", "").ToString();
      return rec_str;
    }
    public static string GetWorkgroupDescription(this GenericObject geoFile)
    {
      string rec_str = "";
      if (geoFile != null)
      {
        string dept_id = geoFile.get("DEPTID", "").ToString();

        string tempWG = string.Format("Business.Department.of.\"{0}\"", dept_id);
        GenericObject geoWorkGroup = (GenericObject)c_CASL.c_object(tempWG);
        rec_str = geoWorkGroup.get("name", "").ToString();
      }
      return rec_str;
    }
    public static string GetWorkgroupDescription(this string dept_id)
    {
      string rec_str = "";
      string tempWG = string.Format("Business.Department.of.\"{0}\"", dept_id);
      GenericObject geoWorkGroup = (GenericObject)c_CASL.c_object(tempWG);
      rec_str = geoWorkGroup.get("name", "").ToString();
      return rec_str;
    }
    public static DateTime GetBankDepositPostDate(this GenericObject geoBankDeposit)
    {
      DateTime dt = new DateTime();
      string date_str = geoBankDeposit.get("POSTDT", "").ToString();
      if (!string.IsNullOrEmpty(date_str))
        dt = DateTime.Parse(date_str).Date;
      else
        dt = DateTime.Now.Date;

      return dt;
    }
    public static DateTime GetTransPostDate(this GenericObject geoTran)
    {
      DateTime dt = new DateTime();
      string date_str = geoTran.get("POSTDT", "").ToString();
      if (!string.IsNullOrEmpty(date_str))
        dt = DateTime.Parse(date_str).Date;
      else
        dt = DateTime.Now.Date;

      return dt;
    }
    public static string GetTTID(this GenericObject geoTran)
    {
      string rec_str = "";
      if (geoTran != null)
        rec_str = geoTran.get("TTID", "").ToString();
      return rec_str;
    }
    public static string GetTTDesc(this GenericObject geoTran)
    {
      string rec_str = "";
      if (geoTran != null)
        rec_str = geoTran.get("TTDESC", "").ToString();
      return rec_str;
    }
    public static string GetTTDesc(this string tt_id)
    {
      string rec_str = "";
      string tempTT = string.Format("Business.Transaction.of.\"{0}\"", tt_id);
      GenericObject geoTT = (GenericObject)c_CASL.c_object(tempTT);
      rec_str = geoTT.get("description", "").ToString();
      return rec_str;
    }
    public static string GetDepositReferenceNbr(this GenericObject geoDeposit)
    {
      string rec_str = "";
      // HX: LATER
      return rec_str;
    }
    // IPAY-1261 HX
    public static string GetPaymentMethod(this List<GenericObject> geoTndrApplied, string format)
    {
      string rec_str = "";
      bool bMultipleTender = false;
      string pre_tta_tender_id = string.Empty;
      bool use_standard_field = true;
      string single_field_name = string.Empty;
      string multiple_tender_value = string.Empty;

      if (geoTndrApplied == null)
        return rec_str;

      // need modifiy later
      if (string.Compare(format, "PymtMethod_1", true) == 0)// "Single Tender: standard tender id; Multiple Tenders: \"MUL\"",
      {
        multiple_tender_value = "MUL";
        use_standard_field = true;
        single_field_name = "TNDRID";
      }
      else if (string.Compare(format, "PymtMethod_2", true) == 0)// "Single Tender: standard tender desc; Multiple Tenders: \"MUL\"",
      {
        multiple_tender_value = "MUL";
        use_standard_field = true;
        single_field_name = "TNDRDESC";
      }
      else if (string.Compare(format, "PymtMethod_3", true) == 0)// "Single Tender: batch update external tender type; Multiple Tenders: \"MUL\"",
      {
        multiple_tender_value = "MUL";
        use_standard_field = false;
        single_field_name = "BUExternalTenderType";
      }


      foreach (GenericObject geoTendr in geoTndrApplied)
      {
        string tta_tender_Id = geoTendr.get("TNDRID", "").ToString();
        if (!string.IsNullOrEmpty(pre_tta_tender_id) && string.Compare(pre_tta_tender_id, tta_tender_Id, true) != 0)
          bMultipleTender = true;

        pre_tta_tender_id = tta_tender_Id;
      }

      if (bMultipleTender)
        rec_str = multiple_tender_value;
      else
      {
        GenericObject geoTndr = geoTndrApplied[0];

        if (use_standard_field)
        {
          rec_str = geoTndr.get(single_field_name, "").ToString();
        }
        else
        {
          GenericObject geoCustData = geoTndr.get("CUST_FIELD_DATA", new GenericObject()) as GenericObject;
          int iCountofCF = geoCustData.getIntKeyLength();
          if (geoCustData != null)
          {
            for (int i = 0; i < iCountofCF; i++)
            {
              GenericObject cust_field = geoCustData.get(i) as GenericObject;
              string custTag = ((string)cust_field.get("CUSTTAG", "")).ToLower();
              string custValue = cust_field.get("CUSTVALUE", "") as string;

              if (string.Compare(custTag, single_field_name, true) == 0)
              {
                rec_str = custValue;
                break;
              }

            }
          }
        }
      }

      return rec_str;
    }
    public static string GetCustomFieldValue(this GenericObject geoTran, string custom_field_tag_name)
    {
      string rec_str = "";
      if (geoTran != null)
      {
        GenericObject geoCustData = geoTran.get("GR_CUST_FIELD_DATA", new GenericObject()) as GenericObject;
        if (geoCustData != null)
        {
          for (int i = 0; i < geoCustData.getLength(); i++)
          {
            GenericObject cust_field = geoCustData.get(i) as GenericObject;
            string custTag = ((string)cust_field.get("CUSTTAG", "")).ToLower();
            string custValue = cust_field.get("CUSTVALUE", "") as string;

            if (string.Compare(custTag, custom_field_tag_name, true) == 0)
            {
              rec_str = custValue;
              break;
            }

          }
        }
      }
      return rec_str;
    }
    public static string GetGLSegmentValue(this List<string> gl_segs, string seg_info)
    {
      string rec_str = "";
      if (gl_segs != null)
      {
        string[] parts = seg_info.Split('|'); // source.value format: string.Format("{0}|{1}", rec.order, rec.name);
        int seg_nbr = Convert.ToInt32(parts[0]);
        if (seg_nbr >= 0 && seg_nbr < gl_segs.Count)
          rec_str = gl_segs[seg_nbr];
      }

      return rec_str;
    }

    public static string GetGLValue(this Dictionary<string, string> dic_gl_info, string gl_key)
    {
      string rec_str = "";
      if (dic_gl_info.ContainsKey(gl_key))
        rec_str = dic_gl_info[gl_key];
      return rec_str;
    }

    public static List<BatchUpdateLineRec> GetFirstOrDefault(this Dictionary<string, List<BatchUpdateLineRec>> dic)
    {
      List<BatchUpdateLineRec> lstDetailLineRec = new List<BatchUpdateLineRec>();
      foreach (KeyValuePair<string, List<BatchUpdateLineRec>> pair in dic) // could have more than than one layer for detail
      {
        // I only get first
        lstDetailLineRec = pair.Value;
        break;
      }

      return lstDetailLineRec;
    }

    public static BatchUpdateLineRec GetFirstOrDefault(this Dictionary<string, BatchUpdateLineRec> dic)
    {
      BatchUpdateLineRec line_rec = new BatchUpdateLineRec();
      foreach (KeyValuePair<string, BatchUpdateLineRec> pair in dic)
      {
        line_rec = pair.Value;
        break;
      }

      return line_rec;
    }

    public static string GetItemValue(this List<string> lst, int idx)
    {
      if (lst.Count <= idx)
        return string.Empty;
      else
        return lst[idx];
    }

    public static string GetGLLayerConfiguredGLSegmentName(this List<Layer> lst_layers)
    {
      string gl_segment_name = "";

      // by logic, should only one gl_segment_name configured
      foreach (Layer layer in lst_layers)
      {
        if (layer.name == nameof(LayerType.GL_Level_Layer))
        {
          if (!string.IsNullOrEmpty(layer.gl.value))
            gl_segment_name = layer.gl.value;
        }
      }

      return gl_segment_name;
    }

    public static string GetCustomFieldLayerConfiguredFieldTagName(this List<Layer> lst_layers)
    {
      string custom_field_tag = "";

      // by logic, should only one custom field tag configured
      foreach (Layer layer in lst_layers)
      {
        if (layer.name == nameof(LayerType.CF_Level_Layer))
        {
          if (!string.IsNullOrEmpty(layer.custom_field.value))
            custom_field_tag = layer.custom_field.value;
        }
      }

      return custom_field_tag;
    }
    public static Dictionary<string, List<BatchUpdateLineRec>> Parse(this List<BatchDetailRecord> lst)
    {
      Dictionary<string, List<BatchUpdateLineRec>> dic = new Dictionary<string, List<BatchUpdateLineRec>>();
      foreach (BatchDetailRecord rec in lst)
      {
        foreach (KeyValuePair<string, BatchUpdateLineRec> pair in rec.dicLineRec)
        {
          if (dic.ContainsKey(pair.Key))
            dic[pair.Key].Add(pair.Value);
          else
          {
            List<BatchUpdateLineRec> lst_line_rec = new List<BatchUpdateLineRec>();
            lst_line_rec.Add(pair.Value);
            dic.Add(pair.Key, lst_line_rec);
          }
        }
      }

      return dic;
    }

    public static List<BatchUpdateLineRecWithLayerInfo> GetBatchLineRecOrderbyLayer(this BatchDetailRecord batch_detail_rec, List<Layer> lst_layer)
    {
      // I need order by layer's sequence order
      List<BatchUpdateLineRecWithLayerInfo> lst_line_rec_with_layer_info = new List<BatchUpdateLineRecWithLayerInfo>();
      lst_line_rec_with_layer_info = GetOrderedBatchLineRec(batch_detail_rec.dicLineRec, lst_layer);

      return lst_line_rec_with_layer_info;
    }

    public static List<BatchUpdateLineRecWithLayerInfo> GetOrderedBatchLineRec(this Dictionary<String, BatchUpdateLineRec> dic, List<Layer> lst_layer)
    {
      List<BatchUpdateLineRecWithLayerInfo> lst_line_rec_with_layer_info = new List<BatchUpdateLineRecWithLayerInfo>();
      foreach (KeyValuePair<string, BatchUpdateLineRec> pair in dic)
      {
        BatchUpdateLineRecWithLayerInfo line_rec_with_layer_info = new BatchUpdateLineRecWithLayerInfo();
        line_rec_with_layer_info.line_rec = pair.Value;
        line_rec_with_layer_info.layer_info = lst_layer.GetLayerInfo(pair.Key);

        lst_line_rec_with_layer_info.Add(line_rec_with_layer_info);
      }
      lst_line_rec_with_layer_info = lst_line_rec_with_layer_info.OrderBy(o => o.layer_info.seq_order).ToList();

      return lst_line_rec_with_layer_info;
    }

    public static Layer GetLayerInfo(this List<Layer> lst_layer, string layer_id)
    {
      Layer my_layer = new Layer();
      foreach (Layer layer in lst_layer)
      {
        if (layer.id == layer_id)
        {
          my_layer = layer;
          break;
        }
      }
      return my_layer;
    }
    public static bool HasOutline(this List<Layer> lst_layer, string outline)
    {
      bool found = false;
      foreach(Layer layer in lst_layer)
      {
        if(string.Compare(layer.layer_type.value,outline, true) == 0)
        {
          found = true;
          break;
        }  
      }

      return found;
    }
    public static string GetBankDepositRefNbr(this GenericObject geoDeposit)
    {
      string rec_str = "";

      if (geoDeposit != null)
      {
        rec_str = geoDeposit.get("DEPOSITID", "").ToString();
        rec_str += "-";
        rec_str += geoDeposit.get("DEPOSITNBR", "").ToString();
      }
      return rec_str;
    }

    public static string GetBankDepositBankId(this GenericObject geoDeposit)
    {
      string rec_str = "";

      if (geoDeposit != null)
      {
        rec_str = geoDeposit.get("BANKID", "").ToString();
      }
      return rec_str;
    }

    public static string GetBankDepositBankName(this GenericObject geoDeposit)
    {
      string rec_str = "";

      if (geoDeposit != null)
      {
        string tempBank = string.Format("Business.Bank_account.of.\"{0}\"", geoDeposit.get("BANKID", "").ToString());
        GenericObject geoBank = (GenericObject)((GenericObject)c_CASL.c_object(tempBank));
        if (geoBank != null)
          rec_str = geoBank.get("bank_name", "").ToString();
      }

      return rec_str;
    }
  }

}
