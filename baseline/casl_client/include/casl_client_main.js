// this file casl_client_main.js in casl_client/include
// Called by: (included with) GEO.peripherals
// Expect to be in 'pos' frame that has a sibling frame named 'main'

/* Entry point: <ipayment/> or /ipayment.htm?  Example: http://localhost/pos_demo/ipayment.htm?app=pos&hidden_size=100&use_client=true
 GEO.ipayment 
   returns FRAMESET with main and pos
 pos frame calls <peripherals/>
 
 FOLLOWING NEEDS TO BE UPDATED:
   returns page with 3 IFRAME of pos_frame, casl_client_container and pos_peripherals_container
 GEO.ipayment has SCRIPT that calls try_casl_client_init 
  window.frames.pos.casl_client_init
    ipayment_client_window.casl_client_init( { type:"form", the_form:a_tag } );
      casl_client_init opens URL into window: /casl_client/init.htm? and sets casl_client variable.
        CASL casl_client.htm_init (should be htm_inst) returns HTML page that called JS embed_control
          embed_control writes HTML for object tag with classid and id="casl_client_obj"
            casl_client_init gets called again.
              calls C# init_casl:  baseline/casl_client_external/casl_client/casl_client.cs: public void init_casl (mshtml.HTMLWindow2Class browser_window_1, string code_base)
                calls c_CASL.init(code_base); // "C:/T4/pos_demo/"
                  and session_start and use_casl_client
                        
  Call to execute code: 
    A.href calls: casl_link_aux, calls casl_form, calls casl_client_process_request_and_display
    FORM.action calls: casl_form_aux, calls casl_link, calls casl_client_process_request_and_display
    
  How remote calls are made:
    CASL method XYZ calls call_remote or call_local_or_remote is called, and it calls cs.CASL_engine.misc.CASL_ipayment_remote_call
       which calls ipayment_remote_call which over the network calls ipayment_handle_remote_call in cs.CASL_engine.misc which calls CASL XYZ method.
    Reference.cs is auto-generated.
*/


function get_current_site () {  // http://localhost/pos_demo/foo/bar => http://localhost/pos_demo
// TODO: simplify using location.pathname;
 var full_href=window.location.href;
 var start_protocol=full_href.indexOf("//");
 var start_site=full_href.indexOf("/",start_protocol+2);
 var start_path=full_href.indexOf("/",start_site+1);
 if (start_path==-1)
   return full_href;
 else
   return full_href.substring(0,start_path);//+1);
}

// Utility
String.prototype.endsWith = function(suffix) {
 var startPos = this.length - suffix.length;
 if (startPos < 0) {
   return false;
 }
 return (this.lastIndexOf(suffix, startPos) == startPos);
};
// end utility

/*
GEO.<defmethod _name='foo'> x=req
 <H1> "foo called with x: " x </H1>
</defmethod>

<BODY>
 <A onclick='casl_call("/pos_demo/GEO.htm?x=10")' >Test CASL call</A>
</BODY>

Clicking the link returns this page:
<H1>foo called with x: 10</H1>

MAY NOT NEED OBJECT IF INDEPENDENTLY INSTALLED AS APP.
<OBJECT ID="casl_client"
    CLASSID="CLSID:A1B8A30B-8AAA-4a3e-8869-1DA509E8A011"
    width="1px" height="1px"
    codebase="/casl_client_plugin.cab#Version=10,0,0,533">
<PARAM NAME="EnableDrillDown" VALUE=1>
</OBJECT>
*/

/* called by: casl_form_aux, casl_link_aux.  called from JS handler on page */
function casl_client_process_request_and_display (url, target) {
 //lert("in ccprad0. uri: " + url + " target: " + target);
 var result_page=ipayment_client_obj.process_request_and_return_html(url);  // This call blocks all threads including show_wait_dialog, even if it is put within its own thread. (MP)
 //lert("result_page: " + result_page);
 if (result_page=="") {  // Like status code 204 response where page should not be changed.
    return;
 }
 // Bug#9100 Mike O
 if (target) {} else target=get_top().frames.main;  // target="main" default
 if ( typeof( target ) == "string" ) {
  //TARGET=window[target].document;
  if (target=="pos_frame") {
    TARGET=get_pos().pos_frame.document
  } else {
    TARGET=window[target].document;
  }
 } else {
  TARGET=target.document;
 }
 
 TARGET.open();
 TARGET.write(result_page);
 //lert("page_was_written");
 TARGET.close();
 //ipayment_client_obj.Respond(""); TEMP
}

// called from C#.  Executes JavaScript code and returns the result to C#.
function js_execute (code) {
  var response = eval(code);
  ipayment_client_obj.Respond(response);
}

// Called by: js casl_link_aux in ide/include/main.js and js casl_link in business/main.js
function casl_link_aux (the_link, target) {
  // error if pos_window not available
  //var pos_window=window.top.frames.pos;
  //if (ipayment_client_obj==null) {
  //  pos_window.casl_client_init( { type:"link", the_link:the_link, link_target:target } );
  //}
  if (typeof the_link!="string")
    the_link=the_link.href;  // assumes it has an href
  //lert("the_link: " + the_link + ", target:" + target);
  casl_client_process_request_and_display(the_link, target);
  return null;
}

// either <FORM action="/xxx.htm"><INPUT type="button" onclick="casl_form(this)"/></FORM> 
// OR:  <FORM action="/xxx.htm" onsubmit="casl_form(this)"> ... </FORM>
function casl_form_aux (the_form, target) {  // should be renamed to casl_form
  //lert("cfa0");
  // error if pos_window not available
  //var pos_window=window.top.frames.pos;
  //if (pos_window.casl_client==null) {
  //  pos_window.casl_client_init( { type:"form", the_form:the_form } );
  //}
  if (the_form.tagName!="FORM") 
    the_form=the_form.form;
  //lert('casl_form_aux:' + the_form.tagName);
  var uri_args=get_form_data_as_query(the_form);
  //lert('casl_form_aux:' + uri_args);
  if (the_form.action.endsWith("?")) the_form.action=the_form.action.substring(0,the_form.action.length-1);  // Remove extra "?" suffix in form action
  var a_request=the_form.action + uri_args;
  casl_client_process_request_and_display(a_request, target);
  return null;
  //lert("IDE SOURCE:" + fget("action_bar","source").value);
 //<INPUT type="button" onclick="parent.frames.pos.casl_client_process_request_and_display(document.getElementById('a_uri').value);"
}

function get_form_data_as_query (the_form) {
  var R = "?";
  var a_element = null;
  var submit_elements = new Array();
  for ( var i=0; i < the_form.elements.length; i++) {
  a_element=the_form.elements[i];
  //lert("a_element.name:" + a_element.name);
  if (a_element.tagName=="select") {
     for(var i=0; i<a_element.options.length; i++) {
       if(a_element.options[i].selected)
        // Bug #10205 Mike O - Must use getAttribute in non-IE browsers
         R = R + encodeURIComponent(a_element.getAttribute("name")) + "=" + encodeURIComponent(a_element.options[i].value);
     }
     // Bug #10205 Mike O - Must use getAttribute in non-IE browsers
  } else if ( a_element.getAttribute("name") &&
              ( a_element.getAttribute("name").length > 0 ) &&
              ( a_element.type != "checkbox" || a_element.checked ) )
    if ( a_element.type == "submit" ) {
      submit_elements.push( a_element );
    } else {
      // Bug #10205 Mike O - Must use getAttribute in non-IE browsers
      R = R + encodeURIComponent(a_element.getAttribute("name")) + "=" + encodeURIComponent(a_element.value) + "&";
    }
  }
  
  var submit_element = null;
  
  for ( var i = 0; i < submit_elements.length; i++ ) {
    a_element = submit_elements[i];
    if ( a_element["clicked"] == true ) {
      submit_element = a_element;
    }
  }
  if ( submit_element == null && ( submit_elements.length > 0 ) ) {
    submit_element = submit_elements[0];
  }
  
  if ( submit_element != null ) {
    // Bug #10205 Mike O - Must use getAttribute in non-IE browsers
    R = R + escape(submit_element.getAttribute("name")) + "=" + escape(submit_element.value) + "&";
  }
  
  R = R.substring(0,R.length-1); // trim off last &
  return R;
}
