﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CASL_engine;

namespace DeviceManagement
{
  public class DeviceManagement
  {
    public Point point { get; }

    public static object fCASL_MakeInstance(CASL_Frame frame)
    {
      GenericObject args = frame.args;

      string IPAddress = args.get("IPAddress").ToString();

      return MakeInstance(IPAddress);
    }

    private static object MakeInstance(string IPAddress)
    {
      return new DeviceManagement(IPAddress);
    }

    public DeviceManagement(string IPAddress)
    {
      point = new Point(IPAddress);
    }

    public static object fCASL_GetPoint(CASL_Frame frame)
    {
      GenericObject args = frame.args;

      DeviceManagement inst = args.get("_this") as DeviceManagement;

      return inst.point;
    }
  }
}
