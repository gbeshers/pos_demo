﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeviceManagement
{
  partial class Point
  {
    private class GetStatusResponse
    {
      public int status { get; set; }
      public int detailedStatus { get; set; }
      public string errorMessage { get; set; } = null;
    }

    private class CancelResponse
    {
      public bool successful { get; set; }
      public string failureResponse { get; set; }
      public string errorMessage { get; set; } = null;
    }

    private class SendMessageResponse
    {
      public string response { get; set; }
      public string errorMessage { get; set; } = null;
    }
  }
}
