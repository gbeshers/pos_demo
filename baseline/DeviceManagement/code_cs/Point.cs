﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CASL_engine;
using Newtonsoft.Json;

namespace DeviceManagement
{
  public partial class Point
  {
    private static readonly int PrimaryPort = 5015;
    private static readonly int SecondaryPort = 5016;

    private string IPAddress;
    private CancellationTokenSource cts = null;
    private BlockingCollection<string> messageQueue = new BlockingCollection<string>(new ConcurrentQueue<string>());
    private HttpClient client = new HttpClient();
    private DateTime lastStatusCheck = DateTime.Now;

    protected string lastStatus;

    private Point() { }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="IPAddress">IP address of Point device</param>
    public Point(string IPAddress)
    {
      this.IPAddress = IPAddress;
      client.Timeout = new TimeSpan(0, 5, 0);
    }

    public static object fCASL_GetMessageAndStatus(CASL_Frame frame)
    {
      GenericObject args = frame.args;

      Point instance = args.get("_this") as Point;

      return instance.GetMessageAndStatus();
    }

    private GenericObject GetMessageAndStatus()
    {
      lastStatusCheck = DateTime.Now;

      Logger.LogDebug(messageQueue.Count.ToString() + ", Status: " + lastStatus, "DeviceManagement.Point.GetMessageAndStatus");
      return new GenericObject(
        "message", messageQueue.Count > 0 ? messageQueue.Take() : null,
        "status", lastStatus
      );
    }

    public static object fCASL_GetMessage(CASL_Frame frame)
    {
      GenericObject args = frame.args;

      Point instance = args.get("_this") as Point;

      return instance.GetMessage();
    }

    private string GetMessage()
    {
      return messageQueue.Take();
    }

    public static object fCASL_IsConnected(CASL_Frame frame)
    {
      GenericObject args = frame.args;

      Point instance = args.get("_this") as Point;

      return instance.IsConnected();
    }

    public bool IsConnected()
    {
      return Task<bool>.Run(async () => { return await IsConnectedAsync(); }).Result;
    }

    public async Task<bool> IsConnectedAsync()
    {
      try
      {
        CancellationTokenSource cts = new CancellationTokenSource();

        await GetStatus(cts.Token);
        return true;
      }
      catch (Exception e)
      {
        Logger.LogError("Exception: " + e.ToMessageAndCompleteStacktrace(), "DeviceManagement.Point.IsConnectedAsync");

        return false;
      }
    }

    public static object fCASL_Cancel(CASL_Frame frame)
    {
      GenericObject args = frame.args;

      Point instance = args.get("_this") as Point;
      int delay = (int)args.get("delay");

      bool result = instance.Cancel(delay).Result;

      Logger.LogError("Cancelled: " + result.ToString(), "DeviceManagement.Point.fCASL_Cancel");

      return result;
    }

    /// <summary>
    /// Send cancel message to device
    /// </summary>
    /// <returns>Whether the cancel was successful</returns>
    public Task<bool> Cancel()
    {
      return Cancel(3000);
    }

    /// <summary>
    /// Send cancel message to device
    /// </summary>
    /// <param name="delay">Amount of time in milliseconds to wait until sending the message</param>
    /// <returns>Whether the cancel was successful</returns>
    public async Task<bool> Cancel(int delay)
    {
      StopGetStatusLoop();

      if (delay > 0)
        await Task.Delay(delay);

      string url = String.Format("{0}/api/Point/Cancel/{1}/{2}",
        c_CASL.c_object("Business.Misc.data.device_management_address").ToString().TrimEnd('/'),
        IPAddress,
        SecondaryPort
      );

      CancelResponse responseMessage;

      HttpResponseMessage response = await client.GetAsync(url);

      if (response.IsSuccessStatusCode)
      {
        StreamReader reader = new StreamReader(await response.Content.ReadAsStreamAsync());
        responseMessage = JsonConvert.DeserializeObject<CancelResponse>(reader.ReadToEnd());
      }
      else
      {
        Logger.cs_log("DMS HTTP Error. URL: " + url + Environment.NewLine + "Status code " + response.StatusCode.ToString());
        throw new Exception("DMS HTTP Error. Status code " + response.StatusCode.ToString());
      }

      if (responseMessage.errorMessage != null)
      {
        Logger.cs_log("DMS Point Cancel error. URL: " + url + Environment.NewLine + "Message: " + responseMessage.errorMessage);
        return false;
      }
      else if (!responseMessage.successful)
      {
        Logger.cs_log("DMS Point Unable to cancel. URL: " + url + Environment.NewLine + "Message: " + responseMessage.failureResponse);
        return false;
      }

      return true;
    }

    public static object fCASL_SendMessage(CASL_Frame frame)
    {
      GenericObject args = frame.args;

      Point instance = args.get("_this") as Point;
      string xml = args.get("xml").ToString();
      bool block = (bool)args.get("block", false);
      bool enqueueResponse = (bool)args.get("enqueue_response", true);

      return instance.StartSendMessage(xml, block, enqueueResponse);
    }

    /// <summary>
    /// Sends message to device, optionally asynchronously
    /// </summary>
    /// <param name="xml">Message to send to device</param>
    /// <param name="block">Whether message sending should block or run asynchronously</param>
    /// <returns></returns>
    private object StartSendMessage(string xml, bool block, bool enqueueResponse)
    {
      Logger.LogDebug($"{block}, {xml}", "DeviceManagement.Point.StartSendMessage");

      if (block)
      {
        string result = SendMessage(xml).Result;
        Logger.LogDebug($"Returning result: {result}", "DeviceManagement.Point.StartSendMessage");
        return result;
      }
      else
      {
        Task.Run(async () => {
          string response = await SendMessage(xml);
          Logger.LogDebug($"Adding response: {response}", "DeviceManagement.Point.StartSendMessage");

          if (enqueueResponse)
            messageQueue.Add(response);
        });

        return true;
      }
    }

    private async Task<string> SendMessage(string xml)
    {
      string url = String.Format("{0}/api/Point/SendMessage?IPAddress={1}&port={2}",
        c_CASL.c_object("Business.Misc.data.device_management_address").ToString().TrimEnd('/'),
        IPAddress,
        PrimaryPort
      );

      SendMessageResponse responseMessage;

      StringContent content = new StringContent("\"" + xml + "\"", Encoding.UTF8, "application/json");
      Logger.LogDebug($"Sending", "DeviceManagement.Point.SendMessage");
      HttpResponseMessage response = await client.PostAsync(url, content);
      Logger.LogDebug($"Received", "DeviceManagement.Point.SendMessage");
      if (response.IsSuccessStatusCode)
      {
        StreamReader reader = new StreamReader(await response.Content.ReadAsStreamAsync());
        responseMessage = JsonConvert.DeserializeObject<SendMessageResponse>(reader.ReadToEnd());
      }
      else
      {
        Logger.cs_log("DMS HTTP Error. URL: " + url + Environment.NewLine + "Status code " + response.StatusCode.ToString());
        throw new Exception("DMS HTTP Error. Status code " + response.StatusCode.ToString());
      }

      if (responseMessage.errorMessage != null)
      {
        Logger.cs_log("DMS Point SendMessage error. URL: " + url + Environment.NewLine + "Message: " + responseMessage.errorMessage);
        return "SERVICE_ERROR";
      }

      Logger.LogDebug($"Response: {responseMessage.response}", "DeviceManagement.Point.SendMessage");
      return responseMessage.response;
    }

    public static object fCASL_StopGetStatusLoop(CASL_Frame frame)
    {
      GenericObject args = frame.args;

      Point instance = args.get("_this") as Point;

      instance.StopGetStatusLoop();

      return true;
    }

    public void StopGetStatusLoop()
    {
      if (cts != null)
        cts.Cancel();

      cts = null;

      lastStatus = "Starting";
    }

    public static object fCASL_StartGetStatusLoop(CASL_Frame frame)
    {
      GenericObject args = frame.args;

      Point instance = args.get("_this") as Point;
      int interval = (int)args.get("interval");
      
      instance.StartGetStatusLoop(new StatusStringProgress(instance), interval);

      return true;
    }

    /// <summary>
    /// Start device status checking
    /// </summary>
    /// <param name="progress">Receive status updates</param>
    /// <param name="interval">How frequently to check for status updates in milliseonds. 3000 is the recommended minimum for Mx devices</param>
    public void StartGetStatusLoop(IProgress<string> progress, int interval)
    {
      if (cts != null)
        StopGetStatusLoop();

      cts = new CancellationTokenSource();

      CancellationToken token = cts.Token;

      lastStatus = "Starting";
      lastStatusCheck = DateTime.Now;

      Task.Run(async () => {
        for (int i = 0; i < 300000; i += interval)
        {
          token.ThrowIfCancellationRequested();

          string status;

          try
          {
            status = await GetStatus(token);
          }
          catch (Exception e)
          {
            status = "Error: " + e.Message;
          }

          token.ThrowIfCancellationRequested();

          progress.Report(status);
          Logger.LogDebug($"Status: {status}{Environment.NewLine}{i}", "DeviceManagement.Point.StartGetStatusLoop");
          await Task.Delay(interval, token);
        }
      }, token);
    }

    private async Task<string> GetStatus(CancellationToken cancellationToken)
    {
      {
        string url = String.Format("{0}/api/Point/GetStatus/{1}/{2}",
          c_CASL.c_object("Business.Misc.data.device_management_address").ToString().TrimEnd('/'),
          IPAddress,
          SecondaryPort
        );

        GetStatusResponse responseMessage;

        HttpResponseMessage response = await client.GetAsync(url, cancellationToken);

        if (response.IsSuccessStatusCode)
        {
          StreamReader reader = new StreamReader(await response.Content.ReadAsStreamAsync());
          responseMessage = JsonConvert.DeserializeObject<GetStatusResponse>(reader.ReadToEnd());
        }
        else
        {
          Logger.cs_log("DMS HTTP Error. URL: " + url + Environment.NewLine + "Status code " + response.StatusCode.ToString());
          throw new Exception("DMS HTTP Error. Status code " + response.StatusCode.ToString());
        }

        if (responseMessage.errorMessage != null)
        {
          Logger.cs_log("DMS Point SendMessage error. URL: " + url + Environment.NewLine + "Message: " + responseMessage.errorMessage);
          throw new Exception("DMS Point GetStatus error. Message: " + responseMessage.errorMessage);
        }

        return GetStatusDescription(responseMessage.status, responseMessage.detailedStatus);
      }
    }

    private static string GetStatusDescription(int status, int detailedStatus)
    {
      string statusText = null;

      if (detailedStatus == 52) statusText = "Prompting to Remove Card";
      else if (status == 11) statusText = "Session Started";
      else if (status == 13 && detailedStatus == 2) statusText = "Prompting for Payment";
      else if (status == 18 && detailedStatus == 2) statusText = "Prompting for Payment";
      else if (status == 13 && detailedStatus == 3) statusText = "Prompting for PIN";
      else if (status == 13 && detailedStatus == 5) statusText = "Prompting for Payment Type";
      else if (status == 13) statusText = "Prompting for Data";
      else if (status == 14) statusText = "Communicating with Processor - Don't Remove Card";
      else if (status == 19) statusText = "Communicating with Processor - Don't Remove Card";
      else if (status == 41) statusText = "Processing";
      else if (status == 51) statusText = "Prompting for Signature";
      else if (status == 0) statusText = "Cancelled";
      else if (status == 10) statusText = "Idle";
      else Logger.LogInfo($"DMS Point: Unknown status code {status},{detailedStatus}", "GetStatusDescription");

      return statusText;
    }
    public static object fCASL_Test(CASL_Frame frame)
    {
      GenericObject args = frame.args;

      string IPAddress = args.get("IPAddress").ToString();
      string func = args.get("func").ToString();

      return Test(IPAddress, func);
    }

    private static object Test(string IPAddress, string func)
    {
      Point point = new Point(IPAddress);

      if (func == "IsConnected")
      {
        return point.IsConnected();
      }
      else if (func == "Cancel")
      {
        return point.Cancel();
      }
      else if (func == "SendMessage")
      {
        return point.SendMessage("<TRANSACTION> <FUNCTION_TYPE>SESSION</FUNCTION_TYPE>  <COMMAND>START</COMMAND>  <INVOICE>56889</INVOICE>  <COUNTER>47</COUNTER></TRANSACTION>");
      }
      else if (func == "GetStatus")
      {
        CancellationTokenSource cts = new CancellationTokenSource();
        return point.GetStatus(cts.Token).Result;
      }
      else if (func == "StartGetStatusLoop")
      {
        StringBuilder sb = new StringBuilder();
        TestProgress tp = new TestProgress(sb);

        point.StartGetStatusLoop(tp, 3000);

        Thread.Sleep(30000);

        point.StopGetStatusLoop();

        return sb.ToString();
      }

      return "Invalid function name";
    }

    public static object fCASL_MakeInstance(CASL_Frame frame)
    {
      GenericObject args = frame.args;

      string IPAddress = args.get("IPAddress").ToString();

      return MakeInstance(IPAddress);
    }

    private static object MakeInstance(string IPAddress)
    {
      return new Point(IPAddress);
    }

    private class StatusStringProgress : IProgress<string>
    {
      private Point point;

      public StatusStringProgress(Point point)
      {
        this.point = point;
      }

      public void Report(string value)
      {
        point.lastStatus = value;

        if (DateTime.Now.Subtract(point.lastStatusCheck).Seconds > 10)
          point.StopGetStatusLoop();
      }
    }

    private class TestProgress : IProgress<string>
    {
      private StringBuilder sb;
      public TestProgress(StringBuilder sb)
      {
        this.sb = sb;
      }
      public void Report(string value)
      {
        sb.AppendLine("Got status: " + value);
      }
    }
  }
}
