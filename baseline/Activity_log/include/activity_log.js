// BUG 24047 - RDM Moving Fred's js into an actual js file
var activity_log_global_data = { activityLogDetailsUri: "" }

function initialize_activity_log_table(ajaxSource, activityLogDetailsUri) {
  $.fn.dataTable.ext.errMode = 'throw'; // don't alert() any errors, just put in console. 
                                        // when logging out from activity log, there's a page refresh before the redirect,
                                        // then datatables trys to redraw itself with ajax request, but the session is invalid so 
                                        // there's a 304 forbiden request. We could either use "fnServerData" to build and handle
                                        // the server request/response, or we could just do this. The fnServerData is certainly
                                        // more robust, but also harder to implement, and I think this is fine, considering that it 
                                        // only fires on the logout request, with the random refresh inbetween redirect.
                                        // Plus, we should do this anyways for all datatables in production. Don't want debug-esque 
                                        // alert messages being displayed to the user!
                                        // See the Resolution section of this article: https://datatables.net/manual/tech-notes/7 
  
  activity_log_global_data.activityLogDetailsUri = activityLogDetailsUri;

  // This will cause the first draw(), the server will respond with no data (all column search vals are empty).
  // The last step will redraw this table with the start and end date search values initialized to today.
  var activityLogTable = $('#activity_log_table').dataTable({
    "pagingType": "full_numbers",
    "bInfo": true,
    "bProcessing": true,
    "bAutoWidth": false,
    "bStateSave": false,
    "aLengthMenu": [10, 25, 50, 100, 250],
    "iDisplayLength": 25,
    "bDeferRender": false,                                
    "sDom": '<"filter_alignment">rt<"bottom"lipf><"clear">',
    "bServerSide": true,
    "sAjaxSource": ajaxSource,
    "iDeferLoading": 0,                     
    "aaSorting": [[1,'desc']],                
    "fnDrawCallback": function () {
              keep_alive();
      },
    "aoColumnDefs": [
      { "aTargets": [0], "bSortable": false, "bSearchable": false, "sWidth": "10px" },  
      { "aTargets": [1], "sTitle": "When", "sType": "date" },
      { "aTargets": [2], "sTitle": "User", "sType": "natural" },
      { "aTargets": [3], "sTitle": "Application", "sType": "natural" },
      { "aTargets": [4], "sTitle": "Action", "sType": "natural" },
      { "aTargets": [5], "sTitle": "Summary", "sType": "natural", "sClass": "summary_column" },
      { "aTargets": [6], "sTitle": "Session", "sType": "natural", "bVisible": false }
    ] 
  });

  activityLogTable.fnFilterOnReturn();

  // BUG 24047 RDM - Setup functions for column searching
  function addDaterangeSearchToCell (cell) {
    var datetimeOptions = { 
      timeFormat: 'hh:mm TT'
    };
    var today = new Date();
    $(cell).html( 'From <input id="activity_log_start_date" type="text" value="" /> To <input id="activity_log_end_date" type="text" value="" />' );
    $('#activity_log_start_date').datetimepicker(datetimeOptions);
    $('#activity_log_end_date').datetimepicker(datetimeOptions);
    $('#activity_log_start_date').datetimepicker('setDate', (new Date()) );
    $('#activity_log_end_date').datetimepicker('setDate', (new Date()) );
  }

  function daterangeSearchHandler (dataTableCol, domInput) {
    var startDate = $('#activity_log_start_date').val();
    var endDate = $('#activity_log_end_date').val();
    var val = startDate + "~" + endDate;
    if ( dataTableCol.search() !== val ) {
      dataTableCol
        .search( val )
        .draw();
    }
  };

  function addTextSearchToCell (cell) {
    var title = $(cell).text();
    $(cell).html( '<input type="text" placeholder="Search '+title+'" />' );
  };

  function textSearchHandler (dataTableCol, domInput) {
    dataTableCol
      .search( domInput.value )
      .draw();
  };

  var columnSearchSetup = 
    [ false
    , { addSearchToCell: addDaterangeSearchToCell, searchHandler: daterangeSearchHandler}
    , { addSearchToCell: addTextSearchToCell, searchHandler: textSearchHandler}
    , { addSearchToCell: addTextSearchToCell, searchHandler: textSearchHandler}
    , { addSearchToCell: addTextSearchToCell, searchHandler: textSearchHandler}
    , { addSearchToCell: addTextSearchToCell, searchHandler: textSearchHandler}
    , { addSearchToCell: addTextSearchToCell, searchHandler: textSearchHandler}
    , { addSearchToCell: addTextSearchToCell, searchHandler: textSearchHandler}
    ]

  // BUG 24047 RDM - attach column search functionality to table footer
  var activityLogTableCapital = 
    $('#activity_log_table')
      .DataTable() // need capital DataTable for the following to work
      .columns().every( function (index) { 
        // for every column, add inputs to each column footer, then for each input in a column footer, add onchange handlers
        var footerCell = this.footer();
        
        if (columnSearchSetup[index] && footerCell) {
          columnSearchSetup[index].addSearchToCell(footerCell);

          var dataTableCol = this;

          $( 'input', this.footer() ).on( 'change', function () { // for each input in this footerCell, and add onchange handlers
            columnSearchSetup[index].searchHandler(dataTableCol, this);
          });
        };
      });

  // BUG 24047 RDM - Move footer to header
  $('#activity_log_table tfoot tr').appendTo('#activity_log_table thead');
  
  // BUG 24047 RDM - Intialize table with server data by drawing
  activityLogTableCapital.draw();
}

function actvity_log_format_details(aSerialID) {
  var uri = activity_log_global_data.activityLogDetailsUri;
  uri = uri + '?database_id=' + aSerialID;
  return $.get(uri, function(data) {
              $("#" + aSerialID + "_details").html(data);
          });
};

function activity_log_showHideSession( iCol ) {
  var oTable = $('#activity_log_table').dataTable();         
  var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;    
  oTable.fnSetColumnVis( iCol, bVis ? false : true );
};

function toggle_details(aDataTableId, aImg, siteStatic) {
  var aRow = aImg.parentNode.parentNode;
  var aSerialID = aRow.id;
  if ( aImg.src.match('arrow_down') ) {
    aImg.src = siteStatic + "/baseline/business/images/arrow_right_alpha.gif";
    $('#' + aDataTableId).dataTable().fnClose(aRow);
  } else {
    aImg.src = siteStatic + "/baseline/business/images/arrow_down_alpha.gif";
    $('#' + aDataTableId).dataTable().fnOpen(aRow, "<div id='" + aSerialID + "_details'/>", 'details');
    actvity_log_format_details(aSerialID);
  }
}