namespace Config_NG

open System
open System.IO
open System.Text.RegularExpressions
open System.Collections.Generic
open System.Diagnostics
open Microsoft.FSharp.Reflection


open CASL_engine
open Util_NS
open SQL_config_NS

open Config_NG
open Config_NG.Path_NS
open Config_NG.Flex_NS
open Config_NG.GenObj_NS

//
// When we read in the Flex table we actually need to make
// three passes over the data before converting to the new
// format.  Also, the new format still has the Flex table
// holding the majority of the data.
//



type MultFlavor =
  | Stand
  | Order
  | ErrMult

type MultInfo =
    {
      pri_cl : string ;  // Primary class
      pri_fd : string ;  // primary field (in secondary class)
      sec_cl : string ;  // Secondary class
      sec_fd : string ;  // secondary field (in primary class)
      flavor : MultFlavor
    }


type GeoRecord =
    {
      gr_path_str : string ;
      gr_path     : Path ;
      gr_fields   : Dictionary<string, Basic_Tree>
    }




module ConfigMod =

    ////////////////////////////////////////////////////////////////
    //
    // Third pass through the hold_table this time calling the
    // CASL post_thaw method on each constructed object.
    //
    ///////////////////////////////////////////////////////////////

    let call_post_thaw (geo_rec : GenObj_Record) =
#if DEBUG_X
      do
        Logging.trace "call_post_thaw: %s (%A, %A) %A"
          geo_rec.raw_container geo_rec.from_holder geo_rec.fields.Count
          geo_rec.gen_obj
#endif
      if geo_rec.from_holder then
        match geo_rec.gen_obj with
        | PR_Base subject | PR_Conf subject ->
          let
            method = subject.get("copy_down_multiples", null, true)
          do
            if method <> null then
              Logging.trace "call_post_thaw %s %A" geo_rec.raw_container geo_rec.name
              try
                misc.base_casl_call(method, subject, new GenericObject (), true)
                  |> ignore
              with
                | ex -> Logging.error "call_post_thaw %s threw exception %A"
                          (GenObjMod.pr_GenObj_str geo_rec) ex
          let
            method = subject.get("post_thaw", null, true)
          do
            if method <> null then
              Logging.trace "call_post_thaw %s %A" geo_rec.raw_container geo_rec.name
              try
                misc.base_casl_call(method, subject, new GenericObject (), true)
                  |> ignore
              with
                | ex -> Logging.error "call_post_thaw %s threw exception %A"
                          (GenObjMod.pr_GenObj_str geo_rec) ex
        | PR_Obj o ->
          Logging.error "call_post_thaw : called on object for %s"
            geo_rec.raw_container
        | PR_Unkn ->
          Logging.error "call_post_thaw : called on PR_Unkn for %s"
            geo_rec.raw_container




    //
    // Replacement for the infamous <thaw_with_path> in CASL.
    // It reads in the entire Flex table and intializes the
    // config data structures (mostly under Business.)
    //
    // This does not currently support the separate path to limit
    // what is being read as that appears never to be used by
    // iPayment.
    //
    // NOTE: the alt_base is a Q/A mechanism to allow old/new
    // copies of the Business hierarchy for DeepDiff comparison.
    //

    let thaw_with_path_ng (db : GenericObject,
                           db_info : Db_Connection_Info,
                           alt_base : string,
                           version : int,
                           timer : Stopwatch) =

      let alt_base_elem = Pstr alt_base
      let rebase_path (Path elem_list as path) =
        if (alt_base.Length > 0) && (elem_list.Length > 0) &&
              (elem_list.Head = (Pstr "Business")) then
          Path (alt_base_elem::elem_list.Tail)
        else
          path


      do
        Logging.timer.Start()
        Logging.error "TIMER: Before opening Flex table %A"
          Logging.timer.Elapsed


      let connection = Sql_API.mkConnectionStr db_info |> Sql_API.newConnection
      use reader = Sql_API.Flex_Read_command connection |> Sql_API.newReader 


      do
        Logging.error "TIMER: Before constructing hold_table %A"
          Logging.timer.Elapsed

      let pipe_trace y x =
        do
          Logging.trace "%s %A" y x
        x
      
      do
        //
        // Read in the Flex table as a sequence
        //
        Sql_API.flex_seq reader            // seq<Flex_row>
        // |> Seq.map (pipe_trace "Row")

        //
        // Convert Flex_rows that come in continuous sequence to a
        // GenObj_Holder with a field for each row.
        //
        |> Flex.Rows_to_Holder           // seq<GenObj_Holder>
        // |> Seq.map (pipe_trace "Raw Holder")

        //
        // For each GenObj_Holder parse:
        //    the container string to a Path
        //    for each field parse:
        //      the string key, value pair into a
        //      (PathElem, BasicTree) pair.
        //
        |> Seq.map Parser.Parse_all_fields     // seq<GenObj_Holder>
        // |> Seq.map (pipe_trace "Parsed Holder")

        //
        // Handle vector parts.
        //    The usual structure of the flex table is abandoned when
        // a "part" (participating in the _container, _name hierarchy)
        // has _parent=vector.  In this case the Flex table contains a
        // field of type vector ('<v> </v>') and the normal row(s) are
        // omitted; however, each element of the vector does have its
        // own rows.
        |> Seq.map FlexCleanup.identify_vector_parts
        // |> Seq.sortBy Flex.holder_depth

        // |> Seq.map (pipe_trace "Vector Part")

        //
        // Add each GenObj_Holder to the GenObj_Table which is an
        // imperitive structure so we only process a single GenObj_Holder
        // at a time.  As part of this process we:
        //   1. test to see if a GenericObject already exists, i.e., was
        //      created as part of the CASL Interpreter setup;
        //   2. if not, but if we have both the
        //      a. the _parent field (pointing to a GenObj_Class)
        //      b. the container GenericObject exists
        //      then call make_part.
        //
        // NOTE: calls to make_part should also be single threaded which
        // we do here to make sure the information in the GenObj_Table is
        // always consistent with CASL GenericObject hierarchy.
        //

        |> GenObjMod.Builder           // seq<GenObj_Holder>
        // |> Seq.map (pipe_trace "Post GenObjMod.Builder")

        |> Seq.iter (fun x -> Logging.trace "After GenObjMod.Builder %s %A"
                               x.raw_container x.name |> ignore)
        Logging.error "Builder Completed"

#if DEBUG
        GenObjMod.dump_table ()
#endif

        //
        // If the associated GenericObject is available forward to
        // add_primitive_fields: excluding anything which has a path as
        // the path may reference a GenericObject that has not been
        // created yet.
        //
        // |> Seq.iter FillFields..populate_primitives           // seq<GenObj_Holder>
        // |> Seq.iter (pipe_trace "Holders_to_primitive_fields " |> ignore)


        // In GenObjMod
        //    Enumerate
        //       PR_Base excluded but their children are not
        //       


      do
        GenObjMod.traverse_table FillFields.populate_genobj

        Multiples.synchronize_inverses ()

        GenObjMod.traverse_table call_post_thaw

      do
        Logging.error "TIMER: Finished %A" Logging.timer.Elapsed
        Logging.timer.Stop()


        Logging.trace_flush ()
        Logging.error_flush ()

      null

