////////////////////////////////////////////////////////////////////
//
// Interface to CASL / GenericObject world.
//
// The central point is the map from paths to GenericObjects and
// making sure that is consistent.
//
////////////////////////////////////////////////////////////////////


namespace Config_NG.GenObj_NS

open System
open System.IO
open System.Text.RegularExpressions
open System.Collections.Generic
open System.Diagnostics
open Microsoft.FSharp.Reflection


open CASL_engine
open Util_NS
open SQL_config_NS

open Config_NG.Path_NS
open Config_NG.Flex_NS


//
// When we are handed a Path (Path.fs) the result is one of the
// following:
//   PR_Base -- the GenericObject exists prior to Config being loaded
//   PR_Conf -- the GenericObject is a part specified in Config
//                ** currently the Flex table.
//   PR_Obj  -- the path points to a leaf object (e.g., int, string,
//                datetime, etc.) -- Never a GenericObject however.
//   PR_Unkn -- the path lookup fails, almost always junk in the
//                Flex table.
//
type PathResult =
   | PR_Base of GenericObject
   | PR_Conf of GenericObject
   | PR_Obj  of obj
   | PR_Unkn

//
// This needs to be mutable to handle out-of-order rows coming in
// from the Flex table without needing the entire Flex table in memory.
//
//   raw_container - is just for debugging.
//   container     - path to _container in associated GenericObject
//   name          - PathElem which becomes _name
//       container . name path to the associated GenericObject
//   parent        - is only valid if 'gen_obj' is PR_Unkn.  It is
//                   only for the case where we can create the
//                   GenObj_Record but not the GenericObject of the
//                   container because we don't know its parent yet.
//   gen_obj       - the associated GenericObject
//   fields        - Basically one per row for this GenericObject
//                   excluding null, null
//   children      - Implements the GenObj_Record tree which is
//                   a projection of the eventual CASL structure.
//   errors        - Not fully implemented.
//   make_part     - null, null Row seen in Flex Table.  We need see this
//                   before it is safe to create VectorParts; simple vectors
//                   which look like VectorParts but don't have an associated
//                   null, null row in the Flex table need special handling.
//   from_holder   - Some GenObj_Records are constructed for CASL
//                   GenericObjects which exist before thaw_with_path_ng
//                   is called; however some of these are modified and
//                   those need to have Populate processing.
//

type GenObj_Record =
   {
     raw_container : string ;
     container : Path ;
     name : PathElem ;
     // mutable parent : GenObj_Class ;
     mutable parent : GenericObject ;
     mutable gen_obj : PathResult ;
     fields : Dictionary<string, GenObj_Field> ;
     children : Dictionary<string, GenObj_Record> ;
     mutable errors : FlexError list ;
     mutable make_part : bool ;
     from_holder : bool
   }


module GenObjMod =

    let copy_value from_geo target_geo =
          misc.Copy(from_geo, target_geo, null, null)

    //
    // Convert a PathElem to a key that CASL understands.
    //
    
    let caslize name_elem =
      match name_elem with
      | Pstr s -> s :> obj
      | Pint i -> box i

    //
    // Wrappers for get() and set().
    //
    // CAREFUL: int_name is an int and the C# get() method
    // handles it differently than a string.
    //
    let geo_get (geo : GenericObject)
                (key : PathElem)
                (missing : obj)
                (lookup : bool) =
      try
        let k = caslize key
        geo.get(k, missing, lookup)
      with
        | ex -> Logging.error "geo_get: key=%A %A" key ex
                missing

    let geo_set (geo : GenericObject)
                (key : PathElem)
                (value : obj) =
      try
        let k = caslize key
        geo.set(k, value) |> ignore
      with
        | ex -> Logging.error "geo_set: key=%A %A" key ex

    let geo_has (geo : GenericObject)
                (key : PathElem) : bool =
      try
        let k = caslize key
        geo.has(k)
      with
        | ex -> Logging.error "geo_has: key=%A %A" key ex
                false

    let exclude_db_orig =
      let
        edo = new GenericObject()
      do
        geo_set edo (Pint 0) "_db_orig"
        geo_set edo (Pint 1) "_db"
      edo

    let exclude_container =
      let
        ec = new GenericObject()
      do
        geo_set ec (Pint 0) "_db_orig"
        geo_set ec (Pint 1) "_db"
        geo_set ec (Pint 2) "_container"
      ec

    let exclude_name_cont =
      let
        ex = new GenericObject()
      do
        geo_set ex (Pint 0) "_db_orig"
        geo_set ex (Pint 1) "_db"
        geo_set ex (Pint 2) "_container"
        geo_set ex (Pint 3) "_name"
      ex

    let is_GenObj (o : obj) =
      match o with
      | null -> false
      | :? GenericObject -> true
      | _ -> false

    let is_GenObjClass (o : obj) =
      match o with
      | null -> false
      | :? GenObj_Class -> true
      | :? GenericObject -> false
      | _ -> false


    let pr_get_GenObj pr =
      match pr with
      | PR_Base geo | PR_Conf geo -> geo
      | _ -> null

    let pr_GenObj_str (gr : GenObj_Record) =
      PathMod.append gr.container gr.name |> PathMod.to_string
    
    let eql (a : GenericObject) (b : GenericObject) =
      LanguagePrimitives.PhysicalEquality a b


        

    let GEO_Base = PR_Base c_CASL.GEO
    //let unknown_parent = null : GenObj_Class

    let parent_key = Pstr "_parent"
    let unknown_parent = null : GenericObject
    let GEO = {
        raw_container = "" ;
        container = Path [] ;
        parent = unknown_parent ;
        name = Pstr "GEO" ;
        gen_obj = GEO_Base ;
        fields = new Dictionary<string, GenObj_Field>() ;
        children = new Dictionary<string, GenObj_Record>() ;
        errors = [] ;
        make_part = false ;
        from_holder = false
      }

    let append_raw cont_rec name =
      let ns = PathMod.elem_to_string name
      if cont_rec <> GEO then
        cont_rec.raw_container + "." + ns
      else
        ns

    //
    // Common code fragment, not expected to be used elsewhere.
    //
    type private get_genobj =
         | Geo_OK of GenericObject
         | Geo_Bad of obj


    let add_error (cur_rec : GenObj_Record) (err : FlexError) =
      cur_rec.errors <- err::cur_rec.errors

    //
    // Implements c_CASL.GEO.get_path but with error logging and
    // a PR_Unkn result when the path is invalid.
    //
    // PR_Conf should always be handled by the Path_GenObj_Table,
    // so that option is never returned from this function.
    //

    let find_geo_path (base_geo : GenericObject) (Path elem_list as path) : PathResult =
      let rec find_obj (current : GenericObject)
          (path : PathElem list) (path_to_me : Path) =
        match path with
        | [] ->
          PR_Base current
        | hd::[] ->
          let obj = geo_get current hd null false
          match obj with
          | :? GenericObject as go -> PR_Base go
          | null ->
            if geo_has current hd then
              PR_Obj null
            else
              PR_Unkn
          | _ -> PR_Obj obj
        | hd::tl ->
          let new_current = geo_get current hd null false
          match new_current with
          | :? GenericObject as go ->
                  find_obj go tl (PathMod.append path_to_me hd)
          | null ->
            if geo_has current hd then
              PR_Obj null
            else
              PR_Unkn
          | _ ->
            Logging.error "find_geo_path %s not a GenericObject; %A %s"
                     (PathMod.to_string path_to_me) hd
                     (PathMod.elem_list_to_string tl)
            PR_Unkn
      let
        res = find_obj base_geo elem_list (Path [])
#if DEBUG_X
      do
        Logging.trace "find_geo_path: %s %A"
          (PathMod.elem_list_to_string elem_list) res
#endif
      res

    type GenObj_Result =
       | R_Exists of GenObj_Record
       | R_GEO of GenObj_Record * PathElem list

    let getContainer (full_path : Path) : GenObj_Result =
      let rec next_child current (elem_list : PathElem list) =
        match elem_list with
        | [] -> R_Exists current
        | hd::tl ->
          let dkey = PathMod.elem_to_key hd
          if current.children.ContainsKey dkey then
            let new_cur = current.children.[dkey]
            next_child new_cur tl
          else
            R_GEO (current, elem_list)
      let elem_list = PathMod.get_list full_path
      next_child GEO elem_list


    let path_to_GenObj (path : Path) =
      let next_cont = getContainer path
      match next_cont with
      | R_Exists nc -> nc.gen_obj
      | R_GEO (cur_rec, path_tail) ->
        match cur_rec.gen_obj with
        | PR_Base geo | PR_Conf geo ->
          find_geo_path geo (Path path_tail)
        | PR_Obj _ ->
          Logging.error "path_to_GenObj found non-GenericObject at %A" path
          cur_rec.gen_obj
        | PR_Unkn -> PR_Unkn

    let get_GenObj = path_to_GenObj >> pr_get_GenObj

    let validate_parent cont_path_str (par : GenericObject) =
      if is_GenObjClass par then
        (par, [])
      else
        let err = {
          err_cont = cont_path_str ;
          err_key = "_parent" ;
          err_val = par.get_path_to_me ;
          err_flag = par.get_path_to_me |>  Err_parent_not_class }

        do
          Logging.error "IPAY-1125 and perhap IPAY-336 %s %s : %A"
            "parent is a GenericObject but not a GenObj_Class"
            cont_path_str par
        (par, [err])

    //
    // Special handling for the _parent field.  First it could
    // be either a BT_Path or BT_Symbol in the later case we
    // just convert it to a path.
    //
    // IMO, the only legitimate case should be a PR_Base to
    // a GenObj_Class --- everything else is an error.  But
    // we do handle GenericObjects still, with a warning.
    //
    // At this time if the path resolves to something useless
    // we create the error and return c_CASL.c_error or c_CASL.GEO
    // rather than unknown_parent.  This means that we can not
    // handle duplicate _parent fields gracefully, but does allow
    // us to complete the loading of the config database.
    //
    // There is the alternative, arguably superior, approach of
    // late processing of the backlog and incomplete holding
    // structures in Builder.
    // 
    
    let resolve_class_path cont_path_str path_or_symbol =
      let find_class path =
        match find_geo_path c_CASL.GEO path with
        | PR_Base p -> validate_parent cont_path_str p
        | PR_Conf p ->
          let (par, err_list) = validate_parent cont_path_str p
          let err = {
            err_cont = cont_path_str ;
            err_key = "_parent" ;
            err_val = par.get_path_to_me ;
            err_flag = PathMod.to_string path |>
                         Err_parent_constructed }
          do
            Logging.error "IPAY-1125 and perhap IPAY-336 %s %s %A"
              "parent has been created by Config"
                cont_path_str p
          (par, err::err_list)
        | PR_Obj o ->
          let err = {
            err_cont = cont_path_str ;
            err_key = "_parent" ;
            err_val = sprintf "%A" o
            err_flag = PathMod.to_string path |>
                         Err_parent_path_not_GenericObject }
          do
            Logging.error "IPAY-336 _parent is not a GenericObject %s path=%s %A"
              cont_path_str (PathMod.to_string path) o
          (c_CASL.c_error :> GenericObject, [err])
        | PR_Unkn ->
          let err = {
            err_cont = cont_path_str ;
            err_key = "_parent" ;
            err_val = "None" ;
            err_flag = PathMod.to_string path |>
                         Err_parent_missing }
          do
            Logging.error "IPAY-336 _parent not found, substituting GEO %s path=%s"
              cont_path_str (PathMod.to_string path)
          (c_CASL.GEO  :> GenericObject, [err])     // Consider c_CASL.c_error

      match path_or_symbol with
      | BT_Path p -> find_class p
      | BT_Symbol s -> Path [ Pstr s ] |> find_class
      | _ ->
        let err = {
            err_cont = cont_path_str ;
            err_key = "_parent" ;
            err_val = sprintf "%A" path_or_symbol ;
            err_flag = sprintf "%A" path_or_symbol |>
                         Err_parent_value_not_path_or_symbol }
        do
          Logging.error "Holder %s has corrupt _parent field %A"
            cont_path_str path_or_symbol
        (c_CASL.c_error :> GenericObject, [err])

    type field_dict = Dictionary<string, GenObj_Field>
    let field_folder (cur_rec : GenObj_Record)
                     (flds : Map<string, GenObj_Field>) =
      let mutable parent = None
      let fields = cur_rec.fields
      let folder k v =
        if k = "_parent" then
           parent <- Some v.value
        elif fields.ContainsKey(k) then
          if v <> fields.[k] then
            add_error cur_rec
              {
                err_cont = cur_rec.raw_container ;
                err_key = PathMod.elem_to_string cur_rec.name ;
                err_val = sprintf "%A" fields.[k] ;
                err_flag = Err_Con_Row (sprintf "%A" v)
              }
        else
          fields.Add(k, v)
      Map.iter folder flds
      parent

    let mkContainer (cont : GenObj_Record)
                    (r_c : string)
                    (n : PathElem)
                    // (par : GenObj_Class)
                    (par : GenericObject)
                    (go : PathResult)
                    (flds : Map<String, GenObj_Field>)
                    (errs : FlexError list)
                    (mk_part : bool)
                    (from_flex : bool)
                    (where : string) =
      let
        // Business not GEO.Business
        cont_path =
          if cont = GEO then
            Path []
          else
            PathMod.append cont.container cont.name

      do
        let raw_cont = PathMod.append cont_path n
        if r_c <> PathMod.to_string raw_cont then
          Logging.error "mkContainer: raw_container issue %s <> %s called from %s with %A"
            r_c (PathMod.to_string cont_path) where go
      let new_child = {
        raw_container = r_c ;
        container = cont_path ;
        name = n ;
        parent = unknown_parent ;
        gen_obj = go ;
        // verify and Filter _parent
        fields = new field_dict () ;
        children = new Dictionary<string, GenObj_Record>() ;
        errors = errs ;
        make_part = mk_part ;
        from_holder = from_flex
      }
      let fld_parent = field_folder new_child flds      

      new_child.parent <-
        match go with
        | PR_Base geo | PR_Conf geo ->
          // check_parent_consistency
          unknown_parent // don't add to confusion
        | PR_Obj _ ->
          Logging.error "mkContainer passed a non GenericObject"
          unknown_parent // Were so broken it don't matter no more...
        | PR_Unkn ->
          match fld_parent with
          | Some fp ->
            let (fld_par, fld_par_errs) =
                  resolve_class_path new_child.raw_container fp
            do
              for err in fld_par_errs do
                add_error new_child err
              if par <> unknown_parent && fld_par <> par then
                Logging.error "mkContainer parent conflict %s.%A  %A <> %A"
                                 cont.raw_container n par fld_par
            fld_par
          | None ->
            do
              if par = unknown_parent then
                Logging.trace "mkContainer unspecified parent %s.%A"
                                 cont.raw_container n
            par

      do
        cont.children.Add (PathMod.elem_to_key n, new_child)
      do 
        Logging.trace "Making GenObj_Record at %s %s"
          (PathMod.to_string cont_path) (PathMod.elem_to_string n)
        Logging.trace "    raw_container=%s(%s)  now has %d children @%s"
          cont.raw_container (PathMod.to_string cont_path) cont.children.Count where
      new_child

    //
    // Keep GenericObject allocation to a single function as it will
    // facilitate allocating different C# objects based on the parent.
    //
    // Also, parent "should" be a GenObj_Class --- always, but there
    // are still a few corner cases.
    //
    let create_GenObj (parent : GenericObject) : GenericObject =
      new GenericObject("_parent", parent)

    let make_part
        (container : GenericObject)
        (name : PathElem)
        (parent : GenericObject) =
      let
        geo = create_GenObj parent
      do
        Logging.trace "make_part: %s.%A _parent=%s"
          container.get_path_to_me name parent.get_path_to_me
        geo_set geo (Pstr "_container")  container
        geo_set geo (Pstr "_name") (caslize name)
        geo_set container name geo |> ignore
      geo


    //
    // This appears to be overkill.  The concern was that
    // rows from the flex table might not be collected together
    // resulting in more than one GenObj_Holder for the same
    // GenObj_Record and associated GenericObject.
    //
    // That appears not to happen, but I am leaving this in place
    // at least for the moment.
    //

    let empty_map = Map<string, GenObj_Field> []
    let getMakeRecord
        (cont : GenObj_Record)
        (holder : GenObj_Holder) =

      let (parent, parent_errs) =
        if holder.gh_fields.ContainsKey("_parent") then
          resolve_class_path holder.gh_raw_cont
            holder.gh_fields.["_parent"].value
        elif holder.gh_name = Pstr "data" then
          (find_geo_path c_CASL.GEO holder.gh_container |> pr_get_GenObj, [])
        else
          (unknown_parent, [])

      let part_path = PathMod.append holder.gh_container holder.gh_name

      let ret_rec =
        match getContainer part_path with
        | R_Exists geo_rec ->
          let
            mutable field_errors = [] : FlexError list
          let
            add_field k v =
              let fld = geo_rec.fields
              if fld.ContainsKey(k) then
                if fld.[k] = v then
                  field_errors <- {err_cont = geo_rec.raw_container ;
                                   err_key = PathMod.elem_to_string v.name ;
                                   err_val = sprintf "%A" v.value ;
                                   err_flag = War_Dup_Row}::field_errors
                else
                  field_errors <- {err_cont = geo_rec.raw_container ;
                                   err_key = PathMod.elem_to_string v.name ;
                                   err_val = sprintf "%A" fld.[k].value ;
                                   err_flag = Err_Con_Row (sprintf "%A" v.value)
                                   }::field_errors
                  
              else
                fld.Add(k, v)
          do
            Map.iter add_field holder.gh_fields
            geo_rec.errors <-
              List.append field_errors parent_errs |>
                List.append holder.gh_errors |>
                List.append geo_rec.errors
          geo_rec
        | R_GEO _ ->
          let (n_geo, n_parent, n_error_opt) =
            match cont.gen_obj with
            | PR_Base cont_geo
            | PR_Conf cont_geo ->
              if geo_has cont_geo holder.gh_name then
                let ng = geo_get cont_geo holder.gh_name null false :?> GenericObject
                // let ng_parent = geo_get ng parent_key null false :?> GenObj_Class
                let ng_parent = geo_get ng parent_key null false
                                  :?> GenericObject
                let p_error =
                  //
                  // Here parent = unknown_parent means that there was not a row
                  // with _parent key in the current GenObj_Holder.  Might be one
                  // in a different Holder for the same object, but the usual
                  // case is we are modifying an existing CASL object and _parent
                  // was not save to the Flex table.
                  //
                  if parent <> unknown_parent && parent <> ng_parent then
                    do
                      Logging.error "getMakeRecord %s %s %s %s %A <> %A"
                        (PathMod.to_string part_path)
                        "existing GenericObject"
                        ng.get_path_to_me
                        "with different _parent "
                        parent
                        ng_parent
                    Some { err_cont = holder.gh_raw_cont ;
                      err_key = "_parent" ;
                      err_val = sprintf "%A" ng_parent ;
                      err_flag = sprintf "%A" parent |> Err_parent_conflict }
                  else
                    None
                (PR_Base ng, unknown_parent, p_error)
              elif parent <> unknown_parent then
                (make_part cont_geo holder.gh_name parent |> PR_Conf,
                   unknown_parent, None)
              else
                (PR_Unkn, unknown_parent, None)
            | PR_Obj _ ->
                // must never happen
                (PR_Unkn, parent, None)
            | PR_Unkn -> (PR_Unkn, parent, None)
          let errors =
            let err_x = List.append parent_errs holder.gh_errors
            match n_error_opt with
            | Some err -> err::err_x
            | None -> err_x
          let ret_rec = mkContainer cont holder.gh_raw_cont holder.gh_name n_parent
                          n_geo holder.gh_fields errors holder.gh_make_part true "getMakeRecord"
          ret_rec

#if DEBUG_OLD
      let (ret_geo, parent_test) =
        match ret_rec.gen_obj with
        | PR_Base g1 -> (g1, false)
        | PR_Conf g2 -> (g2, true)
        | _ -> (null, false)
      let gen_obj_path_str = ret_geo.safe_to_path ()
      let full_path_str = PathMod.to_string full_path

      //
      // Verify Path Consistency
      //
      do
        if gen_obj_path_str <> full_path_str then
          Logging.error "getMakeRecord flunks %s <> %s  (%A)  @%s"
            gen_obj_path_str full_path_str full_path where

      //
      // If we found a record check to make sure that it and the
      // associated GenericObject have agree on _parent.
      //
      let ret_par = geo_get ret_geo parent_key null false
      do
        match ret_par with
        | :? GenObj_Class as rp ->
          if rp <> parent then
            Logging.error "getMakeRecord parent issue at %s --- %s <> %s  @%s"
              full_path_str (rp.full_name ()) (parent.full_name ()) where
        | _ ->
          Logging.error "getMakeRecord parent is a bad object at %s --- %s  @%s"
            full_path_str (CASL_Frame.show_value ret_par) where
#endif
      ret_rec

    //
    // When processing a path there are cases where the GenericObject
    // for that path exists, create by initialize_casl_engine(), but
    // the GenObj_Record does not exist.  Except for GEO all of those
    // cases should be handled here.
    //
    // Note that the process is recursive.  The path is relative to
    // the GenericObject cont.gen_obj which (should) always be a PR_Base.
    //
    let rec makeBaseRecord (cont : GenObj_Record) (path : PathElem list) =
      match path with
      | [] -> (cont, [])
      | hd::tl ->
        match cont.gen_obj with
        | PR_Base cont_geo | PR_Conf cont_geo ->
          if geo_has cont_geo hd then
            let new_part =
              match geo_get cont_geo hd null false with
              | :? GenericObject as geo -> PR_Base geo
              | junk ->
                do
                  Logging.error "Expected a GenericObject but got %A" junk
                PR_Unkn
            let raw_cont = append_raw cont hd
                             
            let new_cont = mkContainer cont raw_cont hd unknown_parent new_part
                             (Map []) [] false false "makeBaseRecord"
            makeBaseRecord new_cont tl
          else
            (cont, path)
        | PR_Obj o ->
          Logging.error "makeBaseRecord %s.%A working on %A"
              cont.raw_container cont.name hd
          (cont, path)
        | PR_Unkn ->
          (cont, path)
    
      


    //
    // The following regular expression and the functions elide_methods
    // an eval_BT_GEO are to handle legacy situations where methods were
    // mistakenly stored in the Flex table.  The function elide_methods
    // is only called if the string "<method>" is found in the value
    // string.
    //
    // NOTE: If "opt.init" is found then we substitute 'opt' in the
    // value string---this covers the common case where the default
    // value is "opt" in the CASL class.
    //
    // NOTE: the text of the value string actually contains just newlines,
    // '\n' --- Environment.NewLine doesn't work.
    //
    
    let
      method_reg = new Regex(@"(\w|_)*\s*=\s*<method>.*?</method>\n",
                        RegexOptions.Compiled ||| RegexOptions.Singleline)

    let rec elide_methods (casl_str : string) =
      let
        ma = method_reg.Match casl_str
      let
        res =
          if ma.Success then
            let
              meth = casl_str.Substring (ma.Index, ma.Length)
            if meth.IndexOf "opt.init" >= 0 then
              let rem = casl_str.Substring(0, ma.Index) + "opt" +
                        casl_str.Substring(ma.Index + ma.Length)
              elide_methods rem
            else
              elide_methods (casl_str.Remove (ma.Index, ma.Length))
          else
            casl_str
      res
      
    let eval_BT_GEO (str : string) =
      if str.IndexOf "opt.init" >= 0 && str.IndexOf "opt.copy" >= 0 &&
         str.IndexOf "opt.insert" >= 0 then
        c_CASL.c_opt :> obj
      elif str.IndexOf "req.init" >= 0 && str.IndexOf "req.copy" >= 0 &&
         str.IndexOf "req.insert" >= 0 then
        c_CASL.c_req :> obj
      else
        do
          Logging.trace "eval_BT_GEO calling execute_string() %s" str
        let
          string_to_execute =
            if str.IndexOf "<method>" >= 0 then
              do
                Logging.trace "calling elide_methods with %s" str
              let
                el_str = elide_methods str
              do
                Logging.trace "elide_methods returned %s" el_str
              el_str
            else
              str
        misc.execute_string (Logging.error_file, string_to_execute)

    //
    // These are GenObj_Class objects for the named CASL types, used
    // to set _parent appropriately.
    //

    let e_parent = Pstr "_parent"
    let e_container = Pstr "_container"
    let e_objects = Pstr "_objects"
    let e_name = Pstr "_name"
    let e_db = Pstr "_db"
    let e_db_orig = Pstr "_db_orig"

    let vector_class =
      match find_geo_path c_CASL.GEO (PathMod.mkPath "vector") with
      | PR_Base geo -> geo
      | _ -> failwith "Vector Casl class not found"


    let multiple =
      match find_geo_path c_CASL.GEO (PathMod.mkPath "multiple") with
      | PR_Base geo -> geo
      | _ -> failwith "Multiple Casl class not found"

    let mult_ordered =
      match find_geo_path c_CASL.GEO (PathMod.mkPath "multiple.ordered") with
      | PR_Base geo -> geo
      | _ -> failwith "multiple.ordered class not found"

    let db_class = geo_get c_CASL.GEO (Pstr "db") null false :?> GenericObject

    let Log_GenObj_Record cur_rec =
      let
        indent = "        "
      let
        b_or_c =
          match cur_rec.gen_obj with
          | PR_Base _ -> "Base"
          | PR_Conf _ -> "Conf"
          | PR_Obj _  -> "Obj"
          | PR_Unkn   -> "Unkn"
      do
        Logging.trace "%sLog_GenObj_Record %s %s" indent cur_rec.raw_container b_or_c
        for v in cur_rec.fields.Values do
           Logging.trace "%s     %A" indent v
        Logging.trace "%schildren count = %d" indent cur_rec.children.Count
        Logging.trace "%sserrors = %A" indent cur_rec.errors



    ////////////////////////////////////////////////////////
    //
    // Used by both Multiples and PopulateFields in addition to
    // the Add_child and add_VectorParts
    //
    ///////////////////////////////////////////////////////

    let is_part
        (subject : GenericObject)
        (container : GenericObject)
        (key : PathElem) =
      let
        cont_chk = geo_get subject e_container null false
                       :?> GenericObject
      let
        res = (eql cont_chk container) &&
                (geo_get subject e_name null false) =
                  caslize key
#if DEBUG_X
      do
        Logging.trace "is_part cont=%s  subj=%s   key=%A  res=%A"
          container.get_path_to_me subject.get_path_to_me key res
#endif
      res


    /////////////////////////////////////////////////////////
    //
    // Wrapper on misc.Copy to make sure that _parent is set
    // correctly.
    //
    ////////////////////////////////////////////////////////

    let copy_geo
        (db_orig : GenericObject)
        (name : PathElem)
        (from_geo : GenericObject)
        (include_list : obj)
        (exclude : GenericObject) =
      if is_GenObjClass from_geo then
        geo_set db_orig name from_geo
      else
        let
          parent = geo_get from_geo parent_key c_CASL.GEO false
                     :?> GenericObject
        let
          target = make_part db_orig name parent
        misc.Copy(from_geo, target, include_list, exclude) |> ignore




    //
    // Called when "_db" needs to be added to the new_part GenericObject.
    //
    let init_db
        (new_part : GenericObject)
        (new_part_path : Path) =
      do
        if geo_has new_part e_db |> not then
          let
            ndb = create_GenObj db_class
          do
            geo_set ndb (Pstr "path") (PathMod.to_string new_part_path)
            geo_set new_part e_db ndb

    //
    // Called when "_db_orig" needs to be added to the new_part GenericObject.
    //
    let init_db_orig
        (new_part : GenericObject)
        (new_part_path : Path) =
      do
        if geo_has new_part e_db_orig |> not then
          let par =
            let
              p = geo_get new_part e_parent c_CASL.GEO false :?> GenericObject
            if p = null then
              Logging.error "init_db_orig new_part %s has bad parent "
                (PathMod.to_string new_part_path)
              c_CASL.GEO :> GenericObject
            else
              p
          let db_orig = create_GenObj par
          do
            geo_set new_part e_db_orig db_orig
            Logging.trace "created _db_orig for %s"
              (PathMod.to_string new_part_path)



    //
    // Called for each entry into a _db_orig object
    let add_to_db_orig (container : GenericObject) cont_path
                       (key : PathElem)
                       (value : obj)
                       location =
      begin
        let caslize_key = caslize key
        let key_str = PathMod.elem_to_string key
        let cont_path_str = PathMod.to_string cont_path
        let mutable message = ""
        let db_orig_path = PathMod.append_key cont_path "_db_orig"
        let db_orig =
          do
            if geo_has container e_db_orig |> not then
              init_db_orig container cont_path
          geo_get container e_db_orig c_CASL.c_error false :?> GenericObject
        let name =
          if container.has "_name" then
            let obja = container.get "_name"
            obja.ToString()
          else
            ""

        let special_of_cast = name = "of"

        let handle_gen_obj (gv : GenericObject) =
          let
            gv_par =
              if gv.has "_parent" then
                gv.get "_parent" :?> GenericObject
              else
                (null : obj) :?> GenericObject

          if is_part gv container key then
            message <- "B 'is_part'"
            let exclude =
              if gv_par.Equals(multiple) || gv_par.Equals(mult_ordered) then
                exclude_name_cont
              else
                exclude_container

            let include_list = gv.get("_db_field_order", null)
            copy_geo db_orig key gv include_list exclude
          elif gv_par.Equals(null) |> not then
            if gv_par.Equals(c_CASL.GEO) then
              message <- "C 'parent is GEO'"
              copy_geo db_orig key gv null exclude_db_orig
            elif gv_par.Equals(multiple) || gv_par.Equals(mult_ordered) then
              message <- "D 'multiple'"
              copy_geo db_orig key gv null exclude_name_cont
            else
              message <- "E 'parent other'"
              copy_geo db_orig key gv null exclude_db_orig
          else
            message <- "F 'no parent'"
            geo_set db_orig key gv
#if OF
        if name = "of" then
          let holder = make_part db_orig key c_CASL.GEO 
          do
            message <- "A 'of case'"
            holder.set("_name", caslize_key ) |> ignore
            geo_set holder e_name caslize_key
#endif

        match value with
        | :? PathResult as pr ->
          match pr with
          | PR_Base geo
          | PR_Conf geo ->
              handle_gen_obj geo
          | PR_Obj o ->
              message <- "H 'not a GenericObject'"
              geo_set db_orig key value
          | PR_Unkn ->
              Logging.error "add_to_db_orig got PR_Unkn %s %A"
                 (PathMod.to_string cont_path) key
              message <- "Error 'got PR_Unkn'"
              geo_set db_orig key value
          
        | :? GenericObject as gv ->
            handle_gen_obj gv
        | :? String as s ->
            message <- "string"
            geo_set db_orig key s
        | :? int as i ->
            message <- "int"
            geo_set db_orig key i
        | :? decimal as d ->
            message <- "decimal"
            geo_set db_orig key d
        | :? bool as b ->
            message <- "decimal"
            geo_set db_orig key b
        | _ ->
            message <- "G 'not a GenericObject'"
            geo_set db_orig key value

#if DEBUG
        Logging.trace "add_to_db_orig %s  %s._db_orig at %A(%A) caller=%s"
                         message cont_path_str key caslize_key location
#endif
      end




    //
    // The BT_VectorPart(s) again need to be handled specially.  The field
    // list and each BT_VectorPart gets a container, but not yet a part.
    // 
    //
    // If a BT_VectorPart is found it new part and container are created
    // and returned --- the later is important for processing the backlog
    // handled in Builder.
    //
    // NOTE:  *AT THIS TIME* all BT_VectorPart values have no vector members;
    // the BT_Path(s) to contained objects have been removed and the contained
    // objects have their own Rows and GenObj_Holders.  This is why the result
    // of mkContainer is ignored
    //

    let rec add_VectorParts (cont : GenObj_Record) =
      let mutable keys_to_remove = ([] : string list)
      let mutable yield_list = []
      let cont_path = PathMod.append cont.container cont.name
      for fk in cont.fields.Keys do
        let field = cont.fields.[fk]
        match field.value with
        | BT_VectorPart ( exts, kids ) ->
          let part =
            match cont.gen_obj with
            | PR_Base container_geo | PR_Conf container_geo ->
                make_part container_geo field.name vector_class |> PR_Conf
            | _ -> PR_Unkn
          let raw_cont = append_raw cont field.name
          let new_rec =
            mkContainer cont raw_cont field.name vector_class part (Map []) []
              false false "add VectorParts"
          keys_to_remove <- fk::keys_to_remove
          yield_list <- new_rec::yield_list
        | _ -> ()
      List.iter (fun k -> cont.fields.Remove k |> ignore) keys_to_remove
      yield_list


    //
    // The standard case of creating an object stored from the information
    // stored in the Flex table.  We are passed the GenObj_Record for the
    // container where we will add the child, and the GenObj_Record for
    // the child.
    //
    // We get the parent from the fields---patching the special case of
    // the "data" objects.  The default parent is c_CASL.GEO.
    //
    // There are a few annoying instances where the parent is not actually
    // a GenObj_Class that can trip us up.
    //
    // Given the parent, we create the new part and GenObj_Record.
    //
    // We also create any VectorParts at this time and process the
    // GenObj_Records to reduce the backlog handled in Builder.
    //

    let Add_child (cont : GenObj_Record) (holder : GenObj_Holder) =
      let new_record = getMakeRecord cont holder
      let cont_path = PathMod.append cont.container cont.name
      let cont_geo = pr_get_GenObj cont.gen_obj
      let new_value = pr_get_GenObj new_record.gen_obj
      if new_value <> null then
        add_to_db_orig cont_geo cont_path holder.gh_name new_value "Add_child"
      let added_vector_parts = add_VectorParts new_record

      do
        let name = PathMod.elem_to_key holder.gh_name
        Logging.trace "Add_child %s  added_vector_parts=%A"
          (PathMod.append cont_path holder.gh_name |> PathMod.to_string)
          (List.map (fun cr -> cr.raw_container) added_vector_parts)

      new_record::added_vector_parts

    let rec add_parts_to_children (cur_recs : GenObj_Record list)
              : GenObj_Record list =
      let mutable yield_list = []
      do
        for cur_rec in cur_recs do
          let cont_geo = pr_get_GenObj cur_rec.gen_obj
          Logging.trace "add_parts_to_children start %s %A   yield_list lentth=%d"
                         cur_rec.raw_container cur_rec.name (List.length yield_list)
          for ch in cur_rec.children do
            let ch_rec = ch.Value
            if ch_rec.parent <> unknown_parent then
              ch_rec.gen_obj <- make_part cont_geo ch_rec.name ch_rec.parent |> PR_Conf
              let added_vps = ch_rec::(add_VectorParts ch_rec)
              yield_list <- List.append added_vps yield_list
              yield_list <- add_parts_to_children added_vps |> List.append yield_list
          Logging.trace "add_parts_to_children after %s %A   yield_list lentth=%d"
                         cur_rec.raw_container cur_rec.name (List.length yield_list)
      yield_list

    //
    // Check to see if the container exists:
    //   if there is a GenObj_Record it may have been created with
    //       add_VectorParts so check_and_complete_vector_part is called
    //       to see if gen_obj is valid or, if parent <> unknown_parent
    //       if it cant be created.
    //       The GenObj_Record is returned
    //   otherwise if there is a GenericObject create the GenObj_Record
    //       and return it.
    // Otherwise if only the last element of the path is missing and
    // it ends in "of", "data", or a couple of other cases and then we
    // create it.
    //
    let rec viable_container (p : Path) =
      let cont = getContainer p
      match getContainer p with
      | R_Exists gen_rec ->
         do
           check_and_complete_vector_part gen_rec
         Some gen_rec
      | R_GEO (cont_rec_A, pl_A) ->
        let (cont_rec, pl) = makeBaseRecord cont_rec_A pl_A
        if pl.Length = 0 then
          Some cont_rec
        elif pl.Length = 1 then
          let elem = List.head pl
          let path = Path pl
          let raw_cont = append_raw cont_rec elem
          if elem = Pstr "of" then
            let cont_geo = pr_get_GenObj cont_rec.gen_obj
            let geo = make_part cont_geo elem c_CASL.GEO |> PR_Conf
            mkContainer cont_rec raw_cont elem c_CASL.GEO geo (Map []) []
              true false "viable_container of" |> Some
          elif elem = Pstr "data" then
            let cont_geo = pr_get_GenObj cont_rec.gen_obj
            let geo = make_part cont_geo elem cont_geo |> PR_Conf
            mkContainer cont_rec raw_cont elem cont_geo geo (Map []) []
              true false "viable_container data" |> Some
          elif elem = Pstr "scandata_usages" &&
               (PathMod.to_string p).IndexOf "collegeamt" > 2 then
            let cont_geo = pr_get_GenObj cont_rec.gen_obj
            let geo = make_part cont_geo elem cont_geo |> PR_Conf
            mkContainer cont_rec raw_cont elem cont_geo geo (Map []) []
              true false "viable_container scandata_usages" |> Some
          elif elem = Pstr "Report" then
            let cont_geo = pr_get_GenObj cont_rec.gen_obj
            let geo = make_part cont_geo elem c_CASL.GEO |> PR_Conf
            mkContainer cont_rec raw_cont elem cont_geo geo (Map []) []
              true false "viable_container Report" |> Some
          else
            None
        else
          None

    and
      check_and_complete_vector_part cur_rec =
      match cur_rec.gen_obj with
      | PR_Unkn ->
        if cur_rec.parent <> unknown_parent then
          match viable_container cur_rec.container with
          | Some cont_rec ->
            match cont_rec.gen_obj with
            | PR_Base cont_geo | PR_Conf cont_geo ->
              cur_rec.gen_obj <-
                make_part cont_geo cur_rec.name cur_rec.parent |> PR_Conf
            | _ -> ()
          | None ->
            Logging.error "check_and_complete_vector_part totally broken %s"
               cur_rec.raw_container
      | PR_Base _ | PR_Conf _ -> ()
      | PR_Obj _ ->
        Logging.error "check_and_complete_vector_part got PR_Obj %s"
          cur_rec.raw_container


    let merge_record
          (cur_rec : GenObj_Record)
          (holder : GenObj_Holder) =
      let
        parent_opt = field_folder cur_rec holder.gh_fields
      do 
        for err in holder.gh_errors do
          add_error cur_rec err

        Logging.trace "merge_record %s '%s' with _parent = %A"
          holder.gh_raw_cont cur_rec.raw_container parent_opt

      let yield_list =
        match parent_opt with
        | Some parent_tree ->
          let (parent, errs) =
            resolve_class_path cur_rec.raw_container parent_tree
          do
            for er in errs do
              add_error cur_rec er
          match getContainer cur_rec.container with
          | R_Exists cont_rec ->
            do
              check_and_complete_vector_part cont_rec
            match cont_rec.gen_obj with
            | PR_Base geo | PR_Conf geo ->
              do
                cur_rec.gen_obj <- make_part geo holder.gh_name parent |> PR_Conf
              let new_records = cur_rec::(add_VectorParts cur_rec)
              List.append new_records (add_parts_to_children new_records)
            | _ ->
              cur_rec.parent <- parent
              []
          | R_GEO _ ->
            Logging.error "merge_record: we are sooo totally broken... %s %s : %A"
              cur_rec.raw_container holder.gh_raw_cont cur_rec.container
            []
        | None ->
          []
      do 
        Logging.trace "merge_record %s yield_list length %d"
          cur_rec.raw_container (List.length yield_list)
      yield_list



    let Builder (src : seq<GenObj_Holder>) : seq<GenObj_Record> =
      let backlog = new Dictionary<int, GenObj_Holder list>()
      let mutable max_backlog_len = 0
      let incomplete = new Dictionary<string, GenObj_Record>()
      let complete (cr : GenObj_Record) where =
        match cr.gen_obj with
        | PR_Base _ | PR_Conf _ ->
          let removed =
            if incomplete.Remove cr.raw_container then
              if incomplete.ContainsKey cr.raw_container then
                Logging.error "complete: incomplete totally bogus"
              "removed"
            else
              ""
          Logging.trace "complete: yielding %s from %s %s %d"
            cr.raw_container where removed incomplete.Count
          true
        | PR_Obj _ ->
          Logging.error "complete got PR_Obj %s from %s %A" cr.raw_container
            where cr.name
          false
        | PR_Unkn ->
          let red_add =
            if incomplete.ContainsKey cr.raw_container then
              "redundant add"
            else
              do
                incomplete.Add (cr.raw_container, cr)
              "add"
          Logging.trace "complete: %s %s %A from %s %d" red_add cr.raw_container
            cr.name where incomplete.Count
          false

      do
        backlog.Add(0, [])

      let process_backlog (n : int) =
        // Logging.trace "GenObjMod.Builder.process_backlog %d" n
        if backlog.ContainsKey n then
          let mutable new_records = []
          let mutable hold_for_later = []
          do
            for hold in backlog.[n] do
              match viable_container hold.gh_container with
              | Some cont_rec ->
                let added_records = Add_child cont_rec hold
                new_records <- List.append added_records new_records
              | None ->
                hold_for_later <- hold::hold_for_later
          
            backlog.[n] <- hold_for_later
          new_records
        else
          []
        
      seq {
        use e = src.GetEnumerator ()

        while e.MoveNext () do
          let cur_hold = e.Current
          do
            Logging.trace "GenObjMod.Builder: holder %s.%s"
              (PathMod.to_string cur_hold.gh_container) (PathMod.elem_to_string cur_hold.gh_name)

          let cur_path = PathMod.append cur_hold.gh_container cur_hold.gh_name
          let cur_result = getContainer cur_path
          match cur_result with
            //
            // 2nd and subsequent GehObj_Holder for single GenericObject,
            // GenObj_Record exists so just merge new information.
            //
          | R_Exists cur_rec ->
            do
              Logging.trace "GenObjMod.Builder: record exists %s %A"
                cur_rec.raw_container cur_rec.name
            let yield_list = merge_record cur_rec cur_hold
              
            for child_rec in yield_list do
              if complete child_rec "merge_record" then
                yield child_rec

          | R_GEO _ ->
            let cont_opt = viable_container cur_hold.gh_container
#if DEBUG_X
            do
              Logging.trace "GenObjMod.Builder: viable_container %A" cont_opt
#endif

            let n = PathMod.length cur_hold.gh_container

            match cont_opt with
            | None ->
              do
                while n > max_backlog_len do
                  max_backlog_len <- max_backlog_len + 1
                  backlog.Add(max_backlog_len, [])
                backlog.[n] <- cur_hold :: backlog.[n]
#if DEBUG_X
              do
                Logging.trace "GenObjMod.Builder: add backlog %s.%s   max_backlog=%d"
                  (PathMod.to_string cur_hold.gh_container)
                  (PathMod.elem_to_string cur_hold.gh_name)
                  max_backlog_len
#endif

            | Some cont_rec ->
              let mutable new_records = Add_child cont_rec cur_hold
              let mutable level = n
              let Logger (c_r : GenObj_Record) where =
                do
#if DEBUG_X
                  Logging.trace "GenObjMod.Builder: new GenObj_Record %s %s.%s"
                    where
                    (PathMod.to_string c_r.container)
                    (PathMod.elem_to_string c_r.name)
#else
                  ()
#endif
              for cur_rec in new_records do
                if complete cur_rec "Add_child" then
                  yield cur_rec

              //
              // If at any point add_VectorParts (called from Add_child) does
              // add a part we need to go down an additional level befoer we
              // conclude there is no more useful work to do, hence the funny
              // test for additional new_records in the middle of the loop.
              //
              while List.length new_records > 0 && level < max_backlog_len do
                level <- level + 1
                new_records <- process_backlog level
                if List.length new_records = 0 then
                  new_records <- process_backlog (level + 1)
                for cur_rec in new_records do
                  if complete cur_rec "process_backlog" then
                    yield cur_rec

        Logging.trace "Builder: dump backlog = %A" backlog
        Logging.trace "Builder: dump incomplete"
        for kvp in incomplete do
          let gen_obj =
              match kvp.Value.gen_obj with
              | PR_Base geo | PR_Conf geo ->
                geo.get_path_to_me
              | PR_Obj obj -> "PR_Obj"
              | PR_Unkn -> "PR_Unkn"
          let parent =
            if kvp.Value.parent = null then
              "null"
            else kvp.Value.parent.get_path_to_me
          Logging.trace "    %s  parent=%s  gen_obj=%s" kvp.Key
              parent gen_obj
          
      }


    //
    // Implement a breath first traversal processing objects
    // closer to the root (GEO) first.  GEO itself is not
    // part of the sequence which is why the inner function trav
    // does not yield geo_rec.
    //
#if FIGURE_OUT_WHY_NOT_WORKING
    let traverse_GenObj_Table =
      do
        Logging.trace "traverse_GenObj_Table"
      let concat2 src1 src2 =
        seq {
          yield! src1
          yield! src2
          }

      let rec trav (geo_rec : GenObj_Record) : seq<GenObj_Record> =
        let
           kids = geo_rec.children.Values |> Seq.cast<GenObj_Record>
                                    |> Seq.toList
        do
          Logging.trace "traverse_GenObj_Table.trav length=%d" kids.Length
        let
          folder (a : seq<GenObj_Record>) (b : GenObj_Record) =
            concat2 a (trav b)
        if kids.Length > 0 then
          List.fold folder (Seq.ofList kids) kids
        else
          Seq.empty
              
#if old
        if geo_recs.Length > 0 then
          concat2
            (Seq.ofList geo_recs)
            (List.fold (fun a b -> concat2 a (trav b))) geo_recs
        else

        match gen_recs with
        | [] -> Seq.empty ;;
        | hd:tl ->
          let
          ch_list = geo_rec.children.Values |> Seq.cast<GenObj_Record>
                    |> Seq.toList
        seq {
          yield! ch_list ;

          for child in ch_list do
            yield! (trav child)          

        }
#endif
      trav GEO
#endif

    let traverse_table (action : GenObj_Record -> unit) =
      do
        Logging.trace "Start of traverse_table"
      let
        queue = new Queue<GenObj_Record>(2048)
      let mutable rec_cnt = 0
      let mutable action_cnt = 0
      do
        queue.Enqueue GEO
        while queue.Count > 0 do
          let
            go_rec = queue.Dequeue ()
          rec_cnt <- rec_cnt+1
          if go_rec.from_holder then
            action_cnt <- action_cnt + 1
            Logging.trace "travers_table action #%d/%d %d %s %A"
              action_cnt rec_cnt queue.Count go_rec.raw_container go_rec.name
            action go_rec
          else
            Logging.trace "travers_table skipping #%d/%d %d %s %A"
              action_cnt rec_cnt queue.Count go_rec.raw_container go_rec.name
          for r in go_rec.children.Values do
            queue.Enqueue r
      do
        Logging.trace "End of travers_table %d/%d"
            action_cnt rec_cnt 



    type to_path_result =
      | ContainerOnly
      | ContainerParent
      | Failed

    let to_path_is_good (geo : GenericObject) =
      let rec to_path (geo : GenericObject) =
        if geo = (c_CASL.GEO :> GenericObject) then
          ContainerOnly
        else
          let cont_chk =
            let
              g = geo_get geo e_container null false
            if g = null then
              Failed
            elif is_GenObj g then
               to_path (g :?> GenericObject)
            else
              do
                Logging.error "Found _container not a GenericObject %s"
                  geo.get_path_to_me
              Failed
          if cont_chk = Failed then
            let
              g = geo_get geo parent_key null false
            if g = null then
              Failed
            elif is_GenObj g then
              if Failed = to_path (g :?> GenericObject) then
                Failed
              else
                ContainerParent
            else
              do
                Logging.error "Found _container not a GenericObject %s"
                  geo.get_path_to_me
              Failed
          else
            cont_chk
      let res = to_path geo
      do
        if res = ContainerParent then
          Logging.trace "To_Path() which needs _parent %s"
            geo.get_path_to_me
      res

#if DEBUG
    let dump_table _ =
      do
        Logging.trace "Start of dump_table"
    
      let rec dump_record (indent : string) (geo_rec : GenObj_Record) =
        let
          field_indent = indent + "     "
        let
          show_field (field : GenObj_Field) =
            let value =
              match field.value with
              | BT_Null -> "null"
              | BT_String s -> "\"" + s + "\""
              | BT_Boolean b -> sprintf "%A" b
              | BT_Int i -> sprintf "%A" i
              | BT_Decimal d -> sprintf "%A" d
              | BT_Path p -> PathMod.to_string p |> sprintf "Path %s"
              | BT_Symbol sym -> sprintf "Symbol %s" sym
              | BT_Vector el -> List.length el |> sprintf "Vector length=%d" 
              | BT_OrderedM _ -> "Ordered Multiple"
              | BT_Multiple _ -> "Multiple"
              | BT_Error msg   -> "Error: " + msg
              | BT_GEO _       -> "GEO"
              | BT_VectorPart (exts , kids) ->
                  let exts_len = List.length exts
                  let kids_len = List.length kids
                  sprintf "VectorPart exts_length=%d kids_length=%d"
                    exts_len kids_len
            Logging.trace "%s%A -> %s" field_indent field.name value
              
        do
          let geo_s =
            match geo_rec.gen_obj with
            | PR_Base geo ->
              "PR_Base " + geo.get_path_to_me
            | PR_Conf geo ->
              "PR_Conf " + geo.get_path_to_me
            | PR_Obj o -> " * TOTALLY BOGUS * "
            | PR_Unkn  -> " * Why was this missed!! * "
          Logging.trace "%s%s {%s}" indent geo_rec.raw_container geo_s
          geo_rec.fields.Values |> Seq.cast<GenObj_Field>
            |> Seq.iter show_field
        do
          geo_rec.children.Values |> Seq.cast<GenObj_Record>
              |> Seq.iter (dump_record (indent + "  "))
      dump_record "" GEO
#endif
