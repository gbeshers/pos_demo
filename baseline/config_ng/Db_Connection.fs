﻿namespace SQL_config_NS

//
// This file simply converts the
//    TranSuite_DB.Config.db_connection_info.casl
// into the Db_Connection_Info structure.
//
// Db_Connection_Info is likely to be enhanced some to support
// postgress.
//


open System
open System.IO
open System.Security.Cryptography
open System.Text
open System.Text.RegularExpressions


type Db_Connection_Info =
  { database_name : string
    datasource_name : string
    login_name : string
    login_password : string }

module Conv_Crypto =
  let tripleDesIV : byte [] = [| 0x01uy; 0x02uy; 0x03uy; 0x04uy; 0x05uy; 0x06uy;
 0x07uy; 0x08uy;
                                 0x09uy; 0x10uy; 0x11uy; 0x12uy; 0x13uy; 0x14uy;
 0x15uy; 0x16uy |]
  let secret_key = Convert.FromBase64String "Ye1/fHWFsvZRhxFj9AjONIzeen85v+fO"

  let symmetric_decrypt password =
    let pProvider = new TripleDESCryptoServiceProvider ()
    let pTranform = pProvider.CreateDecryptor(secret_key, tripleDesIV)
    let decryptStream = new MemoryStream()
    let cryptStream =
        new CryptoStream(decryptStream, pTranform,
                         CryptoStreamMode.Write)
    let arrDataBytes = Convert.FromBase64String password
    let _ = cryptStream.Write(arrDataBytes, 0, arrDataBytes.Length)
    let _ = cryptStream.FlushFinalBlock()
    let _ = decryptStream.Position <- 0L
    let res = decryptStream.ToArray()
    let _ = decryptStream.Close()
    Encoding.ASCII.GetString res



module Db_Connect =

  exception Db_connection_failure of string

  let readLines (fn : string) = seq {
      use sr = new StreamReader(fn)
      while not sr.EndOfStream do
          yield sr.ReadLine ()
    }

  let read_info file_name =
    let file_str = Seq.fold (fun (a : string) b -> a + b) "" (readLines file_name)

    let get_string_for key =
      let reg_string = key + "\s*=\s*\"([^\"]*)\""
      let regex = new Regex (reg_string, RegexOptions.Compiled)
      let m = regex.Match file_str
      if m.Success
        then m.Groups.[1].Captures.[0].Value
        else raise (Db_connection_failure ("database_name not seen in " + file_str))

    {
      database_name = get_string_for "database_name";
      datasource_name = get_string_for "datasource_name";
      login_name = get_string_for "login_name";
      login_password = get_string_for "login_password";
    }
