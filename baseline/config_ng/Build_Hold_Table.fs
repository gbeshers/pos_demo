namespace Config_NG.Flex_NS

open System.Collections.Generic

open SQL_config_NS

open Config_NG.Path_NS

//
// Each GenObj_Holder is a transition state for oonsecutive information in
// the flex table with the identical raw container fields (raw_cont).
// We use this representation because when reading the Flex table rows
// with the same raw container field tend to be groupled together, but we
// allow for a scrambled order.
//
// All the primitive fields are included in the GenObj_Holders but a little
// care is needed with some vector fields which have some redundant information
// with objects define independently in the Flex table.
//


type ErrorKind =
   | Err_None
   | War_Dup_Row
   | Err_Con_Row of string   // The conflict value
   | Err_Bad_Value
   | War_Casl_Value
   | War_Default_Value
   | Err_ParserError of string
   | Err_Par_UnmatchQuote of string
   | Err_parent_conflict of string
   | Err_parent_missing of string
   | Err_parent_not_class of string
   | Err_parent_constructed of string
   | Err_parent_path_not_GenericObject of string
   | Err_parent_value_not_path_or_symbol of string


type FlexError = {
     err_cont : string ;
     err_key : string ;
     err_val : string ;
     err_flag : ErrorKind
   }


//
// The raw_make_part is set only if null, null is seen.  There are
// corner cases where a vector looks like a VectorPart but in fact
// has no associated parts.  Separating out the null, null cases
// facilitates overriding the VectorPart designation.
//
type Raw_Holder = {
     raw_cont : string ;
     raw_fields : Map<string, string> ;
     raw_make_part : bool ;
     raw_errors : FlexError list
   }


//
// We parse the vast majority of values, this saves litterally
// thousands of calls to the CASLInterpreter.  It also makes it
// easier to validate paths and expedite path lookups for the
// duration of loading Config.
//
// The Basic_Tree is what holds the result of the parse, only
// BT_GEO need to call execute_string().
//
type Basic_Tree =
    | BT_Null
    | BT_String   of string
    | BT_Boolean  of bool
    | BT_Int      of int
    | BT_Decimal  of decimal
    | BT_Path     of Path
    | BT_Symbol   of string
    | BT_Vector   of Basic_Tree list
    | BT_OrderedM of Path[]
    | BT_Multiple of Path[]
    | BT_Error    of string
    | BT_GEO      of string
    | BT_VectorPart of externs : Path list * children : Path list
               // A path part with _parent=vector, see
               // filter_container_vector
//    | BT_Dict     of Map<string, Basic_Tree>  // not currently used. GMB.

type GenObj_Field = {
      name : PathElem ;
      value : Basic_Tree
    }

type GenObj_Holder = {
     gh_raw_cont : string ;
     gh_raw_fields : Map<string, string> ;
     gh_container : Path ;
     gh_name : PathElem ;
     gh_make_part : bool ;
     gh_fields : Map<string, GenObj_Field> ;
     gh_errors : FlexError list
   }

//
// We need to map Path's, the container strings in particular, to
// their respective GenericObjects.  This is the responsibility of
// the hold_table.  The HoldTable also is the transition data structure
// between the Flex table and the parts that are defined in CASL.
//
// In constructing the HoldTable we rebase the tree if desired
// (currently, and probably forever, this is a Q/A strategy).  We
// also do some checking and weed out invalid entries.
//
// NOTE: HoldTable is a stateful construct which we update for each row
// and later during the construction of the part hierarchy.
//


module Flex =

    //
    // The tree of Config objects needs to built from GEO downwards
    // to the leaves, so we sort by the container's path length.
    //
    let holder_depth (h : GenObj_Holder) = PathMod.length h.gh_container


    //
    // Rows tend to come in from the database grouped by the container string, e.g.,
    // all the rows for
    //
    //    Business.User.of.cashier
    //
    // will come together with the null, null row being the last in the sequence.
    // Rows_to_Holder simply groups the key, value pairs for a specific container
    // string into a "Raw_Holder".  The sequence of Flex_row(s) becomes a shorter
    // sequence of Raw_Holders(s).
    //
    // Note that the null, null rows are removed --- if a holder exists we either
    // use the existing GenericObject or create a new one.  See GenObjRecord.Builder.
    //
    let Rows_to_Holder (src : seq<Flex_row>) : seq<Raw_Holder> =
      seq {
        use e = src.GetEnumerator()

        if e.MoveNext () then
          let first_row = e.Current
          let mutable container = first_row.container
          let mutable fields = Map.empty
          let mutable make_part = false
          let mutable errors = []

          let add_field row =
            if row.field_key = "null" then
              make_part <- true
            else
              let key = PathMod.strip_end_quotes row.field_key
              if fields.ContainsKey key then
                if fields.[key] = row.field_value then
                  errors <- {
                    err_cont = container ;
                    err_key = key ;
                    err_val = row.field_value ;
                    err_flag = War_Dup_Row }::errors
                else
                  errors <- {
                    err_cont = container ;
                    err_key = key ;
                    err_val = row.field_value ;
                    err_flag = Err_Con_Row fields.[key] }::errors
              else
               fields <- fields.Add (key, row.field_value)

          do
            add_field first_row
          
          while e.MoveNext () do
            let row = e.Current
            if container = row.container then
              add_field row
            else
              yield { raw_cont = container ;
                      raw_fields = fields ;
                      raw_make_part = make_part ;
                      raw_errors = errors }
              do
                container <- row.container
                fields <- Map.empty
                make_part <- false
                errors <- []

                add_field row

          yield { raw_cont = container ;
                  raw_fields = fields ;
                  raw_make_part = make_part ;
                  raw_errors = errors }
        }