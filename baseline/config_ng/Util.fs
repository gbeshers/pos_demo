
namespace Util_NS

//
//    This file contains "globals" in that they are types or objects
// which are ubiquidous and are not complicated enough to justify
// a separate module.
//

// NOTE: After testing multiple configs integrate the error_file
// into the cs_log() output.  Perhaps the trace_file becomes part
// of the flight_recorder.  GMB.

open System
open System.IO
open System.Text
open System.Diagnostics

open CASL_engine   // misc.CASL_get_code_base()

module Logging =
    let date_time = DateTime.Now.ToString("yyyy_MM_dd-HH_mm_ss")
    let site_directory = misc.CASL_get_code_base()
    let timer = new Stopwatch();
#if DEBUG
    let trace_file = File.CreateText (site_directory + "/log/trace_file_" +
                                      date_time + ".txt")
    do trace_file.AutoFlush <- true
    let in_trace_file = File.CreateText (site_directory + "/log/in_trace_file_" +
                                      date_time + ".txt")
    do in_trace_file.AutoFlush <- true

    let trace fmt = fprintfn trace_file fmt
    let trace_flush = trace_file.Flush
    let in_trace fmt = fprintfn in_trace_file fmt
    let in_trace_flush = in_trace_file.Flush
#else

    let trace fmt =
      let do_not_print (s : string) = ()
      Printf.ksprintf do_not_print fmt

    let trace_flush = ignore

    let in_trace fmt =
      let do_not_print (s : string) = ()
      Printf.ksprintf do_not_print fmt
    let in_trace_flush = ignore
#endif


    let error_file = File.CreateText (site_directory + "/log/error_file_" +
                                      date_time + ".txt")
    do error_file.AutoFlush <- true
    let error_flush = error_file.Flush

    let error fmt =
      let print_both (s : string) =
#if DEBUG
        do
          fprintfn trace_file "%s" s
#endif
        fprintfn error_file "%s" s
      Printf.ksprintf print_both fmt
