
namespace Config_NG

open System.Collections.Generic

open CASL_engine
open Util_NS

open Config_NG.Path_NS
open Config_NG.Flex_NS
open Config_NG.GenObj_NS

module Multiples =

    type MultFlavor =
      | Stand
      | Order
      | ErrMult

    type MultInfo =
        {
          pri_cl : string ;  // Primary class
          pri_fd : string ;  // primary field (in secondary class)
          sec_cl : string ;  // Secondary class
          sec_fd : string ;  // secondary field (in primary class)
          flavor : MultFlavor
        }

    let vector_class = GenObjMod.vector_class
    let multiple     = GenObjMod.multiple
    let mult_ordered = GenObjMod.mult_ordered

    let geo_set      = GenObjMod.geo_set
    let geo_get      = GenObjMod.geo_get
    let geo_has      = GenObjMod.geo_has

    let e_objects    = GenObjMod.e_objects
    
    
    //
    // Multiples:
    //
    // Construct the Primary and Secondary multiples from the saved text
    // from the Flex Table.  The prelininary stuff is capturing the
    // primary/secondary information
    //
    let Alg = "Allocation_group"
    let Bri = "Bank_rule"
    let Cus = "Custom_field"
    let Dep = "Department"
    let Doc = "Document"
    let Fee = "Fee"
    let Job = "Job"
    let Men = "Menu"
    let Sit = "Security_item"
    let Spr = "Security_profile"
    let Std = "Std_interface_import"
    let Sys = "System_interface"
    let Ten = "Tender"
    let Tcf = "Tender.Check.Check_Foreign"
    let Tch = "Tender.Check"
    let Tax = "Transaction.Tax"
    let Trn = "Transaction"
    let Usr = "User"
    //
    // Longest first primarily so that "Transaction.Tax" is recognized.
    //
    let AllMultClass =
          Array.sortBy (fun (s : string) -> (30 - s.Length))
             [| Alg ; Bri ; Cus ; Dep ; Doc ; Fee ; Job ; Men ; Sit ;
                Spr ; Std ; Sys ; Ten ; Tax ; Trn ; Usr |]

    let multiple_meta_info =
      begin
        let alg = "allocation_group_classes"
        let bri = "bank_rule_items"
        let cus = "custom_fields"
        let dep = "departments"
        let doc = "documents"
        let fee = "fees"
        let job = "jobs"
        let imp = "imports"
        let men = "menus"
        let sit = "security_items"
        let spr = "security_profiles"
        let sys = "system_interfaces"
        let tax = "tax_transactions"
        let tdp = "taxable_departments"
        let txt = "taxable_transactions"
        let ten = "tenders"
        let trn = "transactions"
        let usr = "users"

        //
        // This structure allows for slight variations in class, field names.
        //

        [|
            // Bank_rule, Tender
            { pri_cl = Bri ; pri_fd = bri ; sec_cl = Ten ; sec_fd = ten ; flavor = Stand } ;

            // Deparment, Menu
            { pri_cl = Dep ; pri_fd = dep ; sec_cl = Men ; sec_fd = men ; flavor = Order } ;
            // Deparment, Tender
            { pri_cl = Dep ; pri_fd = dep ; sec_cl = Ten ; sec_fd = ten ; flavor = Order } ;

            // Fee, Tender
            { pri_cl = Fee ; pri_fd = fee ; sec_cl = Ten ; sec_fd = ten ; flavor = Stand } ;

            // Job, Department
            { pri_cl = Job ; pri_fd = job ; sec_cl = Dep ; sec_fd = dep ; flavor = Stand } ;
            // Job, Std_interface_import
            { pri_cl = Job ; pri_fd = job ; sec_cl = Std ; sec_fd = imp ; flavor = Stand } ;
            // Job, System_interface
            { pri_cl = Job ; pri_fd = job ; sec_cl = Sys ; sec_fd = sys ; flavor = Stand } ;

            // Menu, Transaction
            { pri_cl = Men ; pri_fd = men ; sec_cl = Trn ; sec_fd = trn ; flavor = Order } ;

            // Security_profile, Security_item
            { pri_cl = Spr ; pri_fd = spr ; sec_cl = Sit ; sec_fd = sit ; flavor = Stand } ;

            // Tender, Custom_field
            { pri_cl = Ten ; pri_fd = ten ; sec_cl = Cus ; sec_fd = cus ; flavor = Stand } ;
            // Tender, Document
            { pri_cl = Ten ; pri_fd = ten ; sec_cl = Doc ; sec_fd = doc ; flavor = Order } ;
            // Tender, System_interface
            { pri_cl = Ten ; pri_fd = ten ; sec_cl = Sys ; sec_fd = sys ; flavor = Stand } ;


            // Transaction.Tax, Allocation_group
            { pri_cl = Tax ; pri_fd = tax ; sec_cl = Alg ; sec_fd = alg ; flavor = Stand } ;
            // Transaction.Tax, Department
            { pri_cl = Tax ; pri_fd = tax ; sec_cl = Dep ; sec_fd = tdp ; flavor = Stand } ;
            // Transaction.Tax, Transaction
            { pri_cl = Tax ; pri_fd = tax ; sec_cl = Trn ; sec_fd = trn ; flavor = Stand } ;
            // Transaction.Tax, BOGUS??
            { pri_cl = Tax ; pri_fd = tax ; sec_cl = Trn ; sec_fd = txt ; flavor = Stand } ;
            // Transaction.Tax, System_interface
            { pri_cl = Tax ; pri_fd = tax ; sec_cl = Sys ; sec_fd = sys ; flavor = Stand } ;

            // Transaction, Allocation_group
            { pri_cl = Trn ; pri_fd = trn ; sec_cl = Alg ; sec_fd = alg ; flavor = Stand } ;
            // Transaction, Custom_field
            { pri_cl = Trn ; pri_fd = trn ; sec_cl = Cus ; sec_fd = cus ; flavor = Stand } ;
            // Transaction, Department
            { pri_cl = Trn ; pri_fd = trn ; sec_cl = Dep ; sec_fd = dep ; flavor = Stand } ;
            // Transaction, Document
            { pri_cl = Trn ; pri_fd = trn ; sec_cl = Doc ; sec_fd = doc ; flavor = Order } ;
            // Transaction, Document
            { pri_cl = Trn ; pri_fd = trn ; sec_cl = Men ; sec_fd = men ; flavor = Stand } ;
            // Transaction, System_interface
            { pri_cl = Trn ; pri_fd = trn ; sec_cl = Sys ; sec_fd = sys ; flavor = Stand } ;

            // User, Department
            { pri_cl = Usr ; pri_fd = usr ; sec_cl = Dep ; sec_fd = dep ; flavor = Stand }
        |]
      end


    let find_mult_pri =
      let
        loc_add (m : Map<string, Map<string, MultInfo>>) v =
          let pk = v.pri_cl
          let sk = v.sec_fd
          if m.ContainsKey pk then
            m.Add(pk, m.[pk].Add (sk, v))
          else
            m.Add(pk, Map.empty.Add (sk, v))
      Array.fold loc_add (Map.empty : Map<string, Map<string, MultInfo>>) multiple_meta_info

    let find_mult_sec =
      let
        loc_add (m : Map<string, Map<string, MultInfo>>) v =
          let pk = v.sec_cl
          let sk = v.pri_fd
          if m.ContainsKey pk then
            m.Add(pk, m.[pk].Add (sk, v))
          else
            m.Add(pk, Map.empty.Add (sk, v))
      Array.fold loc_add (Map.empty : Map<string, Map<string, MultInfo>>) multiple_meta_info

    //
    // With multiples we have to be careful as sometimes they are in the flex table as parts,
    // sometimes we create the multiple in init_part to avoid calling CASL, and other
    // times we need to allocate the multiple and _objects vector here.
    //
    let find_or_make container cont_path name parent set_container =
      match PathMod.append cont_path name |> GenObjMod.path_to_GenObj with
      | PR_Base geo -> geo
      | PR_Conf geo -> geo
      | PR_Obj o ->
        do
          Logging.error "find_or_make: found object %A, overwriting" o
        let geo = GenObjMod.create_GenObj parent
        do
          GenObjMod.geo_set container name geo
        geo
      | PR_Unkn ->
        let geo =
          if set_container then
            GenObjMod.make_part container name parent
          else
            GenObjMod.create_GenObj parent
        geo


    //
    // IPAY-84: Verify inverse multiples, particularly documents
    // for Tender and Transaction. CHECK ME.  GMB.
    //
    type add_to_db_orig_record =
      { cont : GenericObject ;
        cont_path : Path ;
        key : PathElem ;
        mult : GenericObject ;
      }
    let inverse_multiples = new HashSet<add_to_db_orig_record> ()

    //
    // Create a multiple.
    //    path - is the container path [ path + "." + name ] becomes the
    //           path of the multiple.
    //    container : the GenericObject
    //    name : container.get(name) will return the multiple on completion of
    //           this function.
    //    mt : the paths of the objects in the multple
    //    ordered : boolean, true if it is an ordered multiple.
    //
    let mkMultiple path container (name : PathElem) (mt : Path[]) ordered =
      begin
        let mult_path = PathMod.append path name
        let par = if ordered then mult_ordered else multiple
        let mult = find_or_make container path name par true
        let objs = find_or_make mult mult_path (Pstr "_objects") vector_class false
        let match_geo path = (path, GenObjMod.path_to_GenObj path)
        let identify_dangling_paths (p : Path, p_res : PathResult) =
          match p_res with
          | PR_Base geo -> true
          | PR_Conf geo -> true
          | PR_Obj o ->
            let
              obj_path = GenObjMod.get_GenObj path
            do
              if obj_path <> null then
                obj_path.set("_force_save", null) |> ignore
              Logging.error "Dropping multiple element %s from %s %s '%A'"
                (PathMod.to_string p) (PathMod.to_string path)
                "becaue it points to the object " o
            false
          | PR_Unkn ->
            let
              obj_path = GenObjMod.get_GenObj path
            do
              if obj_path <> null then
                obj_path.set("_force_save", null) |> ignore
              Logging.error "Dropping multiple element %s from %s"
                (PathMod.to_string p) (PathMod.to_string path)
            false
        let strip_base_conf (p : Path, pr : PathResult) =
          match pr with
          | PR_Base geo -> (p, geo)
          | PR_Conf geo -> (p, geo)
          | PR_Obj o ->
            do
              Logging.error "**** Never should happen %s from %s"
                (PathMod.to_string p) (PathMod.to_string path)
            (p, null)
          | PR_Unkn ->
            do
              Logging.error "**** Never should happen %s from %s"
                (PathMod.to_string p) (PathMod.to_string path)
            (p, null)

        //
        // IPAY-84: FIXME ??
        // Does this really work for different Document types?
        //
        let mult_category (Path elem_list as mc_path) =
          do
            Logging.trace "mult_category %A" elem_list

          let elem_str i =
            let
              el = elem_list.[i]
            match el with
            | Pstr s -> s
            | _ ->
              failwith (sprintf "mult_category expected Pstr %s (%A)"
                (PathMod.to_string path) el)

          // path.[0] = "Business" or "Validate_Copy"
          let cn0 = elem_str 1

          // Check on Document
          let cn =
            if cn0 = "Transaction" && PathMod.length mc_path > 2 &&
                 (elem_str 2) = "Tax"
              then "Transaction.Tax"
              else cn0

          match find_mult_pri.TryFind cn with
          | Some mi -> mi
          | None -> failwith ("Non existent multiple primary class " + cn)

        let add_path i (p : Path, o : GenericObject) =
          do
            objs.set(i, o) |> ignore
            mult.set(o, o) |> ignore

            let mim = mult_category path
            let {pri_cl = pc; pri_fd = pf; sec_cl = sc; sec_fd = sf; flavor = fl} =
              mim.[PathMod.elem_to_string name]
            do
              if not (o.has pf) ||
                  ( let pfx = o.get pf
                    match pfx with
                    | :? GenObj_Class as pf_gec -> (pf_gec = c_CASL.c_opt) ||
                                                   (pf_gec = c_CASL.c_req)
                    | :? GenericObject as pf_geo ->
                      if pf_geo.has "_parent" then
                        match pf_geo.get "_parent" with
                        | :? GenericObject as p_geo -> (p_geo <> multiple) && (p_geo <> mult_ordered)
                        | _ ->
                          Logging.error "mkMultiple.add_path: %s %s"
                            "Expected multiple but found a GenericObject"
                            "with a non GenericObject parent!!!"
                          true
                      else
                        Logging.error "mkMultiple.add_path: %s"
                          "Expected multiple but found a GenericObject without _parent."
                        true
                    | _ ->
                      Logging.error
                        "mkMultiple:add_path '%s' primary field '%s' exists but is not a GenericObject --- '%A'"
                           (PathMod.to_string p) pf pfx
                      true
                   ) then
                let mult_path = PathMod.append_key p pf
                let obj_path = PathMod.append_key mult_path "_objects"
                let sec_container = GenObjMod.get_GenObj p
                let sec_mult = find_or_make sec_container p (Pstr pf) multiple true
                let objects = find_or_make sec_mult mult_path (Pstr "_objects") vector_class
                                          false
                geo_set sec_mult e_objects objects
                sec_mult.set("_name", pf) |> ignore
                o.set(pf, sec_mult) |> ignore
// IPAY-84: HERE -- validate sec_container.  Attention Tender -> Doc, Trans -> Doc.
                inverse_multiples.Add { cont = sec_container ; cont_path = p ; key = Pstr pf ; mult = sec_mult } |> ignore
            let s_mult = o.get(pf, false, true) :?> GenericObject
            let s_objs = s_mult.get("_objects", false, true) :?> GenericObject
            let nix = s_objs.getLength ()
            s_objs.set(nix, container) |> ignore
            s_mult.set(container, container) |> ignore

        do
          mult.set("_objects", objs) |> ignore
          mult.set("_name", GenObjMod.caslize name) |> ignore
          // init_db mult mult_path
          let ap = Array.map match_geo mt
          let ag = Array.filter identify_dangling_paths ap
                   |> Array.map strip_base_conf
          Array.iteri add_path ag
      end

    //
    // This function creates the function init_part used below.
    // The role of this function is to cache pointers to a handful
    // of CASL classes that require empty multiples to be
    // constructed --- I've never figured out why some and not
    // others.
    //
    // So, we use partial application and return init_part as
    // the result.
    //

    let init_part_with_rebase rebase_path =
      begin
        let lookup path =
          match GenObjMod.path_to_GenObj path with
          | PR_Base x -> x
          | PR_Conf x -> x
          | PR_Obj o ->
            do
              Logging.error "init_part.lookup %A is not a GenericObject %A"
                path o
            failwith ("init_Part.lookup found non GenericObject '" +
                      (PathMod.to_string path) + "'")
          | PR_Unkn ->
            do
              Logging.error "init_part.lookup %A not found" path
            failwith ("init_Part.lookup failed '" +
                      (PathMod.to_string path) + "'")

        let parent_Business_Allocation_Group =
          PathMod.mkPath "Business.Allocation_group" |> rebase_path |> lookup
        let parent_Business_Bank_rule_item =
          PathMod.mkPath "Business.Bank_rule_item" |> rebase_path |> lookup
        let parent_Business_Custom_field =
          PathMod.mkPath "Business.Custom_field" |> rebase_path |> lookup

        let parent_Business_Department =
          PathMod.mkPath "Business.Department" |> rebase_path |> lookup
        let parent_Business_Fee =
          PathMod.mkPath "Business.Fee" |> rebase_path |> lookup

        let parent_Business_Job =
          PathMod.mkPath "Business.Job" |> rebase_path |> lookup
        let parent_Business_Menu =
          PathMod.mkPath "Business.Menu" |> rebase_path |> lookup

        let parent_Business_Security_item =
          PathMod.mkPath "Business.Security_item" |> rebase_path |> lookup
        let parent_Business_Security_profile =
          PathMod.mkPath "Business.Security_profile" |> rebase_path |> lookup

        let parent_Business_Std_interface =
          PathMod.mkPath "Business.Std_interface_import" |> rebase_path |> lookup
        let parent_System_interface_Web =
          PathMod.mkPath "Business.System_interface.Web" |> rebase_path |> lookup

        let parent_Business_Tender =
          PathMod.mkPath "Business.Tender" |> rebase_path |> lookup
        let parent_Business_Tender_Check =
          PathMod.mkPath "Business.Tender.Check" |> rebase_path |> lookup
        let parent_Business_Tender_Check_Foreign =
          PathMod.mkPath "Business.Tender.Check.Check_Foreign" |> rebase_path |> lookup

        let parent_Business_Transaction =
          PathMod.mkPath "Business.Transaction" |> rebase_path |> lookup
        let parent_Business_Transaction_Tax =
          PathMod.mkPath "Business.Transaction.Tax" |> rebase_path |> lookup
        let parent_Business_User =
          PathMod.mkPath "Business.User" |> rebase_path |> lookup

        let parent_Business_Document_Barcode =
          PathMod.mkPath "Business.Document.Barcode" |> rebase_path |> lookup
        let parent_Business_Document_Barcode_variable =
          PathMod.mkPath "Business.Document.Barcode_variable" |> rebase_path |> lookup
        let parent_Business_Document_CopyPaste =
          PathMod.mkPath "Business.Document.CopyPaste" |> rebase_path |> lookup
        let parent_Business_Document_DIGITALCHECK =
          PathMod.mkPath "Business.Document.DIGITALCHECK" |> rebase_path |> lookup
        let parent_Business_Document_Epson =
          PathMod.mkPath "Business.Document.Epson" |> rebase_path |> lookup
        let parent_Business_Document_File =
          PathMod.mkPath "Business.Document.File" |> rebase_path |> lookup
        let parent_Business_Document_Ingenico =
          PathMod.mkPath "Business.Document.Ingenico" |> rebase_path |> lookup
        let parent_Business_Document_MiraServ =
          PathMod.mkPath "Business.Document.MiraServ" |> rebase_path |> lookup
        let parent_Business_Document_PANINI =
          PathMod.mkPath "Business.Document.PANINI" |> rebase_path |> lookup
        let parent_Business_Document_PERTECHSCANNER =
          PathMod.mkPath "Business.Document.PERTECHSCANNER" |> rebase_path |> lookup
        let parent_Business_Document_RDM =
          PathMod.mkPath "Business.Document.RDM" |> rebase_path |> lookup
        let parent_Business_Document_TWAIN =
          PathMod.mkPath "Business.Document.TWAIN" |> rebase_path |> lookup
        let parent_Business_Document_ValidationStamp =
          PathMod.mkPath "Business.Document.ValidationStamp" |> rebase_path |> lookup
        let parent_Business_Document_VeriFone =
          PathMod.mkPath "Business.Document.VeriFone" |> rebase_path |> lookup
        let parent_Business_Document_VeriFone_point =
          PathMod.mkPath "Business.Document.VeriFone_point" |> rebase_path |> lookup
        let parent_Business_User =
          PathMod.mkPath "Business.User" |> rebase_path |> lookup

        let business_documents = [
            parent_Business_Document_Barcode;
            parent_Business_Document_Barcode_variable;
            parent_Business_Document_CopyPaste;
            parent_Business_Document_DIGITALCHECK;
            parent_Business_Document_Epson;
            parent_Business_Document_File;
            parent_Business_Document_Ingenico;
            parent_Business_Document_MiraServ;
            parent_Business_Document_PANINI;
            parent_Business_Document_PERTECHSCANNER;
            parent_Business_Document_RDM;
            parent_Business_Document_TWAIN;
            parent_Business_Document_ValidationStamp;
            parent_Business_Document_VeriFone;
            parent_Business_Document_VeriFone_point;
            ]
        let business_document_set =
          new HashSet<GenericObject>(business_documents)

        let add_multiple (go : GenericObject)
             (go_path : Path) (mf : string) (parent : GenericObject) =
          let
            multiple_path = PathMod.append_key go_path mf
          let
            objects_path = PathMod.append_key multiple_path "_objects"
          if not (go.has mf) then
            let sec_mult = GenObjMod.make_part go (Pstr mf) parent
            let objects = GenObjMod.create_GenObj vector_class
            geo_set sec_mult e_objects objects
            // geo_set sec_mult e_name mf
            geo_set go (Pstr mf) sec_mult
            GenObjMod.init_db sec_mult multiple_path |> ignore
            GenObjMod.add_to_db_orig go go_path (Pstr mf) sec_mult "init_part_with_rebase.add_multiple"

        let init_part (part : GenericObject) (part_path : Path) =
          let am = add_multiple part part_path
          let parent =
            match part.get ("_parent", null) with
            | :? GenericObject as p -> p
            | o ->
              let msg = sprintf "init_Part: _parent of %s %s but found %A"
                          (part.safe_to_path ())
                          "is suppose to be a GenericObject"
                          o
              failwith msg
          match parent with
          | _ when parent = parent_Business_Allocation_Group ->
              do
                am "transactions" multiple        // Secondary
          | _ when parent = parent_Business_Bank_rule_item ->
              do
                am "tenders" multiple
          | _ when parent = parent_Business_Custom_field ->
              do
                am "tenders" multiple             // Secondary
                am "transactions" multiple        // Secondary
          | _ when parent = parent_Business_Department ->
              do
                am "jobs" multiple                // Secondary
                am "menus" mult_ordered
                am "tax_transactions" multiple    // Secondary
                am "tenders" mult_ordered
                am "transactions" multiple        // Secondary
                am "users" multiple               // Secondary
          | _ when parent = parent_Business_Fee ->
              do
                am "tenders" multiple
          | _ when parent = parent_Business_Job ->
              do
                am "departments" multiple
                am "imports" multiple
                am "system_interfaces" multiple
          | _ when parent = parent_Business_Menu ->
              do
                am "departments" multiple         // Secondary
                am "transactions" mult_ordered    // Ordered
          | _ when parent = parent_Business_Security_item ->
              do
                am "security_profiles" multiple   // Secondary
          | _ when parent = parent_Business_Security_profile ->
              do
                am "security_items" multiple
          | _ when parent = parent_Business_Std_interface ->
              do
                am "jobs" multiple
          | _ when parent = parent_System_interface_Web ->
              do
                am "tenders" multiple             // Secondary
                am "transactions" multiple        // Secondary
          | _ when parent = parent_Business_Tender ->
              do
                am "bank_rule_items" multiple     // Secondary
                am "custom_fields" multiple
                am "departments" multiple         // Secondary
                am "documents" mult_ordered       // Ordered
                am "fees" multiple                // Secondary
                am "system_interfaces" multiple
// FIX Tender_Check, Tender_Check_Foreign
          | _ when parent = parent_Business_Tender_Check ->
              do
                am "bank_rule_items" multiple
                am "custom_fields" multiple
                am "departments" multiple
                am "fees" multiple
                am "system_interfaces" multiple
          | _ when parent = parent_Business_Tender_Check_Foreign ->
              do
                am "bank_rule_items" multiple
                am "custom_fields" multiple
                am "departments" multiple
                am "documents" multiple
                am "fees" multiple
                am "system_interfaces" multiple
          | _ when parent = parent_Business_Transaction ->
              do
                am "allocation_group_classes" multiple
                am "custom_fields" multiple
                am "departments" multiple
                am "documents" mult_ordered       // Ordered
                am "menus" multiple               // Secondary
                am "system_interfaces" multiple
                am "tax_transactions" multiple    // Secondary
          | _ when parent = parent_Business_Transaction_Tax ->
              do
                am "taxable_departments" multiple
                am "taxable_transactions" multiple
          | _ when parent = parent_Business_User ->
              do
                am "departments" multiple
          | _ when business_document_set.Contains parent ->
              do
                am "tenders" multiple             // Secondary
                am "transactions" multiple        // Secondary
          | _ ->
#if DEBUG_X
              Logging.trace "init_part skipping parent %A" parent.get_path_to_me
#else
              ()
#endif
        init_part
      end


    let synchronize_inverses () =
      let add_db_orig (r : add_to_db_orig_record)  =
        GenObjMod.add_to_db_orig r.cont r.cont_path r.key r.mult
          "Cleanup inverse multiples"

      let inv_mult_logging (x : add_to_db_orig_record) =
        let
          p_str = PathMod.append x.cont_path x.key |> PathMod.to_string
        do
          Logging.trace "Processing inverse multiples %s" p_str
        x
    
      do
        inverse_multiples |> Seq.cast<add_to_db_orig_record>
           |> Seq.map inv_mult_logging
           |> Seq.iter add_db_orig
      ()
