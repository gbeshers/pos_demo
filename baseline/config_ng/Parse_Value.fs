﻿namespace Config_NG

open System
#if DEBUG
open System.Diagnostics;
open Microsoft.FSharp.Reflection
#endif
open System.Text.RegularExpressions
open System.IO

open CASL_engine

open Util_NS

open Config_NG.Path_NS
open Config_NG.Flex_NS

// ToDo: Anchor regexs which requires striping leading spaces.
//       Path regex is a kludge to handle abc."001".xyz



module Parser =

    let default_value_by_key_map =
      Map.empty.
        Add("password_last_changed_on", BT_String "1/1/1970").  // Unix epoch ;)
        Add("previous_passwords", BT_GEO "<GEO/>").          // Change to []
        Add("user_last_login", BT_String "1/1/1970")

    let default_value_by_key key =
      match default_value_by_key_map.TryFind key with
      | Some bt -> bt
      | None -> BT_String ""

#if DEBUG
    let toString (x : Basic_Tree) =
      match FSharpValue.GetUnionFields(x, typeof<Basic_Tree>) with
      | case, _ -> case.Name
#endif

    // If the first and last characters are '"' its a string.
    // Strip the '"' from both ends before returning
    let (|IsString|_|) (s :string) =
      if s.Length < 2 then
        None
      elif s.[0] = '"' && s.[s.Length-1] = '"' then
        Some s.[1..(s.Length - 2)]
      else
        None

    // If the string is just digits possibly with a + or - to start
    // we treat it as an integer
    let int_regex = new Regex ("(\+|-)?\d+", RegexOptions.Compiled)
    let (|RegexInt|_|) s =
      let m = int_regex.Match(s)
      if (m.Success)
        then
          match System.Int32.TryParse s with
          | (true, i) -> Some i
          | _ -> None
        else None

    // If the string is digits with exactly one period possibly proceed
    // by + or - we treat it as decimal.  Note: this insists on a digit
    // both before and after the decimal point --- so far I have seen
    // just 2 digits after the decimal point.
    let decimal_regex = new Regex ("(\+|-)?\d+\.\d+", RegexOptions.Compiled)
    let (|RegexDecimal|_|) s =
      if decimal_regex.Match(s).Success
        then
          match System.Decimal.TryParse s with
          | (true, d) -> Some d
          | _ -> None
        else None

    let business_path_regex = new Regex ("^Business\.", RegexOptions.Compiled)
    let (|BusinessRegexPath|_|) (s : string) =
      if business_path_regex.Match(s).Success
        then
          Some (PathMod.mkPath s)
        else
          None


    let vector_regex = new Regex ("^\s*<v>(.*)</v>", RegexOptions.Compiled)
    let (|RegexVector|_|) (s : string) =
      let m = vector_regex.Match(s)
      if m.Success
        then
          Some m.Groups.[1].Captures.[0].Value
        else
          None

    // Given a string which is a space deliminted list of paths
    // (not necessarily Business) return the associate Path[].
    let parse_path_list (s : string) =
      if s.Length = 0 then
        [| |]
      else
        Array.filter (fun (s0 : string) -> s0.Length > 0) (s.Split ' ') |>
          Array.map (function (ps : string) -> (PathMod.mkPath ps))

    let ordered_multiple_regex = new Regex ("\s*<m.ordered>(.*)</m.ordered>", RegexOptions.Compiled)
    let (|RegexOrderedMultiple|_|) s =
      let m = ordered_multiple_regex.Match s
      if m.Success
        then
          Some (parse_path_list m.Groups.[1].Captures.[0].Value)
        else
          None

    let multiple_regex = new Regex ("\s*<m>(.*)</m>", RegexOptions.Compiled)
    let (|RegexMultiple|_|) s =
      let m = multiple_regex.Match s
      if m.Success
        then
          Some (parse_path_list m.Groups.[1].Captures.[0].Value)
        else
           None

    //
    // Must be run before symbol_regex
    //
    let path_regex = new Regex ("\w+\.", RegexOptions.Compiled)
    let (|RegexPath|_|) s =
      if (path_regex.Match s).Success
        then Some (PathMod.mkPath s)
        else (None : Path option)

    let symbol_regex = new Regex ("\w+", RegexOptions.Compiled)
    let (|RegexSymbol|_|) s =
      if (symbol_regex.Match s).Success
        then Some s
        else None

    let geo_regex = new Regex("<GEO>((.|\n)*)</GEO>|<GEO/>", RegexOptions.Compiled)
    let (|RegexGEO|_|) s =
      let m = geo_regex.Match s
      if m.Success then
        Some s       // we need the bracketing <GEO> </GEO>
                     // m.Groups.[1].Captures.[0].Value
      else
        None

    let type_regex = new Regex("<Type.*>((.|\n)*)</Type.*>", RegexOptions.Compiled)
    let (|RegexType|_|) s =
      let m = type_regex.Match s
      if m.Success then
        Some s       // we need the bracketing <Type...> </Type...>
                     // m.Groups.[1].Captures.[0].Value
      else
        None


    //
    // Translate the string value into BasicTree format which in turn
    // is used to create the GenericObject's CASL expects.
    //
    // The container & key are just for error conditions to identify the
    // row in flex table that is an issue.
    //
    let parse_value container key value =
      begin

        // Given a string which is a list of space delimited vector elements
        // return the array of elements.

        let rec parse_vector_elements (s : string) =
          let rec find_end_quote (st : string) ix =
            let next_quote = st.IndexOf ('"', ix)
            if next_quote = -1 then
              do 
                Logging.error "parse_vector_elements %s@%s no closing quote %s"
                    container key s
              (0, Err_Par_UnmatchQuote (st.Substring (ix - 1)))
            elif st.[next_quote - 1] = '\\' then
              find_end_quote st (next_quote + 1)
            else
              (next_quote, Err_None)

          let rec tokenize (st : string) =
            // do
            //   Logging.trace "tokenize %d '%s'" st.Length st
            if st.Length = 0 then
              ([], Err_None)
            elif st.[0] = ' ' then
              tokenize (st.Substring 1)
            elif st.[0] = '"' then
              let (end_quote, err) = find_end_quote st 1
              if err <> Err_None then
                ([], err)
              else
                let tok_str = BT_String (st.Substring (1, end_quote - 1))
                let (bt1, err1) = st.Substring (end_quote + 1) |> tokenize
                (tok_str::bt1, err1)
            else
              let next_space = st.IndexOf(' ')
              let next_chunk =
                if next_space = -1 then
                  st
                else
                  st.Substring (0, next_space)
              let (tok, tok_err) = match_value next_chunk
              if next_space = -1 || tok_err <> Err_None then
                ([tok], tok_err)
              else
                let (bt3, err3) = st.Substring next_space |> tokenize
                (tok::bt3, err3)

          let res = tokenize s
          // do
          // Logging.trace "parse_vector_elements %A" res
          res
        and
          match_value (str : string) =
          // do
          //   Logging.trace "match_value %d '%s'" str.Length str
            match str with
            | "null"                  -> (BT_Null,          Err_None)
            | "true"                  -> (BT_Boolean true,  Err_None)
            | "false"                 -> (BT_Boolean false, Err_None)
            | RegexInt i              -> (BT_Int i,         Err_None)
            | RegexDecimal d          -> (BT_Decimal d,     Err_None)
            | IsString s              -> (BT_String s,      Err_None)
            | BusinessRegexPath p     -> (BT_Path p,        Err_None)
            | RegexVector v ->
                let (vec_elem, err) = parse_vector_elements v
                (BT_Vector vec_elem, Err_None)
            | RegexOrderedMultiple om -> (BT_OrderedM om,   Err_None)
            | RegexMultiple m         -> (BT_Multiple m,    Err_None)
            | RegexGEO s              -> (BT_GEO s,         Err_None)
            | RegexType s             -> (BT_GEO s,         Err_None)
            | RegexPath path          -> (BT_Path path,     Err_None)
            | RegexSymbol s           -> (BT_Symbol s,      Err_None)
            | "" ->
               do
                 Logging.error "match_value %s@%s is empty string"
                   container key
               (default_value_by_key key, War_Default_Value)
            | _ ->
               do
                 Logging.error "match_value %s@%s Unrecognized type %d '%s'"
                   container key str.Length str
               (BT_Error str, Err_ParserError str)

        let
          (bt, err) = match_value value
        let
          ferr = { err_cont = container ;
                   err_key = key ;
                   err_val = value ;
                   err_flag = err }
        let
          field = {
            name = PathMod.mkElem key ;
            value = bt ;
            }
        (field, ferr)
      end

        
    let Parse_all_fields (rh : Raw_Holder) =
      let (cont, name) = PathMod.mkPath rh.raw_cont
                         |> PathMod.split_into_container_name
      let field_err_map = Map.map (parse_value rh.raw_cont) rh.raw_fields
      let g_fields = field_err_map |> Map.map (fun a b -> fst b)
      let g_errors = field_err_map |> Map.toSeq
                     |> Seq.map snd
                     |> Seq.filter (fun (f, e) -> e.err_flag <> Err_None)
                     |> Seq.map snd
                     |> List.ofSeq
      {
        gh_raw_cont = rh.raw_cont ;
        gh_raw_fields = rh.raw_fields ;
        gh_container = Path cont ;
        gh_name = name ;
        gh_make_part = rh.raw_make_part ;
        gh_fields = g_fields ;
        gh_errors = g_errors
      }