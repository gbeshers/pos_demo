
namespace Config_NG

open System
open System.IO
open System.Text
open System.Collections.Generic
open System.Diagnostics

open Util_NS

open Config_NG.Path_NS
open Config_NG.Flex_NS

module FlexCleanup =

    //
    // This handles a "mis-feature" of the current flex table.
    //
    // In some cases, pathed object is also a vector and we get a
    // line of the form:
    //
    //     container field_key <v> container.field_key.0 </v>
    //
    // where the path in the vector points to itself or one of the
    // contained objects (objects inside the container object
    // referenced in the original Flex table row).
    //
    // These lines must be dropped.  The algorithm is to remove all
    // paths which start with the "container path", e.g., if
    // the flex row is
    //
    //   Bu.A_g.XYZ  "segments"  <v> Bu.A_g.XYZ Bu.A_g.XYZ.segment.0 </v>
    //
    // both vector elements are paths to GenericObjects which
    //   in the first case Bu.A_g.XYZ is the container, or
    //   are is a contained object in 'segment.0'.
    // Thus both are filtered out and pl has length 0, in this case
    // the entire row is discarded.
    //
    let filter_container_vector (Path container)
        (elements : Basic_Tree list) =

      let path_to_container (Path path_to_test) =
        let str_path_cmp (cont_list : PathElem list) (test_path_list : PathElem list) =
          if test_path_list.Length < cont_list.Length then
            true
          else
            let rec cmp_path cl0 pl0 =
              match (cl0, pl0) with
              | ([], _) -> false  // Path points to the container or a subobject
              | (_, []) -> true   // Path points to outside the container
              | (c::cl, p::pl) ->
                match (c, p) with
                | (Pstr cs, Pstr ps) when cs = ps -> cmp_path cl pl
                | (Pint ci, Pint pi) when ci = pi -> cmp_path cl pl
                | _ -> true
            cmp_path cont_list test_path_list
        str_path_cmp container path_to_test



      let isPath elem =
        match elem with
        | BT_Path _ -> true
        | _ -> false

      let extractPath elem =
        match elem with
        | BT_Path p -> p
        | _ ->
          do
            Logging.error "extractPath of non BT_Path"
            // failwith ...
          Path []

      let path_elem = List.filter isPath elements
      
      if elements.Length = 0 then
        BT_Vector elements
      elif path_elem.Length < elements.Length then
        BT_Vector elements
      else
        let (p1, p2) =
          List.map extractPath path_elem |>
                   List.partition path_to_container
        if 0 = p1.Length then
          BT_VectorPart ( externs = p1, children = p2 )
        elif p1.Length < path_elem.Length then
          do
            Logging.error "filter_container_vector partial container %A"
                elements
          BT_VectorPart ( externs = p1, children = p2 )
        else
          List.append p1 p2 |> List.map BT_Path |> BT_Vector


    //
    // The next two functions take a GenObj_Holder and traverse
    // the field values looking for BT_Vectors which are passed
    // to filter_container_vector above.  A BT_Vector or BT_VectorPart
    // is returned.
    //
    // Everything else is unchanged.
    //

    let check_vectors (cont : Path) (key : string) (field : GenObj_Field) =
      let new_value =
        match field.value with
        | BT_Vector el -> filter_container_vector cont el
        | _ as bt -> bt
      { field with value = new_value }

    let identify_vector_parts (holder : GenObj_Holder) =
      let cv = check_vectors holder.gh_container
      { holder with gh_fields = Map.map cv holder.gh_fields } 
