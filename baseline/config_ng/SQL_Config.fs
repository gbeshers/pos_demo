﻿namespace SQL_config_NS

//
// The intention of this file is to be the only F# file
// that open System.Data.SqlClient; we want a separate file
// for each different kind of database, postgress being the
// one on the horizen.
//
// Other that the connection commands the primary focus is
// on creating "readers" which, for the moment, are stream
// wrappers around the database table.  This may not be
// really efficient --- it might be much faster to read the
// database tables in larger chunks however I want to avoid
// having the complete table, or essentially the complete
// table in memory.
//

open System
open System.Data
open System.Data.SqlClient


type Flex_row = {
    container : string
    field_key : string
    field_value : string
 }

module Sql_API =

   let mkConnectionStr {database_name = db_n; datasource_name = ds_n;
                            login_name = lo_n; login_password = lo_p } =
         "Server=" + ds_n + ";Database=" + db_n + ";User=" + lo_n +
         ";Password=" + (Conv_Crypto.symmetric_decrypt lo_p) + ";"

   let newConnection con_str =
     let conn = new SqlConnection(con_str)
     let _ = conn.Open()
     conn

   let newCommand query conn = new SqlCommand(query, conn)

   let newReader (com : SqlCommand) = com.ExecuteReader()

   let newCount (cmd : SqlCommand) =
     let res = cmd.ExecuteScalar()
     res :?> int


   let mkFlexRow (record : IDataRecord) = {container = record.GetString(0);
                                           field_key = record.GetString(1);
                                           field_value = record.GetString(2)}

   let flex_seq (reader : SqlDataReader) =
       seq { while reader.Read() do yield (mkFlexRow reader) }

   let command_count = "SELECT COUNT(*) FROM [CBT_Flex]"
   let command_str =
       "SELECT [container], [field_key], [field_value] FROM [CBT_Flex]"

   let Flex_count connection = newCount (newCommand command_count connection)
   let Flex_Read_command connection = newCommand command_str connection

