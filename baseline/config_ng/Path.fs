
namespace Config_NG.Path_NS

open System
open System.IO
open System.Text
open System.Diagnostics

open Util_NS

//
//     Tie the string (as read from the DB) and the path together.
// We will add the associated GenericObject in time via the hold_table.
// The "tricky" piece of this is handling integer vs. strings that are
// all digits.  The GenericObject piece only comes up with multiples
// and at the moment isn't actually used.
//
type PathElem =
    | Pstr of string
    | Pint of int

type Path =
    | Path of PathElem list

module PathMod =

    //
    // Strip quotes from the beginning and end.  I believe that in
    // practice only '"' occure, but wasn't confident of that when
    // I started.
    //

    let strip_end_quotes (s : string) =
      if s.Length > 1 then
        let c = s.[0]
        if (c = '"' || c = '\'') && (c = s.[s.Length-1]) then
          s.[1..(s.Length - 2)]
        else
          s
      else
        s


    //
    // Various simple manipulations of the Path type.
    //

    let length (Path el) = el.Length
    let get_list (Path el) = el

    let record_separator = char 30 |> string
    let elem_to_key pe =
      match pe with
      | Pstr s -> s
      | Pint i -> record_separator + (string i)
      
    let elem_to_string pe =
      match pe with
      | Pstr s ->
        if s.Length = 0 then
          "\"\""
        elif Char.IsDigit s.[0] then
          "\"" + s + "\""
        else
          s
      | Pint i -> i.ToString()


    let rec elem_list_to_string p =
      match p with
      | [] -> ""
      | h::[] -> elem_to_string h
      | h::t -> (elem_to_string h) + "." + (elem_list_to_string t)

    let to_string (Path el) =
      elem_list_to_string el

    let mkElem (name : string) =
      if name.[0] = '"' then
        strip_end_quotes name |> Pstr
      else
        match System.Int32.TryParse name with
        | (true, i) -> (Pint i)
        | _ -> (Pstr name)

    //
    // Given a path read from the database, translate it into a type Path
    // where we store a list of (Pstr | Pint) explicitly to avoid careless
    // errors parsing later.  See find_geo_str and as the key to hold_table.
    //

    let mkPath (s : string) =
      let rec parse_parts (pp : string list) =
        match pp with
        | [] -> []
        | name::t -> (mkElem name)::(parse_parts t)
      if s.Length = 0 then
        Path []
      else
        let path = s.Split '.' |> Array.toList |> parse_parts
                   |> Path
#if DEBUG
        do
          if s <> (to_string path) then
            Logging.error "mkPath Error '%s' <> '%s'" s
                      (to_string path)
#endif
        path

    //
    // Take a given path and split it into the path to the container
    // and the name of the object in that container, e.g., the last
    // element.
    //

    let split_into_container_name (Path el) =
      let rec spl p =
        match p with
        | [] ->
          failwith "Tried to split empty Path into (container, name)"
        | h::[] -> ([], h)
        | h::t ->
          let (c, n) = spl t
          (h::c, n)
      spl el

    //
    // Take a given path and a key return the extended path
    // with the key appended.
    //

    let append (Path path) (pe : PathElem) =
        path@[pe] |> Path

    let append_key (path : Path) (key : string) =
      let pe =
        match System.Int32.TryParse key with
        | (true, i) -> Pint i
        | _ -> Pstr key
      append path pe

