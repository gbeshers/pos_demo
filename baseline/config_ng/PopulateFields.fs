
namespace Config_NG

#if DEBUG
// open System.Diagnostics
#endif

open CASL_engine
open Util_NS

open Config_NG
open Config_NG.Path_NS
open Config_NG.Flex_NS
open Config_NG.GenObj_NS

module FillFields =

    let vector_class = GenObjMod.vector_class
    

    //
    // Actually responsible for getting the key, value pair into
    // the GenericObject.  Also, handles keeping _db_orig in sync.
    //
    let set_value path (geo : GenericObject) (key : PathElem) (v : obj) (where : string) =
#if DEBUG
      let
        path_str = PathMod.to_string path
      do
        if path_str <> geo.safe_to_path () then
          Logging.error "set_value path discrepency %s %s %A"
            path_str (PathMod.to_string path) key
#endif
      let new_value =
        if GenObjMod.geo_has geo key then
          let existing_value = GenObjMod.geo_get geo key null false
          if GenObjMod.is_GenObj existing_value &&
             GenObjMod.is_GenObj v &&
             (GenObjMod.Failed = (GenObjMod.to_path_is_good (v :?> GenericObject ))) then
            let ev = existing_value :?> GenericObject
            let nv = v :?> GenericObject
            GenObjMod.copy_value nv ev |> ignore
#if DEBUG
            Logging.trace "set_value: Overwriting with GenericObject %A[%A] key=%A old=%A new=%A"
              path geo.get_gen_obj_number key ev nv
#endif
            nv :> obj
          else
            if existing_value <> v then
              Logging.trace "set_value: Overwriting %A[%A] key=%A old=%A new=%A"
                path geo.get_gen_obj_number key existing_value v
            v
        else
          v
      GenObjMod.geo_set geo key new_value |> ignore
      GenObjMod.add_to_db_orig geo path key new_value "add_key_value_pair.set_value"
#if DEBUG
      do
        Logging.trace "   %s: setting key %A %s" path_str key where
#endif

    //
    // Vectors in CASL are not uniform, the element types can vary.
    // This function transforms the BasicTree for a vector into
    // the GenericObject(s) that CASL requires.
    //
    let mkVector vec =
      let new_vec = GenObjMod.create_GenObj vector_class
      let handle_elements elem =
        match elem with
        | BT_Path p ->
           match GenObjMod.path_to_GenObj p with
           | PR_Base geo -> geo :> obj
           | PR_Conf geo -> geo :> obj
           | PR_Obj o -> o
           | PR_Unkn -> null :> obj
        | BT_String s -> s :> obj
        | BT_Boolean b -> box b
        | BT_Int i -> box i
        | BT_Decimal d -> box d
        | BT_Null -> null
        | BT_Symbol s ->
          do
            Logging.error "mkVector: BT_Symbol %s" s
          s :> obj
        | BT_GEO str ->
          do
            Logging.error "mkVector: BT_GEO %s" str
          GenObjMod.eval_BT_GEO str
        | _ as GAR ->
          do
            Logging.error "mkVector Total Garbage %A" GAR
          null
      let add_path i el = new_vec.set(i, handle_elements el) |> ignore
      do
        List.iteri add_path vec
      new_vec

    let init_part = Multiples.init_part_with_rebase id

    let populate_genobj (geo_rec : GenObj_Record) =
      let p = PathMod.append geo_rec.container geo_rec.name
      let add_field geo key_s =
        let fld = geo_rec.fields.[key_s]
        let k = fld.name
        // if k = Pstr "service_name" then
        //   Logging.error "   populate_genobj:add_field %A %A" k fld.value
        match fld.value with
        | BT_Null         -> set_value p geo k null "BT_Null"
                             true
        | BT_String s     -> set_value p geo k s "BT_String"
                             true
        | BT_Boolean b    -> set_value p geo k (box b) "BTR_Boolean"
                             true
        | BT_Int i        -> set_value p geo k (box i) "BT_Int"
                             true
        | BT_Decimal d    -> set_value p geo k (box d) "BT_Decimal"
                             true
        | BT_Path path    -> set_value p geo k (GenObjMod.get_GenObj path) "BT_Path"
                             true
        | BT_Vector vec   ->
          let
            new_vec = mkVector vec
          set_value p geo k new_vec "BT_Vector"
          true
        | BT_OrderedM om  -> Multiples.mkMultiple p geo k om true
                             true
        | BT_Multiple mt  -> Multiples.mkMultiple p geo k mt false
                             true
        | BT_Error msg    -> ()   // of string
                             false
        | BT_GEO casl_str ->
            let o = GenObjMod.eval_BT_GEO casl_str 
            set_value p geo k o "BT_GEO"
            true

        | BT_Symbol id    ->
            let o = GenObjMod.get_GenObj (Path [Pstr id])
            set_value p geo k o "BT_Symbol"
            true
        | BT_VectorPart ( externs = exts; children = kids ) ->
            do    // Processing done in GenObjMod
              Logging.error "populate_genobj %A field %A is a VectorPart exts=%A" p k exts
              for kid in kids do
                let o = GenObjMod.path_to_GenObj kid
                match o with
                | PR_Base geo | PR_Conf geo -> ()
                | PR_Obj o ->
                   Logging.error "populate: %A ends in an arbitrary object" kid
                | PR_Unkn ->
                   Logging.error "populate: %A has no corresponding object" kid
            false

      //
      // The Business.????.of objects get kind-of special handling; not totally
      // sure why.  However, the are members of GenObj_Class and so making
      // modifications to them violates the not changes to CASL classes.
      //
      // I want to kill them in next_gen_v2.
      //
      let name_of = Pstr "of"
      let is_of_container (cont_path : Path) =
        let
          (cont, name) = PathMod.split_into_container_name cont_path
        name = name_of


      //
      // Sanity check, probably can be removed.
      //
      do
        if geo_rec.fields.ContainsKey "_parent" then
          Logging.error "_parent seen in populate_genobj %A" p

      match geo_rec.gen_obj with
        | PR_Base geo ->
          do
            Logging.trace "populate_genobj PR_Base %s %A %A %A gen_obj=%s"
              geo_rec.raw_container geo_rec.container geo_rec.name
              (is_of_container geo_rec.container) geo.get_path_to_me
          do
            if is_of_container geo_rec.container then
              GenObjMod.init_db_orig geo p
              GenObjMod.init_db geo p
            elif geo_rec.name = name_of then
              GenObjMod.init_db_orig geo p

          let mutable added_field = []
          for k in geo_rec.fields.Keys do
            if add_field geo k then
              added_field <- k::added_field
          for k in added_field do
            geo_rec.fields.Remove k |> ignore

        | PR_Conf geo ->
          do
            Logging.trace "populate_genobj PR_Conf %s %A gen_obj=%s"
              geo_rec.raw_container geo_rec.name geo.get_path_to_me

          GenObjMod.init_db_orig geo p
          GenObjMod.init_db geo p
          init_part geo p

          let mutable added_field = []
          for k in geo_rec.fields.Keys do
            if add_field geo k then
              added_field <- k::added_field
          for k in added_field do
            geo_rec.fields.Remove k |> ignore

        | PR_Obj o ->
          Logging.error "Tried to add (key, value) pair to object %A at %s"
            o (PathMod.append geo_rec.container geo_rec.name |> PathMod.to_string)
        | PR_Unkn -> ()
      ()
