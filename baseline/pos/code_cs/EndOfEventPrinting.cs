﻿using CASL_engine;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.UI;

namespace baseline
{
  // Bug 24518 MJO - End of event printing
  public class EndOfEventPrinting
  {
    public static object fCASL_Start(CASL_Frame frame)
    {
      GenericObject geo_args = frame.args;
      GenericObject documents = geo_args.get<GenericObject>("documents");
      GenericObject items = geo_args.get<GenericObject>("items");
      GenericObject app = geo_args.get<GenericObject>("top_app");

      return Start(documents, items, app);
    }

    public static object Start(GenericObject documents, GenericObject items, GenericObject app)
    {
      app.remove("end_of_event_document_index", false);

      List<DocumentItem> eventItems = new List<DocumentItem>();

      for (int i = 0; i < documents.getLength(); i++)
      {
        GenericObject document = documents.get<GenericObject>(i);
        GenericObject item = items.get<GenericObject>(i);

        if (document.get<string>("processing_stage", "page", true) == "end_of_event")
        {
          try
          {
            if (EvaluateCondition(document, item))
              eventItems.Add(new DocumentItem(document, item));
          }
          catch (Exception e)
          {
            return CASL_error.create_str(e.Message).casl_object;
          }
        }
      }

      if (eventItems.Count > 0)
      {
        app.set("end_of_event_document_items", eventItems);
        return true;
      }

      return false;
    }

    private static bool EvaluateCondition(GenericObject document, GenericObject transaction)
    {
      string condition = document.get<string>("process_condition", "").Trim();
      string initialCondition = condition;

      try
      {
        if (condition.Length == 0)
          return true;

        MatchCollection matches = Regex.Matches(condition, @"\{([^}]*)\}");

        foreach (Match match in matches)
        {
          string value = match.Value;
          string tagname = value.Substring(1, value.Length - 2);

          if (transaction.has(tagname))
            condition = condition.Replace(value, transaction.get(tagname).ToString());
          // Bug 26773 MJO - This is invalid, so fail out and log
          else
            throw new Exception($"Tagname '{tagname}' does not exist in this transaction.");
        }

        // https://stackoverflow.com/questions/8476422/logic-evaluator-in-c-sharp-evaluate-logical-expressions
        System.Data.DataTable table = new System.Data.DataTable();
        table.Columns.Add("", typeof(Boolean));
        table.Columns[0].Expression = condition;

        System.Data.DataRow r = table.NewRow();
        table.Rows.Add(r);
        return (Boolean)r[0];
      }
      catch (Exception e)
      {
        Logger.LogError("Failed to process document " + document.get<string>("document_id") + Environment.NewLine +
          "Initial Condition: " + initialCondition + Environment.NewLine +
          "Condition: " + condition + Environment.NewLine +
          e.ToMessageAndCompleteStacktrace(), "End of Event Printing EvaluateCondition");

        throw;
      }
    }

    public static object fCASL_ProcessDocument(CASL_Frame frame)
    {
      GenericObject geo_args = frame.args;
      GenericObject app = geo_args.get<GenericObject>("top_app");
      int index = Int32.Parse(geo_args.get<string>("index"));

      return ProcessDocument(app, index);
    }

    public static object ProcessDocument(GenericObject app, int index)
    {
      List<DocumentItem> documentItems = app.get<List<DocumentItem>>("end_of_event_document_items");

      GenericObject document = documentItems[index].document;
      GenericObject transaction = documentItems[index].item;

      if (!document.has("process_transaction", true))
      {
        return CASL_error.create_str("Document '" + document.get<string>("document_id") + "' does not support end of event printing.");
      }

      object R = document.Call("process_transaction", new GenericObject("top_app", app, "transaction", transaction));

      app.set("end_of_event_document_index", index);

      return R;
    }

    public static object fCASL_Finish(CASL_Frame frame)
    {
      GenericObject geo_args = frame.args;
      GenericObject app = geo_args.get<GenericObject>("top_app");

      return Finish(app);
    }

    public static object Finish(GenericObject app)
    {
      app.remove("end_of_event_document_items");

      return true;
    }

    public static object fCASL_GenerateJSONData(CASL_Frame frame)
    {
      GenericObject geo_args = frame.args;
      GenericObject app = geo_args.get<GenericObject>("top_app");

      return GenerateJSONData(app);
    }

    public static object GenerateJSONData(GenericObject app)
    {
      List<DocumentItem> documentItems = app.get<List<DocumentItem>>("end_of_event_document_items");

      List<List<string>> data = new List<List<string>>();

      for (int i = 0; i < documentItems.Count; i++)
      {
        DocumentItem item = documentItems[i];

        data.Add(MakeJSONRow(item, i));
      }

      return JsonConvert.SerializeObject(data);
    }
    private class JsonColumn
    {
      public string title;
      public string render = null;
      public string type = "string";
    }

    public static object fCASL_GenerateJSONColumns(CASL_Frame frame)
    {
      return GenerateJSONColumns();
    }

    private static object GenerateJSONColumns()
    {
      List<JsonColumn> columns = new List<JsonColumn>();

      foreach (string title in Columns)
      {
        JsonColumn column = new JsonColumn();
        column.title = title;

        if (title == "")
        {
          column.render = "RENDER";
          column.type = "html";
        }

        columns.Add(column);
      }

      return JsonConvert.SerializeObject(columns).Replace("\"RENDER\"", "function ( data, type, row, meta ) { return decode_html_chars(data); }");
    }

    private static string[] Columns = new[] { "", "Transaction", "Primary ID", "Document", "ID", "Amount", "Suggested Quantity", "Number Processed", "" };

    private static List<string> MakeJSONRow(DocumentItem item, int index)
    {
      object primaryId = item.item.Call("get_configured_primary_id");

      if (!(primaryId is string))
        primaryId = "";

      string[] fields = new[] {
        MakeJSONDetailButton(index),
        item.item.get<string>("description"),
        primaryId.ToString(),
        item.document.get<string>("description"),
        item.item.Call("get_receipt_nbr") as string,
        "$" + item.item.get("amount").ToString(),
        item.quantity.ToString(),
        item.numProcessed + "/" + item.quantity,
        "CBUTTON_" + index // placeholder for the Process cbutton
      };

      List<string> row = new List<string>();

      foreach (string field in fields)
      {
        row.Add(field);
      }

      return row;
    }

    private static string MakeJSONDetailButton(int index)
    {
      return String.Format("<img src='/{0}/baseline/business/images/plus.png' data-target='{1}' data-cbtid='{2}' onclick='{3}' id='{4}' data-url='{5}'>",
        misc.get_site_name(),
        "inline",
        "none",
        "end_of_event_toggle_arrow(this);get_details(this);",
        "end_of_event_detail_" + index,
        "DATA_URL_" + index // placeholder
      );
    }

    private static int GetQuantity(GenericObject document, GenericObject transaction)
    {
      int quantity = 1;

      object tagname = document.get("print_quantity_tagname", "");

      if (tagname is string && ((string)tagname).Trim().Length > 0 && transaction.has(tagname))
      {
        string sTagname = tagname as string;
        object value = transaction.get(sTagname);

        if (value is int)
        {
          quantity = (int)value;
        }
        else if (value is string)
        {
          int.TryParse(value as string, out quantity);
        }
      }
      else if (transaction.has("quantity"))
      {
        int.TryParse(transaction.get("quantity").ToString(), out quantity);
      }

      return quantity;
    }

    public static object fCASL_MakeDetailTable(CASL_Frame frame)
    {
      GenericObject geo_args = frame.args;
      GenericObject app = geo_args.get<GenericObject>("top_app");
      int index = Int32.Parse(geo_args.get<string>("index"));

      return MakeDetailTable(app, index);
    }

    private static object MakeDetailTable(GenericObject top_app, int index)
    {
      List<DocumentItem> documentItems = top_app.get<List<DocumentItem>>("end_of_event_document_items");
      GenericObject transaction = documentItems[index].item;
      GenericObject fieldOrder = transaction.get<GenericObject>("receipt_field_order", new GenericObject(), true);
      GenericObject tagnames = transaction.Call("hybrid_field_order_to_tagname_label_pairs", new GenericObject("field_order", fieldOrder, "allow_html_labels", false)) as GenericObject;

      StringBuilder sb = new StringBuilder();

      sb.Append("<table><tr><th>Field Name</th><th>Field Value</th></tr>");

      for (int i = 0; i < tagnames.getLength(); i++)
      {
        string tagname = tagnames.get<string>(i);
        string label = transaction.Call("get_label", new GenericObject("key", tagname)).ToString();
        object value = transaction.get(tagname, "");

        if (value is GenericObject)
          value = "";
          
        // Bug 26738 MJO - Skip null or empty fields
        if (!String.IsNullOrWhiteSpace(value.ToString()))
          sb.Append(String.Format("<tr><td>{0}</td><td>{1}</td></tr>", label, value));
      }

      sb.Append("</table>");

      return sb.ToString();
    }
    public static object fCASL_IncrementNumProcessed(CASL_Frame frame)
    {
      GenericObject geo_args = frame.args;
      GenericObject app = geo_args.get<GenericObject>("top_app");

      return IncrementNumProcessed(app);
    }

    public static int IncrementNumProcessed (GenericObject top_app)
    {
      List<DocumentItem> documentItems = top_app.get<List<DocumentItem>>("end_of_event_document_items");
      int index = (int)top_app.remove("end_of_event_document_index");

      return documentItems[index].IncrementProcessed();
    }

    private class DocumentItem
    {
      public GenericObject document { get; }
      public GenericObject item { get; }
      public int quantity { get; }
      public int numProcessed { get; set; }

      private DocumentItem() { }

      public DocumentItem (GenericObject document, GenericObject item)
      {
        this.document = document;
        this.item = item;
        this.quantity = GetQuantity(document, item);
        numProcessed = 0;
      }

      public int IncrementProcessed()
      {
        if (numProcessed != quantity)
          ++numProcessed;

        return numProcessed;
      }
    }
  }
}
