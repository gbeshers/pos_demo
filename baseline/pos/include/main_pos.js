/*
  ------------ SCAN/SWIPE ENTRY ------------ moved to main_pos_pci.js
*/
var reading = false;
var keyCodeQueue = new Array();
var scan_string = "";
var start_code = -1;

var old_form = null;
var old_action = "";

var timer = null;

function find_form(tag) {
  for ( var form_key in document.forms ) {
    var form_value = document.forms[form_key];
    if ( typeof(form_value) == "object" ) {
      for ( var element_key in form_value.elements ) {
        var element_value = form_value.elements[element_key];
        if ( element_value == tag ) {
          return form_value;
        }
      }
    }
  }
  return false;
}
/*
function increment (id) {
  var tag=document.getElementById(id);
  if ( tag.value == "" ) tag.value = 0;
  tag.value = parseInt(tag.value) + 1; 
  }
*/
function increment(id) {

  if ( document.getElementById(id).value == "" ) { // BUG#5728 
     document.getElementById(id).value = 0; // BUG#5728 
  }
  document.getElementById(id).value = parseInt(document.getElementById(id).value) + 1;// BUG#5728 
  
  CalcTotal();
}

 function elementInId (a_tag, container_id) { 
  var container=a_tag.parentElement; 
  if (container && container.id==container_id)    
     return container; 
  else if (container)    
    return elementInId(container); 
  else    
    return false; 
  }  

function get_nested_element (outer_id, inner_id) {    
  var outer_tags=document.getElementsByName(outer_id);    
  var inner_tags=document.getElementsByName(inner_id);    

  for (var i=0; i<inner_tags.length; i++) {     
     var TAG=elementInId(inner_tags[i],outer_id); 
     if (TAG) return inner_tags[i];    
  }    

  return(false); 
}

// returns "/some_site"
function get_site_logical () {
 var path =  location.pathname;
 return path.substring(0,path.indexOf("/",1));  // Ex: "/some_site"
}

// Move to js file for just cashiering
// Called by: Create_core_item_app.htm_inst    Related to: Gl_number_format.add_allocation_config_js
// Bug 23970 MJO - Changed to post_remote. App path now passed in and method name is hardcoded
function htm_add_allocation (segments, app)  {
  // MP: This method should to be auto-generated from the CASL presentation method for the allocation rows.  This was manually generated, though for now.
  // segments example: [ { len: 5 }, { len: 10 } ]  Future: { len: 5, label: "GL Nbr 1", type: "numeric" }
  // MP: issue: generating JavaScript for the size, number, and types of the GL segments
  post_remote(app, "Business_app.Create_core_item_app.add_allocation", {}, null, true);
  // Resets the timeout when validating so that any action prevents the user from timing out.
  if (typeof resetTimeout == 'function') resetTimeout(); 

  var a_tbody=document.getElementById("new_allocations");

  var NI=allocation_rows + a_tbody.rows.length/2;  // NI = new_allocation_index
  //lert("NAI: " + new_allocation_index);

  // Bug #10264 Mike O - Use the proper DOM code for inserting a row
  a_row = document.createElement("tr");
  a_row.className = "Allocation_htm_inst"; // Bug 14657 UMN need class for alloc totals
  a_tbody.appendChild(a_row);
  // End Bug #10264 Mike O
  
  var elementId = 'args.0.' + NI; //Bug 10473 NJ
  var a_cell=null;
  // Bug #10264 Mike O - Use the proper DOM code for inserting a cell
  a_cell = document.createElement("td");
  
  var a_input = document.createElement("input");
  a_input.setAttribute("type", "text");
  a_input.setAttribute("size", "25");
  a_input.setAttribute("maxLength", "50"); //Bug 10727 NJ - changed Length to capital L.
  a_input.setAttribute("autocomplete", "off");
  a_input.setAttribute("name", "args.0." + NI + ".description");
  a_input.className = "text args.0." + NI + ".description";
  a_input.setAttribute("onblur", "validate_tag(this);");
  a_input.setAttribute("value", "Allocation " + (NI+1));
  a_input.setAttribute("ui", "INPUT-make_widget");
  a_input.setAttribute("id", "args.0." + NI + ".description");
  a_input.setAttribute("field_info", "{label:'Description',types:[['String','error']]}");
  a_cell.appendChild(a_input);
  
  var a_span = document.createElement("span");
  a_span.setAttribute("id", "args.0." + NI + ".description_error");
  a_span.setAttribute("class", "error");
  //Bug 9263-NJ-onchange event for description field 
  if (a_input.attachEvent != undefined) 
      a_input.attachEvent("onchange", function() { AllocationOnChangeSelect(elementId); });
  else
      a_input.addEventListener("change", function() { AllocationOnChangeSelect(elementId); }, false);
      //end bug 9263
  a_cell.appendChild(a_span);
  
  a_row.appendChild(a_cell);
  // End Bug #10264 Mike O
  //lert("description: " + a_cell.innerHTML);
  /* Example: <INPUT class="text args.0.6.description" id=args.0.6.description onblur=validate_tag(this); onfocus=do_focus(this) maxLength=50 size=25 value="Allocation 7" name=args.0.6.description ui="INPUT-make_widget" autocomplete="off"><SPAN class=error id=args.0.6.description_error></SPAN> */
  
  // Bug #10264 Mike O - Use the proper DOM code for inserting a cell
  a_cell = document.createElement("td");
  //BUG 6062 change size from 11 to 17 and maxlength from 9 to 15
  //Bug 9408 SN
  //Bug 9559 NJ
  //Bug 9263 NJ-commented out unused variable
//  var onChangeStr = 'var amount = ' + 
//                    'document.getElementById(\'args.0.' + NI + '.amount\').value.replace(\'$\',\'\').replace(\',\',\'\');' + 
//                    'var isNegative = (amount.indexOf(\'(\') != -1 && amount.indexOf(\')\') != -1);'+ 
//                    'if(isNegative)'+
//                    '{amount = \'-\' + amount.replace(\'(\',\'\').replace(\')\',\'\');}' + 
//                    'document.getElementById(\'args.0.' + NI + '.total\').innerHTML = '+
//                    'formatCurrency( amount * document.getElementById(\'args.0.' + NI + '.quantity\').value ,2);';
                    
  //End bug 9559
  
  a_input = document.createElement("input");
  a_input.setAttribute("type", "text");
  a_input.setAttribute("size", "17");
  a_input.setAttribute("maxLength", "15"); //Bug 10727 NJ - changed Length to capital L.
  a_input.setAttribute("autocomplete", "off");
  a_input.setAttribute("field_info", "{label:'Amount',types:[['required','error'],['currency_positive_zero_and_negative','error']]}");
  a_input.setAttribute("name", "args.0." + NI + ".amount");
  a_input.className = "text amount";
  //a_input.setAttribute("onblur", "validate_tag(this);"); Bug 10668 NJ
  a_input.setAttribute("value", "$0.00");
  a_input.setAttribute("ui", "INPUT-make_widget");
  a_input.setAttribute("id", "args.0." + NI + ".amount");
  //Bug 10473 NJ
  //a_input.setAttribute("onchange", "javascript:" + onChangeStr);
  if (a_input.attachEvent != undefined) //Bug 10625 NJ
  a_input.attachEvent("onchange", function() { AllocationOnChangeSelect(elementId); AllocationOnChangeRecalculate(elementId); });
    //Bug 10625 NJ
    else
        a_input.addEventListener("change", function() { AllocationOnChangeSelect(elementId); AllocationOnChangeRecalculate(elementId); }, false);
    //End Bug 10625 NJ
  //End bug 10473 NJ

    //Bug 10668 NJ
    if (a_input.attachEvent != undefined) {
        a_input.onblur = validate_tag;
    }
    else
        a_input.addEventListener("blur", function() { validate_tag(this); }, false);
    //End Bug 10668 NJ
    
  a_cell.appendChild(a_input);
  
  
  a_span = document.createElement("span");
  a_span.setAttribute("id", "args.0." + NI + ".amount_error");
  a_span.setAttribute("class", "error");
  a_cell.appendChild(a_span);
  
  a_row.appendChild(a_cell);
  // End Bug #10264 Mike O
  //lert("amount: " + a_cell.innerHTML);
  /* <INPUT class="text args.0.6.amount" id=args.0.6.amount onblur=validate_tag(this); onfocus=do_focus(this) maxLength=9 onchange="javascript:document.getElementById('args.0.6.total').innerHTML = formatNumber(document.getElementById('args.0.6.amount').value * document.getElementById('args.0.6.quantity').value ,2);" size=11 value=0.00 name=args.0.6.amount ui="INPUT-make_widget" autocomplete="off"><SPAN class=error id=args.0.6.amount_error></SPAN> */ 
  // Bug #10264 Mike O - Use the proper DOM code for inserting a cell
  a_cell = document.createElement("td");
  str1='';
  //Bug 9408 SN
  
  a_input = document.createElement("input");
  a_input.setAttribute("type", "text");
  a_input.setAttribute("size", "4");
  a_input.setAttribute("maxLength", "9"); //Bug 10727 NJ - changed Length to capital L.
  a_input.setAttribute("autocomplete", "off");
  a_input.setAttribute("field_info", "{label:'Quantity',types:[['required','error'],['positive','error']]}");
  a_input.setAttribute("name", "args.0." + NI + ".quantity");
  a_input.className = "text quantity";
  a_input.setAttribute("onblur", "validate_tag(this);");
  a_input.setAttribute("value", "1");
  a_input.setAttribute("ui", "INPUT-make_widget");
  a_input.setAttribute("id", "args.0." + NI + ".quantity");
  /* Bug 10473 NJ
  a_input.setAttribute("onchange", "javascript:document.getElementById('args.0."
    + NI + ".total').innerHTML = formatCurrency(document.getElementById('args.0."
     + NI + ".amount').value.replace('$','').replace(',','') * document.getElementById('args.0."
    + NI + ".quantity').value, 2);");*/
  if (a_input.attachEvent != undefined) //Bug 10625 NJ
  a_input.attachEvent("onchange", function() { AllocationOnChangeSelect(elementId); AllocationOnChangeRecalculate(elementId); });
  //Bug 10625 NJ 
  else
      a_input.addEventListener("change", function() { AllocationOnChangeSelect(elementId); AllocationOnChangeRecalculate(elementId); }, false);
  //End Bug 10625 NJ 
  //End bug 10473 NJ
  a_cell.appendChild(a_input);
  
  a_span = document.createElement("span");
  a_span.setAttribute("id", "args.0." + NI + ".quantity_error");
  a_span.className = "error";
  a_cell.appendChild(a_span);
  
  a_row.appendChild(a_cell);
  // End Bug #10264 Mike O
  //lert("quantity: " + a_cell.innerHTML);
  /* <INPUT class="text args.0.6.quantity" id=args.0.6.quantity onblur=validate_tag(this); onfocus=do_focus(this) maxLength=9 onchange="javascript:document.getElementById('args.0.6.total').innerHTML = formatNumber(document.getElementById('args.0.6.amount').value * document.getElementById('args.0.6.quantity').value, 2);" size=4 value=1 name=args.0.6.quantity ui="INPUT-make_widget" autocomplete="off"><SPAN class=error id=args.0.6.quantity_error></SPAN> */
  
  // Bug #10264 Mike O - Use the proper DOM code for inserting a cell
  a_cell = document.createElement("td");
  
  var a_div = document.createElement("div");
  a_div.setAttribute("id", "args.0." + NI + ".total");
  a_div.appendChild(document.createTextNode("$0.00"));
  a_cell.appendChild(a_div);
  
  a_row.appendChild(a_cell);
  // End Bug #10264 Mike O
  
  // Bug #10264 Mike O - Use the proper DOM code for inserting a cell
  a_cell = document.createElement("td");
  
  var a_center = document.createElement("center");
  
  a_input = document.createElement("input");
  a_input.setAttribute("id", "args.0." + NI + ".selected_f_value");
  /*Bug 10473 NJ
  a_input.setAttribute("onclick", "javascript:document.getElementById('args.0."
    + NI + ".selected').value = document.getElementById('args.0."
    + NI + ".selected_f_value').checked;validate_sub_items('args.0."
    + NI + "\');");*/
  if (a_input.attachEvent != undefined) //Bug 10625 NJ
    a_input.attachEvent("onclick", function() { AllocationOnClick(elementId); });
  //Bug 10625 NJ 
  else
    a_input.addEventListener("click", function() { AllocationOnClick(elementId); }, false);
  //End Bug 10625 NJ
  //End bug 10473 NJ
  a_input.setAttribute("checked", "true");
  a_input.setAttribute("type", "checkbox");
  a_input.setAttribute("name"," args.0." + NI + ".selected_f_value");
  a_input.className = "selected_f_value"; // Bug 14657 UMN need class for alloc totals
  a_center.appendChild(a_input);
  
  a_input = document.createElement("input");
  a_input.setAttribute("id", "args.0." + NI + ".selected");
  a_input.setAttribute("type", "hidden");
  a_input.setAttribute("name", "args.0." + NI + ".selected");
  // Bug #11327 Mike O - Should start off deselected
  a_input.setAttribute("value", "true"); //Bug 20529 NK Validation missing after adding allocation if checkbox is selected that value should be set to true
  a_center.appendChild(a_input);
  
  // Bug #11548 Mike O - Removed the confusing selected_onchange_value hidden field
  //Bug 10473 NJ/9263 NJ - hidden field to keep track of checkbox previous state. this is used to avoid any confusion between the user
  //checking the checkbox and the checkbox being checked when qty/amount/description fields change.
  a_input = document.createElement("input");
  a_input.setAttribute("id", "args.0." + NI + ".selected_onchange_value");
  a_input.setAttribute("type", "hidden");
  a_input.setAttribute("name", "args.0." + NI + ".selected_onchange_value");
  a_center.appendChild(a_input);
  //End bug 10473 NJ
  a_cell.appendChild(a_center);
  
  a_row.appendChild(a_cell);
  
  //lert("selected: " + a_cell.innerHTML);
  /* <CENTER><INPUT id=args.0.6.selected_f_value onclick="javascript:document.getElementById('args.0.6.selected').value = document.getElementById('args.0.6.selected_f_value').checked;validate_sub_items('args.0.6');" type=checkbox CHECKED name=args.0.6.selected_f_value><INPUT id=args.0.6.selected type=hidden value=true name=args.0.6.selected></CENTER> */

    //TTS20885 

  a_row = document.createElement("tr");
  a_row.className = "gl_row";
  a_tbody.appendChild(a_row);
  a_cell = document.createElement("td");
  a_cell.setAttribute("colSpan", 6);
  a_row.appendChild(a_cell);
  a_div = document.createElement("div");
  a_div.className = "expanded";
  a_div.setAttribute("name", "gl_numbers_expanded");
  a_div.setAttribute("id", "gl_numbers_expanded_0");
  a_cell.appendChild(a_div);
  a_table = document.createElement("table");
  a_table.className = "GL_table";
  a_cell.appendChild(a_table);
  a_tbody_gl = document.createElement("tbody");
  a_table.appendChild(a_tbody_gl);
  a_row = document.createElement("tr");
  a_tbody_gl.appendChild(a_row);

  // empty cell 
  a_cell = document.createElement("td");
  a_row.appendChild(a_cell);

  for (var i in segments) {
      //lert(segments[i].size);
      ST = segments[i];
      a_cell = document.createElement("td");
      a_cell.className = "key";
      a_cell.appendChild(document.createTextNode(ST.name));
      a_row.appendChild(a_cell);
  }

  // Bug #10264 Mike O - Use the proper DOM code for inserting a cell
  a_row = document.createElement("tr");
  a_row.className="gl_row";
  a_tbody_gl.appendChild(a_row);
  // End Bug #10264 Mike O
  
    // Bug #10264 Mike O - Use the proper DOM code for inserting a cell

  //a_cell = document.createElement("td");
  //a_cell.setAttribute("colSpan",6); // colSpan is CASE-SENSITIVE!!
  //var gl_segments=document.createElement("div");
  //gl_segments.setAttribute("id", "gl_numbers_expanded_" + NI);
  //gl_segments.setAttribute("name", "gl_numbers_expanded");
    //gl_segments.className = "expanded";

  a_cell = document.createElement("td");
  a_row.appendChild(a_cell);
  a_span = document.createElement("span");
  a_span.style.color = "black";
  a_span.appendChild(document.createTextNode("GL #"));
  a_cell.appendChild(a_span);
  
  var a_segment=null;
  for (var i in segments) {
    //lert(segments[i].size);
    ST = segments[i];
    a_cell = document.createElement("td");
    a_row.appendChild(a_cell);
    a_input = document.createElement("input");
    a_cell.appendChild(a_input);
    a_input.setAttribute("type", "text");
    a_input.setAttribute("size", ST.len+1);
    a_input.setAttribute("name", "args.0."
      // Bug #6013 Mike O - Modified to use the new path to where GL # segment values are stored
      + NI + ".GL_nbr.segments."
      + i + ".chars");
    a_input.className = "text GL_nbr";
    a_input.setAttribute("onBlur", "validate_tag(this);"); //Bug 10727 NJ - changed Blur to capital B.
    a_input.setAttribute("value", "");
      // Bug #6013 Mike O - Added text validation to ensure that min_size is being checked
    a_input.setAttribute("field_info", "{label:'GL segment',types:[['text',' + ST.minlen + ',' + ST.len + ','typical_text','error'],['' + ST.type + '','error']]}");
    // Bug #6013 Mike O - Modified to use the new path to where GL # segment values are stored
    a_input.setAttribute("maxLength", ST.len); //Bug 10727 NJ - changed Length to capital L.
    a_input.setAttribute("id", "args.0." + NI + ".GL_nbr.segments." + i + ".chars");

   // Bug #6013 Mike O - Modified to use the new path to where GL # segment values are stored
    a_span = document.createElement("span");
    a_span.setAttribute("id", "args.0." + NI + ".GL_nbr.segments." + i + ".chars_error");
    a_span.className = "error";
    a_cell.appendChild(a_span);
  }

  // field_types[\'args.0.' + NI + '.GL_nbr.0\']={label:\'"GL Nbr" part 1\',types:[["numeric","error"]]};

  //lert("gl_segments:" + gl_segments); 
  /*Example: <DIV id="gl_numbers_expanded_6" name="gl_numbers_expanded" class="expanded">GL #<INPUT type="text" size="4" onfocus="do_focus(this)" name="args.0.6.GL_nbr.0" class="text GL_nbr" onblur="validate_tag(this);" value="" maxlength="3" id="args.0.6.GL_nbr.0"/><INPUT type="text" size="5" onfocus="do_focus(this)" name="args.0.6.GL_nbr.1" class="text GL_nbr" onblur="validate_tag(this);" value="" maxlength="4" id="args.0.6.GL_nbr.1"/><INPUT type="text" size="6" onfocus="do_focus(this)" name="args.0.6.GL_nbr.2" class="text GL_nbr" onblur="validate_tag(this);" value="" maxlength="5" id="args.0.6.GL_nbr.2"/><SPAN id="args.0.6.GL_nbr.0_error" class="error"></SPAN><SPAN id="args.0.6.GL_nbr.1_error" class="error"></SPAN><SPAN id="args.0.6.GL_nbr.2_error" class="error"></SPAN> */

  // End Bug #10264 Mike O
  //lert(a_tbody.innerHTML);
  return null;
}

//Bug 10473 NJ
// Bug #11548 Mike O - Cleaned up and fixed so unchecking select boxes will work
function AllocationOnChangeSelect(elementId) {
    //Bug 9263 NJ-store the previous state of the checkbox.this is used to avoid any confusion between the user
    //checking the checkbox and the checkbox being checked when qty/amount/description fields change.
        var onchange_value = true;
        onchange_value = onchange_value + ',' + document.getElementById(elementId + '.selected').value;
        document.getElementById(elementId + '.selected_onchange_value').value = onchange_value;     
     document.getElementById(elementId + '.selected_f_value').checked=true;
     document.getElementById(elementId + '.selected').value=true;                               
}

function AllocationOnChangeRecalculate(elementId)
{
  if ((document.getElementById(elementId + '.amount')!=null) && 
      (document.getElementById(elementId + '.total')!=null) &&
      (document.getElementById(elementId + '.quantity')!=null))
  { 
    var amount = document.getElementById(elementId + '.amount').value.replace('$', '').replace(new RegExp(',', 'g'), '');
    var isNegative = (amount.indexOf('(') != -1 && amount.indexOf(')') != -1);
    if (isNegative) 
    {
      amount = '-' + amount.replace('(', '').replace(')', '');
    } 
    document.getElementById(elementId + '.total').innerHTML =
    formatCurrency(amount * document.getElementById(elementId + '.quantity').value);
  }
  update_alloc_grand_total(); // Bug 14657 UMN add alloc totals
}

// Bug #11548 Mike O - Cleaned up and fixed so unchecking select boxes will work
function AllocationOnClick(elementId) {
 
  //Bug 9263 NJ-brought back hidden field to store the previous state of the checkbox. This is to 
  //avoid use case as stated in bug 9263
  var onchange_value = document.getElementById(elementId + '.selected_onchange_value').value;
  var arrOnChange_value = onchange_value.split(',');
  onchange_value = arrOnChange_value[0];
  var checkbox_prev_state = arrOnChange_value[1];
  if (onchange_value == 'true') {
    if (checkbox_prev_state == 'true' || checkbox_prev_state == '') {
      document.getElementById(elementId + '.selected_f_value').checked = false;
    }
    else {
      document.getElementById(elementId + '.selected_f_value').checked = true;
    }
    onchange_value = '';
  }
  var is_checked = document.getElementById(elementId + '.selected_f_value').checked;
  document.getElementById(elementId + '.selected').value = is_checked;
  onchange_value = onchange_value + ',' + is_checked;
  document.getElementById(elementId + '.selected_onchange_value').value = onchange_value;
     //end bug 9263
  validate_sub_items(this,elementId);
  if(is_checked) 
  { 
    AllocationOnChangeRecalculate(elementId);
  }
  else 
  { 
    document.getElementById(elementId + '.total').innerHTML=formatCurrency(formatNumber(0,2)); 
    update_alloc_grand_total(); // Bug 14657 UMN add alloc totals
  }
}
//End bug 10473 NJ

// Bug 14657 UMN add alloc totals
function update_alloc_grand_total()
{
  var jAllocsSelector = $('#allocations, #new_allocations');
  var total = 0.00;
  
  jAllocsSelector.each(function()
  {
    var jTrSelectors = $(this).find('tr.Allocation_htm_inst');
    jTrSelectors.each(function()
    {
      var jAmtSel = $(this).find('input.amount');
      var jQtySel = $(this).find('input.quantity');
      var jChkSel = $(this).find('input.selected_f_value');
      
      if (jAmtSel.length > 0 && jQtySel.length > 0 && jChkSel.length > 0 && jChkSel.is(":checked"))
      {
        var amount = jAmtSel.val().replace('$', '').replace(new RegExp(',', 'g'), '');
        var isNegative = (amount.indexOf('(') != -1 && amount.indexOf(')') != -1);
        if (isNegative) 
        {
          amount = '-' + amount.replace('(', '').replace(')', '');
        }
        total += amount * jQtySel.val();
      }
    });
  });
  $('H3.alloc_grand_total').text("Total: " + formatCurrency(total));
}

//BUG20173
function SetHiddenFieldOnClick(elementId) {
    var is_checked = document.getElementById(elementId + '.is_taxable_f_value').checked;
    document.getElementById(elementId + '.is_taxable').value = is_checked;
}

