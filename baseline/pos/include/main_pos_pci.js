//This file main_pos_pci.js
/*
  ------------ SCAN/SWIPE ENTRY ------------
*/
/* BUG 6207 PCI BLL: secure code dealing with credit card processing 
*                    Any changes to this code influence PCI certification
*/
var reading = false;
var keyCodeQueue = new Array();
var scan_string = "";
var start_code = -1;

var old_form = null;
var old_action = "";

var timer = null;

function reset_scan_string() {
  var a_element = window.document.activeElement;
  reading = false;
  is_reading=false; //Bug# 6188 ANU
  scan_string = "";
  if (old_form) old_form.action = old_action;
  start_code = -1;
}

// start of scan/swipe processing
var scan_string="";
var is_reading=false;
var ignoring_swipe = false;
var preh_mode = false; // Bug 22443 MJO

function swipe_must_be_completed () {
  //status+=" end swipe";
  if (is_reading==true)
  {
  alert("Start of swipe detected, but ending of swipe not found.");
  ignoring_swipe=false;
  reset_scan_string(); //Bug# 6188 ANU
  }
}

function check_for_not_swipe () {
  var the_scan=scan_string;
  if (the_scan.length < 3) {  // 4 is number of characters that indicates a swipe occurred in 100msec.
    is_reading=false;
    clearTimeout(swipe_must_be_completed_thread);
    var active_tag=window.document.activeElement;
    if (active_tag && (active_tag.tagName=="INPUT" || active_tag.tagName=="TEXTAREA")) {  // if cursor in a text input box, add scan_string because it wasn't actually a swipe.
      addScanToInput($(active_tag).attr("id"), the_scan);   //IPAY-668 NAM
    }   
  }
}

var end_keycode=null;
var swipe_must_be_completed_thread=null;
// Bug #10445 Mike O - Undid the changes I made to remove e.keyCode and fixed more bugs
function handle_keypress(e)
{
  //Bug 25507 SM - Removed fix for 24198.   
  if (ignoring_swipe) {
    return false;
  }
  
  if (is_reading) {
    // Bug 14882 MJO - Cross-browser friendly approach to get key code
    scan_string += String.fromCharCode(e.which || e.charCode || e.keyCode);
    //status+=" " + e.keyCode;

    // Bug 14882 MJO - Cross-browser friendly approach to get key code
    if (end_keycode==(e.which || e.charCode || e.keyCode)) {
      //status+=" saw END_KEYCODE"; 
      var the_scan=scan_string;
      if (isValidSwipe(the_scan) == false){  //IPAY-391 NAM: check swipe before post
        return false;
      }
      //status="SCAN_STRING1:" + the_scan; // alert
      // ignore_reading_remaining_tracks
      is_reading=false;
      ignoring_swipe=true;
      end_keycode=null;
      var form_data={};
      // Bug #10205 Mike O - Find source correctly
      var src = e.srcElement ? e.srcElement : e.target;
      if (src && src.value) {
        //Construct form data
        form_data[src.getAttribute("name")]=src.value;
      }
      device_swipe_receive(the_scan, form_data);
    }
    return false;
  } else {
    // Bug 14882 MJO - Cross-browser friendly approach to get key code
    var is_start_keycode = get_end_keycode(e.which || e.charCode || e.keyCode || 0); // returns false if not a starting keycode, returns ending keycode otherwise.
    if (is_start_keycode) { // new_char==start_char) {
      end_keycode=is_start_keycode;
      //status="START.end_keycode=" + end_keycode;
      is_reading=true;
      // Bug 14882 MJO - Cross-browser friendly approach to get key code
      scan_string=String.fromCharCode(e.which || e.charCode || e.keyCode); 
      //BUG 6006 needed to increase from 1000 to 2000 for PrehKeyTec MSR to work BLL 
      swipe_must_be_completed_thread=setTimeout("swipe_must_be_completed()",2000); // 400 too short.  500 seems to work.  1000 is conservative.
      setTimeout("check_for_not_swipe()",200); // 200  If start_char is entered by user, wait this long before assuming normal user input.
      return false;
    }
  }
  return true;
  }
  // Bug #12182 Mike O - Similar to handle_keypress, but for when you already have a complete string (mostly for the thick client)
  function handle_string_input(input) {
    var form_data = {};
    var src = get_top().main.document.activeElement;

    if (src) {
      if (get_end_keycode(input.charCodeAt(0)) == input.charCodeAt(input.length - 1)) {
        //Construct form data
        form_data[src.getAttribute("name")] = src.value;
      }
      else {
        src.value = input;
        return;
      }
    }
    
    device_swipe_receive(input, form_data);
  }

  // Called by: handle_keypress in js file
  // Bug #12762 Mike O - Added Preh manual key encryption support
  function device_swipe_receive (scan_code, form_data)
  {
  //alert(scan_code);
  // BUG# 8705 DH
  var data = "";
  // Bug#9100 Mike O
  if(scan_code.substring(0,7) == "@B_PREH")
    data = "1_PREH" + get_top().pos.ConvertToHex(scan_code.substring(8)); // BUG# 8705 DH - Disable Base64 and use Base16 //BUG Bug 23822 SH
  else if (scan_code.substring(0, 6) == "@003M%")// This is the second encryption version
      data = "2_PREH" + get_top().pos.ConvertToHex(scan_code.substring(6)); // BUG# 8705 DH - Disable Base64 and use Base16
  else if (scan_code.substring(0, 6) == "@003K%") { // Bug #12762 Mike O - Manual key encryption
    var credit_card_nbr_elements = get_top().main.$(".credit_card_nbr");
    if (credit_card_nbr_elements.length == 0 || $.inArray(get_top().main.document.activeElement, credit_card_nbr_elements[0]) != -1)
      return;
    
    data = "K_PREH" + get_top().pos.ConvertToHex(scan_code.substring(6));
  }
  else
    // Bug 21181 MJO - Encode in base64 so CASL doesn't choke on special characters
    data = Base64.encode(get_top().pos.Adumbrate(scan_code));// BUG# 8705 DH - Disable Base64 and use Base16
 
  if (!waiting) {
    // Bug 15301 MJO - Changed from GET to POST for security reasons
    var args = {
      e_data: data,
      form_data: form_data
    };

    post_remote(get_top().main.get_current_obj(), "Business_app.pos.device_swipe_receive", args, 'main');
  }
}
//BUG 6188 BLL
function escape2 (a_str) { 
  return escape(a_str).replace(/[+]/g,"%2B");  
}
//end BUG 6188
// returns matching end keycode or false if not a starting keycode
function get_end_keycode(starting_key_code) {
  // Bug 22443 MJO - Ignore asterisks when doing Preh manual entry
  if (preh_mode && starting_key_code == 42)
    return false;

  // Bug 19513 MJO - Override percent sign match when secure entry is enabled so Magtek can be caught
  if (starting_key_code == 37 && get_top().main.use_magtek) return 13;
  if (device_config[starting_key_code]) return device_config[starting_key_code]; else return false;
}

// following should be generated from configuration data
// Bug #12822 Mike O - Removed 42:42 (*:*) for Preh BUG18014
// Bug 26234 NK - Added backslash support (92)
var device_config = { 94: 94, 126: 38, 37: 63, 64: 13, 42: 42, 92: 92 };
//BUG 6006 added 126:13, removed 37:63
//BUG 6211 :removed 126:13(~:{ENTER}) and 37:13(%:{ENTER}), added back 37:63(%:?), added 64:13(@:{ENTER})

//IPAY-668 NAM: get curson location
function getInputSelection(el) {
  var start = 0, end = 0, normalizedValue, range,
      textInputRange, len, endRange;
  if (typeof el.selectionStart == "number" && typeof el.selectionEnd == "number") {
      start = el.selectionStart;
      end = el.selectionEnd;
  } else {
      range = document.selection.createRange();
      if (range && range.parentElement() == el) {
          len = el.value.length;
          normalizedValue = el.value.replace(/\r\n/g, "\n");
          // Create a working TextRange that lives only in the input
          textInputRange = el.createTextRange();
          textInputRange.moveToBookmark(range.getBookmark());
          // Check if the start and end of the selection are at the very end
          // of the input, since moveStart/moveEnd doesn't return what we want
          // in those cases
          endRange = el.createTextRange();
          endRange.collapse(false);
          if (textInputRange.compareEndPoints("StartToEnd", endRange) > -1) {
              start = end = len;
          } else {
              start = -textInputRange.moveStart("character", -len);
              start += normalizedValue.slice(0, start).split("\n").length - 1;
              if (textInputRange.compareEndPoints("EndToEnd", endRange) > -1) {
                  end = len;
              } else {
                  end = -textInputRange.moveEnd("character", -len);
                  end += normalizedValue.slice(0, end).split("\n").length - 1;
              }
          }
      }
  }
  return {
      start: start,
      end: end
  };
}
//IPAY-668 NAM: add scan character/string
function addScanToInput(id, str){
  //point to input control
  var input = document.getElementById(id);
  //get insert position
  var index = getInputSelection(input).start;
  //insert special scan character at insert location
  input.value=input.value.substr(0, index) + str + input.value.substr(index);
  //move cursor past inserted char
  input.setSelectionRange(index+1, index+1);
}
//IPAY-391 NAM: check if swipe contains only device_config characters, preamble and postamble
function isValidSwipe(scan){
  if (scan.length == 2 && scan[0]==scan[1] && (device_config[scan[0].charCodeAt()] != "undefined" ) )
    return false;

  return true;
}