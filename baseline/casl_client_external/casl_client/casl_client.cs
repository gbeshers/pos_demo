using System.Runtime.InteropServices;
using System;
using System.Threading;
using CASL_engine;
using System.Windows.Forms;

/* If error: Automation server can't create object 
 * casl_client > Properties > Configuration Properties > Output > Register for COM Interop = true
 * In the build output, should see: 
 *   Build complete -- 0 errors
 *   Building satellite assemblies
 *   Registering project output for COM Interop...
 * 

Error:
Could not copy temporary files to the output directory.
Solution: close browsers
*/

/*
Business_app.<defclass _name='mp_test'>
 <defmethod _name='htm_class'>
  <DIV>
   <SCRIPT> "parent.frames.pos.casl_client_init();" </SCRIPT>
   <!--<INPUT type="button" onclick="" value="Initialize CASL"/>-->
   <INPUT type="text" id="a_uri" value="http://localhost/pos_demo/cbc.htm?" size="50"/>
   <INPUT type="button" onclick="parent.frames.pos.casl_client_process_request_and_display(document.getElementById('a_uri').value);"
     value="Go"/>
  </DIV>
 </defmethod>
</defclass>
GEO.<set> mp_test=Business_app.mp_test </set>
mp_test.<to_htm_top/>
<A> href="http://localhost/pos_demo/ipayment.htm?app=mp_test" target="_blank" "Open window" </A>.<to_htm/>

------
moving CASL_engine/code_casl/*.casl (or *.casle) to CASL_engine.DLL
execute_file should load those files first.

Loading application and config files from across the network.
  Put all CASL files into a single .zip file and read the files in the .zip file,
  rather than loading each file separately over the network.

Database connection working from ActiveX?
  Used for loading from config database.

TODO: just load from file system for now.  Could point to Program Files/iPayment/CASL_engine/code_casl/*.casl
Manually set this path for now.

Change: CASL_get_code_base to get assembly URI like "assembly://CASL_engine"  get_content will get content.


ERROR:
Invalid procedure call or argument. ???

ERROR:
Object reference not set to an instance of an object.  ???

ISSUE:
CASL is not loading correctly because it can't find its CASL files.
How to put casl files into the DLL as resources.
How to modify execute_file to get at files from DLL.
Probably avoid loading things from the database during init.

*/

namespace casl_client
{
  #region IObjectSafety
  [ComImport]
  [Guid("CB5BDC81-93C1-11CF-8F20-00805F2CD064")] //IObjectSafety GUID
  [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
  interface IObjectSafety
  {
    [PreserveSig]
    int GetInterfaceSafetyOptions(ref Guid riid, out int 
      pdwSupportedOptions, out int pdwEnabledOptions);

    [PreserveSig]
    int SetInterfaceSafetyOptions(ref Guid riid, int dwOptionSetMask, 
      int dwEnabledOptions);
  }
  #endregion

      //{E9A22C1C-E048-4E63-B498-A17951C5CCFE} GUID FOR ALL TIME!
  [Guid("E9A22C1C-E048-4E63-B498-A17951C5CCFE")]
  public class casl_client : IObjectSafety
  {

    // JavaScript response objects
    private bool response = true;
    private object responseObject = null;

    // JavaScript execution target
    private mshtml.HTMLWindow2Class browser_window = null;

    public void iPayment_Peripherals() 
    {
    }

    public void Finalize() 
    {
      ///
    }

    /* public string simple_call()   //When calling from JavaScript, can only have a single method with a single signature.
    {
      return "1.5.0, " + System.Reflection.Assembly.GetExecutingAssembly().FullName + ", " +
        System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
    }*/
    public string simple_call(object browser_window_1)  // for debugging  // must be type object so it can be called from JavaScript
    {
      //System.Windows.Forms.MessageBox.Show("simple?" + browser_window.GetType().FullName );
      try 
      {
        ((mshtml.HTMLWindow2Class) browser_window_1).alert("In simple_call.");
      } 
      catch (Exception e) 
      {
        System.Windows.Forms.MessageBox.Show(e.Message + ", " + e.StackTrace);
      }
      return "1.5.0, " + System.Reflection.Assembly.GetExecutingAssembly().FullName + ", " +
         System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
    }

    // Initialize JavaScript execution target
    // called by: JavaScript casl_client_init in casl_client_main.js when in debug mode.

   // called by: JavaScript from casl_client.ipayment_client_start 
   public string initialize_casl_engine_client (mshtml.HTMLWindow2Class browser_window_1, string new_code_base)
   {
    try 
    {
     c_CASL.initialize_casl_engine_client(browser_window_1, new_code_base);
    } 
    catch (CASL_error ce) 
    {
     return "Error: " + ((GenericObject)ce.casl_object).get("message");
    }
     return "OK";
    }

    // JavaScript interface for JS Invoke method response
    public void Respond(object newResponseObject) 
    {
      responseObject = newResponseObject;
      response = true;
    }

    // called by: Javascript casl_client_main.js: casl_client_process_request_and_display
    public string process_request_and_return_html (string a_uri) 
    {
      string html_output="CASL is not initialized.";
      if (c_CASL.GEO!=null)
         html_output = CASL_server.process_request_and_return_html("a_uri",a_uri) as string;
      return html_output;
    }

    // Invokes a JavaScript interface function from CS
    // and returns the response.
    private object js_call(string function, params object[] args) 
    {
      string code = "js_execute(\"" + function + "(";
            
      for( int i = 0; i < args.Length; i++) 
      {
        code += ( (i > 0) ? ", " : "" )
          +  ( ( args[i].GetType().Equals( typeof(string) ) ) ?
          "'" + args[i] + "'" : args[i] );
      }

      code += ")\");";

      response = false;
      responseObject = null;
      browser_window.execScript(code, "JavaScript");
      WaitForResponse();

      return responseObject;
    }

    private object casl_client_call(string function, params object[] args) 
    {
      string code = "casl_client_execute(\"" + function + "(";
            
      for( int i = 0; i < args.Length; i++) 
      {
        code += ( (i > 0) ? ", " : "" )
          +  ( ( args[i].GetType().Equals( typeof(string) ) ) ?
          "'" + args[i] + "'" : args[i] );
      }

      code += ")\");";

      response = false;
      responseObject = null;
      browser_window.execScript(code, "JavaScript");
      WaitForResponse();

      return responseObject;
    }

    // Helper function that waits for the
    // JavaScript interface to respond.
    // Calling this at the wrong time would
    // hang the program.
    private void WaitForResponse() 
    {
      while(!response) 
      {
        System.Threading.Thread.Sleep(0);
      }
    }
    #region IObjectSafety Members
    // Constants for implementation of the IObjectSafety interface.
    private const int INTERFACESAFE_FOR_UNTRUSTED_CALLER = 0x00000001;
    //private const int INTERFACESAFE_FOR_UNTRUSTED_DATA = 0x00000002; //We aren't marked as safe for data
    private const int S_OK = 0;

    // Implementation of the IObjectSafety methods.
    int IObjectSafety.GetInterfaceSafetyOptions(ref Guid riid, out int pdwSupportedOptions, out int pdwEnabledOptions)
    {
      pdwSupportedOptions = INTERFACESAFE_FOR_UNTRUSTED_CALLER; //| INTERFACESAFE_FOR_UNTRUSTED_DATA;
      pdwEnabledOptions = INTERFACESAFE_FOR_UNTRUSTED_CALLER;// | INTERFACESAFE_FOR_UNTRUSTED_DATA;
      return S_OK;   // return S_OK
    }

    int IObjectSafety.SetInterfaceSafetyOptions(ref Guid riid, int 
      dwOptionSetMask, int dwEnabledOptions)
    {
      return S_OK;   // return S_OK
    }

    #endregion
  }
}
