README for developers

test_app is a Windows-based app that makes it easier to debug because you can set breakpoints.
casl_client is a lightweight ActiveX wrapper called from JavaScript calls that calls process_request in CASL_server.cs
casl_client_plugin is currently not used, but is intended to create an ActiveX plugin.
casl_client_setup is used to make a release.
  The files in casl_client_setup/Release are created when you build casl_client_setup.
  When you "Install" casl_client_setup (from the right-click menu in VS.NET), it will
  launch a standard Windows installation tool that will put dll files into Program Files/CORE/CORE iPayment

For building:
 In the solution, build all will not build casl_client_setup.  Do that explicitly.