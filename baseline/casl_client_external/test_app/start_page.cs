using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using mshtml;
using CASL_engine;

/* http://blog.monstuff.com/archives/000052.html
 * In the "Customize Toolbox..." context menu (on the Toolbox), 
 * pick "Microsoft Web Browser" from the COM components list. 
 * This will add an "Explorer" control in the "General" section of the Toolbox.
*/

namespace test_app
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class test_app : System.Windows.Forms.Form
	{
  private System.Windows.Forms.TextBox source_output;
  private System.Windows.Forms.Button uri_request_and_return_html;
  private System.Windows.Forms.TextBox a_uri;
  private System.Windows.Forms.Button init_casl;
  private AxSHDocVw.AxWebBrowser web_browser;
  private System.Windows.Forms.TextBox uri_address;
  private System.Windows.Forms.Button navigate_to;
  private System.Windows.Forms.TextBox ide_buffer;
  private System.Windows.Forms.Button execute_and_format;
  private System.Windows.Forms.TextBox format;
  /// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public test_app()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
   System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(test_app));
   this.init_casl = new System.Windows.Forms.Button();
   this.uri_request_and_return_html = new System.Windows.Forms.Button();
   this.a_uri = new System.Windows.Forms.TextBox();
   this.source_output = new System.Windows.Forms.TextBox();
   this.web_browser = new AxSHDocVw.AxWebBrowser();
   this.uri_address = new System.Windows.Forms.TextBox();
   this.navigate_to = new System.Windows.Forms.Button();
   this.ide_buffer = new System.Windows.Forms.TextBox();
   this.execute_and_format = new System.Windows.Forms.Button();
   this.format = new System.Windows.Forms.TextBox();
   ((System.ComponentModel.ISupportInitialize)(this.web_browser)).BeginInit();
   this.SuspendLayout();
   // 
   // init_casl
   // 
   this.init_casl.Location = new System.Drawing.Point(760, 8);
   this.init_casl.Name = "init_casl";
   this.init_casl.Size = new System.Drawing.Size(96, 23);
   this.init_casl.TabIndex = 0;
   this.init_casl.Text = "Initialize CASL";
   this.init_casl.Visible = false;
   this.init_casl.Click += new System.EventHandler(this.init_casl_Click);
   // 
   // uri_request_and_return_html
   // 
   this.uri_request_and_return_html.Location = new System.Drawing.Point(288, 16);
   this.uri_request_and_return_html.Name = "uri_request_and_return_html";
   this.uri_request_and_return_html.Size = new System.Drawing.Size(96, 23);
   this.uri_request_and_return_html.TabIndex = 1;
   this.uri_request_and_return_html.Text = "Go";
   this.uri_request_and_return_html.Click += new System.EventHandler(this.uri_request_and_return_html_Click);
   // 
   // a_uri
   // 
   this.a_uri.Location = new System.Drawing.Point(8, 16);
   this.a_uri.Name = "a_uri";
   this.a_uri.Size = new System.Drawing.Size(272, 20);
   this.a_uri.TabIndex = 2;
   this.a_uri.Text = "http://localhost/pos_demo/reload.htm?";
   // 
   // source_output
   // 
   this.source_output.Location = new System.Drawing.Point(8, 272);
   this.source_output.Multiline = true;
   this.source_output.Name = "source_output";
   this.source_output.ScrollBars = System.Windows.Forms.ScrollBars.Both;
   this.source_output.Size = new System.Drawing.Size(400, 216);
   this.source_output.TabIndex = 3;
   this.source_output.Text = "CASL not initialized.";
   // 
   // web_browser
   // 
   this.web_browser.Enabled = true;
   this.web_browser.Location = new System.Drawing.Point(416, 40);
   this.web_browser.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("web_browser.OcxState")));
   this.web_browser.Size = new System.Drawing.Size(440, 448);
   this.web_browser.TabIndex = 4;
   // 
   // uri_address
   // 
   this.uri_address.Location = new System.Drawing.Point(424, 8);
   this.uri_address.Name = "uri_address";
   this.uri_address.Size = new System.Drawing.Size(216, 20);
   this.uri_address.TabIndex = 5;
   this.uri_address.Text = "http://localhost/pos_demo/cbc.htm?";
   // 
   // navigate_to
   // 
   this.navigate_to.Location = new System.Drawing.Point(648, 8);
   this.navigate_to.Name = "navigate_to";
   this.navigate_to.TabIndex = 6;
   this.navigate_to.Text = "Go to URI";
   this.navigate_to.Click += new System.EventHandler(this.navigate_to_Click);
   // 
   // ide_buffer
   // 
   this.ide_buffer.Location = new System.Drawing.Point(8, 48);
   this.ide_buffer.Multiline = true;
   this.ide_buffer.Name = "ide_buffer";
   this.ide_buffer.ScrollBars = System.Windows.Forms.ScrollBars.Both;
   this.ide_buffer.Size = new System.Drawing.Size(400, 184);
   this.ide_buffer.TabIndex = 7;
   this.ide_buffer.Text = "\"CASL IDE\"";
   // 
   // execute_and_format
   // 
   this.execute_and_format.Location = new System.Drawing.Point(312, 240);
   this.execute_and_format.Name = "execute_and_format";
   this.execute_and_format.Size = new System.Drawing.Size(96, 23);
   this.execute_and_format.TabIndex = 8;
   this.execute_and_format.Text = "Execute";
   this.execute_and_format.Click += new System.EventHandler(this.execute_and_format_Click);
   // 
   // format
   // 
   this.format.Location = new System.Drawing.Point(248, 240);
   this.format.Name = "format";
   this.format.Size = new System.Drawing.Size(56, 20);
   this.format.TabIndex = 9;
   this.format.Text = "to_inspect";
   // 
   // test_app
   // 
   this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
   this.ClientSize = new System.Drawing.Size(864, 526);
   this.Controls.Add(this.format);
   this.Controls.Add(this.ide_buffer);
   this.Controls.Add(this.uri_address);
   this.Controls.Add(this.source_output);
   this.Controls.Add(this.a_uri);
   this.Controls.Add(this.execute_and_format);
   this.Controls.Add(this.navigate_to);
   this.Controls.Add(this.web_browser);
   this.Controls.Add(this.uri_request_and_return_html);
   this.Controls.Add(this.init_casl);
   this.Name = "test_app";
   this.Text = "CASL test";
   this.Load += new System.EventHandler(this.casl_app_Load);
   ((System.ComponentModel.ISupportInitialize)(this.web_browser)).EndInit();
   this.ResumeLayout(false);

  }
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new test_app());
		}

  private void casl_app_Load(object sender, System.EventArgs e)
  {
    init_casl_Click(sender,e);
  }

  private void init_casl_Click(object sender, System.EventArgs e)
  {
   c_CASL.init(); // "C:/T4/pos_demo/");
   misc.CASL_call("a_method","session_start", "_subject",c_CASL.GEO.get("My"));  // might move this to c_CASL.init
   object o = null;
   web_browser.Navigate("about:blank", ref o, ref o, ref o, ref o);
   source_output.Text = "CASL Initialized.";
  }

  private void uri_request_and_return_html_Click(object sender, System.EventArgs e)
  {
    string uri=a_uri.Text;
    source_output.Text="Accessing " + uri + ".  Please wait...";
    string output_html = CASL_server.process_request_and_return_html(
      "a_uri",a_uri.Text, "http_method","GET", "content_type",null, "content",null
    ) as string; // return a server
    source_output.Text=output_html;
    // send to browser
    IHTMLDocument2 html_doc = (IHTMLDocument2)web_browser.Document;
    IHTMLElement body = (IHTMLElement)html_doc.body;
    body.innerHTML = output_html;
  }

  private void navigate_to_Click(object sender, System.EventArgs e)
  {
   object o = null;
   web_browser.Navigate(uri_address.Text, ref o, ref o, ref o, ref o);
  }

  private void execute_and_format_Click(object sender, System.EventArgs e)
  {
   string source=ide_buffer.Text;
   // prepare for browser output
   IHTMLDocument2 html_doc = (IHTMLDocument2)web_browser.Document;
   IHTMLElement body = (IHTMLElement)html_doc.body;
   try 
   {
    GenericObject a_response=CASL_engine.misc.IDE_execute_and_format("source",source, "output_format","htm", "escape_tags",false) as GenericObject;
    string output_html=misc.CASL_call("subject",a_response.get("result"), "a_method",format.Text) as string;
    source_output.Text=output_html;
    body.innerHTML = output_html;
   } 
   catch (Exception ex) 
   {
    body.innerHTML = "Error occurred.";
   }
  }

//  private void web_browser_DocumentComplete(object sender, AxSHDocVw.DWebBrowserEvents2_NavigateComplete2Event e)
//  {
//   IHTMLDocument2 html_doc = (IHTMLDocument2)web_browser.Document;
//   IHTMLElement body = (IHTMLElement)html_doc.body;
//   body.innerHTML = "<html><body>hello world</body></html>";
//  }
	}
}
