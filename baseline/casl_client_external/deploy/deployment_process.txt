Building and deploying T4 Client

Scenario: Change in casl files
Resolution: Reload clients

---

Scenario: Change in casl_client, CASL_engine or other dependency DLL
Resolution:
In solution named baseline/casl_client_external
1.  Increment the casl_client_setup project version number
    - casl_client_setup->properties pane->version
2.  Check to make sure the web bootstrapper points to the correct URL
    - casl_client_setup->right click->properties->web bootstrapper settings
3.  Recompile/rebuild the casl_client_setup project
    This puts casl_client_setup.msi, Setup.Exe into:
       /pos_demo/baseline/casl_client_external/casl_client_setup/Debug
4.  Run casl_client_external\deploy.bat
    That moves the msi and exe files and updated their timestamp.  Location:
       T4/baseline/casl_client_external/deploy
5.  Deploy the files left in the casl_client_external\deploy folder to the server

Use T4 Config to say where client is found.  Default is fine for HTTP.

For Site-specific distributions that use HTTPS, need to have Paul C. move the file to another server and have the URL specified.
 
ERRORS: ---------- also see support.txt
  
Error when building casl_client_setup: "General failure building bootstrapper"
1.  The timestamp server is nonresponsive
2.  You have the wrong password for a key
3.  Anything else went wrong...
Password for test keys is eroceroc

Never ever ever fail to increment the casl_client_setup project's version number when you deploy!  Bad Things will happen.