using System;
using System.IO;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using CASL_engine;
using System.Text;

namespace ACHAcctVerification
{
  /// <summary>
  /// Used by BankLookup controller
  /// </summary>
  public class BankLookupPOST
  {
    //  MSD-188 DH - Styling changes
    public string tierName { get; set; }
    public string routingNumber { get; set; }
    public string accountNumber { get; set; }
    public string merchantId { get; set; }
    public string accountType { get; set; }//MSD-158 Add support for account type to ACH Service
    public bool forceLookup { get; set; }
    public string platform { get; set; }//MSD-182 Add platform as a parameter to ACH Service
    public string platformInterface { get; set; }//Bug MSD-197 iPayment send appropriate platform_interface based upon usage context
  }

  /// <summary>
  /// IPAY-34 DH - Add system interface for ACH Account verification service
  /// Copy of the original object in the ACH Account Verification Service.
  /// </summary>
  public class ServiceTier
  {
    public int ID { get; set; }
    public string Name { get; set; }
    public int Active { get; set; }
    public string Description { get; set; }
  }

  /// <summary>
  /// IPAY-34 DH - Add system interface for ACH Account verification service
  /// 
  /// Copy of the original object in the ACH Account Verification Service.
  /// If we have too many issues with the data in this class changing, we should just use a dynamic object and get the fields we need.
  /// </summary>
  public class ServiceResponse
  {
    public string code { get; set; }
    public string riskLevel { get; set; }
    public string message { get; set; }
    public string errorMessage { get; set; }// Internal error / Service call failure, etc.
    public string itemReferenceId { get; set; }// Changed to long per the original GIACT response - DH.
    public string basicAdverseActionNotice { get; set; }// MicroBILT
    public string premiumAdverseActionNotice { get; set; }// GIACT
    public bool cashedResponse { get; set; }// Indicates that data came from the in-house database.
    public string dataSource { get; set; }// Basic, Premium, Stub
  }

  /// <summary>
  /// IPAY-34 DH - Add system interface for ACH Account verification service
  /// </summary>
  public class ACHAcctVerification
  {
    private static HttpClient StaticHTTPClient = new HttpClient();// .NET 5 would let us reuse this between calls.

    public ACHAcctVerification()
    {
      StaticHTTPClient.Timeout = new TimeSpan(0, 0, 15);// If it takes 15 seconds then we have a problem.
    }

    public async Task CancelRequest()
    {
      // IPAY-31 DH
      // No need to return anything to iPayment.

      try
      {
        StaticHTTPClient?.CancelPendingRequests();
      }
      catch (Exception e)
      {
        Logger.LogError($"Exception: {ResolveException(e)}", "ACHAcctVerification.CancelRequest");
      }

      await Task.Delay(10);// Eat the async
    }

    /// <summary>
    /// MSD-99 DH - Retrieve JSON output of all tiers status codes and user messages 
    /// 
    /// Get all available service tiers and insert them into the available tiers combo box in the ACHAcctVerification system interface.
    /// </summary>
    /// <param name="argPairs"></param>
    /// <returns>String: true/false</returns>
    public static GenericObject GetAvailableServiceTiers(params object[] argPairs)
    {
      GenericObject geoTierList = new GenericObject();
      string strError = "Unable to retrieve status codes from the ACH Account Verification service.";

      try
      {
        List<ServiceTier> TierList = GetAvailableServiceTiersAsync(argPairs).Result;

        if (TierList != null)
        {
          geoTierList = new GenericObject();
          foreach (ServiceTier st in TierList)
            geoTierList.insert(st.Name);
        }
        else
        {
          // Add the default mode STUB. If tears are loaded, the BASIC stub will be the default.
          geoTierList.insert("Stub");
          Logger.LogError(strError, "ACHAcctVerification.GetAvailableServiceTiers");
        }
      }
      catch(Exception e)
      {
        Logger.LogError($"{strError} Error:{ResolveException(e)}", "ACHAcctVerification.GetAvailableServiceTiers");
      }

      return geoTierList;// It will be empty in case of issues.
    }

    /// <summary>
    /// IPAY-34 DH - Add system interface for ACH Account verification service
    /// 
    /// Return all tiers in a string format for the UI combo box display.
    /// </summary>
    /// <returns>List of tiers</returns>
    public static async Task<List<ServiceTier>> GetAvailableServiceTiersAsync(params object[] argPairs)
    {
      List<ServiceTier> TierList = null;
      string strError = "Unable to retrieve available service tiers from the ACH Account Verification service.";

      try
      {
        GenericObject args = misc.convert_args(argPairs);
        GenericObject workgroup = (GenericObject)args.get("workgroup");
        string strServiceBaseAddress = (string)workgroup.get("base_uri");
        string strServiceEndpointAddress = (string)workgroup.get("tiers_uri");

        if (String.IsNullOrEmpty(strServiceEndpointAddress) || String.IsNullOrEmpty(strServiceBaseAddress))
        {
          Logger.LogError($"{strError} Missing endpoint address.", "ACHAcctVerification.GetAvailableServiceTiersAsync");
          return null;
        }

        HttpResponseMessage httpResponse = null;
        httpResponse = await StaticHTTPClient.GetAsync($"{strServiceBaseAddress}/{strServiceEndpointAddress}");

        if (httpResponse.IsSuccessStatusCode)
        {
          StreamReader reader = new StreamReader(await httpResponse.Content.ReadAsStreamAsync());
          TierList = JsonConvert.DeserializeObject<List<ServiceTier>>(reader.ReadToEnd());
        }
        else
        {
          Logger.LogError($"{strError} Headers: {httpResponse.Headers.ToString()}", "ACHAcctVerification.GetAvailableServiceTiersAsync");
          TierList = null;
        }

        if (TierList == null)
        {
          Logger.LogError(strError, "ACHAcctVerification.GetAvailableServiceTiersAsync");
          TierList = null;
        }
      }
      catch(Exception e)
      {
        Logger.LogError($"Exception thrown when retrieving available tiers from the ACH Account Verification service. Error: {ResolveException(e)}", "ACHAcctVerification.GetAvailableServiceTiersAsync");
        TierList = null;
      }

      return TierList;
    }

    /// <summary>
    /// IPAY-34 DH - Add system interface for ACH Account verification service
    /// </summary>
    /// <param name="argPairs"></param>
    /// <returns>GenericObject with error or success or failure with the correct end user message</returns>
    public static GenericObject VerifyRTN(params object[] argPairs)
    {
      try
      {
        GenericObject args = misc.convert_args(argPairs);       
      }
      catch (Exception e)
      {
        Logger.LogError($"Unable to verify the account. Exception: {ResolveException(e)}", "ACHAcctVerification.VerifyRTN");
      }

      return VerifyRTNAsync(argPairs).Result;
    }

    /// <summary>
    /// MDS-85 DH - Implement stubs for the ACH Account Verification Service.
    /// </summary>
    /// <param name="strRTN"></param>
    /// <param name="strAcct"></param>
    /// <returns></returns>
    public static ServiceResponse LocalStub(string strRTN, string strAcct)
    {
      ServiceResponse sr = new ServiceResponse();
      sr.dataSource = "STUB";
      sr.cashedResponse = true;

      try
      {
        switch (strAcct)
        {
          case "5000000001":
            sr.itemReferenceId = "5027363008";
            sr.riskLevel = "1";
            sr.message = "";
            sr.errorMessage = "";
            break;

          case "5000000002":
            sr.itemReferenceId = "5027363009";
            sr.riskLevel = "3";
            sr.message = "";
            sr.errorMessage = "";
            break;

          case "5000000003":
            sr.itemReferenceId = "5027363017";
            sr.riskLevel = "3";
            sr.message = "";
            sr.errorMessage = "";
            break;

          case "5000000004":
            sr.itemReferenceId = "5027363018";
            sr.riskLevel = "3";
            sr.message = "";
            sr.errorMessage = "";
            break;

          case "5000000005":
            sr.itemReferenceId = "5027363020";
            sr.riskLevel = "3";
            sr.message = "";
            sr.errorMessage = "";
            break;

          case "5000000006":
            sr.itemReferenceId = "5027363021";
            sr.riskLevel = "3";
            sr.message = "";
            sr.errorMessage = "";
            break;

          case "5000000007":
            sr.itemReferenceId = "5027363022";
            sr.riskLevel = "3";
            sr.message = "";
            sr.errorMessage = "";
            break;

          case "5000000008":
            sr.itemReferenceId = "5027363023";
            sr.riskLevel = "1";
            sr.message = "";
            sr.errorMessage = "";
            break;

          case "5000000009":
            sr.itemReferenceId = "5027363024";
            sr.riskLevel = "1";
            sr.message = "";
            sr.errorMessage = "";
            break;

          case "5000000010":
            sr.itemReferenceId = "5027363025";
            sr.riskLevel = "2";
            sr.message = "";
            sr.errorMessage = "";
            break;

          case "5000000011":
            sr.itemReferenceId = "5027363026";
            sr.riskLevel = "3";
            sr.message = "";
            sr.errorMessage = "";
            break;

          case "5000000012":
            sr.itemReferenceId = "5027363028";
            sr.riskLevel = "3";
            sr.message = "";
            sr.errorMessage = "";
            break;

          case "5000000013":
            sr.itemReferenceId = "5027363029";
            sr.riskLevel = "2";
            sr.message = "";
            sr.errorMessage = "";
            break;

          case "5000000014":
            sr.itemReferenceId = "5027363030";
            sr.riskLevel = "2";
            sr.message = "";
            sr.errorMessage = "";
            break;

          case "5000000015":
            sr.itemReferenceId = "5027363032";
            sr.riskLevel = "2";
            sr.message = "";
            sr.errorMessage = "";
            break;

          case "5000000016":
            sr.itemReferenceId = "5027363033";
            sr.riskLevel = "0";
            sr.message = "";
            sr.errorMessage = "";
            break;

          case "5000000017":
            sr.itemReferenceId = "5027363035";
            sr.riskLevel = "0";
            sr.message = "";
            sr.errorMessage = "";
            break;

          case "5000000018":
            sr.itemReferenceId = "5027363036";
            sr.riskLevel = "0";
            sr.message = "";
            sr.errorMessage = "";
            break;

          case "5000000019":
            sr.itemReferenceId = "5027363037";
            sr.riskLevel = "0";
            sr.message = "";
            sr.errorMessage = "";
            break;

          case "5000000020":
            sr.itemReferenceId = "5027363039";
            sr.riskLevel = "0";
            sr.message = "";
            sr.errorMessage = "";
            break;

          case "5000000021":
            sr.itemReferenceId = "5027363044";
            sr.riskLevel = "0";
            sr.message = "";
            sr.errorMessage = "";
            break;

          case "5000000022":
            sr.itemReferenceId = "5027363049";
            sr.riskLevel = "0";
            sr.message = "";
            sr.errorMessage = "";
            break;

          default:// Indication that the call worked but the account number was not out of range.
            sr.itemReferenceId = null;
            sr.riskLevel = "";
            sr.message = "";
            sr.errorMessage = "ACCOUNT NUMBER NOT COVERED BY THE STUB MODE";
            Logger.LogError($"STUB processed. Account number error. Account: {strAcct} out of the stub mode range.", "ACHAcctVerification.LocalStub");
            break;      
        }
      }
      catch (Exception e)
      {
        Logger.LogError($"Exception: {ResolveException(e)}", "ACHAcctVerification.LocalStub");
        return null;
      }

      return sr;
    }

    /// <summary>
    /// MSD-85 DH
    /// 
    /// Log the stub data processed.
    /// </summary>
    /// <param name="StubData"></param>
    public static void LogStub(GenericObject StubData)
    {
      StringBuilder sb = new StringBuilder();
      try
      {
        sb.Append($"riskLevel: {StubData.get("riskLevel")}\n");
        sb.Append($"message: {StubData.get("message")}\n");
        sb.Append($"errorMessage: {StubData.get("errorMessage")}\n");
        sb.Append($"itemReferenceId: {StubData.get("itemReferenceId")}\n");
        sb.Append($"basicAdverseActionNotice: {StubData.get("basicAdverseActionNotice")}\n");
        sb.Append($"premiumAdverseActionNotice: {StubData.get("premiumAdverseActionNotice")}\n");
        sb.Append($"cashedResponse: {StubData.get("cashedResponse")}\n");
        sb.Append($"dataSource: {StubData.get("dataSource")}\n");
        Logger.cs_log($"ACH Account Verification STUB Processed {sb.ToString()}");
      }
      catch(Exception)
      {
        // No need here
      }
    }

    /// <summary>
    /// IPAY-34 DH - Add system interface for ACH Account verification service
    /// 
    /// Verify state of the account based on configuration and established rules.
    /// </summary>
    /// <param name="argPairs"></param>
    /// <returns>GenericObject with error or success or failure with the correct end user message</returns>
    public static async Task<GenericObject> VerifyRTNAsync(params object[] argPairs)
    {
      ServiceResponse serviceResponse = null;
      HttpResponseMessage httpResponse = null;
      GenericObject geoResults = new GenericObject();

      try
      {
        GenericObject args = misc.convert_args(argPairs);
        GenericObject workgroup = (GenericObject)args.get("workgroup");

        string strServiceBaseAddress = (string)workgroup.get("ach_base_uri");
        string strServiceEndpointAddress = (string)workgroup.get("ach_verify_rtn_uri");
        string strTierName = (string)workgroup.get("ach_available_tiers");// Selected tier
        string strRoutingNumber = (string)args.get("routing_nbr");
        string strBankAccount = (string)args.get("bank_acct");
        string strAcctType = (string)args.get("acct_type"); //MSD-158 Add support for account type to ACH Service-
        string strMerchantTracking = (string)workgroup.get("ach_merchant_tracking");// MSD-130 DH - Merchant tracking for reporting purposes
        string strForceLookup = (string)workgroup.get("ach_force_lookup");// MSD-130 DH - Merchant tracking for reporting purposes
        string strPlatform = (string)args.get("platform"); //MSD-182 Add platform as a parameter to ACH Service
        string strPlatformInterface = (string)args.get("platform_interface");  //MSD-197 iPayment send appropriate platform_interface based upon usage context
        
        bool bForceLookup = strForceLookup == "Yes" ? true : false;
        //------------------------------------------------------------------------
        // MDS - 85 DH - Implement stubs for the ACH Account Verification Service.
        // Special case of local stub.
        if (strTierName.ToLower().Equals("stub") && strRoutingNumber.Equals("122105278") && strBankAccount.StartsWith("5"))
        {
          serviceResponse = LocalStub(strRoutingNumber, strBankAccount);
          if (serviceResponse != null)
          {
            geoResults = ConvertToGeo(serviceResponse);

            // Log the processed stub
            LogStub(geoResults);
          }
          else
          {
            // I think that due to business center error handling, c_CASL.c_make causes an issue when trying to create a regular CASL error object.
            // I set it just like another GEO and then handle it in the CASL caller method. 
            geoResults = new GenericObject("error", "true", "code", "ACHAVS-Error1", "message", "Unable to process the STUB ACH verification at this time.", "title", "STUB - ACH Account Verification Error", "throw", false);
          }
          return geoResults;
        }
        // End MDS - 85 DH - Implement stubs for the ACH Account Verification Service.
        //------------------------------------------------------------------------

        // POST request for security
        BankLookupPOST post = new BankLookupPOST();
        post.tierName = strTierName;
        post.routingNumber = strRoutingNumber;
        post.accountNumber = strBankAccount;
        post.merchantId = strMerchantTracking;
        post.accountType = strAcctType; //MSD-158 Add support for account type to ACH Service
        post.forceLookup = bForceLookup;
        post.platform = strPlatform;
        post.platformInterface = strPlatformInterface;
        var json = JsonConvert.SerializeObject(post);
        var data = new StringContent(json, Encoding.UTF8, "application/json");
        
        // MSD-130 DH - Merchant tracking for reporting purposes
        // MSD-119 DH - Force third - party lookup instead of using the cached data
        httpResponse = await StaticHTTPClient.PostAsync($"{strServiceBaseAddress}/{strServiceEndpointAddress}", data);
        if (httpResponse.IsSuccessStatusCode)
        {
          StreamReader reader = new StreamReader(await httpResponse.Content.ReadAsStreamAsync());
          serviceResponse = JsonConvert.DeserializeObject<ServiceResponse>(reader.ReadToEnd());
          geoResults = ConvertToGeo(serviceResponse);
        }
        else
        {
          // I think that due to business center error handling, c_CASL.c_make causes an issue when trying to create a regular CASL error object.
          // I set it just like another GEO and then handle it in the CASL caller method. 
          geoResults = new GenericObject("error", "true", "code", "ACHAVS-Error3", "message", "Unable to verify your account at this time. Please try again later.", "title", "ACH Account Verification Error", "throw", false);
          Logger.LogError($"Unable to verify the account.{httpResponse.Headers.ToString()}", "ACHAcctVerification.VerifyRTNAsync");
        }

        // MSD-188 DH - Add logging for auditing purposes
        ReportToActivityLog("Successful ACH verification service call", strPlatform, strPlatformInterface, geoResults);
      }
      catch (Exception e)
      {
        // I think that due to business center error handling, c_CASL.c_make causes an issue when trying to create a regular CASL error object.
        // I set it just like another GEO and then handle it in the CASL caller method. 

        // iPayment will react to these by throwing an UI message 
        geoResults = new GenericObject("error", "true", "code", "ACHAVS-Error2", "message", "Unable to verify your account at this time. Please try again later.", "title", "ACH Account Verification Error", "throw", false);
        Logger.LogError($"Unable to verify the account. Exception: {ResolveException(e)}", "ACHAcctVerification.VerifyRTNAsync");
      }

      return geoResults;
    }

    /// <summary>
    /// MSD-188 DH - Add logging for auditing purposes
    /// </summary>
    /// <param name="strSummary"></param>
    /// <param name="strPlatform"></param>
    /// <param name="strPlatformInterface"></param>
    /// <param name="data"></param>
    private static void ReportToActivityLog(string strSummary/*We may also need to pass error*/, string strPlatform, string strPlatformInterface, GenericObject data)
    {
      try
      {
        StringBuilder sb = new StringBuilder();
        sb.Append(" Code: [" + data.get("Code") + "]");
        sb.Append(" riskLevel: [" + data.get("riskLevel") + "]");
        sb.Append(" itemReferenceId: [" + data.get("itemReferenceId") + "]");
        sb.Append(" cashedResponse: [" + data.get("cashedResponse") + "]");
        sb.Append(" dataSource: [" + data.get("dataSource") + "]");
        sb.Append(" PlatformInterface: [" + strPlatformInterface + "]");
        sb.Append(" message: [" + data.get("message") + "]");

        misc.CASL_call("a_method", "actlog", "args",
          new GenericObject(
            "user", strPlatform,
            "app", "ACH Verification Service",
            "action", strSummary,
            "summary", "Check verification attempted",
            "detail", sb.ToString()));
      }
      catch(Exception)
      {
        // Don't do anything here. 
      }
    }

    /// <summary>
    /// IPAY-34 DH - Add system interface for ACH Account verification service
    /// 
    /// Return the proper message from the exception.
    /// We could return the full exception with ToString(), but that would return too much info.
    /// </summary>
    /// <param name="e"></param>
    /// <returns>Exception.Message</returns>
    private static string ResolveException(Exception e)
    {
      string strError = "";

      if (e != null)
      {
        if (e.InnerException != null)
          strError = e.InnerException.Message;
        else
          strError = e.Message;
      }

      return strError;
    }

    /// <summary>
    /// IPAY-34 DH - Add system interface for ACH Account verification service
    /// 
    /// Convert the ServiceResponse class to GenericObject class for CASL processing.
    /// </summary>
    /// <param name="serviceResponse">ServiceResponse</param>
    /// <returns>GenericObject</returns>
    private static GenericObject ConvertToGeo(ServiceResponse serviceResponse)
    {
      GenericObject genericObject = null;

      try
      {
        if (serviceResponse != null)
        {
          genericObject = new GenericObject();
          genericObject.Add("Code", serviceResponse.code);
          genericObject.Add("riskLevel", serviceResponse.riskLevel);
          genericObject.Add("message", serviceResponse.message);
          genericObject.Add("errorMessage", serviceResponse.errorMessage);
          genericObject.Add("itemReferenceId", serviceResponse.itemReferenceId);
          genericObject.Add("basicAdverseActionNotice", serviceResponse.basicAdverseActionNotice);
          genericObject.Add("premiumAdverseActionNotice", serviceResponse.premiumAdverseActionNotice);
          genericObject.Add("cashedResponse", serviceResponse.cashedResponse);
          genericObject.Add("dataSource", serviceResponse.dataSource);
        }
      }
      catch(Exception e)
      {
        Logger.LogError($"Unable to verify the account. Exception: {ResolveException(e)}", "ACHAcctVerification.VerifyRTNAsync");
        throw new ArgumentException(message: ResolveException(e));
      }

      return genericObject;
    }
  }
}
