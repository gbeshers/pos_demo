﻿<do>
  <!-- Bug 18129 DJD - This SI is used to store the eWallet web service properties -->
  System_interface.<defclass _name='eWallet'>
    name="eWallet"
    description="Module"
    category="Web"
    kind="inquiry" <!-- Bug 18912 Added host_system_ID_2_tagname -->

    endpoint_address_f_config_prompt = "The URL used to communicate with the eWallet web service (e.g., http://mysite/eWalletService/WalletDBService.svc)."
    endpoint_address=req=String

    repository_f_config_prompt = "Credit Card Payments - PAYWARE: Create customers and contracts., PAYWARE_TOKEN: Create tokens., CORE: Data is stored locally."
    repository = "PAYWARE" = <one_of> "PAYWARE" "PAYWARE_TOKEN" <!-- "CORE"--> </one_of>

    payware_token_renewal_days_f_config_prompt = "PAYWARE_TOKEN repository: Update PAYware token expiration dates by (Today + Number of Renewal Days)."
    payware_token_renewal_days=0=Type.numeric_positive

    CORE_repository_endpoint_address_f_config_prompt = "The URL used to communicate with the CORE Repository web service (e.g., http://mysite/Repository/RepositoryService.svc)."
    CORE_repository_endpoint_address=opt=String

    is_log = "N" = <one_of> "Y" "N" </one_of>
    is_log_f_config_prompt = "Log Inquiry request/response XML?"
    is_log_f_label = "Log XML"

    require_token_validation = "Y" = <one_of> "Y" "N" </one_of>
    require_token_validation_f_config_prompt = "Do all token transactions require validation with eWallet owner credentials?"

    <!-- Bug24590 SX support si has no pageflow affect-->
    has_affect_on_pageflow = "N" = <one_of> "Y" "N" </one_of>
    _config_field_order=<v>
      "name"
      "description"
      "transactions"
      "endpoint_address"
      "repository"
      "payware_token_renewal_days"
      "CORE_repository_endpoint_address"
      "is_log"
      "require_token_validation"
    </v>

    <!-- DB Config Fields -->
    _db_field_order=<v>
      "_parent"
      "name"
      "description"
      "transactions"
      "endpoint_address"
      "repository"
      "payware_token_renewal_days"
      "CORE_repository_endpoint_address"
      "is_log"
      "require_token_validation"
    </v>

    _new_class._parent_f_type.<insert> _new_class </insert>
  </defclass>

  System_interface.eWallet.<defmethod _name='GetEWalletInfo'>
    eWalletToken = req
    <!-- return geo with itemBin and maskedValue -->
    _impl=cs.pci.pci.GetEWalletInfo
  </defmethod>
  
  System_interface.eWallet.<defmethod _name='RemoveItemFromEWallet'>
    eWalletToken = req
    <!-- return string,  return "SUCCESS" for success, return error message for failure-->
    _impl=cs.pci.pci.CASL_RemoveItemFromEWallet
  </defmethod>

  <!-- Bug 23935 - CCF Upgrade: Adding eWallet Methods to the CORE Generic Payment Gateway -->
  System_interface.eWallet.<defmethod _name='UpdateEWalletItem'>
    eWalletToken = req
    expirationDate = req
    zipCode = opt <!-- Bug 25177 - DJD: eWallet Zip Handling -->
    address = opt <!-- Bug 25177 - DJD: eWallet Address Handling -->
    itemName = opt
    shareTokenName = opt <!-- Bug 26719 DJD Added for "Share Token With" from Gateway -->
    <!-- return string,  return "SUCCESS" for success, return error message for failure-->
    _impl=cs.pci.pci.CASL_UpdateEWalletItem
  </defmethod>

  <!-- Bug 23935 - CCF Upgrade: Adding eWallet Methods to the CORE Generic Payment Gateway -->
  System_interface.eWallet.<defmethod _name='GetEWallet'>
    individualID=req
    hostReference=opt
    _impl=cs.pci.pci.CASL_GetEWallet
  </defmethod>

  <!--BUG17981 EWALLET UI get ewallet upon inquiry -->
  System_interface.eWallet.<defmethod _name='get_core_item'>
    a_core_item=req
    validate=opt

    <set>
      top_app = a_core_item.<get> "app_obj" </get>.<get_app/>
    </set>
    <if> top_app.<is_a> cbc </is_a>
      <set>
        this_dept=top_app.a_department
      </set>
    else
      <set>
        this_dept=Department.of.<get>
          a_core_item.<get_core_file/>.department if_missing=false
        </get>
      </set>
    </if>

    <set>
      ewallet = .<ewallet_retrieve>
        SI=_subject
        a_core_item=a_core_item
        this_dept=this_dept
      </ewallet_retrieve>
    </set>

    <!--
    top_app.<set>
      ewallet =
      <GEO  ID='Wallet'  >
        OwnerID="4ecbce64939148b793b5a99ca8878e0a"
        WalletFound=true
        PersonID="E4477"
        HostReference="Ireland"
        FirstName="Corey"
        LastName="LastName"
        MiddleIntial=null
        Address1="123 Some Street"
        Address2=""
        City="Round Rock"
        OwnerState="TX"
        Zip="78665"
        Email=null
        ItemList=<GEO>
        <GEO _name='DetailLine'>
          ItemID="333b5bb9f09c49d2815d7e6529d009d8"
          status="ACTIVE"
          TenderID="003"
          MaskedValue = "411111******1111"
          TenderType = "Visa"
          TenderDesc="My Favorite VISA"
          ItemBIN="411111"
          ExpirationDate="12/31/2016"
          CreationDate="02/27/2016"
          RepositoryID="PAYWARE"
          TenderCategory="VISA"
          TenderName=""
        </GEO>    
        </GEO>
      </GEO>
    </set>
    -->

    <!-- Show ewallet icons if ewallet are found -->
    <if>
      <and>
        ewallet.<get> "WalletFound" </get>.<is> true </is>
        ewallet.<get> "ItemList" if_missing=<GEO/> </get>.<length/>.<more> 0 </more>
      </and>
      <do>
        <set>
          ewallet_image_uri = <join> "/" site_name "/image/eWallet.png"</join>
        </set>
        a_core_item.<set>
          eWallet = <join> <![CDATA[<IMG src="]]> ewallet_image_uri <![CDATA[">]]> </join>
        </set>
      </do>
    </if>
    

    <if>
      ewallet.<is_a> error </is_a>
      <do>
      <!--Bug 24978 - UCMC: eWallet UI Enhancement do not show error if ewallet personal ID is not found yet --> 
        <log> "MESSAGE" = ewallet.<default_description/> </log>
        <return> true </return>
        <!--<set> return_value = ewallet </set> -->
      </do>
    else
      <do>
        a_core_item.<set> _ewallet = ewallet </set>
        <set> return_value =
          <if> a_core_item.<get> "system_interfaces" </get>.<length/>.<more> 1 </more>
            false
          else
            true
          </if>
        </set>
 <!--       
        top_app.<refresh_custom_tenders_to_section_list>
          tenders_in_group=ewallet
          show_no_found_message=true
        </refresh_custom_tenders_to_section_list>
        -->
      </do>
    </if>

    <return> return_value </return>

  </defmethod>

  <!-- Bug 18912 Added host_system_ID_2_tagname -->
  System_interface.eWallet.<defmethod _name='ewallet_retrieve'>
    SI=req
    a_core_item=req
    this_dept=req
    _impl=cs.pci.pci.CASL_ewallet_retrieve
  </defmethod>
  
</do>