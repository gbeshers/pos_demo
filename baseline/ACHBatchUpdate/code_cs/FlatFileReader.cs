using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace SDP.ParserUtils
{
    public class FlatFileReader
    {
		#region private members

		private StreamReader _reader;        
        private int _currentLineNumber;
        private string _currentLine;
		private bool _disposed;
		#endregion

        #region Constructors

        public FlatFileReader(string path, Encoding encoding )
        {
            _reader = new StreamReader(path, encoding);
        }
        #endregion

        #region IDisposable Pattern

        ~FlatFileReader()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!_disposed && disposing )
            {
                _reader.Dispose();
            }
            _disposed = true;
        }

        #endregion

        #region Public Properties
        public int CurrentLineNumber
        {
            get { return _currentLineNumber; }
        }

        public string CurrentLine
        {
            get { return _currentLine; }
        }


        #endregion

        #region Public Methods
        public virtual bool ParseLine()
        {
            for (; ; )
            {
                _currentLine = _reader.ReadLine();
                if (_currentLine == null)
                {
                    _reader.Close();
                    _currentLine = string.Empty;
                    return false;
                }
                _currentLineNumber++;
				if (_currentLine.Length > 0 )
                {
                    break;
                }
            }
            return true;
        }
        #endregion

    }
}
