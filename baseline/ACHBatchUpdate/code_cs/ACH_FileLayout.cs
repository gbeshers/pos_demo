﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using SDP.ParserUtils;

namespace ACH_FlatFile
{
    public class Type1Record_FileHeader : StringLayoutUtility
    {
        #region Private members
        private string _RecordType;               // Always 1
        private string _PriorityCode;             // Always 01
        private string _ImmediateDestination;     // Origination (ACH Processing) bank's routing/transit number
        private string _ImmediateOrigin;          // Ten-character file ID assigned by origination bank
        private string _FileCreationDate;         // YYMMDD
        private string _FileCreationTime;         // HHMM in military format
        private string _FileIDModifier;           // A for the first ACH input file each day. Increment to B, C, and so on for additional files sent the same day.
        private string _RecordSize;               // Always 094
        private string _BlockingFactor;           // Always 10
        private string _FormatCode;               // Always 1
        private string _ImmediateDestinationName; // Origination bank. e.g., "WELLS FARGO"
        private string _ImmediateOriginName;      // Company name (without abbreviations, if possible)
        private string _ReferenceCode;            // Optional; Use for reference or fill with spaces.
        #endregion

        public Type1Record_FileHeader()
        {
            TrimInput = TrimInputMode.Trim;
        }

        #region Properties
        [StringLayoutAttribute(0, 0)]
        public string RecordType
        {
            get { return _RecordType; }
            set { _RecordType = value; }
        }

        [StringLayoutAttribute(1, 2)]
        public string PriorityCode
        {
            get { return _PriorityCode; }
            set { _PriorityCode = value; }
        }

        [StringLayoutAttribute(3, 12, ' ', true)]
        public string ImmediateDestination
        {
            get { return _ImmediateDestination; }
            set { _ImmediateDestination = value; }
        }

        [StringLayoutAttribute(13, 22)]
        public string ImmediateOrigin
        {
            get { return _ImmediateOrigin; }
            set { _ImmediateOrigin = value; }
        }

        [StringLayoutAttribute(23, 28)]
        public string FileCreationDate
        {
            get { return _FileCreationDate; }
            set { _FileCreationDate = value; }
        }

        [StringLayoutAttribute(29, 32)]
        public string FileCreationTime
        {
            get { return _FileCreationTime; }
            set { _FileCreationTime = value; }
        }
    
        [StringLayoutAttribute(33, 33)]
        public string FileIDModifier
        {
            get { return _FileIDModifier; }
            set { _FileIDModifier = value; }
        }

        [StringLayoutAttribute(34, 36)]
        public string RecordSize
        {
            get { return _RecordSize; }
            set { _RecordSize = value; }
        }

        [StringLayoutAttribute(37, 38)]
        public string BlockingFactor
        {
            get { return _BlockingFactor; }
            set { _BlockingFactor = value; }
        }

        [StringLayoutAttribute(39, 39)]
        public string FormatCode
        {
            get { return _FormatCode; }
            set { _FormatCode = value; }
        }

        [StringLayoutAttribute(40, 62)]
        public string ImmediateDestinationName
        {
            get { return _ImmediateDestinationName; }
            set { _ImmediateDestinationName = value; }
        }

        [StringLayoutAttribute(63, 85)]
        public string ImmediateOriginName
        {
            get { return _ImmediateOriginName; }
            set { _ImmediateOriginName = value; }
        }

        [StringLayoutAttribute(86, 93)]
        public string ReferenceCode
        {
            get { return _ReferenceCode; }
            set { _ReferenceCode = value; }
        }

        #endregion

        #region Public Methods
        public override bool IsValid(ref string strError)
        {
          // Bug#14469 QG Revise validation method to provide error details.
          // Validation: Make certain the required values are not empty
          bool bReturn = true;
          StringBuilder sb = new StringBuilder();
          if (String.IsNullOrEmpty(this.RecordType))
          { sb.Append("RecordType "); bReturn = false; }
          if (String.IsNullOrEmpty(this.PriorityCode))
          { sb.Append("PriorityCode "); bReturn = false; }
          if (String.IsNullOrEmpty(this.ImmediateDestination))
          { sb.Append("ImmediateDestination "); bReturn = false; }
          if (String.IsNullOrEmpty(this.ImmediateOrigin))
          { sb.Append("ImmediateOrigin "); bReturn = false; }
          if (String.IsNullOrEmpty(this.FileCreationDate))
          { sb.Append("FileCreationDate "); bReturn = false; }
          if (String.IsNullOrEmpty(this.FileCreationTime))
          { sb.Append("FileCreationTime "); bReturn = false; }
          if (String.IsNullOrEmpty(this.FileIDModifier))
          { sb.Append("FileIDModifier "); bReturn = false; }
          if (String.IsNullOrEmpty(this.RecordSize))
          { sb.Append("RecordSize "); bReturn = false; }
          if (String.IsNullOrEmpty(this.BlockingFactor))
          { sb.Append("BlockingFactor "); bReturn = false; }
          if (String.IsNullOrEmpty(this.FormatCode))
          { sb.Append("FormatCode "); bReturn = false; }
          if (String.IsNullOrEmpty(this.ImmediateDestinationName))
          { sb.Append("ImmediateDestinationName "); bReturn = false; }
          if (String.IsNullOrEmpty(this.ImmediateOriginName))
          { sb.Append("ImmediateOriginName "); bReturn = false; }

          strError = sb.ToString();
          return bReturn;

            //// Validation: Make certain the required values are not empty
            //return (!String.IsNullOrEmpty(this.RecordType) &&
            //        !String.IsNullOrEmpty(this.PriorityCode) &&
            //        !String.IsNullOrEmpty(this.ImmediateDestination) &&
            //        !String.IsNullOrEmpty(this.ImmediateOrigin) &&
            //        !String.IsNullOrEmpty(this.FileCreationDate) &&
            //        !String.IsNullOrEmpty(this.FileCreationTime) &&
            //        !String.IsNullOrEmpty(this.FileIDModifier) &&
            //        !String.IsNullOrEmpty(this.RecordSize) &&
            //        !String.IsNullOrEmpty(this.BlockingFactor) &&
            //        !String.IsNullOrEmpty(this.FormatCode) &&
            //        !String.IsNullOrEmpty(this.ImmediateDestinationName) &&
            //        !String.IsNullOrEmpty(this.ImmediateOriginName));

        }
        #endregion
    }

    public class Type5Record_BatchHeader : StringLayoutUtility
    {
        #region Private members
        private string _RecordType;               // Always 5
        private string _ServiceClassCode;         // Enter as 200 (Can also accept 220 for credits only or 225 for debits only)
        private string _CompanyName;              // Name of the originating company.  Under NACHA rules, the purpose of the company name is to identify the source of a transaction, and it must be the name by which the originator (your company) is known and readily recognizable to the receiver. If your company is not the payee (for debit transactions) or payor (for credit transactions), then the company name must be the name by which the payee or payor is known and readily recognizable to the receiver. Use as much of the company name as the field length allows unless you identify an alternative that is known and readily recognizable by receivers of your transactions.
        private string _CompanyDiscretionaryData; // Optional. (Alphanumeric.) Can be used to include information to help your company identify the transactions in the batch.
        private string _CompanyID;                // Ten-character company ID assigned by Origination bank. (e.g., Wells Fargo)
        private string _StandardEntryClass;       // SEC code
        private string _CompanyEntryDescription;  // For most transactions, this is a company defined description of the entry to the receiver, such as PAYROLL, GAS BILL, or INS PREM. For reversals, this must be REVERSAL in uppercase. for RCK transactions, this must be REDEPCHECK in uppercase.
        private string _CompanyDescriptiveDate;   // Optional. Company-defined reference date for the receiver (for descriptive purposes only).  Examples of possible entries in this field: 091506 09 06 SEP 15 SEP 06
        private string _EffectiveEntryDate;       // Format YYMMDD. Company-defined date on which entries in the batch are to be settled/posted to the account.  The effective entry date should not be a Saturday, Sunday, or holiday.
        private string _SettlementDate;           // Reserved field for ACH operator. Leave blank.
        private string _OriginatorStatusCode;     // Always 1.
        private string _OriginatingDFI_ID;        // Originating (ACH Processing) bank's routing/transit number
        private string _BatchNumber;              // Starting from 0000001 in ascending sequence for each company/batch header record.
        #endregion

        public Type5Record_BatchHeader()
        {
            TrimInput = TrimInputMode.Trim;
        }

        #region Properties
        [StringLayoutAttribute(0, 0)]
        public string RecordType
        {
            get { return _RecordType; }
            set { _RecordType = value; }
        }

        [StringLayoutAttribute(1, 3)]
        public string ServiceClassCode
        {
            get { return _ServiceClassCode; }
            set { _ServiceClassCode = value; }
        }

        [StringLayoutAttribute(4, 19)]
        public string CompanyName
        {
            get { return _CompanyName; }
            set { _CompanyName = value; }
        }

        [StringLayoutAttribute(20, 39)]
        public string CompanyDiscretionaryData
        {
            get { return _CompanyDiscretionaryData; }
            set { _CompanyDiscretionaryData = value; }
        }

        [StringLayoutAttribute(40, 49)]
        public string CompanyID
        {
            get { return _CompanyID; }
            set { _CompanyID = value; }
        }

        [StringLayoutAttribute(50, 52)]
        public string StandardEntryClass
        {
            get { return _StandardEntryClass; }
            set { _StandardEntryClass = value; }
        }
    
        [StringLayoutAttribute(53, 62)]
        public string CompanyEntryDescription
        {
            get { return _CompanyEntryDescription; }
            set { _CompanyEntryDescription = value; }
        }

        [StringLayoutAttribute(63, 68)]
        public string CompanyDescriptiveDate
        {
            get { return _CompanyDescriptiveDate; }
            set { _CompanyDescriptiveDate = value; }
        }

        [StringLayoutAttribute(69, 74)]
        public string EffectiveEntryDate
        {
            get { return _EffectiveEntryDate; }
            set { _EffectiveEntryDate = value; }
        }

        [StringLayoutAttribute(75, 77)]
        public string SettlementDate
        {
            get { return _SettlementDate; }
            set { _SettlementDate = value; }
        }

        [StringLayoutAttribute(78, 78)]
        public string OriginatorStatusCode
        {
            get { return _OriginatorStatusCode; }
            set { _OriginatorStatusCode = value; }
        }

        [StringLayoutAttribute(79, 86)]
        public string OriginatingDFI_ID
        {
            get { return _OriginatingDFI_ID; }
            set { _OriginatingDFI_ID = value; }
        }

        [StringLayoutAttribute(87, 93)]
        public string BatchNumber
        {
            get { return _BatchNumber; }
            set { _BatchNumber = value; }
        }

        #endregion

        #region Public Methods
        public override bool IsValid(ref String strError)
        {
          // Bug#14469 QG Revise validation method to provide error details.
          // Validation: Make certain the required values are not empty
          bool bReturn = true;
          StringBuilder sb = new StringBuilder();
          if (String.IsNullOrEmpty(this.RecordType))
          { sb.Append("RecordType "); bReturn = false; }
          if (String.IsNullOrEmpty(this.ServiceClassCode))
          { sb.Append("ServiceClassCode "); bReturn = false; }
          if (String.IsNullOrEmpty(this.CompanyName))
          { sb.Append("CompanyName "); bReturn = false; }
          if (String.IsNullOrEmpty(this.CompanyID))
          { sb.Append("CompanyID "); bReturn = false; }
          if (String.IsNullOrEmpty(this.StandardEntryClass))
          { sb.Append("StandardEntryClass "); bReturn = false; }
          if (String.IsNullOrEmpty(this.CompanyEntryDescription))
          { sb.Append("CompanyEntryDescription "); bReturn = false; }
          if (String.IsNullOrEmpty(this.EffectiveEntryDate))
          { sb.Append("EffectiveEntryDate "); bReturn = false; }
          if (String.IsNullOrEmpty(this.SettlementDate))
          { sb.Append("SettlementDate "); bReturn = false; }
          if (String.IsNullOrEmpty(this.OriginatorStatusCode))
          { sb.Append("OriginatorStatusCode "); bReturn = false; }
          if (String.IsNullOrEmpty(this.OriginatingDFI_ID))
          { sb.Append("OriginatingDFI_ID "); bReturn = false; }
          if (String.IsNullOrEmpty(this.BatchNumber))
          { sb.Append("BatchNumber "); bReturn = false; }

          strError = sb.ToString();
          return bReturn;


            //return (!String.IsNullOrEmpty(this.RecordType) &&
            //        !String.IsNullOrEmpty(this.ServiceClassCode) &&
            //        !String.IsNullOrEmpty(this.CompanyName) &&
            //        !String.IsNullOrEmpty(this.CompanyID) &&
            //        !String.IsNullOrEmpty(this.StandardEntryClass) &&
            //        !String.IsNullOrEmpty(this.CompanyEntryDescription) &&
            //        !String.IsNullOrEmpty(this.EffectiveEntryDate) &&
            //        !String.IsNullOrEmpty(this.SettlementDate) &&
            //        !String.IsNullOrEmpty(this.OriginatorStatusCode) &&
            //        !String.IsNullOrEmpty(this.OriginatingDFI_ID) &&
            //        !String.IsNullOrEmpty(this.BatchNumber));        
        }
        #endregion
    }

    public class Type6Record_EntryDetail : StringLayoutUtility
    {
        #region Private members
        private string _RecordType;                   // Always 6
        private string _TransactionCode;              // For checking accounts: 22 credit, 27 debit | For savings accounts: 32 credit, 37 debit
        private string _Receiving_DFI_RT_Number;      // First eight digits of routing/transit number of receiving financial depository institution (RDFI) where the transaction is to be posted.
        private string _RT_Number_CheckDigit;         // Last digit of RDFI’s routing/transit number.
        private string _Receiving_DFI_Account_Number; // Account number at the RDFI to be credited or debited.  If the account number is longer than 17 characters, drop the initial characters and include the last 17 characters.
        private string _Amount;                       // Dollar amount of the transaction - Numbers only, Unsigned, No decimal point, Right-justified, Filled with leading zeros.  Example: $18.50 would be entered as 0000001850.
        private string _IndividualID;                 // ID number for the receiver, such as an employee number or customer number. For transactions converted from checks (items with SEC code ARC, BOC, POP, or RCK), this field is required and must include the check serial number, left-justified.
        private string _IndividualName;               // Name of the client or individual for whom the transaction is intended. 
        private string _DiscretionaryData;            // Leave blank (two spaces) for all items except for items using SEC code WEB, this field indicates the payment type: Rb Recurring entry (b indicates a blank space). Sb Single entry (b indicates a blank space).
        private string _AddendaRecordIndicator;       // 0: No addenda record follows this record. 1: One or more addenda records follows this record.
        private string _TraceNumber;                  // Uniquely identifies each entry within a batch in an ACH input file. The first eight positions should be the routing/transit number of the Originating bank. The next seven positions should be in sequential order, starting with 0000001.
        #endregion

        public Type6Record_EntryDetail()
        {
            TrimInput = TrimInputMode.Trim;
        }

        #region Properties
        [StringLayoutAttribute(0, 0)]
        public string RecordType
        {
            get { return _RecordType; }
            set { _RecordType = value; }
        }

        [StringLayoutAttribute(1, 2)]
        public string TransactionCode
        {
            get { return _TransactionCode; }
            set { _TransactionCode = value; }
        }

        [StringLayoutAttribute(3, 10)]
        public string Receiving_DFI_RT_Number
        {
            get { return _Receiving_DFI_RT_Number; }
            set { _Receiving_DFI_RT_Number = value; }
        }

        [StringLayoutAttribute(11, 11)]
        public string RT_Number_CheckDigit
        {
            get { return _RT_Number_CheckDigit; }
            set { _RT_Number_CheckDigit = value; }
        }

        [StringLayoutAttribute(12, 28)]
        public string Receiving_DFI_Account_Number
        {
            get { return _Receiving_DFI_Account_Number; }
            set { _Receiving_DFI_Account_Number = value; }
        }

        [StringLayoutAttribute(29, 38, '0', true)]
        public string Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        [StringLayoutAttribute(39, 53)]
        public string IndividualID
        {
            get { return _IndividualID; }
            set { _IndividualID = value; }
        }

        [StringLayoutAttribute(54, 75)]
        public string IndividualName
        {
            get { return _IndividualName; }
            set { _IndividualName = value; }
        }

        [StringLayoutAttribute(76, 77)]
        public string DiscretionaryData
        {
            get { return _DiscretionaryData; }
            set { _DiscretionaryData = value; }
        }

        [StringLayoutAttribute(78, 78)]
        public string AddendaRecordIndicator
        {
            get { return _AddendaRecordIndicator; }
            set { _AddendaRecordIndicator = value; }
        }

        [StringLayoutAttribute(79, 93)]
        public string TraceNumber
        {
            get { return _TraceNumber; }
            set { _TraceNumber = value; }
        }

        #endregion

        #region Public Methods
        public override bool IsValid(ref String strError)
        {
          // Bug#14469 QG Revise validation method to provide error details.
          // Validation: Make certain the required values are not empty
          bool bReturn = true;
          StringBuilder sb = new StringBuilder();
          if (String.IsNullOrEmpty(this.RecordType))
          { sb.Append("RecordType "); bReturn = false; }
          if (String.IsNullOrEmpty(this.TransactionCode))
          { sb.Append("TransactionCode "); bReturn = false; }
          if (String.IsNullOrEmpty(this.Receiving_DFI_RT_Number))
          { sb.Append("Receiving_DFI_RT_Number "); bReturn = false; }
          if (String.IsNullOrEmpty(this.RT_Number_CheckDigit))
          { sb.Append("RT_Number_CheckDigit "); bReturn = false; }
          if (String.IsNullOrEmpty(this.Receiving_DFI_Account_Number))
          { sb.Append("Receiving_DFI_Account_Number "); bReturn = false; }
          if (String.IsNullOrEmpty(this.Amount))
          { sb.Append("Amount "); bReturn = false; }
          if (String.IsNullOrEmpty(this.IndividualID))
          { sb.Append("IndividualID "); bReturn = false; }
          if (String.IsNullOrEmpty(this.IndividualName))
          { sb.Append("IndividualName "); bReturn = false; }
          if (String.IsNullOrEmpty(this.AddendaRecordIndicator))
          { sb.Append("AddendaRecordIndicator "); bReturn = false; }
          if (String.IsNullOrEmpty(this.TraceNumber))
          { sb.Append("TraceNumber "); bReturn = false; }

          strError = sb.ToString();
          return bReturn;
            //// Validation: Make certain the required values are not empty
            //return (!String.IsNullOrEmpty(this.RecordType) &&
            //        !String.IsNullOrEmpty(this.TransactionCode) &&
            //        !String.IsNullOrEmpty(this.Receiving_DFI_RT_Number) &&
            //        !String.IsNullOrEmpty(this.RT_Number_CheckDigit) &&
            //        !String.IsNullOrEmpty(this.Receiving_DFI_Account_Number) &&
            //        !String.IsNullOrEmpty(this.Amount) &&
            //        !String.IsNullOrEmpty(this.IndividualID) &&
            //        !String.IsNullOrEmpty(this.IndividualName) &&
            //        !String.IsNullOrEmpty(this.AddendaRecordIndicator) &&
            //        !String.IsNullOrEmpty(this.TraceNumber));        
        }
        #endregion
    }

    // Type7Record_AddendaRecord : currently, not supported.

    public class Type8Record_BatchControl : StringLayoutUtility
    {
        #region Private members
        private string _RecordType;                // Always 8
        private string _ServiceClassCode;          // Should be the same as the service class code in the company/batch header record.
        private string _EntryAddendaCount;         // Total number of entry detail and addenda records in the batch. Right-justify and zero-fill the field.
        private string _EntryHash;                 // The 10-character entry hash is the sum of the 8-digit receiving DFI routing/transit numbers in entry detail records in the batch. Add leading zeros as needed and ignore the overflow out of the high order (leftmost) position.
        private string _TotalDebitEntryAmount;     // The sum of entry detail debit totals within the batch. Right-justify and zero-fill the field.
        private string _TotalCreditEntryAmount;    // The sum of entry detail credit totals within the batch. Right-justify and zero-fill the field.
        private string _CompanyID;                 // Should be the same company ID used in the company/batch header record for this batch.
        private string _MessageAuthenticationCode; // Leave blank (filled with spaces).
        private string _Blank;                     // Reserved. Leave blank (filled with spaces).
        private string _OriginatingDFI_ID;         // Originating (ACH Processing) bank's routing/transit number
        private string _BatchNumber;               // Should be the same batch number used in the company/batch header record for the batch.
        #endregion

        public Type8Record_BatchControl()
        {
            TrimInput = TrimInputMode.Trim;
        }

        #region Properties
        [StringLayoutAttribute(0, 0)]
        public string RecordType
        {
            get { return _RecordType; }
            set { _RecordType = value; }
        }

        [StringLayoutAttribute(1, 3)]
        public string ServiceClassCode
        {
            get { return _ServiceClassCode; }
            set { _ServiceClassCode = value; }
        }

        [StringLayoutAttribute(4, 9, '0', true)]
        public string EntryAddendaCount
        {
            get { return _EntryAddendaCount; }
            set { _EntryAddendaCount = value; }
        }

        [StringLayoutAttribute(10, 19, '0', true)]
        public string EntryHash
        {
            get { return _EntryHash; }
            set { _EntryHash = value; }
        }

        [StringLayoutAttribute(20, 31, '0', true)]
        public string TotalDebitEntryAmount
        {
            get { return _TotalDebitEntryAmount; }
            set { _TotalDebitEntryAmount = value; }
        }

        [StringLayoutAttribute(32, 43, '0', true)]
        public string TotalCreditEntryAmount
        {
            get { return _TotalCreditEntryAmount; }
            set { _TotalCreditEntryAmount = value; }
        }

        [StringLayoutAttribute(44, 53)]
        public string CompanyID
        {
            get { return _CompanyID; }
            set { _CompanyID = value; }
        }

        [StringLayoutAttribute(54, 72)]
        public string MessageAuthenticationCode
        {
            get { return _MessageAuthenticationCode; }
            set { _MessageAuthenticationCode = value; }
        }

        [StringLayoutAttribute(73, 78)]
        public string Blank
        {
            get { return _Blank; }
            set { _Blank = value; }
        }

        [StringLayoutAttribute(79, 86)]
        public string OriginatingDFI_ID
        {
            get { return _OriginatingDFI_ID; }
            set { _OriginatingDFI_ID = value; }
        }

        [StringLayoutAttribute(87, 93, '0', true)]
        public string BatchNumber
        {
            get { return _BatchNumber; }
            set { _BatchNumber = value; }
        }

        #endregion

        #region Public Methods
        public override bool IsValid(ref String strError)
        {
          // Bug#14469 QG Revise validation method to provide error details.
          // Validation: Make certain the required values are not empty
          bool bReturn = true;
          StringBuilder sb = new StringBuilder();
          if (String.IsNullOrEmpty(this.RecordType))
          { sb.Append("RecordType "); bReturn = false; }
          if (String.IsNullOrEmpty(this.ServiceClassCode))
          { sb.Append("ServiceClassCode "); bReturn = false; }
          if (String.IsNullOrEmpty(this.EntryAddendaCount))
          { sb.Append("EntryAddendaCount "); bReturn = false; }
          if (String.IsNullOrEmpty(this.EntryHash))
          { sb.Append("EntryHash "); bReturn = false; }
          if (String.IsNullOrEmpty(this.TotalDebitEntryAmount))
          { sb.Append("TotalDebitEntryAmount "); bReturn = false; }
          if (String.IsNullOrEmpty(this.TotalCreditEntryAmount))
          { sb.Append("TotalCreditEntryAmount "); bReturn = false; }
          if (String.IsNullOrEmpty(this.CompanyID))
          { sb.Append("CompanyID "); bReturn = false; }
          if (String.IsNullOrEmpty(this.MessageAuthenticationCode))
          { sb.Append("MessageAuthenticationCode "); bReturn = false; }
          if (String.IsNullOrEmpty(this.Blank))
          { sb.Append("Blank "); bReturn = false; }
          if (String.IsNullOrEmpty(this.OriginatingDFI_ID))
          { sb.Append("OriginatingDFI_ID "); bReturn = false; }
          if (String.IsNullOrEmpty(this.BatchNumber))
          { sb.Append("BatchNumber "); bReturn = false; }

          strError = sb.ToString();
          return bReturn;

          //return (!String.IsNullOrEmpty(this.RecordType) &&
          //        !String.IsNullOrEmpty(this.ServiceClassCode) &&
          //        !String.IsNullOrEmpty(this.EntryAddendaCount) &&
          //        !String.IsNullOrEmpty(this.EntryHash) &&
          //        !String.IsNullOrEmpty(this.TotalDebitEntryAmount) &&
          //        !String.IsNullOrEmpty(this.TotalCreditEntryAmount) &&
          //        !String.IsNullOrEmpty(this.CompanyID) &&
          //        !String.IsNullOrEmpty(this.MessageAuthenticationCode) &&
          //        !String.IsNullOrEmpty(this.Blank) &&
          //        !String.IsNullOrEmpty(this.OriginatingDFI_ID) &&
          //        !String.IsNullOrEmpty(this.BatchNumber));
        }
        #endregion
    }

    public class Type9Record_FileControl : StringLayoutUtility
    {
        #region Private members
        private string _RecordType;                 // Always 9
        private string _BatchCount;                 // Number of batches in the file. Right-justify and zero-fill the field.
        private string _BlockCount;                 // Total number of records in the file, divided by ten and rounded up. Right-justify and zero-fill the field. All records in the file, including this one, are included in the block count. Example: a file with 95 records would have a block count of 000010.
        private string _EntryAddendaCount;          // Total number of entry detail and addenda records in the file. Right-justify and zero-fill the field.
        private string _EntryHashTotal;             // Sum of entry hash fields in all company/batch control records. Add leading zeros as needed, and ignore overflow out of the high order (leftmost) position if the sum is more than ten digits.
        private string _TotalFileDebitEntryAmount;  // The sum of entry detail debit totals within the file. Right-justify and zero-fill the field.
        private string _TotalFileCreditEntryAmount; // The sum of entry detail credit totals within the file. Right-justify and zero-fill the field.
        private string _Filler;                     // Include spaces to make the record 94 characters.
        #endregion

        public Type9Record_FileControl()
        {
            TrimInput = TrimInputMode.Trim;
        }

        #region Properties
        [StringLayoutAttribute(0, 0)]
        public string RecordType
        {
            get { return _RecordType; }
            set { _RecordType = value; }
        }

        [StringLayoutAttribute(1, 6, '0', true)]
        public string BatchCount
        {
            get { return _BatchCount; }
            set { _BatchCount = value; }
        }

        [StringLayoutAttribute(7, 12, '0', true)]
        public string BlockCount
        {
            get { return _BlockCount; }
            set { _BlockCount = value; }
        }

        [StringLayoutAttribute(13, 20, '0', true)]
        public string EntryAddendaCount
        {
            get { return _EntryAddendaCount; }
            set { _EntryAddendaCount = value; }
        }

        [StringLayoutAttribute(21, 30, '0', true)]
        public string EntryHashTotal
        {
            get { return _EntryHashTotal; }
            set { _EntryHashTotal = value; }
        }

        [StringLayoutAttribute(31, 42, '0', true)]
        public string TotalFileDebitEntryAmount
        {
            get { return _TotalFileDebitEntryAmount; }
            set { _TotalFileDebitEntryAmount = value; }
        }

        [StringLayoutAttribute(43, 54, '0', true)]
        public string TotalFileCreditEntryAmount
        {
            get { return _TotalFileCreditEntryAmount; }
            set { _TotalFileCreditEntryAmount = value; }
        }

        [StringLayoutAttribute(55, 93)]
        public string Filler
        {
            get { return _Filler; }
            set { _Filler = value; }
        }

        #endregion

        #region Public Methods
        public override bool IsValid(ref String strError)
        {
            // Bug#14469 QG Revise validation method to provide error details.
            // Validation: Make certain the required values are not empty
            bool bReturn = true;
            StringBuilder sb = new StringBuilder();
            if (String.IsNullOrEmpty(this.RecordType))
            { sb.Append("RecordType "); bReturn = false; }
            if (String.IsNullOrEmpty(this.BatchCount))
            { sb.Append("BatchCount "); bReturn = false; }
            if (String.IsNullOrEmpty(this.BlockCount))
            { sb.Append("BlockCount "); bReturn = false; }
            if (String.IsNullOrEmpty(this.EntryAddendaCount))
            { sb.Append("EntryAddendaCount "); bReturn = false; }
            if (String.IsNullOrEmpty(this.EntryHashTotal))
            { sb.Append("EntryHashTotal "); bReturn = false; }
            if (String.IsNullOrEmpty(this.TotalFileDebitEntryAmount))
            { sb.Append("TotalFileDebitEntryAmount "); bReturn = false; }
            if (String.IsNullOrEmpty(this.TotalFileCreditEntryAmount))
            { sb.Append("TotalFileCreditEntryAmount "); bReturn = false; }
            if (String.IsNullOrEmpty(this.Filler))
            { sb.Append("Filler "); bReturn = false; }

            strError = sb.ToString();
            return bReturn;
            //  return (!String.IsNullOrEmpty(this.RecordType) &&
            //          !String.IsNullOrEmpty(this.BatchCount) &&
            //          !String.IsNullOrEmpty(this.BlockCount) &&
            //          !String.IsNullOrEmpty(this.EntryAddendaCount) &&
            //          !String.IsNullOrEmpty(this.EntryHashTotal) &&
            //          !String.IsNullOrEmpty(this.TotalFileDebitEntryAmount) &&
            //          !String.IsNullOrEmpty(this.TotalFileCreditEntryAmount) &&
            //          !String.IsNullOrEmpty(this.Filler));
        }
        #endregion
    }

}
