using System;
using System.Collections;
using System.Net;
using System.IO;
using System.Text;
using CASL_engine;
using System.Xml;
using System.Xml.Serialization;
using ftp;
using ACH_FlatFile;
using System.Diagnostics;
using System.ComponentModel;
using TranSuiteServices;

namespace ACHBatchUpdate
{
  /// <summary>
  /// This is the data structure for ACH File.
  /// </summary>
  public class ACHFileBatchInfo
  {
    #region Private members
    private Type1Record_FileHeader _FileHeaderRecord;      // File Header Records
    private Type9Record_FileControl _FileControlRecord;    // File Control Records
    private int _FileEntryCount;                           // Count of Entry Details in File
    private Type5Record_BatchHeader _CurrentBatchHeader;   // Current Batch Header
    private GenericObject _CurrentBatchRecords;            // Current Batch Entry Details
    private Type8Record_BatchControl _CurrentBatchControl; // Current Batch Control
    private int _CurrentBatchRecordCount;                  // Current Batch Record Count
    private int _BatchNumber;                              // ascending sequence for each company/batch header record
    private long _CurrentEntryHash;                        // Current batch entry hash
    private long _TotalEntryHash;                          // Total Entry hash
    private decimal _BatchDebitTotal;                      // debits total of current batch
    private decimal _BatchCreditTotal;                     // credit total of current batch
    private decimal _FileDebitTotal;                       // debits total of File
    private decimal _FileCreditTotal;                      // credit total of File
    private DateTime _FileCreationDateTime;                // File Creation Date Time (value is set when File Header Recorder is created). [Bug 21902]
    #endregion

    #region Public Properties
    public Type1Record_FileHeader m_FileHeaderRecord
    {
      get { return _FileHeaderRecord; }
      set { _FileHeaderRecord = value; }
    }
    public Type9Record_FileControl m_FileControlRecord
    {
      get { return _FileControlRecord; }
      set { _FileControlRecord = value; }
    }
    public int m_intFileEntryCount // Collection of records
    {
      get { return _FileEntryCount; }
      set { _FileEntryCount = value; }
    }
    public Type5Record_BatchHeader m_CurrentBatchHeader
    {
      get { return _CurrentBatchHeader; }
      set { _CurrentBatchHeader = value; }
    }
    public GenericObject m_getoCurrentBatchRecords
    {
      get { return _CurrentBatchRecords; }
      set { _CurrentBatchRecords = value; }
    }
    public Type8Record_BatchControl m_CurrentBatchControl
    {
      get { return _CurrentBatchControl; }
      set { _CurrentBatchControl = value; }
    }
    public int m_CurrentBatchRecordCount
    {
      get { return _CurrentBatchRecordCount; }
      set { _CurrentBatchRecordCount = value; }
    }
    public int m_intBatchNumber
    {
      get { return _BatchNumber; }
      set { _BatchNumber = value; }
    }
    public long m_longCurrentEntryHash
    {
      get { return _CurrentEntryHash; }
      set { _CurrentEntryHash = value; }
    }
    public long m_longTotalEntryHash
    {
      get { return _TotalEntryHash; }
      set { _TotalEntryHash = value; }
    }
    public decimal m_dBatchDebitTotal
    {
      get { return _BatchDebitTotal; }
      set { _BatchDebitTotal = value; }
    }
    public decimal m_dBatchCreditTotal
    {
      get { return _BatchCreditTotal; }
      set { _BatchCreditTotal = value; }
    }
    public decimal m_dFileDebitTotal
    {
      get { return _FileDebitTotal; }
      set { _FileDebitTotal = value; }
    }
    public decimal m_dFileCreditTotal
    {
      get { return _FileCreditTotal; }
      set { _FileCreditTotal = value; }
    }
    // Bug 21902 DJD: Configurable File Name for ACH Batch Update System Interface
    public DateTime m_FileCreationDateTime
    {
      get { return _FileCreationDateTime; }
      set { _FileCreationDateTime = value; }
    }
    #endregion

    /// <summary>
    /// Constructor
    /// </summary>
    public ACHFileBatchInfo()
    {
      m_CurrentBatchRecordCount = 1;
      m_intBatchNumber = 1;
      m_longCurrentEntryHash = 0;
      m_longTotalEntryHash = 0;
      m_dBatchDebitTotal = 0;
      m_dBatchCreditTotal = 0;
      m_dFileDebitTotal = 0;
      m_dFileCreditTotal = 0;
      m_getoCurrentBatchRecords = new GenericObject();
      m_intFileEntryCount = 0;
      m_FileCreationDateTime = DateTime.Now; // Bug 21902 DJD: Configurable File Name for ACH Batch Update System Interface
    }

    #region Public Methods


    /// <summary>
    /// Type1Record_FileHeader type1Record = new Type1Record_FileHeader();
    /// type1Record.RecordType = "1";
    /// type1Record.PriorityCode = "01";
    /// type1Record.ImmediateDestination = "091000019";
    /// type1Record.ImmediateOrigin = "1123456789";
    /// type1Record.FileCreationDate = "110913";
    /// type1Record.FileCreationTime = "1330";
    /// type1Record.FileIDModifier = "A";
    /// type1Record.RecordSize = "094";
    /// type1Record.BlockingFactor = "10";
    /// type1Record.FormatCode = "1"; // "1"
    /// type1Record.ImmediateDestinationName = "WELLS FARGO";
    /// type1Record.ImmediateOriginName = "ABC COMPANY";
    /// type1Record.ReferenceCode = " ";
    /// </summary>
    /// <param name="SysInt"></param>
    /// <param name="FileGEO"></param>
    /// <param name="strError"></param>
    /// <returns></returns>
    public bool AddFileHeaderRecord(GenericObject SysInt, GenericObject FileGEO, ref string strError)
    {
      strError = "";
      try
      {
        Type1Record_FileHeader tmpFileHeader = new Type1Record_FileHeader();
        tmpFileHeader.RecordType = "1";
        tmpFileHeader.PriorityCode = "01";
        tmpFileHeader.ImmediateDestination = SysInt.get("Immediate_Destination", "").ToString();
        tmpFileHeader.ImmediateOrigin = SysInt.get("Immediate_Origin", "").ToString();
        if (FileGEO.get("SOURCE_DATE") is DateTime)
        {
          // Bug 21902 DJD: Configurable File Name for ACH Batch Update System Interface
          tmpFileHeader.FileCreationDate = string.Format("{0:yyMMdd}", m_FileCreationDateTime);
          tmpFileHeader.FileCreationTime = string.Format("{0:HHmm}", m_FileCreationDateTime);
        }
        
        // Bug 21902 DJD: Saving "yyMMdd{File ID Modifier}" now (not the file name)
        // Bug 27212: Get the global File ID Modifier, not the one stored in the system interfaces
        string fileID_Value = (c_CASL.c_object("Business.Misc.data") as GenericObject).get("ACH_Global_File_ID_Modifier", "").ToString();

        if (fileID_Value.Length == 7)
        {
          string strDate = fileID_Value.Substring(0, 6);
          if (string.Format("{0:yyMMdd}", m_FileCreationDateTime) == strDate)
          {
            char cIDModifier = (char)((fileID_Value[fileID_Value.Length - 1] + 1 - 65) % 26 + 65);
            tmpFileHeader.FileIDModifier = cIDModifier.ToString();
          }
          else
          {
            tmpFileHeader.FileIDModifier = "A";   // It is a new day; reset the ID counter to A
          }
        }
        else
        {
          // Don't know why this is hardwired here.  Old style?
          tmpFileHeader.FileIDModifier = "A";
        }

        tmpFileHeader.RecordSize = "094";
        tmpFileHeader.BlockingFactor = "10";
        tmpFileHeader.FormatCode = "1";
        tmpFileHeader.ImmediateDestinationName = SysInt.get("Immediate_Destination_Name", "").ToString();
        tmpFileHeader.ImmediateOriginName = SysInt.get("Company_Name", "").ToString();
        tmpFileHeader.ReferenceCode = " ";

        if (tmpFileHeader.IsValid(ref strError)) //Bug#14469
        {
          m_FileHeaderRecord = tmpFileHeader;
          return true;
        }
        else
        {
          strError = string.Format("Error in AddFileHeaderRecord: File Header Record is invalid. FileIDModifier = {0}. The following fields are invalid: {1}", tmpFileHeader.FileIDModifier, strError);
          return false;
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in ACHFile AddFileHeaderRecord", e);

        strError = string.Format("Error in AddFileHeaderRecord: {0} ", e.Message);
        return false;
      }
    }

    /// <summary>
    /// Type5Record_BatchHeader type5Record = new Type5Record_BatchHeader();
    /// type5Record.RecordType = "5";
    /// type5Record.ServiceClassCode = "200";
    /// type5Record.CompanyName = "ABC COMPANY";
    /// type5Record.CompanyDiscretionaryData = "WESTERN REGION";
    /// type5Record.CompanyID = "2123456789";
    /// type5Record.StandardEntryClass = "CCD";
    /// type5Record.CompanyEntryDescription = "CASH CONC";
    /// type5Record.CompanyDescriptiveDate = "123100";  //MMddyy
    /// type5Record.EffectiveEntryDate = "110915";
    /// type5Record.SettlementDate = " ";
    /// type5Record.OriginatorStatusCode = "1";
    /// type5Record.OriginatingDFI_ID = "09100001";
    /// type5Record.BatchNumber = "0000001";
    /// </summary>
    /// <param name="SysInt"></param>
    /// <param name="FileGEO"></param>
    /// <param name="strError"></param>
    /// <returns></returns>
    public bool AddBatchHeaderRecord(GenericObject SysInt, GenericObject FileGEO, ref string strError)
    {
      strError = "";
      try
      {
        Type5Record_BatchHeader tmpBatchHeaderRecord = new Type5Record_BatchHeader();
        tmpBatchHeaderRecord.RecordType = "5";

        // Bug 27559 DJD: Allow ACH Check Reversals and Include Credit Entries in the ACH Batch
        // The "ServiceClassCode" will need to be reset AFTER all debit AND credits(for reversals) are processed (in Project_ProcessDeposits).
        tmpBatchHeaderRecord.ServiceClassCode = "200"; // Set to credits and debits

        tmpBatchHeaderRecord.CompanyName = SysInt.get("Company_Name_short", "").ToString();
        tmpBatchHeaderRecord.CompanyDiscretionaryData = FileGEO.get("FILENAME", "").ToString();
        tmpBatchHeaderRecord.CompanyID = SysInt.get("Company_ID", "").ToString();

        // Bug# 13054 QG Rework the way to get WorkGroup Config
        GenericObject geoWorkGroup = GetWorkgroupFromCoreFile(FileGEO); // Bug 21927 DJD
        string strWorkGroupSEC = geoWorkGroup.get("Standard_Entry_Class", "").ToString();
        if (!String.IsNullOrEmpty(strWorkGroupSEC))
          tmpBatchHeaderRecord.StandardEntryClass = strWorkGroupSEC;
        else
          tmpBatchHeaderRecord.StandardEntryClass = SysInt.get("Standard_entry_class", "").ToString();
        tmpBatchHeaderRecord.CompanyEntryDescription = SysInt.get("Company_entry_description", "").ToString();
        //Effective date
        if (FileGEO.get("EFFECTIVEDT") is DateTime)
          tmpBatchHeaderRecord.CompanyDescriptiveDate = string.Format("{0:MMddyy}", (DateTime)FileGEO.get("EFFECTIVEDT"));
        // Next business day after File generation
        DateTime tmpDT = m_FileCreationDateTime; // Bug 21902 DJD: Configurable File Name for ACH Batch Update System Interface
        do { tmpDT = tmpDT.AddDays(1.00); }
        while ((tmpDT.DayOfWeek == DayOfWeek.Saturday) || (tmpDT.DayOfWeek == DayOfWeek.Sunday));
        tmpBatchHeaderRecord.EffectiveEntryDate = string.Format("{0:yyMMdd}", tmpDT);
        tmpBatchHeaderRecord.SettlementDate = " ";
        tmpBatchHeaderRecord.OriginatorStatusCode = "1";
        tmpBatchHeaderRecord.OriginatingDFI_ID = SysInt.get("Originating_DFI_ID", "").ToString();
        tmpBatchHeaderRecord.BatchNumber = m_intBatchNumber.ToString().PadLeft(7, '0');
        if (tmpBatchHeaderRecord.IsValid(ref strError)) //Bug#14469
        {
          m_CurrentBatchHeader = tmpBatchHeaderRecord;
          return true;
        }
        else
        {
          strError = string.Format("Error in AddBatchHeaderRecord: Batch Header Record is invalid. BatchNumber = {0}. The following fields are invalid:{1}", m_intBatchNumber, strError);
          return false;
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in ACHFile AddBatchHeaderRecord", e);

        strError = string.Format("Error in AddBatchHeaderRecord: Batch Header Record is invalid. {0} BatchNumber = {1}", e.Message, m_intBatchNumber);
        return false;
      }
    }

    // Bug 21927 DJD
    public static GenericObject GetWorkgroupFromCoreFile(GenericObject coreFile)
    {
      string strDeptID = coreFile.get("DEPTID", "").ToString();
      GenericObject workGroup = ((GenericObject)c_CASL.c_object("Department.of")).get(strDeptID) as GenericObject;
      return workGroup;
    }

    /// <summary>
    /// Type6Record_EntryDetail type6Record = new Type6Record_EntryDetail();
    /// type6Record.RecordType = "6";
    /// type6Record.TransactionCode = "27";
    /// type6Record.Receiving_DFI_RT_Number = "07192321";
    /// type6Record.RT_Number_CheckDigit = "3";
    /// type6Record.Receiving_DFI_Account_Number = "0558769606";
    /// type6Record.Amount = "100000";
    /// type6Record.IndividualID = "201213900100001";
    /// type6Record.IndividualName = "ABC COMPANY";
    /// type6Record.DiscretionaryData = " ";
    /// type6Record.AddendaRecordIndicator = "0";
    /// type6Record.TraceNumber = "091000010000001";
    /// Bug 27559 DJD: Allow ACH Check Reversals and Include Credit Entries in the ACH Batch
    /// </summary>
    /// <param name="tender"></param>
    /// <param name="sysInt"></param>
    /// <param name="accountType"></param>
    /// <param name="tenderSeqNum"></param>
    /// <param name="strError"></param>
    /// <returns></returns>
    public bool AddEntryDetailDebitOrCredit(GenericObject tender, GenericObject sysInt, GenericObject workgroup, string accountType, string debitIndividualName, int tenderSeqNum, bool reversal, ref string strError)
    {
      strError = "";
      try
      {
        Type6Record_EntryDetail tmpEntryDetailRecord = new Type6Record_EntryDetail();
        tmpEntryDetailRecord.RecordType = "6";
        // Bug 27559 DJD: A reversal (negative amount) will be a credit (not a debit)
        if (accountType.ToLower() == "checking account")
        {
          tmpEntryDetailRecord.TransactionCode = reversal ? "22" : "27";
        }
        else if (accountType.ToLower() == "saving account")
        {
          tmpEntryDetailRecord.TransactionCode = reversal ? "32" : "37";
        }
        string strBankRountingNbr = tender.get("BANKROUTINGNBR", "").ToString();
        if (strBankRountingNbr.Length == 9)
        {
          tmpEntryDetailRecord.Receiving_DFI_RT_Number = strBankRountingNbr.Substring(0, 8);
          tmpEntryDetailRecord.RT_Number_CheckDigit = strBankRountingNbr.Substring(8, 1);
        }
        string strEncryptedAccountNumber = tender.get("BANKACCTNBR", "") as string;
        if (!string.IsNullOrEmpty(strEncryptedAccountNumber))
        {
          string secret_key = Crypto.get_secret_key();
          byte[] content_bytes = Convert.FromBase64String(strEncryptedAccountNumber);
          byte[] decrypted_bytes = Crypto.symmetric_decrypt("data", content_bytes, "secret_key", secret_key);
          tmpEntryDetailRecord.Receiving_DFI_Account_Number = System.Text.Encoding.Default.GetString(decrypted_bytes);
        }
        //tmpEntryDetailRecord.Receiving_DFI_Account_Number = geoTender.get("BANKACCTNBR", "").ToString();
        string strAmount = tender.get("AMOUNT", "0.0").ToString();
        if (reversal) { strAmount = strAmount.Replace("-", ""); } // Bug 27559 DJD
        tmpEntryDetailRecord.Amount = Math.Round((Decimal.Parse(strAmount) * 100)).ToString();

        // BEGIN Bug 21927 [19081] - DJD: ACH Batch Update: Use Check Number in Detail Record Field 7 for Specific SEC Codes
        // ID number for the receiver, such as an employee number or customer number. For transactions converted
        // from checks (items with SEC code ARC, BOC, POP, or RCK), this field is required and MUST include the 
        // check serial number, left-justified.
        string sEC = m_CurrentBatchHeader.StandardEntryClass.ToUpper();
        if ((sEC == "ARC") || (sEC == "BOC") || (sEC == "POP") || (sEC == "RCK"))
        {
          string checkNumber = tender.get("CC_CK_NBR", "") as string;
          if (!string.IsNullOrEmpty(checkNumber))
          {
            if (sEC == "POP")
            {
              tmpEntryDetailRecord.IndividualID = GetIndividualIDforPOP(workgroup, checkNumber);
            }
            else
            {
              tmpEntryDetailRecord.IndividualID = checkNumber;
            }
          }
          else
          {
            string strLogErr = string.Format("No check number found for an ACH item with SEC code: {0}. Individual ID field was not populated.", sEC);
            Logger.LogError(strLogErr, "ACH Processing Error");
          }
        }
        else // END Bug 21927 [19081] - DJD: ACH Batch Update: Use Check Number in Detail Record Field 7 for Specific SEC Codes
        {
          // Bug 4387 - DJD: Handle More Than 999 CORE Files Per Day [YYYYJJJSSSS] (Reformat IndividualID using 4 chars for DEPFILESEQ and EVENTNBR)
          tmpEntryDetailRecord.IndividualID = tender.get("DEPFILENBR", "").ToString()
          + tender.get("DEPFILESEQ", "").ToString().PadLeft(4, '0')
          + tender.get("EVENTNBR", "").ToString().PadLeft(4, '0');
        }

        //tmpEntryDetailRecord.IndividualName = SysInt.get("Company_Name_short", "").ToString();
        tmpEntryDetailRecord.IndividualName = debitIndividualName;

        if ((sEC == "WEB") || (sEC == "TEL"))
          tmpEntryDetailRecord.DiscretionaryData = "S ";
        else
          tmpEntryDetailRecord.DiscretionaryData = "  ";
        tmpEntryDetailRecord.AddendaRecordIndicator = "0";
        tmpEntryDetailRecord.TraceNumber = (GetOrigDFI_ID(sysInt)) + tenderSeqNum.ToString().PadLeft(7, '0'); // Bug 21927 DJD
        if (tmpEntryDetailRecord.IsValid(ref strError)) // Bug 14469
        {
          m_getoCurrentBatchRecords.insert(tmpEntryDetailRecord);
          // BEGIN Bug 27559 DJD: Allow ACH Check Reversals and Include Credit Entries in the ACH Batch
          if (reversal)
          {
            m_dBatchCreditTotal += Decimal.Parse(strAmount); // Add to Credit Total
          }
          else
          {
            m_dBatchDebitTotal += Decimal.Parse(strAmount); // Add to Debit Total
          }
          // END Bug 27559 DJD
          m_longCurrentEntryHash += Int64.Parse(tmpEntryDetailRecord.Receiving_DFI_RT_Number); //add to Entry Hash
          return true;
        }
        else
        {
          strError = string.Format("Error in AddEntryDetailDebitOrCredit: Entry Detail Record is invalid, Entry Detail SeqNum = {0}. The following fields are invalid:{1} ", tenderSeqNum, strError);
          return false;
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in ACHFile AddEntryDetailDebitOrCredit", e);

        strError = string.Format("Error in AddEntryDetailDebitOrCredit: {0} Entry Detail SeqNum = {1}", e.Message, tenderSeqNum);
        return false;
      }
    }

    /// <summary>
    /// Bug 21927 DJD
    /// Positions 40-54 of a POP Entry Detail Record represent three separate fields:
    /// (1) the check serial number (positions 40-48)
    /// (2) the truncated name or abbreviation of the city or town in which the electronic terminal is located (positions 49-52)
    /// (3) the state in which the electronic terminal is located (positions 53-54).
    /// These three fields are required to be included in the Individual Identification field in the Entry Detail Record.
    /// </summary>
    /// <param name="workgroup"></param>
    /// <param name="checkNumber"></param>
    /// <returns></returns>
    private static string GetIndividualIDforPOP(GenericObject workgroup, string checkNumber)
    {
      string individualID = "";

      // Check Number - 9 characters
      int length = 9;
      checkNumber = checkNumber.PadRight(length).Substring(0, length);

      // Terminal City - 4 characters
      string terminalCity = workgroup.get("Terminal_City", "").ToString();
      if (string.IsNullOrEmpty(terminalCity))
      {
        terminalCity = " ";
        string err = "No Terminal City configured at the Workgroup level for the POP SEC code. Individual ID field was not populated with this value.";
        Logger.LogError(err, "ACH Processing Error");
      }
      length = 4;
      terminalCity = terminalCity.PadRight(length).Substring(0, length);

      // Terminal State - 2 characters
      string terminalState = workgroup.get("Terminal_State", "").ToString();
      if (string.IsNullOrEmpty(terminalState))
      {
        terminalState = " ";
        string err = "No Terminal State configured at the Workgroup level for the POP SEC code. Individual ID field was not populated with this value.";
        Logger.LogError(err, "ACH Processing Error");
      }
      length = 2;
      terminalState = terminalState.PadRight(length).Substring(0, length);

      // Build Individual ID - 15 characters
      individualID = string.Format("{0}{1}{2}", checkNumber, terminalCity, terminalState);

      return individualID;
    }

    // Bug 21927 DJD
    private static string GetOrigDFI_ID(GenericObject SysInt)
    {
      string oDI = SysInt.get("Originating_DFI_ID", "").ToString();
      oDI = oDI.Substring(0, Math.Min(oDI.Length, 8)); // Get first 8 chars of "Originating_DFI_ID" (Do not include checksum digit)
      return oDI;
    }

    /// <summary>
    /// Type6Record_EntryDetail type6Record = new Type6Record_EntryDetail();
    /// type6Record.RecordType = "6";
    /// type6Record.TransactionCode = "22";
    /// type6Record.Receiving_DFI_RT_Number = "07192321";
    /// type6Record.RT_Number_CheckDigit = "3";
    /// type6Record.Receiving_DFI_Account_Number = "0558769606";
    /// type6Record.Amount = "100000";
    /// type6Record.IndividualID = "2012139000001";
    /// type6Record.IndividualName = "ABC COMPANY";
    /// type6Record.DiscretionaryData = " ";
    /// type6Record.AddendaRecordIndicator = "0";
    /// type6Record.TraceNumber = "091000010000001";
    /// Bug 27559 DJD: Allow ACH Check Reversals and Include Credit Entries in the ACH Batch
    /// This method will add a credit (or possibly a debit) for banks that require a "balanced" file (i.e., one
    /// where the total amount of the credits equals the total amount of the debits).
    /// </summary>
    /// <param name="SysInt"></param>
    /// <param name="TenderSeqNum"></param>
    /// <param name="strError"></param>
    /// <returns></returns>
    public bool AddEntryDetailToBalanceBatch(GenericObject SysInt, int TenderSeqNum, GenericObject pCurrentFile, ref string strError)
    {
      try
      {
        // Bug 27559 DJD: If credits and debits are already equal then there is no need to add an entry to balance the batch
        if (m_dBatchCreditTotal == m_dBatchDebitTotal) 
        {
          strError = ACHFileBatchUpdate.NO_RECORD_ADDED;
          return true;
        }

        // Bug 27559 DJD: If total of debits is greater than total of credits then add a credit to balance (otherwise, add a debit)
        bool addCredit = m_dBatchDebitTotal > m_dBatchCreditTotal;
        decimal entryDetailAmt = Math.Abs(m_dBatchDebitTotal - m_dBatchCreditTotal);

        strError = "";
        Type6Record_EntryDetail tmpEntryDetailRecord = new Type6Record_EntryDetail();
        tmpEntryDetailRecord.RecordType = "6";
        string transCode = SysInt.get("Transaction_Code", "").ToString();
        if (!addCredit) // Bug 27559 DJD: May need to switch to "debit" Transaction Code
        {
          if      (transCode == "22") { transCode = "27"; }
          else if (transCode == "32") { transCode = "37"; }
        }
        tmpEntryDetailRecord.TransactionCode = transCode;
        tmpEntryDetailRecord.Receiving_DFI_RT_Number = SysInt.get("Receiving_DFI_RT_number", "").ToString();
        tmpEntryDetailRecord.RT_Number_CheckDigit = SysInt.get("RT_number_check_digit", "").ToString();
        tmpEntryDetailRecord.Receiving_DFI_Account_Number = SysInt.get("Receiving_DFI_account_number", "").ToString();
        tmpEntryDetailRecord.Amount = Math.Round((entryDetailAmt * 100)).ToString();

        // BEGIN Bug 27559 DJD: Allow ACH Check Reversals and Include Credit Entries in the ACH Batch
        if (addCredit)
        {
          m_dBatchCreditTotal += entryDetailAmt;
        }
        else // Adding debit
        {
          m_dBatchDebitTotal += entryDetailAmt;
        }
        // END Bug 27559 DJD

        // Bug 21927 DJD - Get current CORE file number for Entry Detail Credit
        string CurrentCOREFile = ((GenericObject)(pCurrentFile.vectors[0])).get("FILENAME", "").ToString().Trim();
        tmpEntryDetailRecord.IndividualID = CurrentCOREFile;

        tmpEntryDetailRecord.IndividualName = SysInt.get("Company_Name_short", "").ToString();
        if ((m_CurrentBatchHeader.StandardEntryClass.ToUpper() == "WEB") ||
        (m_CurrentBatchHeader.StandardEntryClass.ToUpper() == "TEL"))
          tmpEntryDetailRecord.DiscretionaryData = "S ";
        else
          tmpEntryDetailRecord.DiscretionaryData = "  ";
        tmpEntryDetailRecord.AddendaRecordIndicator = "0";
        tmpEntryDetailRecord.TraceNumber = (GetOrigDFI_ID(SysInt)) + TenderSeqNum.ToString().PadLeft(7, '0'); // Bug 21927 DJD
        if (tmpEntryDetailRecord.IsValid(ref strError)) //Bug#14469
        {
          m_getoCurrentBatchRecords.insert(tmpEntryDetailRecord);
          m_longCurrentEntryHash += Int64.Parse(tmpEntryDetailRecord.Receiving_DFI_RT_Number); //Add to Entry Hash
          return true;
        }
        else
        {
          strError = string.Format("Error in AddEntryDetailCredit: Entry Detail Record is invalid, Tender SeqNum = {0}. The following fields are invalid:{1}", TenderSeqNum, strError);
          return false;
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in ACHFile AddEntryDetailCredit", e);

        strError = string.Format("Error in AddEntryDetailCredit: {0} Tender SeqNum = {1}", e.Message, TenderSeqNum);
        return false;
      }
    }

    /// <summary>
    /// Type8Record_BatchControl type8Record = new Type8Record_BatchControl();
    /// type8Record.RecordType = "8";
    /// type8Record.ServiceClassCode = "200";
    /// type8Record.EntryAddendaCount = "3";
    /// type8Record.EntryHash = "16292323";
    /// type8Record.TotalDebitEntryAmount = "900000";
    /// type8Record.TotalCreditEntryAmount = "0";
    /// type8Record.CompanyID = "2123456789";
    /// type8Record.MessageAuthenticationCode = " ";
    /// type8Record.Blank = " ";
    /// type8Record.OriginatingDFI_ID = "09100001";
    /// type8Record.BatchNumber = "1";
    /// </summary>
    /// <param name="TenderSeqNum"></param>
    /// <param name="strError"></param>
    /// <returns></returns>
    public bool AddBatchControlRecord(int TenderSeqNum, ref string strError)
    {
      strError = "";
      try
      {
        Type8Record_BatchControl tmpBatchControlRecord = new Type8Record_BatchControl();
        tmpBatchControlRecord.RecordType = "8";
        //Get Current Batch Header Record
        tmpBatchControlRecord.ServiceClassCode = m_CurrentBatchHeader.ServiceClassCode;
        tmpBatchControlRecord.EntryAddendaCount = TenderSeqNum.ToString();
        tmpBatchControlRecord.EntryHash = (m_longCurrentEntryHash % 10000000000).ToString();
        tmpBatchControlRecord.TotalDebitEntryAmount = Math.Round(m_dBatchDebitTotal * 100).ToString();
        tmpBatchControlRecord.TotalCreditEntryAmount = Math.Round(m_dBatchCreditTotal * 100).ToString();
        tmpBatchControlRecord.CompanyID = m_CurrentBatchHeader.CompanyID;
        tmpBatchControlRecord.MessageAuthenticationCode = " ";
        tmpBatchControlRecord.Blank = " ";
        tmpBatchControlRecord.OriginatingDFI_ID = m_CurrentBatchHeader.OriginatingDFI_ID;
        tmpBatchControlRecord.BatchNumber = m_CurrentBatchHeader.BatchNumber;
        if (tmpBatchControlRecord.IsValid(ref strError)) //Bug#14469
        {
          m_CurrentBatchControl = tmpBatchControlRecord;
          return true;
        }
        else
        {
          strError = string.Format("Error in AddBatchControlRecord: Batch Control Record is invalid, Batch Number = {0}. The following fields are invalid:{1} ", tmpBatchControlRecord.BatchNumber, strError);
          return false;
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in ACHFile AddBatchControlRecord", e);

        strError = string.Format("Error in AddBatchControlRecord: {0}", e.Message);
        return false;
      }
    }
    /// <summary>
    /// Type9Record_FileControl type9Record = new Type9Record_FileControl();
    /// type9Record.RecordType = "9";
    /// type9Record.BatchCount = "2";
    /// type9Record.BlockCount = "1";
    /// type9Record.EntryAddendaCount = "4";
    /// type9Record.EntryHashTotal = "26692327";
    /// type9Record.TotalFileDebitEntryAmount = "900000";
    /// type9Record.TotalFileCreditEntryAmount = "200000";
    /// type9Record.Filler = " ";
    /// </summary>
    /// <param name="strError"></param>
    /// <returns></returns>
    public bool AddFileControlRecord(ref string strError)
    {
      try
      {
        Type9Record_FileControl tmpFileControlRecord = new Type9Record_FileControl();
        tmpFileControlRecord.RecordType = "9";
        tmpFileControlRecord.BatchCount = (m_intBatchNumber - 1).ToString();
        // BlockCount = (Entry Count + 2 * (Batch Number) + 2(File Header/Control))
        //            divided by ten and Rounded up: (N - 1) DIV 10 + 1
        tmpFileControlRecord.BlockCount = ((m_intFileEntryCount + 2 * m_intBatchNumber - 1) / 10 + 1).ToString();
        // We do not support Addenda for now. Only count of Entry detail records
        tmpFileControlRecord.EntryAddendaCount = (m_intFileEntryCount).ToString();
        tmpFileControlRecord.EntryHashTotal = (m_longTotalEntryHash % 10000000000).ToString();
        tmpFileControlRecord.TotalFileDebitEntryAmount = Math.Round(m_dFileDebitTotal * 100).ToString();
        tmpFileControlRecord.TotalFileCreditEntryAmount = Math.Round(m_dFileCreditTotal * 100).ToString();
        tmpFileControlRecord.Filler = " ";
        if (tmpFileControlRecord.IsValid(ref strError)) //Bug#14469
        {
          m_FileControlRecord = tmpFileControlRecord;
          return true;
        }
        else
        {
          strError = string.Format("File Control Record is invalid. File ID Modifier = {0}. The following fields are invalid: {1}", m_FileHeaderRecord.FileIDModifier, strError);
          return false;
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in ACHFile AddFileControlRecord", e);

        strError = string.Format("{0} File ID Modifier is {1}", e.Message, m_FileHeaderRecord.FileIDModifier);
        return false;
      }
    }

    /// <summary>
    /// Update BatchUpdateInfo instance after a Batch block is successfully created
    /// </summary>
    public void Update()
    {
      m_intBatchNumber++;
      m_dFileDebitTotal += m_dBatchDebitTotal;
      m_dFileCreditTotal += m_dBatchCreditTotal;
      m_longTotalEntryHash = (m_longTotalEntryHash + m_longCurrentEntryHash) % 10000000000;
    }

    /// <summary>
    /// Clean up Current Batch block info
    /// </summary>
    public void CleanUpCurrentBatch()
    {
      m_dBatchCreditTotal = 0;
      m_dBatchDebitTotal = 0;
      m_longCurrentEntryHash = 0;
      m_getoCurrentBatchRecords.removeAll();
      m_CurrentBatchHeader = null;
      m_CurrentBatchControl = null;
      m_CurrentBatchRecordCount = 1;
    }

    #endregion
  }

  public class ACHFileBatchUpdate : AbstractBatchUpdate
  {
    public const string NO_RECORD_ADDED = "{NO_RECORD_ADDED}";             // Bug 27559 DJD: Allow ACH Check Reversals and Include Credit Entries in the ACH Batch

    #region Public Members
    public ACHFileBatchInfo m_ACHFileBatchInfo = new ACHFileBatchInfo();   // ACH File info
    public GenericObject m_ConfigSystemInterface = null;                   // System interface 
    public OutputConnection m_OutputConnection = null;                     // File Connection 
    public string m_strTempFileName = "";                                  // ACH Local File path
    public string m_transferMethod = "";                                   // ACH File Transfer Method (Bug 22001 DJD - Add Folder Option to File Transfer Method)
    public bool bTerminateBatchUpdate = false;                             // Bug 13226 QG: used for prevent from creating and sending ACH File when error occurs
    public bool? m_AllowRevCredits = null;                                 // Bug 27559 DJD: Allow ACH Check Reversals and Include Credit Entries in the ACH Batch
    #endregion

    /// <summary>
    /// Constructor
    /// </summary>
    public ACHFileBatchUpdate() { }

    #region Non-override public Methods

    /// <summary>
    /// Check if tender is a check and has system interface assigned to it.
    /// </summary>
    /// <param name="geoTenderClass"></param>
    /// <param name="SysInt"></param>
    /// <returns></returns>
    public bool IsValidACHTender(GenericObject geoTenderClass, GenericObject SysInt)
    {
      // Tender is valid only if it is a check and has ACH SI assigned to it.
      GenericObject geoSystemInterfaces = geoTenderClass.get("system_interfaces") as GenericObject;
      GenericObject geoParent = geoTenderClass.get("_parent") as GenericObject;
      if (geoSystemInterfaces.has(SysInt) && ((geoParent.get("type", "").ToString()) == "Check"))
        return true;
      else
        return false;
    }

    /// <summary>
    /// Write ACH File
    /// Called by Project_ProcessDeposits (Write a CORE File information into ACH File)
    /// and Project_WriteTrailer (Write File Control Record - the final line into ACH File)
    /// </summary>
    /// <param name="strFileName"></param>
    /// <param name="lettercase"> Indicate the letter case of content in output file</param>
    /// <param name="IsFileControlRecord"> File Control Record (The last line of the output) is treated differently. </param>
    /// <param name="strError"></param>
    /// <returns></returns>
    public bool CreateAndWriteACHFile(string strFileName, string lettercase, bool IsFileControlRecord, ref string strError)
    {
      try
      {
        if (m_OutputConnection == null)
          m_OutputConnection = new OutputConnection();
        if (File.Exists(strFileName))
          m_OutputConnection.m_pOutputFile = File.AppendText(strFileName);
        else
        {
          m_OutputConnection.m_pOutputFile = new StreamWriter(strFileName);
          m_OutputConnection.m_pOutputFile.WriteLine(Convert_Case(m_ACHFileBatchInfo.m_FileHeaderRecord.ToString(), lettercase));
        }
        if (IsFileControlRecord)
          m_OutputConnection.m_pOutputFile.WriteLine(Convert_Case(m_ACHFileBatchInfo.m_FileControlRecord.ToString(), lettercase));
        else
        {
          m_OutputConnection.m_pOutputFile.WriteLine(Convert_Case(m_ACHFileBatchInfo.m_CurrentBatchHeader.ToString(), lettercase));
          foreach (object record in m_ACHFileBatchInfo.m_getoCurrentBatchRecords.vectors)
          {
            m_OutputConnection.m_pOutputFile.WriteLine(Convert_Case(record.ToString(), lettercase));
            m_ACHFileBatchInfo.m_intFileEntryCount++;
          }
          m_OutputConnection.m_pOutputFile.WriteLine(Convert_Case(m_ACHFileBatchInfo.m_CurrentBatchControl.ToString(), lettercase));
        }
        m_OutputConnection.m_pOutputFile.Flush();
        m_OutputConnection.m_pOutputFile.Close();
        return true;
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in ACHFile CreateAndWriteACHFile", e);

        strError = string.Format("Cannot Write to file({0}):{1}", strFileName, e.Message);
        return false;
      }
    }

    string Convert_Case(string strtext, string lettercase)
    {
      switch (lettercase.ToLower())
      {
        case "normal": return strtext;
        case "upper case": return strtext.ToUpper();
        case "lower case": return strtext.ToLower();
        default: return strtext;
      }
    }

    /// <summary>
    /// Bug 21902 DJD: Configurable File Name for ACH Batch Update System Interface
    /// Generate a custom configured or standard ACH File name:
    /// "ACH" +
    /// Creation Date (MMddyy) +
    /// Createion Time (HHmm) +
    /// Increment ID modifier(A B C ... for files created in the same day)
    /// "ACH1206191706H"
    /// </summary>
    /// <returns></returns>
    public string GetACHFileName()
    {
      string strACHFileName = FormatFileNameWithDateTime(m_ConfigSystemInterface.get("custom_file_name", "") as string);
      if (string.IsNullOrEmpty(strACHFileName))
      {
        strACHFileName = "ACH" +
        m_ACHFileBatchInfo.m_FileHeaderRecord.FileCreationDate +
        m_ACHFileBatchInfo.m_FileHeaderRecord.FileCreationTime +
        m_ACHFileBatchInfo.m_FileHeaderRecord.FileIDModifier;
      }
      return strACHFileName;
    }

    /// <summary>
    /// Bug 21902 DJD: Configurable File Name for ACH Batch Update System Interface
    /// This method returns the custom file name with formatted:
    ///   Date/time portion in straight brackets with file creation date/time. e.g., For example, "[MMddyyHHmm]"
    ///   File ID Modifier constant in curly brackets. e.g., "{FILE_ID}"
    /// Example: "ACH[MMddyyHHmm]{FILE_ID}"
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    public string FormatFileNameWithDateTime(string fileName)
    {
      // Format date time in file name
      if (fileName.Contains("[") && fileName.Contains("]"))
      {
        int idxStart = fileName.IndexOf("[");
        int idxFinish = fileName.IndexOf("]");
        if (idxStart < idxFinish)
        {
          string dateFormat = fileName.Substring(idxStart, (idxFinish - idxStart + 1));
          string datePortion = dateFormat.Length > 2 ?
            m_ACHFileBatchInfo.m_FileCreationDateTime.ToString(dateFormat.Substring(1, dateFormat.Length - 2)) : "";
          fileName = fileName.Replace(dateFormat, datePortion);
        }
      }

      // Format File ID Modifier in file name
      const string sFILE_ID = "{FILE_ID}";
      fileName = fileName.Replace(sFILE_ID, m_ACHFileBatchInfo.m_FileHeaderRecord.FileIDModifier);

      return fileName;
    }


    /// <summary>
    /// Bug 21902 DJD: Configurable File Name for ACH Batch Update System Interface
    /// Save the current ACH File name into DB, use it to determine the next file's ID modifier using 
    /// the date and current file id modifier in string format: yyMMdd{File ID}
    /// Removed sys_int name: bug 27212
    /// </summary>
    /// <param name="pBatchUpdate_ProductLevel"></param>
    public void Update_File_ID_Modifier(BatchUpdate_ProductLevel pBatchUpdate_ProductLevel)
    {
      // Generate string value using the file creation date and current file id: yyMMdd{File ID}
      string fileID_Value = string.Format("{0:yyMMdd}{1}"
        , m_ACHFileBatchInfo.m_FileCreationDateTime
        , m_ACHFileBatchInfo.m_FileHeaderRecord.FileIDModifier);

      // Update value in CASL(memory)
      // Bug: 27212: Set the global File_ID_Modifier.  Don't bother with the one in the system interface
      misc.CASL_call("subject", c_CASL.c_object("Business.Misc.data"), "a_method", "set", "args", new GenericObject("ACH_Global_File_ID_Modifier", fileID_Value));

      // Update CBT Table
      string strError = "";

      // Bug 15800 MJO - Use Config DB
      GenericObject database = c_CASL.c_object("TranSuite_DB.Config.db_connection_info") as GenericObject;
      ConnectionTypeSynch pDBConn = null;

      try
      {
        pDBConn = new ConnectionTypeSynch(database);
        string err = null;
        if (!pDBConn.EstablishDatabaseConnection(ref err) || err != null)
        {
          throw CASL_error.create("message", "Error opening database connection.");
        }
        // Bug: 27212: Set the global File_ID_Modifier not the one in the system interface
        string strSQL = string.Format("UPDATE CBT_FLEX SET [field_value] = '\"{0}\"' WHERE [container] = 'Business.Misc.data' and [field_key] = '\"ACH_Global_File_ID_Modifier\"'", fileID_Value);
        if (!pDBConn.ExecuteSQL_NO_RollBack(strSQL, ref strError))
        {
          throw CASL_error.create("message", "Unable to update File_ID_modifier");
        }
      }
      finally // Clean up
      {
        if (pDBConn != null)
        {
          pDBConn.CloseDBConnection();
          pDBConn = null;
        }
      }
    }

    /// <summary>
    /// Send Notification Email. Use Email server and Email from which are defined in config.casl
    /// </summary>
    /// <param name="strEmail_to"></param>
    /// <param name="strSubject"></param>
    /// <param name="strBody"></param>
    public void Send_Notification_Email(string strEmail_to, string strSubject, string strBody)
    {
      try
      {
        string strEmail_from   = c_CASL.GEO.get("ipayment_sender_email", "") as string;
        string strEmail_server = c_CASL.GEO.get("smtp_host", "") as string; // Bug 22627 UMN
        string strProvider = c_CASL.GEO.get("smtp_provider", "") as string; // Bug 26910 NAM
        string strPort = c_CASL.GEO.get("smtp_port", "") as string;
        if (!String.IsNullOrEmpty(strEmail_from) && !String.IsNullOrEmpty(strEmail_to) && !String.IsNullOrEmpty(strEmail_server))
        {
          emails.CASL_email(
          "to", strEmail_to,
          "from", strEmail_from,
          "host_smtp_server", strEmail_server,
          "subject", strSubject,
          "body", strBody,
          "provider", strProvider,      // Bug 26910 NAM
          "port", strPort   
          );
        }
        else
        {
          Logger.LogError("Failed to send email: Email address can not be empty.", "EMAIL SEND");
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in ACHFile Send_Notification_Email", e);

        Logger.LogError(String.Format("Failed to send email: {0}", e.Message), "EMAIL SEND");
      }
    }

    /// <summary>
    /// Individual Name for Debit Entry Detail is retrieved from Custom Fields of Transaction/Tender.
    /// Use 2 fields defined in SI(Individual_Name_From - transactions/tenders;   Individual_Name_Tag_Name)
    /// to get the custom field value.
    /// </summary>
    /// <param name="geoTrans"></param>
    /// <param name="geoTenders"></param>
    /// <param name="SysInt"></param>
    /// <returns></returns>
    public string GetDebitIndividualName(GenericObject geoTrans, GenericObject geoTenders, GenericObject SysInt)
    {
      try
      {
        string strSource = SysInt.get("Individual_Name_From", "").ToString();
        string strTag = SysInt.get("Individual_Name_Tag_Name", "").ToString();
        if (strSource.ToLower() == "transaction")
        {
          int iCountofTrans = geoTrans.getIntKeyLength();
          for (int i = 0; i < iCountofTrans; i++)
          {
            GenericObject geoTran = (GenericObject)geoTrans[i];
            GenericObject geoCustFields = geoTran.get("GR_CUST_FIELD_DATA", new GenericObject()) as GenericObject;
            int iCountofCF = geoCustFields.getIntKeyLength();
            for (int j = 0; j < iCountofCF; j++)
            {
              GenericObject geoCustField = (GenericObject)geoCustFields.get(j);
              if (geoCustField.get("CUSTTAG", "").ToString() == strTag)
              {
                string strValue = geoCustField.get("CUSTVALUE", "").ToString();
                if (!(String.IsNullOrEmpty(strValue)))
                  return strValue;
              }
            }
          }
          return "";
        }
        else if (strSource.ToLower() == "tender")
        {
          int iCountofTenders = geoTenders.getIntKeyLength();
          for (int i = 0; i < iCountofTenders; i++)
          {
            string strValue = "";
            GenericObject geoTender = (GenericObject)geoTenders.get(i); // Bug 21977 - DJD: ACH Batch Update "Individual Name" Not Working With Tender
            GenericObject geoCustFields = geoTender.get("CUST_FIELD_DATA", new GenericObject()) as GenericObject;
            int iCountofCF = geoCustFields.getIntKeyLength();
            for (int j = 0; j < iCountofCF; j++)
            {
              GenericObject geoCustField = (GenericObject)geoCustFields.get(j);
              if (geoCustField.get("CUSTTAG", "").ToString() == strTag)
              {
                strValue = geoCustField.get("CUSTVALUE", "").ToString();
                if (!(String.IsNullOrEmpty(strValue)))
                  return strValue;
              }
            }
            // If the value is not found in the Custom Fields then it may be a "Tender Standard Field" (Check tender)
            strValue = geoTender.get(strTag, "").ToString();
            if (!(String.IsNullOrEmpty(strValue)))
            {
              return strValue;
            }
          }
          return "";
        }
        else
          return "";
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in ACHFile GetDebitIndividualName", e);

        Logger.LogError(String.Format("Failed to get Debit Individual Name {0}", e.Message), "EMAIL SEND");
        return "";
      }
    }

    // Bug 22380 DJD: Removed Deprecated ACH PGP Encryption 

    #endregion

    #region Override Methods

    public override AbstractBatchUpdate GetInstance(GenericObject sysInt)
    {
      return base.GetInstance(sysInt);
    }

    /// <summary>
    /// ACHBatchUpdate is the system_interface type ID 
    /// </summary>
    /// <returns></returns>
    public override string GetCASLTypeName()
    {
      return "ACHBatchUpdate";
    }

    public override bool Project_Cleanup(bool bEarlyTerminationRequestedByProject, object pProductLevelMainClass)
    {
      // Bug 26662 MJO - Skip if this wasn't actually run
      if (m_ConfigSystemInterface == null)
        return true;

      string strError = "";
      try
      {
        string strBatchFTPAddress = m_ConfigSystemInterface.get("ftp_server", "").ToString();
        
        // Clear global values here
        m_ConfigSystemInterface = null;
        m_ACHFileBatchInfo = null;
        m_OutputConnection = null;
        
        // Delete temporary file if FTP or SFTP
        if (File.Exists(m_strTempFileName) && m_transferMethod != "folder") // Bug 22001 DJD - Add Folder Option to File Transfer Method
        {
          File.Delete(m_strTempFileName);
        }
        m_strTempFileName = "";
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in ACHFile CleanUp", e);

        strError = "ACHFile CleanUp:" + e.Message;
        Logger.LogError(e.Message, "ACHFile CleanUp");
        return false;
      }
      return true;
    }

    public override bool Project_ProcessCoreEvent(string strSystemInterface, GenericObject pEventGEO, GenericObject pConfigSystemInterface, GenericObject pFileGEO, object pProductLevelMainClass, string strCurrentFile, ref eReturnAction ReturnAction)
    {
      CASL_engine.BatchUpdate_ProductLevel pBatchUpdate_ProductLevel = (BatchUpdate_ProductLevel)pProductLevelMainClass;
      string strError = "";
      string strSubject;
      string strBody;
      string strEmail_to;
      try
      {
        pFileGEO = pFileGEO.get(0, null) as GenericObject;
        if (m_ConfigSystemInterface == null)
          m_ConfigSystemInterface = pConfigSystemInterface;

        if (m_ACHFileBatchInfo.m_FileHeaderRecord == null)
        {
          // Log and send ACH File Creation notification email
          Logger.LogInfo("", "START ACH File Creation Process.");
          strEmail_to = (string)pConfigSystemInterface.get("Notification_Email_To", ""); // Bug 16549 DJD: Error If NO ACH BU SI Email Configured [Changed ToString() to cast] 
          strSubject = "START ACH File Creation Process";
          strBody = "START ACH File Creation Process";
          Send_Notification_Email(strEmail_to, strSubject, strBody);

          // Add File Header Record
          if (!m_ACHFileBatchInfo.AddFileHeaderRecord(pConfigSystemInterface, pFileGEO, ref strError))
          {
            throw new Exception(strError);
          }
          // Bug: 27212: Increment the global File_ID_Modifier immediatly
          Update_File_ID_Modifier(pBatchUpdate_ProductLevel); // Bug 21902 DJD: Configurable File Name for ACH Batch Update System Interface

        }
        
        // Add Batch Header Record
        if (m_ACHFileBatchInfo.m_CurrentBatchHeader == null)
        {
          if (!m_ACHFileBatchInfo.AddBatchHeaderRecord(m_ConfigSystemInterface, pFileGEO, ref strError))
          {
            throw (new Exception(strError));
          }
        }

        if (!m_AllowRevCredits.HasValue) // Bug 27559 DJD: Allow ACH Check Reversals and Include Credit Entries in the ACH Batch
        {
          m_AllowRevCredits = (bool)m_ConfigSystemInterface.get("allow_ACH_credit_for_check_reversal", false);
        }

        GenericObject geoTransactions = pEventGEO.get("TRANSACTIONS", new GenericObject()) as GenericObject;
        GenericObject geoTenders = pEventGEO.get("TENDERS", new GenericObject()) as GenericObject;
        int iCountofTenders = geoTenders.getIntKeyLength();

        // Get Individual Name for Debit entry detail records.
        string strDebitIndividualName = GetDebitIndividualName(geoTransactions, geoTenders, pConfigSystemInterface);
        if (String.IsNullOrEmpty(strDebitIndividualName))
          strDebitIndividualName = "Name Unknown";

        // Bug 21927 DJD - Get the workgroup for the CORE file
        GenericObject workgroup = ACHFileBatchInfo.GetWorkgroupFromCoreFile(pFileGEO);

        // Bug 27559 DJD: Add Entry Detail Record [debit or credit (for reversal)]
        for (int i = 0; i < iCountofTenders; i++)
        {
          GenericObject geoTender = (GenericObject)geoTenders.get(i);
          GenericObject geoTenderClass = (GenericObject)((GenericObject)c_CASL.c_object("Tender.of")).get(geoTender.get("TNDRID", "").ToString(), "");
          // BEGIN Bug 27559 DJD: Don't process zero tender or tender without SI
          if ((!IsValidACHTender(geoTenderClass, pConfigSystemInterface)) || (Decimal.Parse(geoTender.get("AMOUNT", "0.0").ToString()) == 0))
          {
            continue;
          }
          bool reversal = Decimal.Parse(geoTender.get("AMOUNT", "0.0").ToString()) < 0;
          if(reversal && !((bool)m_AllowRevCredits))
          {
            continue;
          }
          // END Bug 27559 DJD
          string strAccountType = geoTenderClass.get("account_type", "").ToString();
          if (!m_ACHFileBatchInfo.AddEntryDetailDebitOrCredit(geoTender, pConfigSystemInterface, workgroup, strAccountType, strDebitIndividualName, m_ACHFileBatchInfo.m_CurrentBatchRecordCount, reversal, ref strError))
          {
            throw new Exception(strError); // Bug 13226
          }
          else
          {
            m_ACHFileBatchInfo.m_CurrentBatchRecordCount++;
          }
        }
      }
      // Error handling
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in ACHFile Project_ProcessCoreEvent", e);

        strError = String.Format("Project_ProcessCoreEvent:{0} {1} {2}", strSystemInterface, strCurrentFile, e.Message).Replace("'", "");
        Logger.LogError(strError, "ACH Batch Update");
        pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError(strCurrentFile, strSystemInterface, strError);
        //BUG 13226 QG
        bTerminateBatchUpdate = true;
        strEmail_to = (string)pConfigSystemInterface.get("Notification_Email_To", ""); // Bug 16549 DJD: Error If NO ACH BU SI Email Configured [Changed ToString() to cast] 
        strSubject = "ACH File Batch Update Error.";
        strBody = String.Format("Error in ACH File Batch Update, {0}", strError).Replace("'", "");
        Send_Notification_Email(strEmail_to, strSubject, strBody);
        //End of Bug 13226
        return false;
      }
      return true;
    }

    public override bool Project_ProcessDeposits(GenericObject pFileDeposits, GenericObject pConfigSystemInterface, object pProductLevelMainClass, string strSysInterface, GenericObject pCurrentFile, ref eReturnAction ReturnAction)
    {
      string strError = "";
      string strFileName = pFileDeposits.get("FULL_FILE_NAME").ToString();


      if (m_ConfigSystemInterface == null)
      {
        m_ConfigSystemInterface = pConfigSystemInterface;
      }
      CASL_engine.BatchUpdate_ProductLevel pBatchUpdate_ProductLevel = (BatchUpdate_ProductLevel)pProductLevelMainClass;
      try
      {
        if (bTerminateBatchUpdate)  //Bug 13226
        {
          return false;
        }
        else if ((m_ACHFileBatchInfo.m_CurrentBatchRecordCount > 1) && ((m_ACHFileBatchInfo.m_dBatchDebitTotal > 0) || (m_ACHFileBatchInfo.m_dBatchCreditTotal > 0)))
        {
          // Bug 27559 DJD: Add Entry Detail Record for balanced file (usually credit - but possibly debit)
          if (!((string.IsNullOrEmpty(pConfigSystemInterface.get("Transaction_Code", "").ToString())) ||
                (string.IsNullOrEmpty(pConfigSystemInterface.get("Receiving_DFI_RT_number", "").ToString())) ||
                (string.IsNullOrEmpty(pConfigSystemInterface.get("RT_number_check_digit", "").ToString())) ||
                (string.IsNullOrEmpty(pConfigSystemInterface.get("Receiving_DFI_account_number", "").ToString()))))
          {
            if (m_ACHFileBatchInfo.AddEntryDetailToBalanceBatch(pConfigSystemInterface, m_ACHFileBatchInfo.m_CurrentBatchRecordCount, pCurrentFile, ref strError)) // Bug 21927 DJD
            {
              if (strError == ACHFileBatchUpdate.NO_RECORD_ADDED) // Bug 27559 DJD
              {
                strError = "";
                m_ACHFileBatchInfo.m_CurrentBatchRecordCount--;
              }
            }
            else
            {
              Logger.LogError(strError, "ACH Batch Update AddEntryDetailCredit");
              throw (new Exception(strError));
            }
          }
          else
          {
            m_ACHFileBatchInfo.m_CurrentBatchRecordCount--;
          }

          // Bug 27559 DJD: Service Class Code: Enter as 200 for credits and debits. 220 for credits only or 225 for debits only.
          if ((m_ACHFileBatchInfo.m_dBatchDebitTotal > 0) && (m_ACHFileBatchInfo.m_dBatchCreditTotal > 0))
          {
            m_ACHFileBatchInfo.m_CurrentBatchHeader.ServiceClassCode = "200"; // credits and debits
          }
          else
          {
            m_ACHFileBatchInfo.m_CurrentBatchHeader.ServiceClassCode = m_ACHFileBatchInfo.m_dBatchDebitTotal > 0 ? "225" : "220"; // debits only or credits only
          }

          // Bug 23008 [23026] - ACH Failure Caused By Bank Rec Code Update [Added "GeteCheckDepositForACH" method and several null & count checks]
          // Bug 22670/18217: Put deposit slip number in the CompanyDiscretionaryData field
          if (pFileDeposits != null && pFileDeposits.vectors.Count > 0)
          {
            GenericObject depositTableFields = GeteCheckDepositForACH(pFileDeposits, pConfigSystemInterface);
            if (depositTableFields != null)
            {
              string slipNumber = depositTableFields.get("DEPSLIPNBR", "").ToString();
              // Dennis suggested this change so that ACH system is kept "backwards compatible" 
              // with all the sites currently using it that do not have deposit slip 
              // numbers for eChecks.  
              // This condition needs to be added before overwriting the CORE File number in the 
              // "Company Discretionary Data" field:
              if (!string.IsNullOrWhiteSpace(slipNumber))
              {
                m_ACHFileBatchInfo.m_CurrentBatchHeader.CompanyDiscretionaryData = slipNumber;
              }
            }
          }
          // End bug 18217
          // End Bug 23008 [23026]
          // Add Batch Control Record
          if (!m_ACHFileBatchInfo.AddBatchControlRecord(m_ACHFileBatchInfo.m_CurrentBatchRecordCount, ref strError))
          {
            Logger.LogError(strError, "ACH Batch Update AddBatchControlRecord");
            throw (new Exception(strError));
          }
          //Write to Temporary text file
          if (string.IsNullOrEmpty(m_strTempFileName))
          {
            m_strTempFileName = Path.Combine(pConfigSystemInterface.get("local_temp_path", "").ToString(), GetACHFileName());
          }

          if (!CreateAndWriteACHFile(m_strTempFileName, pConfigSystemInterface.get("Letter_Case", "").ToString(), false, ref strError))
          {
            Logger.LogError(strError, "ACH Batch Update CreateAndWriteACHFile");
            throw (new Exception(strError));
          }
          m_ACHFileBatchInfo.Update();
          m_ACHFileBatchInfo.CleanUpCurrentBatch();
        }
        else
        {
          // No available Tender(checks),just skip the current event and continue.
          m_ACHFileBatchInfo.CleanUpCurrentBatch();
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in ACHFile Project_ProcessDeposits", e);

        // Bug 23008 [23026] - ACH Failure Caused By Bank Rec Code Update [Fixed and added more detailed logging]
        string currentCOREFile = (pCurrentFile.vectors.Count > 0) ? ((GenericObject)(pCurrentFile.vectors[0])).get("FILENAME", "").ToString().Trim() : "";
        string strErrorDetail = String.Format("Project_ProcessDeposits: {0} {1}{2}{3}", strSysInterface, currentCOREFile, Environment.NewLine, e.ToMessageAndCompleteStacktrace()).Replace("'", "");
        Logger.LogError(strErrorDetail, "ACH Batch Update ERROR");
        strError = e.Message.Replace("'", "");
        pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError(strFileName, strSysInterface, strError);
        bTerminateBatchUpdate = true;  //Bug 13226
        string strEmail_to = (string)pConfigSystemInterface.get("Notification_Email_To", ""); // Bug 16549 DJD: Error If NO ACH BU SI Email Configured [Changed ToString() to cast] 
        string strSubject = "ACH File Batch Update Error.";
        string strBody = String.Format("Error in ACH File Batch Update, {0}", strError).Replace("'", "");
        Send_Notification_Email(strEmail_to, strSubject, strBody);
        return false;
      }
      return true;
    }

    /// <summary>
    /// Bug 23008 [23026] - Cleveland Clinic Production Issue: ACH Failure Caused By Bank Rec Code Update
    /// This method loops through deposits and finds the one for eCheck (ACH).
    /// </summary>
    /// <param name="pFileDeposits"></param>
    /// <param name="pConfigSystemInterface"></param>
    /// <returns>eCheck Deposit or null if not found (or an error)</returns>
    private GenericObject GeteCheckDepositForACH(GenericObject pFileDeposits, GenericObject pConfigSystemInterface)
    {
      GenericObject depositFields = null;
      try
      {
        GenericObject depositFields_TndrType = null;
        // Make certain that the deposit is for the eCheck tender
        foreach (object objDeposit in pFileDeposits.vectors)
        {
          GenericObject geoDeposit = objDeposit as GenericObject;
          if (geoDeposit.has("DEPOSIT_TENDERS"))
          {
            GenericObject depositTenders = geoDeposit["DEPOSIT_TENDERS"] as GenericObject;
            if (depositTenders != null & depositTenders.vectors.Count > 0)
            {
              foreach (object objDepTndr in depositTenders.vectors)
              {
                // There are 2 ways to deposit tender:
                //   1) As a "Tender Deposit" [This uses a SPECIFIC tender like eCheck] or
                //   2) As a "Tender Type"    [This groups all tenders of the same type or category (like "Check" or "Cash")]
                //   An eCheck tender SHOULD be deposited using a "Tender Deposit" but if it's the
                //   only tender of type "Check" then it may be deposited as a "Tender Type".

                // Break out of loop if the eCheck deposit was found
                if (depositFields != null)
                {
                  break;
                }

                GenericObject depositTender = objDepTndr as GenericObject;
                if (depositTender != null)
                {
                  string TndrProp = (string)depositTender.get("TNDRPROP", "");
                  decimal decimalAmt = System.Convert.ToDecimal(depositTender["AMOUNT"]);
                  switch (TndrProp)
                  {
                    case "TndrID":
                      {
                        string tndrID = (string)depositTender["TNDR"];
                        GenericObject geoTenderClass = (GenericObject)((GenericObject)c_CASL.c_object("Tender.of")).get(tndrID, null);
                        if (decimalAmt != 0.00M && IsValidACHTender(geoTenderClass, pConfigSystemInterface))
                        {
                          depositFields = geoDeposit;
                        }
                      }
                      break;
                    case "TypeInd":
                      {
                        // NOTE: eCheck Deposit amount for the CORE file should match the Batch Debit Total amount.
                        if ((string)depositTender["TNDRDESC"] == "Check" && decimalAmt == m_ACHFileBatchInfo.m_dBatchDebitTotal)
                        {
                          depositFields_TndrType = geoDeposit; // This deposit is MOST LIKELY for the eCheck tender
                        }
                      }
                      break;
                    default:
                      break;
                  }
                }
              }
            }
          }
        }
        if (depositFields == null && depositFields_TndrType != null)
        {
          depositFields = depositFields_TndrType;
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in ACHFile GeteCheckDepositForACH", e);

        // Log the error but don't fail the ACH System Interface for an error here.
        string currentCOREFile = pFileDeposits.get("FULL_FILE_NAME").ToString();
        string strErrorDetail = String.Format("GeteCheckDepositForACH: {0} {1}{2}{3}", "ACH Batch Update", currentCOREFile, Environment.NewLine, e.ToMessageAndCompleteStacktrace()).Replace("'", "");
        Logger.LogError(strErrorDetail, "ACH Batch Update ERROR");
      }
      return depositFields;
    }

    public override bool Project_WriteTrailer(object pProductLevelMainClass, string strSysInterface, ref bool bEarlyTerminationRequestedByProject)
    {
      string strError = "";
      string strEmail_to = (string)m_ConfigSystemInterface.get("Notification_Email_To", ""); // Bug 16549 DJD: Error If NO ACH BU SI Email Configured [Changed ToString() to cast] 
      string strSubject = "";
      string strBody = "";

      CASL_engine.BatchUpdate_ProductLevel pBatchUpdate_ProductLevel = (BatchUpdate_ProductLevel)pProductLevelMainClass;
      try
      {
        // Check if the temporary file is available
        if (bTerminateBatchUpdate)  //BUG 13226
        {
          return false;
        }
        else if ((m_ACHFileBatchInfo.m_intBatchNumber > 1) && ((m_ACHFileBatchInfo.m_dFileDebitTotal > 0) || (m_ACHFileBatchInfo.m_dFileCreditTotal > 0))) // IPAY-466 DJD: Add Credit Total check
        {
          // Bug#13126 QG If no core event is processed, m_FileHeader will be null, causing error in GetACHFileName().
          // Put following code here will prevent this.
          string strBatchFTPAddress        = m_ConfigSystemInterface.get("ftp_server", "").ToString();
          string strBatchFTPPort           = m_ConfigSystemInterface.get("ftp_port", "").ToString();
          string strBatchFTPUserName       = m_ConfigSystemInterface.get("ftp_username", "").ToString();
          string strBatchFTPPassword       = m_ConfigSystemInterface.get("ftp_password", "").ToString();
          string strBatchSFTPKeyFile       = m_ConfigSystemInterface.get("ftp_private_key_file", "").ToString(); // Bug 22380 [16445] DJD: ACH File Transmission Option Changing To S-FTP with ID and Key
          string strBatchSFTPKeyPassphrase = m_ConfigSystemInterface.get("ftp_private_key_passphrase", "").ToString(); // Bug 22380 [16445] DJD: ACH File Transmission Option Changing To S-FTP with ID and Key
          string strBatchFTPPath           = m_ConfigSystemInterface.get("ftp_path", "").ToString();
          string strServerFileName         = Path.Combine(strBatchFTPPath, GetACHFileName());
          m_transferMethod                 = ((string)m_ConfigSystemInterface.get("ftp_method", "sftp")).ToLower(); // Bug 22001 DJD - Add Folder Option to File Transfer Method

          // Bug 25577
          int connectionTimeout = Convert.ToInt32(m_ConfigSystemInterface.get("ftp_connection_timeout", "60000"));  // Bug 23202
          bool logVerbose                  = (bool)m_ConfigSystemInterface.get("rebex_verbose_log", false);   // Bug 24836 
          string rebexLogPath              = m_ConfigSystemInterface.get("rebex_log_path", "").ToString();   // Bug 24836 
          // End Bug 25577

          // Decrypt FTP Password
          if (!string.IsNullOrEmpty(strBatchFTPPassword))
          {
            string secret_key = Crypto.get_secret_key();
            byte[] content_bytes = Convert.FromBase64String(strBatchFTPPassword);
            byte[] decrypted_bytes = Crypto.symmetric_decrypt("data", content_bytes, "secret_key", secret_key);
            strBatchFTPPassword = System.Text.Encoding.Default.GetString(decrypted_bytes);
          }

          // BEGIN Bug 22380 [16445] DJD: ACH File Transmission Option Changing To S-FTP with ID and Key
          // Decrypt S-FTP Key Passphrase
          if (!string.IsNullOrEmpty(strBatchSFTPKeyPassphrase))
          {
            string secret_key = Crypto.get_secret_key();
            byte[] content_bytes = Convert.FromBase64String(strBatchSFTPKeyPassphrase);
            byte[] decrypted_bytes = Crypto.symmetric_decrypt("data", content_bytes, "secret_key", secret_key);
            strBatchSFTPKeyPassphrase = System.Text.Encoding.Default.GetString(decrypted_bytes);
          }
          // END Bug 22380 [16445] DJD: ACH File Transmission Option Changing To S-FTP with ID and Key

          // Add File Control Record
          if (!m_ACHFileBatchInfo.AddFileControlRecord(ref strError))
          {
            throw (new Exception(strError));
          }

          // Write the Last line FileControlRecord to local file.
          if (!CreateAndWriteACHFile(m_strTempFileName, m_ConfigSystemInterface.get("Letter_Case", "").ToString(), true, ref strError))
          {
            throw (new Exception(strError));
          }

          // Bug 22380 DJD: Removed Deprecated PGP Encryption

          if (m_transferMethod == "folder") // Bug 22001 DJD - Add Folder Option to File Transfer Method
          {
            // Successfully Create ACH File, Log and send email
            Logger.LogInfo("ACH FILE successfully created", "FINISH ACH File Creation Process");
            strSubject = "FINISH ACH File Creation Process: ACH FILE successfully created.";
            strBody = String.Format("ACH FILE successfully created. <br/>Total Amount: {0}", m_ACHFileBatchInfo.m_dFileDebitTotal);
            Send_Notification_Email(strEmail_to, strSubject, strBody);
          }

          if (strBatchFTPAddress.Trim() != "")//BUG#14147 do not send file to ftp if ftp address is empty 
          {
            // Transfer File
            // Start ACH file transfer process, log and send email
            Logger.LogInfo("", "START ACH File Transfer Process");

            // Bug 13690: FT/UMN: Switched to new FTP/SFTP class
            FTP ftp = null;
            SFTP sftp = null;
            const bool pwdIsEncrypted = false;  // Force the FTP classes to decrypt
            try
            {
              bool transferSuccessful = (m_transferMethod == "folder"); // Bug 22001 DJD - Add Folder Option to File Transfer Method

              if (m_transferMethod == "ftp")
              {
                ftp = new FTP(strBatchFTPAddress, strBatchFTPUserName, strBatchFTPPassword, strBatchFTPPort, true, pwdIsEncrypted, false, false, false);

                ftp.Connect(connectionTimeout, logVerbose, rebexLogPath);      // Bug 23202
                // Bug 14831 Begin: ACH FTP: File Successfully Transfers but BU Wrongly Reports Error [removed ftp.FileExists confirmation]
                transferSuccessful = true;
                try
                {
                  ftp.SendFile(m_strTempFileName, strServerFileName);
                }
                catch(Exception e)
                {
                  // Bug 17849
                  Logger.cs_log_trace("Error in ACHFile Project_WriteTrailer", e);
                  transferSuccessful = false;
                }
                finally
                {
                  // Bug 22001 DJD - Open connection was affecting sending email notification
                  if (ftp != null)
                  {
                    ftp.Disconnect();
                    ftp = null;
                  }
                }
                // Bug 14831 End: ACH FTP: File Successfully Transfers but BU Wrongly Reports Error [removed ftp.FileExists confirmation]
              }
              else if (m_transferMethod == "sftp")
              {
                // Bug 22380 [16445] DJD: ACH File Transmission Option Changing To S-FTP with ID and Key
                sftp = new SFTP(strBatchFTPAddress, strBatchFTPUserName, strBatchFTPPassword, strBatchFTPPort, true, pwdIsEncrypted, strBatchSFTPKeyFile, "", strBatchSFTPKeyPassphrase);

                sftp.Connect(connectionTimeout, logVerbose, rebexLogPath);      // Bug 23202
                // Bug 14831 Begin: ACH FTP: File Successfully Transfers but BU Wrongly Reports Error [removed ftp.FileExists confirmation]
                transferSuccessful = true;
                try
                {
                  sftp.SendFile(m_strTempFileName, strServerFileName);
                }
                catch(Exception e)
                {
                  // Bug 17849
                  Logger.cs_log_trace("Error in ACHFile Project_WriteTrailer", e);
                  transferSuccessful = false;
                }
                finally
                {
                  // Bug 22001 DJD - Open connection was affecting sending email notification
                  if (sftp != null)
                  {
                    sftp.Disconnect();
                    sftp = null;
                  }
                }
                // Bug 14831 End: ACH FTP: File Successfully Transfers but BU Wrongly Reports Error [removed ftp.FileExists confirmation]
              }

              if (transferSuccessful)
              {
                if (m_transferMethod != "folder") // Bug 22001 DJD - Add Folder Option to File Transfer Method
                {
                  // Succeed to upload file to FTP or SFTP, log and send email
                  Logger.LogInfo("ACH File has been transfered successfully.", "ACH File Transfer");
                  strSubject = "FINISH ACH File Processing: FILE successfully created and transferred";
                  strBody = String.Format("ACH FILE successfully transfered. <br/>Total Amount: {0}", m_ACHFileBatchInfo.m_dFileDebitTotal);
                  Send_Notification_Email(strEmail_to, strSubject, strBody);
                }
              }
              else
              {
                strError = string.Format("Cannot Write to ACH File '{0}' to '{1}' on the FTP server", m_strTempFileName, strServerFileName);
                Logger.LogError(strError, "ACH File Transfer");
                strSubject = "ACH File Transfer Failed.";
                strBody = "Failed to tranfer ACH File: " + strError;
                Send_Notification_Email(strEmail_to, strSubject, strBody);
                pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError("SYS_INTERFACE_SCOPE_ONLY", strSysInterface, strBody);
                return false;
              }
            }
            finally
            {
              // Destructor will call Disconnect, but with .NET it's async scheduled. So calling
              // disconnect here will free the connection faster
              if (ftp != null) ftp.Disconnect();
              if (sftp != null) sftp.Disconnect();
            }
            // end Bug 13690
          }
        }
        else
        {
          // No valid Tenders(checks), log and send failure email
          Logger.LogError("Failed to create ACH File. No valid tenders(checks)", "ACH File Creation Process");
          strSubject = "ACH File Creation Process Failed.";
          strBody = "Failed to create ACH File, no valid tenders(checks). <br/> Total Amount: 0.00";
          Send_Notification_Email(strEmail_to, strSubject, strBody);
          pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError("SYS_INTERFACE_SCOPE_ONLY", strSysInterface, strBody);
          return true;    // Bug 13125 QG Mark corefile as updated if no valid tenders
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error in ACHFile Project_WriteTrailer", e);

        //Logger.LogError(strError, "START ACH Project_WriteTrailer");
        // Log and send failure email
        Logger.LogError(e.Message, "ACH File Batch Update");
        strSubject = "ACH File Batch Update Error.";
        strBody = String.Format("Error in ACH File Batch Update, {0}", e.Message).Replace("'", "");
        Send_Notification_Email(strEmail_to, strSubject, strBody);
        pBatchUpdate_ProductLevel.ProductLevel_AddSystemInterfaceError("SYS_INTERFACE_SCOPE_ONLY", strSysInterface, strBody);
        return false;
      }

      return true;
    }

    public override void Project_Rollback(object pProductLevelMainClass, bool bEarlyTerminationRequestedByProject)
    {
      // Add code here if you want to do something on failure (delete the file?)
    }
    #endregion
  }
}
