using System;
using System.Collections.Generic;
using System.Text;

namespace SDP.ParserUtils
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public sealed class StringLayoutAttribute : Attribute
    {
        #region Private Members
        int _startPosition;
        int _endPosition;
        char _padChar = ' ';          // Default to space
        bool _rightJustified = false; // Default to left justified alignment
        #endregion

        #region Constructor
        public StringLayoutAttribute(int startPosition, int endPosition)
        {
            if (startPosition >= 0 && endPosition >= startPosition)
            {
                _startPosition = startPosition;
                _endPosition = endPosition;
            }
            else
            {
                throw new ArgumentException("StartPosition must be > 0 and EndPosition must be greater than or equal to StartPosition");
            }
        }

        public StringLayoutAttribute(int startPosition, int endPosition, char padChar, bool rightJustified)
        {
            _padChar = padChar;
            _rightJustified = rightJustified;

            if (startPosition >= 0 && endPosition >= startPosition)
            {
                _startPosition = startPosition;
                _endPosition = endPosition;
            }
            else
            {
                throw new ArgumentException("StartPosition must be > 0 and EndPosition must be greater than or equal to StartPosition");
            }
        }
        #endregion

        #region Public Properties

        public int StartPosition
        {
            get { return _startPosition; }
            set { _startPosition = value; }
        }

        public int EndPosition
        {
            get { return _endPosition; }
            set { _endPosition = value; }
        }

        public char PadChar
        {
            get { return _padChar; }
            set { _padChar = value; }
        }

        public bool RightJustified
        {
            get { return _rightJustified; }
            set { _rightJustified = value; }
        }
        #endregion
    }
}
