using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace SDP.ParserUtils
{   
    public abstract class StringLayoutUtility
    {
        public enum TrimInputMode { NoTrim, Trim, TrimStart, TrimEnd };

        #region Members
        private TrimInputMode _trimInput = TrimInputMode.NoTrim;
        private char _paddingChar = ' ';
        #endregion

        #region Properties

        public TrimInputMode TrimInput
        {
            get {return _trimInput; }
            set { _trimInput = value; }
        }

        public char PaddingChar
        {
            get { return _paddingChar; }
            set { _paddingChar = value; }
        }
        #endregion

        #region Public Methods
        public void Parse(string input)
        {
            if (!String.IsNullOrEmpty(input))
            {
                foreach (PropertyInfo property in GetType().GetProperties())
                {
                    foreach (Attribute attribute in property.GetCustomAttributes(true))
                    {
                        StringLayoutAttribute stringLayoutAttribute = attribute as StringLayoutAttribute;
                        if (null != stringLayoutAttribute)
                        {
                            string tmp = string.Empty;
                            if (stringLayoutAttribute.StartPosition <= input.Length - 1)
                            {
                                tmp = input.Substring(stringLayoutAttribute.StartPosition, Math.Min((stringLayoutAttribute.EndPosition - stringLayoutAttribute.StartPosition + 1), input.Length - stringLayoutAttribute.StartPosition));
                            }
                            switch (_trimInput)
                            { 
                                case TrimInputMode.Trim :
                                    tmp = tmp.Trim();
                                    break;
                                case TrimInputMode.TrimStart:
                                    tmp = tmp.TrimStart();
                                    break;
                                case TrimInputMode.TrimEnd:
                                    tmp = tmp.TrimEnd();
                                    break;
                            }
                            property.SetValue(this, tmp, null);
                            break;
                        }
                    }
                }
            }
        }

        public override string ToString()
        {
            string result = string.Empty;

            foreach (PropertyInfo property in GetType().GetProperties())
            {
                foreach (Attribute attribute in property.GetCustomAttributes(false))
                {
                    StringLayoutAttribute stringLayoutAttribute = attribute as StringLayoutAttribute;
                    if (null != stringLayoutAttribute)
                    {
                        string propertyValue = (string)property.GetValue(this, null);
                        if (stringLayoutAttribute.StartPosition > 0 && result.Length < stringLayoutAttribute.StartPosition)
                        {
                            result = result.PadRight(stringLayoutAttribute.StartPosition, _paddingChar);
                        }

                        string left = string.Empty;
                        string right = string.Empty;

                        if (stringLayoutAttribute.StartPosition > 0)
                        {
                            left = result.Substring(0, stringLayoutAttribute.StartPosition);
                        }

                        if (result.Length > stringLayoutAttribute.EndPosition + 1)
                        {
                            right = result.Substring(stringLayoutAttribute.EndPosition + 1);
                        }

                        if (propertyValue.Length < stringLayoutAttribute.EndPosition - stringLayoutAttribute.StartPosition + 1)
                        {
                            //propertyValue = propertyValue.PadRight(stringLayoutAttribute.EndPosition - stringLayoutAttribute.StartPosition + 1, _paddingChar);
                            if (stringLayoutAttribute.RightJustified)
                            {
                                propertyValue = propertyValue.PadLeft(stringLayoutAttribute.EndPosition - stringLayoutAttribute.StartPosition + 1, stringLayoutAttribute.PadChar);
                            }
                            else
                            {
                                propertyValue = propertyValue.PadRight(stringLayoutAttribute.EndPosition - stringLayoutAttribute.StartPosition + 1, stringLayoutAttribute.PadChar);
                            }
                        }
                        result = left + propertyValue + right;
                    }
                    break;
                }
            }
            return result;
        }

        public abstract bool IsValid(ref String strError); //Bug#14469
 
        #endregion
    }
}