﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
#if DEBUG
[assembly: AssemblyTitle("ACHBatchUpdate (Debug)")]
#else
[assembly: AssemblyTitle("ACHBatchUpdate (Release)")]
#endif

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("3be679e8-a94e-4cec-bb41-2d1a1e2b7c9a")]

// Assembly version info now in single place: pos_demo/VersionInfo.cs


