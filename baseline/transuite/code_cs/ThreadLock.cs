﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading;

using CASL_engine;

namespace TranSuiteServices
{
  /// <summary>
  /// ThreadLock singleton.
  /// Class added to fix the deadlocking issues. BUG# 5813 DH 07/26/2008
  /// Re-ordered to prevent double-check locking issue. Bug 11661: FT
  /// Debug writes taken out for Scott & White and retrofitted here.
  /// </summary>
  public sealed class ThreadLock
  {
    // Bug 11661: instanciate once and only once here
    private static readonly ThreadLock instance = new ThreadLock();

    private Mutex m_pMutex = null;

    private string previousStack = "";
    private string currentStack = "";

    /// <summary>
    /// Static Initializer.
    /// This is guaranteed to be called before any methods are called.
    /// Bug 11661 FT: Redesigned to avoid double-check locking
    /// Explicit static constructor to tell C# compiler
    /// not to mark type as beforefieldinit
    /// </summary>
    static ThreadLock()
    {
      // Create the mutex only once here.
      instance.m_pMutex = new Mutex(false);  // BUG# 9932 DH - Needs local visibility.
    }

    /// <summary>
    /// Default constructor is all that is needed
    /// </summary>
    private ThreadLock() { }

    /// <summary>
    /// Simplified GetInstance.  Bug 11455 FT: Redesigned to avoid double-check locking
    /// </summary>
    /// <returns></returns>
    public static ThreadLock GetInstance()
    {
      return instance;
    }

    /// <summary>
    /// Create a mutex wait.
    /// Bug 11661 FT: Simplified by moving the instantiation of mutex to static constructor. 
    /// Catching the AbandonedMutexException here will prevent it from bubbling
    /// up and locking up the application.  Once it is caught and logged, we can
    /// proceed as before without having to explicitly release the mutex because it
    /// has been transfered to this thread. 
    /// <seealso cref="http://msdn.microsoft.com/en-us/library/system.threading.abandonedmutexexception.aspx"/>
    /// </summary>
    /// <remarks>Bug 11661 redux: removed Debug writes; caused slowdown 
    /// and possible AbandonedMutexException.
    /// Also removed call to getNameFromStack; this may have been 
    /// slowing down stress test.</remarks>
    /// <returns></returns>
    public bool CreateLock()
    {
      bool lockStatus = false;
      try
      {
        //previousStack = currentStack;
        //currentStack = getNameFromStack();
        lockStatus = m_pMutex.WaitOne();
      }
      catch(AbandonedMutexException ex)
      {
        // Log the fact the mutex was abandoned, it's still acquired
        string error_message = "ThreadLock.CreateLock(): Abandoned mutex from: " + ex.Source
          + " from: " + previousStack + " current: " + currentStack;
        Logger.LogError(error_message, "Db CreateLock");
        // Bug 17849
        Logger.cs_log_trace("Error Db CreateLock", ex);
      }

      return lockStatus;
    }


    /// <summary>
    /// Release the mutex wait.
    /// </summary>
    /// <remarks>Bug 11661 redux: removed Debug writes; caused slowdown 
    /// and possible AbandonedMutexException</remarks>
    public void ReleaseLock()
    {
        m_pMutex.ReleaseMutex();
    }

    /// <summary>
    /// If there is an error, let's find out where the offending mutex was created.
    /// </summary>
    /// <remarks>Made Obsolete; may have slowed down the stress test.  
    /// StackTrace could be really slow.</remarks>
    /// <returns></returns>
    [Obsolete("Too slow for production; use only for debugging", true)]
    private string getNameFromStack()
    {
      var stackInfo = new StringBuilder();
      var stackTrace = new StackTrace(true);
      int counter = 0;
      foreach (var r in stackTrace.GetFrames())
      {
        // Skip the getNameFromStack() and CreateLock() methods
        if (counter++ <= 1)
          continue;
        // Enough is enough
        if (counter > 3)
          break;
        stackInfo.
            Append(String.Format("{0}:{1}",
            r.GetMethod(), r.GetFileLineNumber())).
            Append('_');
      }
      return stackInfo.ToString();

    }
  }

}
