﻿/*Bug 13934-Reversal overhaul*/
using System;
using System.Text;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;


using TranSuiteServices;
using CASL_engine;


namespace TranSuiteServices
{
  /// <summary>
  /// This class contains functionality related to reversal.
  /// </summary>
  public class CoreEventReversal
  {
    /// <summary>
    /// This method posts the reversal relationship between the new and old event everytime an event is reversed.
    /// In case of partial reversal, it also does the call for storing the transaction and tender amounts reversed for the
    /// original event.
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static object PostReversalEvent(params Object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);

      ThreadLock.GetInstance().CreateLock();

      // Bug 20331: Move here so it can be closed in the finally block if need be
      ConnectionTypeSynch pDBConn_Data = null;

      try
      {
        GenericObject data = args.get("data", c_CASL.c_undefined) as GenericObject;

        string strErr = "";

        if (args.has("DbConnectionClass"))
          pDBConn_Data = (ConnectionTypeSynch)args.get("DbConnectionClass");
        else
        {
          pDBConn_Data = new ConnectionTypeSynch(data);
          if (!pDBConn_Data.EstablishDatabaseConnection(ref strErr))
            TranSuiteServices.HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1255", "");

          if (!pDBConn_Data.EstablishRollback(ref strErr))
            TranSuiteServices.ThrowCASLErrorAndCloseDB("TS-1256", ref pDBConn_Data, "Error creating database transaction.", strErr);
        }

        string strEVENTNBR = TranSuiteServices.GetKeyData(args, "EVENTNBR", false);
        string strDEPFILENBR = TranSuiteServices.GetKeyData(args, "DEPFILENBR", false);
        string strDEPFILESEQ = TranSuiteServices.GetKeyData(args, "DEPFILESEQ", false);

        string strUSERID = TranSuiteServices.GetKeyData(args, "USERID", false);

        string strREVERSED_EVENTNBR = TranSuiteServices.GetKeyData(args, "REVERSED_EVENTNBR", false);
        string strREVERSED_DEPFILENBR = TranSuiteServices.GetKeyData(args, "REVERSED_DEPFILENBR", false);
        string strREVERSED_DEPFILESEQ = TranSuiteServices.GetKeyData(args, "REVERSED_DEPFILESEQ", false);

        bool bVOIDED = (bool)args.get("VOIDED", false);

        string strChecksum = TranSuiteServices.CreateReversalEventChecksum(Crypto.DefaultHashAlgorithm,
          "strEventNbr", strEVENTNBR, "strFileName", strDEPFILENBR + strDEPFILESEQ,
          "strUserID", strUSERID, "strReversedEventNbr", strREVERSED_EVENTNBR, "strReversedFileName",
          strREVERSED_DEPFILENBR + strREVERSED_DEPFILESEQ, "strVoided", (bVOIDED) ? "TRUE" : "FALSE");


        // Bug 20331: FT: Parameterize this INSERT
        string insertString =
            @"INSERT INTO TG_REVERSAL_EVENT_DATA 
                   (EVENTNBR,  DEPFILENBR,  DEPFILESEQ,  USERID,  REVERSED_EVENTNBR,  REVERSED_FILENBR,  REVERSED_FILESEQ, VOIDED,    CHECKSUM) 
            VALUES (@eventnbr, @depfilenbr, @depfileseq, @userid, @reversed_eventnbr, @reversed_filenbr, @reversed_fileseq, @voidedc, @checksum)";

        HybridDictionary sql_args = new HybridDictionary();
        sql_args["eventnbr"] = strEVENTNBR;
        sql_args["depfilenbr"] = strDEPFILENBR;
        sql_args["depfileseq"] = strDEPFILESEQ;
        sql_args["userid"] = strUSERID;
        sql_args["reversed_eventnbr"] = strREVERSED_EVENTNBR;
        sql_args["reversed_filenbr"] = strREVERSED_DEPFILENBR;
        sql_args["reversed_fileseq"] = strREVERSED_DEPFILESEQ;

        sql_args["voidedc"] = bVOIDED;
        sql_args["checksum"] = strChecksum;


        using (SqlCommand paramCommand = ParamUtils.CreateParamCommand(pDBConn_Data, insertString, sql_args))
        {
          paramCommand.ExecuteNonQuery();         // Bug 20331: Should use ExecuteNonQuery instead of ExecuteScalar.  What was I thinking?
        }
        // End Bug 20331

        /******************************************************************************
            Store the transaction and tender amount reversed from the original event
        *******************************************************************************/

        GenericObject oldEvent = args.get("old_event") as GenericObject;
        bool isPartialReversal = (bool)args.get("is_partial_reversal", false);
        if (isPartialReversal)
        {
          object r = PostReversalTransactionsAndTenders(oldEvent, data, pDBConn_Data, ref strErr, strREVERSED_DEPFILENBR, strREVERSED_DEPFILESEQ,
                                                 strREVERSED_EVENTNBR);
          if (r == null || !String.IsNullOrEmpty(strErr))
          {
            TranSuiteServices.HandleErrorsAndLocking("message", "Unable to Post Reversal Tran data. ", "code", "TS-1258", strErr);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix //Bug 11329 NJ-show verbose fix
          }
        }

        // Bug 20331: FT: Commit change if there is no error
        if (!args.has("DbConnectionClass"))
        {
          strErr = "";
          if (!pDBConn_Data.CommitToDatabase(ref strErr))
            TranSuiteServices.HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-1259", strErr);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix4//Bug 11329 NJ-show verbose fix
        }

        postReversalToActivityLog(strUSERID, oldEvent, isPartialReversal, strEVENTNBR, strDEPFILENBR, strDEPFILESEQ, strREVERSED_DEPFILENBR,
          strREVERSED_DEPFILESEQ, strREVERSED_EVENTNBR, bVOIDED);
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error PostReversalEvent", e);

        // Bug 20331: FT: Cleaning up Commit/Rollback. 
        // Rollback here if there are any exception here or in PostReversalTransactionsAndTenders 
        if (!args.has("DbConnectionClass"))
        {
          pDBConn_Data.RollBack();
        }

        // Do you really want to throw another exception here?
        TranSuiteServices.HandleErrorsAndLocking("message", "Unable to post ExecuteNonQuery Reversal Tran data.", "code", "TS-1259", e.ToString());
      }
      finally
      {
        // Bug 20331: FT: Close the database connection if openned locally
        if (!args.has("DbConnectionClass"))
        {
          if (pDBConn_Data != null)
          {
            pDBConn_Data.CloseDBConnection();
          }
        }


        ThreadLock.GetInstance().ReleaseLock();
      }

      return true;
    }
    /// <summary>
    /// Log as much reversal information as possible to the Activity Log.
    /// Bug 17217
    /// </summary>
    /// <param name="strUSERID"></param>
    /// <param name="oldEvent"></param>
    /// <param name="isPartialReversal"></param>
    /// <param name="strEVENTNBR"></param>
    /// <param name="strDEPFILENBR"></param>
    /// <param name="strDEPFILESEQ"></param>
    /// <param name="strREVERSED_DEPFILENBR"></param>
    /// <param name="strREVERSED_DEPFILESEQ"></param>
    /// <param name="strREVERSED_EVENTNBR"></param>
    /// <param name="bVOIDED"></param>
    private static void postReversalToActivityLog(
      string strUSERID,
      GenericObject oldEvent,
      bool isPartialReversal,
      string strEVENTNBR,
      string strDEPFILENBR,
      string strDEPFILESEQ,
      string strREVERSED_DEPFILENBR,
      string strREVERSED_DEPFILESEQ,
      string strREVERSED_EVENTNBR,
      bool bVOIDED)
    {
      string sAction = isPartialReversal ? "Partial Reversal" : "Reversal";
      // Bug 20435 UMN PCI-DSS don't pass session

      Decimal total = Convert.ToDecimal(oldEvent.get("total", 0.0M));

      StringBuilder summaryBuild = new StringBuilder();
      summaryBuild.Append("Reversed ")
        .Append(oldEvent.get("receipt_nbr", ""))
        .Append(" for ").Append(total.ToString("C"));
      string sSummary = summaryBuild.ToString();

      StringBuilder detailBuilder = new StringBuilder();
      detailBuilder.Append(",Old Receipt:").Append(oldEvent.get("receipt_nbr", ""));
      detailBuilder.Append(",New Receipt:").Append(strDEPFILENBR).Append(strDEPFILESEQ.PadLeft(3)).Append('-').Append(strEVENTNBR);
      detailBuilder.Append(",Voided:").Append(bVOIDED ? "True" : "False");
      detailBuilder.Append(",Partial Reversal:").Append(isPartialReversal ? "True" : "False");

      detailBuilder.Append(",Total:").Append(total.ToString("C"));

      Decimal amount = Convert.ToDecimal(oldEvent.get("balance", 0.0M));
      detailBuilder.Append(",Balance:").Append(amount.ToString("C"));

      amount = Convert.ToDecimal(oldEvent.get("change_amount", 0.0M));
      detailBuilder.Append(",Change:").Append(amount.ToString("C"));

      amount = Convert.ToDecimal(oldEvent.get("tran_reversal_total", 0.0M));
      detailBuilder.Append(",Tran Reversal Total:").Append(amount.ToString("C"));

      amount = Convert.ToDecimal(oldEvent.get("tender_reversal_total", 0.0M));
      detailBuilder.Append(",Tender Reversal Total:").Append(amount.ToString("C"));

      amount = Convert.ToDecimal(oldEvent.get("tran_reversible_balance_total", 0.0M));
      detailBuilder.Append(",Trans Reversible Balance Total:").Append(amount.ToString("C"));

      amount = Convert.ToDecimal(oldEvent.get("tender_reversible_balance_total", 0.0M));
      detailBuilder.Append(",Tender Reversible Balance Total:").Append(amount.ToString("C"));

      postReversalTranactionsToActivityLog(oldEvent, detailBuilder);

      string sDetails = detailBuilder.ToString();

      try
      {
        misc.CASL_call("a_method", "actlog", "args",
          new GenericObject(
              "app", ""          // Bug 17217: Let ActivityLogCasl.cs pull the application name from the my variable
              // Bug 20435 UMN PCI-DSS don't pass session
              , "user", strUSERID
              , "action", sAction
              , "summary", sSummary
              , "detail", sDetails
              )
              );
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error postReversalToActivityLog", e);
        string msg = "Error writing to the activity log: " + e.ToString();
        Logger.LogError(msg, "postReversalToActivityLog");
      }
    }

    /// <summary>
    /// Loop through each transaction and record the amount of the reversal for each.
    /// This is important for partial reversals of reversing more than one transactions
    /// in an event.
    /// Bug 17217.
    /// </summary>
    /// <param name="oldEvent"></param>
    /// <param name="detailBuilder"></param>
    private static void postReversalTranactionsToActivityLog(GenericObject oldEvent, StringBuilder detailBuilder)
    {
      if (!oldEvent.has("transactions"))
        return;
      ArrayList vectors = oldEvent.vectors;
      if (vectors.Count == 0)
        return;

      foreach (GenericObject tran in vectors)
      {
        if (tran.has("reversal_amount"))
        {
          Decimal reversalAmount = Convert.ToDecimal(tran.get("reversal_amount"));
          if (reversalAmount == Decimal.Zero)
            continue;
          int seqNum = Convert.ToInt32(tran.get("seq_nbr", 0));
          string desc = tran.get("description", "tran") as string;
          detailBuilder.Append(",Reversed Tran").Append('-').Append(seqNum).Append(' ').Append(desc)
            .Append(':')
            .Append(reversalAmount.ToString("C"));
        }
      }
    }

    /// <summary>
    /// This method posts the amounts of transaction and tender reversed from the original event
    /// Bug 20331: FT: Parameterized and connected this SQL transaction to PostReversalEvent's
    /// transaction so it can all be rolled back at once.
    /// </summary>
    /// <param name="oldEvent"></param>
    /// <param name="data"></param>
    /// <param name="dbConnection"></param>
    /// <param name="error"></param>
    /// <param name="reversedFileNbr"></param>
    /// <param name="reversedFileSeq"></param>
    /// <param name="reversedEventNbr"></param>
    /// <returns></returns>
    private static object PostReversalTransactionsAndTenders(GenericObject oldEvent, GenericObject data, ConnectionTypeSynch dbConnection,
                            ref string error, string reversedFileNbr, string reversedFileSeq, string reversedEventNbr)
    {
      try
      {
        GenericObject transactions = oldEvent.get("transactions") as GenericObject;
        GenericObject tenders = oldEvent.get("tenders") as GenericObject;

        // Bug 20331: FT: Removed the connection open here; the connection HAS to be open so all the 
        // INSERTs together with the calls in the calling routine (PostReversalEvent) 
        // can be rolled back all together if there is an error.
        // The piecemeal commit and rollback was wrong!
        if (dbConnection == null)
          throw new Exception("Reversal Internal error: PostReversalTransactionsAndTenders() must be called with an open connection");

        // Bug 20331: FT: Converted to parameterized INSERTs
        string sqlParameterized = @"
          INSERT INTO TG_REVERSAL_TRAN_DATA 
             (EVENTNBR, DEPFILENBR, DEPFILESEQ, COREITEMNBR, REVERSED_AMOUNT, ISTENDER,CHECKSUM,REVERSAL_ID) 
          VALUES
	          (@eventNbr, @depfilenbr, @depfileseq, @coreitemnbr, @reversedAmount, @istender, @checksum, IDENT_CURRENT('TG_REVERSAL_EVENT_DATA'))";


        int tranCount = transactions.getIntKeyLength();
        for (int i = 0; i < tranCount; i++)
        {
          GenericObject tran = transactions.get(i) as GenericObject;
          decimal amt = 0.0M, totalReversibleAmount = 0.0M;
          /*****************************************
           Post nested transactions
           *****************************************/

          if (tran.has("core_items"))
          {
            GenericObject nestedTransactions = tran.get("core_items") as GenericObject;
            int nestedTranCount = nestedTransactions.getIntKeyLength();
            for (int j = 0; j < nestedTranCount; j++)
            {
              GenericObject nestedTran = nestedTransactions.get(j) as GenericObject;
              string nestedTranNbr = TranSuiteServices.GetKeyData(nestedTran, "seq_nbr", false);
              string nestedReversedAmount = TranSuiteServices.GetKeyData(nestedTran, "reversed_amount", false);
              amt = 0.0M;
              decimal.TryParse(nestedReversedAmount, out amt);
              if (!String.IsNullOrEmpty(nestedReversedAmount) && amt > 0)
              {
                string checksum = TranSuiteServices.CreateReversalTranDataChecksum(Crypto.DefaultHashAlgorithm,
                      "strCoreItemNbr", nestedTranNbr,
                      "strEventNbr", reversedEventNbr,
                      "strDepFileNbr", reversedFileNbr,
                      "strDepFileSeq", reversedFileSeq,
                      "strAmount", nestedReversedAmount);

                // Bug 20331: FT: Converted to parameterized
                HybridDictionary sql_args = new HybridDictionary();
                sql_args["eventNbr"] = reversedEventNbr;
                sql_args["depfilenbr"] = reversedFileNbr;
                sql_args["depfileseq"] = reversedFileSeq;
                sql_args["coreitemnbr"] = nestedTranNbr;
                sql_args["reversedAmount"] = nestedReversedAmount;
                sql_args["istender"] = 0;
                sql_args["checksum"] = checksum;
                using (SqlCommand paramCommand = ParamUtils.CreateParamCommand(dbConnection, sqlParameterized, sql_args))
                {
                  paramCommand.ExecuteNonQuery();
                }

                totalReversibleAmount += amt;
              }
            }
          }

          /*****************************************
           post transactions that don't have nested transactions
           *****************************************/
          else
          {
            string tranNbr = TranSuiteServices.GetKeyData(tran, "seq_nbr", false);
            string reversedAmount = TranSuiteServices.GetKeyData(tran, "reversed_amount", false);
            amt = 0.0M;
            decimal.TryParse(reversedAmount, out amt);

            if (amt == 0 && totalReversibleAmount > 0)
            {
              amt = totalReversibleAmount;
            }

            if (amt > 0)
            {
              string checksum = TranSuiteServices.CreateReversalTranDataChecksum(Crypto.DefaultHashAlgorithm,
                  "strCoreItemNbr", tranNbr,
              "strEventNbr", reversedEventNbr,
              "strDepFileNbr", reversedFileNbr,
              "strDepFileSeq", reversedFileSeq,
              "strAmount", reversedAmount);

              HybridDictionary sql_args = new HybridDictionary();
              sql_args["eventNbr"] = reversedEventNbr;
              sql_args["depfilenbr"] = reversedFileNbr;
              sql_args["depfileseq"] = reversedFileSeq;
              sql_args["coreitemnbr"] = tranNbr;
              sql_args["reversedAmount"] = reversedAmount;
              sql_args["istender"] = 0;
              sql_args["checksum"] = checksum;
              using (SqlCommand paramCommand = ParamUtils.CreateParamCommand(dbConnection, sqlParameterized, sql_args))
              {
                paramCommand.ExecuteNonQuery();
              }

            }
          }

        }


        /*****************************************
             post the tenders
             *****************************************/
        int tenderCount = tenders.getIntKeyLength();
        for (int i = 0; i < tenderCount; i++)
        {
          GenericObject tender = tenders.get(i) as GenericObject;
          string tenderNbr = TranSuiteServices.GetKeyData(tender, "seq_nbr", false);
          string reversedAmount = TranSuiteServices.GetKeyData(tender, "reversed_amount", false);
          decimal amt = 0.0M;
          decimal.TryParse(reversedAmount, out amt);
          if (!String.IsNullOrEmpty(reversedAmount) && amt > 0) /*BUG# 10600 / 10614 - DH 2/19/2011 Combine statements.*/
          {
            string checksum = TranSuiteServices.CreateReversalTranDataChecksum(Crypto.DefaultHashAlgorithm,
                "strCoreItemNbr", tenderNbr,
            "strEventNbr", reversedEventNbr,
            "strDepFileNbr", reversedFileNbr,
            "strDepFileSeq", reversedFileSeq,
            "strAmount", reversedAmount);

            // Bug 20331: FT: Converted to parameterized
            HybridDictionary sql_args = new HybridDictionary();
            sql_args["eventNbr"] = reversedEventNbr;
            sql_args["depfilenbr"] = reversedFileNbr;
            sql_args["depfileseq"] = reversedFileSeq;
            sql_args["coreitemnbr"] = tenderNbr;
            sql_args["reversedAmount"] = reversedAmount;
            sql_args["istender"] = 1;
            sql_args["checksum"] = checksum;
            using (SqlCommand paramCommand = ParamUtils.CreateParamCommand(dbConnection, sqlParameterized, sql_args))
            {
              paramCommand.ExecuteNonQuery();
            }

          }
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error PostReversalTransactionsAndTenders", e);

        error += e.ToString();
        return null;
      }
      return true;
    }


    /// <summary>
    /// This method gets the sum of the previously reversed amount for all the transactions and tenders 
    /// for a given core event. The reversal events which are voided are not taken into consideration.
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static object GetPreviousReversedAmounts(params object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject data = args.get("data", c_CASL.c_undefined) as GenericObject;

      // Bug 20320 UMN parameterize query
      Hashtable sql_args = new Hashtable();
      sql_args["eventnbr"] = TranSuiteServices.GetKeyData(args, "eventnbr", false);
      sql_args["depfilenbr"] = TranSuiteServices.GetKeyData(args, "depfilenbr", false);
      sql_args["depfileseq"] = TranSuiteServices.GetKeyData(args, "depfileseq", false);

      string error = "";

      StringBuilder query = new StringBuilder();
      query.Append(@"SELECT t.DEPFILENBR,t.DEPFILESEQ,t.EVENTNBR,t.COREITEMNBR,SUM(t.REVERSED_AMOUNT) AS REVERSED_AMOUNT,
                        CASE WHEN t.ISTENDER=0 THEN 'false'
                        ELSE 'true' END AS ISTENDER
            FROM TG_REVERSAL_TRAN_DATA t ");
      query.Append(@" INNER JOIN TG_REVERSAL_EVENT_DATA r ON t.REVERSAL_ID=r.REVERSAL_ID AND r.VOIDED=0 ");
      query.Append("  WHERE ");
      query.Append("t.DEPFILENBR= @depfilenbr AND ");
      query.Append("t.DEPFILESEQ= @depfileseq AND ");
      query.Append("t.EVENTNBR= @eventNbr");
      query.Append(" GROUP BY t.DEPFILENBR,t.DEPFILESEQ,t.EVENTNBR,t.COREITEMNBR,t.ISTENDER");

      IDBVectorReader reader = db_reader.Make(data, query.ToString(), CommandType.Text, 0, sql_args, 0, 0, null, ref error);
      if (!error.Equals(""))
      {
        return misc.MakeCASLError("Reversal Data retrieval error", "TS-1249", "Could not create DB reader: " + error);
      }

      GenericObject itemData = new GenericObject();
      reader.ReadIntoRecordset(ref itemData, 0, 0);
      return itemData;
    }
  }
}
