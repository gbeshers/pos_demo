﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Net;
using CASL_engine;
using System.Web;

//
// Bug ??? UMN Moved Logout code here so it's easier to debug than in the ASP page.
//
namespace TranSuiteServices
{
  public class LogOut
  {
    public static void Logout(HttpRequest Request, HttpResponse Response)
    {
      // Bug 11215 DH put everything to try catch block to prevent unhandled exception 
      try
      {
        Response.Buffer = false;
        Response.Clear();
        Response.ContentType = "text/html";
        // Do not cache this page
        Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
        Response.Expires = -1500;
        Response.CacheControl = "no-cache";

        // Bug #13735 Mike O - If a redirect URL is passed in, go there
        if (Request.QueryString["redirect"] != null)
        {
          bool result = false;
          string URL = "";
          try
          {
            // Bug 17045 UMN PCI DSS decrypt URL
            string data = Request.QueryString["redirect"];
            URL = CASL_engine.Crypto.symmetric_decrypt_url("_subject", data);

            Uri uriResult;

            // Bug 17264 UMN Add support for relative URLs. they must start with a './' and be relative to the site
            if (URL.Contains("//"))
            {
              result = Uri.TryCreate(URL, UriKind.Absolute, out uriResult) &&
                          (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
            }
            else if (URL.StartsWith("./"))
            {
              result = Uri.TryCreate(URL, UriKind.Relative, out uriResult);
            }
          }
          catch
          {
          }

          if (result && !Response.IsRequestBeingRedirected) // UMN always check before redirecting
          {
            Response.Redirect(URL, false);
          }
          else
          {
            Response.Write("<html><head><title>iPayment Logout</title>");
            Response.Write("<style type='text/css'>HTML {font-family: \"Trebuchet MS\", Trebuchet, Verdana, Arial, Helvetica, \"Lucida Grande\", sans-serif;}");
            Response.Write("</style></head><body><center><h1>");
            Response.Write("You have successfully logged out, but have not been redirected.<br>The configured redirect URL is invalid.");
            Response.Write("</h1></center></body></html>");
          }
        }
        else if (Request.QueryString["close"] != null)
        {
          Response.Write("<html><head><title>iPayment Logout</title>");
          Response.Write("<style type='text/css'>HTML {font-family: \"Trebuchet MS\", Trebuchet, Verdana, Arial, Helvetica, \"Lucida Grande\", sans-serif;}");
          Response.Write("</style></head>");
          // Bug 8932 UMN - close window after 2 sec timeout
          Response.Write("<body onload=\"window.setTimeout(function(){window.open('','_self').close()},2000);\"><center><h1>You have successfully logged out.<BR/>You may close this window/tab.</h1></center></body></html>");
        }
        // BEGIN 20562 UMN / EOM Add Multi-factor authentication (MFA)     
        else if (Request.QueryString["MFA_close"] != null)
        {
          Response.Write("<html><head><title>iPayment Logout</title>");
          Response.Write("<style type='text/css'>HTML {font-family: \"Trebuchet MS\", Trebuchet, Verdana, Arial, Helvetica, \"Lucida Grande\", sans-serif;}");
          Response.Write("</style></head>");
          Response.Write("<body onload=\"window.setTimeout(function(){window.open('','_self').close()},6000);\"><center><h1>The email link has expired. Please request a new link from system administrator.<BR/>You may close this window/tab.</h1></center></body></html>");
        }
        // Bug 10457 UMN - return to whatever app the user started with
        else if (Request.QueryString["app_name"] != null)
        {
          if (Request.QueryString["app_name"].Contains("pos"))
          {
            if (!Response.IsRequestBeingRedirected) // UMN always check before redirecting
              Response.Redirect(Request.ApplicationPath + "/ipayment.htm?app=pos", false);
          }
          else if (Request.QueryString["app_name"].Contains("cbc"))
          {
            if (!Response.IsRequestBeingRedirected) // UMN always check before redirecting
              Response.Redirect(Request.ApplicationPath + "/cbc.htm", false);
          }
          else if (Request.QueryString["app_name"].Contains("Config"))
          {
            if (!Response.IsRequestBeingRedirected) // UMN always check before redirecting
              Response.Redirect(Request.ApplicationPath + "/ipayment.htm?app=Config", false);
          }
          else if (Request.QueryString["app_name"].Contains("Portal"))
          {
            if (!Response.IsRequestBeingRedirected) // UMN always check before redirecting
              Response.Redirect(Request.ApplicationPath + "/ipayment.htm?app=Portal", false);
          }
        }
        else
        {
          // Bug 10457 UMN - otherwise return to the application's root page (usually the main menu)
          if (!Response.IsRequestBeingRedirected) // UMN always check before redirecting
            Response.Redirect(Request.ApplicationPath, false);
        }
      }
      catch
      {
      }
      finally
      {
        // Send the response before abandoning the session
        // Abandon the session and delete all session variables
        if (Response.IsClientConnected)
        {
          Response.Flush();
        }
        HttpContext.Current.ApplicationInstance.CompleteRequest();  // BUG 16461 UMN replace Response.End with CompleteRequest()
      }
    }
  }
}
