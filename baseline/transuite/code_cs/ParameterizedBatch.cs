﻿using System;
using System.Data;
using System.Threading;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using CASL_engine;

namespace TranSuiteServices
{
  /// <summary>
  /// Pulled the new Batch Parameterization code out of Transuite and put here.
  /// Bug 18766
  /// </summary>
  public class ParameterizedBatch
  {
    /// <summary>
    /// Create the SQL INSERT statement for the TG_ITEM_TABLE.
    /// Then setup a list of parameter names and values in some
    /// obscure, lost-in-the-mists of time arrangement of the chicken
    /// bones needed to get batch parameterization to work.
    /// </summary>
    /// <param name="paramList"></param>
    /// <param name="paramCount"></param>
    /// <param name="IsBatch"></param>
    /// <param name="strFileNumber"></param>
    /// <param name="strFileSequenceNbr"></param>
    /// <param name="strEventNbr"></param>
    /// <param name="strTRANNBR"></param>
    /// <param name="strITEMACCTID"></param>
    /// <param name="strITEMID"></param>
    /// <param name="strTOTAL"></param>
    /// <param name="strITEMNBR"></param>
    /// <param name="strTAXED"></param>
    /// <param name="strAMOUNT"></param>
    /// <param name="strITEMDESC"></param>
    /// <param name="strQTY"></param>
    /// <param name="strGLACCTNBR"></param>
    /// <param name="strACCTID"></param>
    /// <param name="strChecksum"></param>
    /// <param name="strITEMCONFIGID"></param>
    /// <returns></returns>
    public static string CreateSQLFor_TG_ITEM_DATA(
      List<List<KeyValuePair<string, string>>> paramList,     // Bug 16540
      int paramCount,                                         // Bug 16540
      bool IsBatch, /*BUG# 10600 / 10614 - DH 2/19/2011 Combine statements.*/
      string strFileNumber,
      string strFileSequenceNbr,
      string strEventNbr,
      string strTRANNBR,
      string strITEMACCTID,
      string strITEMID,
      string strTOTAL,
      string strITEMNBR,
      string strTAXED,
      string strAMOUNT,
      string strITEMDESC,
      string strQTY,
      string strGLACCTNBR,
      string strACCTID,
      string strChecksum,
      string strITEMCONFIGID
      )
    {
      string strSQL = "";

      /*BUG# 10600 / 10614 - DH 2/19/2011 Combine statements.*/
      if (!IsBatch)
      {
        strSQL = "INSERT INTO TG_ITEM_DATA (ITEMNBR, TRANNBR, EVENTNBR, DEPFILENBR, DEPFILESEQ, ITEMID, GLACCTNBR, ";
        strSQL += "ACCTID, ITEMACCTID, ITEMDESC, AMOUNT, QTY, TAXED, TOTAL, CHECKSUM, ITEMCONFIGID) VALUES (";
      }

      // Bug 16540
      List<KeyValuePair<string, string>> paramListValues = new List<KeyValuePair<string, string>>();

      /*BUG# 10600 / 10614 - DH 2/19/2011 Combine statements.*/
      strSQL += strITEMNBR + ", " + strTRANNBR + ", " + strEventNbr + ", " + strFileNumber + ", " + strFileSequenceNbr + ", " + strITEMID;

      // Bug 16540
      paramListValues.Add(new KeyValuePair<string, string>("@ITEMNBR" + paramCount, strITEMNBR));
      paramListValues.Add(new KeyValuePair<string, string>("@TRANNBR" + paramCount, strTRANNBR));
      paramListValues.Add(new KeyValuePair<string, string>("@EventNbr" + paramCount, strEventNbr));
      paramListValues.Add(new KeyValuePair<string, string>("@FileNumber" + paramCount, strFileNumber));
      paramListValues.Add(new KeyValuePair<string, string>("@FileSequenceNbr" + paramCount, strFileSequenceNbr));
      paramListValues.Add(new KeyValuePair<string, string>("@ITEMID" + paramCount, strITEMID));
      // End Bug 16540

      strSQL = setParamValueNotEmpty("@GLACCTNBR", strGLACCTNBR, strSQL, paramCount, paramListValues);
      strSQL = setParamValueNotEmpty("@ACCTID", strACCTID, strSQL, paramCount, paramListValues);
      strSQL = setParamValueNotEmpty("@ITEMACCTID", strITEMACCTID, strSQL, paramCount, paramListValues);
      strSQL = setParamValueNotEmpty("@ITEMDESC", strITEMDESC, strSQL, paramCount, paramListValues);

      // Bug 23645 MJO - Fixed commas
      strSQL = strSQL + ", " + strAMOUNT + ", " + strQTY + ", '" + strTAXED + "', " + strTOTAL + "," + "'" + strChecksum + "'";

      paramListValues.Add(new KeyValuePair<string, string>("@AMOUNT" + paramCount, strAMOUNT));
      paramListValues.Add(new KeyValuePair<string, string>("@QTY" + paramCount, strQTY));
      paramListValues.Add(new KeyValuePair<string, string>("@TAXED" + paramCount, strTAXED));
      paramListValues.Add(new KeyValuePair<string, string>("@TOTAL" + paramCount, strTOTAL));
      paramListValues.Add(new KeyValuePair<string, string>("@Checksum" + paramCount, strChecksum));

      strSQL = setParamValueNotEmpty("@ITEMCONFIGID", strITEMCONFIGID, strSQL, paramCount, paramListValues);

      // Bug 23645 MJO - Close parentheses if this isn't a batch
      if (!IsBatch)
      {
        strSQL += ")";
      }

      paramList.Add(paramListValues);

      return strSQL;
    }

    public static string CreateSQLFor_GR_CUST_FIELD_DATA(
      List<List<KeyValuePair<string, string>>> paramList,     // Bug 16540
      int paramCount,                                         // Bug 16540
      bool IsBatch /*BUG# 10600 - DH 2/19/2011 Combine statements*/,
      string strTRANNBR, string strEventNbr, string strTTID, string strFileNumber, string strFileSequenceNbr, string strTAG, string strID, string strValue, string strLabel, string strScreenIndex, string strAttr, string strChecksum)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      string strSQL = "";


      /*BUG# 10600 - DH 2/19/2011 Combine statements*/
      if (!IsBatch)
      {
        strSQL = "INSERT INTO GR_CUST_FIELD_DATA (TRANNBR, EVENTNBR, DEPFILENBR, DEPFILESEQ, ";
        strSQL += "SCREENINDEX, CUSTLABEL, CUSTVALUE, CUSTID, TTID, CUSTTAG, CUSTATTR, CHECKSUM) VALUES (";
      }
      else
      {
        strSQL = "(";
      }
      /*end BUG# 10600 - Combine statements*/

      List<KeyValuePair<string, string>> paramListValues = new List<KeyValuePair<string, string>>();

      strSQL += strTRANNBR + ", " + strEventNbr + ", " + strFileNumber + ", " + strFileSequenceNbr + ", " + strScreenIndex;

      paramListValues.Add(new KeyValuePair<string, string>("@trannbr" + paramCount, strTRANNBR));
      paramListValues.Add(new KeyValuePair<string, string>("@EventNbr" + paramCount, strEventNbr));
      paramListValues.Add(new KeyValuePair<string, string>("@FileNumber" + paramCount, strFileNumber));
      paramListValues.Add(new KeyValuePair<string, string>("@FileSequenceNbr" + paramCount, strFileSequenceNbr));
      paramListValues.Add(new KeyValuePair<string, string>("@ScreenIndex" + paramCount, strScreenIndex));


      strSQL = setParamValueNotEmpty("@Label", strLabel, strSQL, paramCount, paramListValues);
      strSQL = setParamValueNotEmpty("@Value", strValue, strSQL, paramCount, paramListValues);
      strSQL = setParamValueNotEmpty("@ID", strID, strSQL, paramCount, paramListValues);
      strSQL = setParamValueNotEmpty("@TTID", strTTID, strSQL, paramCount, paramListValues);
      strSQL = setParamValueNotEmpty("@TAG", strTAG, strSQL, paramCount, paramListValues);
      strSQL = setParamValueNotEmpty("@strAttr", strAttr, strSQL, paramCount, paramListValues);  // BUG3861 S.X.

      strSQL = strSQL + "," + "'" + strChecksum + "'" + ")";
      paramListValues.Add(new KeyValuePair<string, string>("@Checksum" + paramCount, strChecksum));

      paramList.Add(paramListValues);

      return strSQL;
    }

    public static string CreateSQLFor_CUST_FIELD_DATA(
    List<List<KeyValuePair<string, string>>> paramList,
    int paramCount,
    bool IsBatch, string strPOSTNBR, string strEventNbr, string strPOSTID, string strFileNumber, string strFileSequenceNbr, string strTAG, string strID, string strValue, string strLabel, string strScreenIndex, string strPOST_TYPE, string strAttr, string strChecksum)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      /*BUG# 10600 & 12054 - DH 2/19/2011 Combine statements*/
      string strSQL = "";

      if (!IsBatch)
      {
        strSQL = "INSERT INTO CUST_FIELD_DATA (POSTNBR, EVENTNBR, DEPFILENBR, DEPFILESEQ, ";
        strSQL += "SCREENINDEX, CUSTLABEL, CUSTVALUE, CUSTID, POSTID, CUSTTAG, POST_TYPE, CUSTATTR, CHECKSUM) VALUES (";
      }
      else
      {
        strSQL = "(";
      }
      /*end BUG# 10600 & 12054 - Combine statements*/

      strSQL += strPOSTNBR + ", " + strEventNbr + ", " + strFileNumber + ", " + strFileSequenceNbr + ", " + strScreenIndex;

      List<KeyValuePair<string, string>> paramListValues = new List<KeyValuePair<string, string>>();

      paramListValues.Add(new KeyValuePair<string, string>("@POSTNBR" + paramCount, strPOSTNBR));
      paramListValues.Add(new KeyValuePair<string, string>("@EventNbr" + paramCount, strEventNbr));
      paramListValues.Add(new KeyValuePair<string, string>("@FileNumber" + paramCount, strFileNumber));
      paramListValues.Add(new KeyValuePair<string, string>("@FileSequenceNbr" + paramCount, strFileSequenceNbr));
      paramListValues.Add(new KeyValuePair<string, string>("@ScreenIndex" + paramCount, strScreenIndex));

      strSQL = setParamValueNotEmpty("@Label", strLabel, strSQL, paramCount, paramListValues);
      strSQL = setParamValueNotEmpty("@Value", strValue, strSQL, paramCount, paramListValues);
      strSQL = setParamValueNotEmpty("@ID", strID, strSQL, paramCount, paramListValues);
      strSQL = setParamValueNotEmpty("@POSTID", strPOSTID, strSQL, paramCount, paramListValues);
      strSQL = setParamValueNotEmpty("@TAG", strTAG, strSQL, paramCount, paramListValues);

      strSQL = strSQL + "," + "'" + strPOST_TYPE + "'";
      paramListValues.Add(new KeyValuePair<string, string>("@POST_TYPE" + paramCount, strPOST_TYPE));

      strSQL = setParamValueNotEmpty("@strAttr", strAttr, strSQL, paramCount, paramListValues);  // BUG3861 S.X.

      strSQL = strSQL + "," + "'" + strChecksum + "'" + ")";
      paramListValues.Add(new KeyValuePair<string, string>("@Checksum" + paramCount, strChecksum));

      paramList.Add(paramListValues);

      return strSQL;
    }

    /// <summary>
    /// Build the VALUE part of an batched INSERT statement.
    /// 
    /// This still uses the KeyValuePair lists; this would be too
    /// risky to convert to HybridDictionary.
    /// </summary>
    /// <param name="paramList"></param>
    /// <returns></returns>
    public static string buildParameterizedQueryForBatchInsert(
    List<List<KeyValuePair<string, string>>> paramList)
    {
      bool firstValueList = true;
      StringBuilder sb = new StringBuilder();
      foreach (List<KeyValuePair<string, string>> valueList in paramList)
      {
        if (firstValueList)
          firstValueList = false;
        else
          sb.Append(", ");

        bool firstValue = true;
        sb.Append('(');
        foreach (KeyValuePair<string, string> value in valueList)
        {
          if (firstValue)
            firstValue = false;
          else
            sb.Append(", ");

          sb.Append(value.Key);
        }
        sb.Append(')');
      }

      return sb.ToString();
    }

    /// <summary>
    /// Complete the parameterized batch INSERT statement and set the values
    /// based on the paramList.
    /// 
    /// This method still uses the clumsy KeyValuePair lists, but
    /// it is too dangerous to switch to HybridDictionary.
    /// 
    /// Note: there is a limit of 2100 parameters per statement
    /// <see cref="http://msdn.microsoft.com/en-us/library/ms143432%28v=sql.110%29.aspx"/>
    /// It appears that Table-valued Parameters are the way to go for larger numbers of parameters:
    /// <see cref="http://msdn.microsoft.com/en-us/library/bb510489.aspx#BulkInsert"/>
    /// 
    /// </summary>
    /// <param name="pDBConn"></param>
    /// <param name="strSQL"></param>
    /// <param name="parameterizedQueryString"></param>
    /// <param name="paramList"></param>
    public static void runParameterizedBatchedInsert(ConnectionTypeSynch pDBConn,
      string strSQL,
      string parameterizedQueryString,
      List<List<KeyValuePair<string, string>>> paramList)
    {
      string insertCommand = strSQL + ' ' + parameterizedQueryString;

      try
      {
        // Bug 25617: Make sure to use the SqlTransaction of the calling method
        // if it exists.
        SqlConnection sqlConnection = pDBConn.GetSqlConnection();
        SqlTransaction sqlTransaction = pDBConn.GetSqlTransaction();
        SqlCommand cmd = null;
        if (sqlTransaction == null)
          cmd = new SqlCommand(insertCommand, sqlConnection);
        else
          cmd = new SqlCommand(insertCommand, sqlConnection, sqlTransaction);

        using (cmd)
        {
            foreach (List<KeyValuePair<string, string>> valueList in paramList)
            {
              foreach (KeyValuePair<string, string> value in valueList)
              {
                if (value.Value == null)
                  cmd.Parameters.Add(new SqlParameter(value.Key, DBNull.Value));      // This is the double-secret trick to setting this to null.  A C# null with crash it
                else
                  cmd.Parameters.Add(new SqlParameter(value.Key, value.Value));
              }
            }
            cmd.ExecuteNonQuery();
          }

        }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error runParameterizedBatchedInsert", e);
        string msg = string.Format("Error running this parameterized batch INSERT: " + insertCommand + ": " + e.ToString());
        Logger.Log(msg, "Transuite: runParameterizedInsert", "err");
        // Bug 25617: Don't just swallow the exception, force a rollback and tell the user
        throw CASL_error.create("title", "Database Batch Insert Error", "message", e.Message, "code", "TS-1260");
      }
    }

    /// <summary>
    /// Set the parameterized field name and value in the paramList, being careful about nulls.
    /// This used strictly for Batched inserts.
    /// </summary>
    /// <param name="fieldName"></param>
    /// <param name="fieldValue"></param>
    /// <param name="strSQL"></param>
    /// <param name="paramCount"></param>
    /// <param name="paramListValues"></param>
    /// <returns></returns>
    private static string setParamValueNotEmpty(string fieldName, string fieldValue, string strSQL, int paramCount, List<KeyValuePair<string, string>> paramListValues)
    {
      StringBuilder fieldNameBuilder = new StringBuilder();
      fieldNameBuilder.Append(fieldName).Append(paramCount);

      // Bug 23645 MJO - Fixed comma placement
      if (fieldValue.Length == 0)
      {
        strSQL = strSQL + ", NULL";
        paramListValues.Add(new KeyValuePair<string, string>(fieldNameBuilder.ToString(), null));       // The null will be changed to DBNull.value before setting
        return strSQL;
      }
      else
      {
        strSQL = strSQL + ", '" + fieldValue + "'";
        paramListValues.Add(new KeyValuePair<string, string>(fieldNameBuilder.ToString(), fieldValue));
      }
      return strSQL;
    }

  }
}
