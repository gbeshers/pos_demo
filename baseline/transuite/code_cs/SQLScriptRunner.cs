﻿using System;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using System.IO;
using System.Data.SqlClient;
using CASL_engine;
using System.Web;

//
// Bug 21989 UMN Broadcast Messaging support
//
namespace TranSuiteServices
{
  public class SQLScriptRunner
  {
    /// <summary>
    /// Allow CASL code to check if a table is present.
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static object CASL_HasTable(params object[] args)
    {
      GenericObject geoARGS = misc.convert_args(args);
      String connectionString = (string)geoARGS.get("db_connection");
      String tableName = (string)geoARGS.get("table_name");
      return HasTable(connectionString, tableName);
    }
    
    /// <summary>
    /// Check if a database has a table
    /// </summary>
    /// <param name="connectionString"></param>
    /// <param name="tableName"></param>
    /// <returns>True/false or null on exception</returns>
    public static object HasTable(string connectionString, string tableName)
    {
      try
      {
        using (var connection = new SqlConnection(connectionString))
          
        using (var command = new SqlCommand("select case when exists((select * from information_schema.tables where table_name = @tableName)) then 1 else 0 end", connection))
        {
          command.Parameters.Add(new SqlParameter("tableName", tableName));
          connection.Open();
          return (int)command.ExecuteScalar() == 1;
        }
      }
      catch (Exception e)
      {
        Logger.cs_log_trace("Error in TranSuiteServices HasTable", e);
        return false;
      }
    }
    
    /// <summary>
    /// CASL interface to use the SQLServerManagementObject to run a DB script.
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static object CASL_RunSQLScript(params object[] args)
    {
      GenericObject geoARGS = misc.convert_args(args);
      String connectionString = (string)geoARGS.get("db_connection");
      String scriptFileName = (string)geoARGS.get("script_file_name");
      return RunSqlScript(connectionString, scriptFileName);
    }

    /// <summary>
    /// Use the SQLServerManagementObject to run a DB script
    /// </summary>
    /// <param name="connectionString"></param>
    /// <param name="scriptFileName"></param>
    /// <returns></returns>
    public static object RunSqlScript(String connectionString, String scriptFileName)
    {
      string siteLogical = (string)c_CASL.GEO.get("site_logical");
      var pathToScriptFile = HttpContext.Current.Server.MapPath($"{siteLogical}/redist/{scriptFileName}");
      try
      {
        var sqlScript = File.ReadAllText(pathToScriptFile);

        using (var connection = new SqlConnection(connectionString))
        {
          var server = new Server(new ServerConnection(connection));
          server.ConnectionContext.ExecuteNonQuery(sqlScript);
        }
        return true;
      }
      catch (Exception e)
      {
        Logger.cs_log_trace("Error in TranSuiteServices RunSqlScript", e);
        return false;
      }
    }
  }
}
