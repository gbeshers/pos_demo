using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using CASL_engine;

/*
  ERROR CODES FOR TRANSUITE ARE IN THE FORMAT TS-750.... PLEASE ALWAYS USE A SPECIFIC ERROR CODE FOR EACH ERROR.
  ALSO PLEASE UPDATE THIS SECTION WITH THE LARGEST ERROR YOU HAVE USED TO DATE. WHEN THE NEXT PERSON 
  CREATES ONE, THEY WILL BE ABLE TO ASSIGN ERRORS IN SEQUENCE./

  CTS-1 - "ConnectionTypeSync: EstablishRollback(): connection is not open."
*/
namespace TranSuiteServices
{
  /// <summary>
  /// Class moved here from transuite.
  /// This class controls the opening and closing of files in a database-independant way.
  /// This is an ancillary part of Bug 15941
  /// </summary>
  public class ConnectionTypeSynch
  {
    // Daniel wrote this class. 
    // Please do not change it without talking to me first.
    // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!
    // This class provides proper type of connection and transaction manipulation.

    private int m_iDBType = 0;

    // Connection Information
    private GenericObject m_pDBConnInfo = null;

    // SQL Server
    private SqlConnection m_pSqlConn = null;
    private SqlTransaction m_pSqlTran = null;
    public SqlCommand m_pSqlCmd = null;

    // Definitions of currently connected databases.
    public const int m_iCurrentDatabase_Oracle_ODBC = 1;
    public const int m_iCurrentDatabase_SqlServer = 2;
    public const int m_iCurrentDatabase_MsAccess_ODBC = 3;
    public const int m_iCurrentDatabase_Oracle_TNS = 4;

    public enum DBTypes { SqlServer, OracleTNS, OracleODBC, MSAccessODBC, Unknown };

    private DBTypes GetDBConnectionEnum(GenericObject db_connection_info)
    {
      string ConnectionClassString = (db_connection_info.get("_parent") as GenericObject).get("_name", "", true) as string;
      switch (ConnectionClassString)
      {
        case "Sql_server":
          return DBTypes.SqlServer;
        case "Oracle_tns":
          return DBTypes.OracleTNS;
        case "Oracle":
          return DBTypes.OracleODBC;
        case "Access":
          return DBTypes.MSAccessODBC;
        default:
          return DBTypes.Unknown;
      }
    }

    public ConnectionTypeSynch(GenericObject DBConnInfo)
    {
      m_pDBConnInfo = (GenericObject)DBConnInfo;
      m_iDBType = GetDBConnectionType(m_pDBConnInfo);

      if (m_iDBType == m_iCurrentDatabase_SqlServer)
        m_pSqlCmd = new SqlCommand();
    }

    public GenericObject GetDBConnectionInfo()
    {
      return m_pDBConnInfo;
    }
    private int GetDBConnectionType(GenericObject DBConnInfo)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      GenericObject DBInfo = (GenericObject)DBConnInfo;
      string strConnectionClass = (string)(DBInfo.get("_parent") as GenericObject).get("_name", "", true);

      if (strConnectionClass == "Oracle")
        return m_iCurrentDatabase_Oracle_ODBC;
      else if (strConnectionClass == "Sql_server")
        return m_iCurrentDatabase_SqlServer;
      else if (strConnectionClass == "Access")
        return m_iCurrentDatabase_MsAccess_ODBC;
      if (strConnectionClass == "Oracle_tns")
        return m_iCurrentDatabase_Oracle_TNS;

      return -1;// Undefined
    }

    /// <summary>
    /// Determine if the database server is 2008 or greater.
    /// Here are some typical version encodings that I have seen in our databases.
    /// 2008 --> "10.50.1600"
    /// 2012 --> "11.00.3156"
    /// 2014 --> "12.0.4100.1"
    /// Note how the first part is the important part.
    /// Rewrote for Bug 16542 
    /// </summary>
    /// <returns></returns>
    public bool IsServer_SQL2008orLater()
    {
      // Bug 15941: 
      if (m_pSqlConn == null)
      {
        string msg = "ConnectionTypeSync: IsServer_SQL2008(): connection is closed";
        Logger.Log(msg, "IsServer_SQL2008orLater", "warn");
        return false;
      }

      int iDBType = GetDBConnectionType();
      if (iDBType != m_iCurrentDatabase_SqlServer)
      {
        return false;
      }
      
      // So here we are with an SQL Server database and we do not know the version
      string[] parts = m_pSqlConn.ServerVersion.Split('.');

      int majorVersion = Convert.ToInt32(parts[0]);
      if (majorVersion >= 10)
        return true;
      else
        return false;
    }

    [Obsolete("You probably want to use IsServer_SQL2008_or_greater", false)]
    public bool IsServer_SQL2008()
    {
      // Bug 15941: 
      if (m_pSqlConn == null)
      {
        string msg = "ConnectionTypeSync: IsServer_SQL2008(): connection is closed";
        Logger.LogWarn(msg, "SQL Connection Check");
        return false;
      }

      //BUG# 10600 - DH 2/19/2011

      int iDBType = GetDBConnectionType();
      if (iDBType == 2 && m_pSqlConn.ServerVersion.StartsWith("10"))
      {
        return true;
      }

      return false;
    }

    public bool IsServer_SQL2005()
    {

      // Bug 15941
      if (m_pSqlConn == null)
      {
        string msg = "ConnectionTypeSync: IsServer_SQL2005(): connection is closed";
        Logger.LogWarn(msg, "SQL Connection Check");
        return false;
      }

      //BUG# 10600 - DH 2/19/2011

      int iDBType = GetDBConnectionType();
      if (iDBType == 2 && m_pSqlConn.ServerVersion.StartsWith("9"))
      {
        return true;
      }

      return false;
    }
    public bool IsServer_SQL2000()
    {

      // Bug 15941
      if (m_pSqlConn == null)
      {
        string msg = "ConnectionTypeSync: IsServer_SQL2000(): connection is closed";
        Logger.LogWarn(msg, "SQL Connection Check");
        return false;
      }

      //BUG# 10600 - DH 2/19/2011
      int iDBType = GetDBConnectionType();
      if (iDBType == 2 && m_pSqlConn.ServerVersion.StartsWith("8"))
      {
        return true;
      }

      return false;
    }

    // Used outside of class.
    public int GetDBConnectionType()
    {
      return m_iDBType;
    }

    /// <summary>
    /// Expose the connection for parameterization work.
    /// Bug 18766
    /// </summary>
    /// <returns></returns>
    public SqlConnection GetSqlConnection()
    {
      return m_pSqlConn;
    }

    /// <summary>
    /// Expose the transaction so parameterization can get to it.
    /// Bug 18766
    /// </summary>
    /// <returns></returns>
    public SqlTransaction GetSqlTransaction()
    {
      return m_pSqlTran;
    }

    //Bug 13832- Added parameter for returning detailed exception
    public bool EstablishDatabaseConnection(ref string err)
    {
      // Called before establishing a rollback.

      bool bRV = false;
      string strConnString = (string)m_pDBConnInfo.get("db_connection_string", "");

      if (m_iDBType == m_iCurrentDatabase_SqlServer)
      {
        // Bug 15941: Flag a possible missing close
        if (m_pSqlConn != null)
        {
          string msg = "ConnectionTypeSync: EstablishDatabaseConnection(): connection was not closed.  Reusing";
          Logger.LogWarn(msg, "SQL Connection Check");
        }

        m_pSqlConn = new SqlConnection(strConnString);
        try
        {
          m_pSqlConn.Open();
          m_pSqlCmd.Connection = m_pSqlConn;
          m_pSqlCmd.CommandTimeout = misc.Parse<Int32>(c_CASL.GEO.get("sql_query_timeout", "30").ToString());//BUG19591

          bRV = true;
        }
        catch (Exception e)
        {
          // Bug 17849
          Logger.cs_log_trace("Error EstablishDatabaseConnection", e);
          err = TranSuiteServices.ConstructDetailedError(e);//Bug 13832-return inner exception also
        }
      }
      return bRV;
    }

    public bool EstablishRollback(ref string strErrMsg)
    {
      // Called only after establishing a connection.
      try
      {
        if (m_iDBType == m_iCurrentDatabase_SqlServer)
        {
          // Bug 15941: Check for catastrophic condition
          if (m_pSqlConn == null)
          {
            string msg = "ConnectionTypeSync: EstablishRollback(): connection is not open";
            LogErrorAndThrowCASLException("message", "Error EstablishRollback.", "code", "CTS-1", msg);    // Serious error. Log and Throw exception
          }

          m_pSqlTran = m_pSqlConn.BeginTransaction(IsolationLevel.ReadCommitted);
          m_pSqlCmd.Transaction = m_pSqlTran;
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error Rollback", e);
        strErrMsg = TranSuiteServices.ConstructDetailedError(e); //Bug 13832-return inner exception also
        return false;
      }

      return true;
    }

    public bool ExecuteSQLAndRollBackIfNeeded(string strSQL, ref string strErr)
    {
      // Used only if transaction was already establiched.

      bool bRV = false;

      try
      {
        if (m_iDBType == m_iCurrentDatabase_SqlServer)
        {
          m_pSqlCmd.CommandText = strSQL;
          m_pSqlCmd.ExecuteNonQuery();
          bRV = true;
        }
      }
      catch (Exception e)
      {
        RollBack();

        // Bug 17849
        Logger.cs_log_trace("Error ExecuteSQLAndRollBackIfNeeded", e);
        strErr = TranSuiteServices.ConstructDetailedError(e);
        bRV = false;
      }

      return bRV;
    }

    public bool ExecuteSQL_NO_RollBack(string strSQL, ref string strErr)
    {
      // Used only if transaction was already establiched.

      bool bRV = false;

      try
      {
        if (m_iDBType == m_iCurrentDatabase_SqlServer)
        {
          m_pSqlCmd.CommandText = strSQL;
          m_pSqlCmd.ExecuteNonQuery();
          bRV = true;
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error ExecuteSQL_NO_RollBack", e);
        strErr = TranSuiteServices.ConstructDetailedError(e);//13832 - add innerexception for better debugging
        bRV = false;
      }

      return bRV;
    }

    public long GetLastID(ref string strErr)
    {
      try
      {
        if (m_iDBType == ConnectionTypeSynch.m_iCurrentDatabase_SqlServer)
        {
          m_pSqlCmd.CommandText = "SELECT @@IDENTITY";
          return (long)(decimal)m_pSqlCmd.ExecuteScalar();
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error GetLastID", e);
        strErr = TranSuiteServices.ConstructDetailedError(e);//bug 13832-Added innerexception for better debugging
      }
      return 0;
    }


    public void RollBack()
    {
      if (m_iDBType == m_iCurrentDatabase_SqlServer)
      {
        if (m_pSqlTran != null)   // Bug 18766: Protect agains nulls
          m_pSqlTran.Rollback();
      }
    }

    public bool CommitToDatabase(ref string strErr)
    {
      try
      {
        if (m_iDBType == m_iCurrentDatabase_SqlServer)
        {
          if (m_pSqlTran.Connection != null) //Bug 10182 NJ
          {
            m_pSqlTran.Commit();
          }
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error CommitToDB", e);
        strErr = TranSuiteServices.ConstructDetailedError(e);//Bug 13832 - return detailed error
        return false;
      }

      return true;
    }


    public void CloseDBConnection()
    {
      if (m_iDBType == m_iCurrentDatabase_SqlServer)
      {
        // Bug 15941: Prevent Dispose() from being called twice
        if (m_pSqlConn == null)
        {
          string msg = "ConnectionTypeSync: CloseDBConnection(): connection is already closed";
          Logger.LogWarn(msg, "SQL Connection Check");
        }
        else
        {
          m_pSqlConn.Close();
          m_pSqlConn.Dispose();
        }
        m_pSqlConn = null;    // Bug 15941: Flag as closed & disposed
      }
    }

    private static void LogErrorAndThrowCASLException(string strParam1, string strParam2, string strParam3, string strParam4, string innerException)
    {
      string error_message = strParam2 + innerException + "(" + strParam4 + ")";//Bug 13832-missed logging the innerException exception
      Logger.LogDebug(error_message, "ConnectionTypeSynch.HandleErrorsAndLocking");
      throw CASL_error.create(strParam1, strParam2, strParam3, strParam4, "inner_error", innerException, "user_message", true);// This should be the only call to it in the whole file.
    }

  }

}
