﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Net;
using CASL_engine;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web;
using QRCoder;

//
// Bug 20562 UMN / EOM Add Multi-factor authentication (MFA). This code is from:
//   https://github.com/brandonpotter/GoogleAuthenticator
// And it carries an Apache license.
//
// I included the code rather than crafting it as a DLL, because we'll want to change the interface so
// that we can support RSA as an alternative strategy.
//
namespace TranSuiteServices
{
  public class TwoFactorAuthenticator
  {
    public static DateTime _epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
    public TimeSpan DefaultClockDriftTolerance { get; set; }
    public bool UseManagedSha1Algorithm { get; set; }
    public bool TryUnmanagedAlgorithmOnFailure { get; set; }

    public TwoFactorAuthenticator() : this(true, true) { }

    public TwoFactorAuthenticator(bool useManagedSha1, bool useUnmanagedOnFail)
    {
      DefaultClockDriftTolerance = TimeSpan.FromMinutes(5);
      UseManagedSha1Algorithm = useManagedSha1;
      TryUnmanagedAlgorithmOnFailure = useUnmanagedOnFail;
    }

    /// <summary>
    /// Generate a setup code for a Google Authenticator user to scan.
    /// </summary>
    /// <param name="accountTitleNoSpaces">Account Title (no spaces)</param>
    /// <param name="accountSecretKey">Account Secret Key</param>
    /// <param name="qrCodeWidth">QR Code Width</param>
    /// <param name="qrCodeHeight">QR Code Height</param>
    /// <returns>TFASetupCode object</returns>
    public TFASetupCode GenerateTFASetupCode(string accountTitleNoSpaces, string accountSecretKey, int qrCodeWidth, int qrCodeHeight)
    {
      return GenerateTFASetupCode(null, accountTitleNoSpaces, accountSecretKey, qrCodeWidth, qrCodeHeight);
    }

    /// <summary>
    /// Generate a setup code for a Google Authenticator user to scan (with issuer ID).
    /// </summary>
    /// <param name="issuer">Issuer ID (the name of the system, i.e. 'MyApp')</param>
    /// <param name="accountTitleNoSpaces">Account Title (no spaces)</param>
    /// <param name="accountSecretKey">Account Secret Key</param>
    /// <param name="qrCodeWidth">QR Code Width</param>
    /// <param name="qrCodeHeight">QR Code Height</param>
    /// <returns>TFASetupCode object</returns>
    public TFASetupCode GenerateTFASetupCode(string issuer, string accountTitleNoSpaces, string accountSecretKey, int qrCodeWidth, int qrCodeHeight)
    {
      return GenerateTFASetupCode(issuer, accountTitleNoSpaces, accountSecretKey, qrCodeWidth, qrCodeHeight, false);
    }

    /// <summary>
    /// Generate a setup code for a Google Authenticator user to scan (with issuer ID).
    /// </summary>
    /// <param name="issuer">Issuer ID (the name of the system, i.e. 'MyApp')</param>
    /// <param name="accountTitleNoSpaces">Account Title (no spaces)</param>
    /// <param name="accountSecretKey">Account Secret Key</param>
    /// <param name="qrCodeWidth">QR Code Width</param>
    /// <param name="qrCodeHeight">QR Code Height</param>
    /// <param name="useHttps">Use HTTPS instead of HTTP</param>
    /// <returns>TFASetupCode object</returns>
    public TFASetupCode GenerateTFASetupCode(string issuer, string accountTitleNoSpaces, string accountSecretKey, int qrCodeWidth, int qrCodeHeight, bool useHttps)
    {
      if (accountTitleNoSpaces == null) { throw new NullReferenceException("Account Title is null"); }

      accountTitleNoSpaces = accountTitleNoSpaces.Replace(" ", "");

      TFASetupCode sC = new TFASetupCode();
      sC.Account = accountTitleNoSpaces;
      sC.AccountSecretKey = accountSecretKey;
      sC.Issuer = issuer;

      string encodedSecretKey = EncodeAccountSecretKey(accountSecretKey);
      sC.ManualEntryKey = encodedSecretKey;

      string provisionUrl = null;

      if (string.IsNullOrEmpty(issuer))
      {
        provisionUrl = String.Format("otpauth://totp/{0}?secret={1}", UrlEncode(accountTitleNoSpaces), encodedSecretKey);
      }
      else
      {
        provisionUrl = String.Format("otpauth://totp/{0}?secret={1}&issuer={2}", UrlEncode(accountTitleNoSpaces), encodedSecretKey, UrlEncode(issuer));
      }

      sC.QrCodeBytes = CreateQRCode(provisionUrl);

      return sC;
    }

    private string UrlEncode(string value)
    {
      StringBuilder result = new StringBuilder();
      string validChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.~";

      foreach (char symbol in value)
      {
        if (validChars.IndexOf(symbol) != -1)
        {
          result.Append(symbol);
        }
        else
        {
          result.Append('%' + String.Format("{0:X2}", (int)symbol));
        }
      }

      return result.ToString().Replace(" ", "%20");
    }

    /// <summary>
    /// Encodes the secret Key
    /// </summary>
    /// <param name="accountSecretKey">plaintext secret key</param>
    /// <returns>Base32 Encoded Secret Key string</returns>
    private string EncodeAccountSecretKey(string accountSecretKey)
    {
      //if (accountSecretKey.Length < 10)
      //{
      //    accountSecretKey = accountSecretKey.PadRight(10, '0');
      //}

      //if (accountSecretKey.Length > 12)
      //{
      //    accountSecretKey = accountSecretKey.Substring(0, 12);
      //}

      return Base32Encode(System.Text.ASCIIEncoding.Default.GetBytes(accountSecretKey));
    }

    /// <summary>
    /// Base32 Encodes byte array
    /// </summary>
    /// <param name="accountSecretKey">plaintext byte array</param>
    /// <returns>encoded string</returns>
    private string Base32Encode(byte[] data)
    {
      int inByteSize = 8;
      int outByteSize = 5;
      char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567".ToCharArray();

      int i = 0, index = 0, digit = 0;
      int current_byte, next_byte;
      StringBuilder result = new StringBuilder((data.Length + 7) * inByteSize / outByteSize);

      while (i < data.Length)
      {
        current_byte = (data[i] >= 0) ? data[i] : (data[i] + 256); // Unsign

        /* Is the current digit going to span a byte boundary? */
        if (index > (inByteSize - outByteSize))
        {
          if ((i + 1) < data.Length)
            next_byte = (data[i + 1] >= 0) ? data[i + 1] : (data[i + 1] + 256);
          else
            next_byte = 0;

          digit = current_byte & (0xFF >> index);
          index = (index + outByteSize) % inByteSize;
          digit <<= index;
          digit |= next_byte >> (inByteSize - index);
          i++;
        }
        else
        {
          digit = (current_byte >> (inByteSize - (index + outByteSize))) & 0x1F;
          index = (index + outByteSize) % inByteSize;
          if (index == 0)
            i++;
        }
        result.Append(alphabet[digit]);
      }

      return result.ToString();
    }

    public string GeneratePINAtInterval(string accountSecretKey, long counter, int digits = 6)
    {
      return GenerateHashedCode(accountSecretKey, counter, digits);
    }

    internal string GenerateHashedCode(string secret, long iterationNumber, int digits = 6)
    {
      byte[] key = Encoding.ASCII.GetBytes(secret);
      return GenerateHashedCode(key, iterationNumber, digits);
    }

    /// <summary>
    /// Generates the PIN based on key, and iteration
    /// </summary>
    /// <param name="key">secret key in byte array</param>
    /// <param name="iterationNumber">iteration counter</param>
    /// <param name="digits">size of the pin string</param>
    /// <returns>PIN in string form</returns>
    internal string GenerateHashedCode(byte[] key, long iterationNumber, int digits = 6)
    {
      byte[] counter = BitConverter.GetBytes(iterationNumber);

      if (BitConverter.IsLittleEndian)
      {
        Array.Reverse(counter);
      }

      HMACSHA1 hmac = getHMACSha1Algorithm(key);

      byte[] hash = hmac.ComputeHash(counter);

      int offset = hash[hash.Length - 1] & 0xf;

      // Convert the 4 bytes into an integer, ignoring the sign.
      int binary =
          ((hash[offset] & 0x7f) << 24)
          | (hash[offset + 1] << 16)
          | (hash[offset + 2] << 8)
          | (hash[offset + 3]);

      int password = binary % (int)Math.Pow(10, digits);
      return password.ToString(new string('0', digits));
    }

    private long GetCurrentCounter()
    {
      return GetCurrentCounter(DateTime.UtcNow, _epoch, 30);
    }

    private long GetCurrentCounter(DateTime now, DateTime epoch, int timeStep)
    {
      return (long)(now - epoch).TotalSeconds / timeStep;
    }

    /// <summary>
    /// Creates a HMACSHA1 algorithm to use to hash the counter bytes. By default, this will attempt to use
    /// the managed SHA1 class (SHA1Manager) and on exception (FIPS-compliant machine policy, etc) will attempt
    /// to use the unmanaged SHA1 class (SHA1CryptoServiceProvider).
    /// </summary>
    /// <param name="key">User's secret key, in bytes</param>
    /// <returns>HMACSHA1 cryptographic algorithm</returns>        
    private HMACSHA1 getHMACSha1Algorithm(byte[] key)
    {
      HMACSHA1 hmac;

      try
      {
        hmac = new HMACSHA1(key, UseManagedSha1Algorithm);
      }
      catch (InvalidOperationException ioe)
      {
        if (UseManagedSha1Algorithm && TryUnmanagedAlgorithmOnFailure)
        {
          try
          {
            hmac = new HMACSHA1(key, false);
          }
          catch (InvalidOperationException ioe2)
          {
            throw ioe2;
          }
        }
        else
        {
          throw ioe;
        }
      }

      return hmac;
    }

    public bool ValidateTwoFactorPIN(string accountSecretKey, string twoFactorCodeFromClient)
    {
      return ValidateTwoFactorPIN(accountSecretKey, twoFactorCodeFromClient, DefaultClockDriftTolerance);
    }

    public bool ValidateTwoFactorPIN(string accountSecretKey, string twoFactorCodeFromClient, TimeSpan timeTolerance)
    {
      var codes = GetCurrentPINs(accountSecretKey, timeTolerance);
      return codes.Any(c => c == twoFactorCodeFromClient);
    }

    public string GetCurrentPIN(string accountSecretKey)
    {
      return GeneratePINAtInterval(accountSecretKey, GetCurrentCounter());
    }

    public string GetCurrentPIN(string accountSecretKey, DateTime now)
    {
      return GeneratePINAtInterval(accountSecretKey, GetCurrentCounter(now, _epoch, 30));
    }

    public string[] GetCurrentPINs(string accountSecretKey)
    {
      return GetCurrentPINs(accountSecretKey, DefaultClockDriftTolerance);
    }

    /// <summary>
    /// Generates pins using secret key for the specified time period
    /// </summary>
    /// <param name="accountSecretKey">plaintext secret key</param>
    /// <param name="timeTolerance">span of time for which to generate the PINs</param>
    /// <returns>string array of pins for the current time period</returns>
    public string[] GetCurrentPINs(string accountSecretKey, TimeSpan timeTolerance)
    {
      List<string> codes = new List<string>();
      long iterationCounter = GetCurrentCounter();
      int iterationOffset = 0;

      if (timeTolerance.TotalSeconds > 30)
      {
        iterationOffset = Convert.ToInt32(timeTolerance.TotalSeconds / 30.00);
      }
      // Bug 24094 MJO - Default to what was passed in!
      else
      {
        iterationOffset = Convert.ToInt32(timeTolerance.TotalSeconds);
      }

      long iterationStart = iterationCounter - iterationOffset;
      long iterationEnd = iterationCounter + iterationOffset;

      for (long counter = iterationStart; counter <= iterationEnd; counter++)
      {
        codes.Add(GeneratePINAtInterval(accountSecretKey, counter));
      }

      return codes.ToArray();
    }

    /// <summary>
    /// Creates new second factor info like User Key - mfa_user_key (GUID), Manual entry code and QR code.  
    /// Should only be called when new info is needed - User creation or New codes request
    /// </summary>
    /// <param name="args">Standard object array with key/value pair params</param>
    /// <returns>GEO object with MFA info populated</returns>
    public static GenericObject CASLGenerateMFAInfo(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      string user_id = geo_args.get("user_id") as string;
      GenericObject ret = new GenericObject();

      // Generate a secret key for the user
      String mfaUserKey = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10);


      // Get the manual setup code, and QR code based on secret key and the account value
      String accountTitle = String.Format("{0}@{1}", user_id, misc.get_site_name());

      TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
      var setupInfo = tfa.GenerateTFASetupCode("iPayment", accountTitle, mfaUserKey, 300, 300);

      ret.set("mfa_manual_entry_code", setupInfo.ManualEntryKey);
      ret.set("mfa_qr_code_bytes", setupInfo.QrCodeBytes);

      //Encrypt User key
      string keyBase64 = misc.StringToBase64String(mfaUserKey);
      GenericObject bytes = Crypto.symmetric_encrypt("data", keyBase64, "is_ascii_string", false);
      byte[] data = (byte[])bytes.get("data");
      ret.set("mfa_user_key", Convert.ToBase64String(data));

      return ret;
    }

    /// <summary>
    /// Added by EOM
    /// Generates QR code using Gma.QrCodeNet.Encoder.dll 
    /// </summary>
    /// <param name="strQRData">string value to be encoded into QR code.  Authenticator example: "otpauth://totp/iPayment?secret=foo&issuer=bar"</param>
    /// <returns>byte array that is the image </returns>
    private static byte[] CreateQRCode(string strQRData)
    {
      // Bug IPAY-1626 MJO - Replaced Gma.QrCodeNet with QRCoder
      QRCodeGenerator qrGenerator = new QRCodeGenerator();
      QRCodeData qrCodeData = qrGenerator.CreateQrCode(strQRData, QRCodeGenerator.ECCLevel.L); // Google Authenticator uses Low
      PngByteQRCode qrCode = new PngByteQRCode(qrCodeData);

      byte[] black = new[] { Color.Black.R, Color.Black.G, Color.Black.B };
      byte[] white = new[] { Color.White.R, Color.White.G, Color.White.B };

      return qrCode.GetGraphic(5, black, white);
    }
  }

  /// <summary>
  /// Contains the Two Factor Authentication setup info
  /// </summary>
  public class TFASetupCode
  {
    public string Account { get; internal set; }  //Provider
    public string Issuer { get; internal set; }
    public string AccountSecretKey { get; internal set; }
    public string ManualEntryKey { get; internal set; }
    public byte[] QrCodeBytes { get; internal set; }
  }

}
