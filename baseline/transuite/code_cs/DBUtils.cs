﻿using System;
using System.Data;
using System.Threading;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using CASL_engine;

namespace TranSuiteServices
{
  public class DBUtils
  {
    /// <summary>
    /// Parameterize and run the MAX query against the TRAN table.
    /// This call is done inside a transaction in the PostIndividualTransaction() method.
    /// No need to commit / rollback here.
    /// </summary>
    /// <param name="pDBConn"></param>
    /// <param name="strFileNumber"></param>
    /// <param name="strFileSequenceNbr"></param>
    /// <param name="strEventNbr"></param>
    /// <returns></returns>
    public static long runMaxTrannbrQuery(ConnectionTypeSynch pDBConn, string strFileNumber, string strFileSequenceNbr, string strEventNbr)
    {
      try
      {
        string sql_string =
          @"SELECT MAX(TRANNBR) AS COUNT FROM TG_TRAN_DATA 
               WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND EVENTNBR=@eventnbr";

        HybridDictionary sql_args = new HybridDictionary();
        sql_args["depfilenbr"] = strFileNumber;
        sql_args["depfileseq"] = strFileSequenceNbr;
        sql_args["eventnbr"] = strEventNbr;

        using (SqlCommand paramCommand = ParamUtils.CreateParamCommand(pDBConn, sql_string, sql_args))
        {
          var count = paramCommand.ExecuteScalar();

          long longCount = 0;
          if (count.GetType() != typeof(DBNull))
            longCount = Convert.ToInt64(count);
          longCount++;
          return longCount;
        }

      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error runMaxTrannbrQuery", e);

        TranSuiteServices.HandleErrorsAndLocking("message", "runMaxTrannbrQuery: Error executing SQL.", "code", "TS-805", e.ToString());
        throw;
      }
    }

    /// <summary>
    /// Utility method to read all the matching fields from the PAYEVENT table into a Geo.
    /// Parameterized.
    /// Called by ChangeEventStatus in transuite inside a transaction, but let the calling method 
    /// do the commit/rollback.
    /// TODO: investivate the AddCommand() method in db_vector_reader_sql to do this for me.
    /// </summary>
    /// <param name="pDBConn"></param>
    /// <param name="strEventNbr"></param>
    /// <param name="strFileNbr"></param>
    /// <param name="strFileSequenceNbr"></param>
    /// <returns></returns>
    public static GenericObject readAllPayEventFieldsIntoAGeo(ConnectionTypeSynch pDBConn, string strEventNbr, string strFileNbr, string strFileSequenceNbr)
    {
      try
      {
        // SELECT * FROM TG_PAYEVENT_DATA WITH (ROWLOCK) WHERE EVENTNBR=1 AND DEPFILENBR=2014345 AND DEPFILESEQ=1
        string paramQuery = @"SELECT * FROM TG_PAYEVENT_DATA WITH (ROWLOCK) 
          WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND EVENTNBR=@eventnbr";

        HybridDictionary sql_args = new HybridDictionary();
        sql_args["depfilenbr"] = strFileNbr;
        sql_args["depfileseq"] = strFileSequenceNbr;
        sql_args["eventnbr"] = strEventNbr;

        using (SqlCommand selectCommand = ParamUtils.CreateParamCommand(pDBConn, paramQuery, sql_args))
        {
          return readRecordsIntoGeo(selectCommand);
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error readAllPayEventFieldsIntoAGeo", e);
        TranSuiteServices.HandleErrorsAndLocking("message", "readAllFieldsIntoAGeo: Error executing SQL.", "code", "TS-805", e.ToString());
        throw;
      }

    }

    /// <summary>
    /// Utility method to take a SELECT command and read all the records into a Generic Object
    /// using an SqlDataReader.
    /// </summary>
    /// <param name="selectCommand"></param>
    /// <returns>GenericObject without a _parent key</returns>
    public static GenericObject readRecordsIntoGeo(SqlCommand selectCommand)
    {
      using (SqlDataReader dataReader = selectCommand.ExecuteReader())
      {
        // Just read the first record
        if (dataReader.Read() == false)
          return new GenericObject();

        GenericObject tmp = new GenericObject();
        for (int i = 0; i < dataReader.FieldCount; i++)
        {
          if (dataReader.IsDBNull(i))
            continue;
          string fieldName = dataReader.GetName(i);
          var fieldValue = dataReader.GetValue(i);
          tmp[fieldName] = fieldValue;
        }
        // Set a vestigial parent
        tmp["_parent"] = c_CASL.GEO;
        return tmp;
      }
    }

    /// <summary>
    /// Take a SELECT statement and read into a GenericObject.
    /// This is called by UpdateTranChecksum() and done inside a transaction
    /// so there is no need to do a commit/rollback here.
    /// </summary>
    /// <param name="pDBConn">Connection information</param>
    /// <param name="sql_string">SQL Text</param>
    /// <param name="sql_args">Arguments in a HybridDictionary</param>
    /// <returns>GenericObject without a _parent key</returns>
    internal static GenericObject readSelectIntoGeo(ConnectionTypeSynch pDBConn, string sql_string, HybridDictionary sql_args)
    {
      try
      {
        using (SqlCommand selectCommand = ParamUtils.CreateParamCommand(pDBConn, sql_string, sql_args))
        {
          return readRecordsIntoGeo(selectCommand);
        }
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error readSelectIntoGeo", e);
        TranSuiteServices.HandleErrorsAndLocking("message", "readAllFieldsIntoAGeo: Error executing SQL.", "code", "TS-805", e.ToString());
        throw;
      }

    }
  }
}
