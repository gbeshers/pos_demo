﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Collections;
using System.Collections.Concurrent;

// Bug #11869 Mike O - Used to keep track of user sessions

namespace TranSuiteServices
{
  public sealed class EventValidator
  {
    private static readonly EventValidator validator = new EventValidator();

    private ConcurrentDictionary<string, string> dict = new ConcurrentDictionary<string, string>();

    private EventValidator() { }

    // Get the singleton
    public static EventValidator GetInstance()
    {
      return validator;
    }

    // Update the session id with the provided user and app strings
    public void updateLockedEventforUser(string eventnbr, string userid)
    {
      if (userid == "")//original user will pass empty string as userid to release locked event 
      {  string strOut; 
        dict.TryRemove(eventnbr, out strOut);
      }
      else
        dict[eventnbr]=userid;
    }

    // Validate the session id for the provided user and app strings
    public bool validateEventforUser(string eventnbr, string userid)
    {
      string key = eventnbr;

      return dict.Keys.Contains(key) ? dict[key].Equals(userid) : (userid == "" ? true : false);// no event nbr in list mean event is not locked. 
    }
    // Validate the session id for the provided user and app strings
    public string getEventLockedUser(string eventnbr)
    {
      string key = eventnbr;

      return dict.Keys.Contains(key) ? dict[key]:"" ;
    }

  }
}
