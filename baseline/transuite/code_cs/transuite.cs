// This file: transuite.cs
using System;
using System.Data;
using System.Threading;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Specialized;
using CASL_engine;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Reflection;
using System.ComponentModel;
using System.Xml;
using System.Text;
using System.Web;
using System.Net;
using System.Globalization;
using System.Collections.Generic;
using System.Security.Cryptography;




/*
  ERROR CODES FOR TRANSUITE ARE IN THE FORMAT TS-750.... PLEASE ALWAYS USE A SPECIFIC ERROR CODE FOR EACH ERROR.
  ALSO PLEASE UPDATE THIS SECTION WITH THE LARGEST ERROR YOU HAVE USED TO DATE. WHEN THE NEXT PERSON 
  CREATES ONE, THEY WILL BE ABLE TO ASSIGN ERRORS IN SEQUENCE./

  TS-877 - "No User with that UserID found!Make sure the Caps Lock is off and try again."
  TS-878 - "Password must be diffferent!Make sure the new password is different from the old password."
  TS-879 - "The License in the Database is invalid! Please call CORE Buisness Technologies for a new one."
  TS-880 - "The License is not valid yet."
  TS-881 - "The License has expired. Please call CORE Business Technologies for a new License Code."
  TS-758 - "Error opening database connection."
  TS-759 - "Error opening database connection."
  TS-760 - "Unable to set checksum on TG_DEPFILE_DATA. Please call system administrator." + strError
  TS-761 - "Unable to set checksum on TG_DEPFILE_DATA. Please call system administrator." + strError
  TS-762 - "Source Type is required."
  TS-763 - "Error opening database connection."
  TS-764 - "Error creating database transaction." + strErrMsg
  TS-765 - "Error executing SQL." + strErr
  TS-766 - "Error executing SQL." + strErr
  TS-767 - "Unable to commit changes to database." + strErr
  TS-768 - "Unable to set checksum on TG_DEPFILE_DATA. Please call system administrator." + strError
  TS-769 - "Unable to set checksum on TG_DEPFILE_DATA. Please call system administrator." + strError
  TS-770 - "Error executing SQL." + strError
  TS-771 - "Error executing SQL." + strError
  TS-772 - "Error opening database connection."
  TS-773 - "Error creating database transaction." + strErr
  TS-775 - "Error executing SQL." + strErr
  TS-776 - "Unable to commit changes to database." + strErr
  TS-778 - "Error opening database connection."
  TS-779 - "Unable to retrienve image from One-Step database."
  TS-780 - "Error opening database connection."
  TS-781 - "Missing DOC_IMAGE_REF in TG_IMAGE_DATA."
  TS-782 - "Failed to retrieve an image from TG_IMAGE_DATA."
  TS-784 - "Error opening database connection."
  TS-785 - "Error creating database transaction." + strErr
  TS-786 - "Unable to commit changes to database." + strErr
  TS-788 - "Error opening database connection."
  TS-789 - "Error creating database transaction." + strErr
  TS-790 - "Unable to find files to update!"
  TS-791 - "Transaction File " + strFileNumber + " is not Active!  No changes are permitted.","code", "TS-791");
  TS-791 - "Transaction File " + strFileNumber + " is not Active!  No changes are permitted.","code", "TS-791");
  TS-792 - "Incomplete File number provided to set status!"
  TS-793 - "Error executing SQL." + strErr
  TS-794 - "Unable to post transaction." + strError
  TS-795 - "Unable to post transaction. " + strError
  TS-796 - "Unable to process tender."
  TS-797 - "Unable to commit changes to database." + strErr
  TS-798 - "Error opening database connection."
  TS-799 - "Error creating database transaction." + strErr;
  TS-800 - "Error executing SQL." + strErr
  TS-801 - "Unable to process tender."
  TS-802 - "Unable to commit changes to database." + strErr
  TS-803 - "Error opening database connection."
  TS-804 - "Error creating database transaction." + strErr
  TS-805 - "Error executing SQL." + strErr
  TS-806 - "Unable to process custom field for tender."
  TS-807 - "Unable to commit tender changes to database."
  TS-808 - "Unable to commit changes to database." + strErr
  TS-809 - "Error opening database connection."
  TS-810 - "Error creating database transaction." + strErr
  TS-811 - "Error executing SQL." + strErr
  TS-812 - "Unable to commit changes to database." + strErr
  TS-813 - "Unable to set checksum on TG_TRAN_DATA. Please call system administrator." + strError
  TS-814 - "Item number is required.  File post halted."
  TS-821 - "Error opening database connection."
  TS-822 - "Error creating database transaction." + strErr
  TS-823 - "Error executing SQL." + strErr
  TS-824 - "Unable to commit changes to database." + strError
  TS-825 - "Unable to post transaction. " + strError
  TS-826 - "Unable to commit changes to database." + strErr
  TS-827 - "Unable to rollback changes to database."
  TS-828 - "Error opening database connection."
  TS-829 - "Error creating database transaction."
  TS-832 - "Transaction File " + strFileNbr + " is not Active!  No changes are permitted.","code", "TS-832");
  TS-833 - "Transaction could not be voided. " + m_sErrMsg
  TS-835 - "Unable to commit changes to database."
  TS-837 - "Error opening database connection."
  TS-838 - "Error creating database transaction."
  TS-841 - "Transaction File " + strFileNbr + " is not Active!  No changes are permitted.","code", "TS-841");
  TS-842 - "Tender could not be voided."
  TS-843 - "Unable to commit changes to database."
  TS-844 - "Error opening database connection."
  TS-849 - "Unable to commit changes to database." + strErr
  TS-850 - "Error opening database connection."
  TS-851 - "Unable to commit changes to database." + strErr
  TS-853 - "Error opening database connection."
  TS-854 - "Error creating database transaction." + strErr
  TS-855 - "Error executing SQL." + strErr
  TS-856 - "Unable to commit changes to database." + strErr
  TS-857 - "Required information missing in Change Event Status!"
  TS-858 - "Error opening database connection."
  TS-860 - "Unable to process tender."
  TS-861 - "Required information missing in Change Event Status!"
  TS-862 - "Error opening database connection."
  TS-863 - "Required information missing in Change Event Status!"
  TS-864 - "Error opening database connection."
  TS-867 - "CreateDepositChecksum Error: " + strError
  TS-868 - "CreateItemDataChecksum Error : " + strError
  TS-869 - "Error opening database connection."
  TS-870 - "Error creating database transaction." + strError
  TS-871 - "CreateCustFieldChecksum Error: " + strError
  TS-872 - "CreateTranFileChecksum Error : " + strError
  TS-873 - "Error opening database connection."
  TS-874 - "CreateTenderChecksum Error : " + strError
  TS-875 - "CreateTranChecksum Error : " + strError
  TS-876 - "CreatePayEventChecksum Error : " + strError
  TS-877 - "No User with that UserID found!Make sure the Caps Lock is off and try again."
  TS-878 - "Password must be diffferent!Make sure the new password is different from the old password."
  TS-880 - "The License is not valid yet."
  TS-881 - "The License has expired. Please call CORE Business Technologies for a new License Code."
  TS-883 - "Failed to retrieve EventNbr while posting images to TG_IMAGE_DATA.");
  TS-884 - "Failed to retrieve EventNbr while posting images to TG_TENDER_DATA.");
  TS-885 - "Failed to retrieve EventNbr while posting images to TG_TENDER_DATA.");
  TS-886 - "Error creating database transaction."
  TS-887 - "Deposit Type is required!"
  TS-888 - "Transaction File " + strfile_DEPFILENAME + " is not Active!  No changes are permitted.");
  TS-890 - "Unable to post deposit record in file: " + strfile_DEPFILENAME
  TS-891 - "Unable to post tender record in file: " + strfile_DEPFILENAME     
  TS-892 - "Error creating database transaction."
  TS-893 - "User ID is required to Void Deposit!");
  TS-894 - "Either complete Deposit ID and Deposit Nbr OR Source Type, Source Group, and Source RefID are required to void a deposit!");
  TS-895 - "Deposit Data Open Error: No record found for: " + strSQL);
  TS-896 - "Ambiguous Search Error: " + iCount.ToString() + " deposit records found.");
  TS-897 - "Invalid checksum for deposit: " + strDEPOSITNBR);
  TS-898 - "Unable to void deposit. " + strErr);
  TS-899 - "Error creating database transaction." + strErr);
  TS-900 - "Unable to Balance. Transaction File " + strFileNumber + " is not a currently Active file.");
  TS-901 - "Checksum Error in file: " + strFileNumber);
  TS-902 - "Error executing SQL. File: "
  TS-903 - "Unable to post transaction. File: "
  TS-904 - "Unable to post transaction. File: "
  TS-905 - "Unable to modify event status for:"
  TS-906 - "Unable to set checksum for: "
  TS-907 - "Unable to set checksum for: "
  TS-908 - "Unable to reset file to balanced: "
  TS-909 - "Unable to commit changes to database."
  TS-910 - "Error creating database transaction."
  TS-911 - "Transaction File " + strFileName + " is already Active!");
  TS-912 - "Unable to Reset to Active.  Transaction File " + strFileName + " is not a currently Balanced file.");
  TS-913 - "Transaction File " + strFileName + " has already been updated or accepted.  You may not access this File.");    Matt: changed accepted to updated per request of Emilio
  TS-914 - "Unable to void a transaction. File: " + strFileName + " Event:
  TS-915 - "Unable to void a transaction. File: " + strFileName + " Event: 
  TS-916 - "Checksum failed for: "
  TS-918 - "Unable to modify event status for: "
  TS-919 - "Unable to set checksum for: "
  TS-921 - "Unable to reset file to active: " + "
  TS-922 - "Unable to reset file to active: " + "
  TS-923 - "Unable to commit changes to database."
  TS-924 - "Error creating database transaction."
  TS-925 - "Unable to Balance. Transaction File " + strFileNumber + " is not a currently Active file.");
  TS-926 - "Unable to retrieve checksum information.");
  TS-927 - "Checksum failed for: "
  TS-928 - "Unable to update completion date for " 
  TS-929 - "Unable to modify event status for: "
  TS-930 - "Unable to set checksum for: "
  TS-931 - "Unable to set checksum for: "
  TS-932 - "Error executing SQL."
  TS-934 - "Unable to commit changes to database."
  TS-935 - "Error opening database connection."
  TS-936 - "Error creating database transaction."
  TS-937 - "Unable to commit while voiding a file. File #:
  TS-939 - "Error opening database connection."
  TS-940 - "Error creating database transaction.
  TS-941 - "Error opening database connection."
  TS-942 - "The License for this site is for Summary use only!  You may not add another transaction. File:"
  TS-943 - "The License for this site is for Summary use only! You may not add another transaction. File: " + new_PAYEVENT_FileName
  TS-944 - "Transaction checksum invalid. File: " + Susp_PAYEVENT_FileName + "Tran# " + strSusp_TRANNBR);
  TS-945 - "Unable to find the original transaction. File: " + Susp_PAYEVENT_FileName + "Tran# " + strSusp_TRANNBR);
  TS-946 - "Unable to find the original transaction. File: " + Susp_PAYEVENT_FileName + "Tran# " + strSusp_TRANNBR
  TS-947 - "Unable to find the original transaction. File: " + Susp_PAYEVENT_FileName + "Tran# " + strSusp_TRANNBR);
  TS-949 - "Unable to post transaction." + strErr, "code"
  TS-950 - "Unable to post transaction. " + strErr, "code"
  TS-951 - "Error executing SQL."
  TS-952 - "Unable to commit changes to database."
  TS-953 - "Error executing SQL." + strErr
  TS-954 - "Error opening database connection.", "code"
  TS-955 - "Error creating database transaction."
  TS-956 - "Unable to find file: " + strFileName
  TS-957 - "Unable to void event: " + GetKeyData(geoCoreEvent, "EVENTNBR", false) + " in File: " + strFileName + " Error: " + e.Message);
  TS-958 - "Unable to void event: " + GetKeyData(geoCoreEvent, "EVENTNBR", false) + " in File: " + strFileName + " Error: " + m_sErrMsg);
  TS-959 - "Unable to commit while voiding a file. File #: " + strFileName + "Error: " + strErr) 
  TS-960 - "Error opening database connection."
  TS-961 - "No records found. SQL statement: " + strSQL
  TS-962 - "Please provide a config, connection or the checksum data."
  TS-963 - "Error opening database connection."
  TS-964 - "No records found. SQL statement: " + strSQL
  TS-965 - "Please provide a config, connection or the checksum data."
  TS-966 - "Error opening database connection."
  TS-967 - "No records found. SQL statement: " + strSQL
  TS-968 - "Please provide a config, connection or the checksum data."
  TS-969 - "Error opening database connection."
  TS-970 - "No records found. SQL statement: " + strSQL
  TS-971 - "Please provide a config, connection or the checksum data."
  TS-972 - "Error opening database connection."
  TS-973 - "No records found. SQL statement: " + strSQL
  TS-974 - "Please provide a config, connection or the checksum data."
  TS-975 - "Error opening database connection."
  TS-976 - "No records found. SQL statement: " + strSQL
  TS-977 - "Please provide a config, connection or the checksum data."
  TS-978 - "Error opening database connection."
  TS-979 - "No records found. SQL statement: " + strSQL
  TS-980 - "Please provide a config, connection or the checksum data."
  TS-981 - "Error opening database connection."
  TS-982 - "Error creating database transaction." + strError
  TS-983 - "Please provide a method to connect to the database."
  TS-984 - "No records found. SQL statement: " + strSQL
  TS-985 - "Error executing SQL." + strError
  TS-986 - "Unable to commit changes to database."
  TS-987 - "Error opening database connection."
  TS-988 - "Error creating database transaction." + strError
  TS-989 - "Please provide a method to connect to the database."
  TS-990 - "No records found. SQL statement: " + strSQL
  TS-991 - "Error executing SQL." + strError
  TS-992 - "Unable to commit changes to database."
  TS-993 - "Error opening database connection."
  TS-994 - "Error creating database transaction." + strError
  TS-995 - "Please provide a method to connect to the database."
  TS-996 - "No records found. SQL statement: " + strSQL
  TS-997 - "Error executing SQL." + strError
  TS-998 - "Unable to commit changes to database."
  TS-999 - "Error opening database connection."
  TS-1000 - "Error creating database transaction." + strError
  TS-1001 - "Please provide a method to connect to the database."
  TS-1002 - "No records found. SQL statement: " + strSQL
  TS-1003 - "Error executing SQL." + strError
  TS-1004 - "Unable to commit changes to database."
  TS-1005 - "Error opening database connection."
  TS-1006 - "Error creating database transaction." + strError
  TS-1007 - "Please provide a method to connect to the database."
  TS-1008 - "No records found. SQL statement: " + strSQL
  TS-1009 - "Error executing SQL." + strError
  TS-1010 - "Unable to commit changes to database."
  TS-1011 - "Error opening database connection."
  TS-1012 - "Error creating database transaction." + strError
  TS-1013 - "Please provide a method to connect to the database."
  TS-1014 - "No records found. SQL statement: " + strSQL
  TS-1015 - "Error executing SQL." + strError
  TS-1016 - "Unable to commit changes to database."
  TS-1017 - "Error opening database connection."
  TS-1018 - "Error creating database transaction." + strError
  TS-1019 - "Please provide a method to connect to the database."
  TS-1020 - "No records found. SQL statement: " + strSQL
  TS-1021 - "Error executing SQL." + strError
  TS-1022 - "Unable to commit changes to database."
  TS-1023 - "Error opening database connection."
  TS-1024 - "Please provide a config, connection or the checksum data."
  TS-1025 - "Error opening database connection."
  TS-1026 - "No records found. SQL statement: " + strSQL
  TS-1027 - "Please provide a config, connection or the checksum data."
  TS-1028 - "Error opening database connection."
  TS-1029 - "No records found. SQL statement: " + strSQL
  TS-1030 - "Please provide a config, connection or the checksum data."
  TS-1031 - "Error opening database connection."
  TS-1032 - "Please provide a config, connection or the checksum data."	
  TS-1033 - "Error opening database connection."
  TS-1034 - "No records found. SQL statement: " + strSQL
  TS-1035 - "Please provide a config, connection or the checksum data."
  TS-1036 - "Error opening database connection."
  TS-1037 - "No records found. SQL statement: " + strSQL
  TS-1038 - "Please provide a config, connection or the checksum data."
  TS-1039 - "Error opening database connection."
  TS-1040 - "No records found. SQL statement: " + strSQL
  TS-1041 - "Please provide a config, connection or the checksum data."
  TS-1042 - "Error opening database connection."
  TS-1043 - "Please provide a method to connect to the database."
  TS-1044 - "No records found. SQL statement: " + strSQL
  TS-1045 - "No records found. SQL statement: " + strSQL
  TS-1046 - "No records found. SQL statement: " + strSQL
  TS-1047 - throw CASL_error.create("message", "Filename, description or effective date is required."
  TS-1048 - throw CASL_error.create("message", "Error opening database connection.", "code", "");
  TS-1049 - Error creating database transaction." + strErr);
  TS-1050 - ThrowCASLErrorAndCloseDB("", ref pDBConn_Data, "Invalid checksum for file: " + strFileName);
  TS-1051 - ThrowCASLErrorAndCloseDB("TS-1051", ref pDBConn_Data, "Error executing SQL." + strErr + "SQL: " + strSQL);
  TS-1052 - ThrowCASLErrorAndCloseDB("TS-1052", ref pDBConn_Data, "Unable to set checksum on file: " + strFileName);
  TS-1053 - ThrowCASLErrorAndCloseDB("TS-1053", ref pDBConn_Data, "Unable to commit changes to database." + strErr);
  TS-1054 - throw CASL_error.create("message", "Filename is required.", "code", "");
  TS-1055 - throw CASL_error.create("message", "At least one deposit is required.", "code", "");
  TS-1056 - throw CASL_error.create("message", "Error opening database connection.", "code", "");
  TS-1057 - ThrowCASLErrorAndCloseDB("", ref pDBConn_Data, "Error creating database transaction." + strErr);
  TS-1058 - ThrowCASLErrorAndCloseDB("", ref pDBConn_Data, "Unable to retrieve deposit data for DEPOSITID: " + strDEPOSITID + " and DEPOSITNBR: " + strDEPOSITNBR);
  TS-1059 - ThrowCASLErrorAndCloseDB("", ref pDBConn_Data, "Unable to post consolidated deposit. Error: " + strSQL);
  TS-1060 - ThrowCASLErrorAndCloseDB("", ref pDBConn_Data, "Unable to post consolidated deposit. Error: " + e.Message);
  TS-1061 - ThrowCASLErrorAndCloseDB("", ref pDBConn_Data, "Unable to post consolidated deposit.");
  TS-1062 - ThrowCASLErrorAndCloseDB("", ref pDBConn_Data, "Unable to commit changes to database." + strErr);
  TS-1063 - throw CASL_error.create("message", "Error opening database connection.";
  TS-1064 - ThrowCASLErrorAndCloseDB("", ref pDBConn_Data, "Error creating database transaction."
  TS-1065 - throw CASL_error.create("message", "Error executing SQL." + strErr, "code", "");
  TS-1066 - throw CASL_error.create("message", "Unable to commit changes to database." + strErr
  TS-1067 - "Unable to update deposit record in file with generated slip number "
  //-----------------------------------------------------
  // User LOGIN and ChangePassword errors.
  TS-1067 - Invalid License.
  status - Invalid License
  TS-1068 - No User with that UserID found! (See bug #5194) 
  status - No user with userId
  TS-1069 - Your account is locked due to too many login attempts.
  status  - Account Locked
  TS-1070 - Invalid Password.  (is same as TS-1068) (See bug #5194)
  TS-1071 - Not permited to this department.
  status - No Department
  TS-1072 - Your password must be reset.
  TS-1073 - Unable to find date the Password was last changed
  TS-1074 - Your Password has expired!.
  TS-1075 - Unable to connect to database to update session ID.
  status -Problem connecting to database
  TS-1076 - Error creating database transaction for updating session ID.
  TS-1077 - Unable to update session information to database.
  status - Not Updated
  TS-1078 - Unable to commit session ID changes to database.
  TS-1079 - Your account is locked due to inactive for many days.  bug 5689
  status - Locked for Inactivity
  TS-1080 - CreateErrorObject("TS-1080", strErr, false);
  status - No Last 12 passwords
  TS-1081 - Password must be at least 8 characters long,and contain at least two letters and two numbers.
  status - Invalid Password
  TS-1082 - CreateErrorObject("TS-1082", strErr, false);
  TS-1083 - CreateErrorObject("TS-1083", "Unable to commit data. Error: " + strErr, false);
  TS-1118 - Insert BLOB
  TS-1220 - The License will expire on DATE. Please call CORE Business Technologies for a new License Code.
  status License Expiration
  TS-1248 - PASSWORD EXPIRATION -- must change password
  status - Password Expired
  TS-1254 - Password Expiration Warning
  TS-1255 - Invalid PIN  // Bug 20562 UMN / EOM Add Multi-factor authentication (MFA) using Google Authenticator

  //---------------------------------------------------------
  // User session
  TS-1224 - "Error connecting to database to verify user session."


  //-----------------------------------------------------
  //VoidEventInT3
  TS-1084 - "Error opening database connection.";
  TS-1085 - "Error creating database transaction."
  TS-1086 - "Please provide a method of connecting to the database."
  TS-1087 - "You must provide either (EventNbr AND FileNbr) or (SourceType, SourceGroup, AND SourceRefID)."
  TS-1088 - "Transaction File " + strFileNbr + " is not Active!  No changes are permitted."	
  TS-1090 - "No records found. SQL statement: " + strSQL
  TS-1092 - "No records found. SQL statement: " + strSQL
  TS-1093 - "Error executing SQL." + strError
  TS-1094 - "No available TG_PAYEVENT_DATA."		
  TS-1095 - "Error executing SQL." + strError
  TS-1096 - "Unable to commit changes to database. " + strError
  //VoidTender
  TS-1097 - "Error opening database connection."
  TS-1098 - "Error creating database transaction."
  TS-1099 - "Must provide a method of connecting to the database."
  TS-1100 - "No records found."
  TS-1101 - string.Format("Invalid Check Sum for Tender Data Record. TNDRNBR: {0} EVENTNBR: {1} DEPFILENBR: {2} DEPFILESEQ: {3}", strTndrNbr, strEventNbr, strFileNbr, strFileSequenceNbr)		"Error executing SQL. SQL statement: " + strSQL
  TS-1102 - "Error executing SQL. SQL statement: " + strSQL
  TS-1103 - "Unable to commit changes to database. " + strError
  //VoidTransaction
  TS-1104 - "Error opening database connection."
  TS-1105 - "Error creating database transaction."
  TS-1106 - "Must provide a method of connecting to the database."
  TS-1107 - "No records found."
  TS-1108 - string.Format("Invalid Check Sum for Transaction Record. TRANNBR: {0} EVENTNBR: {1} DEPFILENBR: {2} DEPFILESEQ: {3}", strTranNbr, strEventNbr, strFileNbr, strFileSequenceNbr)
  TS-1109 - string.Format("Invalid Check Sum for Item Data Record. TRANNBR: {0} EVENTNBR: {1} DEPFILENBR: {2} DEPFILESEQ: {3}", strTranNbr, strEventNbr, strFileNbr, strFileSequenceNbr)
  TS-1110 - string.Format("Invalid Check Sum for Cust Field Data Record. TRANNBR: {0} EVENTNBR: {1} DEPFILENBR: {2} DEPFILESEQ: {3}", strTranNbr, strEventNbr, strFileNbr, strFileSequenceNbr)
  TS-1111 - "Error executing SQL. SQL statement: " + strSQL
  TS-1112 - "Unable to commit changes to database. " + strError

  // Update BANKACCTNBRID
  TS-1114 - throw CASL_error.create("message", "Error opening database connection.", "code", "TS-1114");
  TS-1115 - throw CASL_error.create("message", "Error creating database transaction." + strError, "code", "TS-1115");
  TS-1116 - throw CASL_error.create("message", "Error executing SQL." + strError, "code", "TS-1116");
  TS-1117 - throw CASL_error.create("message", "Unable to update BANCACCTID in TENDERS tabale.", "code", "TS-1117");

  TS-1119 - throw CASL_error.create("message", "This is not the active session. Logout, then log back in.", "code", "TS-1119");

  //Mark file as closed/unclosed
  TS-1200 - "Transaction File XXXXXXXX is not Active!"
  TS-1201 - "Unable to set file to locked. File: XXXXXXXX " 
  TS-1202 - "Required information missing in Change File Status!"
  TS-1203 - "Unable to set file to unlocked. File: XXXXXXXX "
  TS-1204 - "Transaction File XXXXXXXX has been locked!  You must log into a new file. "	

  //Mark file as accepted/unaccepted
  TS-1210 - "Transaction File XXXXXXXX is not Balanced!"
  TS-1211 - "Unable to set file to accepted. File: XXXXXXXX " 
  TS-1213 - "Unable to set file to unaccepted. File: XXXXXXXX "

  // Create new core event
  TS-1221 - "Unable to create new core event. File: XXXXXX"

  // Activate Suspended Event
  TS-1222 - "Unable to connect to database. " + error_message
  status - Unable to connect to database
  TS-1223 - "Error retrieving file source information"
  TS-1225 - "Suspended event already activated."
  status - suspended event already activated
  TS-1226 - "Unable to determine resume event."
  status - Unable to detarmine resume event
  TS-1234 - "Error creating SQL. " + error_message
  status - Error Creating SQL
  TS-1235 - "Unable to update new event. " + error_message
  status -Unable to update new event

  // Creating checksums
  TS-1227 - "Unable to connect to database. " + error_message
  TS-1228 - "Could not find transaction TRANNBR in event FILENAME-EVENTNBR."

  // Get Transaction
  TS-1229 - "Unable to connect to database. " + error_message
  TS-1230 - "Could not find transaction TRANNBR in event FILENAME-EVENTNBR."
  TS-1231 - "More than one transaction found for transaction number TRANNBR in event FILENAME-EVENTNBR."
  
  // Get Items
  TS-1232 - "Unable to connect to database. " + error_message
  // Get Custom Fields
  TS-1233 - "Unable to connect to database. " + error_message

  // Void Event
  TS-1236 - "Error Voiding Event." + message
  TS-1237 - "Could not validate session id."

  // Consolidate Deposits
  TS-1238 - "Could not validate session id."
  TS-1239 - "Error Consolidating Deposits"
  TS-1240 - "Unable to create new deposit. Deposit: " + deposit_id + ". " + error_message

  // Balance Files
  TS-1241 - "This is not the active session. Log out, then log back in."
  TS-1242 - "Could not validate checksum for file {0}{1}" DEPFILENBR, DEPFILESEQ
  TS-1243 - "Could not update file."

  // Acquire File
  TS-1244 - "Could not acquire file."
  // Release File
  TS-1245 - "Could not release file."
  // Activate File
  TS-1246 - "Could not update file."  
  TS-1247 - "Could not access file data."

 * GetItemData
 * TS-1249 - "Could not create DB reader"
 * TS-1250 - "Invalid where clause"
 * 
 * 
  // File Import 
  TS-1249 - "Error opening database connection"
  TS-1250 - "Error creating database transaction."
  TS-1251 - "Unable to commit changes to database."
 * 
 * 
 TS-1253 - Unable to update image to database.
 * 
 * 
 // Reversal
  TS-1256 - Error creating database transaction
 TS-1258 - Unable to post Reversal Tran Data
 TS-1259 - Unable to post Reversal Tran Data because of error in ExecuteNonQuery
 *
 *
  TS-1260 - Database Batch Insert Error
 *
 *
  TS-1261 - *NEXT AVAILABLE*
 
*/

namespace TranSuiteServices
{

  public class PreDepositTender
  {
    // Daniel wrote this class. Please do not change without talking to me first.
    // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

    private struct Tender
    {
      public string strDEPOSITID;
      public long lDEPOSITNBR;
      public long lSEQNBR;
      public string strTNDRPROP;
      public string strTNDR;
      public string strTNDRDESC;
      public double dAMOUNT;
      public string strSOURCE_REFID;
      public string strSOURCE_DATE;

      public void init()
      {
        // Daniel wrote this function. Please do not change without talking to me first.
        // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

        this.strDEPOSITID = "";
        this.lDEPOSITNBR = 0;
        this.lSEQNBR = 0;
        this.strTNDRPROP = "";
        this.strTNDR = "";
        this.strTNDRDESC = "";
        this.dAMOUNT = 0.00;
        this.strSOURCE_REFID = "";
        this.strSOURCE_DATE = "";
      }
    }

    // This is a self consolidating struct.
    public ArrayList m_TenderList = null;

    // Bug 10394 UMN removed empty destructor

    public PreDepositTender()
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      m_TenderList = new ArrayList();
    }

    public GenericObject ReturnInGEOFormat()
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      GenericObject geoList = new GenericObject();

      for (int i = 0; i < m_TenderList.Count; i++)
      {
        GenericObject geoElement = new GenericObject();
        Tender tmpTender = (Tender)m_TenderList[i];

        geoElement.set("DEPOSITID", tmpTender.strDEPOSITID);// One will be assigned later in PostDepositToT3
        geoElement.set("DEPOSITNBR", tmpTender.lDEPOSITNBR);// One will be assigned later in PostDepositToT3
        geoElement.set("SEQNBR", tmpTender.lSEQNBR);
        geoElement.set("TNDRPROP", tmpTender.strTNDRPROP);
        geoElement.set("TNDR", tmpTender.strTNDR);
        geoElement.set("TNDRDESC", tmpTender.strTNDRDESC);
        geoElement.set("AMOUNT", tmpTender.dAMOUNT);
        geoElement.set("SOURCE_REFID", tmpTender.strSOURCE_REFID);
        geoElement.set("SOURCE_DATE", tmpTender.strSOURCE_DATE);

        geoList.insert(geoElement);
      }

      return geoList;
    }

    public void Add(string strDEPOSITID,
      long lDEPOSITNBR,
      long lSEQNBR,
      string strTNDRPROP,
      string strTNDR,
      string strTNDRDESC,
      double dAMOUNT,
      string strSOURCE_REFID,
      string strSOURCE_DATE)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      Tender pNewTender = new Tender();
      pNewTender.init();

      //----------------------------------------
      // Set data
      pNewTender.strDEPOSITID = strDEPOSITID;
      pNewTender.lDEPOSITNBR = lDEPOSITNBR;
      pNewTender.lSEQNBR = lSEQNBR;
      pNewTender.strTNDRPROP = strTNDRPROP;
      pNewTender.strTNDR = strTNDR;
      pNewTender.strTNDRDESC = strTNDRDESC;
      pNewTender.dAMOUNT = dAMOUNT;
      pNewTender.strSOURCE_REFID = strSOURCE_REFID;
      pNewTender.strSOURCE_DATE = strSOURCE_DATE;
      //----------------------------------------

      //----------------------------------------
      // Consalidate records already in arr.
      if (m_TenderList.Count == 0)// First record.
        m_TenderList.Add(pNewTender);
      else
      {
        bool bAddNewTenderType = true;// If no match found in list.

        for (int i = 0; i < m_TenderList.Count; i++)
        {
          Tender tmpTender = (Tender)m_TenderList[i];
          // Update existing tender
          if (pNewTender.strTNDRPROP == tmpTender.strTNDRPROP
            && pNewTender.strTNDR == tmpTender.strTNDR)
          {
            tmpTender.dAMOUNT += pNewTender.dAMOUNT;
            m_TenderList[i] = tmpTender;
            bAddNewTenderType = false;
          }
        }
        if (bAddNewTenderType)// No match found. Add this new type to list.
          m_TenderList.Add(pNewTender);
      }
      //----------------------------------------
    }
  }


  /// <summary>
  /// TranSuiteServices is the static method container for database interaction
  /// </summary>
  public static class TranSuiteServices// Bug# 27153 DH - Clean up warnings when building ipayment
  {
    private static String m_sErrMsg = "";
    // BEGIN TTS 20649 EOM - Provide generic message for failed login. 
    // Changed middle sentence from "Please enter a valid Username and Password" to "Please enter valid user credentials" to accomodate use of MFA PIN
    private const String TS1068_invalid_user_id = "Invalid Sign In attempt. Please enter valid user credentials. Make sure the Caps Lock is off and try again."; // TS-1068
    private const String TS1070_invalid_password = "Invalid Sign In attempt. Please enter valid user credentials. Make sure the Caps Lock is off and try again.";  //TS-1070
    private const String TS1255_invalid_PIN = "Invalid Sign In attempt. Please enter valid user credentials. Make sure the Caps Lock is off and try again.";  //TS-1255
    private const String TS1079_user_locked = "Invalid Sign In attempt. Please enter valid user credentials. Make sure the Caps Lock is off and try again.";  //TS-1079
    private const String TS1069_too_many_failed = "Your account is locked due to too many login attempts.<br>Please call your system administrator."; //TS-1069
    // END TTS 20649 EOM - Provide generic message for failed login. 
    private const String TS1070_invalid_password_LDAP = "Invalid Sign In attempt. Either the username is incorrect or the password is wrong or expired.  Please enter a valid Username and Password. Make sure the Caps Lock is off and try again.";  //TS-1070
    private const String TS1253_invalid_user_id_LDAP_OK = "Invalid Sign In attempt. User credentials were verified against LDAP, but the user is not found in the iPayment database."; // TS-1253

    // Bug 20804 UMN don't trust config to return ints, decimals etc. TryParse them!
    // Note: can't use a generic like public static T ConfigParse<T>(string container_path, string field_key, T default_val) where T : int, decimal
    // because C# doesn't have some of the template capabilities of C++ (surprise). So trying T.tryParse isn't understood.
    // I could try reflection, but am worried about the performance. So (sigh), provide generic specializations.
    public static int ConfigParse(string container_path, string field_key, int default_val)
    {
      int retVal = default_val;
      try
      {
        retVal = (int) (c_CASL.c_object(container_path) as GenericObject).get(field_key, default_val);
        return retVal;
      }
      catch (Exception e)
      {      
        string err = String.Format("Invalid config data for {0}.{1}. Using default of {2}.", container_path, field_key, default_val);
        Logger.LogWarn(err, "");
        // Bug 17849
        Logger.cs_log_trace("Error ConfigParse", e);
        return default_val;
      }
    }
    // END Bug 20804 UMN don't trust config to return ints, decimals etc. TryParse them!

    public static object CASLChangePassword(params object[] args)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!
      //#region Setting arguments
      GenericObject geo_args = misc.convert_args(args);
      String strUserID = geo_args.get("username") as string;
      String sNewPassword = geo_args.get("new_password") as string;
      String sOldPassword = geo_args.get("old_password") as string;
      String sApp = geo_args.get("app", "authenticator") as string; // Bug #21189 EOM Failed credential change attempts are not in the activity log
      string strErr = "";
      string strUserCurrentlySetPassword = "";
      int iLastFailedAttempts = 0;

      // Bug 20804 UMN don't trust config to return ints, decimals etc. TryParse them!
      int iPasswordsToCheck = ConfigParse("Business.Misc.data", "previous_password_count", 12);

      GenericObject geoLastPasswords = null;
      GenericObject geoUser = null;
      GenericObject geoUsers = (GenericObject)geo_args.get("all_users");
      GenericObject database = geo_args.get("config") as GenericObject;

      // BEGIN Bug 20562 EOM Add Multi-factor authentication (MFA) pin
      string pin = (string)geo_args.get("mfa_token");
      
      //check if the password is from the link and
      Boolean bLinkPass = (Boolean)geo_args.get("link_pass", false);
      // END Bug 20562 EOM Add Multi-factor authentication (MFA) pin

      //#endregion
      //#region Checking for existence of user
      if (geoUsers.has(strUserID))
      {
        geoUser = geoUsers.get(strUserID) as GenericObject;
        geoLastPasswords = (GenericObject)geoUser.get("previous_passwords", c_CASL.c_GEO());// BUG4495
        strUserCurrentlySetPassword = (string)geoUser.get("password");
      }
      else

        return CreateErrorObject("TS-1068", TS1068_invalid_user_id, false, "No user with userId", true); //Bug 11329 NJ-always show verbose

      //#endregion
      //#region Make sure password is in correct format
      if (!CheckPasswordFormat(sNewPassword))
      {
        //Bug 13846-include special chars info in message
        // Bug #21189 EOM Failed credential change attempts are not in the activity log
        LogToActivityLog(sApp, strUserID, "Change Password", "Invalid new password - wrong format");
        strErr = "Password must be at least 8 characters long, contain at least two letters and two numbers. Special characters are allowed except & < >.";
        return CreateErrorObject("TS-1081", strErr, false, "Invalid Password", true); //Bug 11329 NJ-always show verbose
      }
      //#endregion
      //#region Check failed attempts

      iLastFailedAttempts = GetFailedAttemptCount(geoUser, database);
      if (iLastFailedAttempts >= 3)
      {
        // TTS 20649 EOM - Provide generic message
        return CreateErrorObject("TS-1069", TS1069_too_many_failed, false, "Account Locked", true);//Bug 11329 NJ-always show verbose
      }
      //#endregion
      //#region Hash the password    
      string strSalt = strUserID;
      string strNewPassword = Crypto.CASLComputeHash("data", sNewPassword, "salt", strSalt);
      string strOldPassword = Crypto.CASLComputeHash("data", sOldPassword, "salt", strSalt);
      strNewPassword = misc.StringToBase64String(strNewPassword);
      strOldPassword = misc.StringToBase64String(strOldPassword);

      // Bug 20562 EOM Add Multi-factor authentication (MFA) pin
      // set it back to the raw password since it was encoded in the link
      if (bLinkPass) strOldPassword = sOldPassword;

      //#endregion
      //#region On mismatch update failed attempts
      if (strOldPassword != strUserCurrentlySetPassword)
      {
        // Update failed count in logical and physical models
        IncFailedAttemptCount(geoUser, database);
        geoUser.set("count_of_failed_attempts", GetFailedAttemptCount(geoUser, database));
        // Bug #8129 Mike O - Corrected error message
        return CreateErrorObject("TS-1068", TS1070_invalid_password, false, "Incorrect password", true);//Bug 11329 NJ-always show verbose
      }
      //#endregion

      // BEGIN Bug 20562 EOM Add Multi-factor authentication (MFA) 
      if (!String.IsNullOrWhiteSpace(pin)) // Config only uses pin.  All other will have empty string.
      {
          try
          {
              // get the user's secret key
              string user_key = geoUser.get("mfa_user_key") as String;

              //Decrypt user key
              byte[] bUserKeyEncrypted = Convert.FromBase64String(user_key);
              byte[] bUserKeyDecrypted = Crypto.symmetric_decrypt("data", bUserKeyEncrypted, "secret_key", Crypto.get_secret_key());
              user_key = System.Text.Encoding.Default.GetString(bUserKeyDecrypted);

              TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();

              // Validate the PIN, but override library's default clock drift of 5 minutes. 
              // Use clock drift tolerance of +/- 30 seconds instead.
              bool pinValid = tfa.ValidateTwoFactorPIN(user_key, pin, TimeSpan.FromSeconds(30));
              if (!pinValid)
              {
                  // Update failed count in logical and physical models
                  IncFailedAttemptCount(geoUser, database);
                  geoUser.set("count_of_failed_attempts", GetFailedAttemptCount(geoUser, database));
                  return CreateErrorObject("TS-1255", TS1255_invalid_PIN, false, "Invalid PIN", true);
              }
          }
          catch (Exception ex)
          {
          // Bug 17849
          Logger.cs_log_trace("Error CASLChangePassword", ex);
              return CreateErrorObject("TS-1255", ex.Message, false, "Invalid PIN", true);
          }
      }
      // END Bug 20562 EOM Add Multi-factor authentication (MFA)   
      
      //#region If this is the initial password, add it into geoLastPasswords
      // Bug #8126 Mike O
      if (geoLastPasswords.getIntKeyLength() == 0)
      {
        geoLastPasswords.vectors.Insert(0, strOldPassword);
      }
      // End Bug #8126
      //#endregion
   
      //#region Check new password against last n passwords.
      //bug 5689

      int iCountOfPasswords = geoLastPasswords.getIntKeyLength();

      // Bug #8125 Mike O - Changed static 12 previous passwords to be parameterized
      if (iPasswordsToCheck < 4)
        iPasswordsToCheck = 4;
      string previousPassword;

      for (int i = 0; i < iCountOfPasswords && i < iPasswordsToCheck + 1; i++)
      {
        previousPassword = (string)geoLastPasswords.get(i);

        if (previousPassword == strNewPassword)
        {
            // Bug #21189 EOM Failed credential change attempts are not in the activity log
            LogToActivityLog(sApp, strUserID, "Change Password", "Invalid new password - one of last " + iPasswordsToCheck);
            return CreateErrorObject("TS-1080", "Invalid Password<br/>You cannot use one of the last " + iPasswordsToCheck + " passwords.",
              false, "No Last " + iPasswordsToCheck + " Passwords", true);//Bug 11329 NJ-always show verbose
        }
      }

      geoLastPasswords.vectors.Insert(0, strNewPassword);
      if (geoLastPasswords.vectors.Count > iPasswordsToCheck)
      {
        geoLastPasswords.vectors.RemoveRange(iPasswordsToCheck, geoLastPasswords.vectors.Count - iPasswordsToCheck);
      }
      // End Bug #8125
      //#endregion
      //#region Update user's old password to the new password.
      geoUser.set("previous_passwords", geoLastPasswords);
      geoUser.set("count_of_failed_attempts", 0);
      ResetFailedAttemptCount(geoUser, database);
      geoUser.set("password", strNewPassword);
      geoUser.set("password_last_changed_on", DateTime.Now.ToShortDateString());
      geoUser.set("must_change_password", false);
      geoUsers.set(strUserID, geoUser); // Probably unnecessary
      //#endregion
      //#region Freeze the user and report success
      
      // Bug 25984 UMN delete revision
      misc.CASL_call("a_method", "freeze", "_subject", geoUser, "args", new GenericObject());

      return true;
      //#endregion
    }

    // BEGIN Bug 20562 EOM Add Multi-factor authentication (MFA)
    //Called when user tries to change PIN from the Config->Login started by email link
    public static object CASLChangePIN(params object[] args)
    {
        //#region Setting arguments
        GenericObject geo_args = misc.convert_args(args);
        String strUserID = geo_args.get("username") as string;
        String sPassword = geo_args.get("password") as string;
        //String sApp = geo_args.get("app", "authenticator") as string; // Bug #21189 EOM Failed credential change attempts are not in the activity log
        string strUserCurrentlySetPassword = "";
        int iLastFailedAttempts = 0;
        GenericObject geoUser = null;
        GenericObject geoUsers = (GenericObject)geo_args.get("all_users");
        GenericObject database = geo_args.get("config") as GenericObject;
        GenericObject newMFAInfo = null;
        string pin = (string)geo_args.get("mfa_token");

        //check if there password is from the link and      
        String sUserKey = (String)geo_args.get("user_key", "");
        
        //#endregion
        //#region Checking for existence of user
        if (geoUsers.has(strUserID))
        {
            geoUser = geoUsers.get(strUserID) as GenericObject;
            if (geoUser.has("password"))
                strUserCurrentlySetPassword = (string)geoUser.get("password");
            else
            {
                // Bug #21189 EOM Failed credential change attempts are not in the activity log
                //LogToActivityLog(sApp, strUserID, "Change PIN", "Incorrect password");
                return CreateErrorObject("TS-1068", TS1070_invalid_password, false, "Incorrect password", true);
            }
        }
        else
        {
            // Bug #21189 EOM Failed credential change attempts are not in the activity log
            //LogToActivityLog(sApp, strUserID, "Change PIN", "No user with userId");
            return CreateErrorObject("TS-1068", TS1068_invalid_user_id, false, "No user with userId", true); //Bug 11329 NJ-always show verbose
        }
        //#endregion
        
        //#region Check failed attempts

        iLastFailedAttempts = GetFailedAttemptCount(geoUser, database);
        if (iLastFailedAttempts >= 3)
        {
            // Bug #21189 EOM Failed credential change attempts are not in the activity log
            //LogToActivityLog(sApp, strUserID, "Change PIN", "Account Locked");
            // TTS 20649 EOM - Provide generic message
            return CreateErrorObject("TS-1069", TS1069_too_many_failed, false, "Account Locked", true);//Bug 11329 NJ-always show verbose
        }
        //#endregion
        
        //#region Hash the password    
        string strSalt = strUserID;
        string strPassword = Crypto.CASLComputeHash("data", sPassword, "salt", strSalt);

        strPassword = misc.StringToBase64String(strPassword);

        //#region On mismatch update failed attempts
        if (strPassword != strUserCurrentlySetPassword)
        {
            // if this is password and pin change, the passed in password is already encoded
            if (sPassword != strUserCurrentlySetPassword)
            {
                // Update failed count in logical and physical models
                IncFailedAttemptCount(geoUser, database);
                geoUser.set("count_of_failed_attempts", GetFailedAttemptCount(geoUser, database));

                // Bug #21189 EOM Failed credential change attempts are not in the activity log
                // LogToActivityLog(sApp, strUserID, "Change PIN", "Incorrect password");
                return CreateErrorObject("TS-1068", TS1070_invalid_password, false, "Incorrect password", true);//Bug 11329 NJ-always show verbose
            }
        }
        //#endregion
        
        try
        {
            // get the user's secret key
            string user_key = geoUser.get("mfa_user_key") as String;
                
            // Check if user key is passed in, if so, just compare them
            if (sUserKey != "")
            {
                if (sUserKey != user_key)
                {
                    // Bug #21189 EOM Failed credential change attempts are not in the activity log
                    // LogToActivityLog(sApp, strUserID, "Change PIN", "Invalid PIN");
                    return CreateErrorObject("TS-1255", TS1255_invalid_PIN, false, "Invalid PIN", true);
                }
            }
            else
            {
                //Decrypt user key
                byte[] bUserKeyEncrypted = Convert.FromBase64String(user_key);
                byte[] bUserKeyDecrypted = Crypto.symmetric_decrypt("data", bUserKeyEncrypted, "secret_key", Crypto.get_secret_key());
                user_key = System.Text.Encoding.Default.GetString(bUserKeyDecrypted);
                    
                TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();

                // Validate the PIN, but override library's default clock drift of 5 minutes. 
                // Use clock drift tolerance of +/- 30 seconds instead.
                bool pinValid = tfa.ValidateTwoFactorPIN(user_key, pin, TimeSpan.FromSeconds(30));
                if (!pinValid)
                {
                    // Update failed count in logical and physical models
                    IncFailedAttemptCount(geoUser, database);
                    geoUser.set("count_of_failed_attempts", GetFailedAttemptCount(geoUser, database));
                    // Bug #21189 EOM Failed credential change attempts are not in the activity log
                    //LogToActivityLog(sApp, strUserID, "Change PIN", "Invalid PIN");
                    return CreateErrorObject("TS-1255", TS1255_invalid_PIN, false, "Invalid PIN", true);
                }
            }
        }
        catch (Exception ex)
        {
        // Bug 17849
        Logger.cs_log_trace("Error CASLChangePIN", ex);
            return CreateErrorObject("TS-1255", ex.Message, false, "Invalid PIN", true);
        }

        // Reset the login attempts for the user
        geoUser.set("count_of_failed_attempts", 0);
        ResetFailedAttemptCount(geoUser, database);

        //Generate the new MFA key
        newMFAInfo = TwoFactorAuthenticator.CASLGenerateMFAInfo("user_id", strUserID);

        // Set the new values 
        geoUser.set("mfa_user_key", newMFAInfo.get("mfa_user_key"));
        //geoUser.set("mfa_manual_entry_code", newMFAInfo.get("mfa_manual_entry_code"));
        

        // Freeze the user
        // Bug 25984 UMN delete revision
        misc.CASL_call("a_method", "freeze", "_subject", geoUser, "args", new GenericObject());

        //do this after freeze - dont need this data in DB.
        geoUser.set("mfa_qr_code_bytes", newMFAInfo.get("mfa_qr_code_bytes"));
        geoUser.set("mfa_manual_entry_code", newMFAInfo.get("mfa_manual_entry_code"));
        
        return true;
    }

    //Called when user clicks the "Change PIN" button in Config->User
    public static object CASLChangePINValidated(params object[] args)
    {
        //#region Setting arguments
        GenericObject geo_args = misc.convert_args(args);
        String strUserID = geo_args.get("username") as string;
        GenericObject geoUser = null;
        GenericObject newMFAInfo = null;
        GenericObject geoUsers = (GenericObject)geo_args.get("all_users");
        geoUser = geoUsers.get(strUserID) as GenericObject;
        GenericObject database = geo_args.get("config") as GenericObject;

        //Generate the new MFA key
        newMFAInfo = TwoFactorAuthenticator.CASLGenerateMFAInfo("user_id", strUserID);

        // Set the new values 
        geoUser.set("mfa_user_key", newMFAInfo.get("mfa_user_key"));
        //geoUser.set("mfa_manual_entry_code", newMFAInfo.get("mfa_manual_entry_code"));

        // Reset the login attempts for the user
        geoUser.set("count_of_failed_attempts", 0);
        ResetFailedAttemptCount(geoUser, database);

        // Freeze the user
        // Bug 25984 UMN delete revision
        misc.CASL_call("a_method", "freeze", "_subject", geoUser, "args", new GenericObject());

        // BEGIN Bug #21189 EOM Failed credential change attempts are not in the activity log
        string application_name = "Config";
        if (CASLInterpreter.get_my() != null && CASLInterpreter.get_my().has("application_name"))
            application_name = CASLInterpreter.get_my()["application_name"] as string;

        string login_user = (CASLInterpreter.get_my() as GenericObject).get("login_user", "") as string;

        // Write to the activity log
        misc.CASL_call("a_method", "actlog", "args",
          new GenericObject(
            // Bug 20435 UMN PCI-DSS don't pass session
            "user", login_user,
            "app", application_name,
            "action", "PIN Change",
            "summary", "Changed User PIN",
            "detail", "User:" + strUserID));
        // END Bug #21189 EOM Failed credential change attempts are not in the activity log

        if (MFA_Should_Send_Email("geoUser", geoUser))
        {
            if (geoUser.has("user_email"))
                // Send the email,
                emails.CASL_email_login("to", (string)geoUser.get("user_email"),
                    "from", "no-reply@ipayment.com",
                    "user_name", (string)geoUser.get("name"),
                    "change_pin", "true",
                    "change_pwd", "false",
                    "user_pwd", "",
                    "mfa_user_key", (string)geoUser.get("mfa_user_key"),
                    "user_id", (string)geoUser.get("id"));
            else
                return CreateErrorObject("user_email_missing", "Failed to send email.  User email is not specified", false, "User email missing", true);
        }
        return true;
    }

    //Called when user clicks the "Change PIN and Password" button in Config->User
    public static object CASLChangePINandPWD(params object[] args)
    {
        //#region Setting arguments
        GenericObject geo_args = misc.convert_args(args);
        String strUserID = geo_args.get("username") as string;
        GenericObject geoUser = null;
        GenericObject newMFAInfo = null;
        GenericObject geoUsers = (GenericObject)geo_args.get("all_users");
        geoUser = geoUsers.get(strUserID) as GenericObject;
        GenericObject database = geo_args.get("config") as GenericObject;

        //Generate the new MFA key
        newMFAInfo = TwoFactorAuthenticator.CASLGenerateMFAInfo("user_id", strUserID);

        // Set the new values 
        geoUser.set("mfa_user_key", newMFAInfo.get("mfa_user_key"));
        geoUser.set("password", newMFAInfo.get("mfa_user_key"));
        //geoUser.set("mfa_manual_entry_code", newMFAInfo.get("mfa_manual_entry_code"));
        
        // Reset the login attempts for the user
        geoUser.set("count_of_failed_attempts", 0);
        ResetFailedAttemptCount(geoUser, database);

        // Freeze the user
        // Bug 25984 UMN delete revision
        misc.CASL_call("a_method", "freeze", "_subject", geoUser, "args", new GenericObject());

        // BEGIN Bug #21189 EOM Failed credential change attempts are not in the activity log
        string application_name = "Config";
        if (CASLInterpreter.get_my() != null && CASLInterpreter.get_my().has("application_name"))
            application_name = CASLInterpreter.get_my()["application_name"] as string;

        string login_user = (CASLInterpreter.get_my() as GenericObject).get("login_user", "") as string;

        // Write to the activity log
        misc.CASL_call("a_method", "actlog", "args",
          new GenericObject(
            // Bug 20435 UMN PCI-DSS don't pass session
            "user", login_user,
            "app", application_name,
            "action", "PIN and Pwd Change",
            "summary", "Changed User PIN and Pwd",
            "detail", "User:" + strUserID));
        // END Bug #21189 EOM Failed credential change attempts are not in the activity log

        if (MFA_Should_Send_Email("geoUser", geoUser))
        {
            // Send the email,
            if (geoUser.has("user_email"))
                emails.CASL_email_login("to", (string)geoUser.get("user_email"),
                    "from", "no-reply@ipayment.com",
                    "user_name", (string)geoUser.get("name"),
                    "change_pin", "true",
                    "change_pwd", "true",
                    "user_pwd", (string)geoUser.get("mfa_user_key"),
                    "mfa_user_key", (string)geoUser.get("mfa_user_key"),
                    "user_id", (string)geoUser.get("id"));
            else
                return CreateErrorObject("user_email_missing", "Failed to send email.  User email is not specified", false, "User email missing", true);
        }

        return true;
    }
    // END Bug 20562 EOM Add Multi-factor authentication (MFA)

    // BEGIN Bug #21189 EOM Failed credential change attempts are not in the activity log
    public static void LogToActivityLog(string application_name, string strUserID, string action, string summary)
    {
        // Write to the activity log
        misc.CASL_call("a_method", "actlog", "args",
          new GenericObject(
            // Bug 20435 UMN PCI-DSS don't pass session
            "user", strUserID,
            "app", application_name,
            "action", action,
            "summary", summary,
            "detail", "User:" + strUserID));
    }

    /// <summary>
    /// Generic way to log to the Activit Log
    /// Bug 22343
    /// </summary>
    /// <param name="application_name"></param>
    /// <param name="action"></param>
    /// <param name="summary"></param>
    /// <param name="details"></param>
    public static void LogToActivityLogWithDetails(string application_name, string action, string summary, string details)
    {
        try
        {
            string login_user = (CASLInterpreter.get_my() as GenericObject).get("login_user", "") as string;

            // Write to the activity log
            misc.CASL_call("a_method", "actlog", "args",
              new GenericObject(
                // Bug 20435 UMN PCI-DSS don't pass session
                "user", login_user,
                "app", application_name,
                "action", action,
                "summary", summary,
                "detail", details));
        }
        catch (Exception e)
        {
        /*Logger.cs_log(
                string.Format("Cannot write to activity log: {0}. action: {1}, summary: {2}, detail {3}",
                    e.Message, action, summary, details)
                );*/
        // Bug 17849
        Logger.cs_log_trace("Error Write to activity log", e);
        }
    }
    // END Bug #21189 EOM Failed credential change attempts are not in the activity log

    public static string RemoveAllChars(char c, string strFromStr)
    {
      // Daniel added. Please do not change without talking to me first.
      // I need this for database.

      // Description: Removes all occurances of char c in string strFromStr.
      // A replace on a string takes 1/10th the time of building a new string but concatenating strings.
      return strFromStr.Replace(c.ToString(), "");
    }

    public static string CASLCreateLicenseChecksum(params object[] args)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      GenericObject geo_args = misc.convert_args(args);

      string strStartDate = GetKeyData(geo_args, "start_date", true);
      string strEndDate = GetKeyData(geo_args, "end_date", true);
      // Bug #12055 Mike O - Use misc.Parse to log errors
      int    iMaximumDepartments   = misc.Parse<Int32>(GetKeyData(geo_args, "maximum_departments", true));
      string strCustomerName = GetKeyData(geo_args, "customer_name", true);
      string strOneEventPerFile = GetKeyData(geo_args, "one_event_per_file", true);

      // Bug #12055 Mike O - Use misc.Parse to log errors      
      DateTime dtStartDate         = misc.Parse<DateTime>(strStartDate);
      DateTime dtEndDate           = misc.Parse<DateTime>(strEndDate);

      bool bLicUse = strOneEventPerFile == "True";//(strOneEventPerFile == "True" || strOneEventPerFile == "False");

      return CreateLicenseChecksum(dtStartDate, dtEndDate, iMaximumDepartments, strCustomerName, bLicUse);
    }
    public static string CreateLicenseChecksum(DateTime startDate, DateTime endDate, int maxDepartments, string customerName, bool oneEventPerFile)
    {
      StringBuilder licenseStringBuilder = new StringBuilder();
      licenseStringBuilder.Append(customerName.ToUpper());
      licenseStringBuilder.Append(RemoveAllChars('/', startDate.ToString("d", DateTimeFormatInfo.InvariantInfo)));
      licenseStringBuilder.Append(RemoveAllChars('/', endDate.ToString("d", DateTimeFormatInfo.InvariantInfo)));
      licenseStringBuilder.AppendFormat("{0:000000}", maxDepartments);
      licenseStringBuilder.Append((oneEventPerFile) ? "S" : "D");
      // Bug #10955 Mike O
      return misc.StringToBase64String(Crypto.ComputeHash("", licenseStringBuilder.ToString()));
    }

    // Bug #6467 Mike O - Created to resolve ambiguities/difficulties when calling from CASL
    public static GenericObject CASLValidateLicense(params Object[] args)
    {
      GenericObject error = new GenericObject();
      string message = "";
      ValidateLicense(ref message, c_CASL.c_object("License.data") as GenericObject);
      if (message.Length > 0)
        error.set("message", message);
      return error;
    }

    public static bool ValidateLicense(ref string error, GenericObject license)
    {
      int department_count = 0;
      GenericObject departments = c_CASL.c_object("Department.of") as GenericObject;
      department_count = departments.getStringKeys().Count;
      try
      {
        //#region Fail for no license
        if (license == null)
        {
          error = "No License is configured.\nPlease contact CORE Business Technologies for a license code.";
          return false;
        }
        //#endregion
        //#region Get parameters
        // Bug #12055 Mike O - Use misc.Parse to log errors
        DateTime startDate=misc.Parse<DateTime>(license.get("start_date") as string);
        DateTime endDate=misc.Parse<DateTime>(license.get("end_date") as string);
        int maxDepts = (int)license.get("maximum_departments");
        string customerName = (string)license.get("customer_name");
        bool isSummaryMode = (bool)license.get("one_event_per_file");
        //#endregion
        //#region Calculate and test checksum string
        string checksumValue = CreateLicenseChecksum(startDate, endDate, maxDepts, customerName, isSummaryMode);
        if (checksumValue != (string)license.get("license_key"))
        {
          // Bug 15051 MJO - Added customer name to message
          error = string.Format("The License in the Database, issued to {0}, is invalid.\nPlease contact CORE Business Technologies for a valid license code.",
            customerName);
          return false;
        }
        //#endregion
        //#region Test date
        // Check if it is too early to use this license
        DateTime today = DateTime.Now;
        if (startDate > today)
        {
          // Bug 15051 MJO - Added customer name to message
          error = string.Format("The License, issued to {0}, is not valid yet Error: [TS-880]",
            customerName);
          return false;
        }

        // Check if license expired yet.
        //BUG 4834 Licensing BLL: uncommented this section...need to check for expired license
        if (today > endDate)
        {
          // Bug 15051 MJO - Added customer name to message
          error = string.Format("The License, issued to {0}, has expired.\nPlease contact CORE Business Technologies for a new license code. Error:[TS-881]",
            customerName);
          return false;
        }
        //end BUG 4834
        //#endregion
        //#region Test department count
        if (department_count > maxDepts)
        {
          /*Bug 13627-use workgroup*/
          // Bug 15051 MJO - Added customer name to message
          error = string.Format("The License, issued to {3}, covers {0} workgroups. There are {1} configured. You may not access Departmental Deposits until {2} workgroups are removed.\nError: [TS-882]",
            maxDepts, department_count, (maxDepts - department_count), customerName);
          return false;
        }
        //#endregion
        //#region Warn about expiring license
        //BUG 4834 Licensing BLL: if everything else is ok, warn for license about to expire
        // Bug #11850 Mike O - Change warning to 30 days
        if (today.AddDays(30) > endDate)
        {
          // Bug #11850 Mike O - Updated warning message
          // Bug 15051 MJO - Added customer name to message
          error = string.Format("Your iPayment license, issued to {1}, is set to expire on {0}. Please verify that your billing department has received and is processing the invoice from CORE.",
            endDate.ToShortDateString(),
            customerName);
          return true;//Bug#6995 ANU
        }
        //end BUG 4834
        //#endregion
      }
      catch (Exception e)
      {
        error = String.Format("Exception during license validation: {0}",TranSuiteServices.ConstructDetailedError(e)); //Bug 13832-return detailed error

        // Bug 17849
        Logger.cs_log_trace("Error ValidateLicense", e);
        return false;
      }
      return true;
    }
    public static bool ValidateLicense(int iNumberOfDepartments, ref string strError, params object[] args)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      // When using this function, make sure to catch errors
      // so you can finish-up anything you have already started before calling into here.
      // bDetailOK will hold true if detail payments are allowed for this type of license.

      try
      {
        //#region parameters
        GenericObject geo_args = misc.convert_args(args);

        if (!geo_args.has("license"))
          return false;

        GenericObject geoLic = geo_args.get("license") as GenericObject;

        string strLICENSE = GetKeyData(geoLic, "license_key", true);

        string strSTART_DT = GetKeyData(geoLic, "start_date", true);
        string strEND_DT = GetKeyData(geoLic, "end_date", true);
        string strDEPTMAX = GetKeyData(geoLic, "maximum_departments", true);
        string strCUSTNAME = GetKeyData(geoLic, "customer_name", true);
        string strALLOWDETAIL = GetKeyData(geoLic, "one_event_per_file", true);

        // Bug #12055 Mike O - Use misc.Parse to log errors
        int iMaxDepts = (int) misc.Parse<Int32>(strDEPTMAX);
        DateTime dtStartDate      = misc.Parse<DateTime>(strSTART_DT);
        DateTime dtEndDate        = misc.Parse<DateTime>(strEND_DT);
        bool bLicUse = (strALLOWDETAIL == "True" || strALLOWDETAIL == "False");

        //#endregion
        //#region calculate and test license
        string strChecksumValue = CreateLicenseChecksum(dtStartDate, dtEndDate,
          iMaxDepts, strCUSTNAME, bLicUse);


        // Check if license number is OK.
        if (strChecksumValue != strLICENSE)
        {
          strError = "The License in the Database is invalid.\nPlease contact CORE Business Technologies for a new license code.";
          return false;
        }

        //#endregion
        //#region Test date
        // Check if it is too early to use this license
        DateTime dtToday = DateTime.Now;
        if (dtStartDate > dtToday)
        {
          strError = "The License is not valid yet Error: [TS-880]";
          return false;
        }

        // Check if license expired yet.
        //BUG 4834 Licensing BLL: uncommented this section...need to check for expired license
        if (dtToday > dtEndDate)
        {
          strError = "The License has expired.\nPlease contact CORE Business Technologies for a new license code. Error:[TS-881]";
          return false;
        }
        //end BUG 4834
        //#endregion
        //#region test department count
        // Check for maximum number of departments allowed.
        if (iNumberOfDepartments > iMaxDepts)
        {
          string strErrMsg = string.Format("The License covers {0} workgroups. There are {1} configured. "+
            "You may not access Departmental Deposits until {2} workgroups are removed.",/*Bug 13627 use workgroup*/
            iMaxDepts, iNumberOfDepartments, (iMaxDepts - iNumberOfDepartments));
          strError = strErrMsg + "\nError: [TS-882]";
          return false;
        }
        //#endregion
        //#region Warn about expiring license
        //BUG 4834 Licensing BLL: if everything else is ok, warn for license about to expire
        if (dtToday.AddDays(60) > dtEndDate)
        {
          strError = string.Format("The License will expire on {0}. Please call CORE Business Technologies for a new License Code.",
            dtEndDate.ToShortDateString());
          return true; //Bug#6995 ANU
        }
        //end BUG 4834
        //#endregion
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error ValidateLicense", e);
        return false;
      }
      return true;
    }

    // TODO: Move this to misc
    // added status for CreateErrorObject
    public static GenericObject CreateErrorObject(string strCodeID, string strErrorMsg, bool bThrow, string strStatus)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      /*//bug 11329 -keep the code and message as long as possible
       * //Bug 10827 NJ
      //If show verbose error is turned off, clean the message and error code
      bool showVerboseError = (bool)c_CASL.GEO.get("show_verbose_error", false);
      if (!showVerboseError)
      {
          CleanErrorCode(ref strCodeID, ref strErrorMsg);
      }
      //end bug 10827*/
      GenericObject geoError = (GenericObject)c_CASL.c_make("error", "code", strCodeID, "message", strErrorMsg, "title", "transuite error", "throw", bThrow, "status", strStatus);

      return geoError;
    }

    // Called by: Login.logon and Login.unlock  (is implementation for TranSuite_DBAPI.verify_signin) 
    // UserLogin returns true or an error GEO
    // Bug 20562 UMN / EOM Add Multi-factor authentication (MFA) - new param: pin
    public static object UserLogin(GenericObject database, string username, string password,
      string session_id, GenericObject all_users, string department, GenericObject license,
      string file_source_type, bool ldap_verification, bool ldap_success, string pin, bool is_authenticated)
    {
      // Bug 12723: Pulled these out so they are global to the method
      string oldHashPassword = "";
      HashAlgorithm algorithm = null;

      //#region One-time-use setup user
      if (username == "setup")
      {
        return true;
      }

      //#endregion
      //#region Local variables
      GenericObject user, departments;
      int failedLoginAttemptCount = 0;

      string strError = "";
      //#endregion
      //#region Get user GEO
      if (!all_users.has(username))
      {
        // Bug 9757: FT: Use a customized LDAP message if appropriate
        if (ldap_verification)
        {
          if (ldap_success)
            return CreateErrorObject("TS-1253", TS1253_invalid_user_id_LDAP_OK, false, "No iPayment user with userId", true);
          else
            return true;  // User already got an error message
        }
        else
        return CreateErrorObject("TS-1068", TS1068_invalid_user_id, false, "No user with userId", true);//Bug 11329 NJ-always show verbose
      }
      else
      {
        user = all_users.get(username) as GenericObject;
        if (!user.has("departments"))
        {
            return CreateErrorObject("TS-1071", "User must belong to at least one workgroup.", false, "No Workgroup", true); //Bug 11329 NJ-always show verbose
        }
        else
        {
          departments = user.get("departments") as GenericObject;
        }
      }
      //#endregion++
      
      //#region Hash the password
      // Bug #8962 Mike O - Make sure the hash is in base64
      // DJD Bug 15132 Optimize User Log-In During Transfer [bypass hash method if transfer]
      string hashPassword = is_authenticated ? "" : misc.StringToBase64String(Crypto.CASLComputeHash("data", password, "salt", username));
      //#endregion
      
      //#region Check the license
      // Bug #11850 Mike O - Brought back license expiration warning
      bool returnWarning = false;

      // Bug 12723: OK, now we have disposed of LDAP authentication denial.
      // However we could still LDAP enabled and "Authenticated", but not yet 
      // "Authorized" in iPayment
      if (!ldap_verification && !is_authenticated)
      {
        // Bug 23335 UMN add if_missing
        oldHashPassword = user.get("password", c_CASL.c_opt) as string;
        if (String.IsNullOrWhiteSpace(oldHashPassword) || c_CASL.c_opt.Equals(oldHashPassword))
          return CreateErrorObject("TS-1248", "Password not found", false, "Password Expired", true);

        // Bug #10548 Mike O Check to see which algorithm was used to hash the password
        algorithm = Crypto.HashAlgorithmUsed(oldHashPassword);

        // Bug #8962 Mike O - Make sure the hash is in base64
        hashPassword = misc.StringToBase64String(Crypto.ComputeHash(username, password, algorithm));
        //#endregion
        //#region Check the license
        // Bug #11850 Mike O - Brought back license expiration warning
      }

      if (!ValidateLicense(ref strError, license))
      {
        //#region Check if user has Security item - 332
        //BUG 9197 NJ. If the license is not valid,  and if the user has the security item 333 
        //and is logging in from the Config module, then login verification should proceed.
        bool canLogin = false;
        canLogin = (bool)misc.CASL_call("a_method", "Security_item.user_may", "args",
                              new GenericObject("security_item_id", "332", "user_id", username));

        // END BUG 9197 
        //#endregion          

        // Bug 25532 MJO - Log failure
        Logger.LogWarn("User login license validation failure: " + strError, "TranSuiteServices.UserLogin");

        if (!(canLogin && file_source_type.Equals("Config")))
          return CreateErrorObject("TS-1067", "Invalid license.\n" + strError, false, "Invalid License", true);  //Bug 11329 NJ-always show verbose
      }
      else
      {
        if (strError != null && strError.Trim().Length > 0)
        {
          returnWarning = true;
        }
      }
      //#endregion
      
      //#region Check if user has reached the maximum failed attempts
      if (!is_authenticated) // DJD Bug 15132 Optimize User Log-In During Transfer [bypass failed login check if transfer]
      {
      failedLoginAttemptCount = GetFailedAttemptCount(user, database);
      if (failedLoginAttemptCount >= 3)
      {
        // TTS 20649 EOM - Provide generic message
        return CreateErrorObject("TS-1069", TS1069_too_many_failed, false, "Account Locked", true); //Bug 11329 NJ-always show verbose
      }

        // Bug 23847: Don't check the password when LDAP is enabled
        if (!ldap_verification)
        {
          if (hashPassword != oldHashPassword)
          {
            // Update failed count
            IncFailedAttemptCount(user, database);
            user.set("count_of_failed_attempts", GetFailedAttemptCount(user, database));
            // Bug #8129 Mike O - Corrected error message
            return CreateErrorObject("TS-1068", TS1070_invalid_password, false, "Incorrect password", true); //Bug 11329 NJ-always show verbose
          }
        }
        // End Bug 23847
      }

      if (ldap_verification)
      {
        // Bug 9757: FT: Make sure that the failed login count is incremented on failure of LDAP login
        if (!ldap_success)
        {
          // Update failed count
          IncFailedAttemptCount(user, database);
          user.set("count_of_failed_attempts", GetFailedAttemptCount(user, database));
          return true;  // User already got a failure message
        }
      }
      else if (!is_authenticated)
      {
        // Bug 23335 UMN moved following here and added if_missing
        oldHashPassword = user.get("password", c_CASL.c_opt) as string;
        if (String.IsNullOrWhiteSpace(oldHashPassword) || c_CASL.c_opt.Equals(oldHashPassword))
          return CreateErrorObject("TS-1248", "Password not found", false, "Password Expired", true);

        // Bug 9757: FT: If username / password is done via LDAP do not check password here
      if (hashPassword != oldHashPassword)
      {
        // Update failed count
        IncFailedAttemptCount(user, database);
        user.set("count_of_failed_attempts", GetFailedAttemptCount(user, database));
        // Bug #8129 Mike O - Corrected error message
        return CreateErrorObject("TS-1068", TS1070_invalid_password, false, "Incorrect password", true); //Bug 11329 NJ-always show verbose
      }
      }
      //#endregion
      
      
      //#region Check for inactivity
      //bug 6454 config period 
      //GenericObject geoUser           = geo_args.get("user") as GenericObject;

      // Bug 20804 UMN don't trust config to return ints, decimals etc. TryParse them!
      int config_period = ConfigParse("Business.Misc.data", "password_configured_period", 0);

      string strUserLastLogin = GetKeyData(user, "user_last_login", true); // bug 6454 config period
      DateTime dtNow = DateTime.Today;
      DateTime dtLastLogin;
      // Bug #12055 Mike O - Use misc.Parse to log errors
      if (strUserLastLogin == "") // No last login date always passes
        dtLastLogin = dtNow;
      else
        dtLastLogin = misc.Parse<DateTime>(strUserLastLogin);

      // Bug #6736 Mike O - If the resulting date is too large, set the inactive date to be the maximum possible
      DateTime dtInactive;
      try
      {
        dtInactive = dtLastLogin.AddDays(config_period);
      }
      catch (ArgumentOutOfRangeException)
      {
        dtInactive = DateTime.MaxValue;
      }
      // End Bug #6736

      if (dtInactive < dtNow)
      {
        // BEGIN TTS 20649 EOM - Provide generic message
        return CreateErrorObject("TS-1079", TS1079_user_locked, false, "Locked for Inactivity", true);//Bug 11329 NJ-always show verbose
      }
      //#endregion
      
      //#region Check if user is allowed in department
      if (department != null && departments.has(department).Equals(false))
      {
        strError=string.Format("User: {0} is not permitted access to Workgroup: {1}", username, department); //Bug 13627-use workgroup
        return CreateErrorObject("TS-1071", strError, false, "No Workgroup", true);//Bug 11329 NJ-always show verbose
      }
      //#endregion
      //#region Check if this user needs to change password.
      // Bug 8932 - UMN: transfer users are already authenticated outside of 
      // ipayment, so don't check for password expiry
      string error_message = "";
      bool passwordExpirationWarning = false; // Bug #6467 Mike O
      if (!is_authenticated)
      {
      //Enabled per bug #6182
      int change_password_return_code = MustChangePWD("user", user);
      // Possible return values:
      //  0 - Success.
      //  1 - Unable to find specific User with that UserID
      //  2 - Your password must be reset! Enter a new one to log on.
      //  3 - Password never expires. Allow logon.
      //  4 - Unable to find date the Password was last changed!
      //  5 - Your Password has expired! You must enter a new one to log on.
      //  6 - Password will expire soon.

      // Bug 9757: FT: if LDAP verification is enabled, password checking will be done in LDAP instead
      if (!ldap_verification)
      {

      switch (change_password_return_code)
      {
        case 2:
          error_message = "PASSWORD EXPIRATION - Your password must be changed.";
          break;
        case 4:
          error_message = "PASSWORD EXPIRATION - Unable to find date of last password change. Your password must be changed.";
          break;
        case 5:
          error_message = "PASSWORD EXPIRATION - Your password has expired. Your password must be changed.";
          break;
        case 6: // Bug #6467 Mike O
          passwordExpirationWarning = true;
          break;
      }
      if (error_message != "")
      {
        return CreateErrorObject("TS-1248", error_message, false, "Password Expired", true);//Bug 11329 NJ-always show verbose
      }
      }
      }
      //#endregion

      // BEGIN Bug 20562 UMN / EOM Add Multi-factor authentication (MFA) using Google Authenticator
      if (!String.IsNullOrWhiteSpace(pin)) // Cashiering doesn't use a PIN, so will send an empty string
      {
          try
          {
              // get the user's secret key
              string user_key = user.get("mfa_user_key", "missing") as String;

              //need to create MFA key in Config->User->Change PIN
              if (user_key == "missing")
              {
                  return CreateErrorObject("TS-1255", TS1255_invalid_PIN, false, "User has no MFA Key set up.  Unable to verify PIN.", true);
              }

              //Decrypt user key
              byte[] bUserKeyEncrypted = Convert.FromBase64String(user_key);
              byte[] bUserKeyDecrypted = Crypto.symmetric_decrypt("data", bUserKeyEncrypted, "secret_key", Crypto.get_secret_key());
              user_key = System.Text.Encoding.Default.GetString(bUserKeyDecrypted);
              TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();

              // Validate the PIN, but override library's default clock drift of 5 minutes. 
              // Use clock drift tolerance of +/- 30 seconds instead.
              bool pinValid = tfa.ValidateTwoFactorPIN(user_key, pin, TimeSpan.FromSeconds(30));
              if (!pinValid)
              {
                  // Update failed count in logical and physical models
                  IncFailedAttemptCount(user, database);
                  user.set("count_of_failed_attempts", GetFailedAttemptCount(user, database));
                  return CreateErrorObject("TS-1255", TS1255_invalid_PIN, false, "Invalid PIN", true);
              }
          }
          catch (Exception ex)
          {
          // Bug 17849
          Logger.cs_log_trace("Error User Login", ex);
              return CreateErrorObject("TS-1255", ex.Message, false, "Invalid PIN", true);
          }
      }
      // END Bug 20562 UMN / EOM Add Multi-factor authentication (MFA) using Google Authenticator


      //#region  Update session ID.
      // Bug #11869 Mike O - Update with the new session id
      SessionValidator validator = SessionValidator.GetInstance();
      validator.updateSessionID(username, file_source_type, session_id);
      //#endregion
      //#region Reset the login failures to 0 and reset last login date
      user.set("count_of_failed_attempts", 0);

      // Bug 8932 UMN Optimize User Log-In During Transfer
      if (!is_authenticated) ResetFailedAttemptCount(user, database);
      user.set("user_last_login", dtNow.ToString("MM/dd/yyyy"));
      int lastLoginReturn = UpdateLastLogin(user, database);
      if (lastLoginReturn != 0)
      {
        //Log the failure
        return CreateErrorObject("TS-1077", "Unable to update last login date information to database. Error: " + error_message,
            false, "Not Updated", true);//Bug 11329 NJ-always show verbose
      }
      //misc.CASL_call("a_method","freeze", "_subject", user, "args", new GenericObject());
      //#endregion
      //#region BUG 4834 Licensing BLL: will still login, but with a warning displayed
      // Bug #11850 Mike O - Brought back license expiration warning
      if (returnWarning)
      {
        return CreateErrorObject("TS-1220", "LICENSE EXPIRATION WARNING: " + strError, false, "License Expiration Warning", true);//Bug 11329 NJ-always show verbose
      }
      if (passwordExpirationWarning)
      {
        // Bug 20804 UMN don't trust config to return ints, decimals etc. TryParse them!
        int max_days = ConfigParse("Business.Misc.data", "password_expiration_max_days", 0);

        string strPasswordLastChangedOn = GetKeyData(user, "password_last_changed_on", true);
        DateTime dtChange = DateTime.Parse(strPasswordLastChangedOn).AddDays(max_days);
        return CreateErrorObject("TS-1254",
          "Password Expiration Warning: Your password will expire on " + dtChange.ToShortDateString(),
          false, "Password Expiration", true);//Bug 11329 NJ-always show verbose
      }
      else
      {
        return true;
      }
      //#endregion
    }

    //bug 6454 config period adding new method to insert login in database
    public static int UpdateLastLogin(GenericObject user, GenericObject database)
    {
      // BEGIN DJD Bug 15164 Optimize User Log-In During Transfer [write to DB only on initial log in]
      DateTime now = DateTime.Now;
      string now_string = now.ToString("MM/dd/yyyy");
      if (user.has("user_last_login_db"))
      {
        string strUserLastLoginDB = user.get("user_last_login_db") as string;
        if (strUserLastLoginDB == now_string) 
        {
          return 0; // Just return; user logged in previously today and the date was already saved to the DB
        }
      }
      // END DJD Bug 15164 Optimize User Log-In During Transfer [write to DB only on initial log in]
      
      //#region Set Args
      string user_path = (string)misc.ToPath(user, c_CASL.GEO);
      string error_message = "";
      Hashtable sql_args = new Hashtable();
      sql_args["container"] = user_path;
      sql_args["field_key"] = "\"user_last_login\"";
      //#endregion
      //#region Check for prior user_last_login existence
      string sql_string = "SELECT COUNT(*) as COUNT FROM CBT_FLEX WHERE container=@container and field_key=@field_key";
      IDBVectorReader reader = db_reader.Make(database, sql_string, CommandType.Text, 0, sql_args, 0, 0, null, ref error_message);
      if (error_message != "")
      {
        return -1;
      }
      GenericObject record_set = new GenericObject();
      reader.ReadIntoRecordset(ref record_set, 0, 0);
      if (record_set.getLength() != 1)
      {
        return -1;
      }
      GenericObject record = (GenericObject)record_set.get(0);
      int count = (int)record.get("COUNT", 0);
      //#endregion
      //#region Prepare Sql String
      sql_args["field_value"] = "\"" + now_string + "\"";

      if (count == 0)
      {
        // New line
        // Bug 25984 UMN delete revision
        sql_string = "INSERT INTO CBT_FLEX (container, field_key, field_value) values(@container, @field_key, @field_value)";
      }
      else if (count == 1)
      {
        // Update old line
        sql_string = "UPDATE CBT_FLEX SET field_value=@field_value WHERE container=@container AND field_key=@field_key";
      }
      else
      {
        return -1;
      }
      //#endregion
      //#region Update
      reader = db_reader.Make(database, sql_string, CommandType.Text, 0, sql_args, 0, 0, null, ref error_message);
      if (error_message != "")
      {
        //Throw error here
        return -1;
      }
      int row_count = reader.ExecuteNonQuery(ref error_message);
      if (error_message != "" || row_count == 0)
      {
        //Throw error here
        return -1;
      }
      //#endregion

      user.set("user_last_login_db", now_string); // DJD Bug 15164 Optimize User Log-In During Transfer [write to DB only on initial log in]
      return 0;
    }

    public static Object CASLUserLogin(params object[] args)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!
      //#region Setup
      GenericObject geo_args = misc.convert_args(args);
      GenericObject database = (GenericObject)geo_args.get("config");
      string username = (string)geo_args.get("username");
      string password = (string)geo_args.get("password");
      string session_id = (string)geo_args.get("session_id");
      GenericObject all_users = (GenericObject)geo_args.get("all_users");
      object department = geo_args.get("department");
      string department_id;
      if (department is string)
      {
        department_id = (string)department;
      }
      else
      {
        department_id = null;
      }
      GenericObject license = (GenericObject)geo_args.get("license");
      string file_source_type = geo_args.get("file_source_type").ToString();
      bool ldap_success = (bool)geo_args.get("ldap_success", true);     // Bug 9757: FT:
      bool ldap_verification = (bool)geo_args.get("ldap_verification"); // Bug 9757: FT
      string pin = (string)geo_args.get("pin"); // Bug 20562 UMN / EOM Add Multi-factor authentication (MFA) pin

      // Bug 8932 UMN an auth token was used, so don't check password
      bool is_authenticated = (bool)geo_args.get("is_already_authenticated", false);
      //#endregion
      // Bug 20562 UMN / EOM Add Multi-factor authentication (MFA) new param: pin
      return UserLogin(database, username, password, session_id, all_users, department_id, license, file_source_type, ldap_verification, ldap_success, pin, is_authenticated);
    }

    public static Object CASLSetPasswordExpired(params object[] args)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      GenericObject geo_args = misc.convert_args(args);
      string strUserID = geo_args.get("username") as string;// this is realy user_id
      bool is_password_expired = (bool)geo_args.get("must_change_password");
      GenericObject geoUser = null;
      GenericObject geoUsers = (GenericObject)geo_args.get("all_users");
      GenericObject database = geo_args.get("config") as GenericObject;
      if (geoUsers.has(strUserID))
        geoUser = geoUsers.get(strUserID) as GenericObject;
      else
        return CreateErrorObject("TS-1068", TS1068_invalid_user_id, false, "No user with userId", true);//Bug 11329 NJ-always show verbose
      geoUser.set("must_change_password", is_password_expired);
      geoUser.set("password_last_changed_on", DateTime.Now.ToShortDateString());
      geoUsers.set(strUserID, geoUser);
      
      // Bug 25984 UMN delete revision
      misc.CASL_call("a_method", "freeze", "_subject", geoUser, "args", new GenericObject());


      // Bug 18724: Added expired to the activity log
      string details = "User expired:" + strUserID
        + ",Must Change password:" + is_password_expired
        + ",Set password_last_changed_on to:" + DateTime.Now.ToShortDateString();

      string summary = is_password_expired ?
        "Set Password Expired for " :
        "Unset Password Expired for ";

      summary += strUserID;


      string login_user = (CASLInterpreter.get_my() as GenericObject).get("login_user", "") as string;

      // Bug 20065: FT
      string application_name = (CASLInterpreter.get_my() as GenericObject).get("application_name", "Portal") as string;

      misc.CASL_call("a_method", "actlog", "args",
      new GenericObject(
        // Bug 20435 UMN PCI-DSS don't pass session
        "user", login_user,
        "app", application_name,      // Bug 20065: FT
        "action", "Password Management",
        "summary", summary, 
        "detail", details));
      // End 18724

      return true;
    }

    public static object CASLResetFailedLogins(params object[] args)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      GenericObject geo_args = misc.convert_args(args);
      string strUserID = geo_args.get("username") as string;// this is realy user_id
      GenericObject geoUser = null;
      GenericObject geoUsers = (GenericObject)geo_args.get("all_users");
      GenericObject database = geo_args.get("config") as GenericObject;
      if (geoUsers.has(strUserID))
        geoUser = geoUsers.get(strUserID) as GenericObject;
      else
        return CreateErrorObject("TS-1068", TS1068_invalid_user_id, false, "No user with userId", true);//Bug 11329 NJ-always show verbose
      geoUser.set("count_of_failed_attempts", 0);
      ResetFailedAttemptCount(geoUser, database);

      // bug 6454
      DateTime now = DateTime.Now;
      string now_string = now.ToString("MM/dd/yyyy");
      geoUser.set("user_last_login", now_string);
      int updateLastLogin = UpdateLastLogin(geoUser, database);
      if (updateLastLogin == -1)
      {
        return CreateErrorObject("TS-1077", "Unable to update last login date information to database.", false,
            "Not Updated", true);//Bug 11329 NJ-always show verbose
      }

      //end of 6454
      geoUsers.set(strUserID, geoUser); // TOREMOVE: This is probably an unnecessary test
      return true;
    }

    public static bool CheckPasswordFormat(string strNewPassword)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      //-------------------------------------------------
      // Check new password format. 8 char alpha/numeric. Allow special characters except &<>             
        string numericCharacters = "0123456789";
      string  alphaCharacters="abcdefghijklmnopqrstuvwxwzABCDEFGHIJKLMNOPQRSTUVWXYZ";
      /*Bug 13846 - Removed old code. Added better check for password format*/
      string specialChars = "&<>";    
      char[] pwd = strNewPassword.ToCharArray();
      int numCount = 0, charCount=0;
      for (int i = 0; i < pwd.Length; i++)
      {
          if (numericCharacters.IndexOf(pwd[i]) > -1)
              numCount++;
          else if (alphaCharacters.IndexOf(pwd[i]) > -1)
              charCount++;
          else if(specialChars.IndexOf(pwd[i])>-1) 
        return false;
         
      }

      int iNewPassLen = strNewPassword.Length;
      return (numCount>=2 && charCount>=2 && iNewPassLen>=8);        
        //end bug 13846
    }
    //Bug 13846-for accessing password format from CASL
    public static object CASLCheckPasswordFormat(params object[] args)
    {
        GenericObject geo = misc.convert_args(args);
        string newPassword = geo.get("new_value") as string;
        return CheckPasswordFormat(newPassword);        
    }
      //end bug 13846

    public static object CASLAdminChangePassword(params object[] args)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      GenericObject geo_args = misc.convert_args(args);
      String strUserID = geo_args.get("username") as string;
      string sNewPassword = geo_args.get("new_password") as string;
      string strErr = "";
      GenericObject geoUser = null;
      GenericObject geoUsers = (GenericObject)geo_args.get("all_users");
      GenericObject database = geo_args.get("config") as GenericObject;
      if (geoUsers.has(strUserID))
        geoUser = geoUsers.get(strUserID) as GenericObject;
      else
        return CreateErrorObject("TS-1068", TS1068_invalid_user_id, false, "No user with userId", true);//Bug 11329 NJ-always show verbose

      //-------------------------------------------------
      // Check new password format. 8 char alpha/numeric
      if (!CheckPasswordFormat(sNewPassword))
      {
          //Bug 13846-include special chars info in message
          strErr = @"Password must be at least 8 characters long, contain at least two letters and two numbers. 
                    Special characters are allowed except & < >.";
        return CreateErrorObject("TS-1081", strErr, false, "Invalid Password", true);//Bug 11329 NJ-always show verbose
      }
      //-------------------------------------------------

      //-------------------------------------------------
      // Set as previous password BUG19989  
      // Bug 20804 UMN don't trust config to return ints, decimals etc. TryParse them!
      int iPasswordsToCheck = ConfigParse("Business.Misc.data", "previous_password_count", 12);

      string strSalt = strUserID;
      string strNewPassword = Crypto.CASLComputeHash("data", sNewPassword, "salt", strSalt);
      strNewPassword = misc.StringToBase64String(strNewPassword);
      //-------------------------------------------------
      GenericObject geoLastPasswords = (GenericObject)geoUser.get("previous_passwords", c_CASL.c_GEO());// BUG4495
      geoLastPasswords.vectors.Insert(0, strNewPassword);
      if (geoLastPasswords.vectors.Count > iPasswordsToCheck)
      {
          geoLastPasswords.vectors.RemoveRange(iPasswordsToCheck, geoLastPasswords.vectors.Count - iPasswordsToCheck);
      }
      geoUser.set("previous_passwords", geoLastPasswords);
      geoUser.set("password", strNewPassword);
      geoUser.set("count_of_failed_attempts", 0);
      ResetFailedAttemptCount(geoUser, database);
      geoUsers.set(strUserID, geoUser);
      
      // Bug 25984 UMN delete revision
      misc.CASL_call("a_method", "freeze", "_subject", geoUser, "args", new GenericObject());

      // Bug 20098: Capture the login user id
      string login_user = "";
      if (CASLInterpreter.get_my() != null && CASLInterpreter.get_my().has("login_user"))
        login_user=CASLInterpreter.get_my()["login_user"] as string;

      // Bug 20065: FT
      string application_name = "Portal";
      if (CASLInterpreter.get_my() != null && CASLInterpreter.get_my().has("application_name"))
        application_name = CASLInterpreter.get_my()["application_name"] as string;

      // Write to the activity log
      misc.CASL_call("a_method", "actlog", "args",
        new GenericObject(
          // Bug 20435 UMN PCI-DSS don't pass session
          "user", login_user,
          "app", application_name,              // Bug 20065: FT
          "action", "Password Change", 
          "summary", "Changed User Password",
          "detail", "User:" + strUserID));
      // end bug 20098

      // BEGIN Bug 20562 EOM Add Multi-factor authentication (MFA) 
      // Check if user is MFA enabled and MFA is enabled 


    if (MFA_Should_Send_Email("geoUser", geoUser))
    {
        if (geoUser.has("user_email"))
            // need to email the new password to the User
            emails.CASL_email_login("to", (string)geoUser.get("user_email"),
            "from", "no-reply@ipayment.com",
            "user_name", (string)geoUser.get("name"),
            "change_pin", "false",
            "change_pwd", "true",
            "user_pwd", strNewPassword,
            "mfa_user_key", "",
            "user_id", (string)geoUser.get("id"));
        else
            // Bug #21203 EOM No PWD change message when email is missing 
            //return  CreateErrorObject("user_email_missing", "Failed to send email.  User email is not specified", false, "User email missing", true);
            return "no_email";  

    }
    // END Bug 20562 EOM Add Multi-factor authentication (MFA) 

      return true;
    }

    // BEGIN Bug 20562 EOM Add Multi-factor authentication (MFA) 
    private static bool MFA_Should_Send_Email(params object[] args)
    {
        GenericObject geoUser = null;
        GenericObject geo_args = misc.convert_args(args);
        geoUser = (GenericObject)geo_args.get("geoUser");

        if (geoUser.has("security_profile", false) && (bool)(c_CASL.c_object("Business.Misc.data") as GenericObject).get("enable_MFA", false))
        {
            GenericObject security_profile = (GenericObject)geoUser.get("security_profile");
            GenericObject security_items = (GenericObject)security_profile.get("security_items");
            ArrayList keys = security_items.getKeys();

            foreach (object key in keys)
            {
                //There could be plain strings in there... :?
                if (key.GetType() == typeof(GenericObject))
                {
                    GenericObject GO = (GenericObject)key;
                    if (GO.ContainsValue("999") || GO.ContainsValue("294")) //|| GO.ContainsValue("295"))  config | change pwd | portal 
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    // END Bug 20562 EOM Add Multi-factor authentication (MFA) 

    public static int MustChangePWD(params object[] args)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      // Possible return values:
      //  0 - Success.
      //  1 - Unable to find specific User with that UserID
      //  2 - Your password must be reset! Enter a new one to log on.
      //  3 - Password never expires. Allow logon.
      //  4 - Unable to find date the Password was last changed!
      //  5 - Your Password has expired! You must enter a new one to log on.
      //  6 - Password will expire soon.

      GenericObject geo_args = misc.convert_args(args);
      GenericObject geoUser = geo_args.get("user") as GenericObject;

      // Bug #8356 Mike O - If CBT Central Services is enabled, get must_change_password from the DB
      bool must_change_password = (bool)((string)GetKeyData(geoUser, "must_change_password", true).ToLower() == "false" ? false : true);

      int max_days = (int)(c_CASL.c_object("Business.Misc.data") as GenericObject).get("password_expiration_max_days", 0);
      string strPasswordLastChangedOn = GetKeyData(geoUser, "password_last_changed_on", true);

      // Bug #6467 Mike O - Number of days before the password expires that the user should start being warned
      // Bug 20804 UMN don't trust config to return ints, decimals etc. TryParse them!
      int expiration_warning = ConfigParse("Business.Misc.data", "password_expiration_warning", 0);

      // Check if the reset password flag is set.
      if (must_change_password)
        return 2;

      // Bug#8136 To have password max expiration days beween 1 and 90 days as per PA DSS ANU 
      //  return value 3 is deprecated as per PA DSS ANU .
      // If the range is zero, password never expires - allow logon.
      // if(max_days<=0)//strPasswordMaxDays == "0")
      //  return 3;

      if (strPasswordLastChangedOn == "")
        return 4;

      // Check the password expiration date.
      DateTime dtNow = DateTime.Today;
      // Bug #12055 Mike O - Use misc.Parse to log errors
      DateTime dtChange = misc.Parse<DateTime>(strPasswordLastChangedOn).AddDays(max_days);

      // Bug #6467 Mike O - Changed to use the configured number of days
      // Check whether or not a password expiration warning should be displayed
      if (expiration_warning != 0 && dtChange >= dtNow && dtChange <= dtNow.AddDays(expiration_warning))
        return 6;

      // Password expired
      if (dtChange < dtNow)
        return 5;

      return 0;
    }

    /// <summary>
    /// not used in T4
    /// Get any error message
    /// </summary>
    /// <returns></returns>
    public static String GetErrorMsg(params object[] arg)
    {
      return m_sErrMsg;
    }

    public static string StringFromBase64(params object[] args)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      GenericObject geo_args = misc.convert_args(args);

      // Catch the OPT object.
      if (geo_args.get("Base64Value") == c_CASL.c_opt)
        return "";

      string strData = geo_args.get("Base64Value") as string;
      if (strData.Length > 0)
      {
        ASCIIEncoding encoding = new ASCIIEncoding();
        return encoding.GetString(Convert.FromBase64String(strData));
      }

      return "";
    }

    // Bug #10955 Mike O - Commented out so it's not used accidentally
    /*public static String CreateChecksum(params object[] args)
    {
      // DEPRECATED -- just call ComputeHash or CASLComputeHash
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!
      //
      GenericObject geo_args = misc.convert_args(args);

      if(geo_args.get("data")== c_CASL.c_opt)
        return "";

      string strData = (string)geo_args.get("data");// Checksum data.
      if(strData.Length > 0)
        return Crypto.CASLComputeHash("data",strData);

      return "";
    }*/

    /* Establis a connection to the databse then passes the connection with other
     * Information to UpdateTranFileChecksum. So row in TG_DEPFILE_DATA will be updated with
     * a new checksum.
     *  AXEL's function!
     * */
    public static string UpdateTableChecksum(params object[] args_pairs)
    {
      GenericObject geo_args = misc.convert_args(args_pairs);
      GenericObject config = geo_args.get("config") as GenericObject;
      string strFileNumber = geo_args.get("core_file_nbr") as string;
      string strFileSequenceNbr = geo_args.get("core_file_seq") as string;
      string strError = "Default Error";

      ConnectionTypeSynch pDBConn = new ConnectionTypeSynch(config);
      try
      {
      int iDBType = pDBConn.GetDBConnectionType();

      if (!pDBConn.EstablishDatabaseConnection(ref strError))
        HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-759", strError);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix

      //Matt -- replaced old checksum update call 
      string strFileName = strFileNumber + strFileSequenceNbr.PadLeft(3, '0');
      if (!UpdateTranFileChecksum("DbConnectionClass", pDBConn, "strFileName", strFileName))
        HandleErrorsAndLocking("message", "Unable to set checksum on TG_DEPFILE_DATA. Please call system administrator.", "code", "TS-760", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix

      return "Success";
    } 
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnection(pDBConn);
      }
    }

    public static object CreateT3File(params Object[] arg_pairs)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!
      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject a_core_file = (GenericObject)args.get("_subject", c_CASL.c_undefined);
      GenericObject config = args.get("config", c_CASL.c_undefined) as GenericObject;
      GenericObject data = args.get("data", c_CASL.c_undefined) as GenericObject;
      string session_id = args.get("session_id") as string;

      ConnectionTypeSynch pDBConn = null;
      bool connectionOpened = false;          // Bug 15941

      ThreadLock.GetInstance().CreateLock();// Sync user's access to this resource. Bug# 5813 deadlock fix. DH 07/30/2008
      // Bug 11661: Wrap mutex release in finally block
      try
      {
        string strErr = "", strSQL = "";

        //BUG#6693 ANU
        //#region Check if connection passed from PostCoreFile
        if (args.has("DbConnectionClass"))
        {
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
        }
        else
        {
          // We have to get a new database connection and later commit ourselves.
          pDBConn = new ConnectionTypeSynch(data);
          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strErr)))  // Bug 15941
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-788", strErr);//Bug 13832 - added detailed error message. // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix

          if (!pDBConn.EstablishRollback(ref strErr))
              HandleErrorsAndLocking("message", "Error creating database transaction.", "code", "TS-789", strErr);//Bug 13832 - added detailed error message. // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix
        }
        int iDBType = pDBConn.GetDBConnectionType();
        //#endregion
        //BUG#6693 ANU

        //**********************************************************************
        // Create DateTime for ORACLE tables; 
        string strTodayDateTime = CreateTodayDateForOracle();
        //**********************************************************************

        //***********************************************************************
        string strFILETYPE = GetKeyData(a_core_file, "FILETYPE", true);
        string strFILEDESC = GetKeyData(a_core_file, "FILEDESC", true);
        string strSTATUS = GetKeyData(a_core_file, "STATUS", true);
        string strEFFECTIVEDT = GetKeyData(a_core_file, "EFFECTIVEDT", true);
        string strSOURCE_TYPE = GetKeyData(a_core_file, "SOURCE_TYPE", true);
        string strSOURCE_GROUP = GetKeyData(a_core_file, "SOURCE_GROUP", true);
        string strSOURCE_DATE = GetKeyData(a_core_file, "SOURCE_DATE", true);
        string strDEPTID = GetKeyData(a_core_file, "DEPTID", true);
        string strOPEN_USERID = GetKeyData(a_core_file, "OPEN_USERID", true);
        string strCREATOR_ID = GetKeyData(a_core_file, "CREATOR_ID", true); //BUG#4602/PRF
        string strLOGIN_ID = (string)a_core_file.get("LOGIN_ID", ""); //BUG 5395v2 (CreateT3File)


        if (strFILETYPE.Length <= 0)
          strFILETYPE = "S";

        if (strSOURCE_TYPE.Length == 0)
          HandleErrorsAndLocking("message", "Source Type is required.", "code", "TS-762", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix

        // If Source Group is empty, put in "current"
        // NOTE: When putting in a "current" group, should we look for previous current
        // groups and mark them as something else?  Some other group id, like a date, or "pending"?

        //Matt: source group will now be null in cashiering and source type will be Cashier
        if (strSOURCE_GROUP.Length == 0)
          strSOURCE_GROUP = "";
        //***********************************************************************

        //***********************************************************************
        // CREATE FILE NUMBER & NAME
        //----------------------------------------------
        // Get sequence number of any files opened for today   
        //TTS22469 USE JULIAN DATE for inquiry use DEPFILENBR to avoid a full table scan
        DateTime dtDateTime = DateTime.Now;
        // Bug #8305 Mike O - Use default file_name_prefix if none was provided in web.config
        // Bug 8030 MJO - Use the prefixed file number for the initial query
        string strJulianDate = c_CASL.GEO.get("file_name_prefix", "20") + string.Format("{0}", misc.get_YYJJJ());
        long lNewSequence = 0;
        string strSequence = "";

        /* 
           Bug# 26242 DH - I am changing COUNT to MAX(DEPFILESEQ).
        */
        if (iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_ODBC || iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_TNS)
            strSQL = string.Format("SELECT MAX(DEPFILESEQ) AS COUNT FROM TG_DEPFILE_DATA WHERE DEPFILENBR =\'{0}\'", strJulianDate);
        else if (iDBType == ConnectionTypeSynch.m_iCurrentDatabase_SqlServer)
            strSQL = string.Format("SELECT MAX(DEPFILESEQ) AS COUNT FROM TG_DEPFILE_DATA WHERE DEPFILENBR =\'{0}\'", strJulianDate);
        else if (iDBType == ConnectionTypeSynch.m_iCurrentDatabase_MsAccess_ODBC)// Access
            strSQL = string.Format("SELECT MAX(DEPFILESEQ) AS COUNT FROM TG_DEPFILE_DATA WHERE DEPFILENBR =\'{0}\'", strJulianDate);
        // End Bug# 26242 DH

        lNewSequence = GetTableRowCount(data, strSQL, iDBType);
        if (lNewSequence <= 999)
          strSequence = string.Format("{0:000}", lNewSequence + 1);
        else
          strSequence = string.Format("{0}", lNewSequence + 1);
        //----------------------------------------------
        //----------------------------------------------
        // Create file number = YYYY/JulianDay/Sequence
        // DJD 11/12/09 File Number Format change: PPYY/JulianDay/Sequence where
        // "PP" is a numeric prefix code (10-99) designating the server.
        //string strFileNbr   = string.Format("{0}", misc.get_YYYYJJJ());

        string strFileNbr = strJulianDate;
        string strFileName = strFileNbr + strSequence;
        //----------------------------------------------

        //***********************************************************************

        //**********************************************************************
        // Establish connection and transaction. BUG#6693 move up code
        //			if(!pDBConn.EstablishDatabaseConnection(ref err))
        //				HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-763");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        //
        //			string strErrMsg = "";
        //			if(!pDBConn.EstablishRollback(ref strErrMsg))
        //				HandleErrorsAndLocking("message", "Error creating database transaction." + strErrMsg, "code", "TS-764");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        //**********************************************************************

        //**********************************************************************
        // NEW FILE RECORD
        // Create SQL for insert into TG_DEPFILE_DATA table.
        // Bug 23557: Changed to Parameterized
        HybridDictionary sql_args = new HybridDictionary();
        strSQL = CreateParameterizedSQLForInsertInto_TG_DEPFILE_DATA(
          strFileNbr,
          strSequence,
          strFileName,
          strDEPTID,
          strFILETYPE,
          strOPEN_USERID,
          strCREATOR_ID,
          strFILEDESC,
          "N",
          strEFFECTIVEDT,
          strTodayDateTime,
          strSOURCE_DATE,
          strSOURCE_GROUP,
          strSOURCE_TYPE,
          sql_args);

        strErr = "";
        int count = ParamUtils.runExecuteNonQueryCommitRollback(pDBConn, strSQL, sql_args, out string errorMsg);
        if (count != 1 || errorMsg != "")
        {
          HandleErrorsAndLocking("message", "Error executing SQL.", "code", "TS-765", errorMsg);////Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix
        }
        // End Bug 23557
        //**********************************************************************

        // Bug 22611 UMN remove TG_FILESOURCE_DATA

        //**********************************************************************
        // ALL WAS PROCESSED OK, UPDATED THE CORE_FILE AND SEND IT BACK. 
        a_core_file.set("FILENAME", strFileName);
        string strOPENDT = dtDateTime.ToString("MM/dd/yyyy h:mm:ss tt");// BUG#14761  need both date and time 
        a_core_file.set("OPENDT", strOPENDT);
        //**********************************************************************

        //*********************************************************************      
        // ALL GOOD - ACCEPT THE UPDATES.
        if (!args.has("DbConnectionClass")) //BUG#6693 
        {
          strErr = "";
          if (!pDBConn.CommitToDatabase(ref strErr))
              HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-767", strErr);//Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix

        }
        //*********************************************************************

        // Update Checksums
        string strError = "";
        //Matt -- replaced old checksum call
        if (!args.has("DbConnectionClass")) //BUG#6693 
        {
          if (!UpdateTranFileChecksum("data", data, "strFileName", strFileName))
            HandleErrorsAndLocking("message", "Unable to set checksum on TG_DEPFILE_DATA. Please call system administrator.", "code", "TS-768", strError);//13832-detailed error // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix
        }

      }
      finally
      {
        // Bug 15941. Close the database connection and dispose of pDBConn class.
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
        // Bug 11661
        ThreadLock.GetInstance().ReleaseLock(); //Bug# 5813 deadlock fix. DH 07/30/2008
      }

      return a_core_file;
    }

    // SourceGetFileNbr()
    /// <summary>
    /// wrapper for CBT_TG_DBLib method
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    /// 

    public static object GetT3File(params Object[] arg_pairs)
    {
      /*
        NOTE: Be very cautious of this function.  myCtrl will fill in certain
        file level information (such as FILENAME, DEPTID, OPENDT, EFFECTIVEDT
        OPEN_USERID, and SOURCE_DATE) based on the SOURCE_TYPE and SOURCE_GROUP
        value provided.  This means that if an existing file - with transaction
        and tender information - is used to call this function, then the File Number
        (FILENAME) will be overwritten but the RECEIPT INFORMATION WILL REMAIN.
        This would then be a "corrupted file" in the code (with mismatched File Numbers
        and Receipt Numbers), and could potentially cause huge problems in the database.
        This function should ONLY be called for a Core_file object that has just been 
        instantiated, never one that has been in use.
      */
      //			GenericObject args = misc.convert_args(arg_pairs);
      //			GenericObject configData = args.get("config", c_CASL.c_undefined) as GenericObject;
      //			GenericObject a_core_file	= args.get("_subject", c_CASL.c_undefined) as GenericObject;
      //
      //			String datasource_name = configData.get("datasource_name").ToString();
      //			String login_name = configData.get("login_name").ToString();
      //			String login_password = configData.get("login_password").ToString();
      //
      //			CBT_TG_DBLib.CBT_TG_DBClass myCtrl = TranSuiteServicesInit("datasource_name",datasource_name, "login_name",login_name, "login_password",login_password);
      //
      //			//			String sSrcType = args.get("source_type", c_CASL.c_undefined) as string;
      //			//			String sSrcGroup = args.get("source_group", c_CASL.c_undefined) as string;
      //			//Try to create a New File.  
      //			//			GenericObject a_core_file = c_CASL.c_make("Core_file") as GenericObject;
      //
      //			if( !myCtrl.SourceGetFile(a_core_file) )
      //			{
      //				m_sErrMsg = myCtrl.GetErrMsg();
      //				return false;
      //			}
      //			myCtrl = null;
      //			return a_core_file;

      return false;
    }

    // Bug #9633 Mike O - Fixed this up so there won't be any deadlock issues
    public static Object SubmitImageToDatabase(params Object[] arg_pairs)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject data = args.get("data", c_CASL.c_undefined) as GenericObject;
      ConnectionTypeSynch pDBConn_Data = null;
      string strSQL = "";
      string strErr = "";

      // Bug 158941
      bool connectionOpened = false;

      ThreadLock.GetInstance().CreateLock();
      // Bug 11661: Wrap mutex release in finally
      try
      {

        //**********************************************************************
        // Must be passed in from the caller.
        if (args.has("DbConnectionClass"))
          pDBConn_Data = (ConnectionTypeSynch)args.get("DbConnectionClass");
        else
        {
          // We have to get a new database connection and later commit ourselves.
          pDBConn_Data = new ConnectionTypeSynch(data);
          if (!(connectionOpened = pDBConn_Data.EstablishDatabaseConnection(ref strErr)))  // Bug 15941
              HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1063", strErr);//Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix BUG#8622//Bug 11329 NJ-show verbose fix

          if (!pDBConn_Data.EstablishRollback(ref strErr))
            ThrowCASLErrorButDoNotCloseDBConnection("TS-1064", ref pDBConn_Data, "Error creating database transaction.", strErr); //Bug 13832 - added detailed error message.
        }
        //**********************************************************************        

        string strFILENAME = GetKeyData(args, "FILENAME", false);
        // Bug #12055 Mike O - Use misc.Parse to log errors
        string strDEPFILENBR          = misc.Parse<Int64>(GetFileNumber(strFILENAME)).ToString();
        string strDEPFILESEQ          = misc.Parse<Int64>(GetFileSequence(strFILENAME)).ToString();
        string strEVENTNBR = GetKeyData(args, "EVENTNBR", false);
        string strPOSTNBR = GetKeyData(args, "POSTNBR", false);
        string strPOST_TYPE = GetKeyData(args, "POST_TYPE", false);
        string strSEQ_NBR = GetKeyData(args, "SEQ_NBR", false);
        byte[] byteDOC_IMAGE = (byte[])args.get("DOC_IMAGE");
        string strIMAGING_TYPE = GetKeyData(args, "IMAGING_TYPE", false);
        string strCOMPLETE_MICR_LINE = GetKeyData(args, "COMPLETE_MICR_LINE", false);
        string strBANK_ROUTING_NUMBER = GetKeyData(args, "BANK_ROUTING_NUMBER", false);
        string strACCOUNT_NUMBER = GetKeyData(args, "ACCOUNT_NUMBER", false);
        string strCHECK_NUMBER = GetKeyData(args, "CHECK_NUMBER", false);
        string strDOC_TYPE = GetKeyData(args, "DOC_TYPE", false);// Bug# 24736 DH

        // Bug# 19194 DH Add extra validation.
        string strPostID = String.Format("[FN:{0}] - [EN:{1}] - [SN:{2}] - [IT:{3}] - [MICR:{4}]", strFILENAME, strEVENTNBR, strSEQ_NBR, strIMAGING_TYPE, strCOMPLETE_MICR_LINE);

        // Bug# 23195 DH
        if (byteDOC_IMAGE == null || byteDOC_IMAGE.Length == 0)
        {
          Logger.Log($"DEBUG: Posting MISSING image: {strPostID}", "Transuite: SubmitImageToDatabase", "err");
          ThrowCASLErrorButDoNotCloseDBConnection("TS-1253", ref pDBConn_Data, "Attempted to post a MISSING image.", "");
        }
        // end Bug# 23195 DH

        if (byteDOC_IMAGE.Length != 0 && strPOST_TYPE == "TNDR")
        {
          // Bug# 24736 DH
          if (strDOC_TYPE == "MICR" || strDOC_TYPE.Contains("M"))
          {
            if (strIMAGING_TYPE.Contains("FRONT"))
            {
              if (strCOMPLETE_MICR_LINE == null || strCOMPLETE_MICR_LINE == "" || strCOMPLETE_MICR_LINE.Length < 6 || String.Equals(strCOMPLETE_MICR_LINE, "FALSE", StringComparison.InvariantCulture))// Bug# 27153 DH - Clean up warnings when building ipayment
              {
                Logger.Log($"DEBUG: Posting INVALID image: {strPostID}", "Transuite: SubmitImageToDatabase", "err");
                HandleErrorsAndLocking("message", "Invalid check MICR.", "code", "TS-1066", "");
              }
            }
          }
            // end Bug# 24736 DH
        }
        Logger.Log($"Posting image for: {strPostID}", "Transuite: SubmitImageToDatabase", "info");
        // end Bug# 19194 DH Add extra validation. 
        
        // BUG# 8661 DH
        string strDATE_INSERTED = GetKeyData(args, "DATE_INSERTED", false);
        // Bug #12055 Mike O - Use misc.Parse to log errors
        DateTime dt = misc.Parse<DateTime>(strDATE_INSERTED);
        strDATE_INSERTED = String.Format("{0:MM/dd/yyyy}", dt);
        string strUSERID = GetKeyData(args, "USERID", false);
        string strCOMMENTS = GetKeyData(args, "COMMENTS", false);
        string strSOURCE_TYPE = GetKeyData(args, "SOURCE_TYPE", false);
        string strSOURCE_GROUP = GetKeyData(args, "SOURCE_GROUP", false);
        string strSOURCE_REFID = GetKeyData(args, "SOURCE_REFID", false);

        // BUG# 8661 DH
        string strSOURCE_DATE = GetKeyData(args, "SOURCE_DATE", false);
        // Bug #12055 Mike O - Use misc.Parse to log errors
        DateTime dt2 = misc.Parse<DateTime>(strSOURCE_DATE);
        strSOURCE_DATE = String.Format("{0:MM/dd/yyyy h:mm tt}", dt2);

        string strSCAN_DATA = GetKeyData(args, "SCAN_DATA", false);
        string strDOC_IMAGE_REF = GetKeyData(args, "DOC_IMAGE_REF", false);
        string strIMAGE_FORMAT = GetKeyData(args, "IMAGE_FORMAT", false);
        string strDOC_ID = GetKeyData(args, "DOC_ID", false);

        //-------------------------------------------------------
        // Create checksum
        string strChecksum = CreateImageDataChecksum(
          Crypto.DefaultHashAlgorithm, // Bug #10955 Mike O
          "strFileName", strDEPFILENBR + strDEPFILESEQ,
          "strEventNbr", strEVENTNBR,
          "strPostNbr", strPOSTNBR,
          "strPostType", strPOST_TYPE,
          "strSeqNbr", strSEQ_NBR,
          // Convert to Base64 - Validation should do the same.
          "strImage", Convert.ToBase64String(byteDOC_IMAGE),
          "strImagingType", strIMAGING_TYPE,
          "strCompleteMICRLine", strCOMPLETE_MICR_LINE,
          "strBankRoutingNbr", strBANK_ROUTING_NUMBER,
          "strAccountNbr", strACCOUNT_NUMBER,
          "strCheckNbr", strCHECK_NUMBER,
          "strDateInserted", strDATE_INSERTED,
          "strUserID", strUSERID,
          "strComments", strCOMMENTS,
          "strSourceType", strSOURCE_TYPE,
          "strSourceGroup", strSOURCE_GROUP,
          "strSourceRefID", strSOURCE_REFID,
          "strSourceDate", strSOURCE_DATE,
          "strScanData", strSCAN_DATA,
          "strDocImageRef", strDOC_IMAGE_REF,
          "strImageFormat", strIMAGE_FORMAT,
          "strDocID", strDOC_ID);
        //-------------------------------------------------------

        // Format the SQL
        strSQL = "INSERT INTO TG_IMAGE_DATA (";
        strSQL += "DEPFILENBR,";
        strSQL += "DEPFILESEQ,";
        strSQL += "EVENTNBR,";
        strSQL += "POSTNBR,";
        strSQL += "POST_TYPE,";
        strSQL += "SEQ_NBR,";
        strSQL += "DOC_IMAGE,";
        strSQL += "IMAGING_TYPE,";
        strSQL += "COMPLETE_MICR_LINE,";
        strSQL += "BANK_ROUTING_NUMBER,";
        strSQL += "ACCOUNT_NUMBER,";
        strSQL += "CHECK_NUMBER,";
        strSQL += "DATE_INSERTED,";
        strSQL += "USERID,";
        strSQL += "COMMENTS,";
        strSQL += "SOURCE_TYPE,";
        strSQL += "SOURCE_GROUP,";
        strSQL += "SOURCE_REFID,";
        strSQL += "SOURCE_DATE,";
        strSQL += "SCAN_DATA,";
        strSQL += "DOC_IMAGE_REF,";
        strSQL += "IMAGE_FORMAT,";
        strSQL += "DOC_ID,";
        strSQL += "CHECKSUM) VALUES (";

        strSQL += strDEPFILENBR + ",";
        strSQL += strDEPFILESEQ + ",";
        strSQL += strEVENTNBR + ",";
        strSQL += strPOSTNBR + ",";
        strSQL += "'" + strPOST_TYPE + "',";
        strSQL += strSEQ_NBR + ",";

        // SqlClient only supports named parameters.
        // TODO:  @varname is SQLServer specific -- unspecify it
        if (pDBConn_Data.GetDBConnectionType() == ConnectionTypeSynch.m_iCurrentDatabase_SqlServer)
          strSQL += "@ImageBytes,";
        else if (pDBConn_Data.GetDBConnectionType() == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_ODBC ||
          pDBConn_Data.GetDBConnectionType() == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_TNS)
          strSQL += ":ImageBytes,";
        else
          strSQL += "?,";

        strSQL += "'" + strIMAGING_TYPE + "',";
        strSQL += "'" + strCOMPLETE_MICR_LINE + "',";
        strSQL += "'" + strBANK_ROUTING_NUMBER + "',";
        strSQL += "'" + strACCOUNT_NUMBER + "',";
        strSQL += "'" + strCHECK_NUMBER + "',";

        // Need to format dates for ORACLE      
        //to_date('2003/07/09', 'yyyy/mm/dd')  	would return a date value of July 9, 2003.
        if (pDBConn_Data.GetDBConnectionType() == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_TNS ||
          pDBConn_Data.GetDBConnectionType() == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_ODBC)
        {
          strSQL += "to_date('" + strDATE_INSERTED + "', 'mm/dd/yyyy'),";
        }
        else
        {
          strSQL += "'" + strDATE_INSERTED + "',";
        }

        strSQL += "'" + strUSERID + "',";
        strSQL += "'" + strCOMMENTS + "',";
        strSQL += "'" + strSOURCE_TYPE + "',";
        strSQL += "'" + strSOURCE_GROUP + "',";
        strSQL += "'" + strSOURCE_REFID + "',";
        // Need to format dates for ORACLE      
        //to_date('2003/07/09', 'yyyy/mm/dd')  	would return a date value of July 9, 2003.
        if (pDBConn_Data.GetDBConnectionType() == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_TNS ||
          pDBConn_Data.GetDBConnectionType() == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_ODBC)
        {
          strSQL += "to_date('" + strSOURCE_DATE + "', 'mm/dd/yyyy'),";
        }
        else
        {
          strSQL += "'" + strSOURCE_DATE + "',";
        }
        strSQL += "'" + strSCAN_DATA + "',";
        strSQL += "'" + strDOC_IMAGE_REF + "',";
        strSQL += "'" + strIMAGE_FORMAT + "',";
        strSQL += "'" + strDOC_ID + "',";
        strSQL += "'" + strChecksum + "')";

        // Set the param values
        // Genericize this
         if (pDBConn_Data.GetDBConnectionType() == ConnectionTypeSynch.m_iCurrentDatabase_SqlServer)
        {
          SqlParameter param = new SqlParameter();
          param.ParameterName = "@ImageBytes";
          param.SqlDbType = System.Data.SqlDbType.Image;
          param.Direction = ParameterDirection.Input;
          param.Value = byteDOC_IMAGE;
          pDBConn_Data.m_pSqlCmd.Parameters.Add(param);
        }

        // Submit the record
        if (!pDBConn_Data.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strErr))
            HandleErrorsAndLocking("message", "Error executing SQL.", "code", "TS-1065", strErr);//Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix BUG#8622//Bug 11329 NJ-show verbose fix

        //**********************************************************************        
        // ALL GOOD - ACCEPT THE UPDATES.
        if (!args.has("DbConnectionClass"))
        {
          strErr = "";
          if (!pDBConn_Data.CommitToDatabase(ref strErr))
              HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-1066", strErr);//Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix BUG#8622//Bug 11329 NJ-show verbose fix

        }
        //**********************************************************************        

      }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn_Data, connectionOpened);

        // Bug 11661
        ThreadLock.GetInstance().ReleaseLock();
      }

      return true;
    }

    public static bool AddImageRefsToDB(params Object[] arg_pairs)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject a_core_file = args.get("_subject", c_CASL.c_undefined) as GenericObject;
      GenericObject config = args.get("config") as GenericObject;
      GenericObject geoImageList = (GenericObject)a_core_file.get("images");
      GenericObject gTmp = null;
      int iImageCount = geoImageList.getIntKeyLength();
      int iTenderSeqNbr = 0;
      int iTranSeqNbr = 0;
      string strSourceType = "";
      string strSourceGroup = "";
      string strErr = "";
      string strSQL = "";

      // Break the file into file number and sequence number
      string strFileName = GetKeyData(a_core_file, "id", true);
      string strFileNumber = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      //**********************************************************************
      // Create DateTime for ORACLE tables; 
      string strTodayDateTime = CreateTodayDateForOracle();
      //**********************************************************************

      //**********************************************************************        
      ConnectionTypeSynch pDBConn = new ConnectionTypeSynch(config);
      try
      {
      if (!pDBConn.EstablishDatabaseConnection(ref strErr))
          HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-772", strErr);//Bug 13832 - added detailed error message. // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix

      if (!pDBConn.EstablishRollback(ref strErr))
          HandleErrorsAndLocking("message", "Error creating database transaction.", "code", "TS-773", strErr);//Bug 13832 - added detailed error message. // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix

      //-------------------------------------------------------------
      // Set Source Type
      if (geoImageList.has("SOURCE"))
      {
        object pSource = geoImageList.get("SOURCE");
        if (pSource.GetType() == typeof(GenericObject))
        {
          GenericObject pSG = (GenericObject)geoImageList.get("SOURCE");
          strSourceType = (string)pSG.get("_name");
        }
        else
          strSourceType = pSource.ToString();
      }
      //-------------------------------------------------------------

      //-------------------------------------------------------------
      // Set Source Group
      strSourceGroup = geoImageList.get("SOURCE_GROUP").ToString();
      //-------------------------------------------------------------

      //*************************************************************      
      string strT4_EventNbr = "";
      string strT4_TenderNbr = "";
      string strT4_TranNbr = "";
      string strT4_OneStepRefID = "";
      // One-Step
      string strOS_payfile_nbr = "";
      string strOS_ws_nbr = "";
      string strOS_tran_nbr = "";
      string strOS_tran_type = "";
      string strOS_tran_seq = "";
      string strOS_cust_nbr = "";
      string strOS_image_type = "";
      string strOS_MICR = "";
      string strOS_BankRoutingNbr = "";
      string strOS_ACCOUNT_NUMBER = "";
      string strOS_CHECK_NUMBER = "";
      string strOS_DOC_IMAGE_REF = "";
      string strOS_SOURCE_DATE = "";

      // SQL for image insertion
      string strInsertSQL = "";
      strInsertSQL = "INSERT INTO TG_IMAGE_DATA VALUES(";
      //strInsertSQL += "DEPFILENBR,DEPFILESEQ,EVENTNBR,POSTNBR,POST_TYPE,";// Primary key
      //strInsertSQL += "SEQ_NBR,DOC_IMAGE,IMAGING_TYPE,COMPLETE_MICR_LINE,";
      //strInsertSQL += "BANK_ROUTING_NUMBER,ACCOUNT_NUMBER,CHECK_NUMBER,DATE_INSERTED,";
      //strInsertSQL += "USERID,COMMENTS,SOURCE_TYPE,SOURCE_GROUP,SOURCE_REFID,SOURCE_DATE";
      //strInsertSQL += ") VALUES (";

      // Loop through all images and inser one at a time.
      for (int i = 0; i < iImageCount; i++)
      {
        string strSqlData = "";
        strErr = "";
        GenericObject geoImage = (GenericObject)geoImageList.get(i);

        // Needed for retriving event nbr, tender nbr & tran nbr from TG_PAYEVENT_DATA.
        strOS_payfile_nbr = GetKeyData(geoImage, "payfile_nbr", false);
        strOS_ws_nbr = GetKeyData(geoImage, "ws_nbr", false);
        strOS_tran_nbr = GetKeyData(geoImage, "tran_nbr", false);
        strOS_tran_type = GetKeyData(geoImage, "tran_type", false);
        strOS_tran_seq = GetKeyData(geoImage, "tran_seq", false);
        strOS_cust_nbr = GetKeyData(geoImage, "cust_nbr", false);
        strOS_image_type = GetKeyData(geoImage, "image_type", false);
        strOS_MICR = GetKeyData(geoImage, "complete_micr_line", false);
        strOS_BankRoutingNbr = GetKeyData(geoImage, "bank_routing_number", false);
        strOS_ACCOUNT_NUMBER = GetKeyData(geoImage, "account_number", false);
        strOS_CHECK_NUMBER = GetKeyData(geoImage, "check_number", false);
        strOS_DOC_IMAGE_REF = GetKeyData(geoImage, "doc_image_ref", false);
        strOS_SOURCE_DATE = GetKeyData(geoImage, "date_inserted", false);

        //-------------------------------------------------------------
        // Collect some information needed for the TG_PAYEVENT_DATA.
        // If this is a tender image, get the tender nbr for T4
        // If this is a transaction image, get the tran number
        bool bTender = false;
        string strImageType = geoImage.get("image_type").ToString();

        // Re-create T4 event number
        strT4_OneStepRefID = CreateT4PayEventDataRefID_From_OneStep(
          strSourceGroup,
          strOS_cust_nbr);
        strSQL = string.Format("SELECT EVENTNBR,DEPFILENBR,DEPFILESEQ,SOURCE_REFID FROM TG_PAYEVENT_DATA WHERE DEPFILENBR=\'{0}\' AND DEPFILESEQ=\'{1}\' AND SOURCE_REFID=\'{2}\'", strFileNumber, strFileSequenceNbr, strT4_OneStepRefID);
        string error_message = "";
        gTmp = ExecuteSQL(config, strSQL, CommandType.Text, 0, null, 0, 0, null, ref error_message).get(0, null) as GenericObject;
        if (gTmp != null)
          strT4_EventNbr = gTmp.get("EVENTNBR").ToString();
        else
            ThrowCASLErrorButDoNotCloseDBConnection("TS-883", ref pDBConn, "Failed to retrieve EventNbr while posting images to TG_IMAGE_DATA.", error_message);

        if (strImageType == "1"/*check*/ || strImageType == "4"/*deposit*/)
          bTender = true;

        if (bTender)// tender
        {
          iTenderSeqNbr += 1;
          // Format the RefId for tg_tender_data table.
          strT4_OneStepRefID = CreateT4TndrDataRefID_From_OneStep(strSourceGroup,
            strOS_tran_nbr,
            strOS_tran_seq);
          // Bug #12055 Mike O - Use misc.Parse to log errors
          strSQL = string.Format("SELECT TNDRNBR,DEPFILENBR,DEPFILESEQ,SOURCE_REFID FROM TG_TENDER_DATA WHERE DEPFILENBR=\'{0}\' AND DEPFILESEQ=\'{1}\' AND SOURCE_REFID=\'{2}\'", misc.Parse<Int64>(strFileNumber), misc.Parse<Int64>(strFileSequenceNbr), strT4_OneStepRefID);
          gTmp = (GenericObject)GetRecords(config, 0, strSQL, pDBConn.GetDBConnectionType());
          if (gTmp != null)
            strT4_TenderNbr = gTmp.get("TNDRNBR").ToString();
          else
              ThrowCASLErrorButDoNotCloseDBConnection("TS-884", ref pDBConn, "Failed to retrieve EventNbr while posting images to TG_TENDER_DATA.", "");
        }
        else // transaction
        {
          iTranSeqNbr += 1;
          // Format the RefId for tg_tran_data table.
          strT4_OneStepRefID = CreateT4TndrDataRefID_From_OneStep(strSourceGroup,
            strOS_tran_nbr,
            strOS_tran_seq);
          // Bug #12055 Mike O - Use misc.Parse to log errors
          strSQL = string.Format("SELECT TRANNBR,DEPFILENBR,DEPFILESEQ,SOURCE_REFID FROM TG_TRAN_DATA WHERE DEPFILENBR=\'{0}\' AND DEPFILESEQ=\'{1}\' AND SOURCE_REFID=\'{2}\'", misc.Parse<Int64>(strFileNumber), misc.Parse<Int64>(strFileSequenceNbr), strT4_OneStepRefID);
          gTmp = (GenericObject)GetRecords(config, 0, strSQL, pDBConn.GetDBConnectionType());
          if (gTmp != null)
            strT4_TranNbr = gTmp.get("TRANNBR").ToString();
          else
              ThrowCASLErrorButDoNotCloseDBConnection("TS-885", ref pDBConn, "Failed to retrieve EventNbr while posting images to TG_TENDER_DATA.", "");
        }
        //-------------------------------------------------------------
        // Add all deata fields to SQL by position!
        // DEPFILENBR
        AddFieldToSQL(ref strSqlData, strFileNumber, false);
        // DEPFILESEQ
        // Bug #12055 Mike O - Use misc.Parse to log errors
        AddFieldToSQL(ref strSqlData, misc.Parse<Int32>(strFileSequenceNbr).ToString(), false);
        // EVENTNBR
        // Bug #12055 Mike O - Use misc.Parse to log errors
        AddFieldToSQL(ref strSqlData, misc.Parse<Int32>(strT4_EventNbr).ToString(), false);

        if (bTender)
        {
          // POSTNBR
          // Bug #12055 Mike O - Use misc.Parse to log errors
          AddFieldToSQL(ref strSqlData, misc.Parse<Int32>(strT4_TenderNbr).ToString(), false);
          // POST_TYPE
          AddFieldToSQL(ref strSqlData, "TNDR", true);
          // SEQ_NBR
          AddFieldToSQL(ref strSqlData, iTenderSeqNbr.ToString(), false);
        }
        else
        {
          // POSTNBR
          // Bug #12055 Mike O - Use misc.Parse to log errors
          AddFieldToSQL(ref strSqlData, misc.Parse<Int32>(strT4_TranNbr).ToString(), false);
          // POST_TYPE
          AddFieldToSQL(ref strSqlData, "TRAN", true);
          // SEQ_NBR
          AddFieldToSQL(ref strSqlData, iTranSeqNbr.ToString(), false);
        }

        // DOC_IMAGE
        AddFieldToSQL(ref strSqlData, "", false);// N/A
        // IMAGING_TYPE
        AddFieldToSQL(ref strSqlData, strOS_image_type, true);// One-Step
        // COMPLETE_MICR_LINE
        AddFieldToSQL(ref strSqlData, strOS_MICR, true);
        // BANK_ROUTING_NUMBER
        AddFieldToSQL(ref strSqlData, strOS_BankRoutingNbr, true);
        // ACCOUNT_NUMBER
        AddFieldToSQL(ref strSqlData, strOS_ACCOUNT_NUMBER, true);
        // CHECK_NUMBER
        AddFieldToSQL(ref strSqlData, strOS_CHECK_NUMBER, true);
        // DATE_INSERTED - Today
        AddFieldToSQL(ref strSqlData, FormatDateForDB(pDBConn.GetDBConnectionType(), strTodayDateTime), false);
        // USERID
        AddFieldToSQL(ref strSqlData, "auto", true);// N/A
        // COMMENTS
        AddFieldToSQL(ref strSqlData, "auto", true);// N/A
        // SOURCE_TYPE
        AddFieldToSQL(ref strSqlData, strSourceType, true);
        // SOURCE_GROUP
        AddFieldToSQL(ref strSqlData, strSourceGroup, true);
        // SOURCE_REFID
        AddFieldToSQL(ref strSqlData, "not set yet", true);

        // SOURCE_DATE
        // Must first be reformated for T4
        strOS_SOURCE_DATE = ConvertToProperDate(pDBConn.GetDBConnectionType(), strOS_SOURCE_DATE);
        AddFieldToSQL(ref strSqlData, strOS_SOURCE_DATE, false);

        // DOC_IMAGE_REF
        AddFieldToSQL(ref strSqlData, strOS_DOC_IMAGE_REF, true);

        string strFinalSQL = strInsertSQL + strSqlData + ")";

        // Finaly submit the record
        if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strFinalSQL, ref strErr))
            HandleErrorsAndLocking("message", "Error executing SQL." + strErr, "code", "TS-775", strErr);//Bug 13832 - added detailed error message. // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix
      }
      //*************************************************************

      if (!pDBConn.CommitToDatabase(ref strErr))
          HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-776", strErr);//Bug 13832 - added detailed error message. // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix

      return true;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnection(pDBConn);
      }
    }




    public static object GetImageFromOneStep(params Object[] arg_pairs)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      // This function operates on images within the One-Step database.
      // Caller must pass RP_CHECK_DATA criteria to retun a particular image.

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject subject = args.get("_subject") as GenericObject;
      GenericObject config = subject.get("db_connection_info") as GenericObject;
      string strSQL = "", err="";
      object pImageBytes = null;

      string strOSW_PAYFILENBR = GetKeyData(args, "OSW_PAYFILENBR", false);
      string strOSW_WSNBR = GetKeyData(args, "OSW_WSNBR", false);
      string strOSW_TRANSNBR = GetKeyData(args, "OSW_TRANSNBR", false);
      string strOSW_TRANS_TYPE = GetKeyData(args, "OSW_TRANS_TYPE", false);
      string strOSW_TRANS_SEQ = GetKeyData(args, "OSW_TRANS_SEQ", false);
      string strOSW_CUSTNBR = GetKeyData(args, "OSW_CUSTNBR", false);
      string strIMAGE_TYPE = GetKeyData(args, "IMAGE_TYPE", false);

      // Bug 15941
      bool connectionOpened = false;
      ConnectionTypeSynch pDBConn = null;
      try
      {
      //**********************************************************************
      // Must be passed in from the caller.
      if (args.has("DbConnectionClass"))
        pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
      else
      {
        // We have to get a new database connection and later commit ourselves.
        pDBConn = new ConnectionTypeSynch(config);
          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref err)))  // Bug 15941
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-778", err);//Bug 13832 - added detailed error message. // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix
      }
      //**********************************************************************        

      //----------------------------------------
      // NOW RETRIEVE THE IMAGE FROM oNE-sTEP DATABASE.
      strSQL = string.Format("SELECT * FROM RP_CHECK_DATA WHERE OSW_PAYFILENBR={0} AND OSW_WSNBR={1} AND OSW_TRANSNBR={2} AND OSW_TRANS_TYPE={3} AND OSW_TRANS_SEQ={4} AND OSW_CUSTNBR={5}",
        strOSW_PAYFILENBR,
        strOSW_WSNBR,
        strOSW_TRANSNBR,
        strOSW_TRANS_TYPE,
        strOSW_TRANS_SEQ,
        strOSW_CUSTNBR);

      GenericObject gTmp = (GenericObject)GetRecords(config, 0, strSQL, pDBConn.GetDBConnectionType());
      if (gTmp != null)
      {
        if (gTmp.has("DOC_IMAGE"))
          pImageBytes = (object)gTmp.get("DOC_IMAGE");

        if (pImageBytes == null)
        {

        }
      }
      else
      {

        HandleErrorsAndLocking("message", "Unable to retrienve image from One-Step database.", "code", "TS-779", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix
      }

      //----------------------------------------

        return pImageBytes;
      }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static object GetImageFromT4(params Object[] arg_pairs)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      // Optionally pass the connection object in case we are processing multiple images.
      // It is possible topass in a database connection using the "DbConnectionClass" key.
      // Connection must be of type ConnectionTypeSynch class.

      // This function operates on images within the T4 database.
      // Caller must pass TG_IMAGE_DATA criteria to retun a perticullar image or
      // image reference pointer. We will then use the GetImage function to retrieve the 
      // One-Step image.

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = args.get("config") as GenericObject;
      ConnectionTypeSynch pDBConn = null;
      object pImageBytes = null;
      string strSQL = "",err="";
      string strDocImageRefExpression = "";

      // Required fields for the image retrieval
      // These are the primary keys in T4 database.
      string strDEPFILENBR = GetKeyData(args, "DEPFILENBR", false);
      string strDEPFILESEQ = GetKeyData(args, "DEPFILESEQ", false);
      string strEVENTNBR = GetKeyData(args, "EVENTNBR", false);
      string strPOSTNBR = GetKeyData(args, "POSTNBR", false);
      string strPOST_TYPE = GetKeyData(args, "POST_TYPE", false);
      string strSEQ_NBR = GetKeyData(args, "SEQ_NBR", false);

      bool connectionOpened = false;    // Bug 15941
      try
      {
      //**********************************************************************
      // Must be passed in from the caller.
      if (args.has("DbConnectionClass"))
        pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
      else
      {
        // We have to get a new database connection and later commit ourselves.
        pDBConn = new ConnectionTypeSynch(config);
          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref err)))  // Bug 15941
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-780", err);//Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix
      }
      //**********************************************************************        

      // Rrtrieve the actual image data from database.
      // This could also be the reference instead. In that
      // case I will have to use GetImage function to get the image bytes from One-Step database instead.
      // DOC_REF must be executed and GetImage parameters extracted. 
      // Bug #12055 Mike O - Use misc.Parse to log errors
      strSQL = string.Format("SELECT * FROM TG_IMAGE_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} AND POSTNBR={3} AND POST_TYPE=\'{4}\' AND SEQ_NBR={5}",
        misc.Parse<Int32>(strDEPFILENBR),
        misc.Parse<Int32>(strDEPFILESEQ),
        misc.Parse<Int32>(strEVENTNBR),
        misc.Parse<Int32>(strPOSTNBR),
        strPOST_TYPE,
        misc.Parse<Int32>(strSEQ_NBR));
      GenericObject gTmp = (GenericObject)GetRecords(config, 0, strSQL, pDBConn.GetDBConnectionType());
      if (gTmp != null)
      {
        // If image object in table is null,
        // check if the One-Step reference is there. If it is,
        // then execute the expression in reference. That in turn will call the 
        // GetImage function passing all primary keys needed for table to it.
        if (gTmp.has("DOC_IMAGE"))
          pImageBytes = (object)gTmp.get("DOC_IMAGE");

        if (pImageBytes == null)
        {
          if (gTmp.has("DOC_IMAGE_REF"))
            strDocImageRefExpression = gTmp.get("DOC_IMAGE_REF").ToString();

          if (strDocImageRefExpression.Length > 0)
          {
            pImageBytes = (object)misc.CASL_execute_string("_subject", "<do>" + strDocImageRefExpression + "</do>");
          }
          else
            HandleErrorsAndLocking("message", "Missing DOC_IMAGE_REF in TG_IMAGE_DATA.", "code", "TS-781", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix
        }
      }
      else
        HandleErrorsAndLocking("message", "Failed to retrieve an image from TG_IMAGE_DATA.", "code", "TS-782", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix

      // ---------------------------------------------------
      // Now perform any conversions of the image if needed.
      //pImageBytes = pImageBytes;  Bug 12081 NJ- variable assigned to itself-confusing
      // ---------------------------------------------------

      return pImageBytes;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static string ConvertToProperDate(int iDBType, string strDate)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      string strRVDate = "";
      string strTemp = "";

      // "2005-12-20 10:45:56.00"
      // Date part
      string strYear = string.Format("{0:0000}", strDate.Substring(0, 4));
      string strMonth = string.Format("{0:00}", strDate.Substring(5, 2));
      string strday = string.Format("{0:00}", strDate.Substring(8, 2));

      // Time part
      string strHour = string.Format("{0:00}", strDate.Substring(11, 2));
      string strMinute = string.Format("{0:00}", strDate.Substring(14, 2));
      string strSeconds = string.Format("{0:00}", strDate.Substring(17, 2));

      strTemp = string.Format("{0}/{1}/{2} {3}:{4}:{5}", strMonth,
        strday,
        strYear,
        strHour,
        strMinute,
        strSeconds);

      if (iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_ODBC || iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_TNS)// Oracle format: 
      {
        // Final format
        strRVDate = "TO_DATE('" + strTemp + "','MM/DD/YYYY HH24:MI:SS')";
      }
      else// SQL Server & Ms Access
        strRVDate = strTemp;

      return strRVDate;
    }

    public static string FormatDateForDB(int iDBType, string strDate)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      string strDBFormatedDate = "";

      // ORACLE - Date format
      if (iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_ODBC || iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_TNS)
      {
        if (strDate.Length == 10)
          strDate = strDate + " 00:00:00";

        strDBFormatedDate = strDBFormatedDate + "TO_DATE('" + strDate + "','YYYY/MM/DD HH24:MI:SS')";
      }
      else if (iDBType == ConnectionTypeSynch.m_iCurrentDatabase_MsAccess_ODBC || iDBType == ConnectionTypeSynch.m_iCurrentDatabase_SqlServer)
      {
        if (strDate.Length == 10) strDate = strDate + " 00:00:00";
        strDBFormatedDate = strDBFormatedDate + "'" + strDate + "', ";

        strDBFormatedDate = strDBFormatedDate + "'" + strDate + "'";
      }

      return strDBFormatedDate;
    }

    public static void AddFieldToSQL(ref string strSqlData, string strColumnData, bool bMakeDBStringType)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      if (strSqlData.Length > 0)
      {
        if (strSqlData.Substring(strSqlData.Length - 1, 1) != ",")
          strSqlData += ",";
      }

      if (strColumnData.Length > 0)
      {
        // Add single quots to make it database string.
        if (bMakeDBStringType)
          strSqlData += "'" + strColumnData + "'";
        else
          strSqlData += strColumnData;
      }
      else
        strSqlData += "NULL";
    }

    public static string CreateT4TndrDataRefID_From_OneStep(string strSourceGroup,
      string strOS_tran_nbr,
      string strOS_tran_seq)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      string strRefID = "";
      // Bug #12055 Mike O - Use misc.Parse to log errors
      strRefID = string.Format("T{0}/{1:0000}-{2}", strSourceGroup, misc.Parse<Int32>(strOS_tran_nbr),misc.Parse<Int32>(strOS_tran_seq));
      return strRefID; //"T0615761-8/0003-0";
    }

    public static string CreateT4PayEventDataRefID_From_OneStep(string strSourceGroup,
      string strOS_cust_nbr)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      string strRefID = "";
      // Bug #12055 Mike O - Use misc.Parse to log errors
      strRefID = string.Format("R{0}/{1:0000}", strSourceGroup, misc.Parse<Int32>(strOS_cust_nbr));
      return strRefID;
    }

    public static string GetFileNumber(string strFileName)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      return strFileName.Substring(0, 7);
    }

    public static string GetFileSequence(string strFileName)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      // Bug #12055 Mike O - Use misc.Parse to log errors
      return "" + misc.Parse<Int32>(strFileName.Substring(7, strFileName.Length-7));
    }

    /// <summary>
    /// // Added to fix the deadlocking issues. BUG# 5813 DH 07/26/2008
    /// Bug 11661: <b>Removed Mutex release.</b>  Mutexes are now released at a
    /// higher level in a finally block.
    /// </summary>
    /// <param name="strParam1"></param>
    /// <param name="strParam2"></param>
    /// <param name="strParam3"></param>
    /// <param name="strParam4"></param>
    //Bug 11329 NJ-Added inner exception to split error message into friendly and verbose errors
    public static void HandleErrorsAndLocking(string strParam1, string strParam2, string strParam3, string strParam4, string innerException)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!     

      // This is where all CASL errors are routed, this way functionality stays centralized and easier to implement future locking
      // if needed.

      //11322  log serious transuite/database error 
      string error_message = strParam2 + innerException + "(" + strParam4  + ")";
      Logger.LogError(error_message, "Transuite.HandleErrorsAndLocking");     // Bug 25617: Do not swallow these errors with the defaul NLOG level

      /*Bug 11329 NJ-keep the message as long as possible.
       * /Bug 10827 NJ
      //If show verbose error is turned off, clean the message and error code
      bool showVerboseError = (bool)c_CASL.GEO.get("show_verbose_error", false);
      if (!showVerboseError)
      {
          CleanErrorCode(ref strParam2, ref strParam4);
      }
      //end bug 10827*/

      throw CASL_error.create(strParam1, strParam2, strParam3, strParam4, "inner_error", innerException, "user_message", true);// This should be the only call to it in the whole file.
    }

    //Bug 11329 NJ-Added inner exception to split error message into friendly and verbose errors
    public static void ThrowCASLErrorAndCloseDB(string strErrorCode, ref ConnectionTypeSynch pDBConn, string strMessage, string innerException)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!        
      if (pDBConn != null)
        pDBConn.CloseDBConnection();

      HandleErrorsAndLocking("message", strMessage, "code", strErrorCode, innerException);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix
    }

    private static GenericObject MakeCASLErrorAndCloseDB(string title, string code, string message, ref ConnectionTypeSynch pDBConn)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!       
      if (pDBConn != null)
        pDBConn.CloseDBConnection();

      // Bug 11661: Removed ReleaseLock().  This is now done in each finally block.
      // ThreadLock.GetInstance().ReleaseLock(); //Bug# 5813 deadlock fix.

      //Bug 10827 NJ
      //If show verbose error is turned off, clean the message and error code
      bool showVerboseError = (bool)c_CASL.GEO.get("show_verbose_error", false);
      if (!showVerboseError)
      {
        CleanErrorCode(ref code, ref message);
      }
      //end bug 10827
      return misc.MakeCASLError(title, code, message);
    }

    //Bug 11329 NJ-Added inner exception to split error message into friendly and verbose errors
    public static void ThrowCASLErrorButDoNotCloseDBConnection(string strErrorCode, ref ConnectionTypeSynch pDBConn, string strMessage, string innerException)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!        
      // Bug 15941: FT. Close all connections at a higher level  
      //if (pDBConn != null)
      //  pDBConn.CloseDBConnection();

      HandleErrorsAndLocking("message", strMessage, "code", strErrorCode, innerException);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix
    }

    private static GenericObject MakeCASLErrorButDoNOTCloseDB(string title, string code, string message, ref ConnectionTypeSynch pDBConn)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!       
      // Bug 15941. FT. Removed the database close.  This should be done in finally block only!
      //if (pDBConn != null)
      //  pDBConn.CloseDBConnection();

      // Bug 11661: Removed ReleaseLock().  This is now done in each finally block.
      // ThreadLock.GetInstance().ReleaseLock(); //Bug# 5813 deadlock fix.

      //Bug 10827 NJ
      //If show verbose error is turned off, clean the message and error code
      bool showVerboseError = (bool)c_CASL.GEO.get("show_verbose_error", false);
      if (!showVerboseError)
      {
        CleanErrorCode(ref code, ref message);
      }
      //end bug 10827
      return misc.MakeCASLError(title, code, message);
    }

    private static GenericObject MakeCASLErrorButDoNOTCloseDB(string title, string code, string message, ref ConnectionTypeSynch pDBConn, bool user_message)
    {
      // Bug 22923 NK. Override function to display actual message to user instead if general one.

      bool showVerboseError = (bool)c_CASL.GEO.get("show_verbose_error", false);
      if (!showVerboseError)
      {
        CleanErrorCode(ref code, ref message);
      }
      return misc.MakeCASLError(title, code, message, user_message);

    }
    //BUG#6693 ANU
    public static GenericObject CreateAndPostFileToT3(params Object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject DEPFILE = args.get("_subject", c_CASL.c_undefined) as GenericObject;
      GenericObject config = args.get("config") as GenericObject;
      GenericObject data = args.get("data") as GenericObject;

      ////		  #region DEBUG CODE
      //		  string xmlFile = misc.CASL_to_xmli_large("_subject", DEPFILE);
      //		  misc.CASL_call("a_method","log", "args",new GenericObject("DEBUG: DEPFILE in Transuite.CreateAndPostFileToT3()", xmlFile));
      ////		  #endregion


      string strErr = "";
      string strEventNbr = "";
      GenericObject geoTable_TG_PAYEVENT_DATA_Arr = (GenericObject)DEPFILE.get("table_TG_PAYEVENT_DATA");
      int iEventCnt = geoTable_TG_PAYEVENT_DATA_Arr.getIntKeyLength();
      GenericObject geoTranForChecksumUpdate = new GenericObject();

      // We have to get a new database connection and later commit ourselves 
      // in order to ba able to roll back all CoreEvents.
      ConnectionTypeSynch pDBConn = new ConnectionTypeSynch(data);
      try
      {
      if (!pDBConn.EstablishDatabaseConnection(ref strErr))
          HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1249", strErr);//Bug 13832 - added detailed error message.//Bug 11329 NJ-show verbose fix

      if (!pDBConn.EstablishRollback(ref strErr))
          HandleErrorsAndLocking("message", "Error creating database transaction.", "code", "TS-1250", strErr);//Bug 13832 - added detailed error message.//Bug 11329 NJ-show verbose fix

      //Object R = CreateT3File(arg_pairs);
      Object R = CreateT3File("_subject", DEPFILE, "config", config, "data", data, "DbConnectionClass", pDBConn, "session_id", "");
      string FILENAME = DEPFILE.get("FILENAME") as string;

      for (int i = 0; i < iEventCnt; i++)
      {
        GenericObject PAYEVENT = (GenericObject)geoTable_TG_PAYEVENT_DATA_Arr.get(i);
        PAYEVENT.set("FILENAME", FILENAME);

        if (i == 0)// Let PostEventToT3_cs create first event number.
          PostEventToT3_cs("_subject", PAYEVENT, "config", config, "data", data, "geoTranForChecksumUpdate", geoTranForChecksumUpdate, "DbConnectionClass", pDBConn, "EventNbr", "1");
        else
          PostEventToT3_cs("_subject", PAYEVENT, "config", config, "data", data, "geoTranForChecksumUpdate", geoTranForChecksumUpdate, "DbConnectionClass", pDBConn, "EventNbr", strEventNbr);

        DEPFILE.set(i, PAYEVENT);

        // Need to increment event
        strEventNbr = (string)PAYEVENT.get("EVENTNBR");
        long lEventNbr = 0;
            // Bug #12055 Mike O - Use misc.Parse to log errors
            lEventNbr = misc.Parse<Int64>(strEventNbr);
        lEventNbr += 1;
        strEventNbr = string.Format("{0}", lEventNbr);
      }

      if (!pDBConn.CommitToDatabase(ref strErr))
          HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-1251", strErr);//Bug 13832 - added detailed error message.//Bug 11329 NJ-show verbose fix

      // UPDATE FILE and TRAN DATA CHECKSUM
      UpdateTranFileChecksum("data", data, "strFileName", FILENAME);
      UpdateTransactionDataChecksum(geoTranForChecksumUpdate, pDBConn, data);
      return DEPFILE;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnection(pDBConn);
      }
    }
    // Bug#6693 ANU

    public static GenericObject PostFileToT3(params Object[] arg_pairs)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject a_core_file = args.get("_subject", c_CASL.c_undefined) as GenericObject;
      GenericObject config = args.get("config") as GenericObject;
      string strErr = "";
      string strEventNbr = "";
      GenericObject geoTable_TG_PAYEVENT_DATA_Arr = (GenericObject)a_core_file.get("table_TG_PAYEVENT_DATA");
      int iEventCnt = geoTable_TG_PAYEVENT_DATA_Arr.getIntKeyLength();
      GenericObject geoTranForChecksumUpdate = new GenericObject();

      // We have to get a new database connection and later commit ourselves 
      // in order to ba able to roll back all CoreEvents.
      ConnectionTypeSynch pDBConn = new ConnectionTypeSynch(config);
      try
      {
      if (!pDBConn.EstablishDatabaseConnection(ref strErr))
          HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-784", strErr);//Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix

      if (!pDBConn.EstablishRollback(ref strErr))
          HandleErrorsAndLocking("message", "Error creating database transaction.", "code", "TS-785", strErr);//Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix

      for (int i = 0; i < iEventCnt; i++)
      {
        GenericObject a_core_event = (GenericObject)geoTable_TG_PAYEVENT_DATA_Arr.get(i);

        if (i == 0)// Let PostEventToT3_cs create first event number.
          PostEventToT3_cs("_subject", a_core_event, "config", config, "geoTranForChecksumUpdate", geoTranForChecksumUpdate, "DbConnectionClass", pDBConn);
        else
          PostEventToT3_cs("_subject", a_core_event, "config", config, "geoTranForChecksumUpdate", geoTranForChecksumUpdate, "DbConnectionClass", pDBConn, "EventNbr", strEventNbr);

        a_core_file.set(i, a_core_event);

        // Need to increment event
        strEventNbr = (string)a_core_event.get("EVENTNBR");
        long lEventNbr = 0;
        // Bug #12055 Mike O - Use misc.Parse to log errors
        lEventNbr = misc.Parse<Int64>(strEventNbr);
        lEventNbr += 1;
        strEventNbr = string.Format("{0}", lEventNbr);
      }

      if (!pDBConn.CommitToDatabase(ref strErr))
          HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-786", strErr);//Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix

      // UPDATE TRAN DATA CHECKSUM
      UpdateTransactionDataChecksum(geoTranForChecksumUpdate, pDBConn, config);

      return a_core_file;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnection(pDBConn);
      }
    }

    public static GenericObject ExecuteSQL(GenericObject db_connection, string sql_string, CommandType command_type, int query_timeout,
      Hashtable sql_args, int more_data_size, int more_data_timeout, GenericObject object_class,
      ref string error_message)
    {
      GenericObject return_data = new GenericObject();
      IDBVectorReader new_reader = db_reader.Make(db_connection, sql_string, command_type, query_timeout,
        sql_args, more_data_size, more_data_timeout, object_class, ref error_message);
      if (error_message != "")
      {
        return null;
      }
      new_reader.ReadIntoRecordset(ref return_data, 0, 0);
      new_reader.CloseReader();
      return return_data;
    }


    public static int ExecuteNonQuerySQL(GenericObject db_connection, string sql_string, CommandType command_type,
      int query_timeout, Hashtable sql_args, int more_data_size, int more_data_timeout, GenericObject object_class, ref string error_message)
    {
      int return_data = 0;
      IDBVectorReader new_reader = db_reader.Make(db_connection, sql_string, command_type, query_timeout,
        sql_args, more_data_size, more_data_timeout, object_class, ref error_message);
      if (error_message != "")
      {
        return -1;
      }
      return_data = new_reader.ExecuteNonQuery(ref error_message);
      new_reader.CloseReader();
      return return_data;
    }

    /// <summary>
    /// Parameterized version of GetRecords.
    /// I would prefer using HybridDisctionary instead of Hashtable, but 
    /// I have to send it in to db_reader.Make.
    /// I should really convert db_reader.Make to take a HybridDictionary instead of the clunky Hashtable.
    /// Bug 20382: FT
    /// </summary>
    /// <param name="data">Connection information</param>
    /// <param name="sql_args">Name value pairs of parameter name / value</param>
    /// <param name="iFlag">Zero means pull the geo from the vector</param>
    /// <param name="strSQL">The SQL, with @parameter values</param>
    /// <param name="iDBType">Database type.  Only m_iCurrentDatabase_SqlServer is supported</param>
    /// <returns></returns>
    public static GenericObject GetRecordsParameterized(GenericObject data, Hashtable sql_args, int iFlag, string strSQL, int iDBType)
    {
      if (iDBType != ConnectionTypeSynch.m_iCurrentDatabase_SqlServer)
      {
        throw new NotImplementedException("iDBType of " + iDBType + " not supported.");
      }

      string error_message = "";
      IDBVectorReader reader = db_reader.Make(data, strSQL, CommandType.Text, 0, sql_args, 0, 0, null, ref error_message);
      if (error_message != "")
      {
        throw new Exception("Error: cannot read from database: Error: " + error_message + " with this SQL: " + strSQL);
      }
      GenericObject results = new GenericObject();
      reader.ReadIntoRecordset(ref results, 0, 0);

      GenericObject gTmp = null;
      if (results.has(0))
        if (iFlag == 0)
          gTmp = (GenericObject)results.get(0);
        else
          gTmp = (GenericObject)results;

      return gTmp;
    }



    public static GenericObject GetRecords(GenericObject data, int iFlag, string strSQL, int iDBType)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      // Matt -- added int flag  0 - returns the first record
      //			      	     1 - returns all of the records	               
      GenericObject geoDBReturnData = null;
      GenericObject gTmp = null;
      if (iDBType == ConnectionTypeSynch.m_iCurrentDatabase_SqlServer)
      {
        // Handle locking 
        // BUG4410 USE nolock in individual sql statement instead insert it. 
        //        string strNewSQL = strSQL;
        //        strNewSQL = strNewSQL.ToUpper();
        //        int pos = strNewSQL.IndexOf(" FROM ", 0)+5;
        //        int pos2 = strNewSQL.IndexOf(" ", pos+1);
        //        string str1stPart = strNewSQL.Substring(0, pos2);
        //        str1stPart += " (NOLOCK) ";
        //        string str2CondPart = strNewSQL.Substring(pos2+1);
        //        strSQL = str1stPart + str2CondPart;

        db_vector_reader_sql pReader = (db_vector_reader_sql)db_vector_reader_sql.CASL_make("db_connection", data, "sql_statement", strSQL);
        geoDBReturnData = new GenericObject();
        geoDBReturnData = (GenericObject)db_vector_reader_sql.CASL_insert_data("reader", pReader, "_subject", geoDBReturnData, "start", 0, "insert_size", 0);
        if (geoDBReturnData.has(0))
        {
          if (iFlag == 0)
            gTmp = (GenericObject)geoDBReturnData.get(0);
          else
            gTmp = (GenericObject)geoDBReturnData;
        }
        pReader = null;
      }
      //#endregion

      return gTmp;
    }

    public static string GetKeyData(GenericObject geo, object oKeyName, bool bFormatForSQL)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      string strRV = "";
      object oKeyValue = null;
      
      // Bug #12420 Mike O - Set lookup=true to allow default values to be used
      if(geo.has(oKeyName, true))
      {
        // This will be going into the database thru SQL statement.
        // There is no reason why not to convert this to a string.
        // Bug #12453 Mike O - Corrected the arguments
        oKeyValue = geo.get(oKeyName, null, true);
      }

      // Bug #12420 Mike O - Only attempt the conversion if the value is convertible
      if (oKeyValue as IConvertible != null)
        strRV = (string)Convert.ChangeType(oKeyValue, typeof(System.String));

      return strRV;

    }

    /// <summary>
    /// A modern version of GetTableRowCount() that is parameterized
    /// and all the cruft and flab has been cut out.
    /// Bug 20382: FT
    /// </summary>
    /// <param name="pDBConn"></param>
    /// <param name="strSQL"></param>
    /// <param name="sql_args"></param>
    /// <returns></returns>
    public static long GetTableRowCount(ConnectionTypeSynch pDBConn, string strSQL, HybridDictionary sql_args)
    {
      using (SqlCommand paramCommand = ParamUtils.CreateParamCommand(pDBConn, strSQL, sql_args))
      {
        var count = paramCommand.ExecuteScalar();
        return Convert.ToInt64(count);
      }
    }

    // Called by: CreateNewEventNbr
    public static long GetTableRowCount(GenericObject data, string strSQL, int iDBType)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      GenericObject geoDBReturnData = null;
      long lCount = 0;

      if (iDBType == ConnectionTypeSynch.m_iCurrentDatabase_SqlServer)
      {
        db_vector_reader_sql pReader = (db_vector_reader_sql)db_vector_reader_sql.CASL_make("db_connection", data, "sql_statement", strSQL);
        geoDBReturnData = new GenericObject();
        geoDBReturnData = (GenericObject)db_vector_reader_sql.CASL_insert_data("reader", pReader, "_subject", geoDBReturnData, "start", 0, "insert_size", 0);
        GenericObject gTmp = (GenericObject)geoDBReturnData.get(0);
        if (!gTmp.has("COUNT"))
        {
          pReader = null;
          return 0;
        }
        object o = gTmp.get("COUNT");
        // Bug #12055 Mike O - Use misc.Parse to log errors
        lCount = misc.Parse<Int64>(o);

        pReader = null;
      }

      return lCount;
    }

    public static long GetMaxForField(GenericObject data, string strSQL, string strColumn, int iDBType, ref string error_message)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      long lMax = 0;

      GenericObject return_data = new GenericObject();
      IDBVectorReader reader = db_reader.Make(data, strSQL, CommandType.Text, 0, null, 0, 0, null, ref error_message);
      reader.ReadIntoRecordset(ref return_data, 0, 0);

      if (return_data.vectors.Count > 0)
      {
        GenericObject gTmp = (GenericObject)return_data.vectors[0];
        if (gTmp.Contains(strColumn))
        {
          // Bug #12055 Mike O - Use misc.Parse to log errors
          lMax = misc.Parse<Int64>(gTmp[strColumn]);
        }
      }

      return lMax;
    }

    // Bug 22611 UMN remove TG_FILESOURCE_DATA

    /// <summary>
    /// Paramterized and cleaned up version of CreateSQLForInsertInto_TG_DEPFILE_DATA.
    /// This method returns parameterized SQL statement and the sql_args 
    /// dictionary of the parameters, all ready to run.
    /// Bug 23557.
    /// </summary>
    /// <param name="strFileNbr"></param>
    /// <param name="strSequence"></param>
    /// <param name="strFileName"></param>
    /// <param name="strDEPTID"></param>
    /// <param name="strFILETYPE"></param>
    /// <param name="strOPEN_USERID"></param>
    /// <param name="strCREATOR_ID"></param>
    /// <param name="strFILEDESC"></param>
    /// <param name="strAutoBal"></param>
    /// <param name="strEFFECTIVEDT"></param>
    /// <param name="strTodayDateTime"></param>
    /// <param name="strSOURCE_DATE"></param>
    /// <param name="strSOURCE_GROUP"></param>
    /// <param name="strSOURCE_TYPE"></param>
    /// <param name="sql_args"></param>
    /// <returns></returns>
    public static string CreateParameterizedSQLForInsertInto_TG_DEPFILE_DATA(
      string strFileNbr,
      string strSequence,
      string strFileName,
      string strDEPTID,
      string strFILETYPE,
      string strOPEN_USERID,
      string strCREATOR_ID,
      string strFILEDESC,
      string strAutoBal,
      string strEFFECTIVEDT,
      string strTodayDateTime,
      string strSOURCE_DATE,
      string strSOURCE_GROUP,
      string strSOURCE_TYPE,
      HybridDictionary sql_args)
    {
      // Fred wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      string sqlParameterized = @"
        INSERT INTO TG_DEPFILE_DATA
          (DEPFILENBR, DEPFILESEQ, FILENAME, DEPTID, FILETYPE, OPEN_USERID, CREATOR_ID, FILEDESC, 
            AUTOBALANCE, EFFECTIVEDT, OPENDT, BALDT, ACCDT, UPDATEDT, SOURCE_TYPE, 
            SOURCE_GROUP, SOURCE_DATE, CHECKSUM) 
        VALUES(
          @depfilenbr, @depfileseq, @filename, @depid, @fileType, @open_userid, @creator_id, @filedesc,
          @autobalance, @effectivedt, @opendt, @baldt, @accdt, @updatedt, @source_type,
          @source_group, @source_date, @checksum)";

      sql_args["depfilenbr"] = strFileNbr;
      sql_args["depfileseq"] = strSequence;
      sql_args["filename"]   = strFileName;
      sql_args["depid"]      = strDEPTID;
      sql_args["fileType"]   = strFILETYPE;
      sql_args["open_userid"] = strOPEN_USERID;

      // Now do the funky emptry string check.
      // If there is white space, do not turn it into null.  Is this OK?
      if (string.IsNullOrEmpty(strCREATOR_ID))
        sql_args["creator_id"] = null;
      else
        sql_args["creator_id"] = strCREATOR_ID;

      if (string.IsNullOrEmpty(strFILEDESC))
        sql_args["filedesc"] = null;
      else
        sql_args["filedesc"] = strFILEDESC;

      if (string.IsNullOrEmpty(strAutoBal))
        sql_args["autobalance"] = null;
      else
        sql_args["autobalance"] = strAutoBal;

      // If a date is whitespace, consider it null
      if (string.IsNullOrWhiteSpace(strEFFECTIVEDT))
        sql_args["effectivedt"] = null;
      else
        sql_args["effectivedt"] = strEFFECTIVEDT;

      sql_args["opendt"] = strTodayDateTime;

      sql_args["baldt"] = null;
      sql_args["accdt"] = null;
      sql_args["updatedt"] = null;

      
      if (string.IsNullOrEmpty(strSOURCE_TYPE))
        sql_args["source_type"] = null;
      else
        sql_args["source_type"] = strSOURCE_TYPE;

      if (string.IsNullOrEmpty(strSOURCE_GROUP))
        sql_args["source_group"] = null;
      else
        sql_args["source_group"] = strSOURCE_GROUP;

      
      if (string.IsNullOrWhiteSpace(strSOURCE_DATE))
        sql_args["source_date"] = null;
      else
        sql_args["source_date"] = strSOURCE_DATE;

      sql_args["checksum"] = null;      // Will be set later, alas.

      return sqlParameterized;
    }


    

    public static string CreateSQLForInsertInto_TG_PAYEVENT_DATA(string strNewListNbr, string strFileNumber, string strFileSequenceNbr, string strUserID, string strTodayDateTime, string strSourceType, string strSourceGroup, string strSourceRefID, string strSourceDate, string strPayEventChecksum, int iDBType, string strStatus)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      // Bug #8400 Mike O - Added CREATION_DT (when inserting a new event, it's the same as the MOD_DT)
        string strSQL = "INSERT INTO TG_PAYEVENT_DATA WITH (ROWLOCK) (EVENTNBR, DEPFILENBR, DEPFILESEQ, USERID, STATUS, CREATION_DT, MOD_DT, ";
        strSQL += "SOURCE_TYPE, SOURCE_GROUP, SOURCE_REFID, SOURCE_DATE, CHECKSUM";// BUG#13915 Add Active Event User ID 
      //Bug 12835 NJ-add event posting date
        if(!strSourceType.Equals("Cashier"))
        {
            strSQL+=", COMPLETIONDT";
        }
         strSQL+= ") VALUES(";
        //end bug 12835 NJ
         // BUG#13915 Add Active Event User ID, used to prevent two user edit one active event in shopping cart at the same time, last user access shopping cart can post/void tran for active event 
         strSQL += strNewListNbr + ", " + strFileNumber + ", " + strFileSequenceNbr + ", '" + strUserID + "', '" + strStatus + "', ";

      if (iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_ODBC || iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_TNS)
        strSQL = strSQL + "TO_DATE('" + strTodayDateTime + "','MM/DD/YYYY HH24:MI:SS'),";
      else
        strSQL = strSQL + "'" + strTodayDateTime + "',";
      // End Bug #8400 Mike O

      if (iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_ODBC || iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_TNS)
        strSQL = strSQL + "TO_DATE('" + strTodayDateTime + "','MM/DD/YYYY HH24:MI:SS'),";
      else
        strSQL = strSQL + "'" + strTodayDateTime + "',";

      if (strSourceType.Length > 0)
        strSQL = strSQL + "'" + strSourceType + "', ";
      else
        strSQL = strSQL + "NULL, ";

      if (strSourceGroup.Length > 0)
        strSQL = strSQL + "'" + strSourceGroup + "', ";
      else
        strSQL = strSQL + "NULL, ";

      if (strSourceRefID.Length > 0)
        strSQL = strSQL + "'" + strSourceRefID + "', ";
      else
        strSQL = strSQL + "NULL, ";

      if (strSourceDate.Length > 0)
      {
        if (strSourceDate.Length == 10)
          strSourceDate = strSourceDate + " 00:00:00";

        if (iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_ODBC || iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_TNS)
          strSQL = strSQL + "TO_DATE('" + strSourceDate + "','MM/DD/YYYY HH24:MI:SS'), ";
        else
          strSQL = strSQL + "'" + strSourceDate + "', ";
      }
      else
        strSQL = strSQL + "NULL, ";

      //checksum?
      //strSQL = strSQL + "NULL)";
      strSQL = strSQL + "'" + strPayEventChecksum + "' ";

        //Bug 12835 NJ-add event posting date
      if (!strSourceType.Equals("Cashier"))
      {       
      if (iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_ODBC || iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_TNS)
              strSQL = strSQL + ",TO_DATE('" + strTodayDateTime + "','MM/DD/YYYY HH24:MI:SS')";
      else
              strSQL = strSQL + ",'" + strTodayDateTime  +"'";
      }
        strSQL= strSQL+")";
        //end bug 12835 NJ
      return strSQL;
    }


    public static string CreateSQLFor_TG_ITEM_DATA(
      bool IsSQLServer2008, /*BUG# 10600 / 10614 - DH 2/19/2011 Combine statements.*/
      string strFileNumber,
      string strFileSequenceNbr,
      string strEventNbr,
      string strTRANNBR,
      string strITEMACCTID,
      string strITEMID,
      string strTOTAL,
      string strITEMNBR,
      string strTAXED,
      string strAMOUNT,
      string strITEMDESC,
      string strQTY,
      string strGLACCTNBR,
      string strACCTID,
      string strChecksum,
      string strITEMCONFIGID
      )
    {
      string strSQL = "";

      /*BUG# 10600 / 10614 - DH 2/19/2011 Combine statements.*/
      if (!IsSQLServer2008)
      {
        strSQL = "INSERT INTO TG_ITEM_DATA (ITEMNBR, TRANNBR, EVENTNBR, DEPFILENBR, DEPFILESEQ, ITEMID, GLACCTNBR, ";
        strSQL += "ACCTID, ITEMACCTID, ITEMDESC, AMOUNT, QTY, TAXED, TOTAL, CHECKSUM, ITEMCONFIGID) VALUES (";
      }
      else
      {
        strSQL = "(";
      }
      /*BUG# 10600 / 10614 - DH 2/19/2011 Combine statements.*/
      strSQL += strITEMNBR + ", " + strTRANNBR + ", " + strEventNbr + ", " + strFileNumber + ", " + strFileSequenceNbr + ", " + strITEMID;

      if (strGLACCTNBR.Length == 0)
        strSQL = strSQL + ", NULL, ";
      else
        strSQL = strSQL + ", '" + strGLACCTNBR + "', ";

      if (strACCTID.Length == 0)
        strSQL = strSQL + "NULL, ";
      else
        strSQL = strSQL + "'" + strACCTID + "', ";

      if (strITEMACCTID.Length == 0)
        strSQL = strSQL + "NULL, ";
      else
        strSQL = strSQL + "'" + strITEMACCTID + "', ";

      if (strITEMDESC.Length == 0)
        strSQL = strSQL + "NULL, ";
      else
        strSQL = strSQL + "'" + strITEMDESC + "', ";

      strSQL = strSQL + strAMOUNT + ", " + strQTY + ", '" + strTAXED + "', " + strTOTAL + "," + "'" + strChecksum + "'" + ", ";

      if (strITEMCONFIGID.Length == 0)
        strSQL = strSQL + "NULL ) ";
      else
        strSQL = strSQL + "'" + strITEMCONFIGID + "' ) ";

      return strSQL;
    }

    //Called by: PostIndividualTender
    [Obsolete("Use parameterized version", false)]
    public static string CreateSQLFor_TG_TENDER_DATA(
      string strFileNumber,
      string strFileSequenceNbr,
      string strEventNbr,
      string strTNDRID,
      string strSOURCE_DATE,
      string strSOURCE_REFID,
      string strAUTHSTRING,
      string strCCNAME,
      string strBANKROUTINGNBR,
      string strBANKACCTNBR,
      string strAUTHNBR,
      string strTNDRNBR,
      string strCC_CK_NBR,
      string strTNDRDESC,
      string strEXPYEAR,
      string strEXPMONTH,
      string strSYSTEXT,
      string strTYPEIND,			// was: strTNDRIND
      string strAMOUNT,
      string strADDRESS,
      string strAUTHACTION,
      string strTodayDateTime,
      string strSourceType,
      string strSourceGroup,
      string strChecksum,
      int iDBType,
      string strBANKACCTID)
    {
      string strSQL = "INSERT INTO TG_TENDER_DATA (TNDRNBR, EVENTNBR, DEPFILENBR, DEPFILESEQ, TNDRID, TNDRDESC, TYPEIND, AMOUNT, ";
      strSQL += "CC_CK_NBR, EXPMONTH, EXPYEAR, CCNAME, AUTHNBR, AUTHSTRING, AUTHACTION, BANKROUTINGNBR, ";
      strSQL += "BANKACCTNBR, ADDRESS, SYSTEXT, POSTDT, SOURCE_TYPE, SOURCE_GROUP, SOURCE_REFID, SOURCE_DATE, ";
      strSQL += "VOIDDT, CHECKSUM, BANKACCTID) VALUES (";
      strSQL += strTNDRNBR + ", " + strEventNbr + ", " + strFileNumber + ", " + strFileSequenceNbr + ", '" + strTNDRID + "', ";

      if (strTNDRDESC.Length != 0)
        strSQL = strSQL + "'" + strTNDRDESC + "', ";
      else
        strSQL = strSQL + "NULL, ";

      if (strTYPEIND.Length != 0)  // TYPEIND
        strSQL = strSQL + "'" + strTYPEIND + "', ";
      else
        strSQL = strSQL + "NULL, ";

      strSQL = strSQL + strAMOUNT + ", ";

      if (strCC_CK_NBR.Length != 0)
        strSQL = strSQL + "'" + strCC_CK_NBR + "', ";
      else
        strSQL = strSQL + "NULL, ";

      if (strEXPMONTH.Length != 0)
        strSQL = strSQL + "'" + strEXPMONTH + "', '" + strEXPYEAR + "', ";
      else
        strSQL = strSQL + "NULL, NULL, ";

      if (strCCNAME.Length != 0)
        strSQL = strSQL + "'" + strCCNAME + "', ";
      else
        strSQL = strSQL + "NULL, ";

      if (strAUTHNBR.Length != 0)
        strSQL = strSQL + "'" + strAUTHNBR + "', ";
      else
        strSQL = strSQL + "NULL, ";

      if (strAUTHSTRING.Length != 0)
        strSQL = strSQL + "'" + strAUTHSTRING + "', ";
      else
        strSQL = strSQL + "NULL, ";

      if (strAUTHACTION.Length != 0)
        strSQL = strSQL + "'" + strAUTHACTION + "', ";
      else
        strSQL = strSQL + "NULL, ";

      if (strBANKROUTINGNBR.Length != 0)
        strSQL = strSQL + "'" + strBANKROUTINGNBR + "', ";
      else
        strSQL = strSQL + "NULL, ";

      if (strBANKACCTNBR.Length != 0)
        strSQL = strSQL + "'" + strBANKACCTNBR + "', ";
      else
        strSQL = strSQL + "NULL, ";

      if (strADDRESS.Length != 0)
        strSQL = strSQL + "'" + strADDRESS + "', ";
      else
        strSQL = strSQL + "NULL, ";

      if (strSYSTEXT.Length != 0)
        strSQL = strSQL + "'" + strSYSTEXT + "', ";
      else
        strSQL = strSQL + "NULL, ";

      if (iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_ODBC || iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_TNS)
        strSQL = strSQL + "TO_DATE('" + strTodayDateTime + "','MM/DD/YYYY HH24:MI:SS'), ";
      else
        strSQL = strSQL + "'" + strTodayDateTime + "', ";

      if (strSourceType.Length != 0)
        strSQL = strSQL + "'" + strSourceType + "', ";
      else
        strSQL = strSQL + "NULL, ";

      if (strSourceGroup.Length != 0)
        strSQL = strSQL + "'" + strSourceGroup + "', ";
      else
        strSQL = strSQL + "NULL, ";

      if (strSOURCE_REFID.Length != 0)
        strSQL = strSQL + "'" + strSOURCE_REFID + "', ";
      else
        strSQL = strSQL + "NULL, ";

      if (strSOURCE_DATE.Length != 0)
      {
        if (strSOURCE_DATE.Length == 10)
          strSOURCE_DATE = strSOURCE_DATE + " 00:00:00";

        if (iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_ODBC || iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_TNS)
          strSQL = strSQL + "TO_DATE('" + strSOURCE_DATE + "','MM/DD/YYYY HH24:MI:SS'), ";
        else
          strSQL = strSQL + "'" + strSOURCE_DATE + "', ";
      }
      else
        strSQL = strSQL + "NULL, ";

      strSQL = strSQL + "NULL" + ", " + "'" + strChecksum + "'";

      if (strBANKACCTID.Length != 0)
      {
        strSQL = strSQL + ", '" + strBANKACCTID + "')"; // S.X. fix the BANKACCTID as Text field
        //strSQL = strSQL + ", " + strBANKACCTID + ")";
      }
      else
        strSQL = strSQL + ", NULL" + ")";

      return strSQL;
    }

    // Bug 20220: FT: Removed FormatTextForChecksum:  No need to do this with parameterization


    // Bug 20220: FT Removed FormatTextForDB(): No need to escape this anymore with parameterization

    //
    //   public static object test_webservice(params Object[] arg_pairs)
    //   {
    //    GenericObject args = misc.convert_args(arg_pairs);
    //    String a_transaction = args.get("a_string", "") as String;
    //    ipayment_gateway.External_Service a_ipayment_gateway = new ipayment_gateway.External_Service();
    //    return a_ipayment_gateway.test_webservice(a_transaction);
    //   }

    public static object PostEventToT3(params Object[] arg_pairs)
    {

      ////////////////////////////////////////////////////////
      // USE CENTRALIZED WEB SERVICE INSTEAD OF LOCAL CALL
      //return (GenericObject)PostEventToT3_proxy(arg_pairs);

      //////////////////////////////////////////////////////////////////////////
      // use c# code to post
      return PostEventToT3_cs(arg_pairs);

      //////////////////////////////////////////////////////////////////////////
      // use CBT_TG_DB.ocx to post - ODBC, create multiple database connection
      //return PostEventToT3_ocx(arg_pairs);

    }


    public static object PostEventToT3_ocx(params Object[] arg_pairs)
    {
      //			GenericObject args = misc.convert_args(arg_pairs);
      //			GenericObject a_core_event = args.get("_subject", c_CASL.c_undefined) as GenericObject;
      //			GenericObject config = args.get("config", c_CASL.c_undefined) as GenericObject;
      //			//GenericObject geoEventCopy = GenericObject.deepCopy("_subject", a_core_event);
      //
      //			String datasource_name = config.get("datasource_name") as string;
      //			String login_name = config.get("login_name") as string;
      //			String login_password = config.get("login_password") as string;
      //			CBT_TG_DBLib.CBT_TG_DBClass myCtrl = TranSuiteServicesInit("datasource_name",datasource_name, "login_name",login_name, "login_password",login_password);
      //			//			myCtrl = new CBT_TG_DBLib.CBT_TG_DBClass();
      //			//			myCtrl.SetIPayConnectString("TranSuiteDBA", "", "");
      //
      //			bool bRV = myCtrl.SourcePostCoreEvent(a_core_event);
      //			//DON'T USE ->
      //			//geoFileCopy.set("_container", a_core_file.get("_container")); --> Should just replace original
      //			if( bRV == false )
      //			{
      //				m_sErrMsg = myCtrl.GetErrMsg();
      //				HandleErrorsAndLocking("message", m_sErrMsg, "code", "TS-787");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      //			}
      //			myCtrl =null;
      //			//Return the COPY (with changes) if the post was successful
      //			return a_core_event.get("EVENTNBR","");

      return false;
    }

    
    /// <summary>
    /// Create a new Event number using the famous MAX query.
    /// Called By: PostCashieringTransaction
    /// Bug 18766: Parameterization
    /// </summary>
    /// <param name="pDBConn"></param>
    /// <param name="config"></param>
    /// <param name="strFileNumber"></param>
    /// <param name="strFileSequenceNbr"></param>
    /// <returns></returns>
    public static string CreateNewEventNbr(ref ConnectionTypeSynch pDBConn, GenericObject config, string strFileNumber, string strFileSequenceNbr)
    {
      try
      {
        string paramQuery = @"SELECT MAX(EVENTNBR) AS COUNT FROM TG_PAYEVENT_DATA 
          WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq";

        using (SqlCommand maxCommand = new SqlCommand(paramQuery, pDBConn.GetSqlConnection()))
        {
          if (pDBConn.GetSqlTransaction() != null)
          {
            maxCommand.Transaction = pDBConn.GetSqlTransaction();
          }

         
          // Bug 23390
          maxCommand.Parameters.Add("depfilenbr", SqlDbType.Int).Value =  Convert.ToInt32(strFileNumber);
          maxCommand.Parameters.Add("depfileseq", SqlDbType.Int).Value = Convert.ToInt32(strFileSequenceNbr);

          var maxVar = maxCommand.ExecuteScalar();

          // If no event number yet, start at 0
          int max = 0;
          if (maxVar.GetType() != typeof(DBNull))
            max = Convert.ToInt32(maxVar);

          max++;

          return Convert.ToString(max);
    }
      }
      catch (Exception e)
      {
        // Bug 17849
        //Logger.cs_log_trace("Error CreateNewEventNbr", e);
        HandleErrorsAndLocking("message", "CreateNewEventNbr: Error executing SQL.", "code", "TS-805", e.ToString());
        throw;
      }
    }

    /// <summary>
    /// Create new tender number using the infamous MAX query.
    /// Bug 18766: Parameterization
    /// </summary>
    /// <param name="pDBConn"></param>
    /// <param name="config"></param>
    /// <param name="strFileNumber"></param>
    /// <param name="strFileSequenceNbr"></param>
    /// <param name="strEventNbr"></param>
    /// <returns></returns>
    public static string CreateNewTenderNbr(ref ConnectionTypeSynch pDBConn, GenericObject config, string strFileNumber, string strFileSequenceNbr, string strEventNbr)
    {
      try
      {
        string paramQuery = @"SELECT MAX(TNDRNBR) AS COUNT FROM TG_TENDER_DATA 
          WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND EVENTNBR=@eventnbr";

        using (SqlCommand maxCommand = new SqlCommand(paramQuery, pDBConn.GetSqlConnection()))
        {
          if (pDBConn.GetSqlTransaction() != null)
          {
            maxCommand.Transaction = pDBConn.GetSqlTransaction();
          }

    
          // Bug 23390
          maxCommand.Parameters.Add("depfilenbr", SqlDbType.Int).Value = Convert.ToInt32(strFileNumber);
          maxCommand.Parameters.Add("depfileseq", SqlDbType.Int).Value = Convert.ToInt32(strFileSequenceNbr);
          maxCommand.Parameters.Add("eventnbr", SqlDbType.Int).Value = Convert.ToInt32(strEventNbr);

          var maxVar = maxCommand.ExecuteScalar();

          // If no event number yet, start at 0
          int max = 0;
          if (maxVar.GetType() != typeof(DBNull))
            max = Convert.ToInt32(maxVar);

          max++;
          return Convert.ToString(max);
    }
      }
      catch (Exception e)
      {
        HandleErrorsAndLocking("message", "CreateNewEventNbr: Error executing SQL.", "code", "TS-805", e.ToString());
        throw;
      }

    }

    public static string CreateDepositNbr(ref ConnectionTypeSynch pDBConn, GenericObject data, string strDEPOSITID)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!
      // This code is not typesafe nor failsafe

      long lDepositNbr = 0;
      string strNewDepositNbr = "";
      string strSQL;

      strSQL = string.Format("SELECT MAX(DEPOSITNBR) AS COUNT FROM TG_DEPOSIT_DATA WHERE DEPOSITID='{0}'", strDEPOSITID);
      lDepositNbr = GetTableRowCount(data, strSQL, pDBConn.GetDBConnectionType());
      lDepositNbr += 1;
      strNewDepositNbr = string.Format("{0}", lDepositNbr);

      return strNewDepositNbr;
    }

    public struct CoreFileInfoForChecksum
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      public string strFileNumber;
      public string strFileSequenceNbr;
      public string strEventNbr;
      public string strTranNbr;

      public void Init(string strFileNumber, string strFileSequenceNbr, string strEventNbr, string strTranNbr)
      {
        this.strFileNumber = strFileNumber;
        this.strFileSequenceNbr = strFileSequenceNbr;
        this.strEventNbr = strEventNbr;
        this.strTranNbr = strTranNbr;
      }

    }

    public static object PostEventToT3_cs(params Object[] arg_pairs)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      // This function can be called from PostCoreFile. 
      // In that case, connection and database transaction should be established in PostCoreFile
      // and no commits should happen in this function. 
      // PostCoreFile will update the checksums and commit to database. 
      // This way I should be able to roll back all CoreEvents from PostCoreFile.

      // It is possible topass in a database connection using the "DbConnectionClass" key.
      // Connection must be of type ConnectionTypeSynch class.

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject a_core_event = (GenericObject)args.get("_subject", c_CASL.c_undefined);
      GenericObject config = args.get("data", c_CASL.c_undefined) as GenericObject; //bug 6348 changed "config" to "data"
      string strEventNbr = "";
      // Bug #7599 Mike O - Complete status changed from 8 to 5, per spec
      string strStatus = "5"; // System completed
      ConnectionTypeSynch pDBConn = null;

      // Bug 15941. FT. Make sure the database connection is closed
      bool connectionOpened = false;

      ThreadLock.GetInstance().CreateLock();// Sync user's access to this resource. Bug# 5813 deadlock fix. DH 07/30/2008

      try
      { //BUG#6693 need catch unexpected exception and release the lock, otherwise lock is left unintentionally. 
        string strSQL = "";
        string strErr = "";
        GenericObject geoTranForChecksumUpdate = null;


        //**********************************************************************
        // Check if connection passed from PostCoreFile
        if (args.has("DbConnectionClass"))
        {
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
          // Must be set in caller function.
          geoTranForChecksumUpdate = (GenericObject)args.get("geoTranForChecksumUpdate");
        }
        else
        {
          // We have to get a new database connection and later commit ourselves.
          pDBConn = new ConnectionTypeSynch(config);
          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strErr)))  // Bug 15941
              HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-788", strErr);//Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

          if (!pDBConn.EstablishRollback(ref strErr))
              HandleErrorsAndLocking("message", "Error creating database transaction.", "code", "TS-789", strErr);//Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix

          geoTranForChecksumUpdate = new GenericObject();
        }
        //**********************************************************************        

        string strFileName = GetKeyData(a_core_event, "FILENAME", true);
        string strUserID = GetKeyData(a_core_event, "USERID", true);
        string strNbrTenders = GetKeyData(a_core_event, "nbr_tenders", true);
        string strSourceDate = GetKeyData(a_core_event, "SOURCE_DATE", true);
        string strSourceGroup = GetKeyData(a_core_event, "SOURCE_GROUP", true);
        string strSourceRefID = GetKeyData(a_core_event, "SOURCE_REFID", true);
        string strSourceType = GetKeyData(a_core_event, "SOURCE_TYPE", true);
        string strEVENTNBR = GetKeyData(a_core_event, "EVENTNBR", true);
        string strCreationDate = GetKeyData(a_core_event, "CREATION_DT", true);

        DateTime dtDateTime = DateTime.Now;// Will be used later for inserts.

        // Break the file into file number and sequence number
        string strFileNumber = GetFileNumber(strFileName);
        string strFileSequenceNbr = GetFileSequence(strFileName);

        if (!args.has("DbConnectionClass"))//BUG#6693
        {
          //---------------------------------------------
          // Check if file is active:
          // Bug #12055 Mike O - Use misc.Parse to log errors
          long lActiveOrNot = GetTableRowCount(config, string.Format("SELECT COUNT(*) AS COUNT FROM TG_DEPFILE_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND BALDT IS NULL", misc.Parse<Int64>(strFileNumber), misc.Parse<Int64>(strFileSequenceNbr)), pDBConn.GetDBConnectionType());
          if (lActiveOrNot == 0)
            HandleErrorsAndLocking("message", "Transaction File " + strFileNumber + " is not Active!  No changes are permitted.", "code", "TS-791", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix
          //---------------------------------------------
        }

        //***********************************************************************
        // Validate data
        if (strFileName.Length == 0 || strFileName.Length <= 7)
          HandleErrorsAndLocking("message", "Incomplete File number provided to set status!", "code", "TS-792", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix
        //***********************************************************************

        //***********************************************************************
        // Create new event nbr or use the one from PostCoreFile
        strEventNbr = "";
        if (!args.has("EventNbr"))
        {
          // In this case, event must be controled from PostCoreFile.
          strEventNbr = CreateNewEventNbr(ref pDBConn, config, strFileNumber, strFileSequenceNbr);
        }
        else
          strEventNbr = args.get("EventNbr").ToString();
        //***********************************************************************

        //**********************************************************************
        // Create DateTime for ORACLE tables; 
        string strTodayDateTime = CreateTodayDateForOracle();
        //**********************************************************************

        //**********************************************************************
        // Create SQL for insert into TG_PAYEVENT_DATA table and execute it.
      // Bug #12055 Mike O - Use misc.Parse to log errors
      DateTime dtDate = misc.Parse<DateTime>(strTodayDateTime);
        string strPostDT = dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo);

        //Matt -- replacing the old checksum call
        GenericObject gTmp = new GenericObject("USERID", strUserID, "STATUS", strStatus, "MOD_DT", strPostDT, "CREATION_DT", strCreationDate);
        // Bug #10955 Mike O
        string strPayEventChecksum = CreatePayEventChecksum(Crypto.DefaultHashAlgorithm, "strEventNbr", strEventNbr, "strFileName", strFileName, "geoChecksumData", gTmp);


        strSQL = CreateSQLForInsertInto_TG_PAYEVENT_DATA(strEventNbr, strFileNumber, strFileSequenceNbr, strUserID, strTodayDateTime, strSourceType, strSourceGroup, strSourceRefID, strSourceDate, strPayEventChecksum, pDBConn.GetDBConnectionType(), strStatus);
        strErr = "";
        if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strErr))
            HandleErrorsAndLocking("message", "Error executing SQL.", "code", "TS-793", strErr);//Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix

        // Update event.
        a_core_event.set("event_MOD_DT", strPostDT);
        //**********************************************************************

        //***********************************************************************
        // TRANSACTION DATA GLOBAL
        string strError = "";

        GenericObject geoTableTGTranDataArr = (GenericObject)a_core_event.get("table_TG_TRAN_DATA");
        for (int i = 0; i < geoTableTGTranDataArr.getIntKeyLength(); i++)
        {
          // Get each transaction data element in the array.
          GenericObject a_transaction = (GenericObject)geoTableTGTranDataArr.get(i);
          GenericObject pRV = null;
          try
          {
            a_transaction.set("LOGIN_ID", strUserID);//BUG#13915 add transaction POSTUSERID 
            pRV = (GenericObject)PostIndividualTransaction(
              ref strError,
              "data", config,
              "DbConnectionClass", pDBConn,
              "_subject", a_transaction,
              "file_FILENAME", strFileName,
              "event_EVENTNBR", strEventNbr,
              "SOURCE_GROUP", strSourceGroup,
              "SOURCE_TYPE", strSourceType,
              "geoTranForChecksumUpdate", geoTranForChecksumUpdate);
          }
          catch (Exception e)
          {
            // Bug 17849 hanleErrorAndLocking threw the exception
            //Logger.cs_log_trace("Error PostEventToT3_cs", e);

            // DEBUG FUNCTION
            string error_message = ConstructDetailedError(e); //Bug 13832-detailed error message
            HandleErrorsAndLocking("message", "Unable to post transaction.", "code", "TS-794", strError + error_message);//Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix
          }
          if (pRV == null)
          {
              HandleErrorsAndLocking("message", "Unable to post transaction. ", "code", "TS-795", strError);//Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix //Bug 11329 NJ-show verbose fix
          }
        }
        //***********************************************************************

        //***********************************************************************
        // TENDER DATA GLOBAL
        GenericObject geoTableTGTenderDataArr = (GenericObject)a_core_event.get("table_TG_TENDER_DATA");
        int iTenderItems = (int)geoTableTGTenderDataArr.getIntKeyLength();
        for (int r = 0; r < iTenderItems; r++)
        {
          GenericObject a_tender = (GenericObject)geoTableTGTenderDataArr.get(r);
          a_tender.set("LOGIN_ID", strUserID);//BUG#13915 add tender post userid 
          a_tender = (GenericObject)PostIndividualTender(
            "data", config, // BUG# 5856 DH 08/08/08
            "DbConnectionClass", pDBConn,
            "_subject", a_tender,
            "file_FILENAME", strFileName,
            "event_EVENTNBR", strEventNbr,
            "SOURCE_GROUP", strSourceGroup,
            "SOURCE_TYPE", strSourceType);
          if (a_tender == null)
          {
            HandleErrorsAndLocking("message", "Unable to process tender.", "code", "TS-796", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix
          }

          // Bug 25434 MJO - Image support
          GenericObject images = a_tender.get("images", new GenericObject()) as GenericObject;

          for (int s = 0; s < images.getLength(); s++)
          {
            GenericObject image = images.get(s) as GenericObject;

            SubmitImageToDatabase(
              "data", c_CASL.c_object("TranSuite_DB.Data.db_connection_info"),
              "FILENAME", strFileName,
              "EVENTNBR", strEventNbr,
              "POSTNBR", r + 1,
              "POST_TYPE", "TNDR",
              "SEQ_NBR", s,
              "DOC_IMAGE", image.get("content"),
              "IMAGE_FORMAT", image.get("content_type"),
              "DOC_ID", image.get("doc_id"),
              "DOC_TYPE", image.get("doc_type"),
              "DOC_IMAGE_REF", image.get("str_message_to_save"),
              "IMAGING_TYPE", image.get("doc_type"),
              "USERID", strUserID,
              "SOURCE_TYPE", "Gateway",
              "DATE_INSERTED", DateTime.Now.ToShortDateString(),
              "SOURCE_DATE", DateTime.Now.ToShortDateString()
            );
          }
        }
        //***********************************************************************

        // ALL GOOD - ACCEPT THE UPDATES.
        if (!args.has("DbConnectionClass"))
        {
          strErr = "";
          if (!pDBConn.CommitToDatabase(ref strErr))
            HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-797", strErr);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix

          // UPDATE TRAN DATA CHECKSUM
          UpdateTransactionDataChecksum(geoTranForChecksumUpdate, pDBConn, config);
        }
        //*********************************************************************

        a_core_event.set("EVENTNBR", strEventNbr);
      }
            //Bug#6693 ANU 
      finally   // Bug 11661
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
        //Bug#6693 ANU 
        ThreadLock.GetInstance().ReleaseLock(); //Bug# 5813 deadlock fix. DH 07/30/2008
      }

      return strEventNbr;
    }

    public static object PostTTAMap(params Object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject data = args.get("data", c_CASL.c_undefined) as GenericObject;
      ConnectionTypeSynch pDBConn_Data = null;

      bool connectionOpened = false;    // Bug 15941
      ThreadLock.GetInstance().CreateLock();
      // Bug 11661: Wrap mutex release in finally block
      try
      {
        string strSQL = "";
        string strErr = "";

        //**********************************************************************
        // Must be passed in from the caller.
        if (args.has("DbConnectionClass"))
          pDBConn_Data = (ConnectionTypeSynch)args.get("DbConnectionClass");
        else
        {
          // We have to get a new database connection and later commit ourselves.
          pDBConn_Data = new ConnectionTypeSynch(data);
          if (!(connectionOpened = pDBConn_Data.EstablishDatabaseConnection(ref strErr)))  // Bug 15941
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1063", strErr);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix

          if (!pDBConn_Data.EstablishRollback(ref strErr))
          {
            ThrowCASLErrorButDoNotCloseDBConnection("TS-1064", ref pDBConn_Data, "Error creating database transaction.", strErr);
          }
        }
        //**********************************************************************        

        string strEVENTNBR = GetKeyData(args, "EVENTNBR", false);
        string strDEPFILENBR = GetKeyData(args, "DEPFILENBR", false);
        string strDEPFILESEQ = GetKeyData(args, "DEPFILESEQ", false);

        string strTTAMAPNBR = GetKeyData(args, "TTAMAPNBR", false);

        string strCREDITNBR = GetKeyData(args, "CREDITNBR", false);
        string strCREDITITEMNBR = GetKeyData(args, "CREDITITEMNBR", false);
        string strDEBITNBR = GetKeyData(args, "DEBITNBR", false);
        string strDEBITITEMNBR = GetKeyData(args, "DEBITITEMNBR", false);

        string strGLACCTNBR = GetKeyData(args, "GLACCTNBR", false);
        string strAMOUNT = GetKeyData(args, "AMOUNT", false);

        //-------------------------------------------------------
        // Create checksum
        /*string strChecksum = strTTAMAPNBR;
        strChecksum += strEVENTNBR;
        strChecksum += strDEPFILENBR;
        strChecksum += strDEPFILESEQ;

        strChecksum += strCREDITNBR;
        strChecksum += strCREDITITEMNBR;
        strChecksum += strDEBITNBR;
        strChecksum += strDEBITITEMNBR;

        strChecksum += strGLACCTNBR;
        strChecksum += strAMOUNT;

        strChecksum = CreateChecksum("data", strChecksum);
        */

        // Format the SQL
        strSQL = "INSERT INTO TG_TTA_MAP_DATA (";
        strSQL += "TTAMAPNBR,";
        strSQL += "EVENTNBR,";
        strSQL += "DEPFILENBR,";
        strSQL += "DEPFILESEQ,";

        strSQL += "CREDITNBR,";
        strSQL += "CREDITITEMNBR,";
        strSQL += "DEBITNBR,";
        strSQL += "DEBITITEMNBR,";

        strSQL += "GLACCTNBR,";
        strSQL += "AMOUNT) VALUES (";

        strSQL += strTTAMAPNBR + ",";
        strSQL += strEVENTNBR + ",";
        strSQL += strDEPFILENBR + ",";
        strSQL += strDEPFILESEQ + ",";

        strSQL += strCREDITNBR + ",";
        if (strCREDITITEMNBR.Length == 0) { strSQL += "NULL" + ","; }
        else { strSQL += strCREDITITEMNBR + ","; }
        strSQL += strDEBITNBR + ",";
        if (strDEBITITEMNBR.Length == 0) { strSQL += "NULL" + ","; }
        else { strSQL += strDEBITITEMNBR + ","; }

        if (strGLACCTNBR.Length == 0) { strSQL += "NULL" + ","; }
        else { strSQL += "'" + strGLACCTNBR + "',"; }
        strSQL += strAMOUNT + ")";

        // Submit the record
        try
        {
          // Bug 18766: Parameterization
          string sql_string =
            @"INSERT INTO TG_TTA_MAP_DATA 
                (TTAMAPNBR,EVENTNBR,DEPFILENBR,DEPFILESEQ,CREDITNBR,CREDITITEMNBR,
                DEBITNBR,DEBITITEMNBR,GLACCTNBR,AMOUNT) 
            VALUES 
              (@ttamapnbr, @eventnbr , @depfilenbr, @depfileseq, @creditnbr, @credititemnbr,
              @debitnbr, @debititemnbr,@glaccountnbr,@amount)";

          HybridDictionary sql_args = new HybridDictionary();
          sql_args["ttamapnbr"] = strTTAMAPNBR;
          sql_args["eventnbr"] = strEVENTNBR;
          sql_args["depfilenbr"] = strDEPFILENBR;
          sql_args["depfileseq"] = strDEPFILESEQ;
          sql_args["creditnbr"] = strCREDITNBR;

          if (strCREDITITEMNBR.Length == 0)
            sql_args["credititemnbr"] = null;
          else
            sql_args["credititemnbr"] = strCREDITITEMNBR;

          sql_args["debitnbr"] = strDEBITNBR;

          if (strDEBITITEMNBR.Length == 0)
            sql_args["debititemnbr"] = null;
          else
            sql_args["debititemnbr"] = strDEBITITEMNBR;

          if (strGLACCTNBR.Length == 0)
            sql_args["glaccountnbr"] = null;
          else
            sql_args["glaccountnbr"] = strGLACCTNBR;

          sql_args["amount"] = strAMOUNT;

          int count = ParamUtils.runExecuteNonQuery(pDBConn_Data, sql_string, sql_args);
          if (count != 1)
          {
            HandleErrorsAndLocking("message", "PostTTAMap: Inserted: " + count + " records instead of 1", "code", "TS-1116b", "Wrong insert count");
          }

        }
        catch (SqlException sqlException)
        {
          // Bug 27011: Log duplicate error and keep running.  Do not throw an error
          if (sqlException.Number == 2627 || sqlException.Number == 2601) // Cannot insert duplicate key row in object error
          {
            string traceback = sqlException.ToString();
            string recordInfo = "strDEPFILENBR|" + strDEPFILENBR
              + " strDEPFILESEQ|" + strDEPFILESEQ
              + " strEVENTNBR|" + strEVENTNBR
              + " strTTAMAPNBR|" + strTTAMAPNBR;
            Logger.LogError("Duplicate TTA_MAP entry error: " + recordInfo + ' ' + traceback, "PostTTAMap");

            try
            {
              string application_name = "Cashiering";
              if (CASLInterpreter.get_my() != null && CASLInterpreter.get_my().has("application_name"))
                application_name = CASLInterpreter.get_my()["application_name"] as string;

              string login_user = (CASLInterpreter.get_my() as GenericObject).get("login_user", "") as string;

              string cleanedupTrace = traceback.Replace(':', '|').Replace(',', ';');
              string detail = "record:" + recordInfo + ",Trace:" + cleanedupTrace;
              misc.CASL_call("a_method", "actlog", "args",
                new GenericObject("user", login_user,
                  "app", application_name,
                  "action", "PostTTAMap",
                  "summary", "Duplicate TTA_MAP record",
                  "detail", detail));
            }
            catch (Exception e)
            {
              // Bug 17849 hanleErrorAndLocking threw the exception
              //Logger.cs_log_trace("Error PostTTAMap", e);
              Logger.LogError("Duplicate TTA_MAP entry error: error in activity log write: " + e.Message, "PostTTAMap");
            }
            // Bug 27011: Now continue on our merry way and do this show the user an error

          }
          else
            HandleErrorsAndLocking("message", "SQL Error posting TTA mapping.", "code", "TS-1065", sqlException.ToString());// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix
        }

        catch (Exception e)
        {
            //Bug 13832-improved  error message
          HandleErrorsAndLocking("message", "Non-SQL Error posting TTA mapping.", "code", "TS-1065", e.ToString());// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix
        }



        //**********************************************************************        
        // ALL GOOD - ACCEPT THE UPDATES.
        if (!args.has("DbConnectionClass"))
        {
          strErr = "";
          if (!pDBConn_Data.CommitToDatabase(ref strErr))
            HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-1066", strErr);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix
        }
        //**********************************************************************        

      }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn_Data, connectionOpened);

        // Bug 11661
        ThreadLock.GetInstance().ReleaseLock();
      }

      return true;
    }

    public static object PostReversalEvent(params Object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject data = args.get("data", c_CASL.c_undefined) as GenericObject;
      ConnectionTypeSynch pDBConn_Data = null;

      bool connectionOpened = false;    // Bug 15941
      ThreadLock.GetInstance().CreateLock();
      // Bug 11661: Release mutex in finally block
      try
      {

        string strSQL = "";
        string strErr = "";

        //**********************************************************************
        // Must be passed in from the caller.
        if (args.has("DbConnectionClass"))
          pDBConn_Data = (ConnectionTypeSynch)args.get("DbConnectionClass");
        else
        {
          // We have to get a new database connection and later commit ourselves.
          //TODO: Change error codes
          pDBConn_Data = new ConnectionTypeSynch(data);
          if (!(connectionOpened = pDBConn_Data.EstablishDatabaseConnection(ref strErr)))  // Bug 15941
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1063", strErr);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix

          if (!pDBConn_Data.EstablishRollback(ref strErr))
            ThrowCASLErrorButDoNotCloseDBConnection("TS-1064", ref pDBConn_Data, "Error creating database transaction.", strErr);
        }
        //**********************************************************************        

        string strEVENTNBR = GetKeyData(args, "EVENTNBR", false);
        string strDEPFILENBR = GetKeyData(args, "DEPFILENBR", false);
        string strDEPFILESEQ = GetKeyData(args, "DEPFILESEQ", false);

        string strUSERID = GetKeyData(args, "USERID", false);

        string strREVERSED_EVENTNBR = GetKeyData(args, "REVERSED_EVENTNBR", false);
        string strREVERSED_DEPFILENBR = GetKeyData(args, "REVERSED_DEPFILENBR", false);
        string strREVERSED_DEPFILESEQ = GetKeyData(args, "REVERSED_DEPFILESEQ", false);
        //string strSourceType             = GetKeyData(args, "FILE_SOURCE_TYPE", false); //BUG 5395

        bool bVOIDED = (bool)args.get("VOIDED", false);

        //BUG 5395 Prevent multiple session user login BLL 5/2/08 (ActivateSuspendedEvent)

        //-------------------------------------------------------
        // Create checksum
        // Bug #10955 Mike O
        string strChecksum = CreateReversalEventChecksum(Crypto.DefaultHashAlgorithm, "strEventNbr", strEVENTNBR, "strFileName", strDEPFILENBR + strDEPFILESEQ,
          "strUserID", strUSERID, "strReversedEventNbr", strREVERSED_EVENTNBR, "strReversedFileName", strREVERSED_DEPFILENBR + strREVERSED_DEPFILESEQ, "strVoided", (bVOIDED) ? "TRUE" : "FALSE");

        // Format the SQL
        strSQL = "INSERT INTO TG_REVERSAL_EVENT_DATA (";
        strSQL += "EVENTNBR,";
        strSQL += "DEPFILENBR,";
        strSQL += "DEPFILESEQ,";

        strSQL += "USERID,";

        strSQL += "REVERSED_EVENTNBR,";
        strSQL += "REVERSED_FILENBR,";
        strSQL += "REVERSED_FILESEQ,";

        strSQL += "VOIDED,";
        strSQL += "CHECKSUM) VALUES (";

        strSQL += strEVENTNBR + ",";
        strSQL += strDEPFILENBR + ",";
        strSQL += strDEPFILESEQ + ",";

        strSQL += "'" + strUSERID + "',";

        strSQL += strREVERSED_EVENTNBR + ",";
        strSQL += strREVERSED_DEPFILENBR + ",";
        strSQL += strREVERSED_DEPFILESEQ + ",";

        strSQL += (bVOIDED) ? "1" : "0" + ",";
        strSQL += "'" + strChecksum + "')";

        // Submit the record
        if (!pDBConn_Data.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strErr))
          HandleErrorsAndLocking("message", "Error executing SQL." + strErr, "code", "TS-1065", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix

        //**********************************************************************        
        // ALL GOOD - ACCEPT THE UPDATES.
        if (!args.has("DbConnectionClass"))
        {
          strErr = "";
          if (!pDBConn_Data.CommitToDatabase(ref strErr))
            HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-1066", strErr);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix4//Bug 11329 NJ-show verbose fix
        }
        //**********************************************************************        

      }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn_Data, connectionOpened);

        // Bug 11661
        ThreadLock.GetInstance().ReleaseLock();
      }

      return true;
    }

    //Bug#6786 ANU Added this method to update the TG_REVERSAL_EVENT_DATA for voiding reversed event.
    public static object PostReversalVoid(params Object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject data = args.get("data", c_CASL.c_undefined) as GenericObject;
      ConnectionTypeSynch pDBConn_Data = null;
      bool connectionOpened = false;        // Bug 15941

      ThreadLock.GetInstance().CreateLock();
      // Bug 11661: Release mutex in finally block
      try
      {
        //string strSQL = "";// Bug# 27153 DH - Clean up warnings when building ipayment
        string strErr = "";

        //**********************************************************************
        // Must be passed in from the caller.
        if (args.has("DbConnectionClass"))
          pDBConn_Data = (ConnectionTypeSynch)args.get("DbConnectionClass");
        else
        {
          // We have to get a new database connection and later commit ourselves.
          //TODO: Change error codes
          pDBConn_Data = new ConnectionTypeSynch(data);
          if (!(connectionOpened = pDBConn_Data.EstablishDatabaseConnection(ref strErr)))  // Bug 15941
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1063", strErr);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix

          if (!pDBConn_Data.EstablishRollback(ref strErr))
            HandleErrorsAndLocking("message", "Error creating database transaction.", "code", "TS-1064", strErr);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix

        }
        //**********************************************************************        

        string strEVENTNBR = GetKeyData(args, "EVENTNBR", false);
        string strDEPFILENBR = GetKeyData(args, "DEPFILENBR", false);
        string strDEPFILESEQ = GetKeyData(args, "DEPFILESEQ", false);

        string strUSERID = GetKeyData(args, "USERID", false);

        string strREVERSED_EVENTNBR = GetKeyData(args, "REVERSED_EVENTNBR", false);
        string strREVERSED_DEPFILENBR = GetKeyData(args, "REVERSED_DEPFILENBR", false);
        string strREVERSED_DEPFILESEQ = GetKeyData(args, "REVERSED_DEPFILESEQ", false);
        //string strSourceType             = GetKeyData(args, "FILE_SOURCE_TYPE", false); //BUG 5395

        bool bVOIDED = (bool)args.get("VOIDED", false);

        // Bug #12518 Mike O - VOIDED is a bit column, so use a bit value to set it, not all caps strings
        string strVoided = bVOIDED ? "1" : "0";

        //BUG 5395 Prevent multiple session user login BLL 5/2/08 (ActivateSuspendedEvent)




        //-------------------------------------------------------
        // Create checksum
        // Bug #10955 Mike O
        string strChecksum = CreateReversalEventChecksum(Crypto.DefaultHashAlgorithm, "strEventNbr", strEVENTNBR, "strFileName", strDEPFILENBR + strDEPFILESEQ,
          "strUserID", strUSERID, "strReversedEventNbr", strREVERSED_EVENTNBR, "strReversedFileName", strREVERSED_DEPFILENBR + strREVERSED_DEPFILESEQ, "strVoided", strVoided);

        string sqlParameterized = @"
          UPDATE TG_REVERSAL_EVENT_DATA SET USERID=@userid, VOIDED = @voided, CHECKSUM = @checksum 
          WHERE EVENTNBR = @eventnbr AND DEPFILENBR = @depfilenbr AND DEPFILESEQ = @depfileseq 
          AND REVERSED_EVENTNBR = @reversed_eventnbr AND REVERSED_FILENBR = @reversed_filenbr AND REVERSED_FILESEQ = @reversed_fileseq";


         HybridDictionary sql_args = new HybridDictionary();
        sql_args["userid"]   = strUSERID;
        sql_args["voided"]   = strVoided;
        sql_args["checksum"] = strChecksum;

        sql_args["eventnbr"]   = strEVENTNBR;
        sql_args["depfilenbr"] = strDEPFILENBR;
        sql_args["depfileseq"] = strDEPFILESEQ;

        sql_args["reversed_eventnbr"] = strREVERSED_EVENTNBR;
        sql_args["reversed_filenbr"]  = strREVERSED_DEPFILENBR;
        sql_args["reversed_fileseq"]  = strREVERSED_DEPFILESEQ;


        using (SqlCommand paramCommand = ParamUtils.CreateParamCommand(pDBConn_Data, sqlParameterized, sql_args))
        {
          paramCommand.ExecuteNonQuery();
        }

        //**********************************************************************        
        // ALL GOOD - ACCEPT THE UPDATES.
        if (!args.has("DbConnectionClass"))
        {
          strErr = "";
          if (!pDBConn_Data.CommitToDatabase(ref strErr))
            HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-1066", strErr);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix4//Bug 11329 NJ-show verbose fix
        }
        //**********************************************************************        

      }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn_Data, connectionOpened);

        // Bug 11661
        ThreadLock.GetInstance().ReleaseLock();
      }

      return true;
    }
    //Bug#6786 end 

    public static GenericObject PostCashieringTender(params Object[] arg_pairs)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      // Bug #6467 Mike O - Perform license check
      string error_message = "";
      GenericObject license = c_CASL.c_object("License.data") as GenericObject;
      if (!ValidateLicense(ref error_message, license))
        return CreateErrorObject("TS-1067", "Invalid license.\n" + error_message, false, "Invalid License", true);//Bug 11329 NJ-always show verbose

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject a_tender = (GenericObject)args.get("_subject", c_CASL.c_undefined);
      GenericObject config = args.get("config", c_CASL.c_undefined) as GenericObject;
      GenericObject data = args.get("data", c_CASL.c_undefined) as GenericObject;
      string session_id = args.get("session_id") as string;
      string strErr = "";
      string strStatus = "1"; // Active
      ConnectionTypeSynch pDBConn = new ConnectionTypeSynch(data);

      ThreadLock.GetInstance().CreateLock();// Sync user's access to this resource. Bug# 5813 deadlock fix. DH 07/30/2008
      // Bug 11661: Wrap mutex release in finally block
      try
      {
        if (!pDBConn.EstablishDatabaseConnection(ref error_message))
          HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-798", error_message);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        if (!pDBConn.EstablishRollback(ref strErr))
        {
          HandleErrorsAndLocking("message", "Error creating database transaction.", "code", "TS-799", strErr + error_message);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        string strFileName = a_tender.get("file_FILENAME", "").ToString();
        string strEventNbr = a_tender.get("event_EVENTNBR", "").ToString();
        string strEventUserID = a_tender.get("event_USERID", "").ToString();
        string strFileSourceType = a_tender.get("file_SOURCE_TYPE", "").ToString();
        string strFileSourceGroup = a_tender.get("file_SOURCE_GROUP", "").ToString();
        string strSOURCE_REFID = a_tender.get("SOURCE_REFID", "").ToString();
        string strSOURCE_DATE = a_tender.get("SOURCE_DATE", "").ToString();
        string strLOGIN_ID = (string)a_tender.get("LOGIN_ID", ""); //BUG 5395v2 (PostCashieringTender)

        // Break the file into file number and sequence number
        string strFileNumber = GetFileNumber(strFileName);
        string strFileSequenceNbr = GetFileSequence(strFileName);

        // Posting Date
        string strTodayDateTime = CreateTodayDateForOracle();
      // Bug #12055 Mike O - Use misc.Parse to log errors
      DateTime dtDate = misc.Parse<DateTime>(strTodayDateTime);

        //***********************************************************************
        // Create new event nbr or use the one passed in.
        if (strEventNbr.Length == 0)
        {
          // BUG#5396 File has been closed. You must log into a new file
          if (!IsFileClosed(strFileNumber, strFileSequenceNbr, data, ref pDBConn))
            ThrowCASLErrorButDoNotCloseDBConnection("TS-1204", ref pDBConn, "Transaction File " + strFileNumber + strFileSequenceNbr.PadLeft(3, '0') + " has been locked!  You must log into a new file. ", "");

        // Bug #12055 Mike O - Use misc.Parse to log errors
        strEventNbr = CreateNewEventNbr(ref pDBConn, data, misc.Parse<Int64>(strFileNumber).ToString(), misc.Parse<Int64>(strFileSequenceNbr).ToString());
          // New number will be sent back to GUI.
          a_tender.set("event_EVENTNBR", strEventNbr);
          //-------------------------------------------------

          //**********************************************************************
          // Create SQL for insert into TG_PAYEVENT_DATA table and execute it.

          //Matt -- replacing the old checksum call
          GenericObject gTmp = new GenericObject("USERID", strEventUserID, "STATUS", strStatus, "MOD_DT", dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo));
          // Bug #10955 Mike O
          string strPayEventChecksum = CreatePayEventChecksum(Crypto.DefaultHashAlgorithm, "strEventNbr", strEventNbr, "strFileName", strFileName, "geoChecksumData", gTmp);

          string strSQL = CreateSQLForInsertInto_TG_PAYEVENT_DATA(strEventNbr, strFileNumber, strFileSequenceNbr, strEventUserID, strTodayDateTime, strFileSourceType, strFileSourceGroup, strSOURCE_REFID, strSOURCE_DATE, strPayEventChecksum, pDBConn.GetDBConnectionType(), strStatus);
          strErr = "";
          if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strErr))
          {
            // BUG# 9940 DH - Must close allocated connection.
            HandleErrorsAndLocking("message", "Error executing SQL.", "code", "TS-800", strErr);//Bug 13832-added inner exception // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
          }

          // Update the tender
          a_tender.set("event_MOD_DT", strTodayDateTime);
        }
        //***********************************************************************

        //**************************************************************
        // Postb the tender transaction
        a_tender = (GenericObject)PostIndividualTender(
          "data", data,
          "DbConnectionClass", pDBConn,
          "_subject", a_tender,
          "file_FILENAME", strFileName,
          "event_EVENTNBR", strEventNbr,
          "SOURCE_GROUP", strFileSourceGroup,
          "SOURCE_TYPE", strFileSourceType);
        if (a_tender == null)
        {
          HandleErrorsAndLocking("message", "Unable to process tender.", "code", "TS-801", error_message);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
        //**************************************************************

        strErr = "";
        if (!pDBConn.CommitToDatabase(ref strErr))
        {
          HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-802", strErr + error_message);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        // Update tender.
        a_tender.set("POSTDT", strTodayDateTime);

      }
      finally
      {
        // BUG# 9940 DH - Must close allocated connection.
        // Close the database connection and dispose of pDBConn class.
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnection(pDBConn);

        // Bug 11661
        ThreadLock.GetInstance().ReleaseLock(); //Bug# 5813 deadlock fix. DH 07/30/2008
      }

      return a_tender;
    }

    public static GenericObject PostIndividualTender(params Object[] arg_pairs)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      // It is possible topass in a database connection using the "DbConnectionClass" key.
      // Connection must be of type ConnectionTypeSynch class.

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject a_tender = (GenericObject)args.get("_subject", c_CASL.c_undefined);
      GenericObject data = args.get("data", c_CASL.c_undefined) as GenericObject;
      string strErr = "";
      ConnectionTypeSynch pDBConn = null;
      bool connectionOpened = false;      // Bug 15941


      ThreadLock.GetInstance().CreateLock();// Sync user's access to this resource. Bug# 5813 deadlock fix. DH 07/30/2008
      // Bug 11661: Wrap mutex release in finally block
      try
      {
        DateTime dtDate = DateTime.Now;// Will be used later for inserts.

        // Check if connection was passed from another function.
        if (args.has("DbConnectionClass"))
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
        else
        {
          // We have to get a new database connection and later commit.
          pDBConn = new ConnectionTypeSynch(data);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strErr)))  // Bug 15941
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-803", strErr);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

          if (!pDBConn.EstablishRollback(ref strErr))
          {
            HandleErrorsAndLocking("message", "Error creating database transaction.", "code", "TS-804", strErr);//Bug 13832-added inner exception // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
          }
        }

        //**********************************************************************
        // Create DateTime for ORACLE tables; 
        string strTodayDateTime = CreateTodayDateForOracle();
        //**********************************************************************

        //Matt -- Took out the file_ in front of SOURCE_TYPE/GROUP
        string strFileName = args.get("file_FILENAME", "").ToString();
        string strEventNbr = args.get("event_EVENTNBR", "").ToString();
        string strFileSourceType = args.get("SOURCE_TYPE", "").ToString();
        string strFileSourceGroup = args.get("SOURCE_GROUP", "").ToString();

        // Break the file into file number and sequence number
        string strFileNumber = GetFileNumber(strFileName);
        string strFileSequenceNbr = GetFileSequence(strFileName);

        string strTNDRID = GetKeyData(a_tender, "TNDRID", false);
        string strSOURCE_DATE = GetKeyData(a_tender, "SOURCE_DATE", false);
        string strSOURCE_REFID = GetKeyData(a_tender, "SOURCE_REFID", false);
        string strAUTHSTRING = GetKeyData(a_tender, "AUTHSTRING", false);
        string strCCNAME = GetKeyData(a_tender, "CCNAME", false);
        string strBANKROUTINGNBR = GetKeyData(a_tender, "BANKROUTINGNBR", false);
        string strBANKACCTNBR = GetKeyData(a_tender, "BANKACCTNBR", false);
        string strAUTHNBR = GetKeyData(a_tender, "AUTHNBR", false);
        string strTNDRNBR = GetKeyData(a_tender, "TNDRNBR", false);
        string strCC_CK_NBR = GetKeyData(a_tender, "CC_CK_NBR", false);
        string strTNDRDESC = GetKeyData(a_tender, "TNDRDESC", false);
        string strEXPYEAR = "";// removed for Bug#5187 GetKeyData(a_tender, "EXPYEAR", false);				
        string strEXPMONTH = "";// removed for Bug#5187 GetKeyData(a_tender, "EXPMONTH", false);			
        string strSYSTEXT = GetKeyData(a_tender, "SYSTEXT", false);
        string strTYPEIND = GetKeyData(a_tender, "TYPEIND", false);
        string strAMOUNT = GetKeyData(a_tender, "AMOUNT", false);
        string strADDRESS = GetKeyData(a_tender, "ADDRESS", false);
        string strAUTHACTION = GetKeyData(a_tender, "AUTHACTION", false);
        string strAUTHACTION2 = GetKeyData(a_tender, "IDC_T4_CORE_ITEM", false);
        string strBR_BANK_ACCT_ID = GetKeyData(a_tender, "BR_BANK_ACCT_ID", false);
        string strLOGINID = GetKeyData(a_tender, "LOGIN_ID", false);//TTS13915 
        string strTENDER_CATEGORY = GetKeyData(a_tender, "TNDRCATEGORY", false);// Bug# 24024 DH
        string strCHECK_RTN_VALIDATION_DIGITS = GetKeyData(a_tender, "CHECK_RTN_VALIDATION_DIGITS", false);// Bug 27423 DH - Add a configurable list of valid first position digits in the routing transit numbers-->

        //******************************************************
        // Create new tender number if one was not passed in.
        if (strTNDRNBR.Length == 0)
        {
          strTNDRNBR = CreateNewTenderNbr(ref pDBConn, data, strFileNumber, strFileSequenceNbr, strEventNbr);
          a_tender.set("TNDRNBR", strTNDRNBR);
        }
        //******************************************************
        // Matt -- making sure that the POSTDT is set for the structure and replacing old checksum call
        a_tender.set("POSTDT", dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo));

        //-------------------------------------------------------------------------------------------------------------------------------------
        // Bug# 19194/24024 DH
        if (strTYPEIND == "4")
        {
          // Bug 27423 DH - Add a configurable list of valid first position digits in the routing transit numbers
          if (strCHECK_RTN_VALIDATION_DIGITS != null && strCHECK_RTN_VALIDATION_DIGITS != "")
          {
            // Check if the routing number starts with any of the allowed digits
            string c = "";
            if (strBANKROUTINGNBR.Length > 0)
              c = strBANKROUTINGNBR[0].ToString();

            if(!strCHECK_RTN_VALIDATION_DIGITS.Contains(c))
            {
              HandleErrorsAndLocking("message", "Check routing number failed first-digit validation.", "code", "TS-1066", "");// Keeping the same error code
            }
          }
          // end Bug 27423 DH
                           
            // Bug 27423 - Add a configurable list of valid first position digits in the routing transit numbers.

            // Encrypt the routing number for security.
            GenericObject encrypted_bank_number = Crypto.symmetric_encrypt("data", strBANKROUTINGNBR, "secret_key", null, "is_ascii_string", true);
            byte[] eb = (byte[])encrypted_bank_number.get("data");
            string encrypted_brn = Convert.ToBase64String((byte[])eb);
            string strPostID = String.Format("[FN:{0}] - [EN:{1}] - [TI:{2}] - [TNBR:{3}] - [BAN:{4}] - [BRN:{5}] - [CHK:{6}]", strFileName, strEventNbr, strTNDRID, strTNDRNBR, strBANKACCTNBR, encrypted_brn, strCC_CK_NBR);

          // Bug# 24024 - New flow per Jeff, customers will use different categories to avoid parts of this validation for certain check tenders.
          // I'm keeping the ICL and ACH separated below to have the option to change them later.

          //------ECHECK = ROUTING NUMBER, ACCOUNT
          //------ACH = ROUTING NUMBER, ACCOUNT, CHECK NBR
          //------ICL = ROUTING NUMBER, ACCOUNT, CHECK NBR, FRONT IMAGE, BACK IMAGE

          // ECHECK = ROUTING NUMBER, ACCOUNT
          if (strTENDER_CATEGORY == "Check_ECheck")
          {
            // ROUTING NUMBER
            if (strBANKROUTINGNBR == null || string.Equals(strBANKROUTINGNBR, "FALSE", StringComparison.InvariantCulture) || strBANKROUTINGNBR == "" || strBANKROUTINGNBR.Length < 9)// Bug# 27153 DH - Clean up warnings when building ipayment
            {
              Logger.Log($"DEBUG: Posting invalid bank routing number for ECHECK check {strPostID}", "Transuite: PostIndividualTender", "err");
              HandleErrorsAndLocking("message", "Invalid routing number for ECHECK check.", "code", "TS-1066", "");
            }

            // ACCOUNT NBR
            if (strBANKACCTNBR == null || string.Equals(strBANKACCTNBR, "FALSE", StringComparison.InvariantCulture) || strBANKACCTNBR == "" || strBANKACCTNBR.Length < 3)// Bug# 27153 DH - Clean up warnings when building ipayment
            {
              Logger.Log($"DEBUG: Posting invalid bank account number for ECHECK check {strPostID}", "Transuite: PostIndividualTender", "err");
              HandleErrorsAndLocking("message", "Invalid bank number for ECHECK check.", "code", "TS-1066", "");
            }            
          }
          else // ACH = ROUTING NUMBER, ACCOUNT, CHECK NBR
          if (strTENDER_CATEGORY == "Check_ACH")
          {
            // ROUTING NUMBER
            if (strBANKROUTINGNBR == null || string.Equals(strBANKROUTINGNBR, "FALSE", StringComparison.InvariantCulture) || strBANKROUTINGNBR == "" || strBANKROUTINGNBR.Length < 9)// Bug# 27153 DH - Clean up warnings when building ipayment
            {
              Logger.Log($"DEBUG: Posting invalid bank routing number for ACH check {strPostID}", "Transuite: PostIndividualTender", "err");
              HandleErrorsAndLocking("message", "Invalid routing number for ACH check.", "code", "TS-1066", "");
            }

            // ACCOUNT NBR
            if (strBANKACCTNBR == null || string.Equals(strBANKACCTNBR, "FALSE", StringComparison.InvariantCulture) || strBANKACCTNBR == "" || strBANKACCTNBR.Length < 3)// Bug# 27153 DH - Clean up warnings when building ipayment
            {
              Logger.Log($"DEBUG: Posting invalid bank account number for ACH check {strPostID}", "Transuite: PostIndividualTender", "err");
              HandleErrorsAndLocking("message", "Invalid bank number for ACH check.", "code", "TS-1066", "");
            }

            // CHECK NBR
            if (strCC_CK_NBR == null || string.Equals(strCC_CK_NBR, "FALSE", StringComparison.InvariantCulture) || strCC_CK_NBR == "" || strCC_CK_NBR.Length < 1)// Bug# 27153 DH - Clean up warnings when building ipayment
            {
              Logger.Log($"DEBUG: Posting invalid check number for ACH check {strPostID}", "Transuite: PostIndividualTender", "err");
              HandleErrorsAndLocking("message", "Invalid check number for ACH check.", "code", "TS-1066", "");
            }
          }
          else // ICL = ROUTING NUMBER, ACCOUNT, CHECK NBR
          if (strTENDER_CATEGORY == "Check_ICL")
          {
            // ROUTING NUMBER
            if (strBANKROUTINGNBR == null || string.Equals(strBANKROUTINGNBR, "FALSE", StringComparison.InvariantCulture) || strBANKROUTINGNBR == "" || strBANKROUTINGNBR.Length < 9)// Bug# 27153 DH - Clean up warnings when building ipayment
            {
              Logger.Log($"DEBUG: Posting invalid bank routing number for ICL check {strPostID}", "Transuite: PostIndividualTender", "err");
              HandleErrorsAndLocking("message", "Invalid routing number for ICL check.", "code", "TS-1066", "");
            }

            // ACCOUNT NBR
            if (strBANKACCTNBR == null || string.Equals(strBANKACCTNBR, "FALSE", StringComparison.InvariantCulture) || strBANKACCTNBR == "" || strBANKACCTNBR.Length < 3)// Bug# 27153 DH - Clean up warnings when building ipayment
            {
              Logger.Log($"DEBUG: Posting invalid bank account number for ICL check {strPostID}", "Transuite: PostIndividualTender", "err");
              HandleErrorsAndLocking("message", "Invalid bank number for ICL check.", "code", "TS-1066", "");
            }

            // CHECK NBR
            if (strCC_CK_NBR == null || string.Equals(strCC_CK_NBR, "FALSE", StringComparison.InvariantCulture) || strCC_CK_NBR == "" || strCC_CK_NBR.Length < 1)// Bug# 27153 DH - Clean up warnings when building ipayment
            {
              Logger.Log($"DEBUG: Posting invalid check number for ICL check {strPostID}", "Transuite: PostIndividualTender", "err");
              HandleErrorsAndLocking("message", "Invalid check number for ICL check.", "code", "TS-1066", "");
            }
          }
        }
        // end Bug# 19194/24024 DH
        //-------------------------------------------------------------------------------------------------------------------------------------

        // Bug #10955 Mike O
        string strChecksum = CreateTenderChecksum(Crypto.DefaultHashAlgorithm, "strEventNbr", strEventNbr, "strFileName", strFileName, "strTndrNbr", strTNDRNBR, "geoChecksumData", a_tender);

        //strAUTHSTRING = ""; BUG#5094 BUG#5958 
        try
        {
          // Bug 18766: Parameterization
          string sql_string =
            @"INSERT INTO TG_TENDER_DATA 
               (TNDRNBR, EVENTNBR, DEPFILENBR, DEPFILESEQ, TNDRID, TNDRDESC, TYPEIND, AMOUNT, 
                CC_CK_NBR, EXPMONTH, EXPYEAR, CCNAME, AUTHNBR, AUTHSTRING, AUTHACTION, BANKROUTINGNBR, 
                BANKACCTNBR, ADDRESS, SYSTEXT, POSTDT, SOURCE_TYPE, SOURCE_GROUP, SOURCE_REFID, 
                SOURCE_DATE, VOIDDT, CHECKSUM, BANKACCTID,POSTUSERID ) 
              VALUES 
                (@tndrnbr, @eventnbr, @depfilenbr, @depfileseq, @tndrid, @tndrdesc, @typeind, @amount, 
                @cc_ck_nbr, @expmonth, @expyear, @ccname, @authnbr, @authstring, @authaction, @bankrouting, 
                @bankacctnbr, @address, @systext, @postdt, @source_type, @source_group, @source_refid, 
                @source_date, @voiddt, @checksum, @bankacctid,@postuserid)";

          HybridDictionary sql_args = new HybridDictionary();

          sql_args["tndrnbr"] = strTNDRNBR;
          sql_args["eventnbr"] = strEventNbr;
          sql_args["depfilenbr"] = strFileNumber;
          sql_args["depfileseq"] = strFileSequenceNbr;
          sql_args["tndrid"] = strTNDRID;
          ParamUtils.setField(sql_args, "tndrdesc", strTNDRDESC);

          ParamUtils.setField(sql_args, "typeind", strTYPEIND);
          sql_args["amount"] = strAMOUNT;

          ParamUtils.setField(sql_args, "cc_ck_nbr", strCC_CK_NBR);
          ParamUtils.setField(sql_args, "expmonth", strEXPMONTH);
          ParamUtils.setField(sql_args, "expyear", strEXPYEAR);
          ParamUtils.setField(sql_args, "ccname", strCCNAME);
          ParamUtils.setField(sql_args, "authnbr", strAUTHNBR);
          ParamUtils.setField(sql_args, "authstring", strAUTHSTRING);
          ParamUtils.setField(sql_args, "authaction", strAUTHACTION);
          ParamUtils.setField(sql_args, "bankrouting", strBANKROUTINGNBR);

          ParamUtils.setField(sql_args, "bankacctnbr", strBANKACCTNBR);
          ParamUtils.setField(sql_args, "address", strADDRESS);
          ParamUtils.setField(sql_args, "systext", strSYSTEXT);
          sql_args["postdt"] = strTodayDateTime;
          ParamUtils.setField(sql_args, "source_type", strFileSourceType);
          ParamUtils.setField(sql_args, "source_group", strFileSourceGroup);
          ParamUtils.setField(sql_args, "source_refid", strSOURCE_REFID);

          ParamUtils.setField(sql_args, "source_date", strSOURCE_DATE);
          sql_args["voiddt"] = null;
          sql_args["checksum"] = strChecksum;
          ParamUtils.setField(sql_args, "bankacctid", strBR_BANK_ACCT_ID);
          sql_args["postuserid"] = strLOGINID;//TTS13915

          using (SqlCommand paramCommand = ParamUtils.CreateParamCommand(pDBConn, sql_string, sql_args))
        {
            paramCommand.ExecuteNonQuery();
        }

        }
        catch (Exception e)
        {
          // Bug 17849 hanleErrorAndLocking threw the exception
          //Logger.cs_log_trace("Error PostIndividualTender", e);
          HandleErrorsAndLocking("message", "PostIndividualTender: Error executing SQL.", "code", "TS-805", e.ToString());
        }

        // Update tender
        a_tender.set("POSTDT", dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo));

        try
        {
          // Add the tender custom fields.
          if (!AddTenderCustomFields("_subject", a_tender,
            "data", data,
            "DbConnectionClass", pDBConn,
            "file_FILENAME", strFileName,
            "event_EVENTNBR", strEventNbr,
            "tender_TNDRNBR", strTNDRNBR,
            "post_TNDRID", strTNDRID))
          {
            HandleErrorsAndLocking("message", "Unable to process custom field for tender.", "code", "TS-806", strErr);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
          }
        }
        catch (Exception e)
        {
          HandleErrorsAndLocking("message", "Unable to commit tender changes to database.", "code", "TS-807", strErr + ConstructDetailedError(e));// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        // ALL GOOD - ACCEPT THE UPDATES.
        if (!args.has("DbConnectionClass"))// Caller will commit.
        {
          strErr = "";
          if (!pDBConn.CommitToDatabase(ref strErr))
          {
            HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-808", strErr);//Bug 13832-added inner exception// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
          }

        }

      }
      finally
      {
        // BUG# 9940 DH - Must close allocated connection.
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);

        // Bug 11661
        ThreadLock.GetInstance().ReleaseLock(); //Bug# 5813 deadlock fix. DH 07/30/2008
      }

      return a_tender;
    }

    public static bool AddTenderCustomFields(params Object[] arg_pairs)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject pTender = args.get("_subject", c_CASL.c_undefined) as GenericObject;
      GenericObject data = args.get("data", c_CASL.c_undefined) as GenericObject;
      GenericObject pCustKeys = null;
      string strErr = "";
      string strSQL = "";
      string strFileNumber = "";
      string strFileSequenceNbr = "";
      string strFILENAME = "";
      string strEVENTNBR = "";
      string strPOSTNBR = "";
      string strPOSTID = "";
      ConnectionTypeSynch pDBConn = null;
      string strSQLUnionData = "";/*BUG# 10600 & 12054 - DH 2/19/2011 Combine statements*/

      // Bug 18766: Parameterization
      List<List<KeyValuePair<string, string>>> paramList = new List<List<KeyValuePair<string, string>>>();
      int paramCount = 0;
      // End Bug 16540

      bool connectionOpened = false;        // Bug 15941
      try
      {

      // It is possible topass in a database connection using the "DbConnectionClass" key.
      // Connection must be of type ConnectionTypeSynch class.

      // If there are no custom keys, just get out.
      if (!pTender.has("_custom_keys"))
        return true;

      // Check if function called directly from CASL or is part of posting process.
      // If part of posting then it should have a DB connection already and will commit in the caller function.
      if (args.has("DbConnectionClass"))
        pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
      else
      {
        // If we are here, this function was called from CASL.
        // We have to get a new database connection and later commit.
        pDBConn = new ConnectionTypeSynch(data);
          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strErr)))  // Bug 15941
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-809", strErr);//Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        if (!pDBConn.EstablishRollback(ref strErr))
        {
          HandleErrorsAndLocking("message", "Error creating database transaction.", "code", "TS-810", strErr);//Bug 13832-added inner exception // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
      }

      //***************************************************************************

      // These are pased in to level to make easier to post cust fields directly without tender object.
      strFILENAME = GetKeyData(args, "file_FILENAME", false);
      strEVENTNBR = GetKeyData(args, "event_EVENTNBR", false);
      strPOSTNBR = GetKeyData(args, "tender_TNDRNBR", false);
      strPOSTID = GetKeyData(args, "post_TNDRID", false);
      pCustKeys = (GenericObject)pTender.get("_custom_keys", c_CASL.c_error, true);

      // Break the file into file number and sequence number
      strFileNumber = GetFileNumber(strFILENAME);
      strFileSequenceNbr = GetFileSequence(strFILENAME);
      pCustKeys = (GenericObject)pTender.get("_custom_keys", c_CASL.c_error, true);
      //***************************************************************************

      //****************************************************************************
      // LOOP THROUGH ALL TRANSACTION CUSTOM KEYS DATA
      long lScreenIndex = 0;
      int iCustKeysInTransaction = (int)pCustKeys.getIntKeyLength();

 	  /*BUG# 10600 - DH 2/19/2011 Combine statements.*/
      bool IsSQLServer2008orLater = pDBConn.IsServer_SQL2008orLater();
	  
      // Bug 12276 & 12309 DH - Get out if there are no custom keys
      if (iCustKeysInTransaction == 0)
        return true;

      for (int x = 0; x < iCustKeysInTransaction; x++)
      {
        // Get Cust Keys        
        string strTAG = GetKeyData(pCustKeys, x, false);
        string strID = GetKeyData(pTender, strTAG + "_f_id", false);
        string strValue = GetKeyData(pTender, strTAG, false);
        string strLabel = GetKeyData(pTender, strTAG + "_f_label", false);
        string strAttr = GetKeyData(pTender, strTAG + "_f_attributes", false); // BUG3861 S.X. add CUST_FIELD_DATA.CUSTATTR
        string strStoreDB = GetKeyData(pTender, strTAG + "_f_store_in_DB", false); // Bug#6702 ANU - To check the custom field to be stored in DB

        strStoreDB = strStoreDB.ToUpper(); // Bug #13951 UMN

        // Bug #11711 Mike O - For fake CFs, use the tagname as the id
        if (strID.Equals(""))
          strID = strTAG;


        // Get the next screen index.
        /* Bug#7984 To remove max call for screen index 
        if(pDBConn.GetDBConnectionType() == ConnectionTypeSynch.m_iCurrentDatabase_SqlServer)
          strSQL = string.Format("SELECT MAX(SCREENINDEX) AS MAX FROM CUST_FIELD_DATA (NOLOCK) WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} AND POSTNBR={3}", Int64.Parse(strFileNumber), Int64.Parse(strFileSequenceNbr), Int64.Parse(strEVENTNBR), Int64.Parse(strPOSTNBR));
        else
          strSQL = string.Format("SELECT MAX(SCREENINDEX) AS MAX FROM CUST_FIELD_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} AND POSTNBR={3}", Int64.Parse(strFileNumber), Int64.Parse(strFileSequenceNbr), Int64.Parse(strEVENTNBR), Int64.Parse(strPOSTNBR));

         * */
        //string error_message=""; 12081 NJ-unused variable
        /* Bug#7984 To remove max call for screen index 
        if(lScreenIndex == 0)// Do only for the first field.
          lScreenIndex = GetMaxForField(data, strSQL, "MAX", pDBConn.GetDBConnectionType(), ref error_message);
         * */

        lScreenIndex += 1;
        string strScreenIndex = string.Format("{0}", lScreenIndex);

        pCustKeys = (GenericObject)pCustKeys.set("SCREENINDEX", strScreenIndex);
        pCustKeys = (GenericObject)pCustKeys.set("CUSTLABEL", strLabel);
        pCustKeys = (GenericObject)pCustKeys.set("CUSTVALUE", strValue);
        // Matt -- replacing the old checksum call
        // Bug #10955 Mike O
        string strChecksum = CreateCustomFieldChecksum(Crypto.DefaultHashAlgorithm, "geoChecksumData", pCustKeys, "strEventNbr", strEVENTNBR, "strPostNbr", strPOSTNBR, "strFileName", strFILENAME, "strScreenIndex", strScreenIndex);
        //-----------------------------------------

        // Insert custom fields into CUST_FIELD_DATA.
        if (strStoreDB == "TRUE" && !String.IsNullOrEmpty(strValue)) //Bug#6702/13951 UMN
        {
          // Bug 23645 MJO - Only use batch if there aren't too many parameters and hardcode whether batch should be used
          // NOTE: If the number of parameters is changed in the future, the parameter count needs to be updated
          if (IsSQLServer2008orLater && iCustKeysInTransaction * 13 <= 2100)
          {
              /*BUG# 10600 & 12054 - DH 2/19/2011 Combine statements*/
              if (x > 0 && x < iCustKeysInTransaction && strSQLUnionData != "" /*Bug 12054 - DH x may not start at zero because of Bug#6702*/)
                 strSQLUnionData += ",";
            // Bug 18766: Parameterized 
            strSQLUnionData += ParameterizedBatch.CreateSQLFor_CUST_FIELD_DATA(paramList, paramCount, true, strPOSTNBR, strEVENTNBR, strPOSTID, strFileNumber, strFileSequenceNbr, strTAG, strID, strValue, strLabel, strScreenIndex, "TNDR", strAttr, strChecksum);
          }
          else
          {
            /* Bug 18766: Parameterized BUG# 10600 & 12054 - DH 2/19/2011 Combine statements*/
            strSQL = ParameterizedBatch.CreateSQLFor_CUST_FIELD_DATA(paramList, paramCount, false, strPOSTNBR, strEVENTNBR, strPOSTID, strFileNumber, strFileSequenceNbr, strTAG, strID, strValue, strLabel, strScreenIndex, "TNDR", strAttr, strChecksum);
               if(!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strErr))
              {
                HandleErrorsAndLocking("message", "Error executing SQL.", "code", "TS-811", strErr);//Bug 13832-added inner exception // Bug# 5813 deadlock fix. DH 07/30/2008
              }
          } 
          paramCount++;     // Bug 18766: Parameterized 
        }
      }
      //****************************************************************************
      

      /*BUG# 10600 & 12054 - DH 2/19/2011 Combine statements*/
      if (IsSQLServer2008orLater && strSQLUnionData != "")
      {
          strSQL = "INSERT INTO CUST_FIELD_DATA (POSTNBR, EVENTNBR, DEPFILENBR, DEPFILESEQ, ";
          strSQL += "SCREENINDEX, CUSTLABEL, CUSTVALUE, CUSTID, POSTID, CUSTTAG, POST_TYPE, CUSTATTR, CHECKSUM) VALUES ";

          // Bug 18766: Parameterized 
        string parameterizedQueryString = ParameterizedBatch.buildParameterizedQueryForBatchInsert(paramList);
        ParameterizedBatch.runParameterizedBatchedInsert(pDBConn, strSQL, parameterizedQueryString, paramList);
      }         

      // ALL GOOD - ACCEPT THE UPDATES.
      if (!args.has("DbConnectionClass"))// Caller will commit.
      {
        strErr = "";
        if (!pDBConn.CommitToDatabase(ref strErr))
        {
          // Close the database connection and dispose of pDBConn class.          
          HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-812", strErr);//Bug 13832-added inner exception // Bug# 5813 deadlock fix. DH 07/30/2008
        }

      }

      return true;
    }
      finally
      {
        // Bug 15941. FT. Close the database connection and dispose of pDBConn class.
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }

    }

    public static string CreateSQLFor_CUST_FIELD_DATA(bool IsSQLServer2008, string strPOSTNBR, string strEventNbr, string strPOSTID, string strFileNumber, string strFileSequenceNbr, string strTAG, string strID, string strValue, string strLabel, string strScreenIndex, string strPOST_TYPE, string strAttr, string strChecksum)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      /*BUG# 10600 & 12054 - DH 2/19/2011 Combine statements*/
      string strSQL = "";
            
      if (!IsSQLServer2008)
      {
        strSQL = "INSERT INTO CUST_FIELD_DATA (POSTNBR, EVENTNBR, DEPFILENBR, DEPFILESEQ, ";
        strSQL += "SCREENINDEX, CUSTLABEL, CUSTVALUE, CUSTID, POSTID, CUSTTAG, POST_TYPE, CUSTATTR, CHECKSUM) VALUES (";
      }
      else
      {
          strSQL = "(";
      }
      /*end BUG# 10600 & 12054 - Combine statements*/

      strSQL += strPOSTNBR + ", " + strEventNbr + ", " + strFileNumber + ", " + strFileSequenceNbr + ", " + strScreenIndex;

      if (strLabel.Length == 0)
        strSQL = strSQL + ", NULL, ";
      else
        strSQL = strSQL + ", '" + strLabel + "', ";

      if (strValue.Length == 0)
        strSQL = strSQL + "NULL, ";
      else
        strSQL = strSQL + "'" + strValue + "', ";

      if (strID.Length == 0)
        strSQL = strSQL + "NULL, ";
      else
        strSQL = strSQL + "'" + strID + "', ";

      if (strPOSTID.Length == 0)
        strSQL = strSQL + "NULL, ";
      else
        strSQL = strSQL + "'" + strPOSTID + "', ";

      if (strTAG.Length == 0)
        strSQL = strSQL + "NULL ";
      else
        strSQL = strSQL + "'" + strTAG + "' ";

      strSQL = strSQL + "," + "'" + strPOST_TYPE + "', ";
      if (strAttr.Length == 0) // BUG3861 S.X. 
        strSQL = strSQL + "NULL ";
      else
        strSQL = strSQL + "'" + strAttr + "' ";
      strSQL = strSQL + "," + "'" + strChecksum + "'" + ")";

      return strSQL;
    }

    public static bool UpdateTransactionDataChecksum(GenericObject geoTranForChecksumUpdate, ConnectionTypeSynch pDBConn, GenericObject config)
    {
      int iCnt = geoTranForChecksumUpdate.getIntKeyLength();
      for (int i = 0; i < iCnt; i++)
      {
        string strError = "";
        CoreFileInfoForChecksum pChecksumData = (CoreFileInfoForChecksum)geoTranForChecksumUpdate.get(i);

        // Matt -- replace the old update checksum with new update checksum
        string strFileName = pChecksumData.strFileNumber + pChecksumData.strFileSequenceNbr.PadLeft(3, '0');
        if (!UpdateTranChecksum("strFileName", strFileName, "strTranNbr", pChecksumData.strTranNbr, "strEventNbr", pChecksumData.strEventNbr, "DbConnectionClass", pDBConn))
            HandleErrorsAndLocking("message", "Unable to set checksum on TG_TRAN_DATA. Please call system administrator.", "code", "TS-813", strError);//Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

      }

      return true;
    }

    public static object VoidDeposit(GenericObject deposit, GenericObject data,
      ref string error_message, string void_id)
    {
      return true;
    }

    /// <summary>
    /// Voids the payevent given then voids any transactions and tenders in that event.  Assumes the
    /// session is valid.
    /// </summary>
    /// <param name="payevent"></param>
    public static object VoidEvent(GenericObject payevent, GenericObject data, ref string error_message, string void_user)
    {

      ThreadLock.GetInstance().CreateLock();
      // Bug 11661: Wrap mutex release in finally block
      try
      {
        //#region Arguments
        string sub_error_message = "";
        string sql_string = null;

        string payevent_EVENTNBR = GetStringValue(payevent, "EVENTNBR");
        string payevent_DEPFILENBR = GetStringValue(payevent, "DEPFILENBR");
        string payevent_DEPFILESEQ = GetStringValue(payevent, "DEPFILESEQ");

        DateTime today = DateTime.Now;
        string today_string = today.ToString("G", DateTimeFormatInfo.InvariantInfo);
        IDBVectorReader reader = null;
        //#endregion
        //#region Get non-void transactions
        // Bug 20382: FT: Converted to parameterized
        sql_string = "SELECT * FROM TG_TRAN_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND EVENTNBR=@eventnbr AND VOIDDT IS NULL";
        Hashtable sql_args = new Hashtable();
        sql_args["depfilenbr"] = payevent_DEPFILENBR;
        sql_args["depfileseq"] = payevent_DEPFILESEQ;
        sql_args["eventnbr"] = payevent_EVENTNBR;
        // End Bug 20382

        reader = db_reader.Make(data, sql_string, CommandType.Text, 0,
          sql_args, 0, 0, null, ref error_message);
        if (error_message != "")
        {
            //Bug 13832-use this for splitting the exception msg into friendly and verbose
            GenericObject err= CreateErrorObject("TS-1236","Error Voiding Event",false,"",true);
            err.set("inner_error",misc.MakeCASLError("","",error_message));
            return err;
            //end bug 13832
        }
        GenericObject transactions = new GenericObject();
        reader.ReadIntoRecordset(ref transactions, 0, 0);
        //#endregion
        //#region Get non-void tenders
        // Bug 20382: FT: Converted to parameterized
        sql_string = "SELECT * FROM TG_TENDER_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND EVENTNBR=@eventnbr AND VOIDDT IS NULL";
        sql_args = new Hashtable();
        sql_args["depfilenbr"] = payevent_DEPFILENBR;
        sql_args["depfileseq"] = payevent_DEPFILESEQ;
        sql_args["eventnbr"] = payevent_EVENTNBR;
        // End Bug 20382
        reader = db_reader.Make(data, sql_string, CommandType.Text, 0,
          sql_args, 0, 0, null, ref error_message);
        if (error_message != "")
        {
            //Bug 13832-use this for splitting the exception msg into friendly and verbose         
          GenericObject err = CreateErrorObject("TS-1229-A", "Error Voiding Event", false, "", true); //Bug 23378 NK Make all TS-1229 unique
          err.set("inner_error", misc.MakeCASLError("", "", error_message));
          return err;
            //end bug 13832
        }
        GenericObject tenders = new GenericObject();
        reader.ReadIntoRecordset(ref tenders, 0, 0);
        //#endregion
        //#region Get item data
        // Bug 20382: FT: Converted to parameterized
        sql_string = "SELECT * FROM TG_ITEM_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND EVENTNBR=@eventnbr";
        sql_args = new Hashtable();
        sql_args["depfilenbr"] = payevent_DEPFILENBR;
        sql_args["depfileseq"] = payevent_DEPFILESEQ;
        sql_args["eventnbr"] = payevent_EVENTNBR;
        // End Bug 20382
        reader = db_reader.Make(data, sql_string, CommandType.Text, 0,
          sql_args, 0, 0, null, ref error_message);
        if (error_message != "")
        {
          //Bug 13832-use this for splitting the exception msg into friendly and verbose
          GenericObject err = CreateErrorObject("TS-1229-B", "Error Voiding Event", false, "", true); //Bug 23378 NK Make all TS-1229 unique
          err.set("inner_error", misc.MakeCASLError("", "", error_message));
          return err;
            //end bug 13832
        }
        GenericObject items = new GenericObject();
        reader.ReadIntoRecordset(ref items, 0, 0);
        //#endregion
        //#region Get custom field data
        // Bug 20382: FT: Converted to parameterized
        sql_string = "SELECT * FROM GR_CUST_FIELD_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND EVENTNBR=@eventnbr";
        sql_args = new Hashtable();
        sql_args["depfilenbr"] = payevent_DEPFILENBR;
        sql_args["depfileseq"] = payevent_DEPFILESEQ;
        sql_args["eventnbr"] = payevent_EVENTNBR;
        // Bug 20382

        reader = db_reader.Make(data, sql_string, CommandType.Text, 0,
          sql_args, 0, 0, null, ref error_message);
        if (error_message != "")
        {
            //Bug 13832-use this for splitting the exception msg into friendly and verbose
            GenericObject err = CreateErrorObject("TS-1229-C", "Error Voiding Event", false, "", true); //Bug 23378 NK Make all TS-1229 unique
          err.set("inner_error", misc.MakeCASLError("", "", error_message));
            return err;
            //end bug 13832
        }
        GenericObject customFields = new GenericObject();
        reader.ReadIntoRecordset(ref customFields, 0, 0);
        //#endregion
        //#region Validate checksums
        // Bug #7518 Mike O - Added validation
        // Validate transaction checksums
        for (int i = 0; i < transactions.getLength(); i++)
        {
          GenericObject curTran = transactions.get(i) as GenericObject;
          string strEventNbr = curTran.get("EVENTNBR").ToString();
          string strTranNbr = curTran.get("TRANNBR").ToString();
          string strFileNbr = curTran.get("DEPFILENBR").ToString();
          string strFileSequenceNbr = curTran.get("DEPFILESEQ").ToString();
          string strFileName = strFileNbr + strFileSequenceNbr;
          if (!ValidateTranChecksum("geoChecksumData", curTran, "strEventNbr", strEventNbr, "strTranNbr", strTranNbr, "strFileName", strFileName))
          {
            HandleErrorsAndLocking("message", string.Format("Invalid Checksum for Transaction Record. TRANNBR: {0} EVENTNBR: {1} DEPFILENBR: {2} DEPFILESEQ: {3}", strTranNbr, strEventNbr, strFileNbr, strFileSequenceNbr), "code", "TS-1108", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
          }
        }

        // Validate tender checksums
        for (int i = 0; i < tenders.getLength(); i++)
        {
          GenericObject curTender = tenders.get(i) as GenericObject;
          string strEventNbr = curTender.get("EVENTNBR").ToString();
          string strTndrNbr = curTender.get("TNDRNBR").ToString(); // Bug#7941 ANU was TRANNBR
          string strFileNbr = curTender.get("DEPFILENBR").ToString();
          string strFileSequenceNbr = curTender.get("DEPFILESEQ").ToString();
          string strFileName = strFileNbr + strFileSequenceNbr;
          if (!ValidateTenderChecksum("geoChecksumData", curTender, "strEventNbr", strEventNbr, "strTndrNbr", strTndrNbr, "strFileName", strFileName)) // Bug#7941 ANU was TRANNBR
          {
            HandleErrorsAndLocking("message", string.Format("Invalid Checksum for Tender Record. TNDRNBR: {0} EVENTNBR: {1} DEPFILENBR: {2} DEPFILESEQ: {3}", strTndrNbr, strEventNbr, strFileNbr, strFileSequenceNbr), "code", "TS-1108", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
          }
        }

        // Validate ITEM_DATA Checksums
        for (int i = 0; i < items.getLength(); i++)
        {
          GenericObject curItem = items.get(i) as GenericObject;
          string strEventNbr = curItem.get("EVENTNBR").ToString();
          string strTranNbr = curItem.get("TRANNBR").ToString();
          string strFileNbr = curItem.get("DEPFILENBR").ToString();
          string strFileSequenceNbr = curItem.get("DEPFILESEQ").ToString();
          string strFileName = strFileNbr + strFileSequenceNbr;
          if (!ValidateItemDataChecksum("geoChecksumData", curItem, "strEventNbr", strEventNbr, "strTranNbr", strTranNbr, "strFileName", strFileName))
          {
            HandleErrorsAndLocking("message", string.Format("Invalid Checksum for Item Data Record. TRANNBR: {0} EVENTNBR: {1} DEPFILENBR: {2} DEPFILESEQ: {3}", strTranNbr, strEventNbr, strFileNbr, strFileSequenceNbr), "code", "TS-1109", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
          }
        }

        // Validate GR_CUST_FIELD Checksums
        for (int i = 0; i < customFields.getLength(); i++)
        {
          GenericObject curField = customFields.get(i) as GenericObject;
          string strEventNbr = curField.get("EVENTNBR").ToString();
          string strTranNbr = curField.get("TRANNBR").ToString();
          string strFileNbr = curField.get("DEPFILENBR").ToString();
          string strFileSequenceNbr = curField.get("DEPFILESEQ").ToString();
          string strFileName = strFileNbr + strFileSequenceNbr;
          if (!ValidateGRCustFieldChecksum("geoChecksumData", curField, "strEventNbr", strEventNbr, "strTranNbr", strTranNbr, "strFileName", strFileName))
          {
            HandleErrorsAndLocking("message", string.Format("Invalid Checksum for Cust Field Data Record. TRANNBR: {0} EVENTNBR: {1} DEPFILENBR: {2} DEPFILESEQ: {3}", strTranNbr, strEventNbr, strFileNbr, strFileSequenceNbr), "code", "TS-1110", "");				// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
          }
        }
        //#endregion
        //#region Create transaction reader
        reader = db_reader.MakeTransactionReader(data, 0, 0, 0, ref error_message);
        if (error_message != "")
        {
          //Bug 13832-use this for splitting the exception msg into friendly and verbose
          GenericObject err = CreateErrorObject("TS-1229-D", "Error Voiding Event", false, "", true); //Bug 23378 NK Make all TS-1229 unique
          err.set("inner_error", misc.MakeCASLError("", "", error_message));
          return err;
            //end bug 13832
        }
        //#endregion
        //#region Add commands
        //#region Add event void command
        payevent.set("STATUS", 3, "MOD_DT", today_string);
        string checksum = CreateEventChecksum(payevent);

        sql_args = new Hashtable();
        // Bug #8399 Mike O - Update the modified date (MOD_DT) when the event is voided
        // Bug 22529 HX: Set userid as void_user (SYSTEM) passed from paramter consist with trans and tener
        // Bug 24722 NK - Voiding an event shoud not change the USERID(owner/creator of the event) in the Payevent table
        sql_string = @"UPDATE TG_PAYEVENT_DATA
                      SET STATUS=3, /*USERID=@userid,*/ CHECKSUM=@checksum, MOD_DT=@moddt
                      WHERE EVENTNBR=@eventnbr AND DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq";
        sql_args["checksum"] = checksum;
        sql_args["eventnbr"] = payevent_EVENTNBR;
        sql_args["depfilenbr"] = payevent_DEPFILENBR;
        sql_args["depfileseq"] = payevent_DEPFILESEQ;
        sql_args["moddt"] = today_string;
        //sql_args["userid"] = void_user;
        // End Bug #8399

        reader.AddCommand(sql_string, CommandType.Text, sql_args);
        //#endregion
        //#region Add transaction void commands
        int tran_count = transactions.getLength();
        GenericObject void_tran = null;
        for (int i = 0; i < tran_count; i++)
        {
          void_tran = transactions.get(i) as GenericObject;
          //Bug 12757 NJ-don't void an already voided transaction
        if (!void_tran.has("VOIDDT"))
        {
          void_tran.set("VOIDDT", today_string, //Bug 9459 NJ - Void DateTime displaying as 12:00am on search result
            "VOIDUSERID", void_user);
        
          void_tran.set("CHECKSUM", CreateTransactionChecksum(void_tran));
          AddVoidTransactionCommand(ref reader, void_tran);
          if (error_message != "")
          {
            //Bug 13832-use this for splitting the exception msg into friendly and verbose
            GenericObject err = CreateErrorObject("TS-1229-E", "Error Voiding Event", false, "", true); //Bug 23378 NK Make all TS-1229 unique
              err.set("inner_error", misc.MakeCASLError("", "", error_message));
            return err;
              //end bug 13832
          }
//end bug 12757 Nj
        }
      }
        //#endregion
        //#region Add tender void commands
        int tender_count = tenders.getLength();
        GenericObject void_tender = null;
        for (int i = 0; i < tender_count; i++)
        {
            void_tender = tenders.get(i) as GenericObject;
            //Bug 13136 NJ-don't void a tender if it is a refund for a void that failed due to void window(time) expiration
            //Bug 24384 SM Added check for 0 dollar tender in if statement below. If zero tender allow add VOIDDT to TENDER_DATA table.
            decimal amountTendered;
            decimal.TryParse(GetStringValue(void_tender, "AMOUNT"), out amountTendered);
            if (amountTendered == 0 || !CheckTenderIsVoidRefund(void_tender,tenders))
            {
            //Bug 12757 NJ-don't void an already voided tender
            if (!void_tender.has("VOIDDT"))
            {
              void_tender.set("VOIDDT", today_string,
                "VOIDUSERID", void_user);
              void_tender.set("CHECKSUM", CreateTenderChecksum(void_tender));
              AddVoidTenderCommand(ref reader, void_tender);
              if (error_message != "")
              {
                    //Bug 13832-use this for splitting the exception msg into friendly and verbose
              GenericObject err = CreateErrorObject("TS-1229-F", "Error Voiding Event", false, "", true); //Bug 23378 NK Make all TS-1229 unique
                err.set("inner_error", misc.MakeCASLError("", "", error_message));
              return err;
              //end bug 13832
              }
            }
        }
        }

      //Bug 12835 NJ-update the completion date if the event is voided before completion  
      sql_args = new Hashtable();     
      sql_string = @"UPDATE TG_PAYEVENT_DATA SET COMPLETIONDT=@completiondt 
        WHERE EVENTNBR=@eventnbr AND DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq
        AND COMPLETIONDT IS NULL";     
      sql_args["eventnbr"] = payevent_EVENTNBR;
      sql_args["depfilenbr"] = payevent_DEPFILENBR;
      sql_args["depfileseq"] = payevent_DEPFILESEQ;
      sql_args["completiondt"] = today_string;
      reader.AddCommand(sql_string, CommandType.Text, sql_args);
        //end bug 12835 NJ

        //#region Establish and run transaction
        bool success;
        success = reader.EstablishTransaction(ref error_message);
        if (!success || error_message != "")
        {
            //Bug 13832-use this for splitting the exception msg into friendly and verbose
            GenericObject err= CreateErrorObject("TS-1229-G","Error Voiding Event",false,"",true); //Bug 23378 NK Make all TS-1229 unique
          err.set("inner_error", misc.MakeCASLError("", "", error_message));
            return err;
            //end bug 13
        }
        int rows_affected = 0;
        success = reader.ExecuteTransactionCommands(ref error_message, ref rows_affected, true);
        if (!success || error_message != "")
        {
          success = reader.RollbackTransaction(ref sub_error_message);
          error_message += sub_error_message;
          //Bug 13832-use this for splitting the exception msg into friendly and verbose
          GenericObject err = CreateErrorObject("TS-1229-I", "Error Voiding Event", false, "", true); //Bug 23378 NK Make all TS-1229 unique
          err.set("inner_error", misc.MakeCASLError("", "", error_message));
          return err;
            //end bug 13
        }
        success = reader.CommitTransaction(ref error_message);      // Bug 15941: CommitTransaction closes connection
        if (!success || error_message != "")
        {
          success = reader.RollbackTransaction(ref sub_error_message);
          error_message += sub_error_message;
          //Bug 13832-use this for splitting the exception msg into friendly and verbose
          GenericObject err = CreateErrorObject("TS-1229-P", "Error Voiding Event", false, "", true); //Bug 23378 NK Make all TS-1229 unique
          err.set("inner_error", misc.MakeCASLError("", "", error_message));
          return err;
            //end bug 13
        }
        //#endregion
      }
      finally
      {
        // Bug 11661
        ThreadLock.GetInstance().ReleaseLock();
      }
      return true;
    }

    public static GenericObject GetEvent(GenericObject payevent, GenericObject database, ref string error_message)
    {
      string event_EVENTNBR = GetStringValue(payevent, "EVENTNBR");
      string event_DEPFILENBR = GetStringValue(payevent, "DEPFILENBR");
      string event_DEPFILESEQ = GetStringValue(payevent, "DEPFILESEQ");

      string sql_string = "SELECT * FROM TG_PAYEVENT_DATA WHERE EVENTNBR=@eventnbr AND DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq";
      Hashtable sql_args = new Hashtable();
      sql_args["eventnbr"] = event_EVENTNBR;
      sql_args["depfilenbr"] = event_DEPFILENBR;
      sql_args["depfileseq"] = event_DEPFILESEQ;
      IDBVectorReader reader = db_reader.Make(database, sql_string, CommandType.Text, 0, sql_args, 0, 0, null, ref error_message);
      if (error_message != "")
      {
        return null;
      }
      GenericObject events = new GenericObject();
      reader.ReadIntoRecordset(ref events, 0, 0);

      int event_count = events.getLength();
      if (event_count == 1)
      {
        return events.get(0) as GenericObject;
      }
      else if (event_count == 0)
      {
        error_message = String.Format("Event {0}{1}-{2} already void.", event_DEPFILENBR, event_DEPFILESEQ.PadLeft(3, '0'), event_EVENTNBR);
        return null;
      }
      else
      {
        error_message = String.Format("More than one event {0} found in file {1}{2}.", event_EVENTNBR, event_DEPFILENBR, event_DEPFILESEQ.PadLeft(3, '0'));
        return null;
      }
    }

    /// <summary>
    /// Voids a core event defined by a TG_PAYEVENT_DATA object.
    /// Algorithm:
    /// 0. Check session
    /// 1. Update the event status in the DB
    /// 2. Construct update SQL for the event, its transactions and its items
    /// 3. Run the SQL as a transaction
    /// 4. Commit
    /// 5. Return true
    /// 
    /// On failure:
    /// 1. Un-update the event status in the DB
    /// 2. Return false
    /// </summary>
    /// <param name="arg_pairs">
    ///   _subject = a TG_PAYEVENT_DATA object
    ///   config = config database db_connection_info object
    ///   data = data database db_connection_info object
    ///   session_id = session id string
    /// </param>
    /// <returns> whether the void was successful </returns>
    public static object CASLVoidEvent(params Object[] arg_pairs)
    {
      //#region Arguments
      GenericObject args = misc.convert_args(arg_pairs);

      GenericObject payevent = args.get("_subject") as GenericObject;
      GenericObject config_database = args.get("config") as GenericObject;
      GenericObject data_database = args.get("data") as GenericObject;
      string session_id = (string)args.get("session_id");
      string error_message = "";
      //#endregion
      //#region Get event information
      string payevent_FILENAME = GetStringValue(payevent, "file_FILENAME");
      string payevent_VOIDUSERID = GetStringValue(payevent, "VOIDUSERID");
      payevent.set("DEPFILENBR", GetFileNumber(payevent_FILENAME),
        "DEPFILESEQ", GetFileSequence(payevent_FILENAME));
      GenericObject coreevent = GetEvent(payevent, data_database, ref error_message);
      coreevent.set("VOIDUSERID", payevent_VOIDUSERID);
      //#endregion
      return VoidEvent(coreevent, config_database, data_database, session_id);
    }

    // Bug #7518 Mike O - Rewrote this method
    public static object VoidEvent(GenericObject payevent,
    GenericObject config_database, GenericObject data_database,
    string session_id)
    {
      string event_EVENTNBR = "UNKNOWN";
      string event_DEPFILENBR = "UNKNOWN";
      string event_DEPFILESEQ = "UNKNOWN";

      bool success = false;
      ThreadLock.GetInstance().CreateLock();
      // Bug 11661: Wrap mutex release in finally block
      try
      {
        //#region Get event information
        string error_message = "";
        string payevent_VOIDUSERID = GetStringValue(payevent, "VOIDUSERID");
        GenericObject coreevent = GetEvent(payevent, data_database, ref error_message);
        //#endregion
        //#region Confirm that the event to be voided exists
        event_EVENTNBR = GetStringValue(coreevent, "EVENTNBR");
        event_DEPFILENBR = GetStringValue(coreevent, "DEPFILENBR");
        event_DEPFILESEQ = GetStringValue(coreevent, "DEPFILESEQ");
        string old_status = GetStringValue(coreevent, "STATUS");
        string sql_string = @"SELECT * FROM TG_PAYEVENT_DATA WHERE EVENTNBR=@eventnbr AND DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND STATUS!='3' AND STATUS!='4' AND STATUS!='6'";
        Hashtable sql_args = new Hashtable();
        sql_args["eventnbr"] = event_EVENTNBR;
        sql_args["depfilenbr"] = event_DEPFILENBR;
        sql_args["depfileseq"] = event_DEPFILESEQ;
        IDBVectorReader reader = db_reader.Make(data_database, sql_string, CommandType.Text, 0, sql_args,
          0, 0, null, ref error_message);
 
        if (error_message != "")
        {
           //13832-use this method for splitting error into user friendly and verbose
            GenericObject err = CreateErrorObject("TS-1229-J", "Error Voiding Event", false, "", true); //Bug 23378 NK Make all TS-1229 unique
          err.set("inner_error", misc.MakeCASLError("", "", error_message));
            return err;
            //end bug 13832
        }
        //duplicate code
        GenericObject records = new GenericObject();
        reader.ReadIntoRecordset(ref records, 0, 0);
        int rows_read = records.getLength();
        if (error_message != "")
        {
          //13832-use this method for splitting error into user friendly and verbose
            GenericObject err = CreateErrorObject("TS-1229-K", "Error Voiding Event", false, "", true); //Bug 23378 NK Make all TS-1229 unique
          err.set("inner_error", misc.MakeCASLError("", "", error_message));
            return err;
            //end bug 13832
        }
        if (rows_read == 0)
        {
          return misc.MakeCASLError("Error Voiding Event", "TS-1229-L", String.Format("Event {0} not found in file {1}{2}",
            event_EVENTNBR, event_DEPFILENBR, event_DEPFILESEQ.PadLeft(3, '0'))); //Bug 23378 NK Make all TS-1229 unique
        }
        if (rows_read > 1)
        {
          return misc.MakeCASLError("Error Voiding Event", "TS-1229-M", String.Format("More than one event {0} found in file {1}{2}",
            event_EVENTNBR, event_DEPFILENBR, event_DEPFILESEQ.PadLeft(3, '0'))); //Bug 23378 NK Make all TS-1229 unique
        }
        //#endregion
        //#region Mark event void
        success = VoidEvent(coreevent, data_database, ref error_message, payevent_VOIDUSERID).Equals(true);
        //#endregion
      }
      // Bug 27119 MJO - Log exception and re-throw to keep everything working as-is
      catch (Exception e)
      {
        string msg = string.Format("Error voiding an event: file: {0}-{1}, Event: {2}.  Error: {3}",
            event_DEPFILENBR, event_DEPFILESEQ.PadLeft(3, '0'), event_EVENTNBR, e.ToMessageAndCompleteStacktrace());
        Logger.cs_log(msg, Logger.cs_flag.error);

        throw; // For backwards compatibility
      }
      finally
      {
        // Bug 11661
        ThreadLock.GetInstance().ReleaseLock();
      }

      return success;
    }


    public static bool VoidEventInT3(params Object[] arg_pairs)
    {
      //Matt -- Function voids an event, this function is called on a TG_PAYEVENT_DATA which includes:
      //(file_FILENAME, EVENTNBR or file_SOURCE_TYPE, file_SOURCE_GROUP, SOURCE_REFID) and VOIDDT, VOIDUSERID

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = args.get("config", c_CASL.c_undefined) as GenericObject;
      GenericObject data = null;
      GenericObject gTmp = null;
      GenericObject TG_PAYEVENT_DATA = args.get("_subject", c_CASL.c_undefined) as GenericObject;
      GenericObject geoChecksumData = new GenericObject();
      GenericObject geoBuffer = args.get("geoBuffer", c_CASL.c_undefined) as GenericObject;
      GenericObject gTranNbrs = new GenericObject();
      GenericObject gTndrNbrs = new GenericObject();
      string session_id = args.get("session_id") as string;

      ConnectionTypeSynch pDBConn = null;
      bool connectionOpened = false;      // Bug 15941
      string strError = "Default Error";

      bool bVal = true;
      string strStatus = "3";

      // Bug 15941. FT. Added try/finnally block to make sure connections are closed
      try
      {
      // Check if connection passed
      if (args.has("DbConnectionClass"))
      {
        pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
        data = pDBConn.GetDBConnectionInfo();
      }
      else if (args.has("data"))
      {
        data = args.get("data", c_CASL.c_undefined) as GenericObject;
        pDBConn = new ConnectionTypeSynch(data);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strError)))  // Bug 15941
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1084", strError);//Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        if (!pDBConn.EstablishRollback(ref strError))
            HandleErrorsAndLocking("message", "Error creating database transaction.", "code", "TS-1085", strError);//Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

      }
      else
        HandleErrorsAndLocking("message", "Please provide a method of connecting to the database.", "code", "TS-1086", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

      //**********************************************************************   
      int iDBType = pDBConn.GetDBConnectionType();

      //Get the correct information that is needed for a void.
      string strFileName = TG_PAYEVENT_DATA.get("file_FILENAME", "").ToString();
      string strEventNbr = TG_PAYEVENT_DATA.get("EVENTNBR", "").ToString();

      string strFileSourceType = TG_PAYEVENT_DATA.get("file_SOURCE_TYPE", "").ToString();
      string strFileSourceGroup = TG_PAYEVENT_DATA.get("file_SOURCE_GROUP", "").ToString();
      string strSourceRefID = TG_PAYEVENT_DATA.get("SOURCE_REFID", "").ToString();

      string strVoidDT = TG_PAYEVENT_DATA.get("VOIDDT", "").ToString();
      string strVoidUserID = TG_PAYEVENT_DATA.get("VOIDUSERID", "").ToString();
      string strLOGIN_ID = TG_PAYEVENT_DATA.get("LOGIN_ID", "").ToString(); //BUG 5395v2 (VoidEventInT3)
      string strFileNbr = "";
      string strFileSequenceNbr = "";
      string strSQL = "";
      string strChecksumData = "";
      string strUserID = "";
      string strModDT = "";

      DateTime dtDate = DateTime.Now;

      int iTranNbr = 0;
      int iTndrNbr = 0;
      int iCount = 0;

      //if source came in as Portal, change it to Cashier
      if (strFileSourceType == "Portal")
        strFileSourceType = "Cashier";
      //end BUG 5395
      //Need to provide at least the eventnbr/filenbr or the sourcetype, sourcegroup, sourcerefid
      //in order to grab the correct information.
      if ((strEventNbr == "" || strFileName == "") &&
        (strFileSourceType == "" || strFileSourceGroup == "" || strSourceRefID == ""))
      {
        HandleErrorsAndLocking("message", "You must provide either (EventNbr AND FileNbr) or (SourceType, SourceGroup, AND SourceRefID).", "code", "TS-1087", "");				// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      //If Void Date was not provided, set the current date/time
      if (String.IsNullOrEmpty(strVoidDT))
      {
        strVoidDT = dtDate.ToString("MM/dd/yyyy HH:mm:ss");
      }
      else if (strVoidDT.Length == 10)
      {
        strVoidDT = strVoidDT + " 00:00:01";
      }

      //Need to change to get the correct info
      //If the EventNbr or the FileName are empty, use the Source type, group and refid to get them
      if (strEventNbr == "" || strFileName == "")
      {
        //Getting the correct records
        strSQL = string.Format("SELECT * FROM TG_PAYEVENT_DATA WHERE SOURCE_TYPE=\'{0}\' AND SOURCE_GROUP=\'{1}\' AND SOURCE_REFID=\'{2}\'", strFileSourceType, strFileSourceGroup, strSourceRefID);

        gTmp = (GenericObject)GetRecords(data, 0, strSQL, iDBType);

        strFileNbr = GetKeyData(gTmp, "DEPFILENBR", false);
        strEventNbr = GetKeyData(gTmp, "EVENTNBR", false);

        //Need to pad the DEPFILESEQ with 0s for the full FileName
        strFileSequenceNbr = GetKeyData(gTmp, "DEPFILESEQ", false);
        strFileSequenceNbr = strFileSequenceNbr.PadLeft(3, '0');

        strFileName = String.Concat(strFileNbr, strFileSequenceNbr);

      }
      else
      {
        //If we already have the strEventNbr and strFileName, we have already have what we need
        strFileNbr = strFileName.Substring(0, 7);
        strFileSequenceNbr = strFileName.Substring(7, strFileName.Length - 7);
      }

      //Check to see if the file is active
      // Bug 20382: FT: Converted to parameterized
      string sqlString = @"SELECT COUNT(*) AS COUNT FROM TG_DEPFILE_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND BALDT IS NULL";
      HybridDictionary sql_args = new HybridDictionary();
      sql_args["depfilenbr"] = strFileNbr;
      sql_args["depfileseq"] = strFileSequenceNbr;
      long lActiveOrNot = GetTableRowCount(pDBConn, sqlString, sql_args);
      // End Bug 20382

      //If it is not active throw an error
      if (lActiveOrNot == 0)
      {
        HandleErrorsAndLocking("message", "Transaction File " + strFileNbr + " is not Active!  No changes are permitted.", "code", "TS-1088", "");	// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      //Get the UserID and ModDate from the tg_payevent_data table
      // Bug #12055 Mike O - Use misc.Parse to log errors
      strSQL = string.Format("SELECT USERID FROM TG_PAYEVENT_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr));
      gTmp = (GenericObject)GetRecords(data, 0, strSQL, iDBType);

      if (gTmp != null)
      {
        strUserID = gTmp.get("USERID", "").ToString();
        strModDT = strVoidDT;
      }
      else
      {
        // In case theres an exception thrown, we need to close the connection
        if (!args.has("DbConnectionClass"))
        {           
          pDBConn.RollBack();
        }

        HandleErrorsAndLocking("message", "No available TG_PAYEVENT_DATA.", "code", "TS-1094", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      //Get all non-voided Transaction numbers for the receipt
      // Bug #12055 Mike O - Use misc.Parse to log errors
      strSQL = string.Format("SELECT TRANNBR FROM TG_TRAN_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} AND VOIDDT IS NULL", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr));
      gTmp = (GenericObject)GetRecords(data, 1, strSQL, iDBType);

      if (gTmp != null)
      {
        iCount = (int)gTmp.getLength();
        for (int x = 0; x < iCount; x++)
        {
          iTranNbr = int.Parse(((GenericObject)gTmp.get(x)).get("TRANNBR", "").ToString());
          gTranNbrs.insert(iTranNbr);
        }

        //Loop through Transaction Numbers
        //need a conditional check for if gTmp is null or not
        for (int x = 0; x < (int)gTranNbrs.getLength(); x++)
        {
          strError = "";
          //string strTranNbr, string strEventNbr, string strFileName, string strVoidDT, string strVoidUserID, ref ConnectionTypeSynch pDBConn, ref string strError) 
          if (!VoidTransaction("strTranNbr", gTranNbrs.get(x).ToString(),
            "strEventNbr", strEventNbr,
            "strFileName", strFileName,
            "strVoidDT", strVoidDT,
            "strVoidUserID", strVoidUserID,
            "DbConnectionClass", pDBConn,
            "geoBuffer", geoBuffer))
          {
            HandleErrorsAndLocking("message", "Error in Void Transaction.  Transaction File " + gTranNbrs.get(x).ToString(), "code", "TS-1089", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
          }
        }
      }

      //Get all the non voided tender items in the event
      // Bug #12055 Mike O - Use misc.Parse to log errors
      strSQL = string.Format("SELECT TNDRNBR FROM TG_TENDER_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} AND VOIDDT IS NULL", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr));
      gTmp = (GenericObject)GetRecords(data, 1, strSQL, iDBType);

      //If there are some records
      if (gTmp != null)
      {
        //Loop through them and get the tender numbers
        iCount = (int)gTmp.getLength();
        for (int x = 0; x < iCount; x++)
        {
          iTndrNbr = int.Parse(((GenericObject)gTmp.get(x)).get("TNDRNBR", "").ToString());
          gTndrNbrs.insert(iTndrNbr);
        }

        for (int x = 0; x < (int)gTndrNbrs.getLength(); x++)
        {
          strError = "";
          //string strTndrNbr, string strEventNbr, string strFileName, string strVoidDT, string strVoidUserID, ref ConnectionTypeSynch pDBConn, ref string strError
          if (!VoidTender("strTndrNbr", gTndrNbrs.get(x).ToString(),
            "strEventNbr", strEventNbr,
            "strFileName", strFileName,
            "strVoidDT", strVoidDT,
            "strVoidUserID", strVoidUserID,
            "DbConnectionClass", pDBConn,
            "geoBuffer", geoBuffer))
          {
            HandleErrorsAndLocking("message", "Error in Void Tender.  Tender File " + gTndrNbrs.get(x).ToString(), "code", "TS-1091", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
          }
        }
      }

      //Mark Event as Voided ('3')

      strSQL = "UPDATE TG_PAYEVENT_DATA WITH (ROWLOCK) SET STATUS='" + strStatus + "', ";
      //POST DATE: Use TO_DATE for ORACLE 
      if (iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_ODBC || iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_TNS)
      {
        strSQL = strSQL + "MOD_DT=TO_DATE('" + strVoidDT + "','MM/DD/YYYY HH24:MI:SS') ";
      }
      //POST DATE: Specify Date for MSACCESS and SQL Server
      else
      {
        strSQL = strSQL + "MOD_DT='" + strVoidDT + "' ";
      }

      strSQL = strSQL + string.Format("WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr));

      strError = "";

      if (geoBuffer.Equals(c_CASL.c_undefined))
      {

        if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strError))
        {
          // In case theres an exception thrown, we need to close the connection            
          HandleErrorsAndLocking("message", "Error executing SQL.", "code", "TS-1093", strError);//Bug 13832-added inner exception // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
      }
      else
      {
        geoBuffer.insert(strSQL);
      }

      //Creating the new checksum
      geoChecksumData.set("USERID", strUserID, "STATUS", strStatus, "MOD_DT", strModDT);

      // Bug #10955 Mike O
      strChecksumData = CreatePayEventChecksum(Crypto.DefaultHashAlgorithm, "strEventNbr", strEventNbr, "strFileName", strFileName, "geoChecksumData", geoChecksumData);

      //Update the Checksum
      // Bug #12055 Mike O - Use misc.Parse to log errors
      strSQL =  string.Format("UPDATE TG_PAYEVENT_DATA WITH (ROWLOCK) SET CHECKSUM='" + strChecksumData + "' WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr));
      strError = "";
      if (geoBuffer.Equals(c_CASL.c_undefined))
      {
        if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strError))
            HandleErrorsAndLocking("message", "Error executing SQL.", "code", "TS-1095", strError);//Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        //Commit everything to the database
        strError = "";
        // ALL GOOD - ACCEPT THE UPDATES.
        if (!args.has("DbConnectionClass"))// Caller will commit.
        {
          if (!pDBConn.CommitToDatabase(ref strError))
              HandleErrorsAndLocking("message", "Unable to commit changes to database. ", "code", "TS-1096", strError);//Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
      }
      else
      {
        geoBuffer.insert(strSQL);
      }

      return bVal;
    }
      finally
      {
        // Bug 15941. FT. Close the database connection
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }

    }


    public static bool VoidTender(params Object[] arg_pairs)
    {
      //string strTndrNbr, string strEventNbr, string strFileName, string strVoidDT, string strVoidUserID, ref ConnectionTypeSynch pDBConn, ref string strError
      //Matt -- Function that voids a tender with an established connection

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;
      GenericObject geoBuffer = args.get("geoBuffer", c_CASL.c_undefined) as GenericObject;
      ConnectionTypeSynch pDBConn = null;

      string strTndrNbr = args.get("strTndrNbr").ToString();
      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();
      string strVoidUserID = args.get("strVoidUserID").ToString();
      //Bug 12835 NJ-set the void datetime here because CASL datetime does not have seconds
      //string strVoidDT = args.get("strVoidDT").ToString();
      string strVoidDT = DateTime.Now.ToString("G");
      //end bug 12835 NJ
      string strChecksum = "";
      string strError = "";

      string strFileNbr = GetFileNumber(strFileName); //strFileName.Substring(0,7);
      string strFileSequenceNbr = GetFileSequence(strFileName); //strFileName.Substring(7, strFileName.Length-7);
      string strSQL = null;
      int iDBType;

      DateTime dtDate = DateTime.Now;
      bool connectionOpened = false;        // Bug 15941

      // Bug 15941. FT. Make sure connection is always closed
      try
      {
      // Check if connection passed
      if (args.has("DbConnectionClass"))
      {
        pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
        config = pDBConn.GetDBConnectionInfo();
      }
      else if (args.has("config"))
      {
        // We have to get a new database connection and later commit ourselves.
        config = args.get("config", c_CASL.c_undefined) as GenericObject;
        pDBConn = new ConnectionTypeSynch(config);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strError)))  // Bug 15941
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1097", strError);//Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        if (!pDBConn.EstablishRollback(ref strError))
            HandleErrorsAndLocking("message", "Error creating database transaction.", "code", "TS-1098", strError);//Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }
      else
      {
        HandleErrorsAndLocking("message", "Must provide a method of connecting to the database.", "code", "TS-1099", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      if (args.has("geoChecksumData"))
      {
        gTmp = (GenericObject)args.get("geoChecksumData");
      }
      else
      {
        iDBType = pDBConn.GetDBConnectionType();
        // Bug #12055 Mike O - Use misc.Parse to log errors
        if (iDBType == ConnectionTypeSynch.m_iCurrentDatabase_SqlServer) // For for Bug#3314 D.H.
          strSQL = string.Format("SELECT * FROM TG_TENDER_DATA (NOLOCK) WHERE TNDRNBR={0} AND EVENTNBR={1} AND DEPFILENBR={2} AND DEPFILESEQ={3}", misc.Parse<Int64>(strTndrNbr), misc.Parse<Int64>(strEventNbr), misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr));
        else
          strSQL = string.Format("SELECT * FROM TG_TENDER_DATA WHERE TNDRNBR={0} AND EVENTNBR={1} AND DEPFILENBR={2} AND DEPFILESEQ={3}", misc.Parse<Int64>(strTndrNbr), misc.Parse<Int64>(strEventNbr), misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr));

        gTmp = (GenericObject)GetRecords(config, 0, strSQL, iDBType);
      }

      if (gTmp == null)
      {
        HandleErrorsAndLocking("message", "No records found.", "code", "TS-1100", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      //Validate the Tender Data Checksum	
      if (!ValidateTenderChecksum("geoChecksumData", gTmp, "strEventNbr", strEventNbr, "strFileName", strFileName, "strTndrNbr", strTndrNbr))
      {
        HandleErrorsAndLocking("message", string.Format("Invalid Check Sum for Tender Data Record. TNDRNBR: {0} EVENTNBR: {1} DEPFILENBR: {2} DEPFILESEQ: {3}", strTndrNbr, strEventNbr, strFileNbr, strFileSequenceNbr), "code", "TS-1101", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      if (strVoidDT != null && strVoidDT != "")
      {
        // Bug #12055 Mike O - Use misc.Parse to log errors
        dtDate = misc.Parse<DateTime>(strVoidDT);
        strVoidDT = dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo);
      }

      //Create the new checksum with the voided date
      gTmp = (GenericObject)gTmp.set("VOIDDT", strVoidDT);
      // Bug #10955 Mike O
      strChecksum = CreateTenderChecksum(Crypto.DefaultHashAlgorithm, "geoChecksumData", gTmp, "strEventNbr", strEventNbr, "strFileName", strFileName, "strTndrNbr", strTndrNbr);
      //Update the Checksum, the VOIDUSERID and the VOIDDT
      strSQL = "UPDATE TG_TENDER_DATA SET CHECKSUM=" + "'" + strChecksum + "'" + ", VOIDUSERID=";

      if (strVoidUserID.Length > 0)
        strSQL += "'" + strVoidUserID + "',";
      else
        strSQL += "NULL, ";

      //Format for Oracle
      if (pDBConn.GetDBConnectionType() == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_ODBC || pDBConn.GetDBConnectionType() == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_TNS)
        strSQL += "VOIDDT=TO_DATE('" + strVoidDT + "','MM/DD/YYYY HH24:MI:SS')";
      else
        strSQL += "VOIDDT='" + strVoidDT + "'";

      // Bug #12055 Mike O - Use misc.Parse to log errors
      strSQL += " WHERE TNDRNBR=" + misc.Parse<Int64>(strTndrNbr) +" AND EVENTNBR=" + misc.Parse<Int64>(strEventNbr) + " AND DEPFILENBR=" + misc.Parse<Int64>(strFileNbr) + " AND DEPFILESEQ=" + misc.Parse<Int64>(strFileSequenceNbr);

      if (geoBuffer.Equals(c_CASL.c_undefined))
      {
        if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref m_sErrMsg))
        {
          HandleErrorsAndLocking("message", "Error executing SQL: ", "code", "TS-1102", m_sErrMsg + ", SQL statement: " + strSQL);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        // ALL GOOD - ACCEPT THE UPDATES.
        if (!args.has("DbConnectionClass"))
        {
          if (!pDBConn.CommitToDatabase(ref strError))
          {
              HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-1103", strError);//Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
          }
        }
      }
      else
      {
        geoBuffer.insert(strSQL);
      }

      return true;
    }
      finally
      {
        // Bug 15941. FT. Close the database connection
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }

    }

    public static bool VoidTransaction(params Object[] arg_pairs)
    {
      // Matt -- Function voids a transaction with an established connection
      // This is the equivalent to PerformTranVoid in Alison's ocx, this will also replace VoidTransactionInT3

      // Bug 15941. FT.  Moved here
      ConnectionTypeSynch pDBConn = null;
      GenericObject args = misc.convert_args(arg_pairs);
      bool connectionOpened = false;      // Bug 15941

      // Bug 20382: Moved here so that the error logger can see them
      string strTranNbr = args.get("strTranNbr").ToString();
      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();

      // BUG# 7516 D.H
      ThreadLock.GetInstance().CreateLock();
      // Bug 11661: Wrap mutex release in finally block
      try
      {
        GenericObject data = null;      // Bug 20382: FT: Changed from config.  See below
        GenericObject gTmp = null;
        GenericObject geoBuffer = args.get("geoBuffer", c_CASL.c_undefined) as GenericObject;

        string strVoidUserID = args.get("strVoidUserID").ToString();
        //Bug 12835 NJ-set the void datetime here because CASL datetime does not have seconds
      //string strVoidDT = args.get("strVoidDT").ToString();
        string strVoidDT = DateTime.Now.ToString("G");
        //end bug 12835 NJ
        string strError = "";
        string strChecksum = "";
        string strFileNbr = GetFileNumber(strFileName); //strFileName.Substring(0,7);
        string strFileSequenceNbr = GetFileSequence(strFileName); //strFileName.Substring(7, strFileName.Length-7);
        string strSQL = "";

        int iDBType;
        DateTime dtDate;

        //**********************************************************************
        // Check if connection passed
        if (args.has("DbConnectionClass"))
        {
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
          data = pDBConn.GetDBConnectionInfo();
        }
        else if (args.has("data"))
        {
          // We have to get a new database connection and later commit ourselves.
          // Bug 20382: FT: The above "if" statement used to look for "config" not "data".
          // This was a timebomb waiting to happen, but
          // thankfully this code is alway called with a DbConnectionClass 
          // Woe betide the person who calls the old code without DbConnectionClass
          data = args.get("data", c_CASL.c_undefined) as GenericObject;
          pDBConn = new ConnectionTypeSynch(data);
          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strError)))  // Bug 15941
              HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1104", strError);//Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

          if (!pDBConn.EstablishRollback(ref strError))
              HandleErrorsAndLocking("message", "Error creating database transaction.", "code", "TS-1105", strError);//Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
        else
        {
          HandleErrorsAndLocking("message", "Must provide a method of connecting to the database.", "code", "TS-1106", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        if (args.has("geoChecksumData"))
        {
          gTmp = (GenericObject)args.get("geoChecksumData");
        }
        else
        {
          iDBType = pDBConn.GetDBConnectionType();
        // Bug #12055 Mike O - Use misc.Parse to log errors
          // Bug 20382: FT: Converted to parameterized
          if (iDBType == ConnectionTypeSynch.m_iCurrentDatabase_SqlServer) // BUG4410 add nolock to query without hanging for sql server
            strSQL = "SELECT * FROM TG_TRAN_DATA (NOLOCK) WHERE TRANNBR=@trannbr AND EVENTNBR=@eventnbr AND DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq";
          else
            strSQL = "SELECT * FROM TG_TRAN_DATA WHERE TRANNBR=@trannbr AND EVENTNBR=@eventnbr AND DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq";

          // Bug 20382: FT: Converted to parameterized
          Hashtable sql_args = new Hashtable();
          sql_args["trannbr"] = strTranNbr;
          sql_args["eventnbr"] = strEventNbr;
          sql_args["depfilenbr"] = strFileNbr;
          sql_args["depfileseq"] = strFileSequenceNbr;
          gTmp = (GenericObject)GetRecordsParameterized(data, sql_args, 0, strSQL, iDBType);
          // End Bug 20382
        }
        //**********************************************************************        

        if (gTmp == null)
          HandleErrorsAndLocking("message", "No records found", "code", "TS-1107", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        GenericObject gTranTmp = gTmp;

        //Validate Checksum of TRAN_DATA.
        if (!ValidateTranChecksum("geoChecksumData", gTmp, "strEventNbr", strEventNbr, "strTranNbr", strTranNbr, "strFileName", strFileName))
        {
          HandleErrorsAndLocking("message", string.Format("Invalid Check Sum for Transaction Record. TRANNBR: {0} EVENTNBR: {1} DEPFILENBR: {2} DEPFILESEQ: {3}",
              strTranNbr, strEventNbr, strFileNbr, strFileSequenceNbr), "code", "TS-1108", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }


        //Validate ITEM_DATA Checksums
        //Find out how many there are

        if (!ValidateItemDataChecksum("DbConnectionClass", pDBConn, "strEventNbr", strEventNbr, "strTranNbr", strTranNbr, "strFileName", strFileName))
          HandleErrorsAndLocking("message", string.Format("Invalid Check Sum for Item Data Record. TRANNBR: {0} EVENTNBR: {1} DEPFILENBR: {2} DEPFILESEQ: {3}",
              strTranNbr, strEventNbr, strFileNbr, strFileSequenceNbr), "code", "TS-1109", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        //Validate GR_CUST_FIELD Checksums		
        //Find out how many there are

        if (!ValidateGRCustFieldChecksum("DbConnectionClass", pDBConn, "strEventNbr", strEventNbr, "strTranNbr", strTranNbr, "strFileName", strFileName))
          HandleErrorsAndLocking("message", string.Format("Invalid Check Sum for Cust Field Data Record. TRANNBR: {0} EVENTNBR: {1} DEPFILENBR: {2} DEPFILESEQ: {3}",
              strTranNbr, strEventNbr, strFileNbr, strFileSequenceNbr), "code", "TS-1110", "");				// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix


        if (strVoidDT != null && strVoidDT != "")
        {
        // Bug #12055 Mike O - Use misc.Parse to log errors
        dtDate = misc.Parse<DateTime>(strVoidDT);
          strVoidDT = dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo);
        }

        //This is a copy of the first gTmp which contains the TRAN_DATA information, now
        //we update the VOIDDT so that when we create the new check sum it is included
        gTranTmp = (GenericObject)gTranTmp.set("VOIDDT", strVoidDT);
        // Bug #10955 Mike O
        strChecksum = CASLCreateTranChecksum(Crypto.DefaultHashAlgorithm, "geoChecksumData", gTranTmp, "strEventNbr", strEventNbr, "strTranNbr", strTranNbr, "strFileName", strFileName);

        // Bug 20382: FT: Converted to parameterized
        strSQL = @"
          UPDATE TG_TRAN_DATA 
            SET CHECKSUM=@checksum, VOIDUSERID=@voiduserid, VOIDDT=@voiddt 
          WHERE 
            TRANNBR=@trannbr AND EVENTNBR=@eventnbr AND DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq";
        // Lazy hack, but it should not happen very often
        if (strVoidUserID.Length == 0)
          strSQL.Replace("@voiduserid", "NULL");


        if (geoBuffer.Equals(c_CASL.c_undefined))
        {

          HybridDictionary sql_args = new HybridDictionary();
          sql_args["checksum"] = strChecksum;
          if (strVoidUserID.Length > 0)
            sql_args["voiduserid"] = strVoidUserID;
          sql_args["voiddt"] = strVoidDT;
          sql_args["trannbr"] = strTranNbr;
          sql_args["eventnbr"] = strEventNbr;
          sql_args["depfilenbr"] = strFileNbr;
          sql_args["depfileseq"] = strFileSequenceNbr;

          using (SqlCommand paramCommand = ParamUtils.CreateParamCommand(pDBConn, strSQL, sql_args))
        {
            paramCommand.ExecuteNonQuery();
          }
          // End Bug 20382


          // ALL GOOD - ACCEPT THE UPDATES.
          if (!args.has("DbConnectionClass"))// Caller will commit.
          {
            strError = "";
            if (!pDBConn.CommitToDatabase(ref strError))
            {
                HandleErrorsAndLocking("message", "Unable to commit changes to database. ", "code", "TS-1112", strError);//Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
            }
          }
        }
        else
        {
          geoBuffer.insert(strSQL);
        }

      }
      catch (Exception e)
      {
        // Bug 20382: FT: Make sure rollback is done correctly
        if (!args.has("DbConnectionClass"))// Caller will commit.
        {
          pDBConn.RollBack();
          Logger.LogError("Error trying to void a transaction: Error: " + e.Message, "Check CS Log for details");
          string msg = string.Format("Error voiding a transaction: file: {0}, Event: {1}, Tran: {2}.  Error: {3}",
            strFileName, strEventNbr, strTranNbr, e.ToMessageAndCompleteStacktrace());
          Logger.cs_log(msg);
          // Bug 17849
          Logger.cs_log_trace("Error in VoidTransaction", e);

        }
        // End Bug 20382: FT: Converted to parameterized
      }
      finally // Bug 11661
      {
        // Bug 15941. FT. Close the database connection
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);

        // BUG# 7516 D.H
        ThreadLock.GetInstance().ReleaseLock();
      }

      return true;
    }

    public static GenericObject PostCashieringTransaction(params Object[] arg_pairs)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      // Bug #6467 Mike O - Perform license check
      string error_message = "";
      GenericObject license = c_CASL.c_object("License.data") as GenericObject;
      if (!ValidateLicense(ref error_message, license))
        return CreateErrorObject("TS-1067", "Invalid license.\n" + error_message, false, "Invalid License", true);//Bug 11329 NJ-always show verbose

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject a_transaction = args.get("_subject", c_CASL.c_undefined) as GenericObject;
      GenericObject config = args.get("config", c_CASL.c_undefined) as GenericObject;
      GenericObject data = args.get("data", c_CASL.c_undefined) as GenericObject;
      GenericObject geoTransaction = null;
      string session_id = args.get("session_id") as string;
      string strStatus = "1"; // Active
      string strErr = "";

      // Bug 15941. FT. Moved here
      ConnectionTypeSynch pDBConn = null;

      ThreadLock.GetInstance().CreateLock();// Sync user's access to this resource. Bug# 5813 deadlock fix. DH 07/30/2008
      // Bug 11661: Wrap mutex release in finally block
      try
      {
        pDBConn = new ConnectionTypeSynch(data);
        int iDBType = pDBConn.GetDBConnectionType();

        if (!pDBConn.EstablishDatabaseConnection(ref strErr))
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-821", strErr);//Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        // This is used to update all transaction checksums after commit is called on the connection.
        GenericObject geoTranForChecksumUpdate = new GenericObject();

        string strFileName = a_transaction.get("file_FILENAME", "").ToString();
        string strFileDeptID = a_transaction.get("file_DEPTID", "").ToString();            
        string strEventNbr = a_transaction.get("event_EVENTNBR", "").ToString();
        string strEventUserID = a_transaction.get("event_USERID", "").ToString();
        string strLOGIN_ID = (string)a_transaction.get("LOGIN_ID", ""); // BUG 5395v2 (PostCashieringTransaction)
        string strFileSourceType = a_transaction.get("file_SOURCE_TYPE", "").ToString();
        string strFileSourceGroup = a_transaction.get("file_SOURCE_GROUP", "").ToString();
        string strSOURCE_REFID = a_transaction.get("SOURCE_REFID", "").ToString();
        string strSOURCE_DATE = a_transaction.get("SOURCE_DATE", "").ToString();

        // Break the file into file number and sequence number
        string strFileNumber = GetFileNumber(strFileName);
        string strFileSequenceNbr = GetFileSequence(strFileName);

        //if source came in as Portal, change it to Cashier
        if (strFileSourceType == "Portal")
          strFileSourceType = "Cashier";
        //end BUG 5395

        if (!pDBConn.EstablishRollback(ref strErr))
        {
          HandleErrorsAndLocking("message", "Error creating database transaction.", "code", "TS-822", strErr);//Bug 13832-added inner exception // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
        //--------------------------------------
        // Make sure file exists and is currently active BUG4489
        if (!IsFileActive(strFileNumber, strFileSequenceNbr, data, ref pDBConn))
          //ThrowCASLErrorAndCloseDB("TS-791", ref pDBConn,  "Transaction File " + strFileNumber + " is not Active!  No changes are permitted.");
          //Bug #6163
          return MakeCASLErrorButDoNOTCloseDB("Transaction Post Error", "TS-791", "Transaction File " + strFileNumber + " is not Active!  No changes are permitted.", ref pDBConn, true); //Bug 22945 NK

        //***********************************************************************
        // Create new event nbr or use the one passed in.
        if (strEventNbr.Length == 0)
        {
          // Bug 23061 SX Allow for Working Fund Sell Transaction Type to post to a Locked file
          string strTTID = GetKeyData(a_transaction, "TTID", true); 
          String strTTIDWFSellPath= String.Format("Business.Department.of.\"{0}\".wf_sell_transaction_id", strFileDeptID);
          string strTTIDWFSell = String.Format("{0}",c_CASL.c_object(strTTIDWFSellPath));
          // BUG#5396 File has been closed. You must log into a new file
          if (strTTID!=strTTIDWFSell && !IsFileClosed(strFileNumber, strFileSequenceNbr, data, ref pDBConn))
            //ThrowCASLErrorAndCloseDB("TS-1204", ref pDBConn,  "Transaction File " + strFileNumber + strFileSequenceNbr.PadLeft(3,'0') + " has been locked!  You must log into a new file. ");
            return MakeCASLErrorButDoNOTCloseDB("Transaction Post Error", "TS-1204", "Transaction File " + strFileNumber + strFileSequenceNbr.PadLeft(3, '0') + " has been locked!  You must log into a new file. ", ref pDBConn, true); // Bug 22923 NK
          strEventNbr = CreateNewEventNbr(ref pDBConn, data, strFileNumber, strFileSequenceNbr); //CL: DL!
          // New number will be sent back to GUI.
          a_transaction.set("event_EVENTNBR", strEventNbr);
          //-------------------------------------------------

          //**********************************************************************
          // Create SQL for insert into TG_PAYEVENT_DATA table and execute it.
          string strTodayDateTime = CreateTodayDateForOracle();
        // Bug #12055 Mike O - Use misc.Parse to log errors
        DateTime dtDate = misc.Parse<DateTime>(strTodayDateTime);

          //Matt -- replacing the old checksum call, since there is nothing in the db we need to create a struct
          GenericObject gTmp = new GenericObject("USERID", strEventUserID, "STATUS", strStatus, "MOD_DT", dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo));
          // Bug #10955 Mike O
          string strPayEventChecksum = CreatePayEventChecksum(Crypto.DefaultHashAlgorithm, "strEventNbr", strEventNbr, "strFileName", strFileName, "geoChecksumData", gTmp);

          try
          {
            // Bug 18766: Parameterized 
            string sql_string =
              @"INSERT INTO TG_PAYEVENT_DATA WITH (ROWLOCK) 
                (EVENTNBR, DEPFILENBR, DEPFILESEQ, USERID, STATUS, CREATION_DT, MOD_DT, SOURCE_TYPE, SOURCE_GROUP, SOURCE_REFID, SOURCE_DATE, CHECKSUM)
                VALUES(@eventnbr, @depfilenbr, @depfileseq, @userid, @status, @creation_dt,@mod_dt,@source_type, @source_group, @source_refid, @source_date, @checksum)";
            HybridDictionary sql_args = new HybridDictionary();
            sql_args["eventnbr"] = strEventNbr;
            sql_args["depfilenbr"] = strFileNumber;
            sql_args["depfileseq"] = strFileSequenceNbr;
            sql_args["userid"] = strEventUserID;
            sql_args["status"] = strStatus;
            sql_args["creation_dt"] = strTodayDateTime;
            sql_args["mod_dt"] = strTodayDateTime;
            sql_args["source_type"] = strFileSourceType;
            ParamUtils.setField(sql_args, "source_group", strFileSourceGroup);
            ParamUtils.setField(sql_args, "source_refid", strSOURCE_REFID);
            ParamUtils.setField(sql_args, "source_date", strSOURCE_DATE);
            sql_args["checksum"] = strPayEventChecksum;

            //string errorMsg Bug# 27153 DH - Clean up warnings when building ipayment
            int count = ParamUtils.runExecuteNonQuery(pDBConn, sql_string, sql_args);
            if (count != 1)
            {
              HandleErrorsAndLocking("message", "PostCashieringTransaction: Inserted: " + count + " records instead of 1", "code", "TS-823b", "Wrong insert count");
            }
          }
          catch (Exception e)
          {
            //Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
            HandleErrorsAndLocking("message", "PostCashieringTransaction: Error executing SQL.", "code", "TS-823", e.ToString()); 
          }

          // Update the transaction
          a_transaction.set("event_MOD_DT", strTodayDateTime);
        }
        //***********************************************************************

        string strError = "";
        try
        {
          geoTransaction = (GenericObject)PostIndividualTransaction(
            ref strError,
            "data", data,
            "DbConnectionClass", pDBConn,
            "_subject", a_transaction,
            "file_FILENAME", strFileName,
            "event_EVENTNBR", strEventNbr,
            "SOURCE_GROUP", strFileSourceGroup,
            "SOURCE_TYPE", strFileSourceType,
            "geoTranForChecksumUpdate", geoTranForChecksumUpdate);
        }
      catch (Exception e) 
        {
          string msg = "Insertion of transaction records failed: " + e.ToString();
          misc.CASL_call("a_method", "log", "args", new GenericObject("TYPE", "Transuite: PostCashieringTransaction", "ERROR_MESSAGE", msg));

          // Bug 25617: Rollback if there is a problem.
          // According to the doc, "The Close method rolls back any pending transactions."
          // but we should not depend on that.  We should rollback explicitly here.
          try
          {
            pDBConn.RollBack();
          }
          catch (Exception rollbackEx)
          {
            string rollbackMessage = "Cannot rollback insertion of transaction records: " + rollbackEx.ToString();
            misc.CASL_call("a_method", "log", "args", new GenericObject("TYPE", "Transuite: PostCashieringTransaction", "ERROR_MESSAGE", rollbackMessage));
          }
          HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-824", strError + ConstructDetailedError(e));//Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        if (geoTransaction == null)
        {
          HandleErrorsAndLocking("message", "Unable to post transaction. ", "code", "TS-825", strError);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        if (!pDBConn.CommitToDatabase(ref strErr))
        {
          HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-826", strErr);//Bug 13832-added inner exception// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        // UPDATE TRAN DATA CHECKSUM
        UpdateTransactionDataChecksum(geoTranForChecksumUpdate, pDBConn, data);
      }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnection(pDBConn);

        // Bug 11661
        ThreadLock.GetInstance().ReleaseLock(); //Bug# 5813 deadlock fix. DH 07/30/2008
      }

      return geoTransaction;
    }

    public static string CreateTodayDateForOracle()
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      // Create DateTime for ORACLE tables; 
      DateTime dtDateTime = DateTime.Now;// Will be used later for inserts.
      string strTodayDateTime = dtDateTime.ToString("MM/dd/yyyy HH:mm:ss");

      return strTodayDateTime;
    }

    public static string GetStringValue(GenericObject geo, object key)
    {
      object R = geo.get(key, "");
      if (R == null) return "";
      else if (R is double || R is decimal || R is float)
      {
        return R.ToString();
        // Bug#6186 To return double,decimal or float as string - to fix Consolidating deposits calculates over/short incorrectly ANU.
        // Bug #12055 Mike O - Use misc.Parse to log errors
          // Decimal R_float=misc.Parse<Decimal>(R.ToString()); 
        // return String.Format("{0:f}",R_float);

      }
      else if (R is DateTime)
        return ((DateTime)R).ToString("G", DateTimeFormatInfo.InvariantInfo);

      return R.ToString();
    }
    public static string TrimNumericString(string numeric_string)
    {
      string value_string = null;
      char[] trim_chars = " 0".ToCharArray();
      value_string = numeric_string.TrimStart(trim_chars);
      if (value_string.Length == 0)
        value_string = "0";
      else if (value_string[0] == '.')
        value_string = "0" + value_string;
      return value_string;
    }

    public static string CreateDepositUpdateSQL(GenericObject deposit, ref Hashtable sql_args, ref string error_message)
    {
      //#region Arguments
      string deposit_DEPOSITID = GetStringValue(deposit, "DEPOSITID");
      string deposit_DEPOSITNBR = GetStringValue(deposit, "DEPOSITNBR");

      string deposit_DEPOSITTYPE = GetStringValue(deposit, "DEPOSITTYPE");
      string deposit_DEPFILENBR = GetStringValue(deposit, "DEPFILENBR");
      string deposit_DEPFILESEQ = GetStringValue(deposit, "DEPFILESEQ");
      string deposit_OWNERUSERID = GetStringValue(deposit, "OWNERUSERID");
      string deposit_CREATERUSERID = GetStringValue(deposit, "CREATERUSERID");
      string deposit_COMMENTS = GetStringValue(deposit, "COMMENTS");
      string deposit_DEPSLIPNBR = GetStringValue(deposit, "DEPSLIPNBR");
      string deposit_GROUPID = GetStringValue(deposit, "GROUPID");
      string deposit_GROUPNBR = GetStringValue(deposit, "GROUPNBR");
      string deposit_BANKID = GetStringValue(deposit, "BANKID");
      string deposit_SOURCE_TYPE = GetStringValue(deposit, "SOURCE_TYPE");
      string deposit_SOURCE_GROUP = GetStringValue(deposit, "SOURCE_GROUP");
      string deposit_SOURCE_REFID = GetStringValue(deposit, "SOURCE_REFID");
      string deposit_SOURCE_DATE = GetStringValue(deposit, "SOURCE_DATE");

      string deposit_AMOUNT = GetStringValue(deposit, "AMOUNT");
      string deposit_CHECKSUM = GetStringValue(deposit, "CHECKSUM");
      string deposit_POSTDT = GetStringValue(deposit, "POSTDT");

      string deposit_VOIDDT = GetStringValue(deposit, "VOIDDT");
      string deposit_VOIDUSERID = GetStringValue(deposit, "VOIDUSERID");
      string deposit_ACCDT = GetStringValue(deposit, "ACCDT");
      string deposit_ACC_USERID = GetStringValue(deposit, "ACC_USERID");
      //#endregion
      //#region Convert Arguments
      if (deposit_DEPOSITTYPE.Length == 0)
        deposit_DEPOSITTYPE = null;
      if (deposit_DEPFILENBR.Length == 0)
        deposit_DEPFILENBR = null;
      if (deposit_DEPFILESEQ.Length == 0)
        deposit_DEPFILESEQ = null;
      if (deposit_OWNERUSERID.Length == 0)
        deposit_OWNERUSERID = null;
      if (deposit_CREATERUSERID.Length == 0)
        deposit_CREATERUSERID = null;
      if (deposit_COMMENTS.Length == 0)
        deposit_COMMENTS = null;
      if (deposit_DEPSLIPNBR.Length == 0)
        deposit_DEPSLIPNBR = null;
      if (deposit_GROUPID.Length == 0)
        deposit_GROUPID = null;
      if (deposit_GROUPNBR.Length == 0)
        deposit_GROUPNBR = null;
      if (deposit_BANKID.Length == 0)
        deposit_BANKID = null;
      if (deposit_SOURCE_TYPE.Length == 0)
        deposit_SOURCE_TYPE = null;
      if (deposit_SOURCE_GROUP.Length == 0)
        deposit_SOURCE_GROUP = null;
      if (deposit_SOURCE_REFID.Length == 0)
        deposit_SOURCE_REFID = null;
      if (deposit_SOURCE_DATE.Length == 0)
        deposit_SOURCE_DATE = null;
      if (deposit_AMOUNT.Length == 0)
        deposit_AMOUNT = null;
      if (deposit_CHECKSUM.Length == 0)
        deposit_CHECKSUM = null;
      if (deposit_POSTDT.Length == 0)
        deposit_POSTDT = null;
      if (deposit_VOIDDT.Length == 0)
        deposit_VOIDDT = null;
      if (deposit_VOIDUSERID.Length == 0)
        deposit_VOIDUSERID = null;
      if (deposit_ACCDT.Length == 0)
        deposit_ACCDT = null;
      if (deposit_ACC_USERID.Length == 0)
        deposit_ACC_USERID = null;
      //#endregion
      //#region Create SQL
      string sql_text = @"UPDATE TG_DEPOSIT_DATA
SET
  DEPOSITTYPE=@deposittype, GROUPID=@groupid,
  GROUPNBR=@groupnbr, DEPFILENBR=@depfilenbr, DEPFILESEQ=@depfileseq, OWNERUSERID=@owneruserid,
  CREATERUSERID=@createruserid, AMOUNT=@amount, POSTDT=@postdt, VOIDDT=@voiddt,
  VOIDUSERID=@voiduserid, ACCDT=@accdt, ACC_USERID=@acc_userid, COMMENTS=@comments,
  DEPSLIPNBR=@depslipnbr, BANKID=@bankid, SOURCE_TYPE=@source_type, SOURCE_GROUP=@source_group,
  SOURCE_REFID=@source_refid, SOURCE_DATE=@source_date, CHECKSUM=@checksum
WHERE
  DEPOSITID=@depositid AND DEPOSITNBR=@depositnbr";
      //#endregion
      //#region Set SQL Arguments
      sql_args["depositid"] = deposit_DEPOSITID;
      sql_args["depositnbr"] = deposit_DEPOSITNBR;
      sql_args["deposittype"] = deposit_DEPOSITTYPE;
      sql_args["depfilenbr"] = deposit_DEPFILENBR;
      sql_args["depfileseq"] = deposit_DEPFILESEQ;
      sql_args["owneruserid"] = deposit_OWNERUSERID;
      sql_args["createruserid"] = deposit_CREATERUSERID;
      sql_args["comments"] = deposit_COMMENTS;
      sql_args["depslipnbr"] = deposit_DEPSLIPNBR;
      sql_args["groupid"] = deposit_GROUPID;
      sql_args["groupnbr"] = deposit_GROUPNBR;
      sql_args["bankid"] = deposit_BANKID;
      sql_args["source_type"] = deposit_SOURCE_TYPE;
      sql_args["source_group"] = deposit_SOURCE_GROUP;
      sql_args["source_refid"] = deposit_SOURCE_REFID;
      sql_args["source_date"] = deposit_SOURCE_DATE;

      sql_args["amount"] = deposit_AMOUNT;
      sql_args["checksum"] = deposit_CHECKSUM;
      sql_args["postdt"] = deposit_POSTDT;

      sql_args["voiddt"] = deposit_VOIDDT;
      sql_args["voiduserid"] = deposit_VOIDUSERID;
      sql_args["accdt"] = deposit_ACCDT;
      sql_args["acc_userid"] = deposit_ACC_USERID;
      //#endregion
      return sql_text;
    }

    public static string CreateEventUpdateSQL(GenericObject payevent, ref Hashtable sql_args, ref string error_message)
    {
      //#region Arguments
      string event_USERID = GetStringValue(payevent, "USERID");
      string event_STATUS = GetStringValue(payevent, "STATUS");
      string event_MOD_DT = GetStringValue(payevent, "MOD_DT");
      string event_SOURCE_TYPE = GetStringValue(payevent, "SOURCE_TYPE");
      string event_SOURCE_GROUP = GetStringValue(payevent, "SOURCE_GROUP");
      string event_SOURCE_REFID = GetStringValue(payevent, "SOURCE_REFID");
      string event_SOURCE_DATE = GetStringValue(payevent, "SOURCE_DATE");
      string event_CHECKSUM = GetStringValue(payevent, "CHECKSUM");
      string event_UNIQUEID = GetStringValue(payevent, "UNIQUEID");
      //#endregion
      //#region Validate event data and convert
      if (event_SOURCE_TYPE == "")
        event_SOURCE_TYPE = null;
      if (event_SOURCE_GROUP == "")
        event_SOURCE_GROUP = null;
      if (event_SOURCE_REFID == "")
        event_SOURCE_REFID = null;
      if (event_SOURCE_DATE == "")
        event_SOURCE_DATE = null;
      else if (event_SOURCE_DATE.Length == 10)
        event_SOURCE_DATE += " 00:00:00";
      //#endregion
      //#region Create SQL
      string sql_string = @"UPDATE TG_PAYEVENT_DATA
SET USERID=@userid, STATUS=@status, MOD_DT=@mod_dt, SOURCE_TYPE=@source_type, SOURCE_GROUP=@source_group,
    SOURCE_REFID=@source_refid, SOURCE_DATE=@source_date, CHECKSUM=@checksum
WHERE UNIQUEID=@uniqueid";//BUG#13915 Add Active Event User ID 
      //#endregion
      //#region Set SQL arguments
      sql_args.Clear();
      sql_args["userid"] = event_USERID;
      sql_args["status"] = event_STATUS;
      sql_args["mod_dt"] = event_MOD_DT;
      sql_args["source_type"] = event_SOURCE_TYPE;
      sql_args["source_group"] = event_SOURCE_GROUP;
      sql_args["source_refid"] = event_SOURCE_REFID;
      sql_args["source_date"] = event_SOURCE_DATE;
      sql_args["checksum"] = event_CHECKSUM;
      sql_args["uniqueid"] = event_UNIQUEID;
      //#endregion
      return sql_string;
    }

    public static string CreateDepositTenderSQL(GenericObject deposit_tender, ref Hashtable sql_args, ref string error_message)
    {
      //#region Arguments
      string deposit_DEPOSITID = GetStringValue(deposit_tender, "DEPOSITID");
      string deposit_DEPOSITNBR = GetStringValue(deposit_tender, "DEPOSITNBR");
      string deposit_SEQNBR = GetStringValue(deposit_tender, "SEQNBR");
      string deposit_TNDRPROP = GetStringValue(deposit_tender, "TNDRPROP");
      string deposit_TNDR = GetStringValue(deposit_tender, "TNDR");
      string deposit_TNDRDESC = GetStringValue(deposit_tender, "TNDRDESC");
      string deposit_AMOUNT = GetStringValue(deposit_tender, "AMOUNT");
      string deposit_SOURCE_REFID = GetStringValue(deposit_tender, "SOURCE_REFID");
      string deposit_SOURCE_DATE = GetStringValue(deposit_tender, "SOURCE_DATE");
      string deposit_CHECKSUM = GetStringValue(deposit_tender, "CHECKSUM");
      //#region Validate transaction data and convert
      if (deposit_SOURCE_REFID.Length == 0)
        deposit_SOURCE_REFID = null;
      if (deposit_SOURCE_DATE.Length == 0)
        deposit_SOURCE_DATE = null;
      else if (deposit_SOURCE_DATE.Length == 10)
        deposit_SOURCE_DATE += " 00:00:00";
      if (deposit_CHECKSUM.Length == 0)
        deposit_CHECKSUM = null;
      //#endregion
      //#endregion
      //#region Create SQL
      string sql_string = @"INSERT INTO TG_DEPOSITTNDR_DATA
( DEPOSITID, DEPOSITNBR, SEQNBR, TNDRPROP, TNDR, TNDRDESC, AMOUNT, 
  SOURCE_REFID, SOURCE_DATE, CHECKSUM )
VALUES
( @depositid, @depositnbr, @seqnbr, @tndrprop, @tndr, @tndrdesc, @amount, 
  @source_refid, @source_date, @checksum )";
      //#endregion
      //#region Set SQL arguments
      sql_args.Clear();
      sql_args["depositid"] = deposit_DEPOSITID;
      sql_args["depositnbr"] = deposit_DEPOSITNBR;
      sql_args["seqnbr"] = deposit_SEQNBR;
      sql_args["tndrprop"] = deposit_TNDRPROP;
      sql_args["tndr"] = deposit_TNDR;
      sql_args["tndrdesc"] = deposit_TNDRDESC;
      sql_args["amount"] = deposit_AMOUNT;
      sql_args["source_refid"] = deposit_SOURCE_REFID;
      sql_args["source_date"] = deposit_SOURCE_DATE;
      sql_args["checksum"] = deposit_CHECKSUM;
      //#endregion
      return sql_string;
    }

    public static string CreateTransactionSQL(GenericObject transaction, ref Hashtable sql_args, ref string error_message)
    {
      //#region Arguments
      string tran_TRANNBR = GetStringValue(transaction, "TRANNBR");
      string tran_EVENTNBR = GetStringValue(transaction, "EVENTNBR");
      string tran_DEPFILENBR = GetStringValue(transaction, "DEPFILENBR");
      string tran_DEPFILESEQ = GetStringValue(transaction, "DEPFILESEQ");
      string tran_SOURCE_TYPE = GetStringValue(transaction, "SOURCE_TYPE");
      string tran_SOURCE_GROUP = GetStringValue(transaction, "SOURCE_GROUP");
      string tran_SOURCE_DATE = GetStringValue(transaction, "SOURCE_DATE");
      string tran_SOURCE_REFID = GetStringValue(transaction, "SOURCE_REFID");
      string tran_ITEMIND = GetStringValue(transaction, "ITEMIND");
      string tran_TRANAMT = GetStringValue(transaction, "TRANAMT");
      string tran_TTDESC = GetStringValue(transaction, "TTDESC");
      string tran_TTID = GetStringValue(transaction, "TTID");
      string tran_COMMENTS = GetStringValue(transaction, "COMMENTS");
      string tran_SYSTEXT = GetStringValue(transaction, "SYSTEXT");
      string tran_DISTAMT = GetStringValue(transaction, "DISTAMT");
      string tran_TAXEXIND = GetStringValue(transaction, "TAXEXIND");
      string tran_CONTENTTYPE = GetStringValue(transaction, "CONTENTTYPE");
      string tran_POSTDT = GetStringValue(transaction, "POSTDT");
      string tran_VOIDDT = GetStringValue(transaction, "VOIDDT");
      string tran_CHECKSUM = GetStringValue(transaction, "CHECKSUM");
      string tran_email_address = GetStringValue(transaction, "email_address");
      string tran_email_address_id = GetStringValue(transaction, "email_address_f_id");
      string tran_email_address_label = GetStringValue(transaction, "email_address_f_label");
      string tran_update_system_interface = GetStringValue(transaction, "update_system_interface_list");
      string tran_update_system_interface_label = GetStringValue(transaction, "update_system_interface_list_f_label");
      string tran_update_system_interface_list_id = GetStringValue(transaction, "update_system_interface_list_f_id");
      string tran_LOGIN_ID = GetStringValue(transaction, "LOGIN_ID"); // BUG#13915  add transaction post userid 

      if (tran_ITEMIND.Length == 0)
        tran_ITEMIND = null;
      if (tran_COMMENTS.Length == 0)
        tran_COMMENTS = null;
      if (tran_SYSTEXT.Length == 0)
        tran_SYSTEXT = null;
      if (tran_VOIDDT.Length == 0)
        tran_VOIDDT = null;
      if (tran_SOURCE_TYPE.Length == 0)
        tran_SOURCE_TYPE = null;
      if (tran_SOURCE_GROUP.Length == 0)
        tran_SOURCE_GROUP = null;
      if (tran_SOURCE_REFID.Length == 0)
        tran_SOURCE_REFID = null;
      if (tran_SOURCE_DATE.Length == 0)
        tran_SOURCE_DATE = null;
      else if (tran_SOURCE_DATE.Length == 10)
        tran_SOURCE_DATE += " 00:00:00";
      //#endregion
      //#region Validate transaction data and convert
      if (tran_CONTENTTYPE.Length == 0)
      {
        error_message = "Content Type is required. File post halted.";
        return null;
      }
      else if (tran_TTID.Length == 0)
      {
        error_message = "Transaction Type ID is required. File post halted.";
        return null;
      }
      if (tran_DISTAMT.Length == 0)
        tran_DISTAMT = "0";
      if (tran_TRANAMT.Length == 0)
        tran_TRANAMT = "0";

      // Bug #11026 Mike O - Make sure these only have two decimal places 
      tran_TRANAMT = Convert.ToDecimal(tran_TRANAMT).ToString("N2");// BUG19280 NEVER USE single precision float for currency
      tran_DISTAMT = Convert.ToDecimal(tran_DISTAMT).ToString("N2");// BUG19280 NEVER USE single precision float for currency
      //#endregion
      //#region Create SQL
      //BUG#13915 add POSTUSERID, fill by original user id or different user id 
	 
      string sql_string = @"INSERT INTO TG_TRAN_DATA
( TRANNBR, EVENTNBR, DEPFILENBR, DEPFILESEQ, CONTENTTYPE, TTID, TTDESC, TAXEXIND, DISTAMT, TRANAMT, ITEMIND, COMMENTS,
  SYSTEXT, POSTDT, SOURCE_TYPE, SOURCE_GROUP, SOURCE_REFID, SOURCE_DATE, VOIDDT, POSTUSERID, CHECKSUM)
VALUES
(@trannbr,@eventnbr,@depfilenbr,@depfileseq,@contenttype,@ttid,@ttdesc,@taxexind,@distamt,@tranamt,@itemind,@comments,
 @systext,@postdt,@source_type,@source_group,@source_refid,@source_date,@voiddt, @postuserid, @checksum)";
      //#endregion
      //#region Set SQL arguments
      sql_args.Clear();
      sql_args["trannbr"] = tran_TRANNBR;
      sql_args["eventnbr"] = tran_EVENTNBR;
      sql_args["depfilenbr"] = tran_DEPFILENBR;
      sql_args["depfileseq"] = tran_DEPFILESEQ;
      sql_args["contenttype"] = tran_CONTENTTYPE;
      sql_args["ttid"] = tran_TTID;
      sql_args["ttdesc"] = tran_TTDESC;
      sql_args["taxexind"] = tran_TAXEXIND;
      // Bug #11595 Mike O - Convert to a decimal
      sql_args["distamt"] = Convert.ToDecimal(tran_DISTAMT);
      sql_args["tranamt"] = Convert.ToDecimal(tran_TRANAMT);
      sql_args["itemind"] = tran_ITEMIND;
      sql_args["comments"] = tran_COMMENTS;
      ParamUtils.setField(sql_args, "systext", tran_SYSTEXT);
      sql_args["postdt"] = tran_POSTDT;
	    sql_args["source_type"] = tran_SOURCE_TYPE;
      ParamUtils.setField(sql_args, "source_group", tran_SOURCE_GROUP);
      ParamUtils.setField(sql_args, "source_refid", tran_SOURCE_REFID);
      ParamUtils.setField(sql_args, "source_date", tran_SOURCE_DATE);
      sql_args["voiddt"] = tran_VOIDDT;
      sql_args["postuserid"] = tran_LOGIN_ID;//BUG#13915  add transaction post userid 
      sql_args["checksum"] = tran_CHECKSUM;
      //#endregion
      return sql_string;
    }
    public static string CreateTenderSQL(GenericObject tender, ref Hashtable sql_args, ref string error_message)
    {
      //#region Arguments
      string tndr_TNDRNBR = GetStringValue(tender, "TNDRNBR");
      string tndr_EVENTNBR = GetStringValue(tender, "EVENTNBR");
      string tndr_DEPFILENBR = GetStringValue(tender, "DEPFILENBR");
      string tndr_DEPFILESEQ = GetStringValue(tender, "DEPFILESEQ");
      string tndr_TNDRID = GetStringValue(tender, "TNDRID");
      string tndr_TNDRDESC = GetStringValue(tender, "TNDRDESC");
      string tndr_TYPEIND = GetStringValue(tender, "TYPEIND");
      string tndr_AMOUNT = GetStringValue(tender, "AMOUNT");
      string tndr_CC_CK_NBR = GetStringValue(tender, "CC_CK_NBR");
      string tndr_EXPMONTH = GetStringValue(tender, "EXPMONTH");
      string tndr_EXPYEAR = GetStringValue(tender, "EXPYEAR");
      string tndr_CCNAME = GetStringValue(tender, "CCNAME");
      string tndr_AUTHNBR = GetStringValue(tender, "AUTHNBR");
      string tndr_AUTHSTRING = GetStringValue(tender, "AUTHSTRING");
      string tndr_AUTHACTION = GetStringValue(tender, "AUTHACTION");
      string tndr_BANKROUTINGNBR = GetStringValue(tender, "BANKROUTINGNBR");
      string tndr_BANKACCTNBR = GetStringValue(tender, "BANKACCTNBR");
      string tndr_ADDRESS = GetStringValue(tender, "ADDRESS");
      string tndr_SYSTEXT = GetStringValue(tender, "SYSTEXT");
      string tndr_POSTDT = GetStringValue(tender, "POSTDT");
      string tndr_VOIDDT = GetStringValue(tender, "VOIDDT");
      string tndr_VOIDUSERID = GetStringValue(tender, "VOIDUSERID");
      string tndr_SOURCE_TYPE = GetStringValue(tender, "SOURCE_TYPE");
      string tndr_SOURCE_GROUP = GetStringValue(tender, "SOURCE_GROUP");
      string tndr_SOURCE_REFID = GetStringValue(tender, "SOURCE_REFID");
      string tndr_SOURCE_DATE = GetStringValue(tender, "SOURCE_DATE");
      string tndr_CHECKSUM = GetStringValue(tender, "CHECKSUM");
      string tndr_BANKACCTID = GetStringValue(tender, "BANKACCTID");
      string tndr_LOGINID = GetStringValue(tender, "LOGIN_ID");

      if (tndr_TNDRID.Length == 0) tndr_TNDRID = null;
      if (tndr_TNDRDESC.Length == 0) tndr_TNDRDESC = null;
      if (tndr_TYPEIND.Length == 0) tndr_TYPEIND = null;
      if (tndr_AMOUNT.Length == 0) tndr_AMOUNT = null;
      if (tndr_CC_CK_NBR.Length == 0) tndr_CC_CK_NBR = null;
      if (tndr_EXPMONTH.Length == 0) tndr_EXPMONTH = null;
      if (tndr_EXPYEAR.Length == 0) tndr_EXPYEAR = null;
      if (tndr_CCNAME.Length == 0) tndr_CCNAME = null;
      if (tndr_AUTHNBR.Length == 0) tndr_AUTHNBR = null;
      if (tndr_AUTHSTRING.Length == 0) tndr_AUTHSTRING = null;
      if (tndr_AUTHACTION.Length == 0) tndr_AUTHACTION = null;
      if (tndr_BANKROUTINGNBR.Length == 0) tndr_BANKROUTINGNBR = null;
      if (tndr_BANKACCTNBR.Length == 0) tndr_BANKACCTNBR = null;
      if (tndr_ADDRESS.Length == 0) tndr_ADDRESS = null;
      if (tndr_SYSTEXT.Length == 0) tndr_SYSTEXT = null;
      if (tndr_POSTDT.Length == 0) tndr_POSTDT = null;
      if (tndr_VOIDDT.Length == 0) tndr_VOIDDT = null;
      if (tndr_VOIDUSERID.Length == 0) tndr_VOIDUSERID = null;
      if (tndr_SOURCE_TYPE.Length == 0) tndr_SOURCE_TYPE = null;
      if (tndr_SOURCE_GROUP.Length == 0) tndr_SOURCE_GROUP = null;
      if (tndr_SOURCE_REFID.Length == 0) tndr_SOURCE_REFID = null;
      if (tndr_SOURCE_DATE.Length == 0) tndr_SOURCE_DATE = null;
      else if (tndr_SOURCE_DATE.Length == 10)
        tndr_SOURCE_DATE += " 00:00:00";
      if (tndr_CHECKSUM.Length == 0) tndr_CHECKSUM = null;
      if (tndr_BANKACCTID.Length == 0) tndr_BANKACCTID = null;
      //#endregion
      //#region Validate transaction data and convert
      if (tndr_TNDRID.Length == 0)
      {
        error_message = "Tender ID is required. File post halted.";
        return null;
      }
      if (tndr_AMOUNT.Length == 0)
        tndr_AMOUNT = "0";

      // Bug #11026 Mike O - Make sure this only has two decimal places
      tndr_AMOUNT = Convert.ToDecimal(tndr_AMOUNT).ToString("N2");// BUG19280 NEVER USE single-precision-float for currency
      //#endregion
      //#region Create SQL
      // BUG#139115 Add POSTUSERID 
      string sql_string = @"INSERT INTO TG_TENDER_DATA
  ( TNDRNBR, EVENTNBR, DEPFILENBR, DEPFILESEQ, TNDRID, TNDRDESC, TYPEIND,AMOUNT,
    CC_CK_NBR, EXPMONTH, EXPYEAR, CCNAME, AUTHNBR, AUTHSTRING, AUTHACTION, BANKROUTINGNBR,
    BANKACCTNBR, ADDRESS, SYSTEXT, POSTDT, VOIDDT, VOIDUSERID, SOURCE_TYPE, SOURCE_GROUP,
    SOURCE_REFID, SOURCE_DATE, CHECKSUM, BANKACCTID,POSTUSERID )
  VALUES
  ( @tndrnbr, @eventnbr, @depfilenbr, @depfileseq, @tndrid, @tndrdesc, @typeind, @amount,
    @cc_ck_nbr, @expmonth, @expyear, @ccname, @authnbr, @authstring, @authaction, @bankroutingnbr,
    @bankacctnbr, @address, @systext, @postdt, @voiddt, @voiduserid, @source_type, @source_group,
    @source_refid, @source_date, @checksum, @bankacctid,@postuserid )";
      //#endregion
      //#region Set SQL arguments
      sql_args.Clear();
      sql_args["tndrnbr"] = tndr_TNDRNBR;
      sql_args["eventnbr"] = tndr_EVENTNBR;
      sql_args["depfilenbr"] = tndr_DEPFILENBR;
      sql_args["depfileseq"] = tndr_DEPFILESEQ;
      sql_args["tndrid"] = tndr_TNDRID;
      sql_args["tndrdesc"] = tndr_TNDRDESC;
      sql_args["typeind"] = tndr_TYPEIND;
      // Bug #11595 Mike O - Convert to a decimal
      sql_args["amount"] = Convert.ToDecimal(tndr_AMOUNT);
      sql_args["cc_ck_nbr"] = tndr_CC_CK_NBR;
      sql_args["expmonth"] = tndr_EXPMONTH;
      sql_args["expyear"] = tndr_EXPYEAR;
      sql_args["ccname"] = tndr_CCNAME;
      sql_args["authnbr"] = tndr_AUTHNBR;
      sql_args["authstring"] = tndr_AUTHSTRING;
      sql_args["authaction"] = tndr_AUTHACTION;
      sql_args["bankroutingnbr"] = tndr_BANKROUTINGNBR;
      sql_args["bankacctnbr"] = tndr_BANKACCTNBR;
      sql_args["address"] = tndr_ADDRESS;
      sql_args["systext"] = tndr_SYSTEXT;
      sql_args["postdt"] = tndr_POSTDT;
      sql_args["voiddt"] = tndr_VOIDDT;
      sql_args["voiduserid"] = tndr_VOIDUSERID;
      sql_args["source_type"] = tndr_SOURCE_TYPE;
      sql_args["source_group"] = tndr_SOURCE_GROUP;
      sql_args["source_refid"] = tndr_SOURCE_REFID;
      sql_args["source_date"] = tndr_SOURCE_DATE;
      sql_args["checksum"] = tndr_CHECKSUM;
      sql_args["bankacctid"] = tndr_BANKACCTID;
      sql_args["postuserid"] = tndr_LOGINID;// BUG#139115 Add POSTUSERID 
      //#endregion
      return sql_string;
    }
    public static string CreateGRCustomFieldSQL(GenericObject custom_field, ref Hashtable sql_args, ref string error_message)
    {
      //#region Arguments
      string custom_field_TRANNBR = GetStringValue(custom_field, "TRANNBR");
      string custom_field_EVENTNBR = GetStringValue(custom_field, "EVENTNBR");
      string custom_field_DEPFILENBR = GetStringValue(custom_field, "DEPFILENBR");
      string custom_field_DEPFILESEQ = GetStringValue(custom_field, "DEPFILESEQ");
      string custom_field_SCREENINDEX = GetStringValue(custom_field, "SCREENINDEX");
      string custom_field_CUSTLABEL = GetStringValue(custom_field, "CUSTLABEL");
      string custom_field_CUSTVALUE = GetStringValue(custom_field, "CUSTVALUE");
      string custom_field_CUSTID = GetStringValue(custom_field, "CUSTID");
      string custom_field_TTID = GetStringValue(custom_field, "TTID");
      string custom_field_CUSTTAG = GetStringValue(custom_field, "CUSTTAG");
      string custom_field_CUSTATTR = GetStringValue(custom_field, "CUSTATTR");
      string custom_field_CHECKSUM = GetStringValue(custom_field, "CHECKSUM");

      if (custom_field_CUSTLABEL == "")
        custom_field_CUSTLABEL = null;
      if (custom_field_CUSTVALUE == "")
        custom_field_CUSTVALUE = null;
      if (custom_field_CUSTID == "")
        custom_field_CUSTID = null;
      if (custom_field_TTID == "")
        custom_field_TTID = null;
      if (custom_field_CUSTTAG == "")
        custom_field_CUSTTAG = null;
      if (custom_field_CUSTATTR == "")
        custom_field_CUSTATTR = null;
      //#endregion
      //#region Create SQL
      string sql_string = @"INSERT INTO GR_CUST_FIELD_DATA
  ( TRANNBR, EVENTNBR, DEPFILENBR, DEPFILESEQ, SCREENINDEX, CUSTLABEL, CUSTVALUE, CUSTID, TTID, CUSTTAG, CUSTATTR, CHECKSUM )
  VALUES
  (@trannbr,@eventnbr,@depfilenbr,@depfileseq,@screenindex,@custlabel,@custvalue,@custid,@ttid,@custtag,@custattr,@checksum )";
      //#endregion
      //#region Set SQL arguments
      sql_args.Clear();

      sql_args["trannbr"] = custom_field_TRANNBR;
      sql_args["eventnbr"] = custom_field_EVENTNBR;
      sql_args["depfilenbr"] = custom_field_DEPFILENBR;
      sql_args["depfileseq"] = custom_field_DEPFILESEQ;
      sql_args["screenindex"] = custom_field_SCREENINDEX;
      sql_args["custlabel"] = custom_field_CUSTLABEL;
      sql_args["custvalue"] = custom_field_CUSTVALUE;
      sql_args["custid"] = custom_field_CUSTID;
      sql_args["ttid"] = custom_field_TTID;
      sql_args["custtag"] = custom_field_CUSTTAG;
      sql_args["custattr"] = custom_field_CUSTATTR;
      sql_args["checksum"] = custom_field_CHECKSUM;
      //#endregion      
      return sql_string;
    }
    public static string CreateItemSQL(GenericObject item, ref Hashtable sql_args, ref string error_message)
    {
      //#region Arguments
      string item_ITEMNBR = (string)item.get("ITEMNBR", "").ToString();
      string item_TRANNBR = (string)item.get("TRANNBR", "").ToString();
      string item_EVENTNBR = (string)item.get("EVENTNBR", "").ToString();
      string item_DEPFILENBR = (string)item.get("DEPFILENBR", "").ToString();
      string item_DEPFILESEQ = (string)item.get("DEPFILESEQ", "").ToString();
      string item_ITEMID = (string)item.get("ITEMID", "").ToString(); ;
      string item_GLACCTNBR = (string)item.get("GLACCTNBR", "");
      string item_ACCTID = (string)item.get("ACCTID", "");
      string item_ITEMACCTID = (string)item.get("ITEMACCTID", "");
      string item_ITEMDESC = (string)item.get("ITEMDESC", "");
      string item_AMOUNT = (string)item.get("AMOUNT", "").ToString();
      string item_QTY = (string)item.get("QTY", "").ToString();
      string item_TAXED = (string)item.get("TAXED", "");
      string item_TOTAL = (string)item.get("TOTAL", "").ToString();
      string item_CHECKSUM = (string)item.get("CHECKSUM", "");
      string item_ITEMCONFIGID = (string)item.get("ITEMCONFIGID", "");

      if (item_GLACCTNBR.Length == 0)
        item_GLACCTNBR = null;
      if (item_ACCTID.Length == 0)
        item_ACCTID = null;
      if (item_ITEMACCTID.Length == 0)
        item_ITEMACCTID = null;
      if (item_ITEMDESC.Length == 0)
        item_ITEMDESC = null;
      if (item_ITEMCONFIGID.Length == 0)
        item_ITEMCONFIGID = null;
      //#endregion
      //#region Create SQL
      string sql_string = @"INSERT INTO TG_ITEM_DATA
( ITEMNBR, TRANNBR, EVENTNBR, DEPFILENBR, DEPFILESEQ, ITEMID, GLACCTNBR, ACCTID, ITEMACCTID, ITEMDESC, AMOUNT, QTY,
  TAXED, TOTAL, CHECKSUM, ITEMCONFIGID)
VALUES
(@itemnbr,@trannbr,@eventnbr,@depfilenbr,@depfileseq,@itemid,@glacctnbr,@acctid,@itemacctid,@itemdesc,@amount,@qty,
 @taxed,@total,@checksum,@itemconfigid)";
      //#endregion
      //#region Set SQL arguments
      sql_args.Clear();

      sql_args["itemnbr"] = item_ITEMNBR;
      sql_args["trannbr"] = item_TRANNBR;
      sql_args["eventnbr"] = item_EVENTNBR;
      sql_args["depfilenbr"] = item_DEPFILENBR;
      sql_args["depfileseq"] = item_DEPFILESEQ;
      sql_args["itemid"] = item_ITEMID;
      sql_args["glacctnbr"] = item_GLACCTNBR;
      sql_args["acctid"] = item_ACCTID;
      sql_args["itemacctid"] = item_ITEMACCTID;
      sql_args["itemdesc"] = item_ITEMDESC;
      sql_args["amount"] = item_AMOUNT;
      sql_args["qty"] = item_QTY;
      sql_args["taxed"] = item_TAXED;
      sql_args["total"] = item_TOTAL;
      sql_args["checksum"] = item_CHECKSUM;
      sql_args["itemconfigid"] = item_ITEMCONFIGID;
      //#endregion
      return sql_string;
    }
    public static void AddVoidTransactionCommand(ref IDBVectorReader reader, GenericObject transaction)
    {
      string checksum = GetStringValue(transaction, "CHECKSUM");
      string voiduserid = GetStringValue(transaction, "VOIDUSERID");
      string voiddt = GetStringValue(transaction, "VOIDDT");
      string trannbr = GetStringValue(transaction, "TRANNBR");
      string eventnbr = GetStringValue(transaction, "EVENTNBR");
      string depfilenbr = GetStringValue(transaction, "DEPFILENBR");
      string depfileseq = GetStringValue(transaction, "DEPFILESEQ");

      string sql_string = "UPDATE TG_TRAN_DATA SET CHECKSUM=@checksum, VOIDUSERID=@voiduserid, VOIDDT=@voiddt WHERE TRANNBR=@trannbr AND EVENTNBR=@eventnbr AND DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq";
      Hashtable sql_args = new Hashtable();
      sql_args["checksum"] = checksum;
      sql_args["voiduserid"] = voiduserid;
      sql_args["voiddt"] = voiddt;
      sql_args["trannbr"] = trannbr;
      sql_args["eventnbr"] = eventnbr;
      sql_args["depfilenbr"] = depfilenbr;
      sql_args["depfileseq"] = depfileseq;

      reader.AddCommand(sql_string, CommandType.Text, sql_args);
    }
    public static void AddVoidTenderCommand(ref IDBVectorReader reader, GenericObject tender)
    {
      string checksum = GetStringValue(tender, "CHECKSUM");
      string voiduserid = GetStringValue(tender, "VOIDUSERID");
      string voiddt = GetStringValue(tender, "VOIDDT");
      string tndrnbr = GetStringValue(tender, "TNDRNBR");
      string eventnbr = GetStringValue(tender, "EVENTNBR");
      string depfilenbr = GetStringValue(tender, "DEPFILENBR");
      string depfileseq = GetStringValue(tender, "DEPFILESEQ");

      string sql_string = "UPDATE TG_TENDER_DATA SET CHECKSUM=@checksum, VOIDUSERID=@voiduserid, VOIDDT=@voiddt WHERE TNDRNBR=@tndrnbr AND EVENTNBR=@eventnbr AND DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq";
      Hashtable sql_args = new Hashtable();
      sql_args["checksum"] = checksum;
      sql_args["voiduserid"] = voiduserid;
      sql_args["voiddt"] = voiddt;
      sql_args["tndrnbr"] = tndrnbr;
      sql_args["eventnbr"] = eventnbr;
      sql_args["depfilenbr"] = depfilenbr;
      sql_args["depfileseq"] = depfileseq;

      reader.AddCommand(sql_string, CommandType.Text, sql_args);
    }
    public static void AddPostTransactionCommands(ref IDBVectorReader reader, GenericObject transaction, ref string error_message)
    {
      string sql_string = null;
      Hashtable sql_args = null;

      //#region Add transaction command
      sql_args = new Hashtable();
      sql_string = CreateTransactionSQL(transaction, ref sql_args, ref error_message);
      if (sql_string == null)
      {
        return;
      }
      reader.AddCommand(sql_string, CommandType.Text, sql_args);
      //#endregion
      //#region Add Item commands
      if (!transaction.has("table_TG_ITEM_DATA"))
      {
        error_message = "Each Transaction must have at least one item to post.";
        return;
      }
      GenericObject item_data = (GenericObject)transaction.get("table_TG_ITEM_DATA");
      int item_count = item_data.getLength();
      for (int i = 0; i < item_count; i++)
      {
        GenericObject item = item_data.get(i) as GenericObject;
        sql_args = new Hashtable();

        sql_string = CreateItemSQL(item, ref sql_args, ref error_message);
        if (sql_string == null)
        {
          return;
        }
        reader.AddCommand(sql_string, CommandType.Text, sql_args);
      }
      //#endregion
      //#region Add Custom field commands
      GenericObject custom_keys_vector = transaction.get("_custom_keys") as GenericObject;
      GenericObject custom_field = null;
      string custom_field_tag = null;
      int custom_field_count = custom_keys_vector.getLength();
      int screen_index = 0;
      string checksum = null;
      for (int i = 0; i < custom_field_count; i++)
      {
        screen_index++;
        custom_field_tag = (string)custom_keys_vector.get(i);
        custom_field = new GenericObject();
        custom_field.set(
          "TRANNBR", transaction.get("TRANNBR", ""),
          "EVENTNBR", transaction.get("EVENTNBR", ""),
          "DEPFILENBR", transaction.get("DEPFILENBR", ""),
          "DEPFILESEQ", transaction.get("DEPFILESEQ", ""),
          "SCREENINDEX", screen_index,
          "CUSTLABEL", transaction.get(custom_field_tag + "_f_label", ""),
          "CUSTVALUE", transaction.get(custom_field_tag, ""),
          "CUSTID", transaction.get(custom_field_tag + "_f_id", ""),
          "TTID", transaction.get("TTID", ""),
          "CUSTTAG", custom_field_tag,
          "CUSTATTR", transaction.get(custom_field_tag + "_f_attributes", "")
          );
        checksum = CreateGRCustomFieldChecksum(custom_field);
        custom_field.set("CHECKSUM", checksum);

        sql_string = CreateGRCustomFieldSQL(custom_field, ref sql_args, ref error_message);
        if (sql_string == null)
        {
          return;
        }
        reader.AddCommand(sql_string, CommandType.Text, sql_args);
      }
      //#endregion
    }

    public static GenericObject PostIndividualTransaction(ref string strError, params Object[] arg_pairs)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject a_transaction = args.get("_subject", c_CASL.c_undefined) as GenericObject;
      GenericObject data = args.get("data", c_CASL.c_undefined) as GenericObject;
      ConnectionTypeSynch pDBConn = null;// If part of posting, this will be already valid.
      bool connectionOpened = false;      // Bug 15941
      CoreFileInfoForChecksum pChecksumData;
      GenericObject geoTranForChecksumUpdate = null;
      string strFileName = (string)args.get("file_FILENAME");
      string strEventNbr = (string)args.get("event_EVENTNBR");
      string strFileSourceGroup = (string)args.get("SOURCE_GROUP");
      string strFileSourceType = (string)args.get("SOURCE_TYPE");
      string strSQL = "";

      // Break the file into file number and sequence number
      string strFileNumber = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      //**********************************************************************
      // Create DateTime for ORACLE tables; 
      string strTodayDateTime = CreateTodayDateForOracle();
      //**********************************************************************  
      try
      {

      // It is possible topass in a database connection using the "DbConnectionClass" key.
      // Connection must be of type ConnectionTypeSynch class.
      if (args.has("DbConnectionClass"))
      {
        pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");

        // Checksum will be updateted in the caller function.
        geoTranForChecksumUpdate = (GenericObject)args.get("geoTranForChecksumUpdate");
      }
      else
      {
        geoTranForChecksumUpdate = (GenericObject)new GenericObject();

        pDBConn = new ConnectionTypeSynch(data);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strError)))  // Bug 15941
        {
          strError += "Error opening database connection.";
          return null;
        }

        if (!pDBConn.EstablishRollback(ref strError))
        {
          string strErr = "Error creating database transaction." + strError;
          strError = strErr;
          return null;
        }
      }

      bool IsSQLServer2008orLater = pDBConn.IsServer_SQL2008orLater();/*BUG# 10600 / 10614 - DH 2/19/2011 Combine statements.*/

      string strSQLUnionData = "";/*BUG# 10600 / 10614 - DH 2/19/2011 Combine statements.*/

      string strTRANNBR = GetKeyData(a_transaction, "TRANNBR", false);
      string strSOURCE_DATE = GetKeyData(a_transaction, "SOURCE_DATE", false);
      string strSOURCE_REFID = GetKeyData(a_transaction, "SOURCE_REFID", false);
      string strITEMIND = GetKeyData(a_transaction, "ITEMIND", false);
      string strTRANAMT = GetKeyData(a_transaction, "TRANAMT", false);
      string stremail_address_f_label = GetKeyData(a_transaction, "email_address_f_label", false);
      string strupdate_system_interface_list_f_label = GetKeyData(a_transaction, "update_system_interface_list_f_label", false);
      string strupdate_system_interface_list = GetKeyData(a_transaction, "update_system_interface_list", false);
      string strTTDESC = GetKeyData(a_transaction, "TTDESC", false);
      string strTTID = GetKeyData(a_transaction, "TTID", false);
      string strCOMMENTS = GetKeyData(a_transaction, "COMMENTS", false);
      string stremail_address_f_id = GetKeyData(a_transaction, "email_address_f_id", false);
      string strSYSTEXT = GetKeyData(a_transaction, "SYSTEXT", false);
      string strDISTAMT = GetKeyData(a_transaction, "DISTAMT", false);
      string stremail_address = GetKeyData(a_transaction, "email_address", false);
      string strTAXEXIND = GetKeyData(a_transaction, "TAXEXIND", false);
      string strupdate_system_interface_list_f_id = GetKeyData(a_transaction, "update_system_interface_list_f_id", false);
      string strCONTENTTYPE = GetKeyData(a_transaction, "CONTENTTYPE", false);
      string strLOGIN_ID = GetKeyData(a_transaction, "LOGIN_ID", false);

      //---------------------------------------------
      // Get the next transaction number from TG_TRAN_DATA if one is not provided.
      // Loop thru all transactions and data items for each transaction.
      if (strTRANNBR.Length == 0)
      {
        // Bug 18766: Parameterized 
        long lNewTranNumber = DBUtils.runMaxTrannbrQuery(pDBConn, strFileNumber, strFileSequenceNbr, strEventNbr);
        strTRANNBR = Convert.ToString(lNewTranNumber);
      }
      a_transaction.set("TRANNBR", strTRANNBR);
      //---------------------------------------------

      //---------------------------------------------
      // Validate transaction data and convert.
      if (strCONTENTTYPE.Length == 0)
      {
        strError = "Content Type is required. File post halted.";
        return null;
      }

      if (strTTID.Length == 0)
      {
        strError = "Transaction Type ID is required. File post halted.";
        return null;
      }

      strTAXEXIND.ToLower();
      if (strTAXEXIND == "true")
        strTAXEXIND = "Y";
      else
        strTAXEXIND = "N";

      if (strDISTAMT == "")
        strDISTAMT = "0";

      if (strTRANAMT == "")
        strTRANAMT = "0";
      //---------------------------------------------

      //---------------------------------------------
      // CREATE SQL AND POST TRANSACTION DATA
     try
      {
          // Bug 18766: Parameterized INSERT INTO TG_TRAN_DATA //BUG#13915 add POSTUSERID
        string sql_string =
          @"INSERT INTO TG_TRAN_DATA 
            (TRANNBR, EVENTNBR, DEPFILENBR, DEPFILESEQ, CONTENTTYPE, TTID, TTDESC, TAXEXIND, DISTAMT, 
              TRANAMT, ITEMIND, COMMENTS, SYSTEXT, POSTDT, SOURCE_TYPE, SOURCE_GROUP, SOURCE_REFID, 
              SOURCE_DATE, VOIDDT,POSTUSERID) 
            VALUES 
              (@trannbr, @eventnbr, @depfilenbr, @depfileseq, @contenttype, @ttid, @ttdesc, @taxexind, @distamt,
              @tranamt, @itemind, @comments, @systext, @postdt, @source_type, @source_group, @source_refid, 
              @source_date, @voiddt,@postuserid)";
        HybridDictionary sql_args = new HybridDictionary();
        sql_args["trannbr"] = strTRANNBR;
        sql_args["eventnbr"] = strEventNbr;
        sql_args["depfilenbr"] = strFileNumber;
        sql_args["depfileseq"] = strFileSequenceNbr;
        sql_args["contenttype"] = strCONTENTTYPE;
        sql_args["ttid"] = strTTID;
        sql_args["ttdesc"] = strTTDESC;
        sql_args["taxexind"] = strTAXEXIND;
        sql_args["distamt"] = strDISTAMT;
        sql_args["tranamt"] = strTRANAMT;
        ParamUtils.setField(sql_args, "itemind", strITEMIND);
        ParamUtils.setField(sql_args, "comments", strCOMMENTS);
        ParamUtils.setField(sql_args, "systext", strSYSTEXT);

        sql_args["postdt"] = strTodayDateTime;
        ParamUtils.setField(sql_args, "source_type", strFileSourceType);
        ParamUtils.setField(sql_args, "source_group", strFileSourceGroup);
        ParamUtils.setField(sql_args, "source_refid", strSOURCE_REFID);
        ParamUtils.setField(sql_args, "source_date", strSOURCE_DATE);
        sql_args["voiddt"] = null;
        sql_args["postuserid"] = strLOGIN_ID; //BUG#13915 add POSTUSERID
        

        int count = ParamUtils.runExecuteNonQuery(pDBConn, sql_string, sql_args);
        if (count != 1)
      {
          HandleErrorsAndLocking("message", "PostIndivdualTransaction: Inserted: " + count + " records instead of 1", "code", "TS-824b", "Wrong insert count");
      }

      }
      catch (Exception e)
      {
        HandleErrorsAndLocking("message", "PostIndivdualTransaction: Error on TG_TRAN_DATA INSERT: Error executing SQL.", "code", "TS-824", e.ToString());
      }



      // Checksum will be updated after data is commited.
      pChecksumData = new CoreFileInfoForChecksum();
      pChecksumData.Init(strFileNumber, strFileSequenceNbr, strEventNbr, strTRANNBR);
      geoTranForChecksumUpdate.insert(pChecksumData);

      // Update GEO
      a_transaction.set("POSTDT", strTodayDateTime);
      //---------------------------------------------

      //****************************************************************************
      // LOOP THROUGH ALL TRANSACTION ITEM DATA
      if (!a_transaction.has("table_TG_ITEM_DATA"))
      {
        strError = "Each Transaction must have at least one Item to post.";
        return null;
      }

      // Bug 18766: Parameterized
      List<List<KeyValuePair<string, string>>> paramList = new List<List<KeyValuePair<string, string>>>();
      int paramCount = 0;

      GenericObject geoTGItemDataArr = (GenericObject)a_transaction.get("table_TG_ITEM_DATA");
      int lItemsInTransaction = (int)geoTGItemDataArr.getIntKeyLength();

      for (int j = 0; j < lItemsInTransaction; j++)
      {
        GenericObject geoTGItemDataElement = (GenericObject)geoTGItemDataArr.get(j);
        string strITEMACCTID = GetKeyData(geoTGItemDataElement, "ITEMACCTID", false);
        string strITEMID = GetKeyData(geoTGItemDataElement, "ITEMID", false);
        string strTOTAL = GetKeyData(geoTGItemDataElement, "TOTAL", false);
        string strITEMNBR = GetKeyData(geoTGItemDataElement, "ITEMNBR", false);
        string strTAXED = GetKeyData(geoTGItemDataElement, "TAXED", false);
        string strAMOUNT = GetKeyData(geoTGItemDataElement, "AMOUNT", false);
        string strITEMDESC = GetKeyData(geoTGItemDataElement, "ITEMDESC", false);
        string strQTY = GetKeyData(geoTGItemDataElement, "QTY", false);
        string strGLACCTNBR = GetKeyData(geoTGItemDataElement, "GLACCTNBR", false);
        string strACCTID = GetKeyData(geoTGItemDataElement, "ACCTID", false);
        string strITEMCONFIGID = GetKeyData(geoTGItemDataElement, "ITEMCONFIGID", false);
        //-----------------------------------------
        // Some validation of items.
        if (strITEMNBR.Length == 0)
        {
          strError = "Item number is required.  File post halted.";
          return null;
        }
        // BUG20173 support taxable allocation, change to new value T(taxable) X (not taxable), still support old value N(as always taxable)
        if (strTAXED == "T")
          strTAXED = "T";
        else
          strTAXED = "X";

        // Matt -- replacing the old checksum call
        // Bug #10955 Mike O
        string strChecksum = CreateItemDataChecksum(Crypto.DefaultHashAlgorithm, "geoChecksumData", geoTGItemDataElement, "strEventNbr", strEventNbr, "strTranNbr", strTRANNBR, "strFileName", strFileName, "strItemNbr", strITEMNBR);
        //-----------------------------------------

        // Bug 23645 MJO - Only use batch if there aren't too many parameters and hardcode whether batch should be used
        // NOTE: If the number of parameters is changed in the future, the parameter count needs to be updated
        if (IsSQLServer2008orLater && lItemsInTransaction * 16 <= 2100) /*BUG# 10600 / 10614 - DH 2/19/2011 Combine statements.*/
        {
          if (j > 0 && j < lItemsInTransaction)
            strSQLUnionData += ",";

          // Bug 18766: Parameterized
          strSQLUnionData += ParameterizedBatch.CreateSQLFor_TG_ITEM_DATA(paramList, paramCount, true, Int32.Parse(strFileNumber).ToString(),
        // Bug #12055 Mike O - Use misc.Parse to log errors       
          misc.Parse<Int32>(strFileSequenceNbr).ToString(),
          misc.Parse<Int32>(strEventNbr).ToString(),
          misc.Parse<Int32>(strTRANNBR).ToString(),
        strITEMACCTID,
        strITEMID,
        strTOTAL,
        strITEMNBR,
        strTAXED,
        strAMOUNT,
        strITEMDESC,
        strQTY,
        strGLACCTNBR,
        strACCTID,
        strChecksum,
        strITEMCONFIGID
        );
          paramCount++;       // Bug 18766: Parameterized

        }
        else
        {/*BUG# 10600 / 10614 - DH 2/19/2011 Combine statements.*/
          // Bug 18766: Parameterized
          strSQL = ParameterizedBatch.CreateSQLFor_TG_ITEM_DATA(paramList, paramCount, false, Int32.Parse(strFileNumber).ToString(),
                // Bug #12055 Mike O - Use misc.Parse to log errors
                       misc.Parse<Int32>(strFileSequenceNbr).ToString(),
                       misc.Parse<Int32>(strEventNbr).ToString(),
                       misc.Parse<Int32>(strTRANNBR).ToString(),
                    strITEMACCTID,
                    strITEMID,
                    strTOTAL,
                    strITEMNBR,
                    strTAXED,
                    strAMOUNT,
                    strITEMDESC,
                    strQTY,
                    strGLACCTNBR,
                    strACCTID,
                    strChecksum,
                    strITEMCONFIGID
                    );
          paramCount++;       // Bug 18766: Parameterized

          if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strError))
          {
            string strErr = "Error executing SQL." + strError;
            strError = strErr;
            return null;
          }
        }
        //-----------------------------------------
      }

      // BUG Bug 12272 DH - Only insert into TG_ITEM_DATA if there are items to be inserted. 
      // Cash float does not pass any custom fields.
      if (IsSQLServer2008orLater && strSQLUnionData != "") /*BUG# 10600 / 10614 - DH 2/19/2011 Combine statements.*/
      {
        strSQL = "INSERT INTO TG_ITEM_DATA (ITEMNBR, TRANNBR, EVENTNBR, DEPFILENBR, DEPFILESEQ, ITEMID, GLACCTNBR, ";
        strSQL += "ACCTID, ITEMACCTID, ITEMDESC, AMOUNT, QTY, TAXED, TOTAL, CHECKSUM, ITEMCONFIGID) VALUES ";

        // Bug 18766: Parameterized
        string parameterizedQueryString = ParameterizedBatch.buildParameterizedQueryForBatchInsert(paramList);
        ParameterizedBatch.runParameterizedBatchedInsert(pDBConn, strSQL, parameterizedQueryString, paramList);
        }
      //****************************************************************************

      //****************************************************************************
      // LOOP THROUGH ALL TRANSACTION CUSTOM KEYS DATA
      AddCustomFields("_subject", a_transaction,
        "data", data,
        "DbConnectionClass", pDBConn,
        "file_FILENAME", strFileName,
        "event_EVENTNBR", strEventNbr,
        "transaction_TRANNBR", strTRANNBR,
        "transaction_TTID", strTTID);
      //****************************************************************************

      // ALL GOOD - ACCEPT THE UPDATES.
      if (!args.has("DbConnectionClass"))// Caller will commit.
      {
        if (!pDBConn.CommitToDatabase(ref strError))
        {
          string strErr = "Unable to commit changes to database." + strError;
          strError = strErr;
          return null;
        }

        // UPDATE TRAN DATA CHECKSUM
        if (!UpdateTransactionDataChecksum(geoTranForChecksumUpdate, pDBConn, data))
        {
          strError = "Unable to set checksum for file.";
          return null;
        }
      }

    }
      finally
      {
        // Bug 15941. FT. Close the database connection
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }

      return a_transaction;

    }

    public static GenericObject PostTransactionToT3(params Object[] arg_pairs)
    {
      //			GenericObject args = misc.convert_args(arg_pairs);
      //			GenericObject a_transaction = args.get("_subject", c_CASL.c_undefined) as GenericObject;
      //			GenericObject config = args.get("config", c_CASL.c_undefined) as GenericObject;
      //
      //			String datasource_name = config.get("datasource_name") as string;
      //			String login_name = config.get("login_name") as string;
      //			String login_password = config.get("login_password") as string;
      //			CBT_TG_DBLib.CBT_TG_DBClass myCtrl = TranSuiteServicesInit("datasource_name",datasource_name, "login_name",login_name, "login_password",login_password);
      //
      //			//TODO: Get the File and Event level info and temp save in transaction
      //			//Requires:
      //			//	file_nbr
      //			//	source
      //			//	source_group
      //			//	user_id
      //			//Optional:
      //			//	receipt_nbr (Will return a receipt_nbr if this value is not provided)
      //			//	source_event_ref_id (Used if receipt_nbr is blank)
      //			//	source_event_date (Used if receipt_nbr is blank)
      //
      //			bool bRV = myCtrl.SourcePostTransaction(a_transaction);
      //			//DON'T USE ->
      //			//geoFileCopy.set("_container", a_core_file.get("_container")); --> Should just replace original
      //			if( bRV == false )
      //			{
      //				m_sErrMsg = myCtrl.GetErrMsg();
      //				HandleErrorsAndLocking("message", m_sErrMsg, "code", "TS-827");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      //			}
      //			myCtrl =null;
      //			//Return the COPY (with changes) if the post was successful
      //			return a_transaction;

      return new GenericObject();
    }

    public static GenericObject VoidTransactionInT3(params Object[] arg_pairs)
    {
      // Bug 15941. FT.  Moved here
      ConnectionTypeSynch pDBConn = null; 

      // Bug #8353 Mike O - Restored missing lock
      ThreadLock.GetInstance().CreateLock();

      GenericObject a_transaction = null;
      // Bug 11661: Wrap mutex release in finally block
      try
      {
        // Matt -- Voids a transaction, establishes new connection
        // **ToDo: replace body with VoidTransaction
        GenericObject args = misc.convert_args(arg_pairs);
        GenericObject config = args.get("config", c_CASL.c_undefined) as GenericObject;
        GenericObject data = args.get("data", c_CASL.c_undefined) as GenericObject;
        a_transaction = args.get("_subject", c_CASL.c_undefined) as GenericObject;
        string session_id = args.get("session_id") as string;

        pDBConn = new ConnectionTypeSynch(data);
        GenericObject geoChecksumData = new GenericObject();

        string strError = "";

        if (!pDBConn.EstablishDatabaseConnection(ref strError))
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-828", strError);///Bug 13832 - added detailed error message./ Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        if (!pDBConn.EstablishRollback(ref strError))
            HandleErrorsAndLocking("message", "Error creating database transaction.", "code", "TS-829", strError);//Bug 13832 - added detailed error message.// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        string strTranNbr = a_transaction.get("TRANNBR", "").ToString();
        string strFileName = a_transaction.get("file_FILENAME", "").ToString();
        string strEventNbr = a_transaction.get("event_EVENTNBR", "").ToString();

        string strFileSourceType = a_transaction.get("file_SOURCE_TYPE", "").ToString();
        string strFileSourceGroup = a_transaction.get("file_SOURCE_GROUP", "").ToString();
        string strSourceRefID = a_transaction.get("SOURCE_REFID", "").ToString();

        string strVoidDT = a_transaction.get("VOIDDT", "").ToString();
        string strVoidUserID = a_transaction.get("VOIDUSERID", "").ToString();
        string strLOGIN_ID = (string)a_transaction.get("LOGIN_ID", ""); //BUG 5395v2 (VoidTransactionInT3)
        string strFileNbr = "";
        string strFileSequenceNbr = "";
        string strSQL = "";

        //if source came in as Portal, change it to Cashier
        if (strFileSourceType == "Portal")
          strFileSourceType = "Cashier";
        //end BUG 5395

        //DateTime dtDate;
        GenericObject gTmp;

        if ((strTranNbr == "" || strEventNbr == "" || strFileName == "") &&
          (strFileSourceType == "" || strFileSourceGroup == "" || strSourceRefID == ""))
        {
          strError = "You must provide either (TranNbr, EventNbr, AND FileNbr) or (SourceType, SourceGroup, AND SourceRefID).";
          HandleErrorsAndLocking("message", strError, "code", "TS-830", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        //If the TranNbr, EventNbr or the FileName are empty, use the Source type, group and refid to get them
        if (strTranNbr == "" || strEventNbr == "" || strFileName == "")
        {
          strSQL = string.Format("SELECT * FROM TG_TRAN_DATA WHERE SOURCE_TYPE=\'{0}\' AND SOURCE_GROUP=\'{1}\' AND SOURCE_REFID=\'{2}\'", strFileSourceType, strFileSourceGroup, strSourceRefID);

          gTmp = (GenericObject)GetRecords(data, 0, strSQL, pDBConn.GetDBConnectionType());

          strTranNbr = GetKeyData(gTmp, "TRANNBR", false);
          strFileNbr = GetKeyData(gTmp, "DEPFILENBR", false);

          //Need to pad the DEPFILESEQ with 0s for the full FileName
          strFileSequenceNbr = GetKeyData(gTmp, "DEPFILESEQ", false);
          strFileSequenceNbr = strFileSequenceNbr.PadLeft(3, '0');

          strFileName = String.Concat(strFileNbr, strFileSequenceNbr);
          strEventNbr = GetKeyData(gTmp, "EVENTNBR", false);
        }
        else
        {
          strFileNbr = strFileName.Substring(0, 7);
          strFileSequenceNbr = strFileName.Substring(7, strFileName.Length - 7);

        // Bug #12055 Mike O - Use misc.Parse to log errors				
        // Bug 20382: FT: Converted to parameterized
        strSQL = "SELECT * FROM TG_TRAN_DATA WHERE TRANNBR=@trannbr AND EVENTNBR=@eventnbr AND DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq";

        Hashtable sql_args = new Hashtable();
        sql_args["trannbr"] = strTranNbr;
        sql_args["eventnbr"] = strEventNbr;
        sql_args["depfilenbr"] = strFileNbr;
        sql_args["depfileseq"] = strFileSequenceNbr;
        gTmp = (GenericObject)GetRecordsParameterized(data, sql_args, 0, strSQL, pDBConn.GetDBConnectionType());
        // End Bug 20382
        }

        if (gTmp == null)
        {
          strError = "No records found";
          HandleErrorsAndLocking("message", strError, "code", "TS-831", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        //Is File active?
      // Bug #12055 Mike O - Use misc.Parse to log errors
        // Bug 20382: FT: Converted to parameterized
        string sqlString = "SELECT COUNT(*) AS COUNT FROM TG_DEPFILE_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND BALDT IS NULL";
        HybridDictionary hybridDict = new HybridDictionary();
        hybridDict["depfilenbr"] = strFileNbr;
        hybridDict["depfileseq"] = strFileSequenceNbr;
        long lActiveOrNot = GetTableRowCount(pDBConn, sqlString, hybridDict);
        // End Bug 20382

        if (lActiveOrNot == 0)
          HandleErrorsAndLocking("message", "Transaction File " + strFileNbr + " is not Active!  No changes are permitted.", "code", "TS-832", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        if (!VoidTransaction("geoChecksumData", gTmp, "DbConnectionClass", pDBConn, "strTranNbr", strTranNbr, "strFileName", strFileName, "strEventNbr", strEventNbr, "strVoidUserID", strVoidUserID, "strVoidDT", strVoidDT))
          HandleErrorsAndLocking("message", "Transaction could not be voided. ", "code", "TS-833", m_sErrMsg);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        if (!pDBConn.CommitToDatabase(ref strError))
          HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-835", strError);//Bug 13832-added detailed error // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        a_transaction.set("status", "void");

      }
      finally
      {
        // Bug 15941. FT. Close the database connection
        CloseDBConnection(pDBConn);

        // Bug #8353 Mike O - Release missing lock
        // Bug 11661
        ThreadLock.GetInstance().ReleaseLock();
      }

      return a_transaction;

    }

    public static GenericObject PostTenderToT3(params Object[] arg_pairs)
    {
      //			GenericObject args = misc.convert_args(arg_pairs);
      //			GenericObject a_tender = args.get("_subject", c_CASL.c_undefined) as GenericObject;
      //			GenericObject config = args.get("config", c_CASL.c_undefined) as GenericObject;
      //
      //			String datasource_name = config.get("datasource_name") as string;
      //			String login_name = config.get("login_name") as string;
      //			String login_password = config.get("login_password") as string;
      //			CBT_TG_DBLib.CBT_TG_DBClass myCtrl = TranSuiteServicesInit("datasource_name",datasource_name, "login_name",login_name, "login_password",login_password);
      //
      //			//TODO: Get the File and Event level info and temp save in transaction
      //			//Requires:
      //			//	file_nbr
      //			//	source
      //			//	source_group
      //			//	user_id
      //			//Optional:
      //			//	receipt_nbr (Will return a receipt_nbr if this value is not provided)
      //			//	source_event_ref_id (Used if receipt_nbr is blank)
      //			//	source_event_date (Used if receipt_nbr is blank)
      //
      //			bool bRV = myCtrl.SourcePostTender(a_tender);
      //			//DON'T USE ->
      //			//geoFileCopy.set("_container", a_core_file.get("_container")); --> Should just replace original
      //			if( bRV == false )
      //			{
      //				m_sErrMsg = myCtrl.GetErrMsg();
      //				HandleErrorsAndLocking("message", m_sErrMsg, "code", "TS-836");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      //			}
      //			myCtrl =null;
      //			//Return the COPY (with changes) if the post was successful
      //
      //			return a_tender;

      return new GenericObject();
    }

    public static GenericObject VoidTenderInT3(params Object[] arg_pairs)
    {
      // Matt -- Voids a tender from T3, establishes new connection
      // **ToDo: replace body with VoidTender

      ConnectionTypeSynch pDBConn = null;
      GenericObject a_tender = null;
      string strError = "";
      // BUG# 7516 D.H
      ThreadLock.GetInstance().CreateLock();
      // Bug 11661: Wrap mutex release in finally block
      try
      {
        GenericObject args = misc.convert_args(arg_pairs);
        GenericObject config = args.get("config", c_CASL.c_undefined) as GenericObject;
        GenericObject data = args.get("data", c_CASL.c_undefined) as GenericObject;
        a_tender = args.get("_subject", c_CASL.c_undefined) as GenericObject;
        string session_id = args.get("session_id") as string;

        pDBConn = new ConnectionTypeSynch(data);
       

        if (!pDBConn.EstablishDatabaseConnection(ref strError))
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-837", strError);//Bug 13832-added detailed error// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        if (!pDBConn.EstablishRollback(ref strError))
            HandleErrorsAndLocking("message", "Error creating database transaction.", "code", "TS-838", strError);//Bug 13832-added detailed error// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        GenericObject geoChecksumData = new GenericObject();

        string strTndrNbr = a_tender.get("TNDRNBR", "").ToString();
        string strFileName = a_tender.get("file_FILENAME", "").ToString();
        string strEventNbr = a_tender.get("event_EVENTNBR", "").ToString();

        string strFileSourceType = a_tender.get("file_SOURCE_TYPE", "").ToString();
        string strFileSourceGroup = a_tender.get("file_SOURCE_GROUP", "").ToString();
        string strSourceRefID = a_tender.get("SOURCE_REFID", "").ToString();

        string strVoidDT = a_tender.get("VOIDDT", "").ToString();
        string strVoidUserID = a_tender.get("VOIDUSERID", "").ToString();
        string strLOGIN_ID = (string)a_tender.get("LOGIN_ID", ""); //BUG 5395v2 (VoidTenderInT3)

        string strFileNbr = "";
        string strFileSequenceNbr = "";
        string strSQL = "";

        //if source came in as Portal, change it to Cashier
        if (strFileSourceType == "Portal")
          strFileSourceType = "Cashier";
        //end BUG 5395

        GenericObject gTmp;

        if ((strTndrNbr == "" || strEventNbr == "" || strFileName == "") &&
          (strFileSourceType == "" || strFileSourceGroup == "" || strSourceRefID == ""))
        {
          strError = "You must provide either (TndrNbr, EventNbr, AND FileNbr) or (SourceType, SourceGroup, AND SourceRefID).";
          HandleErrorsAndLocking("message", strError, "code", "TS-839", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        //If the TndrNbr, EventNbr or the FileName are empty, use the Source type, group and refid to get them
        if (strTndrNbr == "" || strEventNbr == "" || strFileName == "")
        {
          strSQL = string.Format("SELECT * FROM TG_TENDER_DATA WHERE SOURCE_TYPE=\'{0}\' AND SOURCE_GROUP=\'{1}\' AND SOURCE_REFID=\'{2}\'", strFileSourceType, strFileSourceGroup, strSourceRefID);

          gTmp = (GenericObject)GetRecords(data, 0, strSQL, pDBConn.GetDBConnectionType());

          strTndrNbr = GetKeyData(gTmp, "TNDRNBR", true);
          strFileNbr = GetKeyData(gTmp, "DEPFILENBR", true);

          //Need to pad the DEPFILESEQ with 0s for the full FileName
          strFileSequenceNbr = GetKeyData(gTmp, "DEPFILESEQ", true);
          strFileSequenceNbr = strFileSequenceNbr.PadLeft(3, '0');

          strFileName = String.Concat(strFileNbr, strFileSequenceNbr);
          strEventNbr = GetKeyData(gTmp, "EVENTNBR", true);
        }
        else
        {
          strFileNbr = GetFileNumber(strFileName); //strFileName.Substring(0,7);
          strFileSequenceNbr = GetFileSequence(strFileName); //strFileName.Substring(7, strFileName.Length-7);

        // Bug #12055 Mike O - Use misc.Parse to log errors				
        strSQL = string.Format("SELECT * FROM TG_TENDER_DATA WHERE TNDRNBR={0} AND EVENTNBR={1} AND DEPFILENBR={2} AND DEPFILESEQ={3}", misc.Parse<Int64>(strTndrNbr), misc.Parse<Int64>(strEventNbr), misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr));

          gTmp = (GenericObject)GetRecords(data, 0, strSQL, pDBConn.GetDBConnectionType());
        }

        if (gTmp == null)
        {
          strError = "No records found";
          HandleErrorsAndLocking("message", strError, "code", "TS-840", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        //IS FILE ACTIVE?
      // Bug #12055 Mike O - Use misc.Parse to log errors
        // Bug 20382: FT: Converted to parameterized
        string sqlString = "SELECT COUNT(*) AS COUNT FROM TG_DEPFILE_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND BALDT IS NULL";
        HybridDictionary hybridDict = new HybridDictionary();
        hybridDict["depfilenbr"] = strFileNbr;
        hybridDict["depfileseq"] = strFileSequenceNbr;
        long lActiveOrNot = GetTableRowCount(pDBConn, sqlString, hybridDict);
        // End Bug 20382

        if (lActiveOrNot == 0)
          HandleErrorsAndLocking("message", "Transaction File " + strFileNbr + " is not Active!  No changes are permitted.", "code", "TS-841", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        //Validate the Tender Data Checksum	

        if (!VoidTender("geoChecksumData", gTmp, "DbConnectionClass", pDBConn, "strTndrNbr", strTndrNbr, "strFileName", strFileName, "strEventNbr", strEventNbr, "strVoidUserID", strVoidUserID, "strVoidDT", strVoidDT))
          HandleErrorsAndLocking("message", "Tender could not be voided.", "code", "TS-842", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        if (!pDBConn.CommitToDatabase(ref strError))
            HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-843", strError);//Bug 13832-added detailed error // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        a_tender.set("status", "void");

      }
      finally
      {
        // Bug 11661
        ThreadLock.GetInstance().ReleaseLock();
        CloseDBConnection(pDBConn);   // Bug 15941
      }

      return a_tender;
    }


    public static GenericObject CASLConsolidateDeposits2(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      GenericObject config = (GenericObject)geo_args.get("config", c_CASL.c_undefined);
      GenericObject data = (GenericObject)geo_args.get("data", c_CASL.c_undefined);
      string session_id = (string)geo_args.get("session_id");

      GenericObject deposits = (GenericObject)geo_args.get("DEPOSITS_to_consolidate", c_CASL.c_undefined);
      string filename_id = (string)geo_args.get("FILENAME", "");
      string login_id = (string)geo_args.get("LOGIN_ID", "");
      string update_user_id = (string)geo_args.get("UPDATE_USERID", "");
      string file_source_type = (string)geo_args.get("FILE_SOURCE_TYPE");

      string owner_user_id = (string)geo_args.get("OWNERUSERID", "");
      string create_user_id = (string)geo_args.get("CREATERUSER", "");
      string comments = (string)geo_args.get("COMMENTS", "");
      string slip_nbr = (string)geo_args.get("SLIPNBR", "");

      return ConsolidateDeposits(
        config, data, session_id,
        deposits, filename_id, owner_user_id, create_user_id, comments, slip_nbr, file_source_type, update_user_id, login_id);
    }

    public static GenericObject ConsolidateDeposits(GenericObject config, GenericObject data, string session_id,
      GenericObject deposits,
      string filename_id,
      string owner_user_id,
      string create_user_id,
      string comments,
      string slip_nbr,
      string file_source_type,
      string update_user_id,
      string login_id)
    {
      //#region Arguments
      StringBuilder details = new StringBuilder();

      GenericObject new_deposit = CreateNewDeposit(data,
        String.Format("TSFD{0:yyyyMMdd}", DateTime.Now));
      string error_message = "";
      //#endregion
      //#region Mark deposits to prevent others from attempting to access them
      GenericObject a_deposit;
      string a_deposit_DEPOSITID, a_deposit_DEPOSITNBR;
      StringBuilder where_clause_builder = new StringBuilder();

      //#region Add first deposit to the where clause
      a_deposit = (GenericObject)deposits.get(0);
      a_deposit_DEPOSITID = GetStringValue(a_deposit, "DEPOSITID");
      a_deposit_DEPOSITNBR = GetStringValue(a_deposit, "DEPOSITNBR");
      where_clause_builder.Append("(DEPOSITID='" + a_deposit_DEPOSITID + "' AND DEPOSITNBR=" + a_deposit_DEPOSITNBR + ")");
      //#endregion
      //#region Add remaining deposits to where clause
      for (int i = 1; i < deposits.getLength(); i++)
      {
        a_deposit = (GenericObject)deposits.get(i);
        a_deposit_DEPOSITID = GetStringValue(a_deposit, "DEPOSITID");
        a_deposit_DEPOSITNBR = GetStringValue(a_deposit, "DEPOSITNBR");

        where_clause_builder.Append("OR (DEPOSITID='" + a_deposit_DEPOSITID + "' AND DEPOSITNBR=" + a_deposit_DEPOSITNBR + ")");
      }
      //#endregion
      //#region Create reader
      IDBVectorReader reader = db_reader.MakeTransactionReader(data, 0, 0, 0, ref error_message);
      if (error_message != "")
      {
        //Bug 11329 NJ-used this method to return as user friendly message
        GenericObject return_object = CreateErrorObject("TS-1239", "", false, "", true);
        return_object.set("inner_error", misc.MakeCASLError("", "", error_message));
        return return_object;
        //end bug 11329
      }
      //#endregion
      //#region Update all deposits in list
      string sql_statement = String.Format("UPDATE TG_DEPOSIT_DATA SET DEPOSITTYPE='PRE_DEPOSIT' WHERE DEPOSITTYPE <> 'PRE_DEPOSIT' AND ({0});", where_clause_builder.ToString());
      reader.AddCommand(sql_statement, CommandType.Text, null);

      reader.EstablishTransaction(ref error_message);
      if (error_message != "")
      {
        //Bug 11329 NJ-used this method to return as user friendly message
        GenericObject return_object = CreateErrorObject("TS-1239", "", false, "", true);
        return_object.set("inner_error", misc.MakeCASLError("", "", error_message));
        return return_object;
        //end bug 11329
      }

      int row_count = 0;
      bool success = reader.ExecuteTransactionCommands(ref error_message, ref row_count, true);
      //#endregion
      //#region Rollback and die if not all deposits were updated
      // Undoing mod for 7813 -- deposits is holding the right size now and this 
      if (!success || error_message != "" || row_count != deposits.getLength())
      {
        // Error Condition
        string return_error_message = error_message;
        error_message = "";
        success = reader.RollbackTransaction(ref error_message);
        if (!success || error_message != "")
        {
          return_error_message = "Rollback failed. " + return_error_message + " " + error_message;
        }
        // Rollback and die
        //Bug 11329 NJ-used this method to return as user friendly message
        GenericObject return_object = CreateErrorObject("TS-1239", "", false, "", true);
        return_object.set("inner_error", misc.MakeCASLError("", "", return_error_message));
        return return_object;
        //end bug 11329
      }
      success = reader.CommitTransaction(ref error_message);
      if (!success || error_message != "")
      {
        string return_error_message = error_message;
        error_message = "";
        success = reader.RollbackTransaction(ref error_message);
        if (!success || error_message != "")
        {
          return_error_message = "Commit failed. " + return_error_message + " " + error_message;
        }
        //Bug 11329 NJ-used this method to return as user friendly message
        GenericObject return_object = CreateErrorObject("TS-1239", "", false, "", true);
        return_object.set("inner_error", misc.MakeCASLError("", "", return_error_message));
        return return_object;
        //end bug 11329
      }
      //#endregion
      //#endregion
      //#region Continue to fix up the deposits with the new information
      //#region Get all the old deposits
      GenericObject old_deposits = GetDeposits(deposits, data);
      //#endregion
      //#region Prepare new deposit values
      a_deposit = (GenericObject)old_deposits.get(0);
      string new_groupid = GetStringValue(a_deposit, "GROUPID");
      string new_groupnbr = GetStringValue(a_deposit, "GROUPNBR");
      string new_bankid = GetStringValue(a_deposit, "BANKID");
      string new_dep_source_type = GetStringValue(a_deposit, "file_SOURCE_TYPE");
      string new_dep_source_group = GetStringValue(a_deposit, "file_SOURCE_GROUP");
      string new_dep_source_refid = GetStringValue(a_deposit, "SOURCE_REFID");
      string new_dep_source_date = GetStringValue(a_deposit, "SOURCE_DATE");
      //#endregion
      //#region Set new deposit values
      string depfilenbr = GetFileNumber(filename_id);
      string depfileseq = GetFileSequence(filename_id);

      // Bug 22460: Find out if this is a credit card deposit
      //bool isCreditCard = isCreditCardDeposit(a_deposit);

      // Bug 20731 / 22460: Generate a slip number if the user has not typed it in
            // or is missing in some other way
            // Let's not call this until I check with Jeff.  
            // The slip number will only be blank if the user does not type in anything
      slip_nbr = generateSlipNumberAndStripLeadingZeros(depfilenbr, depfileseq, GetStringValue(new_deposit, "DEPOSITID"), GetStringValue(new_deposit, "DEPOSITNBR"), filename_id, slip_nbr, new_bankid);

      new_deposit.set(
        "DEPOSITTYPE", "FILE",               // static
        "DEPFILENBR", depfilenbr,           // calculated from passed in
        "DEPFILESEQ", depfileseq,           // calculated from passed in
        "OWNERUSERID", owner_user_id,        // passed in
        "CREATERUSERID", create_user_id,       // passed in
        "COMMENTS", comments,             // passed in
        "DEPSLIPNBR", slip_nbr,             // passed in
        "GROUPID", new_groupid,          // grab from first deposit
        "GROUPNBR", new_groupnbr,         // grab from first deposit
        "BANKID", new_bankid,           // grab from first deposit
        "SOURCE_TYPE", new_dep_source_type,  // grab from first deposit
        "SOURCE_GROUP", new_dep_source_group, // grab from first deposit
        "SOURCE_REFID", new_dep_source_refid, // grab from first deposit
        "SOURCE_DATE", new_dep_source_date   // grab from first deposit (Should this be calculated or something?)
        );
      //#endregion
      //#region Increment amount and update old_deposits
      double deposit_total = 0.0;
      GenericObject deposit_tenders, a_deposit_tender;
      error_message = "";
      PreDepositTender deposit_tender_list = new PreDepositTender();
      for (int i = 0; i < old_deposits.getLength(); i++)
      {
        a_deposit = (GenericObject)old_deposits.get(i);
        // Bug #12055 Mike O - Use misc.Parse to log errors
        deposit_total += misc.Parse<Double>(GetStringValue(a_deposit, "AMOUNT"));

        deposit_tenders = GetDepositTenders(a_deposit, data);

        for (int j = 0; j < deposit_tenders.getLength(); j++)
        {
          a_deposit_tender = (GenericObject)deposit_tenders.get(j);
          /* add tender to deposit_tender_list */
          // Bug #12055 Mike O - Use misc.Parse to log errors
          deposit_tender_list.Add(
            GetStringValue(new_deposit, "DEPOSITID"),
            misc.Parse<Int64>(GetStringValue(new_deposit, "DEPOSITNBR")),
            misc.Parse<Int64>(GetStringValue(a_deposit_tender, "SEQNBR")),
            GetStringValue(a_deposit_tender, "TNDRPROP"),
            GetStringValue(a_deposit_tender, "TNDR"),
            GetStringValue(a_deposit_tender, "TNDRDESC"),
            misc.Parse<Double>(GetStringValue(a_deposit_tender, "AMOUNT")),
            GetStringValue(a_deposit_tender, "SOURCE_REFID"),
            GetStringValue(a_deposit_tender, "SOURCE_DATE")
            );
        }
      }
      //#endregion
      //#region Set final attributes for new deposit

      new_deposit.set(
        "AMOUNT", deposit_total.ToString(),
        "POSTDT", DateTime.Now,
        "VOIDDT", null,
        "VOIDUSERID", null,
        "ACCDT", null,
        "ACC_USERID", null
        );
      GenericObject deposit_tender_objects = deposit_tender_list.ReturnInGEOFormat();
      //#endregion
      //#endregion
      //#region Post the new deposit
      //#region Update new deposit
      new_deposit.set("CHECKSUM", CreateDepositChecksum(new_deposit));
      Hashtable sql_args = new Hashtable();
      string sql_string = CreateDepositUpdateSQL(new_deposit, ref sql_args, ref error_message),
        sub_error_message = "";
      bool voided = false;
      if (error_message != "")
      {
        voided = VoidDeposit(new_deposit, data, ref sub_error_message, GetStringValue(new_deposit, "LOGIN_ID")).Equals(true);
        if (!voided)
        {
          error_message += String.Format("Could not void new deposit {0}-{1}. ",
            GetStringValue(new_deposit, "DEPOSITID"),
            GetStringValue(new_deposit, "DEPOSITNBR"))
            + sub_error_message;
        }
        return misc.MakeCASLError("Error Consolidating Deposits", "TS-1239", error_message);
      }
      reader = db_reader.Make(data, sql_string, CommandType.Text, 0,
        sql_args, 0, 0, null, ref error_message);
      if (error_message != "")
      {
        voided = VoidDeposit(new_deposit, data, ref sub_error_message, GetStringValue(new_deposit, "LOGIN_ID")).Equals(true);
        if (!voided)
        {
          error_message += String.Format("Could not void new deposit {0}-{1}. ",
            GetStringValue(new_deposit, "DEPOSITID"),
            GetStringValue(new_deposit, "DEPOSITNBR"))
            + sub_error_message;
        }
        return misc.MakeCASLError("Error Consolidating Deposits", "TS-1239", error_message);
      }
      row_count = reader.ExecuteNonQuery(ref error_message);
      if (error_message != "" || row_count != 1)
      {
        voided = VoidDeposit(new_deposit, data, ref sub_error_message, GetStringValue(new_deposit, "LOGIN_ID")).Equals(true);
        if (!voided)
        {
          error_message += String.Format("Could not void new deposit {0}-{1}. ",
            GetStringValue(new_deposit, "DEPOSITID"),
            GetStringValue(new_deposit, "DEPOSITNBR"))
            + sub_error_message;
        }
        return misc.MakeCASLError("Error Consolidating Deposits", "TS-1239", error_message);
      }
      //#endregion
      //#region Post deposit tenders

      reader = db_reader.MakeTransactionReader(data, 0, 0, 0, ref error_message);
      if (error_message != "")
      {
        //Bug 11329 NJ-used this method to return as user friendly message
        GenericObject return_object = CreateErrorObject("TS-1239", "", false, "", true);
        return_object.set("inner_error", misc.MakeCASLError("", "", error_message));
        return return_object;
        //end bug 11329
        //return misc.MakeCASLError("Error Consolidating Deposits", "TS-1239", error_message);
      }
      GenericObject deposit_tender;
      for (int i = 0; i < deposit_tender_objects.getLength(); i++)
      {
        deposit_tender = (GenericObject)deposit_tender_objects.get(i);
        deposit_tender.set("SEQNBR", i + 1);
        sql_args = new Hashtable();
        sql_statement = CreateDepositTenderSQL(deposit_tender, ref sql_args, ref error_message);
        if (error_message != "")
        {
          return misc.MakeCASLError("Error Consolidating Deposits", "TS-1239", error_message);
        }
        reader.AddCommand(sql_statement, CommandType.Text, sql_args);
      }

      success = reader.EstablishTransaction(ref error_message);
      if (!success || error_message != "")
      {
        reader.RollbackTransaction(ref sub_error_message);
        if (sub_error_message != "")
        {
          error_message += sub_error_message;
        }
        //Bug 11329 NJ-used this method to return as user friendly message
        GenericObject return_object = CreateErrorObject("TS-1239", "", false, "", true);
        return_object.set("inner_error", misc.MakeCASLError("", "", error_message));
        return return_object;
        //end bug 11329
        //return misc.MakeCASLError("Error Consolidating Deposits", "TS-1239", error_message);
      }
      success = reader.ExecuteTransactionCommands(ref error_message, ref row_count, true);
      sub_error_message = "";
      if (!success || error_message != "")
      {
        reader.RollbackTransaction(ref sub_error_message);
        if (sub_error_message != "")
        {
          error_message += sub_error_message;
        }
        //return misc.MakeCASLError("Error Consolidating Deposits", "TS-1239", error_message);
        //Bug 11329 NJ-used this method to return as user friendly message
        GenericObject return_object = CreateErrorObject("TS-1239", "", false, "", true);
        return_object.set("inner_error", misc.MakeCASLError("", "", error_message));
        return return_object;
        //end bug 11329
      }
      success = reader.CommitTransaction(ref error_message);
      if (!success || error_message != "")
      {
        reader.RollbackTransaction(ref sub_error_message);
        if (sub_error_message != "")
        {
          error_message += sub_error_message;
        }
        //return misc.MakeCASLError("Error Consolidating Deposits", "TS-1239", error_message);
        //Bug 11329 NJ-used this method to return as user friendly message
        GenericObject return_object = CreateErrorObject("TS-1239", "", false, "", true);
        return_object.set("inner_error", misc.MakeCASLError("", "", error_message));
        return return_object;
        //end bug 11329
      }
      //bug 9092 start SN
      GenericObject database = c_CASL.c_object("TranSuite_DB.Data.db_connection_info") as GenericObject;
      for (int i = 0; i < deposit_tender_objects.getLength(); i++)
      {
        deposit_tender = (GenericObject)deposit_tender_objects.get(i);
        string strDepositId = GetStringValue(deposit_tender, "DEPOSITID");
        string strDepositNbr = GetStringValue(deposit_tender, "DEPOSITNBR");
        UpdateDepositTndrDataChecksum("strDepositId", strDepositId,
                                      "strDepositNbr", strDepositNbr,
                                      "strSeqNbr", i + 1, "data", database);
      }
      //end bug 9092

      // Bug 22343: Write the consolidation to the Activity Log
      details.Append("DepositFilename:").Append(filename_id);
      details.Append(",DEPOSITNBR:").Append(GetStringValue(new_deposit, "DEPOSITNBR"));
      details.Append(",DEPOSITID:").Append(GetStringValue(new_deposit, "DEPOSITID"));
      details.Append(",Amount:").Append(deposit_total);
      details.Append(",DEPSLIPNBR:").Append(slip_nbr);
      details.Append(",BANKID:").Append(new_bankid);
      details.Append(",Comments:").Append(comments);
      LogToActivityLogWithDetails("Cashiering", "Consolidate Deposit", "Consolidate Deposit", details.ToString());
      // End bug 22343

      return new_deposit;

    }

    public static GenericObject CASLConsolidateDeposits(params Object[] arg_pairs)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      StringBuilder details = new StringBuilder();
      
      // Consolidated deposits will be marked as "Pre Deposits" in the DEPOSITTYPE column.
      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject geoReturnDeposit = null;
      ConnectionTypeSynch pDBConn_Data = null;    // Bug 15941. FT.  Moved here
      bool connectionOpened = false;              // Bug 15941
      ThreadLock.GetInstance().CreateLock();// Sync user's access to this resource. Bug# 5813 deadlock fix. DH 07/30/2008
      // Bug 11661: Wrap mutex release in finally block
      try
      {
        GenericObject data = (GenericObject)args.get("data", c_CASL.c_undefined);
        GenericObject config = (GenericObject)args.get("config", c_CASL.c_undefined);
        GenericObject pAllDeposits = (GenericObject)args.get("DEPOSITS_to_consolidate", c_CASL.c_undefined);
        string strFileName = (string)args.get("FILENAME", "");
        string strLOGIN_ID = (string)args.get("LOGIN_ID", ""); //BUG 5395v2 (ConsolidateDeposits)
        string strUpdateUserId = (string)args.get("UPDATE_USERID", ""); //BUG 5395
        pDBConn_Data = new ConnectionTypeSynch(data);
        string strErr = "";
        string strSQL = "";
        string session_id = args.get("session_id") as string;

        if (strFileName.Length == 0)
          HandleErrorsAndLocking("message", "Filename is required.", "code", "TS-1054", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        if (pAllDeposits == null)
          HandleErrorsAndLocking("message", "At least one deposit is required.", "code", "TS-1055", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        // Used for consolidating deposit tenders.
        PreDepositTender pPreDepositTender = new PreDepositTender();

        //-----------------------------------------------------------------
        // Break the file into file number and sequence number
        string strFileNumber = GetFileNumber(strFileName);
        string strFileSequenceNbr = GetFileSequence(strFileName);
        //-----------------------------------------------------------------

        //-----------------------------------------------------------------
        if (args.has("DbConnectionClass"))
          pDBConn_Data = (ConnectionTypeSynch)args.get("DbConnectionClass");
        else
        {
          pDBConn_Data = new ConnectionTypeSynch(data);

          if (!(connectionOpened = pDBConn_Data.EstablishDatabaseConnection(ref strErr)))  // Bug 15941
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1056", strErr);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

          if (!pDBConn_Data.EstablishRollback(ref strErr))
            ThrowCASLErrorButDoNotCloseDBConnection("TS-1057", ref pDBConn_Data, "Error creating database transaction.", strErr);
        }
        //-----------------------------------------------------------------


        //-----------------------------------------------------------------
        // Consolidated data.
        string strConsolidatedDepositID = "";
        string strConsolidatedDepositNbr = "";
        string strDEPOSITTYPE = "";
        string strDEPFILENAME = strFileName;
        string strAMOUNT = "";
        string strGROUPID = "";
        string strGROUPNBR = "";
        string strOWNERUSERID = "";
        string strCREATERUSERID = "";
        string strCOMMENTS = "";
        string strDEPSLIPNBR = "";
        string strBANKID = "";
        string strDep_SOURCE_TYPE = "";
        string strDep_SOURCE_GROUP = "";
        string strDepSOURCE_REFID = "";
        string strDepSOURCE_DATE = "";
        double dAMOUNT = 0.00; // Grand total for consolidated deposit record.
        string str_Consolidated_DEPSLIPNBR = "";   // This will tie back from the consolidated deposit. Comma seperated slips.
        DateTime dtDate = DateTime.Now;
        string strNowDateTime = dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo); // Consolidated deposit date.

        // BUG 5395 Prevent Multiple Session user login BLL 5/2/08 (ConsolidateDeposits)
        //if an application_type comes in, it will be Cashier or Portal
        object objFileSourceType = null;
        if (args.has("FILE_SOURCE_TYPE"))
          objFileSourceType = args.get("FILE_SOURCE_TYPE");
        string strFileSourceType = objFileSourceType.ToString();

        //file source came in as either Cashier or Portal which is needed for checking the session
        //however, the rest of the code always expects it to be Cashier, so will change it here
        if (strFileSourceType == "Portal")
          strFileSourceType = "Cashier";
        //end BUG 5395

        // Go through all deposits passed in and combine the values.
        int iCountOfDepositsToConsolidate = pAllDeposits.getIntKeyLength();
        for (int i = 0; i < iCountOfDepositsToConsolidate; i++)
        {
          //------------------------------
          GenericObject a_deposit = (GenericObject)pAllDeposits.get(i);
          string strDEPOSITID = (string)a_deposit.get("DEPOSITID");
          string strDEPOSITNBR = (string)a_deposit.get("DEPOSITNBR");
          //------------------------------

          //------------------------------
          // Get PRE_DEPOSIT deposit data
          if (pDBConn_Data.GetDBConnectionType() == ConnectionTypeSynch.m_iCurrentDatabase_SqlServer)
            strSQL = string.Format("SELECT * FROM TG_DEPOSIT_DATA (NOLOCK) WHERE DEPOSITTYPE IS NOT NULL AND DEPOSITID='{0}' AND DEPOSITNBR={1}", strDEPOSITID, strDEPOSITNBR); //BUG4458 add NOLOCK
          else
            strSQL = string.Format("SELECT * FROM TG_DEPOSIT_DATA WHERE DEPOSITTYPE IS NOT NULL AND DEPOSITID='{0}' AND DEPOSITNBR={1}", strDEPOSITID, strDEPOSITNBR);
          GenericObject geoTmp1 = (GenericObject)GetRecords(data, 0, strSQL, pDBConn_Data.GetDBConnectionType());
          if (geoTmp1 == null)
            ThrowCASLErrorButDoNotCloseDBConnection("TS-1058", ref pDBConn_Data, "Unable to retrieve deposit data for DEPOSITID: "
                + strDEPOSITID + " and DEPOSITNBR: " + strDEPOSITNBR, "");
          else
          {
            // Save this data from the first deposit for consolidated deposit record.
            if (i == 0)
            {
              //--------------------------------------------------------------------
              // Also create a new deposit number at this point to avoid concurrency issues.
              strConsolidatedDepositID = "TSFD"; // FILE
              strConsolidatedDepositID += string.Format("{0:yyyyMMdd}", DateTime.Now);
              strConsolidatedDepositNbr = CreateDepositNbr(ref pDBConn_Data, data, strConsolidatedDepositID);

              strDEPOSITTYPE = GetKeyData(geoTmp1, "DEPOSITTYPE", true);
              strGROUPID = GetKeyData(geoTmp1, "GROUPID", true);
              strGROUPNBR = GetKeyData(geoTmp1, "GROUPNBR", true);

              if (args.has("OWNERUSERID"))
                strOWNERUSERID = (string)args.get("OWNERUSERID", "");
              else
                strOWNERUSERID = GetKeyData(geoTmp1, "OWNERUSERID", true);

              if (args.has("CREATERUSERID"))
                strCREATERUSERID = (string)args.get("CREATERUSERID", "");
              else
                strCREATERUSERID = GetKeyData(geoTmp1, "CREATERUSERID", true);

              if (args.has("COMMENTS"))
                strCOMMENTS = (string)args.get("COMMENTS", "");
              else
                strCOMMENTS = GetKeyData(geoTmp1, "COMMENTS", true);

              strDEPSLIPNBR = GetKeyData(geoTmp1, "DEPSLIPNBR", true);
              strBANKID = GetKeyData(geoTmp1, "BANKID", true);
              strDep_SOURCE_TYPE = GetKeyData(geoTmp1, "file_SOURCE_TYPE", true);
              strDep_SOURCE_GROUP = GetKeyData(geoTmp1, "file_SOURCE_GROUP", true);
              strDepSOURCE_REFID = GetKeyData(geoTmp1, "SOURCE_REFID", true);
              strDepSOURCE_DATE = GetKeyData(geoTmp1, "SOURCE_DATE", true);
            }

            // Grand total for all parts.
            strAMOUNT = GetKeyData(geoTmp1, "AMOUNT", true);
          // Bug #12055 Mike O - Use misc.Parse to log errors
          dAMOUNT += misc.Parse<Double>(strAMOUNT);
            strAMOUNT = dAMOUNT.ToString();// Update the grand total.

            // BUG4808 USE DEPOSIT NBR PASSED IN SX012608
            // Tie back from consolidated deposit to its parts.
            //if(i==0)
            //  str_Consolidated_DEPSLIPNBR += strDEPSLIPNBR;
            //else
            //  str_Consolidated_DEPSLIPNBR += "," + strDEPSLIPNBR;

            //***************************************
            // Collect all tenders for this deposit.
            strSQL = string.Format("SELECT * FROM TG_DEPOSITTNDR_DATA WHERE DEPOSITID='{0}' AND DEPOSITNBR={1}", strDEPOSITID, strDEPOSITNBR);
            GenericObject geoTemp2 = (GenericObject)GetRecords(data, 1, strSQL, pDBConn_Data.GetDBConnectionType());
            if (geoTemp2 != null)
            {
              int iTenderCount = geoTemp2.getIntKeyLength();
            // Bug #12055 Mike O - Use misc.Parse to log errors
              for (int j = 0; j < iTenderCount; j++)
              {
                GenericObject pTenderElement = (GenericObject)geoTemp2.get(j);

                string strDEPID = GetKeyData(pTenderElement, "DEPOSITID", false);
              long lDEPOSITNBR        = misc.Parse<Int64>(GetKeyData(pTenderElement, "DEPOSITNBR", false));
              long lSEQNBR            = misc.Parse<Int64>(GetKeyData(pTenderElement, "SEQNBR", false));
                string strTNDRPROP = GetKeyData(pTenderElement, "TNDRPROP", false);
                string strTNDR = GetKeyData(pTenderElement, "TNDR", false);
                string strTNDRDESC = GetKeyData(pTenderElement, "TNDRDESC", false);
              double dTndrAmt         = misc.Parse<Double>(GetKeyData(pTenderElement, "AMOUNT", false));
                string strSOURCE_REFID = GetKeyData(pTenderElement, "SOURCE_REFID", false);
                string strSOURCE_DATE = GetKeyData(pTenderElement, "SOURCE_DATE", false);

                pPreDepositTender.Add(strDEPID,
                  lDEPOSITNBR,
                  lSEQNBR,
                  strTNDRPROP,
                  strTNDR,
                  strTNDRDESC,
                  dTndrAmt,
                  strSOURCE_REFID,
                  strSOURCE_DATE);

                //----------------------------------------
                // Marked processed deposit as PRE_DEPOSIT
                strSQL = string.Format("UPDATE TG_DEPOSIT_DATA SET DEPOSITTYPE='PRE_DEPOSIT' WHERE DEPOSITID='{0}' AND DEPOSITNBR={1}", strDEPID, lDEPOSITNBR);
                if (!pDBConn_Data.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strErr))
                  ThrowCASLErrorButDoNotCloseDBConnection("TS-1059", ref pDBConn_Data, "Unable to post consolidated deposit.", " Error: " + strSQL);
                //----------------------------------------
              }
            }
            //***************************************
          }
          //------------------------------
        }
        // BUG4808 USE DEPOSIT NBR PASSED IN SX012608
        str_Consolidated_DEPSLIPNBR = (string)args.get("SLIPNBR", "");

        //--------------------------------
        // Create consolidated deposit record with consolidated tenders.   
        GenericObject ConsolidatedDeposit = new GenericObject();
        ConsolidatedDeposit.set("DEPOSITTYPE", "FILE");
        ConsolidatedDeposit.set("file_DEPFILENAME", strDEPFILENAME);
        ConsolidatedDeposit.set("DEPOSITNBR", strConsolidatedDepositNbr);
        ConsolidatedDeposit.set("AMOUNT", strAMOUNT);// includes all parts.             
        ConsolidatedDeposit.set("GROUPID", strGROUPID);
        ConsolidatedDeposit.set("GROUPNBR", strGROUPNBR);
        ConsolidatedDeposit.set("OWNERUSERID", strOWNERUSERID);
        ConsolidatedDeposit.set("CREATERUSERID", strCREATERUSERID);
        ConsolidatedDeposit.set("COMMENTS", strCOMMENTS);
        ConsolidatedDeposit.set("DEPSLIPNBR", str_Consolidated_DEPSLIPNBR); // all parts        
        ConsolidatedDeposit.set("BANKID", strBANKID);
        ConsolidatedDeposit.set("Dep_SOURCE_TYPE", strDep_SOURCE_TYPE);
        ConsolidatedDeposit.set("Dep_SOURCE_GROUP", strDep_SOURCE_GROUP);
        ConsolidatedDeposit.set("DepSOURCE_REFID", strDepSOURCE_REFID);
        ConsolidatedDeposit.set("DepSOURCE_DATE", strDepSOURCE_DATE);

        // Bug 22343: Collect data for the activity log
        details.Append("DepositFilename:").Append(strDEPFILENAME);
        details.Append(",DEPOSITNBR:").Append(strConsolidatedDepositNbr);
        details.Append(",DEPOSITID:").Append(GetKeyData(ConsolidatedDeposit, "DEPOSITID", true));
        details.Append(",Amount:").Append(strAMOUNT);
        details.Append(",DEPSLIPNBR:").Append(str_Consolidated_DEPSLIPNBR);
        details.Append(",BANKID:").Append(strBANKID);
        details.Append(",Comments:").Append(strCOMMENTS);
        // End Bug 22343

        // Now add the consolidated tenders.
        ConsolidatedDeposit.set("table_TG_DEPOSITTNDR_DATA", pPreDepositTender.ReturnInGEOFormat());

        // Post the new consolidated records.
        try
        {
          geoReturnDeposit = (GenericObject)PostDepositToT3("_subject", ConsolidatedDeposit, "DbConnectionClass", pDBConn_Data, "config", config, "data", data, "session_id", session_id);
        }
        catch (Exception e)
        {
          ThrowCASLErrorButDoNotCloseDBConnection("TS-1060", ref pDBConn_Data, "Unable to post consolidated deposit.", " Error: " + e.Message);
        }
        if (geoReturnDeposit == null)
        {
          ThrowCASLErrorButDoNotCloseDBConnection("TS-1061", ref pDBConn_Data, "Unable to post consolidated deposit.", "");
        }
        //--------------------------------

        // Bug 22343
        LogToActivityLogWithDetails("Cashiering", "Consolidate Deposit", "Consolidate Deposit", details.ToString());


        //-----------------------------------------------------------------
        // ALL GOOD - ACCEPT THE UPDATES.
        if (!args.has("DbConnectionClass"))
        {
          strErr = "";
          if (!pDBConn_Data.CommitToDatabase(ref strErr))
            ThrowCASLErrorButDoNotCloseDBConnection("TS-1062", ref pDBConn_Data, "Unable to commit changes to database.", strErr);
        }
        //-----------------------------------------------------------------
      }
      finally
      {
        CloseDBConnectionIfOpenedLocally(pDBConn_Data, connectionOpened);

        ThreadLock.GetInstance().ReleaseLock(); //Bug# 5813 deadlock fix. DH 07/30/2008
      }

      return geoReturnDeposit;
    }


    // Axels function that posts a list of Deposits by calling Daniels PostDepositToT3- used by auto balanced
    // Called by 'core_post_deposit_list_to_T3'
    public static Object PostDepositListToT3(params Object[] arg_pairs)
    {
      ConnectionTypeSynch pDBConn = null;   // Bug 15941. FT. Moved here. If part of posting, this will be already valid.
      bool connectionOpened = false;        // Bug 15941
      GenericObject args = misc.convert_args(arg_pairs);

      ThreadLock.GetInstance().CreateLock();// Sync user's access to this resource. Bug# 5813 deadlock fix. DH 07/30/2008
      // Bug 11661: Wrap mutex release in finally block
      try
      {
        GenericObject depositList = args.get("a_deposit_list", c_CASL.c_undefined) as GenericObject;
        GenericObject config = args.get("config", c_CASL.c_undefined) as GenericObject;
        GenericObject data = args.get("data", c_CASL.c_undefined) as GenericObject;
        string strErr = "";
        string session_id = args.get("session_id") as string;

        //**********************************************************************************
        // It is possible topass in a database connection using the "DbConnectionClass" key.
        // Connection must be of type ConnectionTypeSynch class.
        if (args.has("DbConnectionClass"))
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
        else
        {
          pDBConn = new ConnectionTypeSynch(data);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strErr)))  // Bug 15941
          {
            strErr = "Error opening database connection.";
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-844", strErr);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
          }
          if (!pDBConn.EstablishRollback(ref strErr))
          {
            strErr = "Error creating database transaction." + strErr;
            ThrowCASLErrorButDoNotCloseDBConnection("TS-886", ref pDBConn, "Error creating database transaction.", strErr);
          }
        }
        //**********************************************************************************

        // Call Daniels PostDepositToT3 passing connection and a Deposit as '_subject'
        int iCnt = depositList.getIntKeyLength();
        long iDepositNumber = 0;// Added by Daniel H. to fix a bug with locking and querying same table on the same connection issues.

        // BUG 5395 Prevent multiple session user login BLL 5/2/08 (PostDepositListToT3)
        GenericObject a_Deposit_First = (GenericObject)depositList.get(0);
        string strDep_SOURCE_TYPE = GetKeyData(a_Deposit_First, "file_SOURCE_TYPE", true);
        string strLOGIN_ID = GetKeyData(a_Deposit_First, "LOGIN_ID", true); //BUG 5395v2 (PostDepositListToT3)

        //file source could come in as Cashier, OC or Portal which is needed for checking the session
        //however, the rest of the code would expect it to be Cashier or OC, so will change Portal
        //back to Cashier here
        if (strDep_SOURCE_TYPE == "Portal")
          strDep_SOURCE_TYPE = "Cashier";
        // end BUG 5395
        for (int i = 0; i < iCnt; i++)
        {
          GenericObject a_Deposit = (GenericObject)depositList.get(i);

          if (iDepositNumber > 0)// Added by Daniel H. to fix a bug with locking and querying same table on the same connection issues.
            a_Deposit.set("DEPOSITNBR", iDepositNumber.ToString());
          //BUG 5395 Prevent multiple user session login BLL 5/2/08 pass in bSessionCheckDone (PostDepositListToT3)
          PostDepositToT3("_subject", a_Deposit, "config", config, "data", data, "DbConnectionClass", pDBConn, "bSessionCheckDone", true, "session_id", session_id);
          //end BUG 5395
        // Bug #12055 Mike O - Use misc.Parse to log errors
        iDepositNumber = misc.Parse<Int32>((string)a_Deposit.get("DEPOSITNBR"))+1;// Added by Daniel H. to fix a bug with locking and querying same table on the same connection issues.
        }

        // ALL GOOD - ACCEPT THE UPDATES.
        if (!args.has("DbConnectionClass"))// Caller will commit.
        {
          strErr = "";
          if (!pDBConn.CommitToDatabase(ref strErr))
            HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-849", strErr);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

      }
      finally
      {
        // Bug 15941
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
        // Bug 11661
        ThreadLock.GetInstance().ReleaseLock(); //Bug# 5813 deadlock fix. DH 07/30/2008
      }

      return true;
    }

    public static GenericObject PostDepositToT3(params Object[] arg_pairs)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!
      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject a_deposit = args.get("_subject", c_CASL.c_undefined) as GenericObject;
      string session_id = args.get("session_id") as string;
      // Bug 15941
      ConnectionTypeSynch pDBConn = null;// If part of posting, this will be already valid.
      bool connectionOpened = false;      // Bug 15941
      ThreadLock.GetInstance().CreateLock();// Sync user's access to this resource. Bug# 5813 deadlock fix. DH 07/30/2008
      // Bug 11661: Wrap mutex release in finally block
      try
      {
        GenericObject config = args.get("config", c_CASL.c_undefined) as GenericObject;
        GenericObject data = args.get("data", c_CASL.c_undefined) as GenericObject;
        bool bSessionCheckDone = (bool)args.get("bSessionCheckDone", false); //BUG 5395
        string strErr = "";
        string strSQL = "";
        DateTime dtDate = DateTime.Now;
        string strNowDateTime = dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo);

        //**********************************************************************************
        // It is possible topass in a database connection using the "DbConnectionClass" key.
        // Connection must be of type ConnectionTypeSynch class.
        if (args.has("DbConnectionClass"))
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
        else
        {
          pDBConn = new ConnectionTypeSynch(data);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strErr)))  // Bug 15941
          {
            strErr = "Error opening database connection.";
            return MakeCASLErrorButDoNOTCloseDB("Post Deposit Error", "TS-844", strErr, ref pDBConn);
          }
          if (!pDBConn.EstablishRollback(ref strErr))
          {
            strErr = "Error creating database transaction." + strErr;
            return MakeCASLErrorButDoNOTCloseDB("Post Deposit Error", "TS-886", "Error creating database transaction." + strErr, ref pDBConn);
          }
        }
        //**********************************************************************************

        //**********************************************************************************
        // Deposit Global Data
        string strDEPOSITID = GetKeyData(a_deposit, "DEPOSITID", true);
        string strDEPOSITNBR = GetKeyData(a_deposit, "DEPOSITNBR", true);
        string strDEPOSITTYPE = GetKeyData(a_deposit, "DEPOSITTYPE", true);
        string strfile_DEPFILENAME = GetKeyData(a_deposit, "file_DEPFILENAME", true);
        string strAMOUNT = GetKeyData(a_deposit, "AMOUNT", true);
        string strGROUPID = GetKeyData(a_deposit, "GROUPID", true);
        string strGROUPNBR = GetKeyData(a_deposit, "GROUPNBR", true);
        string strOWNERUSERID = GetKeyData(a_deposit, "OWNERUSERID", true);
        string strCREATERUSERID = GetKeyData(a_deposit, "CREATERUSERID", true);
        string strLOGIN_ID = GetKeyData(a_deposit, "LOGIN_ID", true); //BUG 5395v2 (PostDepositInT3)
        string strCOMMENTS = GetKeyData(a_deposit, "COMMENTS", true);
        string strDEPSLIPNBR = GetKeyData(a_deposit, "DEPSLIPNBR", true);
        string strBANKID = GetKeyData(a_deposit, "BANKID", true);
        string strDep_SOURCE_TYPE = GetKeyData(a_deposit, "file_SOURCE_TYPE", true);
        string strDep_SOURCE_GROUP = GetKeyData(a_deposit, "file_SOURCE_GROUP", true);
        string strDepSOURCE_REFID = GetKeyData(a_deposit, "SOURCE_REFID", true);
        string strDepSOURCE_DATE = GetKeyData(a_deposit, "SOURCE_DATE", true);
        //**********************************************************************************

        //file source could come in as Cashier, OC or Portal which is needed for checking the session
        //however, the rest of the code always expects it to be Cashier or OC, so will change Portal
        //back to Cashier it here
        if (strDep_SOURCE_TYPE == "Portal")
          strDep_SOURCE_TYPE = "Cashier";
        //end BUG 5395

        //-------------------------------------------------------------
        // Validate Deposit type
        if (strDEPOSITTYPE.Length == 0)
          return MakeCASLErrorButDoNOTCloseDB("Post Deposit Error", "TS-887", "Deposit Type is required!" + strErr, ref pDBConn);
        //-------------------------------------------------------------

        //-------------------------------------------------------------
        // Break the file into file number and sequence number
        string strFileNumber = GetFileNumber(strfile_DEPFILENAME);
        string strFileSequenceNbr = GetFileSequence(strfile_DEPFILENAME);
        //-------------------------------------------------------------

        // Get the appropriate Deposit ID
        if (strDEPOSITTYPE == "USER")
          strDEPOSITID = "TSUD";
        else if (strDEPOSITTYPE == "FILE")
          strDEPOSITID = "TSFD";
        else if (strDEPOSITTYPE == "GROUP")
          strDEPOSITID = "TSGD";
        strDEPOSITID += string.Format("{0:yyyyMMdd}", DateTime.Now);

        // Update
        a_deposit.set("DEPOSITID", strDEPOSITID);

        //--------------------------------------------------------------------
        // Create new deposit number
        if (strDEPOSITNBR.Length == 0)
        {
          strDEPOSITNBR = CreateDepositNbr(ref pDBConn, data, strDEPOSITID);
          a_deposit.set("DEPOSITNBR", strDEPOSITNBR);
        }
        //--------------------------------------------------------------------

        //--------------------------------------------------------------------
        // Make sure the File is Active IF this is a USER or FILE deposit
        if (strDEPOSITTYPE == "USER" || strDEPOSITTYPE == "FILE")
        {
          if (!IsFileActive(strFileNumber, strFileSequenceNbr, data, ref pDBConn))
            return MakeCASLErrorButDoNOTCloseDB("Post Deposit Error", "TS-888", "Transaction File " + strfile_DEPFILENAME + " is not Active!  No changes are permitted.", ref pDBConn);
        }
        //--------------------------------------------------------------------

        // Bug 22460: Find out if this is a credit card deposit
        //bool isCreditCard = isCreditCardDeposit(a_deposit);

        // Bug 20731 / 22460: Generate a deposit slip number if there is none
        strDEPSLIPNBR = generateSlipNumberAndStripLeadingZeros(strFileNumber, strFileSequenceNbr, strDEPOSITID, strDEPOSITNBR, strfile_DEPFILENAME, strDEPSLIPNBR, strBANKID);
                // End Bug 20731 

        //**********************************************************************************
        // Create depodit record.
        strSQL = CreateSQLFor_TG_DEPOSIT(strDEPOSITID,
          strDEPOSITNBR,
          strDEPOSITTYPE,
          strFileNumber,
          strFileSequenceNbr,
          strOWNERUSERID,
          strCREATERUSERID,
          strDEPSLIPNBR,
          strAMOUNT,
          strGROUPNBR,
          strGROUPID,
          strCOMMENTS,
          strBANKID,
          strDep_SOURCE_TYPE,
          strDep_SOURCE_GROUP,
          strDepSOURCE_DATE,
          pDBConn.GetDBConnectionType(),
          strNowDateTime,
          strDepSOURCE_REFID);
        strErr = "";
        if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strErr))
          return MakeCASLErrorButDoNOTCloseDB("Post Deposit Error", "TS-890", "Unable to post deposit record in file: " + strfile_DEPFILENAME + " Error" + strErr, ref pDBConn);

        // Bug 22460: If the DEPSLIPNBR has not been set, set it to the filename plus the UNIQUEID.
        // This has to be done after the fact because we do not know a-priori what the UNIQUEID is going to be.
        // The trick is to use the SCOPE_IDENTITY() function to get the UNIQUEID.
        // Note: We should probably change this dynamic SQL to use the CONCAT() function
        // instead of the plus operator so I don't have to do the conversion.
        if (string.IsNullOrWhiteSpace(strDEPSLIPNBR))
        {
            string rawSQL = @"UPDATE TG_DEPOSIT_DATA set DEPSLIPNBR = '{0}' + LTRIM(STR(SCOPE_IDENTITY()))
                        WHERE DEPOSITID = '{1}' AND DEPOSITNBR = {2} AND UNIQUEID = SCOPE_IDENTITY()";
            string depositSlipNumberPrefix = strfile_DEPFILENAME + '-';
            strSQL = string.Format(rawSQL, depositSlipNumberPrefix, strDEPOSITID, strDEPOSITNBR);
            if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strErr))
                return MakeCASLErrorButDoNOTCloseDB("Post Deposit Error", "TS-1067", "Unable to update deposit record in file with generated slip number: " + strfile_DEPFILENAME + " Error" + strErr, ref pDBConn);
        }
        // End Bug 22460
  
          
        //**********************************************************************************

        //**********************************************************************************
        // Post Deposit Tender Data
        int iSeqNbr = 1;// New tender sequence number.
        GenericObject geoTmp = (GenericObject)a_deposit.get("table_TG_DEPOSITTNDR_DATA");
        int iCnt = geoTmp.getIntKeyLength();
        GenericObject database = c_CASL.c_object("TranSuite_DB.Data.db_connection_info") as GenericObject;
        if (iCnt >= 1)
        {
          for (int i = 0; i < iCnt; i++)
          {
            GenericObject a_DepositTender = (GenericObject)geoTmp.get(i);
            string strTNDRPROP = GetKeyData(a_DepositTender, "TNDRPROP", true);
            string strTNDR = GetKeyData(a_DepositTender, "TNDR", true);
            string strTNDRDESC = GetKeyData(a_DepositTender, "TNDRDESC", true);
            string strTNDR_AMOUNT = GetKeyData(a_DepositTender, "AMOUNT", true);
            string strTNDR_SOURCE_REFID = GetKeyData(a_DepositTender, "SOURCE_REFID", true);
            string strTNDR_SOURCE_DATE = GetKeyData(a_DepositTender, "SOURCE_DATE", true);
            //TTS20815 SX foreign currency balancing-
            string strCURRENCYTYPE = GetKeyData(a_DepositTender, "CURRENCYTYPE", true);
            string strCURRENCYAMOUNT = GetKeyData(a_DepositTender, "CURRENCYAMOUNT", true);
            string strCURRENCYEXCHANGERATE = GetKeyData(a_DepositTender, "CURRENCYEXCHANGERATE", true);

            strSQL = CreateSQLFor_TG_DEPOSITTNDR_DATA(strDEPOSITNBR,
              strDEPOSITID,
              strTNDRPROP,
              strTNDR,
              strTNDRDESC,
              strTNDR_AMOUNT,
              strCURRENCYTYPE,
              strCURRENCYAMOUNT,
              strCURRENCYEXCHANGERATE,
              pDBConn.GetDBConnectionType(),
              strTNDR_SOURCE_DATE,
              strTNDR_SOURCE_REFID,
              iSeqNbr.ToString());
            strErr = "";
            if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strErr))
              return MakeCASLErrorButDoNOTCloseDB("Post Deposit Error", "TS-891", "Unable to post tender record in file: " + strfile_DEPFILENAME + " Error" + strErr, ref pDBConn);
            iSeqNbr++;
          }
        }
        //**********************************************************************************
        // Bug #13092 Mike O - Move commit to after checksum update, and pass needed arguments into checksum function
        //Bug 9092 start SN
        UpdateDepositChecksum("strDepositID", strDEPOSITID, "strDepositNbr", strDEPOSITNBR, "DbConnectionClass", pDBConn, "geoChecksumData", a_deposit);
        iSeqNbr = 1;// New tender sequence number.
        geoTmp = (GenericObject)a_deposit.get("table_TG_DEPOSITTNDR_DATA");
        iCnt = geoTmp.getIntKeyLength();
        if (iCnt >= 1)
        {
          for (int i = 0; i < iCnt; i++)
          {
            GenericObject a_DepositTender = (GenericObject)geoTmp.get(i);
            UpdateDepositTndrDataChecksum("strDepositId", strDEPOSITID,
 "strDepositNbr", strDEPOSITNBR,
 "strSeqNbr", iSeqNbr.ToString(), "DbConnectionClass", pDBConn, "geoChecksumData", a_DepositTender);

            iSeqNbr++;
          }
        }
        //end bug 9092

        // ALL GOOD - ACCEPT THE UPDATES.
        if(!args.has("DbConnectionClass"))// Caller will commit. Bug 10182 NJ
        {
          strErr = "";
          if (!pDBConn.CommitToDatabase(ref strErr))
            return MakeCASLErrorButDoNOTCloseDB("Post Deposit Error", "TS-849", "Unable to commit changes to database." + strErr, ref pDBConn);
      }
        // End Bug #13092 Mike O
      }
      finally
      {
        // Bug 15941
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);

        // Bug 11661
        ThreadLock.GetInstance().ReleaseLock(); //Bug# 5813 deadlock fix. DH 07/30/2008
      }

      return a_deposit;
    }

        /// <summary>
    /// Loop through the deposits in this deposit set
    /// and determine if this is a credit card deposit.
    /// Unfortunately I had to search on the hardwired 
    /// the deposit number of 3.
    /// 
    /// The assumption is that a credit card deposit will 
    /// only have credit card TNDR's in it.
    /// 
    /// Bug 22460
    /// 
    /// Here are the rules for determining if this is a credit card deposit:
    /// 
    /// Case 1: If TNDRPROP is 'TypeInd', then 3 is credit card. 
    /// Case 2: if TNDRPROP is 'TndrID',  then TNDR is a custom tender container (the TNDRID) in config
    ///
    /// TNDRPROP 	VARCHAR(20) 	This field contains one of the following values: 
    /// ?TndrID? ? This is a Tender Deposit.
    /// ?TypeInd? ? This is a Tender Type Deposit.
    /// No other values are allowed. 
    /// 
    /// TNDR 	VARCHAR(40) 	For Tender Deposits, the Tender ID. For Tender Type Deposits, the Tender Type. 
    /// 
    /// TNDRDESC 	VARCHAR(40) 	This field contains a description of the tender type or tender group deposited. 
    /// 
    /// </summary>
    /// <param name="a_deposit"></param>
    /// <returns></returns>
    [Obsolete("No need to check this anymore.  See Bug 22860")]
    private static bool isCreditCardDeposit(GenericObject a_deposit)
    {
      // Bug: 22928: Cash deposit will not have the table_TG_DEPOSITTNDR_DATA key
      if (!a_deposit.has("table_TG_DEPOSITTNDR_DATA"))
        return false;

      GenericObject geoTmp = (GenericObject)a_deposit.get("table_TG_DEPOSITTNDR_DATA");
        int iCnt = geoTmp.getIntKeyLength();
        if (iCnt == 0)
        {
            // This seems like an error condition.  Should I generate an error?
            return false;
        }

        // This look looks for any 
        // credit card tenders and will return true if found.
        // We might want to reverse this and look for any non-credit card
        // tender and return false is any are found.
        for (int i = 0; i < iCnt; i++)
        {
            GenericObject a_DepositTender = (GenericObject)geoTmp.get(i);
            string strTNDR = GetKeyData(a_DepositTender, "TNDR", true);
            string strTNDRPROP = GetKeyData(a_DepositTender, "TNDRPROP", true);

            // Case 1
            if (strTNDRPROP == "TypeInd" && strTNDR == "3")
                return true;

            // Case 2
            if (strTNDRPROP == "TndrID" && isConfiguredCreditCard(strTNDR))
                return true;

        }
        return false;
    }

    /// <summary>
    /// Read the configuration data and get the tender indicated by the TNDRID.
    /// Then read the _parent and decide if this is a credit card.
    /// 
    ///  Bug 22460
    ///  
    /// </summary>
    /// <param name="TNDRID"></param>
    /// <returns></returns>
    private static bool isConfiguredCreditCard(string TNDRID)
    {
        String tenderString = String.Format("Business.Tender.of.\"{0}\"", TNDRID);
        GenericObject tender = (GenericObject)c_CASL.c_object(tenderString);

        string parent = tender.get("\"_parent\"", "") as string;
        if (parent.Contains("Credit"))
            return true;
        else
            return false;

    }
    /// <summary>
        /// Generate a slip number if it is missing and the generate config option is set.
        /// Bug 20731
    /// Modified for Bug 22460 so that if it is a credit card, use the match key
        /// </summary>
        /// <param name="strDEPOSITID"></param>
        /// <param name="strDEPOSITNBR"></param>
        /// <param name="strfile_DEPFILENAME"></param>
        /// <param name="strDEPSLIPNBR"></param>
        /// <param name="strBANKID"></param>
        /// <returns></returns>
        private static string generateSlipNumberAndStripLeadingZeros(string strFileNumber, 
        string strFileSequenceNbr, 
        string strDEPOSITID, 
        string strDEPOSITNBR, 
        string strfile_DEPFILENAME, 
        string strDEPSLIPNBR, 
        string strBANKID)
        {
        // Case 1:  Easy: the user entered a Deposit Slip Number
        if (!string.IsNullOrWhiteSpace(strDEPSLIPNBR))
        { 
			// Bug 23058: Protect against null strings before the trim below
            if (string.IsNullOrWhiteSpace(strDEPSLIPNBR))
            return strDEPSLIPNBR;

            string noLeadingZeros = strDEPSLIPNBR.TrimStart('0');           // Bug 23058: Trim leading zeros before they are written to the database
            return noLeadingZeros.Length > 0 ? noLeadingZeros : "0";        // Bug 23058: Make sure that all zeros returns a single 0     
        }

            // First let's see if auto-generate is set
            bool auto_generate = isAutoGenerateSet(strBANKID, strDEPOSITID, strDEPOSITNBR);
        if (!auto_generate)
            {
            /*
            if (isCreditCard)
            {
                Logger.cs_log(
                    string.Format ("Warning: generateSlipNumber(): Attempting to generate a deposit slip number for a credit card deposit, but the Auto Generate Slip Number is not enabled on for the bank ({0}); this deposit will not have a deposit slip number",
                    strBANKID)
                    );
            }*/
            return strDEPSLIPNBR;
          }

        // Step two: The deposit slip number is blank; check for a match key.
        // If there is a match key and the bank ID matches, use that
            string matchKey = checkForMatchKey(strFileNumber, strFileSequenceNbr, strBANKID);
            if (!string.IsNullOrWhiteSpace(matchKey))
                return matchKey;

        // Step 3: Probably an ACH Deposit.
        // Do as is done in Cone: see Bug 20731
        // Change as of 2/7/2018: use the CORE filename, a dash, and the UNIQUEID for the slip number.
        // But this cannot be done until after the deposit record is written because we will not know
        // what the match key will be until then.
        // 
        //strDEPSLIPNBR = strDEPOSITNBR + '-' + strfile_DEPFILENAME;
        //return strDEPSLIPNBR;
        return "";
        // End change
        }
        
        /// <summary>
      /// Get the match key config from the workgroup config
      /// and see if the bankid matches.  If so, use the associated 
      /// match key for the deposit slip.
      /// Bug 22460
      /// </summary>
      /// <param name="strFileNumber"></param>
      /// <param name="strFileSequenceNbr"></param>
      /// <param name="targetBANKID"></param>
      /// <returns></returns>
    private static string checkForMatchKey(string strFileNumber, string strFileSequenceNbr, string targetBANKID)
    {
        try
        {
            string workgroup = readWorkgroupID(strFileNumber, strFileSequenceNbr);
            if (string.IsNullOrWhiteSpace(workgroup))
            {
                Logger.cs_log(string.Format("Warning: Trying to generate a deposit slip number for {0} {1}, but cannot read the workgroup",
                    strFileNumber, strFileSequenceNbr));
                return "";
            }

            String dept = String.Format("Business.Department.of.\"{0}\"", workgroup);
            GenericObject workgroupGEO = (GenericObject)c_CASL.c_object(dept);

            if (!workgroupGEO.has("bank_reconcilation_key"))
            {
                Logger.cs_log(string.Format("Warning: Trying to generate a deposit slip number for {0} {1}, but cannot read the bank_reconcilation_key",
                        strFileNumber, strFileSequenceNbr));
                return "";
            }

            string matchKey = workgroupGEO.get("bank_reconcilation_key") as string;
            if (string.IsNullOrWhiteSpace(matchKey))
            {
                Logger.cs_log(string.Format("Warning: Trying to generate a deposit slip number for {0} {1}, but the bank_reconcilation_key is empty.",
                        strFileNumber, strFileSequenceNbr));
                return "";
            }

            // The match key should be of this format 001:1234,002:4567
            string[] matchKeyTuples = matchKey.Split(',');
            foreach (string tuple in matchKeyTuples)
            {
                // Split the bank id from the merchant key
                string[] bankID_merchantKey = tuple.Split(':');
                if (bankID_merchantKey.Length != 2)
                {
                    Logger.cs_log(string.Format("Warning: Trying to generate a deposit slip number for a credit card deposit for {0} {1}, but the bank_reconcilation_key:{2} is not properly formed. This component does not have tow parts: {3}",
                            strFileNumber, strFileSequenceNbr, matchKey, tuple));
                    continue;
                }
                string bankId = bankID_merchantKey[0].Trim();
                string merchantKey = bankID_merchantKey[1].Trim();
                // Found a match
                if (bankId == targetBANKID)
                    return merchantKey;
            }

            // Looped through all the bank_reconcilation_key tuples without a match on the Bank ID
            return "";
        }
        catch (Exception e)
        {
            string msg = string.Format("checkForMatchKey: Cannot read the bank_reconcilation_key from configuration when tryipng to generated a deposit slip number for a credit card deposit.  Error: {0}; for {0} {1}",
                e.ToString(), strFileNumber, strFileSequenceNbr);
            Logger.cs_log(msg);

        // Bug 17849
        Logger.cs_log_trace("Error in checkForMatchKey", e);
            return "";
        }
    }

      /// <summary>
      /// Get the workgroup (DEPTID) from the DEPFILE table for 
      /// this deposit.
      /// Bug 22460
      /// This seems insanely cumbersome; can't I just read this from the in-memory casl configuration?
      /// </summary>
      /// <param name="strFileNumber"></param>
      /// <param name="strFileSequenceNbr"></param>
      /// <returns></returns>
    private static string readWorkgroupID(string strFileNumber, string strFileSequenceNbr)
    {
        string parameterizedSql =
          @"select DEPTID from TG_DEPFILE_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq";

        try
        {

        int depfilenbr = Convert.ToInt32(strFileNumber);
        int depfileseq = Convert.ToInt32(strFileSequenceNbr);


        // I could cache this if I was worried about performance
        GenericObject dbConnection = c_CASL.c_object("TranSuite_DB.Data.db_connection_info") as GenericObject;
        String connectionString = (String)dbConnection.get("db_connection_string");

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(parameterizedSql))
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;

                    connection.Open();

                   
                    // Bug 23390 
                    command.Parameters.Add("@depfilenbr", SqlDbType.Int).Value = Convert.ToInt32(depfilenbr);
                    command.Parameters.Add("@depfileseq", SqlDbType.Int).Value = Convert.ToInt32(depfileseq);
            
                    var workgroup = command.ExecuteScalar();
                    if (workgroup == DBNull.Value || workgroup == null)
                    {
                        return "";
                    }
                    {
                        return workgroup.ToString();
                    }

                }
            }
        }
        catch (Exception ex)
        {
            string msg = string.Format("Cannot read depfile table to get the DEPTID with this sql {0}.  Error: {1}", parameterizedSql, ex.ToString());
            Logger.cs_log("Deposit Processing Generate Slip Number: " + msg);

            // Bug 17849
            Logger.cs_log_trace("Error in readWorkgroupID", ex);
            return "";
        }

    }


    /// <summary>
        /// Query the Bank Account configuration for the auto_generate_slip_number and return the value.
        /// Bug 20731.
        /// </summary>
        /// <param name="bankID"></param>
        /// <returns></returns>
        private static bool isAutoGenerateSet(string bankID, string strDEPOSITID, string strDEPOSITNBR)
        {
          // First see if this account exists!
          GenericObject accountList = (GenericObject)c_CASL.c_object("Business.Bank_account.of");
          if (!accountList.Contains(bankID))
          {
            string error_message = string.Format("ERROR: PostDepositToT3: isAutoGenerateSet: The '{0}' Bank ID in the Deposit ({1}-{2})is not configured",
              bankID, strDEPOSITNBR, strDEPOSITNBR);
            Logger.Log(error_message, "Transuite: isAutoGenerateSet", "err");
            return false;
          }

          // Get the specific account
          String account = String.Format("Business.Bank_account.of.\"{0}\"", bankID);
          GenericObject bankAccountGEO = (GenericObject)c_CASL.c_object(account);


          // Now see if it contains the reconcilable bool
          if (!bankAccountGEO.has("auto_generate_slip_number"))
          {
            // Not set
            return false;
          }

          bool auto_generate = (bool)bankAccountGEO.get("auto_generate_slip_number");

          return auto_generate;
        }

    public static string CreateSQLFor_TG_DEPOSITTNDR_DATA(string strDEPOSITNBR,
      string strDEPOSITID,
      string strTNDRPROP,
      string strTNDR,
      string strTNDRDESC,
      string strTNDR_AMOUNT,
      string strCURRENCYTYPE,
      string  strCURRENCYAMOUNT,
      string strCURRENCYEXCHANGERATE,
      int iDBType,
      string strTNDR_SOURCE_DATE,
      string strTNDR_SOURCE_REFID,
      string strSeqNbr)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      string strSQL = "";
      strSQL = "INSERT INTO TG_DEPOSITTNDR_DATA (DEPOSITID, DEPOSITNBR, SEQNBR, TNDRPROP, TNDR, TNDRDESC, AMOUNT, ";
      strSQL += "SOURCE_REFID, SOURCE_DATE) VALUES ('";
      strSQL += strDEPOSITID + "', " + strDEPOSITNBR + ", " + strSeqNbr + ", '";
      strSQL += strTNDRPROP + "', '" + strTNDR + "', '" + strTNDRDESC + "', " + strTNDR_AMOUNT + ", ";

      if (strTNDR_SOURCE_REFID.Length > 0)
        strSQL = strSQL + "'" + strTNDR_SOURCE_REFID + "', ";
      else
        strSQL = strSQL + "NULL, ";

      if (strTNDR_SOURCE_DATE.Length > 0)
      {
        if (strTNDR_SOURCE_DATE.Length == 10)
          strTNDR_SOURCE_DATE = strTNDR_SOURCE_DATE + " 00:00:00";

        if (iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_ODBC || iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_TNS)
          strSQL = strSQL + "TO_DATE('" + strTNDR_SOURCE_DATE + "','MM/DD/YYYY HH24:MI:SS') );";
        else // MSACCESS and SQL Server
          strSQL = strSQL + "'" + strTNDR_SOURCE_DATE + "');";
      }
      else
        strSQL = strSQL + "NULL);";
      if (strCURRENCYTYPE != "" && !strCURRENCYAMOUNT.StartsWith("0.00"))
      {
          strSQL += " INSERT INTO TG_DEPOSITTNDRCURRENCY_DATA (DEPOSITID, DEPOSITNBR, SEQNBR, CURRENCYTYPE, CURRENCYAMOUNT, CURRENCYEXCHANGERATE ) VALUES ('";
          strSQL += strDEPOSITID + "', " + strDEPOSITNBR + ", " + strSeqNbr + ", '";
          strSQL += strCURRENCYTYPE + "', " + strCURRENCYAMOUNT + ", " + strCURRENCYEXCHANGERATE + ");";
      }
      return strSQL;
    }

    public static string CreateSQLFor_TG_DEPOSIT(string strDEPOSITID,
      string strDEPOSITNBR,
      string strDEPOSITTYPE,
      string strFileNumber,
      string strFileSequenceNbr,
      string strOWNERUSERID,
      string strCREATERUSERID,
      string strDEPSLIPNBR,
      string strAMOUNT,
      string strGROUPNBR,
      string strGROUPID,
      string strCOMMENTS,
      string strBANKID,
      string strDep_SOURCE_TYPE,
      string strDep_SOURCE_GROUP,
      string strDepSOURCE_DATE,
      int iDBType,
      string strNowDateTime,
      string strDepSOURCE_REFID)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      string strSQL;

      strSQL = "INSERT INTO TG_DEPOSIT_DATA (DEPOSITID, DEPOSITNBR, DEPOSITTYPE, GROUPID, GROUPNBR, ";
      strSQL += "DEPFILENBR, DEPFILESEQ, OWNERUSERID, CREATERUSERID, AMOUNT, POSTDT, COMMENTS, DEPSLIPNBR, ";
      strSQL += "BANKID, SOURCE_TYPE, SOURCE_GROUP, SOURCE_REFID, SOURCE_DATE) ";
      strSQL += "VALUES('" + strDEPOSITID + "', " + strDEPOSITNBR + ", '" + strDEPOSITTYPE + "', ";

      if (strGROUPID.Length > 0 && strGROUPNBR.Length > 0)
        strSQL = strSQL + "'" + strGROUPID + "', " + strGROUPNBR + ", ";
      else
        strSQL = strSQL + "NULL, NULL, ";

      if (strFileNumber.Length > 0)
        strSQL = strSQL + strFileNumber + ", ";
      else
        strSQL = strSQL + "NULL, ";

      if (strFileSequenceNbr.Length > 0)
        strSQL = strSQL + strFileSequenceNbr + ", ";
      else
        strSQL = strSQL + "NULL, ";

      if (strOWNERUSERID.Length > 0)
        strSQL = strSQL + "'" + strOWNERUSERID + "', ";
      else
        strSQL = strSQL + "NULL, ";

      if (strCREATERUSERID.Length > 0)
        strSQL = strSQL + "'" + strCREATERUSERID + "', ";
      else
        strSQL = strSQL + "NULL, ";

      strSQL = strSQL + strAMOUNT + ", ";

      // ORACLE 
      if (iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_ODBC || iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_TNS)
        strSQL = strSQL + "TO_DATE('" + strNowDateTime + "','MM/DD/YYYY HH24:MI:SS'), ";
      else // MSACCESS and SQL Server
        strSQL = strSQL + "'" + strNowDateTime + "', ";

      if (strCOMMENTS.Length > 0)
        strSQL = strSQL + "'" + strCOMMENTS + "', ";
      else
        strSQL = strSQL + "NULL, ";

      if (strDEPSLIPNBR.Length > 0)
        strSQL = strSQL + "'" + strDEPSLIPNBR + "', ";
      else
        strSQL = strSQL + "NULL, ";
      if (strBANKID.Length > 0)
        strSQL = strSQL + "'" + strBANKID + "', ";
      else
        strSQL = strSQL + "NULL, ";

      if (strDep_SOURCE_TYPE.Length > 0)
        strSQL = strSQL + "'" + strDep_SOURCE_TYPE + "', ";
      else
        strSQL = strSQL + "NULL, ";

      if (strDep_SOURCE_GROUP.Length > 0)
        strSQL = strSQL + "'" + strDep_SOURCE_GROUP + "', ";
      else
        strSQL = strSQL + "NULL, ";

      if (strDepSOURCE_REFID.Length > 0)
        strSQL = strSQL + "'" + strDepSOURCE_REFID + "', ";
      else
        strSQL = strSQL + "NULL, ";

      if (strDepSOURCE_DATE.Length > 0)
      {
        if (strDepSOURCE_DATE.Length == 10)
          strDepSOURCE_DATE = strDepSOURCE_DATE + " 00:00:00";

        if (iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_ODBC || iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_TNS)
          strSQL = strSQL + "TO_DATE('" + strDepSOURCE_DATE + "','MM/DD/YYYY HH24:MI:SS') )";
        else
          strSQL = strSQL + "'" + strDepSOURCE_DATE + "')";
      }
      else
        strSQL = strSQL + "NULL)";

      return strSQL;
    }

    public static GenericObject VoidDepositInT3(params Object[] arg_pairs)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject a_deposit = args.get("_subject", c_CASL.c_undefined) as GenericObject;
      GenericObject config = args.get("config", c_CASL.c_undefined) as GenericObject;
      GenericObject data = args.get("data", c_CASL.c_undefined) as GenericObject;
      string strErr = "";
      string strSQL = "";
      DateTime dtDate = DateTime.Now;
      string strNowDateTime = dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo);
      ConnectionTypeSynch pDBConn = null;
      bool connectionOpened = false;      // Bug 15941
      string session_id = args.get("session_id") as string;

      StringBuilder details = new StringBuilder();

      // Bug 15941. FT. Make sure connection is always closed
      try
      {
      //**********************************************************************************
      // It is possible to pass in a database connection using the "DbConnectionClass" key.
      // Connection must be of type ConnectionTypeSynch class.
      if (args.has("DbConnectionClass"))
        pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
      else
      {
        pDBConn = new ConnectionTypeSynch(data);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strErr)))  // Bug 15941
        {
          strErr = "Error opening database connection.";
          HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-850", strErr);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
        if (!pDBConn.EstablishRollback(ref strErr))
        {
          strErr = "Error creating database transaction." + strErr;
            ThrowCASLErrorButDoNotCloseDBConnection("TS-892", ref pDBConn, "Error creating database transaction.", strErr);
        }
      }
      //**********************************************************************************

      //**********************************************************************************
      // Deposit Data
      string strVOIDUSERID = GetKeyData(a_deposit, "VOIDUSERID", true);
      string strVOIDDT = GetKeyData(a_deposit, "VOIDDT", true);
      string strDEPOSITID = GetKeyData(a_deposit, "DEPOSITID", true);
      string strDEPOSITNBR = GetKeyData(a_deposit, "DEPOSITNBR", true);
      string strfile_SOURCE_TYPE = GetKeyData(a_deposit, "file_SOURCE_TYPE", true);
      string strfile_SOURCE_GROUP = GetKeyData(a_deposit, "file_SOURCE_GROUP", true);
      string strSOURCE_REFID = GetKeyData(a_deposit, "SOURCE_REFID", true);
      string strLOGIN_ID = GetKeyData(a_deposit, "LOGIN_ID", true); //BUG 5395v2 (VoidDepositInT3)

      // Validate user ID
      if (strVOIDUSERID.Length == 0)
          ThrowCASLErrorButDoNotCloseDBConnection("TS-893", ref pDBConn, "User ID is required to Void Deposit!", "");

      //file source came in as either Cashier or Portal which is needed for checking the session
      //however, the rest of the code always expects it to be Cashier, so will change it here
      if (strfile_SOURCE_TYPE == "Portal")
        strfile_SOURCE_TYPE = "Cashier";
      //end BUG 5395

      //Either Deposit Info or Source Info is required to void
      if ((strDEPOSITID.Length == 0 || strDEPOSITNBR.Length == 0) &&
        (strfile_SOURCE_TYPE.Length == 0 || strfile_SOURCE_GROUP.Length == 0 || strSOURCE_REFID.Length == 0))
      {
          ThrowCASLErrorButDoNotCloseDBConnection("TS-894", ref pDBConn, "Either complete Deposit ID and Deposit Nbr OR Source Type, Source Group, and Source RefID are required to void a deposit!", "");
      }

      // Make sure Void date has either a passed value or the current date/time
      if (strVOIDDT.Length == 0)
        strVOIDDT = strNowDateTime;
      if (strVOIDDT.Length == 10)
        strVOIDDT += " 00:00:01";
      //**********************************************************************************

      //**********************************************************************************

      if (strDEPOSITID.Length == 0 || strDEPOSITNBR.Length == 0)
      {
        strSQL = string.Format("SELECT * FROM TG_DEPOSIT_DATA WHERE DEPOSITTYPE IS NOT NULL AND SOURCE_TYPE='{0}' AND SOURCE_GROUP='{1}' AND SOURCE_REFID='{2}'", strfile_SOURCE_TYPE, strfile_SOURCE_GROUP, strSOURCE_REFID);
        GenericObject geoVoidDeposit = (GenericObject)GetRecords(data, 1, strSQL, pDBConn.GetDBConnectionType());
        if (geoVoidDeposit == null)
            ThrowCASLErrorButDoNotCloseDBConnection("TS-895", ref pDBConn, "Deposit Data Open Error: No record found for: ", strSQL);
        else
        {
          int iCount = geoVoidDeposit.getIntKeyLength();
          if (iCount > 1)
              ThrowCASLErrorButDoNotCloseDBConnection("TS-896", ref pDBConn, "Ambiguous Search Error: " + iCount.ToString() + " deposit records found.", "");
          else
          {
            GenericObject geoTmp = (GenericObject)geoVoidDeposit.get(0);
            strDEPOSITID = (string)GetKeyData(geoTmp, "DEPOSITID", true);
            strDEPOSITNBR = (string)GetKeyData(geoTmp, "DEPOSITNBR", true);
          }
        }
      }
      //**********************************************************************************

      // Validate Deposit
      if (!ValidateDepositChecksum("strDepositID", strDEPOSITID, "strDepositNbr", strDEPOSITNBR, "DbConnectionClass", pDBConn))
          ThrowCASLErrorButDoNotCloseDBConnection("TS-897", ref pDBConn, "Invalid checksum for deposit: " + strDEPOSITNBR, "");

      //**********************************************************************************
      // Void the deposit
      strSQL = "UPDATE TG_DEPOSIT_DATA SET VOIDDT=";

      // Oracle
      if (pDBConn.GetDBConnectionType() == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_ODBC || pDBConn.GetDBConnectionType() == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_TNS)
        strSQL = strSQL + "TO_DATE('" + strVOIDDT + "','MM/DD/YYYY HH24:MI:SS'), ";
      else // MS-Access and SQL Server
        strSQL = strSQL + "'" + strVOIDDT + "', ";

      strSQL = strSQL + "VOIDUSERID='" + strVOIDUSERID + "' ";
      strSQL = strSQL + "WHERE DEPOSITID='" + strDEPOSITID + "' AND DEPOSITNBR=" + strDEPOSITNBR;

      strErr = "";
      if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strErr))
          ThrowCASLErrorButDoNotCloseDBConnection("TS-898", ref pDBConn, " Unable to void deposit. ", strErr);
      //**********************************************************************************

      // ALL GOOD - ACCEPT THE UPDATES.
      if (!args.has("DbConnectionClass"))// Caller will commit.
      {
        strErr = "";
        if (!pDBConn.CommitToDatabase(ref strErr))
          HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-851", strErr);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix//Bug 11329 NJ-show verbose fix
      }

      a_deposit.set("DEPOSITID", strDEPOSITID);
      a_deposit.set("DEPOSITNBR", strDEPOSITNBR);

      // Bug 22343: Log to activity log
      details.Append("VoidUser:").Append(strVOIDUSERID);
      details.Append(",LoginID:").Append(strLOGIN_ID);
      details.Append(",VoidDate:").Append(strVOIDDT);
      details.Append(",DepositID:").Append(strDEPOSITID);
      details.Append(",DepositNbr:").Append(strDEPOSITNBR);
      LogToActivityLogWithDetails(strfile_SOURCE_TYPE, "Void Deposit", "Void Deposit", details.ToString());
      // End Bug 22343

      return a_deposit;
    }
      finally
      {
        // Bug 15941
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }

    }

    public static GenericObject VoidAllDepositsInT3File(params Object[] arg_pairs)
    {
      //			GenericObject args = misc.convert_args(arg_pairs);
      //			GenericObject a_DEPFILE = args.get("a_DEPFILE", c_CASL.c_undefined) as GenericObject;
      //			GenericObject config = args.get("config", c_CASL.c_undefined) as GenericObject;
      //
      //			String datasource_name = config.get("datasource_name") as string;
      //			String login_name = config.get("login_name") as string;
      //			String login_password = config.get("login_password") as string;
      //			CBT_TG_DBLib.CBT_TG_DBClass myCtrl = TranSuiteServicesInit("datasource_name",datasource_name, "login_name",login_name, "login_password",login_password);
      //			bool bRV = myCtrl.SourceVoidAllFileDeposits(a_DEPFILE);
      //
      //			if( bRV == false )
      //			{
      //				m_sErrMsg = myCtrl.GetErrMsg();
      //				HandleErrorsAndLocking("message", m_sErrMsg, "code", "TS-852");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      //			}
      //			myCtrl =null;
      //			return a_DEPFILE;

      return new GenericObject();
    }

    public static GenericObject AddCustomFields(params Object[] arg_pairs)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject pDataGeo = args.get("_subject", c_CASL.c_undefined) as GenericObject;
      GenericObject data = args.get("data", c_CASL.c_undefined) as GenericObject;
      ConnectionTypeSynch pDBConn = null;// If part of posting, this will be already valid.
      bool connectionOpened = false;      // Bug 15941
      string strErr = "";
      string strSQL = "";
      string strSQLUnionData = "";/*BUG# 10600 - DH 2/19/2011 Combine statements*/
      string strFileNumber = "";
      string strFileSequenceNbr = "";
      string strFILENAME = "";
      string strEVENTNBR = "";// Manufactured in posting process or directly from CASL call.
      string strTRANNBR = "";// Manufactured in posting process or directly from CASL call.
      string strTTID = "";
      GenericObject pCustKeys = null;

      // Bug 18766: Parameterized and Bug 16540. Seems too complicated....
      List<List<KeyValuePair<string, string>>> paramList = new List<List<KeyValuePair<string, string>>>();
      int paramCount = 0;
      // End Bug 16540

      // Bug 15941. FT. Make sure connection is always closed
      try
      {

      // Check if function called directly from CASL or is part of posting process.
      // If part of posting then it should have a DB connection already and will commit in the caller function.
      if (args.has("DbConnectionClass"))
        pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
      else
      {
        // If we are here, this function was called from CASL.
        // We have to get a new database connection and later commit.
        pDBConn = new ConnectionTypeSynch(data);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strErr)))  // Bug 15941
          HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-853", strErr);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        if (!pDBConn.EstablishRollback(ref strErr))
          HandleErrorsAndLocking("message", "Error creating database transaction.", "code", "TS-854", strErr);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      strFILENAME = GetKeyData(args, "file_FILENAME", false);
      strEVENTNBR = GetKeyData(args, "event_EVENTNBR", false);
      strTRANNBR = GetKeyData(args, "transaction_TRANNBR", false);
      strTTID = GetKeyData(args, "transaction_TTID", false);

      // Break the file into file number and sequence number
      // Bug #12055 Mike O - Use misc.Parse to log errors
        strFileNumber = misc.Parse<Int64>(GetFileNumber(strFILENAME)).ToString();
      strFileSequenceNbr = misc.Parse<Int32>(GetFileSequence(strFILENAME)).ToString();

      if (!pDataGeo.has("_custom_keys"))
        return pDataGeo;

      pCustKeys = (GenericObject)pDataGeo.get("_custom_keys", c_CASL.c_error, true);

      //****************************************************************************
      // LOOP THROUGH ALL TRANSACTION CUSTOM KEYS DATA
      long lScreenIndex = 0;
      int iCustKeysInTransaction = (int)pCustKeys.getIntKeyLength();

      // Bug 12276 & 12309 DH - Get out if there are no custom keys
      if (iCustKeysInTransaction == 0)
        return pDataGeo;

      /*BUG# 10600 - DH 2/19/2011 Combine statements.*/
      bool IsSQLServer2008orLater = pDBConn.IsServer_SQL2008orLater();
      for (int x = 0; x < iCustKeysInTransaction; x++)
      {
        // Get Cust Keys        
        string strTAG = GetKeyData(pCustKeys, x, false);
        string strID = GetKeyData(pDataGeo, strTAG + "_f_id", false);
        string strValue = GetKeyData(pDataGeo, strTAG, false);
        string strLabel = GetKeyData(pDataGeo, strTAG + "_f_label", false);
        string strAttr = GetKeyData(pDataGeo, strTAG + "_f_attributes", false); // BUG3861 S.X. add GR_CUST_FIELD_DATA.CUSTATTR
        string strStoreDB = GetKeyData(pDataGeo, strTAG + "_f_store_in_DB", false); // Bug#6702 ANU - Custom fields to be checked to store in the Database.
        string strScreenIndex = GetKeyData(pDataGeo, strTAG + "_f_screen_index", false); //Bug 11042 NJ Screen index will be passed, if this function is called from CASL IDE.

        strStoreDB = strStoreDB.ToUpper(); // Bug #13951 UMN

        // Bug #11711 Mike O - For fake CFs, use the tagname as the id
        if (strID.Equals(""))
          strID = strTAG;



        // Get the next screen index.
        /* Bug#7984 To remove max call for screen index 
        if(pDBConn.GetDBConnectionType() == ConnectionTypeSynch.m_iCurrentDatabase_SqlServer)
          strSQL = string.Format("SELECT MAX(SCREENINDEX) AS MAX FROM GR_CUST_FIELD_DATA (NOLOCK) WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} AND TRANNBR={3}", Int64.Parse(strFileNumber), Int64.Parse(strFileSequenceNbr), Int64.Parse(strEVENTNBR), Int64.Parse(strTRANNBR));
        else
          strSQL = string.Format("SELECT MAX(SCREENINDEX) AS MAX FROM GR_CUST_FIELD_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} AND TRANNBR={3}", Int64.Parse(strFileNumber), Int64.Parse(strFileSequenceNbr), Int64.Parse(strEVENTNBR), Int64.Parse(strTRANNBR));
        				
         * */

        //string error_message=""; 12081 NJ-unused variable
        /*  Bug#7984 To remove max call for screen index 
       if(lScreenIndex == 0)// Do only for the first field.
         lScreenIndex = GetMaxForField(data, strSQL, "MAX", pDBConn.GetDBConnectionType(), ref error_message);
        */

        lScreenIndex += 1;
        //Bug 11042 NJ
        //If screen index is not passed, allot one
        if (strScreenIndex == "")
        {
          strScreenIndex = string.Format("{0}", lScreenIndex);
        }
        //End Bug 11042 NJ
        pCustKeys = (GenericObject)pCustKeys.set("SCREENINDEX", strScreenIndex);
        pCustKeys = (GenericObject)pCustKeys.set("CUSTLABEL", strLabel);
        pCustKeys = (GenericObject)pCustKeys.set("CUSTVALUE", strValue);

        //Matt -- replacing the old checksum call
        // Bug #10955 Mike O
        string strChecksum = CreateGRCustomFieldChecksum(Crypto.DefaultHashAlgorithm, "geoChecksumData", pCustKeys, "strEventNbr", strEVENTNBR, "strTranNbr", strTRANNBR, "strFileName", strFILENAME, "strScreenIndex", strScreenIndex);
        //-----------------------------------------

        // Insert custom fields into GR_CUST_FIELD_DATA.
        if (strStoreDB != "FALSE" && !String.IsNullOrEmpty(strValue)) //Bug#6702/13951 UMN
        {
          // Bug 23645 MJO - Only use batch if there aren't too many parameters and hardcode whether batch should be used
          // NOTE: If the number of parameters is changed in the future, the parameter count needs to be updated
          if (IsSQLServer2008orLater && iCustKeysInTransaction * 12 <= 2100)     
          {
            /*BUG# 10600 - DH 2/19/2011 Combine statements*/
            if (x > 0 && x < iCustKeysInTransaction && strSQLUnionData != "" /*Bug 12054 - DH x may not start at zero because of Bug#6702*/)
              strSQLUnionData += ",";
            // Bug 18766: Parameterized
            strSQLUnionData += ParameterizedBatch.CreateSQLFor_GR_CUST_FIELD_DATA(paramList, paramCount, true, strTRANNBR, strEVENTNBR, strTTID, strFileNumber, strFileSequenceNbr, strTAG, strID, strValue, strLabel, strScreenIndex, strAttr, strChecksum);
          }
          else
          {
            strErr = "";
            // Bug 18766: Parameterized
            strSQL = ParameterizedBatch.CreateSQLFor_GR_CUST_FIELD_DATA(paramList, paramCount, false, strTRANNBR, strEVENTNBR, strTTID, strFileNumber, strFileSequenceNbr, strTAG, strID, strValue, strLabel, strScreenIndex, strAttr, strChecksum);
            if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strErr))
              HandleErrorsAndLocking("message", "Error executing SQL.", "code", "TS-855", strErr);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
          }
          paramCount++;     // Bug 18766: Parameterized

        }
      }
      /*BUG# 10600 - DH 2/19/2011 Combine statements*/
      if (IsSQLServer2008orLater && iCustKeysInTransaction > 0 && strSQLUnionData != "")
      {
        strErr = "";
        strSQL = "INSERT INTO GR_CUST_FIELD_DATA (TRANNBR, EVENTNBR, DEPFILENBR, DEPFILESEQ, ";
        strSQL += "SCREENINDEX, CUSTLABEL, CUSTVALUE, CUSTID, TTID, CUSTTAG, CUSTATTR, CHECKSUM) VALUES ";

        // Bug 18766: Parameterized
        string parameterizedQueryString = ParameterizedBatch.buildParameterizedQueryForBatchInsert(paramList);
        ParameterizedBatch.runParameterizedBatchedInsert(pDBConn, strSQL, parameterizedQueryString, paramList);

        strSQL += strSQLUnionData;
      }
      //****************************************************************************

      // ALL GOOD - ACCEPT THE UPDATES.    
      if (!args.has("DbConnectionClass"))// Caller will commit.
      {
        strErr = "";
        if (!pDBConn.CommitToDatabase(ref strErr))
          HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-856", strErr);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }


      return pDataGeo;
    }
      finally
      {
        // Bug 15941
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static object BalanceFile(
      GenericObject data,
      GenericObject config,
      GenericObject depfile,
      string session_id,
      string filename,
      string department_id,
      string user_id,
      double balance_total,
      string file_status,
      string login_id, // Bug#6946 ANU
      GenericObject overshort_event
    )
    {
      //#region Arguments
      string error_message = "";
      string depfilenbr = GetFileNumber(filename);
      string depfileseq = GetFileSequence(filename);
      DateTime today = DateTime.Now;
      //#endregion
      //#region Validate file checksum
      //if(!ValidateFileChecksumTree(filename, data))
      {
        //TS-1242 TODO: unchecked error condition
        //return misc.MakeCASLError("bf","bf","bf");
      }
      //#endregion
      //#region Mark File Balanced
      string sql_text = @"UPDATE TG_DEPFILE_DATA SET BALDT=@balance_date, BALTOTAL=@balance_total, BAL_USERID=@user_id
        WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq"; // BUG# 6263 DH
      Hashtable sql_args = new Hashtable(5);
      sql_args["balance_date"] = DateTime.Now;    // Calculated
      sql_args["balance_total"] = balance_total;  // Passed in
      sql_args["user_id"] = user_id;              // Passed in
      sql_args["depfilenbr"] = depfilenbr;        // Calculated from passed in filename
      sql_args["depfileseq"] = depfileseq;        // Calculated from passed in filename
      IDBVectorReader reader = db_reader.Make(data, sql_text, CommandType.Text, 0, sql_args, 0, 0, null, ref error_message);
      if (error_message != "")
      {
        return misc.MakeCASLError("Balance File Error", "TS-1243", "Could not update file.");
      }
      int row_count = 0;
      row_count = reader.ExecuteNonQuery(ref error_message);
      if (error_message != "" || row_count > 1)
      {
        return misc.MakeCASLError("Balance File Error", "TS-1243", "Could not update file.");
      }
      else if (row_count == 0)
      {
        return misc.MakeCASLError("Balance File Error", "TS-1243", "Could not update file.");
      }
      //#endregion
      //#region Establish Transaction
      reader = db_reader.MakeTransactionReader(data, 0, 0, 0, ref error_message);
      if (error_message != "")
      {
        return misc.MakeCASLError("Balance File Error", "TS-1243", "Could not update file.");
      }
      //#region Add Update File Information SQL


      //#endregion
      //#region Add Over-Short Event
      if (overshort_event != null)
      {
        GenericObject license = c_CASL.c_object("License.data") as GenericObject;
        GenericObject overshort_transaction = null;
        GenericObject overshort_tender = null;

        GenericObject new_event = CreateNewEvent(data, depfilenbr, depfileseq, license);
        string new_event_EVENTNBR = GetStringValue(new_event, "EVENTNBR");
        string new_event_DEPFILENBR = GetStringValue(new_event, "DEPFILENBR");
        string new_event_DEPFILESEQ = GetStringValue(new_event, "DEPFILESEQ");
        string new_event_UNIQUEID = GetStringValue(new_event, "UNIQUEID");
        string new_event_MOD_DT = today.ToString("G", DateTimeFormatInfo.InvariantInfo);

        //#region Merge new_event with overshort_event
        overshort_event.set(
          "DEPFILENBR", new_event_EVENTNBR,
          "DEPFILESEQ", new_event_EVENTNBR,
          "EVENTNBR", new_event_EVENTNBR,
          "MOD_DT", new_event_MOD_DT,
          // Bug #7599 Mike O - Status changed from 8 to 5
          "STATUS", 5,
          "UNIQUEID", new_event_UNIQUEID);
        overshort_event.set("CHECKSUM", CreateEventChecksum(overshort_event));
        sql_args.Clear();
        sql_text = CreateEventUpdateSQL(overshort_event, ref sql_args, ref error_message);
        reader.AddCommand(sql_text, CommandType.Text, sql_args);
        //#endregion

        overshort_transaction = (GenericObject)((GenericObject)overshort_event.get("table_TG_TRAN_DATA")).get(0);
        overshort_transaction.set(
          "DEPFILENBR", new_event_DEPFILENBR,
          "DEPFILESEQ", new_event_DEPFILESEQ,
          "EVENTNBR", new_event_EVENTNBR,
          "POSTDT", new_event_MOD_DT,
          "LOGIN_ID", login_id); //BUG#13915 add transaction post user id 
        overshort_transaction.set("CHECKSUM", CreateTransactionChecksum(overshort_transaction));
        sql_args.Clear();
        sql_text = CreateTransactionSQL(overshort_transaction, ref sql_args, ref error_message);
        reader.AddCommand(sql_text, CommandType.Text, sql_args);

        //#Bug#13711 QG Add Item commands
        if (!overshort_transaction.has("table_TG_ITEM_DATA"))
        {
            return misc.MakeCASLError("Balance File Error", "TS-1243", "Each Transaction must have at least one item to post.");
        }
        GenericObject item_data = (GenericObject)((GenericObject)overshort_transaction.get("table_TG_ITEM_DATA")).get(0);
        item_data.set(
            "DEPFILENBR", new_event_DEPFILENBR,
            "DEPFILESEQ", new_event_DEPFILESEQ,
            "EVENTNBR", new_event_EVENTNBR,
            "POSTDT", new_event_MOD_DT,
            "TRANNBR", overshort_transaction.get("TRANNBR").ToString());
        item_data.set("CHECKSUM", CreateItemChecksum(item_data));
        sql_args.Clear();
        sql_text = CreateItemSQL(item_data, ref sql_args, ref error_message);
        reader.AddCommand(sql_text, CommandType.Text, sql_args);
        //End of Bug#13711  

        if (overshort_event.has("table_TG_TENDER_DATA"))
        {
          overshort_tender = (GenericObject)((GenericObject)overshort_event.get("table_TG_TENDER_DATA")).get(0);
          overshort_tender.set(
            "DEPFILENBR", new_event_DEPFILENBR,
            "DEPFILESEQ", new_event_DEPFILESEQ,
            "EVENTNBR", new_event_EVENTNBR,
            "LOGIN_ID", login_id);//BUG#13915 add tender post user id 
          overshort_tender.set("CHECKSUM", CreateTenderChecksum(overshort_tender));
          sql_text = CreateTenderSQL(overshort_tender, ref sql_args, ref error_message);
          reader.AddCommand(sql_text, CommandType.Text, sql_args);
        }

        // TODO: unchecked error condition:
        // VoidEvent(new_event,data,ref error_message, user_id);
      }



      //#endregion
      //#endregion
      //#region Run and Commit Transaction
      row_count = 0;
      // Bug #11595 Mike O - Report all error messages instead of just the last one
      bool success = reader.EstablishTransaction(ref error_message);
      if (!success || error_message != "")
      {
        string sub_error_message = "";
        object void_obj = VoidEvent(overshort_event, data, ref sub_error_message, user_id);
        if (sub_error_message != "")
        {
          error_message += " " + sub_error_message;
          return misc.MakeCASLError("Balance File Error", "TS-1243", "Could not update file. " + error_message);
        }
        return misc.MakeCASLError("Balance File Error", "TS-1243", "Could not update file. " + error_message);
      }
      success = reader.ExecuteTransactionCommands(ref error_message, ref row_count, true);
      if (!success || error_message != "")
      {
        string sub_error_message = "";
        success = reader.RollbackTransaction(ref sub_error_message);
        if (!success || sub_error_message != "")
        {
          error_message += " " + sub_error_message;
          sub_error_message = "";
        }
        object void_obj = VoidEvent(overshort_event, data, ref sub_error_message, user_id);
        if (sub_error_message != "")
        {
          error_message += " " + sub_error_message;
          return misc.MakeCASLError("Balance File Error", "TS-1243", "Could not update file. " + error_message);
        }
        return misc.MakeCASLError("Balance File Error", "TS-1243", "Could not update file. " + error_message);
      }
      success = reader.CommitTransaction(ref error_message);
      if (!success || error_message != "")
      {
        string sub_error_message = "";
        success = reader.RollbackTransaction(ref sub_error_message);
        if (!success || sub_error_message != "")
        {
          error_message += " " + sub_error_message;
          sub_error_message = "";
        }
        object void_obj = VoidEvent(overshort_event, data, ref sub_error_message, user_id);
        if (sub_error_message != "")
        {
          error_message += " " + sub_error_message;
          return misc.MakeCASLError("Balance File Error", "TS-1243", "Could not update file. " + error_message);
        }
        return misc.MakeCASLError("Balance File Error", "TS-1243", "Could not update file. " + error_message);
      }
      // End Bug #11595 Mike O
      //#endregion

      reader.CloseReader();

      return true;
    }


    public static object MarkFileBalanced(params Object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject geoDepFile = args.get("a_DEPFILE", c_CASL.c_undefined) as GenericObject;
      GenericObject geoPayEvent = null;
      string strFILENAME = (string)geoDepFile.get("FILENAME");
      string strDEPTID = (string)geoDepFile.get("DEPTID");
      string strBAL_USERID = (string)geoDepFile.get("BAL_USERID");
      string strBALTOTAL = geoDepFile.get("BALTOTAL").ToString(); //Bug#8119 ANU to update the BalTotal in TG_DEPFILE_DATA table 
      string strOVERRIDE_INVALID_CHECKSUM = (string)args.get("OVERRIDE_INVALID_CHECKSUM");
      string strLogin_id = (string)geoDepFile.get("LOGIN_ID"); // Bug#6946 ANU
      GenericObject config = args.get("config", c_CASL.c_undefined) as GenericObject;
      GenericObject data = args.get("data", c_CASL.c_undefined) as GenericObject;
      string session_id = args.get("session_id") as string;

      //BUG 5357 Allow core file to be changed from updated to balanced. BLL 4/3/08
      object objFILESTATUS = args.get("a_FILESTATUS");
      string strFILESTATUS = "";
      if (objFILESTATUS != c_CASL.c_opt)
      {
        strFILESTATUS = objFILESTATUS.ToString();
      }

      if (!(args.get("a_PAYEVENT") == c_CASL.c_opt))
      {
        geoPayEvent = (GenericObject)args.get("a_PAYEVENT", c_CASL.c_undefined);
      }
      // Bug#6946 ANU Passing strLogin_id to check the session validity.
      return BalanceFile(
        // Bug #12055 Mike O - Use misc.Parse to log errors
        data, config, geoDepFile, session_id, strFILENAME, strDEPTID, strBAL_USERID, misc.Parse<Double>(strBALTOTAL), strFILESTATUS,strLogin_id,
        geoPayEvent); //bug 7197, 7245 sending check_session
    }
  
    /**
     * CASLActivateFile is the wrapper function for ActivateFile
     */
    public static object CASLActivateFile(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      GenericObject data = (GenericObject)geo_args.get("data");
      string filename = (string)geo_args.get("FILENAME");
      string deptid = (string)geo_args.get("DEPTID");
      // Bug #12055 Mike O - Use misc.Parse to log errors
      string filenbr=misc.Parse<Int64>(GetFileNumber(filename)).ToString();
      string fileseq=misc.Parse<Int64>(GetFileSequence(filename)).ToString();

      return ActivateFile(data, filenbr, fileseq, deptid);
    }

    public static bool AcquireFile(GenericObject data, string filenbr, string fileseq, string where_clause, ref string error_message)
    {
      string sql_statement = "UPDATE TG_DEPFILE_DATA SET VOLATILE=1 WHERE DEPFILENBR=@depfilenbr " +
        "AND DEPFILESEQ=@depfileseq AND VOLATILE=0";
      if (where_clause != null && where_clause != "")
      {
        sql_statement += " AND " + where_clause;
      }
      Hashtable sql_args = new Hashtable();
      sql_args["depfilenbr"] = filenbr;
      sql_args["depfileseq"] = fileseq;
      IDBVectorReader reader = db_reader.Make(data, sql_statement, CommandType.Text, 0,
        sql_args, 0, 0, null, ref error_message);
      int row_count = reader.ExecuteNonQuery(ref error_message);
      if (error_message != "" || row_count != 1)
      {
        return false;
      }
      return true;
    }
    public static bool ReleaseFile(GenericObject data, string filenbr, string fileseq, ref string error_message)
    {
      string sql_statement = "UPDATE TG_DEPFILE_DATA SET VOLATILE=0 WHERE DEPFILENBR=@depfilenbr " +
        "AND DEPFILESEQ=@depfileseq";
      Hashtable sql_args = new Hashtable();
      sql_args["depfilenbr"] = filenbr;
      sql_args["depfileseq"] = fileseq;
      IDBVectorReader reader = db_reader.Make(data, sql_statement, CommandType.Text, 0,
        sql_args, 0, 0, null, ref error_message);
      int row_count = reader.ExecuteNonQuery(ref error_message);
      if (error_message != "" || row_count != 1)
      {
        return false;
      }
      return true;
    }

    /// <summary>
    /// ActivateFile marks a file as active from its current state. The application
    /// assumes that security has already been checked for this action.
    /// </summary>
    /// <param name="data">GenericObject db_connection_info for the data database</param>
    /// <param name="filenbr">String file number</param>
    /// <param name="fileseq">String file sequence</param>
    /// <param name="deptid">String department id</param>
    /// <param name="userid">String user id</param>
    /// <returns></returns>
    public static object ActivateFile(
      GenericObject data, // Data database -- config DB not used
      string filenbr, string fileseq, // File data
      string deptid) // Department id 
    {
      //#region Variable setup
        string userid = "SYSTEM", msg = "";//Bug 13832-declare only once
      //#endregion
      //#region AcquireFile
      string where_clause = "BALDT IS NOT NULL";
      string error_message = "";
      if (!AcquireFile(data, filenbr, fileseq, where_clause, ref error_message))
      {
        //return misc.MakeCASLError("Activate File Error","TS-1245","Could not update file.");
        //Bug 11329 NJ- used this method to return as user friendly error
        //13832-Added inner exception
        GenericObject err = CreateErrorObject("TS-1245", "Could not update file.", false, "Activate File Error", true);
        err.set("inner_error", misc.MakeCASLError("", "", error_message));
        return err;
          //end bug 13832
          
      }
      //#endregion
      //#region Check if valid for activating (cannot be accepted or updated)

      // BEGIN: Bug 15464 DJD - Batch Update Overhaul
      // If a CORE file was previously included in a batch update job and has one or more
      // successfully completed system updates then it CANNOT be reactivated.
      string sql_statement = string.Format("{0} {1} {2} {3}"
          , "SELECT TDD.*, LOA.UPDATE_ID FROM TG_DEPFILE_DATA TDD"
          , "OUTER APPLY (SELECT TOP(1) UPDATE_ID FROM TG_UPDATEFILE_DATA TUD"
          , "WHERE TUD.DEPFILENBR=TDD.DEPFILENBR AND TUD.DEPFILESEQ=TDD.DEPFILESEQ AND TUD.STATUS='COMPLETE') LOA"
          , "WHERE TDD.DEPFILENBR=@depfilenbr AND TDD.DEPFILESEQ=@depfileseq"
          );
      // END: Bug 15464 DJD - Batch Update Overhaul

      Hashtable sql_args = new Hashtable();
      sql_args["depfilenbr"] = filenbr;
      sql_args["depfileseq"] = fileseq;
      GenericObject fileRecords = ExecuteSQL(data, sql_statement, CommandType.Text, 0,
        sql_args, 0, 0, null, ref error_message);
      if (error_message != "" || fileRecords.getLength() != 1)
      {
        //Bug 11329-used this method to return as user friendly error
          //13832-split into friendly and verbose message
          msg = error_message;
          error_message = "";
          GenericObject return_error = CreateErrorObject("TS-1245", "Could not access file data.", false, "Activate File Error", true);
        if (!ReleaseFile(data, filenbr, fileseq, ref error_message) || error_message != "")
        {
            //Bug 13832-show errors from both operation
          return_error.set("inner_error", misc.MakeCASLError("Activate File Error", "TS-1251", "Error releasing file: " + (msg + error_message))); 
        }
        return return_error;
      }
      GenericObject fileRecord = fileRecords.get(0) as GenericObject;
      if (GetStringValue(fileRecord, "UPDATEDT") != "")
      {
        //Bug 11329-used this method to return as user friendly error
        msg = "File " + filenbr.PadLeft(7, '0') + "-" + fileseq.PadLeft(3, '0') +
        " has already been updated. You cannot reactivate it.";
        GenericObject return_object = CreateErrorObject("TS-1246", msg, false, "Activate File Error", true); 
        if (!ReleaseFile(data, filenbr, fileseq, ref error_message) || error_message != "")
        {
          return_object.set("inner_error", misc.MakeCASLError("Activate File Error", "TS-1251", "Error releasing file: " + error_message));
        }
        return return_object;
      }
      else if (GetStringValue(fileRecord, "ACCDT") != "")
      {
        //Bug 11329-used this method to return as user friendly error
        msg = "File " + filenbr.PadLeft(7, '0') + "-" + fileseq.PadLeft(3, '0') +
        " has already been accepted. You cannot reactivate it.";

        GenericObject return_object = CreateErrorObject("TS-1246", msg, false, "Activate File Error", true);
        if (!ReleaseFile(data, filenbr, fileseq, ref error_message) || error_message != "")
        {
          return_object.set("inner_error", misc.MakeCASLError("Activate File Error", "TS-1251", "Error releasing file: " + error_message));
        }
        return return_object;
      }
      else if (GetStringValue(fileRecord, "UPDATE_ID") != "") // BEGIN: Bug 15464 DJD - Batch Update Overhaul
      {
        msg = "The update process for file " + filenbr.PadLeft(7, '0') + "-" + fileseq.PadLeft(3, '0') +
        " has already been started. You cannot reactivate it.";

        GenericObject return_object = CreateErrorObject("TS-1246", msg, false, "Activate File Error", true);
        if (!ReleaseFile(data, filenbr, fileseq, ref error_message) || error_message != "")
        {
          return_object.set("inner_error", misc.MakeCASLError("Activate File Error", "TS-1251", "Error releasing file: " + error_message));
        }
        return return_object;
      }                                                       // END: Bug 15464 DJD - Batch Update Overhaul
      //#endregion
      
      //#region Build sql transaction
      IDBVectorReader reader = db_reader.MakeTransactionReader(data, 0, 0, 0, ref error_message);
      if (error_message != "")
      {
        reader.CloseReader();
        //bug 11329 nj-used this method to display user friendly message
         msg = error_message;
        GenericObject return_object = CreateErrorObject("TS-1246", "Could not open transaction reader.", false, "Activate File Error", true);
        // misc.MakeCASLError("Activate File Error", "TS-1246", "Could not open transaction reader.");
        if (!ReleaseFile(data, filenbr, fileseq, ref error_message) || error_message != "")
        {
            //13832-show errors from both operations
          return_object.set("inner_error", misc.MakeCASLError("Activate File Error", "TS-1251", "Error releasing file: " + msg + error_message));
        }
        return return_object;
      }
      //#endregion
      //#region Void Over/Short records
      // Status 5 indicated over/short (non void system completed event)
      //sql_statement = "SELECT * FROM TG_PAYEVENT_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND STATUS='5' AND USERID='SYSTEM'"; //Bug#8272 included USERID='SYSTEM' to void just the system completed events for overshort ANU.
      // Bug 22529 HX: Overshort transaction and tender record does not get voided on core file status set to Active
      sql_statement = "SELECT event.* FROM TG_PAYEVENT_DATA event,TG_TRAN_DATA trans ";
      sql_statement += "WHERE event.DEPFILENBR = trans.DEPFILENBR AND event.DEPFILESEQ = trans.DEPFILESEQ AND event.EVENTNBR = trans.EVENTNBR and trans.TTID='SYSOVERSHORT' AND "; 
      sql_statement += "event.DEPFILENBR=@depfilenbr AND event.DEPFILESEQ=@depfileseq AND event.STATUS='5' "; 
      //Bug#7599 undid the changes and back to '5' ANU Bug#7590 status was '5' ANU
      GenericObject system_events = ExecuteSQL(data, sql_statement, CommandType.Text, 0, sql_args, 0, 0, null, ref error_message);
      if (error_message != "")
      {
        reader.CloseReader();
        //bug 11329 nj-used this method to show as user friendly method
        msg = error_message;
        GenericObject return_object = CreateErrorObject("TS-1247", "Could not obtain system transaction records.", false, "Activate File Error", true);
        //misc.MakeCASLError("Activate File Error","TS-1247","Could not obtain system transaction records.");
        if (!ReleaseFile(data, filenbr, fileseq, ref error_message) || error_message != "")
        {
            //13832 show errors from both operations
          return_object.set("inner_error", misc.MakeCASLError("Activate File Error", "TS-1251", "Error releasing file: " + msg + error_message));
        }
        return return_object;
      }
      for (int i = 0; i < system_events.getLength(); i++)
      {
        object void_object = TranSuiteServices.VoidEvent((GenericObject)system_events.get(i), data, ref error_message, userid);
        string void_error_message = error_message;
        error_message = "";
        if (!void_object.Equals(true) || void_error_message != "") // A problem occurred
        {
            /* 12081 NJ- unused variable
             * string status="";  //bug 6665
             status="Active File Error"; //bug 6665*/
          //bug 11329 nj-used this method to show as user friendly method
          GenericObject return_object = CreateErrorObject("TS-1248", "Could not void system event.", false, "Activate File Error", true);
          //misc.MakeCASLError("Activate File Error","TS-1248","Could not void system event.");
          // Try to rollback
          bool rollback_successful = reader.RollbackTransaction(ref error_message);
          string rollback_error_message = error_message;
          error_message = "";

          // A failed rollback makes a big error
          if (!void_object.Equals(true))
          {
            return_object.set("void_error", void_object);
          }
          if (void_error_message != "")
          {
            return_object.set("void_error_message", void_error_message);
          }
          if (!rollback_successful)
          {
            return_object.set("rollback_error", misc.MakeCASLError("Activate File Error", "TS-1249", "Could not rollback transaction."));
            return_object.set("rollback_error_message", rollback_error_message);
          }
          reader.CloseReader();
          return return_object;
        }
        
        // Bug 22528 HX Record a Reason Code/Comment for all System Voids, also see Bug 22074
        GenericObject busi_notes_data= c_CASL.c_object("Business.Notes.data") as GenericObject;
        bool b_save_system_void = (bool)busi_notes_data.get("void_reason_for_system_voids", false);
        if (b_save_system_void)
        {
          GenericObject voidreasons = c_CASL.c_object("GEO.sys_void_reasons") as GenericObject;
          string sSys_001 = voidreasons.get("sys_001", "") as string;

          GenericObject args = new GenericObject();
        GenericObject note_info = new GenericObject();
        note_info["FILEID"] = String.Format("{0}{1}",GetStringValue((GenericObject)system_events.get(i), "DEPFILENBR"),GetStringValue((GenericObject)system_events.get(i),"DEPFILESEQ").PadLeft(3,'0'));
        note_info["EVENTNBR"] = GetStringValue((GenericObject)system_events.get(i), "EVENTNBR");
        note_info["OWNER_TYPE"] = "Event";
        note_info["NOTE_TYPE"] = "VoidReason";
          note_info["VOIDREASON"] = sSys_001;
        note_info["VOID_COMMENT"] = "";
        note_info["NOTE_USER_ID"] = "SYSTEM";
        note_info["NOTE_DATETIME"] = ""; // HX: it will be set in c# function
        args["note_info"] = note_info;
        args["db_connection"] = c_CASL.c_object("TranSuite_DB.Data.db_connection_info") as GenericObject;
          VoidNotes.WriteNotesRecord(args);
      }
      }
      //#endregion
      //#region Mark File Active in DB
      fileRecord.remove("BALDT");
      fileRecord.set("BALTOTAL", 0);
      fileRecord.remove("BAL_USERID");
      sql_args["checksum"] = CreateFileChecksum(fileRecord);
      sql_statement = "UPDATE TG_DEPFILE_DATA SET BALDT=NULL, BALTOTAL=0, BAL_USERID=NULL, CHECKSUM=@checksum " +
        " WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq";
      reader.AddCommand(sql_statement, CommandType.Text, sql_args);
      bool success = reader.EstablishTransaction(ref error_message);
      if (!success || error_message != "")
      {
        string sub_error_message = "";
        reader.RollbackTransaction(ref sub_error_message);
        if (sub_error_message != "")
        {
          error_message += sub_error_message;
        }
        //bug 11329 nj-used this method to show as user friendly method
        return CreateErrorObject("TS-1252", error_message, false, "Activate File Error", true);
      }
      //#endregion
      //#region Execute, Commit and unlock file
      string execute_message = "";
      int row_count = 0;
      if (!reader.ExecuteTransactionCommands(ref execute_message, ref row_count, false))
      {
        //bug 11329 nj-used this method to show as user friendly method
        GenericObject return_object = CreateErrorObject("TS-1251", "Could not execute transaction.", false, "Activate File Error", true); //misc.MakeCASLError("Activate File Error", "TS-1251", "Could not execute transaction.");
        // Try to rollback
        bool rollback_successful = reader.RollbackTransaction(ref error_message);
        string rollback_error_message = error_message;
        error_message = "";
        if (!rollback_successful)
        {
          return_object.set("rollback_error", misc.MakeCASLError("Activate File Error", "TS-1249", "Could not rollback transaction."));
          return_object.set("rollback_error_message", rollback_error_message);
        }
        reader.CloseReader();
        return return_object;
      }
      if (!reader.CommitTransaction(ref error_message) || error_message != "")
      {
        reader.CloseReader();
        GenericObject return_object = misc.MakeCASLError("Activate File Error", "TS-1250", "Could not commit transaction.");
        return_object.set("commit_error_message", error_message);
        return return_object;
      }
      if (!ReleaseFile(data, filenbr, fileseq, ref error_message) || error_message != "")
      {
        //Bug 11329 NJ-split the error so that the db error will not be shown when show verbose is turned off
          //13832-split into friendly and verbose errors
        GenericObject return_object = CreateErrorObject("TS-1251", "Error releasing file:  ", false, "Activate File Error", true);
        return_object.set("inner_error", misc.MakeCASLError("", "", error_message));
        return return_object;//misc.MakeCASLError("Activate File Error","TS-1251","Error releasing file: " + error_message);
      }
      //#endregion
      return true;

    }

    //       1         2         3         4         5         6         7         8         9
    //3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
    public static bool MarkFileActive(params Object[] arg_pairs)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      // It is possible topass in a database connection using the "DbConnectionClass" key.
      // Connection must be of type ConnectionTypeSynch class.

      // Bug 15941. FT. Make sure the database connection is closed
      ConnectionTypeSynch pDBConn = null;
      bool connectionOpened = false;      // Bug 15941
      GenericObject args = misc.convert_args(arg_pairs);

      // Sync user's access to this resource. Bug# 5813 deadlock fix. DH 07/30/2008
      ThreadLock.GetInstance().CreateLock();
      // Bug 11661: Wrap mutex release in finally block
      try
      {
        string strFileName = (string)args.get("FILENAME");
        string strDEPTID = (string)args.get("DEPTID");
        //#region BUG 6170 ann - change from config to data database.                                       
        //TODO: Rename references to config to data in this function
        //GenericObject config      = args.get("config", c_CASL.c_undefined) as GenericObject;
        GenericObject config = args.get("data", c_CASL.c_undefined) as GenericObject;
        //#endregion
        DateTime dtDate = DateTime.Now;
        string strNowDateTime = dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo);
        pDBConn = new ConnectionTypeSynch(config);
        string strErr = "";
        string strUserID = "SYSTEM";

        if (strFileName.Length == 0 || strDEPTID.Length == 0)
          // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
          HandleErrorsAndLocking("message",
            "Required information missing in Change Event Status!", "code", "TS-861", "");

        //***************************************
        if (args.has("DbConnectionClass"))
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
        else
        {
          pDBConn = new ConnectionTypeSynch(config);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strErr)))  // Bug 15941. Open connection and set connectionO flagpened
            // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
            HandleErrorsAndLocking("message",
              "Error opening database connection.", "code", "TS-862", strErr);

          if (!pDBConn.EstablishRollback(ref strErr))
            ThrowCASLErrorButDoNotCloseDBConnection("TS-910", ref pDBConn,
              "Error creating database transaction.", strErr);
        }
        //***************************************

        // Break the file into file number and sequence number
      // Bug #12055 Mike O - Use misc.Parse to log errors
      string strFileNumber = misc.Parse<Int64>(GetFileNumber(strFileName)).ToString();
      string strFileSequenceNbr = misc.Parse<Int64>(GetFileSequence(strFileName)).ToString();

        //***************************************
        // Make sure the files is not already active.
        if (IsFileActive(strFileNumber, strFileSequenceNbr, config, ref pDBConn))
          ThrowCASLErrorButDoNotCloseDBConnection("TS-911", ref pDBConn,
            "Transaction File " + strFileName + " is already Active!", "");
        //***************************************

        //***************************************
        // Make sure file exists, is balanced, and has NOT been accepted into One-Step
        string strSQL = "SELECT * FROM TG_DEPFILE_DATA WHERE ";
      // Bug #12055 Mike O - Use misc.Parse to log errors
      strSQL += "DEPFILENBR=" + misc.Parse<Int64>(strFileNumber) +
        " AND DEPFILESEQ=" + misc.Parse<Int64>(strFileSequenceNbr) +
          " AND BALDT IS NOT NULL";
        GenericObject geoNotBalancedRecords = GetRecords(config, 0, strSQL,
          pDBConn.GetDBConnectionType());
        if (geoNotBalancedRecords == null)
        {
          ThrowCASLErrorButDoNotCloseDBConnection("TS-912", ref pDBConn,
            "Unable to Reset to Active. Transaction File " + strFileName +
            " is not a currently Balanced file.", "");
        }
        else
        {
          string strACCDT = GetKeyData(geoNotBalancedRecords, "ACCDT", false);
          string strUPDATEDT = GetKeyData(geoNotBalancedRecords, "UPDATEDT", false);
          // Matt: changed accepted to updated per request of Emilio
          // BUG#5530 add accepted in message
          if (strACCDT.Length != 0 || strUPDATEDT.Length != 0)
            ThrowCASLErrorButDoNotCloseDBConnection("TS-913", ref pDBConn, "Transaction File " + strFileName
              + " has already been updated or accepted.  You may not access this File.", "");
        }
        //***************************************

        //***************************************
        // Void any Over/Short record associated with this file
        // Status "5" is any non-voided System Completed List
        strSQL = "SELECT * FROM TG_PAYEVENT_DATA WHERE ";
      // Bug #12055 Mike O - Use misc.Parse to log errors
      strSQL += "DEPFILENBR=" + misc.Parse<Int64>(strFileNumber) +
        " AND DEPFILESEQ=" + misc.Parse<Int64>(strFileSequenceNbr) + " AND STATUS='5'";
        GenericObject geoTG_PAYEVENT_DATA = GetRecords(config, 1, strSQL,
          pDBConn.GetDBConnectionType());
        if (geoTG_PAYEVENT_DATA != null)
        {
          // Iterate thru all events.
          int iCount = geoTG_PAYEVENT_DATA.getIntKeyLength();
          if (iCount >= 1)
          {
            for (int i = 0; i < iCount; i++)
            {
              GenericObject geoPayEvent = (GenericObject)geoTG_PAYEVENT_DATA.get(i);
              string strEventNbr = GetKeyData(geoPayEvent, "EVENTNBR", true);

              // This is needed for old validations
              string strOldChecksum = GetKeyData(geoPayEvent, "CHECKSUM", true);
              string strOldUserID = GetKeyData(geoPayEvent, "USERID", true);
              string strOldStatus = GetKeyData(geoPayEvent, "STATUS", true);
              string strOldDate = GetKeyData(geoPayEvent, "MOD_DT", true);
            // Bug #12055 Mike O - Use misc.Parse to log errors
            DateTime dtMOD_DT = misc.Parse<DateTime>(strOldDate);
              strOldDate = dtMOD_DT.ToString("G", DateTimeFormatInfo.InvariantInfo);

              //-------------------------------
              // Void all transactions (not previously voided)
              strSQL = "SELECT * FROM TG_TRAN_DATA WHERE ";
            // Bug #12055 Mike O - Use misc.Parse to log errors
            strSQL += "DEPFILENBR=" + misc.Parse<Int64>(strFileNumber) +
              " AND DEPFILESEQ=" + misc.Parse<Int64>(strFileSequenceNbr) +
              " AND EVENTNBR=" + misc.Parse<Int64>(strEventNbr) + " AND VOIDDT IS NULL";
              GenericObject geoTG_TRAN_DATA = GetRecords(config, 1, strSQL,
                pDBConn.GetDBConnectionType());
              int iCnt = 0;
              if (geoTG_TRAN_DATA != null)
              {
                iCnt = geoTG_TRAN_DATA.getIntKeyLength();

                if (iCnt >= 1)
                {
                  // Iterate thru all transactions and void them.
                  for (int j = 0; j < iCnt; j++)
                  {
                    GenericObject geoTran = (GenericObject)geoTG_TRAN_DATA.get(j);
                    string strTranNbr = GetKeyData(geoTran, "TRANNBR", true);

                  // Bug #12055 Mike O - Use misc.Parse to log errors
                    if (!VoidTransaction("strTranNbr", strTranNbr,
                    "strEventNbr", misc.Parse<Int64>(strEventNbr), 
                      "strFileName", strFileName,
                      "strVoidDT", strNowDateTime,
                      "strVoidUserID", strUserID,
                      "DbConnectionClass", pDBConn))
                    {
                      ThrowCASLErrorButDoNotCloseDBConnection("TS-914", ref pDBConn,
                        "Unable to void a transaction. File: " + strFileName +
                        " Event: " + strEventNbr + " TrannNbr: " + strTranNbr,
                        " Error: " + strErr);
                    }
                  }
                }

                // Void all tender
                strSQL = "SELECT * FROM TG_TENDER_DATA WHERE ";
              // Bug #12055 Mike O - Use misc.Parse to log errors
              strSQL += "DEPFILENBR=" + misc.Parse<Int64>(strFileNumber) +
                " AND DEPFILESEQ=" + misc.Parse<Int64>(strFileSequenceNbr) +
                " AND EVENTNBR=" + misc.Parse<Int64>(strEventNbr) +
                  " AND VOIDDT IS NULL";
                GenericObject geoTG_TENDER_DATA = GetRecords(config, 1, strSQL,
                  pDBConn.GetDBConnectionType());
                if (geoTG_TENDER_DATA != null)
                {
                  iCnt = geoTG_TENDER_DATA.getIntKeyLength();
                  if (iCnt >= 1)
                  {
                    // Iterate thru all tenders and void them.
                    for (int j = 0; j < iCnt; j++)
                    {
                      GenericObject geoTender = (GenericObject)geoTG_TENDER_DATA.get(j);
                      string strTenderNbr = GetKeyData(geoTender, "TNDRNBR", true);

                    // Bug #12055 Mike O - Use misc.Parse to log errors
                      if (!VoidTender("strTndrNbr", strTenderNbr,
                      "strEventNbr", misc.Parse<Int64>(strEventNbr), 
                        "strFileName", strFileName,
                        "strVoidDT", strNowDateTime,
                        "strVoidUserID", strUserID,
                        "DbConnectionClass", pDBConn))
                      {
                        ThrowCASLErrorButDoNotCloseDBConnection("TS-915", ref pDBConn,
                          "Unable to void a tender. File: " + strFileName +
                          " Event: " + strEventNbr + " TrannNbr: " + strTenderNbr,
                          " Error: " + strErr);
                      }
                    }
                  }
                }
              }

              //--------------------------------------
              // Validate PayEvent Checksum

              // Matt -- replacing old function call
              GenericObject gTemp = new GenericObject("USERID", strOldUserID,
                "STATUS", strOldStatus, "MOD_DT", strOldDate, "CHECKSUM", strOldChecksum);

              //TODO: Add checksum validation here
              /* NOT YET READY FOR CHECKSUM HERE WILL ADDRESS THIS LATER WHEN
                 WE CONVERT CHECKSUM FUNCTIONALITY TO MIKE'S NEW CODE. */
              /*
              if(!ValidatePayEventChecksum("strEventNbr", strEventNbr,
                "strFileName", strFileName, "geoChecksumData", gTemp))
                ThrowCASLErrorAndCloseDB("TS-916", ref pDBConn, "Checksum failed for: " +
                  "File: " + strFileName + " Event Number " + strEventNbr + " " + strErr);
              */

              //--------------------------------------
              // Set event status to active
              if (!ModifyPayEventStatus(ref pDBConn, strEventNbr, strFileName, "6",
                strNowDateTime, ref strErr))
                ThrowCASLErrorButDoNotCloseDBConnection("TS-918", ref pDBConn,
                  "Unable to modify event status for: " + "File: " + strFileName +
                  " Event Number " + strEventNbr + " ", strErr);
              //--------------------------------------

              //--------------------------------------
              // Update PayEvent checksum.
              //Matt -- replacing old update function call
              gTemp = new GenericObject("USERID", strUserID, "STATUS", 6,
                "MOD_DT", strNowDateTime);
              if (!UpdatePayEventChecksum("DbConnectionClass", pDBConn, "strEventNbr",
                strEventNbr, "strFileName", strFileName, "geoChecksumData", gTemp))
                ThrowCASLErrorButDoNotCloseDBConnection("TS-919", ref pDBConn,
                  "Unable to set checksum for: " + "File: " + strFileName +
                  " Event Number " + strEventNbr + " ", strErr);
            }
          }
        }
        //***************************************

        //***************************************
        // Matt -- replacing old checksum call		
        if (!UpdateTranFileChecksum("DbConnectionClass", pDBConn, "strFileName", strFileName))
          ThrowCASLErrorButDoNotCloseDBConnection("TS-922", ref pDBConn,
            "Unable to reset file to active: " + "File: " + strFileName + " ", strErr);
        //***************************************

        //***************************************
        // Remove balanced date from TG_DEPFILE_DATA so file becomes active.
        strSQL = "UPDATE TG_DEPFILE_DATA SET BALDT=NULL, " +
          "BALTOTAL=0, BAL_USERID=NULL WHERE DEPFILENBR=" + strFileNumber +
          " AND DEPFILESEQ=" + strFileSequenceNbr;
        if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strErr))
          ThrowCASLErrorButDoNotCloseDBConnection("TS-921", ref pDBConn,
            "Unable to reset file to active: " + "File: " + strFileName + " ", strErr);
        //***************************************


        //***************************************
        // BUG#5396
        //Remove closed date from TG_DEPFILE_DATA so file becomes unlocked/unlocked.
        strSQL = "UPDATE TG_DEPFILE_DATA SET CLOSEDT=NULL, " +
          "CLOSE_USERID=NULL WHERE DEPFILENBR=" +
          strFileNumber + " AND DEPFILESEQ=" + strFileSequenceNbr;
        if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strErr))
          ThrowCASLErrorButDoNotCloseDBConnection("TS-921", ref pDBConn,
            "Unable to reset file to active: " + "File: " + strFileName + " ", strErr);
        //***************************************

        //***************************************
        // ALL GOOD - ACCEPT THE UPDATES.
        if (!args.has("DbConnectionClass"))
        {
          strErr = "";
          if (!pDBConn.CommitToDatabase(ref strErr))
            ThrowCASLErrorButDoNotCloseDBConnection("TS-923", ref pDBConn,
              "Unable to commit changes to database.", strErr);
        }
        //***************************************   

      }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
        // Bug 11661
        ThreadLock.GetInstance().ReleaseLock();//Bug# 5813 deadlock fix. DH 07/30/2008
      }

      return true;
    }

    public static Object MarkFileClosed(params Object[] arg_pairs)
    {
      // BUG#5396 
      GenericObject args = misc.convert_args(arg_pairs);
      string strFileName = (string)args.get("FILENAME");
      GenericObject config = args.get("data", c_CASL.c_undefined) as GenericObject; //replaced "config" to "data" bug 6259
      string strCloseUserID = (string)args.get("CLOSE_USERID");
      string strCloseDate = "";
      DateTime dtDate = DateTime.Now;
      string strNowDateTime = dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo);
      ConnectionTypeSynch pDBConn = new ConnectionTypeSynch(config);
      bool connectionOpened = false;      // Bug 15941
      try
      {
      string strErr = "";

      if (strFileName.Length == 0 || strCloseUserID.Length == 0)
        HandleErrorsAndLocking("message", "Required information missing in Change File Status!", "code", "TS-1202", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

      //***************************************
      if (args.has("DbConnectionClass"))
        pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
      else
      {
        pDBConn = new ConnectionTypeSynch(config);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strErr)))  // Bug 15941. Open connection and set connectionO flagpened
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-862", strErr);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        if (!pDBConn.EstablishRollback(ref strErr))
            ThrowCASLErrorButDoNotCloseDBConnection("TS-910", ref pDBConn, "Error creating database transaction.", strErr);
      }
      //***************************************

      // Break the file into file number and sequence number
      // Bug #12055 Mike O - Use misc.Parse to log errors
      string strFileNumber = misc.Parse<Int64>(GetFileNumber(strFileName)).ToString();
      string strFileSequenceNbr = misc.Parse<Int64>(GetFileSequence(strFileName)).ToString();

      //***************************************
      // Make sure the files is not already active.

      if (!IsFileActive(strFileNumber, strFileSequenceNbr, config, ref pDBConn))
          ThrowCASLErrorButDoNotCloseDBConnection("TS-1200", ref pDBConn, "Transaction File " + strFileName + " is not Active!", "");
      //***************************************

      //***************************************
      // Mark the File as Closed with the CLOSE_USERID, CLOSEDT columns

      //ORACLE - Close Date 
      int iDBType = pDBConn.GetDBConnectionType();
      string strBalanceDate = strNowDateTime;
      if (iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_ODBC || iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_TNS)
        strCloseDate = "TO_DATE('" + strNowDateTime + "','MM/DD/YYYY HH24:MI:SS') ";
      else//MSACCESS and SQL Server 
      {
        strCloseDate = "'" + strNowDateTime + "' ";
      }

      // Bug #12055 Mike O - Use misc.Parse to log errors
      string strSQL = string.Format("UPDATE TG_DEPFILE_DATA SET CLOSEDT={0}, CLOSE_USERID='{1}' WHERE DEPFILENBR={2} AND DEPFILESEQ={3}",strCloseDate, strCloseUserID, misc.Parse<Int64>(strFileNumber), misc.Parse<Int64>(strFileSequenceNbr));
      if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strErr))
          ThrowCASLErrorButDoNotCloseDBConnection("TS-1201", ref pDBConn, "Unable to set file to locked. " + "File: " + strFileName + " ", strErr + "SQL Statement:" + strSQL);
      //***************************************

      //IGNORE THE CHECKSUM FOR CLOSEDT,CLOSE_USERID

      //***************************************
      // ALL GOOD - ACCEPT THE UPDATES.
      if (!args.has("DbConnectionClass"))
      {
        strErr = "";
        if (!pDBConn.CommitToDatabase(ref strErr))
            ThrowCASLErrorButDoNotCloseDBConnection("TS-923", ref pDBConn, "Unable to commit changes to database.", strErr);
      }
      //***************************************   
      return true;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static Object MarkFileUnclosed(params Object[] arg_pairs)
    {
      // BUG#5396 
      GenericObject args = misc.convert_args(arg_pairs);
      string strFileName = (string)args.get("FILENAME");
      GenericObject config = args.get("data", c_CASL.c_undefined) as GenericObject; //bug 6259 changing "config" to "data"
      ConnectionTypeSynch pDBConn = null;
      bool connectionOpened = false;      // Bug 15941
      try
      {
        pDBConn = new ConnectionTypeSynch(config);
      string strErr = "";

      if (strFileName.Length == 0)
        HandleErrorsAndLocking("message", "Required information missing in Change File Status!", "code", "TS-1202", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

      //***************************************
      if (args.has("DbConnectionClass"))
        pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
      else
      {
        pDBConn = new ConnectionTypeSynch(config);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strErr)))  // Bug 15941. Open connection and set connectionO flagpened
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-862", strErr);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        if (!pDBConn.EstablishRollback(ref strErr))
            ThrowCASLErrorButDoNotCloseDBConnection("TS-910", ref pDBConn, "Error creating database transaction.", strErr);
      }
      //***************************************

      // Break the file into file number and sequence number
      // Bug #12055 Mike O - Use misc.Parse to log errors
      string strFileNumber = misc.Parse<Int64>(GetFileNumber(strFileName)).ToString();
      string strFileSequenceNbr = misc.Parse<Int64>(GetFileSequence(strFileName)).ToString();

      //***************************************
      // Mark the File as unclosed - set CLOSE_USERID, CLOSEDT columns to NULL
      // Bug #12055 Mike O - Use misc.Parse to log errors
      string strSQL = string.Format("UPDATE TG_DEPFILE_DATA SET CLOSEDT=NULL, CLOSE_USERID=NULL WHERE DEPFILENBR={0} AND DEPFILESEQ={1}",misc.Parse<Int64>(strFileNumber), misc.Parse<Int64>(strFileSequenceNbr));
      if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strErr))
          ThrowCASLErrorButDoNotCloseDBConnection("TS-1203", ref pDBConn, "Unable to set file to unclosed. " + "File: " + strFileName + " ", strErr + "SQL Statement:" + strSQL);
      //***************************************

      //IGNORE THE CHECKSUM FOR CLOSEDT,CLOSE_USERID

      //***************************************
      // ALL GOOD - ACCEPT THE UPDATES.
      if (!args.has("DbConnectionClass"))
      {
        strErr = "";
        if (!pDBConn.CommitToDatabase(ref strErr))
            ThrowCASLErrorButDoNotCloseDBConnection("TS-923", ref pDBConn, "Unable to commit changes to database.", strErr);
      }
      //***************************************   

      return true;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static Object MarkFileAccepted(params Object[] arg_pairs)
    {
      // Bug 15941. FT. Make sure the database connection is closed
      ConnectionTypeSynch pDBConn = null;
      bool connectionOpened = false;      // Bug 15941
      GenericObject args = misc.convert_args(arg_pairs);

      ThreadLock.GetInstance().CreateLock();// Sync user's access to this resource. Bug# 5813 deadlock fix. DH 07/30/2008
      // Bug 11661: Wrap mutex release in finally block
      try
      {
        // BUG#5530
        string strFileName = (string)args.get("FILENAME");
        //BUG6168 ann GenericObject config      = args.get("config", c_CASL.c_undefined) as GenericObject;
        GenericObject config = args.get("data", c_CASL.c_undefined) as GenericObject;//BUG6168 ann point to data database
        string strAcceptUserID = (string)args.get("ACC_USERID");
        string strLoginID = (string)args.get("LOGIN_ID");
        string strAcceptDate = "";
        DateTime dtDate = DateTime.Now;
        string strNowDateTime = dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo);
        pDBConn = new ConnectionTypeSynch(config);
        string strErr = "";

        // Bug #10646 Mike O - New columns for deposit transfer feature
        String acceptFileNbr = (String)args.get("ACC_FILENBR", "");
        String acceptFileSeq = (String)args.get("ACC_FILESEQ", "");
        Decimal acceptTotal = Convert.ToDecimal(args.get("ACC_TOTAL", 0.00));

        if (acceptFileNbr.Equals(""))
          acceptFileNbr = null;
        if (acceptFileSeq.Equals(""))
          acceptFileSeq = null;
        // End Bug #10646 Mike O

        if (strFileName.Length == 0 || strAcceptUserID.Length == 0)
          HandleErrorsAndLocking("message", "Required information missing in Change File Status!", "code", "TS-1202", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        //***************************************
        if (args.has("DbConnectionClass"))
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
        else
        {
          pDBConn = new ConnectionTypeSynch(config);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strErr)))  // Bug 15941. Open connection and set connectionO flagpened
              HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-862", strErr);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

          if (!pDBConn.EstablishRollback(ref strErr))
            ThrowCASLErrorButDoNotCloseDBConnection("TS-910", ref pDBConn, "Error creating database transaction.", strErr);
        }
        //***************************************

        // Break the file into file number and sequence number
      // Bug #12055 Mike O - Use misc.Parse to log errors
      string strFileNumber = misc.Parse<Int64>(GetFileNumber(strFileName)).ToString();
      string strFileSequenceNbr = misc.Parse<Int64>(GetFileSequence(strFileName)).ToString();

        //***************************************
        // Make sure the files is not already active.
        if (!IsFileBalanced(strFileNumber, strFileSequenceNbr, config, ref pDBConn))
          ThrowCASLErrorButDoNotCloseDBConnection("TS-1210", ref pDBConn, "Transaction File " + strFileName + " is not Balanced!", "");
        //***************************************

        //***************************************
        // Mark the File as Closed with the CLOSE_USERID, CLOSEDT columns

        //ORACLE - Close Date 
        int iDBType = pDBConn.GetDBConnectionType();
        string strBalanceDate = strNowDateTime;
        if (iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_ODBC || iDBType == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_TNS)
          strAcceptDate = "TO_DATE('" + strNowDateTime + "','MM/DD/YYYY HH24:MI:SS') ";
        else//MSACCESS and SQL Server 
        {
          strAcceptDate = "'" + strNowDateTime + "' ";
        }

        //Mark all Deposits (except voided or predeposit) as accepted
      // Bug #12055 Mike O - Use misc.Parse to log errors
      string strSQL = string.Format("UPDATE TG_DEPOSIT_DATA SET ACCDT={0}, ACC_USERID='{1}' WHERE DEPFILENBR={2} AND DEPFILESEQ={3} AND DEPOSITTYPE!='PRE_DEPOSIT' AND VOIDDT IS NULL AND ACCDT IS NULL",strAcceptDate, strAcceptUserID, misc.Parse<Int64>(strFileNumber), misc.Parse<Int64>(strFileSequenceNbr));
        if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strErr))
          ThrowCASLErrorButDoNotCloseDBConnection("TS-1211", ref pDBConn, "Unable to set file to Accepted. " + "File: " + strFileName + " ", strErr + "SQL Statement:" + strSQL);
        //***************************************

      // Bug #12055 Mike O - Use misc.Parse to log errors
      strSQL = string.Format("UPDATE TG_DEPFILE_DATA SET ACCDT={0}, ACC_USERID='{1}' WHERE DEPFILENBR={2} AND DEPFILESEQ={3}",strAcceptDate, strAcceptUserID, misc.Parse<Int64>(strFileNumber), misc.Parse<Int64>(strFileSequenceNbr));
        if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strErr))
          ThrowCASLErrorButDoNotCloseDBConnection("TS-1211", ref pDBConn, "Unable to set file to Accepted. " + "File: " + strFileName + " ", strErr + "SQL Statement:" + strSQL);
        //***************************************

        // Bug #10646 Mike O - If doing a deposit transfer, store the destination info
        if (acceptFileNbr != null && acceptFileSeq != null)
        {
          strSQL = string.Format("UPDATE TG_DEPFILE_DATA SET ACC_FILENBR={0}, ACC_FILESEQ='{1}', ACC_TOTAL='{2}' WHERE DEPFILENBR={3} AND DEPFILESEQ={4}", Int64.Parse(acceptFileNbr), Int64.Parse(acceptFileSeq), acceptTotal, Int64.Parse(strFileNumber), Int64.Parse(strFileSequenceNbr));
          if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strErr))
            ThrowCASLErrorButDoNotCloseDBConnection("TS-1211", ref pDBConn, "Unable to set file to Accepted. " + "File: " + strFileName + " ", strErr + "SQL Statement:" + strSQL);
        }
        //***************************************

        //***************************************
        // ALL GOOD - ACCEPT THE UPDATES.
        if (!args.has("DbConnectionClass"))
        {
          strErr = "";
          if (!pDBConn.CommitToDatabase(ref strErr))
            ThrowCASLErrorButDoNotCloseDBConnection("TS-923", ref pDBConn, "Unable to commit changes to database.", strErr);
        }
        //***************************************   

      }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
        // Bug 11661
        ThreadLock.GetInstance().ReleaseLock(); //Bug# 5813 deadlock fix. DH 07/30/2008
      }

      return true;
    }

    public static Object MarkFileUnaccepted(params Object[] arg_pairs)
    {
      // Bug 15941. FT. Make sure the database connection is closed
      GenericObject args = misc.convert_args(arg_pairs);
      ConnectionTypeSynch pDBConn = null;
      bool connectionOpened = false;        // Bug 15941
      ThreadLock.GetInstance().CreateLock();
      // Bug 11661: Wrap mutex release in finally block
      try
      {
        // BUG#5530
        string strFileName = (string)args.get("FILENAME");
        GenericObject config = args.get("config", c_CASL.c_undefined) as GenericObject;
        GenericObject data = args.get("data", c_CASL.c_undefined) as GenericObject;
        pDBConn = new ConnectionTypeSynch(config);
        string strErr = "";

        if (strFileName.Length == 0)
          HandleErrorsAndLocking("message", "Required information missing in Change File Status!", "code", "TS-1202", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        //***************************************
        if (args.has("DbConnectionClass"))
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
        else
        {
          pDBConn = new ConnectionTypeSynch(data);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strErr)))  // Bug 15941. Open connection and set connectionO flagpened
              HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-862", strErr);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

          if (!pDBConn.EstablishRollback(ref strErr))
            ThrowCASLErrorButDoNotCloseDBConnection("TS-910", ref pDBConn, "Error creating database transaction.", strErr);
        }
        //***************************************

        // Break the file into file number and sequence number
      // Bug #12055 Mike O - Use misc.Parse to log errors
      string strFileNumber = misc.Parse<Int64>(GetFileNumber(strFileName)).ToString();
      string strFileSequenceNbr = misc.Parse<Int64>(GetFileSequence(strFileName)).ToString();

        //***************************************
        //Mark all Deposits (except voided or predeposit) as unaccepted
      // Bug #12055 Mike O - Use misc.Parse to log errors
      string strSQL = string.Format("UPDATE TG_DEPOSIT_DATA SET ACCDT=NULL, ACC_USERID=NULL WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND NOT(ACCDT IS NULL)", misc.Parse<Int64>(strFileNumber), misc.Parse<Int64>(strFileSequenceNbr));
        if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strErr))
          ThrowCASLErrorButDoNotCloseDBConnection("TS-1211", ref pDBConn, "Unable to set file to Accepted. " + "File: " + strFileName + " ", strErr + "SQL Statement:" + strSQL);


        //***************************************
        // Mark the File as unaccepted - set ACC_USERID, ACCDT columns to NULL
        // Bug #10646 Mike O - Nullify the deposit transfer columns as well
        //Bug #12055 Mike O - Use misc.Parse to log errors
      strSQL = string.Format("UPDATE TG_DEPFILE_DATA SET ACCDT=NULL, ACC_USERID=NULL, ACC_FILENBR=NULL, ACC_FILESEQ=NULL, ACC_TOTAL=NULL WHERE DEPFILENBR={0} AND DEPFILESEQ={1}", misc.Parse<Int64>(strFileNumber), misc.Parse<Int64>(strFileSequenceNbr));
        if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strErr))
          ThrowCASLErrorButDoNotCloseDBConnection("TS-1213", ref pDBConn, "Unable to set file to unaccepted. " + "File: " + strFileName + " ", strErr + "SQL Statement:" + strSQL);
        //***************************************


        //***************************************
        // ALL GOOD - ACCEPT THE UPDATES.
        if (!args.has("DbConnectionClass"))
        {
          strErr = "";
          if (!pDBConn.CommitToDatabase(ref strErr))
            ThrowCASLErrorButDoNotCloseDBConnection("TS-923", ref pDBConn, "Unable to commit changes to database.", strErr);
        }
        //***************************************   

      }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);

        // Bug 11661
        ThreadLock.GetInstance().ReleaseLock();
      }

      return true;
    }
    // Called by: MarkFileBalanced, MarkFileActive, ChangeEventStatus
    public static bool ModifyPayEventStatus(ref ConnectionTypeSynch pDBConn, string strEventNbr, string strFileName, string strStatus, string strNow, ref string strErr)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!



      // Break the file into file number and sequence number
      // Bug 18766: Parameterized.  Got rid of pointless conversion here.  These data types are int's in the db
      string strFileNumber = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      string sql_string = "";
      try
      {
        // Bug 18766: Parameterized
        sql_string =
          @"UPDATE TG_PAYEVENT_DATA WITH (ROWLOCK) SET STATUS=@status, MOD_DT=@mod_dt 
            WHERE EVENTNBR=@eventnbr AND DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq";
        HybridDictionary sql_args = new HybridDictionary();
        sql_args["status"] = strStatus;
        sql_args["mod_dt"] = strNow;
        sql_args["eventnbr"] = strEventNbr;
        sql_args["depfilenbr"] = strFileNumber;
        sql_args["depfileseq"] = strFileSequenceNbr;

        // Bug 25654: Changed from runExecuteNonQueryCommitRollback: need to do commit/rollback at a higher level
        int count = ParamUtils.runExecuteNonQuery(pDBConn, sql_string, sql_args);

        if (count != 1)
        {
          string msg = "Error: SQL Update failed: only updated " + count + " records instead of one";
          Logger.Log(msg, "Transuite: ModifyPayEventStatus", "err");
          HandleErrorsAndLocking("message", "Error executing SQL.", "code", "TS-ModifyPayEventStatus-null update", "Wrong update count");
          return false;
        }

      }
      catch (Exception e)
      {
        string msg = "Error: this SQL failed: " + sql_string + ". Error: " + e.ToString();
        Logger.Log(msg, "Transuite: ModifyPayEventStatus", "err");
        HandleErrorsAndLocking("message", "Error executing SQL.", "code", "TS-ModifyPayEventStatus", e.Message);
        return false;
      }

      return true;
    }

    /// <summary>
    /// Determine if file is active.
    /// Bug 18766: Parameterized
    /// </summary>
    /// <param name="strFileNumber"></param>
    /// <param name="strFileSequenceNbr"></param>
    /// <param name="data"></param>
    /// <param name="pDBConn"></param>
    /// <returns></returns>
    public static bool IsFileActive(string strFileNumber, string strFileSequenceNbr, GenericObject data, ref ConnectionTypeSynch pDBConn)
    {
      try
      {
        string sql_string =
          @"SELECT COUNT(*) AS COUNT FROM TG_DEPFILE_DATA 
               WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND BALDT IS NULL";

        HybridDictionary sql_args = new HybridDictionary();
        sql_args["depfilenbr"] = strFileNumber;
        sql_args["depfileseq"] = strFileSequenceNbr;

        string strErr = "";     // TODO: check this value
        using (SqlCommand paramCommand = ParamUtils.CreateParamCommand(pDBConn, sql_string, sql_args))
        {
          return ParamUtils.isCountOneorMore(pDBConn, paramCommand, ref strErr);
        }

      }
      catch (Exception e)
      {
        HandleErrorsAndLocking("message", "IsFileActive: Error executing SQL.", "code", "TS-805", e.ToString());
        return false;
      }

    }

    /// <summary>
    /// Bug 25598 DJD: Deposit transfer source file state change to 'Unaccepted' should be prevented
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static object FileDepositsTransferred(params Object[] args)
    {
      GenericObject geoArgs = misc.convert_args(args);
      string coreFileID = geoArgs.get("core_file_id", "") as string;
      string retVal = "false";
      if (FileDepositsTransferred_aux(coreFileID))
      {
        retVal = "true";
      }
      return retVal;
    }

    /// <summary>
    /// Bug 25598 DJD: Deposit transfer source file state change to 'Unaccepted' should be prevented
    /// </summary>
    /// <param name="coreFileID"></param>
    /// <returns></returns>
    public static bool FileDepositsTransferred_aux(string coreFileID)
    {
      bool retVal = false;
      ConnectionTypeSynch pDBConn = null;
      string errMsg = "";
      try
      {
        if (!string.IsNullOrWhiteSpace(coreFileID) && coreFileID.Length > 9)
        {
          string fileNumber = coreFileID.Substring(0, 7);
          string seqNumber = coreFileID.Substring(7);
          int fileNbr = Convert.ToInt32(fileNumber);
          int seqNbr = Convert.ToInt32(seqNumber);

          GenericObject dataDB = c_CASL.c_object("TranSuite_DB.Data.db_connection_info") as GenericObject;
          pDBConn = new ConnectionTypeSynch(dataDB);
          if (!pDBConn.EstablishDatabaseConnection(ref errMsg))
          {
            HandleErrorsAndLocking("message", "FileDepositsTransferred_aux: Error opening database connection.", "code", "TS-821", errMsg);
            return false;
          }
          if (!pDBConn.EstablishRollback(ref errMsg))
          {
            HandleErrorsAndLocking("message", "FileDepositsTransferred_aux: Error creating database transaction.", "code", "TS-822", errMsg);
            return false;
          }

          string sql = @"SELECT COUNT(*) AS COUNT FROM TG_DEPFILE_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND ACC_FILENBR IS NOT NULL";
          HybridDictionary sqlArgs = new HybridDictionary();
          sqlArgs["depfilenbr"] = fileNbr;
          sqlArgs["depfileseq"] = seqNbr;

          using (SqlCommand paramCommand = ParamUtils.CreateParamCommand(pDBConn, sql, sqlArgs))
          {
            retVal = ParamUtils.isCountOneorMore(pDBConn, paramCommand, ref errMsg);
          }
          if (!string.IsNullOrWhiteSpace(errMsg))
          {
            Logger.cs_log(string.Format("Error Checking If File Deposits Are Transferred: {0}", errMsg));
          }
        }
        else
        {
          Logger.cs_log(string.Format("FileDepositsTransferred_aux: Missing or wrong format of CORE File ID. {0}", coreFileID));
        }
      }
      catch (Exception e)
      {
        HandleErrorsAndLocking("message", "FileDepositsTransferred_aux: Error executing SQL.", "code", "TS-805", e.ToString());
        return false;
      }
      finally
      {
        if (pDBConn != null)
        {
          CloseDBConnection(pDBConn);
        }
      }
      return retVal;
    }

    /// <summary>
    /// Determine if file is closed.
    /// Bug 18766: Parameterized
    /// </summary>
    /// <param name="strFileNumber"></param>
    /// <param name="strFileSequenceNbr"></param>
    /// <param name="config"></param>
    /// <param name="pDBConn"></param>
    /// <returns></returns>
    public static bool IsFileClosed(string strFileNumber, string strFileSequenceNbr, GenericObject config, ref ConnectionTypeSynch pDBConn)
    {
      // BUG#5396 
      try
      {
        string sql_string =
          @"SELECT COUNT(*) AS COUNT FROM TG_DEPFILE_DATA 
               WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND CLOSEDT IS NULL";

        HybridDictionary sql_args = new HybridDictionary();
        sql_args["depfilenbr"] = strFileNumber;
        sql_args["depfileseq"] = strFileSequenceNbr;

        string strErr = "";     // TODO: check this value
        using (SqlCommand paramCommand = ParamUtils.CreateParamCommand(pDBConn, sql_string, sql_args))
        {
          return ParamUtils.isCountOneorMore(pDBConn, paramCommand, ref strErr);
        }

      }
      catch (Exception e)
      {
        HandleErrorsAndLocking("message", "IsFileClosed: Error executing SQL.", "code", "TS-805", e.ToString());
        return false;
      }


    }

    /// <summary>
    /// Determine if file is balanced.
    /// Bug 18766: Parameterized
    /// </summary>
    /// <param name="strFileNumber"></param>
    /// <param name="strFileSequenceNbr"></param>
    /// <param name="config"></param>
    /// <param name="pDBConn"></param>
    /// <returns></returns>
    public static bool IsFileBalanced(string strFileNumber, string strFileSequenceNbr, GenericObject config, ref ConnectionTypeSynch pDBConn)
    {
      // BUG#5530 
      try
      {
        string sql_string =
          @"SELECT COUNT(*) AS COUNT FROM TG_DEPFILE_DATA 
               WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND NOT(BALDT IS NULL)";

        HybridDictionary sql_args = new HybridDictionary();
        sql_args["depfilenbr"] = strFileNumber;
        sql_args["depfileseq"] = strFileSequenceNbr;

        string strErr = "";     // TODO: check this value
        using (SqlCommand paramCommand = ParamUtils.CreateParamCommand(pDBConn, sql_string, sql_args))
        {
          return ParamUtils.isCountOneorMore(pDBConn, paramCommand, ref strErr);
        }

      }
      catch (Exception e)
      {
        HandleErrorsAndLocking("message", "IsFileBalanced: Error executing SQL.", "code", "TS-805", e.ToString());
        return false;
      }

      //return true;// Bug# 27153 DH - Clean up warnings when building ipayment
    }

    public static object ActivateSuspendedEvent(params Object[] arg_pairs)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = args.get("config", c_CASL.c_undefined) as GenericObject;
      GenericObject data = args.get("data") as GenericObject;
      string strErr = "";
      string strSQL = "";
      DateTime dtDate = DateTime.Now;
      string strNowDateTime = dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo);
      ConnectionTypeSynch pDBConn_Config = new ConnectionTypeSynch(config);
      ConnectionTypeSynch pDBConn_Data = new ConnectionTypeSynch(data);
      GenericObject geoTranForChecksumUpdate = new GenericObject();
      string strALLOWDETAIL = "S";
      CoreFileInfoForChecksum pChecksumData;
      string strTranNbrWithinEvent = "";
      string strEventNbrForTran = "";
      string session_id = args.get("session_id") as string;

      // Bug 15941. FT. Make sure the database connection is closed
      try
      {
      // Data database
      if (!pDBConn_Data.EstablishDatabaseConnection(ref strErr))
          HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-939", strErr);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix


      if (!pDBConn_Data.EstablishRollback(ref strErr))
          ThrowCASLErrorButDoNotCloseDBConnection("TS-940", ref pDBConn_Data, "Error creating database transaction.", strErr);

      //*************************************************************************************************
      // Suspended PayEvent
      GenericObject SuspPAYEVENT = args.get("suspended_PAYEVENT", c_CASL.c_undefined) as GenericObject;
      string Susp_PAYEVENT_FileName = (string)SuspPAYEVENT.get("FILENAME");
      string Susp_PAYEVENT_EventNbr = (string)SuspPAYEVENT.get("EVENTNBR", c_CASL.c_undefined);
      string Susp_PAYEVENT_FileNbr = (string)SuspPAYEVENT.get("FILENAME");
      // Bug #12055 Mike O - Use misc.Parse to log errors
      string strSuspFileNumber         = misc.Parse<Int64>(GetFileNumber(Susp_PAYEVENT_FileName)).ToString();
      string strSuspFileSeqenceNbr     = misc.Parse<Int64>(GetFileSequence(Susp_PAYEVENT_FileName)).ToString();
      //*************************************************************************************************

      //*************************************************************************************************
      // New PayEvent
      GenericObject NewPAYEVENT = args.get("new_PAYEVENT", c_CASL.c_undefined) as GenericObject;
      string new_PAYEVENT_EventNbr = "";// Unknown at this time.
      string new_PAYEVENT_FileName = (string)NewPAYEVENT.get("FILENAME");
      string new_PAYEVENT_UserID = (string)NewPAYEVENT.get("USERID", "auto");
      string strLOGIN_ID = (string)NewPAYEVENT.get("LOGIN_ID", ""); //BUG 5395v2 (ActivateSuspendedEvent)
      string new_PAYEVENT_file_DeptID = (string)NewPAYEVENT.get("file_DEPTID", "auto");
      // Bug #12055 Mike O - Use misc.Parse to log errors
      string strNewFileNumber         = misc.Parse<Int64>(GetFileNumber(new_PAYEVENT_FileName)).ToString();
      string strNewFileSeqenceNbr     = misc.Parse<Int64>(GetFileSequence(new_PAYEVENT_FileName)).ToString();
      string strNewSourceGroup = "";
      string strNewSourceType = "Cashier"; //BUG 5395 Prevent multiple session user login BLL 5/2/08 (ActivateSuspendedEvent)
      string strNewSourceDate = "";
      string strNewSourceRefID = "";
      //*************************************************************************************************

      //*************************************************************************************************
      // Check if allowed to post
      //If a file exists, determine if any NON-VOIDED transactions exist within it
      //We don't care about any previous EVENT numbers, because they may only have one transaction
      //per FILE, which necessitates one EVENT per file.  However, if all previous transactions
      //have been voided, then they may add a single transaction.

      // Configuration database
      if (!pDBConn_Config.EstablishDatabaseConnection(ref strErr))
          HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-941", strErr);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

      //file source could come in as Cashier, OC or Portal which is needed for checking the session
      //however, the rest of the code would expect it to be Cashier or OC, so will change Portal
      //back to Cashier here
      if (strNewSourceType == "Portal")
        strNewSourceType = "Cashier";
      //end BUG 5395

      // BUG4485 check license info in logical object instead of fix config table.  S.X. 100207
      //			strSQL = "SELECT * FROM TG_GENINFO_CONFIG";
      //			GenericObject geoLic = (GenericObject)GetRecords(config, 0, strSQL, pDBConn_Config.GetDBConnectionType());
      //			if(geoLic != null)
      //				strALLOWDETAIL = GetKeyData(geoLic, "ALLOWDETAIL", true);
      GenericObject a_license_obj = c_CASL.c_object("License.data") as GenericObject;
      if (a_license_obj == null)
        HandleErrorsAndLocking("message", "The License is not valid yet." + new_PAYEVENT_FileName, "code", "TS-880", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      bool b_one_event_per_file = (bool)a_license_obj.get("one_event_per_file", false);

      if (b_one_event_per_file.Equals(false))
        strALLOWDETAIL = "Y";
      else
        strALLOWDETAIL = "N";
      // end of BUG4485 

      if (strALLOWDETAIL != "Y" && strALLOWDETAIL != "y")// 
      {
        // Summary only
        strSQL = string.Format("SELECT COUNT(*) AS COUNT FROM TG_TRAN_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND VOIDDT IS NULL", strNewFileNumber, strNewFileSeqenceNbr);
        long lRecCount = GetTableRowCount(data, strSQL, pDBConn_Data.GetDBConnectionType());

        if (lRecCount > 0)
        {
          // Already has one recortd. Do not allowe more.
          if (!args.has("DbConnectionClass"))
              ThrowCASLErrorButDoNotCloseDBConnection("TS-942", ref pDBConn_Data, "The License for this site is for Summary use only!  You may not add another transaction. File:"
                + new_PAYEVENT_FileName, "");
          else
            HandleErrorsAndLocking("message", "The License for this site is for Summary use only! You may not add another transaction. File: "
                + new_PAYEVENT_FileName, "code", "TS-943", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
      }
      //*************************************************************************************************

      //---------------------------------------
      // Also now I we have to get the SourceGroup and SourceType from database.
      // Bug #12055 Mike O - Use misc.Parse to log errors
      strSQL = string.Format("SELECT * FROM TG_DEPFILE_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1}", misc.Parse<Int64>(strNewFileNumber), misc.Parse<Int32>(strNewFileSeqenceNbr));
      GenericObject geoNewFileInfo = (GenericObject)GetRecords(data, 0, strSQL, pDBConn_Data.GetDBConnectionType());
      if (geoNewFileInfo != null)
      {
        strNewSourceGroup = GetKeyData(geoNewFileInfo, "SOURCE_GROPUP", true);
        strNewSourceType = GetKeyData(geoNewFileInfo, "SOURCE_TYPE", true);
        strNewSourceDate = GetKeyData(geoNewFileInfo, "SOURCE_DATE", true);
        // Bug #12055 Mike O - Use misc.Parse to log errors
        DateTime dtNewSourceDate = misc.Parse<DateTime>(strNewSourceDate);
        strNewSourceDate = dtNewSourceDate.ToString("MM/dd/yyyy HH:mm:ss", DateTimeFormatInfo.InvariantInfo);
      }
      //---------------------------------------

      //---------------------------------------
      // New event for new transactions.
      // Bug #12055 Mike O - Use misc.Parse to log errors
      new_PAYEVENT_EventNbr = CreateNewEventNbr(ref pDBConn_Data, data, misc.Parse<Int64>(strNewFileNumber).ToString(), misc.Parse<Int64>(strNewFileSeqenceNbr).ToString());             
      NewPAYEVENT.set("EVENTNBR", new_PAYEVENT_EventNbr);

      //***********************************************************************
      // Create SQL for insert into TG_PAYEVENT_DATA table and execute it.
      GenericObject gTmp = new GenericObject("USERID", new_PAYEVENT_UserID, "STATUS", "1", "MOD_DT", strNowDateTime);
      // Bug #10955 Mike O
      string strPayEventChecksum = CreatePayEventChecksum(Crypto.DefaultHashAlgorithm, "strEventNbr", new_PAYEVENT_EventNbr, "strFileName", new_PAYEVENT_FileName, "geoChecksumData", gTmp);

      strSQL = CreateSQLForInsertInto_TG_PAYEVENT_DATA(new_PAYEVENT_EventNbr, strNewFileNumber, strNewFileSeqenceNbr, new_PAYEVENT_UserID, strNowDateTime, strNewSourceType, strNewSourceGroup, strNewSourceRefID, strNewSourceDate, strPayEventChecksum, pDBConn_Data.GetDBConnectionType(), "1"/*Active*/);
      strErr = "";
      if (!pDBConn_Data.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strErr))
          HandleErrorsAndLocking("message", "Error executing SQL.", "code", "TS-953", strErr);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      //***********************************************************************

      //---------------------------------------

      //*************************************************************************************************
      // Get suspended transactions
      // Bug #12055 Mike O - Use misc.Parse to log errors
      strSQL = string.Format("SELECT * FROM TG_SUSPTRAN_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2}", strSuspFileNumber, strSuspFileSeqenceNbr, misc.Parse<Int64>(Susp_PAYEVENT_EventNbr));
      GenericObject geoTemp = (GenericObject)GetRecords(data, 1, strSQL, pDBConn_Data.GetDBConnectionType());
      GenericObject geoSuspended = null;
      if (geoTemp != null)
      {
        int iSuspCount = geoTemp.getIntKeyLength();
        int iNewTranNbr = 0;// This will be the new transaction number generator.
        //---------------------------------------------------------
        // Iterate through all the suspended transaction.
        for (int j = 0; j < iSuspCount; j++)
        {
          GenericObject a_transaction = new GenericObject();

          geoSuspended = (GenericObject)geoTemp.get(j);
          string strSusp_TRANNBR = (string)geoSuspended.get("TRANNBR").ToString();

          // Validate checksum of suspended transaction record.  Why wasn't this done before?
          // Bug #12055 Mike O - Use misc.Parse to log errors
          if (!ValidateSuspTranChecksum("DbConnectionClass", pDBConn_Data, "strFileName", Susp_PAYEVENT_FileName, "strEventNbr", misc.Parse<Int64>(Susp_PAYEVENT_EventNbr), "strTranNbr", strSusp_TRANNBR))
              ThrowCASLErrorButDoNotCloseDBConnection("TS-944", ref pDBConn_Data, "Transaction checksum invalid. File: " + Susp_PAYEVENT_FileName + "Tran# " + strSusp_TRANNBR, "");

          //---------------------------------------------------------
          // Validate Checksum of ORIGINAL TRANSACTION RECORD - we're going to be using that information to create the
          // "Acitivated" receipt, so we want to make sure that original record hasn't been tampered with.
          // Bug #12055 Mike O - Use misc.Parse to log errors
          if(!ValidateTranChecksum("DbConnectionClass",pDBConn_Data, "strFileName",Susp_PAYEVENT_FileName, "strEventNbr",misc.Parse<Int64>(Susp_PAYEVENT_EventNbr), "strTranNbr",strSusp_TRANNBR))
              ThrowCASLErrorButDoNotCloseDBConnection("TS-944", ref pDBConn_Data, "Transaction checksum invalid. File: " + Susp_PAYEVENT_FileName + "Tran# " + strSusp_TRANNBR, "");
          //---------------------------------------------------------

          //---------------------------------------------------------
          // Get the original transaction data.
          // Bug #12055 Mike O - Use misc.Parse to log errors
          if (pDBConn_Data.GetDBConnectionType() == ConnectionTypeSynch.m_iCurrentDatabase_SqlServer)
            strSQL = string.Format("SELECT * FROM TG_TRAN_DATA (NOLOCK) WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} AND TRANNBR={3}", strSuspFileNumber, strSuspFileSeqenceNbr, misc.Parse<Int64>(Susp_PAYEVENT_EventNbr), misc.Parse<Int64>(strSusp_TRANNBR));
          else
            strSQL = string.Format("SELECT * FROM TG_TRAN_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} AND TRANNBR={3}", strSuspFileNumber, strSuspFileSeqenceNbr, misc.Parse<Int64>(Susp_PAYEVENT_EventNbr), misc.Parse<Int64>(strSusp_TRANNBR));

          GenericObject geoTemp2 = (GenericObject)GetRecords(data, 1, strSQL, pDBConn_Data.GetDBConnectionType());
          if (geoTemp2 == null)
          {
            if (!pDBConn_Data.CommitToDatabase(ref strErr))
                ThrowCASLErrorButDoNotCloseDBConnection("TS-945", ref pDBConn_Data, "Unable to find the original transaction. File: " + Susp_PAYEVENT_FileName + "Tran# " + strSusp_TRANNBR, "");
            else
              HandleErrorsAndLocking("message", "Unable to find the original transaction. File: " + Susp_PAYEVENT_FileName + "Tran# " + strSusp_TRANNBR, "code", "TS-946", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
          }
          else
          { // Got the original transactin.
            int i = geoTemp2.getIntKeyLength();
            if (i != 1)
            {
              if (!pDBConn_Data.CommitToDatabase(ref strErr))
                  ThrowCASLErrorButDoNotCloseDBConnection("TS-947", ref pDBConn_Data, "Unable to find the original transaction. File: " + Susp_PAYEVENT_FileName + "Tran# " + strSusp_TRANNBR, "");
              else
                HandleErrorsAndLocking("message", "Unable to find the original transaction. File: " + Susp_PAYEVENT_FileName + "Tran# " + strSusp_TRANNBR, "code", "TS-948", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
            }
            else
            {
              //----------------------------
              // Get the original tran data.
              GenericObject geoOriginalTran = (GenericObject)geoTemp2.get(0);

              iNewTranNbr += 1;
              string strTRANNBR = iNewTranNbr.ToString();

              a_transaction.set("EVENTNBR", new_PAYEVENT_EventNbr);
              a_transaction.set("SOURCE_DATE", GetKeyData(geoOriginalTran, "SOURCE_DATE", true));
              a_transaction.set("SOURCE_REFID", GetKeyData(geoOriginalTran, "SOURCE_REFID", true));
              a_transaction.set("ITEMIND", GetKeyData(geoOriginalTran, "ITEMIND", true));
              a_transaction.set("TRANAMT", GetKeyData(geoOriginalTran, "TRANAMT", true));
              a_transaction.set("email_address_f_label", GetKeyData(geoOriginalTran, "email_address_f_label", true));
              a_transaction.set("update_system_interface_list_f_label", GetKeyData(geoOriginalTran, "update_system_interface_list_f_label", true));
              a_transaction.set("update_system_interface_list", GetKeyData(geoOriginalTran, "update_system_interface_list", true));
              a_transaction.set("TTDESC", GetKeyData(geoOriginalTran, "TTDESC", true));
              a_transaction.set("TTID", GetKeyData(geoOriginalTran, "TTID", true));
              a_transaction.set("COMMENTS", GetKeyData(geoOriginalTran, "COMMENTS", true));
              a_transaction.set("email_address_f_id", GetKeyData(geoOriginalTran, "email_address_f_id", true));
              a_transaction.set("SYSTEXT", GetKeyData(geoOriginalTran, "SYSTEXT", true));
              a_transaction.set("DISTAMT", GetKeyData(geoOriginalTran, "DISTAMT", true));
              a_transaction.set("email_address", GetKeyData(geoOriginalTran, "email_address", true));
              a_transaction.set("TAXEXIND", GetKeyData(geoOriginalTran, "TAXEXIND", true));
              a_transaction.set("update_system_interface_list_f_id", GetKeyData(geoOriginalTran, "update_system_interface_list_f_id", true));
              a_transaction.set("CONTENTTYPE", GetKeyData(geoOriginalTran, "CONTENTTYPE", true));
              //----------------------------

              //----------------------------
              // Get the item data
              GenericObject geoItemData = new GenericObject();

              // Bug #12055 Mike O - Use misc.Parse to log errors
              if (pDBConn_Data.GetDBConnectionType() == ConnectionTypeSynch.m_iCurrentDatabase_SqlServer)
                strSQL = string.Format("SELECT * FROM TG_ITEM_DATA WITH (NOLOCK) WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} AND TRANNBR={3}", strSuspFileNumber, strSuspFileSeqenceNbr, misc.Parse<Int64>(Susp_PAYEVENT_EventNbr), misc.Parse<Int64>(strSusp_TRANNBR));
              else
                strSQL = string.Format("SELECT * FROM TG_ITEM_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} AND TRANNBR={3}", strSuspFileNumber, strSuspFileSeqenceNbr, misc.Parse<Int64>(Susp_PAYEVENT_EventNbr), misc.Parse<Int64>(strSusp_TRANNBR));

              GenericObject geoTemp3 = (GenericObject)GetRecords(data, 1, strSQL, pDBConn_Data.GetDBConnectionType());
              if (geoTemp3 != null)
              {
                int iItemCount = geoTemp3.getIntKeyLength();
                for (int n = 0; n < iItemCount; n++)
                {
                  GenericObject geoOriginalItem = (GenericObject)geoTemp3.get(n);
                  GenericObject geoItem = new GenericObject();

                  geoItem.set("ITEMACCTID", GetKeyData(geoOriginalItem, "ITEMACCTID", true));
                  geoItem.set("ITEMID", GetKeyData(geoOriginalItem, "ITEMID", true));
                  geoItem.set("TOTAL", GetKeyData(geoOriginalItem, "TOTAL", true));
                  geoItem.set("ITEMNBR", GetKeyData(geoOriginalItem, "ITEMNBR", true));
                  geoItem.set("TAXED", GetKeyData(geoOriginalItem, "TAXED", true));
                  geoItem.set("AMOUNT", GetKeyData(geoOriginalItem, "AMOUNT", true));
                  geoItem.set("ITEMDESC", GetKeyData(geoOriginalItem, "ITEMDESC", true));
                  geoItem.set("QTY", GetKeyData(geoOriginalItem, "QTY", true));
                  geoItem.set("GLACCTNBR", GetKeyData(geoOriginalItem, "GLACCTNBR", true));
                  geoItem.set("ACCTID", GetKeyData(geoOriginalItem, "ACCTID", true));

                  geoItemData.set(n, geoItem);
                }
                if (iItemCount >= 1)// Add any item data elements to this transaction.
                  a_transaction.set("table_TG_ITEM_DATA", geoItemData);
              }
              //----------------------------

              //----------------------------
              // Get the custom fields
              GenericObject geoCustData = new GenericObject();

              // Bug #12055 Mike O - Use misc.Parse to log errors              
              if (pDBConn_Data.GetDBConnectionType() == ConnectionTypeSynch.m_iCurrentDatabase_SqlServer)
                strSQL = string.Format("SELECT * FROM GR_CUST_FIELD_DATA (NOLOCK) WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} AND TRANNBR={3}", strSuspFileNumber, strSuspFileSeqenceNbr, misc.Parse<Int64>(Susp_PAYEVENT_EventNbr), misc.Parse<Int64>(strSusp_TRANNBR));
              else
                strSQL = string.Format("SELECT * FROM GR_CUST_FIELD_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} AND TRANNBR={3}", strSuspFileNumber, strSuspFileSeqenceNbr, misc.Parse<Int64>(Susp_PAYEVENT_EventNbr), misc.Parse<Int64>(strSusp_TRANNBR));

              GenericObject geoTemp4 = (GenericObject)GetRecords(data, 1, strSQL, pDBConn_Data.GetDBConnectionType());
              if (geoTemp4 != null)
              {
                int iCustItemCount = geoTemp4.getIntKeyLength();
                for (int y = 0; y < iCustItemCount; y++)
                {
                  GenericObject geoOriginalItem = (GenericObject)geoTemp4.get(y);

                  string strCUSTTAG = GetKeyData(geoOriginalItem, "CUSTTAG", true);
                  geoCustData.set(y, strCUSTTAG);
                  a_transaction.set(strCUSTTAG + "_f_id", GetKeyData(geoOriginalItem, "CUSTID", true));
                  a_transaction.set(strCUSTTAG + "_f_label", GetKeyData(geoOriginalItem, "CUSTLABEL", true));
                  a_transaction.set(strCUSTTAG + "_f_attributes", GetKeyData(geoOriginalItem, "CUSTATTR", true));
                  a_transaction.set(strCUSTTAG, GetKeyData(geoOriginalItem, "CUSTVALUE", true));
                }

                if (iCustItemCount > 1)
                  a_transaction.set("_custom_keys", geoCustData);
              }

              // Checksum will be updated after data is commited.
              pChecksumData = new CoreFileInfoForChecksum();
              // Bug #12055 Mike O - Use misc.Parse to log errors
              pChecksumData.Init(strNewFileNumber, misc.Parse<Int64>(strNewFileSeqenceNbr).ToString(), misc.Parse<Int64>(new_PAYEVENT_EventNbr).ToString(), misc.Parse<Int64>(strTRANNBR).ToString());
              geoTranForChecksumUpdate.insert(pChecksumData);

              // Update GEO
              a_transaction.set("POSTDT", strNowDateTime);
              //----------------------------
            }
          }
          //********************************************
          // Post the transaction
          GenericObject pRV = null;
          try
          {
            if (strEventNbrForTran != new_PAYEVENT_EventNbr)
              strEventNbrForTran = "";

            if (strTranNbrWithinEvent.Length > 0 && strEventNbrForTran == new_PAYEVENT_EventNbr)
            {
              // Bug #12055 Mike O - Use misc.Parse to log errors
              long lTran = misc.Parse<Int64>(strTranNbrWithinEvent);
              lTran += 1;
              a_transaction.set("TRANNBR", lTran.ToString());
            }

            pRV = (GenericObject)PostIndividualTransaction(
              ref strErr,
              "data", data, //BUG 5807
              "DbConnectionClass", pDBConn_Data,
              "_subject", a_transaction,// New TranNbr should be created in PostIndividualTransaction.
              "file_FILENAME", new_PAYEVENT_FileName,
              "event_EVENTNBR", new_PAYEVENT_EventNbr,
              "SOURCE_GROUP", strNewSourceGroup,
              "SOURCE_TYPE", strNewSourceType,
              "geoTranForChecksumUpdate", geoTranForChecksumUpdate);

            strTranNbrWithinEvent = (string)pRV.get("TRANNBR").ToString();
            strEventNbrForTran = (string)pRV.get("EVENTNBR").ToString();
          }
          catch (Exception e)
          {
            strErr += " " + ConstructDetailedError(e);
            HandleErrorsAndLocking("message", "Unable to post transaction.", "code", "TS-949", strErr);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
          }
          if (pRV == null)
          {
            HandleErrorsAndLocking("message", "Unable to post transaction. ", "code", "TS-950", strErr);//Bug 13832-Added detailed error message//Bug 13832-added inner exception // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
          }
          //********************************************
        }
      }
      //*************************************************************************************************

      // Mark the Suspend Event Record with the new File and Event Nbrs      
      // Bug #12055 Mike O - Use misc.Parse to log errors
      strSQL = string.Format("UPDATE TG_SUSPEVENT_DATA SET RESUME_EVENTNBR={0},RESUME_FILENBR={1},RESUME_FILESEQ={2} WHERE EVENTNBR={3} AND DEPFILENBR={4} AND DEPFILESEQ={5}",
        misc.Parse<Int64>(new_PAYEVENT_EventNbr),
        misc.Parse<Int64>(strNewFileNumber),
        misc.Parse<Int64>(strSuspFileSeqenceNbr), 
        misc.Parse<Int64>(Susp_PAYEVENT_EventNbr), 
        misc.Parse<Int64>(strSuspFileNumber),
        misc.Parse<Int64>(strSuspFileSeqenceNbr));
      if (!pDBConn_Data.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strErr))
      {
          ThrowCASLErrorButDoNotCloseDBConnection("TS-951", ref pDBConn_Data, "Error executing SQL.", strErr);
      }

      //*************************************************************************************************
      // ALL GOOD - ACCEPT THE UPDATES.
      strErr = "";
      if (!pDBConn_Data.CommitToDatabase(ref strErr))
          ThrowCASLErrorButDoNotCloseDBConnection("TS-952", ref pDBConn_Data, "Unable to commit changes to database.", strErr);

      // UPDATE TRAN DATA CHECKSUM
      UpdateTransactionDataChecksum(geoTranForChecksumUpdate, pDBConn_Data, data);

      //*************************************************************************************************

      return NewPAYEVENT;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnection(pDBConn_Config);
        CloseDBConnection(pDBConn_Data);
        // End bug 15941.
      }
    }
    public static Object ModifyFile(params Object[] arg_pairs)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject data = args.get("data", c_CASL.c_undefined) as GenericObject;
      string strFileName = (string)args.get("FILENAME", "");
      string strFileDesc = (string)args.get("FILEDESC", "");
      string strEffectiveDt = (string)args.get("EFFECTIVEDT", "");

      ConnectionTypeSynch pDBConn_Data = new ConnectionTypeSynch(data);
      bool connectionOpened = false;      // Bug 15941
      string strErr = "";
      string strSQL = "";

      if (strFileName.Length == 0 && strFileDesc.Length == 0 && strEffectiveDt.Length == 0)
        HandleErrorsAndLocking("message", "Filename, description or effective date is required.", "code", "TS-1047", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

      try
      {
      //***************************************
      if (args.has("DbConnectionClass"))
        pDBConn_Data = (ConnectionTypeSynch)args.get("DbConnectionClass");
      else
      {
        pDBConn_Data = new ConnectionTypeSynch(data);

          if (!(connectionOpened = pDBConn_Data.EstablishDatabaseConnection(ref strErr)))  // Bug 15941. Open connection and set connectionO flagpened
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1048", strErr);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        if (!pDBConn_Data.EstablishRollback(ref strErr))
            ThrowCASLErrorButDoNotCloseDBConnection("TS-1049", ref pDBConn_Data, "Error creating database transaction.", strErr);
      }
      //***************************************

      //-------------------------------------------------------
      // Break the file into file number and sequence number
      string strFileNumber = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);
      //-------------------------------------------------------

      //-------------------------------------------------------
      // Validate old checksum first
      if (!ValidateTranFileChecksum("strFileName", strFileName, "DbConnectionClass", pDBConn_Data))
          ThrowCASLErrorButDoNotCloseDBConnection("TS-1050", ref pDBConn_Data, "Invalid checksum for file: " + strFileName, "");
      //-------------------------------------------------------

      //-------------------------------------------------------
      // Update the table.
      strSQL = "UPDATE TG_DEPFILE_DATA";

      if (strFileDesc.Length > 0 && strEffectiveDt.Length == 0)// Update only description
        strSQL += string.Format(" SET FILEDESC='{0}'", strFileDesc);
      else if (strFileDesc.Length == 0 && strEffectiveDt.Length > 0)// Update only data
        strSQL += string.Format(" SET EFFECTIVEDT='{0}'", strEffectiveDt);
      else if (strFileDesc.Length > 0 && strEffectiveDt.Length > 0)// Update both, data and description 
        strSQL += string.Format(" SET FILEDESC='{0}',EFFECTIVEDT='{1}'", strFileDesc, strEffectiveDt);

      // Where
      // Bug #12055 Mike O - Use misc.Parse to log errors
      strSQL += string.Format(" WHERE DEPFILENBR={0} AND DEPFILESEQ={1}", misc.Parse<Int64>(strFileNumber), misc.Parse<Int32>(strFileSequenceNbr));

      strErr = "";
      if (!pDBConn_Data.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strErr))
          ThrowCASLErrorButDoNotCloseDBConnection("TS-1051", ref pDBConn_Data, "Error executing SQL.", strErr + "SQL: " + strSQL);
      //-------------------------------------------------------

      //-------------------------------------------------------
      // Update the checksum
      if (!UpdateTranFileChecksum("DbConnectionClass", pDBConn_Data, "strFileName", strFileName))
          ThrowCASLErrorButDoNotCloseDBConnection("TS-1052", ref pDBConn_Data, "Unable to set checksum on file: " + strFileName, "");
      //-------------------------------------------------------

      //-------------------------------------------------------
      // ALL GOOD - ACCEPT THE UPDATES.
      if (!args.has("DbConnectionClass"))
      {
        strErr = "";
        if (!pDBConn_Data.CommitToDatabase(ref strErr))
            ThrowCASLErrorButDoNotCloseDBConnection("TS-1053", ref pDBConn_Data, "Unable to commit changes to database.", strErr);
      }
      //-------------------------------------------------------

      return true;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn_Data, connectionOpened);
      }
    }

    // Called by: TranSuite_DBAPI.SourceSetEventStatus.TG_PAYEVENT_DATA.core_event_set_status
    public static object ChangeEventStatus(params Object[] arg_pairs)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      // It is possible to pass in a database connection using the "DbConnectionClass" key.
      // Connection must be of type ConnectionTypeSynch class.

      GenericObject args = misc.convert_args(arg_pairs);
      ConnectionTypeSynch pDBConn = null;
      bool connectionOpened = false;      // Bug 15941

      ThreadLock.GetInstance().CreateLock();// Sync user's access to this resource. Bug# 5813 deadlock fix. DH 07/30/2008
      // Bug 11661: Wrap mutex release in finally block
      try
      {
        GenericObject a_core_event = args.get("_subject", c_CASL.c_undefined) as GenericObject;
        GenericObject config = args.get("config", c_CASL.c_undefined) as GenericObject;
        GenericObject data = args.get("data", c_CASL.c_undefined) as GenericObject;
        String strStatus = (string)args.get("STATUS");
        String strEventNbr = (string)a_core_event.get("EVENTNBR");
        String strFileName = (string)a_core_event.get("file_FILENAME");
        pDBConn = new ConnectionTypeSynch(data);
        string session_id = args.get("session_id") as string;
        string strErr = "";
        string strSQL = "";
        string strUserID = "";
        string strOldChecksum = "";
        string strDate = "";
        string strOldStatus = "";
        DateTime dtDate = DateTime.Now;
        string strNowDateTime = dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo);
        //BUG 5395 Prevent multiple session user login BLL 5/2/08 (ChangeEventStatus)
        string strFileSourceType = "";
        if (a_core_event.has("file_SOURCE_TYPE"))
          strFileSourceType = a_core_event.get("file_SOURCE_TYPE").ToString();
        if (a_core_event.has("USERID"))
          strUserID = a_core_event.get("USERID").ToString();

        string strLOGIN_ID = ""; //BUG#8590
        object objLOGIN_ID = args.get("LOGIN_ID", "");//BUG#8590
        if (objLOGIN_ID is string)//BUG#8590
          strLOGIN_ID = objLOGIN_ID as string;//BUG#8590

        //end BUG 5395
        if (strStatus.Length == 0 || strFileName.Length == 0 || strEventNbr.Length == 0)
          HandleErrorsAndLocking("message", "Required information missing in Change Event Status!", "code", "TS-863", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        //***************************************
        if (args.has("DbConnectionClass"))
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
        else
        {
          pDBConn = new ConnectionTypeSynch(data);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strErr)))  // Bug 15941. Open connection and set connectionO flagpened
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-864", strErr);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

          if (!pDBConn.EstablishRollback(ref strErr))
            ThrowCASLErrorButDoNotCloseDBConnection("TS-924", ref pDBConn, "Error creating database transaction.", strErr);
        }
        //***************************************

        //file source could come in as Cashier, OC or Portal which is needed for checking the session
        //however, the rest of the code would expect it to be Cashier or OC, so will change Portal
        //back to Cashier here
        if (strFileSourceType == "Portal")
          strFileSourceType = "Cashier";
        //end BUG 5395

        // Break the file into file number and sequence number
        string strFileNumber = GetFileNumber(strFileName);
        string strFileSequenceNbr = GetFileSequence(strFileName);

        //--------------------------------------
        // Check first to see if file is available for status change.
        if (!IsFileActive(strFileNumber, strFileSequenceNbr, data, ref pDBConn))
          ThrowCASLErrorButDoNotCloseDBConnection("TS-925", ref pDBConn, "Unable to Balance. Transaction File " + strFileNumber + " is not a currently Active file.", "");
        //--------------------------------------

        //--------------------------------------
        // Validate Pay Even checksum.
        // Bug 18766: Parameterized
        GenericObject geoPayEvent = DBUtils.readAllPayEventFieldsIntoAGeo(pDBConn, strEventNbr, strFileNumber, strFileSequenceNbr);
        if (geoPayEvent == null)
          ThrowCASLErrorButDoNotCloseDBConnection("TS-926", ref pDBConn, "Unable to retrieve checksum information.", "");
        else
        {
          strOldChecksum = GetKeyData(geoPayEvent, "CHECKSUM", true);
          // ALSO GET THE USER ID
          strUserID = GetKeyData(geoPayEvent, "USERID", true);
          // ALSO GET DATE
          strDate = GetKeyData(geoPayEvent, "MOD_DT", true);
        // Bug #12055 Mike O - Use misc.Parse to log errors
        DateTime dtMOD_DT = misc.Parse<DateTime>(strDate);
          strDate = dtMOD_DT.ToString("G", DateTimeFormatInfo.InvariantInfo);
          strOldStatus = GetKeyData(geoPayEvent, "STATUS", true); ;
        }

        // Matt -- replacing old function call
        GenericObject gTemp = new GenericObject("USERID", strUserID, "STATUS", strOldStatus, "MOD_DT", strDate, "CHECKSUM", strOldChecksum);
        if (!ValidatePayEventChecksum("strEventNbr", strEventNbr, "strFileName", strFileName, "geoChecksumData", gTemp))
          ThrowCASLErrorButDoNotCloseDBConnection("TS-927", ref pDBConn, "Checksum failed for: " + "File: " + strFileName + " Event Number " + strEventNbr + " ", strErr);

        //--------------------------------------
        // Modify event nbr and post date
        string strNow = dtDate.ToString("MM/dd/yyyy HH:mm:ss");
        if (!ModifyPayEventStatus(ref pDBConn, strEventNbr, strFileName, strStatus, strNow, ref strErr))
          ThrowCASLErrorButDoNotCloseDBConnection("TS-929", ref pDBConn, "Unable to modify event status for: " + "File: " + strFileName + " Event Number " + strEventNbr + " ", strErr);
        //--------------------------------------
      //Bug 12835 NJ update the event post datetime
      if (!UpdateCompletionDate(ref pDBConn, strEventNbr, strFileName, strNow, ref strErr))
          ThrowCASLErrorButDoNotCloseDBConnection("TS-928", ref pDBConn, "Unable to update completion date for: " + "File: " + strFileName + " Event Number " + strEventNbr + " ", strErr);
      //End bug 12835
        //--------------------------------------
        // Update checksum.
        //Matt -- replacing old update function call
        gTemp = new GenericObject("USERID", strUserID, "STATUS", strStatus, "MOD_DT", strNowDateTime);
        if (!UpdatePayEventChecksum("DbConnectionClass", pDBConn, "strEventNbr", strEventNbr, "strFileName", strFileName, "geoChecksumData", gTemp))
          ThrowCASLErrorButDoNotCloseDBConnection("TS-930", ref pDBConn, "Unable to set checksum for: " + "File: " + strFileName + " Event Number " + strEventNbr + " ", strErr);

        //-------------------------------------
        // Check to see if transactions are being suspended. If so, move them.
        if (strStatus == "4")
        {
          // Bug #10955 Mike O
          string strChecksum = CreateSuspEventChecksum(Crypto.DefaultHashAlgorithm, "strFileName", strFileName, "strEventNbr", strEventNbr, "strNowDateTime", strNowDateTime, "strUserID", strUserID);

          strSQL = "INSERT INTO TG_SUSPEVENT_DATA (EVENTNBR, DEPFILENBR, DEPFILESEQ, SUSPEND_DT, USERID,";
          strSQL += " RESUME_EVENTNBR, RESUME_FILENBR, RESUME_FILESEQ, CHECKSUM) VALUES (";
          strSQL += strEventNbr + ", " + strFileNumber + ", " + strFileSequenceNbr + ", ";

          // Oracle date format
          if (pDBConn.GetDBConnectionType() == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_ODBC || pDBConn.GetDBConnectionType() == ConnectionTypeSynch.m_iCurrentDatabase_Oracle_TNS)
            strSQL = strSQL + "TO_DATE('" + strNowDateTime + "','MM/DD/YYYY HH24:MI:SS'), '";
          else // SQL Server Ms-Access format
            strSQL = strSQL + "'" + strNowDateTime + "', '";

          strSQL = strSQL + strUserID + "', 0, 0, 0, " + "'" + strChecksum + "'" + ")";

          // Bug 25654: Changed from ExecuteSQLAndRollBackIfNeeded: need to do commit/rollback at a higher level
          if (!pDBConn.ExecuteSQL_NO_RollBack(strSQL, ref strErr))
            ThrowCASLErrorButDoNotCloseDBConnection("TS-932", ref pDBConn, "Error executing SQL.", strErr);

          string strError = "";
          if (!SuspendTransactions(data, strEventNbr, strFileNumber, strFileSequenceNbr, strNowDateTime, strUserID, ref pDBConn, ref strError, strFileName))
            ThrowCASLErrorButDoNotCloseDBConnection("TS-933", ref pDBConn, strError, "");
        }
        //-------------------------------------

        //***************************************
        // ALL GOOD - ACCEPT THE UPDATES.
        if (!args.has("DbConnectionClass"))
        {
          strErr = "";
          if (!pDBConn.CommitToDatabase(ref strErr))
            ThrowCASLErrorButDoNotCloseDBConnection("TS-934", ref pDBConn, "Unable to commit changes to database.", strErr);
        }
        //***************************************   

        a_core_event.set("MOD_DT", "");

      }
      catch (Exception)
      {
        // Bug 25654: Do an explicit rollback here instead of depending on the 
        // automated rollback during a database connection close
        if (connectionOpened)
        {
          if (pDBConn != null)
            pDBConn.RollBack();
        }
        throw;
      }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);

        // Bug 11661
        ThreadLock.GetInstance().ReleaseLock(); //Bug# 5813 deadlock fix. DH 07/30/2008
      }

      return true;
    }

    public static bool SuspendTransactions(GenericObject data, string strEventNbr, string strFileNumber, string strFileSequenceNbr, string strVoidDt, string strUserID, ref ConnectionTypeSynch pDBConn, ref string strError, string strFileName)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      // BUG# 7516 D.H
      ThreadLock.GetInstance().CreateLock();
      // Bug 11661: Wrap mutex release in finally block
      try
      {
     // string strTemp = ""; 12081 NJ- unused variable
        string strChecksum = "";
        string strTranNbr = "";
        string strSQL = "";

      // Bug #12055 Mike O - Use misc.Parse to log errors
      strSQL =  "SELECT TRANNBR,EVENTNBR,DEPFILENBR,DEPFILESEQ FROM TG_TRAN_DATA WHERE EVENTNBR=" + misc.Parse<Int64>(strEventNbr) + " AND DEPFILENBR=" + misc.Parse<Int64>(strFileNumber) + " AND DEPFILESEQ=" + misc.Parse<Int64>(strFileSequenceNbr) + " AND VOIDDT IS NULL";

        // Get all the non voided trasaction that will be susspended.
        GenericObject geoNonVoidTrans = (GenericObject)GetRecords(data, 1, strSQL, pDBConn.GetDBConnectionType());
        if (geoNonVoidTrans != null)
        {
          // Iterate thru all transactions in TG_TRAN_DATA and add them to TG_SUSPTRAN_DATA table.
          // Also create new checksum for each record.
          int iCount = geoNonVoidTrans.getIntKeyLength();
          for (int i = 0; i < iCount; i++)
          {
            GenericObject geoElement = (GenericObject)geoNonVoidTrans.get(i);

            strTranNbr = GetKeyData(geoElement, "TRANNBR", true);

            // Bug #10955 Mike O
            strChecksum = CreateSuspTranChecksum(Crypto.DefaultHashAlgorithm, "strFileName", strFileNumber + strFileSequenceNbr, "strEventNbr", strEventNbr, "strTranNbr", strTranNbr);

            strSQL = "INSERT INTO TG_SUSPTRAN_DATA (TRANNBR, EVENTNBR, DEPFILENBR, DEPFILESEQ, CHECKSUM) VALUES (";
            strSQL += strTranNbr + ", " + strEventNbr + ", " + strFileNumber + ", " + strFileSequenceNbr + ", " + "'" + strChecksum + "'" + ")";

            // Bug 25654: Changed from ExecuteSQLAndRollBackIfNeeded: need to do commit/rollback at a higher level
            if (!pDBConn.ExecuteSQL_NO_RollBack(strSQL, ref strError))
              return false;

            // Void the transaction.
            if (!VoidTransaction("strTranNbr", strTranNbr,
              "strEventNbr", strEventNbr,
              "strFileName", strFileName,
              "strVoidDT", strVoidDt,
              "strVoidUserID", strUserID,
              "DbConnectionClass", pDBConn))
            {
              // BUG# 7516 D.H
              // Bug 11661: Moved ReleaseLock to finally
              // ThreadLock.GetInstance().ReleaseLock();
              return false;
            }
          }
        }

        // BUG# 7516 D.H
      }
      finally
      {
        // Bug 11661
        // BUG# 7516 D.H
        ThreadLock.GetInstance().ReleaseLock();
      }

      return true;
    }

    // Bug #10955 Mike O
    public static string CreateDepositChecksum(HashAlgorithm alg, params Object[] arg_pairs)
    {
      /* Matt -- this function will create a Deposit Checksum.  In addtion to the params
           listed below the user must provide one of the following: the values to 
           create a checksum in "geoChecksumData", the DbConnection info in "config" 
           or an existing connection to the database in "DbConnectionClass."
       params    strDepositID			the deposit id
           strDepositNbr		the deposit nbr */


      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;

      string strDepositID = args.get("strDepositID").ToString();
      string strDepositNbr = args.get("strDepositNbr").ToString();

      string strSQL = "",err="";
      string strChecksumData = "";
      string strDepositChecksum = "";

      bool connectionOpened = false;    // Bug 15941

      try
      {
      if (args.has("geoChecksumData"))
      {
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else if (args.has("DbConnectionClass") || args.has("config"))
      {
        if (args.has("DbConnectionClass"))
        {
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
          config = pDBConn.GetDBConnectionInfo();
        }
        else if (args.has("config"))
        {
          config = args.get("config", c_CASL.c_undefined) as GenericObject;
          pDBConn = new ConnectionTypeSynch(config);

            if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref err)))  // Bug 15941
              HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-963", err);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        int iDBType = pDBConn.GetDBConnectionType();
        strSQL = string.Format("SELECT * FROM TG_DEPOSIT_DATA WHERE DEPOSITTYPE IS NOT NULL AND DEPOSITID='{0}' AND DEPOSITNBR={1}", strDepositID, strDepositNbr);

        gTmp = (GenericObject)GetRecords(config, 0, strSQL, iDBType);

        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found. ", "code", "TS-964",
              String.Format("Error:{0} \n SQL statement:{1} ", err, strSQL));//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a config, connection or the checksum data.", "code", "TS-965", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      // DEPOSIT DATA ENCRYPTION:
      // DepositID + DepositNbr + DepositType + GroupID + GroupNbr + FileNbr + FileSeq +
      // OwnerID + CreatorID + Amount + PostDt + VoidDt + VoidID + Comments + 
      // DepSlipNbr + BankID + SourceType + SourceGroup + SourceRefID

      string strDepositType = GetKeyData(gTmp, "DEPOSITTYPE", false);
      string strGroupID = GetKeyData(gTmp, "GROUPID", false);
      string strGroupNbr = GetKeyData(gTmp, "GROUPNBR", false);
      string strFileNbr = GetKeyData(gTmp, "DEPFILENBR", false);
      string strFileSequenceNbr = GetKeyData(gTmp, "DEPFILESEQ", false);
      string strOwnerID = GetKeyData(gTmp, "OWNERUSERID", false);
      string strCreatorID = GetKeyData(gTmp, "CREATERUSERID", false);
      string strAmount = GetKeyData(gTmp, "AMOUNT", false);
      string strPostDT = GetKeyData(gTmp, "POSTDT", false);
      string strVoidDT = GetKeyData(gTmp, "VOIDDT", false);
      string strVoidID = GetKeyData(gTmp, "VOIDUSERID", false);
      string strComments = GetKeyData(gTmp, "COMMENTS", false);
      string strDepSlipNbr = GetKeyData(gTmp, "DEPSLIPNBR", false);
      string strBankID = GetKeyData(gTmp, "BANKID", false);
      string strSourceType = GetKeyData(gTmp, "SOURCE_TYPE", false);
      string SourceGroup = GetKeyData(gTmp, "SOURCE_GROUP", false);
      string SourceRefID = GetKeyData(gTmp, "SOURCE_REFID", false);

      DateTime dtDate;
      if (strPostDT != null && strPostDT != "")
      {
        // Bug #12055 Mike O - Use misc.Parse to log errors
        dtDate = misc.Parse<DateTime>(strPostDT);
        strPostDT = dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo);
      }

      //strChecksumData += VOIDDT;
      if (strVoidDT != null && strVoidDT != "")
      {
        // Bug #12055 Mike O - Use misc.Parse to log errors
        dtDate = misc.Parse<DateTime>(strVoidDT);
        strVoidDT = dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo);
      }

      // Bug #12055 Mike O - Use misc.Parse to log errors
      Decimal fConvert1 = misc.Parse<Decimal>(strAmount);// BUG19280 NEVER USE single-precision-float for currency
      strAmount = string.Format("{0:f}", fConvert1);

      strFileSequenceNbr = strFileSequenceNbr.PadLeft(3, '0');
      
      // Bug #14185 Mike O - Move string building into its own function
      strChecksumData = BuildDepositChecksumString(strDepositID, strDepositNbr, strDepositType, strGroupID, strGroupNbr, strFileNbr, strFileSequenceNbr,
        strOwnerID, strCreatorID, strAmount, strPostDT, strVoidDT, strVoidID, strComments, strDepSlipNbr, strBankID, strSourceType, SourceGroup, SourceRefID);

      // Bug #10955 Mike O
      // Hash
      strDepositChecksum = Crypto.ComputeHash("", strChecksumData, alg);


      return strDepositChecksum;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    /// <summary>
    /// Creates a deposit checksum out of a given deposit object
    /// </summary>
    /// <param name="deposit">the deposit to checksum</param>
    /// <returns>the checksum</returns>
    public static string CreateDepositChecksum(GenericObject deposit)
    {
      string strDepositID = GetStringValue(deposit, "DEPOSITID");
      string strDepositNbr = GetStringValue(deposit, "DEPOSITNBR");
      string strDepositType = GetStringValue(deposit, "DEPOSITTYPE");
      string strGroupID = GetStringValue(deposit, "GROUPID");
      string strGroupNbr = GetStringValue(deposit, "GROUPNBR");
      string strFileNbr = GetStringValue(deposit, "DEPFILENBR").PadLeft(3, '0');
      string strFileSequenceNbr = GetStringValue(deposit, "DEPFILESEQ");
      string strOwnerID = GetStringValue(deposit, "OWNERUSERID");
      string strCreatorID = GetStringValue(deposit, "CREATERUSERID");
      string strAmount = GetStringValue(deposit, "AMOUNT");
      string strPostDT = GetStringValue(deposit, "POSTDT");
      string strVoidDT = GetStringValue(deposit, "VOIDDT");
      string strVoidID = GetStringValue(deposit, "VOIDUSERID");
      string strComments = GetStringValue(deposit, "COMMENTS");
      string strDepSlipNbr = GetStringValue(deposit, "DEPSLIPNBR");
      string strBankID = GetStringValue(deposit, "BANKID");
      string strSourceType = GetStringValue(deposit, "SOURCE_TYPE");
      string strSourceGroup = GetStringValue(deposit, "SOURCE_GROUP");
      string strSourceRefID = GetStringValue(deposit, "SOURCE_REFID");
      
      // Bug #14185 Mike O - Move string building into its own function
      string checksum_data = BuildDepositChecksumString(strDepositID, strDepositNbr, strDepositType, strGroupID, strGroupNbr, strFileNbr, strFileSequenceNbr,
        strOwnerID, strCreatorID, strAmount, strPostDT, strVoidDT, strVoidID, strComments, strDepSlipNbr, strBankID, strSourceType, strSourceGroup, strSourceRefID);

      return Crypto.ComputeHash("", checksum_data);
    }

    public static bool ValidateDepositChecksum(params Object[] arg_pairs)
    {
      /* Matt -- this function creates a checksum and compares it to the existing checksum.  
           In addtion to the params listed below the user must provide one of the 
           following: the values to create a checksum in "geoChecksumData", the 
           DbConnection info in "config" or an existing connection to the database in 
           "DbConnectionClass."
       params    strDepositID			the deposit id
           strDepositNbr		the deposit nbr */

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;

      string strDepositID = args.get("strDepositID").ToString();
      string strDepositNbr = args.get("strDepositNbr").ToString();

      string strSQL = "", err="";
      string strDepositChecksum = "";
      string strOldChecksum = "";

      bool bVal = true;
      // Bug 15941. FT. Make sure the database connection is closed
      bool connectionOpened = false;    // Bug 15941
      try
      {
      // Either the connection was passed in or we create one
      if (args.has("DbConnectionClass") || args.has("config"))
      {
        if (args.has("DbConnectionClass"))
        {
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
          config = pDBConn.GetDBConnectionInfo();
        }
        else if (args.has("config"))
        {
          config = args.get("config", c_CASL.c_undefined) as GenericObject;
          pDBConn = new ConnectionTypeSynch(config);

            if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref err)))  // Bug 15941
              HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1028", err);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        int iDBType = pDBConn.GetDBConnectionType();
        strSQL = string.Format("SELECT * FROM TG_DEPOSIT_DATA WHERE DEPOSITTYPE IS NOT NULL AND DEPOSITID='{0}' AND DEPOSITNBR={1}", strDepositID, strDepositNbr);

        gTmp = (GenericObject)GetRecords(config, 0, strSQL, iDBType);

        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found. ", "code", "TS-1029",
              String.Format("Error:{0} \n SQL statement:{1} ", err, strSQL));//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

      }
      else if (args.has("geoChecksumData"))
      {
        // Or we don't need a connection because the structure is already passed in
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a config, connection or the checksum data.", "code", "TS-1030", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      // Bug #10955 Mike O
      strOldChecksum = GetKeyData(gTmp, "CHECKSUM", false);
      HashAlgorithm hashAlg = Crypto.HashAlgorithmUsed(strOldChecksum);

      if (hashAlg == null)
      {
        // Bug #12800 Mike O - If the checksum doesn't match any possible algorithms, fail validation
        return false;
      }

      // The newly created checksum
      strDepositChecksum = CreateDepositChecksum(hashAlg, "strDepositID", strDepositID, "strDepositNbr", strDepositNbr, "geoChecksumData", gTmp);
      // End Bug #10955 Mike O

      if (strOldChecksum == strDepositChecksum)
      {
        bVal = true;
      }

      return bVal;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static bool UpdateDepositChecksum(params Object[] arg_pairs)
    {
      /* Matt -- this function will update the deposit checksum.  In addtion to the 
           params listed below the user must provide one of the following: the 
           DbConnection info in "config" or an existing connection to the database 
           in "DbConnectionClass." The user has the option to provide a structure
           that will be used to create the checksum, if not the information will be
           gathered through the existing or newly made connection.
       params    strDepositID			the deposit id
           strDepositNbr		the deposit nbr */

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;
      bool connectionOpened = false;      // Bug 15941

      string strDepositID = args.get("strDepositID").ToString();
      string strDepositNbr = args.get("strDepositNbr").ToString();

      string strError = "Default Error";
      string strSQL = "";
      string strDepositChecksum = "";

      // Bug 15941. FT. Make sure the database connection is closed
      try
      {
      if (args.has("DbConnectionClass"))
      {
        pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
        config = pDBConn.GetDBConnectionInfo();
      }
      else if (args.has("config"))
      {
        config = args.get("config", c_CASL.c_undefined) as GenericObject;
        pDBConn = new ConnectionTypeSynch(config);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strError)))  // Bug 15941. Open connection and set connectionO flagpened
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-987", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        if (!pDBConn.EstablishRollback(ref strError))
        {
          strError = "Error creating database transaction.";
            ThrowCASLErrorButDoNotCloseDBConnection("TS-988", ref pDBConn, "Error creating database transaction.", strError);
        }
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a method to connect to the database.", "code", "TS-989", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      if (args.has("geoChecksumData"))
      {
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else
      {
        int iDBType = pDBConn.GetDBConnectionType();
        strSQL = string.Format("SELECT * FROM TG_DEPOSIT_DATA WHERE DEPOSITTYPE IS NOT NULL AND DEPOSITID='{0}' AND DEPOSITNBR={1}", strDepositID, strDepositNbr);

        gTmp = (GenericObject)GetRecords(config, 0, strSQL, iDBType);

        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found. ", "code", "TS-990",
              String.Format("Error:{0} \n SQL statement:{1} ", strError, strSQL));//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
      }

      // Bug #10955 Mike O
      strDepositChecksum = CreateDepositChecksum(Crypto.DefaultHashAlgorithm, "strDepositID", strDepositID, "strDepositNbr", strDepositNbr, "geoChecksumData", gTmp);

      // Create SQL
      strSQL = string.Format("UPDATE TG_DEPOSIT_DATA SET CHECKSUM='{0}' WHERE DEPOSITID='{1}' AND DEPOSITNBR={2}", strDepositChecksum, strDepositID, strDepositNbr);

      if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strError))
          HandleErrorsAndLocking("message", "Error executing SQL.", "code", "TS-991", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix


      //***************************************
      // ALL GOOD - ACCEPT THE UPDATES and close the connection we created.
      if (!args.has("DbConnectionClass"))
      {
        if (!pDBConn.CommitToDatabase(ref strError))
            HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-992", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

      }
      //***************************************   

      return true;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static string CreateGRCustomFieldChecksum(GenericObject custom_field)
    {
      string custom_field_DEPFILENBR = GetStringValue(custom_field, "DEPFILENBR");
      string custom_field_DEPFILESEQ = GetStringValue(custom_field, "DEPFILESEQ");
      string custom_field_EVENTNBR = GetStringValue(custom_field, "EVENTNBR");
      string custom_field_TRANNBR = GetStringValue(custom_field, "TRANNBR");
      string custom_field_SCREENINDEX = GetStringValue(custom_field, "SCREENINDEX");
      string custom_field_label = GetStringValue(custom_field, "CUSTLABEL");
      string custom_field_value = GetStringValue(custom_field, "CUSTVALUE");
      
      // Bug #14185 Mike O - Move string building into its own function
      string checksum_data = BuildGRCustFieldChecksumString(custom_field_TRANNBR, custom_field_EVENTNBR, custom_field_DEPFILENBR, custom_field_DEPFILESEQ,
        custom_field_SCREENINDEX, custom_field_label, custom_field_value);

      return Crypto.ComputeHash("", checksum_data);
    }
    // Bug #8400 Mike O - Added CREATION_DT
    public static string CreateEventChecksum(GenericObject payevent)
    {
      string event_EVENTNBR = GetStringValue(payevent, "EVENTNBR");
      string event_DEPFILENBR = GetStringValue(payevent, "DEPFILENBR");
      string event_DEPFILESEQ = GetStringValue(payevent, "DEPFILESEQ");
      string event_USERID = GetStringValue(payevent, "USERID");
      string event_STATUS = GetStringValue(payevent, "STATUS");
      string event_CREATION_DT = GetStringValue(payevent, "CREATION_DT");
      string event_MOD_DT = GetStringValue(payevent, "MOD_DT");
      
      // Bug #14185 Mike O - Move string building into its own function
      string checksum_data = BuildPayEventChecksumString(event_EVENTNBR, event_DEPFILENBR, event_DEPFILESEQ, event_USERID, event_STATUS, event_MOD_DT);

      return Crypto.ComputeHash("", checksum_data);
    }
    public static string CreateItemChecksum(GenericObject item)
    {
      string item_ITEMNBR = GetStringValue(item, "ITEMNBR");
      string item_TRANNBR = GetStringValue(item, "TRANNBR");
      string item_EVENTNBR = GetStringValue(item, "EVENTNBR");
      string item_DEPFILENBR = GetStringValue(item, "DEPFILENBR");
      string item_DEPFILESEQ = GetStringValue(item, "DEPFILESEQ");
      string item_ITEMID = GetStringValue(item, "ITEMID");
      string item_GLACCTNBR = GetStringValue(item, "GLACCTNBR");
      string item_ACCTID = GetStringValue(item, "ACCTID");
      string item_AMOUNT = GetStringValue(item, "AMOUNT");
      string item_QTY = GetStringValue(item, "QTY");
      string item_TAXED = GetStringValue(item, "TAXED");
      string item_TOTAL = GetStringValue(item, "TOTAL");
      
      // Bug #14185 Mike O - Move string building into its own function
      // ITEM DATA HASH:
      // ItemNbr + TranNbr + EventNbr + FileNbr + FileSeq + ItemID + GLAcctNbr + 
      // csAcctID + Amount + Qty + Taxed + Total

      string checksum_data = BuildItemChecksumString(item_ITEMNBR, item_TRANNBR, item_EVENTNBR, item_DEPFILENBR, item_DEPFILESEQ, item_ITEMID, item_GLACCTNBR,
        item_ACCTID, item_AMOUNT, item_QTY, item_TAXED, item_TOTAL);

      return Crypto.ComputeHash("", checksum_data);
    }

    // Bug #10955 Mike O
    public static string CreateItemDataChecksum(HashAlgorithm alg, params Object[] arg_pairs)
    {
      /* Matt -- this function will create a Item Data Checksum.  In addtion to the params
           listed below the user must provide one of the following: the values to 
           create a checksum in "geoChecksumData", the DbConnection info in "config" 
           or an existing connection to the database in "DbConnectionClass."
       params    strFileName			the depfilesequence + the depfilenbr
           strEventNbr			the event number
           strTranNbr			the transaction number
           strItemNbr			the item number	*/

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();
      string strTranNbr = args.get("strTranNbr").ToString();
      string strItemNbr = args.get("strItemNbr").ToString();

      string strSQL = "", err="";
      string strChecksumData = "";
      string strItemDataChecksum = "";

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      bool connectionOpened = false;    // Bug 15941
      try
      {
      if (args.has("geoChecksumData"))
      {
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else if (args.has("DbConnectionClass") || args.has("config"))
      {
        if (args.has("DbConnectionClass"))
        {
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
          config = pDBConn.GetDBConnectionInfo();
        }
        else if (args.has("config"))
        {
          config = args.get("config", c_CASL.c_undefined) as GenericObject;
          pDBConn = new ConnectionTypeSynch(config);

            if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref err)))  // Bug 15941
              HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-966", err);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        int iDBType = pDBConn.GetDBConnectionType();

        // Bug #12055 Mike O - Use misc.Parse to log errors
        // Bug 20382: FT: Converted to parameterized
        strSQL = "SELECT * FROM TG_ITEM_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND EVENTNBR=@eventnbr AND TRANNBR=@trannbr AND ITEMNBR=@itemnbr";

        Hashtable sql_args = new Hashtable();
        sql_args["depfilenbr"] = strFileNbr;
        sql_args["depfileseq"] = strFileSequenceNbr;
        sql_args["eventnbr"] = strEventNbr;
        sql_args["trannbr"] = strTranNbr;
        sql_args["itemnbr"] = strItemNbr;
        gTmp = (GenericObject)GetRecordsParameterized(config, sql_args, 0, strSQL, iDBType);
        // End Bug 20382

        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found.", "code", "TS-967",
              String.Format("Error:{0} \n SQL statement:{1} ", err, strSQL));//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a config, connection or the checksum data.", "code", "TS-968", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      string strItemID = GetKeyData(gTmp, "ITEMID", false);
      string strGLAcctNbr = GetKeyData(gTmp, "GLACCTNBR", false);
      string strAcctID = GetKeyData(gTmp, "ACCTID", false);
      string strAmount = GetKeyData(gTmp, "AMOUNT", false);
      string strQty = GetKeyData(gTmp, "QTY", false);
      string strTaxed = GetKeyData(gTmp, "TAXED", false);
      string strTotal = GetKeyData(gTmp, "TOTAL", false);

      //-----------------------------------------
      // Some validation of items.

      strTaxed.ToLower();
      if (strTaxed == "true" || strTaxed == "y")
        strTaxed = "Y";
      else
        strTaxed = "N";
      //-----------------------------------------

      // Bug #12055 Mike O - Use misc.Parse to log errors
      Decimal fConvert1 = misc.Parse<Decimal>(strAmount);// BUG19280 NEVER USE single-precision-float for currency
      string strTemp1 = string.Format("{0:f}", fConvert1);

      Decimal fConvertQty = misc.Parse<Decimal>(strQty);// BUG19280 NEVER USE single-precision-float for currency
      strQty = string.Format("{0:f}", fConvertQty);

      Decimal fConvert2 = misc.Parse<Decimal>(strTotal);// BUG19280 NEVER USE single-precision-float for currency
      string strTemp2 = string.Format("{0:f}", fConvert2);

      // ITEM DATA ENCRYPTION: 
      // ItemNbr + TranNbr + EventNbr + FileNbr + FileSeq + ItemID + GLAcctNbr + 
      // csAcctID + Amount + Qty + Taxed + Total
      
      // Bug #14185 Mike O - Move string building into its own function
      strChecksumData = BuildItemChecksumString(strItemNbr, strTranNbr, strEventNbr, strFileNbr, strFileSequenceNbr, strItemID, strGLAcctNbr,
        strAcctID, strTemp1, strQty, strTaxed, strTemp2);

      // Bug #10955 Mike O
      // Hash
      strItemDataChecksum = Crypto.ComputeHash("", strChecksumData, alg);

        return strItemDataChecksum;
      }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    // Bug #10955 Mike O
    public static string CreateSuspTranChecksum(HashAlgorithm alg, params Object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();
      string strTranNbr = args.get("strTranNbr").ToString();

      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      string checksum_data = strTranNbr + strEventNbr + strFileNbr + strFileSequenceNbr;
      // Bug #10955 Mike O
      return Crypto.ComputeHash("data", checksum_data, alg);
    }

    // Bug #10955 Mike O
    public static string CreateSuspEventChecksum(HashAlgorithm alg, params Object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();
      string strNowDateTime = args.get("strNowDateTime").ToString();
      string strUserID = args.get("strUserID").ToString();

      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      string strTemp = strEventNbr + strFileNbr + strFileSequenceNbr + strNowDateTime + strUserID;
      // Bug #10955 Mike O
      return Crypto.ComputeHash("data", strTemp, alg);
    }

    // Bug #10955 Mike O
    public static string CreateReversalEventChecksum(HashAlgorithm alg, params Object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();
      string strUserID = args.get("strUserID").ToString();
      string strReversedEventNbr = args.get("strReversedEventNbr").ToString();
      string strReversedFileName = args.get("strReversedFileName").ToString();
      string strVoided = args.get("strVoided").ToString();

      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);
      string strReversedFileNbr = GetFileNumber(strReversedFileName);
      string strReversedFileSequenceNbr = GetFileSequence(strReversedFileName);

      string strTemp = strEventNbr + strFileNbr + strFileSequenceNbr + strUserID + strReversedEventNbr + strReversedFileNbr + strReversedFileSequenceNbr + strVoided;
      // Bug #10955 Mike O
      return Crypto.ComputeHash("data", strTemp, alg);
    }

    // Bug #10955 Mike O
    public static string CreateImageDataChecksum(HashAlgorithm alg, params Object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);

      StringBuilder sb = new StringBuilder();

      string strFileName = args.get("strFileName").ToString();

      sb.Append(GetFileNumber(strFileName));
      sb.Append(GetFileSequence(strFileName));

      sb.Append(args.get("strEventNbr"));
      sb.Append(args.get("strPostNbr"));
      sb.Append(args.get("strPostType"));
      sb.Append(args.get("strSeqNbr"));
      sb.Append(args.get("strImage"));
      sb.Append(args.get("strImagingType"));
      sb.Append(args.get("strCompleteMICRLine"));
      sb.Append(args.get("strBankRoutingNbr"));
      sb.Append(args.get("strAccountNbr"));
      sb.Append(args.get("strCheckNbr"));
      sb.Append(args.get("strDateInserted"));
      sb.Append(args.get("strUserID"));
      sb.Append(args.get("strComments"));
      sb.Append(args.get("strSourceType"));
      sb.Append(args.get("strSourceGroup"));
      sb.Append(args.get("strSourceRefID"));
      sb.Append(args.get("strSourceDate"));
      sb.Append(args.get("strScanData"));
      sb.Append(args.get("strDocImageRef"));
      sb.Append(args.get("strImageFormat"));
      sb.Append(args.get("strDocID"));

      // Bug #10955 Mike O
      return Crypto.ComputeHash("data", sb.ToString(), alg);
    }

    public static bool ValidateItemDataChecksum(params Object[] arg_pairs)
    {
      /* Matt -- this function creates a checksum and compares it to the existing checksum.  
           In addtion to the params listed below the user must provide one of the 
           following: the values to create a checksum in "geoChecksumData", the 
           DbConnection info in "config" or an existing connection to the database in 
           "DbConnectionClass."
       params    strFileName			the depfilesequencenbr + the depfilenbr
           strEventNbr   		the event number 
           strTranNbr			the transaction number */

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();
      string strTranNbr = args.get("strTranNbr").ToString();

      string strSQL = "",err="";
      string strItemDataChecksum = "";
      string strOldChecksum = "";

      bool bVal = true;

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      bool connectionOpened = false;    // Bug 15941
      try
      {
      // Either the connection was passed in or we create one
      if (args.has("DbConnectionClass") || args.has("config"))
      {
        if (args.has("DbConnectionClass"))
        {
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
          config = pDBConn.GetDBConnectionInfo();
        }
        else if (args.has("config"))
        {
          config = args.get("config", c_CASL.c_undefined) as GenericObject;
          pDBConn = new ConnectionTypeSynch(config);

            if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref err)))  // Bug 15941
              HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1031", err);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        int iDBType = pDBConn.GetDBConnectionType();

        // Bug #12055 Mike O - Use misc.Parse to log errors
        strSQL = "SELECT * FROM TG_ITEM_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND EVENTNBR=@eventnbr AND TRANNBR=@trannbr";

        // Bug 20382: FT: Converted to parameterized
        Hashtable sql_args = new Hashtable();
        sql_args["depfilenbr"] = strFileNbr;
        sql_args["depfileseq"] = strFileSequenceNbr;
        sql_args["eventnbr"] = strEventNbr;
        sql_args["trannbr"] = strTranNbr;
        gTmp = (GenericObject)GetRecordsParameterized(config, sql_args, 0, strSQL, iDBType);
        // End Bug 20382:

        if (gTmp == null)
        {
          return true;
        }
      }
      else if (args.has("geoChecksumData"))
      {
        // Or we don't need a connection because the structure is already passed in
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a config, connection or the checksum data.", "code", "TS-1032", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      // Bug #10955 Mike O
      for (int i = 0; i < (int)gTmp.getLength(); i++)
      {
        strOldChecksum = GetKeyData(((GenericObject)gTmp.get(i)), "CHECKSUM", false);
        HashAlgorithm hashAlg = Crypto.HashAlgorithmUsed(strOldChecksum);

        if (hashAlg == null)
        {
          // Bug #12800 Mike O - If the checksum doesn't match any possible algorithms, fail validation
          return false;
        }

        // The newly created checksum
        //strItemDataChecksum = CreateItemDataChecksum( "strFileName", strFileName, "geoChecksumData", ((GenericObject)gTmp[i]), "strEventNbr", strEventNbr, "strTranNbr", strTranNbr, "strItemNbr",((GenericObject)gTmp[i]).get("ITEMNBR"));
        strItemDataChecksum = CreateItemDataChecksum(hashAlg, "strFileName", strFileName, "geoChecksumData", gTmp.get(i), "strEventNbr", strEventNbr, "strTranNbr", strTranNbr, "strItemNbr", ((GenericObject)gTmp.get(i)).get("ITEMNBR"));
        // End Bug #10955 Mike O

        if (strOldChecksum != strItemDataChecksum)
        {
          bVal = false;
        }
      }


      return bVal;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static bool UpdateItemDataChecksum(params Object[] arg_pairs)
    {
      /* Matt -- this function will update the deposit checksum.  In addtion to the 
           params listed below the user must provide one of the following: the 
           DbConnection info in "config" or an existing connection to the database 
           in "DbConnectionClass." The user has the option to provide a structure
           that will be used to create the checksum, if not the information will be
           gathered through the existing or newly made connection.
       params    strFileName			the depfilesequencenbr + the depfilenbr
           strEventNbr   		the event number 
           strTranNbr			the transaction number */

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;
      bool connectionOpened = false;      // Bug 15941

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();
      string strTranNbr = args.get("strTranNbr").ToString();

      string strError = "Default Error";
      string strSQL = "";
      string strItemDataChecksum = "";

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      try
      {
      if (args.has("DbConnectionClass"))
      {
        pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
        config = pDBConn.GetDBConnectionInfo();
      }
      else if (args.has("config"))
      {
        config = args.get("config", c_CASL.c_undefined) as GenericObject;
        pDBConn = new ConnectionTypeSynch(config);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strError)))  // Bug 15941. Open connection and set connectionO flagpened
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1017", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        if (!pDBConn.EstablishRollback(ref strError))
        {
          strError = "Error creating database transaction.";
            ThrowCASLErrorButDoNotCloseDBConnection("TS-1018", ref pDBConn, "Error creating database transaction.", strError);
        }
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a method to connect to the database.", "code", "TS-1019", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      if (args.has("geoChecksumData"))
      {
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else
      {
        int iDBType = pDBConn.GetDBConnectionType();
        // Bug #12055 Mike O - Use misc.Parse to log errors
        strSQL = "SELECT * FROM TG_ITEM_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND EVENTNBR=@eventnbr AND TRANNBR=@trannbr";
        // Bug 20382: FT: Converted to parameterized
        Hashtable sql_args = new Hashtable();
        sql_args["depfilenbr"] = strFileNbr;
        sql_args["depfileseq"] = strFileSequenceNbr;
        sql_args["eventnbr"] = strEventNbr;
        sql_args["trannbr"] = strTranNbr;
        gTmp = (GenericObject)GetRecordsParameterized(config, sql_args, 0, strSQL, iDBType);  

        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found. ", "code", "TS-1020",
              String.Format("Error:{0}\n SQL statement: {1}", strError, strSQL));//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
      }

      //need to LOOP through all of the custfields 
      for (int i = 0; i < (int)gTmp.getLength(); i++)
      {
        // The newly created checksum
        // Bug #10955 Mike O
        strItemDataChecksum = CreateItemDataChecksum(Crypto.DefaultHashAlgorithm, "strFileName", strFileName, "geoChecksumData", ((GenericObject)gTmp.get(i)), "strEventNbr", strEventNbr, "strTranNbr", strTranNbr, "strItemNbr", ((GenericObject)gTmp.get(i)).get("ITEMNBR"));

        // Create SQL
        // Bug #12055 Mike O - Use misc.Parse to log errors
        strSQL = string.Format("UPDATE TG_ITEM_DATA SET CHECKSUM='{0}' WHERE DEPFILENBR={1} AND DEPFILESEQ={2} AND EVENTNBR={3} AND TRANNBR={4} AND ITEMNBR={5}", strItemDataChecksum, misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr), misc.Parse<Int64>(strTranNbr), misc.Parse<Int64>(((GenericObject)gTmp.get(i)).get("ITEMNBR").ToString()));

        // Update database
        if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strError))
            HandleErrorsAndLocking("message", "Error executing SQL.", "code", "TS-1021", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

      }


      //***************************************
      // ALL GOOD - ACCEPT THE UPDATES and close the connection we created.
      if (!args.has("DbConnectionClass"))
      {
        if (!pDBConn.CommitToDatabase(ref strError))
            HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-1022", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }
      //***************************************   

      return true;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    // Bug #10955 Mike O
    public static string CreateGRCustomFieldChecksum(HashAlgorithm alg, params Object[] arg_pairs)
    {
      // Mike O - This is for GR_CUST_FIELD_DATA
      /* Matt -- this function will create a Cust Field Checksum.  In addtion to the params
           listed below the user must provide one of the following: the values to 
           create a checksum in "geoChecksumData", the DbConnection info in "config" 
           or an existing connection to the database in "DbConnectionClass."
       params    strFileName			the depfilesequence + the depfilenbr
           strEventNbr			the event number
           strTranNbr			the transaction number
           strScreenIndex		the screen number 	*/


      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();
      string strTranNbr = args.get("strTranNbr").ToString();
      string strScreenIndex = args.get("strScreenIndex").ToString();

      string strSQL = "", err="";
      string strChecksumData = "";
      string strCustFieldChecksum = "";

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      bool connectionOpened = false;    // Bug 15941
      try
      {
      if (args.has("geoChecksumData"))
      {
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else if (args.has("DbConnectionClass") || args.has("config"))
      {

        if (args.has("DbConnectionClass"))
        {
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
          config = pDBConn.GetDBConnectionInfo();
        }
        else if (args.has("config"))
        {
          config = args.get("config", c_CASL.c_undefined) as GenericObject;
          pDBConn = new ConnectionTypeSynch(config);

            if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref err)))  // Bug 15941
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-960", err);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        int iDBType = pDBConn.GetDBConnectionType();
        // Bug #12055 Mike O - Use misc.Parse to log errors
        strSQL = string.Format("SELECT * FROM GR_CUST_FIELD_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND SCREENINDEX={2} AND TRANNBR={3} AND EVENTNBR={4}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strScreenIndex), misc.Parse<Int64>(strTranNbr), misc.Parse<Int64>(strEventNbr));

        gTmp = (GenericObject)GetRecords(config, 0, strSQL, iDBType);


        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found. ", "code", "TS-961", err + "SQL statement: " + strSQL);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a config, connection or the checksum data.", "code", "TS-962", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      string strLabel = GetKeyData(gTmp, "CUSTLABEL", false);
      string strValue = GetKeyData(gTmp, "CUSTVALUE", false);

      strChecksumData = BuildGRCustFieldChecksumString(strTranNbr, strEventNbr, strFileNbr, strFileSequenceNbr, strScreenIndex, strLabel, strValue);

      // Bug #10955 Mike O
      // Hash
      strCustFieldChecksum = Crypto.ComputeHash("", strChecksumData, alg);


      return strCustFieldChecksum;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    // Bug #10955 Mike O
    public static string CreateCustomFieldChecksum(HashAlgorithm alg, params Object[] arg_pairs)
    {
      // Mike O - This is for CUST_FIELD_DATA
      /* Matt -- this function will create a Cust Field Checksum.  In addtion to the params
           listed below the user must provide one of the following: the values to 
           create a checksum in "geoChecksumData", the DbConnection info in "config" 
           or an existing connection to the database in "DbConnectionClass."
       params    strFileName			the depfilesequence + the depfilenbr
           strEventNbr			the event number
           strPostNbr			the transaction number
           strScreenIndex		the screen number 	*/


      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();
      string strPostNbr = args.get("strPostNbr").ToString();
      string strScreenIndex = args.get("strScreenIndex").ToString();

      string strSQL = "", err="";
      string strChecksumData = "";
      string strCustFieldChecksum = "";

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      // Bug 15941. FT. Make sure the database connection is closed
      bool connectionOpened = false;    // Bug 15941
      try
      {
      if (args.has("geoChecksumData"))
      {
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else if (args.has("DbConnectionClass") || args.has("config"))
      {

        if (args.has("DbConnectionClass"))
        {
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
          config = pDBConn.GetDBConnectionInfo();
        }
        else if (args.has("config"))
        {
          config = args.get("config", c_CASL.c_undefined) as GenericObject;
          pDBConn = new ConnectionTypeSynch(config);

            if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref err)))  // Bug 15941
              HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-960", err);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        int iDBType = pDBConn.GetDBConnectionType();
        // Bug #12055 Mike O - Use misc.Parse to log errors
        strSQL = string.Format("SELECT * FROM CUST_FIELD_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND SCREENINDEX={2} AND POSTNBR={3} AND EVENTNBR={4}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strScreenIndex), misc.Parse<Int64>(strPostNbr), misc.Parse<Int64>(strEventNbr));

        gTmp = (GenericObject)GetRecords(config, 0, strSQL, iDBType);


        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found. ", "code", "TS-961", "SQL statement: " + strSQL);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a config, connection or the checksum data.", "code", "TS-962", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      string strLabel = GetKeyData(gTmp, "CUSTLABEL", false);
      string strValue = GetKeyData(gTmp, "CUSTVALUE", false);

      // Bug #12055 Mike O - Use misc.Parse to log errors
      strChecksumData = misc.Parse<Int32>(strPostNbr).ToString();
      strChecksumData += misc.Parse<Int32>(strEventNbr).ToString();
      strChecksumData += misc.Parse<Int32>(strFileNbr).ToString();
      strChecksumData += misc.Parse<Int32>(strFileSequenceNbr).ToString();
      strChecksumData += misc.Parse<Int32>(strScreenIndex).ToString();
      strChecksumData += strLabel;
      strChecksumData += strValue;

      // Bug #10955 Mike O
      // Hash
      strCustFieldChecksum = Crypto.ComputeHash("", strChecksumData, alg);

        return strCustFieldChecksum;
      }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static bool ValidateGRCustFieldChecksum(params Object[] arg_pairs)
    {
      /* Matt -- this function creates a checksum and compares it to the existing checksum.  
           In addtion to the params listed below the user must provide one of the 
           following: the values to create a checksum in "geoChecksumData", the 
           DbConnection info in "config" or an existing connection to the database in 
           "DbConnectionClass."
        params   strFileName			the depfilesequence + the depfilenbr
           strEventNbr			the event number
           strTranNbr			the transaction number */

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();
      string strTranNbr = args.get("strTranNbr").ToString();

      string strSQL = "", err="";
      string strCustFieldChecksum = "";
      string strOldChecksum = "";

      bool bVal = true;

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      bool connectionOpened = false;    // Bug 15941
      try
      {
      // Either the connection was passed in or we create one
      if (args.has("DbConnectionClass") || args.has("config"))
      {
        if (args.has("DbConnectionClass"))
        {
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
          config = pDBConn.GetDBConnectionInfo();
        }
        else
        {
          config = args.get("config", c_CASL.c_undefined) as GenericObject;
          pDBConn = new ConnectionTypeSynch(config);

            if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref err)))  // Bug 15941
              HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1023", err);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        int iDBType = pDBConn.GetDBConnectionType();
        // Bug #12055 Mike O - Use misc.Parse to log errors
        // Bug 20382: FT: Converted to parameterized 
        strSQL = "SELECT * FROM GR_CUST_FIELD_DATA WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND EVENTNBR=@eventnbr AND TRANNBR=@trannbr";
        Hashtable sql_args = new Hashtable();
        sql_args["depfilenbr"] = strFileNbr;
        sql_args["depfileseq"] = strFileSequenceNbr;
        sql_args["eventnbr"] = strEventNbr;
        sql_args["trannbr"] = strTranNbr;
        gTmp = (GenericObject)GetRecordsParameterized(config, sql_args, 1, strSQL, iDBType);
        // End Bug 20382

        if (gTmp == null)
        {
          // In case theres an no records, we need to close the connection
          return true;
        }
      }
      else if (args.has("geoChecksumData"))
      {
        // Or we don't need a connection because the structure is already passed in
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a config, connection or the checksum data.", "code", "TS-1024", ""); // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      //need to LOOP through all of the custfields 
      for (int i = 0; i < (int)gTmp.getLength(); i++)
      {
        // Bug #10955 Mike O
        strOldChecksum = GetKeyData(((GenericObject)gTmp.get(i)), "CHECKSUM", false);
        HashAlgorithm hashAlg = Crypto.HashAlgorithmUsed(strOldChecksum);

        if (hashAlg == null)
        {
          // Bug #12800 Mike O - If the checksum doesn't match any possible algorithms, fail validation
          return false;
        }

        // The newly created checksum
        strCustFieldChecksum = CreateGRCustomFieldChecksum(hashAlg, "strFileName", strFileName, "geoChecksumData", ((GenericObject)gTmp.get(i)), "strEventNbr", strEventNbr, "strTranNbr", strTranNbr, "strScreenIndex", ((GenericObject)gTmp.get(i)).get("SCREENINDEX"));
        // End Bug #10955 Mike O
        if (strOldChecksum != strCustFieldChecksum)
        {
          bVal = false;
        }
      }
        return bVal;
      }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static bool ValidateCustFieldChecksum(params Object[] arg_pairs)
    {
      /* Matt -- this function creates a checksum and compares it to the existing checksum.  
           In addtion to the params listed below the user must provide one of the 
           following: the values to create a checksum in "geoChecksumData", the 
           DbConnection info in "config" or an existing connection to the database in 
           "DbConnectionClass."
        params   strFileName			the depfilesequence + the depfilenbr
           strEventNbr			the event number
           strPostNbr			the post number */

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();
      string strPostNbr = args.get("strPostNbr").ToString();

      string strSQL = "",err="";
      string strCustFieldChecksum = "";
      string strOldChecksum = "";

      bool bVal = true;

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);
      bool connectionOpened = false;    // Bug 15941
      try
      {
      // Either the connection was passed in or we create one
      if (args.has("DbConnectionClass") || args.has("config"))
      {
        if (args.has("DbConnectionClass"))
        {
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
          config = pDBConn.GetDBConnectionInfo();
        }
        else
        {
          config = args.get("config", c_CASL.c_undefined) as GenericObject;
          pDBConn = new ConnectionTypeSynch(config);

            if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref err)))  // Bug 15941
              HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1023", err);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        int iDBType = pDBConn.GetDBConnectionType();
        // Bug #12055 Mike O - Use misc.Parse to log errors
        strSQL = string.Format("SELECT * FROM CUST_FIELD_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} AND POSTNBR={3}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr), misc.Parse<Int64>(strPostNbr));

        gTmp = (GenericObject)GetRecords(config, 1, strSQL, iDBType);

        if (gTmp == null)
        {
          return true;
        }
      }
      else if (args.has("geoChecksumData"))
      {
        // Or we don't need a connection because the structure is already passed in
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a config, connection or the checksum data.", "code", "TS-1024", ""); // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      //need to LOOP through all of the custfields 
      for (int i = 0; i < (int)gTmp.getLength(); i++)
      {
        strOldChecksum = GetKeyData(((GenericObject)gTmp.get(i)), "CHECKSUM", false);
        // Bug #10955 Mike O
        HashAlgorithm hashAlg = Crypto.HashAlgorithmUsed(strOldChecksum);

        if (hashAlg == null)
        {
          // Bug #12800 Mike O - If the checksum doesn't match any possible algorithms, fail validation
          return false;
        }

        // Bug #10955 Mike O - Determine algorithm of existing checksum
        // The newly created checksum
        strCustFieldChecksum = CreateCustomFieldChecksum(hashAlg, "strFileName", strFileName, "geoChecksumData", ((GenericObject)gTmp.get(i)), "strEventNbr", strEventNbr, "strPostNbr", strPostNbr, "strScreenIndex", ((GenericObject)gTmp.get(i)).get("SCREENINDEX"));
        // End Bug #10955 Mike O
        if (strOldChecksum != strCustFieldChecksum)
        {
          bVal = false;
        }
      }
      //***************************************

      return bVal;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static bool UpdateGRCustFieldChecksum(params Object[] arg_pairs)
    {
      /* Matt -- this function will update the cust field checksum.  In addtion to the 
           params listed below the user must provide one of the following: the 
           DbConnection info in "config" or an existing connection to the database 
           in "DbConnectionClass." The user has the option to provide a structure
           that will be used to create the checksum, if not the information will be
           gathered through the existing or newly made connection.
       params    strFileName			the depfilesequence + the depfilenbr
           strEventNbr			the event number
           strTranNbr			the transaction number */

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;
      bool connectionOpened = false;      // Bug 15941

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();
      string strTranNbr = args.get("strTranNbr").ToString();

      string strError = "Default Error";
      string strScreenIndex = "";
      string strSQL = "";
      string strCustFieldChecksum = "";

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);
      // Bug 15941. FT. Make sure the database connection is closed
      try
      {
      if (args.has("DbConnectionClass"))
      {
        pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
        config = pDBConn.GetDBConnectionInfo();
      }
      else if (args.has("config"))
      {
        config = args.get("config", c_CASL.c_undefined) as GenericObject;
        pDBConn = new ConnectionTypeSynch(config);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strError)))  // Bug 15941. Open connection and set connectionO flagpened
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-981", strError);//Bug 13832-Added detailed error message

        if (!pDBConn.EstablishRollback(ref strError))
        {
          strError = "Error creating database transaction.";
            ThrowCASLErrorButDoNotCloseDBConnection("TS-982", ref pDBConn, "Error creating database transaction.", strError);
        }
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a method to connect to the database.", "code", "TS-983", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      if (args.has("geoChecksumData"))
      {
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else
      {
        int iDBType = pDBConn.GetDBConnectionType();
        strSQL = string.Format("SELECT * FROM GR_CUST_FIELD_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} AND TRANNBR={3}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr), misc.Parse<Int64>(strTranNbr));

        gTmp = (GenericObject)GetRecords(config, 1, strSQL, iDBType);

        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found. ", "code", "TS-984", "SQL statement: " + strSQL + strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
      }

      //need to LOOP through all of the custfields 
      for (int i = 0; i < (int)gTmp.getLength(); i++)
      {
        strScreenIndex = ((GenericObject)gTmp.get(i)).get("SCREENINDEX").ToString();
        // Bug #10955 Mike O
        // The newly created checksum
        strCustFieldChecksum = CreateGRCustomFieldChecksum(Crypto.DefaultHashAlgorithm, "strFileName", strFileName, "geoChecksumData", ((GenericObject)gTmp.get(i)), "strEventNbr", strEventNbr, "strTranNbr", strTranNbr, "strScreenIndex", strScreenIndex);

        // Create SQL
        // Bug #12055 Mike O - Use misc.Parse to log errors
        strSQL = string.Format("UPDATE GR_CUST_FIELD_DATA SET CHECKSUM='{0}' WHERE DEPFILENBR={1} AND DEPFILESEQ={2} AND EVENTNBR={3} AND TRANNBR={4} AND SCREENINDEX={5}", strCustFieldChecksum, misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr), misc.Parse<Int64>(strTranNbr), misc.Parse<Int64>(strScreenIndex));

        // Update database
        if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strError))
            HandleErrorsAndLocking("message", "Error executing SQL.", "code", "TS-985", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      //***************************************
      // ALL GOOD - ACCEPT THE UPDATES and close the connection we created.
      if (!args.has("DbConnectionClass"))
      {
        if (!pDBConn.CommitToDatabase(ref strError))
            HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-786", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix


      }
      //***************************************   

      return true;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static bool UpdateCustFieldChecksum(params Object[] arg_pairs)
    {
      /* Matt -- this function will update the cust field checksum.  In addtion to the 
           params listed below the user must provide one of the following: the 
           DbConnection info in "config" or an existing connection to the database 
           in "DbConnectionClass." The user has the option to provide a structure
           that will be used to create the checksum, if not the information will be
           gathered through the existing or newly made connection.
       params    strFileName			the depfilesequence + the depfilenbr
           strEventNbr			the event number
           strTranNbr			the transaction number */

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;
      bool connectionOpened = false;        // Bug 15941

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();
      string strPostNbr = args.get("strPostNbr").ToString();

      string strError = "Default Error";
      string strScreenIndex = "";
      string strSQL = "";
      string strCustFieldChecksum = "";

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);
      // Bug 15941. FT. Make sure the database connection is closed
      try
      {
      if (args.has("DbConnectionClass"))
      {
        pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
        config = pDBConn.GetDBConnectionInfo();
      }
      else if (args.has("config"))
      {
        config = args.get("config", c_CASL.c_undefined) as GenericObject;
        pDBConn = new ConnectionTypeSynch(config);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strError)))  // Bug 15941. Open connection and set connectionO flagpened
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-981", strError);//Bug 13832-Added detailed error message

        if (!pDBConn.EstablishRollback(ref strError))
        {
          strError = "Error creating database transaction.";
            ThrowCASLErrorButDoNotCloseDBConnection("TS-982", ref pDBConn, "Error creating database transaction.", strError);
        }
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a method to connect to the database.", "code", "TS-983", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      if (args.has("geoChecksumData"))
      {
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else
      {
        int iDBType = pDBConn.GetDBConnectionType();
        strSQL = string.Format("SELECT * FROM CUST_FIELD_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} AND POSTNBR={3}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr), misc.Parse<Int64>(strPostNbr));

        gTmp = (GenericObject)GetRecords(config, 1, strSQL, iDBType);

        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found. ", "code", "TS-984", "SQL statement: " + strSQL);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
      }

      //need to LOOP through all of the custfields 
      for (int i = 0; i < (int)gTmp.getLength(); i++)
      {
        strScreenIndex = ((GenericObject)gTmp.get(i)).get("SCREENINDEX").ToString();
        // Bug #10955 Mike O
        // The newly created checksum
        strCustFieldChecksum = CreateCustomFieldChecksum(Crypto.DefaultHashAlgorithm, "strFileName", strFileName, "geoChecksumData", ((GenericObject)gTmp.get(i)), "strEventNbr", strEventNbr, "strPostNbr", strPostNbr, "strScreenIndex", strScreenIndex);

        // Create SQL
        // Bug #12055 Mike O - Use misc.Parse to log errors
        strSQL = string.Format("UPDATE CUST_FIELD_DATA SET CHECKSUM='{0}' WHERE DEPFILENBR={1} AND DEPFILESEQ={2} AND EVENTNBR={3} AND POSTNBR={4} AND SCREENINDEX={5}", strCustFieldChecksum, misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr), misc.Parse<Int64>(strPostNbr), misc.Parse<Int64>(strScreenIndex));

        // Update database
        if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strError))
            HandleErrorsAndLocking("message", "Error executing SQL.", "code", "TS-985", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      //***************************************
      // ALL GOOD - ACCEPT THE UPDATES and close the connection we created.
      if (!args.has("DbConnectionClass"))
      {
        if (!pDBConn.CommitToDatabase(ref strError))
            HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-786", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }
      //***************************************   

      return true;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    // Bug #10955 Mike O
    public static string CreateTranFileChecksum(HashAlgorithm alg, params Object[] arg_pairs)
    {
      /* Matt -- this function will create a tran file Checksum.  In addtion to the params
           listed below the user must provide one of the following: the values to 
           create a checksum in "geoChecksumData", the DbConnection info in "config" 
           or an existing connection to the database in "DbConnectionClass."
       params    strFileName			the depfilesequence + the depfilenbr */

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;

      string strFileName = args.get("strFileName").ToString();

      string strSQL = "", err="";
      string strChecksumData = "";
      string strTranFileChecksum = "";

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      // Bug 15941. FT. Make sure the database connection is closed
      bool connectionOpened = false;
      try
      {
      if (args.has("geoChecksumData"))
      {
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else if (args.has("DbConnectionClass") || args.has("config"))
      {
        if (args.has("DbConnectionClass"))
        {
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
          config = pDBConn.GetDBConnectionInfo();
        }
        else if (args.has("config"))
        {
          config = args.get("config", c_CASL.c_undefined) as GenericObject;
          pDBConn = new ConnectionTypeSynch(config);

            if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref err)))  // Bug 15941
              HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-978", err);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        int iDBType = pDBConn.GetDBConnectionType();
        // Bug #12055 Mike O - Use misc.Parse to log errors
        strSQL = string.Format("SELECT * FROM TG_DEPFILE_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr));

        gTmp = (GenericObject)GetRecords(config, 0, strSQL, iDBType);

        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found. ", "code", "TS-979", err + "\n SQL statement: " + strSQL);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a config, connection or the checksum data.", "code", "TS-980", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      string DEPTID = GetKeyData(gTmp, "DEPTID", false);
      string OPENDT = GetKeyData(gTmp, "OPENDT", false);
      string BALDT = GetKeyData(gTmp, "BALDT", false);
      string ACCDT = GetKeyData(gTmp, "ACCDT", false);
      string UPDATEDT = GetKeyData(gTmp, "UPDATEDT", false);
      string OPEN_USERID = GetKeyData(gTmp, "OPEN_USERID", false);
      string BAL_USERID = GetKeyData(gTmp, "BAL_USERID", false);
      string ACC_USERID = GetKeyData(gTmp, "ACC_USERID", false);
      string UPDATE_USERID = GetKeyData(gTmp, "UPDATE_USERID", false);
      string GROUPID = GetKeyData(gTmp, "GROUPID", false);
      string GROUPNBR = GetKeyData(gTmp, "GROUPNBR", false);
      string BALTOTAL = GetKeyData(gTmp, "BALTOTAL", false);

      strChecksumData += strFileNbr;
      strChecksumData += strFileSequenceNbr;

      strChecksumData += DEPTID;

      //Below is the date formatting - MM/dd/yyyy HH/mm/ss
      //strChecksumData += OPENDT;
      DateTime dtDate;
      if (OPENDT != null && OPENDT != "")
      {
        // Bug #12055 Mike O - Use misc.Parse to log errors
        dtDate = misc.Parse<DateTime>(OPENDT);
        strChecksumData += dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo);
      }
      else
        strChecksumData += OPENDT;

      //strChecksumData += BALDT;
      if (BALDT != null && BALDT != "")
      {
        // Bug #12055 Mike O - Use misc.Parse to log errors
        dtDate = misc.Parse<DateTime>(BALDT);
        strChecksumData += dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo);
      }
      else
        strChecksumData += BALDT;

      //strChecksumData += ACCDT;
      if (ACCDT != null && ACCDT != "")
      {
        // Bug #12055 Mike O - Use misc.Parse to log errors
        dtDate = misc.Parse<DateTime>(ACCDT);
        strChecksumData += dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo);
      }
      else
        strChecksumData += ACCDT;

      //strChecksumData += UPDATEDT;
      if (UPDATEDT != null && UPDATEDT != "")
      {
        // Bug #12055 Mike O - Use misc.Parse to log errors
        dtDate = misc.Parse<DateTime>(UPDATEDT);
        strChecksumData += dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo);
      }
      else
        strChecksumData += UPDATEDT;


      strChecksumData += OPEN_USERID;
      strChecksumData += BAL_USERID;
      strChecksumData += ACC_USERID;
      strChecksumData += UPDATE_USERID;
      strChecksumData += GROUPID;

      //GROUPNBR and BALTOTAL is only to be added if they are greater then 0
      if (GROUPNBR != null && GROUPNBR != "")
      {
        // Bug #12055 Mike O - Use misc.Parse to log errors
        if( misc.Parse<Int32>(GROUPNBR) > 0 )
        {
          strChecksumData += GROUPNBR;
        }
      }


      //strChecksumData += BALTOTAL;
      if (BALTOTAL != null && BALTOTAL != "")
      {
        // Bug #12055 Mike O - Use misc.Parse to log errors
        if( misc.Parse<Decimal>(BALTOTAL) > 0 )
        {
          strChecksumData += BALTOTAL;
        }
      }

      // Bug #10955 Mike O
      // Hash
      strTranFileChecksum = Crypto.ComputeHash("", strChecksumData, alg);


      return strTranFileChecksum;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static bool ValidateTranFileChecksum(params Object[] arg_pairs)
    {
      /* Matt -- this function creates a checksum and compares it to the existing checksum.  
           In addtion to the params listed below the user must provide one of the 
           following: the values to create a checksum in "geoChecksumData", the 
           DbConnection info in "config" or an existing connection to the database in 
           "DbConnectionClass."
        params   strFileName			the depfilesequence + the depfilenbr */

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;

      string strFileName = args.get("strFileName").ToString();

      string strSQL = "",err="";
      string strTranFileChecksum = "";
      string strOldChecksum = "";

      bool bVal = false;

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      // Bug 15941. FT. Make sure the database connection is closed
      bool connectionOpened = false;    // Bug 15941
      try
      {
      // Either the connection was passed in or we create one
      if (args.has("DbConnectionClass") || args.has("config"))
      {
        if (args.has("DbConnectionClass"))
        {
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
          config = pDBConn.GetDBConnectionInfo();
        }
        else if (args.has("config"))
        {
          config = args.get("config", c_CASL.c_undefined) as GenericObject;
          pDBConn = new ConnectionTypeSynch(config);

            if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref err)))  // Bug 15941
              HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1039", err);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        int iDBType = pDBConn.GetDBConnectionType();
        // Bug #12055 Mike O - Use misc.Parse to log errors
        strSQL = string.Format("SELECT * FROM TG_DEPFILE_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr));

        gTmp = (GenericObject)GetRecords(config, 0, strSQL, iDBType);

        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found. ", "code", "TS-1040", err + "\n SQL statement: " + strSQL);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
      }
      else if (args.has("geoChecksumData"))
      {
        // Or we don't need a connection because the structure is already passed in
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a config, connection or the checksum data.", "code", "TS-1041", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      // Bug #10955 Mike O
      strOldChecksum = GetKeyData(gTmp, "CHECKSUM", false);
      HashAlgorithm hashAlg = Crypto.HashAlgorithmUsed(strOldChecksum);

      if (hashAlg == null)
      {
        // Bug #12800 Mike O - If the checksum doesn't match any possible algorithms, fail validation
        return false;
      }

      // The newly created checksum
      strTranFileChecksum = CreateTranFileChecksum(hashAlg, "strFileName", strFileName, "geoChecksumData", gTmp);
      // End Bug #10955 Mike O



      if (strOldChecksum == strTranFileChecksum)
      {
        bVal = true;
      }

      return bVal;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static bool UpdateTranFileChecksum(params Object[] arg_pairs)
    {
      /* Matt -- this function will update the tran file checksum.  In addtion to the 
           params listed below the user must provide one of the following: the 
           DbConnection info in "config" or an existing connection to the database 
           in "DbConnectionClass." The user has the option to provide a structure
           that will be used to create the checksum, if not the information will be
           gathered through the existing or newly made connection.
        params   strFileName			the depfilesequence + the depfilenbr */

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject data = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;
      bool connectionOpened = false;      // Bug 15941

      string strFileName = args.get("strFileName").ToString();

      string strError = "Default Error";
      string strSQL = "";
      string strTranFileChecksum = "";

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      // Bug 15941. FT. Make sure the database connection is closed
      try
      {
      if (args.has("DbConnectionClass"))
      {
        pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
        data = pDBConn.GetDBConnectionInfo();
      }
      else if (args.has("data"))
      {
        data = args.get("data", c_CASL.c_undefined) as GenericObject;
        pDBConn = new ConnectionTypeSynch(data);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strError)))  // Bug 15941. Open connection and set connectionO flagpened
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1011", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        if (!pDBConn.EstablishRollback(ref strError))
        {
          strError = "Error creating database transaction.";
            ThrowCASLErrorButDoNotCloseDBConnection("TS-1012", ref pDBConn, "Error creating database transaction.", strError);
        }
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a method to connect to the database.", "code", "TS-1013", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }


      if (args.has("geoChecksumData"))
      {
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else
      {
        int iDBType = pDBConn.GetDBConnectionType();
        // Bug #12055 Mike O - Use misc.Parse to log errors
        strSQL = string.Format("SELECT * FROM TG_DEPFILE_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr));

        gTmp = (GenericObject)GetRecords(data, 0, strSQL, iDBType);

        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found. ", "code", "TS-1014",
              String.Format("Error:{0}\n SQL statement:{1}", strError, strSQL));//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

      }

      // Bug #10955 Mike O
      strTranFileChecksum = CreateTranFileChecksum(Crypto.DefaultHashAlgorithm, "strFileName", strFileName, "geoChecksumData", gTmp);

      // Create SQL
      // Bug #12055 Mike O - Use misc.Parse to log errors
      strSQL = string.Format("UPDATE TG_DEPFILE_DATA SET CHECKSUM='{0}' WHERE DEPFILENBR={1} AND DEPFILESEQ={2}", strTranFileChecksum, misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr));

      // Update database
      if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strError))
          HandleErrorsAndLocking("message", "Error executing SQL.", "code", "TS-1015", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix


      //***************************************
      // ALL GOOD - ACCEPT THE UPDATES and close the connection we created.
      if (!args.has("DbConnectionClass"))
      {
        if (!pDBConn.CommitToDatabase(ref strError))
            HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-1016", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }
      //***************************************  

      return true;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static bool UpdateSuspTranChecksum(params Object[] arg_pairs)
    {
      /* Matt -- this function will update the suspended transaction checksum.  In addtion to the 
           params listed below the user must provide one of the following: the 
           DbConnection info in "config" or an existing connection to the database 
           in "DbConnectionClass." The user has the option to provide a structure
           that will be used to create the checksum, if not the information will be
           gathered through the existing or newly made connection.
        params   strFileName			the depfilesequence + the depfilenbr */

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject data = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;
      bool connectionOpened = false;        // Bug 15941

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();
      string strTranNbr = args.get("strTranNbr").ToString();

      string strError = "Default Error";
      string strSQL = "";
      string strSuspTranChecksum = "";

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      // Bug 15941. FT. Make sure the database connection is closed
      try
      {
      if (args.has("DbConnectionClass"))
      {
        pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
        data = pDBConn.GetDBConnectionInfo();
      }
      else if (args.has("data"))
      {
        data = args.get("data", c_CASL.c_undefined) as GenericObject;
        pDBConn = new ConnectionTypeSynch(data);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strError)))  // Bug 15941. Open connection and set connectionO flagpened
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1011", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        if (!pDBConn.EstablishRollback(ref strError))
        {
          strError = "Error creating database transaction.";
            ThrowCASLErrorButDoNotCloseDBConnection("TS-1012", ref pDBConn, "Error creating database transaction.", strError);
        }
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a method to connect to the database.", "code", "TS-1013", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }


      if (args.has("geoChecksumData"))
      {
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else
      {
        int iDBType = pDBConn.GetDBConnectionType();
        // Bug #12055 Mike O - Use misc.Parse to log errors
        strSQL = string.Format("SELECT * FROM TG_SUSPTRAN_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} AND TRANNBR={3}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr), misc.Parse<Int64>(strTranNbr));
        gTmp = (GenericObject)GetRecords(data, 0, strSQL, iDBType);

        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found.", "code", "TS-1014",
              String.Format("Error:{0}\n SQL statement:{1} ", strError, strSQL));//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

      }

      // Bug #10955 Mike O
      strSuspTranChecksum = CreateSuspTranChecksum(Crypto.DefaultHashAlgorithm, "strFileName", strFileName, "strEventNbr", strEventNbr, "strTranNbr", strTranNbr);

      // Create SQL
      // Bug #12055 Mike O - Use misc.Parse to log errors
      strSQL = string.Format("UPDATE TG_SUSPTRAN_DATA SET CHECKSUM='{0}' WHERE DEPFILENBR={1} AND DEPFILESEQ={2} AND EVENTNBR={3} AND TRANNBR={4}", strSuspTranChecksum, misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr), misc.Parse<Int64>(strTranNbr));

      // Update database
      if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strError))
          HandleErrorsAndLocking("message", "Error executing SQL.", "code", "TS-1015", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix


      //***************************************
      // ALL GOOD - ACCEPT THE UPDATES and close the connection we created.
      if (!args.has("DbConnectionClass"))
      {
        if (!pDBConn.CommitToDatabase(ref strError))
            HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-1016", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }
      //***************************************  

      return true;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static bool ValidateSuspTranChecksum(params Object[] arg_pairs)
    {
      /* Matt -- this function creates a checksum and compares it to the existing checksum.  
             In addtion to the params listed below the user must provide one of the 
             following: the values to create a checksum in "geoChecksumData", the 
             DbConnection info in "config" or an existing connection to the database in 
             "DbConnectionClass."
        params   strFileName			the depfilesequence + the depfilenbr 
             strEventNbr			the event nbr
             strTranNbr			the tender nbr */

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();
      string strTranNbr = args.get("strTranNbr").ToString();

      string strSQL = "",err="";
      string strSuspTranChecksum = "";
      string strOldChecksum = "";

      bool bVal = false;

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      // Bug 15941. FT. Make sure the database connection is closed
      bool connectionOpened = false;    // Bug 15941
      try
      {
      // Either the connection was passed in or we create one
      if (args.has("DbConnectionClass") || args.has("config"))
      {
        if (args.has("DbConnectionClass"))
        {
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
          config = pDBConn.GetDBConnectionInfo();
        }
        else if (args.has("config"))
        {
          config = args.get("config", c_CASL.c_undefined) as GenericObject;
          pDBConn = new ConnectionTypeSynch(config);

            if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref err)))  // Bug 15941
              HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1036", err);//Bug 13832-Added detailed error message // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        int iDBType = pDBConn.GetDBConnectionType();

        // Bug #12055 Mike O - Use misc.Parse to log errors
        if (pDBConn.GetDBConnectionType() == ConnectionTypeSynch.m_iCurrentDatabase_SqlServer)
          strSQL = string.Format("SELECT * FROM TG_SUSPTRAN_DATA (NOLOCK) WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} AND TRANNBR={3}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr), misc.Parse<Int64>(strTranNbr));
        else
          strSQL = string.Format("SELECT * FROM TG_SUSPTRAN_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} AND TRANNBR={3}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr), misc.Parse<Int64>(strTranNbr));

        gTmp = (GenericObject)GetRecords(config, 0, strSQL, iDBType);

        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found. ", "code", "TS-1037",
              String.Format("Error:{0}\n SQL statement: {1}", err, strSQL));//Bug 13832-Added detailed error message // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
      }
      else if (args.has("geoChecksumData"))
      {
        // Or we don't need a connection because the structure is already passed in
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a config, connection or the checksum data.", "code", "TS-1038", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      // Bug #10955 Mike O
      strOldChecksum = GetKeyData(gTmp, "CHECKSUM", false);
      HashAlgorithm hashAlg = Crypto.HashAlgorithmUsed(strOldChecksum);

      if (hashAlg == null)
      {
        // Bug #12800 Mike O - If the checksum doesn't match any possible algorithms, fail validation
        return false;
      }

      // The newly created checksum
      strSuspTranChecksum = CreateSuspTranChecksum(hashAlg, "strEventNbr", strEventNbr, "strFileName", strFileName, "strTranNbr", strTranNbr, "geoChecksumData", gTmp);
      // End Bug #10955 Mike O



      if (strOldChecksum == strSuspTranChecksum)
      {
        bVal = true;
      }

      return bVal;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static bool UpdateSuspEventChecksum(params Object[] arg_pairs)
    {
      /* Matt -- this function will update the suspended event checksum.  In addtion to the 
           params listed below the user must provide one of the following: the 
           DbConnection info in "config" or an existing connection to the database 
           in "DbConnectionClass." The user has the option to provide a structure
           that will be used to create the checksum, if not the information will be
           gathered through the existing or newly made connection.
        params   strFileName			the depfilesequence + the depfilenbr */

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject data = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;
      bool connectionOpened = false;

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();

      string strError = "Default Error";
      string strSQL = "";
      string strSuspEventChecksum = "";

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      // Bug 15941. FT. Make sure the database connection is closed
      try
      {
      if (args.has("DbConnectionClass"))
      {
        pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
        data = pDBConn.GetDBConnectionInfo();
      }
      else if (args.has("data"))
      {
        data = args.get("data", c_CASL.c_undefined) as GenericObject;
        pDBConn = new ConnectionTypeSynch(data);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strError)))  // Bug 15941. Open connection and set connectionO flagpened
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1011", strError);//Bug 13832-Added detailed error message // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        if (!pDBConn.EstablishRollback(ref strError))
        {
          strError = "Error creating database transaction.";
            ThrowCASLErrorButDoNotCloseDBConnection("TS-1012", ref pDBConn, "Error creating database transaction.", strError);
        }
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a method to connect to the database.", "code", "TS-1013", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }


      if (args.has("geoChecksumData"))
      {
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else
      {
        int iDBType = pDBConn.GetDBConnectionType();
        // Bug #12055 Mike O - Use misc.Parse to log errors
        strSQL = string.Format("SELECT * FROM TG_SUSPEVENT_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr));
        gTmp = (GenericObject)GetRecords(data, 0, strSQL, iDBType);

        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found. ", "code", "TS-1014", 
              String.Format("Error:{0} \n SQL statement:{1} ",strError, strSQL));// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

      }

      // Bug #10955 Mike O
      // Bug #12055 Mike O - Use misc.Parse to log errors
      strSuspEventChecksum = CreateSuspEventChecksum(Crypto.DefaultHashAlgorithm, "strFileName", strFileName, "strEventNbr", strEventNbr, "strNowDateTime", misc.Parse<DateTime>(gTmp.get("SUSPEND_DT").ToString()).ToString("MM/dd/yyyy HH:mm:ss"), "strUserID", gTmp.get("USERID").ToString());

     
      // Create SQL
      // Bug #12055 Mike O - Use misc.Parse to log errors
      strSQL = string.Format("UPDATE TG_SUSPEVENT_DATA SET CHECKSUM='{0}' WHERE DEPFILENBR={1} AND DEPFILESEQ={2} AND EVENTNBR={3}", strSuspEventChecksum, misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr));

      // Update database
      if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strError))
          HandleErrorsAndLocking("message", "Error executing SQL.", "code", "TS-1015", strError);//Bug 13832-Added detailed error message // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix


      //***************************************
      // ALL GOOD - ACCEPT THE UPDATES and close the connection we created.
      if (!args.has("DbConnectionClass"))
      {
        if (!pDBConn.CommitToDatabase(ref strError))
            HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-1016", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

      }
      //***************************************  

      return true;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static bool ValidateSuspEventChecksum(params Object[] arg_pairs)
    {
      /* Matt -- this function creates a checksum and compares it to the existing checksum.  
             In addtion to the params listed below the user must provide one of the 
             following: the values to create a checksum in "geoChecksumData", the 
             DbConnection info in "config" or an existing connection to the database in 
             "DbConnectionClass."
        params   strFileName			the depfilesequence + the depfilenbr 
             strEventNbr			the event nbr */

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();

      string strSQL = "",err="";
      string strSuspEventChecksum = "";
      string strOldChecksum = "";

      bool bVal = false;

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      // Bug 15941. FT. Make sure the database connection is closed
      bool connectionOpened = false;    // Bug 15941
      try
      {
      // Either the connection was passed in or we create one
      if (args.has("DbConnectionClass") || args.has("config"))
      {
        if (args.has("DbConnectionClass"))
        {
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
          config = pDBConn.GetDBConnectionInfo();
        }
        else if (args.has("config"))
        {
          config = args.get("config", c_CASL.c_undefined) as GenericObject;
          pDBConn = new ConnectionTypeSynch(config);

            if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref err)))  // Bug 15941
              HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1036", err);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        int iDBType = pDBConn.GetDBConnectionType();

        // Bug #12055 Mike O - Use misc.Parse to log errors
        if (pDBConn.GetDBConnectionType() == ConnectionTypeSynch.m_iCurrentDatabase_SqlServer)
          strSQL = string.Format("SELECT * FROM TG_SUSPEVENT_DATA (NOLOCK) WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr));
        else
          strSQL = string.Format("SELECT * FROM TG_SUSPEVENT_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr));

        gTmp = (GenericObject)GetRecords(config, 0, strSQL, iDBType);

        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found.", "code", "TS-1037", err + "\n SQL statement: " + strSQL);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
      }
      else if (args.has("geoChecksumData"))
      {
        // Or we don't need a connection because the structure is already passed in
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a config, connection or the checksum data.", "code", "TS-1038", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      // Bug #10955 Mike O
      strOldChecksum = GetKeyData(gTmp, "CHECKSUM", false);
      HashAlgorithm hashAlg = Crypto.HashAlgorithmUsed(strOldChecksum);

      if (hashAlg == null)
      {
        // Bug #12800 Mike O - If the checksum doesn't match any possible algorithms, fail validation
        return false;
      }

      // The newly created checksum
      // Bug #12055 Mike O - Use misc.Parse to log errors
      strSuspEventChecksum = CreateSuspEventChecksum(hashAlg, "strFileName", strFileName, "strEventNbr", strEventNbr, "strNowDateTime", misc.Parse<DateTime>(gTmp.get("SUSPEND_DT").ToString()).ToString("MM/dd/yyyy HH:mm:ss"), "strUserID", gTmp.get("USERID").ToString());
      // End Bug #10955 Mike O


      if (strOldChecksum == strSuspEventChecksum)
      {
        bVal = true;
      }

      return bVal;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static bool UpdateReversalEventChecksum(params Object[] arg_pairs)
    {
      /* Matt -- this function will update the reversal event checksum.  In addtion to the 
           params listed below the user must provide one of the following: the 
           DbConnection info in "config" or an existing connection to the database 
           in "DbConnectionClass." The user has the option to provide a structure
           that will be used to create the checksum, if not the information will be
           gathered through the existing or newly made connection.
        params   strFileName			the depfilesequence + the depfilenbr
                 strEventNbr      the event number */

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject data = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;
      bool connectionOpened = false;        // Bug 15941

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();

      string strError = "Default Error";
      string strSQL = "";
      string strSuspEventChecksum = "";

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      // Bug 15941. FT. Make sure the database connection is closed
      try
      {
      if (args.has("DbConnectionClass"))
      {
        pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
        data = pDBConn.GetDBConnectionInfo();
      }
      else if (args.has("data"))
      {
        data = args.get("data", c_CASL.c_undefined) as GenericObject;
        pDBConn = new ConnectionTypeSynch(data);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strError)))  // Bug 15941. Open connection and set connectionO flagpened
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1011", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        if (!pDBConn.EstablishRollback(ref strError))
        {
          strError = "Error creating database transaction.";
            ThrowCASLErrorButDoNotCloseDBConnection("TS-1012", ref pDBConn, "Error creating database transaction.", strError);
        }
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a method to connect to the database.", "code", "TS-1013", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }


      if (args.has("geoChecksumData"))
      {
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else
      {
        int iDBType = pDBConn.GetDBConnectionType();
        // Bug #12055 Mike O - Use misc.Parse to log errors
        strSQL = string.Format("SELECT * FROM TG_REVERSAL_EVENT_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr));
        gTmp = (GenericObject)GetRecords(data, 0, strSQL, iDBType);

        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found. ", "code", "TS-1014", strError + "\n SQL statement: " + strSQL);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

      }

      // Bug #10955 Mike O
      strSuspEventChecksum = CreateReversalEventChecksum(Crypto.DefaultHashAlgorithm, "strFileName", strFileName, "strEventNbr", strEventNbr, "strUserID", gTmp.get("USERID").ToString(),
        "strReversedFileName", gTmp.get("REVERSED_FILENBR").ToString() + gTmp.get("REVERSED_FILESEQ").ToString(), "strReversedEventNbr", gTmp.get("REVERSED_EVENTNBR").ToString(),
        "strVoided", ((bool)gTmp.get("VOIDED")) ? "TRUE" : "FALSE");

      // Create SQL
      // Bug #12055 Mike O - Use misc.Parse to log errors
      strSQL = string.Format("UPDATE TG_REVERSAL_EVENT_DATA SET CHECKSUM='{0}' WHERE DEPFILENBR={1} AND DEPFILESEQ={2} AND EVENTNBR={3}", strSuspEventChecksum, misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr));

      // Update database
      if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strError))
          HandleErrorsAndLocking("message", "Error executing SQL.", "code", "TS-1015", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix


      //***************************************
      // ALL GOOD - ACCEPT THE UPDATES and close the connection we created.
      if (!args.has("DbConnectionClass"))
      {
        if (!pDBConn.CommitToDatabase(ref strError))
            HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-1016", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }
      //***************************************  

      return true;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static bool ValidateReversalEventChecksum(params Object[] arg_pairs)
    {
      /* Matt -- this function creates a checksum and compares it to the existing checksum.  
             In addtion to the params listed below the user must provide one of the 
             following: the values to create a checksum in "geoChecksumData", the 
             DbConnection info in "config" or an existing connection to the database in 
             "DbConnectionClass."
        params   strFileName			the depfilesequence + the depfilenbr 
             strEventNbr			the event nbr */

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();

      string strSQL = "",err="";
      string strReversalEventChecksum = "";
      string strOldChecksum = "";

      bool bVal = false;

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      // Bug 15941. FT. Make sure the database connection is closed
      bool connectionOpened = false;    // Bug 15941
      try
      {
      // Either the connection was passed in or we create one
      if (args.has("DbConnectionClass") || args.has("config"))
      {
        if (args.has("DbConnectionClass"))
        {
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
          config = pDBConn.GetDBConnectionInfo();
        }
        else if (args.has("config"))
        {
          config = args.get("config", c_CASL.c_undefined) as GenericObject;
          pDBConn = new ConnectionTypeSynch(config);

            if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref err)))  // Bug 15941
              HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1036", err);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        int iDBType = pDBConn.GetDBConnectionType();

        // Bug #12055 Mike O - Use misc.Parse to log errors
        if (pDBConn.GetDBConnectionType() == ConnectionTypeSynch.m_iCurrentDatabase_SqlServer)
          strSQL = string.Format("SELECT * FROM TG_REVERSAL_EVENT_DATA (NOLOCK) WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr));
        else
          strSQL = string.Format("SELECT * FROM TG_REVERSAL_EVENT_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr));

        gTmp = (GenericObject)GetRecords(config, 0, strSQL, iDBType);

        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found. ", "code", "TS-1037", err + "\n SQL statement: " + strSQL);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
      }
      else if (args.has("geoChecksumData"))
      {
        // Or we don't need a connection because the structure is already passed in
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a config, connection or the checksum data.", "code", "TS-1038", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      // Bug #10955 Mike O
      strOldChecksum = GetKeyData(gTmp, "CHECKSUM", false);
      HashAlgorithm hashAlg = Crypto.HashAlgorithmUsed(strOldChecksum);

      if (hashAlg == null)
      {
        // Bug #12800 Mike O - If the checksum doesn't match any possible algorithms, fail validation
        return false;
      }

      // The newly created checksum
      strReversalEventChecksum = CreateReversalEventChecksum(hashAlg, "strFileName", strFileName, "strEventNbr", strEventNbr, "strUserID", gTmp.get("USERID").ToString(),
        // End Bug #10955 Mike O
        "strReversedFileName", gTmp.get("REVERSED_FILENBR").ToString() + gTmp.get("REVERSED_FILESEQ").ToString(), "strReversedEventNbr", gTmp.get("REVERSED_EVENTNBR").ToString(),
        "strVoided", ((bool)gTmp.get("VOIDED")) ? "TRUE" : "FALSE");


      if (strOldChecksum == strReversalEventChecksum)
      {
        bVal = true;
      }

      return bVal;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static bool UpdateImageDataChecksum(params Object[] arg_pairs)
    {
      /* Matt -- this function will update the image data checksum.  In addtion to the 
           params listed below the user must provide one of the following: the 
           DbConnection info in "config" or an existing connection to the database 
           in "DbConnectionClass." The user has the option to provide a structure
           that will be used to create the checksum, if not the information will be
           gathered through the existing or newly made connection.
        params   strFileName			the depfilesequence + the depfilenbr
                 strEventNbr      the event number
                 strPostNbr
                 strPostType
                 strSeqNum*/

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject data = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;
      bool connectionOpened = false;        // Bug 15941

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();
      string strPostNbr = args.get("strPostNbr").ToString();
      string strPostType = args.get("strPostType").ToString();
      string strSeqNum = args.get("strSeqNum").ToString();

      string strError = "Default Error";
      string strSQL = "";
      string strSuspEventChecksum = "";

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      // Bug 15941. FT. Make sure the database connection is closed
      try
      {
      if (args.has("DbConnectionClass"))
      {
        pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
        data = pDBConn.GetDBConnectionInfo();
      }
      else if (args.has("data"))
      {
        data = args.get("data", c_CASL.c_undefined) as GenericObject;
        pDBConn = new ConnectionTypeSynch(data);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strError)))  // Bug 15941. Open connection and set connectionO flagpened
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1011", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        if (!pDBConn.EstablishRollback(ref strError))
        {
          strError = "Error creating database transaction.";
            ThrowCASLErrorButDoNotCloseDBConnection("TS-1012", ref pDBConn, "Error creating database transaction.", strError);
        }
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a method to connect to the database.", "code", "TS-1013", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }


      if (args.has("geoChecksumData"))
      {
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else
      {
        int iDBType = pDBConn.GetDBConnectionType();
        // Bug #12055 Mike O - Use misc.Parse to log errors
        strSQL = string.Format("SELECT * FROM TG_IMAGE_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} AND POSTNBR={3} AND POST_TYPE='{4}' AND SEQ_NBR={5}",
          misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr), misc.Parse<Int64>(strPostNbr), strPostType, misc.Parse<Int64>(strSeqNum));
        gTmp = (GenericObject)GetRecords(data, 0, strSQL, iDBType);

        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found. ", "code", "TS-1014", strError + "\n SQL statement: " + strSQL);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

      }

      strSuspEventChecksum = CreateImageDataChecksum(
        Crypto.DefaultHashAlgorithm, // Bug #10955 Mike O
        "strFileName", gTmp.get("DEPFILENBR").ToString() + gTmp.get("DEPFILESEQ").ToString(),
        "strEventNbr", gTmp.get("EVENTNBR"),
        "strPostNbr", gTmp.get("POSTNBR"),
        "strPostType", gTmp.get("POST_TYPE"),
        "strSeqNbr", gTmp.get("SEQ_NBR"),
        // Convert to Base64 - Validation should do the same.
        "strImage", Convert.ToBase64String((byte[])gTmp.get("DOC_IMAGE")),
        "strImagingType", gTmp.get("IMAGING_TYPE"),
        "strCompleteMICRLine", gTmp.get("COMPLETE_MICR_LINE"),
        "strBankRoutingNbr", gTmp.get("BANK_ROUTING_NUMBER"),
        "strAccountNbr", gTmp.get("ACCOUNT_NUMBER"),
        "strCheckNbr", gTmp.get("CHECK_NUMBER"),
        "strDateInserted", gTmp.get("DATE_INSERTED"),
        "strUserID", gTmp.get("USERID"),
        "strComments", gTmp.get("COMMENTS"),
        "strSourceType", gTmp.get("SOURCE_TYPE"),
        "strSourceGroup", gTmp.get("SOURCE_GROUP"),
        "strSourceRefID", gTmp.get("SOURCE_REFID"),
        "strSourceDate", gTmp.get("SOURCE_DATE"),
        "strScanData", gTmp.get("SCAN_DATA"),
        "strDocImageRef", gTmp.get("DOC_IMAGE_REF"),
        "strImageFormat", gTmp.get("IMAGE_FORMAT"),
        "strDocID", gTmp.get("DOC_ID"));

      // Create SQL
      // Bug #12055 Mike O - Use misc.Parse to log errors
      strSQL = string.Format("UPDATE TG_IMAGE_DATA SET CHECKSUM='{0}' WHERE DEPFILENBR={1} AND DEPFILESEQ={2} AND EVENTNBR={3} AND POSTNBR={4} AND POST_TYPE='{5}' AND SEQ_NBR={6}",
        strSuspEventChecksum, misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr), misc.Parse<Int64>(strPostNbr), strPostType, misc.Parse<Int64>(strSeqNum));

      // Update database
      if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strError))
          HandleErrorsAndLocking("message", "Error executing SQL.", "code", "TS-1015", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix


      //***************************************
      // ALL GOOD - ACCEPT THE UPDATES and close the connection we created.
      if (!args.has("DbConnectionClass"))
      {
        if (!pDBConn.CommitToDatabase(ref strError))
            HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-1016", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }
      //***************************************  

      return true;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static Object ValidateImageDataChecksum_impl(params Object[] arg_pairs)
    { return ValidateImageDataChecksum(arg_pairs); }

    public static bool ValidateImageDataChecksum(params Object[] arg_pairs)
    {
      /* Matt -- this function creates a checksum and compares it to the existing checksum.  
             In addtion to the params listed below the user must provide one of the 
             following: the values to create a checksum in "geoChecksumData", the 
             DbConnection info in "config" or an existing connection to the database in 
             "DbConnectionClass."
        params   strFileName			the depfilesequence + the depfilenbr
                 strEventNbr      the event number
                 strPostNbr
                 strPostType
                 strSeqNum

                 row              optional, holds all elements so we can skip the DB call*/

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      // Bug #9275 Mike O - If row has been passed in, set it here
      if (args.has("row"))
        gTmp = (GenericObject)args.get("row");

      ConnectionTypeSynch pDBConn = null;

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();
      string strPostNbr = args.get("strPostNbr").ToString();
      string strPostType = args.get("strPostType").ToString();
      string strSeqNum = args.get("strSeqNum").ToString();

      string strSQL = "",err="";
      string strImageDataChecksum = "";
      string strOldChecksum = "";

      bool bVal = false;

      // Set this to true only if the connection is opened locally
      bool connectionOpened = false;

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);
      // Bug 15941. FT. Make sure the database connection is closed
      try
      {
      // Bug #9275 Mike O - Skip DB call if we already have all that info
      if (gTmp == null)
      {
        // Either the connection was passed in or we create one
        if (args.has("DbConnectionClass") || args.has("config"))
        {
          if (args.has("DbConnectionClass"))
          {
            pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
            config = pDBConn.GetDBConnectionInfo();
          }
          else if (args.has("config"))
          {
            config = args.get("config", c_CASL.c_undefined) as GenericObject;
            pDBConn = new ConnectionTypeSynch(config);

              if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref err)))  // Bug 15941
                HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1036", err);//Bug 13832-Added detailed error message//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
          }

          int iDBType = pDBConn.GetDBConnectionType();

        // Bug #12055 Mike O - Use misc.Parse to log errors
          if (pDBConn.GetDBConnectionType() == ConnectionTypeSynch.m_iCurrentDatabase_SqlServer)
            strSQL = string.Format("SELECT * FROM TG_IMAGE_DATA (NOLOCK) WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} AND POSTNBR={3} AND POST_TYPE='{4}' AND SEQ_NBR={5}",
            misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr), misc.Parse<Int64>(strPostNbr), strPostType, misc.Parse<Int64>(strSeqNum));
          else
            strSQL = string.Format("SELECT * FROM TG_IMAGE_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} AND POSTNBR={3} AND POST_TYPE='{4}' AND SEQ_NBR={5}",
            misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr), misc.Parse<Int64>(strPostNbr), strPostType, misc.Parse<Int64>(strSeqNum));

          gTmp = (GenericObject)GetRecords(config, 0, strSQL, iDBType);

          if (gTmp == null)
          {
            HandleErrorsAndLocking("message", "No records found. ", "code", "TS-1037", err + "\n SQL statement: " + strSQL);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
          }
        }
        else if (args.has("geoChecksumData"))
        {
          // Or we don't need a connection because the structure is already passed in
          gTmp = args.get("geoChecksumData") as GenericObject;
        }
        else
        {
          HandleErrorsAndLocking("message", "Please provide a config, connection or the checksum data.", "code", "TS-1038", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
      }

      // Bug #10955 Mike O
      strOldChecksum = GetKeyData(gTmp, "CHECKSUM", false);
      HashAlgorithm hashAlg = Crypto.HashAlgorithmUsed(strOldChecksum);

      if (hashAlg == null)
      {
        // Bug #12800 Mike O - If the checksum doesn't match any possible algorithms, fail validation
        return false;
      }

      // The newly created checksum
      strImageDataChecksum = CreateImageDataChecksum(
        hashAlg,
        "strFileName", gTmp.get("DEPFILENBR").ToString() + gTmp.get("DEPFILESEQ").ToString(),
        "strEventNbr", gTmp.get("EVENTNBR"),
        "strPostNbr", gTmp.get("POSTNBR"),
        "strPostType", gTmp.get("POST_TYPE"),
        "strSeqNbr", gTmp.get("SEQ_NBR"),
        // Convert to Base64 - Validation should do the same.
        "strImage", Convert.ToBase64String((byte[])gTmp.get("DOC_IMAGE")),
        "strImagingType", gTmp.get("IMAGING_TYPE"),
        "strCompleteMICRLine", gTmp.get("COMPLETE_MICR_LINE"),
        "strBankRoutingNbr", gTmp.get("BANK_ROUTING_NUMBER"),
        "strAccountNbr", gTmp.get("ACCOUNT_NUMBER"),
        "strCheckNbr", gTmp.get("CHECK_NUMBER"),
        "strDateInserted", gTmp.get("DATE_INSERTED"),
        "strUserID", gTmp.get("USERID"),
        "strComments", gTmp.get("COMMENTS"),
        "strSourceType", gTmp.get("SOURCE_TYPE"),
        "strSourceGroup", gTmp.get("SOURCE_GROUP"),
        "strSourceRefID", gTmp.get("SOURCE_REFID"),
        "strSourceDate", gTmp.get("SOURCE_DATE"),
        "strScanData", gTmp.get("SCAN_DATA"),
        "strDocImageRef", gTmp.get("DOC_IMAGE_REF"),
        "strImageFormat", gTmp.get("IMAGE_FORMAT"),
        "strDocID", gTmp.get("DOC_ID"));
      // End Bug #10955 Mike O
      //***************************************
      //***************************************   


      if (strOldChecksum == strImageDataChecksum)
      {
        bVal = true;
      }

      return bVal;
    }
      finally
      {
        // Close the connection we created.
        // Bug #9275 Mike O - If the row was passed in, the DB connection was never opened
        // if (!args.has("row"))  // I don't think I need this check since connectionOpened is only set if connection is opened
          CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);  // Bug 15941
      }
    }


    // Bug #10955 Mike O
    //Bug 9092 DepositTndr SN
    #region deposittndr
    public static string CreateDepositTndrDataChecksum(HashAlgorithm alg, params Object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);

      StringBuilder sb = new StringBuilder();

      sb.Append(args.get("strDepositId"));
      sb.Append(args.get("strDepositNbr"));
      sb.Append(args.get("strSeqNbr"));
      sb.Append(args.get("strTndeProp"));
      sb.Append(args.get("strTndr"));
      sb.Append(args.get("strTndeDesc"));
      sb.Append(args.get("strAmount"));
      sb.Append(args.get("strSourceRefId"));
      sb.Append(args.get("strSourceDate"));
      // Bug #10955 Mike O
      return Crypto.ComputeHash("data", sb.ToString(), alg);
    }

    public static bool UpdateDepositTndrDataChecksum(params Object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject data = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;
      string strDepositId = args.get("strDepositId").ToString();
      string strDepositNbr = args.get("strDepositNbr").ToString();
      string strSeqNbr = args.get("strSeqNbr").ToString();
      string strError = "Default Error";
      string strSQL = "";
      string strDepositTndrChecksum = "";

      // Bug 15941. FT. Make sure the database connection is closed
      bool connectionOpened = false;    // Bug 15941
      try
      {
      if (args.has("DbConnectionClass"))
      {
        pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
        data = pDBConn.GetDBConnectionInfo();
      }
      else if (args.has("data"))
      {
        data = args.get("data", c_CASL.c_undefined) as GenericObject;
        pDBConn = new ConnectionTypeSynch(data);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strError)))  // Bug 15941
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1011", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        if (!pDBConn.EstablishRollback(ref strError))
        {
          strError = "Error creating database transaction.";
            ThrowCASLErrorButDoNotCloseDBConnection("TS-1012", ref pDBConn, "Error creating database transaction.", strError);
        }
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a method to connect to the database.", "code", "TS-1013", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }


      if (args.has("geoChecksumData"))
      {
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else
      {
        int iDBType = pDBConn.GetDBConnectionType();
        strSQL = string.Format("SELECT * FROM TG_DEPOSITTNDR_DATA WHERE DEPOSITID='{0}' AND DEPOSITNBR={1} AND SEQNBR={2}",
           strDepositId, Int64.Parse(strDepositNbr), Int64.Parse(strSeqNbr));
        gTmp = (GenericObject)GetRecords(data, 0, strSQL, iDBType);

        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found. ", "code", "TS-1014", strError + "\n SQL statement: " + strSQL);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

      }

      // Bug #13092 Mike O - These are already set, and might not be present in geoChecksumData
      //strDepositId = GetKeyData(gTmp, "DEPOSITID", false);
      //strDepositNbr = GetKeyData(gTmp, "DEPOSITNBR", false);
      //strSeqNbr = GetKeyData(gTmp, "SEQNBR", false);
      string strTndrProp = GetKeyData(gTmp, "TNDRPROP", false);
      string strTndr = GetKeyData(gTmp, "TNDR", false);
      string strTndrDesc = GetKeyData(gTmp, "TNDRDESC", false);
      string strAmount = GetKeyData(gTmp, "AMOUNT", false);
      string strSourceRefId = GetKeyData(gTmp, "SOURCE_REFID", false);
      string strSourceDate = GetKeyData(gTmp, "SOURCE_DATE", false);


      Decimal fConvert1 = Decimal.Parse(strAmount);// BUG19280 NEVER USE single-precision-float for currency
      strAmount = string.Format("{0:f}", fConvert1);

      string strChecksumData = strDepositId;
      strChecksumData += strDepositNbr;
      strChecksumData += strSeqNbr;
      strChecksumData += strTndrProp;
      strChecksumData += strTndr;
      strChecksumData += strTndrDesc;
      strChecksumData += strAmount;
      strChecksumData += strSourceRefId;
      strChecksumData += strSourceDate;

      // Bug #10955 Mike O
      // Hash
      strDepositTndrChecksum = Crypto.ComputeHash("", strChecksumData); //CreateDepositTndrDataChecksum

      // Create SQL
      strSQL = string.Format("UPDATE TG_DEPOSITTNDR_DATA SET CHECKSUM='{0}' WHERE DEPOSITID='{1}' AND DEPOSITNBR={2} AND SEQNBR={3}",
        strDepositTndrChecksum, strDepositId, Int64.Parse(strDepositNbr), Int64.Parse(strSeqNbr));

      // Update database
      if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strError))
          HandleErrorsAndLocking("message", "Error executing SQL.", "code", "TS-1015", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

      if (!args.has("DbConnectionClass"))
      {
        if (!pDBConn.CommitToDatabase(ref strError))
            HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-1016", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }
      return true;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static bool ValidateDepositTndrDataChecksum(params Object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      if (args.has("row"))
        gTmp = (GenericObject)args.get("row");

      ConnectionTypeSynch pDBConn = null;
      string strDepositId = args.get("strDepositId").ToString();
      string strDepositNbr = args.get("strDepositNbr").ToString();
      string strSeqNbr = args.get("strSeqNbr").ToString();

      string strSQL = "", err="";
      string strDepositTndrDataChecksum = "";
      string strOldChecksum = "";

      // Bug 15941. FT. Make sure the database connection is closed
      bool connectionOpened = false;

      bool bVal = false;
      // Bug 15941. FT. Make sure the database connection is closed
      try
      {
      if (gTmp == null)
      {
        if (args.has("DbConnectionClass") || args.has("config"))
        {
          if (args.has("DbConnectionClass"))
          {
            pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
            config = pDBConn.GetDBConnectionInfo();
          }
          else if (args.has("config"))
          {
            config = args.get("config", c_CASL.c_undefined) as GenericObject;
            pDBConn = new ConnectionTypeSynch(config);

              if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref err)))  // Bug 15941
              HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1036", err);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
          }

          int iDBType = pDBConn.GetDBConnectionType();

          if (pDBConn.GetDBConnectionType() == ConnectionTypeSynch.m_iCurrentDatabase_SqlServer)
            strSQL = string.Format("SELECT * FROM TG_DEPOSITTNDR_DATA (NOLOCK) WHERE DEPOSITID='{0}' AND DEPOSITNBR={1} AND SEQNBR={2}",
            strDepositId, Int64.Parse(strDepositNbr), Int64.Parse(strSeqNbr));
          else
            strSQL = string.Format("SELECT * FROM TG_DEPOSITTNDR_DATA WHERE DEPOSITID='{0}' AND DEPOSITNBR={1} AND SEQNBR={2}",
            strDepositId, Int64.Parse(strDepositNbr), Int64.Parse(strSeqNbr));

          gTmp = (GenericObject)GetRecords(config, 0, strSQL, iDBType);

          if (gTmp == null)
          {
            HandleErrorsAndLocking("message", "No records found. ", "code", "TS-1037", err + "\n SQL statement: " + strSQL);//Bug 13832-Added detailed error message // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
          } 
        }
        else if (args.has("geoChecksumData"))
        {
          gTmp = args.get("geoChecksumData") as GenericObject;
        }
        else
        {
          HandleErrorsAndLocking("message", "Please provide a config, connection or the checksum data.", "code", "TS-1038", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
      }

      // Bug #10955 Mike O
      strOldChecksum = GetKeyData(gTmp, "CHECKSUM", false);
      HashAlgorithm hashAlg = Crypto.HashAlgorithmUsed(strOldChecksum);

      if (hashAlg == null)
      {
          // Bug #12800 Mike O - If the checksum doesn't match any possible algorithms, fail validation
          return false;
      }

      strDepositTndrDataChecksum = CreateDepositTndrDataChecksum(
        hashAlg,
        "strDepositId", gTmp.get("DEPOSITID").ToString(),
        "strDepositNbr", gTmp.get("DEPOSITNBR"),
        "strSeqNbr", gTmp.get("SEQNBR"),
        "strTndrProp", gTmp.get("TNDRPROP").ToString(),
        "strTndr", gTmp.get("TNDR").ToString(),
        "strTndrDesc", gTmp.get("TNDRDESC").ToString(),
        "strAmount", gTmp.get("AMOUNT"),
        "strSourceRefid", gTmp.get("SOURCE_REFID").ToString(),
        "strSourceDate", gTmp.get("SOURCE_DATE").ToString());
      // End Bug #10955 Mike O
      if (strOldChecksum == strDepositTndrDataChecksum)
      {
        bVal = true;
      }

      return bVal;
    }
      finally
      {
        // Bug #9275 Mike O - If the row was passed in, the DB connection was never opened
        // Bug 15941. FT. Make sure the database connection is closed
        //if (!args.has("row"))  // FT. I don't think I need this since connectionOpened will only be opened if there is a successful open
          CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }
    #endregion

    // Bug #10955 Mike O
    //Bug 9092 ReversalTran SN
    //This table does not exists at present for reference see Bug 5143
    #region reversaltran
    public static string CreateReversalTranDataChecksum(HashAlgorithm alg, params Object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);

      StringBuilder sb = new StringBuilder();

      sb.Append(args.get("strCoreItemNbr"));//Bug 13934 - modified column name
      sb.Append(args.get("strEventNbr"));
      sb.Append(args.get("strDepFileNbr"));
      sb.Append(args.get("strDepFileSeq"));
      sb.Append(args.get("strAmount"));// bug 13934-new column
      // End Bug #10955 Mike O
      return Crypto.ComputeHash("data", sb.ToString(), alg);
    }

    public static bool UpdateReversalTranDataChecksum(params Object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject data = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;
      string strTranNbr = args.get("strCoreItemNbr").ToString(); //Bug 13934 -Changed column name
      string strEventNbr = args.get("strEventNbr").ToString();
      string strDepFileNbr = args.get("strDepFileNbr").ToString();
      string strDepFileSeq = args.get("strDepFileSeq").ToString();
      string strAmount = args.get("strAmount").ToString(); //Bug 13934 - new column
      string strError = "Default Error";
      string strSQL = "";
      string strSuspEventChecksum = "";
      bool connectionOpened = false;    // Bug 15941

      try
      {
      if (args.has("DbConnectionClass"))
      {
        pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
        data = pDBConn.GetDBConnectionInfo();
      }
      else if (args.has("data"))
      {
        data = args.get("data", c_CASL.c_undefined) as GenericObject;
        pDBConn = new ConnectionTypeSynch(data);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strError)))  // Bug 15941
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1011", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        if (!pDBConn.EstablishRollback(ref strError))
        {
          strError = "Error creating database transaction.";
            ThrowCASLErrorButDoNotCloseDBConnection("TS-1012", ref pDBConn, "Error creating database transaction.", strError);
        }
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a method to connect to the database.", "code", "TS-1013", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }


      if (args.has("geoChecksumData"))
      {
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else
      {
        int iDBType = pDBConn.GetDBConnectionType();
          //Bug 13934-update sql with modified column names
        strSQL = string.Format(@"SELECT * FROM TG_REVERSAL_TRAN_DATA WHERE COREITEMNBR={0} AND EVENTNBR={1} AND DEPFILENBR={2} AND DEPFILESEQ={3} 
          AND REVERSED_AMOUNT={4}",
           Int64.Parse(strTranNbr), Int64.Parse(strEventNbr), Int64.Parse(strDepFileNbr), Int64.Parse(strDepFileSeq),strAmount);
        gTmp = (GenericObject)GetRecords(data, 0, strSQL, iDBType);

        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found. ", "code", "TS-1014", strError + "\n SQL statement: " + strSQL);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

      }

      strSuspEventChecksum = CreateReversalTranDataChecksum(
        // Bug #10955 Mike O
        Crypto.DefaultHashAlgorithm,
        "strCoreItemNbr", gTmp.get("COREITEMNBR"),//Bug 13934-column name changed
        "strEventNbr", gTmp.get("EVENTNBR"),
        "strDepFileNbr", gTmp.get("DEPFILENBR"),
        "strDepFileSeq", gTmp.get("DEPFILESEQ"),
        "strAmount", gTmp.get("REVERSED_AMOUNT"));//Bug 13934-new column added

      // Create SQL
        //Bug 13934-update sql with modified and new columns
      strSQL = string.Format(@"UPDATE TG_REVERSAL_TRAN_DATA SET CHECKSUM='{0}' WHERE COREITEMNBR={1} AND EVENTNBR={2} AND DEPFILENBR={3} AND DEPFILESEQ={4}
                             AND REVERSED_AMOUNT={4}",
        strSuspEventChecksum, Int64.Parse(strTranNbr), Int64.Parse(strEventNbr), Int64.Parse(strDepFileNbr), Int64.Parse(strDepFileSeq),strAmount);

      // Update database
      if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strError))
          HandleErrorsAndLocking("message", "Error executing SQL.", "code", "TS-1015", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

      if (!args.has("DbConnectionClass"))
      {
        if (!pDBConn.CommitToDatabase(ref strError))
            HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-1016", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }
      return true;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static bool ValidateReversalTranDataChecksum(params Object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      if (args.has("row"))
        gTmp = (GenericObject)args.get("row");

      ConnectionTypeSynch pDBConn = null;
      string strTranNbr = args.get("strCoreItemNbr").ToString();//Bug 13934-column name changed
      string strEventNbr = args.get("strEventNbr").ToString();
      string strDepFileNbr = args.get("strDepFileNbr").ToString();
      string strDepFileSeq = args.get("strDepFileSeq").ToString();
      string strAmount =  args.get("strAmount").ToString(); //Bug 13934 -new column added

      string strSQL = "", err="";
      string strReversalTranDataChecksum = "";
      string strOldChecksum = "";

      bool bVal = false;
      // Bug 15941. FT. Make sure the database connection is closed
      bool connectionOpened = false;    // Bug 15941
      try
      {
      if (gTmp == null)
      {
        if (args.has("DbConnectionClass") || args.has("config"))
        {
          if (args.has("DbConnectionClass"))
          {
            pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
            config = pDBConn.GetDBConnectionInfo();
          }
          else if (args.has("config"))
          {
            config = args.get("config", c_CASL.c_undefined) as GenericObject;
            pDBConn = new ConnectionTypeSynch(config);

              if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref err)))  // Bug 15941
                HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1036", err);//Bug 13832-Added detailed error message // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
          }

          int iDBType = pDBConn.GetDBConnectionType();

          if (pDBConn.GetDBConnectionType() == ConnectionTypeSynch.m_iCurrentDatabase_SqlServer)
              //Bug 13934-update sql with modified and new columns
            strSQL = string.Format(@"SELECT * FROM TG_REVERSAL_TRAN_DATA (NOLOCK) WHERE COREITEMNBR={0} AND EVENTNBR={1} AND DEPFILENBR={2} AND DEPFILESEQ={3}
                            AND REVERSED_AMOUNT={4}",
            Int64.Parse(strTranNbr), Int64.Parse(strEventNbr), Int64.Parse(strDepFileNbr), Int64.Parse(strDepFileSeq),strAmount);
          else
              //Bug 13934-update sql with modified and new columns
            strSQL = string.Format(@"SELECT * FROM TG_REVERSAL_TRAN_DATA WHERE COREITEMNBR={0} AND EVENTNBR={1} AND DEPFILENBR={2} AND DEPFILESEQ={3}
                                    AND REVERSED_AMOUNT={4}",
            Int64.Parse(strTranNbr), Int64.Parse(strEventNbr), Int64.Parse(strDepFileNbr), Int64.Parse(strDepFileSeq),strAmount);

          gTmp = (GenericObject)GetRecords(config, 0, strSQL, iDBType);

          if (gTmp == null)
          {
            HandleErrorsAndLocking("message", "No records found. ", "code", "TS-1037", err + "\nSQL statement: " + strSQL);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
          }
        }
        else if (args.has("geoChecksumData"))
        {
          gTmp = args.get("geoChecksumData") as GenericObject;
        }
        else
        {
          HandleErrorsAndLocking("message", "Please provide a config, connection or the checksum data.", "code", "TS-1038", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
      }

      // Bug #10955 Mike O
      strOldChecksum = GetKeyData(gTmp, "CHECKSUM", false);
      HashAlgorithm hashAlg = Crypto.HashAlgorithmUsed(strOldChecksum);

      if (hashAlg == null)
      {
          // Bug #12800 Mike O - If the checksum doesn't match any possible algorithms, fail validation
          return false;
      }

      strReversalTranDataChecksum = CreateReversalTranDataChecksum(
        hashAlg,
        "strCoreItemNbr", gTmp.get("COREITEMNBR"),//Bug 13934-MODIFIED column name
        "strEventNbr", gTmp.get("EVENTNBR"),
        "strDepFileNbr", gTmp.get("DEPFILENBR"),
        "strDepFileSeq", gTmp.get("DEPFILESEQ"),
        "strAmount", gTmp.get("REVERSED_AMOUNT"));//Bug 13934-new column
      // End Bug #10955 Mike O

      if (strOldChecksum == strReversalTranDataChecksum)
      {
        bVal = true;
      }

      return bVal;
    }
      finally
      {
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    
    }
    #endregion
    //end Bug 9092

    // Bug #11026 Mike O - Made this consistent with the other CreateTenderChecksum (this is such a mess)
    public static string CreateTenderChecksum(GenericObject tender)
    {
      //#region Construct checksum data
      string tendernbr = GetStringValue(tender, "TNDRNBR");
      string eventnbr = GetStringValue(tender, "EVENTNBR");
      string filenbr = GetStringValue(tender, "DEPFILENBR");
      string fileseq = GetStringValue(tender, "DEPFILESEQ");
      string tenderid = GetStringValue(tender, "TNDRID");
      string amount = GetStringValue(tender, "AMOUNT");
      string credit_card_number = GetStringValue(tender, "CC_CK_NBR");
      string exp_month = "";
      string exp_year = "";
      string credit_card_name = GetStringValue(tender, "CCNAME");
      string auth_number = GetStringValue(tender, "AUTHNBR");
      string post_date = GetStringValue(tender, "POSTDT");
      string void_date = GetStringValue(tender, "VOIDDT");

      string checksumData = tendernbr;
      checksumData += eventnbr;
      checksumData += filenbr;
      checksumData += fileseq;
      checksumData += tenderid;
      checksumData += Convert.ToDecimal(amount).ToString("N2"); // BUG19 280 NEVER USE single-precision-float for currency// Bug #11026 Mike O - Make sure amount is correctly formatted
      checksumData += credit_card_number;
      checksumData += exp_month;
      checksumData += exp_year;
      checksumData += credit_card_name;
      checksumData += auth_number;
      checksumData += post_date;
      checksumData += void_date;
      //#endregion
      //#region Compute hash and return
      return Crypto.ComputeHash("", checksumData);
      //#endregion
    }

    // Bug #10955 Mike O
    public static string CreateTenderChecksum(HashAlgorithm alg, params Object[] arg_pairs)
    {
      /* Matt -- this function will create a tender Checksum.  In addtion to the params
           listed below the user must provide one of the following: the values to 
           create a checksum in "geoChecksumData", the DbConnection info in "config" 
           or an existing connection to the database in "DbConnectionClass."
       params    strFileName			the depfilesequence + the depfilenbr 
           strEventNbr			the event nbr
           strTndrNbr			the tender nbr */

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();
      string strTndrNbr = args.get("strTndrNbr").ToString();

      string strSQL = "",err="";
      string strChecksumData = "";
      string strTenderChecksum = "";

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);
      // Bug 15941. FT. Make sure the database connection is closed
      bool connectionOpened = false;
      try
      {
      if (args.has("geoChecksumData"))
      {
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else if (args.has("DbConnectionClass") || args.has("config"))
      {
        if (args.has("DbConnectionClass"))
        {
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
          config = pDBConn.GetDBConnectionInfo();
        }
        else if (args.has("config"))
        {
          config = args.get("config", c_CASL.c_undefined) as GenericObject;
          pDBConn = new ConnectionTypeSynch(config);

            if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref err)))  // Bug 15941
              HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-972", err);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        int iDBType = pDBConn.GetDBConnectionType();
        // Bug #12055 Mike O - Use misc.Parse to log errors
        strSQL = string.Format("SELECT * FROM TG_TENDER_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} AND TNDRNBR={3}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr), misc.Parse<Int64>(strTndrNbr));

        gTmp = (GenericObject)GetRecords(config, 0, strSQL, iDBType);

        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found. ", "code", "TS-973", err + "\nSQL statement: " + strSQL);//Bug 13832-Added detailed error message // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a config, connection or the checksum data.", "code", "TS-974", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      // TNDR DATA ENCRYPTION: 
      // TndrNbr + EventNbr + FileNbr + TndrID + Amount + 
      // CC_CKNbr + CCName + AuthNbr + PostDt + VoidDt

      string strTndrID = GetKeyData(gTmp, "TNDRID", false);
      string strAmount = GetKeyData(gTmp, "AMOUNT", false);
      string strCC_CK_NBR = GetKeyData(gTmp, "CC_CK_NBR", false);
      string strExpMonth = "";// removed for Bug#5187 GetKeyData(gTmp, "EXPMONTH", false);			
      string strExpYear = "";// removed for Bug#5187 GetKeyData(gTmp, "EXPYEAR", false);				
      string strCCName = GetKeyData(gTmp, "CCNAME", false);
      string strAuthNbr = GetKeyData(gTmp, "AUTHNBR", false);
      string strPostDT = GetKeyData(gTmp, "POSTDT", false);
      string strVoidDT = GetKeyData(gTmp, "VOIDDT", false);

      // Bug #12055 Mike O - Use misc.Parse to log errors
      Decimal fConvert1 = misc.Parse<Decimal>(strAmount);// BUG19280 NEVER USE single-precision-float for currency
      strAmount = string.Format("{0:f}", fConvert1);

      DateTime dtDate;
      if (strPostDT != null && strPostDT != "")
      {
        // Bug #12055 Mike O - Use misc.Parse to log errors
        dtDate = misc.Parse<DateTime>(strPostDT);
        strPostDT = dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo);
      }

      if (strVoidDT != null && strVoidDT != "")
      {
        // Bug #12055 Mike O - Use misc.Parse to log errors
        dtDate = misc.Parse<DateTime>(strVoidDT);
        strVoidDT = dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo);
      }

      strChecksumData = strTndrNbr;
      strChecksumData += strEventNbr;
      strChecksumData += strFileNbr;
      strChecksumData += strFileSequenceNbr;
      strChecksumData += strTndrID;
      strChecksumData += Convert.ToDecimal(strAmount).ToString("N2");// BUG19280 NEVER USE single-precision-float for currency // Bug #11026 Mike O - Make sure amount is correctly formatted
      strChecksumData += strCC_CK_NBR;
      strChecksumData += strExpMonth;
      strChecksumData += strExpYear;
      strChecksumData += strCCName;
      strChecksumData += strAuthNbr;
      strChecksumData += strPostDT;
      strChecksumData += strVoidDT;

      // Bug #10955 Mike O
      // Hash
      strTenderChecksum = Crypto.ComputeHash("", strChecksumData, alg);

        return strTenderChecksum;
      }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static bool ValidateTenderChecksum(params Object[] arg_pairs)
    {
      /* Matt -- this function creates a checksum and compares it to the existing checksum.  
           In addtion to the params listed below the user must provide one of the 
           following: the values to create a checksum in "geoChecksumData", the 
           DbConnection info in "config" or an existing connection to the database in 
           "DbConnectionClass."
        params   strFileName			the depfilesequence + the depfilenbr 
           strEventNbr			the event nbr
           strTndrNbr			the tender nbr */


      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();
      string strTndrNbr = args.get("strTndrNbr").ToString();

      string strSQL = "", err="";
      string strTenderChecksum = "";
      string strOldChecksum = "";

      bool bVal = false;

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      // Bug 15941. FT. Make sure the database connection is closed
      bool connectionOpened = false;
      try
      {
      // Either the connection was passed in or we create one
      if (args.has("DbConnectionClass") || args.has("config"))
      {
        if (args.has("DbConnectionClass"))
        {
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
          config = pDBConn.GetDBConnectionInfo();
        }
        else if (args.has("config"))
        {
          config = args.get("config", c_CASL.c_undefined) as GenericObject;
          pDBConn = new ConnectionTypeSynch(config);

            if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref err)))  // Bug 15941
              HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1033", err);//Bug 13832-Added detailed error message // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        int iDBType = pDBConn.GetDBConnectionType();
        // Bug #12055 Mike O - Use misc.Parse to log errors
        strSQL = string.Format("SELECT * FROM TG_TENDER_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} AND TNDRNBR={3}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr), misc.Parse<Int64>(strTndrNbr));

        gTmp = (GenericObject)GetRecords(config, 0, strSQL, iDBType);

        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found. ", "code", "TS-1034", err + "\n SQL statement: " + strSQL);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
      }
      else if (args.has("geoChecksumData"))
      {
        // Or we don't need a connection because the structure is already passed in
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a config, connection or the checksum data.", "code", "TS-1035", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      // Bug #10955 Mike O
      strOldChecksum = GetKeyData(gTmp, "CHECKSUM", false);
      HashAlgorithm hashAlg = Crypto.HashAlgorithmUsed(strOldChecksum);

      if (hashAlg == null)
      {
        // Bug #12800 Mike O - If the checksum doesn't match any possible algorithms, fail validation
        return false;
      }

      // The newly created checksum
      strTenderChecksum = CreateTenderChecksum(hashAlg, "strEventNbr", strEventNbr, "strFileName", strFileName, "strTndrNbr", strTndrNbr, "geoChecksumData", gTmp);
      // End Bug #10955 Mike O

      if (strOldChecksum == strTenderChecksum)
      {
        bVal = true;
      }

      return bVal;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static Object VoidCoreFile(params Object[] arg_pairs)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      GenericObject args = misc.convert_args(arg_pairs);
      string strFileName = (string)args.get("FILENAME");
      string strVoidUserID = (string)args.get("VOIDUSERID");
      GenericObject data = args.get("data", c_CASL.c_undefined) as GenericObject;
      ConnectionTypeSynch pDBConn_Data = new ConnectionTypeSynch(data);
      string strDeleteDeposits = "";
      bool bRetVal = false;
      string strSQL = "";
      string strErr = "";

      GenericObject geoBuffer = new GenericObject();
      // Optional flag.
      if (args.has("delete_deposits"))
        strDeleteDeposits = (string)args.get("delete_deposits");

      //---------------------------------------------------------
      // Break the file into file number and sequence number
      // Bug #12055 Mike O - Use misc.Parse to log errors
      string strFileNumber      = misc.Parse<Int64>(GetFileNumber(strFileName)).ToString();
      string strFileSequenceNbr  = misc.Parse<Int64>(GetFileSequence(strFileName)).ToString();
      //---------------------------------------------------------

      // Bug 15941. FT. Make sure the database connection is closed
      try
      {
      //*********************************************************
      // Connection 
      if (!pDBConn_Data.EstablishDatabaseConnection(ref strErr))
          HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-954", strErr);//Bug 13832-Added detailed error message // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

      if (!pDBConn_Data.EstablishRollback(ref strErr))
      {
        strErr = "Error creating database transaction.";
          ThrowCASLErrorButDoNotCloseDBConnection("TS-955", ref pDBConn_Data, "Error creating database transaction.", strErr);
      }
      //*********************************************************

      //*********************************************************
      // Void all events
      // Bug #12055 Mike O - Use misc.Parse to log errors
      strSQL = string.Format("SELECT * FROM TG_DEPFILE_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1}", misc.Parse<Int64>(strFileNumber), misc.Parse<Int32>(strFileSequenceNbr));
      GenericObject geoCoreFile = (GenericObject)GetRecords(data, 0, strSQL, pDBConn_Data.GetDBConnectionType());
      if (geoCoreFile == null)
          ThrowCASLErrorButDoNotCloseDBConnection("TS-956", ref pDBConn_Data, "Unable to find file: " + strFileName, "");

      // Iterate thru all events and void each one.
      // Bug #12055 Mike O - Use misc.Parse to log errors
      strSQL = string.Format("SELECT * FROM TG_PAYEVENT_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1}", misc.Parse<Int64>(strFileNumber), misc.Parse<Int32>(strFileSequenceNbr));
      GenericObject geoTemp = (GenericObject)GetRecords(data, 1, strSQL, pDBConn_Data.GetDBConnectionType());
      if (geoTemp != null)
      {
        int iCountOfEvents = geoTemp.getIntKeyLength();
        for (int j = 0; j < iCountOfEvents; j++)
        {
          GenericObject geoCoreEvent = (GenericObject)geoTemp.get(j);

          GenericObject TG_PAYEVENT_DATA = new GenericObject();
          TG_PAYEVENT_DATA.set("file_FILENAME", strFileName);
          TG_PAYEVENT_DATA.set("EVENTNBR", GetKeyData(geoCoreEvent, "EVENTNBR", false));
          TG_PAYEVENT_DATA.set("VOIDUSERID", strVoidUserID);

          try
          {
            bRetVal = VoidEventInT3("_subject", TG_PAYEVENT_DATA, "DbConnectionClass", pDBConn_Data, "no_error_report", true, "geoBuffer", geoBuffer);
          }
          catch (CASL_error ce)
          {
            GenericObject a_error_object = ce.casl_object;
              ThrowCASLErrorButDoNotCloseDBConnection("" + a_error_object.get("code", "UNKNOWN"), ref pDBConn_Data, "Unable to void event: " + GetKeyData(geoCoreEvent, "EVENTNBR", false) + " in File: " + strFileName, " Error: " + a_error_object.get("message", "UNKNOWN") as string);
          }
          catch (Exception e)
          {
              ThrowCASLErrorButDoNotCloseDBConnection("TS-957", ref pDBConn_Data, "Unable to void event: " + GetKeyData(geoCoreEvent, "EVENTNBR", false) + " in File: " + strFileName, " Error: " + e.Message);
          }
          if (!bRetVal)
          {
              ThrowCASLErrorButDoNotCloseDBConnection("TS-958", ref pDBConn_Data, "Unable to void event: " + GetKeyData(geoCoreEvent, "EVENTNBR", false) + " in File: " + strFileName, " Error: " + m_sErrMsg);
          }
        }
      }
      //*********************************************************
      // Matt: need to update all of the voided transactions and tenders within this buff            
      for (int i = 0; i < (int)geoBuffer.getLength(); i++)
      {
        if (!pDBConn_Data.ExecuteSQLAndRollBackIfNeeded(geoBuffer.get(i).ToString(), ref strErr))
        {
          //get real error code
          HandleErrorsAndLocking("message", "Error executing SQL.", "code", "TS-0000", strErr);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
      }

      //*********************************************************
      // Commit changes and close connection.
      if (!pDBConn_Data.CommitToDatabase(ref strErr))
      {
          ThrowCASLErrorButDoNotCloseDBConnection("TS-959", ref pDBConn_Data, "Unable to commit while voiding a file. File #: " + strFileName, "Error: " + strErr);
      }
      //*********************************************************

      return true;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnection(pDBConn_Data);
      }
    }

    public static bool UpdateTenderChecksum(params Object[] arg_pairs)
    {
      /* Matt -- this function will update the tran file checksum.  In addtion to the 
             params listed below the user must provide one of the following: the 
             DbConnection info in "config" or an existing connection to the database 
             in "DbConnectionClass." The user has the option to provide a structure
             that will be used to create the checksum, if not the information will be
             gathered through the existing or newly made connection.
      params     strFileName			the depfilesequence + the depfilenbr 
             strEventNbr			the event nbr
             strTndrNbr			the tender nbr */

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;
      bool connectionOpened = false;

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();
      string strTndrNbr = args.get("strTndrNbr").ToString();

      string strError = "Default Error";
      string strSQL = "";
      string strTenderChecksum = "";

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      // Bug 15941. FT. Make sure the database connection is closed
      try
      {
      if (args.has("DbConnectionClass"))
      {
        pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
        config = pDBConn.GetDBConnectionInfo();
      }
      else if (args.has("config"))
      {
        config = args.get("config", c_CASL.c_undefined) as GenericObject;
        pDBConn = new ConnectionTypeSynch(config);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strError)))  // Bug 15941. Open connection and set connectionO flagpened
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-999", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        if (!pDBConn.EstablishRollback(ref strError))
        {
          strError = "Error creating database transaction.";
            ThrowCASLErrorButDoNotCloseDBConnection("TS-1000", ref pDBConn, "Error creating database transaction.", strError);//Bug 13832-Added detailed error message
        }
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a method to connect to the database.", "code", "TS-1001", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      if (args.has("geoChecksumData"))
      {
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else
      {
        int iDBType = pDBConn.GetDBConnectionType();
        // Bug #12055 Mike O - Use misc.Parse to log errors
        strSQL = string.Format("SELECT * FROM TG_TENDER_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} AND TNDRNBR={3}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr), misc.Parse<Int64>(strTndrNbr));

        gTmp = (GenericObject)GetRecords(config, 0, strSQL, iDBType);

        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found. ", "code", "TS-1002", strError + "\nSQL statement: " + strSQL);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
      }

      // Bug #10955 Mike O
      strTenderChecksum = CreateTenderChecksum(Crypto.DefaultHashAlgorithm, "strEventNbr", strEventNbr, "strFileName", strFileName, "strTndrNbr", strTndrNbr, "geoChecksumData", gTmp);

      // Create SQL
      // Bug #12055 Mike O - Use misc.Parse to log errors
      strSQL = string.Format("UPDATE TG_TENDER_DATA SET CHECKSUM='{0}' WHERE DEPFILENBR={1} AND DEPFILESEQ={2} AND EVENTNBR={3} AND TNDRNBR={4}", strTenderChecksum, misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr), misc.Parse<Int64>(strTndrNbr));

      // Update database
      if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strError))
          HandleErrorsAndLocking("message", "Error executing SQL.", "code", "TS-1003", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix


      //***************************************
      // ALL GOOD - ACCEPT THE UPDATES and close the connection we created.
      if (!args.has("DbConnectionClass"))
      {
        if (!pDBConn.CommitToDatabase(ref strError))
            HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-1004", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }
      //***************************************  

      return true;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static string CreateFileChecksum(GenericObject depfilerecord)
    {
      string strFileNbr = GetStringValue(depfilerecord, "DEPFILENBR");
      string strFileSequenceNbr = GetStringValue(depfilerecord, "DEPFILESEQ");
      string DEPTID = GetStringValue(depfilerecord, "DEPTID");
      string OPENDT = GetStringValue(depfilerecord, "OPENDT");
      string BALDT = GetStringValue(depfilerecord, "BALDT");
      string ACCDT = GetStringValue(depfilerecord, "ACCDT");
      string UPDATEDT = GetStringValue(depfilerecord, "UPDATEDT");
      string OPEN_USERID = GetStringValue(depfilerecord, "OPEN_USERID");
      string BAL_USERID = GetStringValue(depfilerecord, "BAL_USERID");
      string ACC_USERID = GetStringValue(depfilerecord, "ACC_USERID");
      string UPDATE_USERID = GetStringValue(depfilerecord, "UPDATE_USERID");
      string GROUPID = GetStringValue(depfilerecord, "GROUPID");
      string GROUPNBR = GetStringValue(depfilerecord, "GROUPNBR");
      string BALTOTAL = GetStringValue(depfilerecord, "BALTOTAL");

      string strChecksumData = strFileNbr;
      strChecksumData += strFileSequenceNbr;
      strChecksumData += DEPTID;

      //Below is the date formatting - MM/dd/yyyy HH/mm/ss
      //strChecksumData += OPENDT;
      DateTime dtDate;
      if (OPENDT != null && OPENDT != "")
      {
        // Bug #12055 Mike O - Use misc.Parse to log errors
        dtDate = misc.Parse<DateTime>(OPENDT);
        strChecksumData += dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo);
      }
      else
        strChecksumData += OPENDT;

      //strChecksumData += BALDT;
      if (BALDT != null && BALDT != "")
      {
        // Bug #12055 Mike O - Use misc.Parse to log errors
        dtDate = misc.Parse<DateTime>(BALDT);
        strChecksumData += dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo);
      }
      else
        strChecksumData += BALDT;

      //strChecksumData += ACCDT;
      if (ACCDT != null && ACCDT != "")
      {
        // Bug #12055 Mike O - Use misc.Parse to log errors
        dtDate = misc.Parse<DateTime>(ACCDT);
        strChecksumData += dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo);
      }
      else
        strChecksumData += ACCDT;

      //strChecksumData += UPDATEDT;
      if (UPDATEDT != null && UPDATEDT != "")
      {
        // Bug #12055 Mike O - Use misc.Parse to log errors
        dtDate = misc.Parse<DateTime>(UPDATEDT);
        strChecksumData += dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo);
      }
      else
        strChecksumData += UPDATEDT;


      strChecksumData += OPEN_USERID;
      strChecksumData += BAL_USERID;
      strChecksumData += ACC_USERID;
      strChecksumData += UPDATE_USERID;
      strChecksumData += GROUPID;

      //GROUPNBR and BALTOTAL is only to be added if they are greater then 0
      if (GROUPNBR != null && GROUPNBR != "")
      {
        // Bug #12055 Mike O - Use misc.Parse to log errors
        if (misc.Parse<Int32>(GROUPNBR) > 0)
        {
          strChecksumData += GROUPNBR;
        }
      }


      //strChecksumData += BALTOTAL;
      if (BALTOTAL != null && BALTOTAL != "")
      {
        // Bug #12055 Mike O - Use misc.Parse to log errors
        if( misc.Parse<Decimal>(BALTOTAL) > 0 )
        {
          strChecksumData += BALTOTAL;
        }
      }

      return Crypto.ComputeHash("", strChecksumData);
    }

    /// <summary>
    /// Given a reference to a transaction - filenbr, fileseq, eventnbr,
    /// trannbr create a checksum for the transaction.  Search in the
    /// given database for the transaction.
    /// </summary>
    /// <param name="filenbr">transaction file number</param>
    /// <param name="fileseq">transaction file sequence</param>
    /// <param name="eventnbr">transaction event number</param>
    /// <param name="trannbr">transaction number</param>
    /// <param name="database">database where the transaction can be found</param>
    /// <returns>the checksum as a string</returns>
    public static string CreateTransactionChecksum(string filenbr, string fileseq, string eventnbr, string trannbr, GenericObject database)
    {
      GenericObject transaction = GetTransaction(filenbr, fileseq, eventnbr, trannbr, database);
      return CreateTransactionChecksum(transaction);
    }
    // Bug #11026 Mike O - Made this consistent with the other CreateTransactionChecksum (this is such a mess)
    public static string CreateTransactionChecksum(GenericObject transaction)
    {
      //#region Construct checksum data
      string trannbr = GetStringValue(transaction, "TRANNBR");
      string eventnbr = GetStringValue(transaction, "EVENTNBR");
      string filenbr = GetStringValue(transaction, "DEPFILENBR");
      string fileseq = GetStringValue(transaction, "DEPFILESEQ");
      string content_type = GetStringValue(transaction, "CONTENTTYPE");
      string ttid = GetStringValue(transaction, "TTID");
      string tax_exempt_indicator = GetStringValue(transaction, "TAXEXIND");
      string post_date_string = GetStringValue(transaction, "POSTDT");
      string void_date_string = GetStringValue(transaction, "VOIDDT");

      string checksumData = Int32.Parse(trannbr).ToString();
      checksumData += Int32.Parse(eventnbr).ToString();
      checksumData += Int32.Parse(filenbr).ToString();
      checksumData += Int32.Parse(fileseq).ToString();
      checksumData += Int32.Parse(content_type).ToString();
      checksumData += ttid;
      checksumData += tax_exempt_indicator;
      checksumData += post_date_string;
      checksumData += void_date_string;
      //#endregion
      //#region Compute hash and return
      return Crypto.ComputeHash("", checksumData);
      //#endregion
    }

    // Bug #10955 Mike O
    public static string CASLCreateTranChecksum(HashAlgorithm alg, params Object[] arg_pairs)
    {

      /* Matt -- this function will create a transaction Checksum.  In addtion to the params
             listed below the user must provide one of the following: the values to 
             create a checksum in "geoChecksumData", the DbConnection info in "config" 
             or an existing connection to the database in "DbConnectionClass."
       params    strFileName			the depfilesequence + the depfilenbr 
             strEventNbr			the event nbr
             strTranNbr			the transaction nbr */

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();
      string strTranNbr = args.get("strTranNbr").ToString();

      string strSQL = "",err="";
      string strChecksumData = "";
      string strTranChecksum = "";

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      // Bug 15941. FT. Make sure the database connection is closed
      bool connectionOpened = false;
      try
      {
      if (args.has("geoChecksumData"))
      {
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else if (args.has("DbConnectionClass") || args.has("config"))
      {
        if (args.has("DbConnectionClass"))
        {
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
          config = pDBConn.GetDBConnectionInfo();
        }
        else if (args.has("config"))
        {
          config = args.get("config", c_CASL.c_undefined) as GenericObject;
          pDBConn = new ConnectionTypeSynch(config);

            if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref err)))  // Bug 15941
              HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-975", err);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        int iDBType = pDBConn.GetDBConnectionType();
        // Bug #12055 Mike O - Use misc.Parse to log errors
        strSQL = string.Format("SELECT * FROM TG_TRAN_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} AND TRANNBR={3}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr), misc.Parse<Int64>(strTranNbr));

        gTmp = (GenericObject)GetRecords(config, 0, strSQL, iDBType);


        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found. ", "code", "TS-976", err + "\nSQL statement: " + strSQL);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a config, connection or the checksum data.", "code", "TS-977", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }


      string strContentType = GetKeyData(gTmp, "CONTENTTYPE", false);
      string strTTID = GetKeyData(gTmp, "TTID", false);
      string strTAXEXIND = GetKeyData(gTmp, "TAXEXIND", false);
      string strPostDT = GetKeyData(gTmp, "POSTDT", false);
      string strVoidDT = GetKeyData(gTmp, "VOIDDT", false);

      //TranNbr + EventNbr + FileNbr + FileSeq + ContentType + 
      //TTID + TaxExInd + PostDt + VoidDt

      DateTime dtDate;
      if (strPostDT != null && strPostDT != "")
      {
        // Bug #12055 Mike O - Use misc.Parse to log errors
        dtDate = misc.Parse<DateTime>(strPostDT);
        strPostDT = dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo);
      }

      if (strVoidDT != null && strVoidDT != "")
      {
        // Bug #12055 Mike O - Use misc.Parse to log errors
        dtDate = misc.Parse<DateTime>(strVoidDT);
        strVoidDT = dtDate.ToString("G", DateTimeFormatInfo.InvariantInfo);
      }

      // Bug #12055 Mike O - Use misc.Parse to log errors
      strChecksumData = misc.Parse<Int32>(strTranNbr).ToString();
      strChecksumData += misc.Parse<Int32>(strEventNbr).ToString();
      strChecksumData += misc.Parse<Int32>(strFileNbr).ToString();
      strChecksumData += misc.Parse<Int32>(strFileSequenceNbr).ToString();
      strChecksumData += misc.Parse<Int32>(strContentType).ToString();
      strChecksumData += strTTID; //BUG4525
      strChecksumData += strTAXEXIND;
      strChecksumData += strPostDT;
      strChecksumData += strVoidDT;

      // Bug #10955 Mike O
      // Hash
      strTranChecksum = Crypto.ComputeHash("", strChecksumData, alg);

        return strTranChecksum;
      }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }
    public static GenericObject GetCustomFields(GenericObject transaction, GenericObject database)
    {
      string filenbr = transaction.get("DEPFILENBR").ToString();
      string fileseq = transaction.get("DEPFILESEQ").ToString();
      string eventnbr = transaction.get("EVENTNBR").ToString();
      string trannbr = transaction.get("TRANNBR").ToString();
      return GetCustomFields(filenbr, fileseq, eventnbr, trannbr, database);
    }
    public static GenericObject GetCustomFields(string filenbr, string fileseq, string eventnbr, string trannbr, GenericObject database)
    {
      //#region Get custom field data
      string error_message = "";
      string sql_string = "SELECT * FROM GR_CUST_FIELD_DATA WHERE DEPFILENBR=@filenbr AND DEPFILESEQ=@fileseq AND EVENTNBR=@eventnbr AND TRANNBR=@trannbr";
      Hashtable sql_args = new Hashtable();
      sql_args["filenbr"] = filenbr;
      sql_args["fileseq"] = fileseq;
      sql_args["eventnbr"] = eventnbr;
      sql_args["trannbr"] = trannbr;
      IDBVectorReader reader = db_reader.Make(database, sql_string, CommandType.Text, 0,
        sql_args, 0, 0, null, ref error_message);
      if (error_message != "")
      {
        throw CASL_error.create("code", "TS-1233", "message", "Unable to connect to database. " + error_message);
      }
      GenericObject record_set = new GenericObject();
      reader.ReadIntoRecordset(ref record_set, 0, 0);     // Bug 15941. ReadIntoRecordset closes reader

      return record_set;
      //#endregion

    }
    public static GenericObject GetDepositTenders(GenericObject deposit, GenericObject database)
    {
      string sql_text = "SELECT * FROM TG_DEPOSITTNDR_DATA WHERE DEPOSITID=@deposit_id AND DEPOSITNBR=@deposit_nbr";
      Hashtable sql_args = new Hashtable(2);
      sql_args["deposit_id"] = GetStringValue(deposit, "DEPOSITID");
      sql_args["deposit_nbr"] = int.Parse(GetStringValue(deposit, "DEPOSITNBR"));
      string error_message = "";
      IDBVectorReader reader = db_reader.Make(database, sql_text, CommandType.Text, 0,
        sql_args, 0, 0, null, ref error_message);
      if (error_message != "")
      {
        //TODO: unchecked error condition
      }
      GenericObject deposit_tenders = new GenericObject();
      reader.ReadIntoRecordset(ref deposit_tenders, 0, 0);  // Bug 15941. ReadIntoRecordset closes reader
      return deposit_tenders;
    }
    public static GenericObject GetDeposits(GenericObject deposit_list, GenericObject database)
    {
      GenericObject deposits = new GenericObject();
      GenericObject deposit;
      for (int i = 0; i < deposit_list.getLength(); i++)
      {
        deposit = (GenericObject)deposit_list.get(i);
        deposits.insert(GetDeposit(deposit, database));
      }
      return deposits;
    }
    public static GenericObject GetDeposit(GenericObject deposit, GenericObject database)
    {
      string deposit_id = GetStringValue(deposit, "DEPOSITID");
      string deposit_nbr = GetStringValue(deposit, "DEPOSITNBR");

      return GetDeposit(deposit_id, deposit_nbr, database);
    }
    public static GenericObject GetDeposit(string deposit_id, string deposit_nbr, GenericObject database)
    {
      string sql_text = "SELECT * FROM TG_DEPOSIT_DATA WHERE DEPOSITID=@deposit_id AND DEPOSITNBR=@deposit_nbr";
      Hashtable sql_args = new Hashtable();
      sql_args["deposit_id"] = deposit_id;
      sql_args["deposit_nbr"] = deposit_nbr;
      string error_message = "";
      IDBVectorReader reader = db_reader.Make(database, sql_text, CommandType.Text, 0, sql_args, 0, 0, null, ref error_message);
      if (error_message != "")
      {
        //TODO: unchecked error condition
        //error_message=String.Format("Could not find transaction {3} in event {0}{1}-{2}.",filenbr, fileseq.PadLeft(3,'0'), eventnbr, trannbr);
        //throw CASL_error.create("code","TS-1230", "message",error_message);
      }
      GenericObject results = new GenericObject();
      reader.ReadIntoRecordset(ref results, 0, 0);    // Bug 15941. ReadIntoRecordset closes reader

      if (results.getLength() != 1)
      {
        //error_condition
      }

      return (GenericObject)results.get(0);
    }
    public static GenericObject GetItems(GenericObject transaction, GenericObject database)
    {
      string filenbr = transaction.get("DEPFILENBR").ToString();
      string fileseq = transaction.get("DEPFILESEQ").ToString();
      string eventnbr = transaction.get("EVENTNBR").ToString();
      string trannbr = transaction.get("TRANNBR").ToString();
      return GetItems(filenbr, fileseq, eventnbr, trannbr, database);
    }
    public static GenericObject GetItems(string filenbr, string fileseq, string eventnbr, string trannbr, GenericObject database)
    {
      //#region Get item data
      string error_message = "";
      string sql_string = "SELECT * FROM TG_ITEM_DATA WHERE DEPFILENBR=@filenbr AND DEPFILESEQ=@fileseq AND EVENTNBR=@eventnbr AND TRANNBR=@trannbr";
      Hashtable sql_args = new Hashtable();
      sql_args["filenbr"] = filenbr;
      sql_args["fileseq"] = fileseq;
      sql_args["eventnbr"] = eventnbr;
      sql_args["trannbr"] = trannbr;
      IDBVectorReader reader = db_reader.Make(database, sql_string, CommandType.Text, 0,
        sql_args, 0, 0, null, ref error_message);
      if (error_message != "")
      {
        throw CASL_error.create("code", "TS-1232", "message", "Unable to connect to database. " + error_message);
      }
      GenericObject record_set = new GenericObject();
      reader.ReadIntoRecordset(ref record_set, 0, 0);       // Bug 15941. ReadIntoRecordset closes reader

      return record_set;
      //#endregion
    }

    public static GenericObject GetTransaction(GenericObject transaction, GenericObject database)
    {
      string filenbr = transaction.get("DEPFILENBR").ToString();
      string fileseq = transaction.get("DEPFILESEQ").ToString();
      string eventnbr = transaction.get("EVENTNBR").ToString();
      string trannbr = transaction.get("TRANNBR").ToString();
      return GetTransaction(filenbr, fileseq, eventnbr, trannbr, database);
    }
    public static GenericObject GetTransaction(string filenbr, string fileseq, string eventnbr, string trannbr, GenericObject database)
    {
      //#region Get transaction data
      string error_message = "";
      string sql_string = "SELECT * FROM TG_TRAN_DATA WHERE DEPFILENBR=@filenbr AND DEPFILESEQ=@fileseq AND EVENTNBR=@eventnbr AND TRANNBR=@trannbr";
      Hashtable sql_args = new Hashtable();
      sql_args["filenbr"] = filenbr;
      sql_args["fileseq"] = fileseq;
      sql_args["eventnbr"] = eventnbr;
      sql_args["trannbr"] = trannbr;

      IDBVectorReader reader = db_reader.Make(database, sql_string, CommandType.Text, 0,
        sql_args, 0, 0, null, ref error_message);
      if (error_message != "")
      {
        throw CASL_error.create("code", "TS-1229-N", "message", "Unable to connect to database. " + error_message); //Bug 23378 NK Make all TS-1229 unique
      }
      GenericObject record_set = new GenericObject();
      reader.ReadIntoRecordset(ref record_set, 0, 0);     // Bug 15941. ReadIntoRecordset closes reader

      if (record_set.getLength() == 0)
      {
        error_message = String.Format("Could not find transaction {3} in event {0}{1}-{2}.", filenbr, fileseq.PadLeft(3, '0'), eventnbr, trannbr);
        throw CASL_error.create("code", "TS-1230", "message", error_message);
      }
      else if (record_set.getLength() > 1)
      {
        error_message = String.Format("More than one transaction found for transaction number {3} in event {0}{1}-{2}.", filenbr, fileseq.PadLeft(3, '0'), eventnbr, trannbr);
        throw CASL_error.create("code", "TS-1231", "message", error_message);
      }

      return record_set.get(0) as GenericObject;
      //#endregion
    }
    public static bool ValidateTransactionChecksum(string filenbr, string fileseq, string eventnbr, string trannbr, GenericObject database)
    {
      //#region Get transaction
      GenericObject transaction = GetTransaction(filenbr, fileseq, eventnbr, trannbr, database);
      //#endregion
      //#region Validate with helper
      return ValidateTransactionChecksum(transaction);
      //#endregion
    }
    public static bool ValidateTransactionChecksum(GenericObject transaction)
    {
      return CreateTransactionChecksum(transaction).Equals((string)transaction.get("CHECKSUM"));
    }

    public static bool ValidateTranChecksum(params Object[] arg_pairs)
    {
      /* Matt -- this function creates a checksum and compares it to the existing checksum.  
             In addtion to the params listed below the user must provide one of the 
             following: the values to create a checksum in "geoChecksumData", the 
             DbConnection info in "config" or an existing connection to the database in 
             "DbConnectionClass."
        params   strFileName			the depfilesequence + the depfilenbr 
             strEventNbr			the event nbr
             strTranNbr			the tender nbr */

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();
      string strTranNbr = args.get("strTranNbr").ToString();

      string strSQL = "",err="";
      string strTranChecksum = "";
      string strOldChecksum = "";

      bool bVal = false;

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      // Bug 15941. FT. Make sure the database connection is closed
      bool connectionOpened = false;
      try
      {
      // Either the connection was passed in or we create one
      if (args.has("DbConnectionClass") || args.has("config"))
      {
        if (args.has("DbConnectionClass"))
        {
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
          config = pDBConn.GetDBConnectionInfo();
        }
        else if (args.has("config"))
        {
          config = args.get("config", c_CASL.c_undefined) as GenericObject;
          pDBConn = new ConnectionTypeSynch(config);

            if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref err)))  // Bug 15941
              HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1036", err);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        int iDBType = pDBConn.GetDBConnectionType();

        // Bug #12055 Mike O - Use misc.Parse to log errors
        if (pDBConn.GetDBConnectionType() == ConnectionTypeSynch.m_iCurrentDatabase_SqlServer)
          strSQL = string.Format("SELECT * FROM TG_TRAN_DATA (NOLOCK) WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} AND TRANNBR={3}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr), misc.Parse<Int64>(strTranNbr));
        else
          strSQL = string.Format("SELECT * FROM TG_TRAN_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} AND TRANNBR={3}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr), misc.Parse<Int64>(strTranNbr));

        gTmp = (GenericObject)GetRecords(config, 0, strSQL, iDBType);



        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found. ", "code", "TS-1037", err + "\nSQL statement: " + strSQL);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
      }
      else if (args.has("geoChecksumData"))
      {
        // Or we don't need a connection because the structure is already passed in
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a config, connection or the checksum data.", "code", "TS-1038", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      // Bug #10955 Mike O
      strOldChecksum = GetKeyData(gTmp, "CHECKSUM", false);
      HashAlgorithm hashAlg = Crypto.HashAlgorithmUsed(strOldChecksum);

      if (hashAlg == null)
      {
        // Bug #12800 Mike O - If the checksum doesn't match any possible algorithms, fail validation
        return false;
      }

      // The newly created checksum
      strTranChecksum = CASLCreateTranChecksum(hashAlg, "strEventNbr", strEventNbr, "strFileName", strFileName, "strTranNbr", strTranNbr, "geoChecksumData", gTmp);
      // End Bug #10955 Mike O

      if (strOldChecksum == strTranChecksum)
      {
        bVal = true;
      }

      return bVal;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static bool UpdateTranChecksum(params Object[] arg_pairs)
    {
      /* Matt -- this function will update the tran file checksum.  In addtion to the 
             params listed below the user must provide one of the following: the 
             DbConnection info in "config" or an existing connection to the database 
             in "DbConnectionClass." The user has the option to provide a structure
             that will be used to create the checksum, if not the information will be
             gathered through the existing or newly made connection.
      params     strFileName			the depfilesequence + the depfilenbr 
             strEventNbr			the event nbr
             strTranNbr			the tender nbr */

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();
      string strTranNbr = args.get("strTranNbr").ToString();

      string strError = "Default Error";
      string err="";// Bug# 27153 DH - Clean up warnings when building ipayment
      string strTranChecksum = "";

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);
      // Bug 15941. FT. Make sure the database connection is closed
      bool connectionOpened = false;
      try
      {
      if (args.has("DbConnectionClass"))
      {
        pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
        config = pDBConn.GetDBConnectionInfo();
      }
      else if (args.has("config"))
      {
        config = args.get("config", c_CASL.c_undefined) as GenericObject;
        pDBConn = new ConnectionTypeSynch(config);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref err)))  // Bug 15941
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1005", err);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        if (!pDBConn.EstablishRollback(ref strError))
        {
          strError = "Error creating database transaction.";
            ThrowCASLErrorButDoNotCloseDBConnection("TS-1006", ref pDBConn, "Error creating database transaction.", strError);
        }
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a method to connect to the database.", "code", "TS-1007", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      if (args.has("geoChecksumData"))
      {
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else
      {
        // Bug 18766: Parameterized
        string sql_string =
          @"SELECT * FROM TG_TRAN_DATA 
            WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND EVENTNBR=@eventnbr AND TRANNBR=@trannbr";

        HybridDictionary sql_args = new HybridDictionary();
        sql_args["depfilenbr"] = strFileNbr;
        sql_args["depfileseq"] = strFileSequenceNbr;
        sql_args["eventnbr"] = strEventNbr;
        sql_args["trannbr"] = strTranNbr;

        gTmp = DBUtils.readSelectIntoGeo(pDBConn, sql_string, sql_args);
        if (gTmp.Count == 0)
        {
          HandleErrorsAndLocking("message", "UpdateTranChecksum: No records found. ", "code", "TS-1008", "SQL statement: " + sql_string);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

      }

      // Bug #10955 Mike O
      strTranChecksum = CASLCreateTranChecksum(Crypto.DefaultHashAlgorithm, "strEventNbr", strEventNbr, "strFileName", strFileName, "strTranNbr", strTranNbr, "geoChecksumData", gTmp);

      try
      {
        // Bug 18766: Parameterized
        string sql_string = "UPDATE TG_TRAN_DATA SET CHECKSUM=@checksum WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND EVENTNBR=@eventnbr AND TRANNBR=@trannbr";
        HybridDictionary sql_args = new HybridDictionary();
        sql_args["checksum"] = strTranChecksum;
        sql_args["depfilenbr"] = strFileNbr;
        sql_args["depfileseq"] = strFileSequenceNbr;
        sql_args["eventnbr"] = strEventNbr;
        sql_args["trannbr"] = strTranNbr;

        string errorMsg;
        int count = ParamUtils.runExecuteNonQueryCommitRollback(pDBConn, sql_string, sql_args, out errorMsg);
        if (count != 1)
      {
          HandleErrorsAndLocking("message", "UpdateTranChecksum: Updated: " + count + " records instead of 1", "code", "TS-1010b", "Wrong update count");
      }
      }
      catch (Exception e)
      {
        HandleErrorsAndLocking("message", "UpdateTranChecksum: Unable to commit changes to database.", "code", "TS-1010", e.Message);
      }

      return true;
    }
      finally
      {
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static bool ValidateFileAllChecksums(params Object[] arg_pairs)
    {
      /* Matt -- Function that will validate Tran, Item, Cust and Tender checksums for a file.  
             In addtion to the params listed below the user must provide one of the 
             following: the DbConnection info in "config" or an existing connection to the 
             database in "DbConnectionClass." 
       params    strFileName			the depfilesequencenbr + the depfilenbr   */


      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      ConnectionTypeSynch pDBConn = null;
      GenericObject gTmp = null;
      GenericObject gTmp2 = null;
      GenericObject gTmp3 = null;

      string strEventNbr = "";
      string strTranNbr = "";
      string strTndrNbr = "";
      //string strError = "Default Error";

      string strFileName = args.get("strFileName").ToString();
      string strSQL = "",err="";

      bool bVal = true;

      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      // Bug 15941. FT.
      bool connectionOpened = false;
      try
      {
      //**********************************************************************
      // Check if connection passed
      if (args.has("DbConnectionClass"))
      {
        pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
        config = pDBConn.GetDBConnectionInfo();
      }
      else if (args.has("config"))
      {
        config = args.get("config", c_CASL.c_undefined) as GenericObject;
        pDBConn = new ConnectionTypeSynch(config);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref err)))  // Bug 15941
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1042", err);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a method to connect to the database.", "code", "TS-1043", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }
      //**********************************************************************  

      int iDBType = pDBConn.GetDBConnectionType();

      //validate the tran file checksum     

      GenericObject gTran = null;
      GenericObject gEvent = null;
      GenericObject gTender = null;

      // Bug #12055 Mike O - Use misc.Parse to log errors
      strSQL = string.Format("SELECT * FROM TG_PAYEVENT_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr));

      gEvent = (GenericObject)GetRecords(config, 1, strSQL, iDBType);


      if (gEvent == null)
      {
        return true; //BUG4489 , return true if validating empty file. 
        //HandleErrorsAndLocking("message", "No records found. SQL statement: " + strSQL, "code", "TS-1044");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      for (int i = 0; i < (int)gEvent.getLength(); i++)
      {
        gTmp = ((GenericObject)gEvent.get(i)); //
        strEventNbr = gTmp.get("EVENTNBR").ToString();

        // Validate the current event
        if (!ValidatePayEventChecksum("strEventNbr", strEventNbr, "strFileName", strFileName, "geoChecksumData", gTmp))
          return false;

        // Get the Tran Data
        // Bug #12055 Mike O - Use misc.Parse to log errors
        strSQL = string.Format("SELECT * FROM TG_TRAN_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr));
        gTran = (GenericObject)GetRecords(config, 1, strSQL, iDBType);

        if (gTran == null)
        {
          HandleErrorsAndLocking("message", "No records found. ", "code", "TS-1045", err + "\n SQL statement: " + strSQL);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
          //BUG5882 Ann CORE File may have tenders but no transactions
          //BUG5882 HandleErrorsAndLocking("message", "No records found. SQL statement: " + strSQL, "code", "TS-1045");
        }
        if (gTran != null)//added for BUG5882 
        {//added for BUG5882
          for (int x = 0; x < (int)gTran.getLength(); x++)
          {
            gTmp2 = ((GenericObject)gTran.get(x));
            strTranNbr = gTmp2.get("TRANNBR").ToString();

            // Validate the Checksum for the Transaction
            if (!ValidateTranChecksum("strTranNbr", strTranNbr, "strEventNbr", strEventNbr, "strFileName", strFileName, "geoChecksumData", (GenericObject)gTran.get(x)))
              return false;

            /*if(!args.has("DbConnectionClass"))
            {
              // Validate the Checksum of any Item Data or CustField Data records
              if( !ValidateItemDataChecksum("strTranNbr", strTranNbr, "strEventNbr", strEventNbr, "strFileName", strFileName, "config", config) || 
                !ValidateCustFieldChecksum("strTranNbr", strTranNbr, "strEventNbr", strEventNbr, "strFileName", strFileName, "config", config))
                return false;
            }
            else
            {*/
            // Validate the Checksum of any Item Data or CustField Data records
            if (!ValidateItemDataChecksum("strTranNbr", strTranNbr, "strEventNbr", strEventNbr, "strFileName", strFileName, "DbConnectionClass", pDBConn) ||
              !ValidateCustFieldChecksum("strTranNbr", strTranNbr, "strEventNbr", strEventNbr, "strFileName", strFileName, "DbConnectionClass", pDBConn))
              return false;
            //}
          }
        }//added for BUG5882

        //Get all Tenders for this Event and File
        // Bug #12055 Mike O - Use misc.Parse to log errors
        strSQL = string.Format("SELECT * FROM TG_TENDER_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr));
        gTender = (GenericObject)GetRecords(config, 1, strSQL, iDBType);

        // Daniel chenged this. Suspended list will have no tenders.
        //if(gTender = null)
        if (gTender != null)
        {
          // Daniel chenged this. Suspended list will have no tenders.
          //HandleErrorsAndLocking("message", "No records found. SQL statement: " + strSQL, "code", "TS-1046");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix


          for (int y = 0; y < (int)gTender.getLength(); y++)
          {
            gTmp3 = ((GenericObject)gTender.get(y));
            strTndrNbr = gTmp3.get("TNDRNBR").ToString();

            // Validate the Checksum for the Tender
            if (!ValidateTenderChecksum("strTndrNbr", strTndrNbr, "strEventNbr", strEventNbr, "strFileName", strFileName, "geoChecksumData", (GenericObject)gTender.get(y)))
              return false;
          }
        }
      }

        return bVal;
      }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    // Bug #10955 Mike O
    public static string CreatePayEventChecksum(HashAlgorithm alg, params Object[] arg_pairs)
    {
      /* Matt -- this function will create a payevent Checksum.  In addtion to the params
             listed below the user must provide one of the following: the values to 
             create a checksum in "geoChecksumData", the DbConnection info in "config" 
             or an existing connection to the database in "DbConnectionClass."
       params    strFileName			the depfilesequence + the depfilenbr 
             strEventNbr			the event nbr */

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();

      string strSQL = "",err="";
      string strChecksumData = "";
      string strPayEventChecksum = "";

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      // Bug 15941. FT. Make sure the database connection is closed
      bool connectionOpened = false;
      try
      {
      if (args.has("geoChecksumData"))
      {
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else if (args.has("DbConnectionClass") || args.has("config"))
      {
        if (args.has("DbConnectionClass"))
        {
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
          config = pDBConn.GetDBConnectionInfo();
        }
        else if (args.has("config"))
        {
          config = args.get("config", c_CASL.c_undefined) as GenericObject;
          pDBConn = new ConnectionTypeSynch(config);

            if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref err)))  // Bug 15941
              HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-969", err);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        int iDBType = pDBConn.GetDBConnectionType();
        // Bug #12055 Mike O - Use misc.Parse to log errors
        strSQL = string.Format("SELECT * FROM TG_PAYEVENT_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr));

        gTmp = (GenericObject)GetRecords(config, 0, strSQL, iDBType);

        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found. ", "code", "TS-970", err + "\nSQL statement: " + strSQL);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a config, connection or the checksum data.", "code", "TS-971", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      string strUserID = GetKeyData(gTmp, "USERID", false);
      string strStatus = GetKeyData(gTmp, "STATUS", false);

      // Bug #8400 - Mike O - Added CREATION_DT to the checksum
      // Bug #11860 - Qi Guo - Added a check for CREATION_DT to avoid parse empty string
      string strCreationDate = "";
      if (gTmp.has("CREATION_DT"))
      {
        string strCreationDT = GetKeyData(gTmp, "CREATION_DT", false);
        if (strCreationDT != "")
        {
          strCreationDate = DateTime.Parse(strCreationDT).ToString("G", DateTimeFormatInfo.InvariantInfo);
        }
      }
      // Bug #12055 Mike O - Use misc.Parse to log errors
      string strDate = misc.Parse <DateTime>(GetKeyData(gTmp, "MOD_DT", false)).ToString("G", DateTimeFormatInfo.InvariantInfo);

      //NOTE: EventNbr + DepFileNbr + DepFileSeq + UserID + Status + ModDate
      
      // Bug #14185 Mike O - Move string building into its own function
      strChecksumData = BuildPayEventChecksumString(strEventNbr, strFileNbr, strFileSequenceNbr, strUserID, strStatus, strDate);

      // Bug #10955 Mike O
      // Hash
      strPayEventChecksum = Crypto.ComputeHash("", strChecksumData, alg);

        return strPayEventChecksum;
      }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static bool UpdatePayEventChecksum(params Object[] arg_pairs)
    {
      /* Matt -- this function will update the payevent file checksum.  In addtion to the 
             params listed below the user must provide one of the following: the 
             DbConnection info in "config" or an existing connection to the database 
             in "DbConnectionClass." The user has the option to provide a structure
             that will be used to create the checksum, if not the information will be
             gathered through the existing or newly made connection.
      params     strFileName			the depfilesequence + the depfilenbr 
             strEventNbr			the event nbr */

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();

      string strError = "Default Error";
      string strSQL = "";
      string strPayEventChecksum = "";

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      // Bug 15941. FT. Make sure the database connection is closed
      bool connectionOpened = false;
      try
      {
      if (args.has("DbConnectionClass"))
      {
        pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
        config = pDBConn.GetDBConnectionInfo();
      }
      else if (args.has("config"))
      {
        config = args.get("config", c_CASL.c_undefined) as GenericObject;
        pDBConn = new ConnectionTypeSynch(config);

          if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref strError)))  // Bug 15941
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-993", strError);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

        if (!pDBConn.EstablishRollback(ref strError))
        {
          strError = "Error creating database transaction.";
            ThrowCASLErrorButDoNotCloseDBConnection("TS-994", ref pDBConn, "Error creating database transaction.", strError);
        }
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a method to connect to the database.", "code", "TS-995", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      if (args.has("geoChecksumData"))
      {
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else
      {
        int iDBType = pDBConn.GetDBConnectionType();
        // Bug #12055 Mike O - Use misc.Parse to log errors
        strSQL = string.Format("SELECT * FROM TG_PAYEVENT_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr));

        gTmp = (GenericObject)GetRecords(config, 0, strSQL, iDBType);

        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found.", "code", "TS-996",strError + "\n SQL statement: " + strSQL);// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }
      }

      // Bug #10955 Mike O
      strPayEventChecksum = CreatePayEventChecksum(Crypto.DefaultHashAlgorithm, "strEventNbr", strEventNbr, "strFileName", strFileName, "geoChecksumData", gTmp);


      try
      {
        // Bug 18766: Parameterized
        string sql_string = "UPDATE TG_PAYEVENT_DATA WITH (ROWLOCK) SET CHECKSUM=@checksum WHERE DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND EVENTNBR=@eventnbr";
        HybridDictionary sql_args = new HybridDictionary();
        sql_args["checksum"] = strPayEventChecksum;
        sql_args["depfilenbr"] = strFileNbr;
        sql_args["depfileseq"] = strFileSequenceNbr;
        sql_args["eventnbr"] = strEventNbr;

        // Bug 25654: Changed from runExecuteNonQueryCommitRollback; we need to do the commit / rollback at a higher level
        int count = ParamUtils.runExecuteNonQuery(pDBConn, sql_string, sql_args);
        if (count != 1)
        {
          HandleErrorsAndLocking("message", "UpdatePayEventChecksum: Updated: " + count + " records instead of 1", "code", "TS-997b", "");
        }

      }
      catch (Exception e)
      {
        HandleErrorsAndLocking("message", "UpdatePayEventChecksum: Error executing SQL.", "code", "TS-997", e.ToString());
      }


      return true;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static Object CASLIsValidSession(params Object[] args)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      // Imported from UPHS by DH 07/02/2008
      // BUG#5657 Use one user to perform Stress Test SX 06/16/2008
      bool check_user_session = (bool)c_CASL.GEO.get("check_user_session", true);
      if (!check_user_session)
        return true;

      // END OF BUG#5657
      GenericObject geo_args = misc.convert_args(args);

      string strUserID = (string)geo_args.get("user_id");
      string strAppID = (string)geo_args.get("app_id");
      string strSessionID = (string)geo_args.get("session_id");

      return SessionValidator.GetInstance().validateSessionID(strUserID, strAppID, strSessionID);
    }

    public static bool IsCheckUserSession()
    {
      return (bool)c_CASL.GEO.get("check_user_session", true);
    }

    // Bug 17031 UMN Rewrite sql_verify in C# so can do intelligent check for semicolon

    /// <summary>
    /// Checks to see if sql string has any bad characters (sql injection) and returns true
    /// or an error.
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static object CASLIsSQLValid(params Object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      string strSQL = (string)geo_args.get("sql_string");
      return IsSQLValid(strSQL);
    }

    /// <summary>
    /// Blacklist of characters
    /// </summary>
    private static readonly List<String> aExcludeList = new List<String> { 
      @"--",
      @"\",
      @"$", 
      @"'''" 
    };

    /// <summary>
    /// Checks to see if sql string has any bad characters (sql injection) and returns true
    /// or an error.
    /// </summary>
    /// <param name="strSQL"></param>
    /// <returns></returns>
    public static object IsSQLValid(string strSQL)
    {
      bool ret = true;
      try
      {
        if (String.IsNullOrEmpty(strSQL))
        {
          Logger.LogError("SQL Error(SQL-900) Empty SQL string", "SQL Check");
          return misc.MakeCASLError("Query Error", "SQL-900", "Empty SQL string");
        }

        string strSemi = strSQL.Replace("&amp", "").Replace("&lt;", "").Replace("&gt;", "");

        if (strSemi.Contains(";") && !strSemi.EndsWith(";"))
        {
          Logger.cs_log(strSemi);
          Logger.LogError("SQL Error(SQL-901) Invalid SQL detected [;]", "SQL Check");
          return misc.MakeCASLError("Query Error", "SQL-901", "Invalid SQL detected [;]");
        }

        foreach (var bad in aExcludeList)
        {
          if (strSQL.Contains(bad))
          {
            string strMsg = String.Format("Invalid SQL detected [{0}]", bad);
            Logger.LogError(String.Format("SQL Error(SQL-902) Invalid SQL detected [{0}]", bad), "SQL Check");
            return misc.MakeCASLError("Query Error", "SQL-902", strMsg);
          }
        }
      }
      catch (Exception ex)
      {
        Logger.LogError(String.Format("SQL Error(SQL-903) {0}", ex.Message), "SQL Check");
        // Bug 17849
        Logger.cs_log_trace("Error in IsSQLValid", ex);
          return misc.MakeCASLError("Query Error", "SQL-903", ex.Message);
      }
      return ret;
    }

    // END Bug 17031 UMN Rewrite sql_verify in C# so can do intelligent check for semicolon

    // Bug #11869 Mike O - This shouldn't be used any more (all session validation done from CASLIsValidSession)
    /*public static bool IsValidSession2(GenericObject config, string user_id, string session_id)
    {
      // This needs to not be checked all the time.
      if( !IsCheckUserSession() ) return true;

      // TODO:  @varname is SQLServer specific -- unspecify it
      string sql_string="SELECT COUNT(*) AS COUNT FROM TG_USERSESSION_DATA WHERE USERID=@userid AND SESSIONID=@sessionid";
      string error_message="";
      Hashtable sql_args=new Hashtable();
      sql_args["userid"]=user_id;
      sql_args["sessionid"]=session_id;

      IDBVectorReader reader=db_reader.Make(config, sql_string, CommandType.Text, 0,
        sql_args, 0, 0, null, ref error_message);
      
      if(error_message!="")
      {
        throw CASL_error.create("message","Error connecting to database to verify user session. "+error_message, "code","TS-1224");
      }

      GenericObject record_set=new GenericObject();
      reader.ReadIntoRecordset(ref record_set, 0, 0);

      bool isValid = false;
      // An empty record set means we could not find anything
      if(record_set.getLength() == 0)
      {
        isValid = false;
      } 
      else
      {
        // The first object in the record set will hold the COUNT of entries
        // with the given username/session args
        GenericObject firstRecord = (GenericObject) record_set.get(0);
        // The session is valid iff there exists exactly one entry using the
        // username/session argumenst
        isValid = firstRecord.get("COUNT", 0).Equals(1);
      }
      return isValid;
    }*/

    /// <summary>
    /// Bug 18766: Changed to be parameterized.
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static Object UpdateBankAcctID(params Object[] arg_pairs)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject data = args.get("data") as GenericObject;
      ConnectionTypeSynch pDBConn = new ConnectionTypeSynch(data);
      string strFileName = args.get("FILENAME").ToString();
      string strTenderNbr = args.get("TNDRNBR").ToString();
      string strEventNbr = args.get("EVENTNBR").ToString();
      string strBankAcctNbrID = args.get("BANKACCTID").ToString();
      //string strSQL = ""; Bug# 27153 DH - Clean up warnings when building ipayment
      string strError = "";

      // Bug 15941. FT. Make sure the database connection is closed
      try
      {
      //**********************************************************************
      // Connect to database.
      if (!pDBConn.EstablishDatabaseConnection(ref strError))
          HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1114", strError);//Bug 13832-Added detailed error message // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix

      if (!pDBConn.EstablishRollback(ref strError))
          HandleErrorsAndLocking("message", "Error creating database transaction.", "code", "TS-1115", strError);//Bug 13832-Added detailed error message // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      //**********************************************************************        

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      // Create SQL
      // Bug #12055 Mike O - Use misc.Parse to log errors.  
      // Bug 18766: Let the SQL library do the parsing
      try
      {
        string sql_string = "UPDATE TG_TENDER_DATA SET BANKACCTID=@bankid WHERE TNDRNBR=@tendernbr AND EVENTNBR=@eventnbr AND DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq";
        HybridDictionary sql_args = new HybridDictionary();
        sql_args["bankid"] = strBankAcctNbrID;
        sql_args["tendernbr"] = strTenderNbr;
        sql_args["eventnbr"] = strEventNbr;
        sql_args["depfilenbr"] = strFileNbr;
        sql_args["depfileseq"] = strFileSequenceNbr;

        string errorMsg;
        int count = ParamUtils.runExecuteNonQueryCommitRollback(pDBConn, sql_string, sql_args, out errorMsg);
        if (count != 1)
        {
          HandleErrorsAndLocking("message", "UpdateBankAcctID: Updated: " + count + " records instead of 1", "code", "TS-1116b", "Wrong update count");
        }
      }
      catch (Exception e)
      {
        // Log error and throw exception.  No rollback needed.
        HandleErrorsAndLocking("message", "UpdateBankAcctID: Unable to update BANCACCTID in the TENDERS table. Error executing SQL.", "code", "TS-1116", e.ToString());
      }
      // No errors
      return true;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnection(pDBConn);
      }
    }

    public static bool ValidatePayEventChecksum(params Object[] arg_pairs)
    {
      /* Matt -- this function creates a checksum and compares it to the existing checksum.  
             In addtion to the params listed below the user must provide one of the 
             following: the values to create a checksum in "geoChecksumData", the 
             DbConnection info in "config" or an existing connection to the database in 
             "DbConnectionClass."
        params   strFileName			the depfilesequence + the depfilenbr 
             strEventNbr			the event nbr */

      GenericObject args = misc.convert_args(arg_pairs);
      GenericObject config = null;
      GenericObject gTmp = null;

      ConnectionTypeSynch pDBConn = null;

      string strFileName = args.get("strFileName").ToString();
      string strEventNbr = args.get("strEventNbr").ToString();

      string strSQL = "",err="";
      string strPayEventChecksum = "";
      string strOldChecksum = "";

      bool bVal = false;

      // Break the file into file number and sequence number
      string strFileNbr = GetFileNumber(strFileName);
      string strFileSequenceNbr = GetFileSequence(strFileName);

      // Bug 15941. FT. Make sure the database connection is closed
      bool connectionOpened = false;
      try
      {
      // Either the connection was passed in or we create one
      if (args.has("DbConnectionClass") || args.has("config"))
      {
        if (args.has("DbConnectionClass"))
        {
          pDBConn = (ConnectionTypeSynch)args.get("DbConnectionClass");
          config = pDBConn.GetDBConnectionInfo();
        }
        else if (args.has("config"))
        {
          config = args.get("config", c_CASL.c_undefined) as GenericObject;
          pDBConn = new ConnectionTypeSynch(config);

            if (!(connectionOpened = pDBConn.EstablishDatabaseConnection(ref err)))  // Bug 15941
              HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-1025", err);//Bug 13832-Added detailed error message// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

        int iDBType = pDBConn.GetDBConnectionType();
        // Bug #12055 Mike O - Use misc.Parse to log errors
        strSQL = string.Format("SELECT * FROM TG_PAYEVENT_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2}", misc.Parse<Int64>(strFileNbr), misc.Parse<Int64>(strFileSequenceNbr), misc.Parse<Int64>(strEventNbr));

        gTmp = (GenericObject)GetRecords(config, 0, strSQL, iDBType);

        if (gTmp == null)
        {
          HandleErrorsAndLocking("message", "No records found. ", "code", "TS-1026", err + "\n SQL statement: " + strSQL);//Bug 13832-Added detailed error message // Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
        }

      }
      else if (args.has("geoChecksumData"))
      {
        // Or we don't need a connection because the structure is already passed in
        gTmp = args.get("geoChecksumData") as GenericObject;
      }
      else
      {
        HandleErrorsAndLocking("message", "Please provide a config, connection or the checksum data.", "code", "TS-1027", "");// Bug# 5813 deadlock fix. DH 07/30/2008//Bug 11329 NJ-show verbose fix
      }

      // Bug #10955 Mike O
      strOldChecksum = GetKeyData(gTmp, "CHECKSUM", false);
      HashAlgorithm hashAlg = Crypto.HashAlgorithmUsed(strOldChecksum);

      if (hashAlg == null)
      {
        // Bug #12800 Mike O - If the checksum doesn't match any possible algorithms, fail validation
        return false;
      }

      // The newly created checksum
      strPayEventChecksum = CreatePayEventChecksum(hashAlg, "strEventNbr", strEventNbr, "strFileName", strFileName, "geoChecksumData", gTmp);
      // End Bug #10955 Mike O

      if (strOldChecksum == strPayEventChecksum)
      {
        bVal = true;
      }

      return bVal;
    }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnectionIfOpenedLocally(pDBConn, connectionOpened);
      }
    }

    public static bool InsertBLOBColumns(params Object[] arg_pairs)
    {
      // Daniel wrote this function. Please do not change without talking to me first.
      // ALSO, PLEASE ADD COMMENTS ABOUT YOUR CHANGES!!!

      /*
        This function inserts large byte arrays into the BLOB column.
        Please follow the sample below for naming conventions. There are place holders in the embeded SQL statements that
        are different base on the database type.
           
        
        SAMPLE TEST - You can create the function below and run it.
      
        private void InserBLOBFields_Click(object sender, System.EventArgs e)
        {
          // Replace each BLOB field with :p + field sequence number.
          string strSQL = "";
          string strDBConnection = "";
          GenericObject geoTopLevel = new GenericObject(); // Contains all data.

          int iCurrentDatabase_SqlServer      = 2;

          if(SQLCheck.Checked)
          {
            // SLQ Server
            geoTopLevel.Add("db_type", iCurrentDatabase_SqlServer);
            strSQL = "INSERT INTO MIN (RECEIPT,IMAGE,IMAGE2) VALUES ('A', @p0, @p1)";
            strDBConnection = "Data Source=(local);Initial Catalog=T4;User Id=cashier;Password=cashier";
          }


          //------------------------------
          // Test - Each array must correcpond to parameter sequence above.
          byte[] bData    = new byte [3];
          bData[0]= (byte)'t';
          bData[1]= (byte)'e';
          
          byte[] bData2    = new byte [3];
          bData2[0]= (byte)'s';
          bData2[1]= (byte)'t';
          //------------------------------

          //--------------------------------------------------------------
          // Insert byte arrays
          GenericObject geoData = new GenericObject(); // Main container of all data.
          geoData.Add("slq_statement", strSQL);
          geoData.Add("str_error", ""); // Error will be set back in here.
          geoData.insert(bData);  // PARAM 0
          geoData.insert(bData2); // PARAM 1
          //--------------------------------------------------------------
          
          //--------------------------------------------------------------
          // Insert field names
          GenericObject geoColumns = new GenericObject();
          geoColumns.insert("IMAGE");
          geoColumns.insert("IMAGE2");
          geoData.Add("columns", geoColumns);
          //--------------------------------------------------------------      
          
          //--------------------------------------------------------------
          // Insert all the data GEOs
          geoTopLevel.insert(geoData);
          geoTopLevel.insert(geoData);
          geoTopLevel.Add("db_connection_str", strDBConnection);// Also pass the connection string
          //--------------------------------------------------------------
          
          if(InsertBLOBColumns(geoTopLevel))
          {
            // ORACLE SAMPLE OF LOCAL CONNECTION
            if(CommitOutsideCheck.Checked)
            {
              try
              {
                pOraTran.Commit();
                pOraConn.Close();
              }
              catch(Exception ex)
              {
                MessageBox.Show(ex.Message);
                return;
              }
            }
            MessageBox.Show("Worked!");
          }
          else
          {
            // Get the relevant error message set in PostBLOB().
            MessageBox.Show("Did not work! Error: " + geoTopLevel.get("str_error"));
          }      
        }
      */

      GenericObject args = misc.convert_args(arg_pairs);

      // Sql Server
      SqlConnection pSqlConn = null;
      SqlTransaction pSqlTran = null;
      SqlCommand pSqlCmd = null;

      string strDBConnection = (string)args.get("db_connection_str");

      int iCurrentDatabase_SqlServer = 2;
      int iDBType = (int)args.get("db_type");
      bool bUseExternalCommand = args.has("command_objet");

      //---------------------------------------------------------------------
      // Optional: Command object can be passed in which case database, 
      // transaction and committing will be responsibility of the caller.
      if (bUseExternalCommand)
      {
        if (iDBType == iCurrentDatabase_SqlServer)
          pSqlCmd = (SqlCommand)args.get("command_objet");
      }
      //---------------------------------------------------------------------


      //---------------------------------------------------------------------
      if (!bUseExternalCommand)
      {
        try
        {
          // Open Database
          if (iDBType == iCurrentDatabase_SqlServer)
            pSqlConn = new SqlConnection(strDBConnection);

          if (iDBType == iCurrentDatabase_SqlServer)
          {
            pSqlConn.Open();
            pSqlCmd = new SqlCommand();
            pSqlCmd.Connection = pSqlConn;
          }
        }
        catch (Exception ex)
        {
          args.set("str_error", ex.Message);// Caller will handle displaying this.
          // Bug 17849
          Logger.cs_log_trace("Error in InsertBlobColumn", ex);
          return false;
        }
      }
      //---------------------------------------------------------------------

      //---------------------------------------------------------------------
      // Create Transaction
      if (!bUseExternalCommand)
      {
        try
        {
          if (iDBType == iCurrentDatabase_SqlServer)
          {
            pSqlTran = pSqlConn.BeginTransaction(IsolationLevel.ReadCommitted);
            pSqlCmd.Transaction = pSqlTran;
          }
        }
        catch (Exception ex)
        {
          args.set("str_error", ex.Message);// Caller will handle displaying this.
          // Bug 17849
          Logger.cs_log_trace("Error in InsertBLOBColumns", ex);

          // Cleanup
          if (iDBType == iCurrentDatabase_SqlServer)
            pSqlConn.Close();

          return false;
        }
      }
      //---------------------------------------------------------------------

      //---------------------------------------------------------------------
      // Post the data arrays
      try
      {
        string strParamPlaceholder = "";
        int iTopLevelDataCount = (int)args.getIntKeyLength();

        for (int j = 0; j < iTopLevelDataCount; j++)// Iterate all top level GEOs
        {
          GenericObject geoDATA = (GenericObject)args.get(j);
          GenericObject geoColumns = (GenericObject)geoDATA.get("columns");// Columns
          int iNbrParams = (int)geoDATA.getIntKeyLength();

          if (iDBType == iCurrentDatabase_SqlServer)
          {
            pSqlCmd.CommandText = (string)geoDATA.get("slq_statement");
            pSqlCmd.Parameters.Clear();
          }

          // Add all parameters for the above SQL statement.     
          for (int i = 0; i < iNbrParams; i++)
          {
            byte[] bData = (byte[])geoDATA.get(i);
            string strColumnName = (string)geoColumns.get(i);// Must be at the same position as parameter place holder.

            // TODO:  Genericize this function
            if (iDBType == iCurrentDatabase_SqlServer)
            {
              strParamPlaceholder = "@p" + i.ToString();
              SqlParameter param = new SqlParameter();
              param.ParameterName = strParamPlaceholder;
              param.SqlDbType = System.Data.SqlDbType.Image;
              param.Direction = ParameterDirection.Input;
              param.Value = bData;
              pSqlCmd.Parameters.Add(param);
            }

          }
          if (iDBType == iCurrentDatabase_SqlServer)
            pSqlCmd.ExecuteNonQuery();
        }
      }
      catch (Exception ex)
      {
        args.set("str_error", ex.Message);// Caller will handle displaying this.
        // Bug 17849
        Logger.cs_log_trace("Error in InsertBlobColumn", ex);
        if (!bUseExternalCommand)
        {
          if (iDBType == iCurrentDatabase_SqlServer)
            pSqlConn.Close();
        }
        return false;
      }
      //---------------------------------------------------------------------

      //---------------------------------------------------------------------
      // Commit to Database
      try
      {
        if (!bUseExternalCommand)
        {
          if (iDBType == iCurrentDatabase_SqlServer)
            pSqlTran.Commit();
        }
      }
      catch (Exception ex)
      {
        args.set("str_error", ex.Message);// If rollback fails just return this message at least.
        // Bug 17849
        Logger.cs_log_trace("Error in InsertBlobColumn", ex);
        if (!bUseExternalCommand)
        {
          if (iDBType == iCurrentDatabase_SqlServer)
          {
            pSqlTran.Rollback();// Return to previous state.
            pSqlConn.Close();// Cleanup
          }
        }

        return false;
      }
      //---------------------------------------------------------------------

      //---------------------------------------------------------------------
      // Close Database
      if (!bUseExternalCommand)
      {
        try
        {
          if (iDBType == iCurrentDatabase_SqlServer)
            pSqlConn.Close();
        }
        catch (Exception ex)
        {
          args.set("str_error", ex.Message);// Caller will handle displaying this.
          // Bug 17849
          Logger.cs_log_trace("Error in InsertBlobColumn", ex);
          return false;
        }
      }
      //---------------------------------------------------------------------

      return true;
    }

    /// <summary>
    /// Steps to activating a suspended event:
    /// 0. Get Source type for file
    /// 1. Check for a valid session
    /// 2. Check for a valid license
    /// 3. Update suspended event and create a new event
    ///    A. Establish a transaction
    ///    B. Double-check that Suspended Event (SE) has not been activated
    ///    C. Activate SE
    ///    D. Create New Event (NE) -- Non-transactionally.  Mark event cancelled or void if rolling back
    ///    E. Populate NE with SE core items
    ///    F. Commit transaction
    /// 4. Return new event parameters
    /// </summary>
    /// <param name="arg_pairs">
    ///   config = Config DB info
    ///   data = Data DB info
    ///   session_id = Session ID string
    /// </param>
    /// <returns></returns>

    //#region ActivateSuspendedEvent2
    public static GenericObject ActivateSuspendedEvent2(params Object[] arg_pairs)
    {
      //#region Initialization of arguments
      // TODO: remove geo args
      // GEO form of args
      GenericObject args = misc.convert_args(arg_pairs);

      //#region Config and Data DBs
      GenericObject config = args.get("config", c_CASL.c_undefined) as GenericObject;
      GenericObject data = args.get("data", c_CASL.c_undefined) as GenericObject;
      //#endregion
      //#region Timestamping
      DateTime today = DateTime.Now;
      string today_string = today.ToString("G", DateTimeFormatInfo.InvariantInfo);
      //#endregion
      //#region New PayEvent
      GenericObject new_PAYEVENT = args.get("new_PAYEVENT", c_CASL.c_undefined) as GenericObject;
      string new_PAYEVENT_EVENTNBR = "";// Unknown at this time.
      string new_PAYEVENT_FILENAME = (string)new_PAYEVENT.get("FILENAME");
      string new_PAYEVENT_USERID = (string)new_PAYEVENT.get("USERID", "auto");
      string new_PAYEVENT_LOGIN_ID = (string)new_PAYEVENT.get("LOGIN_ID", "");
      string new_PAYEVENT_file_DEPTID = (string)new_PAYEVENT.get("file_DEPTID", "auto");

      // character trimming using TrimStart is 84% faster than int.Parse->ToString
      string new_PAYEVENT_DEPFILENBR = TrimNumericString(GetFileNumber(new_PAYEVENT_FILENAME));
      string new_PAYEVENT_DEPFILESEQ = TrimNumericString(GetFileSequence(new_PAYEVENT_FILENAME));
      string new_source_group = "";
      string new_source_type = "";
      string new_source_date = "";
      string new_source_refid = "";
      //#endregion
      //#endregion
      //#region Begin Threadlock to Sync with Old Create Event
      GenericObject new_event = null;
      ThreadLock.GetInstance().CreateLock();
      // Bug 11661: Wrap mutex release in finally block
      try
      {
        //#endregion

        //#region Determine Source Type

        // Grab new file source and date information
        // TODO:  @varname is SQLServer specific -- unspecify it
        string sql_string = "SELECT * FROM TG_DEPFILE_DATA WHERE DEPFILENBR=@filenbr AND DEPFILESEQ=@fileseq";
        string error_message = "";
        string sub_error_message = "";
        bool voided = false;
        Hashtable sql_args = new Hashtable();
      // Bug #12055 Mike O - Use misc.Parse to log errors
      sql_args["filenbr"]=misc.Parse<Int32>(new_PAYEVENT_DEPFILENBR);
      sql_args["fileseq"]=misc.Parse<Int32>(new_PAYEVENT_DEPFILESEQ);
        IDBVectorReader reader = db_reader.Make(data, sql_string, CommandType.Text, 0,
          sql_args, 0, 0, null, ref error_message);
        if (error_message != "")
        {
          throw CASL_error.create("message", "Unable to connect to database. " + error_message, "code", "TS-1222", "status", "Unable to Connect to Database");
        }
        GenericObject results = new GenericObject();
        reader.ReadIntoRecordset(ref results, 0, 0);

        if (results.getLength() == 1)
        {
          GenericObject top_result = results.get(0) as GenericObject;
          new_source_group = (string)top_result.get("SOURCE_GROUP", "");
          new_source_type = (string)top_result.get("SOURCE_TYPE", "");
          new_source_refid = (string)top_result.get("SOURCE_REFID", "");
          DateTime dt_new_source_date = (DateTime)top_result.get("SOURCE_DATE", null);
          // Should probably be today's date?
          new_source_date = dt_new_source_date.ToString("MM/dd/yyyy HH:mm:ss", DateTimeFormatInfo.InvariantInfo);
        }
        else
        {
          throw CASL_error.create("message", "Error retrieving file source information", "code", "TS-1223");
        }
        //#endregion
        //#region Session ID testing
        //BUG 5395 Prevent multiple session user login BLL 5/2/08 (ActivateSuspendedEvent)
        // Can we change the source type to an enum or something to streamline this so we don't have to do a string comparison?
        //#region Create new PAYEVENT and check license
        GenericObject license = c_CASL.c_object("License.data") as GenericObject;
        if (!ValidateLicense(ref error_message, license))
        {
          return CreateErrorObject("TS-1067", "Invalid license.\n" + error_message, false, "Invalid License", true); //Bug 11329 NJ-always show verbose
        }
        //Ignore errors if we "successfully" validate because it's just a warning message
        error_message = "";
        new_event = CreateNewEvent(data, new_PAYEVENT_DEPFILENBR, new_PAYEVENT_DEPFILESEQ, license);
        new_PAYEVENT_EVENTNBR = (string)(new_event.get("EVENTNBR").ToString());
        //#region Update new event
        new_event.set("SOURCE_TYPE", new_source_type,
          "SOURCE_DATE", new_source_date,
          "SOURCE_GROUP", new_source_group,
          "SOURCE_REFID", new_source_refid,
          "USERID", new_PAYEVENT_USERID,
          "STATUS", "1",
          "MOD_DT", today_string);
        new_event.set("CHECKSUM", CreateEventChecksum(new_event));
        sql_args = new Hashtable();
        sql_string = CreateEventUpdateSQL(new_event, ref sql_args, ref error_message);
        if (error_message != "")
        {
          voided = VoidEvent(new_event, data, ref sub_error_message, new_PAYEVENT_USERID).Equals(true);
          if (!voided)
          {
            error_message += String.Format("Could not void new event {0}{1}-{2}. ",
              new_PAYEVENT_DEPFILENBR, new_PAYEVENT_DEPFILESEQ.PadLeft(3, '0'), new_PAYEVENT_EVENTNBR)
              + sub_error_message;
          }
          //Bug 11329 NJ-show verbose only when show_verbose_errors is true
          return CreateErrorObject("TS-1234", "Error creating SQL. " + error_message, false, "Error Creating SQL", false);
        }
        reader = db_reader.Make(data, sql_string, CommandType.Text, 0,
          sql_args, 0, 0, null, ref error_message);
        if (error_message != "")
        {
          voided = VoidEvent(new_event, data, ref sub_error_message, new_PAYEVENT_USERID).Equals(true);
          if (!voided)
          {
            error_message += String.Format("Could not void new event {0}{1}-{2}. ",
              new_PAYEVENT_DEPFILENBR, new_PAYEVENT_DEPFILESEQ.PadLeft(3, '0'), new_PAYEVENT_EVENTNBR)
              + sub_error_message;
          }
          //Bug 11329 NJ-show verbose only when show_verbose_errors is true
          return CreateErrorObject("TS-1222", "Unable to connect to database. " + error_message, false, "Unable to connect to database", false);
        }
        int row_count = reader.ExecuteNonQuery(ref error_message);
        if (error_message != "" || row_count != 1)
        {
          voided = VoidEvent(new_event, data, ref sub_error_message, new_PAYEVENT_USERID).Equals(true);
          if (!voided)
          {
            error_message += String.Format("Could not void new event {0}{1}-{2}. ",
              new_PAYEVENT_DEPFILENBR, new_PAYEVENT_DEPFILESEQ.PadLeft(3, '0'), new_PAYEVENT_EVENTNBR)
              + sub_error_message;
          }
          //Bug 11329 NJ-show verbose only when show_verbose_errors is true
          return CreateErrorObject("TS-1235", "Unable to update new event. " + error_message, false, "Unable to update new event", false);
        }
        //#endregion
        //#endregion
        //#region Activate Suspended Event
        sql_args = new Hashtable();

        GenericObject susp_PAYEVENT = args.get("suspended_PAYEVENT") as GenericObject;
        string susp_EVENTNBR = susp_PAYEVENT.get("EVENTNBR").ToString();
        string susp_FILENAME = susp_PAYEVENT.get("FILENAME").ToString();
        string susp_FILENBR = GetFileNumber(susp_FILENAME);
        string susp_FILESEQ = GetFileSequence(susp_FILENAME);

        sql_string = String.Format(@"UPDATE TG_SUSPEVENT_DATA
SET RESUME_EVENTNBR={0}, RESUME_FILENBR={1}, RESUME_FILESEQ={2}
WHERE EVENTNBR={3} AND DEPFILENBR={4} AND DEPFILESEQ={5} AND
      RESUME_EVENTNBR=0 AND RESUME_FILENBR=0 AND RESUME_FILESEQ=0",
          new_PAYEVENT_EVENTNBR, new_PAYEVENT_DEPFILENBR, new_PAYEVENT_DEPFILESEQ,
          susp_EVENTNBR, susp_FILENBR, susp_FILESEQ);

        reader = db_reader.Make(data, sql_string, CommandType.Text, 0,
          sql_args, 0, 0, null, ref error_message);
        if (error_message != "")
        {
          voided = VoidEvent(new_event, data, ref sub_error_message, new_PAYEVENT_USERID).Equals(true);
          if (!voided)
          {
            error_message += String.Format("Could not void new event {0}{1}-{2}. ",
              new_PAYEVENT_DEPFILENBR, new_PAYEVENT_DEPFILESEQ.PadLeft(3, '0'), new_PAYEVENT_EVENTNBR)
              + sub_error_message;
          }
          //Bug 11329 NJ-show verbose only when show_verbose_errors is true
          return CreateErrorObject("TS-1222", "Unable to connect to database. " + error_message, false, "Unable to connect to database", false);
        }
        row_count = 0;
        row_count = reader.ExecuteNonQuery(ref error_message);
        if (row_count == 0)
        {
          sql_string = String.Format(@"SELECT RESUME_EVENTNBR, RESUME_FILENBR, RESUME_FILESEQ FROM TG_SUSPEVENT_DATA
WHERE EVENTNBR={0} AND DEPFILENBR={1} AND DEPFILESEQ={2}",
            susp_EVENTNBR, susp_FILENBR, susp_FILESEQ);
          reader = db_reader.Make(data, sql_string, CommandType.Text, 0,
            sql_args, 0, 0, null, ref error_message);
          if (error_message != "")
          {
            voided = VoidEvent(new_event, data, ref sub_error_message, new_PAYEVENT_USERID).Equals(true);
            if (!voided)
            {
              error_message += String.Format("Could not void new event {0}{1}-{2}. ",
                new_PAYEVENT_DEPFILENBR, new_PAYEVENT_DEPFILESEQ.PadLeft(3, '0'), new_PAYEVENT_EVENTNBR)
                + sub_error_message;
            }
            //Bug 11329 NJ-show verbose only when show_verbose_errors is true
            return CreateErrorObject("TS-1222", "Unable to connect to database. " + error_message, false, "Unable to connect to database", false);
          }

          GenericObject resume_event_set = new GenericObject(), resume_event = null;
          reader.ReadIntoRecordset(ref resume_event_set, 0, 0);
          if (resume_event_set.getLength() != 1)
          {
            voided = VoidEvent(new_event, data, ref sub_error_message, new_PAYEVENT_USERID).Equals(true);
            if (!voided)
            {
              error_message += String.Format("Could not void new event {0}{1}-{2}. ",
                new_PAYEVENT_DEPFILENBR, new_PAYEVENT_DEPFILESEQ.PadLeft(3, '0'), new_PAYEVENT_EVENTNBR)
                + sub_error_message;
            }
            //Bug 11329 NJ-show verbose only when show_verbose_errors is true
            return CreateErrorObject("TS-1226", "Unable to determine resume event.", false, "Unable to determine resume event", false);
          }
          resume_event = resume_event_set.get(0) as GenericObject;
          error_message = String.Format("Suspended event already activated. {0}{1}-{2}",
            GetStringValue(resume_event, "RESUME_FILENBR"),
            GetStringValue(resume_event, "RESUME_FILESEQ").PadLeft(3, '0'),
            GetStringValue(resume_event, "RESUME_EVENTNBR"));
          voided = VoidEvent(new_event, data, ref sub_error_message, new_PAYEVENT_USERID).Equals(true);
          if (!voided)
          {
            error_message += String.Format("Could not void new event {0}{1}-{2}. ",
              new_PAYEVENT_DEPFILENBR, new_PAYEVENT_DEPFILESEQ.PadLeft(3, '0'), new_PAYEVENT_EVENTNBR)
              + sub_error_message;
          }
          //Bug 11329 NJ-show verbose only when show_verbose_errors is true
          return CreateErrorObject("TS-1225", error_message, false, "Suspended event already activated", false);
        }
        //#endregion
        //#region Populate Suspended Event
        //#region Get suspended transactions
        sql_string = string.Format("SELECT * FROM TG_SUSPTRAN_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2} order by DEPFILENBR, DEPFILESEQ, EVENTNBR, TRANNBR ",//BUG#10463 NJ",
          susp_FILENBR, susp_FILESEQ, susp_EVENTNBR);
        reader = db_reader.Make(data, sql_string, CommandType.Text, 0,
          sql_args, 0, 0, null, ref error_message);
        if (error_message != "")
        {
          voided = VoidEvent(new_event, data, ref sub_error_message, new_PAYEVENT_USERID).Equals(true);
          if (!voided)
          {
            error_message += String.Format("Could not void new event {0}{1}-{2}. ",
              new_PAYEVENT_DEPFILENBR, new_PAYEVENT_DEPFILESEQ.PadLeft(3, '0'), new_PAYEVENT_EVENTNBR)
              + sub_error_message;
          }
          //Bug 11329 NJ-show verbose only when show_verbose_errors is true
          return CreateErrorObject("TS-1222", "Unable to connect to database. " + error_message, false, "Unable to connect to database", false);
        }
        GenericObject suspended_transactions = new GenericObject();
        reader.ReadIntoRecordset(ref suspended_transactions, 0, 0);
        //#endregion
        ArrayList new_transactions = new ArrayList();
        int suspended_transaction_count = suspended_transactions.getLength();
        GenericObject susp_TRAN = null;
        GenericObject suspended_transaction = null;
        //#region Validate suspended transactions
        for (int i = 0; i < suspended_transaction_count; i++)
        {
          //#region Validate suspended transaction
          susp_TRAN = suspended_transactions.get(i) as GenericObject;
          //#region Get suspended transaction
          suspended_transaction = GetTransaction(susp_TRAN, data);
          //#endregion
          if (!ValidateTransactionChecksum(suspended_transaction))
          {
            error_message = String.Format("Transaction checksum invalid. Event: {0}{1}-{2}, Tran: {3}",
              susp_TRAN.get("DEPFILENBR").ToString(),
              (susp_TRAN.get("DEPFILESEQ").ToString()).PadLeft(3, '0'),
              susp_TRAN.get("EVENTNBR").ToString(),
              susp_TRAN.get("TRANNBR").ToString());
            throw CASL_error.create("code", "TS-944", "message", error_message);
          }
          //#endregion
          new_transactions.Add(suspended_transaction);
        }
        //#endregion
      
        //#region Post new transactions to event
        int new_TRANNBR = 0;
        GenericObject new_items = null;
        GenericObject new_custom_fields = null;
      //BUG#8198 12445 recall transaction group with void transaction in the old receipt BUG#10376
      string strTGREFID = "1";//_tg_source_ref_id
        foreach (GenericObject new_transaction in new_transactions)
        {
          new_TRANNBR++;
        //BUG#8198 12445
        string strITEMIND = new_transaction.get("ITEMIND", "") as string;
        if (strITEMIND == "T") strTGREFID = "" + new_TRANNBR;
          //#region Update item fields
          new_items = GetItems(new_transaction, data);
          //#region Remove non-copied fields
          int item_count = new_items.getLength();
          GenericObject new_item = null;
          for (int j = 0; j < item_count; j++)
          {

            // Bug#6504 To create the checksum for recall item with all the details ANU
            new_item = (GenericObject)new_items.get(j);
            new_item.set("TRANNBR", new_TRANNBR,
              "EVENTNBR", new_PAYEVENT_EVENTNBR,
              "DEPFILENBR", new_PAYEVENT_DEPFILENBR,
              "DEPFILESEQ", new_PAYEVENT_DEPFILESEQ);

            string strItemID = GetKeyData(new_item, "ITEMID", false);
            string strGLAcctNbr = GetKeyData(new_item, "GLACCTNBR", false);
            string strAcctID = GetKeyData(new_item, "ACCTID", false);
            string strAmount = GetKeyData(new_item, "AMOUNT", false);
            string strQty = GetKeyData(new_item, "QTY", false);
            string strTaxed = GetKeyData(new_item, "TAXED", false);
            string strTotal = GetKeyData(new_item, "TOTAL", false);
            string strItemNbr = GetKeyData(new_item, "ITEMNBR", false);

            //-----------------------------------------
            // Some validation of items.

            strTaxed.ToLower();
            if (strTaxed == "true" || strTaxed == "y")
              strTaxed = "Y";
            else
              strTaxed = "N";
            //-----------------------------------------

          // Bug #12055 Mike O - Use misc.Parse to log errors
            Decimal fConvert1 = misc.Parse<Decimal>(strAmount);// BUG19280 NEVER USE single-precision-float for currency
            string strTemp1 = string.Format("{0:f}", fConvert1);

            Decimal fConvertQty = misc.Parse<Decimal>(strQty);// BUG19280 NEVER USE single-precision-float for currency
            strQty = string.Format("{0:f}", fConvertQty);

            Decimal fConvert2 = misc.Parse<Decimal>(strTotal);// BUG19280 NEVER USE single-precision-float for currency
            string strTemp2 = string.Format("{0:f}", fConvert2);

            // ITEM DATA ENCRYPTION: 
            // ItemNbr + TranNbr + EventNbr + FileNbr + FileSeq + ItemID + GLAcctNbr + 
            // csAcctID + Amount + Qty + Taxed + Total

          string strChecksumData = misc.Parse<Int32>(strItemNbr).ToString();
            strChecksumData += (new_TRANNBR).ToString();
          strChecksumData += misc.Parse<Int32>(new_PAYEVENT_EVENTNBR).ToString();
          strChecksumData += misc.Parse<Int32>(new_PAYEVENT_DEPFILENBR).ToString();
          strChecksumData += misc.Parse<Int32>(new_PAYEVENT_DEPFILESEQ).ToString();
          strChecksumData += misc.Parse<Int32>(strItemID).ToString();
            strChecksumData += strGLAcctNbr;
            strChecksumData += strAcctID;
            strChecksumData += strTemp1;
            strChecksumData += strQty;
            strChecksumData += strTaxed;
            strChecksumData += strTemp2;

            // Bug #10955 Mike O
            new_item.set("CHECKSUM", Crypto.ComputeHash("", strChecksumData));
            //Bug#6504 end -ANU
          }
          //#endregion      
          //#region Add item data to the transaction
          if (item_count > 0)
          {
            new_transaction.set("table_TG_ITEM_DATA", new_items);
          }
          //#endregion


          //#endregion     

          //#region Update custom field fields
          new_custom_fields = GetCustomFields(new_transaction, data);
          //#region Add custom field data to the transaction
          int custom_field_count = new_custom_fields.getLength();
          GenericObject new_custom_field = null;
          GenericObject custom_keys_vector = new GenericObject();
          string custom_field_tag = null;
          for (int j = 0; j < custom_field_count; j++)
          {
            new_custom_field = (GenericObject)new_custom_fields.get(j);
            custom_field_tag = (string)new_custom_field.get("CUSTTAG");
            custom_keys_vector.insert(custom_field_tag);

            new_transaction.set(custom_field_tag, new_custom_field.get("CUSTVALUE", null));
          if (custom_field_tag == "_tg_source_ref_id") //BUG#8198 BUG#10376 BUG#12445
              new_transaction.set(custom_field_tag, strTGREFID);
            new_transaction.set(custom_field_tag + "_f_id", new_custom_field.get("CUSTID", null));
            new_transaction.set(custom_field_tag + "_f_label", new_custom_field.get("CUSTLABEL", null));
            new_transaction.set(custom_field_tag + "_f_attributes", new_custom_field.get("CUSTATTR", null));
          }
          //#endregion
          //#region Add custom keys list to the transaction
          if (custom_field_count > 0)
          {
            new_transaction.set("_custom_keys", custom_keys_vector);
          }
          //#endregion
          //#endregion
          //#region Update transaction fields
          new_transaction.set("TRANNBR", new_TRANNBR,
            "EVENTNBR", new_PAYEVENT_EVENTNBR,
            "DEPFILENBR", new_PAYEVENT_DEPFILENBR,
            "DEPFILESEQ", new_PAYEVENT_DEPFILESEQ,
            "SOURCE_TYPE", new_source_type,
            "POSTDT", today,
            "VOIDDT", null,
            "LOGIN_ID", new_PAYEVENT_LOGIN_ID);  //Bug 24445 NK  POSTUSERID in the tran table
          new_transaction.set("CHECKSUM", CreateTransactionChecksum(new_transaction));
          //#endregion   

          //BUG# 6218 - ANU added the code to write Image Data in the Database when we activate the suspended transaction. 
          //Bug 10055 SN
          susp_TRAN = suspended_transactions.get(new_TRANNBR - 1) as GenericObject;

          string susp_TRANNBR = susp_TRAN.get("TRANNBR").ToString();
          sql_string = string.Format("UPDATE TG_IMAGE_DATA SET EVENTNBR='{0}' , POSTNBR='{1}' WHERE DEPFILENBR='{2}' AND DEPFILESEQ='{3}' AND EVENTNBR='{4}' AND POSTNBR='{5}' ",
          new_PAYEVENT_EVENTNBR, new_TRANNBR, susp_FILENBR, susp_FILESEQ, susp_EVENTNBR, susp_TRANNBR);
          //end bug 10055    
          reader = db_reader.Make(data, sql_string, CommandType.Text, 0,
            sql_args, 0, 0, null, ref error_message);
          if (error_message != "")
          {
            voided = VoidEvent(new_event, data, ref sub_error_message, new_PAYEVENT_USERID).Equals(true);
            if (!voided)
            {
              error_message += String.Format("Could not update event {0}{1}-{2}. ",
                new_PAYEVENT_DEPFILENBR, new_PAYEVENT_DEPFILESEQ.PadLeft(3, '0'), new_PAYEVENT_EVENTNBR)
                + sub_error_message;
            }
            //Bug 11329 NJ-show verbose only when show_verbose_errors is true
            return CreateErrorObject("TS-1222", "Unable to connect to database. " + error_message, false, "Unable to connect to database", false);
          }
          reader.ExecuteNonQuery(ref error_message);
          if (error_message != "")
          {
            reader.CloseReader();
            //Bug 11329 NJ-show verbose only when show_verbose_errors is true
            //13832-Added inner exception
            GenericObject err = CreateErrorObject("TS-1253", "Unable to update image to database. ", false, "Not Updated", true);
            err.set("inner_error", misc.MakeCASLError("", "", error_message));
            return err;
            //end bug 13832
          }
          reader.CloseReader();
          //BUG# 6218 
        }
        //#region Post new transactions
        int rows_affected = 0;
        reader = db_reader.MakeTransactionReader(data, 0, 0, 0, ref error_message);

        //#region Build Transaction Commands
        foreach (GenericObject new_transaction in new_transactions)
        {
          AddPostTransactionCommands(ref reader, new_transaction, ref error_message);
        }
        //#endregion

        if (!reader.EstablishTransaction(ref error_message))
        {
          // Error handling added by Daniel for bug 6218
          //Bug 11329 NJ-show verbose only when show_verbose_errors is true
            //13832-Added inner exception
            GenericObject err = CreateErrorObject("TS-1222", "Unable to connect to database", false, "Unable to connect to database", true);
            err.set("inner_error", misc.MakeCASLError("", "", error_message));
            return err;
            //end bug 13832

        }

        reader.ExecuteTransactionCommands(ref error_message, ref rows_affected, false);
        if (!reader.CommitTransaction(ref error_message))
        {
          // Error handling added by Daniel for bug 6218
          //Bug 11329 NJ-show verbose only when show_verbose_errors is true
            //13832-Added inner exception
            GenericObject err = CreateErrorObject("TS-1222", "Unable to connect to database", false, "Unable to connect to database", true);
            err.set("inner_error", misc.MakeCASLError("", "", error_message));
            return err;
            //end bug 13832

        }

        //#endregion
        //#endregion
        //#endregion
        new_event.set("FILENAME", new_PAYEVENT_DEPFILENBR + new_PAYEVENT_DEPFILESEQ.PadLeft(3, '0'));

        // Error handling added by Daniel for bug 6218
        reader.CloseReader();

        //-------------------------------------------------------------------------------------------------------------------
        // BUG# 8661 DH - Bug 6218 missed deleting old records and created duplicates. With BAD checksums as well.
        // BUG# 6218 should have just updated the event number instead of re-creating a new record for each.
        /* Removing for Bug 10055 SN sql_string = string.Format("DELETE FROM TG_IMAGE_DATA WHERE DEPFILENBR={0} AND DEPFILESEQ={1} AND EVENTNBR={2}",
          susp_FILENBR, susp_FILESEQ, susp_EVENTNBR);

        string strErr = "";
        IDBVectorReader r = db_reader.Make(data, sql_string, CommandType.Text, 0, sql_args, 0, 0, null, ref error_message);
        r.ExecuteNonQuery(ref strErr);
        r.CloseReader();*/
        //------------------------------------------------------------------------------------------------------------------

      }
      finally
      {
        // Bug 11661
        ThreadLock.GetInstance().ReleaseLock();// BUG# 8256 DH
      }
      return new_event;
    }


    //#endregion


    /// <summary>
    /// Creates a new PAYEVENT entry in the TG_PAYEVENT_DATA table.
    /// Checks to make sure that the license allows for more than one event
    /// in a file if necessary. Throws a CASL error upon any problem.
    /// CreateNewDeposit can create un unfilled deposit so SQL statements
    /// may /// have to make sure that it doesn't try to grab any bad deposits
    /// </summary>
    /// <param name="args">
    ///   data = Data database information to construct a db connection from
    ///   filenbr = File number for the new event
    ///   fileseq = File sequence for the new event
    /// </param>
    /// <returns>a GEO describing the new event</returns>
    public static GenericObject CASLCreateNewEvent(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      GenericObject data = geo_args.get("data") as GenericObject;
      string filenbr = (string)geo_args.get("filenbr");
      string fileseq = (string)geo_args.get("fileseq");
      GenericObject license = geo_args.get("license") as GenericObject;

      return CreateNewEvent(data, filenbr, fileseq, license);
    }

    /// <summary>
    /// Creates a new DEPOSIT entry in the TG_DEPOSIT_DATA table.
    /// Does no error checking and merely returns a GEO representing
    /// the new deposit
    /// </summary>
    /// <param name="data">DB connection info</param>
    /// <param name="deposit_id">Deposit ID to make a new deposit for</param>
    /// <returns>a GEO describing the new deposit</returns>
    public static GenericObject CreateNewDeposit(GenericObject data, string deposit_id)
    {
      Hashtable sql_args = new Hashtable(1);
      sql_args["deposit_id"] = deposit_id;

      string error_message = "";

      IDBVectorReader reader = db_reader.Make(data, "sp_CreateDeposit", CommandType.StoredProcedure, 0,
        sql_args, 0, 0, null, ref error_message);
      GenericObject record_set = new GenericObject();
      if (error_message != "")
      {
        throw CASL_error.create("message", String.Format("Unable to create new deposit. Deposit: {0}. {1}", deposit_id, error_message),
          "code", "TS-1240");
      }

      reader.ReadIntoRecordset(ref record_set, 0, 0);
      if (record_set.getLength() == 0)
      {
        throw CASL_error.create("message", String.Format("Unable to create new deposit. Deposit: {0}. {1}", deposit_id, error_message),
          "code", "TS-1240");
      }
      else if (record_set.getLength() > 1)
      {
        throw CASL_error.create("message", String.Format("Unable to create new deposit. Deposit: {0}. {1}", deposit_id, error_message),
          "code", "TS-1240");
      }
      GenericObject return_record = record_set.get(0) as GenericObject;
      return return_record;
    }


    /// <summary>
    /// Creates a new PAYEVENT entry in the TG_PAYEVENT_DATA table.
    /// Checks to make sure that the license allows for more than one event
    /// in a file if necessary. Throws a CASL error upon any problem.
    /// CreateNewEvent can create un unfilled event so SQL statements may
    /// have to make sure that it doesn't try to grab any bad events
    /// </summary>
    /// <param name="data">DB connection info</param>
    /// <param name="filenbr">File number for new event</param>
    /// <param name="fileseq">File sequence for new event</param>
    /// <param name="license">License object to test for allowability of adding
    ///   more than one event</param>
    /// <returns>a GEO describing the new event</returns>
    public static GenericObject CreateNewEvent(GenericObject data, string filenbr, string fileseq, GenericObject license)
    {
      ConnectionTypeSynch pDBConn = null;  // Bug 15941. FT.   
      //------------------------------------------------------------------------------------------
      // BUG# 8256 DH - Remove the stored procedure (sp_CreateEvent) dependency and create the event here. 
      // The duplicates will be prevented by thread scritical section.
      ThreadLock.GetInstance().CreateLock();
      GenericObject geoNewEvent = null;
      // Bug 11661: Wrap mutex release in finally block
      try
      {

        geoNewEvent = new GenericObject();
        string strErr = "";
        string strEventNbr = "";
        string strUniqueID = "";
        string error_message = "";
        pDBConn = new ConnectionTypeSynch(data);
        if (!pDBConn.EstablishDatabaseConnection(ref strErr))
            HandleErrorsAndLocking("message", "Error opening database connection.", "code", "TS-788", strErr);//Bug 13832-Added detailed error message

        if (!pDBConn.EstablishRollback(ref strErr))
            HandleErrorsAndLocking("message", "Error creating database transaction.", "code", "TS-789", strErr);//Bug 13832-Added detailed error message

        //----------
        // Create new event nbr
        // Bug #8400 - Added CREATION_DT
        strEventNbr = CreateNewEventNbr(ref pDBConn, data, filenbr, fileseq);
        string strSQL = "INSERT INTO TG_PAYEVENT_DATA(EVENTNBR, DEPFILENBR, DEPFILESEQ, CREATION_DT) VALUES (";
        strSQL += strEventNbr + ", " + filenbr + ", " + fileseq + ", '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "')";

        strErr = "";
        if (!pDBConn.ExecuteSQLAndRollBackIfNeeded(strSQL, ref strErr))
            HandleErrorsAndLocking("message", "Error executing SQL.", "code", "TS-793", strErr);//Bug 13832-Added detailed error message

        if (!pDBConn.CommitToDatabase(ref strErr))
          HandleErrorsAndLocking("message", "Unable to commit changes to database.", "code", "TS-767", strErr);//Bug 13832-Added detailed error message
        //----------

        //----------
        // Now make the calling code happy and retrieve the UNIQUEID. This was a silly way of doing it.
        // But so not to change the ton of calling code I will do this instead.
        strSQL = string.Format("SELECT UNIQUEID FROM TG_PAYEVENT_DATA WHERE EVENTNBR=\'{0}\' AND DEPFILENBR=\'{1}\' AND DEPFILESEQ=\'{2}\'", strEventNbr, filenbr, fileseq);
        error_message = "";
        GenericObject geoTemp = (GenericObject)GetRecords(data, 0, strSQL, pDBConn.GetDBConnectionType());
        if (geoTemp != null)
          strUniqueID = geoTemp.get("UNIQUEID").ToString();
        //----------

        //----------
        // Now put it together. Caller depends on this geo.
        geoNewEvent.set("EVENTNBR", strEventNbr);
        geoNewEvent.set("DEPFILENBR", filenbr);
        geoNewEvent.set("DEPFILESEQ", fileseq);
        geoNewEvent.set("UNIQUEID", strUniqueID);

        //----------
        // Check the license before we finish
        if ((bool)license.get("one_event_per_file", false) && geoNewEvent.getLength() == 1
          && ((int)geoNewEvent.get("EVENTNBR") != 1))
        {
          error_message = String.Format("The License for this site is for Summary use only.  You may not add another transaction. File: {0}-{1}", filenbr, fileseq);
          throw CASL_error.create("message", error_message, "code", "TS-942");
        }
        //----------

      }
      finally
      {
        // Bug 15941. FT. Make sure the database connection is closed
        CloseDBConnection(pDBConn);
        // Bug 11661
        ThreadLock.GetInstance().ReleaseLock();
      }
      return geoNewEvent;
      //------------------------------------------------------------------------------------------
      /*   BUG# 8256 DH - DISABLED THIS CODE - LOOK ABOVE
      
      Hashtable sql_args=new Hashtable();
      sql_args["filenbr"]=filenbr;
      sql_args["fileseq"]=fileseq;

      IDBVectorReader reader=db_reader.Make(data, "sp_CreateEvent", CommandType.StoredProcedure, 0,
        sql_args, 0, 0, null, ref error_message);
      GenericObject record_set=new GenericObject();
      if(error_message!="")
      {
        throw CASL_error.create("message",String.Format("Unable to create new core event. File: {0}-{1}. {2}", filenbr, fileseq, error_message),
          "code","TS-1221");
      }

      reader.ReadIntoRecordset(ref record_set, 0, 0);
      if(record_set.getLength()==0)
      {
        throw CASL_error.create("message",String.Format("Unable to create new core event. File: {0}-{1}. {2}", filenbr, fileseq, error_message),
          "code","TS-1221");
      } 
      else if(record_set.getLength()>1)
      {
        throw CASL_error.create("message",String.Format("Error creating new core event. File: {0}-{1}. Object returned: {2}. {3}", filenbr, fileseq, record_set.to_xml(), error_message),
          "code","TS-1221");
      }
      GenericObject return_record=record_set.get(0) as GenericObject;

      if((bool)license.get("one_event_per_file", false) && record_set.getLength()==1
        && ((int)return_record.get("EVENTNBR") != 1))
      {
        error_message=String.Format("The License for this site is for Summary use only.  You may not add another transaction. File: {0}-{1}", filenbr, fileseq);
        throw CASL_error.create("message", error_message, "code", "TS-942");
      }

      return return_record;
     */
    }
    public static int GetDBConnectionTypeFromConnectionInfo(GenericObject connection_info)
    {
      string strConnectionClass = (connection_info.get("_parent") as GenericObject).get("_name", "", true) as string;

      switch (strConnectionClass)
      {
        case "Sql_server":
          return ConnectionTypeSynch.m_iCurrentDatabase_SqlServer;
        case "Oracle_tns":
          return ConnectionTypeSynch.m_iCurrentDatabase_Oracle_TNS;
        case "Oracle":
          return ConnectionTypeSynch.m_iCurrentDatabase_Oracle_ODBC;
        case "Access":
          return ConnectionTypeSynch.m_iCurrentDatabase_MsAccess_ODBC;
        default:
          return -1; // Undefined
      }
    }

    public static int GetFailedAttemptCount(GenericObject user, GenericObject database)
    {
      string user_path = (string)misc.ToPath(user, c_CASL.GEO);
      string error_message = "";
      Hashtable sql_args = new Hashtable();
      sql_args["container"] = user_path;
      string sql_string = "SELECT field_value AS fail_count FROM CBT_FLEX WHERE container=@container AND field_key='\"count_of_failed_attempts\"'";
      IDBVectorReader reader = db_reader.Make(database, sql_string, CommandType.Text, 0, sql_args, 0, 0, null, ref error_message);
      if (error_message != "")
      {
        //Throw error here
        return -1;
      }
      GenericObject results = new GenericObject();
      reader.ReadIntoRecordset(ref results, 0, 0);

      if (results.getLength() == 1)
      {
        // Bug #12055 Mike O - Use misc.Parse to log errors
        return misc.Parse<Int32>((string) (results.get(0) as GenericObject).get("fail_count"));
      }
      else if (results.getLength() == 0)
      {
        // Doesn't exist yet
        return 0;
      }
      else
      {
        //Throw error here
        return -1;
      }
    }

    public static void IncFailedAttemptCount(GenericObject user, GenericObject database)
    {
      string user_path = (string)misc.ToPath(user, c_CASL.GEO);
      string error_message = "";
      Hashtable sql_args = new Hashtable();
      sql_args["container"] = user_path;
      sql_args["field_key"] = "\"count_of_failed_attempts\"";
      string where_clause = "container=@container AND field_key=@field_key";
      string sql_string = "UPDATE CBT_FLEX SET field_value=(SELECT (field_value + 1) AS fail_count FROM CBT_FLEX WHERE " +
        where_clause + ") WHERE " + where_clause;

      IDBVectorReader reader = db_reader.Make(database, sql_string, CommandType.Text, 0, sql_args, 0, 0, null, ref error_message);
      if (error_message != "")
      {
        //Throw error here
      }

      reader.ExecuteNonQuery(ref error_message);
      if (error_message != "")
      {
        //Throw error here
      }
    }

    public static void ResetFailedAttemptCount(GenericObject user, GenericObject database)
    {
      string user_path = (string)misc.ToPath(user, c_CASL.GEO);
      string error_message = "";
      Hashtable sql_args = new Hashtable();
      sql_args["container"] = user_path;
      sql_args["field_key"] = "\"count_of_failed_attempts\"";
      string sql_string = "UPDATE CBT_FLEX SET field_value=0 WHERE container=@container AND field_key=@field_key";

      IDBVectorReader reader = db_reader.Make(database, sql_string, CommandType.Text, 0, sql_args, 0, 0, null, ref error_message);
      if (error_message != "")
      {
        //Throw error here
      }

      reader.ExecuteNonQuery(ref error_message);
      if (error_message != "")
      {
        //Throw error here
      }
    }

    // Bug #11869 Mike O - Deleted TestMethod, as it wasn't anything but confusing

    // Bug 18932 UMN parameterize queries
    // Note: is only called by baseline\reporting\code_casl\reports_financial_acct.casl
    public static object CASLGetItemData(params object[] argPairs)
    {
      GenericObject args = misc.convert_args(argPairs);

      GenericObject subject = args.get("_subject") as GenericObject;
      string whereClause = args.get("where_clause", null) as string;
      if (whereClause == null)
      {
        return misc.MakeCASLError("Get Item Data Error", "TS-1250", "Invalid where clause");
      }
      string itemWhereClause = args.get("item_where_clause", "") as string;
      string custFieldWhereClause = args.get("cust_field_where_clause", "") as string;

      // Bug 18932 UMN parameterize queries
      // Bug 23662: sql_args is actually a geo not a Hashtable so convert gently
      GenericObject sql_args = args.get("sql_args", new GenericObject()) as GenericObject;

      // Bug 18932 UMN remove _parent to prevent "Unhandled object type in sql_args" in db_vector_reader_sql
      if (sql_args != null)  // Bug 23662: Should not happen, but....
        sql_args.Remove("_parent");     

      Hashtable sql_hash = sql_args.getHashtable();

      return GetItemData(subject, whereClause, itemWhereClause, custFieldWhereClause, sql_hash);
    }
    
    // Bug 18932 UMN parameterize queries
    public static object GetItemData(GenericObject subject, string whereClause, 
        string itemWhereClause, string custFieldWhereClause, Hashtable sql_args)
    {
      GenericObject items = null;
      ThreadLock.GetInstance().CreateLock();
      // Bug 11661: Wrap mutex release in finally block
      try
      {
        GenericObject data = c_CASL.c_object("TranSuite_DB.Data.db_connection_info") as GenericObject;
        items = c_CASL.c_GEO();
        GenericObject indexedItems = c_CASL.c_GEO();

        //#region Items
        string queryString = null;
        StringBuilder queryBuilder = new StringBuilder(5);
        // Bug #9072 Mike O - Added m.ITEMDESC
        //bug Bug 13971 MPT - Add d.DEPTID for Workgroup ID to Reports
        queryBuilder.Append("SELECT m.DEPFILENBR, m.DEPFILESEQ, m.EVENTNBR, m.TRANNBR, m.ITEMNBR, m.ITEMDESC, m.ACCTID, m.GLACCTNBR, m.TOTAL, d.DEPTID FROM TG_ITEM_DATA m, TG_TRAN_DATA t,TG_DEPFILE_DATA d WHERE m.DEPFILENBR=t.DEPFILENBR AND m.DEPFILESEQ=t.DEPFILESEQ AND m.EVENTNBR=t.EVENTNBR AND m.TRANNBR=t.TRANNBR  AND  m.DEPFILENBR=d.DEPFILENBR AND m.DEPFILESEQ=d.DEPFILESEQ AND t.VOIDDT IS NULL AND NOT(t.ITEMIND=@itemind) AND ");//BUG#12904 SX no report on outside transaction

        // Bug 18932 UMN parameterize queries
        sql_args["itemind"] = "T";
        
        queryBuilder.Append(whereClause);
        if (!itemWhereClause.Equals(""))
        {
          queryBuilder.Append(" AND ");
          queryBuilder.Append(itemWhereClause);
        }
        queryBuilder.Append(" ORDER BY m.DEPFILENBR DESC, m.DEPFILESEQ DESC, m.EVENTNBR ASC, m.TRANNBR ASC, m.ITEMNBR ASC, ACCTID ASC");
        queryString = queryBuilder.ToString();

        string error_message = "";
        
        // Bug 18932 UMN parameterize queries
        IDBVectorReader reader = db_reader.Make(data, queryString, CommandType.Text, 0, sql_args, 0, 0, null, ref error_message);
        if (!error_message.Equals(""))
        {
          //Bug 13832-use this for splitting the exception msg into friendly and verbose
         GenericObject err = CreateErrorObject("TS-1249", "Get Item Data Error", false, "Could not create DB reader: ", true);
          err.set("inner_error", misc.MakeCASLError("", "", error_message));
          return err;
            //end bug 13832
         
        }
        GenericObject itemData = new GenericObject();
        reader.ReadIntoRecordset(ref itemData, 0, 0);

        GenericObject record;
        for (int i = 0; i < itemData.getLength(); i++)
        {
          record = itemData.get(i) as GenericObject;
          string index = String.Format("{0}-{1}-{2}-{3}",
            record.get("DEPFILENBR"), record.get("DEPFILESEQ"), record.get("EVENTNBR"), record.get("TRANNBR"));
          record.set("_parent", subject.get("item_class", false, true));
          items.insert(record);
          if (!indexedItems.has(index))
          {
            indexedItems.set(index, c_CASL.c_GEO());
          }
          (indexedItems.get(index) as GenericObject).insert(record);
        }
        //#endregion
        //#region Transactions
        // Bug #9072 Mike O - Removed TTDESC
        queryString = "SELECT SOURCE_GROUP, TTID, TRANNBR, EVENTNBR, DEPFILENBR, DEPFILESEQ, POSTDT, VOIDDT FROM TG_TRAN_DATA m WHERE " +
          whereClause + " ORDER BY DEPFILENBR DESC, DEPFILESEQ DESC, EVENTNBR ASC";

        // Bug 18932 UMN parameterize queries
        reader = db_reader.Make(data, queryString, CommandType.Text, 0, sql_args, 0, 0, null, ref error_message);
        if (!error_message.Equals(""))
        {
          //Bug 13832-use this for splitting the exception msg into friendly and verbose
          GenericObject err = CreateErrorObject("TS-1249", "Get Item Data Error", false, "Could not create DB reader: ", true);
          err.set("inner_error", misc.MakeCASLError("", "", error_message));
          return err;
            //end bug 13832

        }
        GenericObject tranData = new GenericObject();
        reader.ReadIntoRecordset(ref tranData, 0, 0);
        GenericObject indexedItem = null, tranValue = null;
        for (int i = 0; i < tranData.getLength(); i++)
        {
          tranValue = tranData.get(i) as GenericObject;
          string index = String.Format("{0}-{1}-{2}-{3}",
            tranValue.get("DEPFILENBR"), tranValue.get("DEPFILESEQ"), tranValue.get("EVENTNBR"), tranValue.get("TRANNBR"));

          indexedItem = indexedItems.get(index, null) as GenericObject;
          if (indexedItem != null)
          {
            for (int j = 0; j < indexedItem.getLength(); j++)
            {
              record = indexedItem.get(j) as GenericObject;
              record.set(
                // Bug #9072 Mike O - Removed TTDESC insertion
                "POSTDT", tranValue.get("POSTDT", ""),
                "VOIDDT", tranValue.get("VOIDDT", ""),
                "SOURCE_GROUP", tranValue.get("SOURCE_GROUP", "")
                );
            }
          }
        }
        //#endregion
        //#region Custom Fields

        object customFields = (subject.get("report_criteria") as GenericObject).get("custom_fields", null);

        if (customFields != null)
        {
          queryString = "SELECT m.DEPFILENBR, m.DEPFILESEQ, m.EVENTNBR, m.TRANNBR, m.CUSTLABEL, m.CUSTTAG, m.CUSTVALUE FROM GR_CUST_FIELD_DATA m WHERE " +
           whereClause;
          if (!custFieldWhereClause.Equals(""))
          {
            queryString += " AND " + custFieldWhereClause;
          }

          // Bug 18932 UMN parameterize queries
          reader = db_reader.Make(data, queryString, CommandType.Text, 0, sql_args, 0, 0, null, ref error_message);
          if (!error_message.Equals(""))
          {            
            //Bug 13832-use this for splitting the exception msg into friendly and verbose
            GenericObject err = CreateErrorObject("TS-1249", "Get Item Data Error", false, "Could not create DB reader: ", true);
            err.set("inner_error", misc.MakeCASLError("", "", error_message));
            return err;
              //end bug 13832
          }
          GenericObject custFieldData = new GenericObject();
          reader.ReadIntoRecordset(ref custFieldData, 0, 0);

          for (int i = 0; i < custFieldData.getLength(); i++)
          {
            record = custFieldData.get(i) as GenericObject;
            string index = String.Format("{0}-{1}-{2}-{3}",
              record.get("DEPFILENBR"), record.get("DEPFILESEQ"), record.get("EVENTNBR"), record.get("TRANNBR"));
            string tag = record.get("CUSTTAG") as string;
            string value = record.get("CUSTVALUE", "") as string;
            string label = record.get("CUSTLABEL", tag) as string;

            indexedItem = indexedItems.get(index, null) as GenericObject;
            if (indexedItem != null)
            {
              for (int j = 0; j < indexedItem.getLength(); j++)
              {
                record = indexedItem.get(j) as GenericObject;
                record.set(tag, value, misc.field_key(tag, "label"), label);
              }
            }
          }
        }
        //#endregion


        for (int i = 0; i < items.getLength(); i++)
        {
          record = items.get(i) as GenericObject;
          record.set("file_nbr", String.Format("{0}{1:000}", record.get("DEPFILENBR"), record.get("DEPFILESEQ")));
        }

      }
      finally
      {
        // Bug 11661
        ThreadLock.GetInstance().ReleaseLock();
      }

      return items;
    }

    //Bug 5190 NJ
    //Wrapper for AddTenderCustomFields
    public static Object AddTenderCustomFieldWrapper(params Object[] arg_pairs)
    {
      bool returnValue = false;
      ThreadLock.GetInstance().CreateLock();
      // Bug 11661: Wrap the method in a finally block so mutexes are always released
      try
      {
        returnValue = AddTenderCustomFields(arg_pairs);
      }
      finally
      {
        ThreadLock.GetInstance().ReleaseLock();
      }
      return returnValue;

    }

    //End bug 5190

      //Bug 13136 NJ- Checks if a tender is a refund due to a void failure
        public static bool CheckTenderIsVoidRefund(GenericObject currentTender, GenericObject tenders )
        {

            int currentTndrid,tndrid;
            int.TryParse(GetStringValue(currentTender, "TNDRID"),out currentTndrid);
            decimal currentAmount,amount;
            decimal.TryParse(GetStringValue(currentTender, "AMOUNT"),out currentAmount);
            int tenderCount = tenders.getLength();
            for (int i = 0; i < tenderCount; i++)
            {
                GenericObject tender =(GenericObject)tenders.get(i);
                int.TryParse(GetStringValue(tender, "TNDRID"), out tndrid);
                decimal.TryParse(GetStringValue(tender, "AMOUNT"), out amount);
                if (currentTndrid == tndrid && currentAmount == (0M - amount))
                {
                    return true;
                }
            }

            return false;         

            
        }
      //end bug 13136

    // Bug #8856 Mike O - Update DB to use a different checksum algorithm.
    //                    Pre-update validation is not possible, so make sure everything's correct first.
    // Bug #9941 Mike O - Commented out
    /*public static bool UpdateAllChecksums()
    {
      GenericObject database = c_CASL.c_object("TranSuite_DB.Data.db_connection_info") as GenericObject;

      //#region Deposits - TG_DEPOSIT_DATA
      string sql_text = "SELECT DEPOSITID, DEPOSITNBR FROM TG_DEPOSIT_DATA;";
      string error_message = "";
      IDBVectorReader reader = db_reader.Make(database, sql_text, CommandType.Text, 0, null, 0, 0, null, ref error_message);
      if (error_message != "")
      {
        reader.CloseReader();
        return false;
	}

      GenericObject results = new GenericObject();
      reader.ReadIntoRecordset(ref results, 0, 0);

      for (int i = 0; i < results.vectors.Count; i++)
      {
        GenericObject deposit = (GenericObject)results.vectors[i];
        if (!UpdateDepositChecksum("strDepositID", deposit.get("DEPOSITID"), "strDepositNbr", deposit.get("DEPOSITNBR"), "config", database) ||
           !ValidateDepositChecksum("strDepositID", deposit.get("DEPOSITID"), "strDepositNbr", deposit.get("DEPOSITNBR"), "config", database)
          )
        {
          reader.CloseReader();
          return false;
}
      }
      reader.CloseReader();
      //#endregion
      //#region Transaction Custom Fields - GR_CUST_FIELD_DATA
      sql_text = "SELECT DEPFILENBR, DEPFILESEQ, EVENTNBR, TRANNBR FROM GR_CUST_FIELD_DATA;";
      error_message = "";
      reader = db_reader.Make(database, sql_text, CommandType.Text, 0, null, 0, 0, null, ref error_message);
      if (error_message != "")
      {
        reader.CloseReader();
        return false;
      }

      results = new GenericObject();
      reader.ReadIntoRecordset(ref results, 0, 0);

      for (int i = 0; i < results.vectors.Count; i++)
      {
        GenericObject custField = (GenericObject)results.vectors[i];
        if (!UpdateGRCustFieldChecksum("strFileName", custField.get("DEPFILENBR".ToString()) + custField.get("DEPFILESEQ").ToString(),
          "strEventNbr", custField.get("EVENTNBR"), "strTranNbr", custField.get("TRANNBR"), "config", database) ||
          !ValidateGRCustFieldChecksum("strFileName", custField.get("DEPFILENBR".ToString()) + custField.get("DEPFILESEQ").ToString(),
          "strEventNbr", custField.get("EVENTNBR"), "strTranNbr", custField.get("TRANNBR"), "config", database)
          )
        {
          reader.CloseReader();
          return false;
        }
      }
      reader.CloseReader();
      //#endregion
      //#region Tender Custom Fields - CUST_FIELD_DATA
      sql_text = "SELECT DEPFILENBR, DEPFILESEQ, EVENTNBR, POSTNBR FROM CUST_FIELD_DATA;";
      error_message = "";
      reader = db_reader.Make(database, sql_text, CommandType.Text, 0, null, 0, 0, null, ref error_message);
      if (error_message != "")
      {
        reader.CloseReader();
        return false;
      }

      results = new GenericObject();
      reader.ReadIntoRecordset(ref results, 0, 0);

      for (int i = 0; i < results.vectors.Count; i++)
      {
        GenericObject custField = (GenericObject)results.vectors[i];
        if (!UpdateCustFieldChecksum("strFileName", custField.get("DEPFILENBR".ToString()) + custField.get("DEPFILESEQ").ToString(),
          "strEventNbr", custField.get("EVENTNBR"), "strPostNbr", custField.get("POSTNBR"), "config", database) ||
          !ValidateCustFieldChecksum("strFileName", custField.get("DEPFILENBR".ToString()) + custField.get("DEPFILESEQ").ToString(),
          "strEventNbr", custField.get("EVENTNBR"), "strPostNbr", custField.get("POSTNBR"), "config", database)
          )
        {
          reader.CloseReader();
          return false;
}
      }
      reader.CloseReader();
      //#endregion
      //#region Payevents - TG_PAYEVENT_DATA
      sql_text = "SELECT DEPFILENBR, DEPFILESEQ, EVENTNBR FROM TG_PAYEVENT_DATA;";
      error_message = "";
      reader = db_reader.Make(database, sql_text, CommandType.Text, 0, null, 0, 0, null, ref error_message);
      if (error_message != "")
      {
        reader.CloseReader();
        return false;
  }

      results = new GenericObject();
      reader.ReadIntoRecordset(ref results, 0, 0);

      for (int i = 0; i < results.vectors.Count; i++)
      {
        GenericObject payevent = (GenericObject)results.vectors[i];
        if (!UpdatePayEventChecksum("strFileName", payevent.get("DEPFILENBR").ToString() + payevent.get("DEPFILESEQ").ToString(),
          "strEventNbr", payevent.get("EVENTNBR"), "config", database) ||
          !ValidatePayEventChecksum("strFileName", payevent.get("DEPFILENBR").ToString() + payevent.get("DEPFILESEQ").ToString(),
          "strEventNbr", payevent.get("EVENTNBR"), "config", database)
          )
        {
          reader.CloseReader();
          return false;
}
      }
      reader.CloseReader();
      //#endregion
      //#region Items - TG_ITEM_DATA
      sql_text = "SELECT DEPFILENBR, DEPFILESEQ, EVENTNBR, TRANNBR FROM TG_ITEM_DATA GROUP BY DEPFILENBR, DEPFILESEQ, EVENTNBR, TRANNBR;";
      error_message = "";
      reader = db_reader.Make(database, sql_text, CommandType.Text, 0, null, 0, 0, null, ref error_message);
      if (error_message != "")
      {
        reader.CloseReader();
        return false;
      }

      results = new GenericObject();
      reader.ReadIntoRecordset(ref results, 0, 0);

      for (int i = 0; i < results.vectors.Count; i++)
      {
        GenericObject item = (GenericObject)results.vectors[i];
        if (!UpdateItemDataChecksum("strFileName", item.get("DEPFILENBR").ToString() + item.get("DEPFILESEQ").ToString(),
          "strEventNbr", item.get("EVENTNBR"), "strTranNbr", item.get("TRANNBR"), "config", database) ||
          !ValidateItemDataChecksum("strFileName", item.get("DEPFILENBR").ToString() + item.get("DEPFILESEQ").ToString(),
          "strEventNbr", item.get("EVENTNBR"), "strTranNbr", item.get("TRANNBR"), "config", database)
          )
        {
          reader.CloseReader();
          return false;
        }
      }
      reader.CloseReader();
      //#endregion
      //#region Tenders - TG_TENDER_DATA
      sql_text = "SELECT DEPFILENBR, DEPFILESEQ, EVENTNBR, TNDRNBR FROM TG_TENDER_DATA;";
      error_message = "";
      reader = db_reader.Make(database, sql_text, CommandType.Text, 0, null, 0, 0, null, ref error_message);
      if (error_message != "")
      {
        reader.CloseReader();
        return false;
      }

      results = new GenericObject();
      reader.ReadIntoRecordset(ref results, 0, 0);

      for (int i = 0; i < results.vectors.Count; i++)
      {
        GenericObject tender = (GenericObject)results.vectors[i];
        if (!UpdateTenderChecksum("strFileName", tender.get("DEPFILENBR").ToString() + tender.get("DEPFILESEQ").ToString(),
          "strEventNbr", tender.get("EVENTNBR"), "strTndrNbr", tender.get("TNDRNBR"), "config", database) ||
          !ValidateTenderChecksum("strFileName", tender.get("DEPFILENBR").ToString() + tender.get("DEPFILESEQ").ToString(),
          "strEventNbr", tender.get("EVENTNBR"), "strTndrNbr", tender.get("TNDRNBR"), "config", database)
          )
        {
          reader.CloseReader();
          return false;
        }
      }
      reader.CloseReader();
      //#endregion
      //#region Files - TG_DEPFILE_DATA
      sql_text = "SELECT DEPFILENBR, DEPFILESEQ FROM TG_DEPFILE_DATA;";
      error_message = "";
      reader = db_reader.Make(database, sql_text, CommandType.Text, 0, null, 0, 0, null, ref error_message);
      if (error_message != "")
      {
        reader.CloseReader();
        return false;
      }

      results = new GenericObject();
      reader.ReadIntoRecordset(ref results, 0, 0);

      for (int i = 0; i < results.vectors.Count; i++)
      {
        GenericObject file = (GenericObject)results.vectors[i];
        if (!UpdateTranFileChecksum("strFileName", file.get("DEPFILENBR").ToString() + file.get("DEPFILESEQ").ToString(),
          "data", database) ||
          !ValidateTranFileChecksum("strFileName", file.get("DEPFILENBR").ToString() + file.get("DEPFILESEQ").ToString(),
          "config", database)
          )
        {
          reader.CloseReader();
          return false;
        }
      }
      reader.CloseReader();
      //#endregion
      //#region Transactions TG_TRAN_DATA
      sql_text = "SELECT DEPFILENBR, DEPFILESEQ, EVENTNBR, TRANNBR FROM TG_TRAN_DATA;";
      error_message = "";
      reader = db_reader.Make(database, sql_text, CommandType.Text, 0, null, 0, 0, null, ref error_message);
      if (error_message != "")
      {
        reader.CloseReader();
        return false;
      }

      results = new GenericObject();
      reader.ReadIntoRecordset(ref results, 0, 0);

      for (int i = 0; i < results.vectors.Count; i++)
      {
        GenericObject tran = (GenericObject)results.vectors[i];
        if (!UpdateTranChecksum("strFileName", tran.get("DEPFILENBR").ToString() + tran.get("DEPFILESEQ").ToString(),
          "strEventNbr", tran.get("EVENTNBR"), "strTranNbr", tran.get("TRANNBR"), "config", database) ||
          !ValidateTranChecksum("strFileName", tran.get("DEPFILENBR").ToString() + tran.get("DEPFILESEQ").ToString(),
          "strEventNbr", tran.get("EVENTNBR"), "strTranNbr", tran.get("TRANNBR"), "config", database)
          )
        {
          reader.CloseReader();
          return false;
        }
      }
      reader.CloseReader();
      //#endregion
      //#region Suspended Transactions - TG_SUSPTRAN_DATA
      sql_text = "SELECT DEPFILENBR, DEPFILESEQ, EVENTNBR, TRANNBR FROM TG_SUSPTRAN_DATA;";
      error_message = "";
      reader = db_reader.Make(database, sql_text, CommandType.Text, 0, null, 0, 0, null, ref error_message);
      if (error_message != "")
      {
        reader.CloseReader();
        return false;
      }

      results = new GenericObject();
      reader.ReadIntoRecordset(ref results, 0, 0);

      for (int i = 0; i < results.vectors.Count; i++)
      {
        GenericObject tran = (GenericObject)results.vectors[i];
        if (!UpdateSuspTranChecksum("strFileName", tran.get("DEPFILENBR").ToString() + tran.get("DEPFILESEQ").ToString(),
          "strEventNbr", tran.get("EVENTNBR"), "strTranNbr", tran.get("TRANNBR"), "data", database) ||
          !ValidateSuspTranChecksum("strFileName", tran.get("DEPFILENBR").ToString() + tran.get("DEPFILESEQ").ToString(),
          "strEventNbr", tran.get("EVENTNBR"), "strTranNbr", tran.get("TRANNBR"), "config", database)
          )
        {
          reader.CloseReader();
          return false;
        }
      }
      reader.CloseReader();
      //#endregion
      //#region Suspended Events - TG_SUSPEVENT_DATA
      sql_text = "SELECT DEPFILENBR, DEPFILESEQ, EVENTNBR FROM TG_SUSPEVENT_DATA;";
      error_message = "";
      reader = db_reader.Make(database, sql_text, CommandType.Text, 0, null, 0, 0, null, ref error_message);
      if (error_message != "")
      {
        reader.CloseReader();
        return false;
      }

      results = new GenericObject();
      reader.ReadIntoRecordset(ref results, 0, 0);

      for (int i = 0; i < results.vectors.Count; i++)
      {
        GenericObject tran = (GenericObject)results.vectors[i];
        if (!UpdateSuspEventChecksum("strFileName", tran.get("DEPFILENBR").ToString() + tran.get("DEPFILESEQ").ToString(),
          "strEventNbr", tran.get("EVENTNBR"), "data", database) ||
          !ValidateSuspEventChecksum("strFileName", tran.get("DEPFILENBR").ToString() + tran.get("DEPFILESEQ").ToString(),
          "strEventNbr", tran.get("EVENTNBR"), "config", database)
          )
        {
          reader.CloseReader();
          return false;
        }
      }
      reader.CloseReader();
      //#endregion
      //#region Reversal Events - TG_REVERSAL_EVENT_DATA
      sql_text = "SELECT DEPFILENBR, DEPFILESEQ, EVENTNBR FROM TG_REVERSAL_EVENT_DATA;";
      error_message = "";
      reader = db_reader.Make(database, sql_text, CommandType.Text, 0, null, 0, 0, null, ref error_message);
      if (error_message != "")
      {
        reader.CloseReader();
        return false;
      }

      results = new GenericObject();
      reader.ReadIntoRecordset(ref results, 0, 0);

      for (int i = 0; i < results.vectors.Count; i++)
      {
        GenericObject tran = (GenericObject)results.vectors[i];
        if (!UpdateReversalEventChecksum("strFileName", tran.get("DEPFILENBR").ToString() + tran.get("DEPFILESEQ").ToString(),
          "strEventNbr", tran.get("EVENTNBR"), "data", database) ||
          !ValidateReversalEventChecksum("strFileName", tran.get("DEPFILENBR").ToString() + tran.get("DEPFILESEQ").ToString(),
          "strEventNbr", tran.get("EVENTNBR"), "config", database)
          )
        {
          reader.CloseReader();
          return false;
        }
      }
      reader.CloseReader();
      //#endregion
      //#region Image Data - TG_IMAGE_DATA
      sql_text = "SELECT DEPFILENBR, DEPFILESEQ, EVENTNBR, POSTNBR, POST_TYPE, SEQ_NBR FROM TG_IMAGE_DATA;";
      error_message = "";
      reader = db_reader.Make(database, sql_text, CommandType.Text, 0, null, 0, 0, null, ref error_message);
      if (error_message != "")
      {
        reader.CloseReader();
        return false;
      }

      results = new GenericObject();
      reader.ReadIntoRecordset(ref results, 0, 0);

      for (int i = 0; i < results.vectors.Count; i++)
      {
        GenericObject result = (GenericObject)results.vectors[i];
        if (!UpdateImageDataChecksum("strFileName", result.get("DEPFILENBR").ToString() + result.get("DEPFILESEQ").ToString(),
          "strEventNbr", result.get("EVENTNBR"), "strPostNbr", result.get("POSTNBR"), "strPostType", result.get("POST_TYPE"),
          "strSeqNum", result.get("SEQ_NBR"), "data", database) ||
          !ValidateImageDataChecksum("strFileName", result.get("DEPFILENBR").ToString() + result.get("DEPFILESEQ").ToString(),
          "strEventNbr", result.get("EVENTNBR"), "strPostNbr", result.get("POSTNBR"), "strPostType", result.get("POST_TYPE"),
          "strSeqNum", result.get("SEQ_NBR"), "config", database)
          )
        {
          reader.CloseReader();
          return false;
        }
      }
      reader.CloseReader();
      //#endregion
      //Bug 9092 start SN
         
      #region Update file Data - TG_DEPOSITTNDR_DATA
      sql_text = "SELECT DEPOSITID,DEPOSITNBR,SEQMBR FROM TG_DEPOSITTNDR_DATA;";
      error_message = "";
      reader = db_reader.Make(database, sql_text, CommandType.Text, 0, null, 0, 0, null, ref error_message);
      if (error_message != "")
      {
          reader.CloseReader();
          return false;
      }

      results = new GenericObject();
      reader.ReadIntoRecordset(ref results, 0, 0);

      for (int i = 0; i < results.vectors.Count; i++)
      {
          GenericObject result = (GenericObject)results.vectors[i];
          if (!UpdateDepositTndrDataChecksum("strDepositId", result.get("DEPOSITID").ToString(),
            "strDepositNbr", result.get("DEPOSITNBR"), "strSeqNbr",result.get("SEQNBR"), "data", database) ||
            !ValidateDepositTndrDataChecksum("strDepositId", result.get("DEPOSITID").ToString(),
            "strDepositNbr", result.get("DEPOSITNBR"), "strSeqNbr",result.get("SEQNBR"), "config", database)
            )
          {
              reader.CloseReader();
              return false;
          }
      }
      reader.CloseReader();
      #endregion
      #region Update file Data - TG_REVERSAL_TRAN_DATA 
        //This table does not exists at present for reference see Bug 5143
      sql_text = "SELECT TRANNBR,EVENTNBR,DEPFILENBR, DEPFILESEQ FROM TG_REVERSAL_TRAN_DATA;"; // this table is used for future implementation for reference see Bug 5143
      error_message = "";
      reader = db_reader.Make(database, sql_text, CommandType.Text, 0, null, 0, 0, null, ref error_message);
      if (error_message != "")
      {
          reader.CloseReader();
          return false;
      }

      results = new GenericObject();
      reader.ReadIntoRecordset(ref results, 0, 0);

      for (int i = 0; i < results.vectors.Count; i++)
      {
          GenericObject result = (GenericObject)results.vectors[i];
          if (!UpdateReversalTranDataChecksum("strTranNbr", result.get("TRANNBR").ToString(),
            "strEventNbr", result.get("EVENTNBR"), "strDepfileNbr", result.get("DEPFILENBR"), "strDepFileSeq", result.get("DEPFILESEQ"),"data", database) ||
            !ValidateReversalTranDataChecksum("strTranNbr", result.get("TRANNBR").ToString(),
            "strEventNbr", result.get("EVENTNBR"), "strDepfileNbr", result.get("DEPFILENBR"), "strDepFileSeq", result.get("DEPFILESEQ"),"config", database)
            )
          {
              reader.CloseReader();
              return false;
          }
      }
      reader.CloseReader();
      #endregion
        // end Bug 9092
      return true;
    }*/

    //Bug 10827 NJ
    /// <summary>
    /// This method removes the error code and puts a generic message, if 'Show Verbose Error' is turned off.
    /// For authentication related errors, only the code will be removed.
    /// Bug 14556.  Restored the error code to all messages and conditions.  
    /// </summary>
    /// <param name="code"></param>
    /// <param name="message"></param>
    public static void CleanErrorCode(ref string code, ref string message)
    {
      switch (code)
      {
        case "TS-791":  // Bug 22945 NK
        case "TS-1067":
        case "TS-1068":
        case "TS-1069":
        case "TS-1070":
        case "TS-1071":
        case "TS-1072":
        case "TS-1073":
        case "TS-1074":
        case "TS-1075":
        case "TS-1076":
        case "TS-1077":
        case "TS-1078":
        case "TS-1079":
        case "TS-1080":
        case "TS-1081":
        case "TS-1082":
        case "TS-1083":
        case "TS-1204":   // Bug 22923 NK
        case "TS-1118":
        case "TS-1220":
        case "TS-1248":
          // code = "";   // Bug 14556: Do not blank out these codes
          break;
        default:
          // Bug 14556: Change the message here to a generic message, but include the code.  Do not blank out code.
          // Bug 19817 UMN add datetime
          message =
            string.Format("An error has occurred.  Code: {0}. Please submit your request again. If the problem persists, contact your administrator. {1} ", code, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
          break;


      }

    }
    //end Bug 10827 NJ

    //Bug 11329 NJ
    /// <summary>
    /// Overloaded method for cases where the user should see the message irrespective of  show verbose errors turned off 
    /// </summary>
    /// <param name="strCodeID">Code</param>
    /// <param name="strErrorMsg">MEssage</param>
    /// <param name="bThrow">Flag to indicate whether the error should be thrown</param>
    /// <param name="strStatus">STatus</param>
    /// <param name="user_message">Flag to indicate whether user should always see this message</param>
    /// <returns></returns>
    public static GenericObject CreateErrorObject(string strCodeID, string strErrorMsg, bool bThrow, string strStatus, bool user_message)
    {

      GenericObject geoError = (GenericObject)c_CASL.c_make("error", "code", strCodeID, "message", strErrorMsg, "title", "transuite error", "throw", bThrow, "status", strStatus, "user_message", user_message);

      return geoError;
    }
    //end bug 11329 NJ

      //Bug 13832 - construct and return the detailed error message with the message and innerexception
    public static string ConstructDetailedError(Exception e)
    {
        StringBuilder ex = new StringBuilder(e.Message);
        if (e.InnerException != null)
      {
        ex.AppendLine(e.InnerException.Message);

        }

        return ex.ToString();
    }
      //end bug 13832
  

    // Bug #14185 Mike O - Move string building into its own function
    public static string BuildDepositChecksumString(string depositID, string depositNbr, string depositType, string groupID, string groupNbr,
        string fileNbr, string fileSequenceNbr, string ownerID, string CreatorID, string amount, string postDT, string voidDT, string voidID,
        string comments, string depSlipNbr, string bankID, string sourceType, string sourceGroup, string sourceRefID)
    {
      StringBuilder checksumBuilder = new StringBuilder();
      DateTime dt;

      checksumBuilder.Append(depositID);
      checksumBuilder.Append(depositNbr);
      checksumBuilder.Append(depositType);
      checksumBuilder.Append(groupID);
      checksumBuilder.Append(groupNbr);
      checksumBuilder.Append(fileNbr);
      checksumBuilder.Append(fileSequenceNbr);
      checksumBuilder.Append(ownerID);
      checksumBuilder.Append(CreatorID);
      checksumBuilder.Append(String.Format("{0:f}", float.Parse(amount)));
      if (DateTime.TryParse(postDT, out dt))
        checksumBuilder.Append(dt.ToString("G", DateTimeFormatInfo.InvariantInfo));
      if (DateTime.TryParse(voidDT, out dt))
        checksumBuilder.Append(dt.ToString("G", DateTimeFormatInfo.InvariantInfo));
      checksumBuilder.Append(voidID);
      checksumBuilder.Append(comments);
      checksumBuilder.Append(depSlipNbr);
      checksumBuilder.Append(bankID);
      checksumBuilder.Append(sourceType);
      checksumBuilder.Append(sourceGroup);
      checksumBuilder.Append(sourceRefID);

      return checksumBuilder.ToString();

    }


    public static string BuildPayEventChecksumString(string eventNbr, string depFileNbr, string depFileSeq, string userID, string status, string modDT)
    {
      StringBuilder checksumBuilder = new StringBuilder();
      DateTime dt;

      checksumBuilder.Append(eventNbr);
      checksumBuilder.Append(depFileNbr);
      checksumBuilder.Append(depFileSeq);
      checksumBuilder.Append(userID);
      checksumBuilder.Append(status);
      if (DateTime.TryParse(modDT, out dt))
        checksumBuilder.Append(dt.ToString("G", DateTimeFormatInfo.InvariantInfo));

      return checksumBuilder.ToString();
    }

    public static string BuildGRCustFieldChecksumString(string tranNbr, string eventNbr, string depFileNbr, string depFileSeq, string screenIndex, string label, string value)
    {
      StringBuilder checksumBuilder = new StringBuilder();

      checksumBuilder.Append(Int32.Parse(tranNbr).ToString());
      checksumBuilder.Append(Int32.Parse(eventNbr).ToString());
      checksumBuilder.Append(Int32.Parse(depFileNbr).ToString());
      checksumBuilder.Append(Int32.Parse(depFileSeq).ToString());
      checksumBuilder.Append(Int32.Parse(screenIndex).ToString());
      checksumBuilder.Append(label);
      checksumBuilder.Append(value);

      return checksumBuilder.ToString();
    }

    public static string BuildItemChecksumString(string itemNbr, string tranNbr, string eventNbr, string depFileNbr, string depFileSeq, string itemID, string GLAcctNbr,
      string acctID, string amount, string qty, string taxed, string total)
    {
      StringBuilder checksumBuilder = new StringBuilder();

      checksumBuilder.Append(Int32.Parse(itemNbr));
      checksumBuilder.Append(Int32.Parse(tranNbr));
      checksumBuilder.Append(Int32.Parse(eventNbr));
      checksumBuilder.Append(Int32.Parse(depFileNbr));
      checksumBuilder.Append(Int32.Parse(depFileSeq));
      checksumBuilder.Append(itemID);
      checksumBuilder.Append(GLAcctNbr);
      checksumBuilder.Append(acctID);
      checksumBuilder.Append(amount);
      checksumBuilder.Append(qty);
      checksumBuilder.Append(taxed);
      checksumBuilder.Append(total);

      return checksumBuilder.ToString();
    }
    // End Bug #14185 Mike O

    /// <summary>
    /// bug 12835 NJ-Store completion date(aka receipt datetime) to determine voids before and after receipting.
    /// Bug 18766: Changed to parameterized.
    /// </summary>
    /// <param name="pDBConn"></param>
    /// <param name="strEventNbr"></param>
    /// <param name="strFileName"></param>
    /// <param name="strNow"></param>
    /// <param name="strErr"></param>
    /// <returns></returns>
    public static bool UpdateCompletionDate(ref ConnectionTypeSynch pDBConn, string strEventNbr, string strFileName, string strNow, ref string strErr)
    {

      // Break the file into file number and sequence number
      string strFileNumber = Int64.Parse(GetFileNumber(strFileName)).ToString();
      string strFileSequenceNbr = Int64.Parse(GetFileSequence(strFileName)).ToString();

      string sql_string = "";
      try
      {
        sql_string = "UPDATE TG_PAYEVENT_DATA WITH (ROWLOCK) SET COMPLETIONDT=@completiondt WHERE EVENTNBR=@eventnbr AND DEPFILENBR=@depfilenbr AND DEPFILESEQ=@depfileseq AND COMPLETIONDT IS NULL";
        HybridDictionary sql_args = new HybridDictionary();
        sql_args["completiondt"] = strNow;
        sql_args["eventnbr"] = strEventNbr;
        sql_args["depfilenbr"] = strFileNumber;
        sql_args["depfileseq"] = strFileSequenceNbr;

        // Bug 25654: Changed from runExecuteNonQueryCommitRollback; we need to do the commit / rollback at a higher level
        int count = ParamUtils.runExecuteNonQuery(pDBConn, sql_string, sql_args);
        if (count != 1)
        {
          HandleErrorsAndLocking("message", "UpdateCompletionDate: Updated: " + count + " records instead of 1", "code", "TS-1116b", "Wrong update count");
        }

      }
      catch (Exception e)
      {
        string msg = "Error: this SQL failed: " + sql_string + ". Error: " + e.ToString();
        Logger.Log(msg, "Transuite: UpdateCompletionDate", "err");
        HandleErrorsAndLocking("message", "Error executing SQL.", "code", "TS-UpdateCompletionDate", e.ToString());
        return false;
      }


      return true;

    }
      //end bug 12757

    /// <summary>
    /// Safely close the database connection.   Log any errors.
    /// Bug 15941. FT
    /// </summary>
    /// <param name="args"></param>
    /// <param name="pDBConn_Data"></param>
    [Obsolete("Use connectionOpened boolean and CloseDBConnection(ConnectionTypeSynch pDBConn, bool connectionOpened) instead.", true)]
    private static void CloseDBConnectionIfOpenedLocally(ConnectionTypeSynch pDBConn_Data, GenericObject args)
    {
      if (!args.has("DbConnectionClass"))
      {
        CloseDBConnection(pDBConn_Data);
      }
    }
    
    /// <summary>
    /// Safely close the DB connection.
    /// Most methods in transuite can be passed in an opened database connection or 
    /// open their own connection locally. This method will only close the connection
    /// if it is opened locally.
    /// Bug 15941. FT.
    /// </summary>
    /// <param name="pDBConn"></param>
    /// <param name="connectionOpened">Whether the connection was opened locally</param>
    private static void CloseDBConnectionIfOpenedLocally(ConnectionTypeSynch pDBConn, bool connectionOpened)
    {
      if (connectionOpened)
        CloseDBConnection(pDBConn);
    }

    /// <summary>
    /// Close the connection.  
    /// Catch any closing errors and log.
    /// Bug 15941. FT.
    /// </summary>
    /// <param name="pDBConn"></param>
    private static void CloseDBConnection(ConnectionTypeSynch pDBConn)
    {
      try
      {
        // Close the database connection and dispose of pDBConn class.
        if (pDBConn != null)
          pDBConn.CloseDBConnection();
      }
      catch (Exception e)
      {
        string error_message = "Cannot close database connection: " + ConstructDetailedError(e);
        Logger.LogError(error_message, "CloseDBConnection"); 
        // Bug 17849
        Logger.cs_log_trace("Error in CloseDBConnection", e);
      }
    }
    /// <summary>
    /// Output checksum error information to activity log (TG_ACTLOG_DATA) and 
    /// (at least for now) the error log.
    /// <para>
    /// The following fields will be written to the activity log:
    /// <list type="table">
    ///   <listheader>
    ///     <term>DB Field</term>
    ///     <description>Description</description>
    ///   </listheader>
    /// <item><term>POSTDT</term><description>Timestamp that the checksum error was detected</description></item>
    /// <item><term>SESSION_ID</term><description>Session id from casl object that detected error</description></item>
    /// <item><term>USERID</term><description>ID of user when error detected</description></item>
    /// <item><term>APP</term><description>LogChecksumError</description></item>
    /// <item><term>ACTION_NAME</term><description>Checksum Error</description></item>
    /// <item><term>ARGS</term><description>Key fields of the erroneous records</description></item>
    /// <item><term>SUMMARY</term><description>Checksum Error</description></item>
    /// <item><term>DETAIL</term><description>Key fields of the erroneous records</description></item>
    /// </list>
    /// </para>
    /// See also: <seealso>http://172.16.1.141/bugzilla/show_bug.cgi?id=12303</seealso>
    /// <para>
    /// Originally part of the checksum fix for SD:
    /// <seealso>http://172.16.1.141/bugzilla/show_bug.cgi?id=12297</seealso>
    /// </para>
    /// <remarks>There is no programatic way to tell <i>which</i> field caused the checksum error.  
    /// The best solution would be the "Change Data Capture" facility of SQL Server 2008. </remarks>
    /// </summary>
    /// <param name="table">Database table name</param>
    /// <param name="methodName">Name of method that detected failure</param>
    /// <param name="recordInfo">Database record Key information</param>
    public static void LogChecksumError(string table, string methodName, string recordInfo, string details)
    {
      // Bug 19644: make sure that all this code that depends on CASL behaving itselft does not crash iPayment. CASL cannot be trusted.
      try
      {
        Logger.Log($"Checksum Error detected in {methodName}: record key: {recordInfo} in {table}", "Transuite: LogChecksumError", "err");

        misc.CASL_call("subject", c_CASL.GEO.get("activity_log"), "a_method", "write_database_log",
        "args", new GenericObject(
        "user", (CASLInterpreter.get_my() as GenericObject).get("login_user", ""),
        "subject", "",
        "app", methodName,
        "action", "Checksum Error",
        "args", table + ':' + recordInfo,
        "summary", "Checksum Error",
        "detail", details));
      }
      catch (Exception e)
      {
        try
        {
          Logger.cs_log("Failure to log this Checksum Error: " + string.Format("Checksum Error detected in {0}: record key: {1} in {2}",
        methodName, recordInfo, table) + " Error: " + e.ToString());
        }
        catch (Exception innerException)
        {
          Logger.cs_log("Cannot log checksum error: " + innerException.ToString());
        }

      }
      // End Bug 19644
    }

    // BUG#13915
    public static object CASLUpdateLockedEventforUser(params Object[] args)
    {
        GenericObject geo_args = misc.convert_args(args);

        string strEventNbr = (string)geo_args.get("event_nbr");
        string strUserID = (string)geo_args.get("user_id");

        EventValidator.GetInstance().updateLockedEventforUser(strEventNbr, strUserID);
        return null;
    }
    // BUG#13915
    public static object CASLIsValidEventForUser(params Object[] args)
    {
        GenericObject geo_args = misc.convert_args(args);

        string strEventNbr = (string)geo_args.get("event_nbr");
        string strUserID = (string)geo_args.get("user_id");

        return EventValidator.GetInstance().validateEventforUser(strEventNbr, strUserID);
    }

    // BUG#13915
    public static string CASLGetEventLockedUser(params Object[] args)
    {
        GenericObject geo_args = misc.convert_args(args);

        string strEventNbr = (string)geo_args.get("event_nbr");

        return EventValidator.GetInstance().getEventLockedUser(strEventNbr);
    }

  }

  public static class EventChecker// Bug# 27153 DH - Made static
  {
        public static object GetMaximumTenderAmount(params object[] args)
        {
            // Bug 12590: Selecting FSA Card Tender Displays Incorrect Balance Message
            // Based on Banking Rules and the tender amount posted, this method will
            // return the "maximum" allowable amount (up to the unpaid balance) for a
            // specific tender type.
            GenericObject geoArgs = misc.convert_args(args);
            GenericObject geoTenderObj = geoArgs.get("a_tender_obj") as GenericObject;
            GenericObject geoCoreEvent = geoArgs.get("a_core_event") as GenericObject;
            decimal dCurrentRemainAmt = (decimal)geoArgs.get("a_current_remain_amt", 0.00M);
            GenericObject geoTransactions = null;
            GenericObject geoTenders = null;
            object theObj = null;
            
            // Status of Tenders & Transactions:
            //const string sSTATUS_CANCELLED = "cancelled";
            //const string sSTATUS_VOIDED    = "voided";
            //const string sSTATUS_ACTIVE    =  "active";
            const string sSTATUS_FINISHED  =  "finished";
            const string sSTATUS_COMPLETED =  "completed";

      // The Tender Object may be a configured tender class object OR it could be 
      // an instance of a class; get the parent if it is an instance.
      GenericObject geoTenderClass = geoTenderObj; 
            if (geoTenderObj.has("total") || geoTenderObj.has("core_event"))
            {
                // geoTenderObj may be the instance of a tender class
                GenericObject geoTndrObjParent = geoTenderObj.get("_parent", null) as GenericObject;
                if (geoTndrObjParent != null)
                {
                  geoTenderClass = geoTndrObjParent;
                }
            }

            theObj = geoCoreEvent.get("total", 0.00M);
            decimal dTransTotal = theObj is decimal ? (decimal)theObj : 0.00M;

            // The remaining balance will be the GREATEST amount returned
            theObj = geoCoreEvent.get("balance", 0.00M);
            decimal dMaxAmt = theObj is decimal ? (decimal)theObj : 0.00M;

            // If balance is negative or zero - just return current remaining amount 
            if (dMaxAmt <= 0.00M) { return dCurrentRemainAmt; }

            // Get the list of tenders for this event
            if (geoCoreEvent != null && geoCoreEvent.has("tenders"))
            {
                geoTenders = (GenericObject)geoCoreEvent.get("tenders");
            }

            // Check if any of this tender is posted already
            decimal dTenderPostedAmt = 0.00M;
            int lTenderCount = (int)geoTenders.getLength();
            for (int i = 0; i < lTenderCount; i++)
            {
                GenericObject geoTender = (GenericObject)geoTenders.get(i);
                GenericObject geoStatus = geoTender == null ? null : (GenericObject)geoTender.get("status", null);
                string sStatus = geoStatus == null ? "" : (string)geoStatus.get("_name");
                if (sStatus == sSTATUS_COMPLETED || sStatus == sSTATUS_FINISHED)
                {
                    if ((bool)misc.base_is_a(geoTender, geoTenderClass))
                    {
                        decimal dTotal = (decimal)geoTender.get("total", 0.00M);
                        dTenderPostedAmt += dTotal;
                    }
                }
            }

      // Get the list of transactions for this event  BUG 17636
      geoTransactions = new GenericObject();

            if (geoCoreEvent != null && geoCoreEvent.has("transactions"))
            {
        GenericObject geoTemp = ((GenericObject)geoCoreEvent.get("transactions")); // BUG 17636
        foreach (Object oTran in geoTemp.vectors)
        {
          GenericObject geoTran = oTran as GenericObject;
          if (geoTran.has("core_items"))
          {
            foreach (object geoBTT in (geoTran.get("core_items") as GenericObject).vectors)
            {
              geoTransactions.insert(geoBTT);
            }
          }
          else
            geoTransactions.insert(geoTran);
        }
      }
            
            // Check if the tender type is allowed in ALL bank rules - if so, return the balance.
            bool bTenderInALLBankRules = true;

            // Get the total amount allowed by Bank Rules for the tender.
            decimal dBR_Amt = 0.00M;
            if (geoTransactions != null)
            {
                int lTranCount = (int)geoTransactions.getLength();
                for (int i = 0; i < lTranCount; i++)
                {
                    GenericObject geoTran = (GenericObject)geoTransactions.get(i);
                    GenericObject geoStatus = geoTran == null ? null : (GenericObject)geoTran.get("status", null);
                    string sStatus = geoStatus == null ? "" : (string)geoStatus.get("_name");
                    if (sStatus == sSTATUS_COMPLETED || sStatus == sSTATUS_FINISHED)
                    {
                        bool bTenderInThisBankRule = false;
                        decimal dAmt = (decimal)geoTran.get("amount", 0.00M);
                        GenericObject geoBR = geoTran.get("bank_rule", null, true) as GenericObject;
                        // Bug 24078 check transaction with opt bank_rule
                        if (!c_CASL.c_opt.Equals(geoBR))
                        {
                          GenericObject geoBRItems = geoBR.get("bank_rule_items", null) as GenericObject;
                          foreach (Object objBRItem in geoBRItems.vectors)
                          {
                            GenericObject geoBRItem = objBRItem as GenericObject;
                            GenericObject geoTenderInBR = geoBRItem.get("tenders", null) as GenericObject;
                            if (geoTenderInBR != null)
                            {
                              if (geoTenderInBR.Contains(geoTenderObj))
                              {
                                dBR_Amt += dAmt;
                                bTenderInThisBankRule = true;
                              }
                            }
                        }
                      }
                      if (!bTenderInThisBankRule) { bTenderInALLBankRules = false; }
                       
                    }
                }
            }

            if (!bTenderInALLBankRules)
            {
                // Bug 12634: Add FSA Card Tender to Generic "ANY" Card
                decimal dDiff = dBR_Amt - dTenderPostedAmt;
                if (dDiff < dMaxAmt)
                {
                    dMaxAmt = dDiff > 0.00M ? dDiff : 0.00M;
                } 
                if (dMaxAmt > 0.00M)
                {
                    // Test the tender amount in the Event's TTA Mapping:
                    bool bAmtOK = (bool)misc.CASL_call(
                        "a_method", "test_tender_amt_in_tta",
                        "_subject", geoCoreEvent,
                        "args", new GenericObject(
                            "a_tender_class", geoTenderObj,
                            "a_tender_amt", dMaxAmt
                            )
                        );

                    if (!bAmtOK) 
                    {
                        dMaxAmt = dCurrentRemainAmt;
                    }
                }
                else
                {
                    dMaxAmt = 0.00M;
                }
            }
            return (decimal.Round(dMaxAmt, 2));
        }
    }
}
