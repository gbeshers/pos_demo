﻿using System;
using System.Data;
using System.Threading;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using System.Globalization;

using CASL_engine;

namespace TranSuiteServices
{
  public static class ParamUtils// Bug# 27153 DH - Made it static
  {    
    /// <summary>
    /// xecute INSERT or UPDATE type of SQL command and return a count.
    /// This method will do a Commit / Rollback transactions if the transaction is set.
    /// </summary>
    /// <param name="pDBConn"></param>
    /// <param name="sql_string"></param>
    /// <param name="sql_args"></param>
    /// <param name="errorMsg"></param>
    /// <returns>Count of records updated or inserted</returns>
    [Obsolete("Bug 25654: Please get permission from Fred Thurber before using this.  In general you should do the commit rollbacl at a higher level")]
    public static int runExecuteNonQueryCommitRollback(ConnectionTypeSynch pDBConn, string sql_string, HybridDictionary sql_args, out string errorMsg)
    {
      int count = 0;
      errorMsg = "";
      using (SqlCommand paramCommand = ParamUtils.CreateParamCommand(pDBConn, sql_string, sql_args))
      {
        try
        {
          count = paramCommand.ExecuteNonQuery();
          pDBConn.CommitToDatabase(ref errorMsg);     // pDBConn will check to see if the transaction is set before doing this.
          return count;
        }
        catch (Exception e)
        {
          // Bug 17849
          Logger.cs_log_trace("Error runExecuteNonQueryCommitRollback", e);
          pDBConn.RollBack();      // pDBConn will check to see if the transaction is set before doing this.
          throw;
        }
      }
    }

    
    /// <summary>
    /// Execute INSERT or UPDATE type of SQL command and return a count of records updated or inserted
    /// This method does <b>not</b> do a commit/rollback.
    /// </summary>
    /// <param name="pDBConn"></param>
    /// <param name="sql_string"></param>
    /// <param name="sql_args"></param>
    /// <param name="errorMsg"></param>
    /// <returns>Count of records updated or inserted</returns>
    public static int runExecuteNonQuery(ConnectionTypeSynch pDBConn, string sql_string, HybridDictionary sql_args)
    {
      int count = 0;
      using (SqlCommand paramCommand = ParamUtils.CreateParamCommand(pDBConn, sql_string, sql_args))
      {
          count = paramCommand.ExecuteNonQuery();
          return count;
      }
    }

    /// <summary>
    /// Utility method to ExecuteScalar and determine if the count is one or more.
    /// This typically used for SELECT COUNT(*) commands.
    /// Bug 25617: Removed commit / rollback: This was screwing up the SqlTransaction at the PostCashieringTransaction() level
    /// so that PostCashieringTransaction was never able to rollback!
    /// </summary>
    /// <param name="paramCommand">Typically a parameterized SqlCommand</param>
    /// <returns></returns>
    public static bool isCountOneorMore(ConnectionTypeSynch pDBConn, SqlCommand paramCommand, ref string strErr)
    {
      try
      {
        var count = paramCommand.ExecuteScalar();
        // pDBConn.CommitToDatabase(ref strErr);    // Bug 25617: Don't use up the SqlTransaction of the calling method

        if (count.GetType() == typeof(DBNull))
          return false;

        int intCount = Convert.ToInt32(count);
        if (intCount < 1)
          return false;
        else
          return true;
      }
      catch(Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("Error isCountOneorMore", e);
        // pDBConn.RollBack();      // Bug 25617:  Do this at a higher level
        throw;
      }
    }

    /// <summary>
    /// Utility method (used by the Activity Log for instance) to create a 
    /// new SqlCommand and load its parameters from the passed
    /// in sql_args.
    /// Based on the AddCommand() method in db_vector_reader without the 
    /// scary sql command queue.
    /// </summary>
    /// <param name="connection">Connection with the connection string already set</param>
    /// <param name="sqlText">The SQL Command string</param>
    /// <param name="sql_args"></param>
    /// <returns></returns>
    static public SqlCommand CreateParamCommand(
      SqlConnection connection,
      string sqlText,
      HybridDictionary sql_args)
    {
      SqlCommand newCommand = new SqlCommand();
      newCommand.CommandText = sqlText;
      newCommand.CommandType = CommandType.Text;
      newCommand.Connection = connection;

      loadParamArgs(sql_args, newCommand);

      return newCommand;
    }

    /// <summary>
    /// Utility method to create a 
    /// new SqlCommand and load its parameters from the passed
    /// in sql_args.  Same as above except that the SqlTransaction 
    /// can be specified.
    /// </summary>
    /// <param name="connection">Connection with the connection string already set</param>
    /// <param name="transaction"></param>
    /// <param name="sqlText">The SQL Command string</param>
    /// <param name="sql_args"></param>
    /// <returns></returns>
    static public SqlCommand CreateParamCommand(
      SqlConnection connection,
      SqlTransaction transaction,
      string sqlText,
      HybridDictionary sql_args)
    {

      SqlCommand newCommand = new SqlCommand();
      newCommand.CommandText = sqlText;
      newCommand.CommandType = CommandType.Text;
      newCommand.Connection = connection;
      newCommand.Transaction = transaction;

      loadParamArgs(sql_args, newCommand);

      return newCommand;
    }


    /// <summary>
    /// Utility method (typically used by transuite) to create a 
    /// new SqlCommand and load its parameters from the passed
    /// in sql_args.  This takes a ConnectionTypeSynch to make it
    /// compatible with most transuite calls.
    /// Note that this also sets the Transaction if it is set in ConnectionTypeSynch
    /// so that this command can be rolled back if need be.
    /// </summary>
    /// <param name="pDBConn"></param>
    /// <param name="sqlText"></param>
    /// <param name="sql_args"></param>
    /// <returns></returns>
    static public SqlCommand CreateParamCommand(
      ConnectionTypeSynch pDBConn,
      string sqlText,
      HybridDictionary sql_args)
    {
      SqlCommand newCommand = new SqlCommand();
      newCommand.CommandText = sqlText;
      newCommand.CommandType = CommandType.Text;
      newCommand.Connection = pDBConn.GetSqlConnection();
      if (pDBConn.GetSqlTransaction() != null)
      {
        newCommand.Transaction = pDBConn.GetSqlTransaction();
      }

      loadParamArgs(sql_args, newCommand);
      return newCommand;

    }

    /// <summary>
    /// The DataTables time picker can now send date plus time values.
    /// They come in as strings.
    /// Some of these date strings are not parseable so I have
    /// to hack around that.
    /// 
    /// The datetimepicker will send dates of this format: 
    /// "01/29/2019", "01/29/2019 12:00", "01/30/2019 23:59:59:999", 
    /// and, unfortunately, "01/30/2019 00:09 23:59:59:999"
    /// The last format is unparsable so I had to chop it after the 
    /// first HH:mm.
    /// 
    /// Also note that the full time string comes in as 23:59:59:999
    /// not the standard: 23:59:59.999
    /// 
    /// This method will reset the arg_value if it is a date string
    /// into a DateTime and set the type to System.Data.SqlDbType.DateTime
    /// 
    /// Bug 24047
    /// 
    /// </summary>
    /// <param name="arg_value"></param>
    /// <param name="parameter"></param>
    private static void parseDataTablesDate(ref object arg_value, ref SqlParameter parameter)
    {
      // Let's first decide if this is a date
      DateTime dataTables_datetime;
      string format = "MM/dd/yyyy";
      string dateString = arg_value as string;
      if (dateString.Length < 10 ||
        !DateTime.TryParseExact(dateString.Substring(0, 10), format, CultureInfo.InvariantCulture,
          DateTimeStyles.None, out dataTables_datetime))
      {
        // Not a date, just a string.  Let's skedaddle 
        parameter.SqlDbType = System.Data.SqlDbType.VarChar;
        return;
      }

      // So we have a date.  Try to work around the DataTables weirdness:
      // The datetimepicker will send dates of this format: 
      // "01/29/2019", "01/29/2019 12:00", "01/30/2019 23:59:59:999", 
      // or, unfortunately, "01/30/2019 00:09 23:59:59:999"
      // The last format is unparsable so chop it after the first HH:mm
      if (dateString.Length > 23)
      {
        // We have one of these: 01/30/2019 00:09 23:59:59:999
        // Chop to this: 01/30/2019 00:09
        // Note this should not happen because of a more intelligent parse in constructDateRangeQuery()
        dateString = dateString.Substring(0, 16);
      }
      // Bug 24047: With the change to AM/PM time format pickers, the date comes through as: "03/14/2019 06:00:pm"
      // so I added the  "MM/dd/yyyy hh:mm:tt"
      string[] formats = { "MM/dd/yyyy", "MM/dd/yyyy HH:mm", "MM/dd/yyyy HH:mm:ss:fff", "MM/dd/yyyy hh:mm:tt" };
      if (DateTime.TryParseExact(dateString, formats, CultureInfo.InvariantCulture,
          DateTimeStyles.None, out dataTables_datetime))
      {
        parameter.SqlDbType = System.Data.SqlDbType.DateTime;
        arg_value = dataTables_datetime as object;
      }
      else
      {
        parameter.SqlDbType = System.Data.SqlDbType.VarChar;
      }

    }
    /// <summary>
    /// Utility method to loop through the passed arguments and set the 
    /// parameter type and set the parameter with the Add() method 
    /// and add it to the SqlCommand, being careful
    /// about nulls.
    /// </summary>
    /// <param name="sql_args"></param>
    /// <param name="new_command"></param>
    private static void loadParamArgs(HybridDictionary sql_args, SqlCommand newCommand)
    {
      if (sql_args != null)
      {
        // TODO: loop through string keys of sql_args, and for each, add as SQL parameter based on the type.
        object arg_value = null;
        SqlParameter parameter = null;
        foreach (string key in sql_args.Keys)
        {
          arg_value = sql_args[key];
          parameter = new SqlParameter();
          parameter.ParameterName = "@" + key;

          parameter.SqlDbType = DatabaseParamUtils.getSqlDbType(arg_value, parameter.ParameterName);    // Bug 25175

          parameter.Direction = ParameterDirection.Input;
          if (arg_value == null)
          {
            parameter.Value = DBNull.Value;
          }
          else
          {
            parameter.Value = arg_value;
          }
          newCommand.Parameters.Add(parameter);
        }
      }
    }

  
    internal static void setField(HybridDictionary sql_args, string paramName, string stringParamValue)
    {
      if (string.IsNullOrEmpty(stringParamValue))
        sql_args[paramName] = null;
      else
        sql_args[paramName] = stringParamValue;
    }

    [Obsolete("We should try to move to HybridDictionary", false)]
    internal static void setField(Hashtable sql_args, string paramName, string stringParamValue)
    {
      if (string.IsNullOrEmpty(stringParamValue))
        sql_args[paramName] = null;
      else
        sql_args[paramName] = stringParamValue;
    }



  }
}
