﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.IO;
using System.Xml;

using CASL_engine;

//using Microsoft.Data.Schema.ScriptDom.Sql;
//using Microsoft.SqlServer.Management;

namespace TranSuiteServices
{
  public class ActivityLogUtils
  {

    /// <summary>
    /// Set of keys that should be masked before being written to the activity log.
    /// Bug 17826
    /// </summary>
    private static readonly HashSet<string> privateFieldValues = new HashSet<String>
    { 
      "CC_USERPW",
      "CC_USERPW_OVERRIDE",
      "password", 
      "previous_passwords", 
      "Import_Config_password", 
      "ftp_password", 
      "login_password",
      "user_pwd"      // Bug 23773 
    };


    /// <summary>
    /// Attempt to capture the creation, deletion, or modification of a
    /// user in Configuration.
    /// This is tricky because these actions are never explicitly stated
    /// in the data so I have to infer these actions from the 
    /// data sent.
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static string captureUseChangeDetails(params object[] arg_pairs)
    {
      try
      {
        GenericObject args = misc.convert_args(arg_pairs);
        string details = args.get("details") as string;
        GenericObject sql_commands = args.get("sql_commands") as GenericObject;
        GenericObject modified_objects = args.get("modified_objects") as GenericObject;


        HashSet<string> field_keys = new HashSet<string>();
        string actionName = null;

        // Collect the field_keys to try to determins whether this was a delete, modify or create
        foreach (var command in sql_commands.vectors)
        {
          if (command is GenericObject)
          {
            GenericObject geo = command as GenericObject;
            if (geo.has("field_key"))
            {
              field_keys.Add(geo.get("field_key") as string);
            }
          } 
          else if (command is string)
          {
            // Perversly, the Delete does not pass a GEO but SQL strings, ugh, I hate casl
            string stringCommand = command as string;
            if (stringCommand.Contains("DELETE"))
              actionName = "Delete User";
          }
        }

        // Now let's see of the password was modified
        string modifiedPassword = null;

        foreach (var modifiedObject in modified_objects.Values)
        {
          if (modifiedObject is GenericObject)
          {
            GenericObject geo = modifiedObject as GenericObject;
            if (geo.has("password_last_changed_on"))
            {
              string lastChangedOn = geo.get("password_last_changed_on") as string;
              string today = DateTime.Now.ToShortDateString();
              if (lastChangedOn == today)
              {
                modifiedPassword = "Modified User Password";
              }
            }
          }
        }


        if (field_keys.Contains("\"has_image\"") && field_keys.Contains("\"security_profile\""))
        {
          actionName = "Create User";
        }
        else if (field_keys.Contains("\"departments\"") || 
          field_keys.Contains("\"name\"") ||
          field_keys.Contains("\"security_profile\"") ||
          field_keys.Contains("\"has_image\"")
          )
        {
          actionName = "Modify User";
        }
        else if (actionName == null && modifiedPassword != null)
        {
          // Tricky logic to find out if only the password was modified
          actionName = modifiedPassword;
        }

        // If this is not a Create, Delete, or Modify user, do not change the details
        if (actionName == null)
          return details;

        return details = details + ",Action:" + actionName;
      }
      catch (Exception e)
      {
        //  Bug 17849
        Logger.cs_log_trace("captureUseChangeDetails failed", e);
        return "";
      }
     }


    /// <summary>
    /// Format and filter the details for the activity log and filter out passwords.
    /// Called like this:
    /// .<mask_actlog_details> details name_text field_counter value.container value.field_key value.field_value </mask_actlog_details>
    /// Formally the CASL code did this:
    /// <join>details "," name_text "_" field_counter ":"  "container=" value.container "|" " field_key=" value.field_key "|" " field_value=" value.field_value </join>
    /// Bug 17826.
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static string mask_actlog_details(params object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);
      string details = args.get("details") as string;
      string name_text = args.get("name_text") as string;
      int field_counter = Convert.ToInt32(args.get("field_counter"));
      string container = args.get("container") as string;
      string field_key = args.get("field_key") as string;
      string field_value = args.get("field_value") as string;

      if (privateFieldValues.Contains(field_key.Trim('"')))
        field_value = "********";

      StringBuilder sb = new StringBuilder();
      sb.Append(details).Append(',')
        .Append(name_text).Append('_')
        .Append(field_counter).Append(':')
        .Append("category=").Append(container).Append('|')        // Bug 21014: Do not use "container"
        .Append(" item=").Append(field_key).Append('|')           // Bug 21014: Do not use "field_key"
        .Append(" item_value=").Append(field_value);              // Bug 21014: Do not use "field_value"

      return sb.ToString();
    }

    /// <summary>
    /// Only log the following fields.
    /// We could make this list configurable.  
    /// Could use a HybridDictionary<> or List<> here instead of a HashSet<>.
    /// Bug 18732
    /// </summary>
    private static HashSet<string> m_ipchargeFieldsToLog =
      new HashSet<string> { 
        /* Response */ "RESULT", "RESPONSE_TEXT", 
        /* Request and Response */ "TRANS_AMOUNT", "INVOICE", "COL_3" 
      };


    /// <summary>
    /// Parse the IP Charge XML and set the detail list.
    /// Changed to use the more efficient XmlReader from XDocument (Uttam asked to do this).
    /// Moved from the undebuggable misc.cs and cleaned up.
    /// This is called directly from pci.cs and indirectly from IPCharge.casl.
    /// Bug 18732
    /// </summary>
    /// <param name="xmlText"></param>
    /// <returns></returns>
    public static string parseXMLSetDetails(string xmlText)
    {
      bool firstInList = true;
      StringBuilder sb = new StringBuilder();

      using (XmlReader reader = XmlReader.Create(new StringReader(xmlText)))
      {
        string elementName = "";
        string elementValue = "";
        // Parse the file and display each of the nodes.
        while (reader.Read())
        {
          switch (reader.NodeType)
          {
            case XmlNodeType.Element:
              elementName = reader.Name;
              break;
            case XmlNodeType.Text:
              elementValue = reader.Value;
              break;
            case XmlNodeType.EndElement:
              if (m_ipchargeFieldsToLog.Contains(elementName))
              {
                // Append comma if after the first item
                if (firstInList)
                  firstInList = false;
                else
                  sb.Append(',');

                // Build detail list
                sb.Append(elementName).Append(':').Append(elementValue);
              }
              elementName = elementValue = "";
              break;
          }
        }
      }
      return sb.ToString();
    }


    /// <summary>
    /// Parse the IP Charge XML and create the detail list
    /// Bug 18732
    /// </summary>
    /// <param name="arg_pairs"></param>
    /// <returns></returns>
    public static string actlog_format_ipcharge_message(params Object[] arg_pairs)
    {
      try
      {
        GenericObject args = misc.convert_args(arg_pairs);

        string ipChargeXML = args.get("ip_charge_xml") as string;
        //string typeOfParse = args.get("type_of_parse") as string;     // Not needed yet...

        return parseXMLSetDetails(ipChargeXML);
      }
      catch (Exception e)
      {
        // Bug 17849
        Logger.cs_log_trace("actlog_format_ipcharge_message failed", e);
        return "IP Charge Logging Error: " + e.Message;
      }
    }




  }
}
