﻿using System;
using System.Linq;
using CASL_engine;
using System.Collections.Concurrent;
using System.Xml.Linq;
using System.Threading;
using System.Collections.Specialized;

namespace TranSuiteServices
{
  public sealed class PointCache
  {
    // Bug 24215 UMN Move here
    // Bug 21595 MJO - Store Point XML responses for CoreDirect to retrieve
    private static ConcurrentDictionary<string, string> _PointResponseCache = new ConcurrentDictionary<string, string>();

    // Bug 21595 MJO - Store successful Point responses for later retrieval
    public static void CachePointResponse(string receiptNbr, string response)
    {
      _PointResponseCache[receiptNbr] = response;

      // Bug 24689 MJO - Remove after a period of time
      Thread thread = new Thread(() => RemovePointResponse(receiptNbr));
      thread.Start();
    }

    public static string GetPointResponse(string receiptNbr)
    {
      if (_PointResponseCache.ContainsKey(receiptNbr))
        return _PointResponseCache[receiptNbr];
      else
        return null;
    }

    // Bug 24689 MJO - Remove receipt number after 5 minutes. It shouldn't be needed after that and we don't want it to stick around forever
    private static void RemovePointResponse(string receiptNbr)
    {
      Thread.Sleep(new TimeSpan(0, 5, 0));

      string outStr;

      _PointResponseCache.TryRemove(receiptNbr, out outStr);
    }

    /// <summary>
    /// Takes the point XML elements from the PointResponseCache, and renames them and adds them to the status
    /// that Core Direct checks.
    /// </summary>
    /// <param name="receipt_ref"></param>
    /// <param name="statusChanged"></param>
    public static void IntegratePointResponseIntoStatus(String receipt_ref, GenericObject statusChanged)
    {
      string response;
      if (_PointResponseCache.TryGetValue(receipt_ref, out response))
      {
        XDocument doc = XDocument.Parse(response);
        foreach (XElement element in doc.Root.Elements().ToList())
        {
          // Bug 25188 MJO - Skip FSA_AMOUNT so Direct doesn't mark is_fsa based on it
          if (element.Name == "RECEIPT_DATA" || element.Name == "FSA_AMOUNT") continue;
          string renamed = "POINT_" + element.Name;
          statusChanged.set(renamed, element.Value);
        }
      }

      string card_brand = statusChanged.get("TENDER_TYPE", statusChanged.get("POINT_PAYMENT_MEDIA", "Unknown")) as string;
      statusChanged.set("POINT_PAYMENT_MEDIA", card_brand.Replace(" FSA", ""));

      // Bug 24969 handle manualCC processed FSA card
      if ((bool)statusChanged.get("IS_FSA", false))
      {
        string trans_amount = statusChanged.get("POINT_TRANS_AMOUNT", "") as string;
        trans_amount = statusChanged.get("POINT_APPROVED_AMOUNT", trans_amount) as string;
        statusChanged.set("POINT_FSA_AMOUNT", trans_amount);
      }

      //_PointResponseCache.TryRemove(receipt_ref, out response);
    }
  }
}
