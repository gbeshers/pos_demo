﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using CASL_engine;
using TranSuiteServices;


//namespace baseline
namespace TranSuiteServices
{
  public class VoidNotes
  {
    public static GenericObject WriteNotesRecord(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      try
      {
        GenericObject note_info = (GenericObject)geo_args.get("note_info");
        GenericObject dbConnection = (GenericObject)geo_args.get("db_connection");

        // Check if note_info include voidreason if not, not save in db
        string void_reason = (string)note_info.get("VOIDREASON");
        void_reason = void_reason.Replace('|', ' ');
        if (String.IsNullOrEmpty(void_reason.Trim()))
          return geo_args;

        if (dbConnection == null)
        {
          Logger.LogError("db_connection not in params passed to WriteNotesRecord", "Void Notes WriteNotesRecord");
          return null;
        }
        String conString = (String)dbConnection.get("db_connection_string");


        // get owner which is a foreign key to the primary key of the owner of the note
        List<int> lstOwner = GetOwner(conString, note_info);

        // format note data
        string note_data = FormatNoteData(note_info);

        foreach (int owner in lstOwner)
        {
          string owner_type = (string)note_info.get("OWNER_TYPE","");
          string note_type = (string)note_info.get("NOTE_TYPE","");
          string note_user_id = (string)note_info.get("NOTE_USER_ID","");
         // DateTime note_datetime = Convert.ToDateTime((string)note_info.get("NOTE_DATETIME")); // often got exception
          DateTime note_datetime = DateTime.Now;

          if (string.IsNullOrWhiteSpace(note_user_id))
          {
            GenericObject my = CASLInterpreter.get_my();
            if (my.has("login_user"))
            {
              string login_user = my.get("login_user") as string;
              note_user_id = login_user;
            }
          }

          using (SqlConnection dbConn = new SqlConnection())
          {
            dbConn.ConnectionString = conString;
            dbConn.Open();

            String insertNoteSql = @"INSERT INTO TG_NOTE_DATA (OWNER,OWNER_TYPE,NOTE_TYPE,NOTE_USER_ID,NOTE_DATETIME,NOTE_DATA)
                                  VALUES(@owner,@owner_type,@note_type,@note_user_id,@note_datetime,@note_data)";
            SqlCommand cmd = new SqlCommand(insertNoteSql, dbConn);
           
            cmd.Parameters.Add("@owner",SqlDbType.VarChar).Value = owner;
            cmd.Parameters.Add("@owner_type", SqlDbType.VarChar).Value = owner_type;
            cmd.Parameters.Add("@note_type", SqlDbType.VarChar).Value = note_type;
            cmd.Parameters.Add("@note_user_id", SqlDbType.VarChar).Value = note_user_id;
            cmd.Parameters.Add("@note_datetime", SqlDbType.DateTime).Value = note_datetime;
            cmd.Parameters.Add("@note_data", SqlDbType.Xml).Value = note_data;
            cmd.ExecuteNonQuery();
          }
        }

      }
      catch (Exception e)
      {
        string baseMsg = "Error writing to Void Notes Table: ";
        Logger.LogError(baseMsg + e.Message, "Void Notes Recording");
        // Bug 17849
        Logger.cs_log(baseMsg + e.ToMessageAndCompleteStacktrace());
      }
      return geo_args;
    }

    protected static List<int> GetOwner(string conString, GenericObject note_info)
    {
      List<int> lstOwner = new List<int>();

      string owner_type = note_info.get("OWNER_TYPE") as string;
      string note_type = note_info.get("NOTE_TYPE", "") as string;
      string fileId = note_info.get("FILEID", "") as string;
      string eventnbr = note_info.get("EVENTNBR", "") as string;
      if (string.IsNullOrEmpty(eventnbr))
      {
        if (note_info.get("EVENTNBR", "").GetType() == typeof(int))
          eventnbr = string.Format("{0}", (int)note_info.get("EVENTNBR", 0));
        else
          eventnbr = "0";
      }
      string transnbr = note_info.get("TRANSNBR", "") as string;
      if (string.IsNullOrEmpty(transnbr))
      {
        if (note_info.get("TRANSNBR", "").GetType() == typeof(int))
          transnbr = string.Format("{0}", (int)note_info.get("TRANSNBR", 0));
        else
          transnbr = "0";
      }
      string fileNbr = fileId.Substring(0, 7);
      string seqNbr = fileId.Substring(7);
     

      string selSql = "";
      HybridDictionary sql_args = new HybridDictionary();
      int owner = -1;

      if (note_type.ToUpper() == "VOIDREASON")
      {
        if (owner_type.ToUpper() == "EVENT")
        {
          selSql = @"SELECT UNIQUEID as OWNER FROM TG_PAYEVENT_DATA WHERE DEPFILENBR=@filenbr AND DEPFILESEQ=@seqnbr AND EVENTNBR=@eventnbr AND STATUS=@status";
          sql_args["filenbr"] = Convert.ToInt32(fileNbr);
          sql_args["seqnbr"] = Convert.ToInt32(seqNbr);
          sql_args["eventnbr"] = Convert.ToInt32(eventnbr);
          sql_args["status"] = 3;

        }
        else if (owner_type.ToUpper() == "TRANSACTION")
        {
          selSql = @"SELECT PKID as OWNER FROM TG_TRAN_DATA WHERE DEPFILENBR=@filenbr AND DEPFILESEQ=@seqnbr AND EVENTNBR=@eventnbr AND TRANNBR=@trannbr AND VOIDDT IS NOT NULL";
          sql_args["filenbr"] = Convert.ToInt32(fileNbr);
          sql_args["seqnbr"] = Convert.ToInt32(seqNbr);
          sql_args["eventnbr"] = Convert.ToInt32(eventnbr);
          sql_args["trannbr"] = Convert.ToInt32(transnbr);

        }
        else if (owner_type.ToUpper() == "TENDER")
        {
          selSql = @"SELECT PKID as OWNER FROM TG_TENDER_DATA WHERE DEPFILENBR=@filenbr AND DEPFILESEQ=@seqnbr AND EVENTNBR=@eventnbr AND TNDRNBR=@tndrnbr AND VOIDDT IS NOT NULL";
          sql_args["filenbr"] = Convert.ToInt32(fileNbr);
          sql_args["seqnbr"] = Convert.ToInt32(seqNbr);
          sql_args["eventnbr"] = Convert.ToInt32(eventnbr);
          sql_args["tndrnbr"] = Convert.ToInt32(transnbr);

        }
      }
      else if (note_type.ToUpper() == "REVERSALREASON")
      {
        if (owner_type.ToUpper() == "EVENT")
        {
          List<iPayFileInfo> lstInfo = GetReversalEvent(conString, note_info);

          selSql = @"SELECT UNIQUEID as OWNER FROM TG_PAYEVENT_DATA WHERE ";

          int cnt = 0;
          foreach (iPayFileInfo payfileInfo in lstInfo)
          {
            string paraFileNbr = string.Format("filenbr_{0}", cnt);
            string paraSeqNbr = string.Format("seqnbr_{0}", cnt);
            string paraEventNbr = string.Format("eventnbr_{0}", cnt);
            string sqlWhere = string.Format("(DEPFILENBR=@{0} AND DEPFILESEQ=@{1} AND EVENTNBR=@{2}) OR ", paraFileNbr, paraSeqNbr, paraEventNbr);
            sql_args.Add(paraFileNbr, payfileInfo.depFileNbr);
            sql_args.Add(paraSeqNbr, payfileInfo.depFileSeq);
            sql_args.Add(paraEventNbr, payfileInfo.eventNbr);

            selSql += sqlWhere;
            cnt++;
          }

          if (selSql.EndsWith("OR "))
          {
            selSql = selSql.Remove(selSql.LastIndexOf("OR "));
          }
        }
      }
      

      try
      {
        using (SqlConnection connection = new SqlConnection(conString))
        {
          connection.Open();

          using (SqlCommand command = ParamUtils.CreateParamCommand(connection, selSql, sql_args))      // Bug 17639: Used parameterized query
          {
            command.Connection = connection;
            command.CommandType = CommandType.Text;

            using (SqlDataReader dataReader = command.ExecuteReader())
            {
             
              while (dataReader.Read())
              {
                // get the results of each column
                owner = (int)dataReader["OWNER"];
                lstOwner.Add(owner);

              }

            }
          }
        }
      }
      catch (Exception ex)
      {
        string baseMsg = "Cannot Get Unique ID for voided event, transaction, or tender:";
        Logger.LogError(baseMsg + ex.Message, "VoidNote logging GetOwner");

        // Bug 17849
        Logger.cs_log(baseMsg + ex.ToMessageAndCompleteStacktrace());
        return lstOwner;
      }

      return lstOwner;

    }

    protected static string FormatNoteData(GenericObject note_info)
    {
      string void_reason = (string)note_info.get("VOIDREASON","");
      string void_comment = (string)note_info.get("VOID_COMMENT","");
      GenericObject attachment = (GenericObject)note_info.get("ATTACHMENT", c_CASL.c_instance("GEO"));
      //Bug 24762 SX add void signature 
      string ATTACHMENT_FORMAT = (string)attachment.get("FORMAT", "");
      string ATTACHMENT_CONTENT = "";
      if (attachment.has("CONTENT"))
       ATTACHMENT_CONTENT = Convert.ToBase64String((byte[])attachment.get("CONTENT"));

      NOTES notes = new NOTES();
      notes.lstNote = new List<NOTE>();
      NOTE a_note = new NOTE();

      a_note.reasoncode = void_reason.Split('|').Length == 2 ? void_reason.Split('|')[0] : "";
      a_note.reasondesc = void_reason.Split('|').Length == 2 ? void_reason.Split('|')[1] : "";
      a_note.comment = void_comment;
      //Bug 24762 SX add void signature 
      if (ATTACHMENT_CONTENT != "")
      {
        a_note.ATTACHMENT = new ATTACHMENT();
        a_note.ATTACHMENT.format = ATTACHMENT_FORMAT;
        a_note.ATTACHMENT.content = ATTACHMENT_CONTENT;
      }
      notes.lstNote.Add(a_note);

      var ns = new XmlSerializerNamespaces();
      ns.Add("", "");
      var stringwriter = new System.IO.StringWriter();
      var serializer = new XmlSerializer(typeof(NOTES));
      serializer.Serialize(stringwriter, notes, ns);
      string sXML = stringwriter.ToString();
      //Logger.LogTrace(sXML,"Info");
      return sXML;
    }

    public static GenericObject GetNoteData(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);

      GenericObject list_note_data = new GenericObject();
      try
      {
        GenericObject note_info = (GenericObject)geo_args.get("note_info");
        GenericObject dbConnection = (GenericObject)geo_args.get("db_connection");
        if (dbConnection == null)
        {
          Logger.LogError("db_connection not in params passed to WriteNotesRecord", "Void Notes WriteNotesRecord");
          return null;
        }
        String conString = (String)dbConnection.get("db_connection_string");

        List<int> lstOwner = GetOwner(conString, note_info);

        if (lstOwner.Count == 0)
          return list_note_data;

        string owner_type = note_info.get("OWNER_TYPE") as string;
        string note_type = note_info.get("NOTE_TYPE") as string;

        HybridDictionary sql_args = new HybridDictionary();
        //string selSql = @"SELECT note_data FROM TG_NOTE_DATA WHERE OWNER=@owner AND OWNER_TYPE=@owner_type AND NOTE_TYPE=@note_type";
        string selSql = @"SELECT note_data FROM TG_NOTE_DATA WHERE OWNER_TYPE=@owner_type AND NOTE_TYPE=@note_type AND (";
        sql_args["owner_type"] = owner_type;
        sql_args["note_type"] = note_type;

        int cnt = 0;
        foreach (int owner in lstOwner)
        {
          string owner_name = string.Format("owner_{0}", cnt);
          string sqlOwner = string.Format("OWNER=@{0} OR ", owner_name);
          sql_args.Add(owner_name, owner);

          selSql += sqlOwner;
          cnt++;
        }

        if (selSql.EndsWith("OR "))
        {
          selSql = selSql.Remove(selSql.LastIndexOf("OR "));
        }
        selSql += ")";

        using (SqlConnection connection = new SqlConnection(conString))
        {
          connection.Open();

          using (SqlCommand command = ParamUtils.CreateParamCommand(connection, selSql, sql_args))      // Bug 17639: Used parameterized query
          {
            command.Connection = connection;
            command.CommandType = CommandType.Text;

            using (SqlDataReader dataReader = command.ExecuteReader())
            {
              
              while (dataReader.Read())
              {
                GenericObject note_data = new GenericObject();
                // get the results of each column
                string xml_note_data = (string)dataReader["note_data"];

                NOTES notes = ParseXMLStringToNoteData(xml_note_data);

                //note_data["FILEID"] = notes.lstNote[0].
                note_data["REASONCODE"] = notes.lstNote[0].reasoncode;
                note_data["REASONDESC"] = notes.lstNote[0].reasondesc;
                note_data["COMMENT"] = notes.lstNote[0].comment;
                list_note_data.insert(note_data);
              }

            }
          }
        }

      }
      catch (Exception ex)
      {
        string baseMsg = "Cannot Get note data:";
        Logger.LogError(baseMsg + ex.Message, "VoidNote logging GetNoteData");

        // Bug 17849
        Logger.cs_log(baseMsg + ex.ToMessageAndCompleteStacktrace());
      }

      return list_note_data;
    }

    public static GenericObject ParseNoteData(params object[] args)
    {
      GenericObject geo_args = misc.convert_args(args);
      GenericObject note_data = new GenericObject();
      try
      {
        string xml_note_data = geo_args.get("xml_note_data").ToString();

        NOTES notes = ParseXMLStringToNoteData(xml_note_data);
        note_data["REASONCODE"] = notes.lstNote[0].reasoncode;
        note_data["REASONDESC"] = notes.lstNote[0].reasondesc;
        note_data["COMMENT"] = notes.lstNote[0].comment;
        //Bug 24762 SX 
        ATTACHMENT attachment = notes.lstNote[0].ATTACHMENT;
        if (attachment != null)
        { 
          GenericObject attgeo = c_CASL.c_instance("GEO");
          attgeo.set("FORMAT", attachment.format);
          attgeo.set("CONTENT", attachment.content);
          note_data["ATTACHMENT"] = attgeo;
        };

        #region remove_later
        /*NOTES notes = new NOTES();
        XmlSerializer serializer = new XmlSerializer(typeof(NOTES));
        XmlReaderSettings settings = new XmlReaderSettings();
        using (StringReader textReader = new StringReader(xml_note_data))
        {
          using (XmlReader xmlReader = XmlReader.Create(textReader, settings))
          {
            notes = (NOTES)serializer.Deserialize(xmlReader);

            note_data = new GenericObject();
            note_data["REASONCODE"] = notes.lstNote[0].reasoncode;
            note_data["REASONDESC"] = notes.lstNote[0].reasondesc;
            note_data["COMMENT"] = notes.lstNote[0].comment;

          }
        }*/
        #endregion
      }
      catch (Exception ex)
      {
        string baseMsg = "Error ParseNoteData: ";
        Logger.LogError(baseMsg + ex.Message, "Void Notes Recording");
        // Bug 17849
        Logger.cs_log(baseMsg + ex.ToMessageAndCompleteStacktrace());
      }
      return note_data;
    }

    protected static NOTES ParseXMLStringToNoteData(string xml_note_data)
    {
      NOTES notes = new NOTES();
      try
      {
        XmlSerializer serializer = new XmlSerializer(typeof(NOTES));
        XmlReaderSettings settings = new XmlReaderSettings();
        using (StringReader textReader = new StringReader(xml_note_data))
        {
          using (XmlReader xmlReader = XmlReader.Create(textReader, settings))
          {
            notes = (NOTES)serializer.Deserialize(xmlReader);
          }
        }
      }
      catch (Exception ex)
      {
        string baseMsg = "Error ParseNoteData: ";
        Logger.LogError(baseMsg + ex.Message, "Void Notes Recording");
        // Bug 17849
        Logger.cs_log(baseMsg + ex.ToMessageAndCompleteStacktrace());
      }
      return notes;
    }

    protected static bool IsValidReversalEvent(string conString, GenericObject note_info)
    {
      bool bRst = false;
      string owner_type = note_info.get("OWNER_TYPE") as string;
      string note_type = note_info.get("NOTE_TYPE") as string;
      string fileId = note_info.get("FILEID") as string;
      string eventnbr = note_info.get("EVENTNBR") as string;
      if (eventnbr == null)
        eventnbr = ((int)note_info.get("EVENTNBR")).ToString();
      string transnbr = note_info.get("TRANSNBR") as string;
      if (transnbr == null)
        transnbr = ((int)note_info.get("TRANSNBR")).ToString();
      string fileNbr = fileId.Substring(0, 7);
      string seqNbr = fileId.Substring(7);
      bool bReversed = (string)note_info.get("EVENTNBR") == "false" ? false : true; // meaning for event, transaction or tender that was reversed

      if (note_type.ToUpper() != "REVERSALREASON") // not applied
        return false;

      string selSql = "";
      HybridDictionary sql_args = new HybridDictionary();


      if (owner_type.ToUpper() == "EVENT")
      {
        // for new event 
        if (bReversed == false)
        {
          selSql = @"SELECT count(*) as CNT FROM TG_REVERSAL_EVENT_DATA WHERE DEPFILENBR=@filenbr AND DEPFILESEQ=@seqnbr AND EVENTNBR=@eventnbr";
          sql_args["filenbr"] = Convert.ToInt32(fileNbr);
          sql_args["seqnbr"] = Convert.ToInt32(seqNbr);
          sql_args["eventnbr"] = Convert.ToInt32(eventnbr);
        }
        else // for reversed event
        {
          selSql = @"SELECT count(*) as CNT FROM TG_REVERSAL_EVENT_DATA WHERE REVERSED_DEPFILENBR=@filenbr AND REVERSED_DEPFILESEQ=@seqnbr AND REVERSED_EVENTNBR=@eventnbr";
          sql_args["filenbr"] = Convert.ToInt32(fileNbr);
          sql_args["seqnbr"] = Convert.ToInt32(seqNbr);
          sql_args["eventnbr"] = Convert.ToInt32(eventnbr);
        }
      }
      else if (owner_type.ToUpper() == "TRANSACTION")
      {


      }
      else if (owner_type.ToUpper() == "TENDER")
      {

      }

      try
      {
        using (SqlConnection connection = new SqlConnection(conString))
        {
          connection.Open();

          using (SqlCommand command = ParamUtils.CreateParamCommand(connection, selSql, sql_args))      // Bug 17639: Used parameterized query
          {
            command.Connection = connection;
            command.CommandType = CommandType.Text;

            using (SqlDataReader dataReader = command.ExecuteReader())
            {
             
              while (dataReader.Read())
              {
                // get the results of each column
                bRst = (int)dataReader["CNT"] > 0 ? true : false;
              }

            }
          }
        }
      }
      catch (Exception ex)
      {
        string baseMsg = "IsValidReversalEvent failed";
        Logger.LogError(baseMsg + ex.Message, "VoidNote logging IsValidReversalEvent");

        // Bug 17849
        Logger.cs_log(baseMsg + ex.ToMessageAndCompleteStacktrace());
        return bRst;
      }
      return bRst;
    }

    protected static List<iPayFileInfo> GetReversalEvent(string conString, GenericObject note_info)
    {
      List<iPayFileInfo> lstInfo = new List<iPayFileInfo>();

      string owner_type = note_info.get("OWNER_TYPE") as string;
      string note_type = note_info.get("NOTE_TYPE") as string;
      string fileId = note_info.get("FILEID") as string;
      string eventnbr = note_info.get("EVENTNBR") as string;
      if (eventnbr == null)
        eventnbr = ((int)note_info.get("EVENTNBR")).ToString();
      string transnbr = note_info.get("TRANSNBR") as string;
      if (transnbr == null)
        transnbr = ((int)note_info.get("TRANSNBR")).ToString();
      string fileNbr = fileId.Substring(0, 7);
      string seqNbr = fileId.Substring(7);
      string sReversal_of = (string)note_info.get("REVERSAL_OF", ""); // meaning for old event, transaction or tender that was reversed
      bool bReversal_of = sReversal_of == "true" ? true : false;

      if (note_type.ToUpper() != "REVERSALREASON") // not applied
        return lstInfo;

      string selSql = "";
      HybridDictionary sql_args = new HybridDictionary();


      if (owner_type.ToUpper() == "EVENT")
      {
        // for new event 
        if (bReversal_of == false)
        {
          selSql = @"SELECT * FROM TG_REVERSAL_EVENT_DATA WHERE DEPFILENBR=@filenbr AND DEPFILESEQ=@seqnbr AND EVENTNBR=@eventnbr AND VOIDED=@voided";
          sql_args["filenbr"] = Convert.ToInt32(fileNbr);
          sql_args["seqnbr"] = Convert.ToInt32(seqNbr);
          sql_args["eventnbr"] = Convert.ToInt32(eventnbr);
          sql_args["voided"] = 0;
        }
        else // for reversal of event
        {
          selSql = @"SELECT * FROM TG_REVERSAL_EVENT_DATA WHERE REVERSED_FILENBR=@filenbr AND REVERSED_FILESEQ=@seqnbr AND REVERSED_EVENTNBR=@eventnbr AND VOIDED=@voided";
          sql_args["filenbr"] = Convert.ToInt32(fileNbr);
          sql_args["seqnbr"] = Convert.ToInt32(seqNbr);
          sql_args["eventnbr"] = Convert.ToInt32(eventnbr);
          sql_args["voided"] = 0;
        }
      }
      else if (owner_type.ToUpper() == "TRANSACTION")
      {


      }
      else if (owner_type.ToUpper() == "TENDER")
      {

      }

      try
      {
        using (SqlConnection connection = new SqlConnection(conString))
        {
          connection.Open();

          using (SqlCommand command = ParamUtils.CreateParamCommand(connection, selSql, sql_args))      // Bug 17639: Used parameterized query
          {
            command.Connection = connection;
            command.CommandType = CommandType.Text;

            using (SqlDataReader dataReader = command.ExecuteReader())
            {
              
              while (dataReader.Read())
              {
                iPayFileInfo fileInfo = new iPayFileInfo();
                // get the results of each column
                fileInfo.depFileNbr = (int)dataReader["DEPFILENBR"];
                fileInfo.depFileSeq = (int)dataReader["DEPFILESEQ"];
                fileInfo.eventNbr = (int)dataReader["EVENTNBR"];
                lstInfo.Add(fileInfo);
              }

            }
          }
        }
      }
      catch (Exception ex)
      {
        string baseMsg = "GetReversalEvent failed";
        Logger.LogError(baseMsg + ex.Message, "VoidNote logging GetReversalEvent");

        // Bug 17849
        Logger.cs_log(baseMsg + ex.ToMessageAndCompleteStacktrace());
        return lstInfo;
      }
      return lstInfo;
    }


  }

  [XmlRoot("NOTES")]
  public class NOTES
  {
    [XmlElement("NOTE")]
    public List<NOTE> lstNote { get; set; }
  }


  public class NOTE
  {
    [XmlElement("REASONCODE")]
    public string reasoncode { get; set; }
    [XmlElement("REASONDESC")]
    public string reasondesc { get; set; }
    [XmlElement("COMMENT")]
    public string comment { get; set; }
    //Bug 24762 SX
    [XmlElement("ATTACHMENT")]
    public ATTACHMENT ATTACHMENT { get; set; }
  }
  //Bug 24762 SX
  public class ATTACHMENT
  {
    [XmlElement("FORMAT")]
    public string format { get; set; }
    [XmlElement("CONTENT")]
    public string content { get; set; }

  }

  public class iPayFileInfo
  {
    public int depFileNbr { get; set; }
    public int depFileSeq { get; set; }
    public int eventNbr { get; set; }
    public int transNbr { get; set; }
  }

 
}