﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CASL_engine;
using System.Data;

using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Text.RegularExpressions;

namespace TranSuiteServices
{
    /// <summary>
    /// Move the complicated search query construction from the CASL code to C#
    /// Bug 22995
    /// </summary>
    class SearchUtils
    {

        /// <summary>
        /// Please refer to the to_string method in Sql.casl for an demonstration of the query parts and how they are formatted.
        /// 
        /// Special-case the custom field query: 
        /// Split the query into two instead of doing a join of the two custom field tables which was causing the slowdown
        /// Bug 22995
        /// 
        /// When the sql is sent in from CASL it has already go through the to_string process
        ///     SELECT m.DEPFILENBR, m.DEPFILESEQ, m.EVENTNBR, m.MOD_DT 
        ///        FROM TG_PAYEVENT_DATA m, TG_TRAN_DATA n, TG_DEPFILE_DATA f, TG_TRAN_DATA t 
        ///        WHERE m.DEPFILENBR=n.DEPFILENBR AND m.DEPFILESEQ=n.DEPFILESEQ AND m.EVENTNBR=n.EVENTNBR AND m.DEPFILENBR=f.DEPFILENBR AND m.DEPFILESEQ=f.DEPFILESEQ 
        ///          AND m.USERID=<var>userid</var> 
        ///          AND f.EFFECTIVEDT>='2018-03-12 00:00:00' 
        ///          AND f.EFFECTIVEDT<='2018-03-12 23:59:59' 
        ///          AND f.DEPTID=<var>deptid</var> AND NOT(t.ITEMIND='T') 
        ///          AND m.DEPFILENBR=t.DEPFILENBR AND m.DEPFILESEQ=t.DEPFILESEQ AND m.EVENTNBR=t.EVENTNBR 
        ///          AND t.TTID=<var>ttid</var> 
        ///          AND t.TTDESC=<var>transtype</var> 
        ///        ORDER BY m.MOD_DT, m.DEPFILENBR DESC, m.DEPFILESEQ DESC, m.EVENTNBR DESC
        ///
        /// So this has to be split and modified to be a UNION of two SELECTS (see bug 22995 for details)
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static GenericObject CreateSearchSQLFromQuery(params object[] args)
        {
            GenericObject geo = misc.convert_args(args);
            if (!geo.has("query_geo"))
            {
                Logger.cs_log("SearchUtils: CreateSearchSQL error: query_geo not found");
                throw new Exception("SearchUtils: CreateSearchSQL error: query_geo not found");
            }

            // This will allow use to get at
            // the various parts of the query without deconstructring the sql string as I did in CreateSearchSQL()
            GenericObject query_geo = geo.get("query_geo") as GenericObject;  


            // Get the custom field search string
            GenericObject criteria = geo.get("criteria") as GenericObject;
            string trans_misc = criteria.get("trans_misc", "") as string;
            if (string.IsNullOrWhiteSpace(trans_misc))
            {
                // So far this mehtod has not been generalized for any query
                throw new NotImplementedException("CreateSearchSQLFromQuery: not implemented for non-custom field searches");
            }

            // Process the return columns list
            // Note: this does not handle column names that are renamed with the AS keyword.
            // See Sql.casl to see how this is done but I think I would have to loop through the 
            // HashMap part of the GenericObject also.
            StringBuilder baseQuery = new StringBuilder().Append("SELECT DISTINCT ");
            GenericObject columns = query_geo.get("columns") as GenericObject;
            bool first = true;
            foreach (string column in columns.vectors)
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    baseQuery.Append(", ");
                }
                baseQuery.Append(column);
            }

            // Process the FROM table list
            // Note: this does not handle potential key/value pairs in the join_tables list.
            // See Sql.casl to see how this is done but I htink I would have to loop through the 
            // HashMap part of the GenericObject also.
            baseQuery.Append(" FROM ");
            GenericObject tables = query_geo.get("tables") as GenericObject;
            first = true;
            foreach (string table in tables.vectors)
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    baseQuery.Append(", ");
                }
                baseQuery.Append(table);
            }

            // Process the JOIN prediates.
            // This code section is lame and untested; please refer to Sql.casl to_string
            // Note: this does not handle potential key/value pairs in the join_tables list.
            // See Sql.casl to see how this is done but I htink I would have to loop through the 
            // HashMap part of the GenericObject also.
            if (query_geo.has("join_tables"))
            {
                GenericObject join_vector = query_geo.get("join_tables") as GenericObject;
                if (join_vector.vectors.Count > 0)
                {

                    baseQuery.Append(" JOIN ");

                    first = true;
                    foreach (string join in join_vector.vectors)
                    {
                        if (first)
                        {
                            first = false;
                        }
                        else
                        {
                            baseQuery.Append(", ");
                        }
                        baseQuery.Append(join);
                    }

                    GenericObject on_vector = query_geo.get("on") as GenericObject;
                    if (on_vector.vectors.Count > 0)
                    {
                        string on_value = on_vector.vectors[0] as string;
                        baseQuery.Append(' ').Append(on_value).Append(' ');
                    }
                }
            }

            // Process the WHERE predicates
            baseQuery.Append(" {0} WITH (NOLOCK) WHERE ");      // Leave the cust field type to later as a {0}
 
            GenericObject where_vector = query_geo.get("where") as GenericObject;
            first = true;
            foreach (string where in where_vector.vectors)
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    baseQuery.Append(" AND ");
                }
                baseQuery.Append(where);
            }

            //  Process the GROUP BY.  This is lame and untested
            if (query_geo.has("group_by"))
            {

                GenericObject group_by = query_geo.get("group_by") as GenericObject;
                first = true;
                foreach (string group in group_by.vectors)
                {
                    if (first)
                    {
                        first = false;
                        baseQuery.Append(" GROUP BY ");
                    }
                    else
                    {
                        baseQuery.Append(", ");
                    }
                    baseQuery.Append(group);
                }
            }

            // Process the ODER BY predicates
            StringBuilder orderByList = new StringBuilder();
            if (query_geo.has("order_by"))
            {

                GenericObject order_by = query_geo.get("order_by") as GenericObject;
                first = true;
                foreach (string order in order_by.vectors)
                {
                    if (first)
                    {
                        first = false;
                        orderByList.Append(" ORDER BY ");
                    }
                    else
                    {
                        orderByList.Append(", ");
                    }
                    orderByList.Append(order);
                }
            }

            // Now construct the two parts of the UNION
            StringBuilder grCustFieldPart = new StringBuilder();
            grCustFieldPart.Append(string.Format(baseQuery.ToString(), ",  GR_CUST_FIELD_DATA gr "));
            grCustFieldPart.Append(" AND m.DEPFILENBR=gr.DEPFILENBR AND m.DEPFILESEQ=gr.DEPFILESEQ AND m.EVENTNBR=gr.EVENTNBR ");      // Need to join with the GR Cust
            grCustFieldPart.Append(" AND gr.CUSTVALUE LIKE @transmisc ");                 // Add the cust field like

            // Second part, the tender custom fields
            StringBuilder custFieldPart = new StringBuilder();
            custFieldPart.Append(string.Format(baseQuery.ToString(), ",  CUST_FIELD_DATA cust "));
            custFieldPart.Append(" AND m.DEPFILENBR=cust.DEPFILENBR AND m.DEPFILESEQ=cust.DEPFILESEQ AND m.EVENTNBR=cust.EVENTNBR ");      // Need to join with the GR Cust
            custFieldPart.Append(" AND cust.CUSTVALUE LIKE @transmisc ");                 // Add the cust field like

            // And glue it all back together and add the UNION and the ODER BY
            StringBuilder unionizedSql = new StringBuilder();
            unionizedSql.Append(grCustFieldPart).Append(" UNION ").Append(custFieldPart).Append(orderByList);           


            // Get the parameter list in sql_params
            // We need to add to this and send it back
            GenericObject sql_params = geo.get("sql_params") as GenericObject;

            // Add the custom field query string into the parameter list
            sql_params.Add("transmisc", '%' + trans_misc + '%');

            GenericObject returnGeo = new GenericObject();

            returnGeo.Add("sql", unionizedSql.ToString());
            returnGeo.Add("sql_args", sql_params);

            return returnGeo;
        }


        /// <summary>
        /// Handle the complicated SQL modifications here in a hack-y sort of way.
        /// Although this is hacked (and initial) version of CreateSearchSQLFromQuery()
        /// it might actually be better because of its simplicity.
        /// Special-case the custom field query: 
        /// Split the query into two instead of doing a join of the two custom field tables
        /// Bug 22995
        /// 
        /// When the sql is sent in from CASL it has already go through the to_string process
        ///     SELECT m.DEPFILENBR, m.DEPFILESEQ, m.EVENTNBR, m.MOD_DT 
        ///        FROM TG_PAYEVENT_DATA m, TG_TRAN_DATA n, TG_DEPFILE_DATA f, TG_TRAN_DATA t 
        ///        WHERE m.DEPFILENBR=n.DEPFILENBR AND m.DEPFILESEQ=n.DEPFILESEQ AND m.EVENTNBR=n.EVENTNBR AND m.DEPFILENBR=f.DEPFILENBR AND m.DEPFILESEQ=f.DEPFILESEQ 
        ///          AND m.USERID=<var>userid</var> 
        ///          AND f.EFFECTIVEDT>='2018-03-12 00:00:00' 
        ///          AND f.EFFECTIVEDT<='2018-03-12 23:59:59' 
        ///          AND f.DEPTID=<var>deptid</var> AND NOT(t.ITEMIND='T') 
        ///          AND m.DEPFILENBR=t.DEPFILENBR AND m.DEPFILESEQ=t.DEPFILESEQ AND m.EVENTNBR=t.EVENTNBR 
        ///          AND t.TTID=<var>ttid</var> 
        ///          AND t.TTDESC=<var>transtype</var> 
        ///        ORDER BY m.MOD_DT, m.DEPFILENBR DESC, m.DEPFILESEQ DESC, m.EVENTNBR DESC
        ///
        /// So this has to be split and modified to be a UNION of two SELECTS (see the bug for details)
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static GenericObject CreateSearchSQL(params object[] args)
        {
            GenericObject geo = misc.convert_args(args);

            if (!geo.has("criteria"))
            {
                Logger.cs_log("SearchUtils: CreateSearchSQL error: criteria not found");
                throw new Exception("SearchUtils: CreateSearchSQL error: criteria not found");
            }

            // Get the custom field search string
            GenericObject criteria = geo.get("criteria") as GenericObject;
            string trans_misc = criteria.get("trans_misc", "") as string;
            if (string.IsNullOrWhiteSpace(trans_misc))
            {
                // So far this mehtod has not been generalized for any query
                throw new NotImplementedException("CreateSearchSQL: not implemented for non-custom field searches");
            }

            // Get the raw SQL as show above
            string sql_string = geo.get("sql_string") as string;

            // Get the parameter list in sql_params
            // We need to add to this and send it back
            GenericObject sql_params = geo.get("sql_params") as GenericObject;

            // Not needed yet but may be in the future.  This will allow use to get at
            // the various parts of the query without deconstructring the sql string
            // GenericObject query_geo = geo.get("query_geo") as GenericObject;  

            // The first thing to do is peel off and save the ORDER BY clause
            string[] sqlParts = sql_string.Split(new string[]{ " ORDER BY " }, StringSplitOptions.None);
            string baseSql = sqlParts[0];
            string orderBy = sqlParts[1];

            // Now modify each SELECT query
            string grCustFieldPart = baseSql.Replace("SELECT", "SELECT DISTINCT").Replace(" WHERE ", ",  GR_CUST_FIELD_DATA gr  WITH (NOLOCK) WHERE ") 
                + " AND m.DEPFILENBR=gr.DEPFILENBR AND m.DEPFILESEQ=gr.DEPFILESEQ AND m.EVENTNBR=gr.EVENTNBR "      // Need to join with the GR Cust
                + " AND gr.CUSTVALUE LIKE @transmisc";                                                    // Add the cust field like

            string custFieldPart   = baseSql.Replace("SELECT", "SELECT DISTINCT").Replace(" WHERE ", ",  CUST_FIELD_DATA cust  WITH (NOLOCK) WHERE ")
                + " AND m.DEPFILENBR=cust.DEPFILENBR AND m.DEPFILESEQ=cust.DEPFILESEQ AND m.EVENTNBR=cust.EVENTNBR "      // Need to join with the  Cust
                + " AND cust.CUSTVALUE LIKE @transmisc";

            // And glue it backl together
            string unionizedSql = grCustFieldPart + " UNION " + custFieldPart + " ORDER BY " + orderBy;

            // Add the custom field query string into the parameter list
            sql_params.Add("transmisc", '%' + trans_misc + '%');

            GenericObject returnGeo = new GenericObject();

            returnGeo.Add("sql", unionizedSql);
            returnGeo.Add("sql_args", sql_params);

            return returnGeo;
        }

    }
}
