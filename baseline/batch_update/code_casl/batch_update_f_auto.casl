<!-- Bug 15464 - Batch Update Overhaul (Added Auto Run Batch Update) -->
<do>
<!-- ERROR 
BU-AUTO-1: No eligible file is found for automated batch update process.
BU-AUTO-2: No eligible system interface is found for automated batch update process.
BU-AUTO-3: Please configure an eligible user to run automated batch update process in iPayment Configuration App. 
BU-AUTO-4: Automated batch update do not support CORE file status other than accepted and balanced.
BU-AUTO-5: No email receiver is found for automated batch update error email notification.
BU-AUTO-6: No email receiver is found for automated batch update success email notification.
BU-AUTO-7: Error Occur when generating batchupdate report for automated batch update email notification. 
BU-AUTO-11: Can not run ACH Batch update for CORE File with completed ACH batch update.  
--> 

<!-- called by external scheduler (window with schedule vb script) 
Batch_update.<autorun_batch_update/> -->
  Batch_update.<defmethod _name='autorun_batch_update'>
    _access="public"

    file_status="accepted"=<one_of> "accepted" "balanced"</one_of>
    system_interfaces="all"=String <!-- one or more system interface. format as comma delimited string -->
    system_interfaces_for_check_previous_batch_update=""=String <!-- comma delimited system interface class names e.g. "ACHBatchUpdate,sample" add flexiable check point if batch update will check previous batch, if complete for CORE File and System Interface will not run -->
    <!-- return empty string for success, or error message for failed -->
    <!-- END of ARGS -->
    <set> R = "" </set>
    <if>
      <or>
        file_status.<is> "accepted"</is>
        file_status.<is> "balanced"</is>
      </or>.<not/>
      <do>
        <set> bu_error_message = "Automated batch update does not support CORE file status other than accepted and balanced." </set>
        <set>
          bu_error =<error> code="BU-AUTO-4" message=bu_error_message throw=false </error>
        </set>
        .<handle_autorun_error> the_process_error=bu_error </handle_autorun_error>
        <return>
          bu_error.<default_description/>
        </return>
      </do>
    </if>

    <if>
      class.<is_type_for> _subject </is_type_for>
      <do>
        _local.<set_value>
          key="_subject"
          value=<Batch_update> login_id = Business.Misc.data.autobatchupdate_user </Batch_update>.<make_part/> <!--10743 & 7569 pass in login_id -->
        </set_value>
      </do>
    </if>

    <set>
      core_file_ids = Core_file.<get_ids_with> file_status=file_status </get_ids_with>
      system_interface_ids= <v/>
    </set>
    <if>
      system_interfaces.<is> ""</is>
      <set>
        system_interface_ids = <v/>
      </set>
      system_interfaces.<is> "all"</is>
      <do>
        <set>
          system_interfaces_options = .<find_systems_with_update_core_file_or_files/>
        </set>
        system_interfaces_options.<for_each>
          system_interface_ids.<insert> value.value.name </insert>
        </for_each>
      </do>
      else
      <set>
        system_interface_ids = system_interfaces.<split> "," </split>
      </set>
    </if>
    <set> bu_userid = Business.Misc.data.autobatchupdate_user </set>
    <set>
      bu_user = User.of.<get> bu_userid if_missing=false </get>
    </set>

    <!-- Add check point if COMPLETE SI and CORE FILE , do not pass to batch update process BUG#12798-->
    <if>
      system_interfaces_for_check_previous_batch_update.<is_not> ""</is_not>
      <do>
        <!-- only check ACH for CORE Files  -->
        <set>
          new_core_file_ids = core_file_ids.<copy/>
          removed_core_file_ids=<v/>
        </set>
        system_interface_ids.<for_each>

          <set> system_interface_id = value </set>
          <set>
            a_system_interface = System_interface.of.<get> system_interface_id  if_missing=false </get>
          </set>
          <if>
              <and>
                a_system_interface
                system_interfaces_for_check_previous_batch_update.<has>
                  a_system_interface._parent._name <!-- for testing, use ACHBatchUpdate,sample-->
                </has>
              </and>
            <do>
              core_file_ids.<for_each>
                <set> core_file_id=value </set>
                <set>
                  DEPFILENBR_and_DEPFILESEQ = Core_file.<get_DEPFILENBR_and_DEPFILESEQ>
                    core_file_id
                  </get_DEPFILENBR_and_DEPFILESEQ>
                </set>
                <!-- Bug 16540 UMN parameterize queries -->
                <set>
                  oCompleteFile = db_vector.<query_all>
                    db_connection= TranSuite_DB.Data.db_connection_info
                    sql_statement = "select * from tg_updatefile_data where DEPFILENBR=@filenbr AND DEPFILESEQ=@fileseq and INTERFACE_ID like @sysintid and status=@status"
                    sql_args = <GEO>
                      filenbr = DEPFILENBR_and_DEPFILESEQ.DEPFILENBR
                      fileseq = DEPFILENBR_and_DEPFILESEQ.DEPFILESEQ 
                      sysintid = <join> "%" system_interface_id "%" </join>
                      status = "COMPLETE"
                    </GEO>
                  </query_all>
                </set>
                <if>
                    oCompleteFile.<length/>.<more> 0 </more>
                  <do>
                    <set>
                      removed_core_file_ids.<insert> core_file_id </insert>
                    </set>
                    new_core_file_ids.<remove_value> core_file_id </remove_value>
                  </do>
                </if>
              </for_each> <!-- end of core_file_ids loop -->
            </do>
          </if>
        </for_each><!-- end of system_interface_ids loop -->
        <if>
          core_file_ids.<equal>
           new_core_file_ids 
            </equal>.<not/>
          <do>
            <set>
              bu_error_message = <join>
                "Warning: Can not run ACH Batch update for some CORE File with completed ACH batch update. CORE Files ("
                removed_core_file_ids.<join> separator=","</join>
                ")"
              </join>
            </set>
            <set>
              bu_error =<error> code="BU-AUTO-11" message=bu_error_message throw=false </error>
            </set>
            .<handle_autorun_error> the_process_error=bu_error </handle_autorun_error>
            <set> core_file_ids= new_core_file_ids</set>
          </do>
        </if>
      </do>
    </if>

    <!-- validate input for eligible CORE file, system interfce, file status, user -->
    <if>
      core_file_ids.<length/>.<is> 0 </is> <!-- no eligible file -->
      <do>
        <set> bu_error_message = "No eligible file is found for automated batch update process." </set>
        <set>
          bu_error =<error> code="BU-AUTO-1" message=bu_error_message throw=false </error>
        </set>
        .<handle_autorun_error> the_process_error=bu_error </handle_autorun_error>
        <return>
          bu_error.<default_description/>
        </return>
      </do>
      system_interface_ids.<length/>.<is> 0 </is> <!-- no eligible system interfaces -->
      <do>
        <set> bu_error_message = "No eligible system interface is found for automated batch update process." </set>
        <set>
          bu_error =<error> code="BU-AUTO-2" message=bu_error_message throw=false </error>
        </set>
        .<handle_autorun_error> the_process_error=bu_error </handle_autorun_error>
        <return>
          bu_error.<default_description/>
        </return>
      </do>
      bu_user.<not/> <!-- no user is configured -->
      <do>
        <set> bu_error_message = "Please configure an eligible user to run automated batch update process in iPayment Configuration App. " </set>
        <set>
          bu_error =<error> code="BU-AUTO-3" message=bu_error_message throw=false </error>
        </set>
        .<handle_autorun_error> the_process_error=bu_error </handle_autorun_error>
        <return>
          bu_error.<default_description/>
        </return>
      </do>
    </if>

    <!-- Bug 15947: FT: Pass Session ID for Activity Logger -->
    <!-- Bug 20435 UMN PCI-DSS don't leak real session ID in any logs -->
    <set> session_id = my.<get> "MaskedSessionID" if_missing = "" </get> </set>

    <set>
      R=<try>
        <do>
          .<set> core_file_ids = core_file_ids </set>
          .<run_batch_update>
            corefiles=core_file_ids
            system_interface_list=system_interface_ids
            user_name=bu_userid
            comment="AUTOMATED"
            force_batch_job="false" <!-- Bug 15464 DJD - Batch Update Overhaul -->
            session_id=session_id   <!-- Bug 15947 / 15486: FT: Pass session to Activity Logger -->
          </run_batch_update>
        </do>
        <if_error> the_error </if_error>
      </try>
    </set>

    <if>
      R.<is_a> error </is_a>
      <do>
        .<handle_autorun_error> the_process_error=R </handle_autorun_error>
        <return>
          R.<default_description/>
        </return>
      </do>
    </if>
    <!-- if successful -->
    .<handle_autorun_success/>

    <return> "" </return>
  </defmethod>


Batch_update.<defmethod _name='handle_autorun_error'> 
the_process_error=opt=error

 <!-- log error and send email to "Automated Batch Update Email Receiver" and "iPayment Critical Error Email Receiver"--> 
 <!-- batchupdate_email_receiver -->
 <set> R = ""</set>
 <log>  ACTION="AUTOBATCHUPDATE FAILED"
        ERROR_MESSAGE= <if> the_process_error.<is> opt </is> "Unknown Error" 
                            the_process_error.<is_a> error </is_a> the_process_error.<default_description/>  
                            else ""
                       </if>
 </log>
 <set> email_content = <GEO/> </set>
 <if> .<has> "UPDATE_ID"</has>
        <try>
          <do> 
           <set> bu_report = .<create_batchupdate_report/>.<to_htm_top/> </set> 
           email_content.<insert>  bu_report </insert>   
          </do>
        </try>
 </if>
 <set> email_receiver = <join> Business.Misc.data.batchupdate_email_receiver 
                               GEO.email_for_error
                               separator=","
                        </join> 
 </set>
 <if> email_receiver.<is> "" </is>
        <log>  ACTION="AUTOBATCHUPDATE"
               ERROR_MESSAGE= "No email receiver is found for automated batch update error email notification. (BU-AUTO-5)"
        </log>
       else 
        <send_email_notification>
  		      email_subject="Error Occurred"
		        module="iPayment Batch Update"
		        action="automated batch update process"
		        error = the_process_error
		        email_content=email_content
		        email_receiver=email_receiver
       </send_email_notification> 
 </if>
 <return> R </return>
</defmethod>

Batch_update.<defmethod _name='handle_autorun_success'>

 <!-- log error and send email to "Automated Batch Update Email Receiver" and "iPayment Critical Error Email Receiver"--> 
 <!-- batchupdate_email_receiver -->
  
  <!--
  
 <set> R = ""</set>
 <set> message = "" </set>
 <log>  ACTION="AUTOBATCHUPDATE SUCCESS"
        MESSAGE= message
 </log>
 <set> email_content = <GEO/> </set>
 <set> bu_report = .<create_batchupdate_report/>.<to_htm_top/> </set> 
 email_content.<insert>  bu_report </insert>
 <set> email_receiver = Business.Misc.data.batchupdate_email_receiver </set>
 <if> email_receiver.<is> "" </is>
        <log>  ACTION="AUTOBATCHUPDATE"
               ERROR_MESSAGE= "No email receiver is found for automated batch update success email notification. (BU-AUTO-6)"
        </log>
      else 
        <send_email_notification>
  		       email_subject=<join> "automated batch update process success at" Datetime.<now/> </join>
		         module="iPayment Batch Update"
		         action="Automated batch update process"
		         error = message
		         email_content=email_content
		         email_receiver=email_receiver
        </send_email_notification>
 </if>
 <return> R </return>
 
  -->

  <set> R = "" </set>
  .<build_and_send_bu_complete_email/>
  <return> R </return>
  
</defmethod>

Batch_update.<defmethod _name='create_batchupdate_report'>

  <set> bu_userid = Business.Misc.data.autobatchupdate_user </set>

  .<get_app/>.<set> a_user=Business.User.of.<get> bu_userid if_missing=error</get> </set> <!-- TODO: replace it with system user -->
  <set> si_report_app = GEO.group.report.<si_update_report/> </set>
  .<set_part> si_report_app = si_report_app </set_part>
  <set> top_app = .<get_app/> </set>
  <if> .<has> "UPDATE_ID" </has>.<not/>
    <do>
      <set> bu_error_message = "Error Occur when generating batchupdate report for automated batch update email notification. (BU-AUTO-7)" </set>
      <log>  ACTION="AUTOBATCHUPDATE"
             ERROR_MESSAGE= bu_error_message
      </log>
      <return> bu_error_message </return>
    </do>
  </if>
  <set> R = si_report_app.<create>
      report_name="System Interface Report"
      report_kind="summary"
      file_selection="on"
      core_files=.core_file_ids
      group_by=Group_by.of.date_SI
      updateIDs=<v> .UPDATE_ID </v>
    </create>
  </set>
  <return> R.<to_htm_top/> </return>

</defmethod>
</do>