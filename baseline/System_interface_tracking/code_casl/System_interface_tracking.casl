﻿<do>
  GEO.<defclass _name='System_interface_tracking'>
    Started=<GEO> id="STARTED" order=0 _label="Started" </GEO>
    PostedFailure=<GEO> id="POSTEDFAILURE" order=1 _label="Failed" </GEO>
    PostedSuccess=<GEO> id="POSTEDSUCCESS" order=2 _label="Successful" </GEO>
    Completed=<GEO> id="COMPLETED" order=3 _label="Completed" </GEO>
    PostedSuccessVoided=<GEO> id="POSTEDSUCCESSVOIDED" order=4 _label="Posted Success Voided" </GEO> <!-- Bug 15259 MJO -->
    DeviceError=<GEO> id="DEVICEERROR" order=5 _label="Device Error" </GEO> <!-- Bug 17505 MJO - Device failure before charge request sent -->
    RequestFailure=<GEO> id="REQUESTFAILURE" order=6 _label="Request Failed" </GEO> <!-- Bug #14588 Mike O - Added RequestFailure -->
    RequestTimeout=<GEO> id="REQUESTTIMEOUT" order=7 _label="Request Timeout" </GEO> <!-- Bug #14588 Mike O - Added RequestTimeout -->
  </defclass>
  
  <!-- Returns System_interface_tracking.Request object or false if no request was found -->
  System_interface_tracking.<defmethod _name='get_request'>
    a_core_item=req
    system=opt <!-- Bug 15370 MJO - Made opt in case we're trying to look it up -->
    is_cbc_item=false=Boolean <!-- Bug #10030 Mike O - Look for item with event nbr 0 -->
    get_sale_record=false <!-- Bug 17505 MJO - Get the last sale record instead of the last non-completed one -->
    use_point_placeholder=false <!-- Bug 22429 MJO -->
    
    <set>
      serial_id=a_core_item.<get> "_serial_id" if_missing=null </get> <!-- Bug 17439 MJO Get serial id -->
    </set>
    
    <!-- Bug 25590 MJO - Handle missing card number (for Paypal) -->
    <if>
      use_point_placeholder
      <set> account_number="XXXX" </set>
      a_core_item.<has> "credit_card_nbr" </has>
      <do>
        <set>
          account_length=a_core_item.credit_card_nbr.<length/>
        </set>

        <set>
          account_number=a_core_item.credit_card_nbr.<subvector>
            account_length.<minus> 4 </minus>
            account_length
          </subvector>
        </set>
        <!--Bug 10706 NJ-->
        <if>
          account_number.<length/>.<less> 1 </less>
          <set> account_number = "" </set>
        </if>
        <!--End Bug 10706 NJ-->
      </do>
      a_core_item.<is_a> Tender.Paypal </is_a>
      <set> account_number="XXXX" </set>
      else
      <set> account_number="" </set>
    </if>
    
    <set>
      a_core_event=a_core_item.<get_core_event/>
    </set>
    <set>
      a_core_file=a_core_event.<get_core_file/>
    </set>
    <set>
      filename=Core_file.<get_DEPFILENBR_and_DEPFILESEQ> a_core_file.id </get_DEPFILENBR_and_DEPFILESEQ>
    </set>
    
    <if>
      serial_id.<is> null </is>
      <do>
    <!-- Bug 15370 MJO - Made system opt -->
    <set>
      sql_args = <GEO>
        filenbr=filename.DEPFILENBR
        fileseq=filename.DEPFILESEQ
        eventnbr = <if>
          <or>
            a_core_event.id.<is> '' </is>
            is_cbc_item
            a_core_item.<get> "_event_nbr_zero" if_missing=false </get> <!-- Bug 15431 MJO - If event nbr is 0, look for it as such -->
          </or>
            0
          else
            a_core_event.id
        </if>
        account_number = account_number
        amount = a_core_item.total
        status = "COMPLETED"
        activity = "SALE"
      </GEO>
    </set>

    <if>
      system.<is_not> opt </is_not>
        sql_args.<set> system_type=system.name </set>
      else
        ""
    </if>
    <!-- End Bug 15370 MJO -->

    <set>
      records = db_vector.<query_all>
        sql_statement = <join>
          "SELECT * FROM TG_SYSTEM_INTERFACE_TRACKING WHERE DEPFILENBR=@filenbr AND DEPFILESEQ=@fileseq AND EVENTNBR=@eventnbr AND"
          " ACCOUNT_NUMBER=@account_number AND AMOUNT=@amount"
          <!-- Bug 15370 MJO - Made system opt -->
          <if>
            system.<is_not> opt </is_not>
              " AND SYSTEM_TYPE=@system_type "
            else
            ""
          </if>
          <!-- Bug 17505 MJO - If get_sale_record, get last sale record instead of last completed one -->
          <if>
            get_sale_record
              " AND ACTIVITY=@activity "
            else
              " AND STATUS != @status " <!-- Bug #9047 Mike O - Pretend completed requests don't exist -->
          </if>
          " ORDER BY LAST_DATETIME"
        </join>
        sql_args = sql_args
        db_connection = TranSuite_DB.Data.db_connection_info
      </query_all>
      last_record = false
    </set>

    <!-- Bug #9046 Mike O - Return the most recent matching record -->
    <if>
      records.<length/>.<more> 0 </more>
      records.<length/>.<for_each>
        <set>
          current_record=System_interface_tracking.Request.<from>
            records.<get> key </get>
          </from>
        </set>
        <if>
          <and>
            last_record
            <Datetime> current_record.last_datetime</Datetime>.<more>
              <Datetime> last_record.last_datetime </Datetime>
            </more>
            <!-- Bug 22429 MJO - Skip DeviceError -->
            current_record.status.id.<is_not> System_interface_tracking.DeviceError.id </is_not>
          </and>
          <set> last_record=current_record </set>

            last_record.<not/>
            <set> last_record=current_record </set>
          </if>
        </for_each>
    </if>
    <return> last_record </return>
      </do>
      else
      <!-- Bug 17439 MJO If serial id is available, query with just that -->
      <do>
        <set>
          records = db_vector.<query_all>
            db_connection = TranSuite_DB.Data.db_connection_info
            sql_statement = "SELECT * FROM TG_SYSTEM_INTERFACE_TRACKING WHERE SERIAL_ID=@serial_id"
            sql_args = <GEO> serial_id = serial_id </GEO>
          </query_all>
        </set>
        
        <if>
          records.<length/>.<more> 0 </more>
            System_interface_tracking.Request.<from> records.0 </from>
          else
            false
        </if>
      </do>
    </if>
  </defmethod>
  
  System_interface_tracking.<defmethod _name='start_request'>
    a_core_item=req
    action=null
    system=null
    request_xml=req
    merchant_key=req <!-- Bug 15417 MJO - Added merchant key -->
    <set>
      account_length=a_core_item.credit_card_nbr.<length/>
    </set>
    <set>
      account_number=a_core_item.credit_card_nbr.<subvector>
        account_length.<minus> 4 </minus>
        account_length
      </subvector>
    </set>
    <set>
      a_core_event=a_core_item.<get_core_event/>
    </set>
    <set>
      a_core_file=a_core_event.<get_core_file/>
      top_app=a_core_event._container.<get_app/>
    </set>
    <set>
      user_id=top_app.<get>
        "a_user" if_missing=<GEO/>
      </get>.<get> "id" if_missing="System" </get> <!-- Bug #9475 Mike O - Default user_id to "System" -->
      filename=Core_file.<get_DEPFILENBR_and_DEPFILESEQ> a_core_file.id </get_DEPFILENBR_and_DEPFILESEQ>
      eventnbr=<if>
        a_core_event.id.<is> '' </is> 0 else a_core_event.id
      </if> <!-- Bug #10030 Mike O -->
    </set>
    
    <!-- Bug 15431 MJO - Mark tender has having an event nbr of 0, so it's easy to find and update it later -->
    <if>
      eventnbr.<is> 0 </is>
      a_core_item.<set> _event_nbr_zero=true </set>
    </if>
    
    <!-- Bug 15479 MJO - New columns added -->
    <set>
      tender_type=a_core_item._parent._name
      is_fee=a_core_item.<has> "_fee_key" </has>
    </set>
    
    <!-- Bug 15259 MJO - Moved SQL into its own method -->
    <!-- Bug 17439 MJO Cache serial id for use later -->
    <set>
      serial_id=System_interface_tracking.<start_request_aux>
      user_id=user_id
      filename=filename
      eventnbr=eventnbr
      system_type=system.name
      action=action
      account_number=account_number
      amount=a_core_item.total
      comments=request_xml
      <!-- Bug 15479 MJO - New columns added -->
      tender_type=tender_type
      is_fee=is_fee
      merchant_key=merchant_key <!-- Bug 15417 MJO - Added merchant key -->
    </start_request_aux>
    </set>
    
    a_core_item.<set> _serial_id=serial_id </set>
    
    true
  </defmethod>
  
  <!-- Bug 15259 MJO - Moved SQL into its own method -->
  System_interface_tracking.<defmethod _name='start_request_aux'>
    user_id=req
    filename=req
    eventnbr=req
    system_type=req
    action=req
    account_number=req
    amount=req
    comments=req
    <!-- Bug 15479 MJO - New columns added -->
    tender_type=req
    is_fee=false
    merchant_key=req <!-- Bug 15417 MJO - Added merchant key -->
    
    <!-- Bug 15479 MJO - New columns added (TENDER_TYPE, IS_FEE) -->
    <set>
      result = db_vector.<query_all>
        db_connection = TranSuite_DB.Data.db_connection_info
        sql_statement = <join>
          separator = " "
          "INSERT INTO TG_SYSTEM_INTERFACE_TRACKING ("
          "USERID, INITIAL_DATETIME, INITIAL_SESSION_ID, "
          "DEPFILENBR, DEPFILESEQ, EVENTNBR, SYSTEM_TYPE, "
          "ACTIVITY, ACCOUNT_NUMBER, AMOUNT, STATUS, "
          "LAST_DATETIME, LAST_SESSION_ID, COMMENTS, TENDER_TYPE, IS_FEE, MERCHANT_KEY"
          ") "
          <!-- Bug 17439 MJO Return the serial id -->
          "OUTPUT INSERTED.SERIAL_ID "
          "VALUES (@user_id, @datetime, @session_id, "
          "@filenbr, @fileseq, @eventnbr, @system_type, "
          "@activity, @account_number, @amount, @status, "
          "@datetime, @session_id, @comments, @tender_type, @is_fee, @merchant_key)"
        </join>
        sql_args=<GEO>
          user_id=user_id
          datetime=Datetime.<current/>.<db_date/>
          session_id = <get_masked_session_id/> <!-- Bug 20435 UMN PCI-DSS don't leak real session ID in any logs -->
          filenbr=filename.DEPFILENBR
          fileseq=filename.DEPFILESEQ
          eventnbr=eventnbr
          system_type=system_type
          activity=action
          account_number=account_number
          amount=amount
          status=System_interface_tracking.Started.id
          comments=comments
          tender_type=tender_type
          is_fee=is_fee
          merchant_key=merchant_key <!-- Bug 15417 MJO - Added merchant key -->
        </GEO>
      </query_all>
    </set>
    
    <!-- Bug 17439 MJO Return the serial id -->
    result.0.SERIAL_ID
  </defmethod>
    
  System_interface_tracking.<defmethod _name='update'>
    a_core_item=req
    status=null
    reference=null
    response=null
    system=null
    activity=null
    is_cbc_item=false=Boolean <!-- Bug #10030 Mike O - Look for item with event nbr 0 -->

    <if> <!-- IPAY-1182: Do not record "C" by itself -->
      reference.<is> "C" </is>
      <set> reference = null </set>
    </if>
    <set>
      account_length=a_core_item.credit_card_nbr.<length/>
    </set>
    <set>
      account_number=a_core_item.credit_card_nbr.<subvector>
        account_length.<minus> 4 </minus>
        account_length
      </subvector>
    </set>
    <set>
      a_core_event=a_core_item.<get_core_event/>
    </set>
    <set>
      a_core_file=a_core_event.<get_core_file/>
      top_app=a_core_event._container.<get_app/>
    </set>
    <set>
      user_id=top_app.<get>
        "a_user" if_missing=<GEO/>
      </get>.<get> "id" if_missing="System" </get> <!-- Bug #9475 Mike O - Default user_id to "System" -->
      filename=Core_file.<get_DEPFILENBR_and_DEPFILESEQ> a_core_file.id </get_DEPFILENBR_and_DEPFILESEQ>
      eventnbr=<if>
        a_core_event.id.<is> '' </is> 0 else a_core_event.id
      </if> <!-- Bug #10030 Mike O -->
    </set>
    <set>
      status_id=<if>
        status.<is_not> null </is_not>
        status.id
      </if>
      <!-- Bug 15431 MJO - If the event nbr has changed from 0, update it in the table -->
      event_nbr_changed=<and>
        eventnbr.<is_not> 0 </is_not>
        a_core_item.<remove> "_event_nbr_zero" if_missing=false </remove>
      </and>
      serial_id=a_core_item.<get> "_serial_id" if_missing=null </get> <!-- Bug 17439 MJO Get serial id -->
    </set>

    <!-- Bug 15259 MJO - Moved SQL into its own method -->
    <!-- Bug 15874 MJO - If this fails and is in Business Center, try again with eventnbr 0 -->
    <try>
    System_interface_tracking.<update_aux>
      filename=filename
      is_cbc_item=is_cbc_item
      activity=activity
      eventnbr=eventnbr
      system_type=system.name
      account_number=account_number
      amount=a_core_item.total
      status_id=status_id
      reference=reference
      response=response
      event_nbr_changed=event_nbr_changed<!-- Bug 15431 MJO -->
      serial_id=serial_id <!-- Bug 17439 MJO -->
      tender_type=a_core_item._parent._name <!-- Bug 17878 MJO - Update the tender type -->
    </update_aux>
      <if_error>
        <if>
          <and>
            a_core_event._container.<get_app/>.<is_a> cbc </is_a>
            is_cbc_item.<not/>
          </and>
          System_interface_tracking.<update_aux>
            filename=filename
            is_cbc_item=true
            activity=activity
            eventnbr=eventnbr
            system_type=system.name
            account_number=account_number
            amount=a_core_item.total
            status_id=status_id
            reference=reference
            response=response
            serial_id=serial_id <!-- Bug 17439 MJO -->
            tender_type=a_core_item._parent._name <!-- Bug 17878 MJO - Update the tender type -->
          </update_aux>
          else
          <error>
            message="Failed to retrieve tracking record"
            code=200
            throw=true
            user_message=true <!--Bug 11329 NJ-show verbose always-->
          </error>
        </if>
      </if_error>
    </try>
  </defmethod>
  
  <!-- Bug 15259 MJO - Moved SQL into its own method -->
  System_interface_tracking.<defmethod _name='update_aux'>
    filename=req
    is_cbc_item=req
    activity=req
    eventnbr=req
    system_type =opt
    account_number=req
    amount=req
    status_id=null
    reference=null
    response=null
    event_nbr_changed=false <!-- Bug 15431 MJO -->
    serial_id=null <!-- Bug 17439 MJO -->
    tender_type=null <!-- Bug 17878 MJO - Update the tender type -->

    <!-- Bug 17439 MJO If not available, look up the serial id -->
    <if>
      serial_id.<is> null </is>
    <!-- Bug #13677 Mike O - Only update the event number of the latest entry with the proper session id -->
    <set>
      serial_id=System_interface_tracking.<get_latest_entry_id>
        filename=filename
        <!-- Bug 15431 MJO - If event nbr was 0, look for it as such -->
        eventnbr=<if>
          <or>
            is_cbc_item
            event_nbr_changed
          </or>
          0
          else
          eventnbr
        </if>
        activity=activity
        amount=amount <!-- Bug 15259 MJO - Added amount -->
        sessionid = <if>
          is_cbc_item <get_masked_session_id/> <!-- Bug 20435 UMN PCI-DSS don't leak real session ID in any logs -->
          else opt
        </if>
      </get_latest_entry_id>
    </set>
    </if>

    <!-- Bug 17439 MJO Query using only the serial id -->
    <set>
      sql_args=<GEO>
        eventnbr=eventnbr
        serial_id=serial_id
      </GEO>
    </set>
    
    <!-- Bug #10030 Mike O - Set correct event id if possible -->
    <if>
      <and>
        <or>
        is_cbc_item
          event_nbr_changed <!-- Bug 15431 MJO - Update if it was 0 before -->
        </or>
        eventnbr.<is_not> 0 </is_not>
      </and>
      <do>
        <!-- Bug 16540 UMN parameterize queries -->
        db_vector.<query_all>
          sql_statement = "UPDATE TG_SYSTEM_INTERFACE_TRACKING SET EVENTNBR=@eventnbr WHERE SERIAL_ID=@serial_id"
          sql_args = sql_args
          db_connection = TranSuite_DB.Data.db_connection_info
        </query_all>
      </do>
    </if>
    
    sql_args.<set>
      session_id = <get_masked_session_id/> <!-- Bug 20435 UMN PCI-DSS don't leak real session ID in any logs -->
      datetime=Datetime.<current/>.<db_date/>
    </set>
    <!-- End Bug #10030 Mike O -->
    <!-- End Bug #13677 Mike O -->
    
    sql_args.<remove> "eventnbr" </remove>
    
    <set>
      sql_statement=<join>
        separator = " "
        "UPDATE TG_SYSTEM_INTERFACE_TRACKING SET LAST_DATETIME=@datetime, LAST_SESSION_ID=@session_id"
        <if>
          status_id.<is_not> null </is_not>
          <do>
            sql_args.<set> status_id=status_id </set>
            ", STATUS=@status_id"
          </do>
        </if>
        <if>
          reference.<is_not> null </is_not>
          <do>
            sql_args.<set> reference=reference </set>
            ", REFERENCE=@reference"
          </do>
        </if>
        <if>          
          response.<is_not> null </is_not>
          <do>
            sql_args.<set> response=response </set>
            ", COMMENTS=@response"
          </do>
        </if>
        <!-- Bug 17878 MJO - Update the tender type -->
        <if>
          tender_type.<is_not> null </is_not>
          <do>
            sql_args.<set> tender_type=tender_type </set>
            ", TENDER_TYPE=@tender_type"
          </do>
        </if>
        " WHERE "
        <!-- Bug 15259 MJO - Added serial_id to where clause -->
        "SERIAL_ID=@serial_id"
      </join>
    </set>
    <!-- End Bug 17439 MJO -->
    
    <!-- End Bug #9047 -->
    db_vector.<query_all>
      db_connection = TranSuite_DB.Data.db_connection_info
      sql_statement=sql_statement
      sql_args=sql_args
    </query_all>
    <return> null </return>
  </defmethod>
  
  System_interface_tracking.<defclass _name='Request'>
    response=null
    status=null
    system=null
    filenbr=null
    fileseq=null
    eventnbr=null
    reference=null
    amount=null
    last_datetime=null
    comments=null
    record=null
    accountnbr=null <!-- Bug 15259 MJO - This was missing before -->
    <!-- Bug 15479 MJO - New fields added, and added missing activity field -->
    tender=null
    is_fee=null
    activity=null
    merchant_key=null <!-- Bug 15417 MJO - Added merchant key -->
  </defclass>
  
  System_interface_tracking.Request.<defmethod _name='from'>
    record=req
    System_interface_tracking.<Request>
      response=<GEO>
        body=record.<get> "COMMENTS" if_missing="" </get>
      </GEO>
      status=<if>
        record.STATUS.<is> System_interface_tracking.Started.id </is> System_interface_tracking.Started
        record.STATUS.<is> System_interface_tracking.PostedSuccess.id </is> System_interface_tracking.PostedSuccess
        record.STATUS.<is> System_interface_tracking.PostedFailure.id </is> System_interface_tracking.PostedFailure
        record.STATUS.<is> System_interface_tracking.Completed.id </is> System_interface_tracking.Completed
        record.STATUS.<is> System_interface_tracking.PostedSuccessVoided.id </is> System_interface_tracking.PostedSuccessVoided <!-- Bug 15259 MJO -->
        record.STATUS.<is> System_interface_tracking.RequestFailure.id </is> System_interface_tracking.RequestFailure <!-- Bug #14588 Mike O - Added RequestFailure -->
        record.STATUS.<is> System_interface_tracking.RequestTimeout.id </is> System_interface_tracking.RequestTimeout <!-- Bug #14588 Mike O - Added RequestTimeout -->
        record.STATUS.<is> System_interface_tracking.DeviceError.id </is> System_interface_tracking.DeviceError
        else null
      </if>
      system=record.<get> "SYSTEM_TYPE" if_missing="" </get>
      filenbr=record.<get> "DEPFILENBR" if_missing="" </get>
      fileseq=record.<get> "DEPFILESEQ" if_missing="" </get>
      eventnbr=record.<get> "EVENTNBR" if_missing="" </get>
      reference=record.<get> "REFERENCE" if_missing="" </get>
      amount=record.<get> "AMOUNT" if_missing="" </get>
      last_datetime=record.<get> "LAST_DATETIME" if_missing="" </get>
      comments=record.<get> "COMMENTS" if_missing="" </get>
      accountnbr=record.<get> "ACCOUNT_NUMBER" if_missing="" </get>
      <!-- Bug 15479 MJO - New field added, and added missing activity field -->
      tender=record.<get> "TENDER_TYPE" if_missing="" </get>
      <!-- Bug 23887 MJO - Support INT type for IS_FEE -->
      is_fee=<do>
        <set>
          is_fee=record.<get> "IS_FEE" if_missing=false </get>
        </set>

        <if>
          is_fee.<is> 1 </is>
          true
          is_fee.<is> 0 </is>
          false
          else
          is_fee
        </if>
      </do>
      activity=record.<get> "ACTIVITY" if_missing="" </get>
      merchant_key=record.<get> "MERCHANT_KEY" if_missing="" </get> <!-- Bug 15417 MJO - Added merchant key -->
      serial_id = record.<get> "SERIAL_ID" if_missing = "" </get> <!-- Bug 1384 MJO - Need serial id now -->
    </Request>
  </defmethod>
  
  <!-- Bug #9046 Mike O - Look up the last serial id so we know which record is the most recent -->
  System_interface_tracking.<defmethod _name='get_latest_entry_id'>
    filename=req
    eventnbr=req
    amount=req <!-- Bug #14905 Mike O - Include amount so we can differentiate between original tender and fee -->
    activity=null
    sessionid=opt <!-- Bug #13677 Mike O - Only use for Business Center -->
    
    <!-- Bug 16540 UMN parameterize queries -->
    <set>
      sql_args=<GEO>
        filenbr=filename.DEPFILENBR
        fileseq=filename.DEPFILESEQ
        eventnbr=eventnbr
        amount=amount <!-- Bug #14905 Mike O - Include amount so we can differentiate between original tender and fee -->
      </GEO>
    </set>

    <if>
      activity.<is> null </is>.<not/>
      sql_args.<set> activity=activity </set>
    </if>

    <!-- Bug 15259 MJO - Added amount -->
    <if>
      amount.<is> null </is>.<not/>
      sql_args.<set> amount=amount </set>
    </if>
    
    <!-- Bug #13677 Mike O - Added sessionid -->
    <if>
      sessionid.<is_not> opt </is_not>
      sql_args.<set> sessionid=sessionid </set>
    </if>

    <set>
      records=db_vector.<query_all>
        db_connection = TranSuite_DB.Data.db_connection_info
        sql_statement=<join>
          separator = " "
          "SELECT SERIAL_ID FROM TG_SYSTEM_INTERFACE_TRACKING AS s1 WHERE s1.SERIAL_ID=(SELECT MAX(SERIAL_ID) FROM TG_SYSTEM_INTERFACE_TRACKING"
          "AS s2 WHERE s2.DEPFILENBR=@filenbr AND s2.DEPFILESEQ=@fileseq AND s2.EVENTNBR=@eventnbr"
          <if>
            activity.<is_not> null </is_not>
            <do>
              sql_args.<set> activity=activity </set>
              "AND s2.ACTIVITY=@activity"
            </do>
          </if>
          <!-- Bug #13677 Mike O - Added sessionid -->
          <if>
            sessionid.<is_not> opt </is_not>
            <do>
              sql_args.<set> sessionid = sessionid </set>
              "AND s2.INITIAL_SESSION_ID=@sessionid"
            </do>
          </if>
          <!-- Bug 15259 MJO - Added amount -->
          <if>
            amount.<is> null </is>.<not/>
            " AND s2.AMOUNT=@amount "
            else
            ""
          </if>
            ")"
          " ORDER BY SERIAL_ID" <!-- Bug 15874 MJO- Make sure they're in order -->
        </join>
        sql_args=sql_args
      </query_all>
    </set>
    <if>
      records.<length/>.<is> 1 </is>
        records.0.SERIAL_ID
      <!-- Bug 15874 MJO - If there are more than 1 and we're in Business Center, use the last one -->
      <and>
        records.<length/>.<more> 1 </more>
        eventnbr.<is> 0 </is>
      </and>
      records.<last/>.SERIAL_ID
      else
        <error>
          message="Failed to retrieve tracking record"
          code=200
          throw=true
          user_message=true <!--Bug 11329 NJ-show verbose always-->
        </error>
    </if>
  </defmethod>
  
  System_interface_tracking.<defmethod _name='get_incomplete_requests'>
    core_file_id=req
    <set>
      filename=Core_file.<get_DEPFILENBR_and_DEPFILESEQ> core_file_id </get_DEPFILENBR_and_DEPFILESEQ>
    </set>
    <set>
      <!-- Bug 16540 UMN parameterize queries -->
      records = db_vector.<query_all>
        db_connection = TranSuite_DB.Data.db_connection_info
        sql_statement = <join>
          "SELECT * FROM TG_SYSTEM_INTERFACE_TRACKING WHERE DEPFILENBR=@filenbr AND DEPFILESEQ=@fileseq AND "
          <!-- Bug #9357 Mike O - Ignore postedfailure entries as well -->
          <!-- Bug 15259 MJO - Also ignore postedsuccessvoided -->
          "(STATUS!=@completed AND STATUS!=@postedfailure AND STATUS!=@postedsuccessvoided AND STATUS !=@deviceerror)"
        </join>
        sql_args=<GEO>
          filenbr=filename.DEPFILENBR
          fileseq=filename.DEPFILESEQ
          completed=System_interface_tracking.Completed.id
          postedfailure=System_interface_tracking.PostedFailure.id
          postedsuccessvoided=System_interface_tracking.PostedSuccessVoided.id <!-- Bug 15259 MJO - Added postedsuccessvoided -->
          deviceerror=System_interface_tracking.DeviceError.id
        </GEO>
      </query_all>
      requests=<v/>
    </set>
    records.<for_each>
      requests.<insert>
        System_interface_tracking.Request.<from> value </from>
      </insert>
    </for_each>
    <return> requests </return>
  </defmethod>

  <!-- Bug #9467 Mike O - Rewrote some of this to construct one big table instead of multiple smaller ones -->
  System_interface_tracking.<defmethod _name='display_incomplete_system_interface_requests'>
    _access="public"
    core_file_id=req
    email=false <!-- Bug 15229 MJO - If sending via e-mail, remove unnecessary elements -->

    <set>
      requests=System_interface_tracking.<get_incomplete_requests>
        core_file_id=core_file_id
      </get_incomplete_requests>
    </set>
    <if>
      requests.<length/>.<more> 0 </more>
      <do>
        <set>
          sorted_requests=requests.<sort_on>
            expressions=<v>
              <Expression> .status </Expression>
              <Expression> .system </Expression>
              <Expression> .eventnbr </Expression>
            </v>
          </sort_on>
        </set>
      </do>
    </if>
    <set>
      request_output=<TABLE/>
      current_status=null
      current_system=null
    </set>
    sorted_requests.<for_each>
      <set> request=value </set>
      <if>
        request.status.<is_not> current_status </is_not>
        <do>
          <set>
            current_status=request.status
            current_row=<TR>
                <TH> "Event Number" </TH>
                <TH> "System Interface" </TH>
                <TH> "Reference" </TH>
                <TH> "Account Number" </TH>
                <TH> "Amount" </TH>
                <TH> "Last Update" </TH>
                <TH> "Comments" </TH>
              </TR>
          </set>
          request_output.<insert>
            <TR>
              <TD>
                colspan=7
              <join> current_status._label " Requests" </join>
              </TD>
            </TR>
          </insert>
          request_output.<insert> current_row </insert>
        </do>
      </if>
      
      request_output.<insert>
        <TR>
          <TD> request.eventnbr </TD>
          <TD> request.system </TD>
          <TD> request.reference </TD>
          <!-- Bug #9415 Mike O - Show the last 4 digits of the CC# in the report -->
          <TD> request.accountnbr </TD>
          <TD>
            request.amount.<format_dollar/>
          </TD>
          <TD> request.last_datetime </TD>
          <TD> request.comments </TD>
        </TR>
      </insert>
    </for_each>
    <!-- RETURN -->
    <!-- Bug #11942 Mike O - Cleaned up this HTML, and made the report scrollable -->
    <HTML>
      <HEAD>
        <TITLE> "iPayment Report" </TITLE>
        <!-- Bug 14603 UMN fix issues with IE10 in quirks mode -->
        <if> Business.Misc.data.minify_mode
          <v>
            <LINK> href=<join> baseline_static_script "/business/include/ipayment.min.css" </join> type="text/css" rel="stylesheet" </LINK>
            <SCRIPT> src=<join> baseline_static_script "/business/include/ipayment.min.js" </join> </SCRIPT>
          </v>
        else
          <v>
            <LINK> href=<join> baseline_static "/business/include/main.css" </join>type="text/css" rel="stylesheet" </LINK>
            <SCRIPT> src=<join> baseline_static_script "/business/include/main.js" </join> </SCRIPT>
          </v>
        </if>
        <!-- Bug 15229 MJO - If sending via e-mail, remove unnecessary elements -->
        <if>
          email.<not/>
        <SCRIPT> "window.print();" </SCRIPT>
        </if>
      </HEAD>
      <BODY>
        style=<STYLE> overflow-y="scroll" </STYLE>
        <DIV class="no_wrapper">
          <STYLE>
            "DIV.middle_DIV {
            height: auto;
            }
            TABLE.middle_TABLE {
            height: auto;
            }"
          </STYLE>
          <TABLE class="s printable" casl_func="file_balancing.handle_incomplete_system_interface_requests" >
            <TR>
              <TD class="left"></TD>
              <TD class="middle">
                <stack> request_output </stack>
                <!-- Bug 19691 MJO - This won't work after printing in Edge, so just hide it -->
                <if>
                  <or>
                    my.browser.<equal> "Edge" </equal>
                    email
                  </or>.<not/>
                  <cbutton> label="Close" onclick="window.close();" </cbutton>
                </if>
              </TD>
              <TD class="right"/>
            </TR>
          </TABLE>
        </DIV>
      </BODY>
    </HTML>
  </defmethod>

  System_interface_tracking.<defmethod _name='has_incomplete_system_interface_requests'>
    core_file_id=req

    <set>
      requests=System_interface_tracking.<get_incomplete_requests>
        core_file_id=core_file_id
      </get_incomplete_requests>
    </set>

    <if>
      requests.<length/>.<more> 0 </more>
      true
      else
      false
    </if>

  </defmethod>

  <!-- Bug #9047 Mike O - Determine if a given request was voided by looking for a SALE/VOID pair with matching reference and date -->
  System_interface_tracking.<defmethod _name='request_was_voided'>
    a_core_item=req
    system=req

    <set>
      a_core_event=a_core_item.<get_core_event/>
    </set>
    <set>
      filename=Core_file.<get_DEPFILENBR_and_DEPFILESEQ>
        a_core_event.<get_core_file/>.id
      </get_DEPFILENBR_and_DEPFILESEQ>
    </set>

    <set>
      <!-- Bug 16540 UMN parameterize queries -->
      records=db_vector.<query_all>
        db_connection = TranSuite_DB.Data.db_connection_info
        sql_statement = <join>
          "SELECT * FROM TG_SYSTEM_INTERFACE_TRACKING WHERE ACTIVITY=@void AND DEPFILESEQ=@depfileseq AND DEPFILENBR=@depfilenbr AND "
          "DEPFILESEQ=@depfileseq AND EVENTNBR=@eventnbr AND SYSTEM_TYPE=@system_type AND ACCOUNT_NUMBER=@account_number AND AMOUNT=@amount"
        </join>
        sql_args=<GEO>
          void = "VOID"
          depfilenbr=Integer.<from> filename.DEPFILENBR </from>
          depfileseq=Integer.<from> filename.DEPFILESEQ </from>
          eventnbr=Integer.<from> <if> a_core_event.id.<is> '' </is> 0 else a_core_event.id </if> </from>
          system_type=system.name
          account_number=a_core_item.credit_card_nbr.<subvector>
            a_core_item.credit_card_nbr.<length/>.<minus> 4 </minus>
            a_core_item.credit_card_nbr.<length/>
          </subvector>
          amount=a_core_item.total
        </GEO>
      </query_all>
    </set>
    <if>
      records.<length/>.<is> 0 </is>
        false
      else
        true
    </if>
    
  </defmethod>
  
  <!-- Bug #13645 Mike O - Update amount when performing a partial auth -->
  System_interface_tracking.<defmethod _name='update_amount'>
    a_core_item=req
    system=req
    
    <set>
      account_length=a_core_item.credit_card_nbr.<length/>
      <!-- Bug 17128 UMN fix if_missing=opt bad code smell -->
      serial_id = <if> a_core_item.<has> "_serial_id" </has> a_core_item._serial_id else opt </if> <!-- Bug 17439 MJO Get serial id -->
    </set>
    <set>
      account_number=a_core_item.credit_card_nbr.<subvector>
        account_length.<minus> 4 </minus>
        account_length
      </subvector>
    </set>

    <set>
      a_core_event=a_core_item.<get_core_event/>
    </set>
    <set>
      a_core_file=a_core_event.<get_core_file/>
    </set>
    <set>
      filename=Core_file.<get_DEPFILENBR_and_DEPFILESEQ> a_core_file.id </get_DEPFILENBR_and_DEPFILESEQ>
    </set>
    
    <!-- Bug 17439 MJO Query using just the serial id, if available -->
    <set>
      sql_args=<GEO>
        amount=a_core_item.total
      </GEO>
    </set>

    <set>
      sql_statement=<join>
        "UPDATE TG_SYSTEM_INTERFACE_TRACKING "
        "SET "
        "AMOUNT=@amount"
        " WHERE "
        <if>
          serial_id.<is> opt </is>
          <do>
            sql_args.<set>
              session_id = <get_masked_session_id/> <!-- Bug 20435 UMN PCI-DSS don't leak real session ID in any logs -->
              filenbr=filename.DEPFILENBR
              fileseq=filename.DEPFILESEQ
              eventnbr=<if>
                a_core_event.id.<is> '' </is> 0 else a_core_event.id
              </if> <!-- Bug #10030 Mike O -->
              system_type=system.name
              account_number=account_number
            </set>
            
            <join>
        "DEPFILENBR=@filenbr AND DEPFILESEQ=@fileseq AND EVENTNBR=@eventnbr AND LAST_SESSION_ID=@session_id AND "
        "SYSTEM_TYPE=@system_type AND ACCOUNT_NUMBER=@account_number"
      </join>
          </do>
          else
          <do>
            sql_args.<set> serial_id=serial_id </set>
            "SERIAL_ID=@serial_id"
          </do>
        </if>
      </join>
    </set>
    <!-- End bug 17439 MJO -->

    db_vector.<query_all>
      sql_statement=sql_statement
      sql_args=sql_args
      db_connection=TranSuite_DB.Data.db_connection_info
    </query_all>
    
    null
  </defmethod>
  
  <!-- Bug 15259 MJO - Get all POSTEDSUCCESS records that don't have a matching iPayment tender -->
  System_interface_tracking.<defmethod _name='get_orphaned_records'>
    a_core_event=req
    get_started = false <!-- Bug 1384 MJO - Try to recover STARTED requests using last tran for Point -->
    
    <set>
      a_core_file=a_core_event.<get_core_file/>
    </set>
    <set>
      filename=Core_file.<get_DEPFILENBR_and_DEPFILESEQ> a_core_file.id </get_DEPFILENBR_and_DEPFILESEQ>
    </set>

    <set>
      sql_args = <GEO>
        filenbr = filename.DEPFILENBR
        fileseq = filename.DEPFILESEQ
        eventnbr = <if>
          a_core_event.id.<is> '' </is> 0 else a_core_event.id
        </if>
      </GEO>
    </set>

    <!-- Bug 26751 MJO - Never return "orphaned" records for incomplete BC events -->
    <if>
      sql_args.eventnbr.<is> 0 </is>
      <return> <v/> </return>
    </if>

    <set>
      where_clause=<v/>
    </set>

    <!-- Bug 20733 MJO - Parameterized -->
    a_core_event.<for_each>
      <if>
        <and>
          value.<is_a> Tender.Credit_card </is_a>
          value.<has> "auth_str" </has>
          <!-- Bug 25801 MJO - Could be an empty string -->
          value.auth_str.<is_not> null </is_not>
          value.auth_str.<length/>.<more> 0 </more>
        </and>
        <do>
          sql_args.<set_value> <join> "reference" key </join> value.auth_str </set_value>
          where_clause.<insert> <join> "REFERENCE != @reference" key </join> </insert>
        </do>
      </if>
    </for_each>
    
    <!-- Bug 1384 MJO - Try to recover STARTED requests using last tran for Point -->
    where_clause.<insert>
      <if>
        get_started
        "(STATUS = 'POSTEDSUCCESS' OR STATUS = 'STARTED')"
        else
        "STATUS = 'POSTEDSUCCESS'"
      </if>
    </insert>

    <set>
      sql_statement=<join>
        "SELECT * FROM TG_SYSTEM_INTERFACE_TRACKING "
        " WHERE "
        "DEPFILENBR=@filenbr AND DEPFILESEQ=@fileseq AND EVENTNBR=@eventnbr "
        <if>
          where_clause.<length/>.<is_not> 0 </is_not>
          <join>
            " AND "
            where_clause.<join>
              separator=" AND "
            </join>
          </join>
        </if>
        " ORDER BY DEPFILENBR, DEPFILESEQ, EVENTNBR, REFERENCE, ACCOUNT_NUMBER, AMOUNT, ACTIVITY"
      </join>
    </set>
    
    <set>
      records=db_vector.<query_all>
        sql_statement = sql_statement
        sql_args = sql_args
        db_connection = TranSuite_DB.Data.db_connection_info
      </query_all>
    </set>

    <set>
      orphaned_records=<v/>
      last_record=null
    </set>
    
    <!-- Bug 19554 MJO - Removed the chaotic mess in here: all returned records are orphans -->
    records.<for_each>
            orphaned_records.<insert>
        System_interface_tracking.Request.<from> value </from>
            </insert>
    </for_each>

    orphaned_records
  </defmethod>
  
  <!-- Bug 17505 MJO - Update last 4 digits of card number from "XXXX" to the correct one -->
  System_interface_tracking.<defmethod _name='update_account_nbr_point'>
    a_core_item=req
    system=req

    <set>
      account_length=a_core_item.credit_card_nbr.<length/>
    </set>
    <set>
      account_number=a_core_item.credit_card_nbr.<subvector>
        account_length.<minus> 4 </minus>
        account_length
      </subvector>
    </set>

    <set>
      a_core_event=a_core_item.<get_core_event/>
    </set>
    <set>
      a_core_file=a_core_event.<get_core_file/>
    </set>
    <set>
      filename=Core_file.<get_DEPFILENBR_and_DEPFILESEQ> a_core_file.id </get_DEPFILENBR_and_DEPFILESEQ>
    </set>

    <set>
      sql_args = <GEO>
        session_id = <get_masked_session_id/> <!-- Bug 20435 UMN PCI-DSS don't leak real session ID in any logs -->
        filenbr=filename.DEPFILENBR
        fileseq=filename.DEPFILESEQ
        eventnbr=<if>
          a_core_event.id.<is> '' </is> 0 else a_core_event.id
        </if> <!-- Bug #10030 Mike O -->
        system_type=system.name
        account_number=account_number
        four_xs = "XXXX"
      </GEO>
    </set>

    <set>
      sql_statement=<join>
        "UPDATE TG_SYSTEM_INTERFACE_TRACKING "
        "SET "
        "ACCOUNT_NUMBER=@account_number"
        " WHERE "
        "DEPFILENBR=@filenbr AND DEPFILESEQ=@fileseq AND EVENTNBR=@eventnbr AND LAST_SESSION_ID=@session_id AND "
        "SYSTEM_TYPE=@system_type AND ACCOUNT_NUMBER=@four_xs"
      </join>
    </set>

    db_vector.<query_all>
      sql_statement=sql_statement
      sql_args=sql_args
      db_connection=TranSuite_DB.Data.db_connection_info
    </query_all>

    <return> null </return>
  </defmethod>
  
  System_interface_tracking.<make_public>
    "display_incomplete_system_interface_requests"
  </make_public>
</do>