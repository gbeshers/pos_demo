﻿<do>
  <!--Bug 12757 NJ-Voids and Reversals in Management Report in Detail with Receipt Report mode-->
  <!-- Called by: report.management_summary_report.htm_inst in reports_management.casl -->
  report.<defclass _name='report_voids_reversals'>
    title = "Voids and Reversals"

  </defclass>

  report.report_voids_reversals.<defmethod _name='create'>
    _other_keyed = opt

    <set>
      create_args = <get_args/>
    </set>

    <set>
      a_report = <report.void_reversal_report/>
    </set>

    a_report.<set> _args = create_args </set>
    a_report.<set> report_criteria = create_args </set>

    <set>
      x = report.<core_file_selection> a_report </core_file_selection>
    </set>

    x
  </defmethod>

  report.<defclass _name='void_reversal_report'>

    title = "Voids and Reversals "

    _new_class.<set>

      item_class = GEO.<defclass _name='Void_reversal_record'>
        EVENTNBR = ""
        TTDESC = ""
        POSTDT = ""
        USERID = ""
        DEPFILENBR = ""
        DEPFILESEQ = ""
        TNDRDESC = ""
        RECEIPT_REF = ""
        TOTAL = ""
        COMPLETIONDT = ""
        VOIDDT = ""
        REVERSE_REF_NO = ""
        ORIGINAL_DATE = ""
Void_Type = ""
        Void_Code = ""
        Void_Reason = ""
        Void_Comment = ""
      </defclass>

    </set>

  </defclass>

  <!-- Bug 18932 UMN Remove f'ing copy (excuse my French, but I hate cloning code!) of report.create_where_clause -->

  <!--Bug 13934-code refactored - created separate functions for getting void and reversal data-->
  report.void_reversal_report.<defmethod _name='retrieve_report_data'>

    <!-- Bug 18932 UMN parameterize queries -->
    <set>
      sql_args = <GEO/>
    </set>

    <set>
      where_clause = .<create_where_clause> sql_args = sql_args </create_where_clause>
      items = <GEO/>
    </set>

    <set>
      voids = .<get_void_data>
        where_clause = where_clause
        sql_args = sql_args
      </get_void_data>

    </set>
    items.<insert_unique>
      _args = voids
    </insert_unique>

    <set>
      reversals = .<get_reversals>
        where_clause = where_clause
        sql_args = sql_args
      </get_reversals>
    </set>

    <if>
      reversals.<is_not> error </is_not>
      items.<insert_unique>_args = reversals </insert_unique>
    </if>
    <return> items </return>
  </defmethod>

  report.void_reversal_report.<defmethod _name='get_void_data'>
    where_clause = req
    sql_args = req

    <set>
      items = <GEO/>
    </set>

    <!-- transaction query -->
    <!-- Bug 18932 UMN parameterize queries; now returns GEO with query_string and sql_args -->
    <set>
      tran_void_geo = .<get_transaction_void_query>
        where_clause
      </get_transaction_void_query>
    </set>
    <merge_fields>
      to_obj = sql_args
      from_obj = tran_void_geo.sql_args
      set_only_w_no_value = true
    </merge_fields>

    <!-- Tender query -->
    <!-- Bug 18932 UMN parameterize queries; now returns GEO with query_string and sql_args -->
    <set>
      tender_void_geo = .<get_tender_void_query>
        where_clause
      </get_tender_void_query>
    </set>
    <merge_fields>
      to_obj = sql_args
      from_obj = tender_void_geo.sql_args
      set_only_w_no_value = true
    </merge_fields>

    <!-- union all of transaction and tender data since the columns returned are the same -->
    <set>
      data = db_vector.<query_all>
        db_connection = TranSuite_DB.Data.db_connection_info
        sql_statement = <join> tran_void_geo.query " UNION ALL " tender_void_geo.query </join>
        class = .item_class
        sql_args = sql_args <!-- Bug 18932 UMN parameterize queries -->
      </query_all>
    </set>

    data.<for_each>
      returns = .<vector_keys/>
      <!--set receipt reference-->
      value.<set>
        RECEIPT_REF = <join>
          Core_file.<make_id> value.DEPFILENBR value.DEPFILESEQ </make_id>
          "-"
          value.EVENTNBR
        </join>
      </set>

      <!--determine void is before/after event completion date-->
      <if>
        value.<has> "VOIDDT" </has>
        <if>
          value.<has> "COMPLETIONDT" </has>
          <do>
            <set>
              voiddt= Datetime.<from>
                value.<get> "VOIDDT" </get>
              </from>
              completiondt= Datetime.<from>
                value.<get> "COMPLETIONDT" </get>
              </from>
            </set>

            <if>
              <or>
                voiddt.<less> completiondt </less>
                voiddt.<equal> completiondt </equal>
              </or>
              value.<set> VOID_BEFORE = true </set>
            </if>
          </do>
        </if>
      </if>

      <!--Add minus to amount to indicate voids-->
      <if>
        value.<get> "TOTAL" </get>.<more> 0 </more>
        <do>
          <set>
            total = value.<get> "TOTAL" </get>
          </set>
          value.<set>
            TOTAL = <join> "-"  total </join>
          </set>
        </do>
      </if>

      items.<insert> value </insert>
    </for_each>

    <return> items </return>
  </defmethod>

  report.void_reversal_report.<defmethod _name='get_transaction_void_query'>
    file_where_clause = req

    <set>
      transaction_where_clause_vector = <v/>
      sql_query = Sql_query.<Select/>
	  tran_sql_query = Sql_query.<Select/>
    </set>

    <!-- Bug 18932 UMN parameterize queries -->
    sql_query.<insert_where_clause_param>
      where_str = "NOT(m.ITEMIND=@itemind)"
      where_param_key = "itemind"
      where_param_value = "T"
    </insert_where_clause_param>


    <set>
      transaction_where_clause = <join> _args = transaction_where_clause_vector separator = " AND " </join>
    </set>

    sql_query.<set_columns>
      "m.EVENTNBR"
      "m.DEPFILENBR"
      "m.DEPFILESEQ"
      "m.TTDESC"
      "m.POSTDT"
      "m.VOIDDT"
      "n.USERID"
      TOTAL = "m.TRANAMT"
      "n.COMPLETIONDT"
	  <!--Bug 25750 - Include Void Reason in legacy Management Report: MPT -->
      "o.OWNER_TYPE as e_voider"
      "o.NOTE_DATA.value ('(/NOTES/NOTE/REASONCODE)[1]', 'varchar(max)') as e_reasoncode"
      "o.NOTE_DATA.value('(/NOTES/NOTE/REASONDESC)[1]', 'varchar(max)') as e_reasondec"
      "o.NOTE_DATA.value('(/NOTES/NOTE/COMMENT)[1]', 'varchar(max)') as e_comment"
    </set_columns>

    sql_query.<set_tables> m = "TG_TRAN_DATA" </set_tables>
    sql_query.<set_join_tables>
      n = "TG_PAYEVENT_DATA"
      o = "TG_NOTE_DATA"
    </set_join_tables>


    sql_query.<insert_where_clauses> file_where_clause transaction_where_clause </insert_where_clauses>
    sql_query.<insert_where_clauses>
      <join>
        "( m.VOIDDT IS NOT NULL ) "
      </join>
    </insert_where_clauses>

    <!-- Bug 18932 UMN parameterize queries -->
    .<set_userid_param>
      sql_query = sql_query
      param_name = "n.USERID"
    </set_userid_param>

    sql_query.<insert_on_clauses>
      n = <join>
        "m.EVENTNBR = n.EVENTNBR" " AND "
        "m.DEPFILENBR = n.DEPFILENBR" " AND "
        "m.DEPFILESEQ = n.DEPFILESEQ"
      </join>
      o= <join>"o.OWNER = n.UNIQUEID OR o.OWNER = m.PKID"</join>
    </insert_on_clauses>
	

    <return>
      <GEO>
        query = sql_query.<to_string/>
        sql_args = sql_query.sql_args <!-- Bug 18932 UMN TODO: do we need to copy this? -->
      </GEO>

    </return>

  </defmethod>

  report.void_reversal_report.<defmethod _name = "get_tender_void_query">
    file_where_clause = req

    <set>
      tender_query = Sql_query.<Select/>
    </set>
    tender_query.<set_columns>
      "m.EVENTNBR"
      "m.DEPFILENBR"
      "m.DEPFILESEQ"
      "m.TNDRDESC AS TTDESC"
      "m.POSTDT"
      "m.VOIDDT"
      "n.USERID"
      TOTAL = "m.AMOUNT"
      "n.COMPLETIONDT"
	  <!--Bug 25750 - Include Void Reason in legacy Management Report: MPT -->
      "o.OWNER_TYPE as e_voider"
      "o.NOTE_DATA.value ('(/NOTES/NOTE/REASONCODE)[1]', 'varchar(max)') as e_reasoncode"
      "o.NOTE_DATA.value('(/NOTES/NOTE/REASONDESC)[1]', 'varchar(max)') as e_reasondec"
      "o.NOTE_DATA.value('(/NOTES/NOTE/COMMENT)[1]', 'varchar(max)') as e_comment"
    </set_columns>

    tender_query.<set_tables> m = "TG_TENDER_DATA" </set_tables>
    tender_query.<set_join_tables>
      n = "TG_PAYEVENT_DATA"
      o = "TG_NOTE_DATA"
    </set_join_tables>

    tender_query.<insert_on_clauses>
      n = <join>
        "m.EVENTNBR = n.EVENTNBR" " AND "
        "m.DEPFILENBR = n.DEPFILENBR" " AND "
        "m.DEPFILESEQ = n.DEPFILESEQ"
      </join>
	  <!--Bug 25750 - Include Void Reason in legacy Management Report: MPT -->
      o= <join>"o.OWNER = m.PKID OR o.OWNER = n.UNIQUEID "</join>
    </insert_on_clauses>

    <if>
      file_where_clause tender_query.<insert_where_clauses> file_where_clause </insert_where_clauses>
    </if>

    tender_query.<insert_where_clauses>
      <join>
        " (m.VOIDDT IS NOT NULL ) "
      </join>
    </insert_where_clauses>

    <!-- Bug 18932 UMN parameterize queries -->
    .<set_userid_param>
      sql_query = tender_query
      param_name = "n.USERID"
    </set_userid_param>

    tender_query.<insert_order_by>
      "m.VOIDDT DESC"
    </insert_order_by>

    <return>
      <GEO>
        query = tender_query.<to_string/>
        sql_args = tender_query.sql_args <!-- Bug 18932 UMN TODO: do we need to copy this? -->
      </GEO>
    </return>
  </defmethod>

  report.void_reversal_report.<defmethod _name='get_reversals'>
    where_clause = req
    sql_args = req <!-- Bug 18932 UMN parameterize queries -->

    <set>
      items = <GEO/>
    </set>

    <try>
      <!--1. Retrieve the post date for the the events in the files-->
      <set>
        query = <join>
          "SELECT DISTINCT DEPFILENBR, DEPFILESEQ, EVENTNBR, POSTDT FROM TG_TRAN_DATA m WHERE "
          where_clause
        </join>
      </set>

      <set>
        data1 = db_vector.<query_all>
          db_connection = TranSuite_DB.Data.db_connection_info
          sql_statement = query
          <!-- Bug 18932 UMN parameterize queries, Bug 4387 DJD Send empty sql_args if the sql statement has no parameters -->
          sql_args = <if>
            query.<contains> "@" </contains> sql_args else <GEO/>
          </if>
        </query_all>
      </set>

      where_clause.<replace> "DEPFILENBR" "REVERSED_FILENBR" </replace>
      where_clause.<replace> "DEPFILESEQ" "REVERSED_FILESEQ" </replace>

      <!--2. Retrieve transaction and tender reversal details-->
      <set>
        transaction_query = <join>
          <![CDATA[ SELECT DISTINCT t.DEPFILENBR,t.DEPFILESEQ,t.EVENTNBR,t.TTDESC,t.TRANAMT AS TOTAL,t.POSTDT, m.REVERSED_FILENBR, m.REVERSED_FILESEQ,
m.REVERSED_EVENTNBR, p.USERID FROM TG_REVERSAL_EVENT_DATA m
INNER JOIN TG_TRAN_DATA t ON m.DEPFILENBR = t.DEPFILENBR AND m.DEPFILESEQ = t.DEPFILESEQ AND m.EVENTNBR = t.EVENTNBR
INNER JOIN TG_PAYEVENT_DATA p ON p.DEPFILENBR = m.DEPFILENBR AND p.DEPFILESEQ = m.DEPFILESEQ AND p.EVENTNBR = m.EVENTNBR
      WHERE t.VOIDDT IS NULL AND t.TRANAMT != @amount AND NOT(t.ITEMIND=@itemind) AND
      ]]>  <!--BUG16026 -->
          where_clause
          <if>
            .users.<is_not> "all" </is_not>
            <join> " AND p.USERID = @userid" </join>
          </if>
        </join>
      </set>

      <set>
        tender_query = <join>
          <![CDATA[ SELECT d.DEPFILENBR,d.DEPFILESEQ,d.EVENTNBR,d.TNDRDESC AS TTDESC,d.AMOUNT AS TOTAL,d.POSTDT, m.REVERSED_FILENBR,
 m.REVERSED_FILESEQ,m.REVERSED_EVENTNBR,p.USERID FROM TG_REVERSAL_EVENT_DATA m
 INNER JOIN TG_TENDER_DATA d ON m.DEPFILENBR = d.DEPFILENBR AND m.DEPFILESEQ = d.DEPFILESEQ AND m.EVENTNBR = d.EVENTNBR
 INNER JOIN TG_PAYEVENT_DATA p ON p.DEPFILENBR = m.DEPFILENBR AND m.DEPFILESEQ = p.DEPFILESEQ AND m.EVENTNBR = p.EVENTNBR
      WHERE d.VOIDDT IS NULL AND d.AMOUNT != @amount AND ]]> <!--BUG16026 -->
          where_clause
          <if>
            .users.<is_not> "all" </is_not>
            <join> " AND p.USERID = @userid" </join>
          </if>
          <![CDATA[ ORDER BY t.DEPFILENBR,t.DEPFILESEQ,t.EVENTNBR]]>
        </join>
      </set>

      <!-- Bug 18932 UMN parameterize queries -->
      sql_args.<set>
        amount = 0
        itemind = "T"
      </set>
      <if>
        .users.<is_not> "all" </is_not>
        sql_args.<set>
          userid = .users.<to_sql_string_2/>
        </set>
      </if>

      <set>
        query = <join>
          transaction_query
          " UNION ALL "
          tender_query
        </join>
      </set>

      <set>
        data = db_vector.<query_all>
          db_connection = TranSuite_DB.Data.db_connection_info
          sql_statement = query
          class = .item_class
          sql_args = sql_args <!-- Bug 18932 UMN parameterize queries -->
        </query_all>
      </set>

      data.<for_each>
        returns = .<vector_keys/>
        <set> record = value </set>
        <!--set receipt reference-->
        record.<set>
          RECEIPT_REF = <join>
            Core_file.<make_id> record.DEPFILENBR record.DEPFILESEQ </make_id>
            "-"
            record.EVENTNBR
          </join>
        </set>
        <!--set reversal ref #-->
        record.<set>
          REVERSE_REF_NO = <join>
            Core_file.<make_id>
              record.REVERSED_FILENBR
              record.REVERSED_FILESEQ
            </make_id>
            "-"
            record.REVERSED_EVENTNBR
          </join>
        </set>
        <!--set original event date-->
        data1.<for_each>
          returns = "all"
          <set> event = value </set>

          <if>
            <and>
              event.DEPFILENBR.<equal> record.REVERSED_FILENBR </equal>
              event.DEPFILESEQ.<equal> record.REVERSED_FILESEQ </equal>
              event.EVENTNBR.<equal> record.REVERSED_EVENTNBR </equal>
            </and>
            record.<set> ORIGINAL_DATE = event.POSTDT </set>
          </if>
        </for_each>

        items.<insert> record </insert>
      </for_each>
      <if_error>
        <do>
          <log>
            the_error.<default_description/>
          </log>
          <return> the_error </return>
        </do>
      </if_error>
    </try>

    <return> items </return>
  </defmethod>
  <!--end bug 13934-->

  report.void_reversal_report.<defmethod _name='htm_report'>

    <framer>
      scrollable = false
      center = <framer>
        scrollable = false
        .<htm_inst_content>
          report_data = .top.<get> "_items" </get>
        </htm_inst_content>
      </framer>
    </framer>

  </defmethod>

  report.void_reversal_report.<defmethod _name='htm_inst_content'>
    report_data = req

    <set>
      voids_before = <v/>
      voids_after = <v/>
      reversals = <v/>
    </set>
    <!--split into 3 separate data sets-->
    report_data.<for_each>
      returns = "all"
      <if>
        value.<has> "VOIDDT" </has>
        <do>
          <if>
            value.<has> "VOID_BEFORE" </has>
            voids_before.<insert> value </insert>
            else
            voids_after.<insert> value </insert>
          </if>
        </do>
        value.<has> "REVERSE_REF_NO" </has>
        reversals.<insert> value </insert>
      </if>
    </for_each>

    <DIV>
      <TABLE  border = "0">
        <v>
          <TR>
            <TD>
              <BR/>
            </TD>
          </TR>
          <!--VOIDS BEFORE-->
          <if>
            voids_before.<length/>.<more>0</more>
            <do>
              <v>
                <TR class = "group_level_1">
                  <TD colspan = "10" class = "group_name file_filter_prompt">
                    "Voids before Receipting"
                  </TD>
                </TR>
                <TR class = "group_level_2">
                  <TH> "Post Date" </TH>
                  <TH> "User" </TH>
                  <TH> "Receipt Ref #" </TH>
                  <TH> "Description" </TH>
                  <TH> "Void Date" </TH>
                  <TH class = "tender_amount"> "AMOUNT" </TH>
				  <!--Bug 25750 - Include Void Reason in legacy Management Report: MPT -->
                  <TH> "Void Type" </TH>
                  <TH> "Void Code" </TH>
                  <TH> "Void Reason" </TH>
                  <TH> "Void Comment" </TH>
                </TR>

                voids_before.<for_each>
                  returns = "all"
                  <v>
                    <TR class = "item void">
                      <TD> value.POSTDT </TD>
                      <TD> value.USERID </TD>
                      <TD> value.RECEIPT_REF </TD>
                      <TD> value.TTDESC </TD>
                      <TD> value.VOIDDT </TD>
                      <TD class = "Decimal">
                        value.TOTAL.<format_dollar/>
                      </TD>
					  <!--Bug 25750 - Include Void Reason in legacy Management Report: MPT -->
                     <if>value.<has> "e_voider" </has>
                      <TD> value.e_voider</TD>
                      </if>
                      <if>value.<has> "e_reasoncode" </has>
                      <TD> value.e_reasoncode</TD>
                      </if>
                      <if>value.<has> "e_reasondec" </has>
                      <TD> value.e_reasondec</TD></if>
                      <if>value.<has> "e_comment" </has>
                      <TD> value.e_comment</TD></if>

                    </TR>
                  </v>
                </for_each>
              </v>
            </do>
          </if>
          <TR>
            <TD>
              <BR/>
            </TD>
          </TR>
          <!--VOIDS AFTER-->
          <if>
            voids_after.<length/>.<more>0</more>
            <do>
              <v>
                <TR class = "group_level_1">
                  <TD colspan = "10" class = "group_name file_filter_prompt">
                    "Voids after Receipting"
                  </TD>
                </TR>
                <TR class = "group_level_2">
                  <TH> "Post Date" </TH>
                  <TH> "User" </TH>
                  <TH> "Receipt Ref #" </TH>
                  <TH> "Description" </TH>
                  <TH> "Void Date" </TH>
                  <TH class = "tender_amount"> "AMOUNT" </TH>
				  <!--Bug 25750 - Include Void Reason in legacy Management Report: MPT -->
                  <TH> "Void Type" </TH>
                  <TH> "Void Code" </TH>
                  <TH> "Void Reason" </TH>
                  <TH> "Void Comment" </TH>
                </TR>

                voids_after.<for_each>
                  returns = "all"
                  <v>
                    <TR class = "item void">
                      <TD> value.POSTDT </TD>
                      <TD> value.USERID </TD>
                      <TD> value.RECEIPT_REF </TD>
                      <TD> value.TTDESC </TD>
                      <TD> value.VOIDDT </TD>
                      <TD class = "Decimal">
                        value.TOTAL.<format_dollar/>
                      </TD>
					  <!--Bug 25750 - Include Void Reason in legacy Management Report: MPT -->
                     <if>value.<has> "e_voider" </has>
                      <TD> value.e_voider</TD>
                      </if>
                      <if>value.<has> "e_reasoncode" </has>
                      <TD> value.e_reasoncode</TD>
                      </if>
                      <if>value.<has> "e_reasondec" </has>
                      <TD> value.e_reasondec</TD></if>
                      <if>value.<has> "e_comment" </has>
                      <TD> value.e_comment</TD></if>
                    </TR>
                  </v>
                </for_each>
              </v>
            </do>
          </if>
          <TR>
            <TD>
              <BR/>
            </TD>
          </TR>
          <!--REVERSALS-->
          <if>
            reversals.<length/>.<more>0</more>
            <do>
              <v>
                <TR class = "group_level_1">
                  <TD colspan = "10" class = "group_name file_filter_prompt">
                    "Reversals"
                  </TD>
                </TR>
                <TR class = "group_level_2">
                  <TH> "Reversal Date" </TH>
                  <TH> "User" </TH>
                  <TH> "Receipt Ref #" </TH>
                  <TH> "Description" </TH>
                  <TH> "Reversal Ref #" </TH>
                  <TH> "Post Date" </TH>
                  <TH class = "tender_amount"> "Amount" </TH>
                </TR>

                reversals.<for_each>
                  returns = "all"
                  <v>
                    <TR class = "item">
                      <TD> value.POSTDT </TD>
                      <TD> value.USERID </TD>
                      <TD> value.RECEIPT_REF </TD>
                      <TD> value.TTDESC </TD>
                      <TD> value.REVERSE_REF_NO </TD>
                      <TD> value.ORIGINAL_DATE </TD>
                      <TD class = "Decimal">
                        value.TOTAL.<format_dollar/>
                      </TD>
                    </TR>
                  </v>
                </for_each>
              </v>
            </do>
          </if>
        </v>
      </TABLE>
    </DIV>
  </defmethod>
</do>


