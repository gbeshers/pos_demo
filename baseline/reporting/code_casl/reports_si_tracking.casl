<do>

  <!--===========================
 SYSTEM INTERFACE TRACKING REPORT 
 LEVELS: 
      SUMMARY - contains: file number, user id, system type, activity, amount, status, comments (currently failure messages/codes)
              
=============================-->
  report.<defclass _name='si_tracking_report'>
    title = "Credit Card Tracker Report"

    <!-- populated in init -->
    <!-- file_date_from = "" = Type.date = Date TODO: DEFAULT DATE-->
    <!--file_date_to = "" = Type.date = Date-->
    update_status = "All" = <one_of> "All" "Success" "Failure" </one_of>

    <!-- Bug #9472 Mike O - Fix grouping -->
    group_by = <v> Group_by.of.date_ST </v> =
    <one_of>
      Group_by.of.date_ST
      Group_by.of.ST_date
    </one_of>

    report_kind = "Summary" = <one_of>
      <OPTION> value = "summary" "Summary" </OPTION>
    </one_of>
    header_suffix = " Report"

    file_status = "all"
    file_status_f_type = <one_of>
      <OPTION> value = "all" "All" </OPTION>
      <OPTION> value = "opened" "Opened" </OPTION>
      <OPTION> value = "locked" "Balanced" </OPTION>
      <OPTION> value = "accepted" "Accepted" </OPTION>
      <OPTION> value = "updated" "Updated" </OPTION>
    </one_of> <!-- BUG#-->

    payment_status = "all"
    payment_status_f_type = <multiple_of>
      <OPTION> value = "all" "All" </OPTION>
      <OPTION> value = "started" "Started" </OPTION>
      <OPTION> value = "posted_success" "Posted Success" </OPTION>
      <OPTION> value = "posted_failure" "Posted Failure" </OPTION>
      <OPTION> value = "completed" "Completed" </OPTION>
      <OPTION> value = "device_error" "Device Error" </OPTION>
      <OPTION> value = "request_failure" "Request Failure" </OPTION>
      <OPTION> value = "request_timeout" "Request Timeout" </OPTION>
    </multiple_of>

    system_type = "all"
    system_type_f_type = <multiple_of/>

    security_item_id = "333"
  </defclass>

  report.si_tracking_report.<defmethod _name='create'>
    updateIDs = opt
    _other_keyed = opt

    <set>
      create_args = <get_args/>
    </set>

    <set>
      a_report = <GEO/>
    </set>
    a_report.<set> _args = create_args </set>
    a_report.<set> report_criteria = create_args </set>

    a_report.<set_value> "_parent" report.si_tracking_summary_report </set_value>

    <!-- Bug#8343 ANU To resolve System interface report issue with group by criteria. -->

    <!-- Bug #9472 Mike O - Fix Grouping -->
    <if>
      a_report.group_by.<is> Group_by.of.date_ST </is>
        a_report.<set> group_by = Group_by.of.date_ST </set>
        
      a_report.group_by.<is> Group_by.of.ST_date </is>
        a_report.<set> group_by = Group_by.of.ST_date </set>
    </if>

    <!-- Bug#8343 ANU end -->
  
    <!-- report_date in MM/DD/YYYY 12h:mm AM/PM format Ex: 01/01/2005 2:34 PM -->
    a_report.<set>
      report_date = Datetime.<current/>.<get_formatted> "MM/dd/yyyy h:mm tt" </get_formatted>
    </set>
    a_report.<set>
      report_user = .<get_app/>.a_user
    </set>

    report.<core_file_selection> a_report </core_file_selection>
  </defmethod>


  <!--==============================SYSTEM INTERFACE TRACKING SUMMARY REPORT=========================-->
  report.<defclass _name='si_tracking_summary_report'>

    <!-- query criteria -->
    <!-- ui criteria -->
    title = "Credit Card Tracker " <!--Bug 11415 NJ Rectified name of report-->
    group_levels = <v/>

    group_by = <v> Group_by.of.date_ST </v> =
    <one_of>
      Group_by.of.date_ST
      Group_by.of.ST_date
    </one_of>

    _new_class.<set>
      item_class = GEO.<defclass _name='si_tracking_summary_record'>
        ref_num = ""
        USERID = ""
        ACCOUNT_NUMBER = ""
        LAST_DATETIME = ""
        SYSTEM_TYPE = ""
        ACTIVITY = ""
        TOTAL = "" = Decimal
        STATUS = ""
        COMMENTS = ""
        END_TIME = ""
        INTERFACE_ID = ""
        INTERFACE_DESC = ""
      </defclass>
      
      field_labels = <GEO>
        ref_num = "Reference #"
        USERID = "User ID"
        ACCOUNT_NUMBER = "Account #"
        "LAST_DATETIME"
        SYSTEM_TYPE = "System Type"
        ACTIVITY = "Activity"
        TOTAL = "Amount"
        STATUS = "Status"
        COMMENTS = "Comments"
      </GEO>

      field_ordering = <GEO>
        "ref_num"
        "USERID"
        "ACCOUNT_NUMBER"
        "LAST_DATETIME"
        "SYSTEM_TYPE"
        "ACTIVITY"
        "STATUS"
        "COMMENTS"
        "TOTAL"
      </GEO>
      fields_to_sum = opt <!--<v> "INTERFACE_ID"  </v>-->
    </set>
  </defclass>

  report.si_tracking_summary_report.<defmethod _name='retrieve_report_data'>

    <set>
      items = <GEO/>
      update_ids = <v/>
      sql_query = Sql_query.<Select/>
    </set>

    <set>
      <!-- Bug 18932 UMN parameterize queries -->
      file_where_clause = .<create_where_clause> sql_args = sql_query.sql_args </create_where_clause>
    </set>

    sql_query.<set_columns>
      "DEPFILENBR"
      "DEPFILESEQ"
      "EVENTNBR"
      "USERID"
      "ACCOUNT_NUMBER"
      "LAST_DATETIME"
      "SYSTEM_TYPE"
      "ACTIVITY"
      "TOTAL = m.AMOUNT"
      "STATUS"
      "COMMENTS"
    </set_columns>
    
    sql_query.<set_tables> m = "TG_SYSTEM_INTERFACE_TRACKING" </set_tables>
    sql_query.<insert_where_clauses> file_where_clause </insert_where_clauses>

    <set>
      status_query = <v/>
    </set>

    <!-- Bug 18932 UMN parameterize queries -->
    <if>
      .payment_status.<contains> "started" </contains>
        <do>
          status_query.<insert> "m.STATUS=@started" </insert>
          sql_query.sql_args.<set>
            started = "STARTED"
          </set>
        </do>
    </if>
    <if>
      .payment_status.<contains> "posted_success" </contains>
        <do>
          status_query.<insert> "m.STATUS=@postedsuccess" </insert>
          sql_query.sql_args.<set>
            postedsuccess = "POSTEDSUCCESS"
          </set>
        </do>
    </if>
    <if>
      .payment_status.<contains> "posted_failure" </contains>
        <do>
          status_query.<insert> "m.STATUS=@postedfailure" </insert>
          sql_query.sql_args.<set>
            postedfailure = "POSTEDFAILURE"
          </set>
        </do>
    </if>
    <if>
      .payment_status.<contains> "completed" </contains>
        <do>
          status_query.<insert> "m.STATUS=@completed" </insert>
          sql_query.sql_args.<set>
            completed = "COMPLETED"
          </set>
        </do>
    </if>
    <if>
      .payment_status.<contains> "device_error" </contains>
        <do>
          status_query.<insert> "m.STATUS=@deviceerror" </insert>
          sql_query.sql_args.<set>
            deviceerror = "DEVICEERROR"
          </set>
        </do>
    </if>
    <!-- Bug #14588 Mike O - Added RequestFailure and RequestTimeout -->
    <if>
      .payment_status.<contains> "request_failure" </contains>
        <do>
          status_query.<insert> "m.STATUS=@requestfailure" </insert>
          sql_query.sql_args.<set>
            requestfailure = "REQUESTFAILURE"
          </set>
        </do>
    </if>
    <if>
      .payment_status.<contains> "request_timeout" </contains>
      <do>
        status_query.<insert> "m.STATUS=@requesttimeout" </insert>
        sql_query.sql_args.<set>
          requesttimeout = "REQUESTTIMEOUT"
        </set>
      </do>
    </if>

    <if>
      status_query.<length/>.<more> 0 </more>
        sql_query.<insert_where_clauses>
          <concat>
            "("
            status_query.<join> separator = " OR " </join>
            ")"
          </concat>
        </insert_where_clauses>
    </if>

    <set>
      type_query = <v/>
    </set>

    <if>
      <!-- Bug #9472 Mike O - Make "All" work correctly -->
      <!-- Bug 18932 UMN parameterize queries -->
      <and>
        .system_type.<is_a> String </is_a>
        .system_type.<equal> "all" </equal>.<not/>
      </and>
        <do>
          type_query.<insert>
            <concat>
              "m.SYSTEM_TYPE=@system_type"
            </concat>
          </insert>
          sql_query.sql_args.<set>
            system_type = .system_type
          </set>
        </do>
        
      else
        .system_type.<for_each>
          <set> system_type_param = <join> "system_type" key </join> </set>
          type_query.<insert>
            <concat>
              "m.SYSTEM_TYPE=@"
              system_type_param
            </concat>
          </insert>
          sql_query.sql_args.<set_value>
            key = system_type_param
            value = value
          </set_value>
        </for_each>
    </if>

    <if>
      type_query.<length/>.<more> 0 </more>
        sql_query.<insert_where_clauses>
          <concat>
            "("
            type_query.<join> separator = " OR " </join>
            ")"
          </concat>
        </insert_where_clauses>
    </if>

    <try>
      <set>
        si_report_items = db_vector.<query_all>
          db_connection = TranSuite_DB.Data.db_connection_info
          sql_statement = sql_query.<to_string/>
          class = .item_class
          sql_args = sql_query.sql_args <!-- Bug 18932 UMN parameterize queries -->
        </query_all>
      </set>

      <if_error>
        <do>
          <log>
            ACTION = "DATABASE INQUIRY ERROR" SQL_STRING = sql_query.<to_string/> ERROR_MESSAGE = the_error.<default_description/>
          </log>
          <return> the_error </return>
        </do>
      </if_error>
    </try>
    
    si_report_items.<for_each>
      <set> a_si_report_item = value </set>
      <if>
        System_interface.of.<has> a_si_report_item.INTERFACE_ID </has>
          a_si_report_item.<set>
            INTERFACE_DESC = System_interface.of.<get> a_si_report_item.INTERFACE_ID </get>.description
          </set>
        else
          a_si_report_item.<set> INTERFACE_DESC = a_si_report_item.INTERFACE_ID </set>
      </if>
      a_si_report_item.<set>
        ref_num = <concat>
          Core_file.<make_id>
            DEPFILENBR = a_si_report_item.DEPFILENBR
            DEPFILESEQ = a_si_report_item.DEPFILESEQ
          </make_id>
          "-"
          a_si_report_item.EVENTNBR
        </concat>
      </set>

      <!-- Bug #9472 Mike O - Only use the date part -->
      a_si_report_item.<set> 
        LAST_DATETIME = Datetime.<from>
          a_si_report_item.LAST_DATETIME
        </from>.<get_formatted> "MM/dd/yyyy" </get_formatted> <!-- Bug #11414 Mike O - Make sure leading zeros are inserted -->
      </set>
      <!--Bug 24515 MPT Fortmating issue when comments column contain long text string and skip out the signaturedata string.-->
      <if>
        <and>
          a_si_report_item.<has>"COMMENTS"</has>
          a_si_report_item.COMMENTS.<has>"SIGNATUREDATA"</has>
        </and>
        <do>
          <set>comment_one = a_si_report_item.COMMENTS.<key_of> "SIGNATUREDATA" </key_of></set>
          <set>comment_two = a_si_report_item.COMMENTS.<key_of> "/SIGNATUREDATA"</key_of></set>
          a_si_report_item.<set> COMMENTS = 
              <concat> a_si_report_item.COMMENTS.<subvector> start = 0 end = comment_one</subvector>
                        a_si_report_item.COMMENTS.<subvector> start = comment_two</subvector>
              </concat>
         </set>
        </do>
      </if>
    </for_each>
    <return>si_report_items</return>
    
  </defmethod>

  GEO.group.report.si_tracking_report.<doc>
    <![CDATA[
Sample to: 
<set> si_tracking_app = GEO.group.report.<si_tracking_report/> </set>
si_tracking_app.<set> user = "demo" </set>
si_tracking_app.<make_part> in = my </make_part>
<set> R = si_tracking_app.<create> 
          report_name = "System Interface Tracking Report" 
          report_kind = "summary"  
          file_selection = "on"
          core_files = <v> "2008025001" </v> 
          group_by = Group_by.of.date_SI
          </create>
</set>  
R.<to_htm_top/>
]]>
  </doc>


</do>
