﻿/*
 * Bug 14580 DJM - This file was created to handle all Argos-based code,
 *                if possible try to put only Argos-based code in this file
 */

using CASL_engine;
using System;
using System.IO;
using System.Text;
using System.Net;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Web.Configuration;
using System.Web;

//Include this in namespace baseline unless otherwise requested.
namespace baseline
{
    
    public class SessionIDjson{
        public bool valid {get;set;}
        public Sessiondata data { get; set; }
    }
    public class Sessiondata{
        public string Version {get;set;}
        public List<Mappletsdata> Mapplets {get;set;}
        public string SessionId {get;set;}
    }
    public class Mappletsdata
    {
        public string GUID { get; set; }
        public string CommVersion { get; set; }
        public string Version { get; set; }
        public string Name { get; set; }
    }


    public class AuthenticationTokenjson
    {
        public bool valid { get; set; }
        public Authenticationdata data { get; set; }
    }
    public class Authenticationdata
    {
        public double Id { get; set; }
        public string Username { get; set; }
        public bool IsAdmin { get; set; }
    }


    public class SessionSecjson
    {
        public bool valid { get; set; }
        public SessionSecdata data { get; set; }
    }
    public class SessionSecdata
    {
        public string Token { get; set; }
    }


    public class ArgosSessionAuthenticate
    {
        //Convert the CASL variables to be handled in C# for session validation
        public static object CASLSessionAuthenticate(params object[] argPairs)
        {
            GenericObject args = misc.convert_args(argPairs);

            string username = args.get("username", null) as string;
            if (username == null)
            {
                return misc.MakeCASLError("Get Item Data Error", "91", "Username is null");
            }

            string credential = args.get("credential", "") as string;
            if (credential == null)
            {
                return misc.MakeCASLError("Get Item Data Error", "91", "Password is null");
                
            }

            string serverIP = args.get("serverIP", "") as string;
            if (serverIP == null)
            {
                return misc.MakeCASLError("Get Item Data Error", "91", "Server IP is null");
            }

            // BEGIN Bug 18018 -Test site crashing upon entering Report Generator
            object retObj = null;
            try
            {
                retObj = SessionAuthenticate(username, credential, serverIP);
            }
            catch (Exception e)
            {
                Logger.cs_log(string.Format("Argos Session Authenticate Error{0}Username: {1}{0}ServerIP: {2}{0}Error Details:{0}{3}{0}"
                    , Environment.NewLine, username, serverIP, e.ToMessageAndCompleteStacktrace()));
                return misc.MakeCASLError("Argos Session Authenticate Error", "91", "Argos session could not be authenticated.");
            }
            // END Bug 18018 -Test site crashing upon entering Report Generator

            return retObj; // Bug 18018
        }

        //Authenticate the session, retrieve the security token to be able to access Argos' web viewer while bypassing the login screen
        public static object SessionAuthenticate(string username, string credential, string serverIP)
        {

            //get session.setup json and store it in a string format
            WebRequest mySessionRequest = WebRequest.Create("https://" + serverIP + "/mw/Session.Setup?Version=4.0&JSONData={" + "\"Mapplets\"" + ":[{" + "\"Guid\"" + ":" + "\"B052A35E-DC3B-4283-B732-7BEE3B095C5E\"" + "," + "\"Version\"" + ":" + "\"4.0\"" + "}]}");
            HttpWebResponse SessionID = (HttpWebResponse)mySessionRequest.GetResponse();
            Stream responseStream = SessionID.GetResponseStream();
            StreamReader readStream = new StreamReader(responseStream, Encoding.UTF7);
            string strResponse = readStream.ReadToEnd();
            responseStream.Close();

            SessionIDjson session = new JavaScriptSerializer().Deserialize<SessionIDjson>(strResponse);

            //get session.authenticate json and store it in a string format
            WebRequest myAuthRequest = WebRequest.Create("https://" + serverIP + "/mw/Session.Authenticate?username=" + username + "&password=" + credential + "&sessionid=" + session.data.SessionId);
            HttpWebResponse AuthToken = (HttpWebResponse)myAuthRequest.GetResponse();
            Stream authresponseStream = AuthToken.GetResponseStream();
            StreamReader authreadStream = new StreamReader(authresponseStream, Encoding.UTF7);
            string authstrResponse = authreadStream.ReadToEnd();
            authresponseStream.Close();

            AuthenticationTokenjson authent = new JavaScriptSerializer().Deserialize<AuthenticationTokenjson>(authstrResponse);

            //get session.SecurityToken.Create json and store it in a string format
            WebRequest mySecToken = WebRequest.Create("https://" + serverIP + "/mw/Session.SecurityToken.Create?sessionid=" + session.data.SessionId);
            HttpWebResponse SecToken = (HttpWebResponse)mySecToken.GetResponse();
            Stream secresponseStream = SecToken.GetResponseStream();
            StreamReader secreadStream = new StreamReader(secresponseStream, Encoding.UTF7);
            string secstrResponse = secreadStream.ReadToEnd();
            secresponseStream.Close();

            SessionSecjson secur = new JavaScriptSerializer().Deserialize<SessionSecjson>(secstrResponse);

            return secur.data.Token;
            
        }   

        public static object CASLgetArgosURL(params object[] argPairs)
        {
            GenericObject args = misc.convert_args(argPairs);

            string username = args.get("username", null) as string;
            if (username == null)
            { return misc.MakeCASLError("Get Item Data Error", "91", "Username is null"); }
            
            string Argosusername = args.get("Argosusername", null) as string;
            if (username == null)
            { return misc.MakeCASLError("Get Item Data Error", "91", "Argosusername is null"); }
            
            string ArgosPass = args.get("ArgosPass", null) as string;
            if (username == null)
            { return misc.MakeCASLError("Get Item Data Error", "91", "ArgosPass is null"); }

            string serverIP = args.get("serverIP", "") as string;
            if (serverIP == null)
            {return misc.MakeCASLError("Get Item Data Error", "91", "Server IP is null");}
           
            string datablockID = args.get("datablockID", "") as string;
            if (datablockID == null)
            {return misc.MakeCASLError("Get Item Data Error", "91", "Datablock ID is null");}

            string securityToken = args.get("securityToken", "") as string;
            if (securityToken == null)
            {return misc.MakeCASLError("Get Item Data Error", "91", "Security token not found");}

            //MPT--Bug 23713 - Review/Remove ArgosInteractive System Interface
            //string interactiveID = args.get("interactiveID", "") as string;
            //if (interactiveID == null)
            //{ return misc.MakeCASLError("Get Item Data Error", "91", "Interactive URL not found"); }

            string reportID = args.get("reportID", "") as string;
            if (reportID == null)
            { return misc.MakeCASLError("Get Item Data Error", "91", "Report ID not found"); }

            string hash = args.get("hash", "") as string;

            return getArgosURL(username, serverIP, datablockID, securityToken, hash, reportID, Argosusername,ArgosPass);//MPT BUG 23713 Remove interactiveID
        }

        public HttpBrowserCapabilities Browser { get; set; }
        public static object getArgosURL(string username, string serverIP, string datablockID, string securityToken, string hash, string reportID,string Argosusername, string ArgosPass)//MPT BUG 23713 Remove interactiveID
        {
          // Bug 23065: Pass the iPayment userid to Argos via the command line
          // Get the current user ID. I cannot beleive that this does not exits already. Move this to misc?
          string userid = (CASLInterpreter.get_my() as GenericObject).get("login_user", "") as string;
          string encodedUserid = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(userid));  // Base64
          string escapedUserid = System.Uri.EscapeDataString(encodedUserid);             // Uuencoded

          // Bug 23897: Do not allow unlimited access in IE8 mode
          // Bug 23065: Pass in the encoded userid as the hash so it is sent in the incomming_hash_txt
          return "https://" + serverIP + "/Argos/AWV/#DataBlock=" + datablockID + "&Username=" + username + "&Token=" + securityToken + "&Hash=" + escapedUserid;
        }
  }
}