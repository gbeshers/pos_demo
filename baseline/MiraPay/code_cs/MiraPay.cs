﻿using System;
using System.Linq;
using System.Web;
using CASL_engine;
using System.Security.Cryptography;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Newtonsoft.Json;
using System.Timers;
using System.Net.Http;
using System.IO;

// Bug 26537 MJO - Eigen MiraPay support

namespace baseline
{
  public class MiraPay
  {
    private static object TransactionIdLock = new object();
    private static string PreviousTransactionId = "";

    private static ConcurrentBag<string> DoNotLogKeys = new ConcurrentBag<string>(new[] { "TR", "RT", "CH", "EC", "CA", "ES" });

    private static ConcurrentDictionary<string, GenericObject> TokenResponses = new ConcurrentDictionary<string, GenericObject>();

    private static string APPROVED = "AB";
    private static string RESPONSE_CODE = "RC";
    private static string RECEIPT_REF_NUM = "RN";
    private static string CARD_TYPE = "AT";
    private static string ISO_RESPONSE_CODE = "IR";
    private static string RECEIPT_MSG = "RM";
    private static string INVOICE_NUM = "IN";
    private static string APPROVAL_CODE = "AC";
    private static string EXPIRATION_DATE = "EX";
    private static string TERM_ID = "TI";
    private static string EIGEN_TOKEN = "TK";

    public static object fCASL_GetTransactionId(CASL_Frame frame)
    {
      GenericObject sysInt = frame.args.get("_subject") as GenericObject;

      return GetTransactionId(sysInt);
    }

    private static string GetTransactionId(GenericObject sysInt)
    {
      string transactionId;

      // It's unlikely, but make sure that there aren't duplicate ids
      lock (TransactionIdLock)
      {
        transactionId = DateTime.Now.ToString("yyMMddHHmmssfff");

        while (transactionId == PreviousTransactionId)
        {
          Thread.Sleep(1);
          transactionId = DateTime.Now.ToString("yyMMddHHmmssfff");
        }

        PreviousTransactionId = transactionId;
      }

      return transactionId;
    }

    public static object fCASL_GenerateMiraSecureMessage(CASL_Frame frame)
    {
      GenericObject sysInt = frame.args.get("system_interface") as GenericObject;
      string transactionId = frame.args.get("transaction_id") as string;
      string redirectURL = frame.args.get("redirect_url") as string;
      string total = (Convert.ToDecimal(frame.args.get("total")) * 100).ToString("0");

      return GenerateMiraSecureMessage(sysInt, transactionId, redirectURL, total);
    }

    private static GenericObject GenerateMiraSecureMessage(GenericObject sysInt, string transactionId, string redirectURL, string total)
    {
      string merchantId = sysInt.get("merchant_id").ToString();

      GenericObject ret = new GenericObject();

      string sessionId = CASLInterpreter.get_my().get("MaskedSessionID").ToString();

      TokenRequest payload = new TokenRequest(merchantId, total, transactionId, redirectURL, sessionId);

      Logger.LogInfo($"MiraPay system interface token request{Environment.NewLine}{JsonConvert.SerializeObject(payload, Newtonsoft.Json.Formatting.Indented)}", "MiraPay.GenerateMKEYSecure");

      string json = JsonConvert.SerializeObject(payload);

      // Bug IPAY-1543 MJO - Rewrote most everything below to utilize AES instead of the old MKEY approach
      if (!sysInt.has("mirasecure_secret_key"))
      {
        return CASL_error.create_str("MiraSecure secure key not set in MiraPay system interface").casl_object;
      }

      string secretKey = sysInt.get("mirasecure_secret_key").ToString();

      byte[] bUserPwdEncrypted = Convert.FromBase64String(secretKey);
      byte[] bUserPwdDecrypted = Crypto.symmetric_decrypt("data", bUserPwdEncrypted, "secret_key", Crypto.get_secret_key());
      secretKey = Encoding.UTF8.GetString(bUserPwdDecrypted);

      byte[] secretKeyBytes = Convert.FromBase64String(secretKey);
      byte[] encryptedJson;
      byte[] iv;

      using (AesManaged aes = new AesManaged())
      {
        iv = aes.IV;
        aes.Mode = CipherMode.CBC;

        ICryptoTransform encryptor = aes.CreateEncryptor(secretKeyBytes, aes.IV);

        using (MemoryStream ms = new MemoryStream())
        {
          using (CryptoStream cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
          {
            using (StreamWriter sw = new StreamWriter(cs))
              sw.Write(json);
            encryptedJson = ms.ToArray();
          }
        }
      }

      byte[] hash;

      using (HMACSHA256 hmac = new HMACSHA256(secretKeyBytes))
      {
        hash = hmac.ComputeHash(encryptedJson);
      }

      string identity = sysInt.get("mirasecure_identity").ToString();
      string message = $"{identity}:{Convert.ToBase64String(iv)}:{Convert.ToBase64String(encryptedJson)}:{Convert.ToBase64String(hash)}";

      ret.set(
        "message", message,
        "session_id", sessionId
      );

      return ret;
    }

    public static object fCASL_GetAuthority(CASL_Frame frame)
    {
      return GetAuthority();
    }

    private static string GetAuthority()
    {
      return HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    }

    public static object fCASL_StoreMiraSecureResponse(CASL_Frame frame)
    {
      GenericObject args = frame.args;
      GenericObject response = args.get("response") as GenericObject;
      GenericObject sysInt = args.get<GenericObject>("system_interface");

      return StoreMiraSecureResponse(response, sysInt);
    }

    private static object StoreMiraSecureResponse(GenericObject response, GenericObject sysInt)
    {
      try
      {
      	// Bug IPAY-1543 MJO - Rewrote most everything below to utilize AES instead of the old MKEY approach
        string fullMessage = response.get("data").ToString();

        string secretKey = sysInt.get("mirasecure_secret_key").ToString();

        byte[] bUserPwdEncrypted = Convert.FromBase64String(secretKey);
        byte[] bUserPwdDecrypted = Crypto.symmetric_decrypt("data", bUserPwdEncrypted, "secret_key", Crypto.get_secret_key());
        secretKey = Encoding.UTF8.GetString(bUserPwdDecrypted);
        byte[] secretKeyBytes = Convert.FromBase64String(secretKey);

        string identity = sysInt.get("mirasecure_identity").ToString();

        List<string> fields = new List<string>(fullMessage.Split(':'));

        byte[] iv = Convert.FromBase64String(fields[1]);
        byte[] encrypted = Convert.FromBase64String(fields[2]);
        string hmac = fields[3].Trim();

        using (HMACSHA256 hmacInst = new HMACSHA256(secretKeyBytes))
        {
          if (hmac != Convert.ToBase64String(hmacInst.ComputeHash(encrypted)))
          {
            Logger.LogError($"Failed to parse MiraPay token response: hash does not match", "MiraPay.StoreMiraSecureResponse");

            return CASL_error.create_str("The response could not be authenticated. Please try again.").casl_object;
          }
        }

        string message;

        using (AesManaged aes = new AesManaged())
        {
          aes.IV = iv;
          aes.Mode = CipherMode.CBC;
          aes.Padding = PaddingMode.PKCS7;

          using (MemoryStream memoryStream = new MemoryStream(encrypted))
          {
            using (CryptoStream cryptoStream = new CryptoStream(memoryStream, aes.CreateDecryptor(secretKeyBytes, iv), CryptoStreamMode.Read))
            {
              using (StreamReader srDecrypt = new StreamReader(cryptoStream))
              {
                message = srDecrypt.ReadToEnd();
              }
            }
          }
        }

        TokenResponse tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(message);
        // End Bug IPAY-1543 MJO

        if (tokenResponse.ActionCode == "D")
        {
          Logger.LogInfo($"MiraPay token declined{Environment.NewLine}{JsonConvert.SerializeObject(tokenResponse, Newtonsoft.Json.Formatting.Indented)}", "MiraPay.StoreMiraSecureResponse");

          if (tokenResponse.ResponseCode == "12")
            return CASL_error.create_str("Card entry session expired. Please try again.").casl_object;
          if (tokenResponse.ResponseCode == "10")
            return CASL_error.create_str("Invalid MiraSecure configuration.").casl_object;
          else
            return CASL_error.create_str("An error occurred. Please try again.").casl_object;
        }

        TokenResponses[tokenResponse.EchoData] = response;

        System.Timers.Timer RetryInitializeTimer = new System.Timers.Timer(900000);

        RetryInitializeTimer.Elapsed += (sender, e) => RemoveMiraSecureResponse(sender, e, tokenResponse.EchoData);
        RetryInitializeTimer.AutoReset = false;
        RetryInitializeTimer.Enabled = true;

        Logger.LogInfo("MiraPay token response saved for session id " + tokenResponse.EchoData, "MiraPay.StoreMiraSecureResponse");
      }
      catch (Exception e)
      {
        Logger.LogError($"Failed to parse MiraPay token response: {e.Message}", "MiraPay.StoreMiraSecureResponse");

        return CASL_error.create_str("An error occurred. Please try again.").casl_object;
      }

      return true;
    }

    private static void RemoveMiraSecureResponse(object source, ElapsedEventArgs e, string sessionId)
    {
      GenericObject outGEO;

      TokenResponses.TryRemove(sessionId, out outGEO);
    }

    public static object fCASL_ProcessPayment(CASL_Frame frame)
    {
      GenericObject args = frame.args;
      GenericObject sysInt = args.get("_subject") as GenericObject;
      GenericObject tender = args.get("tender") as GenericObject;

      return ProcessPayment(sysInt, tender);
    }

    private static object ProcessPayment(GenericObject sysInt, GenericObject tender)
    {
      string sessionId = CASLInterpreter.get_my().get("MaskedSessionID").ToString();

      GenericObject responseGeo = null;

      TokenResponses.TryRemove(sessionId, out responseGeo);

      if (responseGeo == null)
      {
        return CreateCASLError("Could not retrieve MiraPay token response", "MiraPay.ProcessPayment");
      }

      string message = responseGeo.get("Message").ToString();
      string requestMkey = responseGeo.get("MKEY").ToString();

      TokenResponse tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(Encoding.UTF8.GetString(Convert.FromBase64String(message)));

      Logger.LogInfo($"MiraPay token response{Environment.NewLine}{JsonConvert.SerializeObject(tokenResponse, Newtonsoft.Json.Formatting.Indented)}", "MiraPay.ProcessPayment");

      if (tokenResponse.ActionCode == "D")
      {
        string err = "MiraPayRequest Declined" + (tokenResponse.ResponseCode == "12" ? " due to session timeout" : "");
        return CreateCASLError(err, "MiraPay.ProcessPayment");
      }

      if (tokenResponse.First6Digits != null)
        tender.set("credit_card_nbr", tokenResponse.First6Digits.PadRight(16, '*'));

      DateTime timestamp = DateTime.Now;

      string requestString = String.Format("MTQ,TI{0},TC{1},A1{2},DT{3},NN{4},TK{5},IN{6}",
        sysInt.get("terminal_id"),
        "27", // Bug IPAY-788 MJO - Changed from "07" to "27" for $0 auth support
        Convert.ToInt32((((decimal)tender.get("total")) * 100)),
        timestamp.ToString("yyyyMMddHHmmss"),
        DateTimeOffset.Now.ToUnixTimeSeconds(),
        tokenResponse.EigenToken,
        CASLInterpreter.get_my().get("_mirapay_transaction_id")
      );

      StringBuilder sb = new StringBuilder("MiraPay request:" + Environment.NewLine);
      string[] requestFields = requestString.Split(',');
      for (int i = 0; i < requestFields.Length; i++)
      {
        sb.AppendLine(requestFields[i].Substring(0, 2) + " " + requestFields[i].Substring(2));
      }
      Logger.LogInfo(sb.ToString(), "MiraPay.ProcessPayment");

      string mkey;

      try
      {
        mkey = GenerateMKEYPay(sysInt, requestString);
      }
      catch (Exception e)
      {
        Logger.LogError("GenerateMKEYPay exception: " + e.Message, "MiraPay.ProcessPayment");
        return CreateCASLError("Failed to generate key", "MiraPay.ProcessPayment");
      }

      string requestArgs = requestString + ",MY" + mkey;

      object httpResponse = SendHttpRequest(sysInt, requestArgs);

      if (httpResponse is CASL_error)
      {
        return (httpResponse as CASL_error).casl_object;
      }

      sb = new StringBuilder("MiraPay response" + Environment.NewLine);
      Dictionary<string, string> responseFields = new Dictionary<string, string>();
      foreach (string pair in (httpResponse as string).Split(','))
      {
        string key = pair.Substring(0, 2);
        string value = pair.Substring(2);

        responseFields[key] = value;

        if (!DoNotLogKeys.Contains(key))
          sb.AppendLine(key + " " + value);
      }
      Logger.LogInfo(sb.ToString(), "MiraPay.ProcessPayment");

      // Bug 26980 MJO - Change description to actual card type
      string cardType = "";
      if (responseFields[CARD_TYPE] == "V")
        cardType = "VISA";
      else if (responseFields[CARD_TYPE] == "M")
        cardType = "MC";
      else if (responseFields[CARD_TYPE] == "D")
        cardType = "DISC";
      else if (responseFields[CARD_TYPE] == "A")
        cardType = "AMEX";

      tender.set(
        "auth_str", responseFields[APPROVAL_CODE],
        "approval_code", responseFields[APPROVAL_CODE],
        "_mirapay_card_type", responseFields[CARD_TYPE],
        "iso_response_code", responseFields[ISO_RESPONSE_CODE],
        "reference_nbr", responseFields[TERM_ID] + " " + responseFields[RECEIPT_REF_NUM] + " M",
        "eigen_token", responseFields[EIGEN_TOKEN],
        "receipt_msg", responseFields[RECEIPT_MSG],
        "mirapay_datetime", timestamp.ToString("yyyy/MM/dd-HH:mm:ss"),
        "terminal_id", sysInt.get("terminal_id"),
        "card_type", cardType
      );

      if (responseFields[APPROVED] != "Y")
      {
        string decl = responseFields[RECEIPT_MSG] == null ? "Declined" : "Declined: " + responseFields[RECEIPT_MSG];
        return CreateCASLError(decl, "MiraPay.ProcessPayment");
      }

      string cardNbrField = tokenResponse.EigenToken.Split(':')[5];
      string creditCardNbr = tokenResponse.First6Digits + cardNbrField.Substring(2).PadLeft(Convert.ToInt32(cardNbrField.Substring(0, 2)) - 6, '*');

      tender.set(
        "credit_card_nbr", creditCardNbr
      );

      return true;
    }

    private static GenericObject CreateCASLError(String msg, String action, bool is_err = false)
    {
      if (!is_err)
        Logger.LogInfo(msg, action);
      else
        Logger.LogError(msg, action);
      //Bug 26996 NAM: Need to return a GenericObject not GenObj_Error
      return (GenericObject)c_CASL.c_make("error", "code", "Err_MiraPay", "message", msg, "title", action, "throw", false);
    }

    public static object fCASL_ProcessVoid(CASL_Frame frame)
    {
      GenericObject args = frame.args;
      GenericObject sysInt = args.get("_subject") as GenericObject;
      GenericObject tender = args.get("tender") as GenericObject;

      return ProcessVoidOrRefund(sysInt, tender, false);
    }

    public static object fCASL_ProcessRefund(CASL_Frame frame)
    {
      GenericObject args = frame.args;
      GenericObject sysInt = args.get("_subject") as GenericObject;
      GenericObject tender = args.get("tender") as GenericObject;

      return ProcessVoidOrRefund(sysInt, tender, true);
    }

    private static object ProcessVoidOrRefund(GenericObject sysInt, GenericObject tender, bool isRefund)
    {
      string requestString = String.Format("MTQ,TI{0},TC{1},A1{2},DT{3},NN{4},AC{5},TK{6}",
        sysInt.get("terminal_id"),
        isRefund ? "04" : "03",
        Convert.ToInt32(((decimal)tender.get("total")) * 100),
        DateTime.Now.ToString("yyyyMMddHHmmss"),
        DateTimeOffset.Now.ToUnixTimeSeconds(),
        tender.get("auth_str"),
        tender.get("eigen_token")
      );

      string action = isRefund ? "MiraPay.ProcessVoidOrRefund (Refund)" : "MiraPay.ProcessVoidOrRefund (Void)";

      StringBuilder sb = new StringBuilder("MiraPay request:" + Environment.NewLine);
      string[] requestFields = requestString.Split(',');
      for (int i = 0; i < requestFields.Length; i++)
      {
        sb.AppendLine(requestFields[i].Substring(0, 2) + " " + requestFields[i].Substring(2));
      }
      Logger.LogInfo(sb.ToString(), action);

      string mkey;

      try
      {
        mkey = GenerateMKEYPay(sysInt, requestString);
      }
      catch (Exception e)
      {
        Logger.LogError("GenerateMKEYPay exception: " + e.Message, "MiraPay.ProcessVoidOrRefund");
        return CreateCASLError("Failed to generate key", "MiraPay.ProcessVoidOrRefund");
      }

      string requestArgs = requestString + ",MY" + mkey;

      object httpResponse = SendHttpRequest(sysInt, requestArgs);

      if (httpResponse is CASL_error)
      {
        return (httpResponse as CASL_error).casl_object;
      }

      sb = new StringBuilder("MiraPay response" + Environment.NewLine);
      Dictionary<string, string> responseFields = new Dictionary<string, string>();
      foreach (string pair in (httpResponse as string).Split(','))
      {
        string key = pair.Substring(0, 2);
        string value = pair.Substring(2);

        responseFields[key] = value;

        if (!DoNotLogKeys.Contains(key))
          sb.AppendLine(key + " " + value);
      }
      Logger.LogInfo(sb.ToString(), action);

      if (responseFields[APPROVED] != "Y")
      {
        string decl = responseFields[RECEIPT_MSG] == null ? "Declined" : "Declined: " + responseFields[RECEIPT_MSG];
        return CreateCASLError(decl, "MiraPay.ProcessPayment");
      }

      return true;
    }

    private static string GenerateMKEYPay(GenericObject sysInt, string requestString)
    {


      string password = sysInt.get("mirapay_mkey_password").ToString();
      byte[] bUserPwdEncrypted = Convert.FromBase64String(password);
      byte[] bUserPwdDecrypted = Crypto.symmetric_decrypt("data", bUserPwdEncrypted, "secret_key", Crypto.get_secret_key());
      password = Encoding.UTF8.GetString(bUserPwdDecrypted);

      string hashString = password + requestString;

      using (SHA256 hash = SHA256Managed.Create())
      {
        return String.Concat(hash
          .ComputeHash(Encoding.UTF8.GetBytes(hashString))
          .Select(item => item.ToString("x2")));
      }
    }

    private static object SendHttpRequest(GenericObject sysInt, string requestArgs)
    {
      UriBuilder uriBuilder = new UriBuilder(sysInt.get("mirapay_primary_url", "", true).ToString());
      uriBuilder.Query = requestArgs;

      using (var client = new HttpClient())
      {
        var response = client.GetAsync(uriBuilder.Uri).Result;

        if (response.IsSuccessStatusCode)
        {
          var responseContent = response.Content;

          return responseContent.ReadAsStringAsync().Result;
        }
        else
        {
          Logger.LogError("MiraPay HTTP request failed: " + uriBuilder.Uri.ToString() + ", " + response.StatusCode + ", " + response.ReasonPhrase, "MiraPay.SendHttpRequest");
          Logger.LogInfo("MiraPay: Trying secondary URL", "MiraPay.SendHttpRequest");

          uriBuilder = new UriBuilder(sysInt.get("mirapay_secondary_url", "", true).ToString());
          uriBuilder.Query = requestArgs;

          response = client.GetAsync(uriBuilder.Uri).Result;

          if (response.IsSuccessStatusCode)
          {
            var responseContent = response.Content;

            return responseContent.ReadAsStringAsync().Result;
          }
          else
          {
            string err = "MiraPay HTTP request failed: " + uriBuilder.Uri.ToString() + ", " + response.StatusCode + ", " + response.ReasonPhrase;
            return CreateCASLError(err, "MiraPay.SendHttpRequest", true);
          }
        }
      }
    }

    [JsonObject(MemberSerialization.OptIn)]
    private class TokenRequest
    {
      private TokenRequest () { }

      public TokenRequest (string merchantId, string amount, string transactionId, string redirectURL, string session_id)
      {
        MerchantID = merchantId;
        Amount = amount;
        TransactionID = transactionId;
        InvoiceNumber = transactionId;
        RedirectURL = redirectURL;
        EchoData = session_id;
      }

      [JsonProperty]
      public string MerchantID;
      [JsonProperty]
      public string Amount;
      [JsonProperty]
      public string TransactionID;
      [JsonProperty]
      public string InvoiceNumber;
      [JsonProperty]
      public string RedirectURL;
      [JsonProperty]
      public string AutoFocusField = "";
      [JsonProperty]
      public string EchoData;
    }

    [JsonObject(MemberSerialization.OptIn)]
    private class TokenResponse
    {
      private TokenResponse() { }

      [JsonProperty]
      public string ActionCode;
      [JsonProperty]
      public string ResponseCode;
      [JsonProperty]
      public string DateTime;
      [JsonProperty]
      public string TransactionID;
      [JsonProperty]
      public string UniqueCardID;
      [JsonProperty]
      public string InvoiceNumber;
      [JsonProperty]
      public string EigenToken;
      [JsonProperty]
      public string ShortToken;
      [JsonProperty]
      public string CardHolderName;
      [JsonProperty]
      public string CardType;
      [JsonProperty]
      public string BankResponse;
      [JsonProperty]
      public string CVVResponse;
      [JsonProperty]
      public string AVSResponse;
      [JsonProperty]
      public string ECI;
      [JsonProperty]
      public string CAVV;
      [JsonProperty]
      public string EchoData;
      [JsonProperty]
      public string Address;
      [JsonProperty]
      public string City;
      [JsonProperty]
      public string PostalCode;
      [JsonProperty]
      public string First6Digits;
      [JsonProperty]
      public string ExpirationDate;
      [JsonProperty]
      public string MKEY;
      [JsonProperty]
      public string ApprovalCode;
    }
  }
}
