using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.SessionState;
using System.IO;
using CASL_engine;

using System.Globalization;
using System.Threading;

using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Reflection;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.Net;

using System.Diagnostics;
using System.Net.Sockets;
using System.Collections.Concurrent;
using DeviceManagement;
using Newtonsoft.Json;
using System.Linq;
using System.Xml.Linq;
using Hangfire;
using Hangfire.SQLite;

/*
  ERROR CODES FOR iPayment ARE IN THE FORMAT HR-750.... PLEASE ALWAYS USE A SPECIFIC ERROR CODE FOR EACH ERROR.
  ALSO PLEASE UPDATE THIS SECTION WITH THE LARGEST ERROR YOU HAVE USED TO DATE. WHEN THE NEXT PERSON 
  CREATES ONE, THEY WILL BE ABLE TO ASSIGN ERRORS IN SEQUENCE./

  HR-101 - "Invalid Input" -- BUG#8455
  HR-102 - "Server Issue"  -- CATCH UNKNOWN EXCEPTION and LOG in cs log file BUG#8455

  HR-103 - *NEXT AVAILABLE*
*/

namespace corebt
{

  public class handle_request_2 : IHttpHandler, IRequiresSessionState
  {
    public handle_request_2() { }
    public bool IsReusable { get { return true; } }
    private static Mutex load_mutex = new Mutex();

    // Bug 25213 UMN squirrel away par args here when redirecting on a POST request
    private static ConcurrentDictionary<string, string> _parArgsDict = new ConcurrentDictionary<string, string>();

    private static bool first_request = true;

    public void ProcessRequest(System.Web.HttpContext context)
    {
      bool initial_request = false;

      if (first_request)
      {
        initial_request = true;
        first_request = false;
        c_CASL.master_stopwatch.Start();
      }
      else
      {
        // If core_entries.htm is loaded first, then the CASLInterpreter
        // is not initalized and a request to encrypt_source_files fails
        // in CASL_server.init_server() which expects the CASL class
        // 'server' (and 'response', 'request') to be defined.  This
        // second check catches the partially initialized condition.
        if (!c_CASL.GEO.has("server"))
          initial_request = true;
      }

      // Stack initialization needs to happen for every thread,
      // which ends up being every request because we don't control
      // thread startup.
      CASL_Stack.Initialization();

      //BUG#14781 Ignore Fake Certificate for calling PB web service with https 
      ServicePointManager.ServerCertificateValidationCallback =
           new RemoteCertificateValidationCallback(IgnoreCertificateErrorHandler);

      string SESSIONID = "";
      if (System.Web.HttpContext.Current != null)
        SESSIONID = System.Web.HttpContext.Current.Session.SessionID; //BUG#8455

      // Bug 18085 MJO - This is now determined on a per-session basis
      bool SecureCookies = context.Request.IsSecureConnection;

      try
      {
        context.Request.ValidateInput(); // Bug#8455  Added by Anu on 03/20/2009 for PCI

        // Handle static files if http GET, no queryString (or post), and file exists
        if (ProcessRequestStatic(context))
        {
          CASL_Stack.Release_Frames();
          return;
        }

        // Initialize some local variables
        String raw_uri = context.Request.RawUrl;
        System.Web.HttpRequest a_http_request = HttpContext.Current.Request;
        string http_method = a_http_request.HttpMethod;
        string content_type = a_http_request.ContentType;
        string client_name = a_http_request.UserHostName; // e.g. 127.0.0.1 
        string server_name = a_http_request.ServerVariables["SERVER_NAME"] as string; //e.g. 172.16.1.83
        bool is_local_request = (client_name == "127.0.0.1" || client_name == server_name);

        // Bug 21856 UMN Set to a value that we might override in a later
        // (after c_CASL.GEO is set) call to SetBrowser
        string browser = a_http_request.Browser.Browser;
        string content = null;

        // TTS#:24656  Co-ordinated with Steve to generate requests of the form
        // test_suite_identification.htm?args=*test name*
        // which enable us to rapidly pinpoint which test is causing messages
        // to be logged.
        bool is_test_suite_identification =
            raw_uri.IndexOf("test_suite_identification") > 0;

        // Bug 17513 MJO - .inspect requests should be considered IDE requests
        bool is_casl_ide_request = (raw_uri.IndexOf("casl_ide") > 0 || raw_uri.IndexOf("console") > 0 || raw_uri.IndexOf("show_database_config") > 0
          || raw_uri.IndexOf(".inspect") > 0);
        bool is_reload_request = (raw_uri.IndexOf("reload") > 0);

        // BUG 25434 RDM
        bool is_gateway_dms_request = (raw_uri.ToLower().IndexOf("gatewaypost") > -1 && a_http_request.Form["device_id"] != null);
        bool is_gateway_dms_sale = (a_http_request.Form["e_transactiontype"] == "sale" && a_http_request.Form["device_id"] != null);

        //Bug 23080 NAM: add logout to session. Required because ProcessRequest is called again after the logout call as a result of a redirect
        // Bug 24531 - Make sure "my" exists
        if (raw_uri.IndexOf("logout") > 0 && System.Web.HttpContext.Current.Session["my"] != null)
          ((GenericObject)System.Web.HttpContext.Current.Session["my"]).Add("logout", true);

        // Bug 8932 UMN to support redirect from secure transfer if args are > 1000 characters.
        // Problem is that IE chokes on them, so need to do a POST and redirect.
        bool is_start_request =
          raw_uri.IndexOf("pos/start.htm") > 0 ||
          raw_uri.IndexOf("pos/start_sso.htm") > 0 ||
          raw_uri.IndexOf("cbc/start.htm") > 0 ||
          raw_uri.IndexOf("cbc/start_sso.htm") > 0 ||
          raw_uri.IndexOf("Portal/start_sso.htm") > 0 ||
          raw_uri.IndexOf("Config/start_sso.htm") > 0;

        // Bug 25938 UMN add jslogger exception handling
        bool is_jslogger_request = raw_uri.IndexOf("jsnlog.logger") > 0;

        // Bug 20642 UMN responsive UI - load and parse templates during start-up
        if (!initial_request)
        {
          Object objA =
              c_CASL.c_object("Business.Business_center_settings.data");
          if (objA is GenericObject)
          {
            GenericObject go = objA as GenericObject;
            Object objB = go.get("cbc_cache_templates", false);
            if (objB is bool && ((bool)objB) &&
                !ProjectBaseline.TemplateLoader.Instance.AreTemplatesLoaded())
              ProjectBaseline.TemplateLoader.Instance.LoadTemplates();
          }
        }

        // Bug #9348 Mike O - Moved this before the check for LOADING and ERROR statuses
        // catch exception in class_loading process - c_CASL.init()
        if (initial_request)
        {
          if (!StartupIPayment(context))
          {
            context.ApplicationInstance.CompleteRequest();
            CASL_Stack.Release_Frames();
            return;
          }
        }

        // Bug #9348 Mike O - Moved this below CASL initialization so we know the true status before handling it
        if ((HttpContext.Current.Application["LOADING_STATUS"] as string) == "LOADING" && !(is_casl_ide_request))
        {
          // Show loading now page during initial loading process (exclude casl ide and server local access) 
          ShowLoadingPage(context, initial_request);
          CASL_Stack.Release_Frames();
          return;
        }
        else if ((HttpContext.Current.Application["LOADING_STATUS"] as string) == "ERROR" &&
          !(is_local_request || is_casl_ide_request || is_reload_request))
        {
          // Show error page upon error of initial loading process (exclude casl ide, reload and server local access) 
          ShowErrorPage(context);
          CASL_Stack.Release_Frames();
          return;
        }
        // END BUG#5532 HANDLE LOADING ISSUE.

        InitializeSession(context, SecureCookies, raw_uri);

        // Bug 21856 UMN possibly override browser setting based on Web.config
        browser = SetBrowser(a_http_request, a_http_request.Browser.Browser);

        raw_uri = AppendPostArgsToURL(context, raw_uri, SESSIONID);

        // Bug 25938 UMN add jslogger exception handling
        if (is_jslogger_request)
        {
          ProcessJSLogRequest(context);
          context.ApplicationInstance.CompleteRequest();
          CASL_Stack.Release_Frames();
          return;
        }

        if (String.IsNullOrWhiteSpace(raw_uri))
        {
          AbandonSession(context);
          context.ApplicationInstance.CompleteRequest();
          CASL_Stack.Release_Frames();
          return;
        }
        // Bug 20280 MJO - Compare source and target headers to make sure they match
        if (!CompareSourceAndTargetHeaders(context, SESSIONID, raw_uri, is_start_request))
        {
          context.ApplicationInstance.CompleteRequest();
          CASL_Stack.Release_Frames();
          return;
        }

        // Bug 16648 MJO - Check to make sure the double-submit value is present in the request and matches the cookie
        if (!VerifyCSRFValues(context, SESSIONID, raw_uri, is_casl_ide_request, a_http_request))
        {
          context.ApplicationInstance.CompleteRequest();
          CASL_Stack.Release_Frames();
          return;
        }

        // Bug 8932 UMN Handle pos.start or cbc.start with a par when method is a POST
        // Bug 26514 RDM now supports config.start/portal.start
        if (!RedirectToStartWithoutArgs(raw_uri, is_start_request, a_http_request, http_method))
        {
          CASL_Stack.Release_Frames();
          return;
        }

        raw_uri = PurgeDoubleSubmitInfo(raw_uri, a_http_request);

        GenericObject a_server = null;// BUG# 9523 DH - Replaced the content type and server parsing code.
        bool bDontProcessAgain = false;// BUG# 9523 DH - Replaced the content type and server parsing code.

        // Bug 26202 MJO - Treat requests coming from the Config report as inspects
        if (CASLInterpreter.get_my().Contains("report_config") && context.Request.Headers["Referer"] != null && context.Request.Headers["Referer"].ToString().Contains("report.htm"))
          raw_uri = raw_uri.Replace(".htm", ".inspect");

        // Process post_remote, batch update update, casl_ide or formula requests
        if (!ProcessSpecialRequests(context, ref raw_uri, a_http_request,
               http_method, ref content, ref a_server, ref bDontProcessAgain,
               is_casl_ide_request, is_test_suite_identification))
        {
          context.ApplicationInstance.CompleteRequest();
          CASL_Stack.Release_Frames();
          return;
        }

        /****************** PRIMARY ACTION TO PROCESS REQUEST ******************/

        Stopwatch watch = new Stopwatch();

        // BUG# 9523 DH - Replaced the content type on the client and modified the server parsing code
        // to recreate key-value pair form data structure.
        if (!bDontProcessAgain)
        {
          watch.Start();
          // Returns the CASL object that is the result of calling the request
          a_server = CASL_server.process_request("a_uri", raw_uri,
            "http_method", http_method,
            "content_type", content_type,
            "content", content,
            "browser", browser) as GenericObject; // return a server
        }

        // Bug 17410 MJO - If the user just logged in, switch the session id
        SESSIONID = SwitchSessionIdIfNeeded(context, SESSIONID);

        // BUG 25434 RDM
        if (is_gateway_dms_request)
        {
          if (!FinishGatewayDmsResponse(a_server, is_gateway_dms_sale))
            FormatResponse(context, SecureCookies, raw_uri, browser, a_server, is_casl_ide_request, bDontProcessAgain, watch);
        }
        else
        {
          FormatResponse(context, SecureCookies, raw_uri, browser, a_server, is_casl_ide_request, bDontProcessAgain, watch);
        }

        foreach (string key in HttpContext.Current.Response.Cookies.AllKeys)
        {
          HttpContext.Current.Response.Cookies[key].HttpOnly = true;
          if (SecureCookies)
          {
            HttpContext.Current.Response.Cookies[key].Secure = true;
            HttpContext.Current.Response.Cookies[key].SameSite = SameSiteMode.None;
          }
        }

        if (raw_uri.Contains("receive_mirasecure_response.htm"))
        {
          foreach (string cookie in context.Response.Cookies.AllKeys)
            context.Response.Cookies.Remove(cookie);
        }

        // Bug 8932 UMN - Send the response before abandoning the session
        if (context.Response.IsClientConnected)
          context.Response.Flush();
        context.ApplicationInstance.CompleteRequest(); // BUG 16461 

        // Bug 16645 MJO - If this session is marked as ready to be abandoned, and this isn't a redirect response, abandon it
        if ((bool)CASLInterpreter.get_my().get("_abandon_session", false) && context.Response.StatusCode != 303)
        {
          // Send the response before abandoning the session
          if (context.Response.IsClientConnected)
            context.Response.Flush();
          context.ApplicationInstance.CompleteRequest(); // BUG 16461 
          AbandonSession(context);
        }
      }
      //START OF BUG#8455
      catch (HttpRequestValidationException)
      {
        // Bug 15298 (Bug 15406) UMN Remove logging of form data in the error message because it can reveal
        // what the user typed, such as a password.
        Logger.cs_log("Invalid Input(HR-101)): SessionID=" + SESSIONID);

        // Bug 23334 UMN
        if (!HttpContext.Current.Response.HeadersWritten)
        {
          context.Response.Clear();
          context.Response.StatusCode = 200;
          // Bug 16644 UMN XSS fix
          context.Response.Write(@"<html><head><center><title>HTML Not Allowed</title></head>
        <body style='font-family: Arial, Sans-serif;'><h1>Invalid Input(HR-101)</h1><p>HTML entry is not allowed on that page.</p><p>Please make sure that your entries do not contain any special characters like &lt; or &gt;.</p>
         </center></body></html>");
        }
        if (context.Response.IsClientConnected)
          context.Response.Flush();
        context.ApplicationInstance.CompleteRequest(); // BUG 16461 replace Response.Close() with CompleteRequest()
        CASL_Stack.Release_Frames();
        return;
      }
      catch (Exception e)
      {
        String raw_uri = context.Request.RawUrl;
        Logger.cs_log("Server Issue(HR-102): SessionID=" + SESSIONID
          + ";RAW_URL= " + raw_uri
          + ";ERROR MESSAGE=" + e.InnerException
          + "\nFull Stack trace: " + e.ToMessageAndCompleteStacktrace()); // Bug 23712: add stack trace and Bug 17849

        try
        {
          // Bug 23712 Write simplifed message (without inner exception) to the activity log
          string login_user = "unknown";
          string application_name = "";
          try
          {
            GenericObject my = CASLInterpreter.get_my();

            if (my != null) {
              login_user = (string)my.get("login_user", "unknown");
              application_name = (string)my.get("application_name", "");
            }
          }
          catch { }

          misc.CASL_call("a_method", "actlog", "args",
            new GenericObject(
              // Bug 20435 UMN PCI-DSS don't pass session
              "user", login_user,
              "app", application_name,
              "action", "Exception",
              "summary", "Server Issue(HR-102)",
              "detail", misc.FormatStacktraceForActivityLog(e)));
        }
        catch (Exception actlogEx)
        {
          // Don't let actlog error screw up error
          Logger.cs_log("Cannot write to the activity log: Error: " + actlogEx.ToString());
        }

        // End Bug 23712
        // Bug 23334 UMN
        if (!HttpContext.Current.Response.HeadersWritten)
        {
          context.Response.Clear();
          context.Response.StatusCode = 200;
          context.Response.Write(@"<html><head><center><title>iPayment Error </title></head>
        <body style='font-family: Arial, Sans-serif;'><h1>Error Occurred when processing request:Server Issue(HR-102)</h1>
         </body></html>");
        }
        if (context.Response.IsClientConnected)
          context.Response.Flush();
        context.ApplicationInstance.CompleteRequest(); // BUG 16461 replace Response.Close() with CompleteRequest()
      }
      CASL_Stack.Release_Frames();
      //END OF BUG#8455
    } // ProcessRequest

    /// <summary>
    /// Write out any JavaScript Log messages to the CS log
    /// Bug 25938 UMN
    /// </summary>
    /// <param name="context"></param>
    private static void ProcessJSLogRequest(HttpContext context)
    {
      var jsonString = String.Empty;
    
      context.Request.InputStream.Position = 0;
      using (var inputStream = new StreamReader(context.Request.InputStream))
      {
        jsonString = inputStream.ReadToEnd();
      }
      //IPAY-675 NK write activity log full error only when verbose is true, otherwise truncate
      if (jsonString.Contains("Uncaught Exception") && (c_CASL.get_show_verbose_error() == false))
       {
        
         Logger.cs_log("JavaScript Error: Uncaught Exception. " + "Set show_verbose_error value to true for full error description.");
       }
      else
        Logger.cs_log("JavaScript Error: " + jsonString);
      if (!HttpContext.Current.Response.HeadersWritten)
      {
        context.Response.Clear();
        context.Response.StatusCode = 200;
      }
    }

    /// <summary>
    /// Calls any web services defined in a system interface. Limits it to system interfaces of type
    /// Business.System_interface.PBStandardInquiry or Business.System_interface.PBStandardRealtimeUpdate
    /// </summary>
    private static void StartPBWebServices()
    {
      GenericObject geoAllSysInt = (GenericObject)c_CASL.c_object("System_interface.of");
      ArrayList listSysInt = geoAllSysInt.getStringKeys();
      List<string> alreadyCalled = new List<string>();  // don't call a WS more than once
      foreach (string siName in listSysInt)
      {
        GenericObject geoSysInt = (GenericObject)geoAllSysInt.get(siName, null);
        if (geoSysInt != null)
        {
          // Bug 23975 Don't warm up Web services if they don't have transactions that use them
          GenericObject geo = geoSysInt.get("transactions", c_CASL.GEO) as GenericObject;
          int transLen = (geo.get("_objects", c_CASL.GEO) as GenericObject).vectors.Count;
          if (transLen > 0 && geoSysInt.has("_parent"))
          {
            GenericObject geoSIParent = (GenericObject)geoSysInt.get("_parent");
            string parentName = geoSIParent.get("_name", "") as string;
            if (parentName == "PBStandardRealtimeUpdate" || parentName == "PBStandardInquiry")
            {
              string sEndpointAddress = geoSysInt.get("path", "") as string; // e.g. http://ssg-banner-dev/WcfServiceSSI/SSI.svc
              if (!String.IsNullOrWhiteSpace(sEndpointAddress) && !alreadyCalled.Contains(sEndpointAddress))
              {
                alreadyCalled.Add(sEndpointAddress);
                // don't care about return value
                ProjectBaseline.CStandardSystemInterface.IsWSConnOK(geoSysInt);
                Logger.cs_log(String.Format("Pre-warmed Web Service endpoint: {0}.", sEndpointAddress));
              }
            }
          }
        }
      }
    }

    /// <summary>
    /// Format the HTML response to the caller.
    /// </summary>
    /// <param name="context"></param>
    /// <param name="SecureCookies"></param>
    /// <param name="raw_uri"></param>
    /// <param name="browser"></param>
    /// <param name="a_server"></param>
    private static void FormatResponse(System.Web.HttpContext context, bool SecureCookies, String raw_uri,
      string browser, GenericObject a_server, bool is_casl_ide_request, bool bDontProcessAgain, Stopwatch watch)
    {
      GenericObject a_response = a_server.get("a_response") as GenericObject;
      context.Response.ContentType = a_response.get("content_type", "text/html").ToString();

      // Bug #14030 Mike O - Explicitly define the charset to fix a Firefox bug
      context.Response.Charset = a_response.get("charset", "utf-8").ToString();

      //BUG#9038 SET COMPACT PRIVARY POLICY in order to embed ipayment app in the external website (need enable third party cookie) 
      HttpContext.Current.Response.AddHeader("p3p", "CP=\"IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT\"");

      // set status code
      string status_code = a_response.get("status_code", "200").ToString();
      context.Response.StatusCode = Int32.Parse(status_code);

      // Bug #13833 UMN - Add correct header and content type for CSV file download
      if (raw_uri.Contains(".csv"))
      {
        GenericObject a_result = a_response.has("result") ? a_response.get("result") as GenericObject : null;
        String code = a_result != null && a_result.has("code") ? a_result.get("code", "200").ToString() : "200";

        if (code == "200")
        {
          context.Response.Clear();
          context.Response.ContentType = "text/csv";
          context.Response.AddHeader("Content-Disposition", "attachment;filename=" + DateTime.Now.ToString("MMddyyHHmmss") + ".csv");
          context.Response.Cache.SetCacheability(HttpCacheability.Private);
        }
      }

      if (a_response.has("location"))
      {
        // set HTTP location header
        context.Response.RedirectLocation = a_response.get("location") as string;
      }

      // Bug #14096/14303 Mike O - Allow .js_c files to be cached
      if (raw_uri.EndsWith(".js_c"))
      {
        context.Response.Cache.SetCacheability(HttpCacheability.Private);
        context.Response.Cache.SetMaxAge(new TimeSpan(24, 0, 0));
      }
      // Bug #13833 UMN - Only change cacheability if not CSV
      else if (!raw_uri.EndsWith(".csv") || raw_uri.Contains("Image") || raw_uri.Contains("get_image"))
      {
        // Bug 17073 UMN PCI-DSS ZAP SCAN Warning:Incomplete or no cache-control and pragma HTTPHeader set
        // UMN: Use suggestions from OWASP
        context.Response.AppendHeader("Cache-Control", "no-cache, no-store, private, must-revalidate"); // HTTP 1.1.
        context.Response.AppendHeader("Pragma", "no-cache"); // HTTP 1.0.
        context.Response.AppendHeader("Expires", "0"); // Proxies.

        // Bug 15105 UMN. Support IE9 and IE10 by forcing quirks mode.
        if (browser == "IE")
        {
          context.Response.AppendHeader("X-UA-Compatible", "IE=EmulateIE7"); // force quirks mode
        }
        else
        {
          context.Response.AppendHeader("X-UA-Compatible", "IE=edge"); // force edge mode
        }

        // Bug 17335 UMN - Use secure transport header if site was loaded with SSL
        // THIS HEADER must never be used with HTTP!
        if (context.Request.IsSecureConnection)
        {
          context.Response.AppendHeader("Strict-Transport-Security", "max-age=31536000; includeSubDomains");
        }
      }

      // set the content
      object R = a_response.get("body_bytes", null);

      // Are we in cbc?
      bool is_cbc = false;
      GenericObject my = CASLInterpreter.get_my();
      if (my != null && System.Web.HttpContext.Current != null)
      {
        is_cbc = (string)(my.get("application_name", "")) == "cbc" ? true : false;
      }

      if (R == null)
      {
        // Bug #10205 Mike O - The response should contain a hyperlink to the redirect target if we're redirecting
        if (a_response.has("location"))
          context.Response.Write("<html><body><a href='" + a_response.get("location") as string + "'>Redirecting</a></body></html>");
        else
        {
          object result = a_response.get("result", "no result");
          if (result == null)
            context.Response.Write("null");
          else
            context.Response.Write(result.ToString());
        }
      }
      else if (R is string)
      {
        //Bug 23080 NAM: check if logout was called
        bool bLogOut = ((GenericObject)System.Web.HttpContext.Current.Session["my"]).Contains("logout");
        string ui_mode = "full";
        if (my != null && System.Web.HttpContext.Current != null)
        {
          ui_mode = my.get("ui_mode", "full") as string;
        }

        // Bug 22592 UMN When UI mode is "terminal", we will get verbose errors, which
        // will contain Runtime error. So 
        // only logout if we're transferring and there's an error
        if (R.ToString().Contains("Runtime error") && ui_mode == "terminal")
        {
          // Might not really be a pos, if pos isn't constructed before the error happens
          GenericObject a_pos = my.get(0) as GenericObject;  // is it ok to hard-code 0 here?
          bool isa_pos = misc.base_is_a(a_pos, c_CASL.c_object("Business.pos"));

          if (isa_pos)
          {
            // Tried redirecting to logout, but it wasn't working
            misc.CASL_call("a_method", "logout", "_subject", a_pos);
          }

          // Bug 23334 Add debugging aid by writing error to file in log directory
          string fileName = String.Format("{0}/{1}/{2}.", c_CASL.GEO.get("site_physical"), "log", "terminal_error.txt");
          TextWriter errorWriter = null;
          try
          {
            errorWriter = TextWriter.Synchronized(File.CreateText(fileName));
            errorWriter.Write(R.ToString());
            errorWriter.Close(); errorWriter = null;
          }
          finally
          {
            if (errorWriter != null) errorWriter.Close();
          }

          // Redirect to a new custom page which has a page title of iPayment Error
          // Core Direct will see that the title is an error, and will close the
          // window. 

          // Note: this may be causing errors in the log about a thread abort. 
          //Ask MikeO and Shuang for help!
          context.Response.Buffer = false;
          context.Response.Clear();
          context.Response.ContentType = "text/html";
          context.Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
          context.Response.Expires = -1500;
          context.Response.CacheControl = "no-cache";

          // UMN always check before redirecting
          if (!context.Response.IsRequestBeingRedirected)
            context.Response.Redirect(context.Request.ApplicationPath + "/error_custom/logout_error.htm", false);
        }
        else if (!is_casl_ide_request && !raw_uri.Contains("validateFieldForABA") && !bDontProcessAgain && is_cbc && !bLogOut) //Bug 23080 NAM
        {
          watch.Stop();
          TimeSpan ts = watch.Elapsed;
          if ((bool)((GenericObject)c_CASL.c_object("Business.Business_center_settings.data")).get("cbc_time_responsiveUI", false))
          {
            Logger.cs_log(string.Format(" Classic UI took {0} seconds.", watch.ElapsedMilliseconds / 1000.00));
          }

          // Bug 20642 responsive TODO UMN may need to prevent back button. Investigate following (commented out for now)
          //context.Response.AppendHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
          //context.Response.AppendHeader("Pragma", "no-cache"); // HTTP 1.0.
          //context.Response.AppendHeader("Expires", "0"); // Proxies.
          string htmlResponse = ResponsiveUI.ReturnResponsiveUI(R.ToString(), browser, context.Request.Url.AbsoluteUri.ToString(), raw_uri);
          context.Response.Write(htmlResponse);
        }
        else
        {
          context.Response.Write(R.ToString());
        }
      }
      else if (R is byte[])
      {
        context.Response.BinaryWrite(R as byte[]);
      }
      else
      {
        throw CASL_error.create_str("Server Error: invalid body bytes");
      }
    }

    // BUG 25434 RDM
    /// <summary>
    /// Handle the end of the Payment Gateway response when the DMS is in use.
    /// An SSE event-stream is already in process, properly get the final payload and send down the stream before closing.
    /// </summary>
    /// <param name="a_server"></param>
    private static bool FinishGatewayDmsResponse(GenericObject a_server, bool is_sale)
    {
      GenericObject a_response = a_server.get("a_response") as GenericObject;
      object R = a_response.get("body_bytes", null);
      string rKvpAsString = R.ToString();

      Dictionary<string, string> payloadAsDict;

      Dictionary<string, string> mergeAllDicts(params Dictionary<string, string>[] dicts)
      {
        Dictionary<string, string> mergedDict = dicts.Aggregate(new Dictionary<string, string>() { }, (acc, dict) =>
        {
          return acc.Concat(dict).GroupBy(d => d.Key).ToDictionary(d => d.Key, d => d.First().Value);
        });

        return mergedDict;
      }

      Dictionary<string, string> kvpStrToDict(string str)
      {
        try
        {
          Dictionary<string, string> kvpStrAsDict;
          kvpStrAsDict =
            str
            .Split('\n')
            .Aggregate( // make sure KVPs are all one line (1 line corresponds to one item in array)
              new LinkedList<string>(),
              (kvps, kvp) =>
              {
                if (!kvp.Contains('=')) // this is not a kvp, and belongs to the value of the previous kvp (located at head of linked list in this aggregate)
                {
                  LinkedListNode<string> mark1 = kvps.First;
                  kvps.RemoveFirst();
                  kvps.AddFirst(mark1.Value + kvp.Replace("\n", ""));
                  return kvps;
                }
                kvps.AddFirst(kvp);
                return kvps;
              },
              kvps => kvps // no reason to convert back into Array, since Linq works just as well off of linked lists
              )
            .Select(value => value.Split('='))
            .ToDictionary(pair => pair[0].Trim().ToLower(), pair => pair[1].Trim());
          return kvpStrAsDict;
        }
        catch (Exception e)
        {
          Logger.LogError(e.Message, "handle_request_2.kvpStrToDict");

          Dictionary<string, string> err = new Dictionary<string, string>()
              {
                { "e_status", "error" },
                { "e_message", "Internal Server Error" },
              };

          return err;
        }
      }

      Dictionary<string, string> rKvpAsDict = kvpStrToDict(rKvpAsString);

      void sendNonSaleResponse()
      {
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.Write(JsonConvert.SerializeObject(rKvpAsDict, Formatting.None));
      }
      void sendFailedSaleResponse()
      {
        string payload = JsonConvert.SerializeObject(rKvpAsDict, Formatting.None);
        HttpResponse res = HttpContext.Current.Response;
        res.StatusCode = 200;
        res.ContentType = "text/event-stream";
        res.Write("event: response" + "\n");
        res.Write("data: " + payload + "\n\n");
        res.Flush();
      }

      // If e_transactiontype is "sale", then FinishServerSentEvents, else, jsonify payload and respond
      if (is_sale)
      {
        if (!CASLInterpreter.get_my().has("DMS_Point"))
        {
          sendFailedSaleResponse();
          return true;
        }

        Point point = CASLInterpreter.get_my().get("DMS_Point") as Point;
        Dictionary<string, string> xmlKeyMap = new Dictionary<string, string>()
        {
          { "RESULT", "auth_text" },
          { "RESULT_CODE", "device_error_code" },
          { "AUTH_CODE", "auth_code" },
          { "PAYMENT_TYPE", "card_type" },
          { "BANK_ROUTING_NBR", "bank_routing_number" },
          { "BANK_ACCOUNT_NBR", "bank_acct_last_four" },
          { "CHECK_NBR", "check_number" },
          { "TRANS_DATE", "trans_date" },
          { "TRANS_TIME", "trans_time" },
          { "REFERENCE", "ref" },
          { "TRANS_AMOUNT", "amount" },
          { "APPROVED_AMOUNT", "amount" },
          { "FSA_AMOUNT", "amount" },
          { "CHECK_AMOUNT", "amount" },
          { "PAYMENT_MEDIA", "card_brand" },
          { "ACCT_NUM", "acct_num" },
          { "CARDHOLDER", "holder_name" },
          { "CARD_EXP_MONTH", "card_exp_month" },
          { "CARD_EXP_YEAR", "card_exp_year" },
          { "CARD_ENTRY_MODE", "emv_card_entry_mode" },
          { "MIME_TYPE", "sig_image_format" },
          { "SIGNATUREDATA", "sig_image" },
          { "CARD_TOKEN", "token" },
          { "EMV_TAG_4F", "emv_app_id" },
          { "EMV_TAG_8A", "emv_app_resp_code" },
          { "EMV_TAG_95", "emv_terminal_verification_results" },
          { "EMV_TAG_9B", "emv_tx_status_info" },
          { "EMV_TAG_9F10", "emv_issuer_app_data" },
          { "EMV_TAG_9F12", "emv_app_name" },
        };
        Dictionary<string, string> approvedXmlToDict(string xml)
        {
          TextReader xmlr = new StringReader(xml);
          Dictionary<string, string> xmlAsDict = XDocument.Load(xmlr).Descendants("RESPONSE").Descendants()
          .Aggregate(new Dictionary<string, string>(), (acc, element) =>
          {
            string mappedKey;
            // e.g. in the case where APPROVED_AMOUNT and TRANS_AMOUNT show up in the same pointXml, the last one in the XML map will prevail.
            // The order in the xml map matches the CORE direct code from Uttam
            if (xmlKeyMap.TryGetValue(element.Name.ToString().Trim(), out mappedKey))
            {
              string val = element.Value.Trim();
              if (mappedKey == "bank_acct_last_four" && !String.IsNullOrWhiteSpace(val))
              {
                string acctNum = val.PadLeft(9, '0');
                val = acctNum.Substring(acctNum.Length - 4, 4);
              }
              if (mappedKey == "card_exp_year")
              {
                val = "20" + val;
              }
              if (mappedKey == "acct_num")
              {
                string acctNum = val.PadLeft(16, '0');
                string card_first_four = acctNum.Substring(0, 4);
                string card_last_four = acctNum.Substring(acctNum.Length - 4, 4);
                acc["card_first_four"] = card_first_four;
                acc["card_last_four"] = card_last_four;
              }
              else if (mappedKey == "trans_time") // if tran_time shows up last in Point Response XML
              {
                string transDate;
                if (acc.TryGetValue("trans_date", out transDate))
                {
                  if (DateTime.TryParse(String.Format("{0} {1}", transDate, val), out DateTime datetime))
                  {
                    // Epic doc asks for the "o" format
                    acc["ref_date_time"] = datetime.ToUniversalTime().ToString("o");
                  }
                }
              }
              else if (mappedKey == "trans_date") // if tran_date shows up last in Point Response XML
              {
                string transTime;
                if (acc.TryGetValue("tran_time", out transTime))
                {
                  if (DateTime.TryParse(String.Format("{0} {1}", val, transTime), out DateTime datetime))
                  {
                    // Epic doc asks for the "o" format
                    acc["ref_date_time"] = datetime.ToUniversalTime().ToString("o");
                  }
                }
              }
              else
              {
                acc[mappedKey.ToLower()] = val;
              }
            }
            return acc;
          });
          return xmlAsDict;
        }

        string pointResponseXml = CASLInterpreter.get_my().get("point_response", "").ToString();
        Dictionary<string, string> pointResponseXmlAsDict = new Dictionary<string, string>() { };

        // logic to be compatible with Core Direct
        Dictionary<string, string> coreDirectFields = new Dictionary<string, string>() { };
        if (rKvpAsDict.TryGetValue("e_status", out string e_status))
        {
          if (e_status.ToLower() == "approved")
          {
            if (pointResponseXmlAsDict.TryGetValue("emv_card_entry_mode", out string cardEntryMode))
            {
              if (cardEntryMode.ToLower() != "chip read")
              {
                if (cardEntryMode.ToLower() == "Keyed") coreDirectFields["holder_name"] = "Unavailable: Keyed";
                coreDirectFields.Add("is_emv", "false");
                pointResponseXmlAsDict.Keys.ToList()
                  .ForEach(key =>
                  {
                    if (key.ToLower().Contains("emv")) pointResponseXmlAsDict.Remove(key);
                  });
              }
              else
              {
                coreDirectFields.Add("emv_auth_mode", "online");
              }
            };
            if ((bool)CASLInterpreter.get_my().get("is_fsa", false))
            {
              coreDirectFields["is_fsa"] = "true";
            }
            else
            {
              coreDirectFields["is_fsa"] = "false";
            }

            if (!rKvpAsDict.TryGetValue("e_message", out string e_message)) coreDirectFields["e_message"] = "Transaction successful";
            if (!String.IsNullOrEmpty(pointResponseXml)) pointResponseXmlAsDict = approvedXmlToDict(pointResponseXml);
          }
        }

        // if outOfBandCancel, add e_sub_status field to response
        Dictionary<string, string> outOfBandCancelDict = new Dictionary<string, string>() { };
        if (rKvpAsString.ToLower().Contains("user or gateway cancelled")) outOfBandCancelDict["e_sub_status"] = "cancel";
        if (rKvpAsString.ToLower().Contains("transaction cancelled")) outOfBandCancelDict["e_sub_status"] = "cancel";

        // since coreDirectFields comes before pointResponseXmlAsDict in the function args, the coreDirectFields will be selected over the point response fields in the final dict.
        payloadAsDict = mergeAllDicts(rKvpAsDict, coreDirectFields, pointResponseXmlAsDict, outOfBandCancelDict);

        baseline.PaymentGateway.FinishServerSentEvents(point, JsonConvert.SerializeObject(payloadAsDict, Formatting.None));
      }
      else
      {
        sendNonSaleResponse();
      }



      return true;
    }

    /// <summary>
    /// Switch the session id if the user just logged in
    /// </summary>
    /// <param name="context"></param>
    /// <param name="SESSIONID"></param>
    /// <returns></returns>
    private static string SwitchSessionIdIfNeeded(System.Web.HttpContext context, string SESSIONID)
    {
      // Bug 17410 MJO - If the user just logged in, switch the session id
      if (CASLInterpreter.get_my().has("_logged_in"))
      {
        GenericObject my = CASLInterpreter.get_my();

        my.remove("_logged_in");

        // Code based upon https://stackoverflow.com/questions/1368403/generating-a-new-asp-net-session-in-the-current-httpcontext
        System.Web.SessionState.SessionIDManager manager = new System.Web.SessionState.SessionIDManager();
        string oldId = manager.GetSessionID(context);
        string newId = manager.CreateSessionID(context);

        // Bug 20435 UMN PCI-DSS
        string maskedID = manager.CreateSessionID(context);

        bool isAdd = false, isRedir = false;
        manager.SaveSessionID(context, newId, out isRedir, out isAdd);
        HttpApplication ctx = (HttpApplication)HttpContext.Current.ApplicationInstance;
        HttpModuleCollection mods = ctx.Modules;
        System.Web.SessionState.SessionStateModule ssm = (SessionStateModule)mods.Get("Session");
        System.Reflection.FieldInfo[] fields = ssm.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
        SessionStateStoreProviderBase store = null;
        System.Reflection.FieldInfo rqIdField = null, rqLockIdField = null, rqStateNotFoundField = null;
        System.Web.SessionState.SessionStateStoreData rqItemField = null;
        foreach (System.Reflection.FieldInfo field in fields)
        {
          if (field.Name.Equals("_store")) store = (SessionStateStoreProviderBase)field.GetValue(ssm);
          if (field.Name.Equals("_rqId")) rqIdField = field;
          if (field.Name.Equals("_rqLockId")) rqLockIdField = field;
          if (field.Name.Equals("_rqSessionStateNotFound")) rqStateNotFoundField = field;
          if (field.Name.Equals("_rqItem")) rqItemField = (System.Web.SessionState.SessionStateStoreData)field.GetValue(ssm);
        }
        object lockId = rqLockIdField.GetValue(ssm);
        // Bug 17071 MJO - Changed to RemoveItem to invalidate the old session ID
        if ((lockId != null) && (oldId != null)) store.RemoveItem(context, oldId, lockId, rqItemField);
        if ((lockId != null) && (oldId != null) && (rqItemField != null)) store.RemoveItem(context, oldId, lockId, rqItemField);
        rqStateNotFoundField.SetValue(ssm, true);
        rqIdField.SetValue(ssm, newId);

        SessionValidator validator = SessionValidator.GetInstance();
        validator.updateSessionID(oldId, newId);
        SESSIONID = newId;
        my.set("SessionID", newId);

        // Bug 20435 UMN PCI-DSS
        my.set("MaskedSessionID", maskedID);

        // Bug 19801: Log the login with the new session, not the old browser session.
        // This was moved from Authentication_f_controller.casl so I could capture the new session.
        string login_user = (string)my.get("login_user", "unknown");
        string workgroup = (string)my.get("login_department", "");
        string application_name = (string)my.get("application_name", "");

        StringBuilder sb = new StringBuilder();
        if (!string.IsNullOrEmpty(workgroup))
          sb.Append("workgroup:").Append(workgroup);

        //#if DEBUG
        //	  CASL_Stack.verify(null);
        //#endif

        misc.CASL_call("a_method", "actlog", "args",
          new GenericObject(
            // Bug 20435 UMN PCI-DSS don't pass session
            "user", login_user,
            "app", application_name,
            "action", "Logon",
            "summary", "Success",
            "detail", sb.ToString()));
        // End Bug 19801
      }
      return SESSIONID;
    }

    /// <summary>
    /// Handle some requests aren't handled by the main process request flow. These include post_remote, 
    /// get_batch_update_activity_progress and get_formula_validation.
    /// </summary>
    /// <param name="context"></param>
    /// <param name="raw_uri"></param>
    /// <param name="a_http_request"></param>
    /// <param name="http_method"></param>
    /// <param name="content"></param>
    /// <param name="a_server"></param>
    /// <param name="bDontProcessAgain"></param>
    /// <returns>true or false</returns>
    private static bool ProcessSpecialRequests(System.Web.HttpContext context, ref String raw_uri,
      System.Web.HttpRequest a_http_request, string http_method, ref string content, ref GenericObject a_server,
      ref bool bDontProcessAgain, bool is_casl_ide_request, bool is_test_suite_identification)
    {
      // Bug 20380 UMN PCI/PA-DSS Disallow .inspect queries if CASL IDE is disabled
      if (is_casl_ide_request && !c_CASL.GEO.has("casl_ide"))
      {
        context.Response.Clear();
        context.Response.StatusCode = 404;
        context.Response.Write(@"<html><head><center><title>Not Allowed</title></head>
          <body style='font-family: Arial, Sans-serif;'><h1>Invalid Request or Not Found</h1></center></body></html>");
        if (context.Response.IsClientConnected)
          context.Response.Flush();
        context.ApplicationInstance.CompleteRequest(); // BUG 16461 replace Response.Close() with CompleteRequest()
        return false;
      }

      // We record the start of a new test to the normal log,
      // the cs_log, and the flight recorder.
      if (is_test_suite_identification)
      {
        string arg = "?args=";
        int p = raw_uri.IndexOf(arg) + arg.Length;
        string test_name = HttpUtility.UrlDecode(raw_uri.Substring(p));

        // strip enclosing "'s.
        if (test_name[0] == '"' && test_name[test_name.Length - 1] == '"')
          test_name = test_name.Substring(1, test_name.Length - 2);

        // Cleanup duplicate from NUnit's script
        p = test_name.IndexOf("&");
        if (p > 0)
        {
          string n1 = test_name.Substring(0, p);
          string n2 = test_name.Substring(p + 1);
          if (n1 == n2)
            test_name = n1;
        }

        Logger.LogInfo(test_name, "Starting Test");
        Logger.cs_log("Starting Test: " + test_name);
        if (Flt_Rec.go(Flt_Rec.where_t.always_flag))
          Flt_Rec.log("Test suite test identification", test_name);
        return false;
      }

      if (http_method == "POST")
      {
        //---------------------------------------------------
        // BUG# 7564 Batch Update DH 
        if (raw_uri.IndexOf("get_batch_update_activity_progress") > 0)
        {
          if (c_CASL.GEO.has("BatchUpdateActivityProgress"))
          {
            string strBatchUpdateActivityProgress = c_CASL.GEO.get("BatchUpdateActivityProgress").ToString();
            context.Response.Write(strBatchUpdateActivityProgress);
            if (context.Response.IsClientConnected)
              context.Response.Flush();
            context.ApplicationInstance.CompleteRequest(); // BUG 16461 replace Response.Close() with CompleteRequest()
          }
          return false;
        }
        // end BUG# 7564 DH
        //---------------------------------------------------

        // Bug 16433 [21921] UMN - add using 
        using (StreamReader a_stream_reader = new System.IO.StreamReader(a_http_request.InputStream, a_http_request.ContentEncoding))
        {
          content = a_stream_reader.ReadToEnd();
          a_stream_reader.Close();
        }

        // Uncomment when DEBUG on CASL_engine conforms to global setting.  GMB.
        //#if DEBUG
        //	  CASL_Stack.verify(null);
        //#endif

        //----------------------------------------------------------
        // Bug 12098 - iPayment Baseline: Addition of Formulas [DJD]
        if (raw_uri.IndexOf("get_formula_validation") > 0)
        {
          string strInvalid = misc.ValidateFormulaMethods_aux(ref content);
          if (strInvalid.Length > 0)
          {
            context.Response.Write(strInvalid);
            if (context.Response.IsClientConnected)
              context.Response.Flush();
            context.ApplicationInstance.CompleteRequest(); // BUG 16461 replace Response.Close() with CompleteRequest()
            return false;
          }

          string strXMLParseErr = misc.ValidateFormulaXMLParse(ref content);
          if (strXMLParseErr.Length > 0)
          {
            strXMLParseErr = "Parsing Error: " + strXMLParseErr;
            context.Response.Write(strXMLParseErr);
          }
          else
          {
            // Try executing formula with test values
            string strExeTestVals = "";
            strExeTestVals = misc.ExecuteFormulaTestValues(content);
            context.Response.Write("VALID! " + strExeTestVals);
          }

          if (context.Response.IsClientConnected)
            context.Response.Flush();
          context.ApplicationInstance.CompleteRequest(); // BUG 16461 replace Response.Close() with CompleteRequest()
          return false;
        }
        // end Bug 12098
        //----------------------------------------------------------

        // BUG# 9523 DH - Replaced the content type on the client and modified the server parsing code
        // to recreate key-value pair form data structure.
        if (raw_uri.IndexOf("post_remote") > 0)
        {
          // 16733 remove ? (.NET 4.5 leave ? as part of POST URL), double check in case browser cache older javascript post_remote
          raw_uri = raw_uri.Replace("?", "");
          a_server = CASL_server.process_request(
              "a_uri", raw_uri + "?" + content, "http_method", http_method, "content_type", "application/x-www-form-urlencoded", "content", content) as GenericObject; // return a server
          bDontProcessAgain = true;
          return true;
        }
        // BUG# 9523 DH
        return true;
      }
      return true;
    }

    /// <summary>
    /// Sets the browser variable.
    /// </summary>
    /// <param name="a_http_request"></param>
    /// <param name="browser"></param>
    /// <returns>String name of the browser</returns>
    private static string SetBrowser(System.Web.HttpRequest a_http_request, string browser)
    {
      // Bug 21856 UMN allow sites to force IE11 if they have standardized on it
      string force_browser = ((string)c_CASL.GEO.get("force_browser", browser)).ToLower();

      // Bug 15003/21856 UMN - IE11 reports as "InternetExplorer", not "IE" as before
      if (browser == "InternetExplorer" || force_browser == "ie11")
      {
        browser = "Firefox"; // Bug 15105 treat IE11 as the gecko browser it self-reports as
      }

      // Bug 15105/21856 UMN - make Edge report as Edge, not Chrome (DJD: Make certain UserAgent is not null).
      if (force_browser == "edge" || a_http_request.UserAgent != null && a_http_request.UserAgent.Contains("Edge/1"))
      {
        browser = "Edge";
      }
      return browser;
    }

    /// <summary>
    /// Redirect to cbs.start or pos.start if we arrived via a POST. Do this because of issues with IE's inability
    /// to handle URL agruments longer than 2083 characters. When an XML integration API request is sent (for
    /// example with Epic), it can be quite a bit larger than 2K. Returns false if a redirect was done, else true.
    /// </summary>
    /// <param name="raw_uri"></param>
    /// <param name="is_start_request"></param>
    /// <param name="a_http_request"></param>
    /// <param name="http_method">Returns false if a redirect was done, else true.</param>
    private static bool RedirectToStartWithoutArgs(String raw_uri, bool is_start_request, System.Web.HttpRequest a_http_request, string http_method)
    {
      GenericObject my = CASLInterpreter.get_my();

      /* 
       * Bug 25213 UMN
       * So as we try to eliminate the dreaded NO ARGS, we have a couple of cases here because we're trying to preserve 
       * backward compatibility of secure transfer *and* iPayment. The latest version of secure transfer will send the
       * token to ipayment if the securetransfer.config file has <send_token>true</send_token>. If a token is NOT sent
       * either because the FAE goofed up, or an older secure transfer is used, then we need to preserve the behavior
       * of iPayment before the token changes.
      */
      if (is_start_request)
      {
        if (http_method == "POST")
        {
          string par = GetArgFromHttpRequest(a_http_request, "par");
          string token = GetArgFromHttpRequest(a_http_request, "token");

          if (!String.IsNullOrWhiteSpace(par))
          {
            // handle old secure transfer
            if (String.IsNullOrWhiteSpace(token))
            {
              my.set("par", par);

              // hack off par from raw_uri and redirect to start.
              string newURI = raw_uri.Substring(0, raw_uri.IndexOf("?"));

              HttpContext.Current.Response.Write("<html><body><a href='" + newURI + "'>Redirecting</a></body></html>");
              HttpContext.Current.Response.RedirectLocation = newURI;
              HttpContext.Current.ApplicationInstance.CompleteRequest();
              return false;
            }
            else
            {
              _parArgsDict[token] = par;

              // hack off arguments from raw_uri and redirect to start.
              string newURI = raw_uri.Substring(0, raw_uri.IndexOf("?"));

              HttpContext.Current.Response.Write("<html><body><a href='" + newURI + "'>Redirecting</a></body></html>");
              HttpContext.Current.Response.RedirectLocation = String.Format("{0}?token={1}", newURI, token);
              HttpContext.Current.ApplicationInstance.CompleteRequest();
              return false;
            }
          }
          return true;
        }
        else if (raw_uri.Contains("&token=") || raw_uri.Contains("?token="))  // don't want to match auth_token!
        {
          string token = GetArgFromHttpRequest(a_http_request, "token");
          if (!String.IsNullOrWhiteSpace(token) && _parArgsDict.ContainsKey(token))
          {
            if (_parArgsDict.TryRemove(token, out string par))
            {
              my.set("par", par);
            }
          }
          return true;
        }
      }
      return true;
    }

    private static String GetArgFromHttpRequest(System.Web.HttpRequest a_http_request, string arg)
    {
      var postString = a_http_request.Form[arg];
      var queryString = a_http_request.QueryString[arg];
      return (postString != null) ? postString : queryString;
    }

    /// <summary>
    /// Verify the CRSF tokens that we use. Returns true if tokens are valid, else false.
    /// Bug 16648 MJO
    /// </summary>
    /// <param name="context"></param>
    /// <param name="SESSIONID"></param>
    /// <param name="raw_uri"></param>
    /// <param name="is_casl_ide_request"></param>
    /// <param name="a_http_request"></param>
    /// <returns>Returns true if tokens are valid, else false</returns>
    private static bool VerifyCSRFValues(System.Web.HttpContext context, string SESSIONID, String raw_uri, bool is_casl_ide_request, System.Web.HttpRequest a_http_request)
    {
      // Bug 16648 MJO - Check to make sure the double-submit value is present in the request and matches the cookie
      // Bug 17513 MJO - Always check for __DOUBLESUBMIT__ (you could skip it by not passing any key/value pairs)
      // Bug 26537 MJO - Let MiraPay response call through as it doesn't support doublesubmit
      if (!is_casl_ide_request && a_http_request.FilePath.IndexOf("/my/") != -1 && !raw_uri.Contains("receive_mirasecure_response.htm"))
      {
        // Look in the query string first
        string doubleSubmitVal = a_http_request.QueryString["__DOUBLESUBMIT__"];

        // If not in the query string, look in the form data
        if (doubleSubmitVal == null)
        {
          doubleSubmitVal = a_http_request.Form["__DOUBLESUBMIT__"];
          doubleSubmitVal = HttpUtility.UrlDecode(doubleSubmitVal);
        }

        // Bug 17410 MJO - Skip manipulation of the string if doubleSubmitVal is null
        if (doubleSubmitVal != null)
        {
          // Sometimes the HTML decode messes up the plus signs, so restore them
          doubleSubmitVal = doubleSubmitVal.Replace(' ', '+');

          // There's a bug in IE that sometimes causes duplicate input fields to be created. Ignore any extras
          if (doubleSubmitVal.Contains(","))
            doubleSubmitVal = doubleSubmitVal.Substring(0, doubleSubmitVal.IndexOf(','));
        }

        // Perform the actual check
        // Bug 17410 MJO - Add site name to cookie to prevent clashing between iPayment sessions
        if (context.Request.Cookies.Get("__DOUBLESUBMIT__" + misc.get_site_name()) == null
          || doubleSubmitVal == null
          || context.Request.Cookies.Get("__DOUBLESUBMIT__" + misc.get_site_name()).Value != doubleSubmitVal)
        {
          // Bug 17513 MJO - Return 403 Forbidden
          context.Response.StatusCode = 403;
          if (context.Response.IsClientConnected)
            context.Response.Flush();

          // Bug 17513 MJO - Log the failure
          string correctVal = context.Request.Cookies.Get("__DOUBLESUBMIT__" + misc.get_site_name()) == null ? "MISSING!" : context.Request.Cookies.Get("__DOUBLESUBMIT__" + misc.get_site_name()).Value;
          string logUri = raw_uri;

          if (raw_uri.IndexOf("?") != -1)
            logUri = raw_uri.Substring(0, raw_uri.IndexOf("?"));

          string errorMsg = String.Format("CSRF Failure: DOUBLESUBMIT value is missing or incorrect.{0}Session ID: {2}{0}URI: {1}{0}Expected value: {3}{0}Actual value:   {4}",
            Environment.NewLine, logUri, SESSIONID, correctVal, doubleSubmitVal);

          Logger.cs_log(errorMsg);

          AbandonSession(context);
          return false;
        }
        return true;
      }
      return true;
    }


    /// <summary>
    /// Compares the source and target HTTP errors and will modify the HTTP response to indicate CSRF failure if they don't match.
    /// Bug 20280 MJO
    /// </summary>
    /// <param name="context"></param>
    /// <param name="SESSIONID"></param>
    /// <param name="raw_uri"></param>
    /// <param name="is_start_request"></param>
    /// <returns></returns>
    private static bool CompareSourceAndTargetHeaders(System.Web.HttpContext context, string SESSIONID, String raw_uri, bool is_start_request)
    {
      string uri5 = context.Request.RawUrl.Split('/').Length > 5 ? context.Request.RawUrl.Split('/')[5] : null;
      int parseResult;
      bool isInt = uri5 == null ? false : Int32.TryParse(uri5, out parseResult);

      // Bug 22170 MS/UMN
      bool bPar = ((GenericObject)System.Web.HttpContext.Current.Session["my"]).Contains("par");

      if (!is_start_request && !bPar && !isInt &&
          (context.Request.RawUrl.Split('/').Length < 5 || context.Request.RawUrl.Split('/')[4] != "my"))
      {
        NameValueCollection headers = context.Request.Headers;
        string sourceOrigin = null;
        string targetOrigin = null;

        if (headers["Origin"] != null)
          sourceOrigin = headers["Origin"].Split('/')[2];
        else if (headers["Referer"] != null)
          sourceOrigin = headers["Referer"].Split('/')[2];

        // Bug 26537 MJO - Let MiraPay response call through
        if (raw_uri.Contains("receive_mirasecure_response.htm"))
          return true;

        if (headers["X-Forwarded-Host"] != null)
          targetOrigin = headers["X-Forwarded-Host"];
        else if (headers["Host"] != null)
          targetOrigin = headers["Host"];

        // UMN Qualify sourceOrigin or targetOrigin if it's set to localhost
        if (sourceOrigin == "localhost") sourceOrigin = GetIPAddress();
        if (targetOrigin == "localhost") targetOrigin = GetIPAddress();

        if (sourceOrigin != null && sourceOrigin != targetOrigin)
        {
          context.Response.StatusCode = 403;
          if (context.Response.IsClientConnected)
            context.Response.Flush();

          string logUri = raw_uri;

          if (raw_uri.IndexOf("?") != -1)
            logUri = raw_uri.Substring(0, raw_uri.IndexOf("?"));

          if (sourceOrigin == null)
            sourceOrigin = "Missing";

          if (targetOrigin == null)
            targetOrigin = "Missing";

          string errorMsg = String.Format("CSRF Failure: Source and target origin headers do not match.{0}Session ID: {2}{0}URI: {1}{0}Source Origin: {3}{0}Target Origin: {4}",
            Environment.NewLine, logUri, SESSIONID, sourceOrigin, targetOrigin);

          Logger.cs_log(errorMsg);
          AbandonSession(context);
          return false;
        }
        return true;
      }
      return true;
    }

    // Bug 23231 MJO - Paths to be excluded from GET/POST checking because both requests are valid
    private static List<string> PostExemptions = new List<string> { "a_file_balancing_app/a_sub_app/finish_deposit.htm" };

    /// <summary>
    /// Appends the HTTP POST arguments to the URL if they are present.
    /// </summary>
    /// <param name="context"></param>
    /// <param name="raw_uri"></param>
    /// <returns></returns>
    private static string AppendPostArgsToURL(System.Web.HttpContext context, String raw_uri, string SESSIONID)
    {
      String post = context.Request.Form.ToString(); // use InputStream instead
      string app_path = context.Request.ApplicationPath; // "/corebt" 
      raw_uri = raw_uri.Substring(raw_uri.IndexOf(app_path, StringComparison.InvariantCultureIgnoreCase));

      // Bug 21975 MJO - Make sure that a URI that's supposed to be called with a POST isn't called using GET instead
      if (!(bool)c_CASL.GEO.get("disable_post_get_check", false))
      {
        GenericObject my = CASLInterpreter.get_my();
        HashSet<string> postMethods = my.get("post_methods", new HashSet<string>()) as HashSet<string>;

        // Bug 23231 MJO - Skip certain calls
        bool skipCheck = false;

        foreach (string str in PostExemptions)
        {
          if (raw_uri.Contains(str))
          {
            skipCheck = true;
            break;
          }
        }

        if (!skipCheck)
        {
          //Bug 23231 MJO - Rewrote to use HashSet instead of GEO, removing a for loop
          string baseUri = raw_uri;

          if (raw_uri.IndexOf("?") != -1)
            baseUri = raw_uri.Substring(0, raw_uri.IndexOf("?"));

          if (context.Request.HttpMethod == "GET" && postMethods.Contains(baseUri))
          {
            //Bug IPAY-722 SM - Add query string parameters to log but leave out sensitive data.                       
            StringBuilder fields = new StringBuilder();
            String requestBody = context.Request.Form.ToString();
            NameValueCollection parameters = HttpUtility.ParseQueryString(requestBody);
            var items = parameters.AllKeys;

            foreach (var item in items)
            {
              string paramValue = HttpUtility.ParseQueryString(requestBody).Get(item);

              if (!item.Equals("args.credit_card_nbr") &&
                  !item.Equals("credit_card_nbr") &&
                  !item.Equals("args.credit_card_nbr_en") &&
                  !item.Equals("args.ccv") &&
                  !item.Equals("args.password") &&
                  !item.Equals("password") &&
                  !item.Equals("args.track") &&
                  !item.Equals("args.new_value") &&
                  !item.Equals("args.bank_account_nbr") &&
                  !item.Equals("args.bank_routing_nbr"))
              {
                fields.Append(item + "=" + " " + paramValue + Environment.NewLine);
              }
              else
              {
                fields.Append(item + Environment.NewLine);
              }

            }

            string errorMsg = String.Format("Security Failure: POST parameters were sent in a GET request.{0}Session ID: {2}{0}URI: {1} Parameters: {0}{3}",
            Environment.NewLine, baseUri, SESSIONID, fields);

            Logger.cs_log(errorMsg);
            return "";
          }
        }
      }

      // context.Request.QueryString.ToString();  // Hack.  MP  QueryString.ToString() should be efficient
      if (post != null && post != "")
      {
        // raw_uri might have a query string.  Ex: /pos_demo/a_app.htm?_args=   Want: /pos_demo/a_app.htm?username=foo&pw=pw
        if (raw_uri.IndexOf("?") > 0)
        {
          if (!raw_uri.EndsWith("?")) raw_uri += "&";
        }
        else
          raw_uri += "?";
        raw_uri += post;
      }
      return raw_uri;
    }



    /// <summary>
    /// Initializes the my, and the session for the request
    /// </summary>
    /// <param name="context"></param>
    /// <param name="SecureCookies"></param>
    private static void InitializeSession(System.Web.HttpContext context, bool SecureCookies, string raw_uri)
    {

      // initialize_server_session_if_necessary
      if (System.Web.HttpContext.Current != null
        && System.Web.HttpContext.Current.Session != null
        && System.Web.HttpContext.Current.Session["my"] == null
        )
      {
        misc.clear_and_set_session();

        // Bug 16648 MJO - Generate the double-submit value and set it both as a cookie and in my
        RandomNumberGenerator rng = new RNGCryptoServiceProvider();

        byte[] tokenData = new byte[32];
        rng.GetBytes(tokenData);

        CASLInterpreter.get_my().set("__DOUBLESUBMIT__", Convert.ToBase64String(tokenData));

        string[] uri_array = raw_uri.Split('/');

        if (uri_array.Length > 1)
        {
          string gateway_uri = String.Format("/{0}/GATEWAYPOST.htm", uri_array[1]);

          CASLInterpreter.get_my().set("post_methods", new HashSet<string> { gateway_uri });
        }

        // Bug 17410 MJO - Add site name to cookie to prevent clashing between iPayment sessions
        // Bug 18085 MJO - Pass in SecureCookie
        var cookie = CreateSecureCookie("__DOUBLESUBMIT__" + misc.get_site_name(), Convert.ToBase64String(tokenData), SecureCookies);
        context.Response.Cookies.Add(cookie);
      }
    }

    /// <summary>
    /// Shows the mapped error page
    /// </summary>
    /// <param name="context"></param>
    private static void ShowErrorPage(System.Web.HttpContext context)
    {
      // Show error page upon error of initial loading process (exclude casl ide, reload and server local access) 
      string custom_error_page_path = ((c_CASL.GEO.get("error",
          new GenericObject()) as GenericObject).get("custom_error_page_path",
            new GenericObject()) as GenericObject).get("325", "") as string;
      if (custom_error_page_path != "")
      {
        custom_error_page_path = custom_error_page_path.Replace("[site_static]", c_CASL.GEO.get("site_static", "") as string);
        // BUG 16461
        // UMN always check before redirecting
        HttpContext.Current.Response.Clear();
        if (!context.Response.IsRequestBeingRedirected)
          context.Response.Redirect(custom_error_page_path, false);
        context.ApplicationInstance.CompleteRequest();
      }
      else
      {
        context.Response.Write("An error occurred during iPayment system loading.&nbsp; Please call your system administrator. (325)");
        if (context.Response.IsClientConnected)
          context.Response.Flush();
        context.ApplicationInstance.CompleteRequest(); // BUG 16461 replace Response.Close() with CompleteRequest()
      }
    }

    private static void ShowLoadingPage(System.Web.HttpContext context,
                                        bool initial_request)
    {
      string custom_error_page_path = "";
      if (!initial_request)
        custom_error_page_path = ((c_CASL.GEO.get("error",
            new GenericObject()) as GenericObject).get("custom_error_page_path",
              new GenericObject()) as GenericObject).get("324", "") as string;
      if (custom_error_page_path != "")
      {
        custom_error_page_path = custom_error_page_path.Replace("[site_static]", c_CASL.GEO.get("site_static", "") as string);

        // BUG 16461
        HttpContext.Current.Response.Clear();
        if (!context.Response.IsRequestBeingRedirected)
          context.Response.Redirect(custom_error_page_path, false);
        context.ApplicationInstance.CompleteRequest();
      }
      else
      {
        context.Response.Write("iPayment system is still loading. Please try again in a few minutes. (324)");
        if (context.Response.IsClientConnected)
          context.Response.Flush();
        context.ApplicationInstance.CompleteRequest(); // BUG 16461 replace Response.Close() with CompleteRequest()
      }
    }

    /// <summary>
    /// Strtup casl engine, init session, etc. Returns false on error.
    /// </summary>
    /// <param name="context"></param>
    /// <returns>Returns false on error</returns>
    private static bool StartupIPayment(System.Web.HttpContext context)
    {
      // initialize CASL engine and session
      // redo following to better handle errors from initialize_casl_engine call above.
      try
      {
        // Bug #9348 Mike O - Prevents multiple threads from trying to
        // load CASL at the same time
        load_mutex.WaitOne();

        if (HttpContext.Current.Application["LOADING_STATUS"] as string == "PRE")
        {
          HttpContext.Current.Application["LOADING_STATUS"] = "LOADING"; // BUG#5532 HANDLE LOADING ISSUE SX 06/04/2008

          // Bug #9348 Mike O - Now that the status is set to LOADING, other threads may continue and show the loading message
          load_mutex.ReleaseMutex();

          object R_init = c_CASL.initialize_casl_engine();

          if ((HttpContext.Current.Application["LOADING_STATUS"] as string) != "ERROR")
          {
            // Bug 20642 UMN responsive UI - load and parse templates during start-up
            // Move it here once debugged
            // TemplateLoader.Instance.LoadTemplates(c_CASL.GEO.get("baseline_physical") + "/business/templates/");

            // Bug 15205 UMN Start Web services by calling them. This should speed up the first inquiry
            StartPBWebServices();
            
            StartHangfire();

            HttpContext.Current.Application["LOADING_STATUS"] = "FINISHED"; // BUG#5532 HANDLE LOADING ISSUE SX 06/04/2008
            return true;
          }
          else
          {
            context.Response.Write(R_init.ToString());
            context.Response.ContentType = "text/html";
            if (context.Response.IsClientConnected)
              context.Response.Flush();
            context.ApplicationInstance.CompleteRequest(); // BUG 16461 replace Response.Close() with CompleteRequest()
            return false;
          }
        }
        else
        {
          // Bug #9348 Mike O - If this thread wasn't the one to start CASL, have it release its lock
          load_mutex.ReleaseMutex();
        }
        return true;
      }
      catch (CASL_error ce)
      {
        // BUG#5532 HANDLE LOADING ISSUE SX 06/04/2008
        HttpContext.Current.Application["LOADING_STATUS"] = "ERROR";

        // Put BP <BP/> Breakpoint here to look for loading errors.
        GenericObject the_error = ce.casl_object;

        // Low-level indication that an error occurred
        // Bug 25532 MJO - Improved logging
        Logger.cs_log("CASL Error on load.");
        Logger.cs_log("Message: " + the_error.get("message", "no message"));
        string trace = ce.ToMessageAndCompleteStacktrace(); // HX: Bug 17849
        Logger.cs_log("Traceback Message: " + trace);

        // BUG #5630 Email Notification SX 06/10/08
        try
        {
          SendLoadFailedEmail(the_error);
        }
        catch
        {
        }

        // BUG #5630 Email Notification SX 06/10/08
        SendLoadFailedEmail(the_error);

        // OR ce.casl_object.very_last_frame."System.Collections.Hashtable".local_env.the_error.message
        //misc.error_add_stack_trace(the_error, ce.StackTrace); 
        //object formatter = CASL_server.get_format_top("obj",the_error, "name","htm");

        //string message = misc.CASL_call("a_method",formatter,
        //    "subject",the_error ) as string;
        //       BS=Body string "args",
        //         new GenericObject("_subject",the_error)
        // SIMPLE FAILS TOO: string message =
        //      misc.CASL_call("a_method",formatter,
        //      "subject",new GenericObject(
        //         GO_kind.unknown, "_parent",c_CASL.GEO, "x",10) ) as string; 
        //string message = misc.CASL_to_xml_large_inst("_subject",the_error);
        string message = (the_error != null) ? (string)the_error.get("message") : ce.ToString();
        context.Response.Write(message);
        context.Response.ContentType = "text/html";
        if (context.Response.IsClientConnected)
          context.Response.Flush();
        context.ApplicationInstance.CompleteRequest(); // BUG 16461 replace Response.Close() with CompleteRequest()

        // Try to log the error
        Logger.cs_log("Error during load process: " + the_error.get("message", "no message"));

        return false;
      }
      catch (Exception ce)
      {
        //Low-level indication that an error occurred
        Logger.cs_log("Error on load.");

        // BUG#5532 HANDLE LOADING ISSUE SX 06/04/2008
        HttpContext.Current.Application["LOADING_STATUS"] = "ERROR";
        Exception ie = ce.InnerException;

        // normal 
        string title = "";
        string message = "";
        /*if (ie is CASL_error)
        {
          title = "Runtime Error during loading";
          message = ie.Message + ie.StackTrace;
         
        }
        else if (ie == null)
        {
          title = "Runtime Error during loading";
          message = ce.Message + ce.StackTrace;
        }
        else
        {
          title = "Runtime Error during loading";
          message = ie.Message + ie.StackTrace;
        }*/

        title = "Runtime Error during loading";
        message = ce.ToMessageAndCompleteStacktrace(); // HX: Bug 17849
        // Low-level message log
        Logger.cs_log("Message: " + message);

        // BUG #5630 Email Notification SX 06/10/08
        SendLoadFailedEmail(new GenericObject("Error", message));

        context.Response.Write(title + ";");
        context.Response.Write(message);
        if (context.Response.IsClientConnected)
          context.Response.Flush();
        context.ApplicationInstance.CompleteRequest(); // BUG 16461 replace Response.Close() with CompleteRequest()
        return false;
      }
    }

    /// <summary>
    /// Sends an email with the error, and logs email send errors
    /// </summary>
    /// <param name="the_error"></param>
    private static void SendLoadFailedEmail(GenericObject the_error)
    {
      // Bug 15390 UMN adding exception handler, so loading errors don't cause white screen when email send fails
      try
      {
        emails.CASL_send_email_notification(
          "module", "iPayment Enterprise",
          "action", "Loading iPayment System",
          "email_subject", "Loading iPayment System Failed. ",
          "error", the_error);
      }
      catch (Exception e)
      {
        // HX: Bug 17849, should I email inner exception message and stack trace (HX:??)
        string msg = "Exception sending email: " + " Error: " + e.ToMessageAndCompleteStacktrace();
        Logger.LogError(msg, "EMAIL SEND");
      }
    }

    /// <summary>
    /// Removes the double submit values from the URL or form data and returns the possibly modified raw_uri
    /// </summary>
    /// <param name="raw_uri"></param>
    /// <param name="a_http_request"></param>
    /// <returns>The raw_uri</returns>
    private static string PurgeDoubleSubmitInfo(String raw_uri, System.Web.HttpRequest a_http_request)
    {
      PropertyInfo isreadonly =
        typeof(System.Collections.Specialized.NameValueCollection).GetProperty(
        "IsReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);

      // Remove the DOUBLESUBMIT argument from the query string or form data
      if (a_http_request.QueryString["__DOUBLESUBMIT__"] != null)
      {
        isreadonly.SetValue(a_http_request.QueryString, false, null);

        a_http_request.QueryString.Remove("__DOUBLESUBMIT__");
      }
      if (a_http_request.Form["__DOUBLESUBMIT__"] != null)
      {
        isreadonly.SetValue(a_http_request.Form, false, null);

        a_http_request.Form.Remove("__DOUBLESUBMIT__");
      }

      // Remove the DOUBLESUBMIT argument from the raw_uri
      // Bug 17513 MJO - Keep trying until they're all gone, as IE sometimes includes it twice
      while (raw_uri.IndexOf("__DOUBLESUBMIT__") != -1)
      {
        int startIndex = raw_uri.IndexOf("__DOUBLESUBMIT__");

        int endIndex = raw_uri.IndexOf("%3d", startIndex);

        if (endIndex == -1)
          raw_uri = raw_uri.Substring(0, startIndex);
        else if (raw_uri.Length > endIndex + 3)
          raw_uri = raw_uri.Substring(0, startIndex) + raw_uri.Substring(endIndex + 4);
        else
          raw_uri = raw_uri.Substring(0, startIndex) + raw_uri.Substring(endIndex + 3);

        // If the DOUBLESUBMIT value was removed, also remove the question mark and RQM argument if the latter is present
        if (a_http_request.QueryString["__RQM__"] != null)
        {
          isreadonly.SetValue(a_http_request.QueryString, false, null);

          a_http_request.QueryString.Remove("__RQM__");

          startIndex = raw_uri.IndexOf("__RQM__");

          if (startIndex != -1)
            raw_uri = raw_uri.Substring(0, startIndex) + raw_uri.Substring(startIndex + 9);

          // Bug 17513 MJO - Combine statements
          if (raw_uri.EndsWith("?&") || raw_uri.EndsWith("?"))
            raw_uri = raw_uri.Substring(0, raw_uri.Length - 1);
        }
      }
      return raw_uri;
    }

    /// <summary>
    /// Processes requests to static files
    /// </summary>
    /// <param name="context"></param>
    /// <returns></returns>
    private bool ProcessRequestStatic(System.Web.HttpContext context)
    {
      if ((context.Request.HttpMethod != "GET") || (context.Request.QueryString.Count != 0))
        return false;

      HttpRequest request = context.Request;
      string fileName = request.PhysicalPath;

      // BUG 24967 RDM - Removing the version number for the file name
      if ((string)HttpContext.Current.Application["LOADING_STATUS"] == "FINISHED") // check first, GEO.hypertext must exist otherwise we get in redirect loop on load
      {
        GenericObject hypertext = (GenericObject)c_CASL.c_object("GEO.hypertext");
        string file_version = (string)hypertext.get("file_version");
        fileName = fileName.Replace(file_version + ".", "");
      }

      FileInfo fi = new FileInfo(fileName);
      if (!fi.Exists) return false;

      HttpResponse response = context.Response;
      string strHeader = request.Headers["If-Modified-Since"];
      if (strHeader != null)
      {
        DateTime dtIfModifiedSince;
        if (DateTime.TryParseExact(strHeader, "r", null, DateTimeStyles.None, out dtIfModifiedSince))
        {
          DateTime ftime;
          ftime = fi.LastWriteTime.ToUniversalTime();
          if (ftime <= dtIfModifiedSince.AddDays(1))  // MP: BIG HACK.  ftime was giving 633417992327959168, but dtIfModifiedSince was giving a truncated 633417992320000000.
          {
            response.StatusCode = 304;
            return true;
          }
        }
      }

      // Bug 17335 UMN - Use secure transport header if site was loaded with SSL
      // THIS HEADER must never be used with HTTP!
      if (context.Request.IsSecureConnection)
      {
        context.Response.AppendHeader("Strict-Transport-Security", "max-age=31536000; includeSubDomains");
      }

      DateTime lastWT = fi.LastWriteTime.ToUniversalTime();
      response.AddHeader("Last-Modified", lastWT.ToString("r"));

      string extension = fi.Extension;
      response.ContentType = getMimeType(extension);

      // BUG 24967 RDM - Adding caching for 1 year to min.js/min.css files
      if (context.Request.RawUrl.Contains(".min.js") || context.Request.RawUrl.Contains(".min.css"))
      {
        context.Response.AppendHeader("Cache-Control", "private, max-age=31536000");
      }

      //TransmitFile
      if (fileName != null)
      {
        response.WriteFile(fileName);
        if (response.IsClientConnected)
          response.Flush();
        context.ApplicationInstance.CompleteRequest(); // BUG 16461 replace Response.Close() with CompleteRequest()
      }
      else
      {
        throw new HttpException(403, "Forbidden.");
      }
      return true;
    }

    //BUG#14781 Ignore Fake Certificate for calling PB web service with https
    public static bool IgnoreCertificateErrorHandler(object sender,
      X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
    {
      return true;
    }

    /// <summary>
    /// Get the IP address. Used to qualify localhost when comparing source and target headers.
    /// </summary>
    /// <returns></returns>
    private static string GetIPAddress()
    {
      string localIP = "127.0.0.1";
      using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0))
      {
        try
        {
          socket.Connect("8.8.8.8", 65530);
          IPEndPoint endPoint = socket.LocalEndPoint as IPEndPoint;
          localIP = endPoint.Address.ToString();
        }
        catch
        {
        }
      }
      return localIP;
    }

    #region Mime Mapping
    private static string APPLICATION_UNKNOWN = "application/unknown";
    private static Dictionary<string, string> mimeMap = null;
    private static Dictionary<string, string> getMimeMap()
    {
      if (mimeMap == null)
      {
        mimeMap = new Dictionary<string, string>(6);
        mimeMap.Add(".css", "text/css");
        mimeMap.Add(".htm", "text/html");
        mimeMap.Add(".js", "application/x-javascript");
        mimeMap.Add(".jpg", "image/jpg");
        mimeMap.Add(".gif", "image/gif");
        mimeMap.Add(".png", "image/png");
      }
      return mimeMap;
    }
    private static string getMimeType(string extension)
    {
      if (getMimeMap().ContainsKey(extension))
      {
        return mimeMap[extension];
      }
      else
      {
        string type = APPLICATION_UNKNOWN;
        extension = extension.ToLower();
        Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey("MIME\\Database\\Content Type");
        foreach (string keyName in key.GetSubKeyNames())
        {
          Microsoft.Win32.RegistryKey temp = key.OpenSubKey(keyName);
          if (extension.Equals(temp.GetValue("Extension")))
          {
            type = keyName;
          }
        }
        mimeMap.Add(extension, type);
        return type;
      }
    }
    #endregion

    // Bug 18085 MJO - Pass in SecureCookies
    /// <summary>
    /// Create a secure cookie and make the cookie not accessible from Javascript
    /// </summary>
    /// <param name="name">Name of the cookie</param>
    /// <param name="value">Value of the cookie</param>
    /// <param name="SecureCookies">Whether secure cookies should be used (HTTPS only)</param>
    /// <returns></returns>
    private static HttpCookie CreateSecureCookie(string name, string value, bool SecureCookies)
    {
      var cookie = new HttpCookie(name, value);
      cookie.HttpOnly = true;                // Bug 17062 UMN PCI-DSS make cookie not accessible in Javascript

      // Bug 27175 MJO - Set SameSite for HTTPS only
      if (SecureCookies)
      {
        cookie.Secure = true;         // Bug 16648 MJO PCI-DSS make cookie secure
        cookie.SameSite = SameSiteMode.None; // Bug 26151 RDM Add SameSite=None to all cookies.
      }

      return cookie;
    }

    // Bug 21975 MJO - Pulled this out as the copypasta code was driving me nuts
    private static void AbandonSession(HttpContext context)
    {
      // Send the response before abandoning the session
      if (context.Response.IsClientConnected)
        context.Response.Flush();
      context.ApplicationInstance.CompleteRequest(); // BUG 16461 

      // Abandon the session and delete all session variables
      try
      {
        context.Session.Abandon();
        context.Session.Contents.Abandon();
        context.Session.Contents.RemoveAll();
      }
      catch
      {
      }
    }


    private static IEnumerable<IDisposable> GetHangfireServers()
    {
      // Bug 26521 MJO - Switch to SQLite to avoid dev/test servers fighting over executing jobs
      string connectionString = $"Data Source={HttpRuntime.AppDomainAppPath}\\SQLite\\Hangfire.sqlite;";

      GlobalConfiguration.Configuration
          .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
          .UseSimpleAssemblyNameTypeSerializer()
          .UseRecommendedSerializerSettings()
          .UseSQLiteStorage(connectionString);

      // Bug 26521 MJO - Only allow one worker to deal with a SQLite bug and because we don't want to run multiple jobs at once
      BackgroundJobServerOptions options = new BackgroundJobServerOptions { WorkerCount = 1 };

      yield return new BackgroundJobServer(options);
    }

    // Bug 26519 MJO - Initialize Hangfire job scheduler and update all jobs
    private static void StartHangfire()
    {
      try
      {
        Logger.cs_log("Begin Hangfire Initialization");

        HangfireAspNet.Use(GetHangfireServers);
        
        BackgroundJob.Enqueue(() => UpdateAllJobs());
      }
      catch (Exception e)
      {
        string msg = String.Format("Hangfire Initialization Error: {0}", e.ToMessageAndCompleteStacktrace());
        
        Logger.cs_log(msg);

        emails.CASL_send_email_notification(
          "module", "iPayment Hangfire",
          "action", "Error Initializing Hangfire: " + e.Message,
          "email_subject", "ERROR initializing Hangfire job scheduler");
      }
      finally
      {
        Logger.cs_log("End Hangfire Initialization");
      }

    }

    public static void UpdateAllJobs()
    {
      Logger.cs_log("Updating all jobs in Hangfire");
      CASL_Stack.Initialization();
      misc.CASL_call("_subject", c_CASL.execute("Business.Job"), "a_method", "update_all_jobs", "args", new GenericObject());
    }
  }
} //namespace corebt
