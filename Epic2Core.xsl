<?xml version="1.0" encoding="UTF-8"?>

<!-- 
    NOTE: This file is for transforming the Epic Hyperspace POS XML elements to xml elements that match the
          the tag names used in a particular project. This file will therefore be different for different
          customers.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:epic="urn:epicsystems-com:ResolutePB.2004-12.Services.PointOfSaleInterface"
                version="1.0">
  <xsl:output method="xml" indent="yes"/>

  <xsl:template match="/">
    <xsl:apply-templates select="/epic:POSCollectPaymentRequest"/>
    <xsl:apply-templates select="/epic:POSGiveRefundRequest"/>
  </xsl:template>

  <xsl:template match="/epic:POSCollectPaymentRequest">
    <request>
      <xsl:apply-templates select="epic:MessageID" mode="Seq"/>
      <system>
        <user_id>$user_id</user_id>
        <workgroup_id>$dept_id</workgroup_id>
        <auto_exit>true</auto_exit>
        <show_ui>1</show_ui>
      </system>
      <event>
        <field key="source">EpicPOSAPI</field>
        <xsl:apply-templates select = "epic:MRNID | epic:ExtendedItem"/>
      </event>
      <transactions>
        <container>
          <id>1</id>
          <field key="tran_type">$ctt_id</field>
          <field key="auto_continue">true</field>
          <field key="REFERENCE_ID">$ext_ref_id</field>
          <xsl:apply-templates select = "epic:Address | epic:City | epic:State"/>
          <xsl:apply-templates select = "epic:AccountID"/>
          <xsl:apply-templates select = "epic:MemberName"/>
          <xsl:apply-templates select = "epic:ZIP"/>
          <xsl:apply-templates select = "epic:UserID"/>
          <xsl:apply-templates select = "epic:MessageID"/>
          <group>
            <field key="id">EpicPatientInfoGroup</field>
            <xsl:apply-templates select = "epic:MRNID | epic:AccountID | epic:MemberName | epic:Address | epic:City | epic:State"/>
          </group>
          <group>
            <field key="id">$group_name</field>
            <xsl:apply-templates select="epic:LineItem"/>
          </group>
        </container>
      </transactions>
    </request>
  </xsl:template>

  <xsl:template match="/epic:POSGiveRefundRequest">
    <request>
      <xsl:apply-templates select="epic:MessageID" mode="Seq"/>
      <system>
        <user_id>$user_id</user_id>
        <workgroup_id>$dept_id</workgroup_id>
        <auto_exit>true</auto_exit>
        <show_ui>1</show_ui>
      </system>
      <event>
        <field key="source">EpicPOSAPI</field>
        <xsl:apply-templates select = "epic:MessageID | epic:UserID | epic:MRNID | epic:RefundReason | epic:ExtendedItem"/>
      </event>
      <transactions>
        <container>
          <id>1</id>
          <field key="tran_type">$ctt_id</field>
          <field key="auto_continue">true</field>
          <field key="REFERENCE_ID">$ext_ref_id</field>
          <xsl:apply-templates select = "epic:Address | epic:City | epic:State"/>
          <xsl:apply-templates select = "epic:AccountID"/>
          <xsl:apply-templates select = "epic:MemberName"/>
          <xsl:apply-templates select = "epic:ZIP"/>
          <group>
            <field key="id">$group_name</field>
            <xsl:apply-templates select="epic:LineItem"/>
          </group>
        </container>
      </transactions>
    </request>
  </xsl:template>

  <xsl:template match="epic:ExtendedItem">
    <field>
      <xsl:attribute name="key">
        <xsl:value-of select="epic:Name"/>
      </xsl:attribute>
      <xsl:value-of select="epic:Data"/>
    </field>
  </xsl:template>
  
  <!-- Might have been cleaner design to have a distinct RefundItem template
  where I could process only the items specific to Refunds (and transform the amount to a negative), but Epic
  indicates in the XSD that there's a RefundItem, but the name is still LineItem. -->
  
  <xsl:template match="epic:LineItem">
    <item>
      <field key="REFERENCE_ID">$ext_ref_id</field>
      <xsl:apply-templates select = "epic:LineNumber"/>
      <xsl:apply-templates select = "epic:EncFormNumber"/>
      <xsl:apply-templates select = "epic:Description"/>
      <xsl:apply-templates select = "epic:Amount"/>
      <xsl:apply-templates select = "epic:MRNID"/>
      <xsl:apply-templates select = "epic:MemberName"/>
      <xsl:apply-templates select = "epic:Address"/>
      <xsl:apply-templates select = "epic:City"/>
      <xsl:apply-templates select = "epic:State"/>
      <xsl:apply-templates select = "epic:ZIP"/>
      <xsl:apply-templates select = "epic:AccountID"/>
      
      <!-- technically only for refunds -->
      <xsl:apply-templates select = "epic:PmtTxID"/> <!-- this has the ipayment receipt ref# -->
      <xsl:apply-templates select = "epic:PmtLineNumber"/>

    </item>
  </xsl:template>

  <!-- TODO: Many of these can be cleaned up with Attribute value templates -->
  <!-- Here Be fields -->
  <xsl:template match="epic:MessageID">
    <field>
      <xsl:attribute name="key">
        <xsl:value-of select ="concat('Epic',local-name())"/>
      </xsl:attribute>
      <xsl:value-of select="text()"/>
    </field>
  </xsl:template>

  <xsl:template match="epic:MessageID" mode="Seq">
    <seq_id>
      <xsl:value-of select="text()"/>
    </seq_id>
  </xsl:template>
  
  <xsl:template match="epic:UserID">
    <field>
      <xsl:attribute name="key">
        <xsl:value-of select ="concat('Epic',local-name())"/>
      </xsl:attribute>
      <xsl:value-of select="text()"/>
    </field>
  </xsl:template>

  <xsl:template match="epic:MRNID">
    <field>
      <xsl:attribute name="key">MRN</xsl:attribute>
      <xsl:value-of select="text()"/>
    </field>
  </xsl:template>

  <xsl:template match="epic:LineNumber">
    <field>
      <xsl:attribute name="key">
        <xsl:value-of select ="concat('Epic',local-name())"/>
      </xsl:attribute>
      <xsl:value-of select="text()"/>
    </field>
  </xsl:template>

  <xsl:template match="epic:PmtLineNumber">
    <field>
      <xsl:attribute name="key">
        <xsl:value-of select ="concat('Epic',local-name())"/>
      </xsl:attribute>
      <xsl:value-of select="text()"/>
    </field>
  </xsl:template>

  <xsl:template match="epic:PmtTxID">
    <field>
      <xsl:attribute name="key">
        <xsl:value-of select ="concat('Epic',local-name())"/>
      </xsl:attribute>
      <xsl:value-of select="text()"/>
    </field>
  </xsl:template>
  
  <xsl:template match="epic:EncFormNumber">
    <field>
      <xsl:attribute name="key">
        <xsl:value-of select ="concat('Epic',local-name())"/>
      </xsl:attribute>
      <xsl:value-of select="text()"/>
    </field>
  </xsl:template>

  <!--<xsl:template match="epic:Description | epic:Address | epic:City | epic:State">-->
  <xsl:template match="epic:Description">
    <field>
      <xsl:attribute name="key">
        <xsl:value-of select ="concat('Epic',local-name())"/>
      </xsl:attribute>
      <xsl:value-of select="text()"/>
    </field>
  </xsl:template>

  <xsl:template match="epic:RefundReason">
    <field>
      <xsl:attribute name="key">
        <xsl:value-of select ="concat('Epic',local-name())"/>
      </xsl:attribute>
      <xsl:value-of select="text()"/>
    </field>
  </xsl:template>

  <xsl:template match="epic:Address">
    <field>
      <xsl:attribute name="key">
        <xsl:value-of select ="concat('Epic',local-name())"/>
      </xsl:attribute>
      <xsl:value-of select="text()"/>
    </field>
  </xsl:template>

  <xsl:template match="epic:City">
    <field>
      <xsl:attribute name="key">
        <xsl:value-of select ="concat('Epic',local-name())"/>
      </xsl:attribute>
      <xsl:value-of select="text()"/>
    </field>
  </xsl:template>

  <xsl:template match="epic:State">
    <field>
      <xsl:attribute name="key">
        <xsl:value-of select ="concat('Epic',local-name())"/>
      </xsl:attribute>
      <xsl:value-of select="text()"/>
    </field>
  </xsl:template>

  <xsl:template match="epic:AccountID">
    <field>
      <xsl:attribute name="key">
        <xsl:value-of select ="concat('Epic',local-name())"/>
      </xsl:attribute>
      <xsl:value-of select="text()"/>
    </field>
  </xsl:template>
  
  <xsl:template match="epic:MemberName">
    <field>
      <xsl:attribute name="key">
        <xsl:value-of select ="concat('Epic',local-name())"/>
      </xsl:attribute>
      <xsl:value-of select="text()"/>
    </field>
  </xsl:template>
  
  <xsl:template match="epic:ZIP">
    <field>
      <xsl:attribute name="key">
        <xsl:value-of select ="concat('Epic',local-name())"/>
      </xsl:attribute>
      <xsl:value-of select="text()"/>
    </field>
  </xsl:template>

  <xsl:template match="epic:Amount">
    <field>
      <xsl:attribute name="key">amount</xsl:attribute>
      <xsl:value-of select='format-number(text(), "0.00")'/>
    </field>
    <field>
      <xsl:attribute name="key">EpicAmount</xsl:attribute>
      <xsl:value-of select='format-number(text(), "0.00")'/>
    </field>
  </xsl:template>

</xsl:stylesheet>