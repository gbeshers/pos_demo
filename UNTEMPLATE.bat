REM UMN: don't rename, just copy so don't have to then svn update 
REM if you change the template files. Also allows you to rerun
REM this command if you change the template files
REM Bug 25097: Make sure current directory if run as admin.
pushd "%~dp0"
for /R %%f in (*.TEMPLATE) DO copy "%%f" "%%~df%%~pf%%~nf" /Y

REM Bug 11018: FT: Create the log directory if not already there
md log
