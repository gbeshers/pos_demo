using System;
using System.IO;
using CASL_engine;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.ComponentModel;
using TranSuiteServices;

namespace fsaprocessingsi
{
    public class EPLProductInquiry
    {
        public static object do_EPL_inquiry(params Object[] arg_pairs)
        {	
            GenericObject geoARGS        = misc.convert_args(arg_pairs);
            GenericObject geoDBInfo      = (GenericObject)geoARGS.get("db_connection_info");
            string strUPC                = (string)geoARGS.get("UPC");

            return do_EPL_inquiry_aux(strUPC, geoDBInfo);
        }

        public static object do_EPL_inquiry_aux(string strUPC, GenericObject geoDBInfo)
        {
            string strErrorMsg = "";

            // If full UPC of 12 digits then remove the check digit (SIGIS only stores first 11 digits)
            if (strUPC.Length == 12)
            {
                strUPC = strUPC.Substring(0, 11);
            }
            
            // UPC MUST be either 11 digits (or 12 digits).  [12 is the FULL UPC and 11 omits the check digit.]
            if (strUPC.Length != 11)
            {
                strErrorMsg = "Product UPC Is Not A Valid Length.";
                GenObj_Error geoError = new GenObj_Error(strErrorMsg);
                geoError.set("title", "EPL Inquiry Error", "throw", false);
                throw CASL_error.create_obj(strErrorMsg, geoError);
            }

            string sql_string = "SELECT * FROM TG_FSA_ELIGIBLE_PRODUCT_LIST WHERE (UPC = @UPC)";

            string error_message = "";
            Hashtable sql_args = new Hashtable();
            sql_args["UPC"] = strUPC;

            IDBVectorReader reader = db_reader.Make(geoDBInfo, sql_string, CommandType.Text, 0,
              sql_args, 0, 0, null, ref error_message);

            if (error_message != "")
            {
                strErrorMsg = "Error connecting to database to check FSA eligibility. " + error_message;
                GenObj_Error geoError = new GenObj_Error(strErrorMsg);
                geoError.set("title", "EPL Inquiry Error", "throw", false);
                throw CASL_error.create_obj(strErrorMsg, geoError);
            }

            GenericObject record_set = new GenericObject();
            reader.ReadIntoRecordset(ref record_set, 0, 0);

            // The CHANGE_INDICATOR_FLAG may have the following values:
			
            // Flag  Meaning
			// A     Added Eligible Item.
			// M     Merchant Added Eligible Item.
			// I     Merchant Added Ineligible Item.
			// D     Deleted Ineligible Item.
            // E     Currently Eligible Item.
            
            // NOTE: There may be more than one record for a UPC because the Merchant can
            // override the status of product already in the SIGIS EPL; this is done with
            // a duplicate record with a CHANGE_INDICATOR_FLAG of M or I.

            int rec_count = record_set.getLength();
            GenericObject geoRec = null;
            bool bFSAeligible    = false;
            
            for (int i = 0; i < rec_count; i++)
            {
                geoRec = record_set.get(i) as GenericObject;
                string ci_flag = geoRec.get("CHANGE_INDICATOR_FLAG").ToString();

                // NOTE: If there are BOTH "Merchant" AND "SIGIS" records then merchant
                // will be used (i.e., Merchant added overrides SIGIS records).

                ci_flag.Trim();

                if (ci_flag == "M")
                {
                    bFSAeligible = true;
                    break; // Break out of FOR loop for merchant record
                }
                else if (ci_flag == "I")
                {
                    bFSAeligible = false;
                    break; // Break out of FOR loop for merchant record
                }
                else if (ci_flag == "A")
                    bFSAeligible = true;
                else if (ci_flag == "E")
                    bFSAeligible = true;
                else if (ci_flag == "D")
                    bFSAeligible = false;
            }

            return bFSAeligible;
        }
    }
}
		
	