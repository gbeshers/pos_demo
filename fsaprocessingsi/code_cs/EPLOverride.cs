﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CASL_engine;
using System.Data.SqlClient;
using System.Collections;
using System.Data;
using System.Globalization;

namespace fsaprocessingsi
{
  public static class EPLOverride
  {
    public static GenericObject Search(params Object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);

      GenericObject geoDBInfo = (GenericObject)args.get("db_connection_info");
      string description = (String)args.get("description", "");
      string upc = (String)args.get("upc", "");

      if (upc.Length == 12)
        upc = upc.Substring(0, 11);

      string shortUpc = upc.Length > 10 ? upc.Substring(1, 10) : upc;

      Hashtable sql_args = new Hashtable();
      sql_args["desc"] = description;
      sql_args["upc"] = upc;
      sql_args["shortUpc"] = shortUpc;

      string otcQuery = "SELECT OtcUpc AS OUPC, OtcDesc AS [DESCRIPTION] FROM OTC_PRODUCT WHERE OtcUpc LIKE '%' + @shortUpc + '%' and OtcDesc LIKE '%' + @desc + '%'";
      string fsaQuery = "SELECT UPC, SUM(CASE CHANGE_INDICATOR_FLAG WHEN 'M' THEN 1 WHEN 'I' THEN 1 ELSE 0 END) AS [OVERRIDE], SUM(CASE CHANGE_INDICATOR_FLAG WHEN 'I' THEN -1 WHEN 'D' THEN 0 ELSE 1 END) AS ELIGIBLE, COUNT(UPC) AS [UPC_COUNT] FROM TG_FSA_ELIGIBLE_PRODUCT_LIST WHERE UPC LIKE '%' + @upc + '%' GROUP BY UPC";
      string query = "SELECT TOP(50) UPC, OUPC, [DESCRIPTION], [OVERRIDE], (CASE ELIGIBLE WHEN 2 THEN 1 ELSE ELIGIBLE END) AS ELIGIBLE, (CASE ([OVERRIDE] + [UPC_COUNT]) WHEN 3 THEN 'O' WHEN 2 THEN 'A' WHEN 1 THEN 'S' ELSE 'P' END) AS OVR_STATUS FROM (" + otcQuery + ") o LEFT JOIN (" + fsaQuery + ") e ON UPC LIKE '%' + OUPC + '%' ORDER BY [DESCRIPTION], UPC";

      // NOTE: OVR_STATUS will have one of the following values:
      // O - This is a merchant override of a SIGIS EPL product (i.e., UPC count is 2 and override value is 1)
      // A - This is a merchant record added to the SIGIS EPL list from the merchant's product table (i.e., UPC count is 1 and override value is 1)
      // S - This is a SIGIS record with no override (and it is not an added record).
      // P - This is a record from the merchant's product table (eg, OTC) - Not found in SIGIS EPL

      string errorMessage = "";
      IDBVectorReader reader = db_reader.Make(geoDBInfo, query, CommandType.Text, 0,
              sql_args, 0, 0, null, ref errorMessage);

      if (errorMessage != "")
      {
        errorMessage = "Error connecting to database to check FSA eligibility: " + errorMessage;
        GenObj_Error geoError = new GenObj_Error(errorMessage);
        geoError.set(errorMessage, "title", "EPL Inquiry Error", "throw", false);
        throw CASL_error.create_obj(errorMessage, geoError);
      }

      GenericObject record_set = new GenericObject();
      reader.ReadIntoRecordset(ref record_set, 0, 0);

      return record_set;
    }

    public static GenericObject FindOverriddenProducts(params Object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);

      GenericObject geoDBInfo = (GenericObject)args.get("db_connection_info");

      Hashtable sql_args = new Hashtable();

      string query =
          @"SELECT EPList.UPC, EPList.UPC AS OUPC, MIN(EPList.PRODUCT_NAME) AS DESCRIPTION, 
            SUM(CASE EPList.CHANGE_INDICATOR_FLAG WHEN 'M' THEN 1 WHEN 'I' THEN 1 ELSE 0 END) AS OVERRIDE, 
            (CASE (SUM(CASE EPList.CHANGE_INDICATOR_FLAG WHEN 'I' THEN -1 WHEN 'D' THEN 0 ELSE 1 END)) 
            WHEN -1 THEN 0 WHEN 0 THEN 0 WHEN 1 THEN 1 WHEN 2 THEN 1 ELSE 0 END) AS ELIGIBLE,
            (CASE COUNT(EPList.UPC) WHEN 2 THEN 'O' WHEN 1 THEN 'A' END) AS OVR_STATUS
            FROM (SELECT UPC
                  FROM TG_FSA_ELIGIBLE_PRODUCT_LIST
                  WHERE (CHANGE_INDICATOR_FLAG = 'M') OR (CHANGE_INDICATOR_FLAG = 'I')
                  ) AS OvrUPCs LEFT OUTER JOIN TG_FSA_ELIGIBLE_PRODUCT_LIST AS EPList
                  ON EPList.UPC = OvrUPCs.UPC
            GROUP BY EPList.UPC
            ORDER BY OVR_STATUS";

      // NOTE: OVR_STATUS will have one of the following values:
      // O - This is a merchant overide of a SIGIS EPL product
      // A - This is a merchant record added to the SIGIS EPL list from the merchant's product table

      string errorMessage = "";
      IDBVectorReader reader = db_reader.Make(geoDBInfo, query, CommandType.Text, 0,
              sql_args, 0, 0, null, ref errorMessage);

      if (errorMessage != "")
      {
        errorMessage = "Error connecting to database to check FSA eligibility: " + errorMessage;
        GenObj_Error geoError = new GenObj_Error(errorMessage);
        geoError.set("title", "EPL Inquiry Error (Find Overridden Products)", "throw", false);
        throw CASL_error.create_obj(errorMessage, geoError);
      }

      GenericObject record_set = new GenericObject();
      reader.ReadIntoRecordset(ref record_set, 0, 0);

      return record_set;
    }

    public static object ToggleOverride(params Object[] arg_pairs)
    {
      GenericObject args = misc.convert_args(arg_pairs);

      GenericObject geoDBInfo = (GenericObject)args.get("db_connection_info");
      GenericObject product = (GenericObject)args.get("product");
      string upc = (String)args.get("upc");

      bool isEligible = product.get("ELIGIBLE", "").Equals(1) ? true : false;
      bool hasOverride = product.get("OVERRIDE", "").Equals(1) ? true : false;

      string query = null;
      string errorMessage = "";
      IDBVectorReader reader = null;

      Hashtable sql_args = new Hashtable();
      sql_args["upc"] = upc;

      if (isEligible)
      {// Remove eligibility
        if (hasOverride)
        {// Delete or Update the override row

          errorMessage = "";
          bool sigisEplElig = Get_EPL_Eligibility(ref upc, ref geoDBInfo, ref errorMessage);
          if (errorMessage != "")
          {
            return errorMessage;
          }
          
          if (sigisEplElig) // Modify the overrride record - SIGIS record is eligible
          {
            query = "UPDATE TG_FSA_ELIGIBLE_PRODUCT_LIST SET CHANGE_INDICATOR_FLAG='I', CHANGE_DATE='"
              + DateTime.Now.ToString("G", DateTimeFormatInfo.InvariantInfo) + "' WHERE UPC=@upc AND CHANGE_INDICATOR_FLAG='M'";
          }
          else // Delete the override record - SIGIS record is ineligible
          {
            query = "DELETE FROM TG_FSA_ELIGIBLE_PRODUCT_LIST WHERE UPC=@upc AND CHANGE_INDICATOR_FLAG='M'";
          }
        }
        else
        {// Insert an override row
          query = "SELECT PRODUCT_NAME FROM TG_FSA_ELIGIBLE_PRODUCT_LIST WHERE UPC=@upc";

          errorMessage = "";
          reader = db_reader.Make(geoDBInfo, query, CommandType.Text, 0, sql_args, 0, 0, null, ref errorMessage);

          if (errorMessage != "")
          {
            Logger.cs_log("Error connecting to database to modify EPL override: " + errorMessage);
            return errorMessage;
          }
          GenericObject record_set = new GenericObject();
          reader.ReadIntoRecordset(ref record_set, 0, 0);

          if (record_set.vectors.Count == 0)
          {
            Logger.cs_log("Error executing EPL override statement: Could not locate EPL entry for UPC " + upc);
            return "Error executing EPL override statement: Could not locate EPL entry for UPC " + upc;
          }

          string description = (String)((GenericObject)record_set.get(0, new GenericObject())).get("PRODUCT_NAME", "");


          query = "INSERT INTO TG_FSA_ELIGIBLE_PRODUCT_LIST (UPC, PRODUCT_NAME, CHANGE_INDICATOR_FLAG, CHANGE_DATE) VALUES "
            + "(@upc, '" + description + "', 'I', '" + DateTime.Now.ToString("G", DateTimeFormatInfo.InvariantInfo) + "')";
        }
      }
      else
      {// Add eligibility
        if (hasOverride)
        {// Delete or Update the override row
          errorMessage = "";
          bool sigisEplElig = Get_EPL_Eligibility(ref upc, ref geoDBInfo, ref errorMessage);
          if (errorMessage != "")
          {
            return errorMessage;
          }
          
          if (sigisEplElig) // Delete the overrride record - SIGIS record is eligible
          {
            query = "DELETE FROM TG_FSA_ELIGIBLE_PRODUCT_LIST WHERE UPC=@upc AND CHANGE_INDICATOR_FLAG='I'";
          }
          else // Modify the override record - SIGIS record is ineligible
          {
            query = "UPDATE TG_FSA_ELIGIBLE_PRODUCT_LIST SET CHANGE_INDICATOR_FLAG='M', CHANGE_DATE='"
              + DateTime.Now.ToString("G", DateTimeFormatInfo.InvariantInfo) + "' WHERE UPC=@upc AND CHANGE_INDICATOR_FLAG='I'";
          }
        }
        else
        {// Needs a new entry in the EPL
          if (product.has("UPC"))
          {// Insert an override row using EPL product name
            query = "SELECT PRODUCT_NAME FROM TG_FSA_ELIGIBLE_PRODUCT_LIST WHERE UPC=@upc";
            
            errorMessage = "";
            reader = db_reader.Make(geoDBInfo, query, CommandType.Text, 0, sql_args, 0, 0, null, ref errorMessage);

            if (errorMessage != "")
            {
              Logger.cs_log("Error connecting to database to modify EPL override: " + errorMessage);
              return errorMessage;
            }
            GenericObject record_set = new GenericObject();
            reader.ReadIntoRecordset(ref record_set, 0, 0);

            if (record_set.vectors.Count == 0)
            {
              Logger.cs_log("Error executing EPL override statement: Could not locate EPL entry for UPC " + upc);
              return "Error executing EPL override statement: Could not locate EPL entry for UPC " + upc;
            }

            string description = (String)((GenericObject)record_set.get(0, new GenericObject())).get("PRODUCT_NAME", "");

            query = "INSERT INTO TG_FSA_ELIGIBLE_PRODUCT_LIST (UPC, PRODUCT_NAME, CHANGE_INDICATOR_FLAG, CHANGE_DATE) VALUES "
            + "(@upc, '" + description + "', 'M', '" + DateTime.Now.ToString("G", DateTimeFormatInfo.InvariantInfo) + "')";
          }
          else
          {// Insert an override row using DESCRIPTION as product name
            query = "INSERT INTO TG_FSA_ELIGIBLE_PRODUCT_LIST (UPC, PRODUCT_NAME, CHANGE_INDICATOR_FLAG, CHANGE_DATE) VALUES "
            + "(@upc, '" + product.get("DESCRIPTION", "") + "', 'M', '" + DateTime.Now.ToString("G", DateTimeFormatInfo.InvariantInfo) + "')";
          }
        }
      }

      errorMessage = "";
      reader = db_reader.Make(geoDBInfo, query, CommandType.Text, 0, sql_args, 0, 0, null, ref errorMessage);

      if (errorMessage != "")
      {
        Logger.cs_log("Error connecting to database to modify EPL override: " + errorMessage);
        return errorMessage;
      }

      reader.ExecuteNonQuery(ref errorMessage);

      if (errorMessage != "")
      {
        Logger.cs_log("Error executing EPL override statement: " + errorMessage);
        return errorMessage;
      }

      // Log the successful update of eligibility in the EPL
      string SuccessLogMsg = string.Format("EPL Updated: Eligibility {0} for UPC: {1}."
          , (isEligible? "removed" : "added"), upc);
      Logger.cs_log(SuccessLogMsg);
      
      return true;
    }

    private static bool Get_EPL_Eligibility(ref string strUPC, ref GenericObject geoDBInfo, ref string errMsg)
    {
        // The CHANGE_INDICATOR_FLAG for SIGIS EPL records may have the following values:
		// Flag  Meaning
		// A     Added Eligible Item.
		// D     Deleted Ineligible Item.
        // E     Currently Eligible Item.

        // NOTE: The eligibility status may have changed with an EPL update so it cannot
        // be assumed that deleting an override record will make the eligibility match
        // the current SIGIS EPL record.

        string query           = "";
        IDBVectorReader reader = null;
        Hashtable sql_args     = new Hashtable();
        sql_args["upc"] = strUPC;

        query = @"SELECT PRODUCT_NAME FROM TG_FSA_ELIGIBLE_PRODUCT_LIST WHERE UPC=@upc
                AND (CHANGE_INDICATOR_FLAG='A' OR CHANGE_INDICATOR_FLAG='E')";
        reader = db_reader.Make(geoDBInfo, query, CommandType.Text, 0, sql_args, 0, 0, null, ref errMsg);
        if (errMsg != "")
        {
            Logger.cs_log("Error connecting to database to get EPL Eligibility: " + errMsg);
            return false;
        }
        GenericObject record_set = new GenericObject();
        reader.ReadIntoRecordset(ref record_set, 0, 0);

        bool is_eligible = (record_set.vectors.Count > 0);

        return is_eligible;
    }
  }
}
