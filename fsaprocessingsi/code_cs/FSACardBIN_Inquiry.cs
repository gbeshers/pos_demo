using System;
using System.IO;
using CASL_engine;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.ComponentModel;
using TranSuiteServices;

namespace fsaprocessingsi
{
    public class FSACardBINInquiry
    {
        public static object do_BIN_inquiry(params Object[] arg_pairs)
        {	
            GenericObject geoARGS   = misc.convert_args(arg_pairs);
            GenericObject geoDBInfo = (GenericObject)geoARGS.get("db_connection_info");
            string strCardNumber    = (string)geoARGS.get("CardNumber");

            return do_BIN_inquiry_aux(strCardNumber, geoDBInfo);
        }

        public static object do_BIN_inquiry_aux(string strCardNumber, GenericObject geoDBInfo)
        {
            string strErrorMsg = "";

            // The First six digits of the Card Number must be sent (at least)
            if (strCardNumber.Length < 6)
            {
                strErrorMsg = "Card Number Is Not A Valid Length.";
		GenObj_Error geoError = new GenObj_Error(strErrorMsg);
		geoError.set("title", "BIN Inquiry Error", "throw", false);
                throw CASL_error.create_obj(strErrorMsg, geoError);
            }

            string strBIN = strCardNumber.Substring(0, 6);

            string sql_string = "SELECT * FROM TG_FSA_BIN_LIST WHERE (BIN = @BIN)";

            string error_message = "";
            Hashtable sql_args = new Hashtable();
            sql_args["BIN"] = strBIN;

            IDBVectorReader reader = db_reader.Make(geoDBInfo, sql_string, CommandType.Text, 0,
              sql_args, 0, 0, null, ref error_message);

            if (error_message != "")
            {
                strErrorMsg = "Error connecting to database to check FSA Card validity. " + error_message;
			   	GenObj_Error geoError = new GenObj_Error(strErrorMsg);
		geoError.set("title", "BIN Inquiry Error", "throw", false);
                throw CASL_error.create_obj(strErrorMsg, geoError);
            }

            GenericObject record_set = new GenericObject();
            reader.ReadIntoRecordset(ref record_set, 0, 0);

            int rec_count = record_set.getLength();
            bool bRecognizedFSACard = rec_count>0? true:false;

            return bRecognizedFSACard;
        }
    }
}
		
	