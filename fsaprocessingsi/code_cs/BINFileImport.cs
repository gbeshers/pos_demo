using System;
using System.IO;
using CASL_engine;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.Collections;
using System.ComponentModel;
using TranSuiteServices;
using System.Text.RegularExpressions;

namespace fsaprocessingsi
{
    public class BINFileImport
    {
		//***************************************************************************
		// BIN FILE IMPORT - This MAY be specific to Heartland (uncertain if Bin list
        // XML format is standard across acquirers/processers).
		
        public static object import_BIN_file (params Object[] arg_pairs)
		{	
			GenericObject args    = misc.convert_args(arg_pairs);
			GenericObject data_db = (GenericObject)args.get("db_connection_info");
		
			string strLocalFileName = args.get("local_file_name","") as string;
			return import_BIN_file_aux(data_db,strLocalFileName);
		}

		public static object import_BIN_file_aux(GenericObject data_db, string strLocalFileName)
		{
            BinValues bins;
            String error_message        = "";
            String sub_error_message    = "";
            string strErrorMsg          = "";
			string strSQL               = "";
            IDBVectorReader reader      = null;

            const string CardType_MC   = "MC";
            const string CardType_Visa = "Visa";

            // Parse the MasterCard BIN XML file.
			try
			{
                System.IO.StreamReader str = new System.IO.StreamReader(strLocalFileName);
                System.Xml.Serialization.XmlSerializer xSerializer = 
                    new System.Xml.Serialization.XmlSerializer(typeof(BinValues));
                bins = (BinValues)xSerializer.Deserialize(str);
			} // end try
			catch(Exception e)
			{
                strErrorMsg = "Error parsing BIN xml file. " + e.Message;
                GenObj_Error geoError = new GenObj_Error(strErrorMsg);
                geoError.set("title", "BIN Inquiry Error", "throw", false);
                throw CASL_error.create_obj(strErrorMsg, geoError);
			}

            // Make the DB transaction reader
            reader = db_reader.MakeTransactionReader(data_db, 0, 0, 0, ref strErrorMsg);
            if (strErrorMsg.Length > 0)
            {
                strErrorMsg = "Database error: Cannot MakeTransactionReader: Cannot populate BIN table."
                    + strErrorMsg;
                GenObj_Error geoError = new GenObj_Error(strErrorMsg);
                geoError.set("title", "BIN Inquiry Error", "throw", false);
                throw CASL_error.create_obj(strErrorMsg, geoError);
            }

            // Populate TG_FSA_BIN_LIST table with data from BIN XML File
            Type class_type_mc      = typeof(BinValuesMchealthcarebins);
            Type class_type_visa    = typeof(BinValuesVisahealthcarebins);
            string strBIN           = "";
            string strCardType      = "";
            bool bDelBINs           = false;
            DateTime dtDateAdded;
                        
            foreach (object obj in bins.Items)
            {
                strCardType = (obj.GetType() == class_type_mc)? CardType_MC : CardType_Visa;

                // First, delete records for current BINs - For either MC XML file or Visa XML file
                if (!bDelBINs)
                {
                    strSQL = string.Format("DELETE FROM TG_FSA_BIN_LIST WHERE CARD_TYPE = '{0}'", strCardType);
                    reader.AddCommand(strSQL, CommandType.Text, null);
                    bDelBINs = true;
                }
                
                strBIN = strCardType == CardType_MC ?
                    ((BinValuesMchealthcarebins)(obj)).IssuerBin :
                    ((BinValuesVisahealthcarebins)(obj)).IssuerBin;

                dtDateAdded = strCardType == CardType_MC ?
                    ((BinValuesMchealthcarebins)(obj)).DateAdded :
                    ((BinValuesVisahealthcarebins)(obj)).DateAdded;

                add_insert_SQL_Tran(reader, strBIN, dtDateAdded, strCardType);
            }

            try
            {
                // Complete DB transaction
                writeRecordsInTransaction(ref error_message, ref sub_error_message, reader);
                if (error_message.Length > 0 || sub_error_message.Length > 0)
                {
                    strErrorMsg = "Database error: Cannot writeRecordsInTransaction: Cannot populate BIN Table: "
                            + error_message + ": " + sub_error_message;
                    GenObj_Error geoError = new GenObj_Error(strErrorMsg);
                    geoError.set("title", "BIN Inquiry Error", "throw", false);
                    throw CASL_error.create_obj(strErrorMsg, geoError);
                }
            }
            catch (Exception e)
            {
                if (reader != null)
                {
                    reader.RollbackTransaction(ref sub_error_message);
                }
                strErrorMsg = "Cannot populate BIN Table. Internal error: " + e.ToString() +
                    (String.Format("Cannot populate BIN Table: SQL or conversion error: Error: {0}: Traceback: {1}",
                            e, e.StackTrace));
                GenObj_Error geoError = new GenObj_Error(strErrorMsg);
                geoError.set("title", "BIN Inquiry Error", "throw", false);
                throw CASL_error.create_obj(strErrorMsg, geoError);
            }
            return "Success";
		}

        private static void add_insert_SQL_Tran(IDBVectorReader reader,
            string strBIN, DateTime dtDATE_ADDED, string strCARD_TYPE)
        {
            string strSql = 
                "INSERT INTO TG_FSA_BIN_LIST (" +
                "BIN, " +
                "DATE_ADDED, " +
                "CARD_TYPE) " +
                "VALUES (" +
                "@BIN, " +
                "@DATE_ADDED, " +
                "@CARD_TYPE)";
          
            Hashtable BIN_Params = new Hashtable();
            BIN_Params.Add("BIN",        strBIN.Trim());
            BIN_Params.Add("DATE_ADDED", dtDATE_ADDED);
            BIN_Params.Add("CARD_TYPE",  strCARD_TYPE.Trim());
            
            reader.AddCommand(strSql, CommandType.Text, BIN_Params);
        }

        private static string writeRecordsInTransaction(ref String error_message, ref String sub_error_message, IDBVectorReader reader)
        {
            bool success = reader.EstablishTransaction(ref error_message);
            if (!success || error_message.Length > 0)
            {
                if (reader != null)
                {
                    reader.RollbackTransaction(ref sub_error_message);
                    if (sub_error_message != "")
                    {
                        error_message += sub_error_message;
                    }
                }
                return "Database error: Cannot EstablishTransaction: " + error_message;
            }
            int row_count = 0;
            success = reader.ExecuteTransactionCommands(ref error_message, ref row_count, true);
            sub_error_message = "";
            if (!success || error_message.Length > 0)
            {
                if (reader != null)
                {
                    reader.RollbackTransaction(ref sub_error_message);
                    if (sub_error_message != "")
                    {
                        error_message += sub_error_message;
                    }
                }
                return "Database error: Cannot ExecuteTransactionCommands: " + error_message;
            }
            success = reader.CommitTransaction(ref error_message);
            if (!success || error_message.Length > 0)
            {
                if (reader != null)
                {
                    reader.RollbackTransaction(ref sub_error_message);
                    if (sub_error_message.Length > 0)
                    {
                        error_message += sub_error_message;
                    }
                }
                return "Database error: Cannot commit transaction: " + error_message;
            }
            return "";
        }
	}
}

