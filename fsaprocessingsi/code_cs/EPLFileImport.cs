using System;
using System.IO;
using CASL_engine;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.Collections;
using System.ComponentModel;
using TranSuiteServices;
using System.Text.RegularExpressions;

namespace fsaprocessingsi
{
    public class FileImport
	{
		public static Object DeleteFile(params Object[] arg_pairs)
		{	
			GenericObject args                = misc.convert_args(arg_pairs);
			string strLocalFile               = args.get("strLocalFile").ToString();
			if (File.Exists(strLocalFile))
			{
				File.Delete(strLocalFile);
			}
            return true;
		}
		
        public static String clip(string stringField, int fieldMax)
        {
            if (stringField == null)
                return "";

            if (stringField.Length <= fieldMax)
                return stringField;

            return stringField.Substring(0, fieldMax - 1);
        }
	}
    
    public class EPLFileImport
    {
		//***************************************************************************
		// EPL FILE IMPORT
		
        public  static string import_EPL_file (params Object[] arg_pairs)
		{	
			GenericObject args                = misc.convert_args(arg_pairs);
			GenericObject data_db             = (GenericObject)args.get("db_connection_info");
		
			string strLocalFileName = args.get("local_file_name","") as string;
			return import_EPL_file_aux(data_db,strLocalFileName);
		}

		public  static string import_EPL_file_aux(GenericObject data_db, string strLocalFileName)
		{
            Product prods;
            String error_message            = "";
            String sub_error_message        = "";
            string strErrorMsg          = "";
			string strSQL                   = "";
            IDBVectorReader reader          = null;

            // Parse the SIGIS EPL XML file.
			try
			{
                System.IO.StreamReader str = new System.IO.StreamReader(strLocalFileName);
                System.Xml.Serialization.XmlSerializer xSerializer = 
                    new System.Xml.Serialization.XmlSerializer(typeof(Product));
                prods = (Product)xSerializer.Deserialize(str);
			} // end try
			catch(Exception e)
			{
                strErrorMsg = "Error parsing EPL xml file. " + e.Message;
                GenObj_Error geoError = new GenObj_Error(strErrorMsg);
                geoError.set("title", "EPL Inquiry Error", "throw", false);
                throw CASL_error.create_obj(strErrorMsg, geoError);
			}

            // Make the DB transaction reader
            reader = db_reader.MakeTransactionReader(data_db, 0, 0, 0, ref strErrorMsg);
            if (strErrorMsg.Length > 0)
            {
                strErrorMsg = "Database error: Cannot MakeTransactionReader: Cannot populate EPL table."
                    + strErrorMsg;
                GenObj_Error geoError = new GenObj_Error(strErrorMsg);
                geoError.set("title", "BIN Inquiry Error", "throw", false);
                throw CASL_error.create_obj(strErrorMsg, geoError);
            }

            // Delete current EPL products - Need to keep Merchant added items (Eligible and Ineligible)
            strSQL = "DELETE FROM TG_FSA_ELIGIBLE_PRODUCT_LIST WHERE " +
                "(CHANGE_INDICATOR_FLAG != 'M') AND (CHANGE_INDICATOR_FLAG != 'I')";
            reader.AddCommand(strSQL, CommandType.Text, null);

            // Populate TG_FSA_ELIGIBLE_PRODUCT_LIST table with data from SIGIS EPL XML File
            foreach (ProductAddition pa in prods.ProductAddition)
            {
                add_insert_SQL_Tran( reader, pa.UPC, "A", pa.GTIN, pa.ProductName,
                    pa.ManufacturerName, pa.FLC, pa.CategoryDescription,
                    pa.SubCategoryDescription, pa.FinestCategoryDescription,
                    pa.AdditionDate );
            }

            foreach (ProductCurrent pc in prods.ProductCurrent)
            {
                add_insert_SQL_Tran( reader, pc.UPC, "E", pc.GTIN, pc.ProductName,
                    pc.ManufacturerName, pc.FLC, pc.CategoryDescription,
                    pc.SubCategoryDescription, pc.FinestCategoryDescription,
                    pc.AdditionDate );
            }

            foreach (ProductDeletion pd in prods.ProductDeletion)
            {
                add_insert_SQL_Tran( reader, pd.UPC, "D", pd.GTIN, pd.ProductName,
                    pd.ManufacturerName, pd.FLC, pd.CategoryDescription,
                    pd.SubCategoryDescription, pd.FinestCategoryDescription,
                    pd.DeletionDate );
            }

            try
            {
                // Complete DB transaction here
                writeRecordsInTransaction(ref error_message, ref sub_error_message, reader);
                if (error_message.Length > 0 || sub_error_message.Length > 0)
                {
                    strErrorMsg = "Database error: Cannot writeRecordsInTransaction: Cannot populate EPL Table: "
                            + error_message + ": " + sub_error_message;
                    GenObj_Error geoError = new GenObj_Error(strErrorMsg);
                    geoError.set("title", "BIN Inquiry Error", "throw", false);
                    throw CASL_error.create_obj(strErrorMsg, geoError);
                }
            }
            catch (Exception e)
            {
                if (reader != null)
                {
                    reader.RollbackTransaction(ref sub_error_message);
                }
                strErrorMsg =  "Cannot populate EPL Table. Internal error: " + e.ToString() +
                    (String.Format("Cannot populate EPL Table: SQL or conversion error: Error: {0}: Traceback: {1}",
                            e, e.StackTrace));
                GenObj_Error geoError = new GenObj_Error(strErrorMsg);
                geoError.set("title", "BIN Inquiry Error", "throw", false);
                throw CASL_error.create_obj(strErrorMsg, geoError);
            }
            return "Success";
		}

        private static void add_insert_SQL_Tran(IDBVectorReader reader,
            string strUPC, string strCHANGE_INDICATOR_FLAG, string strGTIN, string strPRODUCT_NAME,
            string strMANUFACTURER_NAME, string strFLC, string strCATEGORY_DESCRIPTION,
            string strSUBCATEGORY_DESCRIPTION, string strFINEST_CATEGORY_DESCRIPTION,
            DateTime dtCHANGE_DATE)
        {
            string strSql = 
                "INSERT INTO TG_FSA_ELIGIBLE_PRODUCT_LIST (" +
                "UPC, " +
                "CHANGE_INDICATOR_FLAG, " +
                "GTIN, " +
                "PRODUCT_NAME, " +
                "MANUFACTURER_NAME, " +
                "FLC, " +
                "CATEGORY_DESCRIPTION, " +
                "SUBCATEGORY_DESCRIPTION, " +
                "FINEST_CATEGORY_DESCRIPTION, " +
                "CHANGE_DATE) " +
                "VALUES (" +
                "@UPC, " +
                "@CHANGE_INDICATOR_FLAG, " +
                "@GTIN, " +
                "@PRODUCT_NAME, " +
                "@MANUFACTURER_NAME, " +
                "@FLC, " +
                "@CATEGORY_DESCRIPTION, " +
                "@SUBCATEGORY_DESCRIPTION, " +
                "@FINEST_CATEGORY_DESCRIPTION, " +
                "@CHANGE_DATE)";
                

            Hashtable EPL_Params = new Hashtable();
            EPL_Params.Add("UPC",                         strUPC.Trim());
            EPL_Params.Add("CHANGE_INDICATOR_FLAG",       strCHANGE_INDICATOR_FLAG.Trim());
            EPL_Params.Add("GTIN",                        strGTIN.Trim());
            EPL_Params.Add("PRODUCT_NAME",                strPRODUCT_NAME.Trim());
            EPL_Params.Add("MANUFACTURER_NAME",           strMANUFACTURER_NAME.Trim());
            EPL_Params.Add("FLC",                         strFLC.Trim());
            EPL_Params.Add("CATEGORY_DESCRIPTION",        strCATEGORY_DESCRIPTION.Trim());
            EPL_Params.Add("SUBCATEGORY_DESCRIPTION",     strSUBCATEGORY_DESCRIPTION.Trim());
            EPL_Params.Add("FINEST_CATEGORY_DESCRIPTION", strFINEST_CATEGORY_DESCRIPTION.Trim());
            EPL_Params.Add("CHANGE_DATE",                 dtCHANGE_DATE);
            
            reader.AddCommand(strSql, CommandType.Text, EPL_Params);
        }

        private static string writeRecordsInTransaction(ref String error_message, ref String sub_error_message, IDBVectorReader reader)
        {
            bool success = reader.EstablishTransaction(ref error_message);
            if (!success || error_message.Length > 0)
            {
                if (reader != null)
                {
                    reader.RollbackTransaction(ref sub_error_message);
                    if (sub_error_message != "")
                    {
                        error_message += sub_error_message;
                    }
                }
                return "Database error: Cannot EstablishTransaction: " + error_message;
            }
            int row_count = 0;
            success = reader.ExecuteTransactionCommands(ref error_message, ref row_count, true);
            sub_error_message = "";
            if (!success || error_message.Length > 0)
            {
                if (reader != null)
                {
                    reader.RollbackTransaction(ref sub_error_message);
                    if (sub_error_message != "")
                    {
                        error_message += sub_error_message;
                    }
                }
                return "Database error: Cannot ExecuteTransactionCommands: " + error_message;
            }
            success = reader.CommitTransaction(ref error_message);
            if (!success || error_message.Length > 0)
            {
                if (reader != null)
                {
                    reader.RollbackTransaction(ref sub_error_message);
                    if (sub_error_message.Length > 0)
                    {
                        error_message += sub_error_message;
                    }
                }
                return "Database error: Cannot commit transaction: " + error_message;
            }
            return "";
        }
	}
}

