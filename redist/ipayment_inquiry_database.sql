-- This script creates the Project Baseline database tables in the new format used by the
-- always active inquiry import technology (Bug#12994). It ENFORCES being run against a separate database,
-- NOT the transactional database, so that inquiring can be done while importing is happening.
-- If it was in the same database, bulk imports will escalate to a table lock, thus blocking
-- out payment transactions. Moreover, we can optimize the inquiry database to have simple
-- logging and page compression turned on, so that read inquiries against it are fast.
-- Also, our FAEs like to have a separate config database, a transactional database, and
-- now a database for PB inquiry data. Having three separate databases allows us to manage
-- backups more easily.

-- How to use this script:
-- 1. First create an empty transactional database named "PROJECT_type".
--    Examples might include: Geisinger_prod, SW_beta, SF_TEST.
-- 2. Now, run the transactional database script 
--    (example: T4_DATABASE/Scripts/127_MSSQL_data_COMPLETE.sql) against your new database.
-- 3. Run the appropriate script to generate indexes for the transactional and config databases.
-- 4. Create an empty config database named "PROJECT_config". (I'm assuming that production,
--    test, and beta configs are shared. If that's not the case, then use "PROJECT_config_type")
--    Examples might include: Geisinger_config, SW_config_beta, SF_CONFIG.
-- 5. Run the config database script (example:
--    T4_DATABASE/Scripts/124_MSSQL_config_COMPLETE.sql) against your new database.
-- 6. Create an empty inquiry database named "PROJECT_inquiry_type".
--    Examples might include: Geisinger_inquiry_prod, SW_inquiry_beta, SF_INQUIRY_TEST.
-- 7. COPY this script (T4_DATABASE/Scripts/127_MSSQL_inquiry_COMPLETE.sql) to a new query window
--    in SQL Server Management Console.
-- 8. Edit the two variable names (see below) to point to your inquiry database and your config database.
-- 9. Execute the script in the new query window of SQL Server Management Console.

-- Dan wants this to be independent of SQL Server/Oracle, but the problem is finding 
-- a matching datatype in SQL Server for bigint and tinyint. SQL Server's Numeric 
-- doesn't cut it because of the greater storage size. So to use this on Oracle:
-- 1. Search and replace [bigint] with Number(19,0)
-- 2. Search and replace [tinyint] with Number(3,0)
-- 3. Note, that I didn't try to parameterize all the queries with these changes
--    because there isn't a way to check for Sql Server or Oracle from the database,
--    at least, no reliable way.

-- UMN suppress rows affected message in favor of useful messages that indicate what is going on
SET NOCOUNT ON;

-- Check if being run against a transactional db. If so, then exit.
IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TG_DEPFILE_DATA' 
  and TABLE_SCHEMA = 'dbo' and TABLE_TYPE = 'BASE TABLE'))
BEGIN
  PRINT 'Cannot run this script against a transactional database; you must use a separate inquiry database.';
  SET noexec ON;
END
  
-- Check if being run against a config db. If so, then exit.
IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'CBT_Flex' 
  and TABLE_SCHEMA = 'dbo' and TABLE_TYPE = 'BASE TABLE'))
BEGIN
  PRINT 'Cannot run this script against a config database; you must use a separate inquiry database.';
  SET noexec ON;
END

-- remove any tables with old names
IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SI_Inq_Names' 
  and TABLE_SCHEMA = 'dbo' and TABLE_TYPE = 'BASE TABLE'))
	DROP TABLE [dbo].[SI_Inq_Names];
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON

-- UMN SQL standard way (information_schema, rather than sys.objects query)
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SI_DATA1' 
  and TABLE_SCHEMA = 'dbo' and TABLE_TYPE = 'BASE TABLE'))
BEGIN
  CREATE TABLE [dbo].[SI_DATA1]
  (
    DATA_ID [bigint] NOT NULL,
    GROUP_NAME [VARCHAR](128) NULL,
    INQ_DATA [VARCHAR](MAX) NULL
  ) ON [PRIMARY];
  PRINT 'Created table SI_DATA1.';
END
GO

-- UMN SQL standard way (information_schema, rather than sys.objects query)
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SI_DATA2' 
  and TABLE_SCHEMA = 'dbo' and TABLE_TYPE = 'BASE TABLE'))
BEGIN
  CREATE TABLE [dbo].[SI_DATA2]
  (
    DATA_ID [bigint] NOT NULL,
    GROUP_NAME [VARCHAR](128) NULL,
    INQ_DATA [VARCHAR](MAX) NULL
  ) ON [PRIMARY];
  PRINT 'Created table SI_DATA2.';
END
GO

-- UMN SQL standard way (information_schema, rather than sys.objects query)
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SI_KEY1' 
  and TABLE_SCHEMA = 'dbo' and TABLE_TYPE = 'BASE TABLE'))
BEGIN
  CREATE TABLE [dbo].[SI_KEY1]
  (
    TAG [VARCHAR](128) NOT NULL,
    TAG_VALUE [VARCHAR](512) NULL,
    DATA_ID [bigint] NOT NULL,
	CONSTRAINT [PK_SI_KEY1] PRIMARY KEY NONCLUSTERED
	(
		[TAG] ASC, 
		[DATA_ID] ASC
	)
  )
  PRINT 'Created table SI_KEY1.';
END
GO

-- UMN SQL standard way (information_schema, rather than sys.objects query)
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SI_KEY2' 
  and TABLE_SCHEMA = 'dbo' and TABLE_TYPE = 'BASE TABLE'))
BEGIN
  CREATE TABLE [dbo].[SI_KEY2]
  (
    TAG [VARCHAR](128) NOT NULL,
    TAG_VALUE [VARCHAR](512) NULL,
    DATA_ID [bigint] NOT NULL
  ) ON [PRIMARY];
  PRINT 'Created table SI_KEY2.';
END
GO

-- UMN SQL standard way (information_schema, rather than sys.objects query)
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SI_NAMES' 
  and TABLE_SCHEMA = 'dbo' and TABLE_TYPE = 'BASE TABLE'))
BEGIN
  CREATE TABLE [dbo].SI_NAMES
  (
    ROW_ID [tinyint] IDENTITY(1,1) NOT NULL,
    NEXT_HI [bigint] NOT NULL,
    INQ_SI_DATA [VARCHAR](32) NOT NULL,
    INQ_SI_KEY [VARCHAR](32) NOT NULL,
    INQ_SI_WEIGHTS [VARCHAR](32) NOT NULL,
    IMP_SI_DATA [VARCHAR](32) NOT NULL,
    IMP_SI_KEY [VARCHAR](32) NOT NULL,
    IMP_SI_WEIGHTS [VARCHAR](32) NOT NULL
  ) ON [PRIMARY];
  PRINT 'Created table SI_NAMES.';
END
GO

-- UMN SQL standard way (information_schema, rather than sys.objects query)
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SI_WEIGHTS1' 
  and TABLE_SCHEMA = 'dbo' and TABLE_TYPE = 'BASE TABLE'))
BEGIN
  CREATE TABLE [dbo].[SI_WEIGHTS1]
  (
    WEIGHTS_ID [INT] IDENTITY(1,1) NOT NULL,
    GROUP_NAME [VARCHAR](512) NOT NULL,
    TAG [VARCHAR](128) NOT NULL,
    UNIQUE_RANK [INT] NOT NULL,
    IS_QUERY_KEY [VARCHAR](1) NOT NULL
  ) ON [PRIMARY];
  PRINT 'Created table SI_WEIGHTS1.';
END
GO

-- UMN SQL standard way (information_schema, rather than sys.objects query)
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SI_WEIGHTS2' 
  and TABLE_SCHEMA = 'dbo' and TABLE_TYPE = 'BASE TABLE'))
BEGIN
  CREATE TABLE [dbo].[SI_WEIGHTS2]
  (
    WEIGHTS_ID [INT] IDENTITY(1,1) NOT NULL,
    GROUP_NAME [VARCHAR](512) NOT NULL,
    TAG [VARCHAR](128) NOT NULL,
    UNIQUE_RANK [INT] NOT NULL,
    IS_QUERY_KEY [VARCHAR](1) NOT NULL
  ) ON [PRIMARY];
  PRINT 'Created table SI_WEIGHTS2.';
END
GO

-- drop indexes
IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_SI_KEY1_TAG_VALUE_TAG_DATA_ID')
  drop index [IX_SI_KEY1_TAG_VALUE_TAG_DATA_ID] on [dbo].[SI_KEY1];
IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_SI_KEY2_TAG_VALUE_TAG_DATA_ID')
  drop index [IX_SI_KEY2_TAG_VALUE_TAG_DATA_ID] on [dbo].[SI_KEY2];

IF EXISTS (SELECT name FROM sysindexes WHERE name = 'PK_SI_DATA1')
  ALTER TABLE [dbo].[SI_DATA1] DROP CONSTRAINT PK_SI_DATA1

IF EXISTS (SELECT name FROM sysindexes WHERE name = 'PK_SI_DATA2')
  ALTER TABLE [dbo].[SI_DATA2] DROP CONSTRAINT PK_SI_DATA2

IF EXISTS (SELECT name FROM sysindexes WHERE name = 'PK_SI_KEY1')
  ALTER TABLE [dbo].[SI_KEY1] DROP CONSTRAINT PK_SI_KEY1

IF EXISTS (SELECT name FROM sysindexes WHERE name = 'PK_SI_KEY2')
  ALTER TABLE [dbo].[SI_KEY2] DROP CONSTRAINT PK_SI_KEY2

IF EXISTS (SELECT name FROM sysindexes WHERE name = 'PK_SI_NAMES')
  ALTER TABLE [dbo].[SI_NAMES] DROP CONSTRAINT PK_SI_NAMES

IF EXISTS (SELECT name FROM sysindexes WHERE name = 'PK_SI_WEIGHTS1')
  ALTER TABLE [dbo].[SI_WEIGHTS1] DROP CONSTRAINT PK_SI_WEIGHTS1

IF EXISTS (SELECT name FROM sysindexes WHERE name = 'PK_SI_WEIGHTS2')
  ALTER TABLE [dbo].[SI_WEIGHTS2] DROP CONSTRAINT PK_SI_WEIGHTS2

-- remove old indexes if they exist
IF EXISTS (SELECT name FROM sysindexes WHERE name = '_dta_index_SI_KEY1_TAG_VALUE_TAG_DATA_ID')
  drop index [_dta_index_SI_KEY1_TAG_VALUE_TAG_DATA_ID] on [dbo].[SI_KEY1];
IF EXISTS (SELECT name FROM sysindexes WHERE name = '_dta_index_SI_KEY2_TAG_VALUE_TAG_DATA_ID')
  drop index [_dta_index_SI_KEY2_TAG_VALUE_TAG_DATA_ID] on [dbo].[SI_KEY2];

IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_SI_DATA1')
  ALTER TABLE [dbo].[SI_DATA1] DROP CONSTRAINT IX_SI_DATA1

IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_SI_DATA2')
  ALTER TABLE [dbo].[SI_DATA2] DROP CONSTRAINT IX_SI_DATA2

IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_SI_KEY1')
  ALTER TABLE [dbo].[SI_KEY1] DROP CONSTRAINT IX_SI_KEY1

IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_SI_KEY2')
  ALTER TABLE [dbo].[SI_KEY2] DROP CONSTRAINT IX_SI_KEY2

IF EXISTS (SELECT name FROM sysindexes WHERE name = 'PK_SI_INQ_NAMES')
  ALTER TABLE [dbo].[SI_NAMES] DROP CONSTRAINT PK_SI_INQ_NAMES

IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_SI_WEIGHTS1')
  ALTER TABLE [dbo].[SI_WEIGHTS1] DROP CONSTRAINT IX_SI_WEIGHTS1

IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_SI_WEIGHTS2')
  ALTER TABLE [dbo].[SI_WEIGHTS2] DROP CONSTRAINT IX_SI_WEIGHTS2
GO

-- UMN - Create Indexes for the PB Inquiry tables 
ALTER TABLE [dbo].[SI_DATA1] ADD CONSTRAINT [PK_SI_DATA1] PRIMARY KEY CLUSTERED 
(
  DATA_ID ASC
) ON [PRIMARY];

ALTER TABLE [dbo].[SI_DATA2] ADD CONSTRAINT [PK_SI_DATA2] PRIMARY KEY CLUSTERED 
(
  DATA_ID ASC
) ON [PRIMARY];

ALTER TABLE [dbo].[SI_KEY1] ADD CONSTRAINT [PK_SI_KEY1] PRIMARY KEY NONCLUSTERED 
(
  TAG ASC,
  DATA_ID ASC
) ON [PRIMARY];

ALTER TABLE [dbo].[SI_KEY2] ADD CONSTRAINT [PK_SI_KEY2] PRIMARY KEY NONCLUSTERED 
(
  TAG ASC,
  DATA_ID ASC
) ON [PRIMARY];

ALTER TABLE [dbo].[SI_NAMES] ADD CONSTRAINT [PK_SI_NAMES] PRIMARY KEY CLUSTERED 
(
  ROW_ID ASC
) ON [PRIMARY];

ALTER TABLE [dbo].[SI_WEIGHTS1] ADD CONSTRAINT [PK_SI_WEIGHTS1] PRIMARY KEY CLUSTERED 
(
  WEIGHTS_ID ASC
) ON [PRIMARY];

ALTER TABLE [dbo].[SI_WEIGHTS2] ADD CONSTRAINT [PK_SI_WEIGHTS2] PRIMARY KEY CLUSTERED 
(
  WEIGHTS_ID ASC
) ON [PRIMARY];

--CREATE NONCLUSTERED INDEX
--Removed for redundant 
--CREATE NONCLUSTERED INDEX [IDX_SI_DATA1_DATA_ID] ON [dbo].[SI_DATA1]
--(
--	[DATA_ID] ASC
--) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];

--CREATE NONCLUSTERED INDEX [IDX_SI_DATA2_DATA_ID] ON [dbo].[SI_DATA2]
--(
--	[DATA_ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];

CREATE NONCLUSTERED INDEX [IDX_SI_KEY1_Data_ID] ON [dbo].[SI_KEY1] 
(
  [DATA_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];

CREATE NONCLUSTERED INDEX [IDX_SI_KEY1_TAG_TAG_VALUE] ON [dbo].[SI_KEY1] 
(
  [TAG] ASC,
  [TAG_VALUE] ASC
)
INCLUDE ( [DATA_ID]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];

CREATE NONCLUSTERED INDEX [IDX_SI_KEY2_DATA_ID] ON [dbo].[SI_KEY2] 
(
  [DATA_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];

CREATE NONCLUSTERED INDEX [IDX_SI_KEY2_TAG_TAG_VALUE] ON [dbo].[SI_KEY2] 
(
  [TAG] ASC,
  [TAG_VALUE] ASC
)
INCLUDE ( [DATA_ID]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];

CREATE NONCLUSTERED INDEX [IDX_SI_WEIGHTS1_TAG] ON [dbo].[SI_WEIGHTS1]
(
	[TAG]
)INCLUDE ([UNIQUE_RANK]) WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];

CREATE NONCLUSTERED INDEX [IDX_SI_WEIGHTS2_TAG] ON [dbo].[SI_WEIGHTS2]
(
	[TAG]
)INCLUDE ([UNIQUE_RANK]) WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];


-- END Create Indexes for the PB Inquiry tables 

SET ANSI_PADDING OFF
PRINT 'Created PB Inquiry Indexes.';
GO

-- Insert the only row, which will get updated by PB file imports
IF (NOT EXISTS (SELECT * FROM [dbo].[SI_NAMES]))
BEGIN
  INSERT INTO [dbo].[SI_NAMES]
             ([NEXT_HI]
             ,[INQ_SI_DATA]
             ,[INQ_SI_KEY]
             ,[INQ_SI_WEIGHTS]
             ,[IMP_SI_DATA]
             ,[IMP_SI_KEY]
             ,[IMP_SI_WEIGHTS])
  VALUES(
      0,           -- start the hilo generator with 0
      'SI_DATA1',
      'SI_KEY1',
      'SI_WEIGHTS1',
      'SI_DATA2',
      'SI_KEY2',
      'SI_WEIGHTS2');
  PRINT 'Inserted single row into SI_NAMES with reset hilo.';
END
GO

-- Set the recovery model to be simple, since the PB data does not need to be recoverable
DECLARE @sql varchar(255) 
SET @sql = 'ALTER DATABASE ' + DB_NAME() + ' SET RECOVERY SIMPLE'
EXEC (@sql);
PRINT 'Altered DB to use minimal logging (simple recovery mode).'
GO

-- UMN Apply Table Compression only if SQL Server version supports it
DECLARE @compression VARCHAR(128)
set @compression = 'NONE'
IF (EXISTS (SELECT * FROM sys.dm_db_persisted_sku_features where feature_name = 'Compression'))
  set @compression = 'PAGE'
Exec ('ALTER INDEX ALL ON SI_DATA1 REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')');
Exec ('ALTER INDEX ALL ON SI_DATA2 REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')'); 
Exec ('ALTER INDEX ALL ON SI_KEY1 REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')'); 
Exec ('ALTER INDEX ALL ON SI_KEY2 REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')'); 
Exec ('ALTER INDEX ALL ON SI_NAMES REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')'); 
Exec ('ALTER INDEX ALL ON SI_WEIGHTS1 REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')'); 
Exec ('ALTER INDEX ALL ON SI_WEIGHTS2 REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')'); 

Exec ('ALTER TABLE [dbo].[SI_KEY1] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')');
Exec ('ALTER TABLE [dbo].[SI_KEY2] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')');
Exec ('ALTER TABLE [dbo].[SI_DATA1] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')');
Exec ('ALTER TABLE [dbo].[SI_DATA2] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')');
Exec ('ALTER TABLE [dbo].[SI_NAMES] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')');
Exec ('ALTER TABLE [dbo].[SI_WEIGHTS1] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')');
Exec ('ALTER TABLE [dbo].[SI_WEIGHTS2] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')');

IF (EXISTS (SELECT * FROM sys.dm_db_persisted_sku_features where feature_name = 'Compression'))
  PRINT 'Altered inquiry tables and indexes to set page-level data compression.';
GO
UPDATE STATISTICS SI_DATA1 WITH FULLSCAN
GO
UPDATE STATISTICS SI_DATA2 WITH FULLSCAN
GO
UPDATE STATISTICS SI_KEY1 WITH FULLSCAN
GO
UPDATE STATISTICS SI_KEY2 WITH FULLSCAN
GO
UPDATE STATISTICS SI_NAMES WITH FULLSCAN
GO
UPDATE STATISTICS SI_WEIGHTS1 WITH FULLSCAN
GO
UPDATE STATISTICS SI_WEIGHTS2 WITH FULLSCAN
GO

-- Following MUST be last line of script!
SET noexec OFF -- Turn execution back on; only needed in SSMS, so as to be able to run this script again in the same session.

-- Print out a list of the new clustered and non-clustered indexes, and stats
select OBJECT_NAME(i.id) as 'TABLE_NAME', i.name as 'CLUSTERED_INDEX'
from sysindexes i
where i.indid between 1 and 249
and INDEXPROPERTY(i.id, i.name, 'IsClustered') = 1
and INDEXPROPERTY(i.id, i.name, 'IsStatistics') = 0
and INDEXPROPERTY(i.id, i.name, 'IsAutoStatistics') = 0
and INDEXPROPERTY(i.id, i.name, 'IsHypothetical') = 0
and OBJECTPROPERTY(i.id,'IsMSShipped') = 0
and not exists (select *
from sysobjects o
where xtype = 'UQ'
and o.name = i.name ) 

select OBJECT_NAME(i.id) as 'TABLE_NAME', i.name as 'NON_CLUSTERED_INDEX'
from sysindexes i
where i.indid between 1 and 249
and INDEXPROPERTY(i.id, i.name, 'IsClustered') = 0
and INDEXPROPERTY(i.id, i.name, 'IsStatistics') = 0
and INDEXPROPERTY(i.id, i.name, 'IsAutoStatistics') = 0
and INDEXPROPERTY(i.id, i.name, 'IsHypothetical') = 0
and OBJECTPROPERTY(i.id,'IsMSShipped') = 0
and not exists (select *
from sysobjects o
where xtype = 'UQ'
and o.name = i.name ) 

select OBJECT_NAME(i.id) as 'TABLE_NAME', i.name as 'STATISTICS'
from sysindexes i
where i.indid between 1 and 249
and INDEXPROPERTY(i.id, i.name, 'IsStatistics') = 1
and INDEXPROPERTY(i.id, i.name, 'IsAutoStatistics') = 0
and INDEXPROPERTY(i.id, i.name, 'IsHypothetical') = 0
and OBJECTPROPERTY(i.id,'IsMSShipped') = 0
and not exists (select *
from sysobjects o
where xtype = 'UQ'
and o.name = i.name )
