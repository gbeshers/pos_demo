-- Bug 21952 UMN: Remove Revision table, and drop revision and is_root columns from the CBT_Flex table
IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME ='CBT_Revision'
AND TABLE_SCHEMA = 'dbo' AND TABLE_TYPE = 'BASE TABLE'))
BEGIN
	DROP TABLE CBT_Revision
	PRINT 'Removed CBT_Revision table'
END
GO

IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IDX_NTI_CBT_Flex_container_field_key_revision_Enterprise')
BEGIN
  DROP INDEX IDX_NTI_CBT_Flex_container_field_key_revision_Enterprise ON CBT_Flex
  PRINT 'Removed IDX_NTI_CBT_Flex_container_field_key_revision_Enterprise index'
END
IF EXISTS (SELECT name FROM sysindexes WHERE name = '_dta_index_CBT_Flex_CONTAINER_FIELD_KEY_REVISION')
BEGIN
  DROP INDEX _dta_index_CBT_Flex_CONTAINER_FIELD_KEY_REVISION ON CBT_Flex
  PRINT 'Removed _dta_index_CBT_Flex_CONTAINER_FIELD_KEY_REVISION index'
END
IF EXISTS (SELECT name FROM sysindexes WHERE name = 'CBT_Flex_pid')
BEGIN
  drop index CBT_Flex_pid on CBT_Flex;
  PRINT 'Removed CBT_Flex_pid index'
END
IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_CBT_Flex_container_field_key_field_value')
BEGIN
  drop index IX_CBT_Flex_container_field_key_field_value on CBT_Flex;
  PRINT 'Removed IX_CBT_Flex_container_field_key_field_value index'
END
GO

-- Need to recreate index without the revision column
CREATE NONCLUSTERED INDEX [IDX_NTI_CBT_Flex_container_field_key_revision_Enterprise] ON CBT_Flex
(
	container ASC,
	field_key ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
PRINT 'Created IDX_NTI_CBT_Flex_container_field_key_revision_Enterprise index'
GO

-- Now we can drop the revision and is_root column on the CBT_Flex table
IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IDX_NTI_CBT_Flex_container_field_key_revision_Enterprise')
BEGIN
  ALTER TABLE CBT_Flex
  DROP COLUMN revision, is_root
  PRINT 'Removed revision and is_root columns from CBT_Flex table'
END
GO  