-----
How to use a .bak database from Source Safe and replace existing database:
o get latest .bak file
o Open SQL Server Enterprise Manager
o Select (within nested levels) the POS_Cashiering_Config database




Symptom:
---------------------------
Microsoft SQL-DMO (ODBC SQLState: 42000)
File 'POS_Cashiering_Log' cannot be restored over the existing 'C:\Program Files\Microsoft SQL Server\MSSQL\data\POS_Cashiering_Config_Log.LDF'. Reissue the RESTORE statement using WITH REPLACE to overwrite pre-existing files.
RESTORE DATABASE is terminating abnormally.

Solution: Open Services and stop MSSQLSERVER service.

---------------------------



---------------------------
Microsoft SQL-DMO (ODBC SQLState: 42000)
---------------------------
Device activation error. The physical file name 'd:\sql data\MSSQL\data\POS_Cashiering_Config_Data.MDF' may be incorrect.
File 'POS_Cashiering_Data' cannot be restored to 'd:\sql data\MSSQL\data\POS_Cashiering_Config_Data.MDF'. Use WITH MOVE to identify a valid location for the file.
Device activation error. The physical file name 'd:\sql data\MSSQL\data\POS_Cashiering_Config_Log.LDF' may be incorrect.
File 'POS_Cashiering_Log' cannot be restored to 'd:\sql data\MSSQL\data\POS_Cashiering_Config_Log.LDF'. Use WITH MOVE to identify a valid location for the file.
RESTORE DATABASE is terminating abnormally.
---------------------------
OK   
---------------------------


---------------------------
Microsoft SQL-DMO (ODBC SQLState: 42000)
---------------------------
Device activation error. The physical file name 'C:\Program Files\Microsoft SQL Server\MSSQL\data\POS_Cashiering_Config_Data.MDF' may be incorrect.
---------------------------
OK   
---------------------------

---------------------------
Microsoft SQL-DMO (ODBC SQLState: 42000)
---------------------------
The backup of the system database on device C:\T4\pos_demo\redist\POS_Cashiering_Config.bak cannot be restored because it was created by a different version of the server (134218488) than this server (134217922).
RESTORE DATABASE is terminating abnormally.
---------------------------
OK   
---------------------------

---------------------------
Microsoft SQL-DMO (ODBC SQLState: 42000)
---------------------------
RESTORE DATABASE must be used in single user mode when trying to restore the master database.
RESTORE DATABASE is terminating abnormally.
---------------------------
OK   
---------------------------
