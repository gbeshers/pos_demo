/*IPAY-285 - Set the CBT_Flex table's Revision column to nullable for compatibility 
*/

ALTER TABLE CBT_Flex ALTER COLUMN revision int null