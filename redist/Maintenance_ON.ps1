# iPayment Maintenance Mode ON
# Turns Maintenance Mode ON by invoking and submitting a form
# Nuno Medeiros - May 2019

add-type @"
using System.Net;
using System.Security.Cryptography.X509Certificates;
public class TrustAllCertsPolicy : ICertificatePolicy {
    public bool CheckValidationResult(
        ServicePoint srvPoint, X509Certificate certificate,
        WebRequest request, int certificateProblem) {
        return true;
    }
}
"@

# security
$AllProtocols = [System.Net.SecurityProtocolType]'Ssl3,Tls,Tls11,Tls12'
[System.Net.ServicePointManager]::SecurityProtocol = $AllProtocols
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy

#--------------------------------------
# ENTER MAINTENANCE URL & FORM DATA
$form_url = ""
$serviceURL = "";
$clientID = "";
$MaintenanceURL = "";
$user = "";
$password= "";
#--------------------------------------

$ie = New-Object -com InternetExplorer.Application 
$ie.visible = $false
$ie.navigate($form_url) 
# wait for form to become available and populate
while($ie.ReadyState -ne 4) {start-sleep -m 100} 
$ie.document.getElementById("serviceURL").value = $serviceURL;
$ie.document.getElementById("clientID").value = $clientID ;
$ie.document.getElementById("MaintenanceURL").value = $MaintenanceURL;
$ie.document.getElementById("user").value = $user;
$ie.document.getElementById("password").value = $password;
# submit form
$submitButton = $ie.document.getElementById("setMM") 
Write-Host $submitButton.click()
# close request window
Start-Sleep -Seconds 2
$ie.Quit();
# close response window
Start-Sleep -Seconds 3
$shell = New-Object -ComObject Shell.Application
$ie2 = $shell.Windows() | Where-Object { $_.LocationURL -match $MaintenanceURL }
$ie2.Quit();

#NOTE: execute ONLY if IE is not being closed on server
#Get-Process iexplore | Foreach-Object { $_.CloseMainWindow() }