
/* ---------------------------------------------------------------------------
--    	Copyright 2020 Core Business Technologies
--
-- Creates table and Index for the CORE_MESSAGES.
--
-- TranSuite DB Version 
--
-- change based on sql script 
-- Last Updated: 
--    1.)  07/29/2020 TTS:20259 Frederick Thurber: Bug 21989: created  
*/---------------------------------------------------------------


IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'CORE_MESSAGES' 
  AND TABLE_SCHEMA = 'dbo' AND TABLE_TYPE = 'BASE TABLE'))
BEGIN
CREATE TABLE CORE_MESSAGES ( 
  MESSAGE_ID      INT IDENTITY(1,1) NOT NULL, -- auto-generated msg number.
  MESSAGE         varchar(1000)     NOT NULL, -- the message
  MESSAGE_TYPE    varchar(40)       NOT NULL, -- the message type
  SENDER_ID       varchar(40)       NOT NULL, -- iPayment user id that sent the message
  USERS           XML               NOT NULL, -- <v> of iPayment User ID
  WORKGROUPS      XML               NOT NULL, -- <v> of iPayment Dept ID
  PROFILES        XML               NOT NULL, -- <v> of iPayment Security_profile ID
  CLEARED         XML               NOT NULL, -- <v> of iPayment User ID for user cleared
  START_DT        datetime          NOT NULL, -- date and time of message start
  END_DT          datetime          NOT NULL, -- date and time of message expiration
  PRIMARY KEY(MESSAGE_ID))
 PRINT 'Created CORE_MESSAGES table'
END
ELSE 
PRINT 'There is already a CORE_MESSAGES table in the database'
GO
  
-- This index will only be used by the optimizer when the table gets large
IF (NOT EXISTS (SELECT * FROM sys.indexes WHERE name = 'IDX_CORE_MESSAGES_START_DT_END_DT'  AND object_id = OBJECT_ID ('dbo.CORE_MESSAGES')))
BEGIN
CREATE NONCLUSTERED INDEX [IDX_CORE_MESSAGES_START_DT_END_DT] ON [dbo].[CORE_MESSAGES]
(
	[START_DT] ASC,
	[END_DT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
