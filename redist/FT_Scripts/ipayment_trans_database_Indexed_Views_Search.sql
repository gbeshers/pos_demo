/* ---------------------------------------------------------------------------
--    	Copyright 2020 Core Business Technologies
--
-- Creates iPayment transactional indexed views for the iPayment Search Module
--
-- TranSuite DB Version 
--
-- change based on sql script 
-- Last Updated: 
--    1.)  05/13/2020 TTS:20259 Frederick Thurber: Moved the indexed views to their own scripts  
*/---------------------------------------------------------------

--
-- Need these connection options when creating the View, INSERT'ing into the tables
-- and maybe when doing the SELECT DISTINCT.
-- See https://www.brentozar.com/archive/2017/03/checking-strange-client-settings-sys-dm_exec_sessions/
-- See https://docs.microsoft.com/en-us/sql/relational-databases/views/create-indexed-views?view=sql-server-ver15
SET NUMERIC_ROUNDABORT  OFF /* bug 26148 FT */
SET ANSI_PADDING ON /* bug 25047 MPT */
SET ANSI_WARNINGS ON /* bug 25047 MPT */
SET ANSI_NULLS ON /* bug 26148 FT */
SET ARITHABORT ON /* bug 26148 FT */
SET CONCAT_NULL_YIELDS_NULL ON /* bug 25047 FT */
SET QUOTED_IDENTIFIER  ON /* bug 26148 FT */
GO

/*
	Indexed Views for these Criteria for the iPayment Search Module:

	SELECT DISTINCT SOURCE_TYPE FROM TG_DEPFILE_DATA  -- No Indexed View needed

	exec sp_executesql N'SELECT DISTINCT TG_TRAN_DATA.TTID, TG_TRAN_DATA.TTDESC FROM TG_TRAN_DATA WHERE NOT(ITEMIND=@itemind) ORDER BY TG_TRAN_DATA.TTID ASC, TG_TRAN_DATA.TTDESC ASC',N'@itemind varchar(1)',@itemind='T'


	exec sp_executesql N'SELECT DISTINCT TNDRID, TNDRDESC FROM TG_TENDER_DATA WHERE NOT( TNDRID is NULL or TNDRDESC=@tndrdesc0 or TNDRDESC=@tndrdesc1) ORDER BY TG_TENDER_DATA.TNDRID ASC, TG_TENDER_DATA.TNDRDESC ASC',N'@tndrdesc0 varchar(8),@tndrdesc1 varchar(18)',@tndrdesc0='''CHANGE''',@tndrdesc1='''System OverShort'''

	exec sp_executesql N'SELECT DISTINCT TG_PAYEVENT_DATA.USERID FROM TG_PAYEVENT_DATA WHERE NOT(TG_PAYEVENT_DATA.STATUS=@status)',N'@status varchar(1)',@status='5'

	select distinct DEPTID from TG_DEPFILE_DATA		-- No Indexed View Needed

*/

IF EXISTS (SELECT 1 FROM sys.views WHERE name = 'VIEW_INDEXED_TG_TRAN_DATA_TTID_TTDESC' and type = 'v')
DROP VIEW VIEW_INDEXED_TG_TRAN_DATA_TTID_TTDESC
GO
IF EXISTS (SELECT 1 FROM sys.views WHERE name = 'VIEW_INDEXED_TG_TENDER_DATA_DEPFILE_TNDRID_TNDRDESC' and type = 'v')
DROP VIEW VIEW_INDEXED_TG_TENDER_DATA_DEPFILE_TNDRID_TNDRDESC
GO
IF EXISTS (SELECT 1 FROM sys.views WHERE name = 'VIEW_INDEXED_TG_PAYEVENT_DATA_USERID_STATUS' and type = 'v')
DROP VIEW VIEW_INDEXED_TG_PAYEVENT_DATA_USERID_STATUS
GO

--
-- TG_TRAN_DATA
--
-- For Queries such as this:  
-- SELECT DISTINCT TG_TRAN_DATA.TTID, TG_TRAN_DATA.TTDESC FROM TG_TRAN_DATA 
-- WHERE NOT(ITEMIND='T') 
-- ORDER BY TG_TRAN_DATA.TTID ASC, TG_TRAN_DATA.TTDESC ASC
-- 
-- or
-- SELECT DISTINCT TG_TRAN_DATA.TTID, TG_TRAN_DATA.TTDESC FROM TG_TRAN_DATA JOIN...
-- (https://www.brentozar.com/pastetheplan/?id=SyEoqNET4)  
--
--

CREATE VIEW VIEW_INDEXED_TG_TRAN_DATA_TTID_TTDESC
WITH SCHEMABINDING
AS
    SELECT TTID, TTDESC, ITEMIND, COUNT_BIG(*) as TAG_LABEL_COUNT
    FROM dbo.TG_TRAN_DATA
    GROUP BY TTID, TTDESC, ITEMIND
GO
CREATE UNIQUE CLUSTERED INDEX IDX_VIEW_INDEXED_TG_TRAN_DATA_TTID_TTDESC ON VIEW_INDEXED_TG_TRAN_DATA_TTID_TTDESC (TTID, TTDESC, ITEMIND)
GO



--
-- TG_TENDER_DATA
--
-- See Search Module Search Criteria BLL in Tender.casl.
-- Here is the basic Search query without specifiying the department ID:
-- 
-- SELECT DISTINCT TNDRID, TNDRDESC FROM TG_TENDER_DATA 
-- WHERE NOT( TNDRID is NULL or TNDRDESC='CHANGE' or TNDRDESC='System OverShort') 
-- ORDER BY TG_TENDER_DATA.TNDRID ASC, TG_TENDER_DATA.TNDRDESC ASC
--
-- or with the DEPTID
-- SELECT DISTINCT TNDRID, TNDRDESC FROM TG_TENDER_DATA JOIN TG_DEPFILE_DATA ...
-- (https://www.brentozar.com/pastetheplan/?id=Sy-UiE4pE) 
--

CREATE VIEW VIEW_INDEXED_TG_TENDER_DATA_DEPFILE_TNDRID_TNDRDESC
WITH SCHEMABINDING
AS
    SELECT t.TNDRID, t.TNDRDESC, COUNT_BIG(*) as TAG_LABEL_COUNT
    FROM dbo.TG_TENDER_DATA t 
    GROUP BY t.TNDRID, t.TNDRDESC
GO
CREATE UNIQUE CLUSTERED INDEX idx_VIEW_INDEXED_TRAN_DEPFILE_TTID_TTDESC_DEPTID ON VIEW_INDEXED_TG_TENDER_DATA_DEPFILE_TNDRID_TNDRDESC (TNDRID, TNDRDESC)
GO


--
-- TG_PAYEVENT_DATA
--
-- For this Search query:
--	exec sp_executesql N'SELECT DISTINCT TG_PAYEVENT_DATA.USERID 
--	FROM TG_PAYEVENT_DATA 
--	WHERE NOT(TG_PAYEVENT_DATA.STATUS=@status)',N'@status varchar(1)',@status='5'
--
CREATE VIEW VIEW_INDEXED_TG_PAYEVENT_DATA_USERID_STATUS
WITH SCHEMABINDING
AS
    SELECT USERID, STATUS, COUNT_BIG(*) as TAG_LABEL_COUNT
    FROM dbo.TG_PAYEVENT_DATA  
    GROUP BY USERID, STATUS
GO
CREATE UNIQUE CLUSTERED INDEX idx_VIEW_INDEXED_TG_PAYEVENT_DATA_USERID_STATUS ON VIEW_INDEXED_TG_PAYEVENT_DATA_USERID_STATUS (USERID, STATUS)
GO


