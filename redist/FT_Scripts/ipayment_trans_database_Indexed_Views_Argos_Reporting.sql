/* ---------------------------------------------------------------------------
--    	Copyright 2020 Core Business Technologies
--
-- Creates iPayment transactional indexed views for ARGOS
--
-- TranSuite DB Version 
--
-- change based on sql script 
-- Last Updated: 
--    1.)  05/13/2020 TTS:20259 Frederick Thurber: Moved the indexed views to their own scripts  
*/---------------------------------------------------------------

--
-- Need these connection options when creating the View, INSERT'ing into the tables
-- and maybe when doing the SELECT DISTINCT.
-- See https://www.brentozar.com/archive/2017/03/checking-strange-client-settings-sys-dm_exec_sessions/
-- See https://docs.microsoft.com/en-us/sql/relational-databases/views/create-indexed-views?view=sql-server-ver15
--
SET NUMERIC_ROUNDABORT  OFF /* bug 26148 FT */
SET ANSI_PADDING ON /* bug 25047 MPT */
SET ANSI_WARNINGS ON /* bug 25047 MPT */
SET ANSI_NULLS ON /* bug 26148 FT */
SET ARITHABORT ON /* bug 26148 FT */
SET CONCAT_NULL_YIELDS_NULL ON /* bug 25047 FT */
SET QUOTED_IDENTIFIER  ON /* bug 26148 FT */
GO

-- Drop Existing Views
IF EXISTS (SELECT 1 FROM sys.views WHERE name = 'VIEW_INDEXED_CUST_FIELD_DATA_CUSTID' and type = 'v')
DROP VIEW VIEW_INDEXED_CUST_FIELD_DATA_CUSTID
GO
IF EXISTS (SELECT 1 FROM sys.views WHERE name = 'VIEW_INDEXED_GR_CUST_FIELD_DATA_CUSTID' and type = 'v')
DROP VIEW VIEW_INDEXED_GR_CUST_FIELD_DATA_CUSTID
GO

-- Create Views

--
-- GR_CUST_FIELD_DATA
--
-- Use this to speed up this query:
-- For Argos criteria:
-- SELECT DISTINCT GR_CUST_FIELD_DATA.CUSTID
-- FROM            GR_CUST_FIELD_DATA with (NOLOCK)
-- order by CUSTID asc
--
CREATE VIEW VIEW_INDEXED_GR_CUST_FIELD_DATA_CUSTID
WITH SCHEMABINDING
AS
    SELECT CUSTID,COUNT_BIG(*) as TAG_LABEL_COUNT
    FROM dbo.GR_CUST_FIELD_DATA
    GROUP BY CUSTID
GO
CREATE UNIQUE CLUSTERED INDEX IDX_VIEW_INDEXED_GR_CUST_FIELD_DATA_CUSTID ON VIEW_INDEXED_GR_CUST_FIELD_DATA_CUSTID (CUSTID)
GO

--
-- CUST_FIELD_DATA
-- Use this to speed up this query:
-- Used for Argos criteria
-- select distinct CUST_FIELD_DATA.CUSTID from dbo.CUST_FIELD_DATA
--
CREATE VIEW VIEW_INDEXED_CUST_FIELD_DATA_CUSTID
WITH SCHEMABINDING
AS
    SELECT CUSTID,COUNT_BIG(*) as TAG_LABEL_COUNT
    FROM dbo.CUST_FIELD_DATA
    GROUP BY CUSTID
GO
CREATE UNIQUE CLUSTERED INDEX IDX_VIEW_INDEXED_CUST_FIELD_DATA_CUSTID ON VIEW_INDEXED_CUST_FIELD_DATA_CUSTID (CUSTID)
GO

