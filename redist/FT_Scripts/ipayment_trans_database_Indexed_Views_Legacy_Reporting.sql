/* ---------------------------------------------------------------------------
--    	Copyright 2020 Core Business Technologies
--
-- Creates iPayment transactional indexed views for LEGACY Reporting
--
-- TranSuite DB Version 
--
-- change based on sql script 
-- Last Updated: 
--    1.)  05/13/2020 TTS:20259 Frederick Thurber: Moved the indexed views to their own scripts  
*/---------------------------------------------------------------

--
-- Need these connection options when creating the View, INSERT'ing into the tables
-- and maybe when doing the SELECT DISTINCT.
-- See https://www.brentozar.com/archive/2017/03/checking-strange-client-settings-sys-dm_exec_sessions/
-- See https://docs.microsoft.com/en-us/sql/relational-databases/views/create-indexed-views?view=sql-server-ver15
--
SET NUMERIC_ROUNDABORT  OFF /* bug 26148 FT */
SET ANSI_PADDING ON /* bug 25047 MPT */
SET ANSI_WARNINGS ON /* bug 25047 MPT */
SET ANSI_NULLS ON /* bug 26148 FT */
SET ARITHABORT ON /* bug 26148 FT */
SET CONCAT_NULL_YIELDS_NULL ON /* bug 25047 FT */
SET QUOTED_IDENTIFIER  ON /* bug 26148 FT */
GO

-- Drop Existing Views
IF EXISTS (SELECT 1 FROM sys.views WHERE name = 'VIEW_INDEXED_GR_CUST_FIELD_DATA_CUSTTAG_CUSTLABEL' and type = 'v')
DROP VIEW VIEW_INDEXED_GR_CUST_FIELD_DATA_CUSTTAG_CUSTLABEL
GO
IF EXISTS (SELECT 1 FROM sys.views WHERE name = 'VIEW_INDEXED_CUST_FIELD_DATA_CUSTTAG_CUSTLABEL' and type = 'v')
DROP VIEW VIEW_INDEXED_CUST_FIELD_DATA_CUSTTAG_CUSTLABEL
GO

-- Create Views

--
-- GR_CUST_FIELD_DATA
--
-- For Legacy Reporting and possibly search criteria, optimize this query
-- SELECT DISTINCT CUSTTAG, CUSTLABEL, CUSTID FROM GR_CUST_FIELD_DATA WHERE NOT(CUSTTAG IS NULL)ORDER BY CUSTLABEL
--
CREATE VIEW VIEW_INDEXED_GR_CUST_FIELD_DATA_CUSTTAG_CUSTLABEL
WITH SCHEMABINDING
AS
    SELECT CUSTTAG, CUSTLABEL, CUSTID, COUNT_BIG(*) as TAG_LABEL_COUNT
    FROM dbo.GR_CUST_FIELD_DATA
    GROUP BY CUSTTAG, CUSTLABEL, CUSTID
GO
CREATE UNIQUE CLUSTERED INDEX IDX_VIEW_INDEXED_GR_CUST_FIELD_DATA_CUSTTAG_CUSTLABEL ON VIEW_INDEXED_GR_CUST_FIELD_DATA_CUSTTAG_CUSTLABEL (CUSTTAG, CUSTLABEL, CUSTID)
GO


--
-- CUST_FIELD_DATA
-- Used for Legacy reporting and possibly search criteria
-- SELECT DISTINCT CUSTTAG, CUSTLABEL, CUSTID FROM CUST_FIELD_DATA WHERE NOT(CUSTTAG IS NULL)ORDER BY CUSTLABEL
--
CREATE VIEW VIEW_INDEXED_CUST_FIELD_DATA_CUSTTAG_CUSTLABEL
WITH SCHEMABINDING
AS
    SELECT CUSTTAG, CUSTLABEL, CUSTID, COUNT_BIG(*) as TAG_LABEL_COUNT
    FROM dbo.CUST_FIELD_DATA
    GROUP BY CUSTTAG, CUSTLABEL, CUSTID
GO
CREATE UNIQUE CLUSTERED INDEX IDX_VIEW_INDEXED_CUST_FIELD_DATA_CUSTTAG_CUSTLABEL ON VIEW_INDEXED_CUST_FIELD_DATA_CUSTTAG_CUSTLABEL (CUSTTAG, CUSTLABEL, CUSTID)
GO
