
/* ---------------------------------------------------------------------------
--    	Copyright 2016 Core Business Technologies
--
--  	SQL Server DB ONLY
--		DESCRIPTION:
--					Creates a new fields called IMPORT_TIMESTAMP in the 
--                  the bank rec TG_BANK_DEPOSIT_DATA.

--
-- 		Last Updated: 
--    	1.)  4/7/2016 by Frederick Thurber.  Bug 18982
--         	Notes: Created this file.
*/

IF NOT EXISTS( SELECT TOP 1 1 
	FROM INFORMATION_SCHEMA.COLUMNS		
	WHERE [TABLE_NAME] = 'TG_BANK_DEPOSIT_DATA' AND [COLUMN_NAME] = 'IMPORT_TIMESTAMP') 
BEGIN 
	ALTER TABLE [TG_BANK_DEPOSIT_DATA] ADD [IMPORT_TIMESTAMP] DATETIME NULL 
END
GO

IF NOT EXISTS( SELECT TOP 1 1 
	FROM INFORMATION_SCHEMA.COLUMNS		
	WHERE [TABLE_NAME] = 'TG_BANK_DEPOSIT_DATA' AND [COLUMN_NAME] = 'IMPORT_TIMESTAMP') 
BEGIN 
	UPDATE TG_BANK_DEPOSIT_DATA SET IMPORT_TIMESTAMP = null
END
GO



