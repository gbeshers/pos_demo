/*Bug 13915 -
A. New column POSTUSERID in TG_TRAN_DATA to store user id who post individual transaction
B. New column POSTUSERID in TG_TENDER_DATA to store user id who post individual tender 
*/
ALTER TABLE TG_TRAN_DATA ADD POSTUSERID VARCHAR(20) NULL
ALTER TABLE TG_TENDER_DATA ADD POSTUSERID VARCHAR(20) NULL