	
/* ---------------------------------------------------------------------------
--    	Copyright 2017 Core Business Technologies
--
--  	SQL Server DB ONLY
--		DESCRIPTION:
--      2017/02/10 Add  TG_DEPOSITTNDRCURRENCY_DATA TABLE to store foreign currency info for depoist TTS20815 
 ---------------------------------------------------------------------------
 */
	CREATE TABLE TG_DEPOSITTNDRCURRENCY_DATA (
	DEPOSITID		VARCHAR (    12)	NOT NULL, 
	DEPOSITNBR		[INT]   	NOT NULL,
	SEQNBR			[INT]	 	NOT NULL,
	CURRENCYTYPE	VARCHAR (    40)	NULL,
	CURRENCYAMOUNT		DECIMAL (19, 4) 	NULL,
	CURRENCYEXCHANGERATE DECIMAL (19, 9)  	NULL,
	CHECKSUM	    VARCHAR (50) NULL, 
	PRIMARY KEY(DEPOSITID, DEPOSITNBR, SEQNBR))