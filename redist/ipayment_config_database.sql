/* ---------------------------------------------------------------------------
--    	Copyright 2006-2019 Core Business Technologies
--
-- Creates iPayment configuration tables and indexes in an empty SQL Server DB
--
-- TranSuite DB Version 4.0.9.0
--
-- change based on sql script 4.0.9.0
-- Last Updated: 
--    1.)  12/06/07 v4.0.8.0 by SHUANG XIAO 
--         Notes: Add NEW COLUMN: TG_DEPFILE_DATA.CREATOR_ID BUG#4602/PRF
--    2.)  02/21/08 v4.0.10.0 by CODY LEE
--         Notes: Add NEW TABLE: TG_TTA_MAP_DATA
--    3.)  04/23/08 v4.1.0.0 by SHUANG XIAO 
--         Add NEW COLUMN: TG_DEPFILE_DATA.CLOSE_USERID, CLOSEDT BUG#5396
--    4.)  05/20/08 v4.1.0.0  by SHUANG XIAO 
--         Add NEW COLUMN:  TG_DEPOSIT_DATA.ACC_USERID, ACCDT BUG#5530
--    5.)  05/23/08 v4.1.1 by Mike Plusch
--         No schema changes, just reorganized and split into separate data and config files
--    6.)  08/03/2009 Converted 4k fields to VARCHAR(MAX). BUG# 7352 This will generate an error on <2005. MAX started with 2005.
--	  7.)  08/16/2010 Updated size to 20 for column [USER] in CBT_Revision Bug#8665 NJ
--    8)   09/03/2014 UMN Added if exists checking, and index creation, so can rerun script just to create the indexes.
--    9.)  02/17/2021 FT: IPAY-209: Dropped redundant index (non-clustered)
     10.)  03/29/2021 MJO: IPAY-904: Added LOCATION_ID and P2PE_FLAG to TG_DEVICE_ID table
-- --------------------------------------------------------------------------
*/
SET NOCOUNT ON;

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'CBT_FILEIMPORT_CONFIG' 
  AND TABLE_SCHEMA = 'dbo' AND TABLE_TYPE = 'BASE TABLE'))
BEGIN
	CREATE TABLE [dbo].[CBT_FILEIMPORT_CONFIG](
		[FILETYPE] [varchar](40) NOT NULL,
		[FILEDEFINITION] [varchar](8000) NOT NULL,
	 CONSTRAINT [PK__CBT_FILE__CC4E5AC35CA1C101] PRIMARY KEY CLUSTERED 
	(
		[FILETYPE] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
	PRINT 'Created CBT_FILEIMPORT_CONFIG table'
END

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'CBT_Flex' 
  AND TABLE_SCHEMA = 'dbo' AND TABLE_TYPE = 'BASE TABLE'))
BEGIN
	CREATE TABLE [dbo].[CBT_Flex](
		[container] [varchar](255) NOT NULL,
		[field_key] [varchar](255) NOT NULL,
		[field_value] [varchar](8000) NOT NULL,
		[revision] [int] NOT NULL,
		[is_root] [int] NULL
	) ON [PRIMARY]
	PRINT 'Created CBT_Flex table'
END
--MPT: Remove CBT_Flex_byte for unused reason bug 22934
IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME ='CBT_Flex_bytes'
AND TABLE_SCHEMA = 'dbo' AND TABLE_TYPE = 'BASE TABLE'))
BEGIN
	DROP TABLE CBT_Flex_bytes
	PRINT 'Remove CBT_Flex_bytes table'
END

IF(NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME ='CBT_Revision'
AND TABLE_SCHEMA = 'dbo' AND TABLE_TYPE = 'BASE TABLE'))
BEGIN
	CREATE TABLE [dbo].[CBT_Revision](
		[id] [int] NOT NULL,
		[timestamp] [varchar](255) NULL,
		[user] [varchar](20) NULL,
		[description] [varchar](255) NULL
	) ON [PRIMARY]
	PRINT 'Created CBT_Revision table'
END

IF(NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME ='CONNECT_TEST'
AND TABLE_SCHEMA = 'dbo' AND TABLE_TYPE = 'BASE TABLE'))
BEGIN
	CREATE TABLE [dbo].[CONNECT_TEST](
		[TEST_COLUMN] [smallint] NOT NULL,
	 CONSTRAINT [PK__CONNECT_TEST__76CBA758] PRIMARY KEY CLUSTERED 
	(
		[TEST_COLUMN] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
	) ON [PRIMARY]
	PRINT 'Created CONNECT_TEST table'
END

IF(NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME ='TG_DEVICE_ID'
AND TABLE_SCHEMA = 'dbo' AND TABLE_TYPE = 'BASE TABLE'))
BEGIN
	CREATE TABLE [dbo].[TG_DEVICE_ID](
		[DEVICE_ID] [int] IDENTITY(1,1) NOT NULL,
		[DEVICE_TYPE] [varchar](100) NOT NULL,
		[SERIAL_NBR] [varchar](100) NOT NULL,
		[EXTERNAL_ID] [varchar](100) NULL,
		[LOCATION_ID] [varchar](75) DEFAULT('') NOT NULL,
		[P2PE_FLAG] [BIT] DEFAULT(0) NOT NULL
	 CONSTRAINT [PK_TG_DEVICE_ID] PRIMARY KEY CLUSTERED 
	(
		[DEVICE_TYPE] ASC,
		[SERIAL_NBR] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
	PRINT 'Created TG_DEVICE_ID table'
END

IF(NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME ='TG_DYNAMIC_CONFIG'
AND TABLE_SCHEMA = 'dbo' AND TABLE_TYPE = 'BASE TABLE'))
BEGIN
	CREATE TABLE [dbo].[TG_DYNAMIC_CONFIG](
		[PATH] [varchar](300) NOT NULL,
		[VAL] [varchar](max) NULL,
		[VAL_KIND] [varchar](100) NULL,
	 CONSTRAINT [PK__TG_DYNAMIC_CONFI__4316F928] PRIMARY KEY CLUSTERED 
	(
		[PATH] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
	PRINT 'Created TG_DYNAMIC_CONFIG table'
END
-- UMN added this FROM Fred's SF script. Remove the indexes =====================================================
IF EXISTS (SELECT name FROM sysindexes WHERE name = 'PK_CBT_Flex')
  drop index [PK_CBT_Flex] on CBT_Flex;
IF EXISTS (SELECT name FROM sysindexes WHERE name = 'PK_CBT_Revision')
  drop index [PK_CBT_Revision] on CBT_Revision;
IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_CBT_Flex_container_field_key_field_value')
  drop index [IX_CBT_Flex_container_field_key_field_value] on CBT_Flex;
GO

-- IPAY-209: Deleted redundant indexes


--- Drop old indexes

IF EXISTS (SELECT name FROM sysindexes WHERE name = 'CBT_Flex_pid')
  drop index [CBT_Flex_pid] on [dbo].[CBT_Flex];
IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IDX_NTI_CBT_Flex_container_field_key_revision_Enterprise')
  drop index [IDX_NTI_CBT_Flex_container_field_key_revision_Enterprise] on [dbo].[CBT_Flex];
IF EXISTS (SELECT name FROM sysindexes WHERE name = 'CBT_Revision_pid')
  drop index [CBT_Revision_pid] on [dbo].[CBT_Revision];

  

--MTP Removed for duplicated indexes Bug 20519
--- Add standard indexes
--CREATE UNIQUE INDEX PK_CBT_Flex ON [dbo].[CBT_Flex]
--(
--  [container], 
--  [field_key], 
--  [revision]
--) ON [PRIMARY];

CREATE UNIQUE INDEX PK_CBT_Revision ON CBT_Revision
(
  [id]
) ON [PRIMARY];

--MTP Removed for duplicated indexes Bug 20519
--MTP Added Clustered Index back Bug 27089
--- Add new clustered index

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = 'IX_CBT_Flex_container_field_key_field_value_revision' AND object_id = OBJECT_ID('[dbo].[CBT_Flex]'))
BEGIN
CREATE CLUSTERED INDEX [IX_CBT_Flex_container_field_key_field_value_revision] ON [dbo].[CBT_Flex] 
(
  [container] ASC,
  [field_key] ASC,
  [revision] ASC
) ON [PRIMARY]
END
GO

-- UMN FROM Performance Script
-- UMN Apply Table Compression only if SQL Server version supports it
DECLARE @compression VARCHAR(128)
set @compression = 'NONE'
IF (EXISTS (SELECT * FROM sys.dm_db_persisted_sku_features where feature_name = 'Compression'))
  set @compression = 'PAGE'
Exec ('ALTER INDEX ALL ON CBT_Flex REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
--Exec('ALTER INDEX ALL ON CBT_Flex_bytes REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') //Remove bug 22934
Exec ('ALTER INDEX ALL ON CBT_Revision REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON CONNECT_TEST REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON TG_DYNAMIC_CONFIG REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TG_USERSESSION_DATA]') AND type in (N'U'))
	Exec ('ALTER INDEX ALL ON TG_USERSESSION_DATA REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')')

Exec ('ALTER TABLE [dbo].[CBT_Flex] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')')
--Exec ('ALTER TABLE [dbo].[CBT_Flex_bytes] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')') //Remove bug 22934
Exec ('ALTER TABLE [dbo].[CBT_Revision] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')')
Exec ('ALTER TABLE [dbo].[CONNECT_TEST] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')')
Exec ('ALTER TABLE [dbo].[TG_DYNAMIC_CONFIG] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')')
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TG_USERSESSION_DATA]') AND type in (N'U'))
	Exec ('ALTER TABLE [dbo].[TG_USERSESSION_DATA] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')')
GO
UPDATE STATISTICS CBT_Flex WITH FULLSCAN
GO
--Remove bug 22934
--UPDATE STATISTICS CBT_Flex_bytes WITH FULLSCAN
--GO
UPDATE STATISTICS CBT_Revision WITH FULLSCAN
GO
UPDATE STATISTICS CONNECT_TEST WITH FULLSCAN
GO
UPDATE STATISTICS TG_DYNAMIC_CONFIG WITH FULLSCAN
GO
PRINT 'Created Config DB indexes.'

-- Print out a list of the new clustered and non-clustered indexes, and stats
SELECT OBJECT_NAME(i.id) AS 'TABLE_NAME', i.name AS 'CLUSTERED_INDEX'
FROM sysindexes i
WHERE i.indid between 1 AND 249
AND INDEXPROPERTY(i.id, i.name, 'IsClustered') = 1
AND INDEXPROPERTY(i.id, i.name, 'IsStatistics') = 0
AND INDEXPROPERTY(i.id, i.name, 'IsAutoStatistics') = 0
AND INDEXPROPERTY(i.id, i.name, 'IsHypothetical') = 0
AND OBJECTPROPERTY(i.id,'IsMSShipped') = 0
AND not exists (SELECT *
FROM sysobjects o
WHERE xtype = 'UQ'
AND o.name = i.name ) 

SELECT OBJECT_NAME(i.id) as 'TABLE_NAME', i.name as 'NON_CLUSTERED_INDEX'
FROM sysindexes i
WHERE i.indid between 1 AND 249
AND INDEXPROPERTY(i.id, i.name, 'IsClustered') = 0
AND INDEXPROPERTY(i.id, i.name, 'IsStatistics') = 0
AND INDEXPROPERTY(i.id, i.name, 'IsAutoStatistics') = 0
AND INDEXPROPERTY(i.id, i.name, 'IsHypothetical') = 0
AND OBJECTPROPERTY(i.id,'IsMSShipped') = 0
AND not exists (SELECT *
FROM sysobjects o
WHERE xtype = 'UQ'
AND o.name = i.name ) 

SELECT OBJECT_NAME(i.id) as 'TABLE_NAME', i.name as 'STATISTICS'
FROM sysindexes i
WHERE i.indid between 1 AND 249
AND INDEXPROPERTY(i.id, i.name, 'IsStatistics') = 1
AND INDEXPROPERTY(i.id, i.name, 'IsAutoStatistics') = 0
AND INDEXPROPERTY(i.id, i.name, 'IsHypothetical') = 0
AND OBJECTPROPERTY(i.id,'IsMSShipped') = 0
AND not exists (SELECT *
FROM sysobjects o
WHERE xtype = 'UQ'
AND o.name = i.name )