/*
    Copyright 2013 Core Business Technologies

  	SQL Server DB ONLY
  	
	This script was created by Daniel Hofman on 2/14/2013 for bug# 3772.
    Stored Procedure Name:  sp_ImageRetention
    Archive Database Name:  T4ArchiveDB
    Purpose:				Stored procedure to be called from T4 or SQL Server maintenance job for the retention period processing

    Changes on 2/26/2018 to be reviewed by developer: 
	  Ann edited this script on 2/26/2018 for testing against current base databases. 
          The CBT_Flex table is no longer in the transaction database so the user will need to modify this script to read from the config database. 
	  Please edit ipayment_test_config.dbo.CBT_Flex
          **NOTE**We need to consider the possible inconsistent data left behind from the transaction script (trans script can be limited by nbr of depfiles and date, image table is just date) 


*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET NOCOUNT ON
GO

IF OBJECT_ID('sp_ImageRetention') IS NOT NULL 
	DROP PROCEDURE sp_ImageRetention;
GO

CREATE PROCEDURE sp_ImageRetention
AS

DECLARE @ImageDataRetentionPeriod int = 0;
DECLARE @container varchar(255)='';
DECLARE @field_key varchar(255)='';
DECLARE @field_value varchar(400)='';
DECLARE @DoArchive varchar(5) = 'true';
DECLARE @archive_db_name varchar(30)='';
DECLARE @CountOfRecords int =0; -- Reporting
DECLARE @TimeStamp DateTime; -- Reporting
SET @TimeStamp = CURRENT_TIMESTAMP;


-- Errors
DECLARE @ERROR_MSG varchar(MAX)='';

-- ==================== Retrieve the configuration settings ==============
SET @ImageDataRetentionPeriod = 0; -- OFF by default

BEGIN TRY
-- Create the reporting table in case has not been created yet
IF OBJECT_ID('[dbo].[TG_RETENTION_LOG]') IS NULL
BEGIN
CREATE TABLE [dbo].[TG_RETENTION_LOG](
	[RECORD_ID] [int] IDENTITY(1,1) NOT NULL, -- Auto incrementing primary key
	[ENTRY_DATE] [datetime] NOT NULL, -- Date this record was created
	[DBNAME] [varchar](30) NOT NULL, -- Database name where this stored procedure is running
	[ARCHIVE_DBNAME] [varchar](30) NOT NULL, -- Configure archive database name
	[OPERATION_NAME] [varchar](30) NOT NULL, -- Transactional Data Retention / Image Data Retention / Activity Log Data Retention
	[RETENTION_PERIOD] int NOT NULL, -- Configured retention period represented in days
	[OPERATION_RESULT] [varchar](10) NOT NULL, -- Success / Failed
	[RECORDS_COMPLETED] varchar(50) NULL,-- Number of payfiles processed. In case of Imaging & ActivityLog, this is the number of records processed.
	[PAYILES][varchar](MAX) NULL, -- Payfile numbers that were completed
	[ERROR_DESC] [varchar](MAX) NULL, -- Error description
PRIMARY KEY CLUSTERED 
(
	[RECORD_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END

-- Archive database name
SET @container = NULL;
SET @field_key = NULL;
SET @field_value = NULL;
DECLARE DBNameCursor CURSOR FOR
SELECT container, field_key, field_value FROM ipayment_test_config.dbo.CBT_Flex WHERE container='Business.data_retention.data' AND (field_key = '"archive_database_name"')
OPEN DBNameCursor
FETCH FROM DBNameCursor INTO @container, @field_key, @field_value
	IF @field_value IS NOT NULL AND @@FETCH_STATUS = 0
	BEGIN
		SET @archive_db_name=@field_value
	END
CLOSE DBNameCursor;
DEALLOCATE DBNameCursor;

-- Image data retention period
SET @container = NULL;
SET @field_key = NULL;
SET @field_value = NULL;
DECLARE ImageDataRetention CURSOR FOR
SELECT container, field_key, field_value FROM ipayment_test_config.dbo.CBT_Flex WHERE container='Business.data_retention.data' AND (field_key = '"image_data_retention"')
OPEN ImageDataRetention
FETCH FROM ImageDataRetention INTO @container, @field_key, @field_value
	IF @field_value IS NOT NULL AND @@FETCH_STATUS = 0
	BEGIN
		SET @ImageDataRetentionPeriod=@field_value
	END
CLOSE ImageDataRetention;
DEALLOCATE ImageDataRetention;


-- Should we archive data as well
SET @DoArchive = 'true';
DECLARE ArchiveData CURSOR FOR
SELECT container, field_key, field_value FROM ipayment_test_config.dbo.CBT_Flex WHERE container='Business.data_retention.data' AND (field_key = '"do_archive"')
OPEN ArchiveData
FETCH FROM ArchiveData INTO @container, @field_key, @field_value
	IF @field_value IS NOT NULL AND @@FETCH_STATUS = 0
	BEGIN
		SET @DoArchive=@field_value
	END
CLOSE ArchiveData;
DEALLOCATE ArchiveData;

END TRY
BEGIN CATCH
INSERT INTO TG_RETENTION_LOG VALUES(@TimeStamp, DB_NAME(), @archive_db_name, 'Image Data Retention', @ImageDataRetentionPeriod,'Failed', 'Images Processed: 0', '', ERROR_MESSAGE());
RETURN;
END CATCH
-- ==================== END Retrieving the configuration settings ==============

BEGIN TRY
    IF @ImageDataRetentionPeriod > 0
    BEGIN
	BEGIN TRANSACTION
	IF @DoArchive = 'true'
	BEGIN
		select @CountOfRecords = COUNT(*) from TG_IMAGE_DATA WHERE datediff(day, DATE_INSERTED, getdate()) >= @ImageDataRetentionPeriod;
		
		INSERT INTO T4ArchiveDB.dbo.TG_IMAGE_DATA([DEPFILENBR],[DEPFILESEQ],[EVENTNBR],[POSTNBR],[POST_TYPE],[SEQ_NBR],[DOC_IMAGE],[IMAGING_TYPE],[COMPLETE_MICR_LINE],[BANK_ROUTING_NUMBER],[ACCOUNT_NUMBER],[CHECK_NUMBER],[DATE_INSERTED],[USERID],[COMMENTS],[SOURCE_TYPE],[SOURCE_GROUP],[SOURCE_REFID],[SOURCE_DATE],[SCAN_DATA],[DOC_IMAGE_REF],[IMAGE_FORMAT],[CHECKSUM],[DOC_ID])
		SELECT [DEPFILENBR],[DEPFILESEQ],[EVENTNBR],[POSTNBR],[POST_TYPE],[SEQ_NBR],[DOC_IMAGE],[IMAGING_TYPE],[COMPLETE_MICR_LINE],[BANK_ROUTING_NUMBER],[ACCOUNT_NUMBER],[CHECK_NUMBER],[DATE_INSERTED],[USERID],[COMMENTS],[SOURCE_TYPE],[SOURCE_GROUP],[SOURCE_REFID],[SOURCE_DATE],[SCAN_DATA],[DOC_IMAGE_REF],[IMAGE_FORMAT],[CHECKSUM],[DOC_ID] FROM TG_IMAGE_DATA
		WHERE datediff(day, DATE_INSERTED, getdate()) >= @ImageDataRetentionPeriod
	END
	delete from TG_IMAGE_DATA WHERE datediff(day, DATE_INSERTED, getdate()) >= @ImageDataRetentionPeriod
	COMMIT TRANSACTION;
	END
END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION;
	INSERT INTO TG_RETENTION_LOG VALUES(@TimeStamp, DB_NAME(), @archive_db_name, 'Image Data Retention', @ImageDataRetentionPeriod, 'Failed', 'Images Processed: 0', '', ERROR_MESSAGE());
	RETURN;
END CATCH

INSERT INTO TG_RETENTION_LOG VALUES(@TimeStamp, DB_NAME(), @archive_db_name, 'Image Data Retention', @ImageDataRetentionPeriod, 'Success', 'Images Processed: ' + CAST(@CountOfRecords as varchar(8)), '', '');
GO
RETURN;
