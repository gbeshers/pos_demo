:: This file is for consolidating encrypted sites
:: Last Updated 1-14-2019 by George Beshers
::
:: Usage example: C:\T4\pos_demo\redist>Build_iPayment.bat D:\T4\project1 D:\Staging\project1
::

@echo off

:: UMN prompt for from_folder and to_folder, so FAEs don't
:: have to edit this file just to set the paths!
set /p from_folder_entered= "Enter from folder full path (eg: D:\T4\NYC) > "
set /p to_folder_entered= "Enter destination folder full path (eg: D:\T4\NYC_build_1\) > "

:: BUG 25187 SM - Check for backslash at end of from_folder_entered string specified by user and remove if it exists. 
set lastchar=%from_folder_entered:~-1%
set lastcharto=%to_folder_entered:~-1%
IF %lastchar% == \ (set from_folder=%from_folder_entered:~0,-1%) else (set from_folder=%from_folder_entered%)
IF %lastcharto% == \ (set to_folder=%to_folder_entered:~0,-1%) else (set to_folder=%to_folder_entered%)

:: UMN make sure untemplate is done so developer changes to Web.config.TEMPLATE 
:: are made available to FAE in Web.config.TEMPLATE
for /R %%f in (%from_folder%\*.TEMPLATE) DO copy "%%f" "%%~df%%~pf%%~nf" /Y

echo If you haven't created the casle files, then use Control-C to cancel this script.
echo Otherwise, press the space bar to continue.
pause

echo Copying casle and other files to destination folder...

xcopy %from_folder%\*.casle /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.htm /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.asax /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.asmx /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.aspx /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.resx /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.js /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.css /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.config /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.ocx /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.dll /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.bat /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.exe /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.msi /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.pdb /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.lic /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.ilk /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.ini /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.bmp /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.jpg /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.gif /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.png /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.ico /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.vbs /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.ttf /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.svg /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.eot /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.woff /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.woff2 /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.xsd /s %to_folder% /d/y 2> nul
xcopy %from_folder%\*.xsl /s %to_folder% /d/y 2> nul
xcopy %from_folder%\doc\Google_Authenticator_Setup.doc /s %to_folder%\doc\ /d/y 2> nul
xcopy %from_folder%\baseline\BarcodeGenerator\*.* /s %to_folder%\baseline\BarcodeGenerator /d/y 2> nul
xcopy %from_folder%\baseline\ABAValidation\file_download\FedACHdir.abab /s %to_folder%\baseline\ABAValidation\file_download\* /d/y 2> nul
xcopy %from_folder%\baseline\business\include\templates\*.* /s %to_folder%\baseline\business\include\templates\* /d/y 2> nul
xcopy %from_folder%\baseline\system_interface\resources\*.* /s %to_folder%\baseline\system_interface\resources\* /d/y 2> nul
xcopy %from_folder%\main_menu.html /s %to_folder% /d/y 2> nul
xcopy %from_folder%\config.casl %to_folder% /d/y 2> nul

:: UMN - make sure SVN directories are removed!
echo Removing unecessary files in destination folder...

rmdir %to_folder%\.svn /S/Q 2> nul
rmdir %to_folder%\_svn /S/Q 2> nul

del %to_folder%*template* /S/Q 2> nul
rmdir %to_folder%\baseline\redist\TranGateway3.7.4 /S/Q 2> nul
rmdir %to_folder%\baseline\redist\TranGateway3.8.0 /S/Q 2> nul
rmdir %to_folder%\baseline\redist\TranGateway3.8.1 /S/Q 2> nul
rmdir %to_folder%\pos\static /S/Q 2> nul
rmdir %to_folder%\baseline\IPCharge\test /S/Q 2> nul
rmdir %to_folder%\redist\yuicompressor.v2.3.0.0.net /S/Q 2> nul

del %to_folder%\baseline\peripherals_external\Ingenico%20Device%20API\HTML%20Test%20Page\WebPageTest.htm /Q 2> nul
del %to_folder%\baseline\Bank_of_america\test\UAT2XMLWebserviceTest.html /Q 2> nul
del %to_folder%\baseline\business\doc\OneStep_API_structure.htm /Q 2> nul
del %to_folder%\baseline\CASL_engine\test\test_results.htm /Q 2> nul
del %to_folder%\baseline\config\doc\Nice_output_of_configurable_classes_and_example_of_instance.htm /Q 2> nul
del %to_folder%\baseline\peripherals_external\Installer\Peripherals_Setup.msi /Q 2> nul
:: del %to_folder%\frozen\TranSuite_DB.Config.db_connection_info.casle 2> nul
del %to_folder%\developer_framer.htm /Q 2> nul
del %to_folder%\logo.htm /Q 2> nul
del %to_folder%\pos_station.htm /Q 2> nul
del %to_folder%\splash_screen.htm /Q 2> nul
del %to_folder%\core_entries.htm /Q 2> nul
del %to_folder%\config.casle /Q 2> nul
del %to_folder%\IPaymentSupportFunctions.aspx /Q 2> nul
del %to_folder%\baseline\CASL_engine\*.dll /Q 2> nul

echo Renaming Web.config, config.casl, config_aux.casl and loading.vbs as TEMPLATE files in destination folder...

ren %to_folder%\Web.config Web.config.TEMPLATE 2> nul
ren %to_folder%\config_aux.casl config_aux.casl.TEMPLATE 2> nul
ren %to_folder%\redist\loading.vbs loading.vbs.TEMPLATE 2> nul
ren %to_folder%\config.casl config.casl.TEMPLATE 2> nul

echo Making upload_files, upgrade and log directories in destination folder...

mkdir %to_folder%\upload_files 2> nul
mkdir %to_folder%\baseline\FileImport\log 2> nul
mkdir %to_folder%\log 2> nul
mkdir %to_folder%\upgrade 2> nul

echo Press the space bar to delete casle files from the original folder...
pause
::Delete Encrypted Files from Original Site
del %from_folder%\*.casle /S/Q  2> nul

echo Press the space bar to close this window.
pause
:end
