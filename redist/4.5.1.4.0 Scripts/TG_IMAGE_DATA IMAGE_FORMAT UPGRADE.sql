/*BUG 16884 - Modifying TG_IMAGE_DATA table for upload non image file to transaction or tender. 
*/
ALTER TABLE TG_IMAGE_DATA ALTER COLUMN IMAGE_FORMAT varchar(100)