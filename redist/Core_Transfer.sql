/* ---------------------------------------------------------------------------
Copyright 2017 Core Business Technologies

Creates iPayment transfer table and indexes in an empty SQL Server DB

TranSuite DB Version 4.5.1.0.0

This table exists solely for the purpose of notifying an interested party whether a transferred transaction has
completed or failed.

Changes:
  WHO     WHEN        WHAT
  UMN     01/25/18    Added RESPONSE to hold response sent to Epic.
 
*/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE CORE_TRANSFER ( 
  REFERENCE_ID    INT IDENTITY(1,1) NOT NULL, -- auto-generated ref number. Used by CoreDirect, NotifySvc and iPayment
  REQ_TYPE        varchar(20)       NOT NULL, -- CLOSESESSION, POPUP, PAYMENTREQUEST, or REFUNDREQUEST
  STATUS          varchar(20)       NULL,     -- STARTED, COMPLETED OR CANCELED
  FOREIGN_USERID  varchar(20)       NULL,     -- user ID in foreign system; passed on transfer. Used to lookup iPayment UserID
  FOREIGN_LOCID   varchar(40)       NULL,     -- location ID in foreign system; passed on transfer. Used to lookup iPayment DeptID
  WORKSTATION_ID  varchar(20)       NULL,     -- foreign workstation ID
  USERID          varchar(20)       NULL,     -- iPayment UserID
  DEPTID          varchar(40)       NULL,     -- iPayment DeptID
  FILENBR         varchar(20)       NULL,     -- FILENBR used in transfer
  XFER_DT         datetime          NULL,     -- date and time of transfer (xfer)
  TRAN_DT         datetime          NULL,     -- date and time of transaction post, cancel or void
  SESSION_ID      varchar(25)       NULL,     -- iPayment masked session id
  RECEIPT_REF     varchar(20)       NULL,     -- iPayment receipt ref# if available
  TENDER_TYPE     varchar(40)       NULL,     -- foreign system tender type
  TENDER_INFO     varchar(50)       NULL,     -- additional tender type info like a check number
  ERR_MSG         varchar(100)      NULL,     -- optional error message
  REQUEST         XML               NULL,     -- contains request from foreign system (eg. Epic)
  RESPONSE        XML               NULL      -- contains response to foreign system (eg. Epic)
  PRIMARY KEY(REFERENCE_ID))
GO
  
