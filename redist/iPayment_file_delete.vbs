  On Error Resume Next

  Set oFileSys = WScript.CreateObject("Scripting.FileSystemObject")
  
  'Path root to look for files
  sRoot = "D:\T4\SVN_T4_Apps\San_Mateo\sanmateo\log"  
  'Files older than this (in days) will be deleted
  nMaxFileAge = 3                 
  
  today = Date
  DeleteFiles(sRoot)

  Function DeleteFiles(ByVal sFolder)

    Set oFolder = oFileSys.GetFolder(sFolder)
    Set aFiles = oFolder.Files
    Set aSubFolders = oFolder.SubFolders

    For Each file in aFiles
      dFileCreated = FormatDateTime(file.DateCreated, "2")
      If DateDiff("d", dFileCreated, today) > nMaxFileAge Then
        file.Delete(True)
      End If
    Next

    For Each folder in aSubFolders
      DeleteFiles(folder.Path)
    Next

  End Function