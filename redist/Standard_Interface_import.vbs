DisplayIEwindow
CloseIEwindow


Dim objExplorer


' ~$~----------------------------------------~$~
Sub DisplayIEwindow
'Launch IE browser window and execute Request

Dim strRequest
strRequest = "http://localhost/ipayment_trunk/Business/Std_interface_import/file_import_process.htm?&"

On Error Resume Next

Set ObjExplorer = CreateObject ("InternetExplorer.Application")
objExplorer.Navigate  strRequest
objExplorer.ToolBar = 0
objExplorer.StatusBar = 1
objExplorer.Left = 200
objExplorer.Top = 100
objExplorer.Width = 450
objExplorer.Height = 220
objExplorer.Visible = 1
objExplorer.Document.Title = "Importing Inquiry Files" & String(50,".")
objExplorer.Document.Body.Scroll = "no"
objExplorer.Document.Body.Style.Cursor = "wait"
objExplorer.Document.Body.InnerHTML = "Executing. Please Wait..."

' Attempt to make the IE browser window active
WScript.Sleep 2000
WScript.Sleep 2001
End Sub


' ~$~----------------------------------------~$~
Sub CloseIEwindow
'Attemps to close the IE window if it was opened by this script

On Error Resume Next

objExplorer.Quit
End Sub