/* ---------------------------------------------------------------------------
--    	Copyright 2021 Core Business Technologies
--
-- Alter the UIX_TG_TRAN_DATA_DEPFILENBR_DEPFILESEQ_EVENTNBR_TRANNBR index
-- to remove checksum.
-- IPAY-229: FT: 1/5/2021
--
*/ ---------------------------------------------------------------------------
CREATE UNIQUE NONCLUSTERED INDEX [UIX_TG_TRAN_DATA_DEPFILENBR_DEPFILESEQ_EVENTNBR_TRANNBR] ON [dbo].[TG_TRAN_DATA] 
(
	[DEPFILENBR] ASC, -- UMN updated order according to UPHS Bug 14060
	[DEPFILESEQ] ASC,
	[EVENTNBR] ASC,
	[TRANNBR] ASC
)
INCLUDE ( [CONTENTTYPE],
[TTID],
[TTDESC],
[TAXEXIND],
[DISTAMT],
[TRANAMT],
[ITEMIND],
[COMMENTS],
[SYSTEXT],
[POSTDT],
[VOIDDT],
[VOIDUSERID],
[SOURCE_TYPE],
[SOURCE_GROUP],
[SOURCE_REFID],
[SOURCE_DATE])
WITH (PAD_INDEX = OFF, 
STATISTICS_NORECOMPUTE = OFF, 
SORT_IN_TEMPDB = OFF, 
DROP_EXISTING = ON,			-- IPAY-229: FT: Force recreate index with no checksum
ONLINE = OFF,
ALLOW_ROW_LOCKS = ON, 
ALLOW_PAGE_LOCKS = ON, 
FILLFACTOR = 100) 
ON [PRIMARY]
	
GO
