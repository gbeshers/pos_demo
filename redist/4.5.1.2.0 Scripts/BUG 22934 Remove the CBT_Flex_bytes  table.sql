--MPT: Remove CBT_Flex_byte for unused reason bug 22934
IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME ='CBT_Flex_bytes'
AND TABLE_SCHEMA = 'dbo' AND TABLE_TYPE = 'BASE TABLE'))
BEGIN
	DROP TABLE CBT_Flex_bytes
	PRINT 'Remove CBT_Flex_bytes table'
END