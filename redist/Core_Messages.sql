/* ---------------------------------------------------------------------------
Copyright 2017 Core Business Technologies

Creates iPayment message table and indexes in an empty SQL Server DB

TranSuite DB Version 4.5.1.0.0

This table exists solely for the purpose of notifying users of any messages they may have received.
 
*/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE CORE_MESSAGES ( 
  MESSAGE_ID      INT IDENTITY(1,1) NOT NULL, -- auto-generated msg number.
  MESSAGE         varchar(1000)     NOT NULL, -- the message
  MESSAGE_TYPE    varchar(40)       NOT NULL, -- the message type
  SENDER_ID       varchar(40)       NOT NULL, -- iPayment user id that sent the message
  USERS           XML               NOT NULL, -- <v> of iPayment User ID
  WORKGROUPS      XML               NOT NULL, -- <v> of iPayment Dept ID
  PROFILES        XML               NOT NULL, -- <v> of iPayment Security_profile ID
  CLEARED         XML               NOT NULL, -- <v> of iPayment User ID for user cleared
  START_DT        datetime          NOT NULL, -- date and time of message start
  END_DT          datetime          NOT NULL, -- date and time of message expiration
  PRIMARY KEY(MESSAGE_ID))
GO
  
/* This index doesn't seem to be used when querying. May need help to include the columns in it
CREATE NONCLUSTERED INDEX [IX_Core_Messages_Start_DT_End_DT] ON [dbo].[CORE_MESSAGES]
(
	[START_DT] ASC,
	[END_DT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
*/
