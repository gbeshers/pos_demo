/* ---------------------------------------------------------------------------
--    	Copyright 2006-2020 Core Business Technologies
--
-- Creates iPayment configuration tables and indexes in an empty SQL Server DB
--
-- TranSuite DB Version 4.0.9.0
--
-- change based on sql script 4.0.9.0
-- Last Updated: 
--    1)  12/06/07 v4.0.8.0 by SHUANG XIAO 
--        Notes: Add NEW COLUMN: TG_DEPFILE_DATA.CREATOR_ID BUG#4602/PRF
--    2)  02/21/08 v4.0.10.0 by CODY LEE
--        Notes: Add NEW TABLE: TG_TTA_MAP_DATA
--    3)  04/23/08 v4.1.0.0 by SHUANG XIAO 
--        Add NEW COLUMN: TG_DEPFILE_DATA.CLOSE_USERID, CLOSEDT BUG#5396
--    4)  05/20/08 v4.1.0.0  by SHUANG XIAO 
--        Add NEW COLUMN:  TG_DEPOSIT_DATA.ACC_USERID, ACCDT BUG#5530
--    5)  05/23/08 v4.1.1 by Mike Plusch
--        No schema changes, just reorganized and split into separate data and config files
--    6)  08/03/2009 Converted 4k fields to VARCHAR(MAX). BUG# 7352 This will generate an error on <2005. MAX started with 2005.
--	  7)  08/16/2010 Updated size to 20 for column [USER] in CBT_Revision Bug#8665 NJ
--    8)  09/03/2014 UMN Added if exists checking, and index creation, so can rerun script just to create the indexes.
--    9)  12/10/2019 UMN Removed unused CONNECT_TEST DB
-- --------------------------------------------------------------------------
*/
SET NOCOUNT ON;

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'CBT_FILEIMPORT_CONFIG' 
  AND TABLE_SCHEMA = 'dbo' AND TABLE_TYPE = 'BASE TABLE'))
BEGIN
	CREATE TABLE [dbo].[CBT_FILEIMPORT_CONFIG](
		[FILETYPE] [varchar](40) NOT NULL,
		[FILEDEFINITION] [varchar](8000) NOT NULL,
	 CONSTRAINT [PK__CBT_FILE__CC4E5AC35CA1C101] PRIMARY KEY CLUSTERED 
	(
		[FILETYPE] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
	PRINT 'Created CBT_FILEIMPORT_CONFIG table'
END

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'CBT_Flex' 
  AND TABLE_SCHEMA = 'dbo' AND TABLE_TYPE = 'BASE TABLE'))
BEGIN
	CREATE TABLE [dbo].[CBT_Flex](
		[container] [varchar](255) NOT NULL,
		[field_key] [varchar](255) NOT NULL,
		[field_value] [varchar](8000) NOT NULL,
		[revision] [int] NOT NULL,
		[is_root] [int] NULL
	) ON [PRIMARY]
	PRINT 'Created CBT_Flex table'
END

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME ='CBT_Flex_bytes'
AND TABLE_SCHEMA = 'dbo' AND TABLE_TYPE = 'BASE TABLE'))
BEGIN
	CREATE TABLE [dbo].[CBT_Flex_bytes](
		[container] [varchar](255) NULL,
		[field_key] [varchar](255) NULL,
		[field_value] [image] NULL,
		[revision] [int] NULL
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
	PRINT 'Created CBT_Flex_bytes table'
END

IF(NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME ='CBT_Revision'
AND TABLE_SCHEMA = 'dbo' AND TABLE_TYPE = 'BASE TABLE'))
BEGIN
	CREATE TABLE [dbo].[CBT_Revision](
		[id] [int] NOT NULL,
		[timestamp] [varchar](255) NULL,
		[user] [varchar](20) NULL,
		[description] [varchar](255) NULL
	) ON [PRIMARY]
	PRINT 'Created CBT_Revision table'
END

IF(NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME ='TG_DEVICE_ID'
AND TABLE_SCHEMA = 'dbo' AND TABLE_TYPE = 'BASE TABLE'))
BEGIN
	CREATE TABLE [dbo].[TG_DEVICE_ID](
		[DEVICE_ID] [int] IDENTITY(1,1) NOT NULL,
		[DEVICE_TYPE] [varchar](100) NOT NULL,
		[SERIAL_NBR] [varchar](100) NOT NULL,
		[EXTERNAL_ID] [varchar](100) NULL,
	 CONSTRAINT [PK_TG_DEVICE_ID] PRIMARY KEY CLUSTERED 
	(
		[DEVICE_TYPE] ASC,
		[SERIAL_NBR] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
	PRINT 'Created TG_DEVICE_ID table'
END

IF(NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME ='TG_DYNAMIC_CONFIG'
AND TABLE_SCHEMA = 'dbo' AND TABLE_TYPE = 'BASE TABLE'))
BEGIN
	CREATE TABLE [dbo].[TG_DYNAMIC_CONFIG](
		[PATH] [varchar](300) NOT NULL,
		[VAL] [varchar](max) NULL,
		[VAL_KIND] [varchar](100) NULL,
	 CONSTRAINT [PK__TG_DYNAMIC_CONFI__4316F928] PRIMARY KEY CLUSTERED 
	(
		[PATH] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
	PRINT 'Created TG_DYNAMIC_CONFIG table'
END
-- UMN added this FROM Fred's SF script. Remove the indexes =====================================================
IF EXISTS (SELECT name FROM sysindexes WHERE name = 'PK_CBT_Flex')
  drop index [PK_CBT_Flex] on CBT_Flex;
IF EXISTS (SELECT name FROM sysindexes WHERE name = 'PK_CBT_Revision')
  drop index [PK_CBT_Revision] on CBT_Revision;
IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_CBT_Flex_container_field_key_field_value')
  drop index [IX_CBT_Flex_container_field_key_field_value] on CBT_Flex;
GO

--MTP Added to check the edition of SQL whether to create index offline or online Bug 20519
DECLARE @sqlVers VARCHAR(128)
SELECT @sqlVers = left(CAST(SERVERPROPERTY('Edition') as VARCHAR), 128)
IF(@sqlVers LIKE '%Developer%' or @sqlVers LIKE '%Enterprise%'  )
BEGIN
--CREATED INDEX FROM NTirety RECOMMEND
CREATE INDEX [IDX_NTI_CBT_Flex_container_field_key_revision_Enterprise] ON [dbo].[CBT_Flex]
(
  [container] ASC,
  [field_key] ASC,
  [revision] ASC
)WITH (ONLINE = ON);
END
ELSE
BEGIN
CREATE INDEX [IDX_NTI_CBT_Flex_container_field_key_revision_Standard] ON [dbo].[CBT_Flex]
(
  [container] ASC,
  [field_key] ASC,
  [revision] ASC
)WITH (ONLINE = OFF);
END
--- Drop old indexes

IF EXISTS (SELECT name FROM sysindexes WHERE name = 'CBT_Flex_pid')
  drop index [CBT_Flex_pid] on [dbo].[CBT_Flex];
IF EXISTS (SELECT name FROM sysindexes WHERE name = 'CBT_Revision_pid')
  drop index [CBT_Revision_pid] on [dbo].[CBT_Revision];

--MTP Removed for duplicated indexes Bug 20519
--- Add standard indexes
--CREATE UNIQUE INDEX PK_CBT_Flex ON [dbo].[CBT_Flex]
--(
--  [container], 
--  [field_key], 
--  [revision]
--) ON [PRIMARY];

CREATE UNIQUE INDEX PK_CBT_Revision ON CBT_Revision
(
  [id]
) ON [PRIMARY];

--MTP Removed for duplicated indexes Bug 20519
----- Add new clustered index
--CREATE CLUSTERED INDEX [IX_CBT_Flex_container_field_key_field_value] ON [dbo].[CBT_Flex] 
--(
--  [container] ASC,
--  [field_key] ASC,
--  [revision] ASC
--) ON [PRIMARY]
--GO

-- UMN FROM Performance Script
-- UMN Apply Table Compression only if SQL Server version supports it
DECLARE @compression VARCHAR(128)
set @compression = 'NONE'
IF (EXISTS (SELECT * FROM sys.dm_db_persisted_sku_features where feature_name = 'Compression'))
  set @compression = 'PAGE'
Exec ('ALTER INDEX ALL ON CBT_Flex REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec('ALTER INDEX ALL ON CBT_Flex_bytes REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON CBT_Revision REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
Exec ('ALTER INDEX ALL ON TG_DYNAMIC_CONFIG REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')') 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TG_USERSESSION_DATA]') AND type in (N'U'))
	Exec ('ALTER INDEX ALL ON TG_USERSESSION_DATA REBUILD WITH (FILLFACTOR = 100, Data_Compression = ' + @compression + ')')

Exec ('ALTER TABLE [dbo].[CBT_Flex] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')')
Exec ('ALTER TABLE [dbo].[CBT_Flex_bytes] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')')
Exec ('ALTER TABLE [dbo].[CBT_Revision] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')')
Exec ('ALTER TABLE [dbo].[TG_DYNAMIC_CONFIG] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')')
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TG_USERSESSION_DATA]') AND type in (N'U'))
	Exec ('ALTER TABLE [dbo].[TG_USERSESSION_DATA] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = ' + @compression + ')')
GO
UPDATE STATISTICS CBT_Flex WITH FULLSCAN
GO
UPDATE STATISTICS CBT_Flex_bytes WITH FULLSCAN
GO
UPDATE STATISTICS CBT_Revision WITH FULLSCAN
GO
UPDATE STATISTICS TG_DYNAMIC_CONFIG WITH FULLSCAN
GO
PRINT 'Created Config DB indexes.'

-- Print out a list of the new clustered and non-clustered indexes, and stats
SELECT OBJECT_NAME(i.id) AS 'TABLE_NAME', i.name AS 'CLUSTERED_INDEX'
FROM sysindexes i
WHERE i.indid between 1 AND 249
AND INDEXPROPERTY(i.id, i.name, 'IsClustered') = 1
AND INDEXPROPERTY(i.id, i.name, 'IsStatistics') = 0
AND INDEXPROPERTY(i.id, i.name, 'IsAutoStatistics') = 0
AND INDEXPROPERTY(i.id, i.name, 'IsHypothetical') = 0
AND OBJECTPROPERTY(i.id,'IsMSShipped') = 0
AND not exists (SELECT *
FROM sysobjects o
WHERE xtype = 'UQ'
AND o.name = i.name ) 

SELECT OBJECT_NAME(i.id) as 'TABLE_NAME', i.name as 'NON_CLUSTERED_INDEX'
FROM sysindexes i
WHERE i.indid between 1 AND 249
AND INDEXPROPERTY(i.id, i.name, 'IsClustered') = 0
AND INDEXPROPERTY(i.id, i.name, 'IsStatistics') = 0
AND INDEXPROPERTY(i.id, i.name, 'IsAutoStatistics') = 0
AND INDEXPROPERTY(i.id, i.name, 'IsHypothetical') = 0
AND OBJECTPROPERTY(i.id,'IsMSShipped') = 0
AND not exists (SELECT *
FROM sysobjects o
WHERE xtype = 'UQ'
AND o.name = i.name ) 

SELECT OBJECT_NAME(i.id) as 'TABLE_NAME', i.name as 'STATISTICS'
FROM sysindexes i
WHERE i.indid between 1 AND 249
AND INDEXPROPERTY(i.id, i.name, 'IsStatistics') = 1
AND INDEXPROPERTY(i.id, i.name, 'IsAutoStatistics') = 0
AND INDEXPROPERTY(i.id, i.name, 'IsHypothetical') = 0
AND OBJECTPROPERTY(i.id,'IsMSShipped') = 0
AND not exists (SELECT *
FROM sysobjects o
WHERE xtype = 'UQ'
AND o.name = i.name )

GO
/* Non CBT_Flex data */
--need
INSERT [dbo].[CBT_FILEIMPORT_CONFIG] ([FILETYPE], [FILEDEFINITION]) VALUES (N'SAMPLE2-VARIABLELENGTH', N'<IMPORTFILETYPE><FILEID>SAMPLE1-FIXEDLENGTH</FILEID><FILEFORMATTYPE>VARIABLELENGTH</FILEFORMATTYPE><IPAYMENTTRANTYPEID>MISC011</IPAYMENTTRANTYPEID><IPAYMENTSOURCEID>IMPORT</IPAYMENTSOURCEID><FILEFORMATDELIMITED><DELIMITER>,</DELIMITER><ENDOFLINECHAR>LF</ENDOFLINECHAR><HEADERRECORDFORMATDELIMITED><FIELDCOUNT>3</FIELDCOUNT><IDENTIFIERFIELDNBR>1</IDENTIFIERFIELDNBR><IDENTIFIER>H</IDENTIFIER><RECORDCOUNTFIELDNBR>2</RECORDCOUNTFIELDNBR><RECORDTOTALFIELDNBR>3</RECORDTOTALFIELDNBR><SOURCEGROUPFIELDNBR>0</SOURCEGROUPFIELDNBR><SOURCEDATEFIELDNBR>0</SOURCEDATEFIELDNBR></HEADERRECORDFORMATDELIMITED><DETAILEDRECORDFORMATDELIMITED><FIELDCOUNT>6</FIELDCOUNT><IDENTIFIERFIELDNBR>1</IDENTIFIERFIELDNBR><IDENTIFIER>D</IDENTIFIER><IMPORTFIELD><FIELDNBR>2</FIELDNBR><DATATYPE>TEXT</DATATYPE><TRANAMOUNTIND>false</TRANAMOUNTIND><IPAYMENTFIELD>ACCOUNT_NBR</IPAYMENTFIELD></IMPORTFIELD><IMPORTFIELD><FIELDNBR>5</FIELDNBR><DATATYPE>CURRENCY</DATATYPE><TRANAMOUNTIND>true</TRANAMOUNTIND><IPAYMENTFIELD /></IMPORTFIELD></DETAILEDRECORDFORMATDELIMITED></FILEFORMATDELIMITED></IMPORTFILETYPE>')
--need - maybe not
INSERT [dbo].[CBT_Revision] ([id], [timestamp], [user], [description]) VALUES (1, N'24-Oct-2016', N'tadminis', N'')
SET IDENTITY_INSERT [dbo].[TG_DEVICE_ID] ON 
/* CBT_Flex data */
/* Container='Business.Allocation_group' */
/* Container='Business.Bank_account' */
/* Container='Business.Bank_rule' */
/* Container='Business.bank_deposit_recon' */
/* Container='Business.casl_client' */
/* Container='Business.Custom_field' */
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.AmountPaid', N'"_parent"', N'Business.Custom_field', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.AmountPaid', N'"default"', N'10.00', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.AmountPaid', N'"id"', N'"AmountPaid"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.AmountPaid', N'"label"', N'"Amount Paid"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.AmountPaid', N'"label_htmldisplay"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.AmountPaid', N'"line_nbr"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.AmountPaid', N'"max_length"', N'25', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.AmountPaid', N'"min_length"', N'0', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.AmountPaid', N'"prompt"', N'"RESERVED_AMOUNT_PAID"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.AmountPaid', N'"prompt_htmldisplay"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.AmountPaid', N'"read_only"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.AmountPaid', N'"req"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.AmountPaid', N'"store_in_DB"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.AmountPaid', N'"tagname"', N'"AmountPaid"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.AmountPaid', N'"type"', N'Type.currency', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.AmountPaid', N'"value_list"', N'opt', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.AmountPaid', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.CASH_FLOAT_DIST', N'"_parent"', N'Business.Custom_field', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.CASH_FLOAT_DIST', N'"default"', N'"Yes"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.CASH_FLOAT_DIST', N'"id"', N'"CASH_FLOAT_DIST"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.CASH_FLOAT_DIST', N'"label"', N'"CASH_FLOAT_DIST"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.CASH_FLOAT_DIST', N'"line_nbr"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.CASH_FLOAT_DIST', N'"max_length"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.CASH_FLOAT_DIST', N'"prompt"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.CASH_FLOAT_DIST', N'"read_only"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.CASH_FLOAT_DIST', N'"req"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.CASH_FLOAT_DIST', N'"tagname"', N'"CASH_FLOAT_DIST"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.CASH_FLOAT_DIST', N'"type"', N'Type.typical_text', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.CASH_FLOAT_DIST', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.CASH_FLOAT_RETURN', N'"_parent"', N'Business.Custom_field', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.CASH_FLOAT_RETURN', N'"default"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.CASH_FLOAT_RETURN', N'"id"', N'"CASH_FLOAT_RETURN"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.CASH_FLOAT_RETURN', N'"label"', N'"CASH_FLOAT_RETURN"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.CASH_FLOAT_RETURN', N'"line_nbr"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.CASH_FLOAT_RETURN', N'"max_length"', N'25', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.CASH_FLOAT_RETURN', N'"min_length"', N'0', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.CASH_FLOAT_RETURN', N'"prompt"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.CASH_FLOAT_RETURN', N'"prompt_htmldisplay"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.CASH_FLOAT_RETURN', N'"read_only"', N'false', 0, NULL)
GO
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.CASH_FLOAT_RETURN', N'"req"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.CASH_FLOAT_RETURN', N'"store_in_DB"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.CASH_FLOAT_RETURN', N'"tagname"', N'"CASH_FLOAT_RETURN"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.CASH_FLOAT_RETURN', N'"type"', N'Type.typical_text', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.CASH_FLOAT_RETURN', N'"value_list"', N'opt', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.CASH_FLOAT_RETURN', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.exception_throw', N'"_parent"', N'Business.Custom_field', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.exception_throw', N'"default"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.exception_throw', N'"id"', N'"exception_throw"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.exception_throw', N'"label"', N'"exception_throw"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.exception_throw', N'"label_htmldisplay"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.exception_throw', N'"line_nbr"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.exception_throw', N'"mask_value_on_receipts"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.exception_throw', N'"max_length"', N'40', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.exception_throw', N'"min_length"', N'0', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.exception_throw', N'"prompt"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.exception_throw', N'"prompt_htmldisplay"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.exception_throw', N'"read_only"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.exception_throw', N'"req"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.exception_throw', N'"store_in_DB"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.exception_throw', N'"tagname"', N'"exception_throw"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.exception_throw', N'"type"', N'Type.typical_text', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.exception_throw', N'"unmasked_character_count"', N'0', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.exception_throw', N'"value_list"', N'opt', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.exception_throw', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.fee', N'"_parent"', N'Business.Custom_field', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.fee', N'"default"', N'0.25', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.fee', N'"id"', N'"fee"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.fee', N'"label"', N'"fee"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.fee', N'"label_htmldisplay"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.fee', N'"line_nbr"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.fee', N'"max_length"', N'25', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.fee', N'"min_length"', N'0', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.fee', N'"prompt"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.fee', N'"prompt_htmldisplay"', N'"0x3c666f6e7420636f6c6f723d27677265656e272073697a653d322e353e0d0a24302e32352f6d696c650d0a3c2f666f6e743e"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.fee', N'"read_only"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.fee', N'"req"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.fee', N'"store_in_DB"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.fee', N'"tagname"', N'"fee"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.fee', N'"type"', N'Type.currency', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.fee', N'"value_list"', N'opt', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.fee', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.EMAIL_RECEIPT', N'"_parent"', N'Business.Custom_field', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.EMAIL_RECEIPT', N'"default"', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.EMAIL_RECEIPT', N'"id"', N'"EMAIL_RECEIPT"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.EMAIL_RECEIPT', N'"label"', N'"EMail"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.EMAIL_RECEIPT', N'"label_htmldisplay"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.EMAIL_RECEIPT', N'"line_nbr"', N'"8"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.EMAIL_RECEIPT', N'"mask_value_on_receipts"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.EMAIL_RECEIPT', N'"max_length"', N'65', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.EMAIL_RECEIPT', N'"min_length"', N'0', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.EMAIL_RECEIPT', N'"prompt"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.EMAIL_RECEIPT', N'"prompt_htmldisplay"', N'"0x3c68746d6c3e0d0a2020202020202020202020203c686561643e0d0a2020202020202020202020202020202020202020202020203c7469746c653e3c2f7469746c653e0d0a2020202020202020202020203c2f686561643e0d0a2020202020202020202020203c626f64793e0d0a2020202020202020202020202020202020202020202020203c703e0d0a2020202020202020202020202020202020202020202020202020202020202020202020203c7370616e207374796c653d22666f6e742d73697a653a20313870783b223e3c7370616e207374796c653d22636f6c6f723a2072676228382c20382c20313338293b223e3c7374726f6e673e456e74657220656d61696c206164647265737320746f2072656365697665207061796d656e7420636f6e6669726d6174696f6e2e3c2f7374726f6e673e3c2f7370616e3e3c2f7370616e3e3c2f703e0d0a2020202020202020202020203c2f626f64793e0d0a3c2f68746d6c3e0d0a"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.EMAIL_RECEIPT', N'"read_only"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.EMAIL_RECEIPT', N'"req"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.EMAIL_RECEIPT', N'"store_in_DB"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.EMAIL_RECEIPT', N'"tagname"', N'"EMAIL_RECEIPT"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.EMAIL_RECEIPT', N'"type"', N'Type.email', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.EMAIL_RECEIPT', N'"unmasked_character_count"', N'0', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.EMAIL_RECEIPT', N'"value_list"', N'opt', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.EMAIL_RECEIPT', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.total_bar', N'"_parent"', N'Business.Custom_field', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.total_bar', N'"default"', N'"GRAND_BALANCE,GRAND_CHECKALL,GRAND_AMOUNT_PAID"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.total_bar', N'"hide_if"', N'"Do not hide"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.total_bar', N'"id"', N'"total_bar"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.total_bar', N'"label"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.total_bar', N'"label_htmldisplay"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.total_bar', N'"line_nbr"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.total_bar', N'"mask_value_on_receipts"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.total_bar', N'"max_length"', N'25', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.total_bar', N'"min_length"', N'0', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.total_bar', N'"prompt"', N'"RESERVED_TOTAL_BAR"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.total_bar', N'"prompt_htmldisplay"', N'""', 0, NULL)
GO
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.total_bar', N'"read_only"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.total_bar', N'"req"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.total_bar', N'"store_in_DB"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.total_bar', N'"tagname"', N'"total_bar"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.total_bar', N'"type"', N'Type.typical_text', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.total_bar', N'"unmasked_character_count"', N'0', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.total_bar', N'"value_list"', N'opt', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.total_bar', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.payment_selector', N'"_parent"', N'Business.Custom_field', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.payment_selector', N'"default"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.payment_selector', N'"hide_if"', N'"Do not hide"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.payment_selector', N'"id"', N'"payment_selector"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.payment_selector', N'"label"', N'"Selected"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.payment_selector', N'"label_htmldisplay"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.payment_selector', N'"line_nbr"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.payment_selector', N'"mask_value_on_receipts"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.payment_selector', N'"max_length"', N'25', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.payment_selector', N'"min_length"', N'0', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.payment_selector', N'"prompt"', N'"RESERVED_SELECTED"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.payment_selector', N'"prompt_htmldisplay"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.payment_selector', N'"read_only"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.payment_selector', N'"req"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.payment_selector', N'"store_in_DB"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.payment_selector', N'"tagname"', N'"payment_selector"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.payment_selector', N'"type"', N'Type.boolean', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.payment_selector', N'"unmasked_character_count"', N'0', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.payment_selector', N'"value_list"', N'opt', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Custom_field.of.payment_selector', N'null', N'null', 0, NULL)
/* Container='Business.Field_value_list' */
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state', N'"_parent"', N'Business.Field_value_list', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state', N'"id"', N'"state"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state', N'"name"', N'"States"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state', N'"ui"', N'hypertext.SELECT.popup', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state', N'"values"', N'<v>Business.Field_value_list.of.state.values.0 Business.Field_value_list.of.state.values.1 Business.Field_value_list.of.state.values.2 Business.Field_value_list.of.state.values.3 Business.Field_value_list.of.state.values.4 Business.Field_value_list.of.state.values.5 Business.Field_value_list.of.state.values.6 Business.Field_value_list.of.state.values.7 Business.Field_value_list.of.state.values.8 Business.Field_value_list.of.state.values.9 Business.Field_value_list.of.state.values.10 Business.Field_value_list.of.state.values.11 Business.Field_value_list.of.state.values.12 Business.Field_value_list.of.state.values.13 Business.Field_value_list.of.state.values.14 Business.Field_value_list.of.state.values.15 Business.Field_value_list.of.state.values.16 Business.Field_value_list.of.state.values.17 Business.Field_value_list.of.state.values.18 Business.Field_value_list.of.state.values.19 Business.Field_value_list.of.state.values.20 Business.Field_value_list.of.state.values.21 Business.Field_value_list.of.state.values.22 Business.Field_value_list.of.state.values.23 Business.Field_value_list.of.state.values.24 Business.Field_value_list.of.state.values.25 Business.Field_value_list.of.state.values.26 Business.Field_value_list.of.state.values.27 Business.Field_value_list.of.state.values.28 Business.Field_value_list.of.state.values.29 Business.Field_value_list.of.state.values.30 Business.Field_value_list.of.state.values.31 Business.Field_value_list.of.state.values.32 Business.Field_value_list.of.state.values.33 Business.Field_value_list.of.state.values.34 Business.Field_value_list.of.state.values.35 Business.Field_value_list.of.state.values.36 Business.Field_value_list.of.state.values.37 Business.Field_value_list.of.state.values.38 Business.Field_value_list.of.state.values.39 Business.Field_value_list.of.state.values.40 Business.Field_value_list.of.state.values.41 Business.Field_value_list.of.state.values.42 Business.Field_value_list.of.state.values.43 Business.Field_value_list.of.state.values.44 Business.Field_value_list.of.state.values.45 Business.Field_value_list.of.state.values.46 Business.Field_value_list.of.state.values.47 Business.Field_value_list.of.state.values.48 Business.Field_value_list.of.state.values.49</v>', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.0', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.0', N'"description"', N'"Alabama"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.0', N'"value"', N'"AL"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.0', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.1', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.1', N'"description"', N'"Alaska"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.1', N'"value"', N'"AK"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.1', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.2', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.2', N'"description"', N'"Arizona"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.2', N'"value"', N'"AZ"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.2', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.3', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.3', N'"description"', N'"Arkansas"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.3', N'"value"', N'"AR"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.4', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.4', N'"description"', N'"California"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.4', N'"value"', N'"CA"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.4', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.3', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.5', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.5', N'"description"', N'"Colorado"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.5', N'"value"', N'"CO"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.5', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.6', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.6', N'"description"', N'"Connecticut"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.6', N'"value"', N'"CT"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.6', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.7', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.7', N'"description"', N'"Delaware"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.7', N'"value"', N'"DE"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.7', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.8', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.8', N'"description"', N'"Florida"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.8', N'"value"', N'"FL"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.8', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.9', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.9', N'"description"', N'"Georgia"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.9', N'"value"', N'"GA"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.9', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.10', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.10', N'"description"', N'"Hawaii"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.10', N'"value"', N'"HI "', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.10', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.11', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.11', N'"description"', N'"Idaho"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.11', N'"value"', N'"ID"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.11', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.12', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.12', N'"description"', N'"Illinois"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.12', N'"value"', N'"IL"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.12', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.13', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.13', N'"description"', N'"Indiana"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.13', N'"value"', N'"IN"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.13', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.14', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.14', N'"description"', N'"Iowa"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.14', N'"value"', N'"IA"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.14', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.15', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.15', N'"description"', N'"Kansas"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.15', N'"value"', N'"KS"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.15', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.16', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.16', N'"description"', N'"Kentucky"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.16', N'"value"', N'"KY"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.16', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.17', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.17', N'"description"', N'"Louisiana"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.17', N'"value"', N'"LA"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.17', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.18', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.18', N'"description"', N'"Maine"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.18', N'"value"', N'"ME"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.18', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.19', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.19', N'"description"', N'"Maryland"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.19', N'"value"', N'"MD"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.19', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.20', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.20', N'"description"', N'"Massachusetts"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.20', N'"value"', N'"MA"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.20', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.21', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.21', N'"description"', N'"Michigan"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.21', N'"value"', N'"MI"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.21', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.22', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.22', N'"description"', N'"Minnesota"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.22', N'"value"', N'"MN"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.22', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.23', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.23', N'"description"', N'"Missouri"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.23', N'"value"', N'"MO"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.23', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.24', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.24', N'"description"', N'"Montana"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.24', N'"value"', N'"MT"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.24', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.25', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.25', N'"description"', N'"Nebraska"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.25', N'"value"', N'"NE"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.25', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.26', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.26', N'"description"', N'"Nevada"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.26', N'"value"', N'"NV"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.26', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.27', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.27', N'"description"', N'"New Hampshire"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.27', N'"value"', N'"NH"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.27', N'null', N'null', 0, NULL)
GO
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.28', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.28', N'"description"', N'"New Jersey"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.28', N'"value"', N'"NJ"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.28', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.29', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.29', N'"description"', N'"New Mexico"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.29', N'"value"', N'"NM"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.29', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.30', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.30', N'"description"', N'"New York"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.30', N'"value"', N'"NY"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.30', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.31', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.31', N'"description"', N'"North Carolina"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.31', N'"value"', N'"NC"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.31', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.32', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.32', N'"description"', N'"North Dakota"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.32', N'"value"', N'"ND"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.32', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.33', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.33', N'"description"', N'"Ohio"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.33', N'"value"', N'"OH"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.33', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.34', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.34', N'"description"', N'"Oklahoma"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.34', N'"value"', N'"OK"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.34', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.35', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.35', N'"description"', N'"Oregon"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.35', N'"value"', N'"OR"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.35', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.36', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.36', N'"description"', N'"Pennsylvania"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.36', N'"value"', N'"PA"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.36', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.37', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.37', N'"description"', N'"Rhode Island"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.37', N'"value"', N'"RI"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.37', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.38', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.38', N'"description"', N'"South Carolina"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.38', N'"value"', N'"SC"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.38', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.39', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.39', N'"description"', N'"South Dakota"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.39', N'"value"', N'"SD"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.39', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.40', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.40', N'"description"', N'"Tennessee"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.40', N'"value"', N'"TN"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.40', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.41', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.41', N'"description"', N'"Texas"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.41', N'"value"', N'"TX"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.41', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.42', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.42', N'"description"', N'"Utah"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.42', N'"value"', N'"UT"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.42', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.43', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.43', N'"description"', N'"Vermont"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.43', N'"value"', N'"VT"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.43', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.44', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.44', N'"description"', N'"Virginia"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.44', N'"value"', N'"VA"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.44', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.45', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.45', N'"description"', N'"Washington"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.45', N'"value"', N'"WA"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.45', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.46', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.46', N'"description"', N'"Washington, DC"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.46', N'"value"', N'"DC"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.46', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.47', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.47', N'"description"', N'"West Virginia"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.47', N'"value"', N'"WV"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.47', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.48', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.48', N'"description"', N'"Wisconsin"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.48', N'"value"', N'"WI"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.48', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.49', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.49', N'"description"', N'"Wyoming"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.49', N'"value"', N'"WY"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.state.values.49', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences', N'"_parent"', N'Business.Field_value_list', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences', N'"id"', N'"states_and_provences"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences', N'"name"', N'"States and Provences"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences', N'"ui"', N'hypertext.SELECT.popup', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences', N'"values"', N'<v>Business.Field_value_list.of.states_and_provences.values.0 Business.Field_value_list.of.states_and_provences.values.1 Business.Field_value_list.of.states_and_provences.values.2 Business.Field_value_list.of.states_and_provences.values.3 Business.Field_value_list.of.states_and_provences.values.4 Business.Field_value_list.of.states_and_provences.values.5 Business.Field_value_list.of.states_and_provences.values.6 Business.Field_value_list.of.states_and_provences.values.7 Business.Field_value_list.of.states_and_provences.values.8 Business.Field_value_list.of.states_and_provences.values.9 Business.Field_value_list.of.states_and_provences.values.10 Business.Field_value_list.of.states_and_provences.values.11 Business.Field_value_list.of.states_and_provences.values.12 Business.Field_value_list.of.states_and_provences.values.13 Business.Field_value_list.of.states_and_provences.values.14 Business.Field_value_list.of.states_and_provences.values.15 Business.Field_value_list.of.states_and_provences.values.16 Business.Field_value_list.of.states_and_provences.values.17 Business.Field_value_list.of.states_and_provences.values.18 Business.Field_value_list.of.states_and_provences.values.19 Business.Field_value_list.of.states_and_provences.values.20 Business.Field_value_list.of.states_and_provences.values.21 Business.Field_value_list.of.states_and_provences.values.22 Business.Field_value_list.of.states_and_provences.values.23 Business.Field_value_list.of.states_and_provences.values.24 Business.Field_value_list.of.states_and_provences.values.25 Business.Field_value_list.of.states_and_provences.values.26 Business.Field_value_list.of.states_and_provences.values.27 Business.Field_value_list.of.states_and_provences.values.28 Business.Field_value_list.of.states_and_provences.values.29 Business.Field_value_list.of.states_and_provences.values.30 Business.Field_value_list.of.states_and_provences.values.31 Business.Field_value_list.of.states_and_provences.values.32 Business.Field_value_list.of.states_and_provences.values.33 Business.Field_value_list.of.states_and_provences.values.34 Business.Field_value_list.of.states_and_provences.values.35 Business.Field_value_list.of.states_and_provences.values.36 Business.Field_value_list.of.states_and_provences.values.37 Business.Field_value_list.of.states_and_provences.values.38 Business.Field_value_list.of.states_and_provences.values.39 Business.Field_value_list.of.states_and_provences.values.40 Business.Field_value_list.of.states_and_provences.values.41 Business.Field_value_list.of.states_and_provences.values.42 Business.Field_value_list.of.states_and_provences.values.43 Business.Field_value_list.of.states_and_provences.values.44 Business.Field_value_list.of.states_and_provences.values.45 Business.Field_value_list.of.states_and_provences.values.46 Business.Field_value_list.of.states_and_provences.values.47 Business.Field_value_list.of.states_and_provences.values.48 Business.Field_value_list.of.states_and_provences.values.49 Business.Field_value_list.of.states_and_provences.values.50 Business.Field_value_list.of.states_and_provences.values.51 Business.Field_value_list.of.states_and_provences.values.52 Business.Field_value_list.of.states_and_provences.values.53 Business.Field_value_list.of.states_and_provences.values.54 Business.Field_value_list.of.states_and_provences.values.55 Business.Field_value_list.of.states_and_provences.values.56 Business.Field_value_list.of.states_and_provences.values.57 Business.Field_value_list.of.states_and_provences.values.58 Business.Field_value_list.of.states_and_provences.values.59 Business.Field_value_list.of.states_and_provences.values.60 Business.Field_value_list.of.states_and_provences.values.61 Business.Field_value_list.of.states_and_provences.values.62 Business.Field_value_list.of.states_and_provences.values.63</v>', 0, NULL)
GO
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.0', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.0', N'"description"', N'"Alabama"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.0', N'"value"', N'"AL"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.0', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.1', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.1', N'"description"', N'"Alaska"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.1', N'"value"', N'"AK"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.1', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.2', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.2', N'"description"', N'"Arizona"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.2', N'"value"', N'"AZ"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.2', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.3', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.3', N'"description"', N'"Arkansas"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.3', N'"value"', N'"AR"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.3', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.4', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.4', N'"description"', N'"California"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.4', N'"value"', N'"CA"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.4', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.5', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.5', N'"description"', N'"Colorado"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.5', N'"value"', N'"CO"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.5', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.6', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.6', N'"description"', N'"Connecticut"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.6', N'"value"', N'"CT"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.6', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.7', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.7', N'"description"', N'"Delaware"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.7', N'"value"', N'"DE"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.7', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.8', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.8', N'"description"', N'"Florida"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.8', N'"value"', N'"FL"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.8', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.9', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.9', N'"description"', N'"Georgia"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.9', N'"value"', N'"GA"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.9', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.10', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.10', N'"description"', N'"Hawaii"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.10', N'"value"', N'"HI "', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.10', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.11', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.11', N'"description"', N'"Idaho"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.11', N'"value"', N'"ID"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.11', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.12', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.12', N'"description"', N'"Illinois"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.12', N'"value"', N'"IL"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.12', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.13', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.13', N'"description"', N'"Indiana"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.13', N'"value"', N'"IN"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.13', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.14', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.14', N'"description"', N'"Iowa"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.14', N'"value"', N'"IA"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.14', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.15', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.15', N'"description"', N'"Kansas"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.15', N'"value"', N'"KS"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.15', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.16', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.16', N'"description"', N'"Kentucky"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.16', N'"value"', N'"KY"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.16', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.17', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.17', N'"description"', N'"Louisiana"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.17', N'"value"', N'"LA"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.17', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.18', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.18', N'"description"', N'"Maine"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.18', N'"value"', N'"ME"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.18', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.19', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.19', N'"description"', N'"Maryland"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.19', N'"value"', N'"MD"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.19', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.20', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.20', N'"description"', N'"Massachusetts"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.20', N'"value"', N'"MA"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.20', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.21', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.21', N'"description"', N'"Michigan"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.21', N'"value"', N'"MI"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.21', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.22', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.22', N'"description"', N'"Minnesota"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.22', N'"value"', N'"MN"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.22', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.23', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.23', N'"description"', N'"Missouri"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.23', N'"value"', N'"MO"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.23', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.24', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.24', N'"description"', N'"Montana"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.24', N'"value"', N'"MT"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.24', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.25', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.25', N'"description"', N'"Nebraska"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.25', N'"value"', N'"NE"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.25', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.26', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.26', N'"description"', N'"Nevada"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.26', N'"value"', N'"NV"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.26', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.27', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.27', N'"description"', N'"New Hampshire"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.27', N'"value"', N'"NH"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.27', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.28', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.28', N'"description"', N'"New Jersey"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.28', N'"value"', N'"NJ"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.28', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.29', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.29', N'"description"', N'"New Mexico"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.29', N'"value"', N'"NM"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.29', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.30', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.30', N'"description"', N'"New York"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.30', N'"value"', N'"NY"', 0, NULL)
GO
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.30', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.31', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.31', N'"description"', N'"North Carolina"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.31', N'"value"', N'"NC"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.31', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.32', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.32', N'"description"', N'"North Dakota"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.32', N'"value"', N'"ND"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.32', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.33', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.33', N'"description"', N'"Ohio"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.33', N'"value"', N'"OH"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.33', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.34', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.34', N'"description"', N'"Oklahoma"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.34', N'"value"', N'"OK"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.34', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.35', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.35', N'"description"', N'"Oregon"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.35', N'"value"', N'"OR"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.35', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.36', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.36', N'"description"', N'"Pennsylvania"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.36', N'"value"', N'"PA"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.36', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.37', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.37', N'"description"', N'"Rhode Island"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.37', N'"value"', N'"RI"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.37', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.38', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.38', N'"description"', N'"South Carolina"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.38', N'"value"', N'"SC"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.38', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.39', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.39', N'"description"', N'"South Dakota"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.39', N'"value"', N'"SD"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.39', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.40', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.40', N'"description"', N'"Tennessee"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.40', N'"value"', N'"TN"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.40', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.41', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.41', N'"description"', N'"Texas"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.41', N'"value"', N'"TX"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.41', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.42', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.42', N'"description"', N'"Utah"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.42', N'"value"', N'"UT"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.42', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.43', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.43', N'"description"', N'"Vermont"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.43', N'"value"', N'"VT"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.43', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.44', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.44', N'"description"', N'"Virginia"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.44', N'"value"', N'"VA"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.44', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.45', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.45', N'"description"', N'"Washington"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.45', N'"value"', N'"WA"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.45', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.46', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.46', N'"description"', N'"Washington, DC"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.46', N'"value"', N'"DC"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.46', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.47', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.47', N'"description"', N'"West Virginia"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.47', N'"value"', N'"WV"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.47', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.48', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.48', N'"description"', N'"Wisconsin"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.48', N'"value"', N'"WI"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.48', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.49', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.49', N'"description"', N'"Wyoming"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.49', N'"value"', N'"WY"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.49', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.50', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.50', N'"description"', N'"Ontario"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.50', N'"value"', N'"ON"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.50', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.51', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.51', N'"description"', N'"Alberta"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.51', N'"value"', N'"AB"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.51', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.52', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.52', N'"description"', N'"British Columbia"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.52', N'"value"', N'"BC"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.52', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.53', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.53', N'"description"', N'"Manitoba"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.53', N'"value"', N'"MB"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.53', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.54', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.54', N'"description"', N'"New Brunswick"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.54', N'"value"', N'"NB"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.54', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.55', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.55', N'"description"', N'"Newfoundland and Labrador"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.55', N'"value"', N'"NL"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.55', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.56', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.56', N'"description"', N'"Northwest Territories"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.56', N'"value"', N'"NT"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.56', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.57', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.57', N'"description"', N'"Northwest Territories"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.57', N'"value"', N'"NS"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.57', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.58', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.58', N'"description"', N'"Nova Scotia"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.58', N'"value"', N'"NS"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.58', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.59', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.59', N'"description"', N'"Nunavut"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.59', N'"value"', N'"NU"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.59', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.60', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.60', N'"description"', N'"Prince Edward Island"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.60', N'"value"', N'"PE"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.60', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.61', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.61', N'"description"', N'"Quebec"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.61', N'"value"', N'"QC"', 0, NULL)
GO
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.61', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.62', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.62', N'"description"', N'"Saskatchewan"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.62', N'"value"', N'"SK"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.62', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.63', N'"_parent"', N'Business.Field_value', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.63', N'"description"', N'"Yukon"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.63', N'"value"', N'"YT"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Field_value_list.of.states_and_provences.values.63', N'null', N'null', 0, NULL)
/* Container='Business.Document' */
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.backimage', N'"description"', N'"Back Image"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.backimage', N'"document_id"', N'"backimage"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.backimage', N'"document_type"', N'"D"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.backimage', N'"document_type_b"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.backimage', N'"document_type_c"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.backimage', N'"document_type_d"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.backimage', N'"document_type_f"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.backimage', N'"document_type_m"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.backimage', N'"icon_path"', N'"tender_check.jpg"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.RecallReceipt', N'"_parent"', N'Business.Document.Barcode', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.RecallReceipt', N'"description"', N'"Recall Suspended Receipt"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.RecallReceipt', N'"document_id"', N'"RecallReceipt"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.RecallReceipt', N'"scandata_identify_method"', N'"P"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.RecallReceipt', N'"scandata_identify_prefix"', N'"CS"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.RecallReceipt', N'"scandata_usages"', N'<v></v>', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.RecallReceipt', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.receiptsearch', N'"_parent"', N'Business.Document.Barcode', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.receiptsearch', N'"description"', N'"Receipt Search"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.receiptsearch', N'"document_id"', N'"receiptsearch"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.receiptsearch', N'"document_type"', N'"S"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.receiptsearch', N'"flags"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.receiptsearch', N'"icon_path"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.receiptsearch', N'"print_data_back"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.receiptsearch', N'"print_data_front"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.receiptsearch', N'"processing_flags"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.receiptsearch', N'"prompt_txt"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.receiptsearch', N'"scandata_identify_method"', N'"L"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.receiptsearch', N'"scandata_identify_prefix"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.receiptsearch', N'"scandata_usages"', N'<v></v>', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.receiptsearch', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.DCCardImage', N'"_parent"', N'Business.Document.DIGITALCHECK', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.DCCardImage', N'"description"', N'"Digital Check Card Image"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.DCCardImage', N'"digital_capture_type"', N'"None"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.DCCardImage', N'"digital_device_name"', N'"SB650M"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.DCCardImage', N'"digital_doc_resolution"', N'300', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.DCCardImage', N'"digital_doc_side"', N'"Card Image"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.DCCardImage', N'"digital_ocr_bottom"', N'0.00', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.DCCardImage', N'"digital_ocr_left"', N'0.00', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.DCCardImage', N'"digital_ocr_right"', N'0.00', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.DCCardImage', N'"digital_ocr_top"', N'0.00', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.DCCardImage', N'"document_id"', N'"DCCardImage"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.DCCardImage', N'"icon_path"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.DCCardImage', N'"prompt_txt"', N'"Please Insert Card for Scanning"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.DCCardImage', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.DCReleaseForm', N'"_parent"', N'Business.Document.DIGITALCHECK', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.DCReleaseForm', N'"description"', N'"Digital Check Release Form"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.DCReleaseForm', N'"digital_capture_type"', N'"None"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.DCReleaseForm', N'"digital_device_name"', N'"SB650M"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.DCReleaseForm', N'"digital_doc_resolution"', N'200', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.DCReleaseForm', N'"digital_doc_side"', N'"Front and Back"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.DCReleaseForm', N'"digital_ocr_bottom"', N'0.00', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.DCReleaseForm', N'"digital_ocr_left"', N'0.00', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.DCReleaseForm', N'"digital_ocr_right"', N'0.00', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.DCReleaseForm', N'"digital_ocr_top"', N'0.00', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.DCReleaseForm', N'"document_id"', N'"DCReleaseForm"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.DCReleaseForm', N'"icon_path"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.DCReleaseForm', N'"prompt_txt"', N'"Please Sign then insert Release form into Scanner."', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.DCReleaseForm', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.tenderdoc', N'"_parent"', N'Business.Document.Epson', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.tenderdoc', N'"description"', N'"Validation Page"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.tenderdoc', N'"document_id"', N'"tenderdoc"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.tenderdoc', N'"document_type"', N'"F"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.tenderdoc', N'"document_type_b"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.tenderdoc', N'"document_type_c"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.tenderdoc', N'"document_type_d"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.tenderdoc', N'"document_type_f"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.tenderdoc', N'"document_type_m"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.tenderdoc', N'"icon_path"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.tenderdoc', N'"ocr_height"', N'0.00', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.tenderdoc', N'"ocr_width"', N'0.00', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.tenderdoc', N'"ocr_x"', N'0.00', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.tenderdoc', N'"ocr_y"', N'0.00', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.tenderdoc', N'"print_data_back"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.tenderdoc', N'"print_data_front"', N'''"iPayment Receipt Issued."
"Receipt Reference:" file.id "-" event.id
"Receipt Date-Time:" current_datetime
"Receipt Amount: $" tender.total''', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.tenderdoc', N'"prompt_txt"', N'"Insert Document"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.tenderdoc', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.OCR', N'"document_type_c"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.OCR', N'"document_type_d"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.OCR', N'"document_type_f"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.OCR', N'"document_type_m"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.OCR', N'"icon_path"', N'"transcript4.jpg"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.OCR', N'"ocr_height"', N'0.50', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.OCR', N'"ocr_width"', N'8.50', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.OCR', N'"ocr_x"', N'0.50', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.OCR', N'"ocr_y"', N'0.20', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.OCR', N'"print_data_back"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.OCR', N'"print_data_front"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.OCR', N'"prompt_txt"', N'"Please Insert Stub into Printer with Numbers on Right"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.OCR', N'"sig_capture_action_accept"', N'"Continue and require a signature at the end"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.OCR', N'"sig_capture_action_decline"', N'"Continue and require a signature at the end"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.OCR', N'"sig_capture_action_no"', N'"Continue and require a signature at the end"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.OCR', N'"sig_capture_action_yes"', N'"Continue and require a signature at the end"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.OCR', N'"sig_capture_consumer_text"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.OCR', N'"sig_capture_form"', N'"Signature Capture Form"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.OCR', N'"sig_capture_name"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.OCR', N'"sig_capture_on_receipt"', N'"YES"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.OCR', N'"sig_capture_timing"', N'"Transaction/Tender Level"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.OCR', N'"sig_capture_user_text"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.OCR', N'"sig_cc_system_interface"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.OCR', N'"sig_doc_class"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Document.of.OCR', N'null', N'null', 0, NULL)
/* Container='Business.Department' */
/* Container='Business.data_retention' */
/* Container='Business.Gl_number_format' */
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary', N'"_parent"', N'Business.Gl_number_format', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary', N'"id"', N'"primary"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary', N'"name"', N'"Primary Ledger Format"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary', N'"segments"', N'<v>Business.Gl_number_format.of.primary.segments.0 Business.Gl_number_format.of.primary.segments.1 Business.Gl_number_format.of.primary.segments.2 Business.Gl_number_format.of.primary.segments.3 Business.Gl_number_format.of.primary.segments.4</v>', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.0', N'"_parent"', N'Business.Gl_number_segment', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.0', N'"chars"', N'Type.alpha', 0, NULL)
GO
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.0', N'"description"', N'"Org"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.0', N'"max_size"', N'4', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.0', N'"min_size"', N'4', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.0', N'"name"', N'"Organization"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.0', N'"read_only"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.0', N'"required"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.0', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.1', N'"_parent"', N'Business.Gl_number_segment', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.1', N'"chars"', N'Type.alphanumeric_and_space', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.1', N'"description"', N'"Co"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.1', N'"max_size"', N'11', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.1', N'"min_size"', N'0', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.1', N'"name"', N'"Company"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.1', N'"read_only"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.1', N'"required"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.1', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.2', N'"_parent"', N'Business.Gl_number_segment', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.2', N'"chars"', N'Type.numeric', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.2', N'"description"', N'"Fund Number"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.2', N'"max_size"', N'3', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.2', N'"min_size"', N'3', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.2', N'"name"', N'"Fund"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.2', N'"read_only"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.2', N'"required"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.2', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.3', N'"_parent"', N'Business.Gl_number_segment', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.3', N'"chars"', N'Type.alphanumeric', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.3', N'"description"', N'"Acct"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.3', N'"max_size"', N'5', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.3', N'"min_size"', N'5', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.3', N'"name"', N'"Account"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.3', N'"read_only"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.3', N'"required"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.3', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.4', N'"_parent"', N'Business.Gl_number_segment', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.4', N'"chars"', N'Type.typical_text', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.4', N'"description"', N'"Desc"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.4', N'"max_size"', N'20', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.4', N'"min_size"', N'0', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.4', N'"name"', N'"Description"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.4', N'"read_only"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.4', N'"required"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Gl_number_format.of.primary.segments.4', N'null', N'null', 0, NULL)
/* Container='Business.Fee' */
/* Container='Business.ldap_config' */
/* Container='Business.License' */
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.License.data', N'"customer_name"', N'"CORE Business Technologies"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.License.data', N'"end_date"', N'"01/03/2017"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.License.data', N'"license_key"', N'"ZHdaUTZhSGl5VzZNY2hKcHFFSHF0Q0VOYUpVU2ZnQWFyRllvV2hXTkNuTT0="', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.License.data', N'"maximum_departments"', N'9999', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.License.data', N'"one_event_per_file"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.License.data', N'"start_date"', N'"11/04/2011"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.License.data', N'null', N'null', 0, NULL)
/* Container='Business.Menu' */
/* Container='Business.Misc' */
--maybe don't need
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Misc.data', N'"activity_log_date_format"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Misc.data', N'"activity_log_wildcard_all_queries"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Misc.data', N'"autobatchupdate_user"', N'"tadminis"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Misc.data', N'"batchupdate_email_receiver"', N'"tduffy@corebt.com"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Misc.data', N'"blind_balancing_threshold"', N'0.25', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Misc.data', N'"cash_drawer_configured"', N'"None"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Misc.data', N'"cbc_captcha_secret_key"', N'"6LcVOhsTAAAAAGW1OHIUmZTVSbUlpl4UWHMIv7Tl"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Misc.data', N'"cbc_captcha_site_key"', N'"6LcVOhsTAAAAAIPebVFibLNDXp9RYtpVeUdZHOze"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Misc.data', N'"cbc_cutoff_time"', N'"12:00AM"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Misc.data', N'"CSV_email_domain_list"', N'"corebt.com,comcast.net"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Misc.data', N'"enable_manual_preh_encryption"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Misc.data', N'"password_configured_period"', N'90', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Misc.data', N'"password_expiration_max_days"', N'90', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Misc.data', N'"password_expiration_warning"', N'10', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Misc.data', N'"prehWS_endpoint_address"', N'"https://172.16.13.3/Preh_Decrypt_Web_Service/PrehDecryptSvc.svc"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Misc.data', N'"prehWS_endpoint_identity"', N'"MachineA"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Misc.data', N'"previous_password_count"', N'4', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Misc.data', N'"session_timeout_minutes"', N'14', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Misc.data', N'"use_hourly_log_files"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Misc.data', N'null', N'null', 0, NULL)
/* Container='Business.Report_type' */
/* Container='Business.Security_profile' */
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Security_profile.of.admin', N'"_parent"', N'Business.Security_profile', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Security_profile.of.admin', N'"id"', N'"admin3"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Security_profile.of.admin', N'"name"', N'"Administrator All Powerful"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Security_profile.of.admin', N'"security_items"', N'<m>Business.Security_item.of."300" Business.Security_item.of."292" Business.Security_item.of."150" Business.Security_item.of."319" Business.Security_item.of."316" Business.Security_item.of."313" Business.Security_item.of."304" Business.Security_item.of."163" Business.Security_item.of."310" Business.Security_item.of."301" Business.Security_item.of."160" Business.Security_item.of."329" Business.Security_item.of."151" Business.Security_item.of."999" Business.Security_item.of."110" Business.Security_item.of."317" Business.Security_item.of."299" Business.Security_item.of."325" Business.Security_item.of."294" Business.Security_item.of."323" Business.Security_item.of."167" Business.Security_item.of."314" Business.Security_item.of."322" Business.Security_item.of."105" Business.Security_item.of."296" Business.Security_item.of."311" Business.Security_item.of."170" Business.Security_item.of."155" Business.Security_item.of."290" Business.Security_item.of."181" Business.Security_item.of."327" Business.Security_item.of."308" Business.Security_item.of."124" Business.Security_item.of."130" Business.Security_item.of."330" Business.Security_item.of."326" Business.Security_item.of."315" Business.Security_item.of."321" Business.Security_item.of."312" Business.Security_item.of."328" Business.Security_item.of."162" Business.Security_item.of."991" Business.Security_item.of."291" Business.Security_item.of."137" Business.Security_item.of."309" Business.Security_item.of."168" Business.Security_item.of."125" Business.Security_item.of."131" Business.Security_item.of."318" Business.Security_item.of."320" Business.Security_item.of."324" Business.Security_item.of."103" Business.Security_item.of."156" Business.Security_item.of."295" Business.Security_item.of."331" Business.Security_item.of."332" Business.Security_item.of."400" Business.Security_item.of."161" Business.Security_item.of."152" Business.Security_item.of."334" Business.Security_item.of."171" Business.Security_item.of."172" Business.Security_item.of."336" Business.Security_item.of."337" Business.Security_item.of."104" Business.Security_item.of."138" Business.Security_item.of."409" Business.Security_item.of."139" Business.Security_item.of."302" Business.Security_item.of."307" Business.Security_item.of."335" Business.Security_item.of."276" Business.Security_item.of."273" Business.Security_item.of."281" Business.Security_item.of."333" Business.Security_item.of."274" Business.Security_item.of."271" Business.Security_item.of."272" Business.Security_item.of."303" Business.Security_item.of."275" Business.Security_item.of."101" Business.Security_item.of."122" Business.Security_item.of."100" Business.Security_item.of."126" Business.Security_item.of."612" Business.Security_item.of."402" Business.Security_item.of."282" Business.Security_item.of."990" Business.Security_item.of."297" Business.Security_item.of."193" Business.Security_item.of."403" Business.Security_item.of."401" Business.Security_item.of."134" Business.Security_item.of."135" Business.Security_item.of."278" Business.Security_item.of."102" Business.Security_item.of."192" Business.Security_item.of."280" Business.Security_item.of."340"</m>', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Security_profile.of.admin', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Security_profile.of.cashier', N'"_parent"', N'Business.Security_profile', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Security_profile.of.cashier', N'"id"', N'"cashier"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Security_profile.of.cashier', N'"name"', N'"Limited Cashier"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Security_profile.of.cashier', N'"security_items"', N'<m>Business.Security_item.of."110" Business.Security_item.of."130" Business.Security_item.of."991" Business.Security_item.of."168" Business.Security_item.of."166" Business.Security_item.of."161" Business.Security_item.of."170" Business.Security_item.of."131" Business.Security_item.of."135"</m>', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Security_profile.of.cashier', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Security_profile.of.supervi', N'"_parent"', N'Business.Security_profile', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Security_profile.of.supervi', N'"id"', N'"supervi"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Security_profile.of.supervi', N'"name"', N'"Supervisor"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Security_profile.of.supervi', N'"security_items"', N'<m>Business.Security_item.of."126" Business.Security_item.of."290" Business.Security_item.of."292" Business.Security_item.of."134" Business.Security_item.of."168" Business.Security_item.of."167" Business.Security_item.of."161" Business.Security_item.of."160" Business.Security_item.of."163" Business.Security_item.of."301" Business.Security_item.of."277" Business.Security_item.of."170" Business.Security_item.of."275" Business.Security_item.of."274" Business.Security_item.of."273" Business.Security_item.of."272" Business.Security_item.of."271" Business.Security_item.of."295" Business.Security_item.of."294" Business.Security_item.of."156" Business.Security_item.of."280" Business.Security_item.of."281" Business.Security_item.of."282" Business.Security_item.of."103" Business.Security_item.of."102" Business.Security_item.of."101" Business.Security_item.of."100" Business.Security_item.of."181" Business.Security_item.of."105" Business.Security_item.of."104" Business.Security_item.of."110" Business.Security_item.of."990" Business.Security_item.of."991" Business.Security_item.of."162" Business.Security_item.of."276" Business.Security_item.of."155" Business.Security_item.of."152" Business.Security_item.of."150" Business.Security_item.of."151" Business.Security_item.of."122" Business.Security_item.of."125" Business.Security_item.of."124" Business.Security_item.of."139" Business.Security_item.of."192" Business.Security_item.of."278" Business.Security_item.of."291" Business.Security_item.of."296" Business.Security_item.of."309" Business.Security_item.of."310" Business.Security_item.of."303" Business.Security_item.of."130" Business.Security_item.of."135"</m>', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Security_profile.of.supervi', N'null', N'null', 0, NULL)
/* Container='Business.Source' */
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Source.of.IMPORT', N'"_parent"', N'Business.Source', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Source.of.IMPORT', N'"company"', N'"CORE Business Technologies"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Source.of.IMPORT', N'"description"', N'"File Import"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Source.of.IMPORT', N'"fund"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Source.of.IMPORT', N'"id"', N'"IMPORT"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Source.of.IMPORT', N'"status"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.Source.of.IMPORT', N'null', N'null', 0, NULL)
/* Container='Business.Std_interface_import' */
/* Container='Business.System_interface' */
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ABAValidation', N'"_parent"', N'Business.System_interface.ABAValidation', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ABAValidation', N'"description"', N'"ABA Validation"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ABAValidation', N'"is_log"', N'"Y"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ABAValidation', N'"name"', N'"ABAValidation"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ABAValidation', N'"path"', N'"http://securetest5.ipayment.com/ABA_Validation_WebService/achservices.asmx"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ABAValidation', N'"service_name"', N'"BankName"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ABAValidation', N'"stub_mode"', N'"N"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ABAValidation', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInteractive', N'"_parent"', N'Business.System_interface.Web', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInteractive', N'"allow_certificate_address_mismatch"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInteractive', N'"allow_invalid_certificate_expiration_date"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInteractive', N'"allow_untrusted_certificate_authorities"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInteractive', N'"description"', N'"Argos Interactive"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInteractive', N'"enabled"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInteractive', N'"external_id_on_receipt"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInteractive', N'"external_id_on_video_journal"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInteractive', N'"external_id_prefix"', N'"ID:"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInteractive', N'"kind"', N'"update"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInteractive', N'"login_name"', N'"qa_test"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInteractive', N'"login_password"', N'"Hf46haurWnc6HDKROoQdgg=="', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInteractive', N'"name"', N'"ArgosInteractive"', 0, NULL)
GO
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInteractive', N'"uri"', N'"http://argosi.ipayment.com:8080/argos/index.html?report="', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInteractive', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInterface', N'"_parent"', N'Business.System_interface.Web', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInterface', N'"allow_certificate_address_mismatch"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInterface', N'"allow_invalid_certificate_expiration_date"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInterface', N'"allow_untrusted_certificate_authorities"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInterface', N'"description"', N'"Argos Interface"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInterface', N'"enabled"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInterface', N'"external_id_on_receipt"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInterface', N'"external_id_on_video_journal"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInterface', N'"external_id_prefix"', N'"ID:"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInterface', N'"kind"', N'"update"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInterface', N'"login_name"', N'"qa_test"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInterface', N'"login_password"', N'"Hf46haurWnc6HDKROoQdgg=="', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInterface', N'"name"', N'"ArgosInterface"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInterface', N'"uri"', N'"argosi.ipayment.com:8081"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.ArgosInterface', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.cbt_central_services', N'"_parent"', N'Business.System_interface.Web', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.cbt_central_services', N'"allow_certificate_address_mismatch"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.cbt_central_services', N'"allow_invalid_certificate_expiration_date"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.cbt_central_services', N'"allow_untrusted_certificate_authorities"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.cbt_central_services', N'"description"', N'"CBT Central Services"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.cbt_central_services', N'"enabled"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.cbt_central_services', N'"kind"', N'"update"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.cbt_central_services', N'"name"', N'"cbt_central_services"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.cbt_central_services', N'"uri"', N'"https://172.16.1.151/ipayment_trunk/ExternalCallsHandlerService.asmx"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.cbt_central_services', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.GL_Val', N'"_parent"', N'Business.System_interface.GL_Validation', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.GL_Val', N'"database_name"', N'"ipayment_trunk_trans"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.GL_Val', N'"datasource_name"', N'"172.16.13.101"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.GL_Val', N'"db_type"', N'"Sql_server"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.GL_Val', N'"description"', N'"GL Validation"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.GL_Val', N'"login_name"', N'"cashier"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.GL_Val', N'"login_password"', N'"MNLCJPdKgcc="', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.GL_Val', N'"name"', N'"GL_Val"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.GL_Val', N'"tns"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.GL_Val', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.IPCHARGE_TEST', N'"_parent"', N'Business.System_interface.IPCharge', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.IPCHARGE_TEST', N'"alternatehost1"', N'"https://APIDemo.IPCharge.net/IPCHAPI/RH.aspx"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.IPCHARGE_TEST', N'"alternatehost2"', N'"https://APIDemo.IPCharge.net/IPCHAPI/RH.aspx"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.IPCHARGE_TEST', N'"debit_card_test_mode"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.IPCHARGE_TEST', N'"description"', N'"IPCHARGE"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.IPCHARGE_TEST', N'"host"', N'"https://APIDemo.IPCharge.net/IPCHAPI/RH.aspx"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.IPCHARGE_TEST', N'"log_request"', N'true', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.IPCHARGE_TEST', N'"name"', N'"IPCHARGE_TEST"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.IPCHARGE_TEST', N'"pause1"', N'0', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.IPCHARGE_TEST', N'"pause2"', N'0', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.IPCHARGE_TEST', N'"stub_mode"', N'"N"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.IPCHARGE_TEST', N'"timeout"', N'90000', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.IPCHARGE_TEST', N'"timeout1"', N'6000', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.IPCHARGE_TEST', N'"timeout2"', N'6000', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.IPCHARGE_TEST', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate', N'"_parent"', N'Business.System_interface.PBStandardRealtimeUpdate', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate', N'"database_name"', N'"pb_inquiry_autodist"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate', N'"datasource_name"', N'"172.16.13.101"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate', N'"description"', N'"PB Standard Realtime Update"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate', N'"endpoint_identity"', N'"MachineA"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate', N'"exc_to_throw"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate', N'"is_log"', N'"Y"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate', N'"is_mask_in_log"', N'"N"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate', N'"log_timing_data"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate', N'"login_name"', N'"cashier"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate', N'"login_password"', N'"MNLCJPdKgcc="', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate', N'"masked_xml_tags_in_log"', N'"LoginName,bank_routing_nbr,MRN,address,studentid"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate', N'"name"', N'"PBRealTimeUpdate"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate', N'"number_of_retries"', N'3', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate', N'"path"', N'"http://ssg-banner-dev/WcfServiceSSI_4370/SSI.svc"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate', N'"request_type"', N'"PBRealTimeUpdateReqType"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate', N'"update_request_type"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate_2', N'"_parent"', N'Business.System_interface.PBStandardRealtimeUpdate', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate_2', N'"database_name"', N'"pb_inquiry_autodist"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate_2', N'"datasource_name"', N'"172.16.13.101"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate_2', N'"description"', N'"PB Standard Realtime Update_2"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate_2', N'"endpoint_identity"', N'"MachineA"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate_2', N'"exc_to_throw"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate_2', N'"is_log"', N'"Y"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate_2', N'"log_timing_data"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate_2', N'"login_name"', N'"cashier"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate_2', N'"login_password"', N'"MNLCJPdKgcc="', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate_2', N'"masked_xml_tags_in_log"', N'"LoginName,LoginPassword,bank_routing_nbr,MRN,address,studentid"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate_2', N'"name"', N'"PBRealTimeUpdate_2"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate_2', N'"number_of_retries"', N'2', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate_2', N'"path"', N'"http://ssg-banner-dev/WcfServiceSSI_4370/SSI.svc"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate_2', N'"request_type"', N'"PBRealTimeUpdateReqType"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBRealTimeUpdate_2', N'null', N'null', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBStandardInquiry', N'"_parent"', N'Business.System_interface.PBStandardInquiry', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBStandardInquiry', N'"database_name"', N'"ipayment_inquiry"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBStandardInquiry', N'"datasource_name"', N'"W10-MIMIP"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBStandardInquiry', N'"description"', N'"ProjectBaseline Standard Inquiry"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBStandardInquiry', N'"endpoint_identity"', N'"MachineA"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBStandardInquiry', N'"InquiryType"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBStandardInquiry', N'"is_log"', N'"Y"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBStandardInquiry', N'"is_mask_in_log"', N'"Y"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBStandardInquiry', N'"log_timing_data"', N'false', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBStandardInquiry', N'"login_name"', N'"cashier"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBStandardInquiry', N'"login_password"', N'"MNLCJPdKgcc="', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBStandardInquiry', N'"masked_xml_tags_in_log"', N'"LoginName,LoginPassword,bank_routing_nbr,MRN"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBStandardInquiry', N'"name"', N'"PBStandardInquiry"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBStandardInquiry', N'"number_of_retries"', N'2', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBStandardInquiry', N'"path"', N'"http://ssg-banner-dev/WcfServiceSSI_4370/SSI.svc"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBStandardInquiry', N'"request_type"', N'"PBRealTimeInquiryReqType"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBStandardInquiry', N'"stub_mode"', N'"N"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBStandardInquiry', N'"transfer_timeout"', N'"10"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'Business.System_interface.of.PBStandardInquiry', N'null', N'null', 0, NULL)
/* Container='Business.Tender' */
/* Container='Business.Transaction' */
/* Container='Business.User' */
/* Container='TranSuite_DB.Data' */
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'TranSuite_DB.Data.db_connection_info', N'"_label"', N'"Transaction Database"', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'TranSuite_DB.Data.db_connection_info', N'"_parent"', N'Db_connection_info.Sql_server', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'TranSuite_DB.Data.db_connection_info', N'"database_name"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'TranSuite_DB.Data.db_connection_info', N'"datasource_name"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'TranSuite_DB.Data.db_connection_info', N'"login_name"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'TranSuite_DB.Data.db_connection_info', N'"login_password"', N'""', 0, NULL)
INSERT [dbo].[CBT_Flex] ([container], [field_key], [field_value], [revision], [is_root]) VALUES (N'TranSuite_DB.Data.db_connection_info', N'null', N'null', 0, NULL)
GO