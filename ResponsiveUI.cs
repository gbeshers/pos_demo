﻿// Bug 20642 UMN - new responsive UI
using System;
using System.Collections.Generic;
using CASL_engine;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Web; //Bug 23080 NAM
using Fluid;	//Bug 22708 NAM
using Newtonsoft.Json;
using CsQuery;
using TidyManaged;
using KVPair = System.Collections.Generic.KeyValuePair<string, string>; //Bug 23623 NAM

namespace corebt
{
  public class ResponsiveUI
  {
    //Bug 22708 NAM 20180503 - make all members of the page object accessible/public
    /// <summary>
    /// ResponsiveUI - Static constructor
    /// </summary>
    static ResponsiveUI()
    {
      TemplateContext.GlobalMemberAccessStrategy.Register<Page>();
      TemplateContext.GlobalMemberAccessStrategy.Register<Footer>();
      TemplateContext.GlobalMemberAccessStrategy.Register<Header>();
      TemplateContext.GlobalMemberAccessStrategy.Register<Body>();
      TemplateContext.GlobalMemberAccessStrategy.Register<Receipt>();
      TemplateContext.GlobalMemberAccessStrategy.Register<Confirmation>();
      TemplateContext.GlobalMemberAccessStrategy.Register<Reference>();
      TemplateContext.GlobalMemberAccessStrategy.Register<Button>();
      TemplateContext.GlobalMemberAccessStrategy.Register<Section>();
      TemplateContext.GlobalMemberAccessStrategy.Register<Grid>();
      TemplateContext.GlobalMemberAccessStrategy.Register<Row>();
      TemplateContext.GlobalMemberAccessStrategy.Register<Column>();
      TemplateContext.GlobalMemberAccessStrategy.Register<Gparams>();
      TemplateContext.GlobalMemberAccessStrategy.Register<Nav>();
      TemplateContext.GlobalMemberAccessStrategy.Register<NavItem>();
      TemplateContext.GlobalMemberAccessStrategy.Register<Dialog>();
      TemplateContext.GlobalMemberAccessStrategy.Register<Cart>();
      TemplateContext.GlobalMemberAccessStrategy.Register<TenderOrTaxRow>();
      TemplateContext.GlobalMemberAccessStrategy.Register<TransactionRow>();
      TemplateContext.GlobalMemberAccessStrategy.Register<NestedTransactionRow>();
      TemplateContext.GlobalMemberAccessStrategy.Register<Amount>();
      TemplateContext.GlobalMemberAccessStrategy.Register<KVPair>();
      TemplateContext.GlobalMemberAccessStrategy.Register<Menu>();
      TemplateContext.GlobalMemberAccessStrategy.Register<TenderOrTrans>();
      TemplateContext.GlobalMemberAccessStrategy.Register<SimpleField>();
      TemplateContext.GlobalMemberAccessStrategy.Register<Field>();
      TemplateContext.GlobalMemberAccessStrategy.Register<Paypal>();
    }

    /// <summary>
    ///   Parses the original CASL rendered desktop html into objects that are then
    ///   rendered into a new responsive UI using templates. The templates can be
    ///   produced by an outside agency so they look nice.
    ///   Bug 20642 UMN: provide responsive UI
    /// </summary>
    /// <param name="desktopHTML">The original CASL rendered desktop html</param>
    /// <returns>A responsive UI (html) in a string</returns>
    public static string ReturnResponsiveUI(string desktopHTML, string browser, string absoluteURI, string rawURI)
    {
      // Render the non-responsive UI if flag isn't set. Removed page checking because we now can have
      // custom fields that can point to a customized template for any project.

      // Bug 22708 NAM - removed

      if (!(bool)((GenericObject)c_CASL.c_object("Business.Business_center_settings.data")).get("cbc_responsive_ui", false))
      {
        return desktopHTML;
      }

      System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
      TimeSpan ts;

      try
      {
        watch.Start();
        Page page = new Page();
        CQ dom = CQ.Create(desktopHTML);
        ParsePageType(browser, page, dom);

        if (String.IsNullOrWhiteSpace(page.page_type)) return desktopHTML;

        ParseCBCPage(desktopHTML, browser, page, dom);
        watch.Stop();
        ts = watch.Elapsed;
        if ((bool)((GenericObject)c_CASL.c_object("Business.Business_center_settings.data")).get("cbc_time_responsiveUI", false))
          Logger.cs_log(string.Format(" Parse {0} page took {1} seconds.", page.page_type, watch.ElapsedMilliseconds / 1000.00));

        // Bug 26346 UMN Output JSON as soon as it is available to log directory
        ProjectBaseline.ResponsiveUI.WriteJSONToLogDir(page, page.page_type);

        // Determine which template
        string template_name = GetTemplateName(page.page_type);
        // Bug 23816 NAM: add fluid template name to page header
        page.header.template_name = template_name;

        if (String.IsNullOrEmpty(template_name))
        {
          Logger.LogError($"Template not found for page_type: {page.page_type}, rawURI: {rawURI}, absURI: {absoluteURI}", "GetTemplateName");
          return desktopHTML;
        }

        // Bug 22708 NAM 20180426: Using the Fluid template engine
        FluidTemplate page_template = ProjectBaseline.TemplateLoader.Instance.GetTemplate(template_name);
        if (page_template == null)
          throw new Exception($"GetTemplate returned null for {template_name}");
        watch.Start();

        // Bug 22708 NAM 20180426: Using the Fluid template engine; resolves thread-safety issues
        TemplateContext context = new TemplateContext();
        context.SetValue("page", page);
        context.Model = page;
        string responsive_html = FluidTemplateExtensions.Render(page_template, context);

        watch.Stop();
        ts = watch.Elapsed;
        if ((bool)((GenericObject)c_CASL.c_object("Business.Business_center_settings.data")).get("cbc_time_responsiveUI", false))
          Logger.cs_log(string.Format(" Render {0} template took {1} seconds.", page.page_type, watch.ElapsedMilliseconds / 1000.00));

        // Output desktop and responsive html to log directory
        WriteHTMLToLogDir(null, responsive_html, page.page_type);
        return responsive_html;
      }
      catch (Exception e)
      {
        Logger.cs_log(Environment.NewLine + e.Message + e.StackTrace + Environment.NewLine);
        return desktopHTML;
      }
    }

    private static string GetTemplateName(string page_type)
    {
      string template_name = "";
      switch (page_type)
      {
        // Bug 22517 UMN add modal template
        case "cbc.search": template_name = "search.liquid"; break;
        case "cbc.modal": template_name = "modal.liquid"; break;
        case "cbc.zeromatch": template_name = "zeromatch.liquid"; break;
        case "cbc.singlematch": template_name = "singlematch.liquid"; break;
        case "cbc.multimatch": template_name = "multimatch.liquid"; break;
        case "cbc.buying": template_name = "main.liquid"; break;
        case "cbc.tendering": template_name = "tender.liquid"; break;
        case "cbc.ack_trans": template_name = "acknowledgement.liquid"; break;
        case "cbc.ack_tender": template_name = "acknowledgement.liquid"; break;
        case "cbc.receipted": template_name = "receipt.liquid"; break;
        case "cbc.message": template_name = "message.liquid"; break;    // Bug 19615 UMN

        // Bug 22376 UMN Custom Template Support
        default:
          template_name = page_type.Replace("cbc.", "") + ".liquid";
          break;
      }
      return template_name;
    }

    private static void WriteHTMLToLogDir(string desktopHTML, string responsive_html, string pageType)
    {
      //IPAY-1096 NAM: Don't create log file when show_verbose_error is turned off
      if ((!(bool)c_CASL.GEO.get("show_verbose_error", false)) || (!(bool)((GenericObject)c_CASL.c_object("Business.Business_center_settings.data")).get("cbc_log_responsiveUI", false)))
        return;

      string fileName = String.Format("{0}/{1}/{2}.", c_CASL.GEO.get("site_physical"), "log", pageType);
      TextWriter htmlWriter = null;
      try
      {
        // output responsive html, overwriting whatever was in file
        if (!String.IsNullOrEmpty(responsive_html))
        {
          htmlWriter = TextWriter.Synchronized(File.CreateText(fileName + "responsive.html"));
          htmlWriter.Write(responsive_html + Environment.NewLine);
          htmlWriter.Close(); htmlWriter = null;
        }

        // output indented, cleaned up desktop HTML
        if (!String.IsNullOrEmpty(desktopHTML))
        {

          using (TidyManaged.Document doc = TidyManaged.Document.FromString(desktopHTML))
          {
            doc.ShowWarnings = false;
            doc.Quiet = true;
            doc.ForceOutput = true;
            doc.IndentSpaces = 2;
            doc.OutputHtml = true;
            doc.OutputXhtml = false;
            doc.IndentBlockElements = AutoBool.Yes;
            doc.IndentAttributes = true;
            doc.RemoveComments = true;
            doc.AddVerticalSpace = false;
            doc.WrapAttributeValues = true;
            doc.WrapAt = 120;
            doc.DocType = DocTypeMode.Omit;
            doc.PreserveEntities = true;
            doc.CleanAndRepair();
            string formattedHTML = doc.Save();
            if (formattedHTML == "\0")
            {
              htmlWriter = TextWriter.Synchronized(File.CreateText(fileName + "messy-desktop.html"));
              htmlWriter.Write(desktopHTML + Environment.NewLine);
            }
            else
            {
              htmlWriter = TextWriter.Synchronized(File.CreateText(fileName + "desktop.html"));
              htmlWriter.Write(formattedHTML + Environment.NewLine);
            }
            htmlWriter.Close(); htmlWriter = null;
          }
        }
      }
      finally
      {
        if (htmlWriter != null) htmlWriter.Close();
      }
    }

    /// <summary>
    /// New parser based on CsQuery(jquery)
    /// </summary>
    /// <param name="desktopHTML"></param>
    /// <returns>The page data structure</returns>
    private static void ParseCBCPage(string desktopHTML, string browser, Page page, CQ dom)
    {
      WriteHTMLToLogDir(desktopHTML, null, page.page_type);

      // no overall form for buying page, have multiple ones!
      if (page.page_type != "cbc.buying")
      {
        var formSelector = dom["form.form_inst, #form_class"];
        if (formSelector.Length > 0) page.form_action = '"' + formSelector.First().Attr("action") + '"';
      }
      ParseThemeColorsJson(page, dom);
      ParseHeader(page, dom);
      ParseFooter(page, dom);
      ParseBody(desktopHTML, page, dom);

      // Bug 19730 always parse the cart if it is available
      ParseCartRows(page, dom, browser);

      if (page.page_type == "cbc.receipted")
      {
        ParseReceiptCartRows(page, dom, browser);
      }
      else if (page.page_type.StartsWith("cbc.ack"))
      {
        page.body.description = "Acknowledgement";
        page.body.terms = dom["div[data-terms-conditions],table[data-terms-conditions]"].First().Attr("data-terms-conditions");
        ParseAckCartRows(page, dom, browser);
      }
    }

    private static void ParsePageType(string browser, Page page, CQ dom)
    {
      // Bug 22376 UMN Custom Template Support. Not custom templates can only be in modal pages,
      //               not for buying, receipt, or tender
      var customTemplateNode = dom["#custom_template"];
      if (customTemplateNode.Length > 0)
      {
        page.page_type = customTemplateNode.First().Attr("data-page-name");
      }
      else if (dom["table[ui*='cbc.multimatch']"].Length > 0)
      {
        page.page_type = "cbc.multimatch";
      }
      else if (dom["table[ui*='cbc.singlematch']"].Length > 0)
      {
        page.page_type = "cbc.singlematch";
      }
      else if (dom["table[ui*='cbc.zeromatch']"].Length > 0)
      {
        page.page_type = "cbc.zeromatch";
      }
      else
      {
        var selector = (browser == "IE") ? "table[ui*='cbc.']" : "div[ui*='cbc.']";
        var framer = dom[selector];
        if (framer.Length > 0)
        {
          page.page_type = framer.First().Attr("ui");
        }
      }
    }

    private static void ParseBody(string desktopHTML, Page page, CQ dom)
    {
      ParseDescription(page, dom);

      page.body.buttons = ParseBodyButtons(dom, page.page_type);

      // Note: with Bug 19615, ParseErrors if it finds a message with arbitrary page flow (signified by json element),
      // it will reset the page.type to be cbc.message. 
      page.body.errors = ParseErrors(desktopHTML, page, dom);

      ParseVariants(desktopHTML, page, dom);
    }

    private static void ParseVariants(string desktopHTML, Page page, CQ dom)
    {
      switch (page.page_type)
      {
        // Bug 22517 UMN add modal template
        case "cbc.modal":
        case "cbc.search":
          page.body.sections = ParsePageSections(dom);
          //if (page.body.sections.Count == 0)
          //{
          //  var start = dom["form"];
          //  page.body.fields = ParseFields(start);
          //}
          break;

        case "cbc.buying":
          page.body.transactions = ParseTransactionList(dom);
          break;

        case "cbc.tendering":
          page.body.sections = ParseTenderSections(dom);
          break;

        case "cbc.multimatch":
        case "cbc.singlematch":
        case "cbc.zeromatch":
          // Bug 26346 UMN parse regular custom fields before block custom fields
          // we skip fields that don't have a line number
          page.body.fields = ParseFields(dom["form"], true);
          page.body.fields.AddRange(ParseGrids(desktopHTML, page, dom));

          // Bug 26346 UMN parse allocations
          page.body.fields.AddRange(ParseAllocationGrids(desktopHTML, page, dom));
          break;

        case "cbc.receipted":
          ParseReceiptInfo(desktopHTML, page, dom);
          break;

        // Bug 22376 UMN Custom Template Support
        default:
          if (page.page_type.StartsWith("cbc.multimatch") ||
              page.page_type.StartsWith("cbc.singlematch") ||
              page.page_type.StartsWith("cbc.zeromatch"))
          {
            // Bug 26346 UMN parse regular custom fields before block custom fields
            // we skip fields that don't have a line number
            page.body.fields = ParseFields(dom["form"], true);
            page.body.fields.AddRange(ParseGrids(desktopHTML, page, dom));

            // Bug 26346 UMN parse allocations
            page.body.fields.AddRange(ParseAllocationGrids(desktopHTML, page, dom));
          }
          break;
      }
    }

    private static void ParseDescription(Page page, CQ dom)
    {
      // Decided NOT to make this configurable, so we have consistency in all project BC UIs
      switch (page.page_type)
      {
        case "cbc.buying":
          page.body.description = "Search Options";
          break;

        case "cbc.search":
          page.body.description = "Search";
          break;

        // Bug 22517 UMN add modal template
        case "cbc.modal":
          page.body.description = "Enter/Confirm";
          break;

        case "cbc.zeromatch":
        case "cbc.singlematch":
        case "cbc.multimatch":
          ParseQueryKeys(page, dom);
          break;

        case "cbc.tendering":
          page.body.description = "Payment Options";
          break;

        case "cbc.ack_tender":
          page.body.description = "Confirmation of Payment";
          break;

        case "cbc.ack_trans":
          var pageDesc = dom["div[data-page-descr],table[data-page-descr]"].First().Attr<String>("data-page-descr");
          page.body.description = pageDesc.Trim();
          break;

        case "cbc.receipted":
          page.body.description = "Your transaction is complete – Thank you!";
          break;

        // Bug 22376 UMN Custom Template Support
        default:
          if (page.page_type.StartsWith("cbc.multimatch") ||
              page.page_type.StartsWith("cbc.singlematch") ||
              page.page_type.StartsWith("cbc.zeromatch"))
          {
            ParseQueryKeys(page, dom);
          }
          break;
      }
    }


    private static void ParseQueryKeys(Page page, CQ dom)
    {
      var resultsDesc = dom["div[data-results-descr],table[data-results-descr]"].First().Attr<String>("data-results-descr");
      page.body.description = resultsDesc.Trim();

      List<KVPair> queryKeys = new List<KVPair>();
      var queryKeysStr = dom["div[data-query-keys],table[data-query-keys]"].First().Attr<String>("data-query-keys");

      //Bug 23623 NAM: using KeyValuePair class aliased as KVPair
      // query keys is a JS-style string. Examples: "{ "VIOLATION_NUMBER_NOL":"3333333338" }"
      // { "MRN":"123456", "EMPI": "29076" }"
      var queryKeyPair = queryKeysStr.Split(',');
      for (int i = 0; i < queryKeyPair.Length; i++)
      {
        KVPair kv;
        var pair = queryKeyPair[i].Split(':');
        if (pair.Length == 2)
        {
          kv = new KVPair(pair[0].Trim(), pair[1].Trim());
          queryKeys.Add(kv);
        }
        else if (pair.Length == 1)
        {
          kv = new KVPair(pair[0].Trim(), "");
          queryKeys.Add(kv);
        }
      }
      page.body.query_keys = queryKeys;
    }

    private static void ParseReceiptInfo(string desktopHTML, Page page, CQ dom)
    {
      page.body.confirmation.confirmation_msg_1 = dom["p.legal"].First().Text().Trim();
      page.body.confirmation.confirmation_msg_2 = dom["p.legal2"].First().Text().Trim();
      page.body.confirmation.confirmation_msg_3 = dom["p.legal3"].First().Text().Trim();
      page.body.confirmation.confirmation_msg_4 = dom["p.legal4"].First().Text().Trim();

      page.body.receipt.receipt_header = dom["#receipt_header"].Next().First().Text().Trim();   //Bug 27555 NAM: receipt header text in a <PRE> tag
      page.body.receipt.receipt_message_top = dom["#receipt_message_top"].First().Text().Trim();
      page.body.receipt.receipt_message_bottom = dom["#receipt_message_bottom"].First().Text().Trim();
      page.body.receipt.receipt_footer = dom["#receipt_footer"].First().Text().Trim();
      page.body.receipt.duplicate_receipt = "";  // Not supported in BC

      page.body.reference.description = dom["div.confirmation_number_label"].First().Text().Trim();
      page.body.reference.number = dom["div.confirmation_number"].First().Text().Trim();
      page.body.reference.timestamp = dom["div span.core_event_actual_date"].First().Text().Trim();

      page.body.reference.prefix = dom["div.report_barcode[data-barcode-prefix]"].First().Attr<String>("data-barcode-prefix");

      page.body.disclaimer = dom["div[data-disclaimer]"].First().Attr<String>("data-disclaimer");
    }

    static readonly Regex errorPat = new Regex(@"else \{.*enqueue_message\(([^\)]*)\)", RegexOptions.Compiled);

    // Note: with Bug 19615, ParseErrors if it finds a message with arbitrary page flow (signified by json element),
    // it will reset the page.type to be cbc.message. 
    private static List<String> ParseErrors(string desktopHTML, Page page, CQ dom)
    {
      var ret = new List<String>();

      MatchCollection matches = errorPat.Matches(desktopHTML);

      if (matches.Count > 0)
      {
        Match match = matches[0];
        GroupCollection groups = match.Groups;
        string err_msg = "";

        // now we have to search for the error in the error message list
        string err = groups[1].Value.Replace("\"", "").Replace(@"""", "");
        int idx = desktopHTML.IndexOf(err);
        if (idx != -1)
        {
          var start = desktopHTML.IndexOf("err_msg:", idx) + "err_msg:".Length;
          var end = desktopHTML.IndexOf(", content:", start + 1);
          err_msg = ProjectBaseline.ResponsiveUI.StripHTML(desktopHTML.Substring(start + 1, (end - 1) - (start + 1)));
          err_msg = Regex.Replace(err_msg, @"^The host.*Error code:[^\)]*\)", ""); // Bug 26894 UMN

          // Bug 19615 Support messages
          start = desktopHTML.IndexOf("json:", idx) + "json:".Length;
          end = desktopHTML.IndexOf(", actions:", start + 1);
          string json = desktopHTML.Substring(start + 1, (end - 1) - (start + 1));
          if (String.IsNullOrWhiteSpace(json))  // if json string is empty, then it will be a plain old error
          {
            if (!String.IsNullOrEmpty(err_msg)) ret.Add(err_msg);
          }
          else  // there be JSON arrgh. So process it and perpare to invoke the message template
          {
            page.body.description = "Message";
            page.body.terms = err_msg;
            Dialog d = Newtonsoft.Json.JsonConvert.DeserializeObject<Dialog>(json);
            page.body.buttons = d.buttons;
            page.page_type = "cbc.message";
          }
        }
      }
      return ret;
    }

    static readonly Regex blockPat = new Regex(@"/\*0mbl0\*/(.+?)/\*1mbl1\*/", RegexOptions.Compiled | RegexOptions.Singleline);

    /// <summary>
    /// Parses all the UI blocks encoded into JSON for the responsive UI.
    /// </summary>
    /// <param name="desktopHTML"></param>
    /// <param name="dom"></param>
    /// <returns></returns>
    private static List<Field> ParseGrids(string desktopHTML, Page page, CQ dom)
    {
      var grids = new List<Field>();

      MatchCollection matches = blockPat.Matches(desktopHTML);

      // because of the stupid client_execute, the JSON is repeated twice in the output, so skip every other one
      for (var i = 0; i < matches.Count; i += 2)
      {
        Match match = matches[i];
        GroupCollection groups = match.Groups;

        string source = groups[1].Value.Replace(@"\n", "");
        // WriteJSONToLogDir(null, source, "pre-inquiry");
        Field f = Newtonsoft.Json.JsonConvert.DeserializeObject<Field>(source);
        grids.Add(f);
      }

      // Bug 20642 UMN responsive TODO make hoisting search item group configurable
      if (page.page_type == "cbc.zeromatch")
        AdjustSearchItemGroup(grids, page);
      else
        HoistPaymentItemOutOfGroup(grids, page);

      return grids;
    }

    /// <summary>
    /// Hoist the payment item that the user searched for into its own payment group.
    /// In the Responsive UI, hoisting means taking a line item out of a group and putting it at the top so it can be 
    /// seen easily. NYC asked for this initially because when a user did an inquiry on their violation number, they 
    /// wanted that specific violation/ticket to be displayed prominently at the top of the page, outside of the 
    /// Parking Violations group.
    /// 
    /// The hoisting code will only hoist if:
    /// 
    /// 1. The # of query keys was one (so a plate number search wouldn't trigger this code since the search would 
    ///    have a plate number and plate type) AND the query key and value matched one of the name/value pairs in the 
    ///    inquiry detail line item, OR
    /// 2. There's only one group and one line item returned from inquiry.
    /// </summary>
    /// <param name="grids"></param>
    /// <param name="page"></param>
    private static void HoistPaymentItemOutOfGroup(List<Field> grids, Page page)
    {
      Field hoistedField = null;

      // Bug 26903 UMN hoist if there's only one group and one line item returned from inquiry
      if (grids.Count == 1 && grids[0].grid.rows.Count == 1)
      {
        hoistedField = HoistPaymentItem(grids[0], 0);
        // Bug IPAY-448 RDM - Remove group if it's empty after hoist
        if (grids[0].grid.rows.Count == 0)
            grids.RemoveAt(0);
      }
      else if (page.body.query_keys.Count == 1)
      {
        for (int i = 0; i < grids.Count; i++)
        {
          for (int j = 0; grids.Count > 0 && j < grids[i].grid.rows.Count; j++)
          {
            for (int k = 0; k < grids[i].grid.rows[j].data.Count; k++)
            {
              if (grids[i].grid.rows[j].data[k].Key == "VIOLATION_NUMBER_NOL" &&
                  grids[i].grid.rows[j].data[k].Value == page.body.query_keys[0].Value &&
                  grids[i].grid.gparams.block_type == "block_payment")
              {
                hoistedField = HoistPaymentItem(grids[i], j);
                // Bug IPAY-448 RDM - Remove group if it's empty after hoist
                if (grids[i].grid.rows.Count == 0)
                    grids.RemoveAt(i);
                break;
              }
            }
          }
        }
      }
      if (hoistedField != null)
      {
        SetRowPaySelector(hoistedField, "true");
        grids.Insert(0, hoistedField);
      }
    }

    /// <summary>
    /// Hoists an individual payment item out of the group
    /// IPAY-448 UMN Changed method signature so that Clear can't be called on all the grids!
    /// </summary>
    /// <param name="field">The field to hoist</param>
    /// <param name="rowIndex">The row index of the field (used when removing)</param>
    /// <returns></returns>
    private static Field HoistPaymentItem(Field field, int rowIndex)
    {
      Field hoistedField;

      // Can't use copy, need deep copy, so use JSON serialization/deserialization!
      string hoistedFieldStr = Newtonsoft.Json.JsonConvert.SerializeObject(field);
      hoistedField = Newtonsoft.Json.JsonConvert.DeserializeObject<Field>(hoistedFieldStr);

      string hoistedRowStr = Newtonsoft.Json.JsonConvert.SerializeObject(field.grid.rows[rowIndex]);
      Row hoistedRow = Newtonsoft.Json.JsonConvert.DeserializeObject<Row>(hoistedRowStr);

      // Create a new grid with only the hoistedRow.
      hoistedField.grid.rows.Clear();
      hoistedField.grid.rows.Add(hoistedRow);
      hoistedField.grid.gparams.block_type = "block_hoisted";   // // Bug 26903 UMN new type so don't interfere with regular fixed blocks

      // remove the field from the original group, and if the group is now empty, remove the group
      field.grid.rows.RemoveAt(rowIndex);
      return hoistedField;
    }

    static readonly Regex allocPat = new Regex(@"/\*2mbl2\*/(.+?)/\*3mbl3\*/", RegexOptions.Compiled | RegexOptions.Singleline);

    /// <summary>
    /// Parses all the allocation UI blocks encoded into JSON for the responsive UI.
    /// Bug 26346 UMN
    /// </summary>
    /// <param name="desktopHTML"></param>
    /// <param name="dom"></param>
    /// <returns></returns>
    private static List<Field> ParseAllocationGrids(string desktopHTML, Page page, CQ dom)
    {
      var grids = new List<Field>();

      MatchCollection matches = allocPat.Matches(desktopHTML);

      // because of the stupid client_execute, the JSON is repeated twice in the output, so skip every other one
      for (var i = 0; i < matches.Count; i += 2)
      {
        Match match = matches[i];
        GroupCollection groups = match.Groups;

        string source = groups[1].Value.Replace(@"\n", "");
        // WriteJSONToLogDir(null, source, "pre-inquiry");
        Field f = Newtonsoft.Json.JsonConvert.DeserializeObject<Field>(source);
        grids.Add(f);
      }

      // Don't hoist allocation block
      return grids;
    }

    /// <summary>
    /// Set the pay selector for the group.
    /// Bug 20642 UMN responsive.
    /// </summary>
    /// <param name="field"></param>
    /// <param name="value"></param>
    private static void SetRowPaySelector(Field field, string value)
    {
      // First find the column that has RESERVED_SELECTED set
      string selName = "";

      for (int i = 0; i < field.grid.columns.Count; i++)
      {
        if (field.grid.columns[i].role == "RESERVED_SELECTED") selName = field.grid.columns[i].name;
      }

      for (int j = 0; j < field.grid.rows.Count && !String.IsNullOrWhiteSpace(selName); j++)
      {
        for (int k = 0; k < field.grid.rows[j].data.Count; k++)
        {
          //Bug 23623 NAM: use accessor classes - Key & Value
          if (field.grid.rows[j].data[k].Key == selName)
            field.grid.rows[j].data[k] = new KVPair(selName, value);
        }
      }
    }

    /// <summary>
    /// Adjust the search item group for a zero-match.
    /// </summary>
    /// <param name="grids"></param>
    /// <param name="page"></param>
    private static void AdjustSearchItemGroup(List<Field> grids, Page page)
    {
      // for zero-match, just adjust the existing group, setting the block to fixed
      // Will be only one field/grid
      grids[0].grid.gparams.block_type = "block_fixed";
      SetRowPaySelector(grids[0], "true");
    }

    private static List<Section> ParsePageSections(CQ dom)
    {
      var sectionNodes = dom["tbody.field_set"];
      var ret = new List<Section>();

      if (sectionNodes.Length > 0)
      {
        foreach (var sectionNode in sectionNodes)
        {
          var secNodeSel = sectionNode.Cq();
          var help = secNodeSel.Attr<String>("data-fieldset-help");
          var description = secNodeSel.Find("td.legend").Text().Trim();
          Section s = ParseSection(secNodeSel, description, help, "");
          ret.Add(s);
        }
      }
      else
      {
        var form = dom["form"];
        var section = ParseSection(form, "default", "", "");
        ret.Add(section);
      }
      return ret;
    }

    private static List<Section> ParseTenderSections(CQ dom)
    {
      var sectionNodes = dom["div[data-tab-type]"];
      var ret = new List<Section>();
      if (sectionNodes.Length > 0)
      {
        Section s = null;
        foreach (var sectionNode in sectionNodes)
        {
          var secNodeSel = sectionNode.Cq();
          string sectionId = sectionNode.Attributes["data-tab-key"]; // will be 0, 1, 2, etc
          // Bug 23349 NAM: changed identifier from 'help' to 'instructions'
          string sectionInstructions = sectionNode.Attributes["data-tab-instructions"];
          if (String.IsNullOrWhiteSpace(sectionInstructions)) sectionInstructions = "";
          // Bug 26943 UMN handle icons for tenders
          string iconPath = sectionNode.Attributes["data-icon-path"];
          if (String.IsNullOrWhiteSpace(sectionInstructions)) sectionInstructions = "";
          var a = secNodeSel.Find("button");        //Bug 23022 NAM: find button inside div
          var url = a.First().Attr("data-btn-url"); //Bug 23022 NAM: retrieve button href from data-btn-url attribute
          var description = a.Text().Trim();
          string formSectionId = dom["form[data-form-id]"].First().Attr<String>("data-form-id");

          // only one form is available, and it's associated with the first tender
          if (sectionId == formSectionId)
          {
            s = ParseTenderSection(dom["form"], description, sectionInstructions, url, iconPath);  //Bug 23349 NAM
          }
          else
          {
            s = new Section(description, "section", sectionInstructions, url, iconPath);  //Bug 23349 NAM
          }
          ret.Add(s);
        }
      }
      else
      {
        var section = ParseTenderSection(dom["form"], "default", "", "", "");
        ret.Add(section);
      }

      return ret;
    }

    /// <summary>
    /// Parse the transactions list in the buying page.
    /// </summary>
    /// <param name="dom"></param>
    /// <returns></returns>
    private static List<TenderOrTrans> ParseTransactionList(CQ dom)
    {
      var ret = new List<TenderOrTrans>();
      var tranForms = dom["form[ui='Enter_core_item_app.htm_row_inst']"];
      //Bug 23604 NAM: find all divs with button links of class htm_icon
      var tranButtonLinks = dom["div[class='htm_icon']"];
      if (tranForms.Length > 0)
      {
        // The double submit value will be the same in each form, so grab the first one
        string dsv = dom["input[name='__DOUBLESUBMIT__']"].First().Val<string>();

        foreach (var form in tranForms)
        {
          // pull id from: id="my_0_first_section_list_0_0 <== last digits
          string url = form.GetAttribute("action", "");
          string id = form.GetAttribute("id");
          int index = id.LastIndexOf("_") + 1;
          id = id.Substring(index);
          TenderOrTrans transaction = new TenderOrTrans();
          transaction.id = id;
          transaction.url = url.Replace("&amp;", "&"); //Bug 22708 Fluid renders liquid templates html encoded by default
          transaction.dsv = dsv;

          // get description, tagname which is in tr node. It's technically in the FORM, but our HTML is all
          // f'ed up and doesn't validate, so CsQuery doesn't find it. So have to go to parent, and then make
          // sure we get the matching input_class row. Sigh.
          var inputClassSel = String.Format("tr[data-tr-row-idx='{0}']", transaction.id);
          var rowInputClassNode = form.Cq().Parent().Find(inputClassSel);
          var dataTrRowIdx = rowInputClassNode.First().Attr("data-tr-row-idx");
          //BUG 22573 NAM: verify input/node exists before continuing. Transactions without the 'data-tr-row-idx' attribute are not supported
          //These are typically 'links' that have an href on the onclick instead submitting a form. i.e. A link to a menu
          if (rowInputClassNode.Length == 0 && string.IsNullOrEmpty(dataTrRowIdx))
            continue;
          transaction.ttid = rowInputClassNode.First().Attr("id").Replace(".tr", "");

          // get description from input field, not "f description" as CASL will overlay prompt there
          transaction.description = rowInputClassNode.First().Find("td input").First().Val<String>().Trim();

          ret.Add(transaction);
        }

      }
      else if (tranButtonLinks.Length > 0)  //Bug 23604 NAM: add button links as transactions - set id, url and description
      {
        int nIndex = 0;
        foreach (var button in tranButtonLinks)
        {
          TenderOrTrans transaction = new TenderOrTrans();
          transaction.id = nIndex.ToString();   //since there're no ids create one
          // get button href and assign to transaction
          string sURL = button["onclick"];
          int nIndexMatch = sURL.IndexOf("'");
          sURL = sURL.Substring(nIndexMatch + 1, sURL.LastIndexOf("'") - (nIndexMatch + 1));
          transaction.url = sURL;
          // get description from inner text
          transaction.description = button.InnerText;
          nIndex++;
          ret.Add(transaction);
        }
      }

      var icons = dom["td[class='htm_icon_row']"];
      if (icons.Length >= ret.Count)
      {
        for (int i = 0; i < ret.Count; ++i)
        {
          var icon_el = icons.Get(i);
          var icon_path = icon_el.Attributes.GetAttribute("data-icon-path-responsive-ui");
          ret[i].icon_path = icon_path;
        }
      }

      return ret;
    }

    private static Section ParseSection(CQ sectionNode, string description, string help, string iconPath)
    {
      Section ret = new Section(description, "section", help, "", iconPath);
      ret.fields = ParseFields(sectionNode);
      return ret;
    }

    private static Section ParseTenderSection(CQ sectionNode, string description, string help, string url, string iconPath)
    {
      Section ret = new Section(description, "section", help, url, iconPath);
      var paypal_config = sectionNode.Find("#paypal_config");
      string paypal_client_id = paypal_config.Attr("data-paypal-client-id");
      string paypal_amount = paypal_config.Attr("data-paypal-amount");
      bool paypal_enable_paypal = paypal_config.Attr("data-paypal-enable-paypal") == "true";
      bool paypal_enable_venmo = paypal_config.Attr("data-paypal-enable-venmo") == "true";
      string paypal_line_items = paypal_config.Attr("data-paypal-line-items");
      if (paypal_client_id != null & paypal_amount != null)
      {
        ret.paypal = new Paypal();
        ret.paypal.client_id = paypal_client_id;
        ret.paypal.amount = paypal_amount;
        ret.paypal.enable_paypal = paypal_enable_paypal;
        ret.paypal.enable_venmo = paypal_enable_venmo;
        ret.paypal.line_items = paypal_line_items;
      }
      ret.fields = ParseFields(sectionNode);
      return ret;
    }

    // Bug 26346 UMN added ignore_zero_line_numbers parameter
    private static List<Field> ParseFields(CQ sectionNode, bool ignore_zero_line_numbers = false)
    {
      var ret = new List<Field>();

      // Dictionary to preserve order in section fields
      var field_dict = new OrderedDictionary();

      // range over the input elements and the select elements. Need to preserve order!
      // TODO: add grid?
      var fields = sectionNode.Find("input[name],select[name],div.navbar,img.image,#mirasecure_iframe");
      foreach (IDomObject f in fields)
      {
        string tagname = null;
        Field field = null;

        // Bug 26537 UMN - Eigen MiraPay support
        if (f.Id == "mirasecure_iframe")
        {
          field = AddMiraPayFrameField(f);
          ret.Add(field);
          field = AddMiraPayValidationField("mirasecure_iframe_validation");
          ret.Add(field);
          continue;
        }
        else if (f.ClassName == "navbar" || f.ClassName == "image")
        {
          tagname = f.GetAttribute("data-tagname");
        }
        else
        {
          string name = f.Name;
          if (String.IsNullOrEmpty(name) || !name.StartsWith("args.")) continue;

          if (name.Contains("_f_selected"))
            tagname = name.Split('.')[1].Replace("_f_selected", ""); // RDM BUG 23509 _f_selected is only applied for boolean custom fields
          else if (name.Contains("args.0"))  // skip allocations, will be parsed by ParseAllocations
            continue;
          else
            tagname = name.Split('.')[1];
          if (name == "description") tagname = f.Value;
        }

        if (field_dict.Contains(tagname))
        {
          // RDM BUG 23509 - don't process field value lists twice
          if (f.GetAttribute("type").Equals("radio") || f.GetAttribute("type").Equals("checkbox"))
            continue;
          // RDM BUG 23509 - don't process boolean field twice
          if (f.GetAttribute("type").Equals("hidden") && f.GetAttribute("name").Equals(ret[ret.Count - 1].simple.name))
            continue;
          field = field_dict[tagname] as Field;
        }
        else
        {
          field = ParseSimpleField(tagname, f.Cq(), sectionNode);

          // Bug 26346 UMN skip fields that don't have a line number
          if (ignore_zero_line_numbers && field.simple.line_number == 0)
            continue;
          field_dict[tagname] = field;
        }

        // Now get the description
        string descripSelector = String.Format("td.k.{0}, span.k.{0}", tagname);
        var descripNode = sectionNode.Find(descripSelector);
        if (descripNode.Length > 0)
        {
          field.description = descripNode.First().Text().Trim();
          if (field.description.Contains("*"))
          {
            field.simple.required = true;
            field.description = field.description.Replace("*", "");
          }
        }

        // needed because tender required fields aren't setting required properly. So set based on whether
        // there's a * in the description.

        ret.Add(field);
      }
      return ret;
    }

    private static Field ParseSimpleField(string tagname, CQ f, CQ dom)
    {
      Field field = new Field();
      SimpleField simple = new SimpleField();

      simple.options = new List<KVPair>();
      simple.validation_name = tagname;
      //Bug 25774/24868/23816 NAM: add data-type to validation_name to triger proper client side validation. bank name and bank routing number don't have the data-type attribute
      if (!tagname.Equals("bank_name") && !tagname.Equals("bank_routing_nbr") && !tagname.Equals("ccv"))
      {
        string sDataValidation = f[0].GetAttribute("data-type", string.Empty);
        if (!string.IsNullOrEmpty(sDataValidation))
          simple.validation_name = string.Format(" {0}", sDataValidation);
      }
      if (f[0] is IHTMLInputElement)
      {
        field.widget_type = "input";
        string siteKey = f[0].GetAttribute("data-sitekey");
        if (siteKey != null)
        {
          field.widget_type = "captcha";
          simple.url = siteKey.Replace("&amp;", "&"); //Bug 22708 Fluid renders liquid templates html encoded by default
        }

        // Handle aba validation special url
        if (f[0].HasAttribute("data-aba-url"))
        {
          simple.url = (f[0].GetAttribute("data-aba-url")).Replace("&amp;", "&"); //Bug 22708 Fluid renders liquid templates html encoded by default
        }

        // IPAY-350 UMN Handle field help (used to be prompt)
        if (f[0].HasAttribute("data-field-help"))
        {
          field.help = f[0].GetAttribute("data-field-help");
        }

        simple.hidden = f[0].GetAttribute("type") == "hidden" ? true : false;
        int i;
        if (Int32.TryParse(f[0].GetAttribute("maxlength"), out i))
          simple.max_length = i;
        if (Int32.TryParse(f[0].GetAttribute("data-min-length"), out i))
          simple.min_length = i;
        if (Int32.TryParse(f[0].GetAttribute("data-line-nbr"), out i))
          simple.line_number = i;
        simple.required = f[0].GetAttribute("data-required") == "true" ? true : false;

        // Bug 24292 UMN
        simple.read_only = f[0].GetAttribute("data-read-only") == "true" ? true : false;
      }
      else if (f[0].ClassName == "image")
      {
        field.widget_type = "image";
        simple.url = (f[0].GetAttribute("src")).Replace("&amp;", "&"); //Bug 22708 Fluid renders liquid templates html encoded by default
        int i;
        if (Int32.TryParse(f[0].GetAttribute("data-line-nbr"), out i))
          simple.line_number = i;
      }
      else if (f[0] is IHTMLSelectElement)
      {
        int i;
        field.widget_type = "dropdown";
        if (Int32.TryParse(f[0].GetAttribute("data-line-nbr"), out i))
          simple.line_number = i;
        // RDM BUG 23509 - add support for field value lists
        if (f[0].HasAttribute("multiple"))
          simple.attributes.Add(new KVPair("multiple", "multiple"));
        // parse the select's options
        var optNodes = f.Find("option");
        foreach (IHTMLOptionElement opt in optNodes)
        {
          var option = new KVPair(opt.Cq().First().Text(), opt.Value);
          simple.options.Add(option);
        }
      }
      else if (f[0].ClassName == "navbar")
      {
        int i;
        field.widget_type = "tab_menu";
        if (Int32.TryParse(f[0].GetAttribute("data-line-nbr"), out i))
          simple.line_number = i;
        var trSelector = f.Closest("tr").Prev().Find("span.k");
        if (trSelector.Length > 0) field.help = trSelector.Text().Trim();
        field.nav = ParseNavField(f);
      }

      // HACK: need graceful way to handle attributes from the template that have to be merged with the
      // attributes from the parse. For the moment, just jam this in here.
      string klass = f[0].ClassName;
      f[0].SetAttribute("class", klass);
      simple.id = f[0].GetAttribute("id");
      simple.name = f[0].Name;
      simple.field_type = f[0].GetAttribute("type");
      simple.autocomplete = "off";
      //Bug 26218 NAM: if select-multiple assign all selected options to value. DOM only assigns last selected option by default
      if (f[0].Type == "select-multiple")
      {
        //simple.value = "A/B/C";
        var optNodes = f.Find("option");
        foreach (IHTMLOptionElement opt in optNodes)
        {
          if (opt.GetAttribute("selected") == "true")
          {
            string sValue = opt.GetAttribute("value");
            if (!string.IsNullOrEmpty(simple.value) && !simple.value.Contains(sValue))
              sValue = simple.value + string.Format("/{0}", sValue);
            //assign new value
            simple.value = sValue;
          }
        }
      }
      else
      {
        // Bug 20642 UMN responsive TODO: parse/handle field_type
        simple.value = f[0].Value;
      }
      // RDM BUG 23509 - add support for radio and checkbox controls
      if (simple.field_type == "radio" || simple.field_type == "checkbox")
      {
        if (simple.name.Contains("_f_selected")) // RDM BUG 23509 - handle boolean field
        {
          var hiddenCheckbox = dom.Find("input[name='" + simple.name.Replace("_f_selected", "") + "']");
          field.widget_type = "boolean";
          simple.name = hiddenCheckbox[0].Name;
          simple.id = hiddenCheckbox[0].GetAttribute("id");
          simple.read_only = hiddenCheckbox[0].GetAttribute("data-read-only") == "true" ? true : false;
          simple.value = hiddenCheckbox[0].Value;
        }
        else // RDM BUG 23509 - handle field value lists
        {
          if (simple.field_type == "checkbox")
            simple.attributes.Add(new KVPair("multiple", "multiple"));

          var fvl = dom.Find("input[name='" + simple.name + "']");
          field.widget_type = "dropdown"; // all field value lists should be handled as a "dropdown"
          simple.value = ""; // Bug 24730 RDM/NAM - Field Value lists default values not supported in responsive UI, (supported in non-responsive)
          for (int i = 0; i < fvl.Length; i++)
          {
            var option = new KVPair(fvl[i].NextSibling.NodeValue, fvl[i].Value); // Bug 24729 RDM/NAM - Key = Checkbox text, Value = Checkbox Value
            simple.options.Add(option);
            //Bug 26218 NAM: if select-multiple assign all selected options to value. DOM only assigns last selected option by default
            foreach (IHTMLInputElement elem in fvl)
            {
              if (elem.GetAttribute("checked") == "true")
              {
                string sValue = elem.GetAttribute("value");
                if (!string.IsNullOrEmpty(simple.value) && !simple.value.Contains(sValue))
                  sValue = simple.value + string.Format("/{0}", sValue);
                //assign new value
                simple.value = sValue;
              }
            }
          }
        }
      }

      field.simple = simple;
      return field;
    }

    /// <summary>
    /// Add the MiraPay iframe field that will be output as raw html
    /// Bug 26537 UMN - Eigen MiraPay support
    /// </summary>
    /// <param name="tagname"></param>
    /// <param name="line_number"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    private static Field AddMiraPayFrameField(IDomObject f)
    {
      int line_number = 10; // just pick a number in case parsing fails
      if (int.TryParse(f.GetAttribute("data-line-nbr"), out int i))
        line_number = i;
      string url = f.GetAttribute("data-url").Replace("&amp;", "&");
      Field field = new Field
      {
        widget_type = "iframe_mirasecure",
        description = f.Id,
        help = f.GetAttribute("data-message")
      };

      SimpleField simple = new SimpleField
      {
        name = f.Id,
        id = f.Id,
        hidden = false,
        line_number = line_number,
        // Bug IPAY-1543 MJO - Utilize new encryption method (removed mkey)
        url = url
      };
      simple.value = f.OuterHTML;
      field.simple = simple;
      return field;
    }

    /// <summary>
    /// Add the MiraPay iframe validation field that will be used to enable/disable the Next button
    /// Bug 26537 UMN - Eigen MiraPay support
    /// </summary>
    /// <param name="tagname"></param>
    /// <param name="line_number"></param>
    /// <returns></returns>
    private static Field AddMiraPayValidationField(string tagname)
    {
      Field field = new Field
      {
        widget_type = "input",
        description = tagname
      };

      SimpleField simple = new SimpleField
      {
        name = "args." + tagname,
        id = tagname,
        hidden = true,
        validation_name = tagname
      };
      field.simple = simple;
      return field;
    }

    private static Nav ParseNavField(CQ f)
    {
      Nav ret = new Nav();
      ret.nav_items = ParseNavItems(f);
      return ret;
    }

    private static List<NavItem> ParseNavItems(CQ f)
    {
      var ret = new List<NavItem>();
      var a = new List<KVPair>();
      var linkSelector = f.Find("ul li a");
      foreach (var l in linkSelector)
      {
        var ni = new NavItem("menu_item", l.TextContent, l.GetAttribute("href"), a);
        ret.Add(ni);
      }

      return ret;
    }

    private static List<Button> ParseBodyButtons(CQ dom, string page_type)
    {
      var ret = new List<Button>();
      var a = new List<KVPair>();

      // process cancel
      var cancelButtonNode = dom["button[data-btn-url*='cancel.htm']"]; //Bug 23022 NAM: extract button URL from data-btn-url attribute
      if (cancelButtonNode.Length == 0)
        cancelButtonNode = dom["button[data-btn-url*='return_to_buying']"].First(); //Bug 23022 NAM: extract button URL from data-btn-url attribute

      // acknowledgement uses different words for cancel and next
      if (page_type.StartsWith("cbc.ack"))
      {
        //Bug 23022 NAM: retrieve button href from data-btn-url attribute
        ret.Add(new Button("button", "Decline", "cancel", cancelButtonNode.First().Attr("data-btn-url"), "GET", a));
      }
      else
      {
        //Bug 23232 NAM 20180419: display a cancel button if there's a least one cancel.htm or a return_to_buying.htm
        //NOTE: there could be more than one return_to_buying href on the page
        if (cancelButtonNode.Length > 0)
          //Bug 23022 NAM: retrieve button href from data-btn-url attribute
          ret.Add(new Button("button", "Cancel", "cancel", cancelButtonNode.First().Attr("data-btn-url"), "GET", a));
      }

      // process accept on acknowledgement page
      if (page_type.StartsWith("cbc.ack"))
      {
        // tender acknowledgement pages will have a URL of acknowledge_payment.htm but transaction ack pages will just use continue
        var nextButtonNode = dom["button[data-btn-url*='acknowledge_payment.htm'], button[data-btn-url*='continue.htm']"];  //Bug 23022 NAM: extract button URL from data-btn-url attribute
        //Bug 23022 NAM: retrieve button href from data-btn-url attribute
        ret.Add(new Button("button", "Accept", "next", nextButtonNode.First().Attr("data-btn-url"), "GET", a));
      }
      // Don't output Next for multimatch page or buying page
      else if (page_type != "cbc.multimatch" && page_type != "cbc.buying")
      {
        var nextButtonNode = dom["form.form_inst, #form_class"];
        ret.Add(new Button("button", "Next", "next", nextButtonNode.First().Attr("action"), "POST", a));
      }

      // process back button (and Bug 22376 UMN add custom template support)
      if (page_type.StartsWith("cbc.zeromatch") || page_type.StartsWith("cbc.singlematch") || page_type.StartsWith("cbc.multimatch"))
      {
        var backButtonNode = dom["button[data-btn-url*='back.htm']"]; //Bug 23022 NAM: extract button URL from data-btn-url attribute
        if (backButtonNode.Length > 0)
          //Bug 23022 NAM: retrieve button href from data-btn-url attribute
          ret.Add(new Button("button", "Back", "back", backButtonNode.First().Attr("data-btn-url"), "GET", a));
      }

      return ret;
    }

    static readonly Regex caslLinkPat = new Regex(@"casl_link\(([^\)]*)\)", RegexOptions.Compiled);

    private static void ParseCartRows(Page page, CQ dom, string browser)
    {
      var a = new List<KVPair>();
      page.cart = new Cart();

      page.cart.description = "Bill Summary";
      page.cart.total.description = dom["td.total_descr"].Text().Trim();
      page.cart.total.amount = dom["td.total_amount"].First().Attr<String>("data-total-amount");
      page.cart.fee.description = dom["span.fee_descr"].Text().Trim();
      page.cart.fee.amount = dom["span.fee_amount"].Text().Trim();
      page.cart.grand_total.description = dom["span.gt_descr"].Text().Trim();
      page.cart.grand_total.amount = dom["span.gt_amount"].Text().Trim();
      //Bug 26626 NAM: add event total, the grand total changes based on the tender. The event total is always the same
      page.cart.event_total.description = dom["div.grand_total"].Find("span.key").Text().Trim();
      page.cart.event_total.amount = dom["div.grand_total"].Find("span.value").Text().Trim();
      //Bug 26727 NAM: add fsa_total, the total and fsa_total can be different
      page.cart.fsa_total.description = dom["div.fsa_grand_total"].Find("span.key").Text().Trim();
      page.cart.fsa_total.amount = dom["div.fsa_grand_total"].Find("span.value").Text().Trim();

      // Moved the Make Another Payment and Pay buttons to the Cart
      var shopMoreNode = dom["#pcea_shop_more"];
      if (shopMoreNode.Length > 0)
      {
        //Bug 23022 NAM: retrieve button href from data-btn-url attribute
        page.cart.buttons.Add(new Button("button", shopMoreNode.First().Text(), "cancel", shopMoreNode.First().Attr("data-btn-url"), "GET", a));
      }
      var payNode = dom["#pcea_pay"];
      if (payNode.Length > 0)
      {
        //Bug 23022 NAM: retrieve button href from data-btn-url attribute
        page.cart.buttons.Add(new Button("button", payNode.First().Text(), "pay", payNode.First().Attr("data-btn-url"), "GET", a));
      }

      if (String.IsNullOrWhiteSpace(page.cart.grand_total.amount))
      {
        page.cart.grand_total.description = page.cart.total.description;
        page.cart.grand_total.amount = page.cart.total.amount;
      }

      int nestedCount = 0;

      var tranTable = dom["#transactions_TABLE"];
      var trNodes = tranTable.Find("tr.parent-ci");
      foreach (var tr in trNodes)
      {
        TransactionRow cart_row = new TransactionRow();
        int row_id = 0;
        CQ trSel = tr.Cq();

        string idAttr = tr.GetAttribute("id"); // will be r0_row, r1_row etc
        string[] idParts = idAttr.Split('_');
        string idPrefix = idParts[0]; // will be r0, r1 etc

        cart_row.description = trSel.Find("td.cart_description").Text().Trim();
        cart_row.tax_type = trSel.Find("td.cart_taxtype").Text().Trim();
        cart_row.total = trSel.Find("td.cart_total").Text().Trim();
        cart_row.delete.widget_type = "button";
        var trashOnclick = trSel.Find("td.cart_trash")
          .Find("button[data-btn-url*='a_process_core_event_app/delete_core_item']")  //Bug 23022 NAM: retrieve href from button data-btn-url attribute
          .First().Attr("onclick");

        // Bug 19730 UMN
        if (!String.IsNullOrEmpty(trashOnclick))
        {
          // URL is embedded in casl_link call
          MatchCollection matches = caslLinkPat.Matches(trashOnclick);
          if (matches.Count > 0)
          {
            Match match = matches[0];
            GroupCollection groups = match.Groups;
            // Bug 23022 MJO - Remove \x22 encoded quote
            cart_row.delete.url = (groups[1].Value.Replace("\"", "")).Replace("&amp;", "&").Replace("\\x22", ""); //Bug 22708 Fluid renders liquid templates html encoded by default
            cart_row.delete.method = "GET";
          }
        }
        Int32.TryParse(trSel.Attr("data-row-id"), out row_id);
        cart_row.row_id = row_id;

        // Bug 20642 UMN responsive TODO get any details that exist at top-level??

        cart_row.nested_transaction_rows = new List<NestedTransactionRow>();

        // these are all the nested-ci rows, so do need to check the prefix
        var nestedTrNodes = tranTable.Find("tr.nested-ci");
        foreach (var nestedTr in nestedTrNodes)
        {
          NestedTransactionRow nested_row = new NestedTransactionRow();
          CQ nestedTrSel = nestedTr.Cq();

          string nestedIdAttr = nestedTr.GetAttribute("id"); // will be r0_nested, r1_nested etc
          if (!nestedIdAttr.Contains(idPrefix)) continue;

          nested_row.description = nestedTrSel.Find(".description").Text().Trim();
          nested_row.tax_type = nestedTrSel.Find(".taxtype").Text().Trim();
          nested_row.total = nestedTrSel.Find(".total").Text().Trim();

          // only one nested details tr
          var nestedDetailTr = nestedTrSel.Next("tr.nested-ci-details");
          var detailRows = nestedDetailTr.Find(".table_row");
          foreach (var cell in detailRows)
          {
            CQ cellSel = cell.Cq();
            KVPair detail = new KVPair(cellSel.Find(".key").Text().Trim(), cellSel.Find(".value").Text().Trim()); //Bug 23623 NAM
            nested_row.details.Add(detail);
          }
          cart_row.nested_transaction_rows.Add(nested_row);
        }
        //IPAY-459 NAM: add transaction custom fields
        string sIndex = trSel.Attr("Id").ToString();
        if (!string.IsNullOrEmpty(sIndex))
        {
          //the index will be rx_row, looking for items of type rx_details
          string sDetailKey = sIndex.Substring(0, sIndex.IndexOf("_")) + "_details";
          var detailTrNodes = tranTable.Find("[class='" + sDetailKey + "']");
          if (detailTrNodes.Length > 0)
          {
            NestedTransactionRow nested_row = new NestedTransactionRow();
            var detailRows = detailTrNodes.Find(".table_row");
            foreach (var cell in detailRows)
            {
              CQ cellSel = cell.Cq();
              string sKey = cell.Cq().Find("div.key").Text().Trim();
              if (string.IsNullOrEmpty(sKey) || sKey.Trim().Equals(":"))
                continue;
              else
              {
                KVPair detail = new KVPair(cellSel.Find(".key").Text().Trim(), cellSel.Find(".value").Text().Trim());
                nested_row.details.Add(detail);
              }
            }
            cart_row.nested_transaction_rows.Add(nested_row);
          }
        }
        nestedCount += Math.Max(0, cart_row.nested_transaction_rows.Count - 1); // -1 for CTT
        page.cart.transaction_rows.Add(cart_row);
      }
      page.cart.count = page.cart.transaction_rows.Count + nestedCount;
      //Bug 23363 NAM: display tax transactions in shopping cart
      ParseTaxTransactions(page, dom);
    }

    private static void ParseReceiptCartRows(Page page, CQ dom, string browser)
    {
      page.cart = new Cart();

      page.cart.description = "Bill Summary";

      // <div class="grand_total">
      //   <span class="key">Total Amount:</span><span class="value">$369.58</span>
      // </div>
      //Bug 22999 NAM 20180417: Display the "Total" amount only
      page.cart.grand_total.description = dom["div.grand_total span.key"].Last().Text().Trim();
      page.cart.grand_total.amount = dom["div.grand_total span.value"].Last().Text().Trim();

      // Bug 20642 UMN responsive TODO parse cart fee info??
      // page.cart.total.description = dom["td.total_descr"].Text().Trim();
      // page.cart.total.amount = dom["td.total_amount"].First().Attr<String>("data-total-amount");
      // page.cart.fee.description = dom["span.fee_descr"].Text().Trim();
      // page.cart.fee.amount = dom["span.fee_amount"].Text().Trim();

      if (String.IsNullOrWhiteSpace(page.cart.total.amount))
      {
        page.cart.total.description = page.cart.grand_total.description;
        page.cart.total.amount = page.cart.grand_total.amount;
      }

      ParseReceiptCartTransactions(page, dom);
      ParseReceiptCartTenders(page, dom);
    }

    private static void ParseReceiptCartTransactions(Page page, CQ dom)
    {
      var tranTable = dom[".vector_fields"];
      //Bug 26727 NAM: the Transactionr index may contain gaps, switching to parten matching
      //Bug 23447 NAM: iterate through all transactions while completed items are found
      int nCount = 0, nested_row_id = 0;
      var trNodes = tranTable.Find("tr[class^='i Transaction']");  //using wildcard on class name, equivalent to starts with
      nCount = trNodes.Length;
      if (nCount > 0)
      {
        int row_id = 0;
        TransactionRow cart_row = null;
        foreach (var tr in trNodes)
        {
          CQ trSel = tr.Cq();
          // process transactions
          if (!trSel.HasClass("core_items"))
          {
            cart_row = new TransactionRow();
            cart_row.description = trSel.Find("td.description").Text().Trim();
            cart_row.tax_type = trSel.Find("td.taxtype").Text().Trim();
            cart_row.total = trSel.Find("td.total").Text().Trim();
            cart_row.row_id = row_id++;
            page.cart.transaction_rows.Add(cart_row);
            nested_row_id = 0;
            cart_row.nested_transaction_rows = new List<NestedTransactionRow>();
            // process any receipt field details
            trSel = trSel.Next();
            var trDetails = trSel.Find("div.details.table");
            if (trDetails.Length > 0)
            {
              while (trDetails.Children().HasClass("table_row"))
              {
                //Bug 27538 NAM: a detail row may contain multiple details like custom/receipt fields
                {
                  var trDetail = trDetails.Find("div.table_row");
                  foreach (var cell in trDetail)
                  {
                    string sKey = cell.Cq().Find("div.key").Text().Trim();
                    if (sKey.Trim().Equals(":"))
                      continue;
                    else
                    {
                      KVPair detail = new KVPair(cell.Cq().Find(".key").Text().Trim(), cell.Cq().Find(".value").Text().Trim()); //Bug 23623 NAM
                      cart_row.details.Add(detail);
                    }
                  }
                  trDetails = trDetails.Next();
                }
              }
            }
          }
          // process transaction rows
          if (trSel.HasClass("core_items"))
          {
            NestedTransactionRow nested_row = new NestedTransactionRow();
            nested_row.row_id = nested_row_id++;
            nested_row.description = trSel.Find("td.description").Text().Trim();
            nested_row.tax_type = trSel.Find("td.taxtype").Text().Trim();
            nested_row.total = trSel.Find("td.total").Text().Trim();
            // process row detail
            var nestedDetailTr = trSel.Next("tr.r0_nested_details");
            var detailRows = nestedDetailTr.Find(".table_row");
            foreach (var cell in detailRows)
            {
              CQ cellSel = cell.Cq();
              KVPair detail = new KVPair(cellSel.Find(".key").Text().Trim(), cellSel.Find(".value").Text().Trim()); //Bug 23623 NAM
              nested_row.details.Add(detail);
            }
            cart_row.nested_transaction_rows.Add(nested_row);
          }
        }
      }
    }

    private static void ParseReceiptCartTenders(Page page, CQ dom)
    {
      var tranTable = dom[".vector_fields"];

      // Only one tender for Business Center
      int row_id = 0;
      var trNodes = tranTable.Find(".Tenderr0.core_item_completed");
      foreach (var tr in trNodes)
      {
        TenderOrTaxRow cart_row = new TenderOrTaxRow();
        CQ trSel = tr.Cq();

        cart_row.description = trSel.Find("td.description").Text().Trim();
        cart_row.total = trSel.Find("td.total").Text().Trim();
        cart_row.row_id = row_id++;

        // only one details tr
        var nestedDetailTr = trSel.Next().Next();           //Bug 27557 NAM: trSel @ core_item_completed level. Navigate down twice to get tender details
        var detailRows = nestedDetailTr.Find(".table_row");
        foreach (var cell in detailRows)
        {
          KVPair detail = new KVPair(cell.Cq().Find(".key").Text().Trim(), cell.Cq().Find(".value").Text().Trim()); //Bug 23623 NAM
          cart_row.details.Add(detail);
        }
        page.cart.tender_rows.Add(cart_row);
      }
    }

    private static void ParseAckCartRows(Page page, CQ dom, string browser)
    {
      page.cart = new Cart();

      page.cart.description = "Payment Summary";

      // <table class="confirm_table ack-transactions"
      //   data-grand-total="$10.50"
      //   data-grand-total-desc="Grand Total">
      var nodeSel = dom[".ack-transactions"];
      if (nodeSel.Length > 0)
      {
        page.cart.grand_total.description = nodeSel.Attr<String>("data-grand-total-desc");
        page.cart.grand_total.amount = nodeSel.Attr<String>("data-grand-total");
      }

      // get fees from ack-tenders table
      // <td class="confirm_msg fee-total">
      //    Credit Card Service Fee: </td>
      // <td class="confirm_msg fee-total">
      //    $0.50 </td>
      nodeSel = dom[".ack-tenders"];
      var feeNodes = nodeSel.Find(".fee-total");
      if (feeNodes.Length > 0)
      {
        page.cart.fee.description = feeNodes.First().Text().Trim();
        page.cart.fee.amount = feeNodes.First().Next().Text().Trim();
      }

      ParseAckCartTransactions(page, dom);
      ParseAckTaxTransactions(page, dom);
      ParseAckCartTenders(page, dom);
    }

    private static void ParseAckCartTransactions(Page page, CQ dom)
    {
      var tranTable = dom[".ack-transactions"];
      int row_id = 0;

      var trNodes = tranTable.Find("tr.ack_transaction");
      foreach (var tr in trNodes)
      {
        TransactionRow cart_row = new TransactionRow();
        CQ trSel = tr.Cq();
        cart_row.description = trSel.Find("td.description").Text().Trim();
        cart_row.total = trSel.Find("td.total").Text().Trim();
        cart_row.row_id = row_id++;

        // process any receipt field details
        trSel = trSel.Next();
        while (trSel.HasClass("table_row"))
        {
          //Bug 24088 NAM: extract acknowledgement page transaction custom fields. Exclude last row where keys is ":"
          string sKey = trSel.Find("td.key").Text().Trim();
          if (sKey.Trim().Equals(":"))
          {
            trSel = trSel.Next();   //Bug 27556 NAM: continue looping when key is missing
            continue;
          }
          else
          {
            KVPair detail = new KVPair(sKey, trSel.Find("td.value").Text().Trim());   //Bug 23623 NAM
            cart_row.details.Add(detail);
            trSel = trSel.Next();
          }
        }

        int nested_row_id = 0;
        if (trSel.HasClass("core_items"))
        {
          cart_row.nested_transaction_rows = new List<NestedTransactionRow>();
        }
        while (trSel.HasClass("core_items"))
        {
          // process any BTTs
          NestedTransactionRow nested_row = new NestedTransactionRow();
          nested_row.row_id = nested_row_id++;
          nested_row.description = trSel.Find("td.description").Text().Trim();
          nested_row.total = trSel.Find("td.total").Text().Trim();
          //Bug 26727 NAM: add transaction detail
          var tranDetail = trSel.Next();
          if (!tranDetail.HasClass("core_items"))  //skip empty TR
            tranDetail = tranDetail.Next();
          while (tranDetail.HasClass("table_row"))
          {
            KVPair detail = new KVPair(tranDetail.Find("td.key").Text().Trim(), tranDetail.Find("td.value").Text().Trim());   //Bug 23623 NAM
            nested_row.details.Add(detail);
            tranDetail = tranDetail.Next();
          }
          cart_row.nested_transaction_rows.Add(nested_row);
          trSel = tranDetail;
          if (!trSel.HasClass("core_items"))  //skip empty TR
            trSel = trSel.Next();
        }
        page.cart.transaction_rows.Add(cart_row);
      }
    }

    private static void ParseAckTaxTransactions(Page page, CQ dom)
    {
      var tranTable = dom[".ack-taxes"];
      int row_id = 0;
      var trNodes = tranTable.Find("tr.ack_tax_transaction");
      foreach (var tr in trNodes)
      {
        TenderOrTaxRow cart_row = new TenderOrTaxRow();
        CQ trSel = tr.Cq();
        cart_row.description = trSel.Find("td.description").Text().Trim();
        cart_row.total = trSel.Find("td.total").Text().Trim();
        cart_row.row_id = row_id++;
        page.cart.tax_rows.Add(cart_row);
      }
    }

    private static void ParseAckCartTenders(Page page, CQ dom)
    {
      // Only one tender for Business Center
      var tenderTable = dom[".ack-tenders"];

      int row_id = 0;
      var trNodes = tenderTable.Find("tr");
      TenderOrTaxRow cart_row = new TenderOrTaxRow();

      // skip first row, it just has the tender details description
      CQ trSel = trNodes.First().Next();

      ParseAckCartTender(page, row_id, cart_row, trSel, "td.confirm_msg");

      // and optional fee
      TenderOrTaxRow fee_row = new TenderOrTaxRow();
      trSel = trNodes.Find("td.fee-total").Parent();
      if (trSel.Length > 0)   //Bug 22512 NAM: process only if element exists
        ParseAckCartTender(page, ++row_id, fee_row, trSel, "td.fee-total");
    }

    private static void ParseAckCartTender(Page page, int row_id, TenderOrTaxRow cart_row, CQ trSel, String selector)
    {
      var confirmSel = trSel.Find(selector);  // will be two of them
      cart_row.description = confirmSel.First().Text().Trim();
      cart_row.total = confirmSel.First().Next().Text().Trim();

      // now get ref_number, which will be a masked bank_acct_nbr, masked card number or empty
      // for cash
      trSel = trSel.Next();
      if (trSel.Length > 0)
      {
        cart_row.ref_number = trSel.Find("td.confirm_msg").Text();
      }
      cart_row.row_id = row_id;

      trSel = trSel.Parent().Find("tr.table_row");    //Bug 22512 NAM: find all 'tr.table_row' elements 
      if (trSel.Length > 0)
      {
        // only one details tr?
        var detailRows = trSel.Find("td.key");
        foreach (var cell in detailRows)
        {
          var tdSel = cell.Cq().First();
          KVPair detail = new KVPair(tdSel.Text().Trim(), tdSel.Next().Text().Trim());  //Bug 23623 NAM
          cart_row.details.Add(detail);
        }
      }
      page.cart.tender_rows.Add(cart_row);  //Bug 22512 NAM: allways add row even when there is no tender detail 
    }
    //Bug 23363 NAM: add tax transactions to shopping cart
    private static void ParseTaxTransactions(Page page, CQ dom)
    {
      var taxTable = dom[".transactiontaxes"];
      int row_id = 0;
      var trNodes = taxTable.Find("tr.core_item_finished ");
      foreach (var tr in trNodes)
      {
        TenderOrTaxRow cart_row = new TenderOrTaxRow();
        CQ trSel = tr.Cq();
        cart_row.description = trSel.Find("td.description").Text().Trim();
        cart_row.total = trSel.Find("td.total").Text().Trim();
        cart_row.row_id = row_id++;
        page.cart.tax_rows.Add(cart_row);
      }
    }

    private static void ParseThemeColorsJson(Page page, CQ dom)
    {
      page.themeColorsJson = dom["div[data-theme-colors-json]"].First().Attr("data-theme-colors-json");
    }

    private static void ParseHeader(Page page, CQ dom)
    {
      page.header.title = dom["title"].First().Text().Trim();
      page.header.icon = dom["div[data-dept-icon],table[data-dept-icon]"].First().Attr("data-dept-icon"); ;

      // Bug 19065 UMN Support frameless_ui
      var frameless = dom["div[data-frameless-ui],table[data-frameless-ui]"].First().Attr("data-frameless-ui");
      page.header.frameless_ui = (frameless == "true") ? true : false;

      page.header.description = dom["div[data-site-name]"].First().Attr("data-site-name");

      page.header.exit = ParseExit(page, dom);    //Bug 26846 NAM:   

      // Bug 20642 UMN responsive TODO? Support configurable css and js links that get added to the page

      //page.header.css_links = ParseListStrings(dom, "link[rel='stylesheet']");
      //page.header.js_links = ParseListStrings(dom, "script");

      page.header.nav = ParseHeaderNav(dom);

      //Bug 23080 NAM: read department session timeout from Config and add to page header
      int nTimeOut = 0;
      string sDepartmentID = dom["div[data-dept-id]"].First().Attr("data-dept-id");
      if (string.IsNullOrEmpty(sDepartmentID))  //no department ID assigned - use default timeout
      {
        nTimeOut = (int)((GenericObject)c_CASL.c_object("Business.Misc")).get("session_timeout_minutes", 14);
      }
      else
      {
        string sDepartment = "Business.Department.of.'" + sDepartmentID + "'";
        nTimeOut = (int)((GenericObject)c_CASL.c_object(sDepartment)).get("session_timeout_minutes", 0);
        if (nTimeOut == 0)  //Department doesn't have a timeout configured - use default timeout
        {
          nTimeOut = (int)((GenericObject)c_CASL.c_object("Business.Misc")).get("session_timeout_minutes", 14);
        }
      }
      page.header.session_timeout = nTimeOut * 60000;
    }

    private static List<Nav> ParseHeaderNav(CQ dom)
    {
      var ret = new List<Nav>();
      var a = new List<KVPair>();
      var n = ParsePageNavItems(dom, "data-header-nav");
      ret.Add(new Nav("dropdown_menu", "", "Top Nav", a, n));
      return ret;
    }

    private static void ParseFooter(Page page, CQ dom)
    {
      // a11y added a label, so to avoid seeing "Powered by..Swipe", get contents and then text node
      page.footer.description = dom["div.footer div"].Contents().Get(0).NodeValue;

      // IPAY-1077 UMN the logo went to SVG in base, so following won't be found except for old configs
      var footerImage = dom["img.core_logo"];
      page.footer.image = dom["img.core_logo"].First().Attr("src");
      if (footerImage.Length > 0)
      {
        page.footer.image = footerImage.First().Attr("src");
      }
      else
      {
        page.footer.image = c_CASL.GEO.get("baseline_static") + "/business/icons/core_logos/Mark/Full Color/core_logo_FINAL_mark_full-color-20x18.png";
      }
      page.footer.nav = ParseFooterNav(dom);
    }

    private static List<Nav> ParseFooterNav(CQ dom)
    {
      var ret = new List<Nav>();
      var a = new List<KVPair>();
      var n = ParsePageNavItems(dom, "data-footer-nav");
      ret.Add(new Nav("dropdown_menu", "", "Footer Nav", a, n));
      return ret;
    }


    private static List<NavItem> ParsePageNavItems(CQ dom, string navSelector)
    {
      var navNode = dom[String.Format("div[{0}]", navSelector)];
      var ret = new List<NavItem>();
      if (navNode.Length > 0)
      {
        // the attribute is base64 encoded JSON of string pairs (description, and URL)
        string bytesString = navNode.First().Attr(navSelector);
        if (!String.IsNullOrWhiteSpace(bytesString))
        {
          var navItemsJSON = System.Text.Encoding.Default.GetString(Convert.FromBase64String(bytesString)).Replace("\"", "'");
          List<NavItem> navItems = JsonConvert.DeserializeObject<List<NavItem>>(navItemsJSON);
          var a = new List<KVPair>();  // sharing OK, because we won't be modifying the attributes
          foreach (var item in navItems)
          {
            ret.Add(new NavItem("menu_item", item.description, item.url, a));
          }
        }
      }
      return ret;
    }

    private static Dialog ParseExit(Page page, CQ dom)    //Bug 26846 NAM:
    {
      // Bug 20642 UMN Responsive TODO: make exit dialog configurable not hardcoded
      Dialog ret = new Dialog();
      ret.widget_type = "dialog";
      //Bug 26846 NAM: assign dialog exit button message based on page
      if (page.page_type.Equals("cbc.receipted"))
        ret.description = "Are you sure you would like to exit?";
      else
        ret.description = "Are you sure you would like to exit? Any unfinished payments will be lost.";
      ret.buttons = ParseExitButtons(dom);
      return ret;
    }

    // Bug 20642 UMN Responsive TODO: make sure the unused cancel/exit buttons configurable not hardcoded
    private static List<Button> ParseExitButtons(CQ dom)
    {
      List<Button> ret = new List<Button>();
      var a = new List<KVPair>();
      string site_logical = c_CASL.GEO.get("site_logical") as string;
      ret.Add(new Button("button", "No", "cancel",
        site_logical + @"/my/0/get_app.htm?&amp;", "GET", a));
      //Bug 23080 NAM: parse logout URL from dom 
      string sLogoutBtn = dom["div[data-logout-button]"].First().Attr("data-logout-button");
      sLogoutBtn = HttpUtility.UrlDecode(sLogoutBtn);
      ret.Add(new Button("button", "Yes", "exit", sLogoutBtn, "GET", a));
      return ret;
    }

    /// <summary>
    /// Grab all the script tags
    /// </summary>
    /// <param name="dom"></param>
    private static List<string> ParseListStrings(CQ dom, string selector)
    {
      var ret = new List<string>();
      var nodes = dom["script"];
      foreach (var str in nodes)
      {
        ret.Add(str.OuterHTML);
      }
      return ret;
    }

    /// <summary>
    /// Convert the attributes for an HTML tag back to a string for render by the template
    /// </summary>
    private static List<KVPair> MakeDict(IAttributeCollection attributes)
    {
      List<KVPair> list = new List<KVPair>();

      foreach (var attr in attributes)
      {
        string sKey = attr.Key;
        string sValue = attr.Value;

        // don't pass our non HTML5-compliant attributes
        switch (sKey)
        {
          case "obj":
            continue;
          case "field_info":
            continue;
          case "ui":
            continue;
        }
        if (sKey == "selected"
            || sKey == "required"
            || sKey == "disabled"
            || sKey == "readonly"
            || sKey == "hidden"
            || sKey == "sortable")
          list.Add(new KVPair(sKey, sKey));
        else if (sValue.Length > 0 || (sKey == "value" && sValue.Length == 0))
          list.Add(new KVPair(sKey, sValue));
        else
          list.Add(new KVPair(sKey, sKey));
      }
      return list;
    }

    /* 
     * Bug 20642 UMN responsive UI for CBC
     * 
     * CBC Types of pages:
     * ====================
     * 1. Buying Page (the one with the transaction menus)
     * 2. Any modal page (includes inquiry and payment page!) -- We will need to differentiate them because of PB!
     * 3. Any Tender page
     * 4. Acknowledgement Page
     * 5. Receipt Page
     * 
     * So create classes for these types of pages. Instance of a class will be created from the parsing of the
     * desktop HTML. The instance of the page class will then be used to render a template which produces the
     * final responsive UI. Need to make these classes visible to templating engine. For now, derive from dotliquid
     * Drop class. Need to specifically indicate which bits are to be output as JSON, otherwise Newtonsoft will
     * output even the internal Drop class members.
     * 
     * Note: Eventually tell liquid to use Template.NamingConvention = new CSharpNamingConvention();, but for
     * now make the class and field names like Ruby.
     */

    /************ Refactor all this shite. Make constructors that take the CQ Dom and do their own parsing! ***************/
    /************ Refactor all this shite. Make constructors that take the CQ Dom and do their own parsing! ***************/
    /************ Refactor all this shite. Make constructors that take the CQ Dom and do their own parsing! ***************/

    [JsonObject(MemberSerialization.OptIn)]
    public class Field //Bug 22708 NAM
    {
      [JsonProperty]
      public string widget_type { get; set; } // input, select, label, etc

      [JsonProperty]
      public string help { get; set; }

      [JsonProperty]
      public string description { get; set; }  // the value visible to user

      [JsonProperty]
      public SimpleField simple { get; set; }  // simple one-d field (classic custom field)

      [JsonProperty]
      public Grid grid { get; set; }  // two-d field (PB)

      [JsonProperty]
      public Nav nav { get; set; }  // nav links

      public Field()
      {
        widget_type = help = description = "";
        simple = new SimpleField();
        grid = new Grid();
        nav = new Nav();
      }

      public Field(string w, string h, string d)
      {
        widget_type = w;
        help = h;
        description = d;
        simple = new SimpleField();
        grid = new Grid();
        nav = new Nav();
      }

      public override string ToString()
      {
        return JsonConvert.SerializeObject(this);
      }
    }

    /// <summary>
    /// iPayment custom or standard field
    /// </summary>
    /// <summary>
    /// iPayment custom or standard field
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class SimpleField //Bug 22708 NAM
    {

      [JsonProperty]
      public string name { get; set; }

      [JsonProperty]
      public string validation_name { get; set; }

      [JsonProperty]
      public string id { get; set; }

      [JsonProperty]
      public string value { get; set; }  // the value visible to user

      [JsonProperty]
      public int line_number { get; set; }

      [JsonProperty]
      public string field_type { get; set; }

      [JsonProperty]
      public int min_length { get; set; }

      [JsonProperty]
      public int max_length { get; set; }

      [JsonProperty]
      public bool hidden { get; set; }

      [JsonProperty]
      public bool required { get; set; }

      [JsonProperty]
      public bool read_only { get; set; }

      [JsonProperty]
      public bool masked { get; set; }

      [JsonProperty]
      public string autocomplete { get; set; }

      [JsonProperty]
      public string url { get; set; }

      [JsonProperty]
      public List<KVPair> attributes { get; set; }

      [JsonProperty]
      public List<KVPair> options { get; set; }  // for select

      public SimpleField()
      {
        name = validation_name = id = "";
        value = field_type = url = "";
        line_number = min_length = max_length = 0;
        hidden = required = masked = read_only = false;
        autocomplete = "on";
        attributes = new List<KVPair>();
        options = new List<KVPair>();
      }

      public override string ToString()
      {
        return JsonConvert.SerializeObject(this);
      }
    }

    /// <summary>
    /// iPayment Transaction or iPayment Tender
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class TenderOrTrans //Bug 22708 NAM
    {
      [JsonProperty]
      public string help { get; set; }

      [JsonProperty]
      public string description { get; set; }

      [JsonProperty]
      public string id { get; set; }  // 0, 1, 2, 3...

      [JsonProperty]
      public string ttid { get; set; } // eg. Business.Transaction.of.webParkingPayment_CTT

      [JsonProperty]
      public string url { get; set; } // from the action attribute

      [JsonProperty]
      public string dsv { get; set; } // the __DOUBLE_SUBMIT__ value

      [JsonProperty]
      public string icon_path { get; set; } // the __DOUBLE_SUBMIT__ value

      [JsonProperty]
      public List<Field> fields { get; set; }  // for select

      public TenderOrTrans()
      {
        help = description = "";
        id = ttid = url = dsv = "";
        fields = new List<Field>();
      }
    }

    /// <summary>
    /// iPayment menu (tabs_app)
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class Menu //Bug 22708 NAM
    {
      [JsonProperty]
      public string help { get; set; }

      [JsonProperty]
      public string id { get; set; }

      [JsonProperty]
      public string description { get; set; }

      [JsonProperty]
      public string url { get; set; }

      public Menu()
      {
        help = id = description = url = "";
      }
    }

    //Bug 23623 NAM - removed class KVPair

    [JsonObject(MemberSerialization.OptIn)]
    public class Amount //Bug 22708 NAM
    {

      [JsonProperty]
      public string amount { get; set; }

      [JsonProperty]
      public string description { get; set; }

      public Amount()
      {
        amount = description = "";
      }

      public override string ToString()
      {
        return JsonConvert.SerializeObject(this);
      }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class NestedTransactionRow //Bug 22708 NAM
    {

      [JsonProperty]
      public int row_id { get; set; }

      [JsonProperty]
      public string description { get; set; }

      [JsonProperty]
      public string tax_type { get; set; }

      [JsonProperty]
      public string total { get; set; }

      [JsonProperty]
      public List<KVPair> details { get; set; }

      public NestedTransactionRow()
      {
        row_id = 0;
        description = tax_type = total = "";
        details = new List<KVPair>();
      }

      public override string ToString()
      {
        return JsonConvert.SerializeObject(this);
      }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class TransactionRow //Bug 22708 NAM
    {

      [JsonProperty]
      public int row_id { get; set; }

      [JsonProperty]
      public string description { get; set; }

      [JsonProperty]
      public string tax_type { get; set; }

      [JsonProperty]
      public string total { get; set; }

      [JsonProperty]
      public string ref_number { get; set; }

      [JsonProperty]
      public Button delete { get; set; }

      [JsonProperty]
      public List<KVPair> details { get; set; }

      [JsonProperty]
      public List<NestedTransactionRow> nested_transaction_rows { get; set; }

      public TransactionRow()
      {
        row_id = 0;
        description = tax_type = total = ref_number = "";
        delete = new Button();
        details = new List<KVPair>();
        nested_transaction_rows = new List<NestedTransactionRow>();
      }

      public override string ToString()
      {
        return JsonConvert.SerializeObject(this);
      }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class TenderOrTaxRow //Bug 22708 NAM
    {

      [JsonProperty]
      public int row_id { get; set; }

      [JsonProperty]
      public string description { get; set; }

      [JsonProperty]
      public string total { get; set; }

      [JsonProperty]
      public string ref_number { get; set; }

      [JsonProperty]
      public List<KVPair> details { get; set; }

      public TenderOrTaxRow()
      {
        row_id = 0;
        description = total = ref_number = "";
        details = new List<KVPair>();
      }

      public override string ToString()
      {
        return JsonConvert.SerializeObject(this);
      }
    }

    /// <summary>
    /// The Shopping Cart
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class Cart //Bug 22708 NAM
    {

      [JsonProperty]
      public string description { get; set; }

      [JsonProperty]
      public int count { get; set; }
      [JsonProperty]
      public Amount event_total { get; set; }   //Bug 26626 NAM:
      [JsonProperty]
      public Amount fsa_total { get; set; }     //Bug 26727 NAM:
      [JsonProperty]
      public Amount grand_total { get; set; }

      [JsonProperty]
      public Amount total { get; set; }

      [JsonProperty]
      public Amount fee { get; set; }

      [JsonProperty]
      public List<TransactionRow> transaction_rows { get; set; }

      [JsonProperty]
      public List<TenderOrTaxRow> tax_rows { get; set; }

      [JsonProperty]
      public List<TenderOrTaxRow> tender_rows { get; set; }

      [JsonProperty]
      public List<Button> buttons { get; set; }

      public Cart()
      {
        description = "Bill Summary";
        count = 0;
        event_total = new Amount();   //Bug 26626 NAM:
        fsa_total = new Amount();     //Bug 26727 NAM:
        grand_total = new Amount();
        total = new Amount();
        fee = new Amount();
        transaction_rows = new List<TransactionRow>();
        tax_rows = new List<TenderOrTaxRow>();
        tender_rows = new List<TenderOrTaxRow>();
        buttons = new List<Button>();
      }

      public override string ToString()
      {
        return JsonConvert.SerializeObject(this);
      }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class Dialog //Bug 22708 NAM
    {

      [JsonProperty]
      public string widget_type { get; set; }

      [JsonProperty]
      public string help { get; set; }

      [JsonProperty]
      public string description { get; set; }

      [JsonProperty]
      public List<KVPair> attributes { get; set; }

      [JsonProperty]
      public List<Button> buttons { get; set; }

      public Dialog()
      {
        widget_type = help = description = "";
        attributes = new List<KVPair>();
        buttons = new List<Button>();
      }

      public override string ToString()
      {
        return JsonConvert.SerializeObject(this);
      }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class NavItem //Bug 22708 NAM
    {
      [JsonProperty]
      public string widget_type { get; set; }

      [JsonProperty]
      public string description { get; set; }

      [JsonProperty]
      public string url { get; set; }

      [JsonProperty]
      public List<KVPair> attributes { get; set; }

      public NavItem()
      {
        widget_type = url = description = "";
        attributes = new List<KVPair>();
      }

      public NavItem(string w, string d, string u, List<KVPair> a)
      {
        widget_type = w;
        description = d;
        url = u;
        attributes = a;
      }

      public override string ToString()
      {
        return JsonConvert.SerializeObject(this);
      }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class Nav //Bug 22708 NAM
    {

      [JsonProperty]
      public string widget_type { get; set; }

      [JsonProperty]
      public string help { get; set; }

      [JsonProperty]
      public string description { get; set; }

      [JsonProperty]
      public List<KVPair> attributes { get; set; }

      [JsonProperty]
      public List<NavItem> nav_items { get; set; }

      public Nav()
      {
        widget_type = help = description = "";
        attributes = new List<KVPair>();
        nav_items = new List<NavItem>();
      }

      public Nav(string w, string h, string d, List<KVPair> a, List<NavItem> n)
      {
        widget_type = w;
        help = h;
        description = d;
        attributes = a;
        nav_items = n;
      }

      public override string ToString()
      {
        return JsonConvert.SerializeObject(this);
      }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class Gparams //Bug 22708 NAM
    {

      [JsonProperty]
      public string name { get; set; }

      [JsonProperty]
      public string block_type { get; set; }

      [JsonProperty]
      public List<KVPair> labels { get; set; }

      [JsonProperty]                    // BUG 25424 NAM 
      public bool expand { get; set; }

      public Gparams()
      {
        name = block_type = "";
        labels = new List<KVPair>();
        expand = false;                 // BUG 25424 NAM
      }

      public override string ToString()
      {
        return JsonConvert.SerializeObject(this);
      }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class Column //Bug 22708 NAM
    {

      [JsonProperty]
      public string name { get; set; }

      [JsonProperty]
      public string column_type { get; set; }

      [JsonProperty]
      public string default_val { get; set; }

      [JsonProperty]
      public string description { get; set; }

      [JsonProperty]
      public bool hidden { get; set; }

      [JsonProperty]
      public int max_length { get; set; }

      [JsonProperty]
      public int min_length { get; set; }

      [JsonProperty]
      public bool read_only { get; set; }

      [JsonProperty]
      public bool required { get; set; }

      [JsonProperty]
      public string role { get; set; }

      [JsonProperty]
      public string widget_type { get; set; }

      public Column()
      {
        name = column_type = default_val = description = role = widget_type = "";
        hidden = read_only = required = false;
        min_length = max_length = 0;
      }

      public override string ToString()
      {
        return JsonConvert.SerializeObject(this);
      }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class Row //Bug 22708 NAM
    {

      [JsonProperty]
      public int row_id { get; set; }

      [JsonProperty]
      public List<KVPair> data { get; set; }

      public Row()
      {
        row_id = 0;
        data = new List<KVPair>();
      }

      public override string ToString()
      {
        return JsonConvert.SerializeObject(this);
      }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class Grid //Bug 22708 NAM
    {

      [JsonProperty]
      public Gparams gparams { get; set; }

      [JsonProperty]
      public List<Column> columns { get; set; }

      [JsonProperty]
      public List<Row> rows { get; set; }

      public Grid()
      {
        gparams = new Gparams();
        columns = new List<Column>();
        rows = new List<Row>();
      }

      public override string ToString()
      {
        return JsonConvert.SerializeObject(this);
      }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class Paypal //Bug 22708 NAM
    {
      [JsonProperty]
      public string client_id { get; set; }

      [JsonProperty]
      public string amount { get; set; }

      [JsonProperty]
      public bool enable_paypal { get; set; }

      [JsonProperty]
      public bool enable_venmo { get; set; }

      [JsonProperty]
      public string line_items { get; set; }

      public Paypal()
      {
        client_id = amount = line_items = "";
        enable_paypal = true;
        enable_venmo = false;
      }

      public override string ToString()
      {
        return JsonConvert.SerializeObject(this);
      }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class Section //Bug 22708 NAM
    {

      [JsonProperty]
      public string widget_type { get; set; }

      [JsonProperty]
      public string url { get; set; }

      [JsonProperty]
      public string help { get; set; }

      [JsonProperty]
      public string icon_path { get; set; }

      [JsonProperty]
      public string description { get; set; }

      [JsonProperty]
      public string sub_description { get; set; }

      [JsonProperty]
      public string fee { get; set; }

      [JsonProperty]
      public Paypal paypal { get; set; }

      [JsonProperty]
      public List<Field> fields { get; set; }

      public Section()
      {
        description = "default";
        widget_type = help = icon_path = sub_description = fee = url = "";
        fields = new List<Field>();
      }

      public Section(string desc, string wid_type, string hlp, string u, string ip)
      {
        description = desc;
        widget_type = wid_type;
        help = hlp;
        icon_path = ip;
        url = u;
        sub_description = fee = "";
        fields = new List<Field>();
      }

      public override string ToString()
      {
        return JsonConvert.SerializeObject(this);
      }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class Button //Bug 22708 NAM
    {

      [JsonProperty]
      public string widget_type { get; set; }

      [JsonProperty]
      public string description { get; set; }

      [JsonProperty]
      public string role { get; set; }

      [JsonProperty]
      public string url { get; set; }

      [JsonProperty]
      public string method { get; set; }

      [JsonProperty]
      public List<KVPair> attributes { get; set; }

      public Button()
      {
        widget_type = role = url = description = method = "";
        attributes = new List<KVPair>();
      }

      public Button(string wt, string des, string r, string u, string m, List<KVPair> a)
      {
        widget_type = wt;
        role = r;
        url = u;
        description = des;
        method = m;
        attributes = a;
      }

      public override string ToString()
      {
        return JsonConvert.SerializeObject(this);
      }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class Reference //Bug 22708 NAM
    {

      [JsonProperty]
      public string prefix { get; set; }

      [JsonProperty]
      public string number { get; set; }

      [JsonProperty]
      public string description { get; set; }

      [JsonProperty]
      public string timestamp { get; set; }

      public Reference()
      {
        prefix = number = description = timestamp = "";
      }

      public override string ToString()
      {
        return JsonConvert.SerializeObject(this);
      }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class Confirmation //Bug 22708 NAM
    {
      [JsonProperty]
      public string confirmation_msg_1 { get; set; }

      [JsonProperty]
      public string confirmation_msg_2 { get; set; }

      [JsonProperty]
      public string confirmation_msg_3 { get; set; }

      [JsonProperty]
      public string confirmation_msg_4 { get; set; }

      public Confirmation()
      {
        confirmation_msg_1 = confirmation_msg_2 = "";
        confirmation_msg_3 = confirmation_msg_4 = "";
      }

      public override string ToString()
      {
        return JsonConvert.SerializeObject(this);
      }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class Receipt //Bug 22708 NAM
    {
      [JsonProperty]
      public string receipt_header { get; set; }

      [JsonProperty]
      public string receipt_message_top { get; set; }

      [JsonProperty]
      public string receipt_message_bottom { get; set; }

      [JsonProperty]
      public string receipt_footer { get; set; }

      [JsonProperty]
      public string duplicate_receipt { get; set; }

      public Receipt()
      {
        receipt_header = receipt_message_top = "";
        receipt_message_bottom = receipt_footer = duplicate_receipt = "";
      }

      public override string ToString()
      {
        return JsonConvert.SerializeObject(this);
      }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class Body //Bug 22708 NAM
    {

      [JsonProperty]
      public string description { get; set; }

      [JsonProperty]
      public string sub_description { get; set; }

      [JsonProperty]
      public List<KVPair> query_keys { get; set; }

      [JsonProperty]
      public List<Button> buttons { get; set; }

      [JsonProperty]
      public List<Nav> nav { get; set; }

      [JsonProperty]
      public List<String> errors { get; set; }

      [JsonProperty]
      public List<Section> sections { get; set; }

      [JsonProperty]
      public List<Field> fields { get; set; }

      [JsonProperty]
      public List<Menu> tab_menus { get; set; }

      [JsonProperty]
      public List<Menu> tender_menus { get; set; }

      [JsonProperty]
      public List<TenderOrTrans> transactions { get; set; }

      [JsonProperty]
      public List<TenderOrTrans> tenders { get; set; }

      [JsonProperty]
      public string terms { get; set; }

      [JsonProperty]
      public Reference reference { get; set; }

      [JsonProperty]
      public Confirmation confirmation { get; set; }

      [JsonProperty]
      public Receipt receipt { get; set; }

      [JsonProperty]
      public List<KVPair> payment_summary { get; set; }

      [JsonProperty]
      public string disclaimer { get; set; }

      public Body()
      {
        description = sub_description = terms = disclaimer = "";
        query_keys = new List<KVPair>();
        buttons = new List<Button>();
        nav = new List<Nav>();
        errors = new List<String>();
        sections = new List<Section>();
        fields = new List<Field>();
        reference = new Reference();
        confirmation = new Confirmation();
        receipt = new Receipt();
        payment_summary = new List<KVPair>();
        tab_menus = new List<Menu>();
        tender_menus = new List<Menu>();
        tenders = new List<TenderOrTrans>();
        transactions = new List<TenderOrTrans>();
      }

      public override string ToString()
      {
        return JsonConvert.SerializeObject(this);
      }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class Header //Bug 22708 NAM
    {

      [JsonProperty]
      public string title { get; set; }

      [JsonProperty]
      public string icon { get; set; }

      [JsonProperty]
      public bool frameless_ui { get; set; }

      [JsonProperty]
      public string description { get; set; }

      [JsonProperty]
      public string help { get; set; }

      [JsonProperty]
      public Dialog exit { get; set; }

      [JsonProperty]
      public List<string> css_links { get; set; }

      [JsonProperty]
      public List<string> js_links { get; set; }

      [JsonProperty]
      public List<Nav> nav { get; set; }

      [JsonProperty]  //Bug 23080 NAM
      public int session_timeout { get; set; }

      [JsonProperty]  //Bug 23816 NAM
      public string template_name { get; set; }
      public Header()
      {
        title = icon = description = help = "";
        frameless_ui = false;
        exit = new Dialog();
        css_links = new List<string>();
        js_links = new List<string>();
        nav = new List<Nav>();
      }

      public override string ToString()
      {
        return JsonConvert.SerializeObject(this);
      }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class Footer //Bug 22708 NAM
    {

      [JsonProperty]
      public string description { get; set; }  // brand is in description

      [JsonProperty]
      public string image { get; set; }

      [JsonProperty]
      public List<Nav> nav { get; set; }

      public Footer()
      {
        description = image = "";
        nav = new List<Nav>();
      }

      public override string ToString()
      {
        return JsonConvert.SerializeObject(this);
      }
    }

    /// <summary>
    /// Any modal non-PB page (inquiry page, etc)
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class Page //Bug 22708 NAM
    {

      [JsonProperty]
      public string page_type { get; set; }

      [JsonProperty]
      public string form_action { get; set; }

      [JsonProperty]
      public Header header { get; set; }

      [JsonProperty]
      public Footer footer { get; set; }

      [JsonProperty]
      public Cart cart { get; set; }

      [JsonProperty]
      public Body body { get; set; }

      [JsonProperty]
      public string themeColorsJson { get; set; }

      public Page()
      {
        page_type = form_action = "";
        header = new Header();
        footer = new Footer();
        cart = new Cart();
        body = new Body();
      }

      public override string ToString()
      {
        return JsonConvert.SerializeObject(this);
      }
    }
  } // END public class ResponsiveUI
}