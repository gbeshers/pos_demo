
var Controller = null; 
// Bug 13720 DH - iPayment Peripheral Overhaul - Remove the usage of SignaturePadController.
//var SignaturePadController = null; // SignaturePad BUG# 6368 added by DH
var IsPrinterOpen = false,
    IsScannerOpen = false,
    IsMicrOpen    = false;
    //IsCashOpen  = false; //BUG 5793 / /*BUG# 6333 Added dual cash drawer support DH*/
var diagLog = false;

//********************************************************
// Bug 13720 DH - iPayment Peripheral Overhaul
var g_ServiceAddress            = "http://localhost//COREService/"; // Bug# 20825 DH - Configurable Peripherals Service ports
var g_resettingPeripherals      = false;// Bug# 20825 DH - Configurable Peripherals Service ports
DEVICE_RESET_SUCCESS            = true;
var g_bIsRDMAvailable           = true;
var g_bIsEpsonPrinterAvailable  = true;
var g_IsIngenicoAvailable       = true;
var g_IsDigitalCheckInstalled   = true;
var g_IsPertechScannerInstalled = true;// Bug# 17792 DH.
var g_IsPaniniScannerInstalled  = false;// Bug# 19005 & 21370 DH - Panini Device
var g_IsTwainInstalled          = true;
var g_EpsonPhotoIDScanner       = true;
var g_EpsonDocImaging           = true;
var g_card_scanner_user_message = "";
var g_CashDrawerAAvailability   = true;
var g_CashDrawerBAvailability   = true;
var CHK_DI_MODE_CHECKSCANNER    = 0x00;
var CHK_DI_MODE_CARDSCANNER     = 0x01;
var CHK_DI_CHANGE_MODE          = 13;
var g_bIngenicoAddSignature     = false;
var g_iMultiDocCount            = 0;
var g_iCurrentMultiDocIndex     = 0;
// This is an array to chain function calls to allow for sync AJAX calls. 
// Sync calls are noty supported for some browser versions and types so I had to remove all sync peripherals caslls alltogether.
var g_CallQueueForAsyncAJAX; 
var g_VeriFoneIsAvailable          = true;
var g_RDMPrintData                 = "";
var g_UserCanceledCardSwipe        = false;
var g_iNbr_of_documents_to_process = 0;// Bug# 17642 DH. comment# 0.
var g_iCurrent_document_index      = 0;// Bug# 17642 DH. comment# 0.
var g_ScreenBlockerInPlace         = false;// Bug# 17642 comment# 0.
var g_ServiceInitStatus            = false;// Bug# 17902 Comment#45 DH
var g_PointIPAddress               = null; // Bug# 17902 Comment#45 DH
var g_DepartmentLogo               = ""; // Bug# 18309 DH - Add the ability to configure the receipt header image.
// end bug 13720 DH
var CachedCurrentObject          = ""; // Bug 19107 MJO
var g_bfranking                  = false;// TTS 20641 DH
var g_DevicePrompt               = "";// Bug 20680 DH - Use configurable prompts in the Ingenico signature form
var g_EpsonAvailable             = false;// BUG 20854 DH
var g_EpsonOCRSupported          = false;// Bug# 20806 DH
var g_peripherals_http_port      = 80;// Bug# 20825 DH - Configurable Peripherals Service ports
var g_peripherals_https_port     = 443;// Bug# 20825 DH - Configurable Peripherals Service ports
var g_IsMx915                    = false; // Bug 18811 MJO
var g_NoMICRSelectedInConfig     = false;// Bug 21375 DH
var g_EpsonS9000Connected        = false;// Bug 21811 DH
var g_strEpsonImageSiteToCapture = "";// Bug 21811 DH
var g_PaniniImages               = null; // Bug# 22245 & 22371 DH
var g_confirming_documents       = false; // Bug# 22486 DH
var g_doc_count                  = 0; // Bug# 22486 DH
var g_resolved_message           = ""; // Bug# 22486 DH
var g_pos_start                  = false;
var g_auto_continue              = true;//Bug 22715 DH
var g_aba_validation             = false;//Bug 22729 DH
var g_aba_validation_error       = false;//Bug 22729 DH
var g_isValidateACH              = false; //TTS17888 & Bug 21791 DH
var g_bEpson_print_data_back     = false;/*Bug 23916 DH*/
var g_PertechEnableFranking      = false;// Bug# 24109 DH
var g_ServiceConnectAttempts     = 1;// Bug 24475 DH
var g_EpsonH2000Connected        = false;// Bug# 24486 DH
var g_EpsonValidationPrinterConnected = false;// Bug# 24256 DH - TM-U295.
var g_DocType                    = "";// Bug# 24736 DH
var g_EpsonP80Connected          = false;// 24950 DH
var g_S9000DepositData           = "";// Bug# 23408 DH
var g_bPertechProcessing         = false;// Bug# 25201 DH
var g_strPrinterModel            = "";//  Bug# 25291 DH
var g_aba_loaded                 = false;// Bug# 25117 DH
var aba_data_uploaded            = false;// Bug# 25117 DH
var g_PeripheralsConnected = false;// Bug# 26230 DH
var g_ArrPages = new Array();// Bug 27101/27314 DH - Split validation data into pages
var g_iCurrentValidationPageNumber = -1;// Bug 27101/27314 DH - Split validation data into pages

//********************************************************

/* Testing existence*/
function is_defined( variable_name, a_window ) {
  if ( a_window == null ) { a_window = window; }
  return ( typeof( a_window[ variable_name ] ) != "undefined" );
}

/*
  Initialization should open all devices to be opened
  - This could be structured to allow arbitrary
    device additions in initialization
*/

function Init() 
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  CHK_DI_MODE_CHECKSCANNER = 0x00;
  CHK_DI_MODE_CARDSCANNER  = 0x01;
  CHK_DI_CHANGE_MODE       = 13;
  Controller               = null; 
  IsPrinterOpen            = false;
  IsScannerOpen            = false;
  IsMicrOpen               = false;
  diagLog                  = false;
  // Bug 17505 MJO - Use HTTPS if connected to iPayment via SSL
  g_ServiceAddress               = "";// Bug# 20825 DH - Configurable Peripherals Service ports
  DEVICE_RESET_SUCCESS           = true;
  g_bIsRDMAvailable              = true;
  g_bIsEpsonPrinterAvailable     = true;
  g_IsIngenicoAvailable          = true;
  g_EpsonPhotoIDScanner          = true;
  g_VeriFoneIsAvailable          = true;
  g_EpsonDocImaging              = true;
  g_card_scanner_user_message    = "";
  g_CashDrawerAAvailability      = true;
  g_CashDrawerBAvailability      = true;
  get_top().main.$.support.cors  = true;
  g_bIngenicoAddSignature        = false;
  g_iMultiDocCount               = 0;
  g_iCurrentMultiDocIndex        = 0;
  g_CallQueueForAsyncAJAX        = new Array();
  g_IsDigitalCheckInstalled      = true;// Bug# 16027 DH - DigitalCheck device support
  g_IsPertechScannerInstalled    = true;// Bug# 17792 DH.
  g_IsPaniniScannerInstalled     = false; // Bug# 19005 & 21370 DH - Panini Device
  g_IsTwainInstalled             = true;
  g_RDMPrintData                 = "";
  g_iNbr_of_documents_to_process = 0;// Bug# 17642 DH. comment# 0.
  g_iCurrent_document_index      = 0;// Bug# 17642 DH. comment# 0.
  g_ScreenBlockerInPlace         = false;// Bug# 17642 comment# 0.
  g_ServiceInitStatus            = false;// Bug# 17902 Comment#45 DH
  g_PointIPAddress               = null; // Bug# 17902 Comment#45 DH
  g_bfranking                    = false;// TTS 20641 DH
  g_DevicePrompt                 = "";// Bug 20680 DH - Use configurable prompts in the Ingenico signature form
  g_EpsonAvailable               = false;// BUG 20854 DH
  g_EpsonOCRSupported            = false;// Bug# 20806 DH
  g_NoMICRSelectedInConfig       = false;// Bug 21375 DH
  g_EpsonS9000Connected          = false;// Bug 21811 DH
  g_strEpsonImageSiteToCapture   = "";// Bug 21811 DH
  g_PaniniImages                 = null; // Bug# 22245 & 22371 DH
  g_auto_continue                = true/* Bug 22715 DH */
  g_aba_validation               = false;//Bug 22729 DH
  g_aba_validation_error         = false;//Bug 22729 DH
  g_isValidateACH                = false;//Bug 21791 DH
  g_ServiceConnectAttempts       = 1;// Bug 24475 DH
  g_EpsonH2000Connected          = false;// Bug# 24486 DH
  g_EpsonValidationPrinterConnected = false;// Bug# 24256 DH - TM-U295.
  g_EpsonP80Connected            = false;// 24950 DH
  g_S9000DepositData             = "";// Bug# 23408 DH
  g_bPertechProcessing           = false;// Bug# 25201 DH
  g_strPrinterModel              = "";//  Bug# 25291 DH
  g_aba_loaded                   = false;// Bug# 25117 DH
  aba_data_uploaded              = false;// Bug# 25117 DH
  g_PeripheralsConnected = false;// Bug# 26230 DH
  g_ArrPages = new Array();// Bug 27101/27314 DH - Split validation data into pages
  g_iCurrentValidationPageNumber = -1;// Bug 27101/27314 DH - Split validation data into pages
  // end Bug 13720 DH - iPayment Peripheral Overhaul
}


// Bug# 17642 DH. comment# 0.
function set_doc_info(nbr_of_documents_to_process, current_document_index, isValidateACH)
{
  // Keep track of the number of the documents to be processed and the current document index. 
  // This will help to correctly block the screen between document cancels or skips by the user in the UI. DH.
  g_iNbr_of_documents_to_process = nbr_of_documents_to_process;
  g_iCurrent_document_index = (current_document_index + 1);// zero base index so lets bump it up by 1 to simplify the situation.
  g_isValidateACH = isValidateACH;//TTS17888 & Bug 21791 DH
}
// End Bug# 17642 comment# 0.

//--------------------- BUG# 10904 RDM Device Added by DH -------------------------------------
// Bug 11610 DH - New RDM 1.5.3 Compatibility
var g_bTemp_MICR_OR_OCR_Ready          = false;
var g_bTemp_Image_Ready                = false;
var g_strTempImageFront                = "";
var g_strTempImageBack                 = "";// Bug 21811 DH
var g_strTempApp                       = "";
var g_content_type                     = "";
var g_bTemp_OCR_DATA                   = "";
var g_bTemp_OCR_DATA_ERROR             = "";
var g_bWaiting_For_Epson_OCR_and_Image = false;// Bug 12797 DH


function IsEpsonS9000Connected()// Bug 21811 DH
{
    return g_EpsonS9000Connected;
}

// Bug 11610 DH - New RDM 1.5.3 Compatibility
function ResetTempFields()
{
  g_bTemp_MICR_OR_OCR_Ready = false;
  g_bTemp_Image_Ready       = false;
  g_strTempImageFront       = "";
  g_strTempImageBack        = "";// Bug 21811 DH
  g_strTempApp              = "";
  g_content_type            = "";
}

// Bug 11610 DH - New RDM 1.5.3 Compatibility
function RDM_GetErrorInfo()
{
    var strMessage = "";
    strMessage += "Unable to use RDM device. \n\nPlease Reset.";
    
    // Too much information for end users, but good for development.
    //strMessage += "\nError Message: [" + Controller.RDMGetLastErrorMessage() + "]";
    //strMessage += "\nError code: [" + Controller.RDMGetLastErrorCode() + "]";
    
return strMessage;
}

function RDM_CheckScan(strDoFrontAndBackImage, strFRANKingText, top_app, message/*Bug# 11291 DH*/) 
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  if(message != "opt" && message != "GEO.opt" && message != "" && message != undefined)// Bug# 11291 DH
  {
    setTimeout(function() {get_top().main.enqueue_message(message); }, 10);
  }
  
  setTimeout(function () { RDM_CheckScanAsync(strDoFrontAndBackImage, strFRANKingText, top_app, message); }, 100)
  // end bug 13720 DH
}

// Bug 11610 DH - New RDM 1.5.3 Compatibility
// BUG# 10904 RDM Device DH
function RDM_CheckScanAsync(strDoFrontAndBackImage, strFRANKingText, top_app, message/*Bug# 11291 DH*/) 
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  
  ResetTempFields();
  
  if(!RDM_IsAvailable())
  {
    alert(RDM_GetErrorInfo());
    get_top().main.dequeue_message();
    return;
  }
  
  var CheckScanRequest = {};
	CheckScanRequest.strDoFrontAndBackImage = strDoFrontAndBackImage;
	CheckScanRequest.strCashieringScreenObject = top_app;
	
	var RDM_CheckScanAsync_SuccessCallback = function (result, param) 
	{
	  if(result == undefined)
	  {
	    alert("Error scanning document. Please try again.");
	    get_top().main.dequeue_message();
	    return;
	  }
	  
		if(result.RDM_CheckScanResult.iLastErrorCode == 0)
		{
		  Device_Micr_receive({ 
      app:GetCurrentObject(),
      routing_number:result.RDM_CheckScanResult.strMICR_TransitNumber,
      bank_account_number:result.RDM_CheckScanResult.strMICR_AccountNumber,
      check_number:result.RDM_CheckScanResult.strMICR_CheckNumber,
      raw_micr:result.RDM_CheckScanResult.strMICR_Raw,
      country_code:result.RDM_CheckScanResult.strCountryCode,
      check_type:result.RDM_CheckScanResult.strMICR_CheckType,
      check_epc:result.RDM_CheckScanResult.strEPC,
      check_amount:result.RDM_CheckScanResult.strCheckAmount,
      bank_number:result.RDM_CheckScanResult.strBankNumber,
      on_us:result.RDM_CheckScanResult.strOnUs});
    
      RDM_SendImageToScreen({ app:GetCurrentObject(),
                        content_image_front:result.RDM_CheckScanResult.strImage_FRONT_DataString,
                        content_image_back:result.RDM_CheckScanResult.strImage_BACK_DataString,
                        content_type: "image/tiff",
                        doc_type:"MICR" /*Bug# 24736 DH*/}); 
		}	
		else
		{
			alert("RDM check scanning returned an Error: " + result.RDM_CheckScanResult.strLastErrorMessage);
			get_top().main.dequeue_message();
		  return;
		}
	}
	
	var RDM_CheckScanAsync_ErrorCallback = function (result, param) 
	{
		alert("RDM Error: " + result.status + "\nStatus Text: " + result.statusText );
		get_top().main.dequeue_message();
		return;
	}
  
  AjaxCallPeripherals("rdm_check_scan", RDM_CheckScanAsync_SuccessCallback, RDM_CheckScanAsync_ErrorCallback, "", CheckScanRequest);
  // end Bug 13720 DH
}


// BUG# 10904 RDM Device DH
function RDM_SendImageToScreen(args) 
{
  get_top().main.post_remote('Business_app', 'Business_app.Create_core_item_app.rdm_receive_image', args, 'main', true, get_top().main.displayResult, null);   
}

function RDM_IsAvailable() 
{
  // BUG# 10904 RDM Device DH
  // Bug 13720 DH - iPayment Peripheral Overhaul
  // We can only use one printer at a time. If RDM and Epson are installed then choose Epson as the default.
  
  return g_bIsRDMAvailable;
}


function RDM_CheckHealth() 
{   
  // BUG# 10904 RDM Device DH
  // Bug 13720 DH - iPayment Peripheral Overhaul     
 if(RDM_IsAvailable())// Device must be installed
  return 0;
   
 return -1;
}

// BUG# 10904 RDM Device DH
function RDM_ErrorCallBackFunction(sRequestType, strErrorCode, strErrorMSG, top_app) 
{
    // This function can be used in other ways in the future.
    alert(RDM_GetErrorInfo());// Bug 11610 DH - New RDM 1.5.3 Compatibility
}

// BUG# 10904 RDM Device DH
function RDM_PrintMultipleLines(strLines)
{
   // Function replaced for Bug 13720 DH - iPayment Peripheral Overhaul

  if(!RDM_IsAvailable())
  {
      alert(RDM_GetErrorInfo());// Bug 11610 DH - New RDM 1.5.3 Compatibility
      return false;
  }
  
  // When the end of receipt comes, the receipt is send to the RDM printer and this function is called to clear the receipt data DH.
  var RDM_PrintMultipleLines_CallbackToResetReceiptData = function (result, param) 
  {
    if(result  != undefined)
    {
      if(result.RDM_PrintReceiptResult.iLastErrorCode == -1)
        alert("Unable to print. Error: " + result.RDM_PrintReceiptResult.strLastErrorMessage)
    }
    
    g_RDMPrintData = "";
    return
  }
  
  // Catch the end of receipt. This is set in the PML.<defmethod _name='to_javascript'>
  // Epson printer is using queue mechanism that I created, but I want to try this for RDM instead to check which will be more reliable.
  if(strLines == "\n\n\n\n\n\n\n")
	{
	  g_RDMPrintData += strLines// Add the spacer
	  
	  var base16String = {};
    base16String.printData = ConvertToBase16(g_RDMPrintData);// Bug# 25780 DH - Moved the ConvertToBase16 to this file.
	
	  // Print the full receipt
	  AjaxCallPeripherals("rdm_print", RDM_PrintMultipleLines_CallbackToResetReceiptData, RDM_PrintMultipleLines_CallbackToResetReceiptData, "", base16String);
	}
	else
	{
	  g_RDMPrintData += strLines
	}
}


// Bug 11610 DH - New RDM 1.5.3 Compatibility
// BUG# 11144 RDM Device DH - Add the ability to scan the front of a document without the OCR
function RDM_ImageScan(strDoFrontAndBackImage, top_app, message/*Bug# 11291 DH*/) 
{
  // Function replaced for Bug 13720 DH - iPayment Peripheral Overhaul
  
  if(message != "opt" && message != "GEO.opt" && message != "" && message != undefined)
  {
    setTimeout(function() {get_top().main.enqueue_message(message); }, 10);
  }
  
  setTimeout(function () { RDM_ImageScanAsynch(strDoFrontAndBackImage, top_app, message/*Bug# 11291 DH*/); }, 100);
  return 0;
}

function RDM_ImageScanAsynch(strDoFrontAndBackImage, top_app, message/*Bug# 11291 DH*/) 
{
 // Bug 13720 DH - iPayment Peripheral Overhaul
 ResetTempFields();
 return_value = -1;
  
 var OCRRequest = {};
	OCRRequest.strDoFrontAndBackImage = strDoFrontAndBackImage;
	OCRRequest.strCashieringScreenObject = top_app;
	
	var RDM_ImageScanAsynch_SuccessCallback = function (result, param) 
	{
	  if(result == undefined)
	  {
	    alert("Error scanning document. Please try again.");
	    get_top().main.dequeue_message();
	    return;
	  }
	  
		if(result.RDM_ImageScanResult.iLastErrorCode == 0)
		{
			RDM_SendImageToScreen({ app:GetCurrentObject(),
                        content_image_front:result.RDM_ImageScanResult.strImage_FRONT_DataString,
                        content_image_back:result.RDM_ImageScanResult.strImage_BACK_DataString,
                        content_type:"image/tiff"});
		}	
		else if(result.RDM_ImageScanResult.iLastErrorCode != 0)
		{
			alert("Error Returned: " + result.RDM_ImageScanResult.strLastErrorMessage);
		}
		else
		{
			alert("Undefined Error Returned from rdm_ocr_scan()");
		}
		
		get_top().main.dequeue_message();
		return;
	}
	
	var RDM_ImageScanAsynch_ErrorCallback = function (result, param) 
	{
		alert("Error: " + result.status + "\nStatus Text: " + result.statusText );
		get_top().main.dequeue_message();
		return;
	}	
	
	AjaxCallPeripherals("rdm_image_scan", RDM_ImageScanAsynch_SuccessCallback, RDM_ImageScanAsynch_ErrorCallback, "", OCRRequest);
  return 0;
}

// end BUG# 11144 RDM Device DH - Add the ability to scan the front of a document without the OCR

// Bug 11610 DH - New RDM 1.5.3 Compatibility
function RDM_OCRScan(request_type, iOCR_x, iOCR_y, iOCR_width, iOCR_height, top_app, message/*Bug# 11291 DH*/) 
{
  // Function replaced for Bug 13720 DH - iPayment Peripheral Overhaul
  if(message != "opt" && message != "GEO.opt" && message != "" && message != undefined)
  {
    setTimeout(function() {get_top().main.enqueue_message(message); }, 10);
  }
  
  setTimeout(function() {RDM_OCRScanAsynch(request_type, iOCR_x, iOCR_y, iOCR_width, iOCR_height, top_app, message); }, 100);
  return 0;
}

function RDM_OCRScanAsynch(request_type, iOCR_x, iOCR_y, iOCR_width, iOCR_height, top_app, message/*Bug# 11291 DH*/) 
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  ResetTempFields();

  return_value                         = -1;
  var OCRRequest                       = {};
	OCRRequest.strRequestType            = request_type;// User "OCR" to retrieve the front image only and "OCR2" for both images.
	OCRRequest.iOCR_x                    = iOCR_x;
	OCRRequest.iOCR_y                    = iOCR_y;
	OCRRequest.iOCR_width                = iOCR_width;
	OCRRequest.iOCR_height               = iOCR_height;
	OCRRequest.strCashieringScreenObject = top_app;
	
	var RDM_OCRScanAsynch_SuccessCallback = function (result, param) 
	{
	  if(result == undefined)
	  {
	    alert("Error scanning document. Please try again.");
	    get_top().main.dequeue_message();
	    return;
	  }
	  
		if(result.RDM_OCRScanResult.iLastErrorCode == 0)
		{
		  return_value = 0;
		  
		  if(top_app == "" || undefined)
        top_app = GetCurrentObject();
      else
        top_app = top_app.substr(4);
     
     var strImageFront = result.RDM_OCRScanResult.strImage_FRONT_DataString;
     var strImageBack = result.RDM_OCRScanResult.strImage_BACK_DataString;
     g_content_type = "image/tiff";// These images are always TIFF
     
       // Needed for CASL part.
      if(strImageFront == "")
          strImageFront = "undefined";
      if(strImageBack == "")
          strImageBack = "undefined";
        
      g_bTemp_MICR_OR_OCR_Ready = true;
      g_bTemp_OCR_DATA = result.RDM_OCRScanResult.strOCR;
      
      args                        = new Array();
      args["content_image_front"] = strImageFront;
      args["content_image_back"]  = strImageBack;
      args["content_type"]        = g_content_type;
      args["ocr_data"]            = g_bTemp_OCR_DATA;
      args["ocr_data_error"]      = "";
      args["app"]                 = top_app;
              
      get_top().main.post_remote('Business_app', 'Business_app.Create_core_item_app.rdm_receive_ocr_and_image', args, 'main', true);
      delete args;
      get_top().main.dequeue_message();
      return;
		}	
		else if(result.RDM_OCRScanResult.iLastErrorCode != 0)
		{
		  return_value = -1;
		  g_bTemp_OCR_DATA_ERROR = result.RDM_OCRScanResult.strLastErrorMessage;
			alert("RDM OCR Error: " + result.RDM_OCRScanResult.strLastErrorMessage);
			get_top().main.dequeue_message();
			return;
		}
		else
		{
		  return_value = -1;
			alert("Undefined Error Returned from rdm_ocr_scan()");
			get_top().main.dequeue_message();
			return;
		}
	}
	
	var RDM_OCRScanAsynch_ErrorCallback = function (result, param) 
	{
		alert("Error: " + result.status + "\nStatus Text: " + result.statusText );
		get_top().main.dequeue_message();
		return;
	}
	
	AjaxCallPeripherals("rdm_ocr_scan", RDM_OCRScanAsynch_SuccessCallback, RDM_OCRScanAsynch_ErrorCallback, "", OCRRequest);	
	
	get_top().main.dequeue_message();
	// end bug 13720 DH
}
//--------------------- end BUG# 10904 RDM Device Added by DH -------------------------------------

//-------------------------------- BUG 20854 DH ---------------------------------------------------
// Bug 24518 MJO - Show end of event message when finished
function PrintValidationPages(message, formatted_data_to_print, auto_processing, end_of_event)// Bug 27101 DH
{
    // IPAY - 513 NK - Support ampersand and quot
    formatted_data_to_print = formatted_data_to_print.replace("&quot;", '"');
    formatted_data_to_print = formatted_data_to_print.replace("&amp;", "&");

  if (!g_EpsonAvailable && !g_EpsonValidationPrinterConnected)
  {
    if (end_of_event)
    {
      alert("Validation printer not available.");
      ShowEndOfEventMessage(false); // Bug 24518 MJO - Go back to end of event printing
    }

    return;
  }

  if (IsP80Available())// Bug# 24950 DH
    return;

  if (auto_processing != "Yes")
    return;

  if (message != "opt" && message != "GEO.opt" && message != "" && message != undefined)// Bug# 11291 DH
  {
    // Lets do this ina separate thread so it shows. Printer will grab all time slices.
    cancel_btn = "<button onclick=get_pos().CancelValidationStamp(" + end_of_event + ")>Cancel</button>";
    setTimeout(function () { get_top().main.enqueue_message({ content: "<BR/><CENTER>" + message + "</CENTER><BR/>" + cancel_btn, actions: {} }); }, 10);
  }

  var InsertValidationStampCompleteError = function (result, param)
  {
    alert("Error processing document. Please try again.");
    get_top().main.dequeue_message();

    // Bug 24518 MJO - Go back to end of event printing
    if (end_of_event)
      ShowEndOfEventMessage(false);

    return;
  }

  // Bug 21811 DH

  if (IsEpsonS9000Connected() && !g_EpsonValidationPrinterConnected)
  {
    ScannerChangeMode(CHK_DI_MODE_CHECKSCANNER, "");

    var S9000ValidationStampComplete = function (result, param)
    {
      if (result === null || result === undefined || result === "")// Bug 22558 DH
      {
        alert("Error processing validation stamp document.");
        get_top().main.dequeue_message();

        // Bug 24518 MJO - Go back to end of event printing
        if (end_of_event)
          ShowEndOfEventMessage(false);

        return;
      }

      if (result.Epson_PrintS9000ValidationStempResult.iLastErrorCode != 0)// Bug 22558 DH
      {
        if (result.Epson_PrintS9000ValidationStempResult.strLastErrorMessage === "User cancelled")// Bug 22558 DH
        {
          // Controlled condition
          get_top().main.dequeue_message();

          // Bug 24518 MJO - Go back to end of event printing
          if (end_of_event)
            ShowEndOfEventMessage(false);

          return;
        }
        alert("Error processing validation stamp document.\n\n" + result.Epson_PrintS9000ValidationStempResult.strLastErrorMessage);// Bug 22558 DH
        get_top().main.dequeue_message();

        // Bug 24518 MJO - Go back to end of event printing
        if (end_of_event)
          ShowEndOfEventMessage(false);

        return;
      }

      // Bug 27101 DH - Split validation data into pages
      var page = GetNextValidationPage();
      if (page == undefined || page == "")
      {
        get_top().main.dequeue_message();

        // Bug 24518 MJO - Go back to end of event printing
        // Bug 25103 MJO - This is a success case
        if (end_of_event)
          ShowEndOfEventMessage(true);
      }
      else
      {
        alert("Please insert another page.");
        var base16String = {};
        base16String.strPrintData = ConvertToBase16(page);// Bug# 25780 DH - Moved the ConvertToBase16 to this file.
        AjaxCallPeripherals("epson_s9000_validation_stamp_print", S9000ValidationStampComplete, InsertValidationStampCompleteError, "", base16String);
        return;
      }
      // End Bug 27101 DH - Split validation data into pages
    }

    var base16String = {};
    base16String.strPrintData = ConvertToBase16(formatted_data_to_print);// Bug# 25780 DH - Moved the ConvertToBase16 to this file.
    AjaxCallPeripherals("epson_s9000_validation_stamp_print", S9000ValidationStampComplete, InsertValidationStampCompleteError, "", base16String);


    return;
  }
  // end Bug 21811 DH

  var InsertValidationStampComplete = function (result, param)
  {
    if (result === null || result === undefined || result === "")// Bug 22558 DH
    {
      alert("Error processing validation stamp document.");
      get_top().main.dequeue_message();

      // Bug 24518 MJO - Go back to end of event printing
      if (end_of_event)
        ShowEndOfEventMessage(false);

      return;
    }

    if (result.iLastErrorCode != 0)
    {
      if (result.strLastErrorMessage === "User cancelled")// Bug 22558 DH
      {
        // Bug 26965 MJO - Don't dequeue the end of event print manager message
        if (!end_of_event) 
        {
          get_top().main.dequeue_message();// Controlled condition
        }

        return;
      }
      alert("Error processing validation stamp document.\n\n" + result.strLastErrorMessage);// Bug 22558 DH
      get_top().main.dequeue_message();

      // Bug 24518 MJO - Go back to end of event printing
      if (end_of_event)
        ShowEndOfEventMessage(false);

      return;
    }

    // OK lets send the print command
    var PrintValidationStamp = function (result, param)
    {
      // Bug 27101 DH - Split validation data into pages
      var page = GetNextValidationPage();
      if (page == undefined || page == "")
      {
        get_top().main.dequeue_message();

        // Bug 24518 MJO - Go back to end of event printing
        // Bug 25103 MJO - This is a success case
        if (end_of_event)
          ShowEndOfEventMessage(true);
      }
      else
      {
        alert("Please insert another page.");
        formatted_data_to_print = page;// Next page
        AjaxCallPeripherals("epson_validation_stamp_insert", InsertValidationStampComplete, InsertValidationStampCompleteError/*Bug 21811 DH*/, "", "");
        return;
      }
      // End Bug 27101 DH - Split validation data into pages

      return;
    }

    var base16String = {};
    // Bug# 25780 DH - Moved the ConvertToBase16 to this file.
    base16String.strPrintData = ConvertToBase16(formatted_data_to_print);// Convert it so we don't have any issues sending this over it will look better if intercepted as well.
    AjaxCallPeripherals("epson_validation_stamp_print", PrintValidationStamp, PrintValidationStamp, "", base16String);
  }

  AjaxCallPeripherals("epson_validation_stamp_insert", InsertValidationStampComplete, InsertValidationStampCompleteError/*Bug 21811 DH*/, "", "");
}

// Bug 27101 DH - Split validation data into pages
function GetNextValidationPage()
{
  // Bug# 27313 DH
  if (g_ArrPages == null || undefined)
    return undefined;

  if (g_ArrPages.length == 0)
    return undefined;
  // end - Bug# 27313 DH

  var page = "";
  try
  {
    g_iCurrentValidationPageNumber = g_iCurrentValidationPageNumber + 1;
    page = g_ArrPages[g_iCurrentValidationPageNumber];
  }
  catch (err)
  {
    delete g_ArrPages;// Bug# 27313 DH
    return "";
  }

  // Bug# 27313 DH
  if (page == undefined)
    delete g_ArrPages;

  return page;
}

function StripLastNewLine(page)
{
  // Bug 27101 DH - Split validation data into pages

  try
  {
    var s = page;
    if (s[s.length - 1] == '\n')
      s = s.slice(0, s.length - 1);
  }
  catch (err)
  {
    return page;
  }

  return s;
}

function PrintValidation(message, formatted_data_to_print, auto_processing, end_of_event, iPageSizeInLines, validation_char_size/*Bug# 27480 DH*/)// Bug 27101 DH - Split validation data into pages
{
  // Bug 27101 DH - Split validation data into pages

  // Bug# 27480 DH - Add a configurable font size for the validation stamp
  if (validation_char_size == "" || validation_char_size == undefined)
    validation_char_size = "LARGE"
  
  AjaxCallPeripherals("slp_line_chars/" + validation_char_size, "", "", "", "");
  // End Bug# 27480 DH
  
  formatted_data_to_print = formatted_data_to_print.replace(/__/g, '\n');
  formatted_data_to_print = formatted_data_to_print.replace(/String.newline/g, '\n');

  try 
  {
    if (g_ArrPages != null)// Start fresh
    {
      delete g_ArrPages;
      g_ArrPages = new Array();
    }

    // Make it backward compatible. 20 should be enough for most. Normally the validation is about 2/3 lines unless it's a special case like the LADBS 48 lines.
    if (iPageSizeInLines == undefined)
      iPageSizeInLines = 20;

    g_iCurrentValidationPageNumber = -1;
    var iPageSize = 0;
    var iPageNumber = 0;
    var lines = formatted_data_to_print.split('\n');

    if (lines.length > iPageSizeInLines)
    {
      var page = "";
      for (var n = 0; n < lines.length; n++)
      {
        page += lines[n] + "\n";
        iPageSize++;

        if (iPageSize >= iPageSizeInLines)// Add each page
        {
          g_ArrPages[iPageNumber] = StripLastNewLine(page);
          page = "";
          iPageSize = 0;
          iPageNumber++;
        }
      }

      if (page != "")// Catch last page
      {
        g_ArrPages[iPageNumber] = StripLastNewLine(page);
      }
    }
    else//  Lines fit on a single page
    {
      PrintValidationPages(message, formatted_data_to_print, auto_processing, end_of_event);
      return;
    }

    PrintValidationPages(message, GetNextValidationPage(), auto_processing, end_of_event);
  }
  catch (err)
  {
    return;
  }
}

function CancelValidationStamp(end_of_event)// BUG 20854 DH - Print validation stamp after posting transactions or tenders
{
    // User clicked cancel on the prompt screen
    AjaxCallPeripherals("epson_printer_validation_stamp_cancel", "", "", "", "");

  // Bug 25102 MJO - Make sure the end of event message isn't dequeued
  if (!end_of_event)
  {
    g_ArrPages.splice(0, g_ArrPages.length)// Bug 27314 DH - Epson validation: canceling during a multi - page validation fails to stop the next page from printing
    get_top().main.dequeue_message();
  }
  else
  {
    // Bug# 27313 DH - if multi-page validation in effect, remove the prompt.
    if (g_ArrPages != undefined && g_ArrPages != null)
    {
      g_ArrPages.splice(0, g_ArrPages.length)// Bug 27314 DH - Epson validation: canceling during a multi - page validation fails to stop the next page from printing
      get_top().main.dequeue_message();
    }

    ShowEndOfEventMessage(false); // Bug 25102 MJO - Show EoE
  }
}
// end BUG 20854 DH - Print validation stamp after posting transactions or tenders
//-------------------------------- END BUG 20854 DH ----------------------------------------------

//--------------------- Bug# 16027 DH - DigitalCheck device support Added by DH ------------------

function DigitalCheck_ScanDoc(digital_device_name, top_app, doc_side, doc_type, doc_resolution, message, digital_ocr_left, digital_ocr_top, digital_ocr_right, digital_ocr_bottom)
{
  // Bug# 16027 DH - DigitalCheck device support Added by DH
  
  if (!g_IsDigitalCheckInstalled)
  {
    alert("DigitalCheck device [" + digital_device_name + "] is not available. Please check the power and USB cables.");
    return -1;
  }
  
  if(doc_type == "NONE" && doc_side == "NONE")
  {
    alert("The document settings are incorrect. The side and document type are missing.");
    return -1;
  }
    
  var sMessage = get_top().main.get_message(message);// Bug# 20399 DH
  if (confirm(sMessage.content + "\n\nClick OK when ready."))// Bug# 20399 DH 
  {
    // Cancel the insert.
  } 
  else 
  {
    return -1;
  }
    
  // Determin if the we are scanning a full sheet of paper or a check
  scan_full_sheet = "TRUE";
  if(doc_type == "MICR")
    scan_full_sheet = "FALSE";
  
  if (message != "opt" && message != "" && message != undefined)
  {
    bRetVal = get_top().main.enqueue_message(message);
    if(bRetVal == false)
      return false;
  }
  
  setTimeout(function()
  {
    DigitalCheck_ScanDocAsync(digital_device_name, top_app, doc_side, doc_type, doc_resolution, scan_full_sheet, digital_ocr_left, digital_ocr_top, digital_ocr_right, digital_ocr_bottom);
  }, 10
  );
}

function DigitalCheck_ScanDocAsync(device_name, top_app, doc_side, doc_type, doc_resolution, scan_full_sheet, digital_ocr_left, digital_ocr_top, digital_ocr_right, digital_ocr_bottom)
{
  // Bug# 16027 DH - DigitalCheck device support Added by DH
    
  var DigitalCheck_CallbackSuccess = function(result, param/*param = remove UI message*/)
  {
    try
	  {
	    if(result == null || result == undefined || result.iLastErrorCode != 0)
	    {
	      var s = " ";
	      if(result != null && result != undefined)
	        s = result.strLastErrorMessage;
	      
	      alert("Error processing document. " + s);
	      if(param)
	        get_top().main.dequeue_message();
	      return;
	    }
	    
	    // Probably Canceled by the user. This is one case that will not set the strCashieringScreenObject.
	    if(result.strCashieringScreenObject == null)
        return;
           
      var app = result.strCashieringScreenObject; 
      if(app.substr(0, 3) == "GEO")
        app = app.substr(4);
      else if(app == "")
        app = get_top().main.get_current_obj();
        
      
      if(result.strDocumentType.toUpperCase() == "NONE")
      {
       // No MICR or OCR, probably only an image
      }
      else if(result.strDocumentType.toUpperCase() == "MICR")
      {
        // Bug# 21800 DH
        if(result == null ||
          result.strDigitalCheckNumber == null || result.strDigitalCheckNumber == undefined || result.strDigitalCheckNumber == "" ||
          result.strDigitalRoutingNumber == null || result.strDigitalRoutingNumber == undefined || result.strDigitalRoutingNumber == "" ||
          result.strDigitalBankNumber == null || result.strDigitalBankNumber == undefined || result.strDigitalBankNumber == "" ||
          result.str_RAW_MICR == null || result.str_RAW_MICR == undefined || result.str_RAW_MICR == "")
        {
          alert("Invalid scan. Please try again.");
          if (param)
              get_top().main.dequeue_message();
          return -1;
        }

        len = result.strDigitalCheckNumber.length + result.strDigitalRoutingNumber.length + result.strDigitalBankNumber.length;
        
        if(result.strDigitalCheckNumber.length < 1 || result.strDigitalRoutingNumber.length < 9 || result.strDigitalBankNumber.length < 1 || result.str_RAW_MICR.length < len || 
          IsValidMICR(result.str_RAW_MICR) != true)
        {
         alert("Invalid scan. Please try again.");
         if (param)
          get_top().main.dequeue_message();
          return -1;
        }
        // end Bug# 21800 DH
        
        // ICL validation for check types, should happen in the CASL side
        // MICR should always have a check image with it as well. Handeled in the last step.       
        Device_Micr_receive({ 
        app:GetCurrentObject(),
        routing_number:result.strDigitalRoutingNumber,
        bank_account_number:result.strDigitalBankNumber,
        check_number:result.strDigitalCheckNumber,
        raw_micr:result.str_RAW_MICR,// TTS 17880 DH
        country_code:"",
        check_type: result.strDigitalCheckCheckType,// Bug# 20157 DH
        check_epc: result.strDigitalCheckEPC == undefined ? "" : result.strDigitalCheckEPC,// Bug 24188 DH - Develop code for parsing out External Processing Code (EPC) from check MICR,
        check_amount:"",
        bank_number:"",
        on_us:""});
      }
      else if(result.strDocumentType.toUpperCase() == "OCR")
      {
        Device_Scanner_receive_ocr({app:app, ocr_data:result.strOCR, ocr_data_error:""});
      }
      
      if(result.strImage_FRONT_DataString != "" || result.strImage_BACK_DataString != "")
      {
        args                        = new Array();
        args["app"]                 = app;
        args["content_image_front"] = result.strImage_FRONT_DataString;
        args["content_image_back"]  = result.strImage_BACK_DataString;
        args["image_front_tag"]     = "DIGITALCHECK_FRONT_IMAGE";
        args["image_back_tag"]      = "DIGITALCHECK_BACK_IMAGE";
        
        if(result.strDocumentSide.toUpperCase() == "CARD IMAGE")// Color scans
          args["content_type"] = "image/jpeg";
        else
          args["content_type"] = "image/tiff";
         
        args["ocr_data"] = ""; // Bug# 21072 DH

        args["doc_type"] = result.strDocumentType.toUpperCase();// Bug# 24736 DH

        get_top().main.post_remote('Business_app', 'Business_app.Create_core_item_app.digitalcheck_pertech_panini_devices_receive_images', args, 'pos_frame', true);
       }
       
       if(param)
          get_top().main.dequeue_message();
    }
    catch (e) 
    {
      alert("Unable to use device. Error: " + e.description);
      if(RemoveMessageAfterProcessing)
	    get_top().main.dequeue_message();
    }
  }

  var DigitalCheck_CallbackError = function(result, param/*param = remove UI message*/)
  { 
    if(param)
      get_top().main.dequeue_message();
      
    alert("Unable to process document: " + result );
    return -1;
  }
  
  if(digital_ocr_left == undefined || digital_ocr_left == "" || digital_ocr_left == "opt")
    digital_ocr_left = "0";
  if(digital_ocr_top == undefined || digital_ocr_top == "" || digital_ocr_top == "opt")
    digital_ocr_top = "0";
  if(digital_ocr_right == undefined || digital_ocr_right == "" || digital_ocr_right == "opt")
    digital_ocr_right = "0";
  if(digital_ocr_bottom == undefined || digital_ocr_bottom == "" || digital_ocr_bottom == "opt")
    digital_ocr_bottom = "0";
  
  var RemoveMessageAfterProcessingThis = true;// param
  AjaxCallPeripherals("digitalcheck_scan_doc/" + device_name + "/" + top_app + "/" + doc_side + "/" + doc_type + "/" + doc_resolution + "/" + scan_full_sheet + "/" + digital_ocr_left + "/" + digital_ocr_top + "/" + digital_ocr_right + "/" + digital_ocr_bottom, DigitalCheck_CallbackSuccess, DigitalCheck_CallbackError, RemoveMessageAfterProcessingThis, "");  
  
return 0;
}

//--------------------- end Bug# 16027 DH - DigitalCheck device support Added by DH --------------


//------------------------- // Bug# 17792 - Pertech scanner device support Added by Daniel Hofman ----------------
function Pertech_ScanDoc(device_name, top_app, doc_side, doc_type, doc_resolution, message, ocr_left, ocr_top, ocr_right, ocr_bottom, pertech_ocr_recognizer/* Bug# 18512 DH - Image Scholar recognizer engine type */, pertech_keep_dashes_in_micr/* Bug# 19178 DH - ICL requires original dashes in the MICR line */, aba_validation /*Bug 23366 NK */, enable_franking/*Bug# 22738 DH - Adding franking*/)
{
  // Pertech scanner support Added by DH
  g_aba_validation = aba_validation; //Bug 23366 NK
  g_aba_validation_error = false;    //Bug 23366 NK
  g_PertechEnableFranking = enable_franking == "YES" ? true : false;// Bug# 24109 DH
  g_bPertechProcessing = true;// Bug# 25201 DH - It does not need to be plugged in, if this doc is designated for Pertech, it is to be set as such.

  if (!g_IsPertechScannerInstalled)
  {
    // Bug# 17902 Comment#45 DH
    //alert("Pertech device [" + device_name + "] is not available. Please check the power and USB cables.");
    return -1;
  }
  
  if(doc_type == "NONE" && doc_side == "NONE")
  {
    alert("The document settings are incorrect. The side and document type are missing.");
    g_bPertechProcessing = false;// Bug# 25201 DH
    return -1;
  }
    
  if (message != "opt" && message != "" && message != undefined)
  {
    bRetVal = get_top().main.enqueue_message(message);
    if (bRetVal == false)
    {
      g_bPertechProcessing = false;// Bug# 25201 DH
      return false;
    }
  }
  
  setTimeout(function()
  {
    Pertech_ScanDocAsync(device_name, top_app, doc_side, doc_type, doc_resolution, ocr_left, ocr_top, ocr_right, ocr_bottom, pertech_ocr_recognizer/* Bug# 18512 DH - Image Scholar recognizer engine type */, pertech_keep_dashes_in_micr/* Bug# 19178 DH - ICL requires original dashes in the MICR line */, aba_validation /*Bug 23366 NK */, enable_franking/*Bug# 22738 DH - Adding franking*/);
  }, 10
  );
}

function Pertech_ScanDocAsync(device_name, top_app, doc_side, doc_type, doc_resolution, ocr_left, ocr_top, ocr_right, ocr_bottom, pertech_ocr_recognizer/* Bug# 18512 DH - Image Scholar recognizer engine type */, pertech_keep_dashes_in_micr/* Bug# 19178 DH - ICL requires original dashes in the MICR line */, aba_validation /*Bug 23366 NK */, enable_franking/*Bug# 22738 DH - Adding franking*/)
{
  // Bug# 17792 DH.
  
    var PertechScanner_CallbackSuccess = function (result, param/*param = remove UI message*/)
    {
        try
        {
            if (result == null || result == undefined || result.iLastErrorCode != 0)
            {
                var s = " ";
                if (result != null && result != undefined)
                {
                    s = result.strLastErrorMessage;

                  // Bug# 27506 DH
                  if (result.strLastErrorMessage.indexOf("User Canceled Swipe") == -1 && result.strLastErrorMessage.indexOf("User Canceled Scan") == -1)// Bug# 17902 Comment#12 DH - User canceled
                      if (s != "Scanner not connected")// Bug# 23160 DH
                        alert("Error processing document. " + s);
                }

              if (param)
              {
                AjaxCallPeripherals("pertech_eject_doc", "", "", "", ""); // Bug# 27340 DH
                get_top().main.dequeue_message();
              }

            g_bPertechProcessing = false;// Bug# 25201 DH
            return;
            }

      // Probably Canceled by the user. This is one case that will not set the strCashieringScreenObject.
      if (result.strCashieringScreenObject == null)
        return;

      var app = result.strCashieringScreenObject;
      if (app.substr(0, 3) == "GEO")
        app = app.substr(4);
      else if (app == "")
        app = get_top().main.get_current_obj();

      // Bug# 22965 DH
      // Bug# 21263 DH
      doc_side = doc_side.toUpperCase();

      if (result.strImage_BACK_DataString == null)
        result.strImage_BACK_DataString = "";
      if (result.strImage_FRONT_DataString == null)
        result.strImage_FRONT_DataString = "";
      // end Bug# 21263 DH
      
      // BUG# 20506 DH
      // Bug# 21263 DH
      if (doc_side == "FRONT")// Bug# 21263 DH
      {
        result.strImage_BACK_DataString = "";

        if (result.strImage_FRONT_DataString == "")
        {
          alert("Error! \n\nUnable to retrieve image from device." + "\n\nPlease try again.");
          if (param)
            get_top().main.dequeue_message();

          g_bPertechProcessing = false;// Bug# 25201 DH
          return -1;
        }
      }

      if (doc_side == "BACK")
      {
        result.strImage_BACK_DataString = "";

        if (result.strImage_FRONT_DataString == "")// Front has the back image
        {
          alert("Error! \n\nUnable to retrieve image from device." + "\n\nPlease try again.");
          if (param)
            get_top().main.dequeue_message();

          g_bPertechProcessing = false;// Bug# 25201 DH
          return -1;
        }
      }
      // end Bug# 21263 DH

      if (doc_side.indexOf("FRONT") !== -1 && doc_side.indexOf("BACK") !== -1)// Bug# 22965 && 23195 DH
      {
        if (result.strImage_FRONT_DataString == "" || result.strImage_back_DataString == "")
        {
          alert("Error! \n\nUnable to retrieve image from device." + "\n\nPlease try again.");
          if (param)
            get_top().main.dequeue_message();

          g_bPertechProcessing = false;// Bug# 25201 DH
          return -1;
        }
      }
      // end BUG# 20506 DH
      // end Bug# 22965 DH

      if (result.strDocumentType.toUpperCase() == "NONE") 
      {
        // Images only
      }
      else if (result.strDocumentType.toUpperCase() == "MICR") 
      {
        // ICL validation for check types, should happen in the CASL side
        // MICR should always have a check image with it as well. Handeled in the last step.       
        
        // Bug# 21800 DH
        if(result == null ||
          result.strPertechCheckNumber == null || result.strPertechCheckNumber == undefined || result.strPertechCheckNumber == "" ||
          result.strPertechRoutingNumber == null || result.strPertechRoutingNumber == undefined || result.strPertechRoutingNumber == "" ||
          result.strPertechBankNumber == null || result.strPertechBankNumber == undefined || result.strPertechBankNumber == "" ||
          result.str_Pertech_Raw_MICR == null || result.str_Pertech_Raw_MICR == undefined || result.str_Pertech_Raw_MICR == "")
        {
          alert("Invalid scan. Please try again.");
          if (param)
            get_top().main.dequeue_message();

          g_bPertechProcessing = false;// Bug# 25201 DH
          return -1;
        }

        len = result.strPertechCheckNumber.length + result.strPertechRoutingNumber.length + result.strPertechBankNumber.length;
                
        if(result.strPertechCheckNumber.length < 1 || result.strPertechRoutingNumber.length < 9 || result.strPertechBankNumber.length < 1 || result.str_Pertech_Raw_MICR.length < len || 
          IsValidMICR(result.str_Pertech_Raw_MICR) != true)
        {
          alert("Invalid scan. Please try again.");
         if (param)
            get_top().main.dequeue_message();

          g_bPertechProcessing = false;// Bug# 25201 DH
          return -1;
        }
        // end Bug# 21800 DH

        // Bug 21791 DH
        if (result.strPertechCheckType != undefined)
        {
          if (g_isValidateACH && result.strPertechCheckType == "2")
          {
            alert("Commercial Check: Unable to convert to ACH");
            RaiseErrorEvent(1, -1, "Commercial Check: Unable to convert to ACH");
            get_top().main.dequeue_message();
            AjaxCallPeripherals("pertech_frank_check/" + "false", "", "", "", "");// Bug 24108 DH - False will eject the check instead of franking.
            g_bPertechProcessing = false;// Bug# 25201 DH
            return -1;
          }
        }
        // end Bug 21791 DH

        Device_Micr_receive({
          app: GetCurrentObject(),
          routing_number: result.strPertechRoutingNumber,
          bank_account_number: result.strPertechBankNumber,
          check_number: result.strPertechCheckNumber,
          raw_micr: result.str_Pertech_Raw_MICR,
          country_code: "",
          check_type: result.strPertechCheckType, // Bug# 18237 DH. - Added check type for this bug as well. 
          check_epc: result.strPertechEPC == undefined ? "" : result.strPertechEPC,// Bug 24188 DH - Develop code for parsing out External Processing Code (EPC) from check MICR
          check_amount: "",
          bank_number: "",
          on_us: result.strPertechOnUs == undefined ? "" : result.strPertechOnUs/*Bug# 25656 DH*/,// Bug# 25154 DH, Bug# 26183 DH
          aba_validation: g_aba_validation, // Bug 23366 NK
          aux_on_us: result.strPertechAuxOnUs == undefined ? "" : result.strPertechAuxOnUs/*Bug# 25656 DH*/ // Bug# 25093 DH, Bug# 26183 DH
        }); 
      }
      else if (result.strDocumentType.toUpperCase() == "OCR") {
        Device_Scanner_receive_ocr({ app: app, ocr_data: result.strOCR, ocr_data_error: "" });
      }

      // This could be an ID card as well.
      if (result.strImage_FRONT_DataString != "" || result.strImage_BACK_DataString != "") 
      {
        args                        = new Array();
        args["app"]                 = GetCurrentObject();
        args["content_image_front"] = result.strImage_FRONT_DataString;
        args["content_image_back"]  = result.strImage_BACK_DataString;
        args["image_front_tag"]     = "PERTECHSCANNER_FRONT_IMAGE";
        args["image_back_tag"]      = "PERTECHSCANNER_BACK_IMAGE";

        if (result.strDocumentSide.toUpperCase() == "CARD IMAGE")// Color scans
          args["content_type"] = "image/jpeg";
        else
          args["content_type"] = "image/tiff";

        args["ocr_data"] = ""; // Bug# 21072 DH

        args["doc_type"] = result.strDocumentType;// Bug# 24736 DH

        // Bug# 22486 DH
        if (g_doc_count == undefined || g_doc_count == "1")
          get_top().main.post_remote('Business_app', 'Business_app.Create_core_item_app.digitalcheck_pertech_panini_devices_receive_images', args, 'main', true);
        else
        {
          if (g_iCurrent_document_index == g_iNbr_of_documents_to_process)
            get_top().main.post_remote('Business_app', 'Business_app.Create_core_item_app.digitalcheck_pertech_panini_devices_receive_images', args, 'main', true);
          else
            get_top().main.post_remote('Business_app', 'Business_app.Create_core_item_app.digitalcheck_pertech_panini_devices_receive_images', args, 'pos_frame', true);
        }
        // end Bug# 22486 DH

        delete args;// Bug# 22245 & 22371 DH
      }

          if (param)
          {
            get_top().main.dequeue_message();
            if (enable_franking.toUpperCase() != "YES")
            AjaxCallPeripherals("pertech_eject_doc", "", "", "", ""); // Bug# 25499 DH
          }
    }
    catch (e) 
    {
      alert("Unable to use device. Error: " + e.description);
      if (RemoveMessageAfterProcessing)
        get_top().main.dequeue_message();
    }
  }

  var PertechScanner_CallbackError = function(result, param/*param = remove UI message*/)
  { 
    if(param)
      get_top().main.dequeue_message();
      
    alert("Unable to process document: " + result);
    g_bPertechProcessing = false;// Bug# 25201 DH
    return -1;
  }
  
  if(ocr_left == undefined || ocr_left == "" || ocr_left == "opt")
    ocr_left = "0";
  if(ocr_top == undefined || ocr_top == "" || ocr_top == "opt")
    ocr_top = "0";
  if(ocr_right == undefined || ocr_right == "" || ocr_right == "opt")
    ocr_right = "0";
  if(ocr_bottom == undefined || ocr_bottom == "" || ocr_bottom == "opt")
    ocr_bottom = "0";
  
  /* Bug# 18512 DH - Image Scholar recognizer engine type */
  if(pertech_ocr_recognizer == undefined || pertech_ocr_recognizer == "" || pertech_ocr_recognizer == null || pertech_ocr_recognizer == "opt" || pertech_ocr_recognizer == "GEO.opt")
    pertech_ocr_recognizer = "empty";
  
  /* Bug# 19178 DH - ICL requires original dashes in the MICR line */
  if(pertech_keep_dashes_in_micr == undefined || pertech_keep_dashes_in_micr == "" || pertech_keep_dashes_in_micr == null || pertech_keep_dashes_in_micr == "opt" || pertech_keep_dashes_in_micr == "GEO.opt")
    pertech_keep_dashes_in_micr = "empty";
    
  var RemoveMessageAfterProcessingThis = true;

  // Bug# 22731 - DH - Pertech 6100: Regular Imaging Resulted in 404 error
  if (device_name == "" || device_name == "opt" || device_name == undefined)
      device_name = "Pertech 6100";
  // end Bug# 22731

  // Bug# 22738 DH - Adding franking
  if (enable_franking == undefined || enable_franking == "" || enable_franking == null || enable_franking == "opt" || enable_franking == "GEO.opt")
    enable_franking = "EMPTY";
  // end Bug# 22738 DH

  AjaxCallPeripherals("pertech_scanner_scan_doc/" + device_name + "/" + top_app + "/" + doc_side + "/" + doc_type + "/" + doc_resolution + "/" + ocr_left + "/" + ocr_top + "/" + ocr_right + "/" + ocr_bottom + "/" + pertech_ocr_recognizer + "/" + pertech_keep_dashes_in_micr/* Bug# 19178 DH - ICL requires original dashes in the MICR line */ + "/" + enable_franking/*Bug# 22738 DH - Adding franking*/, PertechScanner_CallbackSuccess, PertechScanner_CallbackError, RemoveMessageAfterProcessingThis, "");  
  
return 0;
}

function PERTECH_Reset()
{
    // Bug# 17792 DH
    // Bug# 17902 Comment#12 DH - User canceled
    // Bug# 19005 DH
    AjaxCallPeripherals("pertech_cancel_scan", PANINI_Reset, "", "", "");// This is the last call in reset/cancel chain.
}
//--------------------- end DH - PertechScanner device support Added by DH --------------

function IsValidMICR(s)
{
    // Bug# 19719, 19847 and 21800 DH

  var v = " -$ot0123456789";// Bug# 25162 DH
    var c;
    var rv = true;

    if (s.length == 0) 
    return false;
    for (i = 0; i < s.length && rv == true; i++)
    {
    c = s.charAt(i);
    if (v.indexOf(c) == -1)
        rv = false;
    }
    return rv;
}

function TerminateRequestsAtDevices(document_type)
{
  // Bug# 17792 DH
  // Also, start cancelling other devices one after the other in a chained manner on succesfull returns.
  
  // Bug# 20384 DH
  if (g_IsPertechScannerInstalled && document_type == "H")
    top.main.enqueue_message({ content: "<BR/><CENTER>Processing. Please wait...</CENTER>", actions: {} });// Bug# 19077 DH

  // Bug 26420 DH - Presence of a DigitalCheck driver or certificate interferes with the peripheral service
  PERTECH_Reset();
}
//--------------------- Bug# 19005 DH - Panini Device --------------------------------------------

function Panini_CancelScan()
{
  // Bug# 21072 DH
  AjaxCallPeripherals("panini_cancel_scan", "", "", "", "");
  get_top().main.dequeue_message();

  args = new Array();
  // Bug 22822 DH - Panini: canceling a document scan results in a log error.
  get_top().main.post_remote('Business_app', 'Business_app.Create_core_item_app.finalize_multiple_pages', args, 'main', true);

  delete args;
}

// Bug# 22799 DH
var g_app;
var g_device_name;
var g_doc_type;
var g_doc_side;
var g_doc_resolution;
var g_message;
var g_keep_dashes_in_micr;
var g_enable_franking;
var g_ocr_x;
var g_ocr_y;
var g_ocr_width;
var g_ocr_height;
// end Bug# 22799 DH

// Bug# 22534 DH
var gUserPrompt;
// end Bug# 22534 DH

function Panini_PromptDialog_YES()
{
    // Bug# 22799 DH
    get_top().main.dequeue_message();
    Panini_Scan(g_device_name, g_app, g_doc_type, g_doc_side, g_doc_resolution, g_message, g_keep_dashes_in_micr, g_enable_franking, g_ocr_x, g_ocr_y, g_ocr_width, g_ocr_height) 
}

function Panini_PromptDialog_NO()
{
    // Bug# 22799 DH
    args = new Array();
    get_top().main.post_remote('Business_app', 'Business_app.Create_core_item_app.finalize_multiple_pages', args, 'main', true);
    get_top().main.dequeue_message();
}

function GetNextPaniniPage()
{
    // Bug# 22799 DH
    var yes_btn = "<button onclick=get_pos().Panini_PromptDialog_YES('')> Yes </button>";
    var no_btn = "<button onclick=get_pos().Panini_PromptDialog_NO('')> No </button>";
    get_top().main.enqueue_message({ content: "<CENTER>Would you like to scan another page?</CENTER><BR/>" + yes_btn + no_btn, actions: {} });
}

function Panini_Scan(device_name, app, doc_type, doc_side, doc_resolution, message, keep_dashes_in_micr, enable_franking, ocr_x, ocr_y, ocr_width, ocr_height, aba_validation, endorsement_message/*Bug 21678 DH - Endorsement printing*/)// Bug# 21072 DH  // Bug 23367 NK 
{
  // Bug# 19005 DH - Panini Device
    g_PaniniImages = null;// Bug# 22245 & 22371 DH
    g_aba_validation = aba_validation; //Bug 23367 NK
    g_aba_validation_error = false;    //Bug 23367 NK

    if (!g_IsPaniniScannerInstalled) {
        return -1;
    }

    if (doc_type == "NONE" && doc_side == "NONE") {
        alert("The document settings are incorrect. The side and document type are missing.");
        return -1;
    }
    
    // Bug# 21072 DH
    if (message != "opt" && message != "" && message != undefined) 
    {
      gUserPrompt = message;// Bug# 22534 DH
      cancel_btn = "<button onclick=get_pos().Panini_CancelScan()>Cancel</button>";

      // Bug# 25332 DH - Multi-doc support: Panini scan followed by Epson scan results in error, loss of prompt
      if (g_doc_count > 1)
      {
        if (g_iCurrent_document_index == 1)// Bug 25333 DH - Multi - doc support: Epson scan followed by Panini scan results in error, loss of prompt
          get_top().main.enqueue_message({ content: "<CENTER>" + message + "</CENTER>" + "<BR/>" + cancel_btn, actions: {} });
        else 
        {
          setTimeout(function ()
          {
            get_top().main.enqueue_message({ content: "<CENTER>" + message + "</CENTER>" + "<BR/>" + cancel_btn, actions: {} });
          }, 2000);
        }
      }
      else
      {
        // Bug# 25997 DH
        // Wait a bit longer for the download to finish. Otherwise the first scan will lose it's prompt.
        var WaitForDownload = 10;
        if (g_aba_loaded == false)
          WaitForDownload = 3000;

        setTimeout(function ()
        {
          get_top().main.enqueue_message({ content: "<CENTER>" + message + "</CENTER>" + "<BR/>" + cancel_btn, actions: {} });
        }, WaitForDownload);
        // end Bug# 25332 DH
      }
      // end Bug# 25997 DH
    }
    // end Bug# 21072 DH

    //--------------------------------------------------------------
    // Bug# 25117 DH - Panini: ABA validation failure, log error for first tender attempt after peripheral service starts
    // If we logged in or reset peripherals this file will be forced. 
    setTimeout(function ()
    {
      if (g_aba_loaded == false)
      {
        aba_data_uploaded = false;
        ForceABADownload();
        g_aba_loaded = true;
      }
    }, 10);
  

    var tryagain = function ()
    {
      setTimeout(function ()
      {
        if (aba_data_uploaded) // Bug# 25117 DH - Wait for the service to finish processing the aba validation data.
          Panini_ScanAsync(device_name, app, doc_type, doc_side, doc_resolution, message, keep_dashes_in_micr, enable_franking, ocr_x, ocr_y, ocr_width, ocr_height, aba_validation, endorsement_message/*Bug 21678 DH - Endorsement printing*/)// Bug# 21072 DH // Bug 23367 NK
          else
            tryagain();
      }, 500);
    }
    tryagain();
  // end Bug# 25117 DH
  //--------------------------------------------------------------
}

function Panini_ScanAsync(device_name, app, doc_type, doc_side, doc_resolution, message, keep_dashes_in_micr, enable_franking, ocr_x, ocr_y, ocr_width, ocr_height, aba_validation, endorsement_message/*Bug 21678 DH - Endorsement printing*/)// Bug# 21072 DH  // Bug 23367 NK
{
    // Bug# 19005 DH - Panini Device

  doc_side = doc_side.toUpperCase();// Bug# 22965 DH

    // Bug# 21072 DH
    // Bug# 22965 DH
    if(doc_type != "FULL SHEET")// MICR and Multiple Page docs cannot use the OCR functionality.
    {
      ocr_x = ""
      ocr_y = ""
      ocr_width = ""
      ocr_height = ""
    }
    
    if (ocr_x == undefined || ocr_x == "" || ocr_x == null || ocr_x == "opt" || ocr_x == "GEO.opt")
      ocr_x = "EMPTY";
    
    if (ocr_y == undefined || ocr_y == "" || ocr_y == null || ocr_y == "opt" || ocr_y == "GEO.opt")
      ocr_y = "EMPTY";

    if (ocr_width == undefined || ocr_width == "" || ocr_width == null || ocr_width == "opt" || ocr_width == "GEO.opt")
      ocr_width = "EMPTY";

      if (ocr_height == undefined || ocr_height == "" || ocr_height == null || ocr_height == "opt" || ocr_height == "GEO.opt")
      ocr_height = "EMPTY";
    // end Bug# 21072 DH

    var PaniniScanner_CallbackSuccess = function (result, param) {
        try {
            if (result == null || result == undefined || result.iLastErrorCode != 0) {
                var s = " ";
                if (result != null && result != undefined) {
                  s = result.strLastErrorMessage;

                    if (result.strLastErrorMessage != "Canceled by user") {
                        alert("Error processing document. " + s);
                    }
                    // Bug# 22761 DH
                    else if (result.strLastErrorMessage == "Canceled by user")
                    {
                      // Bug# 24172 DH - ABA validation.
                      // Maybe the service was reloaded.
                      if (result.OtherMessage == "download_aba_data")
                      {
                        alert("No ABA data available for this scan.\n\nPlease scan again.");
                        ForceABADownload();
                      }
                      // End Bug# 24172 DH - ABA validation.

                        try{
                            get_top().main.dequeue_message();}
                        catch (e)
                        {
                                if (e.description.indexOf("dequeue_message") == -1) 
                                    return;// Ignore it, one too many removes is ok.
                        }
                        return;
                    }
                    // end Bug# 22761 DH
                }

              // Bug# 22245 & 22371 DH
              alert("Invalid scan.\n\nPlease try again.");// Change to the second line so we know which error in case users report it.

                if (param)
                    get_top().main.dequeue_message();
                return;
            }

            if (result.strCashieringScreenObject == null)
                return;

            var app = result.strCashieringScreenObject;
            if (app.substr(0, 3) == "GEO")
                app = app.substr(4);
            else if (app == "")
                app = get_top().main.get_current_obj();


          if (doc_type == "NONE") // Bug# 22965 DH
          {
            // Nothing here to sub
          }
            
          if (doc_type == "MICR") // Bug# 22965 DH
          {
            // Bug# 21800 DH
            if(result == null ||
              result.strPaniniCheckNumber == null || result.strPaniniCheckNumber == undefined || result.strPaniniCheckNumber == "" ||
              result.strPaniniRoutingNumber == null || result.strPaniniRoutingNumber == undefined || result.strPaniniRoutingNumber == "" ||
              result.strPaniniBankNumber == null || result.strPaniniBankNumber == undefined || result.strPaniniBankNumber == "" ||
              result.strPaniniRawMICR == null || result.strPaniniRawMICR == undefined || result.strPaniniRawMICR == "")
            {
              alert("Invalid scan. Please try again.");
              if (param)
                  get_top().main.dequeue_message();
              return -1;
            }
            
            len = result.strPaniniCheckNumber.length + result.strPaniniRoutingNumber.length + result.strPaniniBankNumber.length;

            if(result.strPaniniCheckNumber.length < 1 || result.strPaniniRoutingNumber.length < 9 || result.strPaniniBankNumber.length < 1 || result.strPaniniRawMICR < len || 
               IsValidMICR(result.strPaniniRawMICR) != true)// Bug# 19005 & 21800 - DH
            {
              alert("Invalid scan. Please try again.");
              if (param)
                  get_top().main.dequeue_message();
              return -1;
            }


            // Bug 21791 DH
            if (result.strPaniniCheckType != undefined)
            {
              if (g_isValidateACH && result.strPaniniCheckType === "2")
              {
                alert("Commercial Check: Unable to convert to ACH");
                RaiseErrorEvent(1, -1, "Commercial Check: Unable to convert to ACH");
                get_top().main.dequeue_message();
                EpsonRemoveDocument("");
                return -1;
              }
            }
            // end Bug 21791 DH

            // end Bug# 21800 DH
            Device_Micr_receive({
                app: GetCurrentObject(),
                routing_number: result.strPaniniRoutingNumber,
                bank_account_number: result.strPaniniBankNumber,
                check_number: result.strPaniniCheckNumber,
                raw_micr: result.strPaniniRawMICR,
                country_code: "",
                check_type: result.strPaniniCheckType,
                check_epc: result.strPaniniEPC == undefined ? "" : result.strPaniniEPC,// Bug 24188 DH - Develop code for parsing out External Processing Code (EPC) from check MICR
                check_amount: "",
                bank_number: "",
              on_us: result.strPaniniOnUs == undefined ? "" : result.strPaniniOnUs/*Bug# 25656 DH*/,// Bug# 25154, Bug# 26183 DH
                aba_validation: g_aba_validation, // Bug 23367 NK
              aux_on_us: result.strPaniniAuxOnUs == undefined ? "" : result.strPaniniAuxOnUs/*Bug# 25656 DH*/// Bug# 25093 DH, Bug# 26183 DH
            });
            
            // Bug# 22245 & 22371 DH
            doc_side = doc_side.toUpperCase();// Bug# 23194 DH
            if (result.strImage_BACK_DataString == null)
              result.strImage_BACK_DataString = "";
            if (result.strImage_FRONT_DataString == null)
              result.strImage_FRONT_DataString = "";

            if (doc_side == "FRONT")// Bug# 22965 DH
            {
              result.strImage_BACK_DataString = "";

              if (result.strImage_FRONT_DataString == "")
              {
                  alert("Error! \n\nUnable to retrieve image from device." + "\n\nPlease try again.");
                  if (param)
                      get_top().main.dequeue_message();
                  return -1;
              }
            }

            if (doc_side == "BACK")// Bug# 22965 DH
            {
              // Bug 25106 - Panini: check document image sides are retrieved incorrectly
              // Back image must go into the front position so we do not interrupt any code downstream.
              // There is new code in the database submision block to make it properly in the table so the ICL can then pickup the correct one.
              result.strImage_FRONT_DataString = result.strImage_BACK_DataString;
              result.strImage_BACK_DataString = "";

              if (result.strImage_FRONT_DataString == "")// Bug 25106 - Panini: check document image sides are retrieved incorrectly
              {
                  alert("Error! \n\nUnable to retrieve image from device." + "\n\nPlease try again.");
                  if (param)
                      get_top().main.dequeue_message();
                  return -1;
              }
            }

            if (doc_side.indexOf("FRONT") !== -1 && doc_side.indexOf("BACK") !== -1)// Bug# 22965 && 23195 DH
            {
              if (result.strImage_FRONT_DataString == "" || result.strImage_BACK_DataString == "")
              {
                  alert("Error! \n\nUnable to retrieve image from device." + "\n\nPlease try again.");
                  if (param)
                      get_top().main.dequeue_message();
                  return -1;
              }
            }

            // Bug 25106 - Panini: check document image sides are retrieved incorrectly
            if (doc_side == "NONE")
            {
              result.strImage_FRONT_DataString = "";
              result.strImage_BACK_DataString = "";
            }
            // Bug 25106

            if (result.strImage_FRONT_DataString != "" || result.strImage_BACK_DataString != "") 
            {
              args                        = new Array();
              args["app"]                 = GetCurrentObject();
              args["content_image_front"] = result.strImage_FRONT_DataString;
              args["content_image_back"]  = result.strImage_BACK_DataString;
              args["content_type"]        = "image/tiff";
              args["image_front_tag"]     = "PANINI_FRONT_IMAGE";
              args["image_back_tag"]      = "PANINI_BACK_IMAGE";
              args["ocr_data"]            = "";
              args["doc_type"]            = doc_type;

              g_PaniniImages = args;// This will be used for a call back from the server after the MICR is submitted.
              delete args;
            }

            if (param)
              get_top().main.dequeue_message();

            return;
            }
            // end Bug# 22245 & 22371 DH
            
          // Bug# 21072 DH
          // Bug# 22965 DH
          if (doc_type == "FULL SHEET" || doc_type == "CARD IMAGE")// Bug# 22371 DH - Remove the MICR/check type images or they will be submitted twice.
          {
              var content_type = "image/tiff";
              if(doc_type.toUpperCase() == "CARD IMAGE")
                content_type = "image/bmp";             


            // Bug# 23195 DH
            if (doc_side == "FRONT")
            {
              result.strImage_BACK_DataString = "";

              if (result.strImage_FRONT_DataString == "")
              {
                alert("Error! \n\nUnable to retrieve image from device." + "\n\nPlease try again.");
                if (param)
                  get_top().main.dequeue_message();
                return -1;
              }
            }

            if (doc_side == "BACK")
            {
              result.strImage_BACK_DataString = "";

              if (result.strImage_FRONT_DataString == "")// Front has the back image
              {
                alert("Error! \n\nUnable to retrieve image from device." + "\n\nPlease try again.");
                if (param)
                  get_top().main.dequeue_message();
                return -1;
              }
            }

            if (doc_side.indexOf("FRONT") !== -1 && doc_side.indexOf("BACK") !== -1)
            {
              if (result.strImage_FRONT_DataString == "" || result.strImage_back_DataString == "")
              {
                alert("Error! \n\nUnable to retrieve image from device." + "\n\nPlease try again.");
                if (param)
                  get_top().main.dequeue_message();
                return -1;
              }
            }
            // end Bug# 23195 DH
            
              if (doc_side == "NONE")// Bug# 22965 DH
                SubmitPaniniImages("", "", content_type, app, result.strPaniniOCR, doc_type.toUpperCase());
              else if (doc_side == "FRONT")// Bug# 22965 DH
                SubmitPaniniImages(result.strImage_FRONT_DataString, "", content_type, app, result.strPaniniOCR, doc_type.toUpperCase());
              else if (doc_side == "BACK")// Bug# 22965 DH
                SubmitPaniniImages("", result.strImage_BACK_DataString, content_type, app, result.strPaniniOCR, doc_type.toUpperCase());
              else
                SubmitPaniniImages(result.strImage_FRONT_DataString, result.strImage_BACK_DataString, content_type, app, result.strPaniniOCR, doc_type);
              
              get_top().main.dequeue_message();
              return;
		  }	

        if (doc_type == "MULTIPLE PAGES")// Bug# 22965 DH
        {
            // Bug# 23195 DH
            if (doc_side == "FRONT")
            {
              result.strImage_BACK_DataString = "";

              if (result.strImage_FRONT_DataString == "")
              {
                alert("Error! \n\nUnable to retrieve image from device." + "\n\nPlease try again.");
                if (param)
                  get_top().main.dequeue_message();
                return -1;
              }
            }

            if (doc_side == "BACK")
            {
              result.strImage_BACK_DataString = "";

              if (result.strImage_FRONT_DataString == "")// Front has the back image
              {
                alert("Error! \n\nUnable to retrieve image from device." + "\n\nPlease try again.");
                if (param)
                  get_top().main.dequeue_message();
                return -1;
              }
            }

            if (doc_side.indexOf("FRONT") !== -1 && doc_side.indexOf("BACK") !== -1)
            {
              if (result.strImage_FRONT_DataString == "" || result.strImage_back_DataString == "")
              {
                alert("Error! \n\nUnable to retrieve image from device." + "\n\nPlease try again.");
                if (param)
                  get_top().main.dequeue_message();
                return -1;
              }
            }
            // end Bug# 23195 DH

            // Bug# 22799 DH
            g_app                 = app;
            g_device_name         = device_name;
            g_doc_type            = doc_type;
            g_doc_side            = doc_side;
            g_doc_resolution      = doc_resolution;
            g_message             = message;
            g_keep_dashes_in_micr = keep_dashes_in_micr;
            g_enable_franking     = enable_franking;
            g_ocr_x               = ocr_x;
            g_ocr_y               = ocr_y;
            g_ocr_width           = ocr_width;
            g_ocr_height          = ocr_height;
            content_type          = "image/tiff";// BUG# 22486 DH - Always the same for this type
            
            SubmitPaniniImages(result.strImage_FRONT_DataString, result.strImage_BACK_DataString, content_type, app, result.strPaniniOCR, "MULTIPLE PAGES");
            return;
            // Bug# 22799 DH
		    }	
        // end Bug# 21072 DH
        }
        catch (e)
        {
            alert("Unable to use device. Error: " + e.description);
            if (RemoveMessageAfterProcessing)
                get_top().main.dequeue_message();
        }
    }

    var Panini_CallbackError = function (result, param) {
        if (param)
            get_top().main.dequeue_message();

        alert("Unable to scan document: " + result);
        return -1;
    }

    if (keep_dashes_in_micr == undefined || keep_dashes_in_micr == "" || keep_dashes_in_micr == null || keep_dashes_in_micr == "opt" || keep_dashes_in_micr == "GEO.opt")
        keep_dashes_in_micr = "EMPTY";

    if (enable_franking == undefined || enable_franking == "" || enable_franking == null || enable_franking == "opt" || enable_franking == "GEO.opt")
        enable_franking = "EMPTY";

    var RemoveMessageAfterProcessingThis = true;

    // Bug# 22736 - DH
    if (device_name == "" || device_name == "opt" || device_name == undefined)
        device_name = "WI";
    // end Bug# 22736

    // Bug# 21072 DH
    if (doc_type == "MULTIPLE PAGES")// Bug# 22965 DH
    {
      // Bug 21678 DH - Endorsement printing*/
      // Bug# 24172 DH - Enable ABA validation
      var aba_validate = "FALSE";
      // Bug 25451 NK - Panini: multi-page scanning not working*/
      if (endorsement_message == undefined || endorsement_message == "" || endorsement_message == null || endorsement_message == "opt" || endorsement_message == "GEO.opt")
        endorsement_message = "EMPTY";
      AjaxCallPeripherals("panini_scan_doc/" + device_name + "/" + app + "/" + "FULL SHEET" + "/" + doc_side + "/" + doc_resolution + "/" + keep_dashes_in_micr + "/" + enable_franking + "/" + ocr_x + "/" + ocr_y + "/" + ocr_width + "/" + ocr_height + "/" + endorsement_message + "/" + aba_validate, PaniniScanner_CallbackSuccess, Panini_CallbackError, RemoveMessageAfterProcessingThis, "");
    }
    else
    {
      // Bug 23429 DH
      // Lets just reuse this flag to keep things backward compatible.
      if (g_isValidateACH && enable_franking === "YES")
        enable_franking += "dont_frank_commercial_checks";
      // Bug 23429 DH

      // Bug 21678 DH - Endorsement printing*/
      if (endorsement_message == undefined || endorsement_message == "" || endorsement_message == null || endorsement_message == "opt" || endorsement_message == "GEO.opt")
        endorsement_message = "EMPTY";

      var aba_validate = g_aba_validation == true ? "TRUE" : "FALSE";// Bug# 24172 DH - Enable ABA validation
      AjaxCallPeripherals("panini_scan_doc/" + device_name + "/" + app + "/" + doc_type + "/" + doc_side + "/" + doc_resolution + "/" + keep_dashes_in_micr + "/" + enable_franking + "/" + ocr_x + "/" + ocr_y + "/" + ocr_width + "/" + ocr_height + "/" + endorsement_message + "/" + aba_validate, PaniniScanner_CallbackSuccess, Panini_CallbackError, RemoveMessageAfterProcessingThis, "");// Bug 21678 DH - Endorsement printing*/
    }
    // end Bug# 21072 DH

    return 0;
}

function GetPaniniCheckImages()// Bug# 22245 & 22371 DH
{
  if(g_PaniniImages != undefined && g_PaniniImages != null)
  {
    if(g_PaniniImages["doc_type"] == "MICR")// Make sure this is what we think it is.
    {
      get_top().main.post_remote('Business_app', 'Business_app.Create_core_item_app.digitalcheck_pertech_panini_devices_receive_images', g_PaniniImages, 'main', true); //Bug 22798 NK
      g_PaniniImages = null;
    }
  }
}

function SubmitPaniniImages(strImageFront, strImageBack, content_type, app, strPaniniOCR, doc_type)
{
  // Bug# 21072 DH
  args                        = new Array();
  args["content_image_front"] = strImageFront;
  args["content_image_back"]  = strImageBack;
  args["content_type"]        = content_type;
  args["image_front_tag"]     = "PANINI_FRONT_IMAGE";
  args["image_back_tag"]      = "PANINI_BACK_IMAGE";
  args["ocr_data"]            = strPaniniOCR;
  args["app"]                 = app;
  args["doc_type"]            = doc_type;
  
  if (doc_type.toUpperCase() == "MULTIPLE PAGES")
    get_top().main.post_remote('Business_app', 'Business_app.Create_core_item_app.digitalcheck_pertech_panini_devices_receive_images', args, 'main'); // Bug #19440 NK Support for OCR parsing
  else
  {
    // Bug 25333 DH - Multi - doc support: Epson scan followed by Panini scan results in error, loss of prompt
    var frame = "main";
    if (g_doc_count > 1)
    {
      if (g_iCurrent_document_index == g_iNbr_of_documents_to_process)// Bug# 5940 DH
        frame = "main";
      else
        frame = "pos_frame";
    }

    get_top().main.post_remote('Business_app', 'Business_app.Create_core_item_app.digitalcheck_pertech_panini_devices_receive_images', args, frame, true);// Bug #19440 NK Support for OCR parsing
    // end Bug 25333 DH
  }
  
  delete args;
  return;
}

function PANINI_Reset() {
    // Bug# 19005 DH - Panini Device
    AjaxCallPeripherals("panini_cancel_scan", "", "", "", "");// This is the last call in reset/cancel chain. Chain started by TerminateRequestsAtDevices.
}
//--------------------- end Bug# 19005 DH - Panini Device -----------------------------------------

//------------------------------ Action Bar OCR Scanline for Bug# 21370 - Daniel Hofman ----------------------------------

function DocOCR_ScanAsync(prompt, document_type, device_name, ocr_coordinate_1, ocr_coordinate_2, ocr_coordinate_3, ocr_coordinate_4, pertech_ocr_recognizer /*Bug 25830 DH*/)
{
  // Bug# 21370 DH
  var endorsement_message = "EMPTY"; //Bug 25452 NK OCR-as-Barcode not wokring

  if (prompt != "opt" && prompt != "" && prompt != undefined) 
  {
    var cancel_btn = "<button onclick=get_pos().DocOCR_CancelScan(" + "'" + document_type + "'" + ")>Cancel</button>";
    get_top().main.enqueue_message({ content: "<CENTER>" + prompt + "</CENTER>" + "<BR/>" + cancel_btn, actions: {} });
  }
  
  var DeviceCallbackError = function (result, param) 
  {
    get_top().main.dequeue_message();
    alert("Error scanning document: " + result);
    return;
  }

  var DeviceCallbackSuccess = function (result, param) 
  {
    try 
    {
      if (result == null || result == undefined || result.iLastErrorCode != 0) 
      {
        var s = " ";
        if (result != null && result != undefined) 
        {
            s = result.strLastErrorMessage;

            if (result.strLastErrorMessage != "Canceled by user")
                alert("Error processing document. " + s);
        }
        get_top().main.dequeue_message();
        return;
      }

      // Process the OCR data for specific device here
      // Bug# 25830 - Provide the ability to scan a document OCR using a Pertech device and feed it into the existing barcode process
      if (document_type == "N" || document_type == "D" || document_type == "H")// Panini or Epson - Bug# 25829 DH - Provide the ability to scan a document OCR using an Epson device and feed it into the existing barcode process
      {
        // Bug# 25829 DH
        var sv = "~";
        if (document_type == "N")
          sv += result.strPaniniOCR;
        else if (document_type == "D" || document_type == "H")// // Bug# 25830 DH - Both using the same member
          sv += result.strOCR;
        sv += '&';
        // Bug# 25829 DH

          data = get_top().main.Base64.encode(Adumbrate(sv)); //Bug 25913 NK Uppercase letter V barcode
        var args = {
            e_data: data
        };
        
        post_remote(get_top().main.get_current_obj(), "Business_app.pos.device_swipe_receive", args, 'main');
        get_top().main.dequeue_message();
      }
    }
    catch (e) 
    {
      alert("Error processing document. Error: " + e.description);
      get_top().main.dequeue_message();
    }
  }

  // Bug 25829 DH - Provide the ability to scan a document OCR using an Epson device and feed it into the existing barcode process
  var ocr_x = ocr_coordinate_1;
  var ocr_y = ocr_coordinate_2;
  var ocr_width = ocr_coordinate_3;
  var ocr_height = ocr_coordinate_4;
  // end Bug 25829 DH

  if(document_type == "N")// Panini
  {
    if (ocr_x == undefined || ocr_x == "" || ocr_x == null || ocr_x == "opt" || ocr_x == "GEO.opt")
        ocr_x = "EMPTY";
    
    if (ocr_y == undefined || ocr_y == "" || ocr_y == null || ocr_y == "opt" || ocr_y == "GEO.opt")
      ocr_y = "EMPTY";

    if (ocr_width == undefined || ocr_width == "" || ocr_width == null || ocr_width == "opt" || ocr_width == "GEO.opt")
      ocr_width = "EMPTY";

    if (ocr_height == undefined || ocr_height == "" || ocr_height == null || ocr_height == "opt" || ocr_height == "GEO.opt")
      ocr_height = "EMPTY";

	  var app                 = get_top().main.get_current_obj();
	  var doc_type            = "FULL SHEET";
	  var doc_side            = "NONE";  
	  var doc_resolution      = "100";
	  var keep_dashes_in_micr = "false"
	  var enable_franking     = "NO";

    // Bug 21678 DH - Endorsement printing*/
    // Bug# 24172 DH - Enable ABA validation
    var aba_validate = "FALSE";
    
    AjaxCallPeripherals("panini_scan_doc/" + device_name + "/" + app + "/" + doc_type + "/" + doc_side + "/" + doc_resolution + "/" + keep_dashes_in_micr + "/" + enable_franking + "/" + ocr_x + "/" + ocr_y + "/" + ocr_width + "/" + ocr_height + "/" + endorsement_message + "/" + aba_validate, DeviceCallbackSuccess, DeviceCallbackError, "", "");
  }

  // Bug 25829 DH - Provide the ability to scan a document OCR using an Epson device and feed it into the existing barcode process
  if (document_type == "D")// Epson
  {
    // Define coordinates
    g_bWaiting_For_Epson_OCR_and_Image = false;
    if (ocr_x != 0 || ocr_y != 0 || ocr_width != 0 || ocr_height != 0)
      g_bWaiting_For_Epson_OCR_and_Image = true;

    var OCRReceived = function (result, param)
    {
      if (result != null && result.Epson_PrinterProcessOCRDocResult != undefined)
        DeviceCallbackSuccess(result.Epson_PrinterProcessOCRDocResult, param);// Dismiss the prompt as well
      return;
    }

    var CoordinatesReceived = function (result, param) 
    {
      ScannerChangeMode(CHK_DI_MODE_CHECKSCANNER, "");
      AjaxCallPeripherals("epson_printer_scanner_insert", "", "", "", "");
      AjaxCallPeripherals("epson_printer_process_ocr_doc", OCRReceived, "", true, "");
      return;
    }

    AjaxCallPeripherals("epson_printer_define_ocr_coordinates/" + ocr_x + "/" + ocr_y + "/" + ocr_width + "/" + ocr_height, CoordinatesReceived, "", "", "");
  }
  // end Bug 25829 DH

  if (document_type == "H")// Pertech
  {
    if (pertech_ocr_recognizer == undefined || pertech_ocr_recognizer == "" || pertech_ocr_recognizer == null || pertech_ocr_recognizer == "opt" || pertech_ocr_recognizer == "GEO.opt")
      pertech_ocr_recognizer = "empty";
   
    if (device_name == "" || device_name == "opt" || device_name == undefined)
      device_name = "Pertech 6100";
  
    var PertechOCRReceived = function (result, param)
    {
      if (result != null && result != undefined)
        DeviceCallbackSuccess(result, param);
      AjaxCallPeripherals("pertech_eject_doc", "", "", "", ""); // Dug# 25499 DH
      return;
    }
    
    AjaxCallPeripherals("pertech_scanner_scan_doc/" + device_name + "/" + "EMPTY" + "/" + "FRONT" + "/" + "OCR" + "/" + "200" + "/" + ocr_y + "/" + ocr_height + "/" + ocr_width + "/" + ocr_x + "/" + pertech_ocr_recognizer + "/" + "true" + "/" + "NO", PertechOCRReceived, "", true, "");
   }
}

function DocOCR_CancelScan(document_type)
{
  // Bug# 21370 DH
  
  if(document_type == "N")// Panini
  {
    AjaxCallPeripherals("panini_cancel_scan", "", "", "", "");
    get_top().main.dequeue_message();
  }
  else if (document_type == "H")/*Bug 25830 DH Pertech*/
  {
    TerminateRequestsAtDevices(document_type);// Dug# 25997 DH
    get_top().main.dequeue_message();
  }
  else
  {// Bug# 25829 DH - Epson
    PrinterCancel();
    get_top().main.dequeue_message();// Dug# 25997 DH
  }
  
}
//----------------------- end Action Bar OCR Scanline for Bug# 21370 - Daniel Hofman ------------

//--------------------- BUG# 12778 TWAIN imaging device Added by DH -------------------------------

function TWAIN_ScanDoc_asnyc(twain_device_name, top_app, twain_doc_type, twain_doc_side, twain_capture_ocr, twain_doc_resolution, twain_jpg_quality, message)
{
  // BUG# 12778 TWAIN Device DH.
  // Bug 13720 DH - iPayment Peripheral Overhaul
  
  var Twain_CallbackSuccess = function(result, param/*param = remove UI message*/)
  {    
    try
	  {
	    if(result == null || result == undefined || result.iLastErrorCode != 0)
	    {
	      var s = " ";
	      if(result != null && result != undefined)
	        s = result.strLastErrorMessage;
	      
	      alert("Error processing document. " + s);
	      if(param)
	        get_top().main.dequeue_message();
	      return;
	    }
    
      var app = result.strCashieringScreenObject; 
      if(app.substr(0, 3) == "GEO")
        app = app.substr(4);
      else if(app == "")
        app = get_top().main.get_current_obj();

		  // Only send the expected document back to screen.
      // PDF would be front only.
      if (result.strDocumentSide == "FRONT")
        strTwainDocBack = "";
      else if (result.strDocumentSide == "BACK")
        strTwainDocFront = "";
      
      twain_content_type = "image/tiff";// Default
      if (result.strDocumentType == "TIFF")
        twain_content_type = "image/tiff";
      else if (result.strDocumentType == "JPG")
        twain_content_type = "image/bmp";
      else if (result.strDocumentType == "PDF")
        twain_content_type = "PDF";
      else
        twain_content_type = "N/A";

      TWAIN_SendImageToScreen
      ({
        app: app,
        content_doc_front: result.strImage_FRONT_DataString,
        content_doc_back: result.strImage_BACK_DataString,
        ocr_data: result.strOCR,
        content_type: twain_content_type
        });
      }
      catch (e) 
      {
        alert("Unable to use device. Error: " + e.description);
        if(param)
          get_top().main.dequeue_message();
        return -1;
      } 
      
      if(param)
        get_top().main.dequeue_message();
      
  return 0;
  }
  
  
  var Twain_CallbackError = function(result, param/*param = remove UI message*/)
  { 
    if(param)
      get_top().main.dequeue_message();
      
    alert("Unable to process document: " + result );
    return -1;
  }
  
  var RemoveUIMessageAfterProcessingThis = true;// param
  AjaxCallPeripherals("twain_scan_doc/" + twain_device_name + "/" + top_app + "/" + twain_doc_type + "/" + twain_doc_side + "/" + twain_capture_ocr + "/" + twain_doc_resolution + "/" + twain_jpg_quality, Twain_CallbackSuccess, Twain_CallbackError, RemoveUIMessageAfterProcessingThis, "");

return 0;
}

function TWAIN_ScanDoc(twain_device_name, top_app, twain_doc_type, twain_doc_side, twain_capture_ocr, twain_doc_resolution, twain_jpg_quality, message) 
{
  // BUG# 12778 TWAIN Device DH.
  // Bug 13720 DH - iPayment Peripheral Overhaul
  
  if (!g_IsTwainInstalled)
  {
    alert("TWAIN device [" + twain_device_name + "] is not available. Please check the power and USB cables.");
    get_top().main.dequeue_message();
    return -1;
  }

  if (message != "opt" && message != "" && message != undefined)
  {
    bRetVal = get_top().main.enqueue_message(message);
    if(bRetVal == false)
      return false;
  }
  
  // Lets do an easy async call here so the screen can redraw properly.
  setTimeout(function()
  {
    TWAIN_ScanDoc_asnyc(twain_device_name, top_app, twain_doc_type, twain_doc_side, twain_capture_ocr, twain_doc_resolution, twain_jpg_quality, message);
  }, 10
  );
  
  return 0;
}

function TWAIN_SendImageToScreen(args) 
{
  // BUG# 12778 TWAIN Device DH.
  // Bug 13720 DH - iPayment Peripheral Overhaul  
  
  post_remote('Business_app', 'Business_app.Create_core_item_app.twain_receive_image', args, 'main', true, displayResult, null);
}
//--------------------- BUG# 12778 TWAIN imaging device Added by DH ---------------------------------

//--------------------- SignaturePad BUG# 6368 added by DH ------------------------------
var Ingenico_block_calls = false;
function Ingenico_CancelLastRequest(close_prompt)
{	
  if(Ingenico_block_calls == true)
    return;

  Ingenico_block_calls = true;
  
  // Bug 13720 DH - iPayment Peripheral Overhaul
  if(close_prompt != undefined && close_prompt == true)
  {
    // Also close the last user prompt
    get_top().main.dequeue_message();
  }
  
  var Ingenico_CancelLastRequest_Success = function(result, param)
  {
    Ingenico_block_calls = false;
  }
  
  g_UserCanceledCardSwipe = true;
  
  // Tell the device to cancel
  AjaxCallPeripherals("ingenico_cancel_last_request", Ingenico_CancelLastRequest_Success, Ingenico_CancelLastRequest_Success, "", "");
}

function Ingenico_CancelLastRequestWithBlocking()
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  get_top().main.enqueue_message({content:"<BR/><CENTER>Processing. Please wait...</CENTER>", actions:{}});
  Ingenico_CancelLastRequest(true);
}

function Ingenico_SignaturePadSignatureCapture_ErrorCallback(result, params)
{
  no_ads = params["no_ads"];

  Ingenico_ConfirmationText = "";
  
  // Bug 13720 DH - iPayment Peripheral Overhaul
  get_top().main.dequeue_message();
	alert("Signature capture error: " + result.status + "\nStatus Text: " + result.statusText );
  if(no_ads == "no_ads")
		return;
	else
		Ingenico_DisplayAds();
}

function Ingenico_SignaturePadSignatureCapture_SuccessCallback(ImageData, params)
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  top_app = params["top_app"];
  XMLMessage = params["xml"];
  no_ads = params["no_ads"];// Multi documents will not interrupt the flow
  strUserPrompt = params["strUserPrompt"];
  strConsumerPrompt = params["strConsumerPrompt"];
   
  try
  {
	    if(ImageData == "")
	    {
	      get_top().main.dequeue_message();
	      alert("Signature capture aborted.");
	      get_top().main.enqueue_message({content:"<BR/><CENTER>Cancelling. Please wait...</CENTER>", actions:{}});
	      SignaturePadDocMoveNext('cancel', top_app);
	      Ingenico_DisplayAds();
	      if(top_app == null || top_app == "null")// Call to Ingenico_SignaturePadSignatureCapture came from the manual entry method (Ingenico_SignaturePadManualEntry)
        {
          // BUG# 8534 DH - Add CC void here in case user Canceled.
		      get_pos().SignaturePadVoidLastCC({
																		    app:get_top().main.get_current_obj(),
			                                  sig_cc_system_interface:g_str_sig_cc_system_interface
			                                  });
	        // end BUG# 8534 DH - Add CC void here in case user Canceled.
        }
      }
      else
      {
        get_top().main.dequeue_message();
        
        if(XMLMessage != "")
          XMLMessage = ConvertToBase16(XMLMessage);// Bug# 25780 DH - Moved the ConvertToBase16 to this file.
          
        image = "<BR/><img src='data:image/png;base64," + ImageData + "' alt='' /><BR/>";
        cancel_btn = "<button onclick=get_pos().Ingenico_CancelImageCapture('" + top_app + "')>Cancel</button>";
        accept_btn = "<button onclick=get_pos().Ingenico_AcceptImageCapture('" + ImageData + "','" + XMLMessage + "','" + top_app + "','" + no_ads + "'" + ")>Accept</button>";
        retry_message = strUserPrompt.replace(/ /g, '%20');
        if (strConsumerPrompt == undefined)
          strConsumerPrompt = "Message not configured!";
         strConsumerPrompt = strConsumerPrompt.replace(/ /g, '%20');
          strConsumerPrompt = strConsumerPrompt.replace('\n', '\\n');// Bug# 18301 DH

        retry_btn = "<button onclick=get_pos().Ingenico_RetryImageCapture(" + "'" + no_ads + "','" + retry_message + "','" + strConsumerPrompt + "','" + XMLMessage + "','" + top_app + "'" + ")>Retry</button>";
        get_top().main.enqueue_message({ content: "<CENTER>" + "Do you accept this image?" + "</CENTER>" + image + retry_btn + accept_btn + cancel_btn, actions: {} });
	      return "success";// Does not really matter here since we are async.
      }
    }
  catch (e) 
  {
    get_top().main.dequeue_message();
    alert("Signature capture error: " + e.description);
    if(no_ads == "no_ads")
			return;
		else
			Ingenico_DisplayAds();
  }
}

var Ingenico_ConfirmationText = "";

function Ingenico_SignaturePadSignatureCapture(callbackNotifier, strUserPrompt, strConsumerPrompt, XMLMessage, top_app/*BUG# 9951 DH*/) 
{
  // Bug 13720 DH - iPayment Peripheral Overhaul

  Ingenico_ConfirmationText = "";
   
  if(strConsumerPrompt == "" || strConsumerPrompt == " " || strConsumerPrompt == null || strConsumerPrompt == undefined)// Added check here. Can't have this going over to the web service. Not sure decided to add it. DH.
    strConsumerPrompt = "Message not configured!";
    
  if(!g_IsIngenicoAvailable)
  {
    SignaturePadDocMoveNext('cancel', get_top().main.get_current_obj());
    return
  }
  
  if(top_app != null)
  {
    if(top_app.indexOf("GEO.") !== -1)
      top_app = top_app.substr(4);
  }
  
	var subject=get_top().main.get_current_obj();
	strUserPrompt = strUserPrompt.replace(/%20/g, ' ');
	
	// This will be dismissed in the success or error handler
	get_top().main.enqueue_message({content:"<CENTER>" + strUserPrompt + "</CENTER> <BR/><button onclick=get_pos().Ingenico_CancelLastRequestWithBlocking()>Cancel</button>", actions:{}});
	
	// Pass tyhese to the callback
	var params                 = new Array();
	params["top_app"]          =top_app;
	params["xml"]              =XMLMessage;
	params["no_ads"]           =callbackNotifier;
	params["strUserPrompt"]    =strUserPrompt;
	params["strConsumerPrompt"]=strConsumerPrompt;

  // Bug# 20824 DH /Bug 23589 NK - Ingenico: Add "User chose "
  if(strConsumerPrompt == "" || strConsumerPrompt == undefined)
    strConsumerPrompt = "empty";
 
  if (strConsumerPrompt != "empty")
    Ingenico_ConfirmationText = strConsumerPrompt + Ingenico_ConfirmationText;

	AjaxCallPeripherals("ingenico_request_signature/" + strConsumerPrompt, Ingenico_SignaturePadSignatureCapture_SuccessCallback, Ingenico_SignaturePadSignatureCapture_ErrorCallback, params, "");
  
	return "";
}

function Ingenico_AcceptImageCapture(ImageData, XMLMessage, top_app, callbackNotifier)
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  
  var content_type="image/bmp";
    
  if(XMLMessage != "")
    XMLMessage = get_top().main.ConvertFromBase16(XMLMessage);
    
  var subject=get_top().main.get_current_obj();
  get_top().main.dequeue_message();
  get_top().main.enqueue_message({content:"<BR/><CENTER>Processing. Please wait...</CENTER>", actions:{}});
  Device_SignaturePad_ReceiveSignature(ImageData, content_type, XMLMessage, top_app/*BUG# 9951 DH*/);
  SignaturePadDocMoveNext('continue', subject); 	// Bug	23478 NK
  
  if(callbackNotifier != "no_ads")
    Ingenico_DisplayAds();  
}

function Ingenico_CancelImageCapture(top_app)
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  
  var subject=get_top().main.get_current_obj();
  get_top().main.dequeue_message();
	alert("Signature capture canceled.");
	Ingenico_DisplayAds();
	if(top_app == null || top_app == 'null')// Call to Ingenico_SignaturePadSignatureCapture came from the manual entry method (Ingenico_SignaturePadManualEntry)
  {
    // BUG# 8534 DH - Add CC void here in case user Canceled.
    get_pos().SignaturePadVoidLastCC({
															    app:subject,
                                  sig_cc_system_interface:g_str_sig_cc_system_interface
                                  });
    // end BUG# 8534 DH - Add CC void here in case user Canceled.
  }
}

function Ingenico_RetryImageCapture(callbackNotifier, strUserPrompt, strConsumerPrompt, XMLMessage, top_app)
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  Ingenico_CancelLastRequest();
  get_top().main.dequeue_message();
	Ingenico_SignaturePadSignatureCapture(callbackNotifier, strUserPrompt, strConsumerPrompt, XMLMessage, top_app);
}

function Ingenico_MultiSignaturePadSignatureCapture(callbackNotifier, strUserPrompt, strConsumerPrompt, str_message, top_app, strXMLObjectID) 
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  if(!g_IsIngenicoAvailable)
  {
    SignaturePadDocMoveNext('cancel', get_top().main.get_current_obj());
    return
  }
   
  
  try
  {
    get_top().main.dequeue_message();
    
    if(top_app != null)
    {
      if(top_app.indexOf("GEO.") !== -1)
        top_app = top_app.substr(4);
    }
    
	  var subject=get_top().main.get_current_obj();
    strUserPrompt = strUserPrompt.replace(/%20/g, ' ');
    if (strConsumerPrompt != undefined)
	    strConsumerPrompt = strConsumerPrompt.replace(/%20/g, ' ');
  	
	  get_top().main.enqueue_message({content:"<CENTER>" + strUserPrompt + "</CENTER> <BR/><button onclick=get_pos().Ingenico_CancelLastRequest(true)>Cancel</button>", actions:{}});
  		
    var Ingenico_MultiSignaturePadSignatureCapture_SuccessCallback = function(result, param) 
    {
      if(result == "")
      {
        Ingenico_CancelLastRequest();
        
        if(callbackNotifier != "no_ads")
          Ingenico_DisplayAds();
        
        get_top().main.dequeue_message();
        alert("Signature capture canceled.");
        Ingenico_DisplayAds();
        SignaturePadDocMoveNext('cancel', subject);
      }
      else
      {
    
        get_top().main.dequeue_message();

        if (str_message != "")
          str_message = ConvertToBase16(str_message);// Bug# 25780 DH - Moved the ConvertToBase16 to this file.

        image = "<BR/><img src='data:image/png;base64," + result + "' alt='' /><BR/>";
        cancel_btn = "<button onclick=get_pos().Ingenico_CancelImageCapture('" + top_app + "')>Cancel</button>";
        accept_btn = "<button onclick=get_pos().Ingenico_AcceptMultiImageCapture(" + "'" + result + "','" + str_message + "','" + top_app + "','" + callbackNotifier + "','" + strXMLObjectID + "'" + ")>Accept</button>";
        retry_message = strUserPrompt.replace(/ /g, '%20');
        if (strConsumerPrompt == undefined)
          strConsumerPrompt="Please sign your name."
        strConsumerPrompt = strConsumerPrompt.replace(/ /g, '%20');
        retry_btn = "<button onclick=get_pos().Ingenico_RetryMultiImageCapture(" + "'" + callbackNotifier + "','" + retry_message + "','" + strConsumerPrompt + "','" + str_message + "','" + top_app + "','" + strXMLObjectID + "'" + ")>Retry</button>";
        Ingenico_DisplayAds();
        get_top().main.enqueue_message({ content: "<CENTER>" + "Do you accept this image?" + "</CENTER>" + image + retry_btn + accept_btn + cancel_btn, actions: {} });
        return "success";// Does not really matter here since we are async.
      }
    }
    
    var Ingenico_MultiSignaturePadSignatureCapture_ErrorCallback = function (result, param) 
	  {
	    get_top().main.dequeue_message();
		  alert("Signature capture error: " + result.status + "\nStatus Text: " + result.statusText );
      if(callbackNotifier == "no_ads")
			  return;
		  else
			  Ingenico_DisplayAds();
	  }
    if (Ingenico_ConfirmationText == undefined)
      Ingenico_ConfirmationText = "";
    // Bug# 20824 DH 
	//Bug 23589 NK - Ingenico: Add "User chose "
    if (strConsumerPrompt == "" || strConsumerPrompt == undefined)
      strConsumerPrompt = "empty";
    
    if (strConsumerPrompt != "empty")
        Ingenico_ConfirmationText = strConsumerPrompt + Ingenico_ConfirmationText;

    AjaxCallPeripherals("ingenico_request_signature/" + strConsumerPrompt, Ingenico_MultiSignaturePadSignatureCapture_SuccessCallback, Ingenico_MultiSignaturePadSignatureCapture_ErrorCallback, "", "");
  }
  catch (e) 
  {
    get_top().main.dequeue_message();
    alert("Signature capture error: " + e.description);
    if(callbackNotifier == "no_ads")
			return;
		else
			Ingenico_DisplayAds();
  }
	
	return "";
}

function Ingenico_AcceptMultiImageCapture(content, str_message, top_app, callbackNotifier, strXMLObjectID)
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  
  var content_type="image/bmp";
  
  if(str_message != "")
    str_message = get_top().main.ConvertFromBase16(str_message);
  
  var subject=get_top().main.get_current_obj();
  get_top().main.dequeue_message();
  get_top().main.enqueue_message({content:"<BR/><CENTER>Processing. Please wait...</CENTER>", actions:{}});
  Device_SignaturePad_ReceiveSignature(content, content_type, str_message, top_app/*BUG# 9951 DH*/);
  get_top().main.dequeue_message();
    
 if(strXMLObjectID != "")// Bug 13720 DH - iPayment Peripheral Overhaul - New service base XML object
	  XML_DeleteXMLObject(strXMLObjectID);
	        
	ResetMultipleDocArray();
	
	if(callbackNotifier != "no_ads")
    Ingenico_DisplayAds();
    
  // Bug 22921/22922/20586 MJO/NK 
SignaturePadDocMoveNext("continue", top_app);// BUG# 10554 DH - Limited printer support   
}

function Ingenico_RetryMultiImageCapture(callbackNotifier, retry_message, strConsumerPrompt, strXMLMessage, top_app, strXMLObjectID)
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  get_top().main.dequeue_message();
  Ingenico_CancelLastRequest();
	Ingenico_MultiSignaturePadSignatureCapture(callbackNotifier, retry_message, strConsumerPrompt, strXMLMessage, top_app, strXMLObjectID);
}

function Device_SignaturePad_ReceiveSignature(content, content_type, str_message, top_app/*BUG# 9951 DH*/)
{
  // Bug#9100 Mike O
  var app_obj = null;
  if(top_app != null && top_app != "null")
    app_obj = top_app;
  else
    app_obj = get_top().main.get_current_obj();

  // Bug 23529 MJO - Remove NA at the beginning as this will be displayed later
  while (str_message.indexOf("NA") === 0)
    str_message = str_message.substr(2);
  
  Device_SignaturePad_DataReceive({ app:app_obj, /*BUG# 9951 DH*/
    content: content, content_type: content_type,
    // Bug 23529 MJO - Store confirmation text as well if present
    str_message_to_save: Ingenico_ConfirmationText.length == 0 ? str_message : Ingenico_ConfirmationText,
    screen_args: args_for_later_chaining
  });

  Ingenico_ConfirmationText = "";
}

function Device_SignaturePad_DataReceive (args) 
{
  // BUG# 8276 Third receipt fix DH
  // Bug#9100 Mike O
  // Bug #11783 Mike O - No longer calls from main frame, as it might not be fully loaded yet
  // Bug #11845 Mike O - Refer to the displayResult in the current frame
  // Bug #11959 Mike O - Post JS to run into pos_frame instead of overwriting the whole main frame (causes timing issues)
  
  // Bug 13720 DH - iPayment Peripheral Overhaul
  post_remote('Device.SignaturePad', 'Device.SignaturePad.receive_signature', args, 'pos_frame'/*Bug# 18301 DH - Removed: true*/);
  
  args_for_later_chaining = null;// BUG# 8276 Third receipt fix DH  
}
//***************************************************************
//                      ACCEPT / DECLINE
function Ingenico_SignaturePadAcceptDecline(strUserPrompt, strConsumerPrompt, action_accept, action_decline, action_yes, action_no, doc_class, top_app, strXMLObjectID) 
{ 
  // Bug 13720 DH - iPayment Peripheral Overhaul
   
  if(!g_IsIngenicoAvailable)
  {
    SignaturePadDocMoveNext('cancel', get_top().main.get_current_obj());
    return
  }
            
   try
   {
	    // Cannot enforce this in the Config for now. If left blank, user will see the last message instead.
      if(strConsumerPrompt == "")
		    strConsumerPrompt = "Message not configured!";
    		
      var str_message = strConsumerPrompt;
     
      if(strUserPrompt != "opt" && strUserPrompt != "GEO.opt" && strUserPrompt != "" && strUserPrompt != undefined)
      {
        get_top().main.dequeue_message();// Start with a clean slate, a must here!
		// Bug 21221 MJO - Show the processing dialog
        get_top().main.enqueue_message({content:"<CENTER>" + strUserPrompt + "</CENTER> <BR/><button onclick=get_pos().Ingenico_CancelLastRequestWithBlocking(true)>Cancel</button>", actions:{}});
      }
        
      var AcceptDecline_SuccessCallback = function(result, param) 
      {
          Ingenico_ProcessDocumentResult("accept_decline", result.Ingenico_AcceptDeclineResult /*Bug# 20679 & 20634 DH*/, action_accept, action_decline, action_yes, action_no, doc_class, strConsumerPrompt, top_app, strXMLObjectID);
      }
      
      var AcceptDecline_ErrorCallback = function(result, param)
      {
          Ingenico_ProcessDocumentResult("accept_decline", "cancel", action_accept, action_decline, action_yes, action_no, doc_class, strConsumerPrompt, top_app, strXMLObjectID);
      }
          
      // Bug# 10949 DH
      // Need to send data here via POST instead. These prompts can get quite long.
      var base16String = {};
	    base16String.strUserPrompt = strUserPrompt;
     base16String.strConsumerPrompt = strConsumerPrompt;

     Ingenico_ConfirmationText = "||" + strConsumerPrompt;

	    AjaxCallPeripherals("ingenico_accept_decline", AcceptDecline_SuccessCallback, AcceptDecline_ErrorCallback, strXMLObjectID, base16String);
	    // end Bug# 10949
  }
  catch (e) 
  {
    Ingenico_ProcessDocumentResult("accept_decline", "cancel", action_accept, action_decline, action_yes, action_no, doc_class, strConsumerPrompt, top_app, strXMLObjectID);
  }
}
//                    END ACCEPT / DECLINE
//***************************************************************

//***************************************************************
//                          CARD SWIPE
function Ingenico_SignaturePadSwipeCard_ProcessCardSwipe(swipe_result, expected_card_type, sig_cc_system_interface, top_app, strAmount, strUserPrompt, credit_debit_mode, callback/*Added while merging. Looks like Mike O needs this now for fees bug 14905*/)
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  
  if(swipe_result == null || swipe_result == undefined || swipe_result== -1)
  {
    // There should be no alert for Ingenico per Tim comment 191 in the 13720 TTS entry.
    return;
  }
  
  if(swipe_result.iLastErrorCode == -1)
  {  
    if(swipe_result.strLastErrorMessage.indexOf("cancel") > -1 || 
    swipe_result.strLastErrorMessage.indexOf("invalid") > -1 || 
    swipe_result.strLastErrorMessage.indexOf("Timeout reached.") > -1 ||// Bug# 21255 DH
    swipe_result.strLastErrorMessage.indexOf("Unable to communicate with the Ingenico device") > -1)
    {
      if(swipe_result.strLastErrorMessage.indexOf("invalid") > -1)
      {
        alert("\nCARD READ ERROR\n");
        Ingenico_CancelLastRequest();// Bug# 21255 DH
        get_top().main.dequeue_message();
        return;
      }
      else if(swipe_result.strLastErrorMessage.indexOf("Unable to communicate with the Ingenico device") > -1)
      {
        alert("\nUnable to communicate with the Ingenico device\n\nPlease reset peripherals.");
        get_top().main.dequeue_message();
        return;
      }
      // Bug# 21255 DH
      else if(swipe_result.strLastErrorMessage.indexOf("Timeout reached.") > -1)
      {
        alert("\nTimeout reached. Please try again.\n");
        get_top().main.dequeue_message();
        SignaturePadDocMoveNext("cancel", top_app);
	      Ingenico_DisplayAds();
        return;
      }
      else
      {
        alert("\nSwipe Canceled\n");
      }
      
      get_top().main.dequeue_message();

      // Bug 25130 MJO - Cancel if this is a fee swipe
      if (callback === "fee")
        SignaturePadDocMoveNext("cancel", top_app);
      else
        MarkTheScreenForManualCCEntry(sig_cc_system_interface, top_app);

      Ingenico_DisplayAds();
	    return;
    }
  }
    
  var Ingenico_TrackDataCallbackSuccess = function(result, param)
  {       
    // Exception in the peripherals service code.
    if(result.iLastErrorCode == -1)
    {
      get_top().main.dequeue_message();
      alert("\nCARD READ ERROR\n");
      Ingenico_DisplayAds();
      return;
    }
    
    //-----------------------------------------
    // Bug# 19518 DH - Credit Card Encryption
    // The card number has extra 4 digits left unencrypted for this code to be able to identify the card type. These are extra numbers and must be removed.
    // The encrypted string contains the full acct number.
    var CardIdentity = result.IngenicoTrack1.substr(0, 4);
      //-------------------------------------------

    // Get the scan code for the last swipe
    Track1Data = "%B";// BUG 8705 D.H
    Track1Data += result.IngenicoTrack1.substr(4);// Bug# 19518 DH - Credit Card Encryption
    Track1Data += "?";// BUG 8705 D.H
      //Bug#7227 ANU 
    Track2Data = ";";// BUG 8705 D.H
    Track2Data += result.IngenicoTrack2;
    Track2Data += "?";// BUG 8705 D.H
  	
    // BUG 18632 AG Mastercard accepting new 2 series as well as 5 series
    // BUG 7493 D.H. Need to validate type in case user swipes the wrong card for this selected screen
    var expectedID = [];
    if(expected_card_type == "MC")
        expectedID = [2, 5];
    else if(expected_card_type == "VISA")
        expectedID = [4];
    else if(expected_card_type == "DISC")
        expectedID = [6];
    else if(expected_card_type == "AMEX")
        expectedID = [3];
  		
    var charID = CardIdentity.substr(0, 1).charAt(0);// Bug# 19518 DH - Credit Card Encryption    

    var is_valid = false;
     //BUG 20144 AG Removed foreach loop as it is throwing an error in some browsers
   
    for (var idi = 0; idi < expectedID.length; ++idi) {
        var id = expectedID[idi];
        is_valid = is_valid || (id == charID);
    }   
    if (!is_valid && expected_card_type != "ANY")
    {
      get_top().main.dequeue_message();
      alert("\nWrong Card Swiped!\nPlease select the correct card and try again.\n");
      Ingenico_DisplayAds();
      return;
    }
    // end BUG 7493 D.H. 	      
    // end Bug AG 18632
    
    // Track1Data = get_top().main.ConvertToHex(Track1Data);// Bug# 19518 DH the card data comes encrypted from peripherals no need for this hex conversion
    // Track2Data = get_top().main.ConvertToHex(Track2Data);// Bug# 19518 DH the card data comes encrypted from peripherals no need for this hex conversion

    get_top().main.dequeue_message();
    
    // Bug #14905 Mike O - Skip payment type selection on convenience fee swipe or already set by configuration
    payment_type = "credit"; // I guess this forces it to submit whatever and deal with the type in config.
    if (callback != "fee" && credit_debit_mode != "credit" && credit_debit_mode != "debit")
	  {
      Ingenico_SignaturePadSwipeCard_RetrievePaymentType(top_app, Track1Data, Track2Data, expected_card_type, sig_cc_system_interface, strAmount);       
    }
    else
    {
      // Trying to add fees. Not sure how it works exactly, it was added to my code while I was out DH.
      Ingenico_SignaturePadSwipeCard_SendDataToiPayment(top_app, Track1Data, Track2Data, "", payment_type, expected_card_type, sig_cc_system_interface, strAmount);
    }
  }

  var Ingenico_TrackDataCallbackSuccessError = function(result, param)
  {
     alert("\nCARD READ ERROR\n");
     get_top().main.dequeue_message();
     Ingenico_DisplayAds();
     return;
  }

  AjaxCallPeripherals( "ingenico_get_track_data", Ingenico_TrackDataCallbackSuccess, Ingenico_TrackDataCallbackSuccessError, "", "");
}
    
function Ingenico_SignaturePadSwipeCard_RetrievePinNbr(top_app, Track1Data, Track2Data, PaymentType, expected_card_type, sig_cc_system_interface, strAmount)
{
  // Bug 13720 DH - iPayment Peripheral Overhaul

  try
  {
    // Ask user for PIN#
    strUserPrompt = "Ask user to enter PIN number";// This could be configurable one day. DH
    strConsumerPrompt = "*";
    get_top().main.enqueue_message({content:"<CENTER>" + strUserPrompt + "</CENTER> <BR/><button onclick=get_pos().Ingenico_CancelLastRequest(true)>Cancel</button>", actions:{}});// Bug 13720 DH - iPayment Peripheral Overhaul

    var Pin_ErrorCallback = function (result, param) 
	  { 
	    UnmarkTheScreenForManualCCEntry(top_app/*Added top_app for BUG# 9698 DH*/);
	    get_top().main.dequeue_message();
      alert("Error retrieving card pin#. Please try again. \n\nError: \n" + result.status + "\nStatus Text: \n" + result.statusText );
      SignaturePadDocMoveNext("cancel");
      Ingenico_CancelLastRequest();
      Ingenico_DisplayAds();
      return;
	  }
	  
    var CardPin_SuccessCallback = function(result, param) 
    {
      UnmarkTheScreenForManualCCEntry(top_app/*Added top_app for BUG# 9698 DH*/);
      
      if(result == false)
      {
        SignaturePadDocMoveNext("cancel");
  		  Ingenico_DisplayAds();  
  		  get_top().main.dequeue_message();
        return;
      }
      else if(result == true)
      {
        var Pin_SuccessCallback = function(DebitPinNbr, param)
        {
          get_top().main.dequeue_message();
		      Ingenico_SignaturePadSwipeCard_SendDataToiPayment(top_app, Track1Data, Track2Data, DebitPinNbr, PaymentType, expected_card_type, sig_cc_system_interface, strAmount);
		      return;
        }
        
        // Retrieve the PIN# data
        AjaxCallPeripherals("ingenico_get_data", Pin_SuccessCallback, Pin_ErrorCallback, "", "");
        return;
      }
    }
		
		AjaxCallPeripherals("ingenico_card_pin_entry/" + strUserPrompt + "/" + strConsumerPrompt, CardPin_SuccessCallback, Pin_ErrorCallback, "", "");
  }
  catch (e) 
  {
     UnmarkTheScreenForManualCCEntry(top_app/*Added top_app for BUG# 9698 DH*/);
     get_top().main.dequeue_message();
     alert("Error retrieving card pin#. Error returned: " + e.description +  "\n\nPlease try again.");
     SignaturePadDocMoveNext("cancel");
     Ingenico_CancelLastRequest();
     Ingenico_DisplayAds();  
     return;
  }  
  
  return;
}

// Bug# 21317 DH
var Ingenico_block_calls = false;
function Ingenico_CancelLastRequestAtRetrievePT(close_prompt, sig_cc_system_interface, top_app)
{	
  if(Ingenico_block_calls == true)
    return;

  Ingenico_block_calls = true;
  if(close_prompt != undefined && close_prompt == true)
  {
    get_top().main.dequeue_message();
  }
  
  var Ingenico_CancelLastRequest_SuccessPT = function(result, param)
  {
    alert("\nSwipe Canceled\n");
    Ingenico_block_calls = false;
    MarkTheScreenForManualCCEntry(sig_cc_system_interface, top_app);
	  SignaturePadDocMoveNext("cancel", top_app);
	  Ingenico_DisplayAds();
  }
  
  top.main.enqueue_message({ content: "<BR/><CENTER>Processing. Please wait...</CENTER>", actions: {} });// Bug# 21255 DH

  g_UserCanceledCardSwipe = true;
  AjaxCallPeripherals("ingenico_cancel_last_request", Ingenico_CancelLastRequest_SuccessPT, Ingenico_CancelLastRequest_SuccessPT, "", "");
}
// end Bug# 21317 DH

function Ingenico_SignaturePadSwipeCard_RetrievePaymentType(top_app, Track1Data, Track2Data, expected_card_type, sig_cc_system_interface, strAmount)
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  var payment_type = "";
  var strUserPrompt = "Ask user to select payment type";// This could be configurable one day. DH

  // Bug# 21317 DH
  get_top().main.enqueue_message({content:"<BR/><CENTER>" + strUserPrompt + "</CENTER> <BR/><button onclick=get_pos().Ingenico_CancelLastRequestAtRetrievePT(true,'" + sig_cc_system_interface + "','" + top_app + "')>Cancel</button>", actions:{}});// Bug 13720 DH - iPayment Peripheral Overhaul
  
  var CallbackError = function (result, param) 
  {
    get_top().main.dequeue_message();
    alert("Error retrieving payment type. Please try again. \n\nError: " + result.status + "\nStatus Text: " + result.statusText );
    SignaturePadDocMoveNext("cancel");
    Ingenico_CancelLastRequest();
    Ingenico_DisplayAds();  
    return;
  }
	    
  var rv = false;
  try
  {
    var PaymentTypeSelection_CallbackSuccess = function(result, param)
    {
        if(result == false)
        {
          // Cancel was pressed by teller or by user on the device. Advance to next doc.
          get_top().main.dequeue_message();
          Ingenico_DisplayAds();
          return;
        }
        else if(result == true)
        {
          PaymentType = "";
          var PaymentTypeData_CallbackSuccess = function(result, param)
          {  
            PaymentType = result;
            
            if(PaymentType == "credit")
            {
              // No need for pin here.
              get_top().main.dequeue_message();
              Ingenico_SignaturePadSwipeCard_SendDataToiPayment(top_app, Track1Data, Track2Data, "", PaymentType, expected_card_type, sig_cc_system_interface, strAmount);
              return;
            }
            else if(PaymentType == "debit")
            {
              get_top().main.dequeue_message();
              Ingenico_SignaturePadSwipeCard_RetrievePinNbr(top_app, Track1Data, Track2Data, PaymentType, expected_card_type, sig_cc_system_interface, strAmount);
              return;
            }
          }
          
          // Retrieve the card type choice.
          AjaxCallPeripherals("ingenico_get_data", PaymentTypeData_CallbackSuccess, CallbackError, "", "");
        }
      }
       
      
  		// Ask user for the card type.
      AjaxCallPeripherals("ingenico_payment_type_selection/" + strUserPrompt + "/" + "empty", PaymentTypeSelection_CallbackSuccess, CallbackError, "", "");
  }
  catch (e)
  {
     get_top().main.dequeue_message();
     alert("Error retrieving payment type. \n\nError returned: " + e.description +  "\n\nPlease try again.");
     SignaturePadDocMoveNext("cancel");
     Ingenico_CancelLastRequest();
     Ingenico_DisplayAds();  
     return;
  }
}

// Bug #14905 Mike O - Added debit credit mode argument
function Ingenico_SignaturePadSwipeCard(callback, strUserPrompt, strConsumerPrompt, sig_cc_system_interface, expected_card_type, top_app/*Added top_app for BUG# 9698 DH*/, credit_debit_mode) 
{    
  // Bug 13720 DH - iPayment Peripheral Overhaul
  var Track1Data = null;
  var Track2Data = null;
  var payment_type = null;
  var debit_pin = null;
	
	if(!g_IsIngenicoAvailable)// Bug# 18078 DH.
  {
    return;
  }
  
  try
  {
    if(top_app.substr(0, 3) == "GEO")
      top_app = top_app.substr(4);// Added top_app for BUG# 9698 DH 
	  
    get_top().main.enqueue_message({content:"<BR/><CENTER>" + strUserPrompt + "</CENTER> <BR/><button onclick=get_pos().Ingenico_CancelLastRequestWithBlocking()>Cancel</button>", actions:{}});// Bug 13720 DH - iPayment Peripheral Overhaul
    
    var SignaturePadSwipeCard_CallbackSuccess = function(swipe_result, param) 
    {
      if(swipe_result.strLastErrorMessage.indexOf("Unable to communicate with the Ingenico device") > -1)
      {
        // There should be no alert for Ingenico per Tim comment 191 in the 13720 TTS entry.
        get_top().main.dequeue_message();
        return;
      }  
    
       Ingenico_SignaturePadSwipeCard_ProcessCardSwipe(swipe_result, expected_card_type, sig_cc_system_interface, top_app, strConsumerPrompt, strUserPrompt, credit_debit_mode, callback)
       return;
    }
    
    var SignaturePadSwipeCard_CallbackError = function(swipe_result, param) 
    {
       if(swipe_result.strLastErrorMessage.indexOf("Unable to communicate with the Ingenico device") > -1)
      {
        // There should be no alert for Ingenico per Tim comment 191 in the 13720 TTS entry.
        get_top().main.dequeue_message();
        return;
      }
    }
    
    AjaxCallPeripherals("ingenico_swipe_card/" + strUserPrompt + "/" + strConsumerPrompt, SignaturePadSwipeCard_CallbackSuccess, SignaturePadSwipeCard_CallbackError, "", "");
  }
  catch(e)
  {
    return "cancel";
  }
  
  return true;
}

function Ingenico_SignaturePadSwipeCard_SendDataToiPayment(top_app, Track1Data, Track2Data, DebitPinNbr, PaymentType, expected_card_type, sig_cc_system_interface, strAmount)
{ 
  // Bug 13720 DH - iPayment Peripheral Overhaul

  get_top().main.enqueue_message({ content: '<BR/><BR/><CENTER><b><font size="+2"> Authorizing Card Information. Please Wait.</font></b></CENTER>', actions: {} });

  var form_data = null;
  get_pos().SignaturePadSwipeReceive({
                              app:top_app,/*Added top_app for BUG# 9698 DH*/
                              //e_data:top.pos.Base64.encode(Track1Data), // BUG 8705 D.H - Disabled Base64
                        	    //e_data2:top.pos.Base64.encode(Track2Data), // BUG 8705 D.H - Disabled Base64
                              e_data: "ing" + Track1Data, // BUG 8705 D.H - Disabled Base64 - Use Base16 // Bug# 19518 DH
                              e_data2: "ing" + Track2Data, // BUG 8705 D.H - Disabled Base64 - Use Base16 // Bug# 19518 DH
                              form_data: form_data,
                              amt:strAmount,
                              payment_type:PaymentType,
                              debit_pin:DebitPinNbr,
                              sig_cc_system_interface:sig_cc_system_interface,
                              card_type:expected_card_type});
}

function Ingenico_GetErrorMsg()
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  ErrorMessage = "";
  var Callback = function(rv, param)
  {
    ErrorMessage = rv;
  }
  AjaxCallPeripherals("ingenico_get_error_msg", Callback, "", "", "");
  
  
return ErrorMessage;
}

function sleep(milliseconds)
{
  // Bug 13720 DH - iPayment Peripheral Overhaul - Function replaced
  
  // This will allow to give some AJAX calls time to complete before the next request is fired.
  var start = new Date().getTime();
  var now = start;
  while(now - start < milliseconds)
    now = new Date().getTime();
}

function Ingenico_DisplayAds()
{
	// Bug 13720 DH - iPayment Peripheral Overhaul - Function replaced
	
	if(!g_IsIngenicoAvailable)
    return "";
    
	var callbackSuccess = function(result, param)
	{
	  // Does not matter here but nutralizes the error alert from the service.
	} 
	
	var callbackError = function(result, param)
	{
	  // Does not matter here but nutralizes the error alert from the service.
	} 
	
  AjaxCallPeripherals("ingenico_displayads", callbackSuccess, callbackError, "", "");
  sleep(1000);// Give a ads chance to finish
}

// Function called after succesfull swipe. Called from:Device.Swipe.<defmethod _name='receive_custom'>
var args_for_later_chaining = null;// BUG# 8276 Third receipt fix DH

// BUG# 9008 D.H.
function SignaturePadWrongCardSwiped (args) 
{
// Bug#9100 Mike O
  // Bug #11783 Mike O - No longer calls from main frame, as it might not be fully loaded yet
  post_remote('Device.Swipe','Device.Swipe.SignaturePadWrongCardSwiped', args, 'main' ); 
}
// end BUG# 9008 D.H.

function SignaturePadSwipeReceive (args) 
{
// Bug#9100 Mike O
  // Bug #11783 Mike O - No longer calls from main frame, as it might not be fully loaded yet
  // Bug #11867 Mike O - Set async to true
  // Bug #11845 Mike O - Refer to the displayResult in the current frame
  get_pos().post_remote('Device.Swipe','Device.Swipe.receive_custom', args, 'main', true);
}

// BUG# 7434 D.H. Need to void any credit card transactions in IPCharge
// Bug#9100 Mike O
  // Bug #11783 Mike O - No longer calls from main frame, as it might not be fully loaded yet
function SignaturePadVoidLastCC (args) 
{
  post_remote('Device.Swipe','Device.Swipe.receive_custom_cc_void', args, 'main', true);
}

// BUG# 7497 D.H. auto continue on tender after CC approval
// Bug#9100 Mike O
function AutoContinueTenderScreen(screen_args)
{
	get_pos().SignaturePadAutoContinue({
																			app:get_top().main.get_current_obj(),
																			screen_args:screen_args
																			});
}

// Bug#9100 Mike O
function SignaturePadAutoContinue (args) 
{
  get_top().main.cover();
  // Bug #11783 Mike O - No longer calls from main frame, as it might not be fully loaded yet
  post_remote('Device.Swipe','Device.Swipe.receive_custom_cc_auto_continue', args, 'main' ); 
}
// end BUG# 7497 D.H. auto continue on tender after CC approval

//                    END CARD SWIPE
//***************************************************************

//***************************************************************
//                        YES NO
function Ingenico_SignaturePadYesNo(strUserPrompt, strConsumerPrompt, action_accept, action_decline, action_yes, action_no, doc_class, top_app, strXMLObjectID) 
{    
  // Bug 13720 DH - iPayment Peripheral Overhaul
  
  // Cannot enforce this in the Config for now. If left blank, user will see the last message instead.
  if (strConsumerPrompt == "" || strConsumerPrompt == undefined )
	  strConsumerPrompt = "Message not configured!";
 
  if(!g_IsIngenicoAvailable)
  {
    SignaturePadDocMoveNext('cancel', get_top().main.get_current_obj());
    return
  }
  
  // Bug 13720 DH - iPayment Peripheral Overhaul
  if(strUserPrompt != "opt" && strUserPrompt != "GEO.opt" && strUserPrompt != "" && strUserPrompt != undefined)
  {
    get_top().main.dequeue_message();// Start with a clean slate, a must here!
    get_top().main.enqueue_message({content:"<CENTER>" + strUserPrompt + "</CENTER> <BR/><button onclick=get_pos().Ingenico_CancelLastRequest(true)>Cancel</button>", actions:{}});
  }
  
  var YesNo_SuccessCallback = function(result, param) 
  {
      Ingenico_ProcessDocumentResult("yes_no", result.Ingenico_YesNoResult/*Bug# 20679 & 20634 DH*/, action_accept, action_decline, action_yes, action_no, doc_class, strConsumerPrompt, top_app, param);
  }
  
  var YesNo_ErrorCallback = function(result, param)
  {
    Ingenico_ProcessDocumentResult("yes_no", "cancel", action_accept, action_decline, action_yes, action_no, doc_class, strConsumerPrompt, top_app, param);
  }
      
  // Bug# 10949 DH
  // Need to send data here via POST instead. These prompts can get quite long.
  var base16String = {};
	base16String.strUserPrompt = strUserPrompt;
  base16String.strConsumerPrompt = strConsumerPrompt;

  Ingenico_ConfirmationText = "||" + strConsumerPrompt;

  AjaxCallPeripherals("ingenico_yes_no", YesNo_SuccessCallback, YesNo_ErrorCallback, strXMLObjectID, base16String);
  // end Bug# 10949
}

//***************************************************************

//***************************************************************
//                      MOVE TO NEXT DOC OR CANCEL

function SignaturePadDocMoveNext(content, subject)
{     
	if(subject == null || subject == undefined)
	{
	  // Bug 13720 DH - iPayment Peripheral Overhaul
	  // Bug 16347 MJO - Fixed this so it points to main, not pos
	  subject = get_top().main.get_current_obj();
	}
  
  if(subject.substr(0, 3) == "GEO")
    subject = subject.substr(4);
      
  get_top().pos.SignaturePadCancelDoc({ app:subject, content:content }, subject);
}

// Bug#9100 Mike O
function SignaturePadCancelDoc(args, subject) 
{ 
	if(subject == null ||subject == undefined) 
	  subject = get_top().main.get_current_obj();
	
  var nAgt = navigator.userAgent;

  // Bug 25000 MJO - Always use the current obj as the app
  args["app"] = get_top().main.get_current_obj();
 
  if (nAgt.indexOf("Safari") !=-1)
  {
    post_remote(subject,'Device.SignaturePad.ReceiveCancel', args, "pos_frame", true);
  }
  else
   post_remote(subject, 'Device.SignaturePad.ReceiveCancel', args, 'pos_frame');
}
//                    END CANCEL
//***************************************************************

//***************************************************************
// BUG# 7495 Manual CC entry support
var g_strTempXMLObjectID = "";
var g_str_sig_cc_system_interface = "";
function Ingenico_SignaturePadManualEntry(strUserPrompt, total_line, screen_args, sig_cc_system_interface)
{
  // Bug 13720 DH - iPayment Peripheral Overhaul - Rewritten
    
  args_for_later_chaining = screen_args;// BUG# 8534
	var g_bAddSignature = false;
	
	// Error handler for all steps
	var Ingenico_CallbackError = function(result, param)
	{  
	  alert("Unable to process signature. Please cancel and try again.");
	  get_top().main.dequeue_message();
	  if(g_strTempXMLObjectID != undefined && g_strTempXMLObjectID != null && g_strTempXMLObjectID != "")
	    XML_DeleteXMLObject(g_strTempXMLObjectID);
	  
	  // Void the last CC transaction
		get_pos().SignaturePadVoidLastCC({
									  app:get_top().main.get_current_obj(),
                    sig_cc_system_interface:g_str_sig_cc_system_interface
                    });
			                                  
	  Ingenico_DisplayAds();
	  return;
	}
	
	// Step 1 - Create the document and get the document ID
	var Ingenico_SignaturePadManualEntryCallback = function(strXMLObjectID, param)
	{
	    if(strXMLObjectID == undefined)
	    {
	      alert("Unable to process signature. Please cancel and try again.");
	      get_top().main.dequeue_message();
	      return;
	    }
	    g_strTempXMLObjectID = strXMLObjectID;
	    g_str_sig_cc_system_interface = sig_cc_system_interface;// Needed for voiding tenders when user cancels.
	    //-------------------------------------------------------------------
  	  
  	      // Step 2 B - add child elements recursivly
  	      var AddParentCallBack = function(result, param)
  	      {
  	        // Step 3 B - Append child element 1 result
  	        var AppendChild1CallBack = function(result, param)
  	        {
  	            // Step 4 B - Append child element 2 result
  	            var AppendChild2CallBack = function(result, param)
  	            {
      	            // Step 5 B - Append child element 3 result
  	                var AppendChild3CallBack = function(result, param)
  	                {
  	                  // Last step
  	                  // Step 6 A - All the recursive stuff completed, get the full XML object
  	                  var GetFullXMLDoc = function(full_doc, param)
  	                  {
  	                    // Check for errors in the service call
  	                    if(full_doc == undefined || full_doc == "")
	                      {
	                        alert("Unable to process signature. Please cancel and try again.");
	                        
	                        if(g_strTempXMLObjectID != undefined && g_strTempXMLObjectID != null && g_strTempXMLObjectID != "")
	                          XML_DeleteXMLObject(g_strTempXMLObjectID);
	                        
	                        get_top().main.dequeue_message();
	                        
	                        // Void the last CC transaction
		                      get_pos().SignaturePadVoidLastCC({
									                        app:get_top().main.get_current_obj(),
                                          sig_cc_system_interface:g_str_sig_cc_system_interface
                                          });
                                          
                          Ingenico_DisplayAds();
	                        return;
	                      }
	                      
	                      // Cleanup the XML object inside of the peripherals web service
	                      if(g_strTempXMLObjectID != "")
	                        XML_DeleteXMLObject(strXMLObjectID);
                        
                        // Performe the actual capture
                        get_top().main.dequeue_message();
	                      Ingenico_SignaturePadSignatureCapture('', strUserPrompt, total_line, full_doc, null/*Must be a null for the void to work*/);
  	                  }  	                  
                      // Step 6 B - Get XML document trigger
          	          AjaxCallPeripherals("xml_get_xml/" + g_strTempXMLObjectID, GetFullXMLDoc, Ingenico_CallbackError, "", "");
  	                }
  	                // Step 5 A - Append child element trigger
  	                AjaxCallPeripherals("xml_append_child_element/" + g_strTempXMLObjectID + "/" + "Action" + "/" + "I agree to pay the below amount in accordance with the terms and conditions of the cardmember agreement.", AppendChild3CallBack, "", "", "");
  	            }
  	            // Step 4 A - Append child element trigger
  	            AjaxCallPeripherals("xml_append_child_element/" + g_strTempXMLObjectID + "/" + "Verbiage" + "/" + "NA", AppendChild2CallBack, "", "", "");
  	        }
  	        // Step 3 A - Append child element trigger
  	        AjaxCallPeripherals("xml_append_child_element/" + g_strTempXMLObjectID + "/" + "DocClass" + "/" + "NA", AppendChild1CallBack, "", "", "");   
  	      }
  	      // Step 2 A - add XML parent element
  	      AjaxCallPeripherals("xml_add_parent_element/" + strXMLObjectID + "/" + "Signature", AddParentCallBack, Ingenico_CallbackError, "", "");
   	    }
	
	// Trigger for the recursion
	AjaxCallPeripherals("xml_init/Signatures", Ingenico_SignaturePadManualEntryCallback, Ingenico_CallbackError, "", "");
}

// Bug#9100 Mike O
function MarkTheScreenForManualCCEntry(sig_cc_system_interface, top_app/*Bug 9931 NJ*/)
{
	MarkScreenForManualCCEntry({ app:top_app,
															 sig_cc_system_interface:sig_cc_system_interface
															});
}

function MarkScreenForManualCCEntry(args) 
{
  // Bug 13720 DH - iPayment Peripheral Overhaul - Rewrote old functionality
  var nAgt = navigator.userAgent;  
  if (nAgt.indexOf("Safari") !=-1)
    post_remote('Device.Swipe','Device.Swipe.mark_screen_for_manual_cc_entry', args, "main", true);
  else
    post_remote('Device.Swipe','Device.Swipe.mark_screen_for_manual_cc_entry', args, "pos_frame", true);
}

function UnmarkTheScreenForManualCCEntry(top_app/*Added top_app for BUG# 9698 DH*/)
{
  // Bug 13720 DH - iPayment Peripheral Overhaul - Rewrote old functionality

  if(top_app == "")
    top_app = get_top().main.get_current_obj();
  
	get_pos().UnmarkScreenForManualCCEntry({app:top_app});
}

function UnmarkScreenForManualCCEntry (args) 
{
  var nAgt = navigator.userAgent;
  
  if (nAgt.indexOf("Chrome") !=-1)
    post_remote('Device.Swipe','Device.Swipe.unmark_screen_for_manual_cc_entry', args, "main", true, get_top().main.displayResult);
  else if (nAgt.indexOf("Safari") !=-1)
    post_remote('Device.Swipe','Device.Swipe.unmark_screen_for_manual_cc_entry', args, "main", true);
  else
    post_remote('Device.Swipe','Device.Swipe.unmark_screen_for_manual_cc_entry', args, "pos_frame"); 
}
//*************************************************************************


//***********************HANDEL MULTIPLE SIGNATURE PAD DOCS ********************************

var MultipleDocsArray = new Array();
var iMultipleDocSequence = 0;
function CollectAllDocs(doc_type, strUserPrompt, strConsumerPrompt, doc_class, action_yes, action_no, action_accept, action_decline, sig_capture_consumer_signature_prompt_text/*Bug# 20823 DH*/)
{
	var DocsArray = new Array();
	DocsArray[0]= doc_type;
	DocsArray[1] = strUserPrompt;
	DocsArray[2] = strConsumerPrompt;
	DocsArray[3] = doc_class;
	DocsArray[4] = action_yes;
	DocsArray[5] = action_no;
	DocsArray[6] = action_accept;
	DocsArray[7] = action_decline;
  DocsArray[8] = sig_capture_consumer_signature_prompt_text;//Bug# 20823 DH
	MultipleDocsArray[iMultipleDocSequence] = DocsArray;
	iMultipleDocSequence++;
}

function ResetMultipleDocArray() 
{
	// Reset the multiple doc array for next set of documents.
	delete MultipleDocsArray;
	MultipleDocsArray = new Array();
	iMultipleDocSequence = 0;
}

function Ingenico_ProcessDocumentResult(doc_type, result, action_accept, action_decline, action_yes, action_no, doc_class, strConsumerPrompt, top_app, strXMLObjectID)
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  
  // Bug 22921/22922/20586 MJO/NK - Set the result properly if this is a PB object
  if (result.Ingenico_AcceptDeclineResult)
    result = result.Ingenico_AcceptDeclineResult;
  else if (result.Ingenico_YesNoResult)
    result = result.Ingenico_YesNoResult;

  if(doc_type == "accept_decline")
	{	  
		if(action_accept == "1" || action_decline == "1")// Get signature at end.
			g_bIngenicoAddSignature = true;
			
		// Bug 13720 DH - iPayment Peripheral Overhaul - New service base XML object
		XMLObject_AppendChild(strXMLObjectID, "DocClass", doc_class);
		XMLObject_AppendChild(strXMLObjectID, "Verbiage", strConsumerPrompt);	
		
		if(result == "accept")
    {
      Ingenico_ConfirmationText = Ingenico_ConfirmationText + "|| Customer chose: " + "Accept"; // Bug 23589 NK

			// Terminate process
			if(action_accept == "2")
				result = "";
      if (action_accept == "3")
        result = "cancel";
		  XMLObject_AppendChild(strXMLObjectID, "Action", "accept");// Bug 13720 DH - iPayment Peripheral Overhaul - New service base XML object
		}
		else if(result == "decline")
    {
      Ingenico_ConfirmationText = Ingenico_ConfirmationText + "|| Customer chose: " + "Decline"; // Bug 23589 NK

			// Terminate process

      if (action_decline == "3")
        result = "cancel";
      if (action_decline == "2")
        result = "";
      if (action_decline == "3" || action_decline == "2")
      {
				// BUG# 7418 User has to accept the terms.
				if(strXMLObjectID != "")// Bug 13720 DH - iPayment Peripheral Overhaul - New service base XML object
          XML_DeleteXMLObject(strXMLObjectID);
          
        
			//	return;
			}
     
		  // BUG# 7418 User has to accept the terms.
		  if(strXMLObjectID != "")// Bug 13720 DH - iPayment Peripheral Overhaul - New service base XML object
			  XMLObject_AppendChild(strXMLObjectID, "Action", "decline");
		 }
		
    if (result == "cancel" || result == "")
	   {
		    if(strXMLObjectID != "")// Bug 13720 DH - iPayment Peripheral Overhaul - New service base XML object
          XML_DeleteXMLObject(strXMLObjectID);
          
		    ResetMultipleDocArray();
		    g_bIngenicoAddSignature = false;
		    Ingenico_DisplayAds();/*Bug 13720 DH - iPayment Peripheral Overhaul*/
        SignaturePadDocMoveNext(result, top_app);// BUG# 10554 DH - Limited printer support
		    return;
	    }
	}
 
  if(doc_type == "yes_no")// doc_type
	{		  
	  if(result == "yes" && action_yes == "1")// Get signature at end.
		{
			g_bIngenicoAddSignature = true;
		}
		else if(result == "no" && action_no == "1")// Get signature at end.
		{
			g_bIngenicoAddSignature = true;
		}
						
			// Bug 13720 DH - iPayment Peripheral Overhaul - New service base XML object
			XMLObject_AppendChild(strXMLObjectID, "DocClass", doc_class);
			XMLObject_AppendChild(strXMLObjectID, "Verbiage", strConsumerPrompt);
      if (Ingenico_ConfirmationText == undefined)
        Ingenico_ConfirmationText = "";
			if(result == "yes")
      {
        Ingenico_ConfirmationText = Ingenico_ConfirmationText + "|| Customer chose: " + "Yes"; //Bug 23589 NK

				// Terminate process
				if(action_yes == "2")
					result = "";
        if (action_yes == "3")
          result = "cancel";
			  XMLObject_AppendChild(strXMLObjectID, "Action", "yes");// Bug 13720 DH - iPayment Peripheral Overhaul - New service base XML object
			}
			else if(result == "no")
      {
        Ingenico_ConfirmationText =  Ingenico_ConfirmationText + "|| Customer chose: " + "No"; //Bug 23589 NK

				// Terminate process

        if (action_no == "2")
          result = "";
        if(action_no == "3")
          result = "cancel";

        if (action_no == "2" || action_no == "3" )
				{
					// BUG# 7418 User has to accept the terms.
					if(strXMLObjectID != "")// Bug 13720 DH - iPayment Peripheral Overhaul - New service base XML object
	          XML_DeleteXMLObject(strXMLObjectID);      		
				}
        
				// Bug 13720 DH - iPayment Peripheral Overhaul - New service base XML object
				if(strXMLObjectID != "")
				  XMLObject_AppendChild(strXMLObjectID, "Action", "no");
			}
			
      if (result == "cancel" || result == "" )
			{
				if(strXMLObjectID != "")// Bug 13720 DH - iPayment Peripheral Overhaul - New service base XML object
	        XML_DeleteXMLObject(strXMLObjectID);
	        
				ResetMultipleDocArray();
				g_bIngenicoAddSignature = false;
				Ingenico_DisplayAds();
				
				// Bug 13720 DH - iPayment Peripheral Overhaul
        // Bug 22921/22922/20586 MJO/NK - Always cancel here
				//if(!g_UserCanceledCardSwipe)
        SignaturePadDocMoveNext(result, top_app);// BUG# 10554 DH - Limited printer support
				return;
      }
      
		}
		
		Ingenico_ProcessDocument(strXMLObjectID, top_app);
}

function Ingenico_ProcessDocument(strXMLObjectID, top_app)
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  
  if(!g_IsIngenicoAvailable)
  {
    SignaturePadDocMoveNext('cancel', get_top().main.get_current_obj());
    return
  }
  
  if(g_iCurrentMultiDocIndex < g_iMultiDocCount)
  {
    var DocsArray					= MultipleDocsArray[g_iCurrentMultiDocIndex];
	  var doc_type					= DocsArray[0];
	  var strUserPrompt			= DocsArray[1];
	  var strConsumerPrompt = DocsArray[2];
	  var doc_class					= DocsArray[3];
	  var action_yes				= DocsArray[4];
	  var action_no					= DocsArray[5];
	  var action_accept			= DocsArray[6];
	  var action_decline		= DocsArray[7];
    var sig_capture_consumer_signature_prompt_text = DocsArray[8];//Bug# 20823 DH

    // Bug# 20823 DH
    // Update the prompt for the signature
    g_DevicePrompt = sig_capture_consumer_signature_prompt_text;
    // end Bug# 20823 DH

	  XMLObject_AddParentElement(strXMLObjectID, "Signature") // Bug #7928 Mike O - New signature element for each doc
    g_iCurrentMultiDocIndex = g_iCurrentMultiDocIndex + 1;
    
    if(doc_type == "yes_no")
	  {
		    Ingenico_SignaturePadYesNo(strUserPrompt, strConsumerPrompt, action_accept, action_decline, action_yes, action_no, doc_class, top_app, strXMLObjectID);
    }
    
    if(doc_type == "accept_decline")
		{
			  Ingenico_SignaturePadAcceptDecline(strUserPrompt, strConsumerPrompt, action_accept, action_decline, action_yes, action_no, doc_class, top_app, strXMLObjectID);
	  }
	  
	  return; // End of this function will finalize mutli documents. It should only execute when the index reaches the count of docs.
  }
  
  //==================================================================
  // Finalize the multidocs here
  
  if(g_bIngenicoAddSignature == true)
	{
	  var XMLDataCallbackTarget = function(data, param)
	  {
	    var XMLData = "";
	    if(data == "")
	      XMLData = "No Data Returned from xml_get_xml";
	     else
	      XMLData = data;
	    
	    Ingenico_MultiSignaturePadSignatureCapture("no_ads", "Please ask for signature", g_DevicePrompt/*Bug 20680 DH - Use configurable prompts in the Ingenico signature form*/, XMLData, top_app, strXMLObjectID);
	  }
	
		XMLObject_GetXML(strXMLObjectID, XMLDataCallbackTarget);
		return;
	}
	
	ResetMultipleDocArray();
	Ingenico_DisplayAds();
	SignaturePadDocMoveNext("cancel", top_app);// BUG# 10554 DH - Limited printer support
}

// Bug #14905 Mike O - Made prompts configurable
function ProcessMultiDocuments(top_app/*BUG# 9951 DH*/, device_prompt, user_prompt) 
{
    // Bug 20680 DH - Use configurable prompts in the Ingenico signature form
    g_DevicePrompt = device_prompt;

  // Bug 13720 DH - iPayment Peripheral Overhaul
  if(!g_IsIngenicoAvailable)
  {
    SignaturePadDocMoveNext('cancel', get_top().main.get_current_obj());
    return
  }
  
  // BUG# 11430 DH - CASL error
  if(top_app != null)// Bug 13720 DH - iPayment Peripheral Overhaul
  {
    if(top_app.substr(0, 3) == "GEO")// It could be recursive call due to document choices. If call originated in CASL then it has a GEO as the 1st 3 chars.
      top_app = top_app.substr(4);// Added top_app for BUG# 9951 DH
  }

	g_bIngenicoAddSignature = false;	
		
	var XMLObjectID_SuccessCallback = function(strXMLObjectID, param) 
  {
		if(strXMLObjectID == "")
			alert("Error creating an XML object via xml_init: " + rv);
		else
		{
			// Global indexes to control the iterations of all possible multi documents
	    g_iMultiDocCount = MultipleDocsArray.length;
	    g_iCurrentMultiDocIndex = 0;
    	
	    Ingenico_ProcessDocument(strXMLObjectID, top_app);
		}
		return;
	}
	
	var XMLObjectID_ErrorCallback = function(rv, param)
	{
	  alert("Error: " + rv.status + "\nStatus Text: " + rv.statusText );
	  return;
	}
	
	AjaxCallPeripherals("xml_init/Signatures", XMLObjectID_SuccessCallback, XMLObjectID_ErrorCallback, "", "");	
}

// BUG# 8611 DH
// Bug#9100 Mike O
function BlockUIInput() 
{
  // BUG# 12479 DH - More error checking to prevent crashing due to application timing and this screen.
  if(top != undefined && top != null)
  {
    if (top.main != undefined && top.main != null) 
    {
      if (top.main != undefined && top.main != null) 
      {
        if (top.main.enqueue_message != undefined && top.main.enqueue_message != null)
          top.main.enqueue_message({ content: "<BR/><CENTER>Processing. Please wait...</CENTER>", actions: {} });
      }
    }
  }
  // end BUG# 12479 DH
}

////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////XML Object/////////////////////////////////////////////

function XMLObject_AddParentElement(strXMLObjectID, strParentElement)
{		
	// Bug 13720 DH - iPayment Peripheral Overhaul
	var ErrorCallback = function(error, param)
  {
    alert("Unexpected error occurred. Please cancel and try again.\n" + error);
    get_top().main.dequeue_message();
    return;
  }
  
	AjaxCallPeripherals("xml_add_parent_element/" + strXMLObjectID + "/" + strParentElement, "", ErrorCallback, "", "");
}

function XMLObject_AppendChild(strXMLObjectID, strChildElement, strChildText)
{
	// Bug 13720 DH - iPayment Peripheral Overhaul
	
	if(strChildText == "")
	  strChildText = "NA";
	  
	var XMLObject_AppendChild_ErrorCallback = function(error, param)
  {
    alert("Unexpected error occurred. Please cancel and try again.\n" + error);
    get_top().main.dequeue_message();
  }
  
	AjaxCallPeripherals("xml_append_child_element/" + strXMLObjectID + "/" + strChildElement + "/" + strChildText, "", XMLObject_AppendChild_ErrorCallback, "", "");
}

function XMLObject_GetXML(strXMLObjectID, XMLDataCallbackTarget)
{		
	// Bug 13720 DH - iPayment Peripheral Overhaul
	AjaxCallPeripherals("xml_get_xml/" + strXMLObjectID, XMLDataCallbackTarget, XMLDataCallbackTarget, "", "");
}

function XML_DeleteXMLObject(strXMLObjectID)
{		
  // Bug 13720 DH - iPayment Peripheral Overhaul
  
  var ErrorCallback = function(error, param)
  {
    alert("Unexpected error occurred. Please cancel and try again.\n" + error);
    get_top().main.dequeue_message();
    return;
  }
  
  AjaxCallPeripherals("xml_delete_xml_object/" + strXMLObjectID, "", ErrorCallback, "", "");
}


////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

// BUG# 8705 DH
function des (key, message, encrypt, mode, iv, padding) 
{
  //declaring this locally speeds things up a bit
  var spfunction1 = new Array (0x1010400,0,0x10000,0x1010404,0x1010004,0x10404,0x4,0x10000,0x400,0x1010400,0x1010404,0x400,0x1000404,0x1010004,0x1000000,0x4,0x404,0x1000400,0x1000400,0x10400,0x10400,0x1010000,0x1010000,0x1000404,0x10004,0x1000004,0x1000004,0x10004,0,0x404,0x10404,0x1000000,0x10000,0x1010404,0x4,0x1010000,0x1010400,0x1000000,0x1000000,0x400,0x1010004,0x10000,0x10400,0x1000004,0x400,0x4,0x1000404,0x10404,0x1010404,0x10004,0x1010000,0x1000404,0x1000004,0x404,0x10404,0x1010400,0x404,0x1000400,0x1000400,0,0x10004,0x10400,0,0x1010004);
  var spfunction2 = new Array (-0x7fef7fe0,-0x7fff8000,0x8000,0x108020,0x100000,0x20,-0x7fefffe0,-0x7fff7fe0,-0x7fffffe0,-0x7fef7fe0,-0x7fef8000,-0x80000000,-0x7fff8000,0x100000,0x20,-0x7fefffe0,0x108000,0x100020,-0x7fff7fe0,0,-0x80000000,0x8000,0x108020,-0x7ff00000,0x100020,-0x7fffffe0,0,0x108000,0x8020,-0x7fef8000,-0x7ff00000,0x8020,0,0x108020,-0x7fefffe0,0x100000,-0x7fff7fe0,-0x7ff00000,-0x7fef8000,0x8000,-0x7ff00000,-0x7fff8000,0x20,-0x7fef7fe0,0x108020,0x20,0x8000,-0x80000000,0x8020,-0x7fef8000,0x100000,-0x7fffffe0,0x100020,-0x7fff7fe0,-0x7fffffe0,0x100020,0x108000,0,-0x7fff8000,0x8020,-0x80000000,-0x7fefffe0,-0x7fef7fe0,0x108000);
  var spfunction3 = new Array (0x208,0x8020200,0,0x8020008,0x8000200,0,0x20208,0x8000200,0x20008,0x8000008,0x8000008,0x20000,0x8020208,0x20008,0x8020000,0x208,0x8000000,0x8,0x8020200,0x200,0x20200,0x8020000,0x8020008,0x20208,0x8000208,0x20200,0x20000,0x8000208,0x8,0x8020208,0x200,0x8000000,0x8020200,0x8000000,0x20008,0x208,0x20000,0x8020200,0x8000200,0,0x200,0x20008,0x8020208,0x8000200,0x8000008,0x200,0,0x8020008,0x8000208,0x20000,0x8000000,0x8020208,0x8,0x20208,0x20200,0x8000008,0x8020000,0x8000208,0x208,0x8020000,0x20208,0x8,0x8020008,0x20200);
  var spfunction4 = new Array (0x802001,0x2081,0x2081,0x80,0x802080,0x800081,0x800001,0x2001,0,0x802000,0x802000,0x802081,0x81,0,0x800080,0x800001,0x1,0x2000,0x800000,0x802001,0x80,0x800000,0x2001,0x2080,0x800081,0x1,0x2080,0x800080,0x2000,0x802080,0x802081,0x81,0x800080,0x800001,0x802000,0x802081,0x81,0,0,0x802000,0x2080,0x800080,0x800081,0x1,0x802001,0x2081,0x2081,0x80,0x802081,0x81,0x1,0x2000,0x800001,0x2001,0x802080,0x800081,0x2001,0x2080,0x800000,0x802001,0x80,0x800000,0x2000,0x802080);
  var spfunction5 = new Array (0x100,0x2080100,0x2080000,0x42000100,0x80000,0x100,0x40000000,0x2080000,0x40080100,0x80000,0x2000100,0x40080100,0x42000100,0x42080000,0x80100,0x40000000,0x2000000,0x40080000,0x40080000,0,0x40000100,0x42080100,0x42080100,0x2000100,0x42080000,0x40000100,0,0x42000000,0x2080100,0x2000000,0x42000000,0x80100,0x80000,0x42000100,0x100,0x2000000,0x40000000,0x2080000,0x42000100,0x40080100,0x2000100,0x40000000,0x42080000,0x2080100,0x40080100,0x100,0x2000000,0x42080000,0x42080100,0x80100,0x42000000,0x42080100,0x2080000,0,0x40080000,0x42000000,0x80100,0x2000100,0x40000100,0x80000,0,0x40080000,0x2080100,0x40000100);
  var spfunction6 = new Array (0x20000010,0x20400000,0x4000,0x20404010,0x20400000,0x10,0x20404010,0x400000,0x20004000,0x404010,0x400000,0x20000010,0x400010,0x20004000,0x20000000,0x4010,0,0x400010,0x20004010,0x4000,0x404000,0x20004010,0x10,0x20400010,0x20400010,0,0x404010,0x20404000,0x4010,0x404000,0x20404000,0x20000000,0x20004000,0x10,0x20400010,0x404000,0x20404010,0x400000,0x4010,0x20000010,0x400000,0x20004000,0x20000000,0x4010,0x20000010,0x20404010,0x404000,0x20400000,0x404010,0x20404000,0,0x20400010,0x10,0x4000,0x20400000,0x404010,0x4000,0x400010,0x20004010,0,0x20404000,0x20000000,0x400010,0x20004010);
  var spfunction7 = new Array (0x200000,0x4200002,0x4000802,0,0x800,0x4000802,0x200802,0x4200800,0x4200802,0x200000,0,0x4000002,0x2,0x4000000,0x4200002,0x802,0x4000800,0x200802,0x200002,0x4000800,0x4000002,0x4200000,0x4200800,0x200002,0x4200000,0x800,0x802,0x4200802,0x200800,0x2,0x4000000,0x200800,0x4000000,0x200800,0x200000,0x4000802,0x4000802,0x4200002,0x4200002,0x2,0x200002,0x4000000,0x4000800,0x200000,0x4200800,0x802,0x200802,0x4200800,0x802,0x4000002,0x4200802,0x4200000,0x200800,0,0x2,0x4200802,0,0x200802,0x4200000,0x800,0x4000002,0x4000800,0x800,0x200002);
  var spfunction8 = new Array (0x10001040,0x1000,0x40000,0x10041040,0x10000000,0x10001040,0x40,0x10000000,0x40040,0x10040000,0x10041040,0x41000,0x10041000,0x41040,0x1000,0x40,0x10040000,0x10000040,0x10001000,0x1040,0x41000,0x40040,0x10040040,0x10041000,0x1040,0,0,0x10040040,0x10000040,0x10001000,0x41040,0x40000,0x41040,0x40000,0x10041000,0x1000,0x40,0x10040040,0x1000,0x41040,0x10001000,0x40,0x10000040,0x10040000,0x10040040,0x10000000,0x40000,0x10001040,0,0x10041040,0x40040,0x10000040,0x10040000,0x10001000,0x10001040,0,0x10041040,0x41000,0x41000,0x1040,0x1040,0x40040,0x10000000,0x10041000);

  //create the 16 or 48 subkeys we will need
  var keys = des_createKeys (key);
  var m=0, i, j, temp, temp2, right1, right2, left, right, looping;
  var cbcleft, cbcleft2, cbcright, cbcright2
  var endloop, loopinc;
  var len = message.length;
  var chunk = 0;
  //set up the loops for single and triple des
  var iterations = keys.length == 32 ? 3 : 9; //single or triple des
  if (iterations == 3) {looping = encrypt ? new Array (0, 32, 2) : new Array (30, -2, -2);}
  else {looping = encrypt ? new Array (0, 32, 2, 62, 30, -2, 64, 96, 2) : new Array (94, 62, -2, 32, 64, 2, 30, -2, -2);}

  //pad the message depending on the padding parameter
  if (padding == 2) message += "        "; //pad the message with spaces
  else if (padding == 1) {temp = 8-(len%8); message += String.fromCharCode (temp,temp,temp,temp,temp,temp,temp,temp); if (temp==8) len+=8;} //PKCS7 padding
  else if (!padding) message += "\0\0\0\0\0\0\0\0"; //pad the message out with null bytes

  //store the result here
  result = "";
  tempresult = "";

  if (mode == 1) { //CBC mode
    cbcleft = (iv.charCodeAt(m++) << 24) | (iv.charCodeAt(m++) << 16) | (iv.charCodeAt(m++) << 8) | iv.charCodeAt(m++);
    cbcright = (iv.charCodeAt(m++) << 24) | (iv.charCodeAt(m++) << 16) | (iv.charCodeAt(m++) << 8) | iv.charCodeAt(m++);
    m=0;
  }

  //loop through each 64 bit chunk of the message
  while (m < len) {
    left = (message.charCodeAt(m++) << 24) | (message.charCodeAt(m++) << 16) | (message.charCodeAt(m++) << 8) | message.charCodeAt(m++);
    right = (message.charCodeAt(m++) << 24) | (message.charCodeAt(m++) << 16) | (message.charCodeAt(m++) << 8) | message.charCodeAt(m++);

    //for Cipher Block Chaining mode, xor the message with the previous result
    if (mode == 1) {if (encrypt) {left ^= cbcleft; right ^= cbcright;} else {cbcleft2 = cbcleft; cbcright2 = cbcright; cbcleft = left; cbcright = right;}}

    //first each 64 but chunk of the message must be permuted according to IP
    temp = ((left >>> 4) ^ right) & 0x0f0f0f0f; right ^= temp; left ^= (temp << 4);
    temp = ((left >>> 16) ^ right) & 0x0000ffff; right ^= temp; left ^= (temp << 16);
    temp = ((right >>> 2) ^ left) & 0x33333333; left ^= temp; right ^= (temp << 2);
    temp = ((right >>> 8) ^ left) & 0x00ff00ff; left ^= temp; right ^= (temp << 8);
    temp = ((left >>> 1) ^ right) & 0x55555555; right ^= temp; left ^= (temp << 1);

    left = ((left << 1) | (left >>> 31)); 
    right = ((right << 1) | (right >>> 31)); 

    //do this either 1 or 3 times for each chunk of the message
    for (j=0; j<iterations; j+=3) {
      endloop = looping[j+1];
      loopinc = looping[j+2];
      //now go through and perform the encryption or decryption  
      for (i=looping[j]; i!=endloop; i+=loopinc) { //for efficiency
        right1 = right ^ keys[i]; 
        right2 = ((right >>> 4) | (right << 28)) ^ keys[i+1];
        //the result is attained by passing these bytes through the S selection functions
        temp = left;
        left = right;
        right = temp ^ (spfunction2[(right1 >>> 24) & 0x3f] | spfunction4[(right1 >>> 16) & 0x3f]
              | spfunction6[(right1 >>>  8) & 0x3f] | spfunction8[right1 & 0x3f]
              | spfunction1[(right2 >>> 24) & 0x3f] | spfunction3[(right2 >>> 16) & 0x3f]
              | spfunction5[(right2 >>>  8) & 0x3f] | spfunction7[right2 & 0x3f]);
      }
      temp = left; left = right; right = temp; //unreverse left and right
    } //for either 1 or 3 iterations

    //move then each one bit to the right
    left = ((left >>> 1) | (left << 31)); 
    right = ((right >>> 1) | (right << 31)); 

    //now perform IP-1, which is IP in the opposite direction
    temp = ((left >>> 1) ^ right) & 0x55555555; right ^= temp; left ^= (temp << 1);
    temp = ((right >>> 8) ^ left) & 0x00ff00ff; left ^= temp; right ^= (temp << 8);
    temp = ((right >>> 2) ^ left) & 0x33333333; left ^= temp; right ^= (temp << 2);
    temp = ((left >>> 16) ^ right) & 0x0000ffff; right ^= temp; left ^= (temp << 16);
    temp = ((left >>> 4) ^ right) & 0x0f0f0f0f; right ^= temp; left ^= (temp << 4);

    //for Cipher Block Chaining mode, xor the message with the previous result
    if (mode == 1) {if (encrypt) {cbcleft = left; cbcright = right;} else {left ^= cbcleft2; right ^= cbcright2;}}
    tempresult += String.fromCharCode ((left>>>24), ((left>>>16) & 0xff), ((left>>>8) & 0xff), (left & 0xff), (right>>>24), ((right>>>16) & 0xff), ((right>>>8) & 0xff), (right & 0xff));

    chunk += 8;
    if (chunk == 512) {result += tempresult; tempresult = ""; chunk = 0;}
  } //for every 8 characters, or 64 bits in the message

  //return the result as an array
  return result + tempresult;
} //end of des

// BUG# 8705 DH
function des_createKeys (key) 
{
  //declaring this locally speeds things up a bit
  pc2bytes0  = new Array (0,0x4,0x20000000,0x20000004,0x10000,0x10004,0x20010000,0x20010004,0x200,0x204,0x20000200,0x20000204,0x10200,0x10204,0x20010200,0x20010204);
  pc2bytes1  = new Array (0,0x1,0x100000,0x100001,0x4000000,0x4000001,0x4100000,0x4100001,0x100,0x101,0x100100,0x100101,0x4000100,0x4000101,0x4100100,0x4100101);
  pc2bytes2  = new Array (0,0x8,0x800,0x808,0x1000000,0x1000008,0x1000800,0x1000808,0,0x8,0x800,0x808,0x1000000,0x1000008,0x1000800,0x1000808);
  pc2bytes3  = new Array (0,0x200000,0x8000000,0x8200000,0x2000,0x202000,0x8002000,0x8202000,0x20000,0x220000,0x8020000,0x8220000,0x22000,0x222000,0x8022000,0x8222000);
  pc2bytes4  = new Array (0,0x40000,0x10,0x40010,0,0x40000,0x10,0x40010,0x1000,0x41000,0x1010,0x41010,0x1000,0x41000,0x1010,0x41010);
  pc2bytes5  = new Array (0,0x400,0x20,0x420,0,0x400,0x20,0x420,0x2000000,0x2000400,0x2000020,0x2000420,0x2000000,0x2000400,0x2000020,0x2000420);
  pc2bytes6  = new Array (0,0x10000000,0x80000,0x10080000,0x2,0x10000002,0x80002,0x10080002,0,0x10000000,0x80000,0x10080000,0x2,0x10000002,0x80002,0x10080002);
  pc2bytes7  = new Array (0,0x10000,0x800,0x10800,0x20000000,0x20010000,0x20000800,0x20010800,0x20000,0x30000,0x20800,0x30800,0x20020000,0x20030000,0x20020800,0x20030800);
  pc2bytes8  = new Array (0,0x40000,0,0x40000,0x2,0x40002,0x2,0x40002,0x2000000,0x2040000,0x2000000,0x2040000,0x2000002,0x2040002,0x2000002,0x2040002);
  pc2bytes9  = new Array (0,0x10000000,0x8,0x10000008,0,0x10000000,0x8,0x10000008,0x400,0x10000400,0x408,0x10000408,0x400,0x10000400,0x408,0x10000408);
  pc2bytes10 = new Array (0,0x20,0,0x20,0x100000,0x100020,0x100000,0x100020,0x2000,0x2020,0x2000,0x2020,0x102000,0x102020,0x102000,0x102020);
  pc2bytes11 = new Array (0,0x1000000,0x200,0x1000200,0x200000,0x1200000,0x200200,0x1200200,0x4000000,0x5000000,0x4000200,0x5000200,0x4200000,0x5200000,0x4200200,0x5200200);
  pc2bytes12 = new Array (0,0x1000,0x8000000,0x8001000,0x80000,0x81000,0x8080000,0x8081000,0x10,0x1010,0x8000010,0x8001010,0x80010,0x81010,0x8080010,0x8081010);
  pc2bytes13 = new Array (0,0x4,0x100,0x104,0,0x4,0x100,0x104,0x1,0x5,0x101,0x105,0x1,0x5,0x101,0x105);

  //how many iterations (1 for des, 3 for triple des)
  var iterations = key.length > 8 ? 3 : 1; //changed by Paul 16/6/2007 to use Triple DES for 9+ byte keys
  //stores the return keys
  var keys = new Array (32 * iterations);
  //now define the left shifts which need to be done
  var shifts = new Array (0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0);
  //other variables
  var lefttemp, righttemp, m=0, n=0, temp;

  for (var j=0; j<iterations; j++) { //either 1 or 3 iterations
    left = (key.charCodeAt(m++) << 24) | (key.charCodeAt(m++) << 16) | (key.charCodeAt(m++) << 8) | key.charCodeAt(m++);
    right = (key.charCodeAt(m++) << 24) | (key.charCodeAt(m++) << 16) | (key.charCodeAt(m++) << 8) | key.charCodeAt(m++);

    temp = ((left >>> 4) ^ right) & 0x0f0f0f0f; right ^= temp; left ^= (temp << 4);
    temp = ((right >>> -16) ^ left) & 0x0000ffff; left ^= temp; right ^= (temp << -16);
    temp = ((left >>> 2) ^ right) & 0x33333333; right ^= temp; left ^= (temp << 2);
    temp = ((right >>> -16) ^ left) & 0x0000ffff; left ^= temp; right ^= (temp << -16);
    temp = ((left >>> 1) ^ right) & 0x55555555; right ^= temp; left ^= (temp << 1);
    temp = ((right >>> 8) ^ left) & 0x00ff00ff; left ^= temp; right ^= (temp << 8);
    temp = ((left >>> 1) ^ right) & 0x55555555; right ^= temp; left ^= (temp << 1);

    //the right side needs to be shifted and to get the last four bits of the left side
    temp = (left << 8) | ((right >>> 20) & 0x000000f0);
    //left needs to be put upside down
    left = (right << 24) | ((right << 8) & 0xff0000) | ((right >>> 8) & 0xff00) | ((right >>> 24) & 0xf0);
    right = temp;

    //now go through and perform these shifts on the left and right keys
    for (var i=0; i < shifts.length; i++) {
      //shift the keys either one or two bits to the left
      if (shifts[i]) {left = (left << 2) | (left >>> 26); right = (right << 2) | (right >>> 26);}
      else {left = (left << 1) | (left >>> 27); right = (right << 1) | (right >>> 27);}
      left &= -0xf; right &= -0xf;

      //now apply PC-2, in such a way that E is easier when encrypting or decrypting
      //this conversion will look like PC-2 except only the last 6 bits of each byte are used
      //rather than 48 consecutive bits and the order of lines will be according to 
      //how the S selection functions will be applied: S2, S4, S6, S8, S1, S3, S5, S7
      lefttemp = pc2bytes0[left >>> 28] | pc2bytes1[(left >>> 24) & 0xf]
              | pc2bytes2[(left >>> 20) & 0xf] | pc2bytes3[(left >>> 16) & 0xf]
              | pc2bytes4[(left >>> 12) & 0xf] | pc2bytes5[(left >>> 8) & 0xf]
              | pc2bytes6[(left >>> 4) & 0xf];
      righttemp = pc2bytes7[right >>> 28] | pc2bytes8[(right >>> 24) & 0xf]
                | pc2bytes9[(right >>> 20) & 0xf] | pc2bytes10[(right >>> 16) & 0xf]
                | pc2bytes11[(right >>> 12) & 0xf] | pc2bytes12[(right >>> 8) & 0xf]
                | pc2bytes13[(right >>> 4) & 0xf];
      temp = ((righttemp >>> 16) ^ lefttemp) & 0x0000ffff; 
      keys[n++] = lefttemp ^ temp; keys[n++] = righttemp ^ (temp << 16);
    }
  } //for each iterations
  //return the keys we've created
  return keys;
} //end of des_createKeys

//***********************END HANDEL MULTIPLE SIGNATURE PAD DOCS ********************************

//----------------- end SignaturePad BUG# 6368 added by DH -------------------




/*
  ---------------------------- Printer --------------------------------
  Printer
    Printer functions do not automagically add newlines.
    PrinterString( station, data )
    PrinterBitmap( station, data )
    PrinterBarcode( station, data )
    PrinterCut()
    PrinterOpen()
    PrinterClose()
*/

function ProcessNextInAJAXQueue(parm1, param2/*AJAX callback required*/)
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  if(g_CallQueueForAsyncAJAX == null)
	return;
	
  pos = g_CallQueueForAsyncAJAX.length;
  
  var x = null;  
  if(pos > 0)
    x = g_CallQueueForAsyncAJAX[0];
  
  if(x != null && x != undefined)
  {
    RemoveOldestElementFromAJAXQueue();

    // Bug# 26243 DH - Credit card receipts sometimes don't print (intermittent issue)
    // The most important timing is between cutting the receipt and the next slip, but 200 milliseconds seems to be an OK trade-off so we don't have to fish for the cutting command in the array.
    setTimeout(function () { x(); }, 50);
    // End Bug# 26243 DH
  }
}

function ClearAJAXQueue(param1, param2)
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  
  // Clearing the queue will be usually done on errors in AJAX calls.
  // For example, if there was a error printing then there is no point in cutting the paper.
  if(g_CallQueueForAsyncAJAX == null)
	return;
	
  while(g_CallQueueForAsyncAJAX.length != 0)
    g_CallQueueForAsyncAJAX.pop();
}

function RemoveOldestElementFromAJAXQueue(param1, param2/*If function used as a AJAX callback then param 1 & 2 is required*/)
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  if(g_CallQueueForAsyncAJAX == null)
	return;
	
  // Remove the oldest element that was added. This should be at zero pos.
  if(g_CallQueueForAsyncAJAX.length != -1)
    g_CallQueueForAsyncAJAX.splice(0, 1);
}

function AddToAJAXQueue(method)
{
  // Bug 13720 DH - iPayment Peripheral Overhaul 
  // Add to the top so the oldest is at the lowest index.
  // The oldest elements will always be processed first and then removed so the next zero will be ready to be processed. 
  if(g_CallQueueForAsyncAJAX == null)
	g_CallQueueForAsyncAJAX = new Array();
	
  g_CallQueueForAsyncAJAX.push(method);
}

function PrinterString(station, data)
{
  // BUG# 10904 RDM Device DH - We are using RDM device instead
  if(RDM_IsAvailable())
  {
    RDM_PrintMultipleLines(data);
    return;
  } 

  data = data.replace(/\u2013|\u2014/g, "-"); //Bug 22864 - Merge to base Bug #22815 - special characters cause receipt not to print. Replacing "en dash" and "em dash" with hyphen

  // Bug# 23408 DH
  if (IsEpsonS9000Connected() && station == 4 /*deposits only*/)
  {
    g_S9000DepositData += data;
    return;
  }
  // end Bug# 23408 DH

  // Bug 13720 DH - iPayment Peripheral Overhaul
  var base16String = {};
  base16String.strPrintData = ConvertToBase16(data);// Bug# 25780 DH - Moved the ConvertToBase16 to this file.
	base16String.strStation = station;
  
	// Add to queue for later. The CUT function will be the trigger to fire the first queued function.
	// The print data comes in pieces and all at the same time. This queue provides synchronized calls to the device.

  AddToAJAXQueue(function(){ return AjaxCallPeripherals("epson_print", ProcessNextInAJAXQueue, ClearAJAXQueue, "", base16String);});
	// end bug 13720 DH
}

function PrinterBitmap(station, data) 
{
  // NEED TO IMPLEMENT FOR 13720
  // BUG# 10904 RDM Device DH - We are using RDM device instead. This is not supported.
  if(RDM_IsAvailable())
    return
 
 // Bug 13720 DH - iPayment Peripheral Overhaul
 AjaxCallPeripherals("epson_printer_bitmap/" + station + "/" + data, "", "", "", "");
}

function PrinterBarcode(station, data) 
{
  // BUG# 10904 RDM Device DH - We are using RDM device instead. This is not supported.
  if(RDM_IsAvailable())
    return;

 // Bug 13720 DH - iPayment Peripheral Overhaul
 AddToAJAXQueue(function(){ return AjaxCallPeripherals("epson_print_barcode_on_receipt/" + station + "/" + data, ProcessNextInAJAXQueue, ClearAJAXQueue, "", "");});
}

function cancel_reprint_btn()
{
  /*Bug# 18706 DH - Deposit slip re-printing is attempting to scan the document, but it should only print*/
  
  get_top().main.dequeue_message();
  AjaxCallPeripherals("epson_printer_cancel", "", "", "", "");// Bug# 23408 DH
  AjaxCallPeripherals("epson_printer_remove", "", "", "", "");
  return;
}

function ok_reprint_btn()
{
  /*Bug# 18706 DH - Deposit slip re-printing is attempting to scan the document, but it should only print*/

  AddToAJAXQueue(function() { return AjaxCallPeripherals("epson_printer_remove", ProcessNextInAJAXQueue, ClearAJAXQueue, "", ""); });
  ProcessNextInAJAXQueue("", "");
  get_top().main.dequeue_message();
  return;
}

function SendToPrinter(deposit, reprinting_deposit/*Bug# 18706 DH - Deposit slip re-printing is attempting to scan the document, but it should only print*/)
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  
  // If this is a deposit, handle it here. All parts of the deposit processing must be fired in sequence. 
  // The old code needed to be rewritten this way to function properly. BTW the deposit printing has been broken in the trunk code for about two years.
  // Now it finally works in the peripherals overhaul.
  if (deposit == true) 
  {
    /*Bug# 18706 DH - Deposit slip re-printing is attempting to scan the document, but it should only print*/
    if (reprinting_deposit != undefined && reprinting_deposit != "GEO.opt" && reprinting_deposit == true) 
    {      
      get_top().main.dequeue_message();// Remove and replace the standard prompt here, the other one could also be used for other functionality    
      cancel_btn = "<button onclick=get_pos().cancel_reprint_btn()> Cancel </button>";
      ok_btn = "<button onclick=get_pos().ok_reprint_btn()>   OK   </button>";

      if (g_EpsonH2000Connected)// Bug# 24486 & 23408 DH
        get_top().main.enqueue_message({ content: "<CENTER>" + "<br/>Please insert deposit slip face down and press OK <br/><br/>" + "</CENTER>" + ok_btn + cancel_btn, actions: {} });
      else if (IsEpsonS9000Connected())// Bug# 23408 DH - Face down does not apply here.
        get_top().main.enqueue_message({ content: "<CENTER>" + "<br/>Please insert deposit slip<br/><br/>" + "</CENTER>", actions: {} });
      else
        get_top().main.enqueue_message({ content: "<CENTER>" + "<br/>Please insert deposit slip and press OK <br/><br/>" + "</CENTER>" + ok_btn + cancel_btn, actions: {} });

      // Bug# 23408 DH
      if (IsEpsonS9000Connected())
      {
        var DepositReprintCallback = function (result, param)
        {
          get_top().main.dequeue_message();
          ClearAJAXQueue();
          AjaxCallPeripherals("epson_printer_cancel", "", "", "", "");
          return;
        }

        var base16String = {};
        g_S9000DepositData = g_S9000DepositData.replace(/&#27/g, '');
        g_S9000DepositData = g_S9000DepositData.replace(/;|N/g, '\n');
        g_S9000DepositData = g_S9000DepositData.replace(/\|/g, '');
        base16String.strPrintData = ConvertToBase16(g_S9000DepositData);// Bug# 25780 DH - Moved the ConvertToBase16 to this file.
        base16String.strStation = 4;
        ClearAJAXQueue();
        AddToAJAXQueue(function () { return AjaxCallPeripherals("epson_printer_change_print_side/" + 4, ProcessNextInAJAXQueue, ClearAJAXQueue, "", ""); });
        AddToAJAXQueue(function () { return AjaxCallPeripherals("epson_print", ProcessNextInAJAXQueue, ClearAJAXQueue, "", base16String); });

        // Just so we wait for a longer return to remove the previous screen message with a delay since printing is happening async.
        AddToAJAXQueue(function () { return AjaxCallPeripherals("epson_printer_process_card_scanner_doc", DepositReprintCallback, ClearAJAXQueue, "", ""); });

        ProcessNextInAJAXQueue("", "");
      }
      // end Bug# 23408 DH

      return;
    }
    
    var ImageCaptureCallback = function(result, param)// Send image back to screen
    {
      if (result.Epson_PrinterProcessImageDocResult.iLastErrorCode == 0)
      {      
        // Bug# 18706 DH
        Device_Scanner_receive_image({
        app:GetCurrentObject(),
        content:result.Epson_PrinterProcessImageDocResult.strImage_FRONT_DataString,
        content_type:result.Epson_PrinterProcessImageDocResult.strImageFormat});
        get_top().main.dequeue_message();
        return;
      }
      else 
      {
        // Bug 20940 NK
        if (g_EpsonDocImaging)
        {
          // Bug# 25673 DH - Do not display this error if user canceled.
          if (result.Epson_PrinterProcessImageDocResult.strLastErrorMessage != "User Canceled")
            alert("Error saving deposit slip image");
        }
        get_top().main.dequeue_message();
        return;
      }
    }

    // Bug# 24486 DH
    if (g_EpsonH2000Connected)
    {
      if (g_CallQueueForAsyncAJAX != null)
      {
        g_CallQueueForAsyncAJAX.splice(0, 0, function (){return AjaxCallPeripherals("epson_printer_remove", ProcessNextInAJAXQueue, ClearAJAXQueue, "", "")});
        g_CallQueueForAsyncAJAX.splice(1, 0, function (){return AjaxCallPeripherals("epson_printer_insert", ProcessNextInAJAXQueue, ClearAJAXQueue, "", "")});
      }
    }
    else
      if (!IsEpsonS9000Connected())// Bug# 23408 DH
        AddToAJAXQueue(function () { return AjaxCallPeripherals("epson_printer_remove", ProcessNextInAJAXQueue, ClearAJAXQueue, "", ""); });
    // end Bug# 24486 DH  

    if (g_EpsonDocImaging)// Bug 21721 DH - Skip this step for non imaging printers
    {
      if (!IsEpsonS9000Connected())// Bug# 23408 DH
      {
        AddToAJAXQueue(function () { return AjaxCallPeripherals("epson_printer_scanner_direct_io/" + 13 + "/" + CHK_DI_MODE_CHECKSCANNER + "/" + "empty", ProcessNextInAJAXQueue, ClearAJAXQueue, "", ""); }); /*Bug# 18706 DH - Not sure how this one call changed to a call bypassing the Queue*/
        AddToAJAXQueue(function () { return AjaxCallPeripherals("epson_printer_process_image_doc", ImageCaptureCallback, ClearAJAXQueue, "", ""); });
      }
    }

    // Bug# 24486 DH    
    if (g_EpsonH2000Connected)
    {
      var RemoveDoc = function (result, param)
      {
        get_top().main.dequeue_message();
      }
      AddToAJAXQueue(function () { return AjaxCallPeripherals("epson_printer_remove", RemoveDoc, "", "", ""); });
    }
    // end Bug# 24486 DH

    // Bug# 23408 DH
    if (IsEpsonS9000Connected())
    {
      var s9000DepositScanCallback = function (result, param)
      {
        if (result.Epson_PrinterProcessCardScannerDocResult.iLastErrorCode == 0 && result.Epson_PrinterProcessCardScannerDocResult.strLastErrorMessage != "User Canceled")
        {
          args = new Array();
          args["app"] = GetCurrentObject();
          args["content_image_front"] = result.Epson_PrinterProcessCardScannerDocResult.strImage_FRONT_DataString;
          args["image_front_tag"] = "EPSON_S9000_FRONT_IMAGE";
          args["content_type"] = "image/tiff";
          post_remote('Device.Scanner', 'Device.Scanner.receive_epson_front_and_back_images', args, 'main', true);
          get_top().main.dequeue_message();
        }
        else if (result.Epson_PrinterProcessCardScannerDocResult.iLastErrorCode == -1)
        {
          // Bug# 25094 DH
          if (result.Epson_PrinterProcessCardScannerDocResult.strLastErrorMessage != "User Canceled")
            alert("Error scanning deposit image. Please try again.");
        }

        get_top().main.dequeue_message();// Bug# 25094 DH
        ClearAJAXQueue();
      }

      var base16String = {};
      g_S9000DepositData = g_S9000DepositData.replace(/&#27/g, '');
      g_S9000DepositData = g_S9000DepositData.replace(/;|N/g, '\n');
      g_S9000DepositData = g_S9000DepositData.replace(/\|/g, '');
      base16String.strPrintData = ConvertToBase16(g_S9000DepositData);// Bug# 25780 DH - Moved the ConvertToBase16 to this file.
      base16String.strStation = 4;
      ClearAJAXQueue();
      AddToAJAXQueue(function () { return AjaxCallPeripherals("epson_printer_change_print_side/" + 4, ProcessNextInAJAXQueue, ClearAJAXQueue, "", ""); });
      AddToAJAXQueue(function () { return AjaxCallPeripherals("epson_print", ProcessNextInAJAXQueue, ClearAJAXQueue, "", base16String); });
      AddToAJAXQueue(function () { return AjaxCallPeripherals("epson_printer_process_card_scanner_doc", s9000DepositScanCallback, s9000DepositScanCallback, "", ""); });
    }
    // end Bug# 23408 DH

    // Bug# 25522 DH
    if (g_EpsonH2000Connected)
    {
      // We'll need to hold on to the array and only send it when the user inserted the doc.
      // If the array is sent to the printer before the doc is inserted, it's impossible to cancel on the H2000 device. The prompt goes away, but the printers continue to wait.
      var ReadyToPrintH2000Deposit = function (result, param)
      {
        ProcessNextInAJAXQueue("", "");
      }
      
      var ReadyToInsertH2000Deposit = function (result, param)
      {
        AjaxCallPeripherals("epson_printer_insert", ReadyToPrintH2000Deposit, ClearAJAXQueue, "", "")
      }

      // Take out zero and one since we will send that over again to get the callback.
      if (g_CallQueueForAsyncAJAX != null)
      {
        RemoveOldestElementFromAJAXQueue();// Remove
        RemoveOldestElementFromAJAXQueue();// Insert
      }

      AjaxCallPeripherals("epson_printer_remove", ReadyToInsertH2000Deposit, ClearAJAXQueue, "", "")
    }// end Bug# 25522 DH
    else// Normal process
      ProcessNextInAJAXQueue("", "");
  }
  else 
  {      
    ProcessNextInAJAXQueue("", ""); // Normal processing
  }
}

function PrinterCut()
{
  // BUG# 10904 RDM Device DH - We are using RDM device instead. This is not supported.
  if(RDM_IsAvailable())
    return;

  // Bug 13720 DH - iPayment Peripheral Overhaul
  AddToAJAXQueue(function(){ return AjaxCallPeripherals("epson_cut_receipt", ProcessNextInAJAXQueue, ClearAJAXQueue, "", "");});
}

function PrinterChangePrintSide(side) 
{
  // BUG# 10904 RDM Device DH - We are using RDM device instead. This is not supported.
  if(RDM_IsAvailable())
    return;
    
  // Bug 13720 DH - iPayment Peripheral Overhaul
  AddToAJAXQueue(function(){ return AjaxCallPeripherals("epson_printer_change_print_side/" + side, ProcessNextInAJAXQueue, ClearAJAXQueue, "", "");});
}

function PrinterRotate(station, rotation) 
{
  // BUG# 10904 RDM Device DH - We are using RDM device instead. This is not supported.
  if(RDM_IsAvailable())
    return;
  
  // Bug 13720 DH - iPayment Peripheral Overhaul
  AddToAJAXQueue(function(){ return AjaxCallPeripherals("epson_printer_rotate/" + station + "/" + rotation, ProcessNextInAJAXQueue, ClearAJAXQueue, "", "");});
}

// Bug 13720 DH - iPayment Peripheral Overhaul
  // Commented out. No more use for it.
  // function PrinterOpen(){}
// end bug 13720 DH

// Bug 13720 DH - iPayment Peripheral Overhaul
  // Commented out. No more use for it.
  // function PrinterClose() {}
// end bug 13720 DH

function PrinterInsert() 
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  AjaxCallPeripherals("epson_printer_insert", "", "", "", "");
  return true;
}

function PrinterInsertH2000(DocType)
{
  // IPAY-198 DH
  g_DocType = DocType;
  AjaxCallPeripherals("epson_printer_insert", Device_Printer_accept_callback, "", "", "");
  return true;
}

function PrinterRemove() 
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  AjaxCallPeripherals("epson_printer_remove", "", "", "", "");
	return true;
	// end bug 13720 DH
}

function PrinterReady(callbackNotifier) 
{
  // Bug 13720 DH - iPayment Peripheral Overhaul

  g_S9000DepositData = "";// Bug# 23408 DH

  AjaxCallPeripherals("epson_printer_ready", Device_Printer_accept_callback, "", "", ""); 
  
  return true;
	// end bug 13720 DH
}

function PrinterCancel()
{
  // Bug 13720 DH - iPayment Peripheral Overhaul

  if (IsEpsonS9000Connected())// Bug# 23914 DH - It was already canceled.
    return;
  
  if (g_bIsEpsonPrinterAvailable)// Bug# 27506 DH
    AjaxCallPeripherals("epson_printer_cancel", "", "", "", "");
}

function PrinterAccepted() 
{ 
  // Bug 13720 DH - iPayment Peripheral Overhaul
  // This function is not used.
}

function PrinterLogo(station, data) 
{
  // Bug 13720 DH - iPayment Peripheral Overhaul - The PrinterDirectIO has been changed.
  
  // Bug# 18309 DH � Add the ability to configure the receipt header image.
  // A better design would be to buffer the logos on the peripherals station, but that's a bit risky and the price of sending the image every time is acceptable.
  // Each logo bitmap is only 2-3k and this approach will prevent printing wrong logos and/or not deleting the old ones from the server.
  if (g_CallQueueForAsyncAJAX.length == 0) {
    // Make sure the logo is on top by inserting it as the first item to process.
    if (g_DepartmentLogo != "" && g_DepartmentLogo != undefined)
      AddToAJAXQueue(function() { return AjaxCallPeripherals("epson_print_receipt_logo", ProcessNextInAJAXQueue, ClearAJAXQueue, "", g_DepartmentLogo); });
  }
}
 
//bug 6021 & 7058
var PRINTER_RETRY_MAX = 2;
function CheckPrinterEmpty()
{
// BUG# 10904 RDM Device DH - We are using RDM device instead. This is not supported.
  if(RDM_IsAvailable())
    return 0;
    
	var health;
	for(var i=0;i < PRINTER_RETRY_MAX; i++) {
	  health = CheckHealth();
		if(health == 0) return health;
		else if(health == 106) PrinterRemove();
	}
	CheckHealthEx();
	return health;
  }
  
/* PrinterDirectIO is a wrapper for the scanner's DirectIO function */
var PTR_DI_PRINT_FLASH_BITMAP = 111,
    PTR_DI_SELECT_SLIP	  	  = 120,
    PTR_DI_SLIP_CHANGE_SIDE	  = 121,
    PTR_DI_SLIP_FULLSLIP      =   0,
    PTR_DI_SLIP_VALIDATION	  =   1,
    PTR_DI_SLIP_ENDORSEMENT	  =   2,
    PTR_BM_CENTER             =  -2;

// Bug 20145 MJO - Delay printing the back
function Epson_PrintEndorsementOrValidationBothSides(return_val, iteration_data, doc_type, user_prompt, skip_scan, endorsement_char_size/*Bug 17825 DH*/) 
{
  Side2Function = function () { Epson_PrintEndorsementOrValidation(return_val, iteration_data, doc_type, user_prompt, false, endorsement_char_size/*Bug 17825 DH*/) };
}

function Epson_PrintEndorsementOnS9000(data, size, bEpson_print_data_back/*Bug 23916 DH*/)
{
  // Bug# 21811 DH - Endorsement must be sent first for this model.

  g_bEpson_print_data_back = bEpson_print_data_back;/*Bug 23916 DH*/

  // Bug 21811 DH
    if (!IsEpsonS9000Connected())
    return;

  if (g_bEpson_print_data_back == undefined || g_bEpson_print_data_back == null || g_bEpson_print_data_back == false)// Bug 23916 DH
    return;

    var base16String = {};
  base16String.strPrintData = ConvertToBase16(data);// Bug# 25780 DH - Moved the ConvertToBase16 to this file.
	  base16String.strStation = 4;
	  AjaxCallPeripherals("epson_print", "", "", "", base16String);
    return;
}

// Called From: Document.Epson.<defmethod _name='remove_document'>
var DocType = undefined;
var UserPrompt = undefined;
var Side2Function = false; // Bug 20145 MJO - Delay printing the back
// Bug 20145 MJO - Added skip_scan so it's only called once when printing both sides
function Epson_PrintEndorsementOrValidation(return_val, iteration_data, doc_type, user_prompt, skip_scan, endorsement_char_size/*Bug 17825 DH*/)
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  // Function prints recursivily calling itself
  // First we change the side and then we call printing.

  // Bug# 24361 DH - Validate endorsement formatting ahead of check processing to prevent incomplete check scanning.
  if (return_val.indexOf("Invalid format") != -1)
  {
    get_top().main.dequeue_message();
    PrinterRemove();
    alert("The endorsement or validation message configured is in an incorrect format. \n\nPlease correct the message text and try again.\n\nSee log for details.");
    return -1;
  }
  // end Bug# 24361 DH

  // Bug 24368 DH - If a document is converted from another, some fields are not saved in the database and come through as invalid parameters. Make sure we have a value in those cases.
  if (endorsement_char_size == "" || endorsement_char_size == undefined)
    endorsement_char_size = "LARGE"

  // Bug 20145 MJO - Added skip_scan so it's only called once when printing both sides
  if (skip_scan === undefined)
    skip_scan = false;

  // Bug# 21811 DH - Endorsement was sent already as the first request to the S9000.
  if (IsEpsonS9000Connected())
     return;
  
  // Bug 17825 DH
  AjaxCallPeripherals("slp_line_chars/" + endorsement_char_size, "", "", "", "");
    
  if(RDM_IsAvailable())
  {
    RDM_PrintMultipleLines(return_val);
    return;
  }

  // Save these for the recursive last step.
  if(doc_type != undefined)
  {
    DocType = doc_type;
    UserPrompt = user_prompt;
    
    // Sometimes this is empty message
    if(user_prompt == "GEO.opt")
      UserPrompt = "";
  }
  
  var Epson_PrintEndorsementOrValidation_Print = function(result, param)
  {
    var base16String = {};
    base16String.strPrintData = ConvertToBase16(param);// Bug# 25780 DH - Moved the ConvertToBase16 to this file.
	  base16String.strStation = 4;
	  AjaxCallPeripherals("epson_print", Epson_PrintEndorsementOrValidation_Scan, "", Epson_PrintEndorsementOrValidationError, base16String);
    return;
  }
  
  var Epson_PrintEndorsementOrValidation_Scan = function(result, param)
  {
    // Bug 20145 MJO - Added skip_scan so it's only called once when printing both sides
    if (!skip_scan)
    ScannerReady(DocType, UserPrompt);
    else {
      Side2Function();
  }
  }
  
  var Epson_PrintEndorsementOrValidationError = function(result, param)
  {
    alert("Error: " + result);
  }
  
  if(iteration_data == "PTR_DI_SLIP_ENDORSEMENT")
  {
    AjaxCallPeripherals("epson_printer_change_print_side/" + PTR_DI_SLIP_ENDORSEMENT, Epson_PrintEndorsementOrValidation_Print, Epson_PrintEndorsementOrValidationError, return_val, "");
  }
  else if(iteration_data == "PTR_DI_SLIP_VALIDATION")
  {
    AjaxCallPeripherals("epson_printer_change_print_side/" + PTR_DI_SLIP_VALIDATION, Epson_PrintEndorsementOrValidation_Print, Epson_PrintEndorsementOrValidationError, return_val, "");
  }
}

// Called From: Create_core_item_app.<defmethod _name='handle_printer'>
function Epson_PrintEndorsementOrValidation2(return_val, iteration_data)
{
  // Bug# 25796 - Checks are not ejected when processing without MICR
  if (return_val.Epson_PrintReceiptResult == "-1")
    return;

  // IPAY-198 DH
  // Only for the H2000 doc_type F & B and missing M.
  var final_handler = function ()
  {
    if (g_EpsonH2000Connected && (g_DocType.indexOf("F") != -1 || g_DocType.indexOf("B") != -1) & g_DocType.indexOf("M") == -1)
    {
      EpsonRemoveDocument();
      get_top().main.dequeue_message();
    }
  }

  // Bug 13720 DH - iPayment Peripheral Overhaul
  // Function prints recursively calling itself
  // First we change the side and then we call printing.
  if (RDM_IsAvailable())
  {
    RDM_PrintMultipleLines(return_val);
    return;
  }

  if (iteration_data == "PTR_DI_SLIP_ENDORSEMENT")
  {
    AjaxCallPeripherals("epson_printer_change_print_side/" + PTR_DI_SLIP_ENDORSEMENT, Epson_PrintEndorsementOrValidation2, "", return_val, "");
  }
  else if (iteration_data == "PTR_DI_SLIP_VALIDATION")
    AjaxCallPeripherals("epson_printer_change_print_side/" + PTR_DI_SLIP_VALIDATION, Epson_PrintEndorsementOrValidation2, "", return_val, "");
  else
  {
    var base16String = {};
    base16String.strPrintData = ConvertToBase16(iteration_data);// Bug# 25780 DH - Moved the ConvertToBase16 to this file.
    base16String.strStation = 4;

    AjaxCallPeripherals("epson_print", final_handler, "", "", base16String);// IPAY-198 DH
  }
}

function PrinterDirectIO(Command, pData, pString)
{
  // BUG# 10904 RDM Device DH - We are using RDM device instead. This is not supported.
  if(RDM_IsAvailable())
    return;
  
  // Bug 13720 DH - iPayment Peripheral Overhaul
  if(pString == "")// Mark it for the service, it makes the call simpler.
    pString = "empty";
    

  AjaxCallPeripherals("epson_direct_io/" + Command + "/" + pData + "/" + pString, "", "", "", "");
  return 0;
}

/*
  Micr
    Micr functions are funny
  MP: Many of these functions are no longer used (I think)
*/



/*
function MicrCallback(transit, account, serial) {
  get_top().result.document.open();
  get_top().result.document.writeln('<BODY><FORM id="a_form" method="POST" action="http://localhost/pos_demo/GEO">' +
   '<INPUT style="display:none;" name="serial"  value="' + serial  + '"></INPUT>' + 
   '<INPUT style="display:none;" name="account" value="' + account + '"></INPUT>' + 
   '<INPUT style="display:none;" name="transit" value="' + transit + '"></INPUT>' + '</FORM></BODY>');
  get_top().result.document.close();
  get_top().result.a_form.submit();
}
*/
function MicrInsert() 
{
  // Function replaced by: Bug 13720 DH - iPayment Peripheral Overhaul
  AjaxCallPeripherals("epson_micr_insert", "", "", "", "");
  return true; 
}
 
function MicrRemove() 
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  AjaxCallPeripherals("epson_micr_remove", "", "", "", "");
  return true;
}
  
function MicrRead()
{
  // Function replaced by: Bug 13720 DH - iPayment Peripheral Overhaul
  AjaxCallPeripherals("epson_micr_read", "", "", "", "");
}

function MicrReady(DocType, message, franking, no_micr_selected_in_config, auto_continue/* Bug 22715 DH */, aba_validation /*Bug 22729 DH*/, bEpson_print_data_back/*Bug 23916 DH*/)
{
  if (IsP80Available())// Bug# 24950 DH
    return;

  g_DocType = DocType;/*Bug# 24736 DH*/
  g_S9000DepositData = "";// Bug# 23408 DH

  g_NoMICRSelectedInConfig = no_micr_selected_in_config;// Bug 21375 DH
  g_bfranking = franking;//Bug 20641 DH
  g_auto_continue = auto_continue//Bug 22715 DH
  g_aba_validation = aba_validation;//Bug 22729 DH
  g_aba_validation_error = false;//Bug 22729 DH
  g_bEpson_print_data_back = bEpson_print_data_back;/*Bug 23916 DH*/

  // Bug 13720 DH - iPayment Peripheral Overhaul
  if(message != "opt" && message != "GEO.opt" && message != "" && message != undefined)
  {
    setTimeout(function() {get_top().main.enqueue_message(message); }, 10);//Bug# 19053 DH
  }
  
  setTimeout(function() {MICRReadyAcync(message); }, 100);//Bug# 19053 DH
}

function MICRReadyAcyncCallbackError(param1, param2)
{
  alert("MICRReadyAcyncCallbackError function Error");
}

// Bug 13720 DH - iPayment Peripheral Overhaul
function MICRReadyAcyncCallbackSuccess(param1, param2)
{
  // Bug 13720 DH - iPayment Peripheral Overhaul

   // Bug 22729 DH
  var sMsg = "Error reading check MICR.\nRequired MICR field is missing.";
  if (param1.Epson_PrinterMICRReadyResult.strLastErrorMessage === "Printer not available.")
    sMsg = "Printer not available.";
   // end Bug 22729 DH

  // Bug 21811 DH
  if(param1 == undefined || param1 == null || param1 == "")
  {
    alert("Error reading check MICR.\n\nService error.");
    RaiseErrorEvent(1, -1, "Error reading check MICR.\n\nService error.");
    get_top().main.dequeue_message();
    g_bfranking = false;// Bug# 22661 DH - MICR error, do not frank
    EpsonRemoveDocument("");
    return -1;
  }
  
  if(param1.Epson_PrinterMICRReadyResult.iLastErrorCode != 0)
  {
    // Bug# 22939 DH
    if (param1.Epson_PrinterMICRReadyResult.strLastErrorMessage === "User Canceled")
    {
      get_top().main.dequeue_message();
      return -1;
    }
    // end Bug# 22939 DH

    // Bug# 23227 DH
    alert(sMsg);
    // end Bug# 23227 DH

    RaiseErrorEvent(1, -1, "Error reading check MICR. Required MICR field is missing.");
    get_top().main.dequeue_message();
    g_bfranking = false;// Bug# 22661 DH - MICR error, do not frank
    EpsonRemoveDocument("");
    return -1;
  }
  // end Bug 21811 DH

  if(param1 != undefined)
  {
    if(param1.Epson_PrinterMICRReadyResult.iLastErrorCode == 0)
    {
      // Bug 22729 DH
      if (param1.Epson_PrinterMICRReadyResult.strEpsonMICRSerialNumber.length < 1 || param1.Epson_PrinterMICRReadyResult.strEpsonMICRAccountNumber.length < 1 || param1.Epson_PrinterMICRReadyResult.strEpsonMICRBankNumber.length < 1)
      {
        alert(sMsg);
        RaiseErrorEvent(1, -1, "Error reading check MICR. Required MICR field is missing.");
        get_top().main.dequeue_message();
        g_bfranking = false;
        EpsonRemoveDocument("");
        return -1;
      }
      // end Bug 22729 DH

      // Bug# 22661 DH
      if (g_bfranking == true)
      {
        if(param1.Epson_PrinterMICRReadyResult.strEpsonMICROnUS != "" && param1.Epson_PrinterMICRReadyResult.strEpsonMICROnUS != null && param1.Epson_PrinterMICRReadyResult.strEpsonMICROnUS != undefined)
        {
            // This validation OnUs length comes from the official CORE ICL validation "ICLValidate_SI.casl". I think Steve got the bug description incorrect.
            if (param1.Epson_PrinterMICRReadyResult.strEpsonMICROnUS.length > 19) {
                g_bfranking = false;
                alert("Checks with more than 19 characters in the On Us field may not be deposited electronically.");
            }
         }
      }
      // end Bug# 22661 DH

      Device_Micr_receive_callback(param1.Epson_PrinterMICRReadyResult.strEpsonMICRTransitNumber, 
      param1.Epson_PrinterMICRReadyResult.strEpsonMICRAccountNumber, 
      param1.Epson_PrinterMICRReadyResult.strEpsonMICRSerialNumber, 
      param1.Epson_PrinterMICRReadyResult.strEpsonMICRRawData, 
      param1.Epson_PrinterMICRReadyResult.strEpsonMICRCountryCode, 
      param1.Epson_PrinterMICRReadyResult.strEpsonMICRCheckType, 
      param1.Epson_PrinterMICRReadyResult.strEpsonMICREPC, 
      param1.Epson_PrinterMICRReadyResult.strEpsonMICRAmount, 
      param1.Epson_PrinterMICRReadyResult.strEpsonMICRBankNumber, 
      param1.Epson_PrinterMICRReadyResult.strEpsonMICROnUS == undefined ? "" : param1.Epson_PrinterMICRReadyResult.strEpsonMICROnUS/*Bug# 25656 DH, Bug# 26183 DH*/,
      param1.Epson_PrinterMICRReadyResult.strEpsonMICRAuxOnUS == undefined ? "" : param1.Epson_PrinterMICRReadyResult.strEpsonMICRAuxOnUS/*Bug# 25656 DH, Bug# 26183 DH*/);// Bug# 25093 DH
    }	
    else if(param1.Epson_PrinterMICRReadyResult.iLastErrorCode != 0)
    {
      // Bug 21375 DH
      // Since the 'M' is not present, but we have a MICR read, this means that it was forced MICR flow to keep the integrity of the flow and bypass the MICR collection and error reporting.
       if(g_NoMICRSelectedInConfig == true)
       {
          Device_Micr_receive_callback("", "", "", "", "", "", "", "", "", "", "");// Bug# 25093 DH
       }
      else
      {
         alert("Error reading check MICR");
         RaiseErrorEvent(1, param1.Epson_PrinterMICRReadyResult.iLastErrorCode, param1.Epson_PrinterMICRReadyResult.strLastErrorMessage);
      }
    }
  }
}

function ValidateEpsonMICRPart1(strEpsonMICRSerialNumber, strEpsonMICRAccountNumber, strEpsonMICRBankNumber, strEpsonMICRRawData)
{
  // Bug 21811 DH

  if(strEpsonMICRSerialNumber == null || strEpsonMICRSerialNumber == undefined || strEpsonMICRSerialNumber == "" ||
      strEpsonMICRAccountNumber == null || strEpsonMICRAccountNumber == undefined || strEpsonMICRAccountNumber == "" ||
      strEpsonMICRBankNumber == null || strEpsonMICRBankNumber == undefined || strEpsonMICRBankNumber == "" ||
      strEpsonMICRRawData == null || strEpsonMICRRawData == undefined || strEpsonMICRRawData == "")
      {
        return false;
      }

  return true;
}

function ValidateEpsonMICRPart2(strEpsonMICRSerialNumber, strEpsonMICRAccountNumber, strEpsonMICRBankNumber, strEpsonMICRRawData)
{
  // Bug 21811 DH
  
  len = strEpsonMICRSerialNumber.length + strEpsonMICRAccountNumber.length + strEpsonMICRBankNumber.length;
  
  if(strEpsonMICRSerialNumber.length < 1 || strEpsonMICRAccountNumber.length < 9 || strEpsonMICRBankNumber.length < 1 || strEpsonMICRRawData.length < len || 
        IsValidMICR(strEpsonMICRRawData) != true)
        {
          return false;
        }

  return true;
}

function MICRReadyAcync(message/*In case I will need it later*/) 
{
  // Bug 13720 DH - iPayment Peripheral Overhaul

  // Bug 21811 DH
  if (IsEpsonS9000Connected())
  {    
    var GetAllCheckData = function(param1, param)// Send image back to screen
    {
      // Bug 21811 DH
      if(param1 == undefined || param1 == null || param1 == "")
      {
        alert("Error reading check MICR.\n\nService error.");
        RaiseErrorEvent(1, -1, "Error reading check MICR.\n\nService error.");
        get_top().main.dequeue_message();
        EpsonRemoveDocument("");
        return -1;
      }
  
      if(param1.Epson_PrinterMICRReadyResult.iLastErrorCode != 0)
      {
          // Bug# 22576 DH
          if (param1.Epson_PrinterMICRReadyResult.strLastErrorMessage.indexOf("User Canceled") > -1){
              get_top().main.dequeue_message();
              return -1;
          }
          // end Bug# 22576 DH
          
        alert("Error reading check MICR.\nRequired MICR field is missing.");
        RaiseErrorEvent(1, -1, "Error reading check MICR. Required MICR field is missing.");
        get_top().main.dequeue_message();
        EpsonRemoveDocument(""); 
        return -1;
      }

      if(param1.Epson_PrinterMICRReadyResult.iLastErrorCode == 0)
      {
        // Bug# 22447 DH - I removed extra validation

        // Bug 23375 DH - The Epson S9000 does not report MICR error correctly.
        if (param1.Epson_PrinterMICRReadyResult.strEpsonMICRTransitNumber === "" || param1.Epson_PrinterMICRReadyResult.strEpsonMICRTransitNumber == null ||
            param1.Epson_PrinterMICRReadyResult.strEpsonMICRAccountNumber === "" || param1.Epson_PrinterMICRReadyResult.strEpsonMICRAccountNumber == null ||
            param1.Epson_PrinterMICRReadyResult.strEpsonMICRRawData === "" || param1.Epson_PrinterMICRReadyResult.strEpsonMICRRawData == null)
        {
          alert("Error reading check MICR.\nRequired MICR field is missing.");
          RaiseErrorEvent(1, -1, "Error reading check MICR. Required MICR field is missing.");
          get_top().main.dequeue_message();
          EpsonRemoveDocument("");
          return -1;
        }
        // end Bug 23375 DH

        // Bug 21791 DH
        if (param1.Epson_PrinterMICRReadyResult.strEpsonMICRCheckType != undefined)
        {
          if (g_isValidateACH && param1.Epson_PrinterMICRReadyResult.strEpsonMICRCheckType == "2")
          {
            alert("Commercial Check: Unable to convert to ACH");
            RaiseErrorEvent(1, -1, "Commercial Check: Unable to convert to ACH");
            get_top().main.dequeue_message();
            EpsonRemoveDocument("");
            return -1;
          }
        }
        // end Bug 21791 DH

        // Update the server
        Device_Micr_receive_callback(param1.Epson_PrinterMICRReadyResult.strEpsonMICRTransitNumber, 
        param1.Epson_PrinterMICRReadyResult.strEpsonMICRAccountNumber, 
        param1.Epson_PrinterMICRReadyResult.strEpsonMICRSerialNumber, 
        param1.Epson_PrinterMICRReadyResult.strEpsonMICRRawData, 
        param1.Epson_PrinterMICRReadyResult.strEpsonMICRCountryCode, 
        param1.Epson_PrinterMICRReadyResult.strEpsonMICRCheckType, 
        param1.Epson_PrinterMICRReadyResult.strEpsonMICREPC, 
        param1.Epson_PrinterMICRReadyResult.strEpsonMICRAmount, 
        param1.Epson_PrinterMICRReadyResult.strEpsonMICRBankNumber, 
        param1.Epson_PrinterMICRReadyResult.strEpsonMICROnUS == undefined ? "" : param1.Epson_PrinterMICRReadyResult.strEpsonMICROnUS/*Bug# 25656 DH, Bug# 26183 DH*/,
        param1.Epson_PrinterMICRReadyResult.strEpsonMICRAuxOnUS == undefined ? "" : param1.Epson_PrinterMICRReadyResult.strEpsonMICRAuxOnUS/*Bug# 25656 DH, Bug# 26183 DH*/);// Bug# 25093 DH Bug# 26183 DH
        
        args                        = new Array();
        args["app"]                 = GetCurrentObject();/*Bug# 23520 DH*/
        args["content_image_front"] = param1.Epson_PrinterMICRReadyResult.strImage_FRONT_DataString;
        args["content_image_back"]  = param1.Epson_PrinterMICRReadyResult.strImage_BACK_DataString;

        if(g_strEpsonImageSiteToCapture.toUpperCase() == "FRONT")
          args["content_image_back"] = "";

        if(g_strEpsonImageSiteToCapture.toUpperCase() == "BACK")
        {
          args["content_image_front"] = param1.Epson_PrinterMICRReadyResult.strImage_BACK_DataString;
          args["content_image_back"]  = "";
        }

        if(g_strEpsonImageSiteToCapture.toUpperCase() == "FRONT AND BACK")
        {
          args["content_image_front"] = param1.Epson_PrinterMICRReadyResult.strImage_FRONT_DataString;
          args["content_image_back"]  = param1.Epson_PrinterMICRReadyResult.strImage_BACK_DataString;
        }
        args["image_front_tag"] = "EPSON_S9000_FRONT_IMAGE";// Bug# 23863 DH
        args["image_back_tag"] = "EPSON_S9000_BACK_IMAGE"; // Bug# 23863 DH
        args["content_type"]    = "image/tiff";

        // Bug# 23002 DH
        // Bug# 23408 DH - Wait for print to submit the images for deposit.
        if (g_DocType != "deposit")
        {
          setTimeout(function ()
          {
            // Need to give iPayment some time to react. S9000 returns all data at the same time.
            Epson_Receive_front_and_Back_Images(args);// Bug# 22486 DH
          }, 1000);
        }
        // end Bug# 23002 DH
        
        get_top().main.dequeue_message();
        ScannerRemove();
        return true;
      }
    }

    AjaxCallPeripherals("epson_printer_micr_ready", GetAllCheckData, "", "", "");
    return true;
  }
  // end Bug 21811 DH
  
  AjaxCallPeripherals("epson_printer_micr_ready", MICRReadyAcyncCallbackSuccess, "", "", "");
  return true;
}

function Epson_Receive_front_and_Back_Images(args)// Bug# 22486 DH
{
  args["doc_type"]=g_DocType; //Bug# 24736 DH

  if (g_doc_count == undefined || g_doc_count == "1")
    post_remote('Device.Scanner', 'Device.Scanner.receive_epson_front_and_back_images', args, 'main', true);
  else
  {
    if (g_iCurrent_document_index == g_iNbr_of_documents_to_process)
      post_remote('Device.Scanner', 'Device.Scanner.receive_epson_front_and_back_images', args, 'main', true);
    else
      post_remote('Device.Scanner', 'Device.Scanner.receive_epson_front_and_back_images', args, 'pos_frame', true);
  }
}

function MicrCancel() 
{
  // Bug 13720 DH - iPayment Peripheral Overhaul

  // Bug# 22576 DH
  AjaxCallPeripherals("epson_printer_micr_cancel", "", "", "", "");
 
 return true;
}

//function MicrAccepted() 
//{ Commented out for: Bug 13720 DH - iPayment Peripheral Overhaul
//}


/*
  ---------------------------- SCANNER --------------------------------
*/


var CHK_DI_RETRIEVEQUALITY = 17,
    CHK_DI_SHARPNESS_IMAGE = 14,
    CHK_DI_IMAGESIDE       = 16,
    CHK_DI_SHARPNESS_OFF   = 0x00,
    CHK_DI_SHARPNESS_ON    = 0x01,
    CHK_DI_200X200         = 0x01;
    CHK_DI_OBVERSE_SIDE    = 0x01;
    CHK_DI_REVERSE_SIDE    = 0x02;
    CHK_DI_BOTH_SIDE       = 0x03;
    
// Bug 13720 DH - iPayment Peripheral Overhaul
function ScannerOpenCallbackSuccess(param1, param2)
{
  IsScannerOpen = (result == 0);
  
  ScannerDirectIO(CHK_DI_RETRIEVEQUALITY, CHK_DI_200X200, "");
  ScannerDirectIO(CHK_DI_SHARPNESS_IMAGE, CHK_DI_SHARPNESS_ON, "");
  ScannerDirectIO(CHK_DI_IMAGESIDE, CHK_DI_OBVERSE_SIDE, "");
  
  return true;
}

function EpsonScanCardSetMessage(message, resolved_message, card_app)
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  g_card_scanner_user_message = message;

  // Bug# 22486 DH
  g_resolved_message = resolved_message;
  g_card_app = card_app;
  // end Bug# 22486 DH
}

function EpsonScanCard(AJAXSuccessValue, StepID)
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  // Start of the card scanner - recursive process with each step.
  
  // Bug 21811 DH
    if (IsEpsonS9000Connected())// No need for it for the S9000
  {
    var CallbackTarget = function(){return ScannerReady("C", g_card_scanner_user_message);}
    ScannerChangeMode(CHK_DI_MODE_CARDSCANNER, CallbackTarget);
    return 0;
  }
  // end Bug 21811 DH


  // AJAXSuccessValue = AJAX success returned value
  // StepID = switch catch
  if(AJAXSuccessValue != 0)// First time call will be undefined, every next call should be zero indicating success on the peripherals side.
  {
    if(AJAXSuccessValue != undefined)
    {
      alert("Unable to scan the Photo ID. Please try again.");
      ScannerCancel();
     return;
    }
  }
  
  if(StepID == undefined)
  {
    AjaxCallPeripherals("epson_printer_scanner_close", EpsonScanCard, "", "2", "");
    return;
  }
  
  if(StepID == "2")
  {
    AjaxCallPeripherals("epson_printer_scanner_open", EpsonScanCard, "", "3", "");
    return;
  }
  
  if(StepID == "3")
  {    
    var CallbackTarget = function(){return ScannerReady("C", g_card_scanner_user_message);}
    ScannerChangeMode(CHK_DI_MODE_CARDSCANNER, CallbackTarget);
    return;
  }
}

function EpsonScanCheck(DocType, UserMessage)
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  var CallbackTarget = function(){return ScannerReady(DocType, UserMessage);}
  ScannerChangeMode(CHK_DI_MODE_CHECKSCANNER, CallbackTarget);
}

function ScannerInsert() 
{  
  // Function replaced for Bug 13720 DH - iPayment Peripheral Overhaul
	
  // Bug 21811 DH
    if (IsEpsonS9000Connected())// No need for it for the S9000
  {
     return 0;
  }
  // end Bug 21811 DH

	AjaxCallPeripherals("epson_printer_scanner_insert", "", "", "", "");
	
	return 0;
}

function ScannerRemove() 
{ 
  // Function replaced for Bug 13720 DH - iPayment Peripheral Overhaul
  AjaxCallPeripherals("epson_printer_scanner_remove", "", "", "", "");
	
	return 0; 
}

function ScannerScan() 
{ 
  // Function replaced for Bug 13720 DH - iPayment Peripheral Overhaul
  AjaxCallPeripherals("epson_printer_scanner_scan", "", "", "", "");
	
	return 0;  
}
  
//----------------------------------------------------------------------
// BUG# 9721 - Epson OCR support DH
function DefineOCRCoordinates(iOCR_x, iOCR_y, iOCR_width, iOCR_height) 
{ 
  g_bWaiting_For_Epson_OCR_and_Image = false;
  if(iOCR_x != 0 || iOCR_y != 0 || iOCR_width != 0 || iOCR_height != 0)
    g_bWaiting_For_Epson_OCR_and_Image = true;
  
  AjaxCallPeripherals("epson_printer_define_ocr_coordinates/" + iOCR_x + "/" + iOCR_y + "/" + iOCR_width + "/" + iOCR_height, "", "", "", "");
 }

// BUG# 9721 - Epson OCR support DH
function OCR_Data_Callback(ocr_data, ocr_data_error)
{
  //-----------------------------------------------------------------------------------------
  // Bug 12797 DH - OCR image disappears after number is displayed
  // Need to capture the OCR and the image and send them to the screen together in one call.
  if(g_bWaiting_For_Epson_OCR_and_Image)
  {
    g_bTemp_OCR_DATA = ocr_data;
    g_bTemp_OCR_DATA_ERROR = ocr_data_error;
    if(g_bTemp_Image_Ready)// OR keep waiting.
    {
      DeviceScannerReceive_Image_and_OCR();
      return;
    }
    return;// Keep waiting
  }
  //-----------------------------------------------------------------------------------------
  
  Device_Scanner_receive_ocr({app:GetCurrentObject(), ocr_data:ocr_data,ocr_data_error:ocr_data_error});
}

// BUG# 9721 - Epson OCR support DH
function Device_Scanner_receive_ocr (args) 
{ 
  //post_remote(GetCurrentObject(),'Device.Scanner.receive_ocr', args, 'pos_frame');//BUG#9641
  // Bug #11845 Mike O - Refer to the displayResult in the current frame
  // Bug #9889 Mike O - Return result into pos_frame (just Javascript now)
    post_remote(GetCurrentObject(), 'Device.Scanner.receive_ocr', args, 'pos_frame', true);//BUG#9641 <!--Bug #19440 NK Support for OCR parsing-->
}
//----------------------------------------------------------------------

function EpsonSetDocImagingSide(doc_side)
{
  // Bug 21811 DH
  g_strEpsonImageSiteToCapture = doc_side;
}

function EpsonSetCurrectDocumentState(confirming_documents, doc_count, auto_continue /* Bug 22715 DH */)
{
    // Bug# 22486 DH
    g_confirming_documents = confirming_documents;
    g_doc_count = doc_count
    g_auto_continue = auto_continue/* Bug 22715 DH */
}

// BUG# 10554 DH - Limited printer support
function LateRemoval() 
{
   Device_Scanner_receive_callback("", "");
   PrinterRemove();// Bug 13720 DH - iPayment Peripheral Overhaul
}
//end BUG# 10554 DH - Limited printer support

function ScannerReady(DocType /*Bug 12797 DH - OCR image disappears after number is displayed*/, message)
{
  g_DocType = DocType;/*Bug# 24736 DH*/

  if (IsP80Available())// Bug# 24950 DH
    return 0;

  // Bug 13720 DH - iPayment Peripheral Overhaul

  // Bug 21811 DH
  if (IsEpsonS9000Connected() && DocType == "D" && !g_bWaiting_For_Epson_OCR_and_Image)
  {
    if (message != "opt" && message != "GEO.opt" && message != "" && message != undefined)
      setTimeout(function () { get_top().main.enqueue_message(message); }, 0);// Bug 23230 DH

    // Bug 21811 DH - Plain image. Do not mix with images for checks and OCR, it will not work. S9000 fires ALL events for everything processed. This means I need to turn of MICR and OCR for this plain image. Otherwize we will get errors for unrelated options.
    RemoveMessageAfterProcessing = true;
    setTimeout(function () { AjaxCallPeripherals("epson_printer_process_card_scanner_doc", HandleEpsonDuealIDImages, "", RemoveMessageAfterProcessing, ""); }, 100);//Bug 23230 DH
    return;
  }

    if (IsEpsonS9000Connected() && !g_bWaiting_For_Epson_OCR_and_Image && DocType != "C")
  {
    // This is probably part of a check micr request
    return 0;
  }
  // end Bug 21811 DH

  if (!g_EpsonDocImaging)// TTS 18670 DH
  {
    // Bug# 19053 DH. Message probably setup by CASL code, since this is probably the last step, lets remove it.
    if (message == undefined || message == "" || message == "GEO.opt")
      get_top().main.dequeue_message();

    // Bug 22715 DH
    if (g_auto_continue)
    {
      setTimeout(function()
      {
        get_top().main.enqueue_message({ content: "<BR/><CENTER>Processing peripherals. Please wait...</CENTER>", actions: {} });
        EpsonRemoveDocument("");
      }, 10);
      setTimeout(function()
      {
        if (g_auto_continue)
        {
          // Bug 22729 DH
          try
          {
            var el = get_top().main.document.getElementById("args.bank_routing_nbr");
            if (el == HTMLInputElement)
            {
              el.onblur();
              el.onchange();
            }
          }
          catch (e)
          {
            //...
          }
          // end Bug 22729 DH

          get_top().main.form_inst['_method_name.continue'].value = 'continue';
          get_top().main.form_inst.submit();
        }
      }, 4000);
    }
    else
      EpsonRemoveDocument("");
    // end Bug 22715 DH  
    
    return -1;
  }
  
  // Bug# 20806 DH - Skip the OCR processing
  if(g_EpsonOCRSupported == false && DocType == "D" && g_bWaiting_For_Epson_OCR_and_Image == true)
  {
    return -1;
  }

  if(message != "opt" && message != "GEO.opt" && message != "" && message != undefined)
  {
    // Bug# 22486 DH - Need to implement a custom skip. Otherwise next doc is called to fast before cancel has a chance to cancel at the service.
    if (DocType == "C")
    {
      cancel_btn = "<button onclick=get_pos().Epson_CanceledCardID()>Cancel</button>";
      get_top().main.enqueue_message({ content: "<CENTER>" + g_resolved_message + "</CENTER><BR/>" + cancel_btn, actions: {} });
    }
    else// end Bug# 22486 DH
    {
      setTimeout(function () { get_top().main.enqueue_message(message); }, 10);
    }
  }
  
  try
  {
    setTimeout(function() {ScannerReadyAsync(DocType, message); }, 100);
  }
  catch (e) 
  {
    alert("Error calling ScannerReadyAsync: " + e.description);
  }
    
  // end bug 13720 DH
}

function Epson_CanceledCardID()
{
  // Bug# 22486 DH - Need to implement a custom skip. Otherwise next doc is called to fast before cancel has a chance to cancel at the service.
  get_top().main.dequeue_message();
  ScannerCancel();
}

function ScannerReadyAsyncCallback_DMBFType(result, param)
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  
  try
  {
    RemoveMessageAfterProcessing = param;
    
    if(result.Epson_PrinterProcessImageDocResult.iLastErrorCode == 0)
    {
      Device_Scanner_receive_callback(result.Epson_PrinterProcessImageDocResult.strImage_FRONT_DataString, result.Epson_PrinterProcessImageDocResult.strImageFormat);
      if(RemoveMessageAfterProcessing)
          get_top().main.dequeue_message();
      
      // Bug# 20455 & 22729 DH
      var el = get_top().main.document.getElementById("args.bank_routing_nbr");
      if (el == HTMLInputElement)
        el.focus();

      return;
    }
    else if(result.Epson_PrinterProcessImageDocResult.iLastErrorCode != 0)
    {
      if (result.Epson_PrinterProcessImageDocResult.iLastErrorCode != 1006)// TTS 18670 DH
        alert(result.Epson_PrinterProcessImageDocResult.strLastErrorMessage);
        
      if(RemoveMessageAfterProcessing)
          get_top().main.dequeue_message();

      // Bug# 20455 & 22729 DH
      var el = get_top().main.document.getElementById("args.bank_routing_nbr");
      if (el == HTMLInputElement)
        el.focus();

      return;
    }
  }
  catch (e) 
  {
    if(RemoveMessageAfterProcessing)
      get_top().main.dequeue_message();
    return;
  }
    
  if(RemoveMessageAfterProcessing)
    get_top().main.dequeue_message();
    
  return;
}

function ScannerReadyAsyncCallback_CType(result, param)
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  
  RemoveMessageAfterProcessing = param;
  
  try
  {
    if(result.Epson_PrinterProcessCardScannerDocResult.iLastErrorCode == 0)
    {
      Device_Scanner_receive_callback(result.Epson_PrinterProcessCardScannerDocResult.strImage_FRONT_DataString, result.Epson_PrinterProcessCardScannerDocResult.strImageFormat);
      
      if(RemoveMessageAfterProcessing)
        get_top().main.dequeue_message();
      var elm = get_top().main.form_inst["_method_name.continue"];
        elm.focus();
        
  return;
    }	
    else if(result.Epson_PrinterProcessCardScannerDocResult.iLastErrorCode != 0)
    {
      if(RemoveMessageAfterProcessing)
        get_top().main.dequeue_message();
      
      // Bug# 22486 DH
      if (result.Epson_PrinterProcessCardScannerDocResult.iLastErrorCode == -1 && result.Epson_PrinterProcessCardScannerDocResult.strLastErrorMessage == "User Canceled")
      {
        var app = "";
        if (g_card_app.substr(0, 3) == "GEO")
          app = g_card_app.substr(4);
        else
          app = g_card_app;
        
        post_remote(app, 'Create_core_item_app.skip_document', { 'already_canceled':true}, 'pos_frame', true);
      }
      // end Bug# 22486 DH
      return;
    }
  }
  catch (e) 
  {
    if(RemoveMessageAfterProcessing)
      get_top().main.dequeue_message();
    return
  }
  
  if(RemoveMessageAfterProcessing)
    get_top().main.dequeue_message();
}

function ScannerReadyAsyncCallback_OCRType(result, param)
{
  // Bug 13720 DH - iPayment Peripheral Overhaul

  RemoveMessageAfterProcessing = param;
  try
  {
    if(result.Epson_PrinterProcessOCRDocResult.iLastErrorCode == 0)
    {
       g_strTempImageFront = result.Epson_PrinterProcessOCRDocResult.strImage_FRONT_DataString;
        
      // Bug 21811 DH
       if (IsEpsonS9000Connected())
        g_strTempImageBack = result.Epson_PrinterProcessOCRDocResult.strImage_BACK_DataString;
      else
        g_strTempImageBack = "";
      // Bug 21811 DH

      g_content_type = result.Epson_PrinterProcessOCRDocResult.strImageFormat;
      g_bTemp_OCR_DATA = result.Epson_PrinterProcessOCRDocResult.strOCR;
      g_bTemp_OCR_DATA_ERROR = "";
    }	
    else if(result.Epson_PrinterProcessOCRDocResult.iLastErrorCode != 0)
    {
      g_bTemp_OCR_DATA_ERROR = result.Epson_PrinterProcessOCRDocResult.strLastErrorMessage;
      alert(result.Epson_PrinterProcessOCRDocResult.strLastErrorMessage);
      
      g_bWaiting_For_Epson_OCR_and_Image = false;

      if (RemoveMessageAfterProcessing)
        get_top().main.dequeue_message();

      return;
    }
    	    
    DeviceScannerReceive_Image_and_OCR();
    if(RemoveMessageAfterProcessing)
      get_top().main.dequeue_message();
  }
  catch (e) 
  {
    if(RemoveMessageAfterProcessing)
      get_top().main.dequeue_message();
      
    return;
  }
  
  if(RemoveMessageAfterProcessing)
    get_top().main.dequeue_message();
}

function ScannerReadyAsync(DocType/*Bug 12797 DH - OCR image disappears after number is displayed*/, message)
{
    // Function replaced for Bug 13720 DH - iPayment Peripheral Overhaul

    if (IsP80Available())// Bug# 24950 DH
      return 0;

    g_DocType = DocType;/*Bug# 24736 DH*/

    var RemoveMessageAfterProcessing = false;
    if(message != "opt" && message != "" && message != undefined)
    {
      RemoveMessageAfterProcessing = true;
    }
      
    if (!g_EpsonDocImaging) //BUG# 10554 DH - Limited printer support
    {
        var t = setTimeout("LateRemoval()", 600);
        if(RemoveMessageAfterProcessing)get_top().main.dequeue_message();
        return 0;
    }
            
    //-----------------------------------------------------------------------------------------
    // Bug 12797 DH - OCR image disappears after number is displayed
    // Need to capture the OCR and the image and send them to the screen together in one call.
    var OCRDoc = false;// Bug 13720 DH - iPayment Peripheral Overhaul
    if(DocType != "undefined" && DocType != null && DocType != undefined && DocType == "D")
    {
      ResetTempFields();
      g_bTemp_OCR_DATA = "";
      g_strTempImageFront = "";
      OCRDoc = true;// Bug 13720 DH - iPayment Peripheral Overhaul
    }
    //-----------------------------------------------------------------------------------------
    
    //-----------------------------------------------------------------------------------------
    // OCR functionality added for Bug 12797 DH
    if(OCRDoc == true && g_bWaiting_For_Epson_OCR_and_Image == true)
    {
      AjaxCallPeripherals("epson_printer_process_ocr_doc", ScannerReadyAsyncCallback_OCRType, "", RemoveMessageAfterProcessing, "");
      return;
    }
    //-----------------------------------------------------------------------------------------
    
    //-----------------------------------------------------------------------------------------
    // Handle Image without the OCR component
    if ((OCRDoc == true && g_bWaiting_For_Epson_OCR_and_Image == false) || DocType == "D,M,B" || DocType == "DMB" || DocType == "DMF" || DocType == "MB" || DocType == "D,M,F"/* D,M,B is part of check processing*/) 
    {
      if (DocType == "MB")// TTS# 18685 DH
      {
        // User chose not to do scanning for this doc. Remove the prompt is setup by CASL
        if (!RemoveMessageAfterProcessing) 
        {
          // Bug 22715 DH
          setTimeout(function () // Also add a delay to make sure the MICR finished before it's interrupted
          {
            get_top().main.dequeue_message();
            AjaxCallPeripherals("epson_printer_remove", "", "", "", "");
                       
            if (g_auto_continue)
            {
              // Bug 22729 DH
              try
              {
                var el = get_top().main.document.getElementById("args.bank_routing_nbr");
                if (el == HTMLInputElement)
                {
                  el.onblur();
                  el.onchange();
                }
              }
              catch (e)
              {
                //...
              }
              // end Bug 22729 DH

              get_top().main.form_inst['_method_name.continue'].value = 'continue';
              get_top().main.form_inst.submit();
            }
          }, 1000);
          // end Bug 22715 DH
          return;
        }
      }
      AjaxCallPeripherals("epson_printer_process_image_doc", ScannerReadyAsyncCallback_DMBFType, "", RemoveMessageAfterProcessing, "");
      return;
    }
    //-----------------------------------------------------------------------------------------
    
    //-----------------------------------------------------------------------------------------
    // Handle Image without the OCR component
    if(OCRDoc == false && g_bWaiting_For_Epson_OCR_and_Image == false && DocType == "C")
    {
      // Bug 21811 DH
      if (IsEpsonS9000Connected())
        AjaxCallPeripherals("epson_printer_process_card_scanner_doc", HandleEpsonDuealIDImages, "", RemoveMessageAfterProcessing, "");
      else
      {
        
        AjaxCallPeripherals("epson_printer_process_card_scanner_doc", ScannerReadyAsyncCallback_CType, "", RemoveMessageAfterProcessing, "");
      }
      // Bug 21811 DH
      
      return;
    }
    //-----------------------------------------------------------------------------------------
    
    //-----------------------------------------------------------------------------------------
    // Release Form
    // Bug 20145 MJO - Support DMBF
    if(DocType == "DBF" || DocType == "DB" || DocType == "DF" || DocType == "DMBF")
    {
      AjaxCallPeripherals("epson_printer_process_image_doc", ScannerReadyAsyncCallback_DMBFType, "", RemoveMessageAfterProcessing, "");
      return;
    }
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Bug 18728 - Check configured without printing on FRONT or BACK gets stuck in the printer
    if(DocType == "DM")
    {
      RemoveMessageAfterProcessing = true;
      AjaxCallPeripherals("epson_printer_process_image_doc", ScannerReadyAsyncCallback_DMBFType, "", RemoveMessageAfterProcessing, "");
      return;
    }
    //-----------------------------------------------------------------------------------------
    
    //-----------------------------------------------------------------------------------------
    // General handler
    if (DocType != "MB") // TTS# 18685 DH
      AjaxCallPeripherals("epson_printer_scanner_ready", "", "", "", "");
    //-----------------------------------------------------------------------------------------
    // end bug 13720 DH
    
 return true;
}

function HandleEpsonDuealIDImages(param1, param)
{
  // Bug 21811 DH

  RemoveMessageAfterProcessing = param;

  if(param1.Epson_PrinterProcessCardScannerDocResult.iLastErrorCode != 0)
  {
    alert(param1.Epson_PrinterProcessCardScannerDocResult.strLastErrorMessage);
    if(RemoveMessageAfterProcessing)
      get_top().main.dequeue_message();
    return;
  }

  args                        = new Array();
  args["app"]                 = GetCurrentObject();
  args["content_image_front"] = param1.Epson_PrinterProcessCardScannerDocResult.strImage_FRONT_DataString;
  args["content_image_back"]  = param1.Epson_PrinterProcessCardScannerDocResult.strImage_BACK_DataString;

  if(g_strEpsonImageSiteToCapture.toUpperCase() == "FRONT")
    args["content_image_back"] = "";

  if(g_strEpsonImageSiteToCapture.toUpperCase() == "BACK")
  {
    args["content_image_front"] = param1.Epson_PrinterProcessCardScannerDocResult.strImage_BACK_DataString;
    args["content_image_back"]  = "";
  }

  if(g_strEpsonImageSiteToCapture.toUpperCase() == "FRONT AND BACK")
  {
    args["content_image_front"] = param1.Epson_PrinterProcessCardScannerDocResult.strImage_FRONT_DataString;
    args["content_image_back"]  = param1.Epson_PrinterProcessCardScannerDocResult.strImage_BACK_DataString;
  }

  args["image_front_tag"] = "EPSON_S9000_FRONT_IMAGE";// Bug# 23863 DH
  args["image_back_tag"] = "EPSON_S9000_BACK_IMAGE";// Bug# 23863 DH
  args["content_type"]    = "image/tiff";

  Epson_Receive_front_and_Back_Images(args);// Bug# 22486 DH
  
  if(RemoveMessageAfterProcessing)
        get_top().main.dequeue_message();
 
  ScannerRemove();
  return;
}

function EpsonRemoveDocument(remove_prompt/*TTS 20641 DH*/)
{
    // TTS 20641 DH - Make sure the franking is the last step. This is why I added it to this function.
    if (g_bfranking == true)
    {
        var franking_success = function (result, param)
        {
            g_bfranking = false;
            EpsonRemoveDocument(remove_prompt);
            MicrRemove();
            return;
        }
        var franking_error = function (result, param)
        {
            alert("There was a franking error. Please do it manually.");
            g_bfranking = false;
            EpsonRemoveDocument(remove_prompt);
        }
        AjaxCallPeripherals("epson_direct_io/" + 1800 + "/" + 0 + "/" + "empty", franking_success, franking_error, "", "");
        return;
    }
    // end TTS 20641 DH
    
  // Bug# 18728 DH
  AjaxCallPeripherals("epson_printer_remove", "", "", "", "");

  if(remove_prompt != undefined && remove_prompt == true)
    get_top().main.dequeue_message();
}

function ScannerCancel() 
{
  // Bug 13720 DH - iPayment Peripheral Overhaul

  if (IsEpsonS9000Connected())// Bug# 23914 DH - It was already canceled.
    return;

  AjaxCallPeripherals("epson_printer_scanner_cancel", "", "", "", "");  
  return;
}

function ScannerAccepted() 
{
  // Bug 13720 DH - iPayment Peripheral Overhaul - No need for it here.
}

function ScannerDirectIO(Command, pData, pString, CallbackTarget) 
{ 
  // BUG# 10554 DH - Limited printer support.
  if (Command == 13 && pData == 1)
  {
    if(!g_EpsonPhotoIDScanner)// Bug 13720 DH - iPayment Peripheral Overhaul
      return false;
  }

  if(Command == 13 && pData == 0) 
  {    
    if(!g_EpsonDocImaging)// Bug 13720 DH - iPayment Peripheral Overhaul
      return false;
  }
  // end BUG# 10554 DH - Limited printer support
 
 
   // Bug 13720 DH - iPayment Peripheral Overhaul
   if(pString == "")// Mark it for the service, it makes the call simpler.
    pString = "empty";
    
   if(CallbackTarget != undefined && CallbackTarget != null)
    AjaxCallPeripherals("epson_printer_scanner_direct_io/" + Command + "/" + pData + "/" + pString, CallbackTarget, "", "", "");
   else
    AjaxCallPeripherals("epson_printer_scanner_direct_io/" + Command + "/" + pData + "/" + pString, "", "", "", "");
    // end bug 13720 DH
}
  
function ScannerChangeMode(mode, CallbackTarget) 
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  if(CallbackTarget != undefined)
    return ScannerDirectIO(13/*CHK_DI_CHANGE_MODE*/, mode, "", CallbackTarget);
  else
    return ScannerDirectIO(13/*CHK_DI_CHANGE_MODE*/, mode, "");
}

/* 
------------------------Cash Drawer------------------------
*/

function CashOpen(strCashDrawerID/*BUG# 6333 Added dual cash drawer support DH*/) 
{ 
  // Function replaced for Bug 13720 DH - iPayment Peripheral Overhaul
  // BUG# 6333 Added dual cash drawer support DH  

  if (g_bIsEpsonPrinterAvailable)// Bug 27536 DH - Error during receipt print when the OPOS is not installed
    AjaxCallPeripherals("epson_printer_open_cash_drawer/" + strCashDrawerID, "", "", "", "");	
}

function CashClose(strCashDrawerID/*BUG# 6333 Added dual cash drawer support DH*/) 
{
  // Function replaced for Bug 13720 DH - iPayment Peripheral Overhaul

  if (!g_bIsEpsonPrinterAvailable)// Bug 27536 DH - Error during receipt print when the OPOS is not installed
    return -1;

  if(strCashDrawerID != "A" && strCashDrawerID != "B")/*BUG# 6333 Added dual cash drawer support DH*/
  {
    CashClose_Dual("A");
    CashClose_Dual("B");
  }
  
  CashClose_Dual(strCashDrawerID);/*BUG# 6333 Added dual cash drawer support DH*/
    
  return return_val;
}

function CashClose_Dual(strCashDrawerID) 
{
  if (!g_bIsEpsonPrinterAvailable)// Bug 27536 DH - Error during receipt print when the OPOS is not installed
    return -1;

  // Bug 13720 DH - iPayment Peripheral Overhaul
  AjaxCallPeripherals("epson_printer_close_cash_drawer/" + strCashDrawerID, "", "", "", "");
}

function CashOpenDrawer(strCashDrawerID/*BUG# 6333 Added dual cash drawer support DH*/) 
{
  // Function replaced for Bug 13720 DH - iPayment Peripheral Overhaul
  if(strCashDrawerID == "None" || strCashDrawerID == "")/*BUG# 6333 & 10180 Added dual cash drawer support DH*/
    return;
    
 // BUG# 10816 DH - If not A or B then it is probably a USB cash drawer
 if(strCashDrawerID != "A" && strCashDrawerID != "B" && strCashDrawerID != "Single") // Bug# 11760 - DH Cash Drawer selection box should only appear for systems configured for dual cash drawers
 {
  AjaxCallPeripherals("usb_cash_drawer_open/" + strCashDrawerID, "", "", "", "");
  return;
 }
  
 return CashOpen(strCashDrawerID/*BUG# 6333 Added dual cash drawer support DH*/);
}

function CashWaitForDrawerClose(strCashDrawerID/*BUG# 6333 Added dual cash drawer support DH*/) 
{
  // Function replaced for Bug 13720 DH - iPayment Peripheral Overhaul
  var args             = {};
	args.beepTimeout     = "20000";
	args.beepFrequency   = "500";
	args.beepDuration    = "300";
	args.beepDelay       = "10000";
	args.strCashDrawerID = "A";
	AjaxCallPeripherals("epson_printer_close_cash_drawer", "", "", "", args);// Use POST
	return;
}

//Bug#6680 ANU
function Device_cash_test_health (args) 
{ 
  post_remote(GetCurrentObject(),'Device.cash_test_health', args, 'pos_frame'); 
}

function DeviceCashTest() 
{
  /*BUG# 6333 Added dual cash drawer support DH*/
  
  var health1=g_CashDrawerAAvailability;
  var health2=g_CashDrawerBAvailability;
  var health = 0;
  
  if(health1==0 && health2==0)
    health = 1;
  
  Device_cash_test_health( {health:health} );
}
//end BUG 5793

var DEVICE_PRINTER = 0,
    DEVICE_MICR    = 1,
    DEVICE_SCANNER = 2,
    DEVICE_CASH    = 3; //BUG 5793 BLL
     
// Bug#9100 Mike O   
function RaiseErrorEvent(device, code, message) 
{
  if ( device == DEVICE_MICR ) {
    if ( code == "101" || code == "114" /*BUG# 10121 DH*/) {// Data Error: No Data Found
      MicrCancel();
      MicrRemove();
      get_top().main.dequeue_message();
      get_top().main.enqueue_message("micr_fail");
    }
  } else { alert("Unknown error condition: " + device + ", " + code + ", " + message); }
}

/*
Javascript proxy methods made in CASL
  Device.micr.receive
    routing_number = req = String
    bank_account_number = req = String
    check_number = req = String
  Device.scanner.receive
    content = req = String
    content_type = req = String
  Device.swipe.receive
    data = req = String
    
Client Call methods made in CASL

*/
function log ( args ) { if(diagLog) alert("log:" + args); /*post_remote("log", args, "pos_frame");*/ }
  
// Bug#9100 Mike O
function Device_Printer_accept_callback() 
{ 
  Device_Printer_accept( {app:get_top().main.get_current_obj()} );
}

//Bug#6680 ANU
function Device_Printer_accept(args)
{
  post_remote(GetCurrentObject(), 'Device.Printer.accept', args, 'pos_frame'); 
}

function Device_Micr_receive_callback(routing_number, bank_account_number, check_number, raw_micr, country_code, check_type, check_epc, check_amount, bank_number, on_us, aux_on_us/*Bug# 25093 DH*/) 
{
  if (g_isValidateACH && check_type == "2")//14687 TTS17888 & Bug 21791 DH
  {
    MicrCancel();
    MicrRemove();
    get_top().main.dequeue_message("document_prompt_0");
    get_top().main.enqueue_message("commercial_check_invalid_tender");
    g_bfranking = false;// Bug 23429 DH
  }
  else
    Device_Micr_receive(
      {
        app: GetCurrentObject(),
        routing_number: routing_number,
        bank_account_number: bank_account_number,
        check_number: check_number,
        raw_micr: raw_micr,/*BUG# 9831 DH*/
        country_code: country_code,/*BUG# 9831 DH*/
        check_type: check_type,/*BUG# 9831 DH*/
        check_epc: check_epc,/*BUG# 9831 DH*/
        check_amount: check_amount,/*BUG# 9831 DH*/
        bank_number: bank_number,/*BUG# 9831 DH*/
        on_us: on_us == undefined ? "" : on_us/*Bug# 25656 DH Bug# 26183 DH*/,
        aux_on_us: aux_on_us == undefined ? "" : aux_on_us/*Bug# 25656 DH*/ /*Bug# 25093 DH, Bug# 26183 DH*/
      }
    );
}
//Bug#6680 ANU
// Bug#9100 Mike O
function Device_Micr_receive (args) 
{ 
  // Bug 22729 DH - We need to trigger this manually
  if (g_aba_validation == true)
  {
    args["aba_validation"]=true;
  }

 // Bug 22729 DH
  if (g_EpsonS9000Connected && !g_bPertechProcessing)// Bug# 25201 DH
  {
    // Bug# 23002 DH
    setTimeout(function ()
    {
      // Need to give iPayment some time to react. S9000 returns all data at the same time.
      post_remote(GetCurrentObject(), 'Device.Micr.receive', args, 'pos_frame', true);
    }, 500);
   // end Bug# 23002 DH
  }
  else
    post_remote(GetCurrentObject(), 'Device.Micr.receive', args, 'pos_frame', true);

  g_bPertechProcessing = false;// Bug# 25201 DH
}

function CancelDepositPrint()
{
  // Bug# 25522 DH
  ClearAJAXQueue();// Remove all print instructions
  AjaxCallPeripherals("epson_printer_cancel_deposit", "", "", "", "");
  return;
}

function ABAValidationCompleted(passed, doc_type)// Bug# 24109 DH - Do not frank documents if ABA Validation was required and it failed.
{
  if (passed === true)
  {
    if (doc_type === "H")// Pertech
    {
      AjaxCallPeripherals("pertech_frank_check/" + passed, "", "", "", "");
    }
  }
  else
  {
    if (doc_type === "H")// Pertech
    {
      if (g_PertechEnableFranking && !g_aba_validation)// Force franking without the ABA validation
        AjaxCallPeripherals("pertech_frank_check/" + "true", "", "", "", "");
      else if (!g_PertechEnableFranking)
      {
        // Already removed.
      }
      else
        AjaxCallPeripherals("pertech_frank_check/" + passed, "", "", "", "");
    }
  }
}

function ClearCheckFields()
{  
  // Bug 27129 DH - Epson 6000-III: previous check image appears when ABA validation fails

  var el1 = get_top().main.document.getElementById("args.bank_routing_nbr");
  if (el1 != undefined)
    el1.value = "";

  var el2 = get_top().main.document.getElementById("args.bank_account_nbr");
  if (el2 != undefined)
    el2.value = "";

  var el3 = get_top().main.document.getElementById("args.bank_name");
  if (el3 != undefined)
    el3.value = "";

  var el4 = get_top().main.document.getElementById("args.check_nbr");
  if (el4 != undefined)
    el4.value = "";
}

function DisplayABABankName(bank_name)
{
  // Bug 25545 - ABA Validation after check scan validates routing number but does not display bank name

  if (bank_name != undefined && bank_name != null)
  {
    var el = get_top().main.document.getElementById("args.bank_name");
    if (el != undefined)
      el.value = bank_name;
  }
}

function DisplayRoutingNumberError(error /*Bug 25545 DH*/)// Bug 22729 DH
{
  // Bug 25545 - ABA Validation after check scan validates routing number but does not display bank name

  g_aba_validation_error = true;

  if (g_EpsonS9000Connected)
  {
    if (get_top().main.dequeue_message != undefined && get_top().main.dequeue_message != null)
      get_top().main.dequeue_message();

    ClearCheckFields();// Bug 26691 & 27129 DH - ABA Validation: unrecognized routing number can appear as if it's recognized
    alert("Error reading check MICR.\n\nRouting " + error/*Bug 25545 DH*/);
    return;
  }

  // Show the error, cancel all printer operations and remove the check
  if (get_top().main.dequeue_message != undefined && get_top().main.dequeue_message != null)
    get_top().main.dequeue_message();

  MicrCancel();
  MicrRemove();
  EpsonRemoveDocument("");
    
  ClearCheckFields();// Bug 26691 & 27129 DH - ABA Validation: unrecognized routing number can appear as if it's recognized
  alert("Error reading check MICR.\n\nRouting " + error /*Bug 25545 DH*/); //Bug 23366/23367 NK
}

function Device_Scanner_receive_callback(content, content_type) 
{
  get_top().main.dequeue_message();
  Device_Scanner_receive_image(
    { 
      app:GetCurrentObject(),
      content:content,
      content_type: content_type,
      image_tag: "EPSON_FRONT_IMAGE",/*Bug# 24736 DH*/
      doc_type: g_DocType/*Bug# 24736 DH*/
    }
  );
}

//Bug#6681 ANU
function Device_Scanner_receive_image (args) 
{
  //-----------------------------------------------------------------------------------------
  // Bug 12797 DH - OCR image disappears after number is displayed
  // Need to capture the OCR and the image and send them to the screen together in one call.    
  if(g_bWaiting_For_Epson_OCR_and_Image)
  {
    g_bTemp_Image_Ready = true;
    g_strTempImageFront = args["content"];
    g_content_type = args["content_type"];
  
    if(g_bTemp_OCR_DATA != "" || g_bTemp_OCR_DATA_ERROR != "")
    {
      DeviceScannerReceive_Image_and_OCR();
      return;
    }
    return;// Keep waiting.
  }
  //------------------------------------------------------------------------------------------
  
  // Bug 13720 DH - iPayment Peripheral Overhaul
  var nAgt = navigator.userAgent;
  if (nAgt.indexOf("Chrome") !=-1)
    args["skip_loader"]=true;

  // Bug# 22486 DH
  if (args["content_type"] == "image/jpeg")// Photo IDs
  {    
    if (g_confirming_documents == true)
    {
      if (g_iCurrent_document_index < g_iNbr_of_documents_to_process)
        post_remote(GetCurrentObject(), 'Device.Scanner.receive', args, 'pos_frame', true);
      else if (g_iCurrent_document_index >= g_iNbr_of_documents_to_process)
        post_remote(GetCurrentObject(), 'Device.Scanner.receive', args, 'main', true);
    }
    else if (g_confirming_documents == false)
    {
      post_remote(GetCurrentObject(), 'Device.Scanner.receive', args, 'pos_frame', true);
    }
    return;
  }
  // end Bug# 22486 DH

  // Bug# 22486 & 23520 DH
  if (g_confirming_documents == true)
  {
    if (g_doc_count == undefined || g_doc_count == "1")
      post_remote(GetCurrentObject(), 'Device.Scanner.receive', args, 'main', true);
    else
    {
      if (g_iCurrent_document_index == g_iNbr_of_documents_to_process)
        post_remote(GetCurrentObject(), 'Device.Scanner.receive', args, 'main', true); // Bug# 5940 DH
      else
        post_remote(GetCurrentObject(), 'Device.Scanner.receive', args, 'pos_frame', true);
    }
  }
    else 
      post_remote(GetCurrentObject(), 'Device.Scanner.receive', args, 'pos_frame', true);
  // end Bug# 22486 & 23520 DH
}

// Bug 12797 DH
// Need to capture the OCR and the image and send them to the screen together in one call.
function DeviceScannerReceive_Image_and_OCR()
{
   new_args                =new Array();
   new_args["app"]         =GetCurrentObject();
   new_args["content"]     =g_strTempImageFront;
   new_args["content_type"]=g_content_type;
   new_args["OCR_DATA"]    =g_bTemp_OCR_DATA;
   new_args["OCR_ERROR"]   =g_bTemp_OCR_DATA_ERROR;
    
   // Bug 21811 DH
   if (IsEpsonS9000Connected())
   {
     args = new Array();
     args["app"]=GetCurrentObject();

     if(g_strEpsonImageSiteToCapture.toUpperCase() == "FRONT")
          args["content_image_front"] = g_strTempImageFront;

     if(g_strEpsonImageSiteToCapture.toUpperCase() == "BACK")
       args["content_image_front"] = g_strTempImageBack;// Put back in the front position

     if(g_strEpsonImageSiteToCapture.toUpperCase() == "FRONT AND BACK")
     {
       args["content_image_front"] = g_strTempImageFront;
       args["content_image_back"]  = g_strTempImageBack;
     }

     args["image_front_tag"] = "EPSON_S9000_FRONT_IMAGE";// Bug# 23863 DH
     args["image_back_tag"] = "EPSON_S9000_BACK_IMAGE";// Bug# 23863 DH
     args["content_type"]    = "image/tiff";
     args["OCR_DATA"]        = g_bTemp_OCR_DATA;
     //g_bTemp_OCR_DATA_ERROR;

     // Bug# 22486 DH
     Epson_Submit_OCR_AND_Image('Business_app.Create_core_item_app.epson_receive_ocr_and_images', args);

     ResetTempFields();
     g_bWaiting_For_Epson_OCR_and_Image = false;
     delete args;
     return;
   }
   // end Bug 21811 DH
   
   // Bug# 22486 DH
   Epson_Submit_OCR_AND_Image('Business_app.Create_core_item_app.epson_receive_ocr_and_image', new_args);

   ResetTempFields();
   g_bWaiting_For_Epson_OCR_and_Image = false;// Bug 12797 DH

   delete new_args;// Bug 21811 DH
}
// end Bug 12797 DH

function Epson_Submit_OCR_AND_Image(path, args)// Bug# 22486 DH
{
  if (g_doc_count == undefined || g_doc_count == "1")
    get_top().main.post_remote('Business_app', path, args, 'main', true);
  else
  {
    if (g_iCurrent_document_index == g_iNbr_of_documents_to_process)
      get_top().main.post_remote('Business_app', path, args, 'main', true);
    else
      get_top().main.post_remote('Business_app', path, args, 'pos_frame', true);
  }
}


//function Device_Swipe_receive_callback(data) { Device_Swipe_receive( {data:data} ); }
//Bug#6680 ANU
// Bug#9100 Mike O
  // Bug #11783 Mike O - No longer calls from main frame, as it might not be fully loaded yet
function Device_Swipe_receive (args) {post_remote('Device.Swipe','Device.Swipe.receive', args, 'main' ); }

function CheckHealth() 
{
  // Bug 13720 DH - iPayment Peripheral Overhaul - Not used, still here for backward compatibility.
  return 0;
}

var printer_error=false;
function CheckHealthEx() 
{
  var health = CheckHealth();
  if (health == 0) 
  {
     return true; 
  }
  else 
  {
    Device_test_health({ health: health, bInitializing: false/*BUG# 10554 DH - Limited printer support*/});
    // Bug#9100 Mike O
    if (!get_top().main.printer_error) 
    {
      // Bug# 10113 - Only show peripherals failed to connect messages when "Reset Peripherals" button is clicked.
      if(!g_IsIngenicoAvailable) // Bug 13720 DH - iPayment Peripheral Overhaul 
      {
        get_top().main.printer_error=true;
        get_top().main.enqueue_alert_message("<CENTER>Trouble communicating with printer.<BR/>Please check printer status and try again.<BR/>You may need to reset printer communication.</CENTER>");
      }
    }
    return false;
  }
}

// Bug 19153 MJO - Removed point_ip (it's passed in later if needed)
function UpdatePeripheralsStatus(ShowPeripheralsStatusUI)// Bug# 17902 Comment #16 - DH
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  
  // Bug# 17902 Comment #16 - DH
  args = new Array(); 
  args[0]=ShowPeripheralsStatusUI;
  
  // Added the ability to configure the peripherals station IP address.
  // The correct address is in this format: http://localhost/COREService/ and is located in this file in the service installation directory ip.config.
  var error_handler = function(error, param)
	{	
	  // Bug# 22129 DH - Panini: resetting peripherals does not detect the device in iPayment on certain computers; logout is required to detect it
    if (!g_ServiceInitStatus)
    {
      if ((result.indexOf("Service Unavailable") != -1) || (result.indexOf("Error calling peripherals") != -1))
        InformUser_ServiceDown();
    }

     UpdatePeripheralsStatusCallback(-1/*set the devices in my properly*/, ShowPeripheralsStatusUI);// We still need to set the devices and show error messages.
	  return;
	}
	
	var UpdateServiceAddress = function(result, args)
	{
	  // Setup the globals first
    if(ShowPeripheralsStatusUI == false)
      Init();
       
    // Bug 17505 MJO - Set HTTP/HTTPS properly
    g_ServiceAddress = "http" + (get_top().main.location.href.indexOf("https") == 0 ? "s" : "") + result.substr(result.indexOf("://"));

    // Bug# 20825 DH - Configurable Peripherals Service ports
    g_ServiceAddress = UpdatePeripheralsServiceAddress();

    // Continue on to initialization of devices, but this time hitting the target service that was configured.
    UpdatePeripheralsStatusEx(args[0]);
	}
	
  if(!g_resettingPeripherals)// Bug# 20825 DH - Configurable Peripherals Service ports
  {
	  // Bug 18055 MJO - Set HTTP/HTTPS properly here too
    g_ServiceAddress = "http" + (get_top().main.location.href.indexOf("https") == 0 ? "s" : "") + g_ServiceAddress.substr(g_ServiceAddress.indexOf("://"));
  
    // Bug# 20825 DH - Configurable Peripherals Service ports
    g_ServiceAddress = UpdatePeripheralsServiceAddress();
  }

  /*Bug# 22129 DH -
  "get_pos().g_pos_start = true;" in pos.start sets this to TRUE and causes pos.start_aux to be called which in turn stops iPayment from transferring in.
  It only gets set if we transfer in. So this is a hacky fix since I don't know the transfer code, but it looks to me that it could be because the department is not yet set when transferring in.
  */
  g_pos_start = false;
  g_ServiceInitStatus = true;
  AjaxCallPeripherals("get_service_ip_address", UpdateServiceAddress, error_handler, args, "");
  // end Bug# 17902 Comment #16 - DH
}

function UpdatePeripheralsServiceAddress()
{
  // Bug# 20825 DH - Configurable Peripherals Service ports
  // LOCAL AND REMOTE SERVICE WILL USE THE SAME PORT NUMBER. QA NEEDS TO SET THIS UP PROPERLY.
  // Startup.ini file will set it for the service and iPayment Config must set the same ports.
  // Make sure to preserve the computer name in case of a remote connection.
  var pos = g_ServiceAddress.indexOf("//COREService");
  if(pos == -1)
    pos = g_ServiceAddress.indexOf("/COREService");// User could enter single or double slash

  // Bug 21494 MJO - Skip if port is already included
  if (g_ServiceAddress.slice(g_ServiceAddress.indexOf("//"), pos).indexOf(":") == -1) {
    if (g_ServiceAddress.indexOf("https") == 0) {
    var strTemp = [g_ServiceAddress.slice(0, pos), ":" + g_peripherals_https_port, g_ServiceAddress.slice(pos)].join('');
    g_ServiceAddress = strTemp;
  }
    else {
    var strTemp = [g_ServiceAddress.slice(0, pos), ":" + g_peripherals_http_port, g_ServiceAddress.slice(pos)].join('');
    g_ServiceAddress = strTemp;
  }
  }

  return g_ServiceAddress;
}

// Bug 19153 MJO - Removed point_ip (it's passed in later if needed)
function UpdatePeripheralsStatusEx(ShowPeripheralsStatusUI)// Bug# 17902 Comment #16 - DH Renamed
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  var error_handler = function(error, param)
	{
	  // Does not matter here but nutralizes the error alert from the service.
	  //alert("Unexpected error occurred. Unable to connect to the peripheral devices.\n\n" + error + "\n\nPeripheral service: " + g_ServiceAddress);
	  UpdatePeripheralsStatusCallback(-1/*set the devices in my properly*/, ShowPeripheralsStatusUI);
	  return;
	}
  
  AjaxCallPeripherals("check_health_all" , UpdatePeripheralsStatusCallback, error_handler, ShowPeripheralsStatusUI, "");
}

function UpdatePeripheralsStatusCallbackError(xhrMessageOrExceptionMessage, ShowPeripheralsStatusUI) 
{
  UpdatePeripheralsStatusCallback(-1/*set the devices in my properly*/, ShowPeripheralsStatusUI);
}

function PeripheralsSetPorts(peripherals_http_port, peripherals_https_port)
{
  // Bug# 20825 DH - Configurable Peripherals Service ports
  g_peripherals_http_port = peripherals_http_port;// Bug# 20825 DH - Configurable Peripherals Service ports
  g_peripherals_https_port = peripherals_https_port;
}

function IsP80Available()
{
  // Bug# 24950 DH
  if (g_EpsonP80Connected)
    return true;

  return false;
}

function UpdatePeripheralsStatusCallback(DeviceStatus, ShowPeripheralsStatusUI) 
{
  // Bug 13720 DH - iPayment Peripheral Overhaul - Function added for the bug.
  if (DeviceStatus == null || DeviceStatus == undefined)
    DeviceStatus = -1;// Set it properly.
  
  // BUG# 10554 DH - Limited printer support
  // I am using the exact words and spelling of error messages to make things clearer. Please do not change it or it may fail.
  new_args = new Array();

  g_PeripheralsConnected              = true;// Bug# 26230 DH
  var EpsonPrinter                    = 0; // true in CASL
  g_EpsonDocImaging                   = true;
  g_bIsRDMAvailable                   = true;
  g_IsIngenicoAvailable               = true;
  g_EpsonPhotoIDScanner               = true;
  g_VeriFoneIsAvailable               = true; // Bug #14288 Mike O - Added VeriFone device
  var TwainDeviceAvailable            = true;
  g_CashDrawerAAvailability           = true;
  g_CashDrawerBAvailability           = true;
  var DigitalCheckDeviceAvailable     = true;// Bug# 16027 DH - DigitalCheck device support
  var PertechScannerDeviceAvailable   = true;// DH - Pertech scanner device support
  var PaniniScannerInstalled          = true; // Bug# 19005 DH - Panini Device
  g_IsDigitalCheckInstalled           = true;// Bug# 16027 DH - DigitalCheck device support
  g_IsTwainInstalled                  = true;
  g_ServiceInitStatus                 = true;// Bug# 17902 Comment#45 DH
  g_EpsonAvailable                    = true;// BUG 20854 DH
  g_EpsonOCRSupported                 = true;// Bug# 20806 DH
  // Bug# 17902 Comment#45 DH - I renamed g_PointMACAddress to g_PointIPAddress.
  g_PointIPAddress                    = null; // Bug 17505 MJO - -Point device
  g_PointMACAddresses                 = null; // Bug 19153 MJO - Added MAC addresses
  g_EpsonS9000Connected               = false;// Bug 21811 DH
  g_EpsonValidationPrinterConnected   = true;// Bug# 24256 DH - TM-U295.
  g_EpsonP80Connected                 = false;// Bug# 24950 DH

  // Bug 13720 DH - iPayment Peripheral Overhaul
  if(DeviceStatus == -1)// Service is unreachable
  {
    g_PeripheralsConnected            = false;// Bug# 26230 DH
    EpsonPrinter                      = -1;
    g_EpsonDocImaging                 = false;
    g_bIsRDMAvailable                 = false;
    g_IsIngenicoAvailable             = false;
    g_EpsonPhotoIDScanner             = false;
    g_VeriFoneIsAvailable             = false;
    TwainDeviceAvailable              = false;
    g_CashDrawerAAvailability         = false;
    g_CashDrawerBAvailability         = false;
    DigitalCheckDeviceAvailable       = false;// Bug# 16027 DH - DigitalCheck device support
    PertechScannerDeviceAvailable     = false;
    PaniniScannerInstalled            = false; // Bug# 19005 DH - Panini Device
    g_IsDigitalCheckInstalled         = false;// Bug# 16027 DH - DigitalCheck device support
    g_IsTwainInstalled                = false;
    // Bug 18078 MJO - Default is now false
    g_PointIPAddress                  = false;// Bug# 17902 Comment#45 DH - Added this setting.
    g_PointMACAddresses               = false; // Bug 19153 MJO - Added MAC addresses
    g_ServiceInitStatus               = false;// Bug# 17902 Comment#45 DH
    g_EpsonAvailable                  = false;// BUG 20854 DH
    g_EpsonOCRSupported               = false;// Bug# 20806 DH
    g_EpsonS9000Connected             = false;// Bug 21811 DH
    g_EpsonValidationPrinterConnected = false;// Bug# 24256 DH - TM-U295.
    g_EpsonP80Connected               = false;// Bug# 24950 DH

    CallPOSStart();
  }
  else
  {
    if (DeviceStatus.EpsonPrinterHealth != "INSTALLED, DEVICE IS ON")// Exact spelling, don't change!
    {
        EpsonPrinter = -1;
        g_EpsonAvailable = false;// BUG 20854 DH
    }

    // Bug# 24256 DH - TM-U295.
    if (DeviceStatus.EpsonValidationPrinterHealth != "INSTALLED, DEVICE IS ON")// Exact spelling, don't change!
    {
      g_EpsonValidationPrinterConnected = false;
    }
    // end Bug# 24256 DH - TM-U295.
      
    // Bug# 20806 DH
    if(DeviceStatus.EpsonPrinterOCRSupported != undefined && DeviceStatus.EpsonPrinterOCRSupported.toUpperCase() == "TRUE")
      g_EpsonOCRSupported = true;
    else
      g_EpsonOCRSupported = false;
    // end Bug# 20806 DH

    if(DeviceStatus.EpsonScannerHealth != "INSTALLED, DEVICE IS ON")// Exact spelling, don't change!
      g_EpsonDocImaging = false;
      
    if(DeviceStatus.RDMHealth != "INSTALLED, DEVICE IS ON") // Exact spelling, don't change!
      g_bIsRDMAvailable = false;
    else if(EpsonPrinter == 0)// turn OFF the RDM if Epson is being used. We can only have one printer connected at a time/
      g_bIsRDMAvailable = false
    
    if(DeviceStatus.IngenicoHealth != "INSTALLED, DEVICE IS ON")// Exact spelling, don't change!)
      g_IsIngenicoAvailable = false;
      
    if(DeviceStatus.EpsonPhotoIDScanner != "INSTALLED, DEVICE IS ON")// Exact spelling, don't change!)
      g_EpsonPhotoIDScanner = false;
            
    if(DeviceStatus.VeriFoneHealth != "INSTALLED")
      g_VeriFoneIsAvailable = false;
      
     if(DeviceStatus.TwainDeviceHealth != "INSTALLED")
       TwainDeviceAvailable = false;// just for not, there is a problem with TWAIN' control reporting offline incorectly.
     else
       TwainDeviceAvailable = true;
        
     if(DeviceStatus.EpsonCashDrawerAHealth != "INSTALLED, DEVICE IS ON")
       g_CashDrawerAAvailability = false;
       
     if(DeviceStatus.EpsonCashDrawerBHealth != "INSTALLED, DEVICE IS ON")
       g_CashDrawerBAvailability = false;
       
      // Bug# 16027 DH - DigitalCheck device support
      if(DeviceStatus.DigitalCheckDeviceHealth != "INSTALLED")// Some devices cannot be detected past installed at startup.
       DigitalCheckDeviceAvailable = false; 
      else
       DigitalCheckDeviceAvailable = true;
       
      // Bug# 17792 DH - Pertech device support
      if(DeviceStatus.PertechScannerDeviceHealth != "INSTALLED")// Some devices cannot be detected past installed at startup.
       PertechScannerDeviceAvailable = false;
      else
       PertechScannerDeviceAvailable = true;
      
      // Bug# 19005 DH - Panini Device
      if (DeviceStatus.PaniniScannerHealth != "INSTALLED") {
          PaniniScannerInstalled = false;
      }
      else {
          PaniniScannerInstalled = true;
      }

      // Bug 17505 MJO - -Point device
        g_PointIPAddress = DeviceStatus.PointIPAddress;
    g_PointMACAddresses = DeviceStatus.PointMACAddresses; // Bug 19153 MJO - Added MAC addresses
  } 
  
  // Bug 21494 MJO - If XPI drivers are installed but a device isn't connected, retry until found
  if (DeviceStatus.VeriFoneDriversInstalled == "true") 
  {
    if (!g_VeriFoneIsAvailable) {
      setTimeout(function () {
        // Bug 21708 MJO - Only recheck VeriFone
        VeriFone_RecheckHealth();
      }, 5000);
    }
  }
  
  // Keep it global for performance
  g_IsDigitalCheckInstalled   = DigitalCheckDeviceAvailable;// Bug# 16027 DH - DigitalCheck device support
  g_IsTwainInstalled          = TwainDeviceAvailable;
  g_IsPertechScannerInstalled = PertechScannerDeviceAvailable;// Bug# 17792 DH
  g_IsPaniniScannerInstalled = PaniniScannerInstalled;// Bug# 19005 DH - Panini Device

  new_args["health"] = EpsonPrinter;
  new_args["doc_imaging"]               = g_EpsonDocImaging;
  new_args["card_imaging"]              = g_EpsonPhotoIDScanner;
  new_args["rdm_scanner"]               = g_bIsRDMAvailable == true ? 0 : -1;
  new_args["verifone"]                  = g_VeriFoneIsAvailable;
  new_args["twain_scanner"]             = TwainDeviceAvailable; // BUG# 12778 TWAIN Device DH.
  new_args["ingenico"]                  = g_IsIngenicoAvailable; // Bug 18208 MJO - Changed back to boolean
  new_args["digital_check_scanner"]     = DigitalCheckDeviceAvailable;// Bug# 16027 DH - DigitalCheck device support
  new_args["pertech_scanner"]           = PertechScannerDeviceAvailable;// Bug# 17792 DH
  new_args["point_ip_address"]          = g_PointIPAddress; // Bug 17505 MJO - -Point device
  new_args["point_mac_addresses"]       = g_PointMACAddresses; // Bug 19153 MJO - Added MAC addresses
  new_args["panini_scanner"]            = g_IsPaniniScannerInstalled;// Bug# 19005 DH - Panini Device
  new_args["epson_s9000_connected"]     = false;// Bug 22496 DH
  new_args["epson_p80_connected"]       = g_EpsonP80Connected;// Bug 24950 DH
  new_args["epson_validation_printer"]  = g_EpsonValidationPrinterConnected;// Bug# 24256 DH - TM-U295.

  g_bIsEpsonPrinterAvailable = EpsonPrinter == 0 ? true : false;

  if(DeviceStatus == -1)// Service is unreachable
  {
    // Bug# 17902 Comment#45 DH
    //get_top().main.enqueue_alert_message("<CENTER>Unable to connect to the peripherals service. Your peripherals will not be available. <BR/><BR/>Please contact the system administrator.</CENTER>");
  }
  else
  {
    g_strPrinterModel = DeviceStatus.EpsonPrinterModel;//  Bug# 25291 DH - This comes up all the time. Save it and have it for later as well.

    // Bug# 23243 DH
    if (DeviceStatus.ServiceVersionNumber != undefined && DeviceStatus.ServiceVersionNumber != null)
    {
      new_args["peripherals_service_version"] = DeviceStatus.ServiceVersionNumber;
    }
    // end Bug# 23243 DH

    // Bug# 21811 DH
    if (DeviceStatus.EpsonPrinterModel.lastIndexOf("S9000") != -1)
    {
        g_EpsonS9000Connected = true;
        new_args["epson_s9000_connected"] = g_EpsonS9000Connected;// Bug# 22496
    }
    // end Bug# 21811 DH

    // Bug# 24950 DH
    if (DeviceStatus.EpsonPrinterModel.lastIndexOf("P80") != -1)
    {
      g_EpsonP80Connected = true;
      new_args["health"] = false;// Disable everything except for printing the receipt.
      new_args["epson_p80_connected"] = g_EpsonP80Connected;// Bug# 24950
    }
    // end Bug# 24950 DH

    // Bug# 24486 DH
    if (DeviceStatus.EpsonPrinterModel.lastIndexOf("H2000") != -1)
    {
      g_EpsonH2000Connected = true;
      new_args["epson_H2000_connected"] = g_EpsonH2000Connected;
    }
    // end Bug# 24486 DH

    // A new way to handle "unable to connect" would be to show a message if A PRINTER(Epson or RDM) is not available but installed.
    // Bug# 19630 DH
    if (DeviceStatus.EpsonPrinterHealth == "NOT INSTALLED")
    {
        // Just ignore
    }
    else if(DeviceStatus.EpsonPrinterHealth == "INSTALLED, NOT RESPONDING")
    {
      get_top().main.enqueue_alert_message("<CENTER>Unable to connect to the Epson device. Please check the cables.</CENTER>");
    }
    
    // Epson takes presedence
    if(DeviceStatus.RDMHealth == "INSTALLED, NOT RESPONDING" && g_bIsEpsonPrinterAvailable == false)
    {
      get_top().main.enqueue_alert_message("<CENTER>Unable to connect to the RDM device. Please check the cables.</CENTER>");
    }
    
    if(g_bIsRDMAvailable == true && g_bIsEpsonPrinterAvailable == true)
    {
      get_top().main.enqueue_alert_message("<CENTER>You installed multiple receipt printers. This can cause unpredictable results.</CENTER>");
    } 
    
    if(g_bIsRDMAvailable == false && g_bIsEpsonPrinterAvailable == false)
    {
        // Bug# 19630 DH
        if (DeviceStatus.EpsonPrinterHealth == "NOT INSTALLED" && DeviceStatus.RDMHealth == "NOT INSTALLED") {
            // Ignore any printer messages.
        }
        else {
            get_top().main.enqueue_alert_message("<CENTER>No printer detected.</CENTER>");
        }
    }
  }
 
  // Show the connection success message after reset call
  if(ShowPeripheralsStatusUI)
  {
    if(g_bIsRDMAvailable == true || g_bIsEpsonPrinterAvailable == true)
    {
      alert("Printer available.");
    }
  }
  
  //---------------------------------------------------------
  if(g_VeriFoneIsAvailable)// Bug 13720 DH - iPayment Peripheral Overhaul
  {
    VeriFone_SetInfo(DeviceStatus.VeriFoneDeviceDeviceName, DeviceStatus.VeriFoneDeviceSerialNbr, DeviceStatus.VeriFoneDeviceDerivedKey); 
  }
  //---------------------------------------------------------

  // Bug# 25117 DH - Panini: ABA validation failure, log error for first tender attempt after peripheral service starts
  g_aba_loaded = false;
  aba_data_uploaded = false;
  // end Bug# 25117 DH

  post_remote(GetCurrentObject(), 'Device.test_health', new_args, 'pos_frame', true); // BUG# 9386 DH

  Ingenico_DisplayAds();
}

function SetABAValidationData(ABAData)
{
  // Bug# 24172 DH - ABA validation.
  // Some devices need to validate checks in the middle of paper document processing on the device. 
  //  - ABA validation data must be available beforehand.

  // Bug 25826 - Panini: log error for first ABA tender attempt after peripheral service starts or site loads 
  // Notes: We do not need the first time reload upload. This will happen only for a Panini at the time the Panini check is used with the ABAValidation system interface available. It's only for the first time since login or peripherals reset.
  // This way the login will be lighter without the need for the ABA file upload.

  var base16String = {};
  base16String.strABAData = ABAData;// No need to convert this here.
  var args = new Array;

  var sucess_handler = function (param1, param2)
  {
    if (param1.Epson_ReceiveABADataResult == true)
      aba_data_uploaded = true;// Bug# 25117 DH - Panini: ABA validation failure, log error for first tender attempt after peripheral service starts
    else
      aba_data_uploaded = false;// Bug# 25117 DH - Panini: ABA validation failure, log error for first tender attempt after peripheral service starts
    
    delete args;
    return;
  }
  var error_handler = function (param1, param2)
  {    
    delete args;
    return;
  }

  AjaxCallPeripherals("receive_aba_data", sucess_handler, error_handler, "", base16String);
}

function ForceABADownload()
{
  // Bug# 24172 DH - ABA validation.
  
  setTimeout(function () 
  {
    var args = new Array();
    
    var frame = "main";

    if (g_confirming_documents == true)
    {
      if (g_doc_count == undefined || g_doc_count == "1")
        frame = "main";
      else
      {
        if (g_iCurrent_document_index == g_iNbr_of_documents_to_process)
          frame = "main";
        else
          frame = "pos_frame";
      }
    }
    else
      frame = "pos_frame";
    
    post_remote(GetCurrentObject(), 'Device.force_aba_validation_data_download', args, frame, true);
    delete args;
  }, 10);
}

function SetDepartmentReceiptLogo(strReceiptLogo) 
{
  // Bug# 18309 DH � Add the ability to configure the receipt header image.
  if (strReceiptLogo == "" || strReceiptLogo == undefined || strReceiptLogo == null)
    return;
    
  var base16String = {};
  base16String.strLogoBitmapBase16 = ConvertToBase16(strReceiptLogo);// Bug# 25780 DH - Moved the ConvertToBase16 to this file.
  g_DepartmentLogo = base16String;
}

                                  
function ConvertToBase16(s) // BUG# 8223 DH - HTML Editor
{
  // Bug# 25780 DH - Moved the ConvertToBase16 to this file.

  var r = "0x";
  var hexes = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f");
  for (var i = 0; i < s.length; i++) { r += hexes[s.charCodeAt(i) >> 4] + hexes[s.charCodeAt(i) & 0xf]; }
  return r;
}


function Device_test_health(args) 
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  // Does not matter here for now.
  // end bug 13720 DH
  
  return true;
}

function ResetAllPeripheralDevices()// Called by: DeviceReset
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  get_top().main.dequeue_message();
  ShowPeripheralsStatusUI = true;
  UpdatePeripheralsStatus(ShowPeripheralsStatusUI);
  return true;
}

function InformUser_ServiceDown()// Bug 23790 DH
{
  g_ServiceInitStatus = false;

  // Bug# 21651 DH
  alert("The peripherals service is currently not detected by iPayment. Ensure the service has loaded, then log into iPayment again.");
}

function CheckServiceHealth()// Bug 23790 DH
{
  var error__handler = function(result, param)
  {
    InformUser_ServiceDown();
    UpdatePeripheralsStatusCallback(null, false)// Update the flags
    get_top().main.dequeue_message(); // Bug 24056 DH
    return;
  }

  var success__handler = function (DeviceStatus, param)
  {
    if (DeviceStatus == null || DeviceStatus == undefined)
    {
      InformUser_ServiceDown();
      UpdatePeripheralsStatusCallback(null, false)// Update the flags
      get_top().main.dequeue_message();// Bug 24056 DH
      return;
    }
    else
    {
      // The way I did this is to restart the web service altogether. This is the most reliable way to clean USB/Serial connections up.
      // That means we need to wait a bit for the service to kick back in and start accepting REST requests. 
      get_top().main.dequeue_message();// Bug 24056 DH

      g_resettingPeripherals = true

      // Bug 17505 MJO - Moved reset call and message dequeueing into callbacks
      var error_handler = function ()
      {
        get_top().main.dequeue_message();
        g_resettingPeripherals = false;// Bug# 20825 DH - Configurable Peripherals Service ports
      }

      var success_handler = function()
      {
        ResetAllPeripheralDevices();
        g_resettingPeripherals = false;// Bug# 20825 DH - Configurable Peripherals Service ports
      }

      var tmp = g_ServiceAddress;// Bug# 20825 DH - Configurable Peripherals Service ports
      Init();
      g_ServiceAddress = tmp;// Bug# 20825 DH - Configurable Peripherals Service ports
    }
  }

  get_top().main.enqueue_message({ content: "<BR/><CENTER>Resetting peripherals. Please wait...</CENTER>", actions: {} });// Bug 24056 DH - Sometimes this takes a bit to comeback with a result. I didn't see this on my PC, but James did.
  AjaxCallPeripherals("check_health_all", success__handler, error__handler, "", "");
}

function DeviceReset()
{
  // Bug# 22129 DH - Panini: resetting peripherals does not detect the device in iPayment on certain computers; logout is required to detect it
  // Bug 13720 DH - iPayment Peripheral Overhaul
  g_ServiceInitStatus = true;  

  var error_handler = function (result, param)
  {
    get_top().main.dequeue_message();

    // Bug# 22129 DH - Panini: resetting peripherals does not detect the device in iPayment on certain computers; logout is required to detect it
    if ((result.indexOf("Service Unavailable") != -1) || (result.indexOf("Error calling peripherals") != -1))
      InformUser_ServiceDown();

    g_resettingPeripherals = false;// Bug# 20825 DH - Configurable Peripherals Service ports
    
    // Bug 26404 DH - Unable to print network receipts if peripheral service crashes while Epson printer was connected
    // Need to update the server side.
    UpdatePeripheralsStatusCallback(-1, false);
    return;
  }

  var success_handler = function (DeviceStatus, param)
  {
    setTimeout(function ()
    {
      CheckServiceHealth();// Bug 23790 DH - I moved the DeviceReset() code to provide better and more reliable user info while resetting devices when the service is down.
      ResetAllPeripheralDevices();
      g_resettingPeripherals = false;// Bug# 20825 DH - Configurable Peripherals Service ports
    }, 3000);// Let the reset finish up resetting or it will just run through async
    
  }

  get_top().main.enqueue_message({ content: "<BR/><CENTER>Resetting peripherals. Please wait...</CENTER>", actions: {} });
  AjaxCallPeripherals("reset_all_devices", success_handler, error_handler, "", "");
  // end Bug# 22129 DH 
  return;
}

function to_js_string(str) {
  //lert( '\"' + html_encode(str).replace(/"/g, '&amp;quot;') + '\"' );
  return "'" + html_encode(str).replace(/'/g, "\\'") + "'";
}

function get_current_obj () {  // http://localhost/pos_demo/foo/bar => my.foo.bar
	// TODO: simplify using location.pathname;
	var full_href=window.location.href;
	var start_protocol=full_href.indexOf("//");
	var start_site=full_href.indexOf("/",start_protocol+2);
	var start_path=full_href.indexOf("/",start_site+1);
	if (start_path==-1) return "my";
	else {
		var path_no_ext=full_href.substring(start_path+1,full_href.lastIndexOf("."));
		return path_no_ext.split("/").join(".");
	}
}

// Bug 19107 MJO - Cache value in CachedCurrentObject and return it if it can't find a valid CurrentObject
function GetCurrentObject(target) {
  // http://localhost/pos_demo/foo/bar => my.foo.bar
  // TODO: simplify using location.pathname;
  // Bug#9100 Mike O
  var W;
  if (get_top().main != null)
    W = get_top().main;
  else
    W = get_top();
  if (target != null) {
    W = GetWindow(target);
  }
  var full_href = W.location.href;
  var start_protocol = full_href.indexOf("//");
  var start_site = full_href.indexOf("/", start_protocol + 2);
  var start_path = full_href.indexOf("/", start_site + 1);
  if (start_path == -1)
    return CachedCurrentObject == "" ? "my" : CachedCurrentObject;
  else {
    var path_no_ext = full_href.substring(start_path + 1, full_href.lastIndexOf("."));
    var split_path = path_no_ext.split("/");
    if (split_path.length < 2) {
      return CachedCurrentObject == "" ? "my" : CachedCurrentObject;
    } else {
      CachedCurrentObject = split_path[0] + "." + split_path[1];
      return CachedCurrentObject;
    }
  }
}

// Bug 19080 MJO - Deleted outdated duplicate version of GetWindow

////////////////////////////////////////////////////////////////////////////////////////
//******************** VeriFone modyfied by DH for the peripherals overhaul ************

// Bug #14288 Mike O - VeriFone device variables
IsVeriFoneOpen = false,
VeriFoneSysInt = null,
VeriFoneTopApp = null,
VeriFoneAmount = null,
//VeriFoneCommandCard = false; // Bug 13720 DH - iPayment Peripheral Overhaul
VeriFoneSkip = false; // Bug #14891 Mike O

function VeriFone_SendCancel(type)// Bug 13720 DH - iPayment Peripheral Overhaul
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  get_top().main.dequeue_message();
  AjaxCallPeripherals("verifone_cancel/" + type, "", "", "", "");
}

// Bug 18811 MJO - The toggle is now sent separately and before processing is restarted
// Bug 20678 MJO - Use a variable to determine what we're switching to
// Bug 23412 MJO - Added separate manual entry prompt text
function VeriFone_ToggleManualEntry(amount, sig_cc_system_interface, top_app, timeout, debit, custom_message, manual_entry_message, toggle_to_manual) {
  try {
    var VeriFone_ToggleManualCallbackSuccess = function (result, param) {
      if (result == null)
        return;

      if (result.iLastErrorCode == -1) {
        VeriFone_LogError(result.strVeriFoneErrorMessage);
        alert(result.strLastErrorMessage);
        VeriFone_Canceled("error");// Don't need to cancel on device again, something went wrong
        return;
      }

      if (toggle_to_manual)
        VeriFone_ManualEntryReady(amount, sig_cc_system_interface, top_app, timeout, debit, custom_message, manual_entry_message);
      else
        VeriFone_ScannerReady(amount, sig_cc_system_interface, top_app, timeout, debit, custom_message, manual_entry_message);
    }

    var VeriFone_ToggleManualCallbackError = function (result, param) {
      VeriFone_LogError(result.strVeriFoneErrorMessage);
      alert(result.strLastErrorMessage);
      VeriFone_Canceled("error");// Don't need to cancel on device again, something went wrong
      return;
    }
  }
  catch (e) {
    VeriFone_LogError("VeriFone_ToggleManualEntry exception: " + e.message);
    alert("Toggle Manual Entry Communications Error");
    VeriFone_Canceled("error");// Don't need to cancel on device again, something went wrong
    return;
  }

  AjaxCallPeripherals("verifone_toggle_manual_entry", VeriFone_ToggleManualCallbackSuccess, VeriFone_ToggleManualCallbackError, "", "");
}


// Bug 18811 MJO - Manual entry now has its own function
// Bug 23412 MJO - Added separate manual entry prompt text and cleaned up a bit
function VeriFone_ManualEntryReady(amount, sig_cc_system_interface, top_app, timeout, debit, custom_message, manual_entry_message)
{
	// Bug 13720 DH - iPayment Peripheral Overhaul - Added this function since there is no UI in the service.
  
  // Bug 19451 MJO - Apparently Javascript can't multiply properly, so round this
  ConvertedAmount = (amount * 100).toFixed(0);
  
	var app = top_app;
	if(app.substr(0, 3) == "GEO")
    app = top_app.substr(4);
  
  if (manual_entry_message == "undefined" || manual_entry_message == undefined || manual_entry_message == null || manual_entry_message == "")
    manual_entry_message = "Please ask customer to enter card information";
  
  // Bug 23150 DJD If a zero amount is sent then it may be assumed that this is NOT a sale transaction but rather a wallet transaction for PAYware token 
  // creation.  Verifone devices do not work with zero (0.00) so the amount will be set to 0.01.
  if (amount == 0) {
    amount = 0.01;
  }
    
  // Create the auto entry prompt
  // Bug 22919 MJO - Disable buttons for 3 seconds to prevent spam causing timing issues
  cancel_btn = "<button class=verifone disabled=true onclick=\"get_pos().VeriFone_Canceled('cashiering');\">Cancel</button>";
  // Bug 20678 MJO - Use a variable to determine what we're switching to
  set_manual_entry = "get_top().main.dequeue_message();get_top().main.show_wait_dialog();get_pos().VeriFone_ToggleManualEntry(" + amount + ",'" + sig_cc_system_interface + "','" + top_app + "'," + timeout + "," + debit + "," +
    "'" + custom_message.replace("'", "\\'") + "','" + manual_entry_message.replace("'", "\\'") + "',false);";
  manual_btn = "<button class=verifone disabled=true onclick=\"" + set_manual_entry + "\">Tap & Swipe Entry</button>";

  get_top().main.enqueue_message({ content: "<CENTER>" + manual_entry_message + "</CENTER><BR/>" + manual_btn + cancel_btn, actions: {} });

  setTimeout(function () { get_top().main.$("button.verifone").prop("disabled", false); }, 3000);

  try
  {
    var VeriFone_ManualCallbackSuccess = function(result, param)
    {
      if(result == null)
        return;
        
      if(result.iLastErrorCode == -1) 
		  {	
        VeriFone_LogError(result.strVeriFoneErrorMessage);
        alert(result.strLastErrorMessage);
			    
			  get_top().main.dequeue_message();
			  VeriFone_Canceled("error");
			  return;
		  }
		  else if(result.iLastErrorCode == 0) 
		  {
		    if(result.strCancelSource == "timeout")// VeriFone will not cancel last operation. I need to send a cancel to the device.
			  {
			    alert('Timeout reached. Please try again.');
			    VeriFone_Canceled('cashiering');
			    return;
			  }
			  
		    // First check if user Canceled on the device
		    if(result.strCancelSource == "device")
			  {
			    VeriFone_Canceled("device");
			    return;
			  }
		  
		    if(result.strCancelSource == "cashiering")
			  {
			    return;// Canceled by clicking cancel on the UI screen. Do nothing. The device was already forced to cancel by VeriFone_Canceled().
			  }
		    
		    if(result.strCancelSource == "swipe_toggled_on_screen")
			  {
			    return;
			  }	
			  
		    // AJAX timeout
		    if((result.strManualCardNbr == null && result.strManualExpDate == null) || 
			  (result.strManualCardNbr == undefined && result.strManualExpDate == undefined) ||
			  (result.strManualCardNbr == "" && result.strManualExpDate == ""))
			  {
			    // Probably an AJAX timeout reached. I must cancel or we'll not get notified of device timeout since this thread blocking finished.
			    alert('Timeout reached. Please try again.');
			    VeriFone_Canceled('cashiering');
			    return;
			  }

		    VeriFone_ReceiveManualData(result.strManualCardNbr, result.strManualExpDate);
        return;
		  }
    }
    
    var VeriFone_ManualCallbackError = function(result, param)
    {
      VeriFone_LogError("VeriFone_ManualEntryReady communications error");
      alert("Manual Entry Communications Error");
		  VeriFone_Canceled("error");// Don't need to cancel on device again, something went wrong
		  return;
    }
  }
  catch (e) 
	{
    VeriFone_LogError("VeriFone_ManualEntryReady exception: " + e.message);
    alert("Manual Entry Communications Error");
		VeriFone_Canceled("error");// Don't need to cancel on device again, something went wrong
		return;
	}
  
  // Bug 19451 MJO - Use ConvertedAmount
	AjaxCallPeripherals("verifone_prompt_card_manual/" + ConvertedAmount, VeriFone_ManualCallbackSuccess, VeriFone_ManualCallbackError, "", "");
}

// Bug 16093 MJO - Added debit support
// Bug 18811 MJO - Moved manual entry processing into its own function
// Bug 23412 MJO - Added separate manual entry prompt text and cleaned up a bit
function VeriFone_ScannerReady(amount, sig_cc_system_interface, top_app, timeout, debit, custom_message, manual_entry_message) 
{
  // Bug 13720 DH - iPayment Peripheral Overhaul - Converted to peripherals overhaul code.
  
  if(top_app.substr(0, 3) == "GEO")
    VeriFoneTopApp = top_app.substr(4);
  else
    VeriFoneTopApp = top_app;
      
  if (custom_message == "undefined" || custom_message == undefined || custom_message == null || custom_message == "")
    custom_message = "Please ask customer to slide card";

  // Bug 23150 DJD If a zero amount is sent then it may be assumed that this is NOT a sale transaction but rather a wallet transaction for PAYware token 
  // creation.  Verifone devices do not work with zero (0.00) so the amount will be set to 0.01 and a custom message will be set.
  if (amount == 0) {
    amount = 0.01;
    custom_message = "Use payment device to add card to wallet; Card will NOT be charged 0.01.";
  }
  
  // Bug 19451 MJO - Apparently Javascript can't multiply properly, so round this
  // Bug 18811 MJO - Do this in the peripheral service
  //ConvertedAmount = (amount * 100).toFixed(0);
  
  VeriFoneAmount = amount;
  
  // Bug 19107 MJO - Don't overwrite with empty value
  if (sig_cc_system_interface != "")
    VeriFoneSysInt = sig_cc_system_interface;
	
  // Create the dialog for cancel/manual and retry
  // Bug 22919 MJO - Disable buttons for 3 seconds to prevent spam causing timing issues
  cancel_btn = "<button class=verifone disabled=true onclick=\"get_pos().VeriFone_Canceled('cashiering');\">Cancel</button>";
  // Bug 20678 MJO - Use a variable to determine what we're switching to
  set_manual_entry = "get_top().main.dequeue_message();get_top().main.show_wait_dialog();get_pos().VeriFone_ToggleManualEntry(" + amount + ",'" + sig_cc_system_interface + "','" + top_app + "'," + timeout + "," + debit + "," +
     "'" + custom_message.replace("'", "\\'") + "','" + manual_entry_message.replace("'", "\\'") + "',true);";
  
  // Bug 20752 MJO - Mx915 XPI manual entry support
  manual_btn = "<button class=verifone disabled=true onclick=\"" + set_manual_entry + "\">Manual Entry</button>";
  
  get_top().main.enqueue_message({content:"<CENTER>" + custom_message + "</CENTER><BR/>" + manual_btn + cancel_btn, actions:{}});

  setTimeout(function () { get_top().main.$("button.verifone").prop("disabled", false); }, 3000);

	try
	{
		var VeriFone_CallbackSuccess = function(result, param)
		{
		  if(result.iLastErrorCode == -1) 
			{	
        VeriFone_LogError(result.strVeriFoneErrorMessage);
        alert(result.strLastErrorMessage);
			    
			  get_top().main.dequeue_message();
			  VeriFone_Canceled("error");
			  return;
			}
			else if(result.iLastErrorCode == 0) 
			{		
			  if(result.strCancelSource == "timeout")// VeriFone will not cancel last operation. I need to send a cancel to the device.
			  {
			    alert('Timeout reached. Please try again.');
			    VeriFone_Canceled('cashiering');
			    return;
			  }
			
			  if(result.strCancelSource == "device")
			  {
			    VeriFone_Canceled('device');
			    return;
			  }
			  
			  if (result.strCancelSource == "swipe_toggled_on_screen")
			  {
			    return;
			  }
			  
			  if(result.strCancelSource == "cashiering")
			  {
			    return;// Canceled by clicking cancel on the UI screen. Do nothing. The device was already forced to cancel by VeriFone_Canceled().
			  }		  
			  
			  // AJAX timeout
			  if((result.strVeriFoneTrack1 == null && result.strVeriFoneTrack2 == null && result.strVeriFoneTrack3 == null) || 
			  (result.strVeriFoneTrack1 == undefined && result.strVeriFoneTrack2 == undefined && result.strVeriFoneTrack3 == undefined) ||
			  (result.strVeriFoneTrack1 == "" && result.strVeriFoneTrack2 == "" && result.strVeriFoneTrack3 == ""))
			  {
			    // Probably an AJAX timeout reached. I must cancel or we'll not get notified of device timeout since this thread blocking finished.
			    alert('Timeout reached. Please try again.');
			    VeriFone_Canceled('cashiering');
			    return;
			  }
			  
			  // Normal processing
			  VeriFone_ReceiveTrackData(result.strVeriFoneTrack1, result.strVeriFoneTrack2, result.strVeriFoneTrack3, result.strPin, result.strSwipeInd)
				return;
			}
		}
 
		var VeriFone_CallbackError = function(result, param)
    {
      VeriFone_LogError("VeriFone_ScannerReady communications error");
      alert("Card Capture Communications Error");
		  VeriFone_Canceled("error");// Don't need to cancel on device again, something went wrong
		  return;
    }
	}
	catch (e) 
	{
    VeriFone_LogError("VeriFone_ScannerReady exception: " + e.message);
    alert("Card Capture Communications Error");
		VeriFone_Canceled("error");// Don't need to cancel on device again, something went wrong
		return;
	}
  	
	AjaxCallPeripherals("verifone_prompt_card/" + amount + "/" + timeout + "/" + debit, VeriFone_CallbackSuccess, VeriFone_CallbackError, "", ""); 
}

// Bug #14288 Mike O - Store device-specific details retrieved from the VeriFone device
// Bug #14891 Mike O - Added derived key support
function VeriFone_SetInfo(deviceName, serialNbr, derivedKey) 
{
  if (typeof derivedKey == "undefined")
    derivedKey = "";
  
  if(VeriFoneTopApp.substr(0, 3) == "GEO")
    VeriFoneTopApp = VeriFoneTopApp.substr(4);
        
  // Bug 18811 MJO - Remove Manual Entry button if using Mx915
  g_IsMx915 = deviceName.indexOf("MX-XPI") > -1;
  
  args = {
    serial_nbr: serialNbr,
    device_name: deviceName,
    derived_key: derivedKey, // Bug #14891 Mike O - Added derived key support
    app: VeriFoneTopApp
  };

  // Bug #14891 Mike O - If not initializing, skip sending info back to the server (causes unwanted page refresh)
  if (VeriFoneSkip)
  {
    VeriFoneSkip = false;
  }
  else
  {
  // Bug #14885 Mike O - Just use the old approach to post directly back into the main frame
    // Bug 15151 MJO - Use iframe loader now instead of overwriting main frame, as it could get ugly when happening on a PB page
    post_remote('Device.Swipe', 'Device.Swipe.set_verifone_info', args, "pos_frame", true, null, null);
  }
}

// Bug #14288 Mike O - Send track data back to iPayment from the VeriFone device
// Bug 16093 MJO - PIN support
// Bug 26935 MJO - Added tap/swipe indicator
function VeriFone_ReceiveTrackData(track1, track2, track3, pin, swipeInd) 
{
//  if (VeriFoneCommandCard) // Bug 13720 DH - iPayment Peripheral Overhaul - removed since I have a dedicated handler for it
//  {
//    VeriFoneCommandCard = false;
//    VeriFone_ReceiveCommandCardData(track1, track2, track3);
//    return;
//  }
  
  if(VeriFoneTopApp.substr(0, 3) == "GEO")// DH added this. It will not run without it.
    VeriFoneTopApp = VeriFoneTopApp.substr(4);
  
  // Bug 15294 MJO - Removed VeriFoneFee
  args = {
    app: GetCurrentObject(),
    // Bug 21181 MJO - Added base64 encoding so CASL doesn't choke on special characters
    e_data: Base64.encode(Adumbrate(track1)),       //BUG 12666 NK credit card Adumbrate replaced ConverttoHex
    e_data2: Base64.encode(Adumbrate(track2)),      //BUG 12666 NK credit card Adumbrate replaced ConverttoHex
    amt: VeriFoneAmount,
    sig_cc_system_interface: VeriFoneSysInt,
    payment_type: "VeriFone",
    debit_pin: pin,
    swipe_ind: swipeInd
  };
  
  // Bug 15294 MJO - Show message here, and switched back to post_remote for security reasons
  
  get_top().main.dequeue_message();// Bug 13720 DH - iPayment Peripheral Overhaul
  get_top().main.enqueue_message({ content: '<BR/><BR/><CENTER><b><font size="+2"> Authorizing Card Information. Please Wait.</font></b></CENTER>', actions: {} });
  post_remote('Device.Swipe', 'Device.Swipe.receive_custom', args, 'main', true);
}

// Bug #14288 Mike O - Send manually-entered card number and exp. date back to iPayment from the VeriFone device
function VeriFone_ReceiveManualData(nbr, exp) 
{ 
  if(VeriFoneTopApp.substr(0, 3) == "GEO")
    VeriFoneTopApp = VeriFoneTopApp.substr(4);
    
  args = {
    app:VeriFoneTopApp,
    // Bug 21181 MJO - Added base64 encoding so CASL doesn't choke on special characters
    e_data: "+" + Base64.encode(Adumbrate(nbr + "|" + exp))  //BUG 12666 NK changing from ConverttoHex since all the credit card data is now encrypted with Adumbrate
  };
  
  // Bug 15294 MJO - Show message here, and switched back to post_remote for security reasons
  get_top().main.dequeue_message();// Bug 13720 DH - iPayment Peripheral Overhaul
  get_top().main.enqueue_message({ content: '<BR/><BR/><CENTER><b><font size="+2"> Authorizing Card Information. Please Wait.</font></b></CENTER>', actions: {} });
  post_remote('Device.Swipe', 'Device.Swipe.receive', args, 'main', true);
}

// Bug #14288 Mike O - Prompt has been Canceled by the VeriFone device or on-screen prompt, so cancel tender in iPayment
function VeriFone_Canceled(strCancelSource) // Bug 13720 DH - iPayment Peripheral Overhaul
{ 
  if(VeriFoneTopApp.substr(0, 3) == "GEO")
    VeriFoneTopApp = VeriFoneTopApp.substr(4);

	if(strCancelSource == "cashiering")
  {
    // Bug 23281 MJO - Don't cancel in iPayment until it's finished on the device side
    // Will need to void the tender as well.
    var VeriFone_Cancel_CallbackSuccess = function (result, param) {
      if (VeriFoneTopApp.indexOf("setup_app") == -1)
        post_remote(VeriFoneTopApp, 'Create_core_item_app.cancel', {}, 'main', true);
      else
        post_remote(VeriFoneTopApp, 'VeriFone_setup_app.cancel', {}, 'main', true);
    }

    var cancel_source = "cashiering";
    AjaxCallPeripherals("verifone_cancel/" + cancel_source, VeriFone_Cancel_CallbackSuccess, "", "", "");

    get_top().main.dequeue_message();
    get_top().main.cover();
    return;
  }
  
  if(strCancelSource == "device")// Canceled on device
	{
    // Cancelling on device, no need to call the device again, just backout the tender if any
  }
  
  if(strCancelSource == "error")
	{
    // Get out to main screen, everything will be backed out below. No need to call the service.
  }
  
  // Also cancel tender
  if (VeriFoneTopApp.indexOf("setup_app") == -1)
    post_remote(VeriFoneTopApp, 'Create_core_item_app.cancel', {}, 'main', true);
  else
    post_remote(VeriFoneTopApp, 'VeriFone_setup_app.cancel', {}, 'main', true);
}

// Debit card support
function VeriFone_ScannerReadyCommandCard(top_app) 
{
  // Bug 13720 DH - iPayment Peripheral Overhaul - Changed to to use a service instead 
  if(VeriFoneTopApp.substr(0, 3) == "GEO")// DH added this. It will not run without it.
    VeriFoneTopApp = top_app.substr(4);
  else // Bug 19032 MJO - This was missing for some reason
    VeriFoneTopApp = top_app;
  
  try
	{
		var VeriFone_ScannerReadyCommandCard_CallbackSuccess = function(result, param)
		{
		  if(result.iLastErrorCode == -1) 
      {	
        VeriFone_LogError("Command card error: " + result.strVeriFoneErrorMessage);
			  alert(result.strLastErrorMessage);
			  get_top().main.dequeue_message();
			  return;
			}
			else if(result.iLastErrorCode == 0) 
			{		
			  if(result.strCancelSource == "timeout")// VeriFone will not cancel last operation. I need to send a cancel to the device.
			  {
			    alert('Timeout reached. Please try again.');
			    VeriFone_Canceled('cashiering');
			    return;
			  }
			
			  if(result.strCancelSource == "device")
			  {
			    VeriFone_Canceled('device');
			    return;
			  }
			  
        // Bug 19032 MJO - Removed manual entry for command card
			  if(result.strCancelSource == "cashiering")
			  {
			    return;// Canceled by clicking cancel on the UI screen. Do nothing. The device was already forced to cancel by VeriFone_Canceled().
			  }
			  
			  // AJAX timeout
			  if((result.strVeriFoneTrack1 == null && result.strVeriFoneTrack2 == null && result.strVeriFoneTrack3 == null) || 
			  (result.strVeriFoneTrack1 == undefined && result.strVeriFoneTrack2 == undefined && result.strVeriFoneTrack3 == undefined) ||
			  (result.strVeriFoneTrack1 == "" && result.strVeriFoneTrack2 == "" && result.strVeriFoneTrack3 == ""))
			  {
			    // Probably an AJAX timeout reached. I must cancel or we'll not get notified of device timeout since this thread blocking finished.
			    alert('Timeout reached. Please try again.');
			    VeriFone_Canceled('cashiering');
			    return;
			  }
			  
			  // Normal processing
			  VeriFone_ReceiveCommandCardData(result.strVeriFoneTrack1, result.strVeriFoneTrack2, result.strVeriFoneTrack3)
				return;
			}
		}
 
		var VeriFone_ScannerReadyCommandCard_Error = function(result, param)
    {
      VeriFone_LogError("VeriFone_ScannerReadyCommandCard communications error");
      alert("Command card capture communications error");
		  VeriFone_Canceled("error");// Don't need to cancel on device again, something went wrong
		  return;
    }
    
    // Bug 19032 MJO - The prompt was missing
    cancel_btn = "<button onclick=get_pos().VeriFone_Canceled('cashiering')>Cancel</button>";
    get_top().main.enqueue_message({content:"<CENTER>" + "Please swipe the Command Card" + "</CENTER><BR/>" + cancel_btn, actions:{}});
	}
	catch (e) 
  {
    VeriFone_LogError("VeriFone_ScannerReadyCommandCard exception: " + e.message);
    alert("Command card capture communications error");
		VeriFone_Canceled("error");// Don't need to cancel on device again, something went wrong
		return;
	}
  	
  // Bug 19032 MJO - Added missing argument
	AjaxCallPeripherals("verifone_prompt_card/100/0/false", VeriFone_ScannerReadyCommandCard_CallbackSuccess, VeriFone_ScannerReadyCommandCard_Error, "", "");
}

function VeriFone_ReceiveCommandCardData(track1, track2, track3) 
{
  if (track1.indexOf("CMD GenNewKeys") == -1) 
  {
    alert("Non-Command Card swiped. Please try again.");
    if (IsVeriFoneOpen) {
      // Bug 16093 MJO - Debit card support
      // Bug 13720 DH - iPayment Peripheral Overhaul - No need for controller calls anylonger
      //Controller.VeriFone.PromptCard(this, 100, 0, false);
      setTimeout(function() {VeriFone_ScannerReadyCommandCard(VeriFoneTopApp);}, 10);
    }
    return;
  }
  
  args = {
    track_data: track2
  };

  // Bug 15294 MJO - Show message here, and cleaned up post_remote
  get_top().main.dequeue_message();// Bug 13720 DH - iPayment Peripheral Overhaul
  get_top().main.enqueue_message({ content: '<BR/><BR/><CENTER><b><font size="+2"> Authorizing Card Information. Please Wait.</font></b></CENTER>', actions: {} });
  // Bug 19032 MJO - Return into pos_frame for cleanliness
  post_remote(VeriFoneTopApp, 'VeriFone_setup_app.receive_command_card_data', args, 'pos_frame', true);
}

// Bug 15294 MJO - Removed button click for security reasons (GET is bad!)
// Bug 15294 MJO - Added convenience fee support
function VeriFone_ScannerFeeReady(amount, app, timeout, debit) 
{
  // Bug 13720 DH - iPayment Peripheral Overhaul - Changed this to call the prompt with custom message instead.

  // Bug 15294 MJO - Removed VeriFoneFee
  // Bug 19107 MJO - Don't bother overwriting this
  // VeriFoneTopApp = app;
  
  //if (IsVeriFoneOpen) 
  //{
    // Bug 15294 MJO - Added a short delay, to make sure the previous prompt finished cleaning up
    setTimeout(function() 
    {
      // Bug 18811 MJO - No need to use debit for fee confirmation, increased delay
      // Bug IPAY-1576 MJO - Fixed one bug with XPI fees (there are more)
      VeriFone_ScannerReady(amount, "", app, timeout, false, "", "Please prompt user to swipe again to accept fees");
      
      // Bug 13720 DH - iPayment Peripheral Overhaul - Useing the above instead
      //Controller.VeriFone.PromptCardWithCustomMessage(this, amount * 100, timeout, debit, "Please prompt user to swipe again to accept fees");
      }, 1000);
  //}
}

// Bug 18811 MJO - Mx915 XPI support
function VeriFone_SignatureReady(user_message, customer_message, auto_continue, top_app) {
  if (top_app != null) {
    if (VeriFoneTopApp.substr(0, 3) == "GEO")
      VeriFoneTopApp = top_app.substr(4);
    else
      VeriFoneTopApp = top_app;
  }

  // Create the dialog for cancel/manual and retry
  cancel_btn = "<button onclick=get_pos().VeriFone_Canceled('cashiering')>Cancel</button>";

  if (user_message == undefined || user_message == null || user_message == "")
    get_top().main.enqueue_message({ content: "<CENTER>" + "Please sign" + "</CENTER><BR/>" + cancel_btn, actions: {} });
  else
    get_top().main.enqueue_message({ content: "<CENTER>" + user_message + "</CENTER><BR/>" + cancel_btn, actions: {} });

  try {
    var VeriFone_CallbackSuccess = function (result, param) {
      if (result.iLastErrorCode == -1) {


        alert(result.strLastErrorMessage);// Could be undefined error so show it to the user.

        get_top().main.dequeue_message();
        VeriFone_Canceled("error");
        return;
      }
      else if (result.iLastErrorCode == 0) {
        if (result.strCancelSource == "timeout")// VeriFone will not cancel last operation. I need to send a cancel to the device.
        {
          alert('Timeout reached. Please try again.');
          VeriFone_Canceled('cashiering');
          return;
        }

        if (result.strCancelSource == "device") {
          VeriFone_Canceled('device');
          return;
        }

        if (result.strCancelSource == "cashiering") {
          return;// Canceled by clicking cancel on the UI screen. Do nothing. The device was already forced to cancel by VeriFone_Canceled().
        }

        // AJAX timeout
        if ((result.strSignatureData == "")) {
          // Probably an AJAX timeout reached. I must cancel or we'll not get notified of device timeout since this thread blocking finished.
          alert('Timeout reached. Please try again.');
          VeriFone_Canceled('cashiering');
          return;
        }

        // Normal processing
        VeriFone_ReceiveSignatureData(result.strVeriFoneSignatureData, user_message, customer_message, auto_continue);

        return;
      }
    }

    var VeriFone_CallbackError = function (result, param) {
      alert("SIGNATURE CAPTURE ERROR!");
      VeriFone_Canceled("error");// Don't need to cancel on device again, something went wrong
      return;
    }
  }
  catch (e) {
    alert("SIGNATURE CAPTURE ERROR!\n\n" + e.description)
    VeriFone_Canceled("error");// Don't need to cancel on device again, something went wrong
    return;
  }

  AjaxCallPeripherals("verifone_signature_capture/" + customer_message, VeriFone_CallbackSuccess, VeriFone_CallbackError, "", "");
}

function VeriFone_ReceiveSignatureData(signature_data, user_message, customer_message, auto_continue) {
  if (signature_data == null)
    return;

  get_top().main.dequeue_message();
  image = "<BR/><img src='data:image/bmp;base64," + encodeURIComponent(signature_data) + "' alt='' /><BR/>";
  // Bug 20678 MJO - Don't send another Cancel message
  cancel_btn = "<button onclick=get_pos().VeriFone_Canceled('')>Cancel</button>";
  accept_btn = "<button onclick=get_pos().VeriFone_AcceptSignatureData('" + encodeURIComponent(signature_data) + "'," + auto_continue + ",'" + encodeURIComponent(user_message) + "','" + encodeURIComponent(customer_message) + "')>Accept</button>";
  retry_btn = "<button onclick=\"get_top().main.dequeue_message();get_pos().VeriFone_SignatureReady('" + user_message + "','" + customer_message + "','" + auto_continue + "', null)\">Retry</button>";
  get_top().main.enqueue_message({ content: "<CENTER>" + "Do you accept this image?" + "</CENTER>" + image + "<BR/>" + retry_btn + accept_btn + cancel_btn, actions: {} });
}

function VeriFone_AcceptSignatureData (signature_data, auto_continue, user_message, customer_message) {
  args = {
    app: GetCurrentObject(),
    content: decodeURIComponent(signature_data),
    content_type: "image/bmp",
    // Bug 19858 MJO - Store info about the request for the image table
    // Bug 23529 MJO - Save customer message & confirmation text as well if present
    str_message_to_save: customer_message + VeriFone_ConfirmationText
  };

  get_top().main.dequeue_message();
  get_top().main.cover();

  // Bug 23497 MJO - Wait until the signature has been stored to move on
  var on_finish = function () {
    if (auto_continue) {
      get_top().main.form_inst['_method_name.continue'].value = 'continue';
      get_top().main.form_inst.submit();
    }
  };

  if (!auto_continue) {
    // Bug 22881 MJO - Move on to next document
    args["next_document"] = true;
  }

  post_remote('Device.SignaturePad', 'Device.SignaturePad.receive_signature', args, "pos_frame", true, on_finish, on_finish);

  VeriFone_ConfirmationText = "";
}

var VeriFone_ConfirmationText = "";

function VeriFone_ConfirmationReady(confirmation_prompt, confirmation_action_positive, confirmation_action_negative, confirmation_button_positive, confirmation_button_negative, user_message, customer_message, top_app) {
  if (top_app != null) {
    if (VeriFoneTopApp.substr(0, 3) == "GEO")
      VeriFoneTopApp = top_app.substr(4);
    else
      VeriFoneTopApp = top_app;
  }

  // Create the dialog for cancel/manual and retry
  cancel_btn = "<button onclick=get_pos().VeriFone_Canceled('cashiering')>Cancel</button>";

  // Bug 26337 MJO - Use configured text if present
  if (user_message == undefined || user_message == null || user_message == "")
    get_top().main.enqueue_message({ content: "<CENTER>" + "Confirmation in progress" + "</CENTER><BR/>" + cancel_btn, actions: {} });
  else
    get_top().main.enqueue_message({ content: "<CENTER>" + user_message + "</CENTER><BR/>" + cancel_btn, actions: {} });

  VeriFone_ConfirmationText = "";

  try {
    var VeriFone_CallbackSuccess = function (result, param) {
      if (result.iLastErrorCode == -1) {
        // Device may not have the debit firmware, although I don't think it's possible to do a manual debit.
          alert(result.strLastErrorMessage);// Could be undefined error so show it to the user.

        get_top().main.dequeue_message();
        VeriFone_Canceled("error");
        return;
      }
      else if (result.iLastErrorCode == 0) {
        if (result.strCancelSource == "timeout")// VeriFone will not cancel last operation. I need to send a cancel to the device.
        {
          alert('Timeout reached. Please try again.');
          VeriFone_Canceled('cashiering');
          return;
        }

        if (result.strCancelSource == "device") {
          VeriFone_Canceled('device');
          return;
        }

        if (result.strCancelSource == "cashiering") {
          return;// Canceled by clicking cancel on the UI screen. Do nothing. The device was already forced to cancel by VeriFone_Canceled().
        }

        // AJAX timeout
        if ((result.strSignatureData == "")) {
          // Probably an AJAX timeout reached. I must cancel or we'll not get notified of device timeout since this thread blocking finished.
          alert('Timeout reached. Please try again.');
          VeriFone_Canceled('cashiering');
          return;
        }

        // Normal processing
        VeriFone_ConfirmationText = "||" + confirmation_prompt + "||" +
          "Customer chose " + (result.strVeriFoneConfirmationData == "T" ? confirmation_button_positive : confirmation_button_negative);

        VeriFone_ReceiveConfirmationData(result.strVeriFoneConfirmationData, confirmation_action_positive, confirmation_action_negative, user_message, customer_message);
        return;
      }
    }

    var VeriFone_CallbackError = function (result, param) {
      alert("CONFIRMATION ERROR!");
      VeriFone_Canceled("error");// Don't need to cancel on device again, something went wrong
      return;
    }
  }
  catch (e) {
    alert("CONFIRMATION ERROR!\n\n" + e.description)
    VeriFone_Canceled("error");// Don't need to cancel on device again, something went wrong
    return;
  }

  AjaxCallPeripherals("verifone_confirmation/" + Base64.encode(confirmation_prompt) + "/" + confirmation_button_positive + "/" + confirmation_button_negative, VeriFone_CallbackSuccess, VeriFone_CallbackError, "", "");
}

function VeriFone_ReceiveConfirmationData(confirmation_data, confirmation_action_positive, confirmation_action_negative, user_message, customer_message) {

  get_top().main.dequeue_message();

  var confirmation_action = confirmation_data == "T" ? confirmation_action_positive : confirmation_action_negative;

  if (confirmation_action == "SIGN") {
    // Bug 23412 MJO - Hardcode signature prompt
    setTimeout(function () { VeriFone_SignatureReady("Please sign", customer_message, true, null) }, 500);
  }
  else if (confirmation_action == "CONTINUE") {
    get_top().main.form_inst['_method_name.continue'] = 'continue';
    get_top().main.form_inst.submit();
  }
  // Bug 20158 MJO - Consolidated fee prompt
  else if (confirmation_action == "CONFIRM_FEE") {
    post_remote(VeriFoneTopApp, 'Create_core_item_app.next_document', { 'return_iframe_loader': true }, 'pos_frame', true);
  }
  else {
    post_remote(VeriFoneTopApp, 'Create_core_item_app.cancel', {}, 'main', true);
  }
}
// End Bug 18811 MJO - Mx915 XPI support

// Bug 21708 MJO - Only recheck VeriFone
function VeriFone_RecheckHealth() {
  
  var VeriFone_CallbackSuccess = function (result, param) {
    if (result.VeriFoneHealth == "INSTALLED" && result.VeriFoneDeviceDeviceName != "" && result.VeriFoneDeviceSerialNbr != "") {
      VeriFone_SetInfo(result.VeriFoneDeviceDeviceName, result.VeriFoneDeviceSerialNbr, result.VeriFoneDeviceDerivedKey);
    }
    else {
      setTimeout(function () {
        VeriFone_RecheckHealth();
      }, 5000);
    }
  }

  var VeriFone_CallbackError = function (result, param) {

  }

  AjaxCallPeripherals("verifone_recheck_health", VeriFone_CallbackSuccess, VeriFone_CallbackError, "", "");
}

// Bug 23824 MJO - Log peripheral service errors
function VeriFone_LogError(errStr) {
  var args = {
    top_app: GetCurrentObject(),
    err_str: errStr
  };

  post_remote("Document.VeriFone", "Document.VeriFone.log_error", args, "pos_frame", true);
}

////////////////////////////////////////////////////////////////////////////////////////

function AjaxCallPeripherals(sMethodNameWithParameters/*Service address and params for GET verb*/, SuccessCallback/*Callback to handle success, can be empty str*/, ErrorCallback/*Callback to handle errors, can be empty str*/, CallbackFunctionParams/*Params passed to callback*/, PostJsonData/*Data sent via POST*/)
{
  // Bug 13720 DH - iPayment Peripheral Overhaul
  var xdr = null;
  var XDomain = false;
  var bAlreadyHandeledError = false;// Different versions and browsers support different menthods.
  var bAlreadyHandeledSuccess = false;// Different versions and browsers support different menthods.
  var nAgt = navigator.userAgent;
  var PeripheralsOverhaul_CrossDomain = true;
  jQuery.support.cors = PeripheralsOverhaul_CrossDomain; // Seems to be needed by all browsers including IE!

  try
  {
    // Some madness here:
    try
    {
      // Try this first.
      //if(typeof XDomainRequest != "undefined")
      //{
      // No SYNC supported, all calls will be ASYNC <-- stinks
      var xdr = XDomainRequest();// These will explode sometimes when the flags are wrong.
      XDomain = true;
      //}
    } catch (e) { XDomain = false; }

    try
    {
      if (XDomain == false)
      {
        if (window.XMLHttpRequest)
        {
          xdr = new XMLHttpRequest();// These will explode sometimes when the flags are wrong.
          XDomain = false;
        }
      }
    } catch (e) { }

    if (xdr) 
    {
      xdr.onload = function ()
      {
        // Bug# 18606 DH - Merging - Bug 18603 - JavaScript "Invalid Character" Error On Login [Added check for status 404 (not found)]
        if (!bAlreadyHandeledSuccess && xdr.status != 503 && xdr.status != 404)
        {
          if (SuccessCallback != "" && SuccessCallback != undefined)
            SuccessCallback(get_top().main.$.parseJSON(xdr.responseText), CallbackFunctionParams);
          bAlreadyHandeledSuccess = true;
        }
      }
      xdr.onerror = function ()
      {
        if (!bAlreadyHandeledError)
        {
          errText = "Status: " + xdr.status + "\nStatus text: " + xdr.statusText;
          if (errText == "")// Missing error description. Firefox does this.
            errText = "Undefined error";

          if (ErrorCallback == "" || ErrorCallback == "undefined")// Only show if not handled by caller
          {
            // Only show this if the service is initialized.
            if (g_ServiceInitStatus)// Bug# 17902 Comment#45 DH
              alert("Error calling peripherals service.\n\nCalling: " + sMethodNameWithParameters + "\n\nError: " + errText);// Bug 24367 DH - Provide peripherals REST endpoint call information and parameters
          }

          if (ErrorCallback != "" && ErrorCallback != "undefined")// We may not care for this error at all. Just disregard it.
            ErrorCallback("", CallbackFunctionParams);// Recipient may choose to do nothing when error received. It could be the same function as success that was passed in. Let it know there was an error.

          bAlreadyHandeledError = true;
        }
      }

      if (PostJsonData != "" && PostJsonData != undefined)
      {
        xdr.open("POST", g_ServiceAddress + sMethodNameWithParameters, true);
        xdr.setRequestHeader("Content-Type", "application/json");
        xdr.send(get_top().main.JSON.stringify(PostJsonData));
      }
      else
      {
        xdr.open("GET", g_ServiceAddress + sMethodNameWithParameters, true);
        xdr.send();
      }
      xdr.onreadystatechange = function () // callback support
      {
        // New way MS does it
        if (xdr.readyState == 4 && xdr.status == 200) 
        {
          if (!bAlreadyHandeledSuccess)
          {
            if (SuccessCallback != "" && SuccessCallback != undefined)
            {
              var data = $.parseJSON(xdr.responseText);
              SuccessCallback(data, CallbackFunctionParams);
            }
            bAlreadyHandeledSuccess = true;
          }
          return;
        }
        else if (xdr.readyState == 4 && xdr.status != 200)
        {
          if (!bAlreadyHandeledError)
          {
            errText = "Status: " + xdr.status + "\nStatus text: " + xdr.statusText;
            if (errText == "")// Missing error description. Firefox does this.
              errText = "Undefined error";

            if (ErrorCallback == "" || ErrorCallback == "undefined")// Only show if not handled by caller
            {
              if (g_ServiceInitStatus)// Bug# 17902 Comment#45 DH
                alert("Error calling peripherals service.\n\nCalling: " + sMethodNameWithParameters + "\n\nError: " + errText); // Bug 24367 DH - Provide peripherals REST endpoint call information and parameters
              return;
            }

            if (ErrorCallback != "" && ErrorCallback != "undefined")// We may not care for this error at all. Just disregard it.
            {
              if (g_ServiceInitStatus)// Bug# 17902 & 18886 Comment#45 DH
                ErrorCallback("Error calling peripherals service.\n\nCalling: " + sMethodNameWithParameters + "\n\nError: " + errText, CallbackFunctionParams);// Bug 24367 DH - Provide peripherals REST endpoint call information and parameters
              else
              {
                // Bug 24475 DH
                // Lets see if we can reconnect in few seconds. User could be switching to another user on the same workstation in witch case the service takes a bit to reconnect all devices.
                // Not connecting here could force users to need to log out and log back in to iPayment or reset peripherals.
                if (xdr.status == 503 && sMethodNameWithParameters == "get_service_ip_address" && g_ServiceConnectAttempts <= 4)// If we can't get a connection in 3 attempts, the services is probably down for good.
                {
                  g_ServiceConnectAttempts++;
                  setTimeout(function () { AjaxCallPeripherals(sMethodNameWithParameters, SuccessCallback, ErrorCallback, CallbackFunctionParams, PostJsonData); }, 1000);
                  return;
                }
                // end Bug 24475 DH

                CallPOSStart();
              }
            }

            bAlreadyHandeledError = true;
          }
          return;
        }
      }
      return;
    }
  }
  catch (e) 
  {
    if (ErrorCallback == "" || ErrorCallback == "undefined")// Only show if not handled by caller
    {
      if (g_ServiceInitStatus)// Bug# 17902 Comment#45 DH
        alert("Error calling peripherals service.\n\nCalling: " + sMethodNameWithParameters + "\n\n Error: " + e.description);// Bug 24367 DH - Provide peripherals REST endpoint call information and parameters
    }

    if (ErrorCallback != "" && ErrorCallback != "undefined")// We may not care for this error at all. Just disregard it.
      ErrorCallback(e.description + "\n\nCalling: " + sMethodNameWithParameters, CallbackFunctionParams);// Receipt may choose to do nothing when error received. It could be the same function as success that was passed in. Let it know there was an error. // Bug 24367 DH - Provide peripherals REST endpoint call information and parameters
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
// Bug 17505 MJO - -Point device
var Point_DocumentType = null;
var Point_DequeueMessage = null;

// Code from https://stackoverflow.com/questions/3446170/escape-string-for-use-in-javascript-regex
function escapeRegExp(str) {
  return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

var Point_SendRequestAsync_SuccessCallback = function (result, param) {
  if (result.Point_SendRequestResult.strPointMessage.indexOf("Cancel not allowed now") !== -1)
    --Point_RequestCount;

  // Bug 23838 MJO - Stop sending status requests if they are failing
  message = result.Point_SendRequestResult.strPointMessage;

  if (Point_EnablePolling && message.indexOf("A connection attempt failed") != -1 && message.indexOf(":5016") != -1) {
    Point_EnablePolling = false;
    Point_SendPollRequest = true;
    get_top().main.$("#point_status").text("Status Unknown");
  }

  // Bug 22592 MJO - If this is a status response, update the text in the message
  // Bug 8932 MJO - Check for session duration instead of detailed status in case the latter is missing
  if (result.Point_SendRequestResult.strPointMessage.indexOf("SESSION_DURATION") !== -1 || result.Point_SendRequestResult.strPointMessage.indexOf("Operation SUCCESSFUL") !== -1) {
    Point_SendPollRequest = true;
    
    start = message.indexOf("<SECONDARY_DATA>") + 16;
    len = message.indexOf("</SECONDARY_DATA>") - start;
    status_code = message.substr(start, len);

    if (message.indexOf("<DETAILED_STATUS>") == -1) {
      detailed_status_code = "";
    }
    else {
      start = message.indexOf("<DETAILED_STATUS>") + 17;
      len = message.indexOf("</DETAILED_STATUS>") - start;
      detailed_status_code = message.substr(start, len);
    }

    if (detailed_status_code == "52") status_text = "Prompting to Remove Card";
    else if (status_code == "11") status_text = "Session Started";
    else if (status_code == "13" && detailed_status_code == "2") status_text = "Prompting for Payment";
    else if (status_code == "13" && detailed_status_code == "3") status_text = "Prompting for PIN";
    else if (status_code == "13" && detailed_status_code == "5") status_text = "Prompting for Payment Type";
    else if (status_code == "13") status_text = "Prompting for Data";
    else if (status_code == "14") status_text = "Communicating with Processor - Don't Remove Card";
    else if (status_code == "41") status_text = "Processing";
    else if (status_code == "51") status_text = "Prompting for Signature";
    else if (status_code == "0") status_text = "Cancelled";
    else status_text = "Unknown Status Codes: " + status_code + ", " + detailed_status_code;

    //Point_StatusCodes += status_text + "\t" + status_code + "\t" + detailed_status_code + "\n";

    //status_text += " " + status_code + ", " + detailed_status_code;

    get_top().main.$("#point_status").text(status_text);
    return;
  }

  var args = {
    message:result.Point_SendRequestResult.strPointMessage,
    err:result.Point_SendRequestResult.bPointError,
    app:get_top().main.get_current_obj()
  };
  
  // Bug 23838 MJO - Make case insensitive for the e355
  // Bug 25482 MJO - Cancel on card capture, not session finish
  if (args.message.toLowerCase().indexOf("<result>") > -1) {
    Point_EnablePolling = false;
    Point_SendPollRequest = true;
  }
  
  args.message = args.message.replace(new RegExp(escapeRegExp("\'"), 'g'), '"');
  
  var to_execute = Point_DequeueMessage ? function(){get_top().main.dequeue_message();} : null;

  // Bug 25482 MJO - Only use target for session fixing for secondary messages to prevent a race condition
  // Bug 27335 MJO - Added Transaction CANCELLED
  // Bug IPAY-693 MJO - Added Cancel not allowed
  var ignore_response = message.indexOf("<SECONDARY_DATA>") !== -1 &&
    (message.indexOf("No Transaction to CANCEL") === -1 && message.indexOf("Transaction CANCELLED") === -1 && message.indexOf("Cancel not allowed now") === -1);
  
  // Bug 17898 MJO - Don't show wait dialog for cancel
  // Bug 17898 MJO - Don't show wait dialog for cancel failure either
  // Bug 21323 MJO - Updated this to fix manual entry processing loop
  // Bug 19231 MJO - Don't show if doing signature confirmation
  if (param.is_card && (args.message.indexOf("<RESULT>CAPTURED</RESULT>") != -1 && args.message.indexOf("<TERMINATION_STATUS>SUCCESS</TERMINATION_STATUS>") != -1) &&
    args.message.indexOf("<SIGNATUREDATA>") == -1 && args.message.indexOf("<RESPONSE_TEXT>Cancelled by POS</RESPONSE_TEXT>") == -1 && !args.err) {
    // Bug 23116 MJO - Use cover instead of show_wait_dialog
    get_top().main.dequeue_message();
    get_top().main.cover();
  }
  else if (param.is_card && args.message.indexOf("<RESPONSE_TEXT>Cancelled by POS</RESPONSE_TEXT>") == -1) {
    get_top().main.dequeue_message();
    get_top().main.cover(5); // Bug 27205 MJO - Show cover almost right away so it doesn't pop over the signature confirmation
  }

  post_remote(Point_DocumentType, 'Business.Document.VeriFone_point.receive_response', args, ignore_response ? null : 'pos_frame', true, to_execute);
  
  return;
};

var Point_SendRequestAsync_ErrorCallback = function (result, param) {
  var args = {
    message: (typeof result.Point_SendRequestResult === "undefined") ? result : result.Point_SendRequestResult.strPointMessage,
    err: (typeof result.Point_SendRequestResult === "undefined") ? true : result.Point_SendRequestResult.bPointError,
    app:get_top().main.get_current_obj()
  };
  
  args.message = args.message.replace(new RegExp(escapeRegExp("\'"), 'g'), '"');
  
  var to_execute = Point_DequeueMessage ? function(){get_top().main.dequeue_message();} : null;

  post_remote(Point_DocumentType, 'Business.Document.VeriFone_point.receive_response', args, 'pos_frame', true, to_execute);
  
  return;
};

var Point_RequestCount = 0; // Bug 22592 MJO - Keep track of requests so we know when it's safe to continue during a transfer
var Point_LastRequestTime = new Date(); // Bug 23838 MJO

// Bug 23838 MJO - Proper message delay functionality
function SendPointRequest(xml, port, document, dequeue_message, is_card, delay) {
  if (delay !== undefined && new Date().getTime() - Point_LastRequestTime.getTime() < delay)
    setTimeout(function () { SendPointRequestAux(xml, port, document, dequeue_message, is_card); }, delay - (new Date().getTime() - Point_LastRequestTime.getTime()));
  else
    SendPointRequestAux(xml, port, document, dequeue_message, is_card);
}

function SendPointRequestAux (xml, port, document, dequeue_message, is_card) {
  if (document != null) {
  Point_DocumentType = document;
  Point_DequeueMessage = dequeue_message;
  }

  if (is_card !== true)
    is_card = false;

  // Bug 25482 MJO - Secondary messages shouldn't be involved in delays
  if (port !== 5016)
  Point_LastRequestTime = new Date();

  AjaxCallPeripherals("point_send_request", Point_SendRequestAsync_SuccessCallback, Point_SendRequestAsync_ErrorCallback, { is_card: is_card }, { message:xml, port:port.toString() });

  Point_RequestCount++;
}

// Bug 19153 MJO - Peripheral service needs to be provided with the device's IP address
function SetPointIPAddress(device_ip) {
  AjaxCallPeripherals("point_set_ip_address", Point_SetPointIPAddressAsync_SuccessCallback, function () { }, "", { IPAddress: device_ip });
}

var Point_SetPointIPAddressAsync_SuccessCallback = function (result, param) {
  if (result.Point_SetIPAddressResult) {
    var args = {
      app: get_top().main.get_current_obj()
    };

    post_remote('Business.Document.VeriFone_point', 'Business.Document.VeriFone_point.complete_initialization', args, 'pos_frame', true);
  }

  return;
};

// Bug 22592 MJO - Device status display support
var Point_EnablePolling = false;
// Bug 23838 - Don't send multiple status requests at once
var Point_SendPollRequest = true;
//var Point_StatusCodes = "";
var PointPollingInterval = null;

function Point_StartPolling() {
  Point_EnablePolling = true;
  
  // Bug 26056 MJO - Make sure there aren't overlapping intervals (unlikely but CYA)
  if (PointPollingInterval !== null)
    clearInterval(PointPollingInterval);

  PointPollingInterval = setInterval(function () {
    if (Point_EnablePolling) {
      if (Point_SendPollRequest) {
        SendPointRequest(Base64.encode("<TRANSACTION><FUNCTION_TYPE>SECONDARYPORT</FUNCTION_TYPE><COMMAND>STATUS</COMMAND><POS_RECON>1234</POS_RECON></TRANSACTION>"), 5016, null, false, false);
        Point_SendPollRequest = false;
      }
    }
    else {
      //alert(Point_StatusCodes);
      clearInterval(PointPollingInterval);
      PointPollingInterval = null;
    }
  }, 2000);
}

// Bug 25434 MJO - Start DMS Point code
var Point_StopDMSUpdate = false;

function Point_Start_DMS_Update() {
  Point_StopDMSUpdate = false;
  Point_DMS_Update();
}

function Point_DMS_Update() {
  var args = { app: GetCurrentObject() };

  post_remote(Point_DocumentType, 'Business.Document.VeriFone_point.dms_update', args, 'pos_frame', true);
}

function Point_Stop_DMS_Update() {
  Point_StopDMSUpdate = true;
}

function Point_Process_DMS_Update(message, status) {
  if (status != null)
    get_top().main.$("#point_status").text(status);

  var args = { app: get_top().main.get_current_obj() };

  if (message !== null) {
    /*if ((message.indexOf("<RESULT>CAPTURED</RESULT>") != -1 && message.indexOf("<TERMINATION_STATUS>SUCCESS</TERMINATION_STATUS>") != -1) &&
      message.indexOf("<SIGNATUREDATA>") == -1 && message.indexOf("<RESPONSE_TEXT>Cancelled by POS</RESPONSE_TEXT>") == -1 && args.err) {
      // Bug 23116 MJO - Use cover instead of show_wait_dialog
      get_top().main.dequeue_message();
      get_top().main.cover();
    }
    else if (message.indexOf("<RESPONSE_TEXT>Cancelled by POS</RESPONSE_TEXT>") == -1) {
      get_top().main.dequeue_message();
    }*/

    if (message.toUpperCase().indexOf("SESSION STARTED") === -1 && message.toUpperCase().indexOf("SESSION FINISHED") === -1)
      get_top().main.dequeue_message();

    args.message = message;
    args.err = false;

    post_remote(Point_DocumentType, 'Business.Document.VeriFone_point.receive_response', args, 'pos_frame', true);
  }

  if (!Point_StopDMSUpdate)
    setTimeout("Point_DMS_Update();", 1000);
}
// Bug 25434 MJO - End DMS Point code
// End Point device code

// Bug 19372 MJO - Moved from main.js
// Bug 19153 MJO - Removed point_ip (it's passed in later if needed)
function InitializePeripheralsModal(top_app) 
{
  
  setTimeout(function () { InitializePeripheralsModalAux(top_app) }, 1000);
}

function InitializePeripheralsModalAux(top_app) {
  // Bug 13720 DH - iPayment Peripheral Overhaul / No need to initialize peripherals any longer.
  try {
    UpdatePeripheralsStatus(false);
    VeriFoneTopApp = top_app;// Bug #14288 Mike O - Pass through top_app so it can have VeriFone data stored there later
  }
  catch (e) {
    alert(e.description);
  }
  // End Bug 13720 DH - iPayment Peripheral Overhaul
}

// Bug 8932 MJO - For continuing from pos.start to start_aux when the peripheral service isn't running
function CallPOSStart() 
{
  if (g_pos_start) 
  {
    post_remote(GetCurrentObject(), "pos.start_aux", {}, "main", true);
    g_pos_start = false;
  }
}

// Bug 24518 MJO - Go back to end of event printing
function ShowEndOfEventMessage(successful) {
  var args = {
    top_app: GetCurrentObject(),
    iframe: true,
    increment_processed: successful
  };

  post_remote("Business.End_of_event_printing", 'Business.End_of_event_printing.show_message', args, 'pos_frame', true);
}
// Bug 21870 - Add HTML5 Signature Capture feature
var signaturePad = null;
var canvas = null;
var clearButton = null;
var savePNGButton = null;
var CancelButton = null;
var wrapper = null;
var gl_app = null;

//Bug 24762 - NK added void_sign flag for signature for void reason

function InitSignaturePad(app, content, str_message_to_save, void_sign) {
    wrapper = get_top().main.document.getElementById("signature-pad");
    clearButton = wrapper.querySelector("[data-action=clear]");
    savePNGButton = wrapper.querySelector("[data-action=save-png]");
    CancelButton = wrapper.querySelector("[data-action=cancel-svg]");
    gl_app = app;
    canvas = wrapper.querySelector("canvas");

    window.onresize = resizeCanvas;
    resizeCanvas();
    signaturePad = new SignaturePad(canvas);

    clearButton.addEventListener('click', function (event) {
        signaturePad.clear();
    });

    savePNGButton.addEventListener("click", function (event) {
        if (signaturePad.isEmpty()) {
            alert("Please provide signature first.");
        } else {
            var data = signaturePad.toDataURL();
            var result = data.replace(/^.*base64,/, "");
            
            SignaturePad_SignatureData(gl_app, encodeURIComponent(result), true, content, str_message_to_save, void_sign);

        }
    });

    CancelButton.addEventListener('click', function (event) {
      signaturePad.clear();
      CancelSignaturePad();
    });

}
function resizeCanvas() {
    var ratio = window.devicePixelRatio || 1;
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
}


function CancelSignaturePad() {
  
   get_top().main.dequeue_message();
   subject = get_top().main.get_current_obj();
  alert("Signature capture canceled.");

   get_pos().SignaturePadVoidLastCC({
 	//Bug 25544 NK cancelling closes out of Event Tools -->
     app: gl_app,
     //app: get_top().main.get_current_obj(),
     sig_cc_system_interface: g_str_sig_cc_system_interface
 });
 
}

function SaveSignaturePad() {
    if (signaturePad.isEmpty()) {
        alert("Please provide signature first.");
    } else {
        window.open(signaturePad.toDataURL());
    }
}

function SignaturePadReady(app, sign_prompt, content, str_message_to_save, void_sign ) {

    var signature_header = "Please provide signature";
    signature_header = sign_prompt;
 
    header = "<head></head> ";
    body = "<div id='signature-pad' class='m-signature-pad'> <div class ='m-signature-pad--header'>" + signature_header + "<div class='description'></div> <div class='m-signature-pad--body'><canvas></canvas></div>";
    footer = "<div class='m-signature-pad--footer'> <div class='description'></div> <div class='left'> <button type='button' class='button clear m-signature-pad--button' data-action='clear'>Clear</button> </div> <div class='right'> <button type='button' class='button save m-signature-pad--button' data-action='save-png'>OK</button> <button type='button' class='button save m-signature-pad--button' data-action='cancel-svg'>Cancel</button></div></div></div>";
    message = header + body + footer;
    get_top().main.enqueue_message({ content: message, actions: {}, onshow: function () { InitSignaturePad(app, content, str_message_to_save, void_sign ); } });

}

function SignaturePad_SignatureData(top_app, signature_data, auto_continue, content, str_message_to_save, void_sign) {

  var msg = str_message_to_save + "||" + content;

    args = {
        app: top_app,
        content: decodeURIComponent(signature_data),
        content_type: "image/png",
        str_message_to_save: msg
    };

    get_top().main.dequeue_message();
    get_top().main.cover();

    post_remote('Device.SignaturePad', 'Device.SignaturePad.receive_html_signature', args, "pos_frame", true);
    
    if (void_sign == false)
      {
        if (auto_continue) {
            get_top().main.form_inst['_method_name.continue'].value = 'continue';
            get_top().main.form_inst.submit();
        }
        else {
            get_top().main.location.reload();
        }
    }
    else
      post_remote('Document.VeriFone_point', 'Business.Document.VeriFone_point.accept_signature', args, 'pos_frame', true); //Bug 24762 - NK added void_sign flag for signature for void reason

}
// End Bug 21870 - Add HTML5 Signature Capture feature