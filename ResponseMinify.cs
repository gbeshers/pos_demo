﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

using WebMarkupMin.Core;
using WebMarkupMin.NUglify;

//
// Bug 20433 UMN PCI-PA-DSS. We need to filter out developer comments per PA-DSS 5.2.5
//

namespace ResponseMinify
{
  public class HttpModule : IHttpModule
  {
    static HtmlMinifier htmlMinifier = new HtmlMinifier(
      new HtmlMinificationSettings(),     // any settings we want to play with
      new NUglifyCssMinifier(),           // The CSS minifier to use
      new NUglifyJsMinifier(),            // The JS minifier to use
      null                                // The logger to use
      );

    void IHttpModule.Init(HttpApplication app)
    {
      app.BeginRequest += AppBeginRequest;
      app.EndRequest += AppEndRequest;
    }

    private void AppBeginRequest(object sender, EventArgs e)
    {
      HttpApplication app = sender as HttpApplication;

      string extension = VirtualPathUtility.GetExtension(app.Context.Request.FilePath);

      // Only filter htm, and js_c files. Assume js/css etc already minified
      if (extension.Equals(".js_c") || extension.Equals(".htm"))
      {
        OutputFilterStream filter = new OutputFilterStream(app.Response.Filter);
        app.Response.Filter = filter;
      }
    }

    private void AppEndRequest(object sender, EventArgs e)
    {
      HttpApplication app = sender as HttpApplication;
      string extension = VirtualPathUtility.GetExtension(app.Context.Request.FilePath);

      // Only filter htm, and js_c files. Assume js/css etc already minified
      if (extension.Equals(".js_c") || extension.Equals(".htm"))
      {
        // Filter whole response
        HttpResponse resp = app.Context.Response;
        string respStr = ((OutputFilterStream)app.Response.Filter).ReadStream();
        resp.Clear();

        MarkupMinificationResult result = htmlMinifier.Minify(respStr, false);

        resp.Write(result.MinifiedContent);
        resp.ContentType = "text/html";
        if (resp.IsClientConnected)
        {
          resp.Flush();
        }
        //app.Context.ApplicationInstance.CompleteRequest();
      }
    }

    void IHttpModule.Dispose()
    {
      // Nothing to dispose; 
    }
  }

  /// <summary>
  /// A stream which keeps an in-memory copy as it passes the bytes through
  /// </summary>
  public class OutputFilterStream : Stream
  {
    private readonly Stream InnerStream;
    private readonly MemoryStream CopyStream;

    public OutputFilterStream(Stream inner)
    {
      this.InnerStream = inner;
      this.CopyStream = new MemoryStream();
    }

    public string ReadStream()
    {
      lock (this.InnerStream)
      {
        if (this.CopyStream.Length <= 0L ||
            !this.CopyStream.CanRead ||
            !this.CopyStream.CanSeek)
        {
          return String.Empty;
        }

        long pos = this.CopyStream.Position;
        this.CopyStream.Position = 0L;
        try
        {
          return new StreamReader(this.CopyStream).ReadToEnd();
        }
        finally
        {
          try
          {
            this.CopyStream.Position = pos;
          }
          catch { }
        }
      }
    }

    public override bool CanRead
    {
      get { return this.InnerStream.CanRead; }
    }

    public override bool CanSeek
    {
      get { return this.InnerStream.CanSeek; }
    }

    public override bool CanWrite
    {
      get { return this.InnerStream.CanWrite; }
    }

    public override void Flush()
    {
      this.InnerStream.Flush();
    }

    public override long Length
    {
      get { return this.InnerStream.Length; }
    }

    public override long Position
    {
      get { return this.InnerStream.Position; }
      set { this.CopyStream.Position = this.InnerStream.Position = value; }
    }

    public override int Read(byte[] buffer, int offset, int count)
    {
      return this.InnerStream.Read(buffer, offset, count);
    }

    public override long Seek(long offset, SeekOrigin origin)
    {
      this.CopyStream.Seek(offset, origin);
      return this.InnerStream.Seek(offset, origin);
    }

    public override void SetLength(long value)
    {
      this.CopyStream.SetLength(value);
      this.InnerStream.SetLength(value);
    }

    public override void Write(byte[] buffer, int offset, int count)
    {
      this.CopyStream.Write(buffer, offset, count);
      this.InnerStream.Write(buffer, offset, count);
    }
  }
}
