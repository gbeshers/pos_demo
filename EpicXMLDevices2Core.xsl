<?xml version="1.0" encoding="UTF-8"?>

<!-- 
    NOTE: This file is for transforming the Epic XML Devices XML elements to xml elements that match the
          the tag names used in a particular project. This file will therefore be different for different
          customers.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:epic="http://schemas.epic.com/xmldevices"
                xmlns:i="http://www.w3.org/2001/XMLSchema-instance"
                version="1.0">
  <xsl:output method="xml" indent="yes"/>

  <xsl:template match="/">
    <xsl:apply-templates select="/epic:TransactionRequest"/>
  </xsl:template>

  <xsl:template match="/epic:TransactionRequest">
    <request>
      <xsl:apply-templates select="epic:MessageId" mode="Seq"/>
      <system>
        <user_id type="transfer">$transfer_user_id</user_id>
        <workgroup_id type="transfer">$transfer_dept_id</workgroup_id>
        <auto_exit>true</auto_exit>
        <finished_buying>true</finished_buying>
        <show_ui>1</show_ui>
      </system>
      <event>
        <field key="source">EpicXMLDevice</field>
      </event>
      <transactions>
        <container>
          <id>1</id>
          <field key="tran_type">$ctt_id</field>
          <field key="skip_inquiry">true</field>
          <field key="REFERENCE_ID">$ext_ref_id</field>
          <field key="auto_done">true</field>
          <xsl:apply-templates select="epic:TransactionType"/>
          <xsl:apply-templates select="epic:Amount" mode="Total"/>
          <group>
            <field key="id">$group_name</field>
            <item>
              <field key="REFERENCE_ID">$ext_ref_id</field>
              <xsl:apply-templates select="epic:MessageId"/>
              <xsl:apply-templates select="epic:AllowPartialPayment"/>
              <xsl:apply-templates select="epic:RequestToken"/>
              <xsl:apply-templates select="epic:Amount"/>
              <!-- UMN TODO re-enable support for Epic custom fields 
              <xsl:apply-templates select="epic:Configuration"/> We'll be ignoring Epic Custom fields for now -->
            </item>
          </group>
        </container>
      </transactions>
    </request>
  </xsl:template>

  <!-- UMN TODO re-enable support for Epic custom fields -->
  
  <!-- TODO: Many of these can be cleaned up with Attribute value templates -->
  <!-- Here Be fields -->
  <xsl:template match="epic:MessageId">
    <field>
      <xsl:attribute name="key">EpicMessageId</xsl:attribute>
      <xsl:value-of select="text()"/>
    </field>
  </xsl:template>

  <xsl:template match="epic:MessageId" mode="Seq">
    <seq_id>
      <xsl:value-of select="text()"/>
    </seq_id>
  </xsl:template>

  <xsl:template match="epic:TransactionType">
    <field>
      <xsl:attribute name="key">EpicTransactionType</xsl:attribute>
      <xsl:value-of select="text()"/>
    </field>
  </xsl:template>

  <xsl:template match="epic:AllowPartialPayment">
    <field>
      <xsl:attribute name="key">EpicAllowPartialPayment</xsl:attribute>sale</field>
  </xsl:template>

  <xsl:template match="epic:RequestToken">
    <field>
      <xsl:attribute name="key">EpicRequestToken</xsl:attribute>
      <xsl:value-of select="text()"/>
    </field>
  </xsl:template>

  <xsl:template match="epic:Amount">
    <field>
      <xsl:attribute name="key">EpicAmtPaid</xsl:attribute>
      <xsl:value-of select='format-number(text(), "0.00")'/>
    </field>
    <field>
      <xsl:attribute name="key">amount</xsl:attribute>
      <xsl:value-of select='format-number(text(), "0.00")'/>
    </field>
  </xsl:template>

  <xsl:template match="epic:Amount" mode="Total">
    <field>
      <xsl:attribute name="key">total</xsl:attribute>
      <xsl:value-of select='format-number(text(), "0.00")'/>
    </field>
  </xsl:template>

</xsl:stylesheet>