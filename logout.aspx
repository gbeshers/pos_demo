﻿<!-- This page is used to terminate a session after the associated user is finished. -->
<!-- WARNING: If you're customizing the logoff message, note that images are NOT supported, unless you can load them somehow without
     unintentionally starting a new session-->
<!-- Mike O -->
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="logout.aspx.cs" %>
<%

  // moved into function so can debug. It's in transuite/TwoFactorAuthenticator.cs
  TranSuiteServices.LogOut.Logout(Request, Response);

%>
